/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
import { LightningElement, api, track } from "lwc";

import LABEL_REQUIRED_VALUE from "@salesforce/label/c.Scheduled_email_ctrl_select_required_value";
import LABEL_MIN_ERROR_MESSAGE from "@salesforce/label/c.Scheduled_email_ctrl_select_error_min_value";
import LABEL_DATE_PLACEHOLDER from "@salesforce/label/c.Scheduled_email_ctrl_select_date_placeholder";
import { normalizeBoolean } from "c/utils";

export default class DateTimePicker extends LightningElement {
	_min;
	_date;
	_time;
	_amOrPm;
	errorMessage;
	dirty = false;
	@track _disabled = false;
	@track _readOnly = false;
	@track _required = false;
	@api
	get min() {
		return this._min;
	}
	set min(value) {
		const newMin = new Date(value);
		if (this._min !== newMin) {
			this._min = new Date(value);
			this.checkIsInValid();
		}
	}
	@api
	get date() {
		return this._date;
	}
	set date(value) {
		if (this._date !== value) {
			this._date = value;
		}
	}
	@api
	get time() {
		return this._time;
	}
	set time(value) {
		if (this._time !== value) {
			this._time = value;
		}
	}
	@api
	get amOrPm() {
		return this._amOrPm;
	}
	@api
	get disabled() {
		return this._disabled || this._readOnly || false;
	}
	set disabled(value) {
		this._disabled = normalizeBoolean(value);
	}
	@api
	get readOnly() {
		return this.disabled;
	}
	set readOnly(value) {
		this._readOnly = normalizeBoolean(value);
	}
	set amOrPm(value) {
		if (this._amOrPm !== value) {
			this._amOrPm = value;
		}
	}
	@api
	get required() {
		return this._required;
	}
	set required(value) {
		this._required = normalizeBoolean(value);
	}

	get isNotUndefined() {
		return this.date && this.time && this.amOrPm;
	}
	get datePlaceholder() {
		return LABEL_DATE_PLACEHOLDER;
	}
	get showErrorMessage() {
		return this.errorMessage && this.dirty;
	}
	checkIsInValid() {
		if (!this.isNotUndefined && this.required) {
			this.errorMessage = LABEL_REQUIRED_VALUE;
			this.triggerValidityChange(false);
			return true;
		}
		if (!this.validateDate()) {
			this.errorMessage = `${LABEL_MIN_ERROR_MESSAGE} ${this.min}`;
			this.triggerValidityChange(false);
			return true;
		}
		this.errorMessage = undefined;
		this.triggerValidityChange(true);
		return false;
	}
	handleDateChange(event) {
		event.stopPropagation();
		this._date = event.target.value;
		if (!this.checkIsInValid()) {
			this.triggerEvent();
		}
	}
	handleTimeChange(event) {
		event.stopPropagation();
		if (event.detail) {
			this._time = event.detail.time;
			this._amOrPm = event.detail.amOrPm;
			if (!this.checkIsInValid()) {
				this.triggerEvent();
			}
		}
	}
	handleFocus(event) {
		event.stopPropagation();
		this.dirty = true;
	}

	validateDate() {
		if (!this.min) {
			return true;
		}
		if (!this.isNotUndefined) {
			return false;
		}
		const dateStr = this.date + " " + this.time + ":00:00 " + this.amOrPm;
		const selectedDateTime = new Date(dateStr);
		selectedDateTime.setHours(this.convertTimeToTwentyFour(), 0, 0, 0);
		return selectedDateTime >= this.min;
	}
	convertTimeToTwentyFour() {
		let hour = this.time;
		if (this._time === "12") {
			hour = "00";
		}
		return this.amOrPm === "PM" ? +hour + 12 : +hour;
	}
	triggerEvent() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: {
					date: this.date,
					time: this.time,
					amOrPm: this.amOrPm
				}
			})
		);
	}
	triggerValidityChange(isValid) {
		this.dispatchEvent(
			new CustomEvent("validitychange", {
				detail: {
					valid: isValid
				}
			})
		);
	}
}