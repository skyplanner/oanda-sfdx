/**
 * @description       : 
 * @author            : Fernando Gomez
 * @group             : 
 * @last modified on  : 11-09-2022
 * @last modified by  : Yaneivys Gutierrez
**/
import { LightningElement, wire, api, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import CASE_ACCOUNT_FIELD
	from '@salesforce/schema/Case.AccountId';
import CASE_FXACCOUNT_FIELD
	from '@salesforce/schema/Case.fxAccount__c';
import CASE_CONTACT_FIELD
	from '@salesforce/schema/Case.ContactId';
import CASE_CONTACT_EMAIL_FIELD
	from '@salesforce/schema/Case.Contact.Email';
import CASE_FXACCOUNT_CODE_FIELD
	from '@salesforce/schema/Case.fxAccount__r.Authentication_Code__c';
import CASE_ACCOUNT_CODE_FIELD
		from '@salesforce/schema/Case.Account.Authentication_Code__c';
import CASE_FXACCOUNT_CODE_EXPIRATION_FIELD
	from '@salesforce/schema/Case.fxAccount__r.Authentication_Code_Expires_On__c';
import CASE_ACCOUNT_CODE_EXPIRATION_FIELD
	from '@salesforce/schema/Case.Account.Authentication_Code_Expires_On__c';

import sendEmail
	from '@salesforce/apex/CaseAuthenticationCodeResource.sendAuthenticationCodeEmail';

export default class CaseClientAuthentication extends LightningElement {
	@api
	recordId;

	@api
	email;

	codeRecordId;

	objectApiName;

	clientAuthCode;

	clientAuthCodeExpiresOn;

	isValidCode;
	
	isEmailDisabled;

	isEmailConfirmationActive;

	isEmailSucess;

	isEmailError;

	emailErrorMsg;

	emailMsgTurnOff;

	@wire(getRecord, {
		recordId: '$recordId',
		fields: [
			CASE_ACCOUNT_FIELD,
			CASE_FXACCOUNT_FIELD,
			CASE_CONTACT_FIELD,
			CASE_CONTACT_EMAIL_FIELD,
			CASE_FXACCOUNT_CODE_FIELD,
			CASE_ACCOUNT_CODE_FIELD,
			CASE_FXACCOUNT_CODE_EXPIRATION_FIELD,
			CASE_ACCOUNT_CODE_EXPIRATION_FIELD
		]
	})
	currentCase({ error, data }) {
		if (data) {
			this.email = getFieldValue(data, CASE_CONTACT_EMAIL_FIELD);
			this.codeRecordId = getFieldValue(data, CASE_FXACCOUNT_FIELD);
			this.objectApiName = 'fxAccount__c';
			
			// if we don't have an fxaccount, we use the account
			if (!this.codeRecordId) {
				this.codeRecordId = getFieldValue(data, CASE_ACCOUNT_FIELD);
				this.objectApiName = 'Account';
			}

			console.log(this.codeRecordId)
			var fxAccountCode = getFieldValue(data, CASE_FXACCOUNT_CODE_FIELD);
			var accountCode = getFieldValue(data, CASE_ACCOUNT_CODE_FIELD);
			if (fxAccountCode) {
				this.clientAuthCode = fxAccountCode;
			}
			else if (accountCode) {
				this.clientAuthCode = accountCode;
			}
			else {
				this.clientAuthCode = null;
			}

			var fxAccountCodeExp = getFieldValue(data, CASE_FXACCOUNT_CODE_EXPIRATION_FIELD);
			var accountCodeExp = getFieldValue(data, CASE_ACCOUNT_CODE_EXPIRATION_FIELD);
			if (fxAccountCodeExp) {
				this.clientAuthCodeExpiresOn = fxAccountCodeExp;
			}
			else if (accountCodeExp) {
				this.clientAuthCodeExpiresOn = accountCodeExp;
			}
			else {
				this.clientAuthCodeExpiresOn = null;
			}

			this._reset();
		} else if (error)
			console.error(error);
	}

	handleCodeGenerated(e) {
		this.clientAuthCode = e.detail.Authentication_Code__c;
		this.clientAuthCodeExpiresOn = e.detail.Authentication_Code_Expires_On__c;
		this._reset();
	}

	handleCodeExpired(e) {
		this.clientAuthCode = null;
		this.clientAuthCodeExpiresOn = null;
		this._reset();
	}

	handleSendEmailClick(e) {
		this.isEmailSucess = false;
		this.isEmailError = false;
		this.emailErrorMsg = null;
		this.isEmailConfirmationActive = true;
	}

	handleSendEmailCancelClick(e) {
		this.isEmailConfirmationActive = false;
	}

	/**
	 * Lanched after email confirmation.
	 * Sends the code 
	 * @param {*} e 
	 */
	handleSendEmailConfirmClick(e) {
		this.isLoading = true;
		this.isEmailSucess = false;
		this.isEmailError = false;
		this.isEmailConfirmationActive = false;
		this.emailErrorMsg = null;

		let turnOff = () => {
			if (this.emailMsgTurnOff)
				clearTimeout(this.emailMsgTurnOff);

			this.emailMsgTurnOff = setTimeout(() => {
				this.isEmailSucess = false;
				this.isEmailError = false;
				this.emailErrorMsg = null;
			}, 20000);
		};

		sendEmail({
			caseId: this.recordId
		})
		.then(result => {
			this.isLoading = false;
			this.isEmailSucess = true;
			turnOff();
		})
		.catch(error => {
			console.error(error);
			this.isLoading = false;
			this.isEmailError = true;
			this.emailErrorMsg = 
				Array.isArray(error.body) ?
					error.body.reduce(acc, curr =>
						(acc ? acc + ". " : "") + curr.message) :
				(error.body.message || "There was an unexpected problem");
			turnOff();
		});
	}

	_reset() {
		this.isValidCode = this.clientAuthCode != null &&
			this.clientAuthCodeExpiresOn != null &&
			new Date() < new Date(this.clientAuthCodeExpiresOn);

		this.isEmailDisabled = this.email == null;
		this.isEmailConfirmationActive = false;
		this.isLoading = false;
		this.isEmailSucess = false;
		this.isEmailError = false;
		this.emailErrorMsg = null;
	}
}