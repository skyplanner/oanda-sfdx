import { LightningElement, api, wire } from 'lwc';
import getTranscriptEvents from '@salesforce/apex/ChatTranscriptEventsController.getTranscriptEvents';

const columns = [
     { label: 'Time', fieldName: 'Time', type: 'date', sortable: "true",
          typeAttributes: {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
          },
    },
    { label: 'Agent', fieldName: 'AgentName', sortable: "true"},
    { label: 'Type', fieldName: 'Type', sortable: "true" },
    { label: 'Detail', fieldName: 'Detail', sortable: "true" }
];

export default class ChatTranscriptEventRelatedList extends LightningElement {

    @api recordId;
    data = [];
    columns = columns;
    noRecords=false;
    sortBy='Time';
    sortDirection='asc';

    connectedCallback() {
         getTranscriptEvents({recordId: this.recordId}).then( result =>{
             this.data = result;
             if(this.data==undefined || this.data.length==0) this.noRecords=true;
             for (var i = 0; i < this.data.length; i++) {
                var row = this.data[i];
                if (row.AgentId) {
                    row.AgentName = row.Agent.Name;
                }
            }
        });
    }

    sortRecords(event) {
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
    }

    sortData(fieldname, direction) {
        let parseData = JSON.parse(JSON.stringify(this.data));
        let keyValue = (a) => {
            return a[fieldname];
        };
        let isReverse = direction === 'asc' ? 1: -1;
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : '';
            y = keyValue(y) ? keyValue(y) : '';
            return isReverse * ((x > y) - (y > x));
        });
        this.data = parseData;
    }

}