import { LightningElement,api } from 'lwc';
import rerunBatch from '@salesforce/apex/BatchApexErrorHandler.rerunBatch';
import checkBatchStatus from '@salesforce/apex/BatchApexErrorHandler.checkBatchStatus';
import { CloseActionScreenEvent } from 'lightning/actions';


export default class RerunBatchJobLWC extends LightningElement {


    @api recordId;
    result ='';
    isInitialized = false;
    title = 'Rerun Batch Job';
    loading=true;
    batchSize=200;
    isRerun = false;
    errorMessage;
    
    renderedCallback() {
        if(!this.isInitialized && this.recordId!=undefined){
            console.log('record  id: '+this.recordId);
            //do we need to check it at the beggining? (it slows down loading LWC significantly)
            checkBatchStatus({ 
                recordId: this.recordId
            }).then( result => {
                if(result!='ok'){
                    this.result=result;
                    this.isRerun=true;
                }
                this.isInitialized=true;
                this.loading=false;
            });
        }
    }

    handleRerun(){
        this.errorMessage='';
        this.loading=true;
        console.log('handle rerun');
        if(this.batchSize!=null && this.batchSize>0 && this.batchSize<200001){
            rerunBatch({ 
                recordId: this.recordId,
                batchSize: this.batchSize
            }).then( result => {
                this.result=result;
                this.loading=false;
                this.isRerun=true;
            })
            .catch(error => {
                this.result=error;
                this.loading=false;
                this.isRerun=true;
            });
        }else{
            this.errorMessage='Batch size must be between 1-20000.';
            this.loading=false;
            this.isRerun=false;

        }
        
    }
    
    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    handleBatchSizeChange(event){
        this.batchSize=event.target.value;
    }

}