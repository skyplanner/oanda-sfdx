import { LightningElement, api } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import execute from '@salesforce/apex/ComplyAdvantageHelper.sendToComplyAdvantage';

/**
 * Creates or updates the comply advantage serach for the record
 * @author Fernando Gomez
 * @version 1.0
 */
export default class SendToComplyAdvantageAction extends LightningElement {
	@api
	recordId;

	// attrs
	isLoading = false;
	isDisabled = false;
	isDone = false;
	isSuccess;
	message;

	/**
	 * User confirmed, so we proceed
	 */
	handleContinueClick() {
		this.isLoading = true;
		this.isDisabled = true;

		execute({
			recordId: this.recordId
		}).then(r => {
			this.isDone = true;
			this.isSuccess = true;
		})
		.catch(error => {
			this.isDone = true;
			this.isSuccess = false;
			this.message = (error && error.body && error.body.message) ||
				"There was an issue and the operation could not be completed.";
			console.error(error);
		});
	}

	/**
	 * User wants out, we close this thing
	 */
	handleCancelClick() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}
}