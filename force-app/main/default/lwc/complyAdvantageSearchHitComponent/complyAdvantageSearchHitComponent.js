import { LightningElement ,api, track } from 'lwc';

export default class ComplyAdvantageSearchHitComponent extends LightningElement {
    
    @track showCommentsDialog = false;
    @track showMatchDetailsDialog = false;

    isInternalHit = false;
    isMatchCommentDialog= false;
    badgeCSSClass = 'badge';

    @api match
    @api selectAllHandler(eventData)
    {
        this.match.isSelected = eventData.selectAll;
    }
            
    connectedCallback()
    {
        if(this.match.amlTypes === 'Internal-Hit' )
        {
            this.isInternalHit = true;
        }
        if(this.match.monitorStatus == 'Added')
        {
            this.badgeCSSClass = 'monitor-status-badge badge-added';
        }
        if(this.match.monitorStatus == 'Updated')
        {
            this.badgeCSSClass = 'monitor-status-badge badge-updated';
        }
        if(this.match.monitorStatus == 'Removed')
        {
            this.badgeCSSClass = 'monitor-status-badge badge-removed';
        }
    }

    selectMatch(event)
    {
        const selectedEvent = new CustomEvent('matchselectionchange', 
        { 
            detail : 
            {
                isSelected: event.target.checked,
                matchid :  this.match.entityId
            }
        });
        this.dispatchEvent(selectedEvent);
    }

    get whiteListIconName() 
    {
        return this.match.isWhiteListed ? 'action:approval' : 'action:close';
    }
    
   openCommentsDialog(events)
   {
       this.isMatchCommentDialog = true
       this.showCommentsDialog = true;
   }
   hideCommentsDialog(events)
   {
      this.isMatchCommentDialog = false
      this.showCommentsDialog = false;
   }
   
   newCommentHandler(event)
   {
       let matchInfo =  {...this.match};

       matchInfo.commentsCount = matchInfo.commentsCount || 0
       matchInfo.commentsCount += 1

       this.match = matchInfo
       this.showCommentsDialog = false;
       
       //notify parent to update case count
       const newCommentEvent = new CustomEvent('newmatchcomment')
       this.dispatchEvent(newCommentEvent);
   }

   openMatchDetailsDialog(events)
   {
       this.showMatchDetailsDialog = true;
   }
   hideMatchDetailsDialog(events)
   {
      this.showMatchDetailsDialog = false;
   }
}