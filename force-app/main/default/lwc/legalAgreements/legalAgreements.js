import { LightningElement, api } from 'lwc';
import fetchAgreements from '@salesforce/apex/LegalAgreementsCtrl.fetchAgreements';

const COLUMNS = [
    {
        type: 'text',
        fieldName: 'name',
        label: 'Name',
    },
    { 
        label: 'Version',
        fieldName: 'version',
        type: 'text',
    },
    { 
        label: 'Updated',
        fieldName: 'update_Datetime',
        type: 'date',
        typeAttributes:{
            year: "numeric",
            month: "numeric",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        }
    }
];

export default class LegalAgreements extends LightningElement {

    columns = COLUMNS;
    legalAs;
    error;
    loaded;
    @api recordId;
    @api header;

    connectedCallback() {
        this.fetchAgreements();
    }

    fetchAgreements(){
        console.log('fetchAgreements...');
        this.loaded = false;
        fetchAgreements({fxAccId: this.recordId})
            .then(data => {
                console.log('legal agrements:', data);

                if (data.length) {
                    this.legalAs = data.map(function(row) {
                        let newrow = Object.assign({}, row);
                        
                        if(newrow.children &&
                            newrow.children.length)
                                newrow._children = newrow.children;
                        
                        return newrow;
                    });
                } else
                    this.legalAs = [];

                this.loaded = true;
            })
            .catch(error => {
                console.log('error ===> ', error);
                this.error = error.body.message;
                this.legalAs = undefined;
                this.loaded = true;
            });
    }

    refresh(){
        console.log('refresh...');
        this.fetchAgreements();
    }

    get hasLegalAs(){
        return this.legalAs && this.legalAs.length;
    }

}