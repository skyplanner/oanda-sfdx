import { LightningElement, api} from 'lwc';

export default class ComboboxMultiItem extends LightningElement {
    @api item;
    @api checked;

    handleClick(event){
        event.stopPropagation();
        event.preventDefault();
        this.checked = !this.checked;
        this.dispatchCheckedAction(this.checked);
    }

    dispatchCheckedAction(checked) {
        let eventName = checked ? 'checked':'unchecked';
        const selectedvalue = new CustomEvent(eventName, {
            detail: this.item
        });
        this.dispatchEvent(selectedvalue);
    }    
}