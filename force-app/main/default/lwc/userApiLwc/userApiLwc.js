/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 07-03-2023
 * @last modified by  : Mikolaj Juras
**/
import { LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import init from '@salesforce/apex/UserApiLwcCtrl.init';
import getUserInfo from '@salesforce/apex/UserApiLwcCtrl.getUserInfo';
import getPermissions from '@salesforce/apex/UserApiLwcCtrl.getPermissions';
import searchSf from '@salesforce/apex/UserApiLwcCtrl.searchSf';
import searchExt from '@salesforce/apex/UserApiLwcCtrl.searchExternal';
import saveSf from '@salesforce/apex/UserApiLwcCtrl.saveSf';
import saveExt from '@salesforce/apex/UserApiLwcCtrl.saveExternal';
import getV20AccsDetails from '@salesforce/apex/UserApiLwcCtrl.getV20AccsDetails';
import getMT5AccsDetails from '@salesforce/apex/UserApiLwcCtrl.getMT5AccsDetails';
import getAuditTrails from '@salesforce/apex/UserApiLwcCtrl.getAuditTrails';
import getHistoricalAuditLogs from '@salesforce/apex/UserApiLwcCtrl.getHistoricalAuditLogs';
import callAction from '@salesforce/apex/UserApiLwcCtrl.callAction';
import updatev20Account from '@salesforce/apex/UserApiLwcCtrl.updatev20Account';
import resendMT4Notification from '@salesforce/apex/UserApiLwcCtrl.resendMT4Notification';
import MT4_LOGO from '@salesforce/resourceUrl/mt4_logo';
import lockUnlockMt5Acc from '@salesforce/apex/UserApiLwcCtrl.lockUnlockMt5Acc';

const auditTrailsColumns = [
    { 
        label: 'Time', 
        fieldName: 'Timestamp__c',  
        type: 'date', 
        sortable: true,
        initialWidth: 200,
        typeAttributes: {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
        }
    },
    { 
        label: 'User', 
        fieldName: 'User_Url__c',
        type: 'url',
        initialWidth: 200,
        typeAttributes: {label: { fieldName: 'User_Full_Name__c' }}
    }, 
    { 
        label: 'Field',
        fieldName: 'Field__c',
        initialWidth: 200 
    },
    { 
        label: 'Original Value',
        fieldName: 'Original_Value__c',
        initialWidth: 200 
    },
    { 
        label: 'New Value',
        fieldName: 'New_Value__c',
        initialWidth: 200
    },
    { 
        label: 'Action',
        fieldName: 'Action__c',
        type: 'text',
        wrapText: true
    }
];

const auditLogsColumns = [
    { label: 'Time', fieldName: 'createdOn' },
    { label: 'Table', fieldName: 'tableName' },
    { label: 'Field', fieldName: 'field' },
    { label: 'Old Value', fieldName: 'old_value' },
    { label: 'New Value', fieldName: 'new_value' },
    { label: 'Description', fieldName: 'description' },
    { label: 'Admin', fieldName: 'user' }
];

const piuOutdatedStatusName = 'PIU Outdated';

export default class UserApiLwc extends LightningElement {
    mt4Logo = MT4_LOGO;
    status;
    action;
    error;
    extCriteria;
    sfCriteria;
    records = [];
    @track detail = {};
    originalRecord = {};
    toUpdate = {};
    actions = [];
    extFields = [];
    sfFields = [];
    columns = [];
    extColumns = [];
    sfColumns = [];
    statuses = [];
    rowSelected = {};
    rowLimit = 50;
    rowOffSet = 0;
    loadMoreStatus;
    loading = true;
    hasRecords = false;
    disableSave = true;
    isSfSearch = false;
    isExtSearch = false;
    showPIUOutdatedBadge = false;
    piuBadgeLabel = piuOutdatedStatusName;
    searchDisabled = true;
    recordsSecVisible = false;
    detailsSecVisible = false;
    totalNumberOfRows = 2000;
    gettingv20Accounts = false;
    enableInfiniteLoading = true;
    isDetailsSectionBlocked= false;
    useDetailFields = false;
    isNoSuchUser = false;
    isBizOpsL2Profile = false;
    @track v20AccDetails = [];
    @track mT5AccDetails = [];
    @track gettingAuditTrails = false;
    auditTrailsColumns = auditTrailsColumns;
    auditTrailDetails = [];
    auditLogsColumns = auditLogsColumns;
    auditLogs = [];
    gettingAuditLogs = false;
    @track permSave = false;
    @track permSaveV20Accounts = false;
    @track permResendMT4Notif = false;
    gettingMt5Accs = false;
    @track activeSections;
    selectedRowEnv;

    get fields() {
        return this.isExtSearch || this.useDetailFields ?
            this.extFields :
            this.sfFields;
    }

    get mt5Columns() {
        let columns = [
            { label: 'Account ID', fieldName: 'mt5UserID'},
            { label: 'Locked', fieldName: 'isDisabled'},
            {   type:"button",
                label: "Lock/Unlock",
                
                typeAttributes: {
                    name: 'lockunlock',
                    variant: 'brand',
                    label: {fieldName: 'lockUnlockButtonLabel' },
                    disabled : {fieldName: 'userCanPerformAction' }
                }
            },
            { label: 'Balance', fieldName: 'balance' },
            { label: 'Currency', fieldName: 'accountCurrency' },
            { label: 'Type', fieldName: 'accountType' },
            { label: 'Shares Enabled', fieldName: 'sharesEnabled' }
        ];

        if(this.isBizOpsL2Profile) {
            columns.push({ label: 'Book', fieldName: 'book' });
        }

        return columns;
    }

    connectedCallback() {
        this.init();
        this.getPermissions();
    }

    init() {
        init()
            .then(data => {
                this.extFields = data.extFields;
                this.sfFields = data.sfFields;
                this.isBizOpsL2Profile = data.isBizOpsL2Profile;
                this.loading = false;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    updatePermissions(env) {
        this.permSave = 
            this.permissions[env]['save'];
        this.permSaveV20Accounts = 
            this.permissions[env]['save-v20-account'];
        this.permResendMT4Notif = 
            this.permissions[env]['resend-mt4'];
    }

    handleRowAction(event) {
        let row = event.detail.row;

        let env = this.isSfSearch 
            ?  (row["Record_Type_Name_Formula__c"] == 'Retail Live' 
                ? 'live' 
                : (row["Record_Type_Name_Formula__c"] == 'Retail Practice' 
                    ? 'demo' 
                    : row["Record_Type_Name_Formula__c"]))
            : row["Env"];
            
        this.selectedRowEnv = env;
        let fxTradeUserId = row['fxTrade_User_Id__c'];
        let fxTradeName = row['Name'];
        let email = row['Email'];
        let emailSF = row['Email__c'];

        this.isDetailsSectionBlocked = true
        this.useDetailFields = true;
        this.getUserInfo(env, fxTradeUserId, fxTradeName);
        this.updatePermissions(env);
        this.getV20AccsDetails(env, fxTradeUserId);
        this.getHistoricalAuditLogs(env, fxTradeUserId);
        let currentEmail = email ? email : emailSF;
        this.getMT5AccsDetails(env, fxTradeUserId, currentEmail);
        this.activeSections = ['v20Accounts','mt5Accs'];
    }

    buildDetailsInfo(row, picklistOptions) {
        const dataRow = row; 
        this.rowSelected = dataRow;
        var temp = {
            Id: dataRow['Id'],
            fxTrade_User_Id__c: dataRow['fxTrade_User_Id__c'],
            Name: dataRow['Name'],
            Env: dataRow["Env"], 
            Email : dataRow["Email"], 
            fields: []
        };
        var row;
        var fieldsShow = [];

        this.fields.forEach(function(f) {
            if (f.showOnDetails)
                fieldsShow.push(f);
        });

        function buildItem(data, f, picklists) {
            var item = Object.assign({}, f);
            item.value = item.isPicklist ? '' + data[f.name] : data[f.name];

            if (item.isPicklist)
                item.options = picklists[item.name] 
                    ? picklists[item.name] : [{}];

            return item;
        };

        for (var i = 0; i < fieldsShow.length; i += 2) {
            row = {};
            row.rowIndex = i/2;
            var f1 = fieldsShow[i];
            row.f1 = buildItem(dataRow, f1, picklistOptions);
            var f2 = fieldsShow[i + 1];
            var noNext = typeof f2 === 'undefined';
            row.f2 = noNext ? {} : buildItem(dataRow, f2, picklistOptions);
            temp.fields.push(row);
        }

        this.detail = temp;
    }

    loadMoreData(event) {
        if (this.records.length % this.rowLimit != 0) { 
            //if number of records is not rounding to rowLimit it means its the end of results
            this.setMaximumRecordsDisplayedInfo();
        } else {
            event.target.isLoading = true;
            this.rowOffSet = this.rowOffSet + this.rowLimit;
            this.search(event.target, true);
        }
    }

    search(target, loadMore) {
        if (!this.fields.length)
            this.showError('Error', 'There are no fields configured.');
        else {
            this.enableInfiniteLoading = true;
            if (!loadMore) {
                this.isDetailsSectionBlocked = false;
                this.loading = true;
                this.rowOffSet = 0;
                this.records = [];
                this.loadMoreStatus = '';
                this.recordsSecVisible = false;
                this.detailsSecVisible = false;
                this.assignColumns();
            }
            if (this.isSfSearch)
                this.performSFSearch(target, loadMore);
            else if (this.isExtSearch)
                this.performExternalSearch(target, loadMore);
        }
    }
    
    performSFSearch(target, loadMore) {
        var criteria = {
            criteria: this.sfCriteria,
            limitSize: this.rowLimit,
            offset : this.rowOffSet
        };

        searchSf(criteria)
            .then(data => {
                this.onSearchResults(target, loadMore, data);
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    performExternalSearch(target, loadMore) {
        var criteria = {
            criteria: this.extCriteria,
            limitSize: this.rowLimit,
            offset : this.rowOffSet
        };
        searchExt(criteria)
            .then(data => {
                this.onSearchResults(target, loadMore, data);
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    onSearchResults(target, loadMore, data) {
        if (loadMore) {
            target.isLoading = false;
            if (!data.length) {
                //thats for scenario where we have results rounding to rowLimit
                //and loadMore is not gonna return any more records
                this.setMaximumRecordsDisplayedInfo();
            } else {
                this.records = this.records.concat(data);
                if(this.records.length >= this.totalNumberOfRows) {
                    this.setMaximumRecordsDisplayedInfo();
                }
            }
        } else {
            this.records = data;
            this.loading = false;
        }

        this.hasRecords = this.records.length > 0;

        if(!this.isDetailsSectionBlocked) {
            this.recordsSecVisible = true;
            this.detailsSecVisible = false;
        }
    }

    setMaximumRecordsDisplayedInfo() {
        this.enableInfiniteLoading = false;
        this.loadMoreStatus = this.records.length +
            ' Results (maximum displayed)';
    }

    save() {
        this.loading = true;

        this.saveRecordExt();
    }

    saveRecord() {
        this.toUpdate['Id'] = this.detail.Id;
       
        saveSf({record: this.toUpdate})
            .then(data => {
               Object.assign(this.rowSelected, this.toUpdate);
                this.toUpdate = {};
                this.disableSave = true;
                this.loading = false;
                this.showMsg('Save',
                    'The record was updated successfully.');
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }
    
    saveRecordExt() {
        this.toUpdate['fxTrade_User_Id__c'] 
                = this.detail['fxTrade_User_Id__c'];
        this.originalRecord = {};
        Object.assign(this.originalRecord, this.rowSelected);

        saveExt({
            record: this.toUpdate,
            oldRecord: this.originalRecord})
            .then(data => {
                Object.assign(this.rowSelected, this.toUpdate);
                this.toUpdate = {};
                this.disableSave = true;
                this.loading = false;
                this.showMsg('Save',
                    'The record was updated successfully.');
                this.getAuditTrailDetails();
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    reset() {
        this.rowOffSet = 0;
        this.records = [];
        this.toUpdate = {};
        this.error = null;
        this.disableSave = true;
        this.recordsSecVisible = false;
        this.detailsSecVisible = false;
        this.isDetailsSectionBlocked = false;
        this.enableInfiniteLoading = true;
        this.template.querySelector(
            'c-user-api-search-lwc').reset();
    }

    backToList() {
        this.toUpdate = {};
        this.disableSave = true;
        this.isDetailsSectionBlocked = false;
        this.recordsSecVisible = true;
        this.detailsSecVisible = false;
    }

    handleEnter(event) {
        if (event.keyCode === 13)
            this.search();
    }

    getUserInfo(env, fxTradeUserId, fxTradeName) {
        this.loading = true;

        getUserInfo({
            env: env,
            fxTradeUserId: fxTradeUserId,
            fxTradeName: fxTradeName})
            .then(data => {
                this.statuses = data.statuses;
                this.status = data.status;

                this.showPIUOutdatedBadge = this.statuses.userStatuses
                    .find(s => s.name === piuOutdatedStatusName)?.status;

                this.actions = data.actions.map((a, index) => {
                    return { ...a,
                        actionIndex: index,
                        buttonAction: a.action == 'Button', 
                        linkAction: a.action == 'Link' }
                });

                this.buildDetailsInfo(data.record, data.groups);
                this.getAuditTrailDetails();

                this.detailsSecVisible = true;
                this.recordsSecVisible = false;
                this.disableSave = true;
                this.toUpdate = {};
                this.useDetailFields = false;

                this.loading = false;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    getPermissions() {
        getPermissions()
            .then(data => {
                this.permissions = data;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
            });
    }

    getV20AccsDetails(env, fxTradeUserId) {
        this.gettingv20Accounts = true;

        getV20AccsDetails({
            env: env,
            fxTradeUserId: fxTradeUserId})
            .then(data => {
                if (data === null) {
                    this.isNoSuchUser = true;
                } else {
                    this.isNoSuchUser = false;
                    this.v20AccDetails = data;
                }
                this.gettingv20Accounts = false;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.gettingv20Accounts = false;
            });
    }

    getMT5AccsDetails(env, fxTradeUserId, email) {
        this.gettingMt5Accs = true;
            getMT5AccsDetails({
                env: env,
                fxTradeUserId: fxTradeUserId,
                email : email})
                .then(data => {
                    this.mT5AccDetails = data.map(row => {
                        let newRow = {...row};
                        newRow.lockUnlockButtonLabel = row.isDisabled ? 'Unlock' : 'Lock';
                        let accType = this.selectedRowEnv == 'live' ? 'Live' : 'Demo;'
                        newRow.env = accType.toLocaleLowerCase();
                        let permissionName = 'UAPI_' + newRow.lockUnlockButtonLabel + '_mt5_' + accType;
                        newRow.userCanPerformAction = this.permissions[accType.toLocaleLowerCase()][permissionName] != null 
                                                    ? !this.permissions[accType.toLocaleLowerCase()][permissionName]
                                                    : true;
                        return newRow;
                    });
                })
                .catch(error => {
                    this.showError('Error', error.body.message);
                }).finally(() => {
                    this.gettingMt5Accs = false;
                });        
    }

    getAuditTrailDetails() {
        this.gettingAuditTrails = true;

        getAuditTrails({
            env: this.detail['Env'],
            fxTradeUserId: this.detail['fxTrade_User_Id__c']})
            .then(data => {
                this.auditTrailDetails = data;
                this.auditTrailListVisible = data != null && data.length > 0;
                this.gettingAuditTrails = false;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.gettingAuditTrails = false;
            });
    }

    getHistoricalAuditLogs(env, fxTradeUserId) {
        this.gettingAuditLogs = true;

        getHistoricalAuditLogs({
            env: env,
            fxTradeUserId: fxTradeUserId})
            .then(data => {
                if (data) {
                    this.auditLogs = data.map((item, index) => {
                        return {
                            ...item,
                            key: index
                        }
                    });
                }
                this.gettingAuditLogs = false;
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.gettingAuditLogs = false;
            });
    }

    assignColumns() {
        if (this.isSfSearch) {
            this.sfColumns = this.sfColumns.length ?
                this.sfColumns : this.buildSfColumns();
            this.columns = this.sfColumns;
        } else if (this.isExtSearch) {
            this.extColumns = this.extColumns .length ?
                this.extColumns : this.buildExtColumns();
            this.columns = this.extColumns;
        }
    }

    buildSfColumns() {
        return this.buildColumns(this.sfFields);
    }

    buildExtColumns() {
        return this.buildColumns(this.extFields);
    }

    buildColumns(fields) {
        var cols= [
            {
                label: 'View',
                type: 'button-icon',
                initialWidth: 75,
                typeAttributes: {
                    iconName: 'action:preview',
                    title: 'Preview',
                    variant: 'border-filled',
                    alternativeText: 'View'
                }
            }
        ];

        fields.forEach(function(field) {
            if (field.showOnList)
                cols.push(
                    {
                        fieldName: field.name,
                        label: field.label,
                        type: field.isCheck ? 'boolean' : 'text'
                    });
        });
        return cols;
    }

    callAction(event) {
        this.loading = true;
        var actionLabel = event.target.dataset.actionLabel;
        var actionName = event.target.dataset.actionName;
        var className = event.target.dataset.actionClass;

        callAction({
                'actionName': actionName,
                'actionLabel': actionLabel,
                'actionManagerClass': className,
                'record': this.detail
            })
            .then(data => {
                this.showMsg(actionLabel, data);
                this.loading = false;
                this.useDetailFields = true;
                this.getUserInfo(
                    this.detail['Env'],
                    this.detail['fxTrade_User_Id__c'],
                    this.detail['Name']
                );
                this.getAuditTrailDetails();
            })
            .catch(error => {
                this.showError('Error', error.body.message);
                this.loading = false;
            });
    }

    updateCriteria(e) {
        var data = e.detail;        
        this.extCriteria = data.extCriteria;
        this.sfCriteria = data.sfCriteria;
        this.isExtSearch = data.isExtSearch;
        this.isSfSearch = data.isSfSearch;
        this.searchDisabled = !data.isOkSearch;
    }

    handleFieldUpdated(e) {
        this.disableSave = false;
        this.toUpdate[e.detail.field] = e.detail.value;
	}

    updatev20Account(e) {
        this.gettingAuditTrails = true;
        let index = e.target.dataset.v20AccountIndex;
        let item = this.v20AccDetails[index];

        updatev20Account({
            request: {
                env: this.detail['Env'],
                fxTradeUserId: this.detail['fxTrade_User_Id__c'],
                fxAccountId: this.detail['fxAccountId'],
                v20AccountId: item.accountID,
                divisionTradingGroupId: item.divisionTradingGroupID,
                prevDivisionTradingGroupId: item.originalDivisionTradingGroupID,
                mt4ServerId: item.mt4ServerID,
                prevMt4ServerId: item.originalMt4ServerID
            }
        }).then(data => {
            item.unchanged = true;
            item.dtgChanged = false;
            item.serverChanged = false;
            item.originalDivisionTradingGroupID = item.divisionTradingGroupID;
            item.originalMt4ServerID = item.mt4ServerID;
            this.gettingAuditTrails = false;
            this.showMsg('Account Updated',
                'Account ' + item.accountID + ' updated successfully.');
            this.getAuditTrailDetails();
        }).catch(error => {
            this.showError('Error', error.body.message);
            this.gettingAuditTrails = false;
        });
    }

    handleDTGChange (e) {
        let index = e.target.dataset.v20AccountIndex;
        let item = this.v20AccDetails[index];
        item.divisionTradingGroupID = e.target.value;
        item.dtgChanged = (item.divisionTradingGroupID != item.originalDivisionTradingGroupID);
        item.unchanged = !(item.dtgChanged || item.serverChanged);
    }

    handleServerChange (e) {
        let index = e.target.dataset.v20AccountIndex;
        let item = this.v20AccDetails[index];
        item.mt4ServerID = e.target.value;
        item.serverChanged = (item.mt4ServerID != item.originalMt4ServerID);
        item.unchanged = !(item.dtgChanged || item.serverChanged);
    }

    resendMT4Notification(e) {
        this.gettingAuditTrails = true;
        let index = e.currentTarget.dataset.v20AccountIndex;
        let item = this.v20AccDetails[index];

        resendMT4Notification({
            request: {
                env: this.detail['Env'],
                fxTradeUserId: this.detail['fxTrade_User_Id__c'],
                fxAccountId: this.detail['fxAccountId'],
                v20AccountId: item.accountID
            }
        }).then(data => {
            this.gettingAuditTrails = false;
            this.showMsg('MT4 notification resent.');
            this.getAuditTrailDetails();
        }).catch(error => {
            this.showError('Error', error.body.message);
            this.gettingAuditTrails = false;
        });
    }

    showMsg(t, m) {
        const evt = new ShowToastEvent({
            title: t,
            message: m,
            variant: "Success"
        });
        this.dispatchEvent(evt);
    }

    showError(t, m) {
        const evt = new ShowToastEvent({
            title: t,
            message: m,
            variant: "Error",
            mode: 'sticky'
        });
        this.dispatchEvent(evt);
    }

    handleLockUnlockButton(event){
        if (event.detail.action.name === 'lockunlock') {
            this.gettingMt5Accs = true; 
            let currentAction = event.detail.row.isDisabled == false ? 'lock' : 'unlock';
            lockUnlockMt5Acc({
                mt5AccountId: event.detail.row.mt5UserID, 
                env: this.detail['Env'], 
                action: currentAction,
                fxTradeUserId: this.detail['fxTrade_User_Id__c']
            })
            .then(data => {      
                this.getMT5AccsDetails(this.detail['Env'], 
                                        this.detail['fxTrade_User_Id__c'], 
                                        this.detail['Email']);
                this.showMsg(event.detail.row.lockUnlockButtonLabel + ' of MT5 Account sucessfully triggered');
                this.getAuditTrailDetails();
            }).catch(error => {
                this.showError('Error', error.body.message);
                this.gettingMt5Accs = false;
            })   
        }   
    }
}