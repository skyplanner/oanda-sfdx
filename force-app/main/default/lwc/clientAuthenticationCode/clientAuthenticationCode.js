import { LightningElement, track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import generateNewCode from
'@salesforce/apex/AuthenticationCodeGenerator.generateAuthenticationCodeAndSaveToRecord';
import validateCode from
	'@salesforce/apex/AuthenticationCodeGenerator.validateAuthenticationCode';
import getGeneralSettings from
	'@salesforce/apex/SPGeneralSettings.getGeneralSettingsValues';

export default class ClientAuthenticationCode extends LightningElement {
	@api
	recordId;

	@api
	objectApiName;

	@api
	codeFieldApiName;

	@api
	codeExpiresOnFieldApiName;

	@api
	fields;

	@track
	authCode;

	@api
	generalSettings;

	@track
	expiresOn;

	@track
	isCodeExpired;

	@track
	digits;

	@track
	isLoading;

	@track
	isInitialized;

	@track
	isError;

	@track
	errorMessages;

	@track
	showValidation;

	@track
	isValidating;

	@track
	isValidating;

	@track
	showValidation;

	@track
	showValidationMessage;
	
	@track
	showValidationSucess;
	
	@track
	showValidationError;

	get codeValidatorClass() {
		return this.showValidationSucess ? 'slds-form-element slds-has-success' :
			this.showValidationError ? 'slds-form-element slds-has-error' :
			'slds-form-element';
	}

	get codeValidatorIcon() {
		return this.showValidationSucess ? 'utility:success' :
			this.showValidationError ? 'utility:error' : 'utility:number_input';
	}

	get codeValidatorVariant() {
		return this.showValidationSucess ? 'success' :
			this.showValidationError ? 'error' : '';
	}

	get isValidatoButtonDisabled() {
		let code = null;
		return this.isValidating ||
			(code = this._getCode()) == null ||
			code.length != 6;
	}

	// will check every 60 seconds
	_expireDateChecker;

	@wire(getRecord, {
		recordId: '$recordId',
		fields: '$fields'
	})
	record({ data, error }) {
		if (error) {
			this._processError(error);
			this.isInitialized = false;
		// we need to make sure the the fields exists
		} else if (data) {
			this.isInitialized = true;
			this._processRecord(data);
		}
	}

	/**
	 * Happens when the component is fully
	 * initialized in the page
	 */
	connectedCallback() {
		this.isLoading = false;
		this.isInitialized = false;
		this.isError = false;
		this.errorMessages = null;

		// fields to add to query
		this.fields = [
			`${this.objectApiName}.${this.codeFieldApiName}`,
			`${this.objectApiName}.${this.codeExpiresOnFieldApiName}`
		];

		this._getSettings();
	}

	/**
	 * Handles clicks on "Generate New Code" button
	 */
	handleNewCodeClick() {
		this._generateNew();
	}

	/**
	 * Handles clicks on "Send Email" button
	 */
	handleSendEmailClick() {
		this._sendEmail();
	}

	/**
	 * Handles clicks on "Validate Code" button
	 */
	handleValidateCodeClick() {
		if (!this.showValidation)
			this.showValidation = true;
		else {
			this.showValidation = false;
			this.showValidationMessage = false;
			this.showValidationSucess = false;
			this.showValidationError = false;
		}
	}

	/**
	 * Handles clicks on "Close: Validate Code" button
	 */
	handleValidatingCodeClick(e) {
		this._validate(this._getCode());
	}

	/**
	 * Processes an error response
	 * @param {*} error 
	 */
	_processError(error) {
		if (!error || !error.body)
			this._addError("There was an unexpected problem");
		else if (Array.isArray(error.body))
			error.body.forEach(e => this._addError(e.message));
		else
			this._addError(error.body.message || "There was an unexpected problem");
	}

	/**
	 * Removes any error
	 */
	_removeErrors() {
		this.isError = false;
		this.errorMessages = null;
	}

	/**
	 * Adds an error
	 * @param {string} message 
	 */
	_addError(message) {
		this.isError = true;

		if (this.errorMessages == null)
			this.errorMessages = [];

		this.errorMessages.push({
			id: this.errorMessages.length,
			msg: message
		});
	}

	/**
	 * Process the code when received
	 */
	_processRecord(record) {
		this.isCodeExpired = false;
		this.digits = null;
		
		// we save authcode and expriation date
		this._setRecurrentCalculation(
			record.fields[this.codeFieldApiName].value,
			record.fields[this.codeExpiresOnFieldApiName].value);

		// we break the code into dogits
		if (this.authCode)
			this.digits = this.authCode.split("").map(
				(c, i) => {
					return { 
						id: i,
						digit: c
					};
				});
	}

	/**
	 * Fetches the general settings
	 */
	_getSettings() {
		getGeneralSettings().then(result => {
			this.generalSettings = result;
			if (!this.generalSettings.SP_Authenticatio_Code_Expires_In_Minutes)
				this.generalSettings.SP_Authenticatio_Code_Expires_In_Minutes = 15;
		});
	}

	/**
	 * Generates a new code and saves it
	 * to the current record.
	 */
	_generateNew() {
		this.isLoading = true;
		this._removeErrors();

		generateNewCode({
			record: { "Id": this.recordId },
			codeFieldApiName: this.codeFieldApiName,
			codeExpiresOnFieldApiName: this.codeExpiresOnFieldApiName
		})
		.then(result => {
			var fields = {};
			fields[this.codeFieldApiName] = {
				value: result[this.codeFieldApiName]
			};
			fields[this.codeExpiresOnFieldApiName] = {
				value: result[this.codeExpiresOnFieldApiName]
			};

			this.isLoading = false;
			this._processRecord({fields: fields});
			this.dispatchEvent(new CustomEvent("codegenerated", {
				detail: result
			}));
		})
		.catch(error => {
			this.isLoading = false;
			this._processError(error);
		});
	}

	/**
	 * Validates a code
	 */
	_validate(code) {
		this.isValidating = true;
		this.showValidationMessage = true;
		this.showValidationSucess = false;
		this.showValidationError = false;
		
		validateCode({
			codeToValidate: code,
			recordId: this.recordId,
			codeFieldApiName: this.codeFieldApiName,
			codeExpiresOnFieldApiName: this.codeExpiresOnFieldApiName
		})
		.then(result => {
			console.log(code, result);
			this.isValidating = false;

			if (result)
				this.showValidationSucess = true;
			else
				this.showValidationError = true;
		})
		.catch(error => {
			console.error(error);
			this.isValidating = false;
			this.showValidationError = true;
		});
	}

	/**
	 * Check if code has expired every 60 seconds
	 */
	_setRecurrentCalculation(code, expiration) {
		// we forget any existing timeout
		if (this._expireDateChecker)
			clearTimeout(this._expireDateChecker);

		// we the process the expiration if any
		if (expiration) {
			this.expiresOn = this._getTimeDifference(expiration, new Date());
			this.isCodeExpired = this.expiresOn != null &&
				this.expiresOn.difference <= 0;

			// if code has not expired, we check again in 60 seconds
			if (!this.isCodeExpired) {
				this.authCode = code;
				this._expireDateChecker = setTimeout(
					() => this._setRecurrentCalculation(code, expiration),
					1000);
			// once the code expires, we clear the auth code
			} else {
				this.authCode = null;
				this.showValidation = false;
				this.showValidationMessage = false;
				this.showValidationSucess = false;
				this.showValidationError = false;
				this.dispatchEvent(new CustomEvent("codeexpired"));
			}
		} else {
			this.expiresOn = null;
			this.authCode = null;
			this.isCodeExpired = false;
		}
	}

	/**
	 * @param {*} date1 
	 * @param {*} date2
	 * @returns the hours and minutes between two dates
	 */
	_getTimeDifference(date1, date2) {
		var difference = new Date(date1).getTime() - new Date(date2).getTime(),
			hours = 0,
			minutes = 0,
			seconds = 0,
			mod,
			pad = (n) => {
				var s = String(n);
				while (s.length < 2) {s = "0" + s;}
				return s;
			};
		
		// hours, pretty
		hours = Math.floor(difference / 3600000);
		mod = difference % 3600000;

		// minutes, pretty
		minutes = Math.ceil(mod / 60000);
		mod = mod % 60000;

		// seconds, pretty
		seconds = Math.floor(mod / 1000);

		return {
			difference: difference,
			hours: hours,
			minutes: minutes,
			seconds: seconds,
			message: `Expires in ${hours ?
				pad(hours) + ':' : ''} ${pad(minutes)}:${pad(seconds)}`
		};
	}

	_getCode() {
		let input = this.template.querySelector(".validator input");
		return input ? input.value : null;
	}
}