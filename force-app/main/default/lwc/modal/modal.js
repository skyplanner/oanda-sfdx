import { api, LightningElement, track } from "lwc";

// Selectors to target elements
const modalSelectors = {
  HIDE: "slds-hide",
  CONTAINER: `[data-id="modal-container"]`,
  TAGLINE: `[data-id="modal-tagline"]`,
  FOOTER: `[data-id="modal-footer"]`
};

/**
 * Modal Component, based on SLDS modal.
 *
 * This component is not exposed and is meant to be used by other components
 * Modals are used to display content in a layer above the app.
 * This paradigm is used in cases such as the creation
 * or editing of a record, as well as various types
 * of messaging and wizards.
 */
export default class Modal extends LightningElement {
  /**
   * Public API to set the header of the modal.
   * @param {string} value The Header String to be rendered
   */
  @api
  set header(value) {
    this.hasHeaderString = typeof value === "string" ? true : false;
    this.headerString = value;
  }
  get header() {
    return this.headerString;
  }

  /**
   * Public method to show the modal and it's contents
   */
  @api
  show() {
    const modalContainer = this.template.querySelector(modalSelectors.CONTAINER);
    modalContainer.classList.remove(modalSelectors.HIDE);
  }

  /**
   * Public method to hide the modal and it's contents
   */
  @api
  hide() {
    const modalContainer = this.template.querySelector(modalSelectors.CONTAINER);
    modalContainer.classList.add(modalSelectors.HIDE);
  }

  /**
   * Public property to allow a wider modal width. For a wider modal,
   * set this property to true. Defaults to false.
   */
  @api
  wideModal = false;

  get modalContainerClass() {
    return this.wideModal ? "slds-modal__container wide-modal-container" : "slds-modal__container medium-modal-container";
  }

  /**
   * In case header is provided as a string, we use headerString property
   * to store it's value
   */
  headerString;

  @track
  hasHeaderString = false;

  handleDialogClose() {
    this.hide();

    const closeEvent = new CustomEvent("modalclose");
    this.dispatchEvent(closeEvent);
  }

  /**
   * If a tagline is provided in the modal, show the containing element
   */
  handleSlotTaglineChange() {
    const taglineElement = this.template.querySelector(modalSelectors.TAGLINE);
    taglineElement.classList.remove(modalSelectors.HIDE);
  }

  /**
   * If a Footer is provided in the modal, show the containing element
   */
  handleSlotFooterChange() {
    const footerElement = this.template.querySelector(modalSelectors.FOOTER);
    footerElement.classList.remove(modalSelectors.HIDE);
  }
}