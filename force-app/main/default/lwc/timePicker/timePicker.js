/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-20-2022
 * @last modified by  : Ariel Niubo
 **/
import { LightningElement, api, track } from "lwc";
import LABEL_TIME_PLACEHOLDER from "@salesforce/label/c.Scheduled_email_ctrl_select_time";
import LABEL_AM_OR_PM_PLACEHOLDER from "@salesforce/label/c.Scheduled_email_ctrl_select_am_pm";
import SCHEDULED_EMAIL_TIME_FIELD from "@salesforce/schema/Scheduled_Email__c.Time__c";
import SCHEDULED_EMAIL_OBJECT from "@salesforce/schema/Scheduled_Email__c";
import SCHEDULED_EMAIL_AM_PM_FIELD from "@salesforce/schema/Scheduled_Email__c.AM_or_PM__c";
import { normalizeBoolean } from "c/utils";

export default class TimePicker extends LightningElement {
	labels = {
		time: LABEL_TIME_PLACEHOLDER,
		ampm: LABEL_AM_OR_PM_PLACEHOLDER
	};
	scheduledInfo = {
		objName: SCHEDULED_EMAIL_OBJECT,
		time: SCHEDULED_EMAIL_TIME_FIELD,
		ampm: SCHEDULED_EMAIL_AM_PM_FIELD
	};
	_timeValue;
	_amOrPmValue;
	@track _disabled = false;
	@track _readOnly = false;
	@track _required = false;
	@api
	get timeValue() {
		return this._timeValue;
	}
	set timeValue(value) {
		if (this._timeValue !== value) {
			this._timeValue = value;
		}
	}
	@api
	get amPmValue() {
		return this._amOrPmValue;
	}
	set amPmValue(value) {
		if (this._amOrPmValue !== value) {
			this._amOrPmValue = value;
		}
	}
	@api
	get disabled() {
		return this._disabled || this._readOnly || false;
	}
	set disabled(value) {
		this._disabled = normalizeBoolean(value);
	}
	@api
	get readOnly() {
		return this.disabled;
	}
	set readOnly(value) {
		this._readOnly = normalizeBoolean(value);
	}
	set amOrPm(value) {
		this._amOrPmValue = value;
	}
	@api
	get required() {
		return this._required;
	}
	set required(value) {
		this._required = normalizeBoolean(value);
	}
	get objName() {
		return this.scheduledInfo.objName.objectApiName;
	}
	get timeName() {
		return this.scheduledInfo.time.fieldApiName;
	}
	get ampmName() {
		return this.scheduledInfo.ampm.fieldApiName;
	}
	handleChange(event) {
		event.stopPropagation();
		const fieldName = "_" + event.target.name + "Value";
		const newValue = event.detail.value;
		if (this[fieldName] !== newValue) {
			this[fieldName] = newValue;
			if (this.timeValue && this.timeValue) {
				this.dispatchEvent(
					new CustomEvent("change", {
						detail: {
							time: this.timeValue,
							amOrPm: this.amPmValue
						}
					})
				);
			}
		}
	}
	handleFocus(event) {
		event.stopPropagation();
		this.dispatchEvent(
			new CustomEvent("focus", { bubbles: false, composed: false })
		);
	}
}