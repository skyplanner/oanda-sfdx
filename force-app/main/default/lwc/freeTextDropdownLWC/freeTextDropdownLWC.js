import { LightningElement, api, track } from 'lwc';

export default class FreeTextDropdownLWC extends LightningElement {
    @api caseId;
    @api title;
    @api placeholder = '';
    @api selectedvalue;
    @api values = [];
    @track isOpen = false;

    get dropdownClass() {
        return this.isOpen ?
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' :
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    }

    handleFocus(event) {
        this.isOpen = true;
        event.preventDefault();
    }

    closeDropdown() {
        this.isOpen = false;
    }

    handleChange(event) {
        this.selectedvalue = event.target.value;
        if (!this.selectedvalue)
            this.selectedvalue = 0;
        this.dispatchSetValue(this.title, this.selectedvalue);
    }

    handleclick(event) {
        this.selectedvalue = event.currentTarget.id.split('-')[0];
        this.dispatchSetValue(this.title, this.selectedvalue);
        this.isOpen = false;
    }

    dispatchSetValue(key, value) {
        const selectedvalue = new CustomEvent('changevalue',
        {
            detail: {
                key: key,
                value: value
            }
        });
        this.dispatchEvent(selectedvalue);
    }

    avoidKeyPress(event){
        event.preventDefault();
    }
}