/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LightningConfirm from "lightning/confirm";
import { subscribe, unsubscribe, onError } from "lightning/empApi";

import { refreshApex } from '@salesforce/apex';

import getScheduledEmail from "@salesforce/apex/ScheduledEmailController.getScheduledEmail";
import saveScheduledEmail from "@salesforce/apex/ScheduledEmailController.saveScheduledEmail";
import deleteScheduledEmail from "@salesforce/apex/ScheduledEmailController.deleteScheduledEmail";
import SUCCESSFULLY_CREATED_MSG from "@salesforce/label/c.Scheduled_email_ctrl_successfully_created";
import SUCCESSFULLY_UPDATED_MSG from "@salesforce/label/c.Scheduled_email_ctrl_successfully_successfully_updated";
import SUCCESSFULLY_DELETED_MSG from "@salesforce/label/c.Scheduled_email_ctrl_successfully_successfully_deleted";
import DELETE_CONFIRMATION_MSG from "@salesforce/label/c.Scheduled_email_ctrl_delete_confirmation";
import TOAST_ERROR_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toast_labe_error";
import TOAST_WARNING_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toast_labe_warning";
import TOAST_INFO_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toast_labe_info";

export default class ScheduledEmail extends LightningElement {
	@api recordId;
 

	@track scheduleEmail = {
		scheduledDate: "",
		scheduledTime: "4",
		scheduledAmOrPm: "PM",
		id: null,
		emailMessageId: null
	};

	// update staled cache with fresh when the schedule time is changed
	@wire(getScheduledEmail, { caseId: '$recordId'})
	wiredGetScheduledEmail(value) {
		
		console.log('wiredGetScheduledEmail', value.data)
		// variable used in refreshApex function
		this.wiredScheduledEmail = value;
		
		// new content
		const { data, error } = value;
		if (data) { 
			this.scheduleEmail.scheduledDate = data.scheduledDate
			this.scheduleEmail.scheduledTime = data.scheduledTime
			this.scheduleEmail.scheduledAmOrPm = data.scheduledAmOrPm
			this.scheduleEmail.id = data.id

			this.setModes();
		}
		else if (error) { 
			console.log(error)
		}
	}

	toolbarMode = "create";
	mode = "view" | "edit" | "create";
	previousMode = "view";
	previousToolbarMode = "create";
	previousScheduleEmail = undefined;
	loading = false;
	disabledSave = false;

	channelName = "/event/Email_Message__e";
	subscription = {};
	eventInfo = {
		data: {
			schema: "",
			payload: {
				Parent_Id__c: "",
				CreatedById: "",
				CreatedDate: ""
			},
			event: {
				replayId: 0
			}
		},
		channel: "/event/Email_Message__e"
	};

	get readOnly() {
		return this.mode === "view";
	}
	get time() {
		return this.scheduleEmail.scheduledTime;
	}
	get date() {
		return this.scheduleEmail.scheduledDate;
	}
	get amOrPm() {
		return this.scheduleEmail.scheduledAmOrPm;
	}
	get minDateTime() {
		const now = new Date();
		const hour = now.getHours() + 1;
		now.setHours(hour, 0, 0, 0);
		return now;
	}

	connectedCallback() {
		this.registerErrorListener();
		this.eventSubscribe();
		//read scheduled Email
		this.read();
	}
	disconnectedCallback() {
		this.eventUnsubscribe();
	}
	handleDatetimePickerChange(event) {
		event.stopPropagation();
		if (event.detail) {
			this.scheduleEmail.scheduledAmOrPm = event.detail.amOrPm;
			this.scheduleEmail.scheduledDate = event.detail.date;
			this.scheduleEmail.scheduledTime = event.detail.time;
		}
	}
	handleValidityChange(event) {
		event.stopPropagation();
		this.disabledSave = !event.detail.valid;
	}

	handleSave(event) {
		//save Ok
		event.stopPropagation();
		this.save(); 
	}

	handleCancel(event) {
		event.stopPropagation();
		this.scheduleEmail = { ...this.previousScheduleEmail };
		this.toolbarMode = this.previousToolbarMode;
		this.mode = this.previousMode;
	}

	handleEdit(event) {
		event.stopPropagation();
		this.mode = "edit";
		this.toolbarMode = "update";
 
	}
	async handleDelete(event) {
		event.stopPropagation();
		const canDelete = await this.showConfirmation(DELETE_CONFIRMATION_MSG);
		if (canDelete) {
			this.delete();
		}
	}

	async read() {
		this.loading = true;
		try {
			const data = await getScheduledEmail({
				caseId: this.recordId
			});
			this.handleSuccess(data);
		} catch (error) {
			this.handleError(error);
		}
		this.setModes();
	}

	async save() {
		this.loading = true;
		const isScheduled = !this.scheduleEmail.id;
		try {
			const data = await saveScheduledEmail({
				caseId: this.recordId,
				wrapper: this.scheduleEmail
			});
			this.handleSuccess(
				data,
				isScheduled
					? SUCCESSFULLY_CREATED_MSG
					: SUCCESSFULLY_UPDATED_MSG
			);
 
			
		} catch (error) {
			console.log(error)
			this.handleError(error);
		}
		this.setModes();
	}
	
	async delete() {
		this.loading = true;
		try {
			const data = await deleteScheduledEmail({
				scheduledEmailId: this.scheduleEmail.id
			});
			this.handleSuccess(data, SUCCESSFULLY_DELETED_MSG);
		} catch (error) {
			this.handleError(error);
		} 
	}

	handleSuccess(data, message) {
		// this.scheduleEmail = { ...data };
		if (message) {
			this.showSuccessMessage(message);
		}

		// update schedule email time cache
		refreshApex(this.wiredScheduledEmail);
	}

	handleError(error) {
		this.showErrorMessage(error.body.message);
		console.log(error);
	}

	async showConfirmation(message) {
		const result = await LightningConfirm.open({
			message: message,
			theme: "warning",
			label: TOAST_WARNING_LABEL
		});
		return result;
	}

	showSuccessMessage(message) {
		this.showNotification(TOAST_INFO_LABEL, message, "success");
	}

	showErrorMessage(errorMessage) {
		this.showNotification(TOAST_ERROR_LABEL, errorMessage, "error");
	}

	showNotification(title, message, variant) {
		const evt = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(evt);
	}

	setModes() {
		this.mode = this.scheduleEmail.id ? "view" : "create";
		this.toolbarMode = this.mode === "view" ? "edit" : "create";
		this.setPreviousState();
	}

	setPreviousState() {
		this.previousMode = this.mode;
		this.previousScheduleEmail = { ...this.scheduleEmail };
		this.previousToolbarMode = this.toolbarMode;
		this.loading = false;
	}

	eventSubscribe() {
		// const messageCallback = function (response) {
		// 	console.log("New message received: ", JSON.stringify(response));
		// 	this.refreshControl(response);
		// };

		subscribe(this.channelName, -1, (response) => {
			console.log("New message received: ", JSON.stringify(response));
			this.refreshControl(response);
		}).then((response) => {
			console.log(
				"Subscription request sent to: ",
				JSON.stringify(response.channel)
			);
			this.subscription = response;
		});
	}

	eventUnsubscribe() {
		unsubscribe(this.subscription, (response) => {
			console.log("unsubscribe() response: ", JSON.stringify(response));
			// Response is true for successful unsubscribe
		});
	}

	registerErrorListener() {
		// Invoke onError empApi method
		onError((error) => {
			console.log("Received error from server: ", JSON.stringify(error));
			// Error contains the server-side error
		});
	}

	refreshControl(response) {

		// update schedule email time cache
		refreshApex(this.wiredScheduledEmail);

		/*this.eventInfo = { ...response };
		if (
			this.eventInfo.channel === this.channelName &&
			this.recordId === this.eventInfo.data.payload.Parent_Id__c
		) {
			this.read();
		}*/
	}
}