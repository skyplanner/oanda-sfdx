/**
 * @description       : 
 * @author            : William Garcia
 * @group             : 
 * @last modified on  : 02-09-2023
 * @last modified by  : Dianelys Velazquez
**/
import { LightningElement, api } from 'lwc';
import getOptions from '@salesforce/apex/UserApiFieldLwcCtrl.getOptions';

export default class UserApiFieldLwc extends LightningElement {
    @api field;
    _options = [];
    loading;
    error;

    @api set options(val) {
        this._options = val;
    }
    
    get options() {
        return this._options;
    }

    connectedCallback() {
        if (this.field.isPicklist)
            this.getOptions();
    }

    getOptions() {
        if (this.options)
            return;

        this.loading = true;
        
        getOptions({
            fieldName: this.field.name})
            .then(data => {
                this.options = data;
                this.loading = false;
                console.log('options:', this.options);
            })
            .catch(error => {
                this.error = error.body.message;
                this.loading = false;
                console.log(error);
            });
    }

    updateDetail(event) {
        var data =
            {
                value: event.target.dataset.isCheck ?
                    event.target.checked :
                    event.target.value,
                field: event.target.dataset.fieldName
            };

        this.dispatchEvent(
            new CustomEvent(
                "fieldupdated",
                {detail: data}));
    }
}