import { LightningElement, track, api, wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getMatches from "@salesforce/apex/ComplyAdvantageSearchEntities.getSearchEntities"; 
import updateMatches from "@salesforce/apex/ComplyAdvantageSearchEntities.updateSearchEntities";
import acknowledgeChanges from "@salesforce/apex/ComplyAdvantageSearchEntities.acknowledgeChanges";


const nameMatchDescMap = 
{
   'equivalent_name' : 'Equivalent name match',
   'equivalent_aka' :  'Equivalent AKA match',
   'name_fuzzy' :'Name nearly matched exactly',
   'name_exact' : 'Name matched exactly',
   'year_of_birth' : 'Matched birth year filter',
   'aka_exact' : 'Aka matched exactly',
   'aka_fuzzy' :'Aka nearly matched exactly',
   'fuzzy_match': 'Fuzzy match',
   'unknown' : 'Unknown',
   'name_variations_removal' : 'Name Variations Removal'
};

const parentAMLTypes = [
   'sanction',
   'warning',
   'pep',
   'adverse-media',
   'fitness-probity',
   'Internal-Hit'
];

export default class ComplyAdvantageSearchHitsComponent extends LightningElement 
{
   
  
   @api recordId;
   
   caseCommentsCount = 0
   matchComments={}
   entitiesObject= {}
   isCaseCommentDialog = false

   matchListBeforeFilter = []

   @track matchList = []
   @track minOneMatchSelected = false
   @track inProgress = false
   @track hasMonitorStatusChanges = false

   @track caseCommentButtonLabel = 'Case Comments'
   @track showCommentsDialog = false;

   searchMatchesSectionClass = 'slds-section slds-is-open';
   searchId = ''
   selectedMatchIds=[]
   allMatchIds = []
   selectAll = false
   

   filterAMLTypes = [
      { label: 'All', value: 'All' },
      { label: 'Sanction', value: 'sanction' },
      { label: 'Warning', value: 'warning' },
      { label: 'PEP', value: 'pep' },
      { label: 'Adverse-Media', value: 'adverse-media' },
      { label: 'Fitness-Probity', value: 'fitness-probity' },
      { label: 'Internal-Hit' , value: 'Internal-Hit' }
   ]
   selectedCategoryFilter
 

  connectedCallback()
  {
      this.fetchMatches();
  }

  toggleSearchMatchesSection(event)
  { 
      event.preventDefault()

      if(this.searchMatchesSectionClass.includes('slds-is-open'))
      { // if section is open
         this.searchMatchesSectionClass = 'slds-section'; //set class to close 
      }
      else
      {
         this.searchMatchesSectionClass = 'slds-section slds-is-open'; //set class to be open
      }
  }

	fetchMatches() {
		this.inProgress = true;

		getMatches({
			"caSearchId": this.recordId
		}).then(result => {
			this.processResponse(result);
			this.inProgress = false;
		}).catch(error => {
			console.error(error);
			this.inProgress = false;
		})
	}

	processResponse(response) 
   {
		if (response && response.length) 
      {
			// this.loadComments(response);
			this.formatMatches(response);
		}
	}

   loadComments(response)
   {
      /* var _this = this;
      if(response.content != null && response.content.length > 0)
      {
         response.content.forEach(function(comment, index) 
         {
            var cmt = {};
            cmt.id = comment.id
            cmt.message = comment.message

            if(comment.entity_id != null)
            {
               var mComments = _this.matchComments[comment.entity_id]
               if(mComments == null)
               {
                  mComments = []
               }
               mComments.push(cmt)
               _this.matchComments[comment.entity_id] = mComments;
            }
         })

         this.caseCommentsCount = response.content.length
         this.caseCommentButtonLabel = 'Case Comments ' + '(' +  this.caseCommentsCount + ')'
      } */
   }

   filterMatches(event)
   {
      var _this = this;
      var selectedFilter = event.target.value;

      this.clearSelection()

      if(selectedFilter == 'All')
      {
          this.matchList = this.matchListBeforeFilter
      }
      else
      {       
         var filteredMatches = []
         this.matchListBeforeFilter.forEach(function(match, index) 
         {
            if(match.amlTypes.indexOf(selectedFilter) != -1)
            {
               filteredMatches.push(match)
            }
         })
         this.matchList = filteredMatches
      } 
   }

	formatMatches(response) 
   {
		response.forEach((hit, index) => 
      {
			let matchInfo = 
         {
				searchId: this.recordId,
				entityId: hit.Entity_Id__c,
				name: hit.Name,
				entityType: hit.Entity_Type__c,
				matchStatus: hit.Match_Status__c,
				isWhiteListed: hit.Is_Whitelisted__c,
				riskLevel: hit.Risk_Level__c || 'None',
				countries: hit.Countries__c,
				dob: hit.Date_Of_Birth__c,
				types: this._toList(hit.Entity_Match_Types__c, ';'),
				matchTypes: this._toList(hit.Match_Types__c),
            monitorStatus : hit.Monitor_Change_Status__c,
            isInternalHit : hit.Is_Internal_Hit__c,
            internalHitId : hit.Internal_Hit_Id__c,
            internalHitNotes : hit.Internal_Hit_Notes__c,
            internalHitPhone : hit.Internal_Hit_Phone__c,
            internalHitEmail : hit.Internal_Hit_Email__c,
            internalHitAddress : hit.Internal_Hit_Address__c,
            source : hit.Sources__c
			};

			if (matchInfo.matchTypes.length)
         {
            let matchTypeDescArr = [];
            matchInfo.matchTypes.forEach(function (matchTypeKey, index) 
            {
               matchTypeDescArr.push(nameMatchDescMap[matchTypeKey]);
            });
				matchInfo.nameMatchDesc = matchTypeDescArr.join(', ');
         }

			if (matchInfo.types.length)
         {
				matchInfo.amlTypes = matchInfo.types.filter(
					amlType => parentAMLTypes.indexOf(amlType) != -1).join(', ');
         }
			matchInfo.comments = [];

			if(this.matchComments[matchInfo.entityId] != null &&
					this.matchComments[matchInfo.entityId].length > 0) {
				matchInfo.commentsCount = this.matchComments[matchInfo.entityId].length
				matchInfo.comments = this.matchComments[matchInfo.entityId]
			}

			matchInfo.isSelected = false;
			matchInfo.rawDetail = hit;

         if(matchInfo.monitorStatus == 'Added' || matchInfo.monitorStatus == 'Updated' || matchInfo.monitorStatus == 'Removed')
         {
            matchInfo.hasMonitorChanges = true;
            this.hasMonitorStatusChanges = true;
         }
			this.entitiesObject[hit.Entity_Id__c] = matchInfo;
			this.matchList.push(matchInfo);
			this.matchListBeforeFilter.push(matchInfo);
			this.allMatchIds.push(hit.Entity_Id__c);
		});

     /*  if(response.content.data.total_blacklist_hits > 0)
      {
         let hits = response.content.data.blacklist_hits;

         for (let index = 0; index < hits.length; index++) 
         {
            let hit = hits[index];
            if(hit.doc.id != null && this.allMatchIds.indexOf(hit.doc.id) == -1)
            {
               let matchInfo = {};
               matchInfo.searchId = this.searchId
               matchInfo.entityId = hit.doc.id
               matchInfo.name = hit.doc.name
               matchInfo.entityType = hit.doc.entity_type
               matchInfo.matchStatus = hit.match_status;
               matchInfo.isWhiteListed = hit.is_whitelisted;
               matchInfo.riskLevel = hit.risk_level || 'None';
               matchInfo.internalList= hit.doc.sources[0];
               //Name match --todo add other types
               let mt = hit.match_types[0];
               matchInfo.nameMatchDesc = nameMatchDescMap[mt];

               matchInfo.amlTypes = 'Internal-Hit';
               let userDefinedName = hit.doc.fields.find(item => item.name === 'User-defined ID');
               matchInfo.displayId= userDefinedName.value;
               let notesFromResp = hit.doc.fields.find(item => item.name === 'Notes'); 
               matchInfo.notes= notesFromResp.value;
               matchInfo.isSelected = false
               matchInfo.rawDetail = hit

               this.entitiesObject[hit.doc.id] = matchInfo
               this.matchList.push(matchInfo)
               this.matchListBeforeFilter.push(matchInfo)
               this.allMatchIds.push(hit.doc.id)
            }
         }
      } */
   }
   
   matchSelectionChangeEvent(event)
   {
      const matchEntityId = event.detail.matchid
      const matchSelected = event.detail.isSelected

      if(matchSelected && this.selectedMatchIds.indexOf(matchEntityId) == -1){
         this.selectedMatchIds.push(matchEntityId);
      }
      if(!matchSelected){
         const index = this.selectedMatchIds.indexOf(matchEntityId)
         if (index > -1) {
            this.selectedMatchIds.splice(index, 1)
         }
      }
      this.toggleActionMenu()
      this.toggleSelectAllCheckbox()
   }
   
   selectAllMatches(event)
   {
      this.selectAll = event.target.checked
      for (let index = 0; index < this.matchList.length; index++) 
      {
          this.matchList[index].isSelected = event.target.checked 
      }

      if(event.target.checked)
      {
         this.selectedMatchIds =  [...this.allMatchIds]
      }
      else
      {
         this.selectedMatchIds = []
      }

      this.toggleActionMenu()
   }

   clearSelection()
   {
      this.selectAll = false
      this.selectedMatchIds = []
      for (let index = 0; index < this.matchList.length; index++) 
      {
          this.matchList[index].isSelected = false
      }
      for (let index = 0; index < this.matchList.length; index++) 
      {
          this.matchListBeforeFilter[index].isSelected = false
      }
      this.toggleActionMenu()
   }
   
   toggleActionMenu()
   {
      this.minOneMatchSelected =  this.selectedMatchIds.length >0;
   }

   toggleSelectAllCheckbox()
   {
      this.selectAll = this.selectedMatchIds.length == this.allMatchIds.length
   }
   
   
   getChangeSummary(updateDetails)
   {
       var matchSummary = updateDetails + ' for match(es) : \n';
      
       let matchNames = []
       let selectedMatches = this.matchList.filter(match => this.selectedMatchIds.indexOf(match.entityId) != -1)
       selectedMatches.forEach(function(selectedMatch, index) 
       {
            matchNames.push(selectedMatch.name);
       })

       matchSummary += matchNames.join(',\n')
       return matchSummary;
   }

   updateMatchStatus(event)
   {  
      event.preventDefault()

      let summaryTitle = 'updated match status to ' + event.detail.value
      let summaryBody= this.getChangeSummary('updated match status to ' +   event.detail.value)

		this.updateComplyAdvantage(
			this.selectedMatchIds,
			{ match_status : event.detail.value },
			summaryTitle,
			summaryBody);
   }

   updateWhiteListStatus(event)
   {  
      event.preventDefault()

      let summaryTitle = 'set whitelisted ' +  (event.detail.value == 'true' ? 'On' : 'Off')
      let summaryBody= this.getChangeSummary('set whitelisted ' + (event.detail.value == 'true' ? 'On' : 'Off'))

	 	this.updateComplyAdvantage(
			this.selectedMatchIds,
			{ is_whitelisted: event.detail.value == 'true' },
			summaryTitle,
			summaryBody);
   }

   updateRiskLevel(event)
   {  
      event.preventDefault()

      let summaryTitle = 'updated Risk Level to ' + event.detail.value
      let summaryBody  = this.getChangeSummary('updated Risk Level to ' + event.detail.value)

		this.updateComplyAdvantage(
			this.selectedMatchIds,
			{ risk_level: event.detail.value },
			summaryTitle,
			summaryBody);
   }

	updateComplyAdvantage(entityIds, template, summaryTitle, summaryBody) {
		updateMatches({
			searchId : this.recordId,
			entityIds: entityIds,
			template: template,
			summaryTitle : summaryTitle,
			summaryBody : summaryBody
		}).then(response => {
			this.updateUIDataset(template) 
			this.notifyUser(200);
		}).catch(error => {
			console.error(error);
			this.notifyUser(500)    
		})
	}

   updateUIDataset(requestBody)
   {
      let selectedMatches = this.matchList.filter(match => this.selectedMatchIds.indexOf(match.entityId) != -1)
      for (let index = 0; index < selectedMatches.length; index++) 
      {
          if(requestBody.match_status != null)
          {
               selectedMatches[index].matchStatus = requestBody.match_status
          }
          else if(requestBody.is_whitelisted != null)
          {
               selectedMatches[index].isWhiteListed = requestBody.is_whitelisted
          }
          else if(requestBody.risk_level != null)
          {
               selectedMatches[index].riskLevel = requestBody.risk_level
          }
          selectedMatches[index].isSelected = true
      }
   }

   notifyUser(statusCode)
   {
      let message = statusCode == 200 ? 'Comply Advantage updated successfully' : 'Comply Advantage update failed';
      let variant =  statusCode == 200 ? 'success' : 'error';

      this.dispatchEvent(new ShowToastEvent({
         title: 'Comply Advantage Update',
         message: message,
         variant: variant,
         mode : 'dismissable'
       }));
   }

   openCommentsDialog(events)
   {
      this.isCaseCommentDialog = true
      this.showCommentsDialog = true
   }
   hideCommentsDialog(events)
   {
      this.isCaseCommentDialog = false
      this.showCommentsDialog = false    
   }

   openDetailsCommentsDialog(events)
   {
       this.showCommentsDialog = true
   }

   acknowledgeMonitorChanges(events)
   {
      acknowledgeChanges(
      {
			searchId : this.recordId
		})
      .then(response => 
      {
			this.clearMonitorChanges();
			this.notifyUser(200);
		})
      .catch(error => 
      {
			console.error(error);
			this.notifyUser(500)    
		})
   }
   clearMonitorChanges()
   {
      var matchListAfterAck = [];
      this.matchList.forEach((matchInfo, index) => 
      {
         if(matchInfo.monitorStatus == 'Added' || matchInfo.monitorStatus == 'Updated')
         {
            matchInfo.monitorStatus = '';
            matchInfo.hasMonitorChanges = false;
            matchListAfterAck.push(matchInfo);
         }
         else if(matchInfo.monitorStatus == 'Removed')
         {
            matchInfo.isRemoved = true;
            matchInfo.hasMonitorChanges = false;
            matchInfo.monitorStatus = '';
         }
         else
         {
            matchListAfterAck.push(matchInfo);
         }        
      });

      if(matchListAfterAck.length > 0)
      {
         this.matchList = matchListAfterAck;
         this.matchListBeforeFilter = matchListAfterAck;
         this.hasMonitorStatusChanges = false;
      }
   }

   newCommentHandler(event)
   {
      this.caseCommentsCount += 1;
      this.caseCommentButtonLabel = 'Case Comments ' + '(' +  this.caseCommentsCount + ')'

      if(event.detail.matchid)
      {
         let mdt = this.matchList.filter(m => m.entityId == event.detail.matchid)

         if(mdt.length > 0)
         {
            mdt[0].commentsCount =  mdt[0].commentsCount ||0
            mdt[0].commentsCount += 1
         }
      }

      this.showCommentsDialog = false;
   }

   newMatchCommentHandler(event)
   {
      this.caseCommentsCount += 1;
      this.caseCommentButtonLabel = 'Case Comments ' + '(' +  this.caseCommentsCount + ')'
   }

	_toList(value) {
		return ((value && value.split(',')) || []).map(type => type.trim());
	}

   _toList(value, delimitter) {
		return ((value && value.split(delimitter)) || []).map(type => type.trim());
	}
}