import { LightningElement ,api, track } from 'lwc';
import addComment from "@salesforce/apex/ComplyAdvantageSearchEntities.addComments";
import retrieveComments from "@salesforce/apex/ComplyAdvantageSearchEntities.getAllComments"; 
import addCommentsSummaryNote from "@salesforce/apex/ComplyAdvantageSearchEntities.addCommentsSummaryNote";
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class ComplyAdvantageSearchComments extends LightningElement 
{
    newComment

    @api searchId
    @api matchList
    @api matchIds
    @api matchId
    @api matchInfo
    @api comments

    @api isCaseCommentDialog
    @api isMatchCommentDialog

    matchDetailsById = {}

    users = {}
    inProgress = true

    successSaveMatches = []
    failedSaveMatches = []

    captureComment(event)
    {
        this.newComment = event.target.value;
    }
    connectedCallback()
    {
        var _this = this;

        if(this.matchList != null)
        {
            this.matchList.forEach(function(m, index) 
            {
                _this.matchDetailsById[m.entityId] = m
            })
        }

        if(this.isCaseCommentDialog || this.isMatchCommentDialog)
        {
            retrieveComments({searchId: this.searchId}).then(response=>
            {
                this.processResponse(response)
                this.inProgress = false

            }).catch(error=>
            {
                this.inProgress = false
                console.error(error);
            }) 
        }
        else
        {
            this.inProgress = false
        }
    }
    
	processResponse(response) {
		this.loadComments(response)
	}

   loadComments(response) {
        this.comments = [];

        if(this.matchId == null)
        {
            response.forEach((comment, index) =>
            {
                var cmt = {};
                cmt.id = comment.id
                cmt.createdAt = this.formatDate(new Date(comment.created_at))
                cmt.message = comment.message
                cmt.userId = comment.user_id
				cmt.userName = comment.user ? comment.user.name : ''
                if(comment.entity_id != null)
                {
                    cmt.isEntityComment = true
                    cmt.matchName = this.matchDetailsById[comment.entity_id].name
                }

                this.comments.push(cmt);
            })
        }
        else
        {
            response.forEach((comment, index) =>
            {
                if(comment.entity_id != null &&
                    this.matchId != null &&
                   comment.entity_id == this.matchId)
                {
                    var cmt = {};
                    cmt.id = comment.id
                    cmt.createdAt = this.formatDate(new Date(comment.created_at))
                    cmt.message = comment.message
                    cmt.userId = comment.user_id
					cmt.userName = comment.user ? comment.user.name : ''
                    this.comments.push(cmt);
                }          
            })
        }
     
   }

   formatDate(dt) 
   {
        const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return (months[dt.getMonth()] + ' ' + dt.getDate() + ', ' + dt.getFullYear())
    }

    saveComments(event)
    {
        var _this= this

        this.successSaveMatches = []
        this.failedSaveMatches = []

        if (this.newComment != null &&
				this.newComment.length > 0 &&
				this.searchId != null)
        {
            if(this.isCaseCommentDialog || this.isMatchCommentDialog)
            {
                this.saveCAComment(this.searchId, this.matchId, this.newComment)
            }
            else if(this.matchIds != null && this.matchIds.length >0)
            {
                this.matchIds.forEach(function(matchId, index) 
                {
                    _this.saveCAComment(_this.searchId, matchId, _this.newComment)
                })
            }
        }
    }

    saveCAComment(searchId, matchId, newComment) {
        addComment(
        {
            searchId : searchId,
            matchId: matchId,
            comment : newComment
        }).then(response => {
            this.successSaveMatches.push(matchId != null ? matchId : searchId);
            this.afterCASave(matchId != null ? matchId : searchId)
        }).catch(error =>  {
            this.failedSaveMatches.push(matchId != null ? matchId : searchId);
            this.afterCASave()
        })
    }

    afterCASave(matchId)
    {
        var _this= this

         //post save -> show notification and add note to search record
        if(this.isCaseCommentDialog || this.isMatchCommentDialog)
        {         
            if(this.successSaveMatches.length == 1)
            {
                //notify ComplyAdvantageSearchHitsComponent to update the count
                if(matchId)
                {
                    _this.dispatchEvent(new CustomEvent('newcomment',  
                    { 
                        detail : 
                        {
                            matchid : matchId
                        }
                    }));
                }

                //show success message to user
                this.notifyUser(204, 'Comment added successfully !!')

                //save the notes in Salesforce and attach it to Search record for audit purpose
                this.addSummaryNote();
            }
            if(this.failedSaveMatches.length == 1)
            {
                this.notifyUser(500, 'Failed to add Comment..')

                const closeEvent = new CustomEvent('closecommentsdialog');
                this.dispatchEvent(closeEvent);
            }
        }
        else
        {
            if((this.successSaveMatches.length +  this.failedSaveMatches.length) == this.matchIds.length)
            {
                //show message to user with both success and failure count
                let message = (this.successSaveMatches.length > 0 ? 'comment added successfully for ' +  this.successSaveMatches.length  + ' match(es).' : '')
                message += (this.failedSaveMatches.length > 0 ? + ' Failed to add comments for ' + this.failedSaveMatches.length + ' match(es).' : '')            
                let variantType = (this.successSaveMatches.length == this.matchIds.length) ? 204 : 500                
                this.notifyUser(variantType, message) 
                
                //notify ComplyAdvantageSearchHitComponent to update the message count
                if(this.successSaveMatches.length > 0)
                {
                    this.successSaveMatches.forEach(function(successSaveMatchId, index) 
                    {
                        _this.dispatchEvent(new CustomEvent('newcomment',  
                        { 
                            detail : 
                            {
                                matchid : successSaveMatchId
                            }
                        }));
                    })

                    this.addSummaryNote();
                }            
               
            }
        }
    }
    addSummaryNote()
    {
        var matchName;
        if(this.matchInfo != null)
        {
            matchName = this.matchInfo.name
        }
        if(this.matchList != null && this.matchList.length > 0)
        {
            let matchNames = []
            let selectedMatches = this.matchList.filter(match => this.successSaveMatches.indexOf(match.entityId) != -1)
            selectedMatches.forEach(function(selectedMatch, index) 
            {
                matchNames.push(selectedMatch.name);
            })
            matchName = matchNames.join(',\n')
        }

        addCommentsSummaryNote(
        {
            searchId : this.searchId,
            matchName: matchName,
            comment : this.newComment
        }).then(response=>
        {
           
        }).catch(error=>
        {
           
        })
    }
    closeWindow(event)
    {
        const closeEvent = new CustomEvent('closecommentsdialog');
        this.dispatchEvent(closeEvent);
    }

    notifyUser(statusCode, message)
    {
       //let message = statusCode == 204 ? 'Comment added successfully' : 'Failed to add comments';
       let variant =  statusCode == 204 ? 'success' : 'error';
 
       this.dispatchEvent(new ShowToastEvent({
          title: 'New Comment',
          message: message,
          variant: variant,
          mode : 'dismissable'
        }));
    }
    
}