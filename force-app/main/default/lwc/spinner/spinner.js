import { LightningElement, api, track, wire } from 'lwc';

export default class Spinner extends LightningElement {
    @api showSpinner = false;
    @api variant = 'brand';
    @api size = 'large';
    @api cssClass = 'container';
    @api loadingMessage = '';
}