import { LightningElement, wire, api, track} from 'lwc';

import { getRecord } from 'lightning/uiRecordApi';
import { showToast, validateEmail, sleep, formatLabelWithParameters  } from 'c/utils'; 
import CURRENT_USER_ID from '@salesforce/user/Id';
import CURRENT_USER_NAME from '@salesforce/schema/User.Name';
import CURRENT_USER_EMAIL from '@salesforce/schema/User.Email';
import CURRENT_USER_SIGNATURE from '@salesforce/schema/User.Signature';
import getRecordEmail from "@salesforce/apex/EmailComponentController.getRecordEmail";
import getEmailTemplates from "@salesforce/apex/EmailComponentController.getEmailTemplates";
import getFolders from "@salesforce/apex/EmailComponentController.getFolders";
import getTemplateDetails from "@salesforce/apex/EmailComponentController.getTemplateDetails";
import sendAnEmailMsg from "@salesforce/apex/EmailComponentController.sendAnEmailMsg";
import getTranslatedTemplate from "@salesforce/apex/EmailComponentController.getTranslatedTemplate";

//import custom labels
import OAP_DIsclaimer from "@salesforce/label/c.OAP_DIsclaimer";
import OAU_DIsclaimer from "@salesforce/label/c.OAU_DIsclaimer";
import OCAN_Disclaimer from "@salesforce/label/c.OCAN_Disclaimer";
import OC_Disclaimer from "@salesforce/label/c.OC_Disclaimer";
import OEL_DIsclaimer from "@salesforce/label/c.OEL_DIsclaimer";
import OEM_Disclaimer from "@salesforce/label/c.OEM_Disclaimer";
import OGM_DIsclaimer from "@salesforce/label/c.OGM_DIsclaimer";
import OC_ECP_DIsclaimer from "@salesforce/label/c.OC_ECP_DIsclaimer";
import Email_Component_Default_Signature from "@salesforce/label/c.Email_Component_Default_Signature";
import OANDA_LOGO_URL from "@salesforce/label/c.OandaLogoUrl";


export default class EmailComponent extends LightningElement {
    subscription = null;
    @api recordId;
    @api relatedField;
    @api disabledInputFormats = [];
    @api includedTemplateFolders = [];
    @api setDefaultLogo = false;
    @api ccDefaultUser;
    @api disclaimerToggleDisabled = false;
    @api signatureConfig = 'Default Signature';


    setComponentParametrization(paramConfig){
        console.log("paramConfig: " + JSON.stringify(paramConfig));
        this.signatureConfig = paramConfig.Signature__c;
        this.setDefaultLogo = paramConfig.SetDefaultLogo__c;
        this.includedTemplateFolders = paramConfig.TemplateFolders__c;
        this.disabledInputFormats = paramConfig.DisabledEditorControls__c != null && paramConfig.DisabledEditorControls__c != undefined ? paramConfig.DisabledEditorControls__c.split(',') : [];
        this.ccDefaultUser = paramConfig.CC_User__c;
        this.disclaimerToggleDisabled = paramConfig.DisclaimerToggleDisabled__c;
        this.cleanSignature();
        this.setDefaultTemplateFolders();
        this.removeDisabledRichTextControls();
        this.loadDefaultBodyContent();


    }
    @track recordName;
    defaultFromAddress = "frontdesk@oanda.com";
    vfpBaseUrl = '';
    @track fromOptions = [
        { label: "Front Desk <frontdesk@oanda.com>", value: "frontdesk@oanda.com" },
        { label: "Do Not Reply <donotreply@oanda.com>", value: "donotreply@oanda.com" },
    ];
    //"font", "size", "color",\
    richTextInputFormats = [ "font", "size", "color","bold", "italic", "underline", "strike", "list", "indent", "align", "link", "image", "clean", "table", "header", "background"];
    currentUserEmailId;
    templateSearchExtraFields = {fields: ["Description"], mergeTemplate: "{0}"};
    hasRendered = false;
    disclaimerToggleValue = true;
    isFirstSetBody = true;
    retrySetBody = false;
    recCc = {};
    accId = null;
    htmlContext = null;
    @track showBccInput = false;
    @track showCcInput = false;
    @track toAddresses = [];
    @track ccAddresses = [];
    @track bccAddresses = [];
    @track fromAddress;
    @track relatedToSObject;
    @track relatedToIcon;
    @track emailSubject = "";
    @track subject = "";
    @track emailBody = "";
    @track body = "";
    @track signature = "";
    showMergeButtonSpinner = false;
    showSendButtonSpinner = false;
    @track files = [];
    @track templateFolders = [ ];
    @track templates = [ { label: "-- None --", value: "-- None --" }];
    @track selectedFolder = {};
    @track selectedTemplate = {};
    @track selectedTranslatedTemplate = null;
    languagePreference;
    showTemplateModal = false;
    showPreviewModal = false;
    addSignatureToggleValue = true;
    division = '';
    disclaimerText = '';
    relatedToId = null;
    threadId = null;
    fileDownloadBaseUrl = '';
    previewStyle='';
    contextUserName = '';
    @wire(getRecord, { recordId: CURRENT_USER_ID, fields: [CURRENT_USER_EMAIL, CURRENT_USER_NAME, CURRENT_USER_SIGNATURE]}) 
    userDetails({error, data}) {
        if (data) {

            console.log("data: " + JSON.stringify(data));
            this.fromOptions.push({ label: data.fields.Name.value + " <" + data.fields.Email.value + ">", value: data.fields.Email.value });
            this.fromAddress = data.fields.Email.value;
            this.fromName = this.contextUserName = data.fields.Name.value;
            if(data.fields.Signature.value != null && data.fields.Signature.value != undefined && data.fields.Signature.value != 0 && data.fields.Signature.value != 'null' ){
                this.signatureRaw = data.fields.Signature.value;
            } 
            if(this.fromName != null && this.fromName != undefined){
                this.fromName = this.fromName.split(" <")[0];
            }
            this.currentUserEmailId = data.fields.Email.value;
            let input = this.template.querySelector("[data-id='from-input']");
            if(input != null && input != undefined){
                console.log("setting from-input: " + this.fromAddress);
                input.options = this.fromOptions;
                input.value = this.defaultFromAddress;
            }
            this.cleanSignature();
            this.loadDefaultBodyContent();
            
            
        } else if (error) {
            this.error = error ;
        }
    }

    get templateLookupFilters(){
        let ret = '(RelatedEntityType = \'' + this.getSobjectTypeFromId(this.recordId) + '\' OR RelatedEntityType = null) ';
        if(this.selectedFolder != null && this.selectedFolder != undefined && this.selectedFolder.value != 'ALL_FOLDERS'){
            ret += 'AND Folder.Name = \'' + this.selectedFolder.label + '\'';
        }
        
        return ret;
    }

    get transaltedTemplateFound(){
        return this.selectedTranslatedTemplate != null && this.selectedTranslatedTemplate != undefined;
    }

    get enableMergeBtn(){
        return this.selectedTemplate == null || this.selectedTemplate == undefined;
    }

    get showBcc(){
        return this.showBccInput;
    }

    get selectedTemplates() {
        return this.templates.filter(temp => temp.folder === this.selectedFolder.value);
    }

    get availableFolders() {
        return this.templateFolders;
    }

    get currentRecord(){
        return this.recordId;
    }

    get previewBody(){
        let ret = '';
        if(this.htmlContext != null){
            console.log("this.htmlContext is not null" + this.htmlContext.length);
            ret = this.htmlContext + '<body style="margin: 0px; padding: 0px; height: auto; min-height: auto;">';
        }

        if(this.setDefaultLogo){ 
            ret +=  '<p style="text-align: center;padding-right: 3rem;" ><img  class="img-logo" src="' + OANDA_LOGO_URL + '" /> </p>';
        }
        if(this.body != null && this.body != undefined){
            ret += this.body;
        }
        if(this.htmlContext != null){
            ret += '</body></html>';
        }
        console.log("ret previewBody: " + ret);
        if(this.disclaimerToggleValue == false){
            return ret;
        }

        return ret + this.disclaimerText;
    }

    getSobjectTypeFromId(recordId){
        let ret = '';
        if(recordId != null && recordId != undefined){
            switch (recordId.substring(0,3)) {
                case '001':
                    ret = 'Account';
                    break;
                case '003':
                    ret = 'Contact';
                    break;
                case '005':
                    ret = 'User';
                    break;
                case '006':
                    ret = 'Opportunity';
                    break;
                case '00Q':
                    ret = 'Lead';
                    break;
                case '500':
                    ret = 'Case';
                    break;
                default:
                    ret = '';
            }
        }
        return ret;
    }

    cleanSignature(){
        console.log("this.signatureConfig: " + this.signatureConfig);
        console.log("this.fromName: " + this.contextUserName);
        if(this.signatureConfig == 'Default Signature' ){
            this.signatureRaw = formatLabelWithParameters(Email_Component_Default_Signature, [this.contextUserName ]);
        }
        if(this.signatureConfig == 'No Signature' ){
            this.signatureRaw = '';
        }
        this.signature = this.signatureRaw;
        this.signature = this.signature.replaceAll('\r\n', '<br>');
        this.signature = this.signature.replaceAll('\n', '<br>');
        this.signature = this.signature.replaceAll('\r', '<br>');
        this.signature = '<br><br><br>' + this.signature;
    }

    renderedCallback(){
        if(!this.hasRendered){
            this.template.querySelector("[data-id='from-input']").value = this.defaultFromAddress;
            this.fromName = null;
            this.fromAddress = this.defaultFromAddress;
            this.modal = this.template.querySelector(`[data-id='modal']`);
            this.sendBodyContentToVF();
            this.hasRendered = true;
        }
        if(this.showCcInput && this.recCc != null && this.recCc != undefined){
            this.template.querySelector("[data-id='cc-emails-input']").setDefaultEmail(this.recCc);
            this.recCc = null;
        }
    }

    connectedCallback(){
        window.addEventListener('message', this.receiveMessage);
        this.getRecordInfo();
        this.setVfpUrl();
        this.fileDownloadBaseUrl = window.location.origin.split('.com')[0].replace('lightning', 'file') + '.com/sfc/servlet.shepherd/version/download/';
    }

    receiveMessage = (event) => {
        console.log("event.data en el LWC: " + event.data);
        if(event.data != null && event.data.includes('"isLoaded":true') && this.isFirstSetBody ){
            console.log("isLoaded:true en el LWC: " + event.data); 
            this.sendBodyContentToVF();
            return;
        }
        if(event.data != null && event.data.includes('force__vfRecordDataInvalidation') 
            || event.data.includes('"isLoaded"')
            || event.data.includes('locationChanged')
            || event.data.includes('isOriginBacon')
            || event.data.includes('isIosDomElementFixNeeded')
            || event.data.includes('"event":"force')){
            return;
        }
        this.body = event.data;
    }

    setVfpUrl(){
        let aux = window.location.origin;
        aux = aux.replace('lightning', 'vf');
        aux = aux.replace('.sandbox', '--c.sandbox');
        this.vfpBaseUrl = aux;
    }

    setDefaultTemplateFolders(){
        let aux = [];
        if(this.includedTemplateFolders != null && this.includedTemplateFolders != undefined && this.includedTemplateFolders.length > 0){
            this.includedTemplateFolders.split(',').forEach(item => aux.push({ label: item, value: item}));
        }
        this.templateFolders = aux;
        if(this.templateFolders.length == 0){
            this.doGetFolders();
        }
    }

    setDisclaimerFromDivision(){
        let aux = '';
        switch(this.division){
            case 'OANDA Corporate':
                aux = OC_Disclaimer;
                break;
            case 'OANDA Canada':
                aux = OCAN_Disclaimer;
                break;
            case 'OANDA Europe':
                aux = OEL_DIsclaimer;
                break;
            case 'OANDA Corporate ECP':
                aux = OC_ECP_DIsclaimer;
                break;
            case 'OANDA Japan':
                aux = OC_ECP_DIsclaimer;
                break;
            case 'OANDA Australia':
                aux = OAU_DIsclaimer;
                break;
            case 'OANDA Asia Pacific':
                aux = OAP_DIsclaimer;
                break;
            case 'OANDA Asia Pacific CFD':
                aux = OAP_DIsclaimer;
                break;
            case 'OANDA Corporation':
                aux = OC_Disclaimer;
                break;
            case 'OANDA Europe Markets':
                aux = OEM_Disclaimer;
                break;
            case 'OANDA Global Markets':
                aux = OGM_DIsclaimer;
                break;
            case 'OAP':
                aux = OAP_DIsclaimer;
                break;
            case 'OAU':
                aux = OAU_DIsclaimer;
                break;
            case 'OC':
                aux = OC_Disclaimer;
                break;
            case 'OCAN':
                aux = OCAN_Disclaimer;
                break;
            case 'OEL':
                aux = OEL_DIsclaimer;
                break;
            case 'OEM':
                aux = OEM_Disclaimer;
                break;
            case 'OGM':
                aux = OGM_DIsclaimer;
                break;
            default:
                aux = '';
        }
        console.log("aux disclaimer: " + aux);
        this.disclaimerText = '\n\n<br><br><p style="font-size: 0.73em; color: #6f7287;">' + aux + '</p>';

    }

    sendBodyContentToVF() {
        console.log("sendBodyContentToVF");
        console.log("this.template.querySelector(iframe): " + this.template.querySelector("iframe"));
        //Firing an event to send data to VF
        this.body = this.body.replaceAll('bgcolor="', 'style="background-color:');
        if(this.body.includes('<body style="margin: 0px; padding: 0px; height: auto; min-height: auto;">')){
            console.log('includes body tag preformatted')
            this.htmlContext = this.body.split('<body style="margin: 0px; padding: 0px; height: auto; min-height: auto;">')[0];
            console.log("this.htmlContext: " + this.htmlContext);
        }
        if(this.template.querySelector("iframe") != null && this.template.querySelector("iframe") != undefined){
            if(this.isFirstSetBody){
                sleep(1000).then(() => {
                    this.isFirstSetBody = false;
                    this.template.querySelector("iframe").contentWindow.postMessage('FROM_LWC_TO_VFP,' + this.body, this.vfpBaseUrl);
                });
            } else {    
                this.template.querySelector("iframe").contentWindow.postMessage('FROM_LWC_TO_VFP,' + this.body, this.vfpBaseUrl);
            }   
        } else {
            sleep(250).then(() => {
                this.sendBodyContentToVF();
            });
        }
    }

    loadDefaultBodyContent(){
        console.log("loadDefaultBodyContent");
        console.log("this.setDefaultLogo: " + this.setDefaultLogo);
        let aux = '';
        if(this.signature != null && this.signature != undefined && this.signature != 0 && this.signature != 'null'){
            aux += this.signature;
        }
        this.emailBody = this.body = aux;
        this.sendBodyContentToVF();
    }

    removeDisabledRichTextControls(){
        if(this.disabledInputFormats == null || this.disabledInputFormats == undefined || this.disabledInputFormats.length == 0){
            return;
        }
        this.richTextInputFormats = this.richTextInputFormats.filter(item => !this.disabledInputFormats.includes(item));
       
    }

    displayBccInput(){
        this.showBccInput = true;
    }

    displayCcInput(){
        this.showCcInput = true;
    }

    openTemplateModal(){
        this.modal.show();
        this.showPreviewModal = false;
        this.showTemplateModal = true;
        this.selectedTemplate = null;
        
    }

    openPreviewModal(){
        this.modal.show();
        this.showTemplateModal = false;
        this.showPreviewModal = true;
    }

    closeTemplateModal(){
        this.modal.hide();
        this.selectedTemplate = null;
        this.showTemplateModal = false;
    }

    closePreviewModal(){
        this.modal.hide();
        this.showPreviewModal = false;
    }

    isSystemUserEmail(email){
        let sysEmails = ['systemuser.fxtrade@oanda.com','systemuser.fxtrade@oanda.com.invalid'];
        return sysEmails.includes(email);
    }

    getRecordInfo() {
        getRecordEmail({ recordId: this.recordId, relatedField: this.relatedField, ccField: this.ccDefaultUser})
            .then(result => {
                console.log("result getRecordEmail: " + JSON.stringify(result));
                let validEmail = result.email != undefined && validateEmail(result.email) != null;
                let mail = result.email? result.email : '-null-';
                let rec = {showIcon: true, icon:"standard:" + result.sObjectType.toLowerCase(), label:result.name, email: mail, valid: validEmail};
                if(result.ccEmail != undefined && validateEmail(result.ccEmail) != null && !this.isSystemUserEmail(result.ccEmail)){
                    this.showCcInput = true;
                    this.recCc = {showIcon: true, icon:"standard:user", label:result.ccName, email: result.ccEmail, valid: true};
                    
                }
                if(result.division != undefined && result.division != null){
                    this.division = result.division;
                }
                this.threadId = result.threadId;
                this.template.querySelector("[data-id='to-emails-input']").setDefaultEmail(rec);
                let relatedLookup =  this.template.querySelector("[data-id='related-to-lookup']");
                relatedLookup.setSelectedRecord({name: result.name, id: this.recordId});
                relatedLookup.objectApiName = result.sObjectType;
                relatedLookup.parentApiName = result.sObjectType;
                relatedLookup.iconName = rec.icon;
                this.relatedToSObject = result.sObjectType;
                this.relatedToIcon = rec.icon;
                this.recordName = result.name;
                this.relatedToId = result.recordId;
                this.languagePreference = result.languagePreference;
                this.accId = result.accId;
                this.setDisclaimerFromDivision();
                if(result.config != null && result.config != undefined){
                    this.setComponentParametrization(result.config);
                } else {
                    this.setDefaultTemplateFolders();
                }
            })
            .catch(error => {
            this.error = error;
            this.recordsList = undefined;
            console.log("error2 on getRecordEmail: " + JSON.stringify(error));
            });
    }

    handleUploadFinished(event){
        this.files.push(...event.detail.files);
    }

    removeFile(event){
        this.files = this.files.filter(function( obj ) {
            return obj.documentId !== event.currentTarget.getAttribute("data-key");
        });
    }

    handleSubjectChange(event){
        this.subject = event.target.value;
    }

    getTemplates(){
        let folderNames = []
        if(this.selectedFolder != null && this.selectedFolder != undefined){
            folderNames.push(this.selectedFolder.label);
        }
        getEmailTemplates({recordId : this.recordId, folderNames: folderNames.length > 0? folderNames : null})
            .then(result => {
                console.log("result get Templates: " + JSON.stringify(result.length));
                let _this = this;
                let folders = this.templateFolders;
                let temps = this.templates;
                result.forEach(function(template, index){
                    if(folders.filter(fold => fold.value === template.FolderId).length == 0){
                        console.log("adding folder: " + JSON.stringify(template.Folder));
                        if(template.Folder == null || template.Folder == undefined){
                            let existing = folders.filter(fold => fold.label === 'Unfiled Public Email Templates');
                            if(existing.length > 0){
                                template.Folder = {Name: 'Unfiled Public Email Templates', Id: existing[0].value};
                            } else {
                                template.Folder = {Name: 'Unfiled Public Email Templates'};
                                console.log("adding folder: " + template.Folder.Name);
                            }
                        }
                        folders.push({ label: template.Folder.Name, value: template.FolderId});
                    } 
                    temps.push({ label: template.Name, value: template.Id, folder: template.FolderId, subject: template.Subject});
                });
                this.templateFolders = folders;
                this.templates = temps;
                this.selectedFolder = this.templateFolders[0];
                let input = this.template.querySelector("[data-id='template-folders-input']");
                if(input != null && input != undefined){
                    input.options = this.templateFolders;
                    input.value = this.selectedFolder.value;
                }
            })
            .catch(error => {
                this.error = error;
                console.log("error2: " + error);
            });
                
    }

    doGetFolders(){
        getFolders()
            .then(result => {
                console.log("result get Folders: " + JSON.stringify(result));
                this.templateFolders = [{ label: 'Search All', value: 'ALL_FOLDERS'}];
                let _this = this;
                result.forEach(function(folder, index){
                    _this.templateFolders.push({ label: folder.Name, value: folder.Id});
                });
                this.templateFolders = [... this.templateFolders];
                
            })
            .catch(error => {
                this.error = error;
                console.log("error doGetFolders: " + error);
            });
    }

    handletoAddressesChange(event){
        this.toAddresses = event.detail;
    }

    handleCcAddressesChange(event){
        this.ccAddresses = event.detail;
    }

    handleBccAddressesChange(event){
        this.bccAddresses = event.detail;
    }

    handleTemplateFolderChange(event){
        console.log('handleTemplateFolderChange');
        console.log('event: ' + JSON.stringify(event.detail));
        this.selectedFolder = {label: event.detail.label, value: event.detail.value};
        this.selectedTemplate = null;
        this.setSelectedFolderFiltersAndSearch();
    }

    handleTemplateChange(event){
        this.selectedTemplate = {label: event.detail.label, value: event.detail.value};
    }

    insertTemplateMerge(){
        this.doGetTemplateDetails();
    }

    doGetTemplateDetails(){
        this.showMergeButtonSpinner = true;
        getTemplateDetails({templateId: this.selectedTemplate.value, whoId: null, whatId: this.recordId})
            .then(result => {
                this.htmlContext = null;
                this.subject = this.emailSubject = result.subject;
                this.template.querySelector("[data-id='subject-input']").value = this.subject;
                if(this.addSignatureToggleValue){
                    result.body += this.signature;
                }
                this.body = this.emailBody = result.body;
                let tableWidth = this.getTableWidth(this.body);
                if(tableWidth != null){
                    //check if the value is an integer
                    if(!isNaN(tableWidth)){
                        tableWidth = tableWidth + 'px';
                    }
                    //this.previewStyle = 'width: ' + tableWidth + ';';
                }
                console.log("tableWidth: " + tableWidth);
                //this.querySelectorAll('lightning-input-rich-text').forEach(element => {element.value = this.body; });
                this.sendBodyContentToVF();
                this.closeTemplateModal();
                this.showMergeButtonSpinner = false;
                this.validateInputs();
            })
            .catch(error => {
                this.error = error;
                showToast(this, 'Error when inserting Template', error.body.message, 'error', 'dismissable');
                console.log("error2: " + JSON.stringify(error));
            });
    }

    getIndicesOf(searchStr, str) {
        var searchStrLen = searchStr.length;
        if (searchStrLen == 0) {
            return [];
        }
        var startIndex = 0, index, indices = [];
        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    }

    currentBaseUrl(){
        let aux = window.location.href;
        aux = aux.substring(0, aux.indexOf('/lightning/'));
        return aux;
    }

    getInlineImagesIds(){
        this.body = this.body.replaceAll(this.fileDownloadBaseUrl, '');
        this.body = this.body.replaceAll('?asPdf=false&amp;operationContext=CHATTER', '');
        let imgIndices = this.getIndicesOf('src=\"0', this.body);
        let _this = this;
        let ret = [];
        imgIndices.forEach(index => ret.push(_this.body.substring(index + 5, index + 20)));
        return ret;
    }

    sendEmail(){ 

        console.log('sendEmail 1');
        this.showSendButtonSpinner = true;
        if(this.showPreviewModal){
            this.closePreviewModal();
        }
        let validationMessage = this.validateInputs();
        let versionIds = this.getInlineImagesIds();

        console.log('sendEmail 2');
        if(validationMessage.length == 0){
            console.log('sendEmail 3');
            let ptoAddresses = this.getAddressesFromArray(this.toAddresses);
            let pccAddressesStr = this.getAddressesFromArray(this.ccAddresses);
            let pbccAddressesStr = this.getAddressesFromArray(this.bccAddresses);
            let contentDocIds = [];
            this.files.forEach(item => contentDocIds.push(item.documentId));
            console.log('this.secondaryWhatId: ' + this.relatedToSObject == 'Case' ? this.accId : null);   
            console.log('sending the emai');
            console.log(' relatedTo ' + this.relatedToId);
            let auxBody = this.threadId != null && this.threadId != undefined && this.threadId != '' ? this.previewBody +  '<br><br> ' + this.threadId : this.previewBody;
            let auxSubject = this.subject;
            sendAnEmailMsg(
                {   fromAddress: this.fromAddress, 
                    toAddressesStr: ptoAddresses, 
                    ccAddressesStr: pccAddressesStr, 
                    bccAddressesStr: pbccAddressesStr, 
                    subject: auxSubject, 
                    whoId: this.relatedToSObject == 'Lead' ? this.relatedToId : null, 
                    whatId: this.relatedToSObject == 'Lead' ? null : this.relatedToId, 
                    secondaryWhatId: this.relatedToSObject == 'Case' ? this.accId : null, 
                    body: auxBody, 
                    senderDisplayName: this.fromName, 
                    contentDocumentIds: contentDocIds, 
                    attachmentIds: [], 
                    createActivity: true, 
                    contentVersionIds: versionIds,
                    threadId: this.threadId})
                .then(result => {
                    this.resetComponent();
                    this.showSendButtonSpinner = false;
                    showToast(this, 'Email Sent', '', 'success', 'dismissable');
                })
                .catch(error => {
                    this.error = error;
                    showToast(this, 'Error While sending email', error,'error', 'dismissable');
                    this.showSendButtonSpinner = false;
                });

        } else {
            showToast(this, 'Please review the following errors:', validationMessage,'error', 'dismissable');
            this.showSendButtonSpinner = false;
        }
    }

    validateInputs(){

        this.template.querySelectorAll('lightning-input').forEach(element => {element.reportValidity(); });
        let res = "";
        if(this.toAddresses.length == 0){
            res += "Please select a To recipient.\n";
        }
        if(this.subject.length == 0){
            res += "Please select a Subject.\n";
        }
        if(this.toAddresses.filter(function( add ) {
            return add.valid != true; 
        }).length > 0) {
            res += "There are some invalid email addresses in the To recipients.\n\r";
        }
        if(this.ccAddresses.filter(function( add ) {return add.valid != true;}).length > 0) {
            res += "There are some invalid email addresses in the Cc recipients.\n";
        }
        if(this.bccAddresses.filter(function( add ) {return add.valid != true;}).length > 0) {
            res += "There are some invalid email addresses in the Bcc recipients.\n";
        }
        return res;
        
    }

    getAddressesFromArray(addressArray){
        return Array.prototype.map.call(addressArray, function(item) { return item.email; }).join(",");   
    }

    resetComponent(){
        this.template.querySelectorAll('lightning-input').forEach(element => {element.value = ""; });
        //this.template.querySelectorAll('lightning-input-rich-text').forEach(element => {element.value = ""; });
        this.template.querySelector("[data-id='to-emails-input']").clearInput();
        if(this.showCcInput){
            this.template.querySelector("[data-id='cc-emails-input']").clearInput();  
        }
        if(this.showBccInput){
            this.template.querySelector("[data-id='bcc-emails-input']").clearInput();    
        }
        this.template.querySelector("[data-id='related-to-lookup']").removeRecord({});
        this.toAddresses = [];
        this.ccAddresses = [];
        this.bccAddresses = [];
        this.subject = this.emailSubject = "";
        this.body = this.emailBody = "";
        this.files = [];
        this.selectedTemplate = null;
        this.getRecordInfo();
        this.loadDefaultBodyContent();
    }

    handleFromChange(event){
        this.fromAddress = event.detail.value;
        this.fromName = null;

    }

    handleRelatedToSelection(event){
        console.log(" in handleRelatedToSelection");
        console.log("event.detail: " + JSON.stringify(event.detail));
    }
    handleTemplateSelection(event){
        console.log(" in handleTemplateSelection");
        console.log("event.detail: " + JSON.stringify(event.detail));
        this.selectedTemplate = {label: event.detail.selectedValue, value: event.detail.selectedRecordId};
        this.doGetTranslatedTemplate();
    }
    
    doGetTranslatedTemplate(){
        if(this.languagePreference != null && this.languagePreference != undefined && this.languagePreference != ''){
            getTranslatedTemplate({recordId: this.recordId, templateName: this.selectedTemplate.label, language: this.languagePreference})
            .then(result => {
                console.log("result doGetTranslatedTemplate: " + JSON.stringify(result));
                if(result.length > 0){
                    this.templates.push({ label: result[0].Name, value: result[0].Id, folder: result[0].FolderId, subject: result[0].Subject});
                    this.selectedTranslatedTemplate = {label: result[0].Name, value: result[0].Id};
                } else {
                    this.selectedTranslatedTemplate = null;
                }
            })
            .catch(error => {
                this.error = error;
                showToast(this, 'Error when inserting Template', error.body.message, 'error', 'dismissable');
                console.log("error2: " + JSON.stringify(error));
            });
        }
    }

    getTableWidth(htmlBody) {
        // Regular expression pattern to match table tags
        const tableRegex = /<table[^>]*>/g;

        // Find all table tags in the HTML body
        const tableTags = htmlBody.match(tableRegex);

        if (tableTags && tableTags.length > 0) {
            // Extract the first table tag
            const firstTableTag = tableTags[0];

            // Regular expression pattern to match the width property
            const widthRegex = /width="([^"]*)"/;

            // Extract the width property from the first table tag
            const widthMatch = firstTableTag.match(widthRegex);

            if (widthMatch && widthMatch.length > 1) {
                // Return the width value
                return widthMatch[1];
            }
        }

        // Return null if no table tag or width property found
        return null;
    }

    insertTranslatedTemplateMerge(){
        this.selectedTemplate = this.selectedTranslatedTemplate;
        this.doGetTemplateDetails();
    }

    setSelectedFolderFiltersAndSearch(){ 
        let input = this.template.querySelector("[data-id='search-template-lookup']");
        let aux;
        if(this.selectedFolder != null){
            console.log("setting filters: " + this.templateLookupFilters);
            aux = this.templateLookupFilters;
            input.setFilters(aux);
            input.removeRecord();
            input.doGetLookupResult();
        } else {

        }
    }

    handleDisclaimerToggleChange(event){
        console.log("handleDisclaimerToggleChange: " + event.detail.checked);
        this.disclaimerToggleValue = event.detail.checked;
    }

    handleAddSingatureToggleChange(event){
        console.log("handleAddSingatureToggleChange: " + event.detail.checked);
        this.addSignatureToggleValue = event.detail.checked;
    }

}