import { LightningElement, api } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getCaseDetails from '@salesforce/apex/SubmitCaseForApprovalController.getCaseDetails';
import submitApproval from '@salesforce/apex/SubmitCaseForApprovalController.submitApproval';

export default class SubmitCaseForApproval extends LightningElement {

    @api recordId;
    result ='';
    isInitialized = false;
    title = 'Submit for Approval';
    loading=true;
    isMissingCAS = false;
    errorMessage;
    comments;

    renderedCallback() {
        if(!this.isInitialized && this.recordId!=undefined){
            getCaseDetails({
                caseId: this.recordId
            }).then( result => {
                if(result!='ok'){
                    this.isMissingCAS=true;
                }
                this.isInitialized=true;
                this.loading=false;
            });
        }
    }

    handleCommentsChange(event){
        this.comments = event.detail.value;
    }

    handleSubmit(event){
        this.errorMessage='';
        this.loading=true;
        submitApproval({
            caseId: this.recordId, comments: this.comments
        }).then( result => {
            this.loading=false;
            if(result!='ok'){
                this.errorMessage=result;
            }else{
                this.showNotification();
                this.dispatchEvent(new CloseActionScreenEvent());
            }
        });
    }

    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

	showNotification() {
		const evt = new ShowToastEvent({
			title: 'Success',
			message: 'Approval sent.',
			variant: 'success'
		});
		this.dispatchEvent(evt);
	}
}