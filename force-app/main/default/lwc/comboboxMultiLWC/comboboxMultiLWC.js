import { LightningElement, api, track } from 'lwc';

export default class ComboboxMultiLWC extends LightningElement {
    @api title;
    @api placeholder = '';
    selectedvalue = '';
    @api values = [];
    @api multiselectable = false;
    @track isOpen = false;

    selectedElements = new Set();

    get dropdownClass() {
        return this.isOpen ?
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' :
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    }

    handleFocus(event) {
        this.isOpen = true;
    }

    closeDropdown() {
        this.isOpen = false;
    }

    handleOnChecked(event){
        this.selectedElements.add(event.detail.value);
        this.updateSelectedValues(this.selectedElements);
    }

    handleOnUnchecked(event){
        this.selectedElements.delete(event.detail.value);
        this.updateSelectedValues(this.selectedElements);
    }

    updateSelectedValues(elements){
        var selectedvaluesList = [];
        this.selectedvalue = '';
        for (let item of elements){
            selectedvaluesList.push(item);
        }
        if(elements.size == 1)
            this.selectedvalue = selectedvaluesList[0];
        else if(elements.size > 1)
            this.selectedvalue = elements.size + ' selected';

        this.dispatchSetValue(selectedvaluesList);
    }

    dispatchSetValue(value) {
        const selectedvalue = new CustomEvent('changevalue',
        {
            detail: value
        });
        this.dispatchEvent(selectedvalue);
    }
}