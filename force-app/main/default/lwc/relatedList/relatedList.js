/**
 * @description       :
 * @author            : Yaneivys Gutierrez
 * @group             :
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
 **/
import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import fetchRelatedList from "@salesforce/apex/RelatedListCntrl.fetchRelatedList";

export default class RelatedList extends LightningElement {
	@api recordId;
	@api title;
	@api relatedMdtDevName;
	wrapper;
	columns;
	data;
	colspan;
	loaded;
	label = {};

	connectedCallback() {
		this.loadRecords();
	}
	loadRecords() {
		fetchRelatedList({
			recordId: this.recordId,
			mdtDevName: this.relatedMdtDevName
		})
			.then((r) => {
				if (r) {
					this.wrapper = r;
					this.loadColumns(r);
					this.loaded = true;
				}
				console.log("wrapper: ", r);
			})
			.catch((error) => {
				this.onError(error);
			});
	}
	loadColumns(r) {
		this.data = [];
		this.columns = {};
		this.colspan = 0;

		var result = this.loadColumnsRecursive(r);
		if (result.columns) {
			this.columns = result.columns;
		}
		if (result.data) {
			this.data = result.data;
		}
		if (!this.title) {
			this.title = result.title;
		}
	}
	loadColumnsRecursive(r) {
		const data = [];
		let columns = {};
		for (let i = 0; i < r.records.length; i++) {
			const fields = r.records[i].fields;
			const record = {};
			for (let j = 0; j < fields.length; j++) {
				record[fields[j].apiName] = {
					label: fields[j].label,
					type: fields[j].type,
					value: fields[j].value
				};
			}

			const children = r.records[i].children;
			const childrenData = [];
			let haveChildren = false;
			if (children.length > 0) {
				for (let k = 0; k < children.length; k++) {
					const result = this.loadColumnsRecursive(children[k]);
					if (result) {
						childrenData.push(result);
					}
					if (result.data.length > 0) {
						haveChildren = true;
					}
				}
			}

			data.push({
				id: r.records[i].id,
				colspan: fields.length + 1,
				record: Object.entries(record).map(([key, value]) => ({
					key,
					value
				})),
				children: childrenData,
				haveChildren,
				mainClass: haveChildren ? "accordion" : ""
			});
		}

		for (let i = 0; i < r.fieldLabels.length; i++) {
			columns[r.apiNames[i]] = r.fieldLabels[i];
		}

		return {
			title: r.mdtName,
			columns: Object.entries(columns).map(([key, value]) => ({
				key,
				value
			})),
			data
		};
	}

	onError(err) {
		console.log("RD-Error:", err);
	}

	showError(aTitle) {
		showError(aTitle, "");
	}

	showError(aTitle, aMessage) {
		const evt = new ShowToastEvent({
			title: aTitle,
			message: aMessage,
			variant: "Error"
		});
		this.dispatchEvent(evt);
	}
}