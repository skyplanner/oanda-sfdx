import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getInitialValues from '@salesforce/apex/RandomizerLWCController.getInitialValues';
import getRandomCases from '@salesforce/apex/RandomizerLWCController.getRandomCases';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class RandomizerLWC extends NavigationMixin(LightningElement) {
    @track filtersIsOpen = true;
    @track kpiIsOpen = true;
    @track ticketsIsOpen = true;
    @track loading = true;
    @track cases = [];
    filters = {
        caseOwnerFilter: null,
        caseOwnerRoleFilter: null,
        accountDivisionFilter: null,
        chatDivisionFilter: null,
        caseOriginFilter: null,
        inquireNatureFilter: null,
        typeFilter: null,
        subTypeFilter: null,
        fromDateFilter: null,
        toDateFilter: null,
        recordTypeFilter: null,
        channelFilter: null,
        languagePreferenceFilter: null,
        chatLanguageFilter: null,
        chatDurationFilter: null,
        countryOfResidencyFilter: null,
        agentTenureFilter: null,
        numberCasesFilter: 20,
        qiReviewedFilter: null,
        isReviewdFilter: null,
        caseFirstOwnerFilter : null,
        caseQIOwnerFilter : null,
        isHVCFilter: null
    };

    originOptions = [];
    @track originSelected;

    inquireNatures = [];
    types = [];
    subTypes = [];

    @track fromDate;
    @track toDate;

    tenureOptions = [
        { label: 'All', value: '' },
        { label: '< 0.5 Year', value: '< 0.5 Year' },
        { label: '< 1 Year', value: '< 1 Year' },
        { label: '> 1 Year', value: '> 1 Year' }];

    booleanOptions = [
        { label: 'All', value: '' },
        { label: 'True', value: 'True' },
        { label: 'False', value: 'False' }];

    get filtersSectionClass() {
        return this.filtersIsOpen ? 'slds-section slds-is-open' : 'slds-section';
    }

    get kpiSectionClass() {
        return this.kpiIsOpen ? 'slds-section slds-is-open' : 'slds-section';
    }

    get ticketsSectionClass() {
        return this.ticketsIsOpen ? 'slds-section slds-is-open' : 'slds-section';
    }

    get hasCases(){
        return this.cases && this.cases.length > 0;
    }

    filtersToggleShow(){
        this.filtersIsOpen = !this.filtersIsOpen;
    }

    kpiToggleShow() {
        this.kpiIsOpen = !this.kpiIsOpen;
    }

    ticketToggleShow() {
        this.ticketsIsOpen = !this.ticketsIsOpen;
    }

    handleMultiSelectChange(event){
        this.filters.caseOriginFilter = event.detail;
    }

    handleInputChange(event) {
        
        let value = event.target.value;
        if (!event.target.value)
            value = null;

		console.log(event.target.id);

        if (event.target.id.indexOf('form-element-01') === 0)
            this.filters.caseOwnerFilter = value;
        else if(event.target.id.indexOf('form-element-02') === 0)
            this.filters.caseOwnerRoleFilter = value;
        else if (event.target.id.indexOf('form-element-03') === 0)
            this.filters.accountDivisionFilter = value;
        else if (event.target.id.indexOf('form-element-04') === 0)
            this.filters.chatDivisionFilter = value;
        else if (event.target.id.indexOf('form-element-05') === 0)
            this.filters.caseOriginFilter  = value;
        else if (event.target.id.indexOf('form-element-06') === 0)
            this.filters.inquireNatureFilter = value;
        else if (event.target.id.indexOf('form-element-07') === 0)
            this.filters.typeFilter = value;
        else if (event.target.id.indexOf('form-element-08') === 0)
            this.filters.subTypeFilter = value;
        else if (event.target.id.indexOf('form-element-09') === 0)
            this.filters.fromDateFilter = value;
        else if (event.target.id.indexOf('form-element-11') === 0)
            this.filters.toDateFilter = value;
        else if (event.target.id.indexOf('form-element-10') === 0)
            this.filters.recordTypeFilter = value;
        else if (event.target.id.indexOf('form-element-12') === 0)
            this.filters.channelFilter = value;
        else if (event.target.id.indexOf('form-element-13') === 0)
            this.filters.languagePreferenceFilter = value;
        else if (event.target.id.indexOf('form-element-14') === 0)
            this.filters.chatLanguageFilter = value;
        else if (event.target.id.indexOf('form-element-15') === 0)
            this.filters.chatDurationFilter = value;
        else if (event.target.id.indexOf('form-element-16') === 0)
            this.filters.countryOfResidencyFilter = value;
        else if (event.target.id.indexOf('form-element-17') === 0)
            this.filters.agentTenureFilter = value;
        else if (event.target.id.indexOf('form-element-18') === 0)
            this.filters.numberCasesFilter = value;
        else if (event.target.id.indexOf('form-element-19') === 0)
            this.filters.qiReviewedFilter = value;
        else if (event.target.id.indexOf('form-element-20') === 0)
            this.filters.isComplaintFilter = value;
        else if (event.target.id.indexOf('form-element-22') === 0)
            this.filters.caseQIOwnerFilter = value;
        else if (event.target.id.indexOf('form-element-23') === 0)
            this.filters.isHVCFilter = value;
    }

    connectedCallback() {
        getInitialValues()
        .then(
            initialValues => {
                this.loading = false;
                if (initialValues) {
                    this.originOptions = initialValues.caseOriginValues.map(row => ({
                            label: row.label,
                            value: row.value,
                            selected: false
                        }));
                    this.inquireNatures = initialValues.inquireNatureValues;
                    this.types = initialValues.typeValues;
                    this.subTypes = initialValues.subTypeValues;
                    this.recordTypes = initialValues.recordTypeValues;
                }
            }
        )
        .catch(error => {
            this.loading = false;
            const evt = new ShowToastEvent({
                title: 'Opss!',
                message: error.body.message,
                variant: 'Error',
            });
            this.dispatchEvent(evt);
        });
    }

    applyFilters() {
        this.loading = true;
        getRandomCases({ pFilters: this.filters})
        .then(
            resCases => {
                if (resCases) {
                    this.cases = resCases.map(row => ({
                        CaseNumber: row.CaseNumber,
                        Chat_Division__c: row.Chat_Division__c,
                        RecordType : {Name:row.RecordType.Name},
                        Type : row.Type__c,
                        CreatedDate: row.CreatedDate,
                        Case_Channel__c: row.Case_Channel__c,
                        Country__c: row.Country__c,
                        Origin: row.Origin,
                        url: '/lightning/r/Case/' + row.Id + '/view'
                    }));
                }                
                this.loading = false;
            }
        )
        .catch(error => {            
            const evt = new ShowToastEvent({
                title: 'Opss!',
                message: error.body.message,
                variant: 'Error',
            });
            this.dispatchEvent(evt);
            this.loading = false;
        });
    }
}