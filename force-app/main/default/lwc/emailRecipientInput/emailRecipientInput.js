import { LightningElement, wire, api, track } from "lwc";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { NavigationMixin } from "lightning/navigation";
import { formatLabelWithParameters, validateEmail } from "c/utils";
import searchRecords from "@salesforce/apex/LookupController.searchRecords";
export default class EmailRecipientInput extends NavigationMixin(LightningElement) {
  @api selectedValue = false;
  @api objectApiName;
  @api parentApiName;
  @api fieldApiName;
  @api iconName;
  @api extraFields = {fields: ["Email"], mergeTemplate: "{0}"};
  @api showCcButton = false;
  @api showBccButton = false;
  message;
  recordsList;
  searchKey = "";
  inputSearchKey = "";
  showRecordsList = false; 
  isRecentRecords = false;
  NO_RECORDS_FOUND = "No records found";
  RECENT_RECORDS = "Recent Records";
  @track selectedEmails = [];

  labels = {
    recentRecordsLabel: this.RECENT_RECORDS,
    required: this.REQUIRED_FIELD,
  };

  get searchCssClass() {
    let css = "slds-input slds-combobox__input searchCssClass";
    if (this.showRecordsList) css += " slds-has-focus";
    return css;
  }

  get hasHelptext() {
    return this.showHelptext;
  }
  connectedCallback() {
    this.selectedEmails = this.selectedEmails;
  }

  renderedCallback(){
  }

  @api
  setDefaultEmail(record) {
    console.log('en setdefaultEmail : ' + JSON.stringify(record));
    this.selectedEmails.push({showIcon: true, icon:record.icon, label:record.label, email:record.email, valid: record.valid, cssClass: this.pillCssClass(record.valid) });
    console.log('setdefaultemail:' + JSON.stringify(record));
    this.sendCurrentEmailList();
  }

  @api
  clearInput(){
    this.selectedEmails = [];
  }

  setSearchFocus(){
    this.template.querySelector("[data-id='search-input']").focus();
  }  
  
  removeSearchFocus(){
    this.template.querySelector("[data-id='search-input']").blur();
  }

  onLeave(event) {
    this.showRecordsList = false;
    if(this.searchKey.length > 0){
      this.addManualEmail();
    }
  }

  onFocus() {
    this.showRequired = false;
  }

  showLabelWithEmail(event) {
    console.log('en showLabelWithEmail');
    console.log('event.currentTarget.getAttribute("data-email"):' + JSON.stringify(event.currentTarget.dataset));
    event.stopPropagation();
    let aux = this.selectedEmails.filter(function( obj ) {
      return obj.email == event.currentTarget.dataset.mail;
    });
    if(aux.length > 0){
      aux[0].showEmailAddress = true;
    }
  }

  handleRecordSelection(event) {
    let aux = this.selectedEmails.filter(function( obj ) {
      return obj.email == event.currentTarget.getAttribute("data-email");
    });
    if(aux.length == 0){
      this.selectedEmails.push({showIcon: true, icon:event.currentTarget.getAttribute("data-icon"), label:event.currentTarget.getAttribute("data-name"), email:event.currentTarget.getAttribute("data-email"), valid: true, cssClass: this.pillCssClass(true) });
    }
    this.inputSearchKey = "";
    this.showRecordsList = false;
    this.sendCurrentEmailList();
  }

  handleSearchChange(event) {
    
    console.log('event.key:' + event.key);
    if(event.key == 'Enter'|| event.key == 'Tab' ) {
      this.addManualEmail();
    } 
  }

  handleKeyPress(event){
    console.log('event.key:' + event.key);
    if(event.key == 'Enter'|| event.key == 'Tab' ) {
      this.addManualEmail();
    } else if(event.key == 'Backspace' && this.searchKey.length == 0){
      this.selectedEmails.pop();
    }
  }

  handleSearch(event) {
    console.log('event.key:' + event.key);
    this.inputSearchKey = this.searchKey = event.target.value;
    this.showRecordsList = true;
    if(event.key == 'Enter' || event.key == 'Tab' ) {
      this.addManualEmail();
    } else {
      this.getLookupResult();
    }
  }

  addManualEmail(){
    if(this.searchKey.length > 0) {
      let validEmail = validateEmail(this.searchKey) != null;
      this.selectedEmails.push({ 
                      showIcon: !validEmail, icon: "standard:scheduling_constraint", label: this.searchKey, email: this.searchKey, valid: validEmail, cssClass: this.pillCssClass(validEmail) });
      this.searchKey = this.inputSearchKey = "";
      this.showRecordsList = false;
      this.sendCurrentEmailList();
      this.removeSearchFocus();
    }
  }

  removeRecordOnLookup(event) {
    this.selectedEmails = this.selectedEmails.filter(function( obj ) {
      return obj.email !== event.currentTarget.dataset.email;
    });
    this.sendCurrentEmailList();
  }

  getLookupResult() {
    if(this.searchKey.length > 1){
      searchRecords({ searchKey: this.searchKey})
        .then(result => {
          if (result.length === 0) {
            this.recordsList = [];
          } else {
            this.recordsList = [];
            for (var [key, values] of Object.entries(result)) {
              values.forEach(aux => {
                let record = {...aux};
                record.icon = "standard:" + key.toLowerCase() ;
                if(key == 'Account') record.Email = record.PersonEmail;
                this.recordsList.push(record);
              });
            }
            this.message = "";
            this.prepareRows();
          }
          this.error = undefined;
          if(this.recordsList.length == 0) this.message = this.NO_RECORDS_FOUND;
        })
        .catch(error => {
          this.error = error;
          this.recordsList = undefined;
          console.log("error2: " + error);
        });
    }
    
  }

  showBcc(event) {
    event.stopPropagation();
    console.log('en showBcc');
    const showBcc = new CustomEvent("openbcc", {});
    this.showBccButton = false;
    this.dispatchEvent(showBcc);
  }

  showCc(event) {
    event.stopPropagation();
    const showCc = new CustomEvent("opencc", {});
    this.showCcButton = false;
    this.dispatchEvent(showCc);
  }

  sendCurrentEmailList() {
    const evt = new CustomEvent("emailschanged", {
      detail:  this.selectedEmails
    });
    this.dispatchEvent(evt);
  }

  prepareRows() {
    let newList = [];
    this.recordsList.forEach(record => {
      let newRecord = { ...record };
      if (this.extraFields != null && this.extraFields.fields.length > 0) {
        var values = [];
        this.extraFields.fields.forEach(field => {
          values.push(record[field]);
        });
        newRecord.Description = formatLabelWithParameters(this.extraFields.mergeTemplate, values);
      }
      newList.push(newRecord);
    });
    this.recordsList = newList;
  }

  pillCssClass(isValid) {
    let cls = "slds-box slds-combobox__form-element slds-input-has-icon slds-input-has-icon_left-right mail-pill";
    if(!isValid) {
      cls += " invalid-mail";
    }
    return cls;
  }

}