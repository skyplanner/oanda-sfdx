/**
 * @description       :
 * @author            : OANDA
 * @group             :
 * @last modified on  : 02-06-2023
 * @last modified by  : Dianelys Velazquez
 **/
import { LightningElement, api, wire, track } from "lwc";
import { getRecord } from "lightning/uiRecordApi";

const columns = [
    { label: 'Activity', fieldName: 'activityDate', initialWidth: 180 },
    { label: 'Details', fieldName: 'details', wrapText: true},
];

export default class HelpDeskLWC extends LightningElement {
	@api recordId;
	@track case;
	historyList;
	columns = columns;
	error;

	@wire(getRecord, { recordId: "$recordId", fields: ["Case.HD_Legacy_Ticket_History__c"] })
	getCaseRecord({ data, error }) {
		this.historyList = [];

		if (data) {
			this.case = data;
			this.processCase();
		} else if (error) {
			this.error = error;
			console.error("ERROR: ", JSON.stringify(error));
		}
	}

	processCase() {
		this.error = undefined;
		var legacyHistory = this.case.fields
			.HD_Legacy_Ticket_History__c.value;

		if (!legacyHistory) {
			this.error = "Legacy Ticket History field is empty.";
			return;
		}

		try {			
			const legacyHistoryList = JSON.parse(legacyHistory);

			for (var i = 0; i < legacyHistoryList.length; i++) {
				this.historyList.push({
					activityDate: legacyHistoryList[i].activity,
					details: legacyHistoryList[i].details
				});
			}
		} catch (error) {
			this.error = error;
		}
	}
}