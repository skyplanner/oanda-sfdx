/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 01-09-2023
 * @last modified by  : Dianelys Velazquez
**/
import { LightningElement, api, track } from 'lwc';

export default class UserApiSearchLwc extends LightningElement {
    @track extCriteria = {
        userName: '',
        v20AccId: '',
        mt4AccId: '',
        v20AccEnv: 'live',
        userNameEnv: 'all'
    };
    @track sfCriteria = {
        email: '',
        accName: '',
        userId: ''
    };

    get envOptions() {
        return [
            { label: 'Live', value: 'live' },
            { label: 'Demo', value: 'demo' }
        ];
    }

    get userEnvOptions() {
        return [
            { label: 'All', value: 'all' },
            { label: 'Live', value: 'live' },
            { label: 'Demo', value: 'demo' }
        ];
    }

    @api
    reset() {
        this.cleanExtCriteria();
        this.cleanSfCriteria();
        this.dispatch();
    }
    
    // External Search

    updateCriteriaUserName(event) {
        this.extCriteria.userName = event.detail.value;
        this.extCriteria.v20AccId = '';
        this.cleanSfCriteria(); 
        this.dispatch();
    }

    updateCriteriaUserNameEnv(event) {
        this.extCriteria.userNameEnv = event.detail.value;
        this.extCriteria.v20AccId = '';
        this.cleanSfCriteria(); 
        this.dispatch();
    }

    updateCriteriaV20AccId(event) {
        this.extCriteria.v20AccId = event.detail.value;
        this.extCriteria.userName = '';
        this.cleanSfCriteria(); 
        this.dispatch();
    }

    updateCriteriaV20AccEnv(event) {
        this.extCriteria.v20AccEnv = event.detail.value;
        this.extCriteria.userName = '';
        this.cleanSfCriteria(); 
        this.dispatch();
    }

    updateCriteriAmt4AccId(event) {
        this.extCriteria.mt4AccId = event.detail.value;
        this.cleanSfCriteria(); 
        this.dispatch();
    }

    // Salesforce Search
    
    updateCriteriaEmail(event) {
        this.sfCriteria.email = event.detail.value;
        this.cleanExtCriteria(); 
        this.dispatch();
    }

    updateCriteriaAccName(event) {
        this.sfCriteria.accName = event.detail.value;
        this.cleanExtCriteria(); 
        this.dispatch();
    }

    updateCriteriaUserId(event) {
        this.sfCriteria.userId = event.detail.value;
        this.cleanExtCriteria(); 
        this.dispatch();
    }

    cleanExtCriteria() {
        this.isSfSearch = true;
        this.isExtSearch = false;
        this.extCriteria = {
            userName: '',
            v20AccId: '',
            mt4AccId: '',
            v20AccEnv: 'live',
            userNameEnv: 'all'
        };
    }

    cleanSfCriteria() {
        this.isExtSearch = true;
        this.isSfSearch = false;
        this.sfCriteria = {};
    }

    isOkSearch() {
        return (this.sfCriteria.email || this.sfCriteria.accName 
            || this.sfCriteria.userId || this.extCriteria.userName 
            || this.extCriteria.v20AccId || this.extCriteria.mt4AccId) 
            ? true : false;
    }

    dispatch() {
        var data = {
            extCriteria: this.extCriteria,
            sfCriteria: this.sfCriteria,
            isSfSearch: this.isSfSearch,
            isExtSearch: this.isExtSearch,
            isOkSearch: this.isOkSearch()
        };

        console.log('dispatch data:', data);

        this.dispatchEvent(new CustomEvent("updatecriteria", {detail: data}));
    }

    handleEnter(event) {
        if (this.isOkSearch() && event.keyCode === 13)
            this.dispatchEvent(new CustomEvent("enterkey", {detail: {}}));
    }
}