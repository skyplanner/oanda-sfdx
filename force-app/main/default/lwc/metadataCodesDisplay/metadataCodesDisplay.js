import { LightningElement, api, track, wire} from 'lwc';
import { getRecord,getFieldValue } from 'lightning/uiRecordApi';

import REJECTION_REASON_CODE from '@salesforce/schema/TAX_Declarations__c.Rejection_Reason_Code__c';
import getMetadataRecords from '@salesforce/apex/MetadataCodesDisplayController.getMetadataRecords';

const columns = [
    { label: 'Code', fieldName: 'DeveloperName' },
    { label: 'Description', fieldName: 'Description__c', wrapText: true}
];

export default class MetadataCodesDisplay extends LightningElement {
    @api recordId;
    @api customMetadataName;
    @api customMetadataFields;
    isLoading = false;
    @track customMetadataList = [];

    errorCodes;
    taxDelaration;

    columns = columns;


    @wire(getRecord, { recordId: '$recordId', fields: [REJECTION_REASON_CODE] })
    wiredRecord({ error, data }) {
        if (data) {
            this.errorCodes = getFieldValue(data, REJECTION_REASON_CODE);
            this.connectedCallback();
        } else if (error) {
            console.error('Error fetching account:', error);
        }
    }
    connectedCallback () {
        this.retrieveCustomMetadata();
    }

    async retrieveCustomMetadata() {
        if (this.customMetadataName && this.errorCodes) {
            this.isLoading = true;
            try {
                this.customMetadataList = await getMetadataRecords({metadataName: this.customMetadataName, 
                                                                    metadataFields: this.customMetadataFields, 
                                                                    errorCodes: this.errorCodes});
            } catch (error) {
                console.error('Error retrieving custom metadata:', error);
            } finally {
                this.isLoading = false;
            }
        } else {
            this.customMetadataList = null;
        }
    }
}