import { LightningElement, api } from 'lwc';

export default class Illustrationnodatalwc extends LightningElement {
    @api mainmessage;
    @api submessage;
}