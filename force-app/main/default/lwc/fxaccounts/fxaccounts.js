import { LightningElement, wire, api } from 'lwc';
import getFxAccounts from '@salesforce/apex/FxAccountListviewCtrl.getFxAccounts';
import { NavigationMixin } from 'lightning/navigation';

const columns = [
    { label: 'Account Name', fieldName: "recordLink", type: "url", wrapText: true,
        typeAttributes: {
            label: { fieldName: "accountName" },
            tooltip:"Name",
            target: "_blank"}
    },
    { label: 'Primary fxAccount', fieldName: 'primaryFxAccount', wrapText: true},
    { label: 'FxAccount Name', fieldName: 'fxAccountName', wrapText: true},
    { label: 'Email', fieldName: 'emailAddress', wrapText: true},
    { label: 'Account Locked', fieldName: 'accountLocked', type: 'boolean', wrapText: true},
    { label: 'IS Closed', fieldName: 'accountClosed', type: 'boolean', wrapText: true},
    { label: 'Primary Division Name', fieldName: 'primaryDivisionName', wrapText: true},
    { label: 'Ready for Funding Date/Time', fieldName: 'Ready_for_Funding_DateTime__c', type: 'date',
        typeAttributes:{
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        }, wrapText: true},
    { label: 'Division Name', fieldName: 'divisionName', wrapText: true},
    { label: 'Funnel Stage', fieldName: 'funnelStage', wrapText: true}
];

export default class FxaccountsListview extends NavigationMixin(LightningElement) {
    @api recordId;
    @api hideFooter = false;
    fxAccounts;
    error;
    columns = columns;

    connectedCallback(){
        getFxAccounts({accountId: this.recordId})
            .then(data => {
                if (data.length > 0) {
                    console.log('data ===> ', data);
                    this.fxAccounts = data.map(function(row) {
                        let newrow = Object.assign({}, row);
                        newrow.recordLink= "/" + newrow.accId;
                        newrow._children = newrow.children;
                        return newrow;
                    });
                    this.error = undefined;
                }
            })
            .catch(error => {
                console.log('error ===> ', error);
                this.error = error.body.message;
                this.fxAccounts = undefined;
            });
    }

    get hasFxAccounts(){
        return this.fxAccounts && this.fxAccounts.length > 0;
    }

    openFxAccountList(){
        this[NavigationMixin.Navigate]({
            type: 'standard__component',
            attributes: {
                componentName: 'c__fxaccountsListViewWrapper',
                tabLabel: 'fxAccounts'
            },
            state: {
                c__id: this.recordId
            }
        });
    }
}