import { LightningElement, track, api } from 'lwc';
import getInitialValues from '@salesforce/apex/QualityIndexingLWCController.getInitialValues';
import updateQI from '@salesforce/apex/QualityIndexingLWCController.updateQI';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class qualityIndexingLWC extends LightningElement {

    @api recordId;

    @track speedToAnswerValue = null;
    @track accuracyValue = null;
    @track toneValue = null;
    @track literacyValue = null;
    @track taggingValue = null;
    @track firstResponseValue = null;
    @track selfServiceValue = null;
    @track tradingAdvice;

    @track notChanges = true;
    @track loading = true;
    @track score;
    @track qiCaseOwnerId;

    speedToAnswerValues = [];
    accuracyValues = [];
    toneValues = [];
    literacyValues = [];
    taggingValues = [];
    firstResponseValues = [];
    selfServiceValues = [];
    caseType;

    get isPhoneOrChat(){
        return this.caseType === 'Call' || this.caseType === 'Chat';
    }

    get isEmail() {
        return this.caseType === 'Email';
    }

    get isEmailOrChat() {
        return this.caseType === 'Email' || this.caseType === 'Chat';
    }

    checkBoxChange(event){
        this.notChanges = false;
        this.tradingAdvice = !this.tradingAdvice;
        console.log('this.tradingAdvice ===> ', this.tradingAdvice);
    }

    changeValue(event){
        const key = event.detail.key;
        const value = event.detail.value;
        this.notChanges = false;

        if (key === 'Speed to Answer')
            this.speedToAnswerValue = value;
        if (key === 'First Response Time')
            this.firstResponseValue = value;
        if (key === 'Self Service Method')
            this.selfServiceValue = value;
        if (key === 'Accuracy')
            this.accuracyValue = value;
        if (key === 'Tone/Mannerism')
            this.toneValue = value;
        if (key === 'Literacy/Grammar')
            this.literacyValue = value;
        if (key === 'Case Tagging')
            this.taggingValue = value;
    }

    handleQiCaseOwnerChanged(event) {
		this.qiCaseOwnerId = event.detail;
		this.notChanges = false;
	}

    connectedCallback() {
        getInitialValues({ pCaseId: this.recordId})
            .then(
                initialValues => {
                    this.loading = false;
                    if (initialValues) {
                        this.speedToAnswerValues = initialValues.speedToAnswerValues;
                        this.accuracyValues = initialValues.accuracyValues;
                        this.toneValues = initialValues.toneValues;
                        this.literacyValues = initialValues.literacyValues;
                        this.taggingValues = initialValues.taggingValues;
                        this.firstResponseValues = initialValues.firstResponseValues;
                        this.selfServiceValues = initialValues.selfServiceValues;
                        this.caseType = initialValues.caseType;
                        this.tradingAdvice = initialValues.isTradingAdvice;

                        this.speedToAnswerValue = initialValues.speedToAnswer;
                        this.accuracyValue = initialValues.accuracy;
                        this.toneValue = initialValues.tone;
                        this.literacyValue = initialValues.literacy;
                        this.taggingValue = initialValues.tagging;
                        this.firstResponseValue = initialValues.firstResponse;
                        this.selfServiceValue = initialValues.selfService;
                        this.score = initialValues.score;
                        this.qiCaseOwnerId = initialValues.qiCaseOwnerId;
                    }
                }
            )
            .catch(error => {
                this.loading = false;
                const evt = new ShowToastEvent({
                    title: "Opss!",
                    message: error.body.message,
                    variant: "Error",
                });
                this.dispatchEvent(evt);
            });
    }

    submitForm(){
        this.loading = true;
		let wrapper = {
			speedToAnswer: this.speedToAnswerValue,
			accuracy: this.accuracyValue,
			tone: this.toneValue,
			literacy: this.literacyValue,
			tagging: this.taggingValue,
			firstResponse: this.firstResponseValue,
			selfService: this.selfServiceValue,
			isTradingAdvice: this.tradingAdvice,
			qiCaseOwnerId: this.qiCaseOwnerId
		};

        updateQI({ 
            pCaseId: this.recordId, 
            pFromWrapper : wrapper
        }).then(
            result => {
                this.notChanges = true;
                this.loading = false;
                this.score = result;
            }
        )
        .catch(error => {
            this.loading = false;
            const evt = new ShowToastEvent({
                title: "Opss!",
                message: error.body.message,
                variant: "Error",
            });
            this.dispatchEvent(evt);
        });
    }
}