/**
 * Custom Lookup componenent to be used in any custom form.
 * @authro Fernando Gomez, SkyPlanner LLC
 * @version 1.0
 */
import { LightningElement, wire, api } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import search from '@salesforce/apex/LookupCtrl.doSearch';
import getCurrent from '@salesforce/apex/LookupCtrl.getCurrentRecord';

export default class Lookup extends LightningElement {

	// lookup label
	@api
	label;

	// placeholder
	@api
	placeholder;

	@api
	variant;

	@api
	objectApiName;

	@api
	searchInFields;

	// lookn and feel props
	label;
	style;
	iconUrl;

	// the current record
	record;

	// internal record id
	recordId;

	// show menu when focused
	showMenu;

	// keywords as specified by user
	keywords;

	// resulting object selectable as per the search
	results;

	// value, an Id pointing to a record, or null is value is empty
	@api
	get value() {
		return this.recordId;
	}

	set value(v) {
		// we make sure the value
		// came from the right place
		let isGood = v == null ||
			(typeof v === "string" &&
				(v.length == 0 || v.length == 15 || v.length == 18));

		// we need an Id for this value, null is good
		if (isGood) {
			if (this.recordId != v) {
				this.recordId = v;
				this.reset();
			}
		} else
			console.error("Invalid Id: " + JSON.stringify(v));
	}

	@wire(
		getObjectInfo,
		{ objectApiName: "$objectApiName" }
	)
	handleResult({error, data}) {
		if (data) {
			let themeInfo = data.themeInfo || {};
			// the original object cannot be expanded,
			// so we use a custom one to add properties accrdingly
			this.iconUrl = themeInfo.iconUrl;
			this.style = `background-color: #${themeInfo.color};`;
		}

		if (error)
			console.error(error);
	}

	/**
	 * main constructor
	 */
	connectedCallback() {
		this._setDefaults();
		this.reset();
	}

	/**
	 * Resets the lokkup for a new value
	 */
	reset() {
		console.log("reseting...");
		this._clearSearch();

		// if we have no record, we fetch it
		if (this.recordId)
			this._getCurrent(r => {
				this._applySelection(r);
			});
		else
			this._clearSelection();
	}

	/**
	 * Handles On Blur
	 * @param {*} e 
	 */
	handleOnBlur(event) {
		// we hide the menu in 300 ms
		setTimeout(() => {
			//this._clearSearch();
		}, 300);
	}

	/**
	 * Handles On Change
	 * @param {*} event 
	 */
	handleOnChange(event) {
		// we update the value ok keywords
		this.keywords = (event.target.value || "").trim();

		// if keywords were cleared, we clear the search
		if (!this.keywords) {
			this._clearSearch();
		} else {
			// we'll search for results and
			// show them in the list
			this.showMenu = true;
			this._search((r) => {
				this.results = r;
			});
		}
	}

	/**
	 * Select the current record
	 * @param {*} event 
	 */
	handleOnSelect(event) {
		let index = event.currentTarget.dataset.index;
		this._applySelection(this.results[index]);
		// this._clearSearch();
		this.dispatchEvent(new CustomEvent("change", { detail: this.recordId }));
	}

	/**
	 * Select the current record
	 * @param {*} event 
	 */
	handleOnRemoveSelection(event) {
		this._clearSelection();
		this._clearSearch();
		this.dispatchEvent(new CustomEvent("change", { detail: null }));
	}

	/**
	 * Sets default values for empty fields
	 */
	_setDefaults() {
		if (!this.searchInFields)
			this.searchInFields = ["Name"];

		if (!this.iconName)
			this.iconName = "utility:sobject";
	}

	/**
	 * Searches for records in the server
	 * @param {*} oncomplete 
	 */
	_search(oncomplete) {
		search({
			"keyword": this.keywords,
			"objectName": this.objectApiName,
			"searchInFields": this.searchInFields
		})
		.then(result => {
			oncomplete(result);
		})
		.catch(error => {
			console.error(error);
			oncomplete([]);
		});
	}

	/**
	 * Fetches an existing record
	 * @param {*} oncomplete 
	 */
	_getCurrent(oncomplete) {
		getCurrent({
			"recordId": this.recordId,
			"objectName": this.objectApiName,
			"searchInFields": this.searchInFields
		})
		.then(result => {
			oncomplete(result);
		})
		.catch(error => {
			console.error(error);
			oncomplete(null);
		});
	}

	/**
	 * Removes the result set and clears the search
	 */
	_clearSearch() {
		this.showMenu = false;
		this.keywords = null;
		this.results = [];
	}

	/**
	 * Applies the record as the selection
	 */
	_applySelection(record) {
		this.record = record;
		this.recordId = record ? record.Id : null;
	}

	/**
	 * Removes the selected record
	 */
	_clearSelection() {
		this.record = null;
		this.recordId = null;
	}
}