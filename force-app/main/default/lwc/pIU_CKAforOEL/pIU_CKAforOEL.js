import { LightningElement, api, wire, track  } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import getQandAs from '@salesforce/apex/PIUCKAHelperClass.getQandA';
import RISK_ACK_Q1_FIELD from '@salesforce/schema/fxAccount__c.Risk_Acknowledgement_Question_1__c';
import RISK_ACK_Q2_FIELD from '@salesforce/schema/fxAccount__c.Risk_Acknowledgement_Question_2__c';
import DIVISION_NAME_FIELD from '@salesforce/schema/fxAccount__c.Division_Name__c';

const OAU = 'OANDA Australia'

export default class PIU_CKAforOEL extends LightningElement 
{
    @api recordId;
    @track hasItemsToShow;
    @track itemsToShow=[];
    @track picklistvalues=[];
    @track selectedValue =null ;
    @track currentQandAset;
    @track currentPIU;


    @wire(getQandAs , {fxAccountId : '$recordId'})
    wiredQandAs(value)
    {
        const {error,data} = value;
        if(data && data.length > 0)
        {
            this.itemsToShow = data;
            this.hasItemsToShow = true;
            for(let temp in data)
            {
                if( this.selectedValue === null)
                {
                    this.selectedValue = data[temp].Id;
                    this.currentQandAset = data[temp].Questionnaire__r ;
                    this.currentPIU = data[temp];
                }
                this.picklistvalues.push({ value: data[temp].Id,label: data[temp].Name});
                
            }
        }
        else
        {
            this.hasItemsToShow = false;
        }
    }

    handleChange(event)
    {
        this.selectedValue = event.detail.value;
        for(let temp in this.itemsToShow)
        {
            if(this.selectedValue === this.itemsToShow[temp].Id)
            {
                this.currentQandAset = this.itemsToShow[temp].Questionnaire__r ;
                this.currentPIU =  this.itemsToShow[temp];                
            }
        }
    }

    @wire(getRecord, { recordId: '$recordId', fields: [
        RISK_ACK_Q1_FIELD,
        RISK_ACK_Q2_FIELD,
        DIVISION_NAME_FIELD
    ]})
    fxAccount;

    get riskQuestion1() {
        return getFieldValue(this.fxAccount.data, RISK_ACK_Q1_FIELD);
    }

    get riskQuestion2() {
        return getFieldValue(this.fxAccount.data, RISK_ACK_Q2_FIELD);
    }

    get isOAU() {
        return getFieldValue(this.fxAccount.data, DIVISION_NAME_FIELD) === OAU ? true : false;
    }
}