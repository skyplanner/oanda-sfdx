/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 07-15-2022
 * @last modified by  : Ariel Niubo
**/
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export function normalizeString (value, config = {}) {
  const { fallbackValue = '', validValues, toLowerCase = true } = config
  let normalized = (typeof value === 'string' && value.trim()) || ''
  normalized = toLowerCase ? normalized.toLowerCase() : normalized
  if (validValues && validValues.indexOf(normalized) === -1) {
    normalized = fallbackValue
  }
  return normalized
}

export function normalizeBoolean (value) {
  return typeof value === 'string' || !!value
}

export function normalizeArray (value) {
  if (Array.isArray(value)) {
    return value
  }
  return []
}

export function normalizeAriaAttribute (value) {
  let arias = Array.isArray(value) ? value : [value]
  arias = arias
		.map(ariaValue => {
  if (typeof ariaValue === 'string') {
    return ariaValue.replace(/\s+/g, ' ').trim()
  }
  return ''
})
		.filter(ariaValue => !!ariaValue)

  return arias.length > 0 ? arias.join(' ') : null
}

/**
 * Common method for showing toast messages
 * @param eventTarget
 * @param title
 * @param message
 * @param variant
 * @param mode
 */
export function showToast(eventTarget, title = '', message = '', variant = 'info', mode = 'dismissable') {
  const evt = new ShowToastEvent({
      title,
      message: getFilteredErrorMessage(message),
      variant,
      mode
  });

  if(variant === 'error' && message !== evt.message){
      //Log the error object to the console.
      console.log(message);
  }

  eventTarget.dispatchEvent(evt);
}

/**
 * Common method for showing toast messages on the community.
 * @param eventTarget
 * @param title
 * @param message
 * @param variant
 * @param mode
 */
export function showToastCommunity(eventTarget, title = '', message = '', variant = 'info', mode = 'dismissable',obj = '') {
  const evt = new ShowToastEvent({
      title,
      message: message,
      variant,
      mode
  });



  if(variant === 'error'){
      if(obj != ''){
          console.log(getFilteredErrorMessage(obj));
      }
  }

  eventTarget.dispatchEvent(evt);
}


const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};
const formatLabelWithParameters = (label, args) => {
  return label.replace(/{(\d+)}/g, function(match, number) {
    return typeof args[number] != "undefined" ? args[number] : match;
  });
};

/**
 * Gets error message that is thrown from apex
 * @param msg
 * @returns {string|*}
 */
export function getFilteredErrorMessage(msg = '') {
  if (msg.hasOwnProperty('message')) {
      return msg.message;
  }

  if (msg.hasOwnProperty('body') && msg.body.hasOwnProperty('enhancedErrorType')) {
      if (msg.body.enhancedErrorType === 'RecordError') {
          if (msg.body.hasOwnProperty('output')) {
              return msg.body.output.errors[0].message;
          }
      }
  }

  if (msg.hasOwnProperty('body') && msg.body.hasOwnProperty('message')) {
      return msg.body.message;
  }

  if (msg.hasOwnProperty('body') && msg.body.hasOwnProperty('pageErrors')) {
      const pageError = msg.body.pageErrors[0];
      if (pageError.hasOwnProperty('message')) {
          return pageError.message;
      }
  }

  return msg;
}

export const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

export {
  validateEmail,
  formatLabelWithParameters
}