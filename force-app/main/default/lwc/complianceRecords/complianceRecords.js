import { LightningElement, api } from 'lwc';
import fetchAgreements from '@salesforce/apex/ComplianceRecordsCtrl.fetchAgreements';
import fetchKIDs from '@salesforce/apex/ComplianceRecordsCtrl.fetchKIDs';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const COLUMNS_LA = [
    {
        fieldName: 'name',
        label: 'Name',
        type: 'text'
    },
    { 
        fieldName: 'version',
        label: 'Version',
        type: 'text'
    },
    { 
        fieldName: 'update_Datetime',
        label: 'Updated',
        type: 'date',
        typeAttributes: {
            year: "numeric",
            month: "numeric",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        }
    }
];

const COLUMNS_KIDS = [
    {
        fieldName: 'name',
        label: 'Document Name',
        type: 'text'
    },
    {
        fieldName: 'mt5AccountNumber',
        label: 'MT5 Account Number',
        type: 'text'
    },
    {
        fieldName: 'mt5OrderNumber',
        label: 'MT5 Order #',
        type: 'text'
    },
    { 
        fieldName: 'version',
        label: 'Document Version',
        type: 'text'
    },
    { 
        fieldName: 'dateSent',
        label: 'Date Sent',
        type: 'date',
        typeAttributes: {
            year: "numeric",
            month: "numeric",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit"
        }
    }
];

export default class ComplianceRecords extends LightningElement {

    @api recordId;
    @api header;

    columnsLA = COLUMNS_LA;
    legalAgreements;
    errorLA;
    loadedLA;

    columnsKIDs = COLUMNS_KIDS;
    kids;
    errorKIDs;
    loadedKIDs;
    displayKIDsSection = false;

    connectedCallback() {
        this.fetchAgreements();
        this.fetchKIDs();
    }

    fetchAgreements(){
        console.log('fetchAgreements...');
        this.loadedLA = false;
        fetchAgreements({fxAccId: this.recordId})
            .then(data => {
                console.log('legal agrements:', data);

                if (data.length) {
                    this.legalAgreements = this.prepareRowsForGridTree(data);
                } else {
                    this.legalAs = [];
                }

                this.loadedLA = true;
            })
            .catch(error => {
                console.log('error ===> ', error);
                this.errorLA = error.body.message;
                this.legalAgreements = undefined;
                this.loadedLA = true;
            });
    }

    fetchKIDs() {
        console.log('fetchKIDs...');
        this.loadedKIDs = false;
        fetchKIDs({fxAccId: this.recordId})
            .then(data => {
                console.log('KIDs', data);

                if(data.isAllowed) {
                    this.displayKIDsSection = true;

                    if (data.results) {
                        this.kids = this.prepareRowsForGridTree(data.results);
                    } else {
                        this.kids = [];
                    }
                }

                this.loadedKIDs = true;
            })
            .catch(error => {
                console.log('error ===> ', error);
                this.errorKIDs = error.body.message;
                const event = new ShowToastEvent({
                    title: 'Failed to load KIDs data',
                    message: this.errorKIDs,
                    variant: 'error'
                });
                this.dispatchEvent(event);
                this.kids = undefined;
                this.loadedKIDs = true;
            });
    }

    prepareRowsForGridTree(array) {
        return array.map(function(row) {
            let newrow = Object.assign({}, row);
            
            if(newrow.children &&
                newrow.children.length) {
                    newrow._children = newrow.children;
                    delete newrow.children;
                }
            
            return newrow;
        });
    }

    refreshLA(){
        console.log('refresh...');
        this.fetchAgreements();
    }

    refreshKIDs(){
        console.log('refresh...');
        this.fetchKIDs();
    }

    get hasLA(){
        return this.legalAgreements && this.legalAgreements.length;
    }

    get hasKIDs(){
        return this.kids && this.kids.length;
    }

}