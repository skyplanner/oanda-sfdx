/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-20-2022
 * @last modified by  : Ariel Niubo
 **/
import { LightningElement, api,track } from "lwc";
import { normalizeBoolean } from "c/utils";
import SCHEDULED_BUTTON_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toolbar_scheduled_btn";
import SAVE_BUTTON_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toolbar_save_btn";
import CANCEL_BUTTON_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toolbar_cancel_btn";
import EDIT_BUTTON_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toolbar_edit_btn";
import REMOVE_BUTTON_LABEL from "@salesforce/label/c.Scheduled_email_ctrl_toolbar_delete_btn";

export default class ScheduledEmailToolbar extends LightningElement {
	@track _disabled = false;
	@track _readOnly = false;
	@api
	get disabled() {
		return this._disabled || this._readOnly || false;
	}
	set disabled(value) {
		this._disabled = normalizeBoolean(value);
	}
	@api
	get readOnly() {
		return this.disabled;
	}
	set readOnly(value) {
		this._readOnly = normalizeBoolean(value);
	}

	@api mode = "create" | "edit" | "update";
	get isCreateMode() {
		return this.mode === "create" || this.mode === "update";
	}

	get isUpdateMode() {
		return this.mode === "update";
	}

	get isEditMode() {
		return this.mode === "edit";
	}

	get saveLabel() {
		return this.mode === "create"
			? SCHEDULED_BUTTON_LABEL
			: SAVE_BUTTON_LABEL;
	}
	get cancelLabel() {
		return CANCEL_BUTTON_LABEL;
	}
	get editLabel() {
		return EDIT_BUTTON_LABEL;
	}
	get deleteLabel() {
		return REMOVE_BUTTON_LABEL;
	}
	handleClick(event) {
		event.stopPropagation();
		this.triggerEvent(event.target.name);
	}
	triggerEvent(action) {
		this.dispatchEvent(new CustomEvent(action));
	}
}