import { LightningElement, api, track } from "lwc";

export default class Picklist extends LightningElement {
  @api
  set options(value) {
    this._options = JSON.parse(JSON.stringify(value));
    this.processOptions();
  }
  get options() {
    return this._options;
  }

  @api
  set value(value) {
    console.log(' en picklist set value: ' + value);
    this._value = value;
    this.setSelectedItem(value, false);
  }
  get value() {
    return this._value;
  }

  @api
  set iconOptions(value) {
    this._iconOptions = value;
  }
  get iconOptions() {
    return this._iconOptions;
  }

  @api disabled = false;
  @api label;
  @api placeholder = "";
  @api dropUp = false;
  @api hidelabel = "false";

  @track _options;

  _value;
  _iconOptions;
  _cancelBlur = false;
  customIcons = false;
  showListbox = false;
  inputValue;
  iconOption1Selected = false;
  iconOption2Selected = false;
  isFocused = false;
  keyboardSelect = false;
  keyBuffer = [];

  processOptions() {
    this.processIconOptions();
    let selectedOption = this.selectedOption ? this.selectedOption.value : null;
    this.setSelectedItem(selectedOption, false);
  }

  get inputClass() {
    let inputClass = "slds-input slds-combobox__input slds-combobox__input-value";
    if (this.isFocused) {
      inputClass += " slds-has-focus";
    }
    if (!this.selectedOption) {
      inputClass += " placeholder-color";
    }
    return inputClass;
  }

  get showLabel() {

    return this.hidelabel != "true";
  }

  get inputDivClass() {
    return this.showInputIcon ? "slds-combobox__form-element slds-input-has-icon slds-input-has-icon_left-right" : "slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right";
  }
  get dropdownClass() {
    let value = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";

    if (this.showListbox) {
      value += " slds-is-open";
    }

    return value;
  }
  get arrowIconName() {
    return this.dropUp ? "utility:up" : "utility:down";
  }
  get iconOption1Variant() {
    return this._iconOptions[0].variant;
  }
  get iconOption1Icon() {
    return this._iconOptions[0].iconName;
  }
  get iconOption2Variant() {
    return this._iconOptions[1].variant;
  }
  get iconOption2Icon() {
    return this._iconOptions[1].iconName;
  }
  get dropdownLengthClass() {
    let value = "slds-dropdown slds-dropdown_fluid";
    if (this.dropUp) {
      value += " dropup";
    }
    value += this._options.length > 5 ? " slds-dropdown_length-5" : " slds-dropdown_length-5";
    return value;
  }
  get selectedVariant() {
    let variant;
    if (this.iconOption1Selected) {
      variant = this._iconOptions[0].variant;
    } else if (this.iconOption2Selected) {
      variant = this._iconOptions[1].variant;
    }
    return variant;
  }
  get selectedIcon() {
    let icon;
    if (this.iconOption1Selected) {
      icon = this.iconOptions[0].iconName;
    } else if (this.iconOption2Selected) {
      icon = this.iconOptions[1].iconName;
    }
    return icon;
  }
  get showInputIcon() {
    return this.customIcons && (this.iconOption1Selected || this.iconOption2Selected);
  }
  get selectedOption() {
    return this._options.find(option => {
      return option.value === this._value;
    });
  }

  processIconOptions() {
    if (this._iconOptions) {
      this.customIcons = true;

      this._options.forEach(option => {
        if (option[this.iconOptions[0].name]) {
          option.iconOption1 = true;
        } else if (this.iconOptions.length > 1 && option[this.iconOptions[1].name]) {
          option.iconOption2 = true;
        }

        if (this.selectedOption === option.label) {
          this.setIconVisibility(option);
        }
      });
    }
  }

  setSelectedItem(itemKey, dispatchEvent = true) {
    let selectedLabel;

    if (!itemKey) {
      itemKey = "";
    }

    if (this._options) {
      this._options.forEach(option => {
        if (option.value === itemKey) {
          option.selected = true;
          // set inputValue (option label)
          selectedLabel = option.label;
          // set value (option value)
          this._value = option.value;

          // set icon visibility
          if (option.iconOption1) {
            this.iconOption1Selected = true;
            this.iconOption2Selected = false;
          } else if (option.iconOption2) {
            this.iconOption1Selected = false;
            this.iconOption2Selected = true;
          } else {
            this.iconOption1Selected = false;
            this.iconOption2Selected = false;
          }

          if (dispatchEvent) {
            //dispatch select event
            this.dispatchEvent(
              new CustomEvent("change", {
                detail: { label: option.label, value: option.value }
              })
            );
          }
        } else {
          option.selected = false;
        }
      });
    }

    this.setInputValue(selectedLabel);
    this.hideListbox();
  }

  setInputValue(value) {
    this.inputValue = value ? value : this.placeholder;
  }

  showListboxPanel() {
    this.showListbox = true;
    this.highlightSelectedOption();
  }

  hideListbox() {
    this.showListbox = false;
    this.removeHighlight();
  }

  highlightSelectedOption() {
    //if there is a selected option, highlight it on open
    if (this.selectedOption) {
      let key = this.selectedOption.value;
      this.highlightOption(key);
    } else {
      //if no option selected, highlight the first option
      this.highlightOption(this._options[0].value);
    }
  }

  highlightOption(optionKey) {
    this.removeHighlight();
    let option = this.template.querySelector(`[data-key="${optionKey}"]`);
    if (option) {
      this._highlightedOption = optionKey;
      option.classList.add("slds-has-focus");
      option.setAttribute("aria-selected", true);

      //only scroll into view when using keyboard
      if (this.keyboardSelect) {
        //if not visible, scroll option into view
        this.scrollOptionIntoViewIfHidden(option);
        this.keyboardSelect = false;
      }
    }
  }

  scrollOptionIntoViewIfHidden(option) {
    const parent = this.template.querySelector("[data-id=listbox-div]"),
      parentRect = parent.getBoundingClientRect(),
      optionRect = option.getBoundingClientRect();

    if (optionRect.bottom > parentRect.bottom) {
      //option is hidden below parent rect
      parent.scrollTop += optionRect.bottom - parentRect.bottom;
    } else if (optionRect.top < parentRect.top) {
      //option is hidden above parent rect
      parent.scrollTop = option.offsetTop;
    }
  }

  removeHighlight() {
    if (this._highlightedOption) {
      let option = this.template.querySelector(`[data-key="${this._highlightedOption}"]`);
      if (option) {
        option.classList.remove("slds-has-focus");
        option.setAttribute("aria-selected", false);
      } else {
        this._highlightedOption = undefined;
      }
    }
  }

  highlightNextItem() {
    let nextIndex;
    if (this._highlightedOption) {
      let index = this.getIndexByValue(this._highlightedOption);

      if (index === this._options.length - 1) {
        nextIndex = 0;
      } else {
        nextIndex = ++index;
      }

      this.highlightItemByIndex(nextIndex);
    }
  }

  highlightPreviousItem() {
    let prevIndex;
    if (this._highlightedOption) {
      let index = this.getIndexByValue(this._highlightedOption);

      if (index === 0) {
        prevIndex = this._options.length - 1;
      } else {
        prevIndex = --index;
      }

      this.highlightItemByIndex(prevIndex);
    }
  }

  highlightPageDown() {
    let pageDownIndex, currentIndex;
    if (this._highlightedOption) {
      currentIndex = this.getIndexByValue(this._highlightedOption);
    } else {
      currentIndex = 0;
    }

    pageDownIndex = currentIndex + 10;

    if (pageDownIndex > this._options.length - 1) {
      pageDownIndex = this._options.length - 1;
    }

    this.highlightItemByIndex(pageDownIndex);
  }

  highlightPageUp() {
    let pageUpIndex, currentIndex;
    if (this._highlightedOption) {
      currentIndex = this.getIndexByValue(this._highlightedOption);
    } else {
      currentIndex = 0;
    }

    pageUpIndex = currentIndex - 10 >= 0 ? currentIndex - 10 : 0;
    this.highlightItemByIndex(pageUpIndex);
  }

  highlightItemByIndex(index) {
    if (this._options && this._options.length > index - 1) {
      this.highlightOption(this._options[index].value);
    }
  }

  getIndexByValue(value) {
    return this._options.findIndex(option => option.value === value);
  }

  handleDropdownTriggerClick(event) {
    event.stopPropagation();

    if (this.showListbox) {
      this.hideListbox();
    } else {
      this.showListboxPanel();
    }
  }

  handleListItemClick(event) {
    let listItem = event.target.dataset.key;
    this.setSelectedItem(listItem);
    //return focus to input after item selected
    this.template.querySelector("[data-id=picklistInput]").focus();
    event.preventDefault();
  }

  handleFocus() {
    this.isFocused = true;
  }

  handleBlur() {
    if (!this._cancelBlur) {
      this.isFocused = false;
      this.hideListbox();
    }
  }

  handleInputKeyDown(e) {
    switch (e.key) {
      case "Down": // IE/Edge specific value
      case "ArrowDown":
        if (this.showListbox) {
          this.keyboardSelect = true;
          this.highlightNextItem();
        } else {
          this.showListboxPanel();
          //highlight first item
          this.highlightItemByIndex(0);
        }
        break;
      case "Up": // IE/Edge specific value
      case "ArrowUp":
        if (this.showListbox) {
          this.keyboardSelect = true;
          this.highlightPreviousItem();
        } else {
          this.showListboxPanel();
          //highlight last item
          this.highlightItemByIndex(this._options.length - 1);
        }
        break;
      case "PageUp":
        if (!this.showListbox) {
          this.showListboxPanel();
        }
        this.keyboardSelect = true;
        this.highlightPageUp();
        break;
      case "PageDown":
        if (!this.showListbox) {
          this.showListboxPanel();
        }
        this.keyboardSelect = true;
        this.highlightPageDown();
        break;
      case "Home":
        if (!this.showListbox) {
          this.showListboxPanel();
        }
        this.keyboardSelect = true;
        //highlight first item
        this.highlightItemByIndex(0);
        break;
      case "End":
        if (!this.showListbox) {
          this.showListboxPanel();
        }
        this.keyboardSelect = true;
        //highlight last item
        this.highlightItemByIndex(this._options.length - 1);
        break;
      case "Enter":
        if (this.showListbox) {
          //set highlighted option to checked when enter pressed
          if (this._highlightedOption) {
            this.setSelectedItem(this._highlightedOption);
          }
        } else {
          this.showListboxPanel();
        }
        break;
      case "Esc": // IE/Edge specific value
      case "Escape":
        this.hideListbox();
        break;
      case "Tab":
        // prevent tab being consumed
        // by preventDefault below
        return;
      default:
        if (e.key.length === 1) {
          this.bufferKeysThenHighlight(e.key);
        } else {
          return;
        }
    }

    // Cancel the default action to avoid it being handled twice
    e.preventDefault();
  }

  bufferKeysThenHighlight(key) {
    const currentKeyTime = Date.now();

    //if its been more than .8 seconds, reset buffer
    if (this.lastKeyTime && currentKeyTime - this.lastKeyTime > 800) {
      this.keyBuffer = [];
    }

    this.lastKeyTime = currentKeyTime;
    this.keyBuffer.push(key);
    this.highlightOptionByText(this.keyBuffer.join("").toLowerCase());
  }

  highlightOptionByText(text) {
    for (let i = 0; i < this._options.length; i++) {
      const option = this._options[i];

      if (!option.header && option.label && option.label.toLowerCase().indexOf(text) === 0) {
        //set keyboard select so it will scroll to the item
        this.keyboardSelect = true;
        this.highlightItemByIndex(i);
        //match found, so return
        return;
      }
    }
  }

  //stops blur event from firing on mouse down
  handleDropdownMouseDown() {
    this._cancelBlur = true;
  }

  handleDropdownMouseUp() {
    this._cancelBlur = false;
  }

  handleMouseDown(e) {
    e.preventDefault();
  }

  handleLiMouseEnter(e) {
    let optionKey = e.target.dataset.key;

    if (this.selectedOption && this.selectedOption.value !== optionKey) {
      this.removeHighlight(this.selectedOption.value);
    }

    this.highlightOption(optionKey);
  }
}