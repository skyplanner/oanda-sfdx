import { LightningElement, track, api } from 'lwc';
import getDetails
    from "@salesforce/apex/ComplyAdvantageSearchEntities.getSingleEntity"; 

const matchSourceMap = {
    'sanction' : 'sanction',
    'warning'  : 'warning',
    'fitness-probity'  : 'fitness-probity',
    'pep' : 'pep',
    'pep-class-1' : 'pep',
    'pep-class-2' : 'pep',
    'pep-class-3' : 'pep',
    'pep-class-4' : 'pep',
    'adverse-media' : 'adverse-media',
    'adverse-media-financial-crime' : 'adverse-media',
    'adverse-media-violent-crime' : 'adverse-media',
    'adverse-media-sexual-crime' : 'adverse-media',
    'adverse-media-terrorism' : 'adverse-media',
    'adverse-media-fraud' : 'adverse-media',
    'adverse-media-narcotics' : 'adverse-media',
    'adverse-media-general' : 'adverse-media'
}

export default class ComplyAdvantageMatchDetailsComponent extends LightningElement 
{
    @api matchDetail

    matchInformation = {};

	isLoading = false;

	@track
	mediaList = [];
	
	@track
	sourceList = [];

    connectedCallback()
    {
        this.getDisplayInformation()
    }

    getDisplayInformation()
    {
        this.matchInformation.name = this.matchDetail.Name;
        this.getKeyInfo();

		// BEFORE CALLING THIS TWO METHODS,
		// WE NEED TO FECTH THE SINGLEW ENTITY
		// INFO FROM COMPLY ADVANTAGE...
        this.getListingDetails();
    }

    getKeyInfo() { 
        this.matchInformation.keyInfoList = [];

		this.addDetail(
			this.matchInformation.keyInfoList,
			'Full Name',
			this.matchDetail.Name);

		if (this.matchDetail.Entity_Type__c)
			this.addDetail(this.matchInformation.keyInfoList,
				'Entity Type',
				this.matchDetail.Entity_Type__c);

		if (this.matchDetail.AKA__c)
			this.addDetail(this.matchInformation.keyInfoList,
				'AKA',
				this.matchDetail.AKA__c);
		
		//countries
		if (this.matchDetail.Countries__c)
			this.addDetail(this.matchInformation.keyInfoList,
				'Countries',
				this.matchDetail.Countries__c);

		//matchInfo.yearOfBirth = 
		if (this.matchDetail.Date_Of_Birth__c)
			this.addDetail(this.matchInformation.keyInfoList,
				'Date Of Birth',
				this.matchDetail.Date_Of_Birth__c)
    }

	getListingDetails() {
		this.isLoading = true;

		getDetails({
			"searchId": this.matchDetail.Comply_Advantage_Search__c,
			"entityId": this.matchDetail.Entity_Id__c,
		}).then(result => {
			this.getMediaList(result);
			this.getSourceList(result);
			this.isLoading = false;
		}).catch(error => {
			console.error(error);
			this.isLoading = false;
		});
	}

	/**
	 * Polishes the media information
	 */
	getMediaList(details) {
		if (details && details.uncategorized && details.uncategorized.media)
			details.uncategorized.media.forEach((m, index) => {
				let item = {
					id: index,
					url: m.url,
					title: m.title,
					snippet: m.snippet,
					publishedInfo: m.date ?
						this.formatDate(new Date(m.date)) :
						'(no date)'
				};
				this.mediaList.push(item);
			});
	}

	/**
	 * Polishes the full listing information
	 */
	getSourceList(details) {
		if (details && details.full_listing) {
			var count = 0;

			for (var category in details.full_listing) {
				let data = details.full_listing[category];
				
				for (var name in data) {
					let listing = data[name];
					let item = {
						id: count++,
						category: category,
						categoryGroup: matchSourceMap[category],
						name: name,
						list: []
					};

					if (listing.data)
						listing.data.forEach((src, index) => {
							this.addDetail(
								item.list,
								src.name,
								src.value);
						});

					this.sourceList.push(item);
				}
			}
		}
	}

    addDetail(parentCollection, key, value)
    {
        var keyObj = {}
        keyObj.id = Math.random()
        keyObj.label = key
        keyObj.value = value
        parentCollection.push(keyObj)
    }

    formatDate(dt) 
    {
        var hours = dt.getHours();
        var minutes = dt.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return (dt.getMonth()+1) + "/" + dt.getDate() + "/" + dt.getFullYear() + "  " + strTime;
   }

    closeWindow(event)
    {
        const closeEvent = new CustomEvent('closematchdetailsdialog');
        this.dispatchEvent(closeEvent);
    }
}