import { LightningElement, wire, api } from "lwc";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { NavigationMixin } from "lightning/navigation";
import { formatLabelWithParameters } from "c/utils";

import findRecords from "@salesforce/apex/LookupController.findRecords";
import getRecentlyViewed from "@salesforce/apex/LookupController.getRecentlyViewed";
import getRecentEmailTemplates from "@salesforce/apex/EmailComponentController.getRecentEmailTemplates";

import NO_RECORDS_FOUND from "@salesforce/label/c.NoRecordsFound";
import RECENT_RECORDS from "@salesforce/label/c.RecentRecords";
import REQUIRED_FIELD from "@salesforce/label/c.RequiredField";
import SEARCH_RECORDS from "@salesforce/label/c.SearchRecords";
import NEW_RECORD from "@salesforce/label/c.NewRecord";

export default class LookupLwc extends NavigationMixin(LightningElement) {
  @api selectedValue = false;
  @api selectedRecordId;
  @api objectApiName;
  @api parentApiName;
  @api fieldApiName;
  @api iconName;
  @api extraFields = {fields:[]};
  @api isRequired;
  @api labelOverride;
  @api recordId;
  @api filters;
  showHelptext = false;
  showRequired = false;
  message;
  recordsList;
  searchKey = "";
  showRecordsList = false;
  isRecentRecords = false;

  labels = {
    lookupLabel: "",
    newRecordLabel: "",
    recentRecordsLabel: RECENT_RECORDS,
    required: REQUIRED_FIELD,
    helptext: ""
  };

  @wire(getObjectInfo, { objectApiName: "$parentApiName" })
  parentObjectInfo({ data }) {
    if (data) {
      if (data.fields) {
        if (data.fields[this.fieldApiName]) {
          this.labels.lookupLabel = this.labelOverride.length > 0 ? this.labelOverride : data.fields[this.fieldApiName].label;
          this.labels.helptext = data.fields[this.fieldApiName].inlineHelpText;
          this.labels = { ...this.labels };
          this.showHelptext = this.labels.helptext != null && this.labels.helptext.length > 0;
        }
      }
    }
  }

	@wire(getObjectInfo, { objectApiName: "$objectApiName" })
	sobjectInfo({ data, error }) {
		if (data) {
			this.formatObjectLabels(data.label, data.labelPlural);
		}
		if(error){
			console.log('error en sobjectInfo : ' + JSON.stringify(error));
			if(this.objectApiName == 'EmailTemplate'){
				this.formatObjectLabels('Email Template', 'Email Templates');
			}
			
		}
	}

  	formatObjectLabels(label, labelPlural){
		this.labels.newRecordLabel = formatLabelWithParameters(NEW_RECORD, [label]);
      	this.labels.recentRecordsLabel =formatLabelWithParameters(RECENT_RECORDS, [labelPlural]) ;
      	this.labels.search = formatLabelWithParameters(SEARCH_RECORDS, [labelPlural]);
      	this.labels = { ...this.labels };
	}

  get searchCssClass() {
    let css = "slds-input slds-combobox__input";
    if (this.showRequired) css += " slds-has-error revert-border-with";
    if (this.showRecordsList) css += " slds-has-focus";
    return css;
  }

  get hasHelptext() {
    return this.showHelptext;
  }
  connectedCallback() {
    this.getRecentRecords();
  }

	getRecentRecords() {
		if(this.objectApiName){
			if(this.objectApiName == 'EmailTemplate'){
				getRecentEmailTemplates( { recordId : this.recordId } )
				.then(result => {
					if (result.length === 0) {
						this.recordsList = [];
						this.message = "";
					} else {
						this.recordsList = result;
						this.message = this.labels.recentRecordsLabel;
						this.prepareRows();
						this.isRecentRecords = true;
					}
					this.error = undefined;
				})
				.catch(error => {
					console.log('error en getrecent templates : ' + JSON.stringify(error));
					this.error = error;
					this.recordsList = undefined;
				});
					
			} else {
				getRecentlyViewed({ objectName: this.objectApiName, extraFields: this.extraFields.fields })
				.then(result => {
					if (result.length === 0) {
					this.recordsList = [];
					this.message = "";
					} else {
					this.recordsList = result;
					this.message = this.labels.recentRecordsLabel;
					this.prepareRows();
					this.isRecentRecords = true;
					}
					this.error = undefined;
				})
				.catch(error => {
					console.log('error en getRecentlyViewed : ' + JSON.stringify(error));
					this.error = error;
					this.recordsList = undefined;
				});
			}
		}
	}

  onLeave(event) {
    if (this.selectedRecordId == null) {
      if (this.isRequired) {
        this.showRequired = true;
      }
    }
    this.searchKey = "";
    this.showRecordsList = false;
    this.getRecentRecords();
  }

  onFocus() {
    this.showRequired = false;
    this.showRecordsList = true;
  }

  @api
  setSelectedRecord(record){
    console.log('in setSelectedRecord : ');
    console.log('json reord : ' + JSON.stringify(record));
    this.selectedRecordId = record.id;
    this.selectedValue = record.name;
    this.searchKey = "";
    this.showRequired = false;
    this.showRecordsList = false;
    this.onSeletedRecordUpdate();
  }

  @api
  setFilters(filter){
      this.filters =filter;
  }

  handleRecordSelection(event) {
    this.selectedRecordId = event.currentTarget.getAttribute("data-id");
    this.selectedValue = event.currentTarget.getAttribute("data-name");
    this.searchKey = "";
    this.showRequired = false;
    this.showRecordsList = false;
    this.onSeletedRecordUpdate();
  }

  handleKeyChange(event) {
    const searchKey = event.target.value;
    this.searchKey = searchKey;
    this.getLookupResult();
  }

  removeRecordOnLookup(event) {
    this.searchKey = "";
    this.selectedValue = null;
    this.selectedRecordId = null;
    this.showRecordsList = true;
    this.getRecentRecords();
    this.onSeletedRecordUpdate();
  }

  @api 
  doGetLookupResult(){
    this.getLookupResult();
  }

  @api
  removeRecord(event) {
    this.searchKey = "";
    this.selectedValue = null;
    this.selectedRecordId = null;
    this.showRequired = false;
    this.getRecentRecords();
    this.onSeletedRecordUpdate();
  }

  @api
  forceShowRequired() {
    this.showRequired = true;
  }

  getLookupResult() {
    findRecords({ searchKey: this.searchKey, objectName: this.objectApiName, extraFields: this.extraFields.fields, filter: this.filters })
      .then(result => {
        if (result.length === 0) {
          this.recordsList = [];
          this.message = NO_RECORDS_FOUND;
        } else {
          this.recordsList = result;
          this.message = "";
          this.prepareRows();
        }
        this.error = undefined;
      })
      .catch(error => {
		console.log('error en findRecords : ' + JSON.stringify(error));
        this.error = error;
        this.recordsList = undefined;
      });
  }

  onSeletedRecordUpdate() {
    console.log('in onSeletedRecordUpdate : ');
    console.log('selectedRecordId : ' + this.selectedRecordId);
    console.log('selectedValue : ' + this.selectedValue);
  
    const passEventr = new CustomEvent("recordselection", {
      detail: { selectedRecordId: this.selectedRecordId, selectedValue: this.selectedValue }
    });
    this.dispatchEvent(passEventr);
  }

  prepareRows() {
    let newList = [];
    this.recordsList.forEach(record => {
      let newRecord = { ...record };
      if (this.extraFields != null && this.extraFields.fields.length > 0) {
        var values = [];
        this.extraFields.fields.forEach(field => {
			if(record[field] != null && record[field] != undefined && record[field] != ''){
				values.push(record[field]);
			}
        });
        newRecord.Description = values.length > 0 ? formatLabelWithParameters(this.extraFields.mergeTemplate, values) : "";
      }
      newList.push(newRecord);
    });
    this.recordsList = newList;
  }

  createRecord(event) {
    let navAttr = {
      type: "standard__objectPage",
      attributes: {
        objectApiName: this.objectApiName,
        actionName: "new"
      },
      state: {
        navigationLocation: "RELATED_LIST"
      }
    };
    this[NavigationMixin.Navigate](navAttr);
    // TO DO: Check modal close and take action, possibly fetching for a just created record and using it.
  }
}