import { LightningElement,api} from 'lwc';
import getQueues from '@salesforce/apex/QueueManagementController.getQueues';
import saveQueuesChanges from '@salesforce/apex/QueueManagementController.saveQueuesChanges';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import hasCustomPermission from '@salesforce/customPermission/Manage_Queues';

export default class UsersQueuesManagement extends LightningElement {

    @api recordId;
    get hasManageQueuePermission() {
        return hasCustomPermission;
    }

    initialQueues = [];
    displaySpinner = true;
    error;
    options = [];
    values = [];
    queueNotUpdated = true;

    get allowEdit() {
        return !this.queueNotUpdated;
    }

    loadQueues() {
        getQueues({recordId: this.recordId})
        .then((result) => {
            this.options=result.availableQueues;
            this.values=result.selectedQueues;
            this.initialQueues=result.selectedQueues;
            this.queueNotUpdated = true;
            this.displaySpinner = false;
        })
        .catch((error) => {
            console.log(error);
            this.displaySpinner = false;
        });
    }

    connectedCallback() {
        this.loadQueues();
    }

    handleChange(event) {
        const selectedOptions = event.detail.value;
        var is_same = (selectedOptions.length == this.initialQueues.length) && this.initialQueues.every(function(element, index) {
            return selectedOptions.includes(element);
        });
        this.values = selectedOptions;
        this.queueNotUpdated = is_same;
    }
    saveQueue(event) {
        this.displaySpinner = true;
        saveQueuesChanges({recordId: this.recordId, selectedQueues: this.values, initialQueues: this.initialQueues})
        .then((result) => {
            var status = result.status;
            var msgRes = result.message;
            this.displaySpinner = false;
            this.queueNotUpdated = true;
            this.initialQueues=this.values;
            const evt = new ShowToastEvent({
                title: status,
                message: msgRes,
                variant: status.toLowerCase(),
            });
            this.dispatchEvent(evt);
        })
        .catch((error) => {
            this.displaySpinner = false;
            const evt = new ShowToastEvent({
                title: 'Error while saving!',
                message: 'Something went wrong. Please contact your System Administrator.',
                variant: 'error',
            });
            this.dispatchEvent(evt);
        })

    }
}