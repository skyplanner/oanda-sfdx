import { LightningElement, api, wire, track} from 'lwc';
import { getRecord } from "lightning/uiRecordApi";

const EID_FIELDS = ['EID_Result__c.Address_Affiliation__c', 
                    'EID_Result__c.Address_Affiliation_Reasons__c',
                    'EID_Result__c.Address_Insights__c',
                    'EID_Result__c.Address_Insights_Reason__c',
                    'EID_Result__c.Address_Verification__c',
                    'EID_Result__c.Address_Verification_Reasons__c',
                    'EID_Result__c.Address_Trust__c',                                
                    'EID_Result__c.Device_Association__c',
                    'EID_Result__c.Device_Association_Reason__c',
                    'EID_Result__c.Device_Reputation__c',
                    'EID_Result__c.Device_Reputation_Reason__c',
                    'EID_Result__c.Device_Trust__c',
                    'EID_Result__c.DOB_Affiliation__c',
                    'EID_Result__c.DOB_Affiliation_Reason__c',
                    'EID_Result__c.DOB_Insights__c',
                    'EID_Result__c.DOB_Insights_Reason__c',
                    'EID_Result__c.DOB_Verification__c',
                    'EID_Result__c.DOB_Verification_Reason__c',
                    'EID_Result__c.DOB_Trust__c',
                    'EID_Result__c.Email_Affiliation__c', 
                    'EID_Result__c.Email_Affiliation_Reason__c',
                    'EID_Result__c.Email_Insights__c',
                    'EID_Result__c.Email_Insights_Reason__c', 
                    'EID_Result__c.Email_Verification__c',
                    'EID_Result__c.Email_Verification_Reason__c',
                    'EID_Result__c.Email_Trust__c',
                    'EID_Result__c.Identity_Verification__c',
                    'EID_Result__c.Identity_Verification_Reason__c',
                    'EID_Result__c.Identity_Trust__c',
                    'EID_Result__c.Identity_Risk__c',
                    'EID_Result__c.Identity_Risk_Reason__c',
                    'EID_Result__c.Identity_Resolution__c',
                    'EID_Result__c.Identity_Resolution_Reason__c',
                    'EID_Result__c.Phone_Affiliation__c',
                    'EID_Result__c.Phone_Affiliation_Reason__c',
                    'EID_Result__c.Phone_Insights__c',
                    'EID_Result__c.Phone_Insights_Reason__c',
                    'EID_Result__c.Phone_Verification__c',
                    'EID_Result__c.Phone_Verification_Reason__c',
                    'EID_Result__c.Phone_Trust__c',
                    'EID_Result__c.SSN_Affiliation__c',
                    'EID_Result__c.SSN_Affiliation_Reason__c',
                    'EID_Result__c.SSN_Insights__c',
                    'EID_Result__c.SSN_Insights_Reason__c',
                    'EID_Result__c.SSN_Verification__c',
                    'EID_Result__c.SSN_Verification_Reason__c',
                    'EID_Result__c.SSN_Trust__c']



export default class EquifaxDITresults extends LightningElement {
    @api recordId;
    showEquifaxDITinfo;
    @track formattedData ={};

    @wire(getRecord, { recordId: "$recordId", fields: EID_FIELDS })
	getEIDRecord({ data, error }) {
		if (data) {
            for (const [key, valueObj] of Object.entries(data.fields)) {
                this.formattedData[key] = valueObj.value;
            }
		} else if (error) {
            //keep it temporary to track whihch FLS is missing
            console.log('Error: ' + JSON.stringify(error));
			this.error = error;
		}
	}
}