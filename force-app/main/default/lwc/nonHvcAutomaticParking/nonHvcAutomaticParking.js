/**
 * @File Name          : NonHvcAutomaticParking.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/31/2022, 12:46:14 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/30/2022, 11:55:00 PM   acantero     Initial Version
**/
import { LightningElement, api, wire } from 'lwc';
import { getRecord, updateRecord  } from 'lightning/uiRecordApi';

import Id from '@salesforce/user/Id';

import ID_FIELD from '@salesforce/schema/Non_HVC_Case__c.Id';
import OWNERID_FIELD from '@salesforce/schema/Non_HVC_Case__c.OwnerId';
import STATUS_FIELD from '@salesforce/schema/Non_HVC_Case__c.Status__c';

const FIELDS = [
    ID_FIELD,
    OWNERID_FIELD,
    STATUS_FIELD
];

export default class NonHvcAutomaticParking extends LightningElement {

    @api recordId;
    @api enabled;

    userId = Id;

    connectedCallback() {
        console.debug('NonHvcAutomaticParking 1.2');
    }

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredAccount({ error, data }) {
        if (!this.enabled) {
            return;
        }
        //else...
        if (data) {
            const recOwnerId = data.fields.OwnerId.value;
            const recStatus = data.fields.Status__c.value;
            console.log(
                'NonHvcAutomaticParking -> wiredAccount' +
                'recStatus: ' + recStatus +
                ', recOwnerId: ' + recOwnerId + 
                ', userId: ' + this.userId
            );
            if (
                (recStatus == 'Default') &&
                (recOwnerId == this.userId)
            ) {
                console.log('Non-HVC Case: should be parked');
                this.parkNonHvcCase();
                //...
            } else {
                console.log('Non-HVC Case: no need to park');
            }
            //...
        } else if (error) {
            console.error('NonHvcAutomaticParking -> fails, error: ' + error);
        }
    }

    parkNonHvcCase() {
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[STATUS_FIELD.fieldApiName] = 'Parked';
        const recordInput = {
            fields: fields
        };
        updateRecord(recordInput)
            .then((record) => {
                console.log('NonHvcAutomaticParking -> parkNonHvcCase: ok');
            })
            .catch(error => {
                console.error('NonHvcAutomaticParking -> parkNonHvcCase: fails');
                const validError = (error.body && error.body.message)
                    ? error.body.message
                    : error;
                console.error('NonHvcAutomaticParking -> error: ' + validError);
            });
    }

}