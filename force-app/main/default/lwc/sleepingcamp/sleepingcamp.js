import { LightningElement, api } from 'lwc';

export default class Sleepingcamp extends LightningElement {
    @api mainmessage;
    @api submessage;
}