import { LightningElement, api } from 'lwc';
import getAffiliateUsername from '@salesforce/apex/DisplayAffiliateUsernameController.getAffiliateUsername';

export default class DisplayAffiliateUsername extends LightningElement {

    @api recordId;
    affiliateUsername;
    containsOlabs=false;
    showComponent=false;

    connectedCallback() {
         getAffiliateUsername({recordId: this.recordId}).then( result =>{
             if(result==null) return;
             if(result!=null && result.toLowerCase().includes('olabs')) this.containsOlabs=true;
             this.affiliateUsername = result;
             this.showComponent = true;
                    }).catch(error => {
                        console.log('error ===> ', error);
                    });

    }
}