/**
 * @Description  : LWC component displays Affiliate__c objects with the same value of Email__c field.
 * @Author       : Jakub Fik
 * @Date         : 2024-04-18
 **/
import { LightningElement, api, wire, track } from 'lwc';
import getRelatedPartnerProfiles from '@salesforce/apex/RelatedPartnerProfilesController.getRelatedPartnerProfiles';

const columns = [
    {
        label: 'Partner Name',
        fieldName: 'Redirect',
        type: 'url',
        typeAttributes: {
            label: { fieldName: 'Username__c' },
            target: '_blank'
        }
    },
    { label: 'Division Name', fieldName: 'Division_Name__c' }
];

export default class RelatedPartnerProfiles extends LightningElement {
    @api recordId;
    @track profiles;
    columns = columns;

    @wire(getRelatedPartnerProfiles, { profileId: '$recordId' })
    wiredProfiles({ data }) {
        if (data && data.length) {
            let records = [];
            data.forEach((record) => {
                let updatedRecord = { ...record };
                updatedRecord.Redirect = '/' + record.Id;
                records.push(updatedRecord);
            });
            this.profiles = records;
        }
    }
}