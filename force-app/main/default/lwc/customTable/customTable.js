/**
 * @description       :
 * @author            : Yaneivys Gutierrez
 * @group             :
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
 **/
import { LightningElement, api } from "lwc";

export default class CustomTable extends LightningElement {
	@api columns;
	@api tableData;
	@api classTable;

	connectedCallback() {
		console.log(JSON.stringify(this.columns));
		console.log(JSON.stringify(this.tableData));
	}

	handleAccordionClick(event) {
		var divblock = this.template.querySelector(
			'[data-id="' + event.currentTarget.dataset.id + '"]'
		);
		if (divblock && divblock.classList.contains('accordion')) {
			divblock.classList.toggle("active");

			var panel = divblock.nextElementSibling;
			if (panel.style.display === "table-row") {
				panel.style.display = "none";
			} else {
				panel.style.display = "table-row";
			}
		}
	}

    handlerUrlOnClick(event) {
        event.stopPropagation();
        return false;
    }
}