/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-20-2022
 * @last modified by  : Ariel Niubo
 **/
import { LightningElement, api, track } from "lwc";
import { normalizeBoolean } from "c/utils";
import getPicklistInfo from "@salesforce/apex/ComboPicklistController.getPicklistInfo";

export default class ComboPicklist extends LightningElement {
	@track _required = false;
	@track _disabled = false;
	@track _readOnly = false;
	@track selectedValue;
	_objectName;
	_field;
	@track options;
	error = false;
	errorMessage;

	@api label;
	@api name;
	@api placeholder;
	@api
	get required() {
		return this._required;
	}
	set required(value) {
		this._required = normalizeBoolean(value);
	}
	@api
	get disabled() {
		return this._disabled || this._readOnly || false;
	}
	set disabled(value) {
		this._disabled = normalizeBoolean(value);
	}
	@api
	get readOnly() {
		return this.disabled;
	}
	set readOnly(value) {
		this._readOnly = normalizeBoolean(value);
	}
	@api
	get value() {
		return this.selectedValue;
	}
	set value(newValue) {
		if (newValue !== this.selectedValue) {
			this.selectedValue = newValue;
		}
	}
	@api
	get objectName() {
		return this._objectName;
	}
	set objectName(value) {
		if (value !== this._objectName) {
			this._objectName = value;
			this.loadPicklistInfo();
		}
	}
	@api
	get field() {
		return this._field;
	}
	set field(value) {
		if (value !== this._field) {
			this._field = value;
			this.loadPicklistInfo();
		}
	}
	handleChange(event) {
		this.selectedValue = event.detail.value;
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: {
					value: this.selectedValue
				}
			})
		);
	}
	handleFocus() {
		this.dispatchEvent(
			new CustomEvent("focus", { bubbles: false, composed: false })
		);
	}

	loadPicklistInfo() {
		if (this.objectName && this.field) {
			console.log(this.objectName);
			getPicklistInfo({
				objName: this.objectName,
				picklistField: this.field
			})
				.then((result) => {
					this.options = result;
				})
				.catch((error) => {
					this.error = true;
					this._readOnly = true;
					this.errorMessage = error.body.message;
				});
		}
	}
}