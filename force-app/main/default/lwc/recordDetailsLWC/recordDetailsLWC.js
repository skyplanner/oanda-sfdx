import { LightningElement, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchRelatedRecord from '@salesforce/apex/RecordDetailsLWCCtrl.fetchRelatedRecord';

export default class RecordDetailsLWC extends LightningElement {

    @api recordId;
    @api lookup;
    @api fields;
    @api title;
    @api iconName;
    relatedRecordId;
    relatedRecordName;
    loaded;
    
    connectedCallback() {
        this.fields =
            this.fields ? 
                this.fields.split(',') :
                [];
        console.log('RD-recordId:', this.recordId);
        console.log('RD-fields:', this.fields);
        console.log('RD-lookup:', this.lookup);
        
        this.loadRelatedRecord();
    }
    loadRelatedRecord() {
        fetchRelatedRecord({ 
            recordId: this.recordId,
            lookup: this.lookup
        }).then(
            r => {
                if(r) {
                    this.relatedRecordId = r.id;
                    this.relatedRecordName = r.objName;
                    this.loaded = true;
                }
                console.log('RD-related:', r);
        })
        .catch(error => {
            this.onError(error);
        });
    }
    onError(err) {
        console.log('RD-Error:', err);
        this.showError("Error!",
            err.body.message);
    }
     showError (aTitle) {
        showError (aTitle, '');
    }
    showError (aTitle, aMessage) {
        const evt = new ShowToastEvent({
            title: aTitle,
            message: aMessage,
            variant: "Error"
        });
        this.dispatchEvent(evt);        
    }
}