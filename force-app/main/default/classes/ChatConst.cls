/**
 * @File Name          : ChatConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/9/2021, 4:12:25 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/27/2021, 9:48:45 AM   acantero     Initial Version
**/
public without sharing class ChatConst {

	public static final String IS_REDIRECT = 'isRedirect';
	public static final String FROM_LANGUAGE = 'fromLanguage';
    public static final String LANGUAGE_PREFERENCE_OFFLINE = 'LanguagePreferenceOffline';
    public static final String LANGUAGE_PREFERENCE = 'LanguagePreference';
    public static final String CUSTOMER_LAST_NAME = 'CustomerLastName';
	public static final String CUSTOMER_EMAIL = 'CustomerEmail';
	public static final String ACCOUNT_TYPE = 'AccountType';

	public static final String POSTCHAT_REDIRECT = 'postchatRedirect';

	public static final String DEFAULT_LANGUAGE = 'en_US';

	public static final String CHAT_LANGUAGE_COOKIE = 'chat_language';

}