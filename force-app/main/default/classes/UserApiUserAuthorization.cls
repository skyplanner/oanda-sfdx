/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 09-01-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserAuthorization {
    public Integer can_authorize_tpa { get; set; }
    
    public Integer can_change_leverage { get; set; }
    
    public Integer can_close_trades { get; set; }
    
    public Integer can_create_funding_source { get; set; }
    
    public Integer can_create_subaccount { get; set; }
    
    public Integer can_deposit { get; set; }
    
    public Integer can_launch_gui { get; set; }
    
    public Integer can_read_trades { get; set; }
    
    public Integer can_read_transfers { get; set; }
    
    public Integer can_trade { get; set; }
    
    public Integer can_transfer { get; set; }
    
    public Integer can_update_profile { get; set; }
    
    public Integer can_withdraw { get; set; }
    
    public String update_timestamp { get; set; }

    public transient Boolean canAuthorizeTpa {
        get {
            return can_authorize_tpa == 1;
        }
    }

    public transient Boolean canDeposit {
        get {
            return can_deposit == 1;
        }
    }

    public transient Boolean canTrade {
        get {
            return can_trade == 1;
        }
    }
}