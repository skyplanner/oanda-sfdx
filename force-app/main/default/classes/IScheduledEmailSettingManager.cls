/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 07-19-2022
 * @last modified by  : Ariel Niubo
**/
public interface IScheduledEmailSettingManager {
    IScheduledEmailSetting getSetting();
}