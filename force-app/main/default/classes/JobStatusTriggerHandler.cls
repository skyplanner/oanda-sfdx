/**
 * @File Name          : JobStatusTriggerHandler.cls
 * @Description        :
 * @Author             : acantero
 * @Group              :
 * @Last Modified By   : Ariel Niubo
 * @Last Modified On   : 07-28-2022
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/25/2021, 12:54:30 AM   acantero     Initial Version
 **/
public without sharing class JobStatusTriggerHandler {
	//********************************************************

	public static final String OBJ_API_NAME = 'Job_Status__c';

	public static Boolean enabled { get; set; }

	public static Boolean isEnabled() {
		return enabled != null ? enabled : true;
		// check los mecanismos de oanda para desactivar los triggers
	}

	//********************************************************

	final List<Job_Status__c> newList;
	final Map<ID, Job_Status__c> oldMap;

	public JobStatusTriggerHandler(
		List<Job_Status__c> newList,
		Map<ID, Job_Status__c> oldMap
	) {
		this.newList = newList;
		this.oldMap = oldMap;
	}

	//********************************************************

	public void handleAfterUpdate() {
		checkJobActivation();
	}

	void checkJobActivation() {
		for (Job_Status__c jobStatus : newList) {
			Job_Status__c oldRecord = oldMap.get(jobStatus.Id);
			if (
				(jobStatus.Name == SendScheduledEmailJob.SEND_EMAIL_JOB) &&
				(jobStatus.Status__c == JobStatusHelper.ACTIVE_STATUS) &&
				(jobStatus.Status__c != oldRecord.Status__c)
			) {
				// System.enqueueJob(SendScheduledEmailJob.getNewInstance());
				
				return;
			}
		}
	}
}