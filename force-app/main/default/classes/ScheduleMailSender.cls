/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 08-02-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public without sharing class ScheduleMailSender {

	private String EMAIL_MESSAGE_STATUS_SENT = '3';

	public Integer processScheduledEmails(
		Datetime processingDateTime,
		Integer max
	) {
		//ScheduleDateTime schedule = getScheduleDateTime(processingDateTime);
		List<Scheduled_Email__c> scheduledEmailList = getScheduledEmails(
			processingDateTime,
			max
		);
		if (scheduledEmailList.isEmpty()) {
			return 0;
		}
		Integer result = scheduledEmailList.size();
		sendEmails(scheduledEmailList);
		 
		return result;
	}

	public void processScheduledEmails(List<Scheduled_Email__c> scheduledEmailList) {
		sendEmails(scheduledEmailList); 
	}
 
	private static void sendClientErrorNotification(List<String> failedIds) {
		List<Scheduled_Email__c> sEmailList = ScheduledEmailRepo.getAll(failedIds);
		if(sEmailList.isEmpty())
			return;
		 

		List<Id> targetObjectIds = new List<Id>();
		List<Id> setWhatIds = new List<Id>();

		Schedule_Mail_Setting__mdt settings = ScheduleMailSettingRepo.getFirst();

		EmailTemplate template =  [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = :settings.Notification_Template_Failed_Message__c LIMIT 1];

		List<Messaging.SingleEmailMessage > messagesToSend = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage sEmailTemplate;
		for (Scheduled_Email__c se : sEmailList) {
 
			sEmailTemplate = Messaging.renderStoredEmailTemplate(template.Id, se.Case__r.OwnerId, se.Case__c);
			sEmailTemplate.setSaveAsActivity(false);

			messagesToSend.add(sEmailTemplate);
		}
  
		System.debug(Messaging.sendEmail(messagesToSend));
	}
 
	private List<Messaging.SingleEmailMessage> buildSingleEmailMessage(List<Id> emailDraftList) {

		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		Schedule_Mail_Setting__mdt settings = ScheduleMailSettingRepo.getFirst();

		List<EmailMessage> messageList = [SELECT Id, FromAddress, FromName, HtmlBody, ParentId, ToAddress, Subject FROM EmailMessage WHERE Id IN :emailDraftList];

		OrgWideEmailAddress[] owea = [select Id, Address from OrgWideEmailAddress where DisplayName = :settings.Org_Address_Sender_Name__c];
		if(owea.size() == 0) {
			throw new ScheduleMailSenderException('There is not a OrgWideEmailAddress');
		}

		Messaging.SingleEmailMessage temporal;
		for (EmailMessage em : messageList) {

			if(String.isEmpty(em.ToAddress))
				continue;

			temporal = new Messaging.SingleEmailMessage();
  
			temporal.setOrgWideEmailAddressId(owea[0].Id);

			temporal.setSubject(em.Subject);
			temporal.setToAddresses(em.ToAddress.split(';')); 
			temporal.setHtmlBody(em.HtmlBody);
			temporal.setWhatId(em.ParentId);
			// temporal.setSaveAsActivity(true);

			emailList.add(temporal);

			// update status of emailmessage
			em.Status = EMAIL_MESSAGE_STATUS_SENT;
		}

		update messageList;

		return emailList;
	}

	private void sendEmails(List<Scheduled_Email__c> scheduledEmailList) {
		List<Id> emailDraftList = new List<Id>();
		for (Scheduled_Email__c scheduledEmail : scheduledEmailList) {
			if (scheduledEmail.Email_Message__c != null) {
				emailDraftList.add(scheduledEmail.Email_Message__c);
			}
		}
		Messaging.reserveSingleEmailCapacity(emailDraftList.size());
		Messaging.SendEmailResult[] result = Messaging.sendEmail(buildSingleEmailMessage(emailDraftList), false);
 
		// send exception with errors
		List<Messaging.SendEmailError> errorList = new List<Messaging.SendEmailError>();
		List<String> failedIds = new List<String>();
		
		Messaging.SendEmailResult r;
		for (Integer i = 0; i < result.size(); i++) {
			r = result[i];

			if(r.isSuccess())
				continue;

			errorList.addAll(r.getErrors());
			failedIds.add(scheduledEmailList[i].Id);
		}
		 
 
		if(!errorList.isEmpty()) {
			InternalErrorManager.safeLogException('ScheduleMailSender', new ScheduleMailSenderException(JSON.serialize(errorList))); 

			// send notification of scheduled email not sent 
			sendClientErrorNotification(failedIds);

		}  
	}

	@testVisible
	private ScheduleDateTime getScheduleDateTime(Datetime processingDateTime) {
		Date scheduledDate = processingDateTime.date();
		Integer hour = processingDateTime.hour();
		String amOrPm = hour > 11 ? 'PM' : 'AM';
		String scheduledTime = String.valueOf(convertTo12(hour));
		return new ScheduleDateTime(scheduledDate, scheduledTime, amOrPm);
	}
	@testVisible
	private List<Scheduled_Email__c> getScheduledEmails(
		Datetime scheduledDateTime,
		Integer max
	) {
		return ScheduledEmailRepo.getByDateTime(scheduledDateTime, max);
	}
	private Integer convertTo12(Integer hour) {
		if (hour == 0) {
			return 12;
		}
		return hour > 12 ? hour - 12 : hour;
	}

	public class ScheduleDateTime {
		public Date scheduledDate { get; set; }
		public String scheduledTime { get; set; }
		public String amOrPm { get; set; }
		public ScheduleDateTime(
			Date scheduledDate,
			String scheduledTime,
			String amOrPm
		) {
			this.scheduledDate = scheduledDate;
			this.scheduledTime = scheduledTime;
			this.amOrPm = amOrPm;
		}
	}

	public class ScheduleMailSenderException extends Exception {}
}