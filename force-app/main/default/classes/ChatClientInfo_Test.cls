/**
 * @File Name          : ChatClientInfo_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 10:11:51 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 10:07:05 AM   acantero     Initial Version
**/
@isTest
private without sharing class ChatClientInfo_Test {

    @isTest
    static void test() {
        Test.startTest();
        ChatClientInfo result1 = ChatClientInfo.getAccountInfo(
            true, //isHVC
            OmnichanelConst.OANDA_CANADA, //division
            'fake fxAccount Id' //fxAccount
        );
        ChatClientInfo result2 = ChatClientInfo.getLeadInfo(
            null, //fxAccount
            null //lead record type id
        );
        Test.stopTest();
        System.assertEquals(true, result1.isAnAccount);
        System.assertEquals(false, result2.isAnAccount);
    }
    
}