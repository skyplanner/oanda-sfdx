/**
 * @File Name          : SObjectFieldsUpdateManager.cls
 * @Description        : Processes updates for multiple fields of a record
 *                       of a given object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/26/2023, 3:36:54 PM
**/
public inherited sharing abstract class SObjectFieldsUpdateManager {

	@TestVisible
	Map<String, SObject> recordsById;

	public SObjectFieldsUpdateManager() {
		this.recordsById = new Map<String, SObject>();
	}

	public abstract SObject getRecordToUpdate();

	public void processUpdate(List<FieldValueSource> sources) {
		try {
			ExceptionTestUtil.execute();
			processUpdateInfo(sources);

		} catch (LogException lex) {
			throw lex;
			
		} catch (Exception ex) {
			throw LogException.newInstance(
				SObjectFieldsUpdateManager.class.getName(),
				ex
			);
		}
	}

	@TestVisible
	void processUpdateInfo(List<FieldValueSource> sources) {    
		if (
			(sources == null) ||
			sources.isEmpty()
		) {
			return;
		}
		// else... 
		for (FieldValueSource source : sources) {
			setFieldValues(source);
		}
		updateRecords();
	} 

	@TestVisible
	void setFieldValues(FieldValueSource source) {
		SObject record = getRecordToUpdate();
		record.Id = source.getRecordId();
		Map<String, Object> valuesByFieldName = 
			getFieldValueByFieldNames(source);

		for (String fieldName : valuesByFieldName.keySet()) {
			record.put(fieldName, valuesByFieldName.get(fieldName));
		}

		recordsById.put(record.Id, record);
	}

	@TestVisible
	void updateRecords() {
		if (recordsById.isEmpty() == false) {
			update recordsById.values();
		}
	}

	protected virtual Map<String, Object> getFieldValueByFieldNames(
		FieldValueSource source
	) {
		return source.getFieldValueByFieldNames();
	}

}