/**
 * @File Name          : MilestoneUtils_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/6/2024, 4:40:22 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/15/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class MilestoneUtils_Test {

    @TestSetup
    static void setup() {
        SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
        initManager.setFieldValues( ServiceTestDataKeys.CASE_1,
        new Map<String,Object>{
            'Tier__c' => 'Tier 1',
            'Ready_For_Routing__c' => true,
             'Origin' => 'Chat'
        });
        ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
    }
    
    @isTest
    static void getMilestoneSettings_test() {
        String milestoneTypeName = 'fake_test_Milestone_Name';
        List<Milestone_Time_Settings__mdt> milSettList = [
            select Id, Milestone_Type_Name__c from Milestone_Time_Settings__mdt
        ];
        Boolean existsMilestoneSettings = !milSettList.isEmpty();
        Boolean fakeNameError = false;
        Milestone_Time_Settings__mdt milestoneSettings = null;
        Test.startTest();
        try {
            milestoneSettings = 
                MilestoneUtils.getMilestoneSettings(milestoneTypeName, true);
            //...
        } catch(Exception ex) {
            fakeNameError = true;
        }
        if (existsMilestoneSettings) {
            milestoneTypeName = milSettList[0].Milestone_Type_Name__c;
        }
        milestoneSettings = MilestoneUtils.getMilestoneSettings(milestoneTypeName, false);
        Test.stopTest();
        System.assertEquals(true, fakeNameError);
        System.assertEquals(existsMilestoneSettings, (milestoneSettings <> null));
    }
    @isTest
    private static void testCompleteMilestones() {
        // Test data setup
        SPDataInitManager initManager = SPDataInitManager.reload();
        Id caseId =  initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
        Case toClose =  new Case(
            Id = caseId,
            Status = 'Closed', 
            Inquiry_Nature__c ='Account',
            Live_or_Practice__c= 'Live',
            Type__c ='Individual'
        );
        // Actual test
        Test.startTest();
        update toClose;
        Test.stopTest();
    
        System.assertEquals('Closed', toClose.Status);
        // Asserts
    }

}