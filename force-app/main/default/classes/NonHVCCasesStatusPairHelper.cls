/**
 * @File Name          : NonHVCCasesStatusPairHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 3:21:08 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/23/2021, 5:05:02 PM   acantero     Initial Version
**/
public inherited sharing class NonHVCCasesStatusPairHelper {

    public static Non_HVC_Cases_Status_Pair__mdt getByStatusAllowing(
        String statusAllowing
    ) {
        if (String.isBlank(statusAllowing)) {
            return null;
        }
        //else...
        Non_HVC_Cases_Status_Pair__mdt result = null;
        List<Non_HVC_Cases_Status_Pair__mdt> nonHVCCasesStatusPairList = [
            SELECT
                Status_not_allow__c,
                Max_Number_Cases__c
            FROM 
                Non_HVC_Cases_Status_Pair__mdt
            WHERE 
                Status_allowing__c = :statusAllowing
            LIMIT 1
        ];
        if (!nonHVCCasesStatusPairList.isEmpty()) {
            result = nonHVCCasesStatusPairList[0];
        }
        return result;
    }

    public static Non_HVC_Cases_Status_Pair__mdt getByStatusNotAllowing(
        String statusNotAllowing
    ) {
        if (String.isBlank(statusNotAllowing)) {
            return null;
        }
        //else...
        Non_HVC_Cases_Status_Pair__mdt result = null;
        List<Non_HVC_Cases_Status_Pair__mdt> nonHVCCasesStatusPairList = [
            SELECT
                Status_allowing__c,
                Max_Number_Cases__c,
                Min_Occupancy_Percentage__c
            FROM 
                Non_HVC_Cases_Status_Pair__mdt
            WHERE 
                Status_not_allow__c = :statusNotAllowing
            AND 
                Bidirectional__c = true
            LIMIT 1
        ];
        if (!nonHVCCasesStatusPairList.isEmpty()) {
            result = nonHVCCasesStatusPairList[0];
        }
        return result;
    }
    
}