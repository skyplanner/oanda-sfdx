public with sharing class BatchSetPrimaryFxAccount implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection  {
    
    private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    private String o;
    private Map<Id, List<String>> conversionErrors;
    
    public static final Integer BATCH_SIZE = 50;
    
    public BatchSetPrimaryFxAccount(String obj) {
        o = obj;
        if(o=='Lead') {
            query = 'SELECT Id FROM Lead WHERE IsConverted = false AND fxAccount__c=null AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN (\'Retail Lead House Account\', \'Retail Practice Lead\'))';
        } else if(o=='Account') {
            query = 'SELECT Id FROM Account WHERE fxAccount__c=null';
        }
    }
    
    public BatchSetPrimaryFxAccount(String obj, boolean doRefresh) {
        o = obj;
		if(doRefresh==false) {
			this(obj);
		} else {
	        if(o=='Lead') {
	            query = 'SELECT Id FROM Lead WHERE IsConverted = false AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN (\'Retail Lead House Account\', \'Retail Practice Lead\'))';
	        } else if(o=='Account') {
	            query = 'SELECT Id FROM Account';
	        }
		}
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public BatchSetPrimaryFxAccount(String obj, String divisionName) {
    	/*Set<Id> leadIds = new Set<Id>();
    	Set<Id> acctIds = new Set<Id>();
    	for(fxAccount__c fxa : [SELECT Id, Lead__c, Account__c FROM fxAccount__c WHERE Division_Name__c = :divisionName]) {
    		if(fxa.Lead__c!=null) {
    			leadIds.add(fxa.Lead__c);
    		}
    		if(fxa.Account__c!=null) {
    			acctIds.add(fxa.Account__c);
    		}
    	}
    	*/
        o = obj;
        if(o=='Lead') {
            query = 'SELECT Id FROM Lead WHERE IsConverted = false AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN (\'Retail Lead House Account\', \'Retail Practice Lead\')) AND Division_Name__c=\'' + divisionName + '\'';
        } else if(o=='Account') {
            query = 'SELECT Id FROM Account WHERE Division_Name__c=\'' + divisionName + '\'';
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
		System.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }
    
    private void execute(List<Lead> leads) {
        new fxAccountRollup().doRollupFromLeads(leads);
    }
    
    private void execute(List<Account> accounts) {
        new fxAccountRollup().doRollupFromAccounts(accounts);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> batch) {
        if(o=='Lead') {
            this.execute((List<Lead>)batch);
        } else if(o=='Account') {
            this.execute((List<Account>)batch);
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        EmailUtil.sendEmailForBatchJob(bc.getJobId());
        this.sendErrorEmail(bc);        
    }
    
    private void sendErrorEmail(Database.BatchableContext bc) {
        if (this.conversionErrors != null && this.conversionErrors.size() > 0) {
            
            String subject = this.conversionErrors.size() + ' Conversion Error(s)';
            
            EmailUtil.sendEmailForBatchJob(bc.getJobId(), subject, getErrorMsg());
        }
    }
    
    private String getErrorMsg() {
        String msg = '';
            
        for (Id leadId : this.conversionErrors.keySet()) {
            List<String> errors = this.conversionErrors.get(leadId);
            
            if (errors.size() == 0) {
                continue;
            }
            
            msg += leadId + ': \n';
            
            for (String error : errors) {
                msg += error + '\n';
            }
            
            msg += '\n\n';
        }
        
        return msg;
    }
    
    public static Id executeBatchLead() {
        return executeBatch('Lead', BATCH_SIZE, false);
    }
    
    public static Id executeBatchAccount() {
        return executeBatch('Account', BATCH_SIZE, false);
    }
    
    public static Id executeBatchLeadRefresh() {
        return executeBatch('Lead', BATCH_SIZE, true);
    }
    
    public static Id executeBatchAccountRefresh() {
        return executeBatch('Account', BATCH_SIZE, true);
    }
    
    public static Id executeBatchAccountRefreshOau(String obj) {
    	return Database.executeBatch(new BatchSetPrimaryFxAccount(obj, 'OANDA Australia'), BATCH_SIZE);
    }
    
    public static Id executeBatch(String obj, Integer batchSize, boolean doRefresh) {
        return Database.executeBatch(new BatchSetPrimaryFxAccount(obj, doRefresh), batchSize);
    }
    
    // BatchSetPrimaryFxAccount.oneLead(id);
    public static void oneLead(String id) {
        Lead l = new Lead(Id = id);
        new fxAccountRollup().doRollupFromLeads(l);
    }
    
    // BatchSetPrimaryFxAccount.oneAccount(id);
    public static void oneAccount(String id) {
        Account acc = new Account(Id = id);
        new fxAccountRollup().doRollupFromAccounts(acc);
    }
}