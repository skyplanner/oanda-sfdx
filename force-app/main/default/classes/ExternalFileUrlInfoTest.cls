/**
 * @File Name          : ExternalFileUrlInfoTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 4:27:25 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 4:27:25 PM   aniubo     Initial Version
 **/
@isTest
private class ExternalFileUrlInfoTest {
	@isTest
	private static void testExternalFileUrlInfoConstructor() {
		// Test data setup

		// Actual test
		Test.startTest();
		External_File_URL__mdt mdt = new External_File_URL__mdt(
			Is_Relative__c = true,
			Key__c = 'key',
			Language_Code__c = 'en_US',
			Region__c = 'US',
			Url__c = '/site/test'
		);
		ExternalFileUrlInfo info = new ExternalFileUrlInfo(mdt);
		System.assertEquals(
			true,
			info.url.contains(mdt.URL__c),
			'url must contain /site/test'
		);
		System.assertEquals(mdt.Key__c, info.key, 'key must be the same');
		System.assertEquals(
			mdt.Language_Code__c,
			info.languageCode,
			'languageCode must be the same'
		);
		System.assertEquals(
			mdt.Region__c,
			info.region,
			'languageCode must be the same'
		);
		System.assertEquals(
			mdt.Is_Relative__c,
			info.isRelative,
			'isRelative must be the same'
		);
		info = new ExternalFileUrlInfo();
		Assert.areEqual(true, info != null, 'info must not be null');

		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testGetByKeyRegionAndLanguage() {
		// Test data setup
		External_File_URL__mdt setting = getFileUrl();
		// Actual test
		Test.startTest();
		ExternalFileUrlInfo info = ExternalFileUrlInfo.getByKeyRegionAndLanguage(
			setting?.Key__c,
			setting?.Region__c,
			setting?.Language_Code__c
		);

		Test.stopTest();
		// Asserts
		Assert.areEqual(
			setting != null,
			info != null,
			'info must not be null if setting is not null'
		);
	}

	@isTest
	private static void testGetByKeyRegionAndLanguage1() {
		// Test data setup
		// Actual test
		Test.startTest();
		ExternalFileUrlInfo info = ExternalFileUrlInfo.getByKeyRegionAndLanguage(
			'Key_test_4563',
			null,
			null
		);

		Test.stopTest();
		// Asserts
		Assert.areEqual(
			false,
			info != null,
			'info must not be null if setting is not null'
		);
	}

	private static External_File_URL__mdt getFileUrl() {
		List<External_File_URL__mdt> settings = [
			SELECT Is_Relative__c, Key__c, Language_Code__c, Region__c, URL__c
			FROM External_File_URL__mdt
			LIMIT 1
		];
		return settings.isEmpty() ? null : settings[0];
	}
}