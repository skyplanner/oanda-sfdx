@isTest
private class LiveChatTranscriptUtilTest {
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	
	static testMethod void testLinkTranscriptToCaseAndLead() {
		System.runAs(SYSTEM_USER) {
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id, Origin='Chat');
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now());
			insert t;
			
			t = [SELECT LeadId, CaseId FROM LiveChatTranscript WHERE Id=:t.Id];
			
			System.assertEquals(c.Id, t.CaseId);
			System.assertEquals(l.Id, t.LeadId);
		}
	}
	
	/*
	static testMethod void testLinkTranscriptToTwoCaseAndLead() {
		System.runAs(SYSTEM_USER) {
			Lead l1 = new Lead(LastName='test', Email='test1@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			Lead l2 = new Lead(LastName='test', Email='test2@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert new List<Lead> {l1, l2};
			
			Case c2 = new Case(Subject='test subject', Lead__c=l2.Id, Origin='Chat');
			insert c2;

			LiveChatVisitor v2 = new LiveChatVisitor();
			insert v2;

			LiveChatTranscript t2 = new LiveChatTranscript(LiveChatVisitorId=v2.Id, StartTime=DateTime.now());
			insert t2;
			
			t2 = [SELECT LeadId, CaseId FROM LiveChatTranscript WHERE Id=:t2.Id];
			
			System.assertEquals(c2.Id, t2.CaseId);
			System.assertEquals(l2.Id, t2.LeadId);

			Case c1 = new Case(Subject='test subject', Lead__c=l1.Id, Origin='Chat');
			insert c1;
			
			LiveChatVisitor v1 = new LiveChatVisitor();
			insert v1;
			
			LiveChatTranscript t1 = new LiveChatTranscript(LiveChatVisitorId=v1.Id, StartTime=DateTime.now());
			insert t1;
			
			t1 = [SELECT LeadId, CaseId, Debug_Message__c FROM LiveChatTranscript WHERE Id=:t1.Id];
			
			System.assertNotEquals(null, t1.Debug_Message__c);
			System.assertEquals(c1.Id, t1.CaseId);
			System.assertEquals(l1.Id, t1.LeadId);

			Case c1b = new Case(Subject='test subject', Lead__c=l1.Id, Origin='Chat');
			Case c2b = new Case(Subject='test subject', Lead__c=l2.Id, Origin='Chat');
			insert new List<Case> {c1b, c2b};
			
			LiveChatTranscript t1b = new LiveChatTranscript(LiveChatVisitorId=v1.Id, StartTime=DateTime.now());
			LiveChatTranscript t2b = new LiveChatTranscript(LiveChatVisitorId=v2.Id, StartTime=DateTime.now());
			insert new List<LiveChatTranscript> {t1b, t2b};
			
			t1b = [SELECT LeadId, CaseId FROM LiveChatTranscript WHERE Id=:t1b.Id];
			
			System.assertEquals(c1b.Id, t1b.CaseId);
			System.assertEquals(l1.Id, t1b.LeadId);
			
			t2b = [SELECT LeadId, CaseId FROM LiveChatTranscript WHERE Id=:t2b.Id];
			
			System.assertEquals(c2b.Id, t2b.CaseId);
			System.assertEquals(l2.Id, t2b.LeadId);
		}
	}
	*/
	static testMethod void testLinkTranscriptToCaseAndLeadFail() {
		System.runAs(SYSTEM_USER) {
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id, Origin='Chat');
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now().addSeconds(11));
			insert t;
			
			t = [SELECT LeadId, CaseId FROM LiveChatTranscript WHERE Id=:t.Id];
			
			System.assertEquals(null, t.CaseId);
			System.assertEquals(null, t.LeadId);
		}
	}
	
	static testMethod void testLinkToAccount() {
		System.runAs(SYSTEM_USER) {
			Account a = new Account(Name='test account');
			insert a;
			
			Contact c = new Contact(LastName='test', Email='test@oanda.com', AccountId=a.Id);
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now(), ContactId=c.Id);
			insert t;
			
			t = [SELECT AccountId FROM LiveChatTranscript WHERE Id=:t.Id];
			
			System.assertEquals(a.Id, t.AccountId);
		}
	}
	
	static testMethod void testLinkPostChatSurvey() {
		System.runAs(SYSTEM_USER) {
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id, Origin='Chat');
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			Post_Chat_Survey__c s = new Post_Chat_Survey__c(Chat_Key__c='testkey');
			insert s;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now(), ChatKey='testkey');
			insert t;
			
			s = [SELECT Live_Chat_Transcript__c FROM Post_Chat_Survey__c WHERE Id=:s.Id];
			
			System.assertEquals(t.Id, s.Live_Chat_Transcript__c);
		}
	}

	@isTest
	static void updateCaseOwnerForTransfers() {
		Lead l = new Lead(
			LastName = 'test',
			Email = 'test@oanda.com',
			RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
		insert l;
		
		Case c = new Case(
			Subject = 'test subject',
			Lead__c = l.Id,
			Origin = 'Chat');
		insert c;
		
		LiveChatVisitor v = new LiveChatVisitor();
		insert v;
		
		Post_Chat_Survey__c s =
			new Post_Chat_Survey__c(
				Chat_Key__c='testkey');
		insert s;
		
		Test.startTest();

		LiveChatTranscript t;

		System.runAs(
			UserUtil.getUserByName('Registration User'))
		{
			t = new LiveChatTranscript(
				LiveChatVisitorId = v.Id,
				StartTime = DateTime.now(),
				ChatKey = 'testkey',
				CaseId = c.Id);
			insert t;
		}

		Test.stopTest();

		System.assertEquals(
			[SELECT OwnerId
				FROM LiveChatTranscript
				WHERE Id = :t.Id].OwnerId,
			[SELECT OwnerId
				FROM Case
				WHERE Id = :c.Id].OwnerId);
	}


	@isTest
	static void populateWordCountInsertTest() {
		Lead l = new Lead(
			LastName = 'test',
			Email = 'test@oanda.com',
			RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
		insert l;
		
		Case c = new Case(
			Subject = 'test subject',
			Lead__c = l.Id,
			Origin = 'Chat');
		insert c;
		
		LiveChatVisitor v = new LiveChatVisitor();
		insert v;

		
		Test.startTest();

		LiveChatTranscript t = new LiveChatTranscript(
			LiveChatVisitorId = v.Id,
			StartTime = DateTime.now(),
			CaseId = c.Id,
			Body='<p align="center">Chat Started: Tuesday, August 08, 2023, 15:18:33 (-0400)</p><p align="center">Chat Origin: English by skills</p>'+
				+'<p align="center">Agent Michael C</p>( 20s ) Michael: Thank you for contacting OANDA. My name is Michael. '+
				+'How may I help you today?<br>( 23s ) Visitor: test <br>');
		insert t;

		Test.stopTest();

		System.assertEquals(16,[SELECT Word_Count__c FROM LiveChatTranscript WHERE Id=:t.Id].Word_Count__c);
	}

	@isTest
	static void populateWordCountUpdateTest() {
		Lead l = new Lead(
			LastName = 'test',
			Email = 'test@oanda.com',
			RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
		insert l;
		
		Case c = new Case(
			Subject = 'test subject',
			Lead__c = l.Id,
			Origin = 'Chat');
		insert c;
		
		LiveChatVisitor v = new LiveChatVisitor();
		insert v;

		
		Test.startTest();

		LiveChatTranscript t = new LiveChatTranscript(
			LiveChatVisitorId = v.Id,
			StartTime = DateTime.now(),
			CaseId = c.Id);
		insert t;

		t.Body='<p align="center">Chat Started: Tuesday, August 08, 2023, 15:18:33 (-0400)</p><p align="center">Chat Origin: English by skills</p>'+
			+'<p align="center">Agent Michael C</p>( 20s ) Michael: Thank you for contacting OANDA. My name is Michael. '+
			+'How may I help you today?<br>( 1m 23s ) Visitor: test<br>';
		update t;

		Test.stopTest();

		System.assertEquals(16,[SELECT Word_Count__c FROM LiveChatTranscript WHERE Id=:t.Id].Word_Count__c);
	}

	@isTest
	static void extractTranscriptTest() {
		String s1 = LiveChatTranscriptUtil.extractTranscript(null);
		String s2 = LiveChatTranscriptUtil.extractTranscript('');
		String s3 = LiveChatTranscriptUtil.extractTranscript('<p align="center">Chat Started: Tuesday, August 08, 2023, 15:18:33 '+
		+'(-0400)</p><p align="center">Chat Origin: English by skills</p>'+
		+'<p align="center">Agent Michael C</p>( 1 20s ) Michael: Thank you for contacting OANDA. My name is Michael.');
		String s4 = LiveChatTranscriptUtil.extractTranscript('test');
		String s5 = LiveChatTranscriptUtil.extractTranscript(' test ');
		String s6 = LiveChatTranscriptUtil.extractTranscript('<p align="center">Chat Started: Tuesday, August 08, 2023, 15:18:33 '+
		+'(-0400)</p><p align="center">Chat Origin: English by skills</p>'+
		+'<p align="center">Agent Michael C</p>( 1 20s ) Michael: Thank you    for contacting OANDA. My  name  is  Michael.');
		System.assertEquals('',s1);
		System.assertEquals('',s2);
		System.assertEquals('Thank you for contacting OANDA. My name is Michael.', s3);
		System.assertEquals('test', s4);
		System.assertEquals('test', s5);
		System.assertEquals('Thank you for contacting OANDA. My name is Michael.', s6);
	}
	
	@isTest
	static void countWordsTest() {
		String s1 = null;
		String s2 = '';
		String s3 = 'Thank you for contacting OANDA. My name is Michael.';
		System.assertEquals(0, LiveChatTranscriptUtil.countWords(s1));
		System.assertEquals(0, LiveChatTranscriptUtil.countWords(s2));
		System.assertEquals(9, LiveChatTranscriptUtil.countWords(s3));
	}
}