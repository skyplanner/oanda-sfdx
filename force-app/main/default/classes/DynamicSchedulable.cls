/**
 * @File Name          : DynamicSchedulable.cls
 * @Description        : 
 * Class that implements the "Schedulable" interface and invokes the actual 
 * implementation class dynamically
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/19/2024, 12:00:35 AM
**/
public without sharing virtual class DynamicSchedulable implements Schedulable {

	final String className;
	final Type dynamicApexType;

	public DynamicSchedulable(String className) {
		System.debug('DynamicSchedulable -> constructor');
		this.className = className;
		 
		if (String.isBlank(className)) {
			throw LogException.newInstance(
				'Invalid class name (className is blank)', // msg
				DynamicSchedulable.class.getName() // source
			);
		}
		// else...
		dynamicApexType = Type.forName(className);

		if (dynamicApexType == null) {
			throw LogException.newInstance(
				'Invalid class name: ' + className, // msg
				DynamicSchedulable.class.getName() // source
			);
		}
	}

	public void execute(SchedulableContext ctx) {
		System.debug('DynamicSchedulable -> execute');
		try {
			ExceptionTestUtil.execute();
			Schedulable instance = getDynamicIntance();
			instance.execute(ctx);
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				DynamicSchedulable.class.getName(),
				ex
			);
		}
	}

	@TestVisible
	Schedulable getDynamicIntance() {
		System.debug('DynamicSchedulable -> getDynamicIntance');
		Object result = dynamicApexType.newInstance();

		if ((result instanceof Schedulable) == false) {
			throw new SPException(
				'Class "' + className + 
				'" does not implement the Schedulable interface'
			);
		}
		// else...
		return (Schedulable) result;
	}

}