/**
 * @File Name          : MessagingSessionUpdateManagerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:10:18 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/21/2023, 1:23:27 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class MessagingSessionUpdateManagerTest {
	@isTest
	private static void testProcessUpdate() {
		// Test data setup
		UpdateInfo updateInfo = new UpdateInfo();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'Inquiry_Nature__c';
		updateInfo.fieldValue = 'value';
		// Actual test
		Test.startTest();
		MessagingSessionUpdateManager sessionUpdateManager = new MessagingSessionUpdateManager();
		try {
			sessionUpdateManager.processUpdate(
			new List<FieldValueSource>{ UpdateInfo }
		);
		} catch (Exception ex) {
			
		}
		
		Test.stopTest();

		MessagingSession session = (MessagingSession) sessionUpdateManager.recordsById.get(
			null
		);
		// Asserts
		Assert.areEqual(
			'value',
			session.Case_Subtype__c,
			'Case Subtype should be value'
		);
		Assert.areEqual(
			'value',
			session.Case_Type__c,
			'Case type should be value'
		);
		Assert.areEqual(
			'value',
			session.Inquiry_Nature__c,
			'Inquiry Nature should be value'
		);
	}

	@isTest
	private static void testProcessUpdate2() {
		// Test data setup
		UpdateInfo updateInfo = new UpdateInfo();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'Case_Record_Type__c';
		updateInfo.fieldValue = 'value';
		// Actual test
		Test.startTest();
		MessagingSessionUpdateManager sessionUpdateManager = new MessagingSessionUpdateManager();
		try {
			sessionUpdateManager.processUpdate(
			new List<FieldValueSource>{ UpdateInfo }
		);
		} catch (Exception ex) {
			
		}
	
		Test.stopTest();

		MessagingSession session = (MessagingSession) sessionUpdateManager.recordsById.get(
			updateInfo.messagingSessionId
		);
		// Asserts
		Assert.areEqual(
			'value',
			session.Case_Subtype__c,
			'Case Subtype should be value'
		);
		Assert.areEqual(
			'value',
			session.Case_Type__c,
			'Case type should be value'
		);
	}

	@isTest
	private static void testProcessUpdate3() {
		// Test data setup
		// Actual test
		Test.startTest();
		MessagingSessionUpdateManager sessionUpdateManager = new MessagingSessionUpdateManager();
		sessionUpdateManager.processUpdate(new List<FieldValueSource>());

		Test.stopTest();
		// Asserts
		Assert.areEqual(
			true,
			sessionUpdateManager.recordsById.isEmpty(),
			'recordsById must be empty'
		);
	}

	@isTest
	private static void testProcessUpdateEx() {
		// Test data setup
		UpdateInfo updateInfo = new UpdateInfo();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'Inquiry_Nature__c';
		updateInfo.fieldValue = 'value';
		ExceptionTestUtil.prepareDummyException();
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		try {
			MessagingSessionUpdateManager sessionUpdateManager = new MessagingSessionUpdateManager();
			sessionUpdateManager.processUpdate(
				new List<FieldValueSource>{ UpdateInfo }
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();
		// Asserts
		Assert.areEqual(true, exceptionThrown, 'Exception should be thrown');
	}

	@isTest
	private static void testProcessUpdateEx2() {
		// Test data setup
		UpdateInfo2 updateInfo = new UpdateInfo2();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'Inquiry_Nature__c';
		updateInfo.fieldValue = 'value';
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		try {
			MessagingSessionUpdateManager sessionUpdateManager = new MessagingSessionUpdateManager();
			sessionUpdateManager.processUpdate(
				new List<FieldValueSource>{ UpdateInfo }
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();
		// Asserts
		Assert.areEqual(true, exceptionThrown, 'Exception should be thrown');
	}

	public class UpdateInfo implements FieldValueSource {
		public String messagingSessionId;

		public String fieldName;

		public String fieldValue;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			Map<String, Object> result = new Map<String, Object>();
			result.put(fieldName, fieldValue);
			result.put(
				MessagingSessionUpdateManager.CASE_SUBTYPE_FIELD,
				'value'
			);
			result.put(MessagingSessionUpdateManager.CASE_TYPE_FIELD, 'value');
			return result;
		}
	}

	public class UpdateInfo2 implements FieldValueSource {
		public String messagingSessionId;

		public String fieldName;

		public String fieldValue;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			throw new LogException('Test Exception');
		}
	}
}