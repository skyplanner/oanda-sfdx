@isTest
private class fxAccountRiskAssessmentTest {

	@testSetup static void init(){

		// Create test data

		TestDataFactory testHandler = new TestDataFactory();

		Country_Setting__c can = new Country_Setting__c(Name='Canada', Group__c='Canada', ISO_Code__c='ca', Region__c='North America', Zone__c='Americas', Risk_Rating__c='LOW');
        insert can;

        Settings__c settings = new Settings__c(Name='Default', Enable_CRA_Calculation__c = true);
        insert settings;

		Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
      	fxAccount.Contact__c = contact.Id;
    	fxAccount.Funnel_Stage__c = FunnelStatus.AWAITING_CLIENT_INFO;
    	fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
      	fxAccount.Division_Name__c = 'OANDA Corporation';
      	fxAccount.Employment_Status__c = 'Unemployed';
      	fxAccount.Citizenship_Nationality__c = null;
      	    	
    	insert fxAccount;
	}
	
	// Testing Incomplete Data
	@isTest static void testCalculateRiskAssessmentNoCitizenship() {
		
		Test.startTest();

		fxAccount__c fxaBefore = [SELECT Id, Citizenship_Nationality__c, Customer_Risk_Assessment_Text__c FROM fxAccount__c LIMIT 1];

		fxaBefore.Citizenship_Nationality__c = null;
		update fxaBefore;

		fxAccount__c fxaAfter = [SELECT Id, Citizenship_Nationality__c, Customer_Risk_Assessment_Text__c FROM fxAccount__c LIMIT 1];

		Test.stopTest();

		system.assertEquals('Incomplete Data', fxaAfter.Customer_Risk_Assessment_Text__c);
	}
	
	// Testing populating Customer Risk Assessment value
	@isTest static void testCalculateRiskAssessment() {

		Test.startTest();

		fxAccount__c fxaBefore = [SELECT Id, Citizenship_Nationality__c, Customer_Risk_Assessment_Text__c FROM fxAccount__c LIMIT 1];

		fxaBefore.Citizenship_Nationality__c = 'Canada';
		update fxaBefore;

		fxAccount__c fxaAfter = [SELECT Id, Citizenship_Nationality__c, Customer_Risk_Assessment_Text__c FROM fxAccount__c LIMIT 1];

		Test.stopTest();

		system.assertNotEquals(null, fxaAfter.Customer_Risk_Assessment_Text__c);
		system.assertNotEquals('Incomplete Data', fxaAfter.Customer_Risk_Assessment_Text__c);
	}
	
}