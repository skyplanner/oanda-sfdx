/**
 * @File Name          : MilestonesSLAViolationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 3:34:57 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 10:36:18 PM   acantero     Initial Version
**/
@isTest
private without sharing class MilestonesSLAViolationManager_Test {

	@testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        );
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        OmnichanelRoutingTestDataFactory.createHvcAccountTestCase();
    }

    //assigned case (owner <> queue)
    @isTest
    static void processSLAViolations1() {
        String caseId = [select Id from Case limit 1].Id;
        Case caseOld = getCaseById(caseId);
        Case caseNew = getCaseById(caseId);
        caseNew.Milestone_Violated__c = 
            SLAViolationsTestDataFactory.getMilestoneTypeName();
        System.debug('caseNew.Milestone_Violated__c: ' + caseNew.Milestone_Violated__c);
        System.debug('caseOld.Milestone_Violated__c: ' + caseOld.Milestone_Violated__c);
        List<Case> caseList = new List<Case> {caseNew};
        Map<Id,Case> oldMap = new Map<Id,Case> { caseOld.Id => caseOld };
        Test.startTest();
        MilestonesSLAViolationManager instance = 
            new MilestonesSLAViolationManager(
                caseList,
                oldMap
            );
        instance.processSLAViolations();
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(1, count);
    }

    //unassigned case (owner = queue)
    @isTest
    static void processSLAViolations2() {
        String caseId = [select Id from Case limit 1].Id;
        String queueId = [
            select Id 
            from Group 
            where DeveloperName = :OmnichanelConst.SKILL_CASES_QUEUE_DEVNAME
        ].Id;
        Case caseOld = getCaseById(caseId);
        Case caseNew = getCaseById(caseId);
        caseNew.Milestone_Violated__c = 
            SLAViolationsTestDataFactory.getMilestoneTypeName();
        caseNew.OwnerId = queueId; //IMPORTANT!!!
        System.debug('caseNew.Milestone_Violated__c: ' + caseNew.Milestone_Violated__c);
        System.debug('caseOld.Milestone_Violated__c: ' + caseOld.Milestone_Violated__c);
        List<Case> caseList = new List<Case> {caseNew};
        Map<Id,Case> oldMap = new Map<Id,Case> { caseOld.Id => caseOld };
        Test.startTest();
        MilestonesSLAViolationManager instance = 
            new MilestonesSLAViolationManager(
                caseList,
                oldMap
            );
        instance.processSLAViolations();
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(1, count);
    }

    static Case getCaseById(String caseId) {
        Case result = [
            select 
                Id, CaseNumber, Milestone_Violated__c, OwnerId,
                Chat_Division__c, Account_Division_Name__c,
                Inquiry_Nature__c, Is_HVC__c, Chat_Language_Preference__c,
                Language_Preference__c, Live_or_Practice__c,Tier__c,Origin
            from Case
            where Id = :caseId
        ];
        return result;
    }
    
}