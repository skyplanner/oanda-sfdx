/**
 * Helper class to manage SF limits
 */
public with sharing class LimitsHelper {
    
    /**
     * Know if we are ok to do DMLs
     */
    public static Boolean isAbleToDML(List<SObject> objs){
        return (Limits.getLimitDMLRows() - Limits.getDMLRows() >= objs.size());
    }

}