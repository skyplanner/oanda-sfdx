/**
 * @File Name          : AuthenticationCodeEmailProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/21/2023, 9:50:09 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 11:30:41 AM   aniubo     Initial Version
 **/
@isTest
private class AuthenticationCodeEmailProcessTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}

	@isTest
	private static void testSendAuthenticationCodeEmail() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Contact contact = getContact();

		// Actual test
		Test.startTest();
		AuthenticationCodeEmailProcess process = new AuthenticationCodeEmailProcess(
			caseId,
			contact,
			'es'
		);
		Boolean result = process.sendAuthenticationCodeEmail();
		Test.stopTest();
		// Asserts
		Assert.areEqual(true, result, 'The result should be true');
	}

	@isTest
	private static void testSendAuthenticationCodeEmailNoEmailTemplate() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Contact contact = getContact();
		// Actual test
		Test.startTest();
		AuthenticationCodeEmailProcess process = new AuthenticationCodeEmailProcess(
			caseId,
			contact,
			'xscd'
		);
		Boolean result = process.sendAuthenticationCodeEmail();
		Test.stopTest();
		String emailTemplateDevName = SPSettingsManager.getSettingOrDefault(
			MessagingConst.MSG_AUTH_EMAIL_TEMPLATE_SETTING, // settingName
			'Case_Authentication_Code' // defaultValue
		);
		EmailTemplate template = EmailTemplateRepo.getByDevName(
			emailTemplateDevName
		);

		// Asserts
		Assert.areEqual(
			template != null,
			result,
			'The result should be false. No email template'
		);
	}

	private static Contact getContact() {
		List<Contact> recList = [
			SELECT Name, Email
			FROM Contact
			LIMIT 1
		];
		return recList.get(0);
	}
}