/**
 * @File Name          : LiveChatSRoutingInfoManager.cls
 * @Description        : 
 * Creates ServiceRoutingInfo records from PendingServiceRouting records 
 * linked to LiveChatTranscript object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/18/2024, 11:56:02 PM
**/
public inherited sharing class LiveChatSRoutingInfoManager 
	extends CommonSRoutingInfoManager 
	implements SWorkItemUpdater, SRoutingLogCreator {

	public LiveChatSRoutingInfoManager() {
		super();
	}

	protected override List<SObject> getWorkItems(Set<ID> workItemIdSet) {
		return [
			SELECT
				CaseId,
				Chat_Email__c,
				Chat_Division__c,
				Chat_Type_of_Account__c,
				Chat_Language_Preference__c,
				Inquiry_Nature__c,
				Type__c,
				Subtype__c,
				Spot_Crypto__c,
				Tier__c
			FROM LiveChatTranscript 
			WHERE Id IN :workItemIdSet
		];
	}

	@TestVisible
	protected override BaseServiceRoutingInfo getRoutingInfo(
		PendingServiceRouting routingObj, 
		SObject workItem
	) {
		LiveChatTranscript chat = (LiveChatTranscript) workItem;

		ServiceRoutingInfoFactory factory = 
			ServiceRoutingInfoFactory.getInstance();

		ServiceRoutingInfo result = factory.createNewRecord(
			routingObj, // psrObj
			ServiceRoutingInfo.Source.CHAT, // infoSource
			chat // workItem
		);
		result.workItemUpdater = this;
		result.routingLogCreator = this;
		result.dropAdditionalSkills = true;
		result.initTier(chat.Tier__c);
		result.email = chat.Chat_Email__c;
		result.division = chat.Chat_Division__c;
		result.accountType = chat.Chat_Type_of_Account__c;
		result.language = chat.Chat_Language_Preference__c;
		result.inquiryNature = chat.Inquiry_Nature__c;
		result.type = chat.Type__c;
		result.subtype = chat.Subtype__c;
		result.infoIsIncomplete = String.isNotBlank(result.email);
		result.crypto = (chat.Spot_Crypto__c == true);
		//DO NOT REMOVE
		//result.isEmpty = false;
		
		// NEW_CUSTOMER is the default value for chats because of 
		// the imposibility of get Leads created by the pre chat form
		result.relatedCustomerType = 
			OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER;
			
		ServiceLogManager.getInstance().log(
			'getRoutingInfo -> result', // msg
			LiveChatSRoutingInfoManager.class.getName(), // category
			result // details
		);

		return result;
	}

	public List<SObject> getRecordsToUpdate(BaseServiceRoutingInfo routingInfo) {
		ServiceRoutingInfo sRoutingInfo = (ServiceRoutingInfo) routingInfo;
		LiveChatTranscript chat = (LiveChatTranscript) sRoutingInfo.workItem;
		List<SObject> result = new List<SObject>();

		if (sRoutingInfo.tierHasChanged == true) {
			result.add(
				new LiveChatTranscript(
					Id = sRoutingInfo.workItem.Id,
					Tier__c = sRoutingInfo.tier
				)
			);

			// The case associated with the record cannot be updated because 
			// at the time the routing occurs it does not yet exist
			
			// if (chat.CaseId != null) {
			// 	result.add(
			// 		new Case(
			// 			Id = chat.CaseId,
			// 			Tier__c = sRoutingInfo.tier
			// 		)
			// 	);	
			// }
		}
		return result;
	}

	public SObject getRoutingLog(BaseServiceRoutingInfo routingInfo) {
		SObject result = 
			new RoutingLogManager(routingInfo).getLogForChatTranscript();
		return result;
	}

}