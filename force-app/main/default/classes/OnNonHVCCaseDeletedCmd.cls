/**
 * @File Name          : OnNonHVCCaseDeletedCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/3/2021, 7:21:22 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/1/2021, 12:31:42 PM   acantero     Initial Version
**/
public inherited sharing class OnNonHVCCaseDeletedCmd {

    final NonHVCCaseRegister register = new NonHVCCaseRegister();

    public void checkRecord(
        Non_HVC_Case__c rec
    ) {
        if (rec.Status__c == OmnichanelConst.NON_HVC_CASE_STATUS_PARKED) {
            register.onCaseReleased(rec);
        }
    }

    public void execute() {
        register.process();
    }
    
}