/**
 * @File Name          : OmnichanelUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/1/2021, 2:21:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/27/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class OmnichanelUtil_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createTestAccounts();
        OmnichanelRoutingTestDataFactory.createTestLeads();
    }

    @isTest
    static void getRequiredQueueId() {
        Boolean error = false;
        Test.startTest();
        ID result = null;
        try {
            result = OmnichanelUtil.getRequiredQueueId('fake_devname', null);
        } catch (Exception ex) {
            error = true;
        } 
        Test.stopTest();
        System.assertEquals(true, error);
    }

    @isTest
    private static void getHVCEmailSet() {
        Set<String> emailSet = new Set<String> {
            OmnichanelRoutingTestDataFactory.HVC_EMAIL,
            OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL 
        };
        Test.startTest();
        Set<String> result = OmnichanelUtil.getHVCEmailSet(emailSet);
        Test.stopTest();
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
        System.assert(result.contains(OmnichanelRoutingTestDataFactory.HVC_EMAIL));
    }

    @isTest
    private static void getHVCAccountIdSet() {
        // OmnichanelRoutingTestDataFactory.createTestAccounts();
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        ID activeCustomerAccountId = accountMap.get(OmnichanelRoutingTestDataFactory.ACTIVE_CUSTOMER_EMAIL);
        Set<String> accountIdSet = new Set<String> {
            hvcId,
            activeCustomerAccountId 
        };
        Test.startTest();
        Set<String> result = OmnichanelUtil.getHVCAccountIdSet(accountIdSet);
        Test.stopTest();
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
        System.assert(result.contains(hvcId));
    }

    @isTest
    private static void getAccountInfoByEmail() {
        ChatClientInfo result1;
        ChatClientInfo result2;
        Test.startTest();
        result1 = OmnichanelUtil.getAccountInfoByEmail(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        result2 = OmnichanelUtil.getAccountInfoByEmail(OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL);
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assertEquals(null, result2);
    }

    @isTest
    private static void getLeadInfoByEmail() {
        ChatClientInfo result1;
        ChatClientInfo result2;
        Test.startTest();
        result1 = OmnichanelUtil.getLeadInfoByEmail(OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL);
        result2 = OmnichanelUtil.getLeadInfoByEmail(null);
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assertEquals(null, result2);
    }

    @isTest
    private static void getClientInfoByEmail() {
        Test.startTest();
        ChatClientInfo result1 = OmnichanelUtil.getClientInfoByEmail(OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL);
        ChatClientInfo result2 = OmnichanelUtil.getClientInfoByEmail(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        ChatClientInfo result3 = OmnichanelUtil.getClientInfoByEmail('superfakeemail@fake.com');
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assertNotEquals(null, result2);
        System.assertEquals(null, result3);
    }

}