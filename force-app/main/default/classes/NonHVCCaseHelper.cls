/**
 * @File Name          : NonHVCCaseHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/15/2024, 4:47:54 PM
**/
public inherited sharing class NonHVCCaseHelper {

    public static List<Non_HVC_Case__c> getById(
        Set<ID> nonHvcCaseIdSet
    ) {
        if (
            (nonHvcCaseIdSet == null) ||
            nonHvcCaseIdSet.isEmpty()
        ){
            return new List<Non_HVC_Case__c>();
        }
        //else...
        List<Non_HVC_Case__c> result = [
            SELECT 
                Case__c,
                OwnerId
            FROM Non_HVC_Case__c
            WHERE Id in :nonHvcCaseIdSet
        ];
        return result;
    }

    public static List<Non_HVC_Case__c> getByCase(
        Set<ID> caseIdSet
    ) {
        if (
            (caseIdSet == null) ||
            caseIdSet.isEmpty()
        ){
            return new List<Non_HVC_Case__c>();
        }
        //else...
        List<Non_HVC_Case__c> result = [
            SELECT 
                Case__c,
                Status__c
            FROM Non_HVC_Case__c
            WHERE Case__c in :caseIdSet
        ];
        return result;
    }

    public static List<Non_HVC_Case__c> getCaseInfoById(
        Set<ID> nonHvcCaseIdSet
    ) {
        if (
            (nonHvcCaseIdSet == null) ||
            nonHvcCaseIdSet.isEmpty()
        ){
            return new List<Non_HVC_Case__c>();
        }
        //else...
        List<Non_HVC_Case__c> result = [
            SELECT 
                Id, 
                OwnerId,
                Case__r.Id,
                Case__r.Routed_And_Assigned__c,
                Case__r.CaseNumber,
                Case__r.Tier__c
            FROM Non_HVC_Case__c
            WHERE Id in :nonHvcCaseIdSet
        ];
        return result;
    }

}