/**
 * @File Name          : NonHVCCaseSRoutingInfoManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 8:35:23 PM
**/
@IsTest
private without sharing class NonHVCCaseSRoutingInfoManagerTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		initManager.storeData();
	}

	// NON_HVC_CASE_1 is linked to CASE_1
	@IsTest
	static void getRoutingInfoList1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		ID nonHVCCase1Id = initManager.getObjectId(
			ServiceTestDataKeys.NON_HVC_CASE_1, 
			true
		);
	
		Test.startTest();
		NonHVCCaseSRoutingInfoManager instance = 
			new NonHVCCaseSRoutingInfoManager();
		instance.registerPendingServiceRouting(
			new PendingServiceRouting(
				WorkItemId = nonHVCCase1Id
			)
		);
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		Assert.areEqual(
			case1Id, 
			result[0].workItem.Id, 
			'Invalid result'
		);
	}
	
}