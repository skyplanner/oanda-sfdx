/**
 * @File Name          : PicklistUtil_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 2/14/2020, 11:37:45 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/14/2020   dmorales     Initial Version
**/
@isTest
private class PicklistUtil_Test {
    @isTest
    private static void testPicklistDependent()
    {
        //Testing getDependentPickList method 
        Test.startTest();
        Map<Object, List<String>> result =
            PicklistUtil.getDependentPicklistValues
                            (Case.Type);
               
        System.assert( result == null );       

        Map<Object, List<String>> resultWithValues =
           PicklistUtil.getDependentPicklistValues
                            (Case.Type__c);
               
        System.assert(resultWithValues != null ); 
        Test.stopTest(); 
    }
}