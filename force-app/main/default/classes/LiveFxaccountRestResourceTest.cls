/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-10-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class LiveFxaccountRestResourceTest {
    @isTest
	static void testUpdateFxAccount500() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = (new TestDataFactory()).createFXTradeAccount(acc);
        fxAcc.fxTrade_User_Id__c = 5412558;
        update fxAcc;

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{' + JSON.serialize(fxAcc) + '}');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(500, RestContext.response.statusCode);
	}

    @isTest
    static void testUpdateFxAccount404() {
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Conditionally_Approved__c": true}');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(404, RestContext.response.statusCode);
    }

    @isTest
    static void testUpdateFxAccount200() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = (new TestDataFactory()).createFXTradeAccount(acc);
        fxAcc.fxTrade_User_Id__c = 5412558;        
        Id liveRtId = RecordTypeUtil.getFxAccountLiveId();
        String liveRtId15 = liveRtId.to15();
        fxAcc.fxTrade_Global_ID__c = String.format('{0}+{1}', new List<String> { 
            '5412558', liveRtId15 });
            
        update fxAcc;

        fxAcc.Conditionally_Approved__c = true;
        fxAcc.US_Shares_Trading_Enabled__c = true;

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(JSON.serialize(fxAcc));
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(200, RestContext.response.statusCode);
    }

    @isTest
	static void testGetFxAccount500() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = (new TestDataFactory()).createFXTradeAccount(acc);
        fxAcc.fxTrade_User_Id__c = 5412558;
        update fxAcc;

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.params.put('fields', 'Conditionally_Approved');
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doGet();
        
        Test.stopTest();

		System.assertEquals(500, RestContext.response.statusCode);
	}

    @isTest
    static void testGetFxAccount404() {
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.params.put('fields', 'Conditionally_Approved__c');
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doGet();
        
        Test.stopTest();

		System.assertEquals(404, RestContext.response.statusCode);
    }

    @isTest
    static void testGetFxAccount200() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = (new TestDataFactory()).createFXTradeAccount(acc);
        fxAcc.fxTrade_User_Id__c = 5412558;
        Id liveRtId = RecordTypeUtil.getFxAccountLiveId();
        String liveRtId15 = liveRtId.to15();
        fxAcc.fxTrade_Global_ID__c = String.format('{0}+{1}', new List<String> { 
            '5412558', liveRtId15 });
        fxAcc.Conditionally_Approved__c = true;
        fxAcc.US_Shares_Trading_Enabled__c = true;
            
        update fxAcc;

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/fxaccounts/live/5412558';
        request.params.put('fields', 'Conditionally_Approved__c');
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        LiveFxaccountRestResource.doGet();
        
        Test.stopTest();

		System.assertEquals(200, RestContext.response.statusCode);
    }
}