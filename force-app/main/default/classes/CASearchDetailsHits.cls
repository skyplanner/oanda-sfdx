/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsHits {
    public CASearchDetailsDoc doc { get; set; }
    
    public List<String> match_types { get; set; }

    public Map<String, CASearchDetailsMatchTypeDet> match_types_details { get; set; }

    public String match_status { get; set; }

    public Boolean is_whitelisted { get; set; }

    public Decimal score { get; set; }
    
    public String risk_level { get; set; }
}