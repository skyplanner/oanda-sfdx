/**
 * @File Name          : OmnichanelConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2024, 1:50:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/17/2020   acantero     Initial Version
**/
public class OmnichanelConst {

	public static final String OMNICHANEL_ERROR_CAT = 'Omnichanel routing';

	public static final String PRACTICE = 'Practice';
	public static final String LIVE = 'Live';
	public static final String NEW_ACCOUNT = 'New Account';

	public static final String YES = 'Yes';
	public static final String NO = 'No';

	//Skills
	//...OTHER UNRELATED SKILLS (Y)
	public static final String CRYPTO_SKILL = 'Crypto'; // Code = YC
	//...CHAT SKILL (Z)
	public static final String CHAT_SKILL = 'Chat_Skill';
	public static final String CHAT_SKIL_CODE = 'ZZ';
	//...Account (A)
	public static final String PRACTICE_SKILL = 'Practice_Account';
	public static final String PRACTICE_SKILL_CODE = 'AP';
	public static final String LIVE_SKILL = 'Live_Account';
	public static final String LIVE_SKILL_CODE = 'AL';
	//...Language (L)
	public static final String ENGLISH_SKILL = 'English';
	public static final String ENGLISH_SKILL_CODE = 'LE';
	public static final String CHINESE_SKILL = 'Chinese';
	public static final String CHINESE_SKILL_CODE = 'LC';
	public static final String GERMAN_SKILL = 'German';
	public static final String GERMAN_SKILL_CODE = 'LG';
	public static final String SPANISH_SKILL = 'Spanish';
	public static final String SPANISH_SKILL_CODE = 'LS';
	public static final String RUSSIAN_SKILL = 'Russian';
	public static final String RUSSIAN_SKILL_CODE = 'LR';
	public static final String POLISH_SKILL = 'Polish';
	public static final String POLISH_SKILL_CODE = 'LP';
	public static final String FRENCH_SKILL = 'French';
	public static final String FRENCH_SKILL_CODE = 'LF';
	public static final String ITALIAN_SKILL = 'Italian';
	public static final String ITALIAN_SKILL_CODE = 'LI';
	//...Division (O)
	public static final String OEL_SKILL = 'OEL';
	public static final String OEL_SKILL_CODE = 'OEL';
	public static final String OAU_SKILL = 'OAU';
	public static final String OAU_SKILL_CODE = 'OAU';
	public static final String OC_SKILL = 'OC';
	public static final String OC_SKILL_CODE = 'OC';
	public static final String OAP_SKILL = 'OAP';
	public static final String OAP_SKILL_CODE = 'OAP';
	public static final String OCAN_SKILL = 'OCAN';
	public static final String OCAN_SKILL_CODE = 'OCA';
	public static final String OME_SKILL = 'OME';
	public static final String OME_SKILL_CODE = 'OME';

	// Senior Agent Tier =1
	public static final String SENIOR_AGENT_SKILL_CODE = 'SA';
	public static final String SENIOR_AGENT_SKILL = 'Senior_Agent';
	
	//...Customer (C)
	public static final String HVC_SKILL = 'HVC';
	public static final String HVC_SKILL_CODE = 'CH';
	public static final String NEW_CUSTOMER_SKILL = 'New_customer';
	public static final String NEW_CUSTOMER_SKILL_CODE = 'CN';
	public static final String ACTIVE_CUSTOMER_SKILL = 'Active_customer';
	public static final String ACTIVE_CUSTOMER_SKILL_CODE = 'CA';
	public static final String CUSTOMER_SKILL = 'Customer';
	public static final String CUSTOMER_SKILL_CODE = 'CC';
	//...Inquiry Nature (I)
	public static final String LOGIN_SKILL = 'Login';
	public static final String LOGIN_SKILL_CODE = 'IL';
	public static final String REGISTRATION_SKILL = 'Registration';
	public static final String REGISTRATION_SKILL_CODE = 'IR';
	public static final String ACCOUNT_SKILL = 'Account';
	public static final String ACCOUNT_SKILL_CODE = 'IA';
	public static final String PLATFORM_SKILL = 'Platform';
	public static final String PLATFORM_SKILL_CODE = 'IP';
	public static final String TRADE_SKILL = 'Trade';
	public static final String TRADE_SKILL_CODE = 'IT';
	public static final String OTHER_SKILL = 'Other';
	public static final String OTHER_SKILL_CODE = 'IO';
	public static final String DEPOSIT_SKILL = 'Deposit';
	public static final String DEPOSIT_SKILL_CODE = 'ID';
	public static final String WITHDRAWAL_SKILL = 'Withdrawal';
	public static final String WITHDRAWAL_SKILL_CODE = 'IW';
	public static final String FUND_OTHER_SKILL = 'Fund_Other';
	public static final String FUND_OTHER_SKILL_CODE = 'IF';

	public static final String ENGLISH_LANG_CODE = 'en_US';
	public static final String ENGLISH_LANG = 'English';

	public static final String CHINESE_SIMP_LANG_CODE = 'zh_CN';
	public static final String CHINESE_SIMP_LANG = 'Chinese Simplified';
	public static final String CHINESE_LANG = 'Chinese';

	public static final String GERMAN_LANG_CODE = 'de';
	public static final String GERMAN_LANG = 'German';
	public static final String GERMAN_LANG_NATIVE = 'Deutsch';

	public static final String SPANISH_LANG_CODE = 'es';
	public static final String SPANISH_LANG = 'Spanish';

	public static final String RUSSIAN_LANG_CODE = 'ru';
	public static final String RUSSIAN_LANG = 'Russian';
	
	public static final String POLISH_LANG_CODE = 'pl';
	public static final String POLISH_LANG = 'Polish';
	
	public static final String OANDA_CORPORATION = 'OANDA Corporation';
	public static final String OANDA_CORPORATE = 'OANDA Corporate';
	public static final String OANDA_EUROPE = 'OANDA Europe';
	public static final String OANDA_CANADA = 'OANDA Canada';
	public static final String OANDA_ASIA_PACIFIC = 'OANDA Asia Pacific';
	public static final String OANDA_AUSTRALIA = 'OANDA Australia';
	public static final String OANDA_EUROPE_MARKETS = 'OANDA Europe Markets';
	
	public static final String OANDA_CORPORATION_AB = 'OC';
	public static final String OANDA_EUROPE_AB = 'OEL';
	public static final String OANDA_CANADA_AB = 'OCAN';
	public static final String OANDA_ASIA_PACIFIC_AB = 'OAP';
	public static final String OANDA_AUSTRALIA_AB = 'OAU';
	public static final String OANDA_EUROPE_MARKETS_AB = 'OEM';
	public static final String OANDA_GLOBAL_MARKETS = 'OGM';

	public static final String FUNNEL_STAGE_READY_FOR_FUNDING = 'Ready For Funding';
	public static final String FUNNEL_STAGE_FUNDED = 'Funded';
	public static final String FUNNEL_STAGE_TRADED = 'Traded';

	public static final String INQUIRY_NAT_LOGIN = 'Login';
	public static final String INQUIRY_NAT_FUNDING_WITHDRAWAL = 'Funding & Withdrawal';
	public static final String INQUIRY_NAT_REGISTRATION = 'Registration';
	public static final String INQUIRY_NAT_ACCOUNT = 'Account';
	public static final String INQUIRY_NAT_THIRD_PARTY = 'Third Party';
	public static final String INQUIRY_NAT_TRADE = 'Trade';
	public static final String INQUIRY_NAT_OTHER = 'Other';
	public static final String INQUIRY_NAT_FEES_CHARGES = 'Fees & Charges';

	public static final String TYPE_DEPOSIT = 'Deposit';
	public static final String TYPE_WITHDRAWAL = 'Withdrawal';
	public static final String TYPE_FORGOT_OANDA_USERNAME = 'Forgot OANDA username';

	public static final String CASE_ORIGIN_EMAIL_FRONTDESK = 'Email - Frontdesk';
	public static final String CASE_ORIGIN_EMAIL_API = 'Email - API';
	public static final String CASE_ORIGIN_CHATBOT_CASE = 'Chatbot - Case';
	public static final String CASE_ORIGIN_CHATBOT_EMAIL = 'Chatbot - Email';
	public static final String CASE_ORIGIN_CHAT = 'Chat';
	public static final String CASE_ORIGIN_PHONE = 'Phone';

	public static final String CASE_CAPACITY_STATUS_IN_USE = 'In use';
	public static final String CASE_CAPACITY_STATUS_RELEASED = 'Released';
	public static final String CASE_CAPACITY_STATUS_PARKED = 'Parked';
	public static final String CASE_CAPACITY_STATUS_CLOSED = 'Closed';
	public static final String CASE_CAPACITY_STATUS_ROUTING = 'Routing';
	public static final String CASE_CAPACITY_STATUS_REROUTE = 'Release and Reroute';

	public static final String CASE_PRIORITY_HIGH = 'High';
	public static final String CASE_PRIORITY_CRITICAL = 'Critical';

	public static final String CASE_STATUS_OPEN = 'Open';
	public static final String CASE_STATUS_RE_OPENED = 'Re-opened';
	public static final String CASE_STATUS_IN_PROGRESS = 'In Progress';
	public static final String CASE_STATUS_CLOSED = 'Closed';

	public static final String CHAT_IS_HVC_YES = YES;
	public static final String CHAT_IS_HVC_NO = NO;

	public static final String CHAT_STATUS_MISSED = 'Missed';

	//public static final String CASE_SUBJECT_CHATBOT_SERVICE_REQ = 'Chatbot Service Request';

	public static final String SKILLS_BASED_ROUTING_TYPE = 'SkillsBased';
	
	public static final String EMAIL_CASES_ROUTING_CONFIG = 'EmailCaseBySkills';
	public static final String NON_HVC_EMAIL_CASES_ROUTING_CONFIG = 'NonHVCEmailCaseBySkills';
	public static final String CHAT_ROUTING_CONFIG = 'ChatBySkills';

	public static final String CASE_SERVICE_CHANNEL = 'Case';
	public static final String NON_HVC_CASE_SERVICE_CHANNEL = 'Non_HVC_Case';

	public static final String SKILL_CASES_QUEUE_DEVNAME = 'Skill_Cases';
	public static final String SKILL_NON_HVC_CASES_QUEUE_DEVNAME = 'Skill_Non_HVC_Cases';

	public static final String SERVICE_RES_AGENT = 'A';

	public static final String AGENT_WORK_STATUS_ASSIGNED = 'Assigned';
	public static final String AGENT_WORK_STATUS_OPENED = 'Opened';

	public static final Integer DEFAULT_SKILL_PRIORITY = 1;
	public static final Integer DEFAULT_SKILL_LEVEL = 1;
	public static final Integer MAX_SKILL_LEVEL = 10;

	public static final String NON_HVC_CASE_STATUS_PARKED = 'Parked';
	public static final String NON_HVC_CASE_STATUS_RELEASED = 'Released';
	public static final String NON_HVC_CASE_STATUS_DEFAULT = 'Default';

	public static final String NON_HVC_CASE_NOTIF_DEV_NAME = 'Non_HVC_Case_Notification';

	public static final String ROUTING_STATUS_ROUTING = 'Routing';
	public static final String ROUTING_STATUS_ASSIGNED = 'Assigned';
	public static final String ROUTING_STATUS_OPENED = 'Opened';

}