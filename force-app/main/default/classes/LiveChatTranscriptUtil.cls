public without sharing class LiveChatTranscriptUtil {
	static public void linkPostChatSurvey(List<LiveChatTranscript> transcripts, Map<Id, LiveChatTranscript> oldMap, boolean isInsert) {
		Map<String, LiveChatTranscript> liveChatTranscriptByChatKey = new Map<String, LiveChatTranscript>();
		for(LiveChatTranscript t : transcripts) {
			if((isInsert && t.ChatKey!=null && t.ChatKey!='') || (!isInsert && t.ChatKey != oldMap.get(t.Id).ChatKey)) {
				liveChatTranscriptByChatKey.put(t.ChatKey, t);
			}
		}
		
		List<Post_Chat_Survey__c> surveys = [SELECT Id, Chat_Key__c FROM Post_Chat_Survey__c WHERE Chat_Key__c IN :liveChatTranscriptByChatKey.keySet() AND Live_Chat_Transcript__c=null AND Chat_Key__c!=null];
		
		if(surveys.size()>0) {
			for(Post_Chat_Survey__c s : surveys) {
				LiveChatTranscript t = liveChatTranscriptByChatKey.get(s.Chat_Key__c);
				s.Live_Chat_Transcript__c = t.Id;
				s.OwnerId = t.OwnerId;
			}
			
			update surveys;
		}
	}
	
	// during a chat transfer, the transcript does not get attached properly, so attach them by time stamps
	static public void linkLiveChatTranscriptToCaseAndLead(List<LiveChatTranscript> transcripts) {
		List<LiveChatTranscript> transcriptsWithoutCase = new List<LiveChatTranscript>();
		for(LiveChatTranscript t : transcripts) {
			// get all the transcripts that aren't tied to a case
			if(t.CaseId==null) {
				transcriptsWithoutCase.add(t);
			}
		}

		if(transcriptsWithoutCase.size()>0) {		
			List<LiveChatTranscript> transcriptsToTry = new List<LiveChatTranscript>();
			// get cases that were created around the same time as the transcripts, and aren't already tied to a transcript
			Map<Id, Case> caseById = new Map<Id, Case>([SELECT Id, Lead__c, AccountId, ContactId, CreatedDate FROM Case WHERE Origin='Chat' AND CreatedDate > :DateTime.now().addHours(-8) AND Id NOT IN (SELECT CaseId FROM LiveChatTranscript WHERE RequestTime > :DateTime.now().addHours(-8) AND CaseId!=null) ORDER BY CreatedDate]);
			String emailMessage = '';
			for(LiveChatTranscript t : transcriptsWithoutCase) {
				if(t.StartTime!=null) {
					List<Case> eligibleCases = new List<Case>();
					for(Case c : caseById.values()) {
						if(t.StartTime.addSeconds(-1) <= c.CreatedDate && c.CreatedDate < t.StartTime.addSeconds(10)) {
							eligibleCases.add(c);
						}
					}
					
					if(eligibleCases.size()==1) {
						if(t.CaseId==null) {
							t.CaseId = eligibleCases[0].Id;
						}
						if(t.LeadId==null) {
							t.LeadId = eligibleCases[0].Lead__c;
						}
						if(t.AccountId==null) {
							t.AccountId = eligibleCases[0].AccountId;
						}
						if(t.ContactId==null) {
							t.ContactId = eligibleCases[0].ContactId;
						}
					} else {
						transcriptsToTry.add(t);
						String debugMessage = 'eligibleCases.size():' + eligibleCases.size() + ':';
						for(Case c : eligibleCases) {
							debugMessage += ' CaseId: ' + c.Id;
							debugMessage += ' LeadId: ' + c.Lead__c;
							debugMessage += ' AccountId: ' + c.AccountId;
							debugMessage += ' ContactId: ' + c.ContactId + '; ';
						}
						t.Debug_Message__c += debugMessage;
						
						// If this becomes a problem, can use first Live Chat Transcript Event: Agent to match up with Case createdby 
						String message = 'Cannot link transcript and case\n';
						message += 'transcript: ' + t;
						message += '\neligibleCases: ' + eligibleCases;
						message += '\ncaseById: ' + caseById;
						System.debug(message);
						
						emailMessage += message + '\n\n';
					}
				}
			}
			if(emailMessage!='') {
				try {
					EmailUtil.sendEmail(UserUtil.getUserId('ahwang@oanda.com'), 'LiveChatTranscriptUtil.linkLiveChatTranscriptToCaseAndLead() error', emailMessage);
				} catch (Exception e) {}
			}
			if(transcriptsToTry.size()>0) {
				linkTranscriptViaVisitor(transcriptsToTry);
			}
		}
	}
	
	public static List<LiveChatTranscript> linkTranscriptViaVisitor(List<LiveChatTranscript> transcripts) {
		Map<Id, LiveChatTranscript> transcriptToUpdateById = new Map<Id, LiveChatTranscript>();
		Map<Id, List<LiveChatTranscript>> transcriptsByVisitorId = new Map<Id, List<LiveChatTranscript>>();
		for(LiveChatTranscript t : transcripts) {
			List<LiveChatTranscript> transForVid = transcriptsByVisitorId.get(t.LiveChatVisitorId);
			if(transForVid==null) {
				transForVid = new List<LiveChatTranscript>();
				transcriptsByVisitorId.put(t.LiveChatVisitorId, transForVid);
			}
			transForVid.add(t);
		}
		Set<Id> visitorIds = new Set<Id>();
		// Map<Id, LiveChatVisitor> visitorById = new Map<Id, LiveChatVisitor>();
		Set<Id> transcriptIds = new Set<Id>();
		for(LiveChatTranscript t : transcripts) {
			visitorIds.add(t.LiveChatVisitorId);
			transcriptIds.add(t.Id);
		}
	
		// List<Case> casesToUpdate = new List<Case>();
		List<LiveChatTranscript> existingTranscripts = [SELECT Id, LiveChatVisitorId, CaseId, LeadId, AccountId, ContactId FROM LiveChatTranscript WHERE LiveChatVisitorId IN :visitorIds AND Id NOT IN :transcriptIds];
		Set<Id> leadIds = new Set<Id>();
		Set<Id> contactIds = new Set<Id>();
		Set<Id> caseIds = new Set<Id>();
		for(LiveChatTranscript t : existingTranscripts) {
			if(t.CaseId!=null) {
				caseIds.add(t.CaseId);
			}
			if(t.LeadId!=null) {
				leadIds.add(t.LeadId);
			} else if(t.ContactId!=null) {
				contactIds.add(t.ContactId);
			}
		}
		
		// get cases for those leads/contacts but don't have a transcript tied to them
		Map<Id, List<Case>> casesWithoutTranscriptsByLeadId = new Map<Id, List<Case>>();
		Map<Id, List<Case>> casesWithoutTranscriptsByContactId = new Map<Id, List<Case>>();
		
		for(Case c : [SELECT Id, Lead__c, AccountId, ContactId FROM Case WHERE (Lead__c IN :leadIds OR ContactId IN :contactIds) AND Id NOT IN :caseIds]) {
			if(c.Lead__c!=null) {
				List<Case> cases = casesWithoutTranscriptsByLeadId.get(c.Lead__c);
				if(cases==null) {
					cases = new List<Case>();
					casesWithoutTranscriptsByLeadId.put(c.Lead__c, cases);
				}
				cases.add(c);
			} else if(c.ContactId!=null) {
				List<Case> cases = casesWithoutTranscriptsByContactId.get(c.ContactId);
				if(cases==null) {
					cases = new List<Case>();
					casesWithoutTranscriptsByContactId.put(c.ContactId, cases);
				}
				cases.add(c);
			}
		}
		
		for(LiveChatTranscript existingTranscript : existingTranscripts) {
			if(existingTranscript.LeadId!=null || existingTranscript.ContactId!=null) {
				for(LiveChatTranscript t : transcriptsByVisitorId.get(existingTranscript.LiveChatVisitorId)) {
					List<Case> cases;
					if(existingTranscript.LeadId!=null) {
						t.LeadId = existingTranscript.LeadId;
						
						cases = casesWithoutTranscriptsByLeadId.get(t.LeadId);
					} else if(existingTranscript.ContactId!=null) {
						t.AccountId = existingTranscript.AccountId;
						t.ContactId = existingTranscript.ContactId;
						
						cases = casesWithoutTranscriptsByContactId.get(t.ContactId);
					}
					if(cases!=null) {
						if(cases.size()==1) {
							t.CaseId = cases[0].Id;
							t.Debug_Message__c += '; CaseId set by linkTranscriptViaVisitor(); ';
						} else if(cases.size()==0) {
							t.Debug_Message__c += '; linkTranscriptViaVisitor(): no cases found; ';
						} else { // cases.size()>1
							t.Debug_Message__c += '; linkTranscriptViaVisitor(): more than 1 case found (ids: ';
							for(Case c : cases) {
								t.Debug_Message__c += c.Id + ',';
							}
							t.Debug_Message__c += ')';
						}
					}
					transcriptToUpdateById.put(t.Id, t);
				}
			}
		}
		return transcriptToUpdateById.values();
	}
	
	// transcripts don't attach to accounts, just the contact, so need to attach them
	static public void linkToAccount(List<LiveChatTranscript> transcripts) {
		Map<Id, LiveChatTranscript> transcriptsWithoutAccountByContactId = new Map<Id, LiveChatTranscript>();
		for(LiveChatTranscript t : transcripts) {
			// get all the transcripts that aren't tied to an account
			if(t.AccountId==null && t.ContactId!=null) {
				transcriptsWithoutAccountByContactId.put(t.ContactId, t);
			}
		}

		if(transcriptsWithoutAccountByContactId.size()>0) {		
			// get transcripts that aren't attached to an account, but are attached to a contact
			Map<Id, Contact> contactById = new Map<Id, Contact>([SELECT Id, AccountId FROM Contact WHERE Id IN :transcriptsWithoutAccountByContactId.keySet()]);
			
			for(LiveChatTranscript t : transcriptsWithoutAccountByContactId.values()) {
				Contact c = contactById.get(t.ContactId);
				if(c!=null) {
					t.AccountId = c.AccountId;
				}
			}
		}
	}

	static public void populateWordCount(List<LiveChatTranscript> transcripts, Map<Id,LiveChatTranscript> oldTranscripts) {
		for(LiveChatTranscript t : transcripts) {
			if(oldTranscripts==null || (oldTranscripts.containsKey(t.Id) && t.Body!=oldTranscripts.get(t.Id).Body)){
				String parsedTranscript = extractTranscript(t.Body);
				t.Word_Count__c = countWords(parsedTranscript);
			}
		}
	}
	

	static public String extractTranscript(String body) {
		if(!String.isBlank(body)){
			Extract extract = extractAgentsName(body);
			String htmlRemoved = extract.transcript.replaceAll('<.*?>', '');
			String timestampsRemoved = htmlRemoved.replaceAll('\\( (.*?)s \\)', '');
			String agentsNameRemoved = timestampsRemoved.replaceAll(extract.agentsName+': ', '');
			String visitorRemoved = agentsNameRemoved.replaceAll('Visitor: ', '');
			String spacesNormalized = visitorRemoved.normalizeSpace();
			return spacesNormalized;
		}else{
			return '';
		}
	}

	static public Integer countWords(String transcript) {
		if(String.isBlank(transcript)) return 0;
		return transcript.split(' ').size();
	}

	static public Extract extractAgentsName(String transcript) {
		Extract extract = new Extract(transcript, '');
		if(transcript.contains('Agent ')){
			extract.transcript = transcript.substring(transcript.indexOf('Agent ')+6);
			extract.agentsName = extract.transcript.SubStringBefore('</p>');
			extract.transcript = extract.transcript.SubStringAfter('</p>');
			if(extract.agentsName .length()>2) extract.agentsName  = extract.agentsName .substring(0, extract.agentsName .length()-2);
			return extract;
		}else{
			return extract;
		}
	}

	public class Extract{
		public String transcript;
		public String agentsName;

		Extract(String transcript, String agentsName) {
			this.transcript=transcript;
			this.agentsName=agentsName;
		}
	}
}