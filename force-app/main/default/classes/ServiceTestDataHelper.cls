/**
 * @File Name          : ServiceTestDataHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 4:39:32 AM
**/
@IsTest
public without sharing class ServiceTestDataHelper {

	public static Account getAccount(ID accountId) {
		return [
			SELECT 
				Name,
				PersonEmail
			FROM Account
			WHERE Id = :accountId
		];
	}

	public static fxAccount__c getFxAccount(ID fxAccountId) {
		return [
			SELECT 
				Name,
				Account__c
			FROM fxAccount__c
			WHERE Id = :fxAccountId
		];
	}

}