/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchCASearchInfoUpdateTest {
    @isTest
	static void updateCA() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
			Custom_Division_Name__c = 'OANDA Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			200,
			'{' +
			'	"content": {' +
			'		"data": {' +
			'			"id": 111111111,' +
			'			"ref": "1111111111-UNWU1V0z",' +
			'			"searcher_id": 478009745,' +
			'			"assignee_id": 478009746,' +
			'			"filters": {' +
			'				"birth_year": 1998,' +
			'				"country_codes": [],' +
			'				"exact_match": false,' +
			'				"fuzziness": 0.5,' +
			'				"remove_deceased": 0,' +
			'				"types": [' +
			'					"pep-class-1"' +
			'				]' +
			'			},' +
			'			"match_status": "false_positive",' +
			'			"risk_level": "low",' +
			'			"search_term": "Qian-Yu Tong",' +
			'			"submitted_term": "Qian-Yu Tong",' +
			'			"client_ref": "Qianyu1998",' +
			'			"total_hits": 1,' +
			'			"total_matches": 1,' +
			'			"total_blacklist_hits": 0,' +
			'			"created_at": "2019-08-02 01:49:14",' +
			'			"updated_at": "2022-09-07 01:13:14",' +
			'			"blacklist_filters": {' +
			'				"blacklist_ids": [' +
			'					"61ea14d3b338db93377345545"' +
			'				]' +
			'			},' +
			'			"tags": [],' +
			'			"labels": [],' +
			'			"search_profile": {' +
			'				"name": "OAP",' +
			'				"slug": "oap"' +
			'			},' +
			'			"limit": 100,' +
			'			"offset": 0,' +
			'			"searcher": {' +
			'				"id": 6601,' +
			'				"email": "oap-compliance-test@oanda.com",' +
			'				"name": "Oanda Compliance Team ",' +
			'				"phone": "",' +
			'				"created_at": "2019-07-01 02:34:03",' +
			'				"user_is_active": false' +
			'			},' +
			'			"assignee": {' +
			'				"id": 7670,' +
			'				"email": "Aira.test_demo@test.com",' +
			'				"name": "Aira Test",' +
			'				"phone": "",' +
			'				"created_at": "2019-10-25 20:43:09",' +
			'				"user_is_active": false' +
			'			},' +
			'			"hits": [' +
			'				{' +
			'					"doc": {' +
			'						"id": "4857893475W2WBST",' +
			'						"last_updated_utc": "2021-02-05T12:20:06Z",' +
			'						"created_utc": "2021-02-05T12:20:06Z",' +
			'						"fields": [' +
			'							{' +
			'								"name": "Country",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "China"' +
			'							},' +
			'							{' +
			'								"name": "Original Country Text",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "China"' +
			'							},' +
			'							{' +
			'								"name": "Political Position",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"tag": "political_position",' +
			'								"value": "Member of the Local Council"' +
			'							},' +
			'							{' +
			'								"name": "Chamber",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "Standing Committee of Qingdao Municipal People\'s Congress"' +
			'							},' +
			'							{' +
			'								"name": "Function",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "委 员"' +
			'							},' +
			'							{' +
			'								"name": "Institution Type",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "City Council"' +
			'							},' +
			'							{' +
			'								"name": "Locationurl",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "http://index.html"' +
			'							},' +
			'							{' +
			'								"name": "Region",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "Shandong"' +
			'							},' +
			'							{' +
			'								"name": "Countries",' +
			'								"tag": "country_names",' +
			'								"value": "China"' +
			'							}' +
			'						],' +
			'						"types": [' +
			'							"pep",' +
			'							"pep-class-4"' +
			'						],' +
			'						"name": "童 煜 (Tong  Yu )",' +
			'						"entity_type": "person",' +
			'						"aka": [' +
			'							{' +
			'								"name": "Tong  Yu "' +
			'							},' +
			'							{' +
			'								"name": "童 煜"' +
			'							}' +
			'						],' +
			'						"sources": [' +
			'							"china-standing-committee-of-qingdao-municipal-peoples-congress-members"' +
			'						],' +
			'						"keywords": [],' +
			'						"source_notes": {' +
			'							"china-standing-committee-of-qingdao-municipal-peoples-congress-members": {' +
			'								"aml_types": [' +
			'									"pep-class-4"' +
			'								],' +
			'								"country_codes": [' +
			'									"CN"' +
			'								],' +
			'								"name": "China Standing Committee of Qingdao Municipal People\'s Congress Members",' +
			'								"url": "http://rdcwh.qingdao.gov.cn/n8146584/index.html"' +
			'							}' +
			'						}' +
			'					},' +
			'					"match_types": [' +
			'						"name_exact",' +
			'						"name_variations_removal"' +
			'					],' +
			'					"match_types_details": {' +
			'						"Tong Yu": {' +
			'							"match_types": {' +
			'								"qian": [' +
			'									"name_variations_removal"' +
			'								],' +
			'								"tong": [' +
			'									"name_exact"' +
			'								],' +
			'								"yu": [' +
			'									"name_exact"' +
			'								]' +
			'							},' +
			'							"type": "name"' +
			'						}' +
			'					},' +
			'					"match_status": "potential_match",' +
			'					"is_whitelisted": false,' +
			'					"score": 30.107817' +
			'				}' +
			'			],' +
			'			"blacklist_hits": []' +
			'		}' +
			'	}' +
			'}',
			'OK',
			null
		);
		
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        BatchCASearchInfoUpdate b = new BatchCASearchInfoUpdate(' Id = \'' + search.Id + '\'');
        Database.executeBatch(b);

		Test.stopTest();

		List<AsyncApexJob> aj = [SELECT Id
			FROM AsyncApexJob
			WHERE ApexClass.Name = 'BatchCASearchInfoUpdate'
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		System.assertEquals(true, aj.size() > 0);
	}
}