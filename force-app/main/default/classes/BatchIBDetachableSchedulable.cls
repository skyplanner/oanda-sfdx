/* Name: BatchIBDetachableSchedulable 
 * Description : removes the link to IB on fxaccounts of those all accounts deactivated.
 * Author: Sri 
 * Date : 2020-09-01
 */
public class BatchIBDetachableSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable 
{
    
    private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public static final Integer BATCH_SIZE = 200;
    
    public BatchIBDetachableSchedulable() 
    {
     /* //   execute below for  detaching the inactive IB from fxaccount
            BatchIBDetachableSchedulable b=  new BatchIBDetachableSchedulable ('Select Id from fxAccount__c where Introducing_Broker__c != null AND  Introducing_Broker__r.Is_an_Introducing_Broker__c  = true AND Introducing_Broker__r.IB_Status__c = \'Inactive\' ') ;
            System.schedule('BatchIBDetachableSchedulable', '0 30 22 * * ?', b);
        */
    }

    public BatchIBDetachableSchedulable(String q) 
    {
        query = q;
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'Select Id from fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> batch) 
    {
        List<fxAccount__c> fxAccs= (List<fxAccount__c>) batch;
        for(fxAccount__c var : fxAccs )
        {
            var.Introducing_Broker__c = null;
            var.Introducing_Broker_Number__c  = null;
        }
        if(fxAccs.size()>0)
        {
            try
            {
                update fxAccs;
            }
            catch(Exception e)
            {
                System.debug('Error :'+e.getMessage());
            }
        }
    }
    
    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchIBDetachableSchedulable (query), BATCH_SIZE);
    
    }

    public void finish(Database.BatchableContext BC) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

}