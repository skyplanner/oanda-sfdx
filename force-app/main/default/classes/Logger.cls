/**
 * Helper class for custom logs
 * Logs types
 * - Info
 * - Warning
 * - Debug
 * - Error
 * - Exception
 */
public class Logger {

    /*************** INFO ***************/

    /**
     * Log an info
     *  @param msg
     */
    public static void info(String msg) {
        new LogUtil().createLog(
            new Log(
                Log.Type.INFO,
                null,
                msg,
                null));
    }

    /**
     * Log an info
     *  @param msg
     *  @param category
     */
    public static void info(
        String msg,
        String category)
    {
        new LogUtil().createLog(
            new Log(
                Log.Type.INFO,
                category,
                msg,
                null));
    }

    /**
     * Log an info
     *  @param msg
     *  @param category
     *  @param details
     */
    public static void info(
        String msg,
        String category,
        Object details)
    {
        info(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an info
     *  @param msg
     *  @param category
     *  @param details SObject
     */
    public static void info(
        String msg,
        String category,
        SObject details)
    {
        info(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an info
     *  @param msg
     *  @param category
     *  @param details List<SObject>
     */
    public static void info(
        String msg,
        String category,
        List<SObject> details)
    {
        info(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an info
     *  @param msg
     *  @param category
     *  @param details
     */
    public static void info(
        String msg,
        String category,
        String details)
    {
        new LogUtil().createLog(
            new Log(    
                Log.Type.INFO,
                category,
                msg,
                details));
    }

    /*************** WARNING ***************/

    /**
     * Log a warning
     *  @param msg
     */
    public static void warning(String msg) {
        new LogUtil().createLog(
            new Log(
                Log.Type.WARNING,
                null,
                msg,
                null));
    }

    /**
     * Log a warning
     *  @param msg
     *  @param category
     */
    public static void warning(
        String msg,
        String category)
    {
        new LogUtil().createLog(
            new Log(
                Log.Type.WARNING,
                category,
                msg,
                null));
    }

    /**
     * Log a warning
     *  @param msg
     *  @param category
     *  @param details Object
     */
    public static void warning(
        String msg,
        String category,
        Object details)
    {
        warning(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a warning
     *  @param msg
     *  @param category
     *  @param details SObject
     */
    public static void warning(
        String msg,
        String category,
        SObject details)
    {
        warning(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a warning
     *  @param msg
     *  @param category
     *  @param details List<SObject>
     */
    public static void warning(
        String msg,
        String category,
        List<SObject> details)
    {
        warning(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a warning
     *  @param msg
     *  @param category
     *  @param details
     */
    public static void warning(
        String msg,
        String category,
        String details)
    {
        new LogUtil().createLog(
            new Log(    
                Log.Type.WARNING,
                category,
                msg,
                details));
    }

    /*************** DEBUG ***************/

     /**
     * Log a debug
     *  @param msg
     */
    public static void debug(String msg)
    {
        new LogUtil().createLog(
            new Log(
                Log.Type.DEBUG,
                null,
                msg,
                null));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     */
    public static void debug(
        String msg,
        String category)
    {
        new LogUtil().createLog(
            new Log(
                Log.Type.DEBUG,
                category,
                msg,
                null));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     *  @param details Object
     */
    public static void debug(
        String msg,
        String category,
        Object details)
    {
        debug(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     *  @param details SObject
     */
    public static void debug(
        String msg,
        String category,
        SObject details)
    {
        debug(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     *  @param details List<SObject>
     */
    public static void debug(
        String msg,
        String category,
        List<SObject> details)
    {
        debug(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     *  @param details
     */
    public static void debug(
        String msg,
        String category,
        String details)
    {
        new LogUtil().createLog(
            new Log(
                Log.Type.DEBUG,
                category,
                msg,
                details));
    }

    /**
     * Log a debug
     *  @param msg
     *  @param category
     *  @param request
     *  @param response
     */
    public static Log debug(
        String msg,
        String category,
        HttpRequest request,
        HttpResponse response)
    {
        LogUtil lUtil = new LogUtil();
        
        Log l = new Log(
            Log.Type.DEBUG,
            category,
            msg,
            lUtil.getDetails(request, response));
        
            lUtil.createLog(l);

        return l;
    }

    /*************** ERROR ***************/

    /**
     * Log an error
     *  @param msg
     */
    public static Log error(String msg)
    {
        Log l = new Log(
            Log.Type.ERROR,
            null,
            msg,
            null);

        new LogUtil().createLog(l);

        return l;
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     */
    public static Log error(
        String msg,
        String category)
    {
        Log l = new Log(
            Log.Type.ERROR,
            category,
            msg,
            null);

        new LogUtil().createLog(l);

        return l;
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     *  @param details Object
     */
    public static Log error(
        String msg,
        String category,
        Object details)
    {
        return error(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     *  @param details SObject
     */
    public static Log error(
        String msg,
        String category,
        SObject details)
    {
        return error(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     *  @param details List<SObject>
     */
    public static Log error(
        String msg,
        String category,
        List<SObject> details)
    {
        return error(
            msg,
            category,
            Utilities.serialize(
                details, true));
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     *  @param details
     */
    public static Log error(
        String msg,
        String category,
        String details)
    {
        Log l = new Log(
            Log.Type.ERROR,
            category,
            msg,
            details);
        
        new LogUtil().createLog(l);

        return l;
    }

    /**
     * Log an error
     *  @param msg
     *  @param category
     *  @param request
     *  @param response
     */
    public static Log error(
        String msg,
        String category,
        HttpRequest request,
        HttpResponse response)
    {
        LogUtil lUtil = new LogUtil();
        
        Log l = new Log(
            Log.Type.ERROR,
            category,
            msg,
            lUtil.getDetails(request, response));
        
            lUtil.createLog(l);

        return l;
    }

    /**
     * Log an error
     *  @param category
     *  @param saveResult
     */
    public static void error(
        String category,
        Database.SaveResult[] saveResults)
    {
        LogUtil lutil = new LogUtil();

        for(Database.SaveResult r :saveResults) {
            if(!r.isSuccess()) {
                for(Database.Error err :r.getErrors()) {
                    lutil.createLog(
                        new Log(
                            Log.Type.ERROR,
                            category,
                            String.valueOf(err.getStatusCode()),
                            lUtil.getDetails(r.getId(), err)));
                }
            }
        }
    }

    /**
     * Log an error
     *  @param category
     *  @param upsertResults
     */
    public static void error(
        String category,
        Database.UpsertResult[] upsertResults)
    {
        LogUtil lutil = new LogUtil();

        for(Database.UpsertResult r :upsertResults) {
            if(!r.isSuccess()) {
                for(Database.Error err :r.getErrors()) {
                    lutil.createLog(
                        new Log(
                            Log.Type.ERROR,
                            category,
                            String.valueOf(err.getStatusCode()),
                            lUtil.getDetails(r.getId(), err)));
                }
            }
        }
    }

    /**
     * Log an error
     *  @param category
     *  @param deleteResult
     */
    public static void error(
        String category,
        Database.DeleteResult[] delResults)
    {
        LogUtil lutil = new LogUtil();

        for(Database.DeleteResult r :delResults) {
            if(!r.isSuccess()) {
                for(Database.Error err :r.getErrors()) {
                    lutil.createLog(
                        new Log(
                            Log.Type.ERROR,
                            category,
                            String.valueOf(err.getStatusCode()),
                            lUtil.getDetails(r.getId(), err)));
                }
            }
        }
    }

    /*************** EXCEPTION ***************/

    /**
     * Log an exception
     *  @param category
     *  @param ex
     */
    public static Log exception(
        String category,
        Exception ex)
    {
       Log l = new Log(
            Log.Type.EXCEPT,
            category,
            ex.getMessage(),
            LogException.getDetailsMessage(ex));

        new LogUtil().createLog(l);
        return l;
    }

    /**
     * Log an exception
     *  @param msg
     *  @param category
     *  @param details
     */
    public static Log exception(
        String msg,    
        String category,
        String details)
    {
       Log l = new Log(
            Log.Type.EXCEPT,
            category,
            msg,
            details);

        new LogUtil().createLog(l);
        return l;
    }

    /**
     * Log an exception
     *  @param msg
     *  @param category
     *  @param request
     *  @param response
     */
    public static Log exception(
        String msg,
        String category,
        HttpRequest request,
        HttpResponse response)
    {
        LogUtil lUtil = new LogUtil();

        Log l = new Log(
            Log.Type.EXCEPT,
            category,
            msg,
            lUtil.getDetails(request, response));
        
        lUtil.createLog(l);

        return l;
    }

}