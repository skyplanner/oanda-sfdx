/**
 * @File Name          : NonHVCCase_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/11/2022, 7:42:38 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2021, 3:28:50 PM   acantero     Initial Version
**/
@IsTest
private without sharing class NonHVCCase_Test {

	@testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createTestLeads();
        SPCaseTriggerHandler.enabled = false; //i do not want routing logic here
        OmnichanelRoutingTestDataFactory.createTestCasesForLeads();
    }

    @IsTest
    static void afterUpdate() {
        Integer nonHvcCasesCount = 0;
        ID userId = null;
        ID caseId = null;
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0005');
        System.runAs(testUser) {
            userId = UserInfo.getUserId();
            System.debug('userId: ' + userId);
            //...
            caseId = [select Id from Case limit 1].Id;
            Non_HVC_Case__c nonHvcCase = new Non_HVC_Case__c(
                Case__c = caseId
            );
            insert nonHvcCase;
            //...
            nonHvcCase.Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED;
            update nonHvcCase;
            //...
            nonHvcCasesCount = [
                select Non_HVC_Cases__c 
                from Agent_Extra_Info__c
                where Agent_ID__c = :userId
                limit 1
            ].Non_HVC_Cases__c.intValue();
        }
        Test.stopTest();
        Case case1 = [
            select Routed_And_Assigned__c, OwnerId, Agent_Capacity_Status__c
            from Case 
            where Id = :caseId
        ];
        System.assert(case1.Routed_And_Assigned__c);
        System.assertEquals(userId, case1.OwnerId);
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_PARKED, 
            case1.Agent_Capacity_Status__c
        );
        System.assertEquals(1, nonHvcCasesCount);
    }

    @IsTest
    static void afterDelete() {
        Integer FAKE_NON_HVC_COUNT = 5;
        Integer nonHvcCasesCount = 0;
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0005');
        System.runAs(testUser) {
            String userId = UserInfo.getUserId();
            System.debug('userId: ' + userId);
            //...
            ID caseId = [select Id from Case limit 1].Id;
            Non_HVC_Case__c nonHvcCase = new Non_HVC_Case__c(
                Case__c = caseId,
                Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED
            );
            insert nonHvcCase;
            //...
            insert new Agent_Extra_Info__c(
                Agent_ID__c = userId,
                Non_HVC_Cases__c = FAKE_NON_HVC_COUNT
            );
            //...
            delete nonHvcCase;
            //...
            nonHvcCasesCount = [
                select Non_HVC_Cases__c 
                from Agent_Extra_Info__c
                where Agent_ID__c = :userId
                limit 1
            ].Non_HVC_Cases__c.intValue();
        }
        Test.stopTest();
        System.assertEquals(FAKE_NON_HVC_COUNT - 1, nonHvcCasesCount);
    }

    @IsTest
    static void exception1() {
        Boolean exceptionOk = false;
        ID caseId = [select Id from Case limit 1].Id;
        Non_HVC_Case__c nonHvcCase = new Non_HVC_Case__c(
            Case__c = caseId
        );
        insert nonHvcCase;
        Test.startTest();
        try {
            ExceptionTestUtil.prepareDummyException();
            //...
            nonHvcCase.Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED;
            update nonHvcCase;
            //...
        } catch (Exception ex) {
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    @IsTest
    static void exception2() {
        Boolean exceptionOk = false;
        ID caseId = [select Id from Case limit 1].Id;
        Non_HVC_Case__c nonHvcCase = new Non_HVC_Case__c(
            Case__c = caseId
        );
        insert nonHvcCase;
        Test.startTest();
        try {
            ExceptionTestUtil.exceptionInstance = 
                LogException.newInstance('abc', 'def');
            //...
            nonHvcCase.Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED;
            update nonHvcCase;
            //...
        } catch (Exception ex) {
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }
    
}