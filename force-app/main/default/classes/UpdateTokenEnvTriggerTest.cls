@IsTest
public class UpdateTokenEnvTriggerTest {
    @IsTest
    static void updateTokenAfterInsertPositiveTest(){
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://currentorg.url.com', 200, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            Database.SaveResult sr =  EventBus.publish(
                new Update_Token__e(
                    External_Credential_DeveloperName__c = 'Test_API',
                    Request_Body__c = JSON.serialize('Test Body'),
                    Is_Update_Credential__c = true
                )
            );

            Assert.isTrue(sr.isSuccess());
        Test.stopTest();
        Assert.isTrue([SELECT Id FROM Log__c].isEmpty());
    }

    @IsTest
    static void updateTokenAfterInsertNegativeTest(){
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://currentorg.url.com', 404, '', 'Error', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Assert.isTrue([SELECT Id FROM Log__c].isEmpty());
        Test.startTest();
            Database.SaveResult sr =  EventBus.publish(
                new Update_Token__e(
                    External_Credential_DeveloperName__c = 'Tas_API',
                    Request_Body__c = JSON.serialize('Test Body'),
                    Is_Update_Credential__c = false
                )
            );
            Assert.isTrue(sr.isSuccess());
        Test.stopTest();
    }
}