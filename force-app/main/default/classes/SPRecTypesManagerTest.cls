/**
 * @File Name          : SPRecTypesManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/26/2023, 3:16:00 AM
**/
@IsTest
private without sharing class SPRecTypesManagerTest {

	static final String MASTER_REC_TYPE = 'Master';

	// test 1 : sObjectInfo = null => return null
	// test 2 : recTypeDevName is blank => return null
	// test 3 : cacheResults = false => ok
	// test 4 : cacheResults = true, first search => ok
	// test 5 : cacheResults = true, second search => ok (return from cache)
	@IsTest
	static void getRecordTypeIdByDevName() {
		DescribeSObjectResult sObjectInfo = Schema.SObjectType.Account;

		Test.startTest();
		SPRecTypesManager instance1 = new SPRecTypesManager(
			false // cacheResults
		);
		// test 1
		ID result1 = instance1.getRecordTypeIdByDevName(
			null, // sObjectInfo
			MASTER_REC_TYPE // recTypeDevName
		);
		// test 2
		ID result2 = instance1.getRecordTypeIdByDevName(
			sObjectInfo, // sObjectInfo
			null // recTypeDevName
		);
		// test 3
		ID result3 = instance1.getRecordTypeIdByDevName(
			sObjectInfo, // sObjectInfo
			MASTER_REC_TYPE // recTypeDevName
		);
		// test 4
		SPRecTypesManager instance2 = new SPRecTypesManager(
			true // cacheResults
		);
		ID result4 = instance2.getRecordTypeIdByDevName(
			sObjectInfo, // sObjectInfo
			MASTER_REC_TYPE // recTypeDevName
		);
		// The call is repeated to obtain the result stored in the cache
		ID result5 = instance2.getRecordTypeIdByDevName(
			sObjectInfo, // sObjectInfo
			MASTER_REC_TYPE // recTypeDevName
		);
		Test.stopTest();

		System.assertEquals(null, result1, 'Invalid result');
		System.assertEquals(null, result2, 'Invalid result');
		System.assertNotEquals(null, result3, 'Invalid result');
		System.assertNotEquals(null, result4, 'Invalid result');
		System.assertNotEquals(null, result5, 'Invalid result');
	}    

	// test 1 : sObjectInfo = null => return empty list
	// test 2 : recTypeDevNames = null => return empty list
	// test 3 : recTypeDevNames is empty => return empty list
	// test 4 : cacheResults = false => ok
	// test 5 : cacheResults = true => ok
	@IsTest
	static void getRecordTypeIdsByDevNames() {
		DescribeSObjectResult sObjectInfo = Schema.SObjectType.Account;
		List<String> recTypeDevNames = new List<String> { MASTER_REC_TYPE };

		Test.startTest();
		SPRecTypesManager instance1 = new SPRecTypesManager(
			false // cacheResults
		);
		// test 1
		List<ID> result1 = instance1.getRecordTypeIdsByDevNames(
			null, // sObjectInfo
			recTypeDevNames // recTypeDevNames
		);
		// test 2
		List<ID> result2 = instance1.getRecordTypeIdsByDevNames(
			sObjectInfo, // sObjectInfo
			null // recTypeDevNames
		);
		// test 3
		List<ID> result3 = instance1.getRecordTypeIdsByDevNames(
			sObjectInfo, // sObjectInfo
			new List<String>() // recTypeDevNames
		);
		// test 4
		List<ID> result4 = instance1.getRecordTypeIdsByDevNames(
			sObjectInfo, // sObjectInfo
			recTypeDevNames // recTypeDevNames
		);
		// test 5
		SPRecTypesManager instance2 = new SPRecTypesManager(
			true // cacheResults
		);
		List<ID> result5 = instance2.getRecordTypeIdsByDevNames(
			sObjectInfo, // sObjectInfo
			recTypeDevNames // recTypeDevNames
		);
		Test.stopTest();

		System.assertEquals(true, result1.isEmpty(), 'Invalid result');
		System.assertEquals(true, result2.isEmpty(), 'Invalid result');
		System.assertEquals(true, result3.isEmpty(), 'Invalid result');
		System.assertEquals(false, result4.isEmpty(), 'Invalid result');
		System.assertEquals(false, result5.isEmpty(), 'Invalid result');
	}    
	
}