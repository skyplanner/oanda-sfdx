/**
 * @File Name          : SLAViolationsTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 3:42:21 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 10:57:39 AM   acantero     Initial Version
**/
@isTest
public without sharing class SLAViolationsTestDataFactory {

    public static String getMilestoneTypeName() {
        return getMilestoneTypeName(false);
    }

    public static String getMilestoneTypeName(Boolean triggeredByOwnerChange) {
        String result = [
            select Milestone_Type_Name__c 
            from Milestone_Time_Settings__mdt 
            where Triggered_by_owner_change__c = :triggeredByOwnerChange
            AND Violation_Type__c = 'AHT Exceeded' 
            AND Milestone_Type_Name__c like '%Tier%'
            Limit 1
        ].Milestone_Type_Name__c;
        return result;
    }

    public static SupervisorByRoles getMockSupervisorByRoles() {
        SupervisorByRoles result = 
            (SupervisorByRoles) Test.createStub(
                SupervisorByRoles.class, 
                new MockSupervisorProvider()
            );
        return result;
    }

    public static SupervisorByDivisions getMockSupervisorByDivisions() {
        SupervisorByDivisions result = 
            (SupervisorByDivisions) Test.createStub(
                SupervisorByRoles.class, 
                new MockSupervisorProvider()
            );
        return result;
    }

    public static void createTestChats() {
        //disable LiveChatTranscript Trigger
        SPLiveChatTranscriptTriggerHandler.enabled = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        insert chatList;
    }

    public static List<String> createTestChatsForHVCAccount(
        String division,
        String inquiryNature
    ) {
        //disable LiveChatTranscript Trigger
        SPLiveChatTranscriptTriggerHandler.enabled = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChatsForHVCAccount(
                division,
                inquiryNature
            );
        insert chatList;
        //..
        List<String> result = new List<String>();
        for(LiveChatTranscript chat : chatList) {
            result.add(chat.Id);
        }
        return result;
    }

    public static List<String> getChatIdList() {
        List<String> result = new List<String>();
        //...
        List<LiveChatTranscript> chatList = [
            select Id from LiveChatTranscript
        ];
        for(LiveChatTranscript chat : chatList) {
            result.add(chat.Id);
        }
        return result;
    }

    public static SLAChatViolationInfo getSLAChatViolationInfo(
        String chatId,
        String violationType
    ) {
        SLAChatViolationInfo result = 
            new SLAChatViolationInfo(
                chatId,
                'fake chat name',
                violationType
            );
        result.isPersistent = true;
        result.division = OmnichanelConst.OANDA_CANADA;
        result.divisionCode = OmnichanelConst.OANDA_CANADA_AB;
        result.inquiryNature = OmnichanelConst.INQUIRY_NAT_LOGIN;
        result.language = OmnichanelConst.ENGLISH_LANG;
        result.liveOrPractice = OmnichanelConst.PRACTICE;
        result.tier = OmnichannelCustomerConst.TIER_1;
        //...
        result.accountName = 'fake account name';
        return result;
    }

    public static SLAMilestoneViolationInfo getSLAMilestoneViolationInfo(
        String caseId,
        String milestoneTypeName
    ) {
        Milestone_Time_Settings__mdt milestoneSettings = 
            MilestoneUtils.getMilestoneSettings(
                milestoneTypeName, 
                true
            );
        SLAMilestoneViolationInfo result = 
            new SLAMilestoneViolationInfo(
                caseId,
                'fake Case Number',
                milestoneSettings
            );
        result.isPersistent = true;
        result.division = OmnichanelConst.OANDA_CANADA;
        result.inquiryNature = OmnichanelConst.INQUIRY_NAT_LOGIN;
        result.language = OmnichanelConst.ENGLISH_LANG;
        result.liveOrPractice = OmnichanelConst.PRACTICE;
        result.tier = OmnichannelCustomerConst.TIER_1;
        return result;
    }

}