/**
 * Callout util class
 */
public abstract class Callout {	
	// Methods
	public enum METHODS {
		GET,
		POST,
		PUT,
		PATCH,
		DEL
	}
	
	// Integration base url
	protected String baseUrl;
	// Request
	protected HttpRequest req;
	// Response
	protected HttpResponse resp;

	protected System.CalloutException exceptionDetails;

	// Default request timeout
	protected Integer timeout = 120000;
	// Default successful response status codes
	protected Set<Integer> successCodes = new Set<Integer> { 200 };

    /**
     * Method to do a GET request
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params
     */
    protected void get(
		String resource,
		Map<String, String> headers,
		Map<String, String> params)
	{
		call(
			METHODS.GET,
			resource,
			headers,
			params,
			null);
	}

    /**
     * Method to do a POST request
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params
     * @param:  body, 
     */
	protected void post(
		String resource,
		Map<String, String> headers,
		Map<String, String> params,
		String body)
	{
		call(
			METHODS.POST,
			resource,
			headers,
			params,
			body);
	}

    /**
     * Method to do a PUT request
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params
     * @param:  body, 
     */
	protected void put(
		String resource,
		Map<String, String> headers,
		Map<String, String> params,
		String body)
	{
		call(
			METHODS.PUT,
			resource,
			headers,
			params,
			body);
	}

    /**
     * Method to do a PATCH request
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params
     * @param:  body, 
     */
	protected void patch(
		String resource,
		Map<String, String> headers,
		Map<String, String> params,
		String body)
	{
		call(
			METHODS.PATCH,
			resource,
			headers,
			params,
			body);
	}

    /**
     * Method to do a DEL request
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params
     */
	protected void del(
		String resource,
		Map<String, String> headers,
		Map<String, String> params)
	{
		call(
			METHODS.DEL,
			resource,
			headers,
			params,
			null);
	}

    /**
     * Method to do a request
     * @param:  method, 
     * @param:  resource, 
     * @param:  headers, 
     * @param:  params, 
     * @param:  body, 
     */
	protected HttpResponse call(
		METHODS method,
		String resource,
		Map<String, String> headers,
		Map<String, String> params, 
		String body)
	{
		try 
		{
			req = new HttpRequest();

			PageReference pRef = 
				String.isBlank(resource) ?
					new PageReference(baseUrl) :
					new PageReference(baseUrl + resource);

			// Set url parameters
			if (params != null) {
				pRef.getParameters().putAll(params);
			}

			String endPoint = pRef.getUrl();
			
			req.setTimeout(timeout);
			req.setMethod(method.name());
			req.setEndpoint(endPoint);
			
			// Set headers
			if (headers != null) {
				for(String h :headers.keySet()) {
					req.setHeader(h, headers.get(h));    
				}
			}
			
			// Set body
			if (!String.isBlank(body)) {
				req.setBody(body);
			}
			
			// Send
			resp = new Http().send(req);
		}
		catch (System.CalloutException e) {
            exceptionDetails = e;
        } 
		
		return resp;
	}

	/**
	 * Know if the response is success
	 */
	protected Boolean isResponseCodeSuccess() {
		if (resp == null) {
			return false;
		}

		return successCodes.contains(resp.getStatusCode());
	}

	/**
	 * Set baseUrl from a named credential name
	 */
	protected void setBaseUrlFromNamedCredential(
		String namedCredentialDevName
	) {
		baseUrl = 'callout:' + namedCredentialDevName;
	}

	protected Object parseJsonToObject(
		Type returnType, Map<String, String> propertyMappings
	) {
		String jsonContent = resp.getBody();

		for (String baseName : propertyMappings.keySet())
			jsonContent = jsonContent.replaceAll(
				'"' + baseName + '"\\s*:',
				'"' + propertyMappings.get(baseName) + '":');

		return parseJsonToObject(jsonContent, returnType);
	}

	protected Object parseJsonToObject(String jsonContent, Type returnType) {
		Object o = returnType.newInstance();		

		JsonParser parser = Json.createParser(jsonContent);
		parser.nextToken();

		return parser.readValueAs(returnType);
	}

	protected Object parseJsonToObject(Type returnType) {
		return parseJsonToObject(resp.getBody(), returnType);
	}	

    protected Boolean notFound() {
        return resp != null && 
			resp.getStatusCode() == 404;
    }
}