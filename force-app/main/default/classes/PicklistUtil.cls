/**
 * @File Name          : PicklistUtil.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/5/2021, 11:49:04 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/14/2020   dmorales     Initial Version
**/
public without sharing class PicklistUtil {
    
    public static Map<Object,List<String>> getDependentPicklistValues(Schema.sObjectField dependToken)
    {
      Schema.DescribeFieldResult depend = dependToken.getDescribe();
      Schema.sObjectField controlToken = depend.getController();
    
      if ( controlToken == null ) return null;
     
      Schema.DescribeFieldResult control = controlToken.getDescribe();
      List<Schema.PicklistEntry> controlEntries =
       (   control.getType() == Schema.DisplayType.Boolean
         ?   null
         :   control.getPicklistValues()
       );
      
     
 
     String base64map = 
     'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
     Map<Object,List<String>> dependentPicklistValues = 
                                                 new Map<Object,List<String>>();
                                            
     for ( Schema.PicklistEntry entry : depend.getPicklistValues() ){
       // System.debug('Schema.PicklistEntry entry -->' + entry); 
       //  System.debug('validFor-->' + ((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(entry))).get( 'validFor' ));
      if ( entry.isActive() )
      {
        //base64chars have reference to controllers of entry 
        List<String> base64chars =
            String.valueOf
            (((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(entry)))
              .get( 'validFor' ))
              .split( '' );
        //if there is any entry that doesn't have any controller value will 
        //scape here
        if(base64chars.size() == 1)
          continue;
        Integer lenght = (controlEntries != null ? controlEntries.size() : 2);  
    

        for ( Integer index = 0; index < lenght ; index++ )
        {
            Object controlValue = (controlEntries == null?(Object)(index == 1)
            :(Object) (controlEntries[ index ].isActive() ? 
                controlEntries[ index ].getValue() : null)
            );
            Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
        
            if  (controlValue == null              
                ||(base64map.indexOf(base64chars[ bitIndex ]) & (1 << bitShift)) == 0) continue;

            if ( !dependentPicklistValues.containsKey( controlValue ) )
            {
              dependentPicklistValues.put( controlValue, new List<String>() );
            }
            dependentPicklistValues.get( controlValue ).add( entry.getValue() );       
        }
      }      
     
     }
     
      return dependentPicklistValues; 
    }

}