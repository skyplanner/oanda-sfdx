/**
 * Created by mcasella on 6/19/20.
 */

@IsTest
public with sharing class NF_InitializationTest {

    @IsTest
    static void getLanguages(){
        Map<String, String> results = NF_Initialization.buildContext();
        System.assert(results.size() > 0);
        System.assert(results.get('English') != null);
    }
}