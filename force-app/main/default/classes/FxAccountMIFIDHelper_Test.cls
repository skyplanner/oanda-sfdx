/**
 * @File Name          : FxAccountMIFIDHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   acantero     Initial Version
**/
@istest
private class FxAccountMIFIDHelper_Test {

    //invalid param values
    @isTest
    static void getExpValLastModifiedDate_test1() {
        Test.startTest();
        Datetime result = FxAccountMIFIDHelper.getExpValLastModifiedDate(null);
        Test.stopTest();
        System.assertEquals(null, result);
    }
    @isTest
    static void checkDivision() {
        Test.startTest();
        Boolean checkEmpty = FxAccountMIFIDHelper.checkDivision('');
        Boolean checkOandaEurope = FxAccountMIFIDHelper.checkDivision('OANDA Europe');
        Boolean checkOandaCanada = FxAccountMIFIDHelper.checkDivision('OANDA Canada');
        Test.stopTest();
        System.assertEquals(false, checkEmpty);
        System.assertEquals(true, checkOandaEurope);
        System.assertEquals(false, checkOandaCanada);
    }
}