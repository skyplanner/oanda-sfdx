/* Name: Lead_Before
* Description : Batch Class to create cases  for Onboarding eID/AMP follow up 
* Documentation: Please, use this document as a reference on how to run this batch
*   https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
* Author: Sahil Handa
* */
public class BatchOnboardingEID_AMPCases implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection ,database.stateful, Schedulable{
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static final Integer BATCH_SIZE = 200;
    public static final String CRON_NAME = 'BatchOnboardingEID_AMPCases';
	public static final String CRON_SCHEDULE = '0 0 9 * * ?';
    
    public static void schedule(String q) {
    	System.schedule(CRON_NAME, CRON_SCHEDULE, new BatchOnboardingEID_AMPCases(q));
  	}

    public void execute(SchedulableContext context) {
    	Database.executeBatch(new BatchOnboardingEID_AMPCases(query), BATCH_SIZE);
  	}
    public BatchOnboardingEID_AMPCases(String q) {
        query = q;
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id,Division_Name__c,EID_Pass_Count__c,Intermediary__c,Works_for_a_CFTC_NFA_member_org__c,Contact__c,Account__c,Account__r.Id  FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    
    public Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<fxAccount__c> scope){
        Group queue = [SELECT Id, Name FROM Group WHERE Type = 'queue' and DeveloperName = 'OC_eID_AMP_follow_ups'];

        List <Case> caseToBeInserted = new List<Case>();
        for(fxAccount__c fxAccount : scope){
            String sub;
            if(fxAccount.Intermediary__c  == true){
                sub = 'Intermediary';
            }else if(fxAccount.Works_for_a_CFTC_NFA_member_org__c == true){
                sub = 'CFTC Member/PEP';
            }else{
                sub = 'Financial/Employment details';
            }
            Case newCase = new Case(fxAccount__c = fxAccount.ID, 
                                    Subject = 'OANDA fxtrade account: Information required' ,
                                    Description = 'OANDA fxtrade account: Information required',
                                    Live_or_Practice__c = 'Live',
                                    Inquiry_Nature__c  = 'Registration',
                                    Type__c  = 'eID follow up',
                                    Subtype__c = sub,
                                    ownerId = queue.id,
                                    AccountId = fxAccount.Account__c,
                                    ContactId = fxAccount.Contact__c 
                                   );
            System.debug('Cases are');
            caseToBeInserted.add(newCase);
        }
        try{
            Database.insert(caseToBeInserted,false);
        }catch(Exception e){
            System.debug('Exception=>>>'+e.getMessage());
        }
    }
    public void finish(Database.BatchableContext BC){
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
}