/**
 * @File Name          : RelatedMessagingSessionsReader.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 2:14:33 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 2:06:59 PM   aniubo     Initial Version
 **/
public with sharing class RelatedMessagingSessionsReader implements IRelatedMessagingSessionsReader {

	public Integer getCount(
		Id currentSessionId,
		String filterFieldApiName,
		Set<Id> filterValues
	) {
		String queryTemplate =
			'SELECT  COUNT() FROM MessagingSession ' +
			'WHERE {0} IN ({1}) AND Id <> \'\'{2}\'\' ';

		String inQuery = convertToInQuery(filterValues);
		String query = String.format(
			queryTemplate,
			new List<String>{
				filterFieldApiName,
				inQuery,
				currentSessionId
			}
		);
		return Database.countQuery(query);
	}

	public List<MessagingSession> getRelatedMessagingSessions(
		Set<Id> messagingEndUserIds,
		List<String> fields,
		String filterFieldApiName,
		Integer pageSize,
		Integer pageNo
	) {
		String queryTemplate =
			'SELECT {0} FROM MessagingSession ' +
			'WHERE {4} IN ({1}) ' +
			'ORDER BY CreatedDate DESC ' +
			'LIMIT {2} OFFSET {3}';
		String fieldsString = String.join(fields, ',');
		String inQuery = convertToInQuery(messagingEndUserIds);
		String query = String.format(
			queryTemplate,
			new List<String>{
				fieldsString,
				inQuery,
				String.valueOf(pageSize),
				String.valueOf(pageNo * pageSize),
				filterFieldApiName
			}
		);
		return (List<MessagingSession>) Database.query(query);
	}

	public MessagingSessionRelatedEntity getMessagingRelatedEntity(
		Id messagingSessionId
	) {
		List<MessagingSession> messagingSessions = [
			SELECT Id, EndUserAccountId, LeadId
			FROM MessagingSession
			WHERE Id = :messagingSessionId
		];
		if (messagingSessions.isEmpty()) {
			return null;
		}
		MessagingSession messagingSession = messagingSessions[0];
		return this.getMessagingRelatedEntity(messagingSession);
	}

	public Set<Id> getMessagingEndUsers(Id accountId) {
		Set<Id> messagingEndUserIds = new Set<Id>();
		List<MessagingEndUser> messagingEndUsers = [
			SELECT Id
			FROM MessagingEndUser
			WHERE AccountId = :accountId
		];
		for (MessagingEndUser messagingEndUser : messagingEndUsers) {
			messagingEndUserIds.add(messagingEndUser.Id);
		}
		return messagingEndUserIds;
	}

	@testVisible
	private String convertToInQuery(Set<Id> filters) {
		String inQuery;
		String pattern = '\'\'{0}\'\'';
		for (Id filter : filters) {
			if (String.isBlank(inQuery)) {
				inQuery = String.format(pattern, new List<String>{ filter });
			} else {
				inQuery +=
					',' + String.format(pattern, new List<String>{ filter });
			}
		}
		return inQuery ?? '\'\'';
	}
	@testVisible
	private MessagingSessionRelatedEntity getMessagingRelatedEntity(
		MessagingSession messagingSession
	) {
		if (
			String.isBlank(messagingSession.EndUserAccountId) &&
			String.isBlank(messagingSession.LeadId)
		) {
			return null;
		}

		Id recordId = String.isBlank(messagingSession.EndUserAccountId)
			? messagingSession.LeadId
			: messagingSession.EndUserAccountId;
		return new MessagingSessionRelatedEntity(
			recordId,
			String.isNotBlank(messagingSession.EndUserAccountId)
		);
	}
}