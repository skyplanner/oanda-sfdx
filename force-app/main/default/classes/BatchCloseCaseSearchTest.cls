/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchCloseCaseSearchTest {
    @isTest
	static void updateCAcase() {
        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

        Case c = new Case(
            AccountId = accs[0].Id
        );
        insert c;

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada',
            Case__c = c.Id
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		Test.StartTest();

        BatchCloseCaseSearch b = new BatchCloseCaseSearch(' Id = \'' + search.Id + '\'');
        Database.executeBatch(b);

		Test.stopTest();

        c = [
			SELECT Status 
			FROM Case
			WHERE Id = :c.Id
		];

        System.assertEquals('Closed', c.Status);
	}
}