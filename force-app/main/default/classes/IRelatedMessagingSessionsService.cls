/**
 * @File Name          : IRelatedMessagingSessionsService.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/11/2024, 12:06:43 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 3:55:42 PM   aniubo     Initial Version
 **/
public interface IRelatedMessagingSessionsService {
	Integer getCount();

	List<MessagingSession> getRelatedMessagingSessions(
		List<String> fields,
		Integer pageSize,
		Integer pageNo
	);
}