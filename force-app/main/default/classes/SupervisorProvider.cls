/**
 * @File Name          : SupervisorProvider.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/3/2021, 1:20:10 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/2/2021, 2:14:29 PM   acantero     Initial Version
**/
public interface SupervisorProvider {

    void addCriteria(String criteria);

    void getSupervisors();

    Set<String> getSupervisors(String criteria);

    Boolean isEmpty();

}