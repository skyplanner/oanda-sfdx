/**
 * @File Name          : QueueRoutingConfigHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/12/2021, 12:19:39 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/11/2021, 2:41:46 PM   acantero     Initial Version
**/
public inherited sharing class QueueRoutingConfigHelper {

    public static QueueRoutingConfig getByDevName(
        String devName,
        Boolean required
     ) {
        if (String.isBlank(devName)) {
            throw new QueueRoutingConfigHException(
                'input param "devName" is blank'
            );
        }
        //else...
        List<QueueRoutingConfig> recordList = [
            SELECT
                RoutingModel,
                RoutingPriority,
                CapacityWeight
            FROM
                QueueRoutingConfig
            WHERE
                DeveloperName = :devName
            LIMIT 1
        ];
        if (!recordList.isEmpty()) {
            return recordList[0];
        }
        //else...
        checkRequired(
            required, 
            'QueueRoutingConfig not found, DeveloperName: ' + devName
        );
        return null;
    }

    public static void checkRequired(
        Boolean required, 
        String error
    ){
        if (required == true) {
            throw new QueueRoutingConfigHException(
                error
            );
        }
    }

    //**************************************************************** */

    public class QueueRoutingConfigHException extends Exception {
    }

}