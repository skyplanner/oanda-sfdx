/* Name: BatchUpdateCryptoFunnelStageTest
 * Description : Tests the BatchUpdateCryptoFunnelStage class
 * Author: Eugene
 * Created Date : 2022-01-31
 */
@isTest
public with sharing class BatchUpdateCryptoFunnelStageTest {
    @isTest
    static void updateCryptoFunnelStage() {
        Account acc = TestDataFactory.getPersonAccount(true);
		fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);

        fxAcc.Funnel_Stage__c = null;
        update fxAcc;

        System.assertEquals(null, fxAcc.Funnel_Stage__c);

        Test.StartTest();

        BatchUpdateCryptoFunnelStage b = new BatchUpdateCryptoFunnelStage(null);
        Id batchProcessId = Database.executeBatch(b);

        Test.stopTest();

        AsyncApexJob aaj = [
            SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
            FROM AsyncApexJob
            WHERE Id =: batchProcessId ];

            fxAcc = [SELECT Id, Funnel_Stage__c FROM fxAccount__c WHERE Id = :fxAcc.Id];

        System.assertEquals(true, aaj != null);
        System.assertEquals(1, aaj.TotalJobItems);
        System.assertEquals('Ready For Funding', fxAcc.Funnel_Stage__c);
    }

}