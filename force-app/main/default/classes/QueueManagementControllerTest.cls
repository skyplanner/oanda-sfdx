/**
 * Created by Agnieszka Kajda on 31/01/2024.
 */

@IsTest
public with sharing class QueueManagementControllerTest {
    @IsTest
    static void testQueueManagementMethods() {

        Boolean isError = false;
        Map<String,String> results = new Map<String,String>();
        Id userId = UserInfo.getUserId();
        Boolean isUserOptedOut = false;
        QueueManagementController.Queues queues = new QueueManagementController.Queues();

        Test.startTest();

        try {
            queues = QueueManagementController.getQueues(userId);
            QueueManagementController.createAddedAndDeletedMemberList(userId, new List<String>(), new List<String>(), new List<GroupMember>(), new List<GroupMember>());
            results = QueueManagementController.saveQueuesChanges(userId, new List<String>(), new List<String>());
            QueueManagementController.saveUserOptingOutChange(userId,true);
            isUserOptedOut = QueueManagementController.isUserOptedOut(userId);
        }catch (Exception e) {
            isError = true;
        }

        Test.stopTest();

        System.assertEquals(false, isError);
        System.assertEquals(false, results.isEmpty());
        System.assertEquals('Success', results.get('status'));
        System.assertEquals(true, queues.availableQueues.size()>0);
        System.assertEquals(true, queues.availableQueues.size()>0);
        System.assertEquals(true, queues.availableQueues.size()>0);
        System.assertEquals(true, isUserOptedOut);
    }
}