/*
 *  @author : Deepak Malkani.
 *  Created Date : Jan 30 2017
 *  Purpose : The purpose of this Email Handler is to service emails coming for onBoarding requirements.
 *
*/
global class OnBoardingEmailHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
                                                                Messaging.InboundEnvelope env) {
 
        // Create an Apex Email Service to process all inBound Emails originating for onBoarding.

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        EmailMessage[] newEmailMessage = new EmailMessage[0];
        List<Id> fxAccountIds = new List<Id>();
        Map<Id, Case> caseMap = new Map<Id, Case>();
        
        try{
            //Search for Accounts with the from Email Address
            List<Account> accntLst = [SELECT id,FirstName,LastName,Name,fxAccount__c,PersonEmail FROM Account WHERE PersonEmail =: email.fromAddress LIMIT 1];

            if(!accntLst.isEmpty()){
                fxAccountIds.add(accntLst[0].fxAccount__c);
            }
            else{//if nothing found, we search for Leads with the email address
                List<Lead> leadLst = [SELECT id, LastName, FirstName, Email, Name, fxAccount__c FROM Lead WHERE Email =: email.fromAddress AND RecordType.Name = 'Retail Lead House Account' LIMIT 1];
                if(!leadLst.isEmpty()){
                    fxAccountIds.add(leadLst[0].fxAccount__c);
                }
            }
            system.debug('=====> fxAccount Ids is '+fxAccountIds + 'and is it empty? '+fxAccountIds.isEmpty());
            if(!fxAccountIds.isEmpty() && fxAccountIds[0] != null){
                //we call the Case Util getOnBoardingCase method to create a new or re-open an existing case for the found fxAccount.
                caseMap = CaseUtil.getOnBoardingCaseByFxAccount(fxAccountIds, 'Email - Onboarding', 'Open', true);
            }
            else{
                //we create an orphan support case
                caseMap = CaseUtil.createorphanOnboardingCase('Email - Onboarding', 'Open', email.fromAddress, email.Subject, email.fromName);
            }

            //if no case is created or found, then stop here
            if(caseMap == null || caseMap.size() == 0){
                result.success = true;
                return result;
            }

            EmailMessage eMessage = new EmailMessage(FromAddress = email.fromAddress, Incoming = true, MessageDate = system.now(),
                                                    HTMLBody = email.htmlBody, Subject = email.Subject, FromName = email.fromName, 
                                                    TextBody = email.plainTextBody, ParentId = caseMap.values()[0].Id, ToAddress = String.valueOf(email.toAddresses).substring(1, String.valueOf(email.toAddresses).length() - 1));
            if(email.ccAddresses != null)
                eMessage.CcAddress = String.valueOf(email.ccAddresses).substring(1, String.valueOf(email.ccAddresses).length() - 1 );
            newEmailMessage.add(eMessage);
            insert newEmailMessage;
            
            List<Attachment> emailAttachments = new List<Attachment>();

            if(email.binaryAttachments != null && email.binaryAttachments.size() >0 && newEmailMessage.size() > 0){
                for(Integer i=0; i< email.binaryAttachments.size(); i++){
                    system.debug('=====> inside binary attachments loop');
                    Attachment attchObjBin = new Attachment();
                    attchObjBin.Name = email.binaryAttachments[i].filename;
                    attchObjBin.Body = email.binaryAttachments[i].body;
                    
                    emailAttachments.add(attchObjBin);
                }
            }
            
            if(email.textAttachments != null && email.textAttachments.size() >0 && newEmailMessage.size() > 0){
                for(Integer j=0; j< email.textAttachments.size(); j++){
                    system.debug('=====> inside text attachments loop');
                    Attachment attchObjText = new Attachment();
                    attchObjText.Name = email.textAttachments[j].filename;
                    attchObjText.Body = Blob.valueOf(email.textAttachments[j].body);
                    
                    emailAttachments.add(attchObjText);
                }
            }

            //create document for each attachment
            Map<Attachment, Document__c> docByAttachment = new Map<Attachment, Document__c>();
        
            for(Attachment att : emailAttachments){
                Document__c doc = new Document__c();
            
                doc.name = att.Name.left(80);
                doc.Case__c = caseMap.values()[0].Id;
                
                if(caseMap.values()[0].recordTypeId == RecordTypeUtil.getOnBoardingCaseTypeId()){
                    doc.Account__c = caseMap.values()[0].AccountId;
                    doc.Lead__c = caseMap.values()[0].Lead__c;
                    doc.fxAccount__c = caseMap.values()[0].fxAccount__c;
                }

                docByAttachment.put(att, doc);
            }
        
        
            if(docByAttachment.values().size() > 0){
                System.debug('Number of Documents: ' + docByAttachment.values().size());
            
                insert docByAttachment.values();
            }
        
            //attach the attachment to document
            for(Attachment att : emailAttachments){
                Document__c doc = docByAttachment.get(att);
            
                if(doc != null){
                   att.ParentId = doc.Id;
                }
            }
        
            if(emailAttachments.size() > 0){
                System.debug('Number of Attachments: ' + emailAttachments.size() );
            
                insert emailAttachments;
            }

        }
        catch(Exception e){
            system.debug('====> error '+e.getMessage());
        }
        finally {
            caseMap.clear();
            fxAccountIds.clear();
        }
        
        result.success = true;
        return result;
    }
}