/**
 * @File Name          : BaseSRoutingInfoManager.cls
 * @Description        : 
 * Base class for classes that implement the SRoutingInfoManager interface
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 2:29:32 AM
**/
public inherited sharing abstract class BaseSRoutingInfoManager 
	implements SRoutingInfoManager {

	final protected Map<ID, PendingServiceRouting> psrByWorkItemId;

	public BaseSRoutingInfoManager() {
		this.psrByWorkItemId = new Map<ID, PendingServiceRouting>();
	}

	public void registerPendingServiceRouting(
		PendingServiceRouting pendingRoutingObj
	) {
		registerPendingServiceRouting(
			pendingRoutingObj, // pendingRoutingObj
			pendingRoutingObj.WorkItemId // workItemId
		);
	}

	public void registerPendingServiceRouting(
		PendingServiceRouting pendingRoutingObj,
		ID workItemId
	) {
		psrByWorkItemId.put(workItemId, pendingRoutingObj);
	}

	public abstract List<BaseServiceRoutingInfo> getRoutingInfoList();
	
}