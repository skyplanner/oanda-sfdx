/**
 * @File Name          : MessagingLeadProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/11/2024, 11:49:33 PM
**/
public inherited sharing class MessagingLeadProcess {

	final MessagingSession session;

	public MessagingLeadProcess(MessagingSession session) {
		this.session = session;
	}

	public Lead createLeadFromMsgSession() {
		if (session == null) {
			return null;
		}
		// else...
		User userObj = UserRepo.getSummaryByName(Label.System_user);
		String leadRecordTypeDevName = 
			SPSettingsManager.getSettingOrDefault(
				MessagingConst.LEAD_RECORD_TYPE_DEV_NAME_SETTING, // settingName
				MessagingConst.STANDARD_LEAD_REC_TYPE_DEV_NAME // defaultValue
			);
		SPRecTypesManager recTypesManager = 
			new SPRecTypesManager(false); // cacheResults = false
		Lead leadObj = new Lead();
		leadObj.RecordTypeId = recTypesManager.getRecordTypeIdByDevName(
			Schema.SObjectType.Lead, // sObjectInfo
			leadRecordTypeDevName // recTypeDevName
		);
		leadObj.Email = session.User_Email__c;
		leadObj.LastName = session.User_Email__c; // just like NF does

		if (userObj != null) {
			leadObj.OwnerId = userObj.Id;
		}

		if (String.isNotBlank(session.Region__c)) {
			leadObj.Division_Name__c = 
				ServiceDivisionsManager.getInstance().getDivisionName(
					session.Region__c
				);
		}

		if (String.isNotBlank(session.Language_Code__c)) {
			leadObj.Language_Preference__c = 
				getLeadLanguage(session.Language_Code__c);
		}
		
		insert leadObj;
		return leadObj;
	}

	public String getLeadLanguage(String languageCode) {
		Set<String> languagesWithAlternativeName = new Set<String> {
			OmnichanelConst.CHINESE_SIMP_LANG_CODE
		};
		String result = getLeadLanguage(
			languageCode, // languageCode
			languagesWithAlternativeName
		);
		return result;
	}

	@TestVisible
	String getLeadLanguage(
		String languageCode,
		Set<String> languagesWithAlternativeName
	) {
		String altLanguage = null;
		String language = ServiceLanguagesManager.getLanguage(languageCode);

		if (
			(languagesWithAlternativeName != null) &&
			languagesWithAlternativeName.contains(languageCode)
		) {
			altLanguage = 
				ServiceLanguagesManager.getAlternativeName(languageCode);
		}

		String result = String.isNotBlank(altLanguage)
			? altLanguage
			: language;
		return result;
	}
	
}