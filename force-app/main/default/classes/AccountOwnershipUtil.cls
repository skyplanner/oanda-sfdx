/**
 * Util class to handle account ownership updates/processes
 */
public with sharing class AccountOwnershipUtil {
    
    // user to avoid processing
    final Set<Id> USER_AVOID_PROCESSING = new Set<Id>{
        UserUtil.getSystemUserId(),
        UserUtil.getRetentionUserId()};

    //Account to create 'Owner Changed' tasks 
    List<Account> accsToCreateTaks;

    /**
     * Constructor
     */
    public AccountOwnershipUtil() {
        accsToCreateTaks = new List<Account>();
    }

    /**
     * Verify if account owner was changed under
     * some other account conditions
     */
    public void verify(
        Account acc,
        Account oldAcc)
    {
        // Know if owner change and some other conditions
        if(isRightAccForNewTaskOwnerChanged(acc, oldAcc)) {
            accsToCreateTaks.add(acc);
        }

        // Add more here...
    }

    /**
     * Process when account owner changed
     * - Create task owner changed
     * ...
     */
    public void process() {
        TaskUtil.createTaskOwnerChanged(
            accsToCreateTaks, true);

        // Add more here...
    }

    /**
     * Validate some account conditions to know if it
     * is right create a new "Owner Changed" task
     */
    private Boolean isRightAccForNewTaskOwnerChanged (
        Account acc,
        Account oldAcc)
    {
        // Know if the owner changed
        Boolean isOwnerChanged =
            SObjectUtil.isRecordFieldsChanged(
                acc,
                oldAcc,
                'OwnerId');

        return
            isOwnerChanged &&
            AccountUtil.isPrimaryDivisionAsia(acc) &&
            AccountUtil.isTraded(acc) &&
            // avoid process particular users as owners
            !USER_AVOID_PROCESSING.contains(acc.OwnerId);
    }

}