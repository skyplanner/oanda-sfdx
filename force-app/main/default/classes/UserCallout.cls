/**
 * User API manager class 
 **/
public class UserCallout extends CryptoCallout {
    final static String JSON_USER_NAME_FILTER 
        = '{"wildcard": {"username": "*[USERNAME]*"}}';
    
    /**
     ** User API search endpoint, json filter sample:
     * {
     * "and": [
     *   {"not": {"term": {"name": "Test User"}}},
     *   {"or": [{
     *     "wildcard": {"ssn": "123*"}
     *   },{
     *     "range": {"birthdate": {"from": "2001-01-01"}}
     *   }]}
     * ]
     * }
    **/
    public UserApiSearchResponse search(UserSearchQuery searchQuery) {
        String jsonFilter = JSON_USER_NAME_FILTER.replace(
            '[USERNAME]', searchQuery.username);
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_SEARCH_ENDPOINT),
            new List<String> {
                searchQuery.env, 
                String.valueOf(searchQuery.pageSize),
                String.valueOf(searchQuery.pageNumber)}
        );

        post(resource, header, null, jsonFilter);

        if (!isResponseCodeSuccess())
            throw getError();

        return (UserApiSearchResponse) JSON.deserialize(
                resp.getBody(), UserApiSearchResponse.class);
    }

    public UserApiUserGetResponse getUser(EnvironmentIdentifier identifier) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_GET_USER_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);

        if (notFound())
            return null;

        if (!isResponseCodeSuccess())
            throw getError();
        
        UserApiUserGetResponse result = 
            (UserApiUserGetResponse) JSON.deserialize(
                resp.getBody(), UserApiUserGetResponse.class);

        if (result?.user != null)
            result.user.isLive = identifier.env == 'live';

        return result;
    }

    public void updateUser(EnvironmentIdentifier identifier,
        UserApiUserPatchRequest user
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_GET_USER_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, Json.serialize(user, true));

        if (!isResponseCodeSuccess())
            throw getError();
    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value.
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
    public void updateUserIntroducingBroker(String oneId,
        UserApiUserPatchRequest user
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_INTRODUCING_BROKER_ENDPOINT),
            new List<String> {oneId}
        );

        patch(resource, header, null, Json.serialize(user, true));


        if (!isResponseCodeSuccess())
            throw getError();
    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public void updateUser(EnvironmentIdentifier identifier, String body
//    ) {
//        String resource = String.format(
//            SPGeneralSettings.getInstance().getValue(
//                Constants.USER_API_GET_USER_ENDPOINT),
//            new List<String> {identifier.id, identifier.env}
//        );
//
//        patch(resource, header, null, body);
//
//        if (!isResponseCodeSuccess())
//            throw getError();
//    }

    public void updateAddress(EnvironmentIdentifier identifier,
        UserApiUserPatchRequest user
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_UPD_ADDRESS_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, Json.serialize(user, true));

        if (!isResponseCodeSuccess())
            throw getError();
    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value.
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public void updateAddress(EnvironmentIdentifier identifier, String body) {
//        String resource = String.format(
//            SPGeneralSettings.getInstance().getValue(
//                Constants.USER_API_UPD_ADDRESS_ENDPOINT),
//            new List<String> {identifier.id, identifier.env}
//        );
//
//        patch(resource, header, null, body);
//
//        if (!isResponseCodeSuccess())
//            throw getError();
//    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value.
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public void updateContactPhone(EnvironmentIdentifier identifier, String body) {
//        String resource = String.format(
//            SPGeneralSettings.getInstance().getValue(
//                Constants.USER_API_CONTACT_PHONE_ENDPOINT),
//            new List<String> {identifier.id, identifier.env}
//        );
//
//        patch(resource, header, null, body);
//
//        if (!isResponseCodeSuccess())
//            throw getError();
//    }

    public void updateStatus(
    	EnvironmentIdentifier identifier, UserApiStatus status
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_STATUS_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, Json.serialize(status, true));

        if (!isResponseCodeSuccess())
            throw getError();
    }

    public void updateUserPrivileges(EnvironmentIdentifier identifier,
        UserApiUserPrivilegePatchRequest privilege
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_PRIVILEGE_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );
    
        patch(resource, header, null, Json.serialize(privilege, true));
    
        if (!isResponseCodeSuccess())
            throw getError();
    }
    

    public UserApiAuditLogsGetResponse getAuditLogs(
        EnvironmentIdentifier identifier
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_GET_AUDIT_LOG_ENDPOINT),

            new List<String> {identifier.id, identifier.env}
        );

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);

        if (notFound())
            return null;

        if (!isResponseCodeSuccess())
            throw getError();
        
        UserApiAuditLogsGetResponse result = 
            (UserApiAuditLogsGetResponse) JSON.deserialize(
                resp.getBody().replace('datetime', 'createdOn'), 
                UserApiAuditLogsGetResponse.class);

        return result;
    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public String postOneIdByEmail(String email) {
//        String uuid;
//        String resource = String.format(
//                CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),
//                new List<String> { email + '?skip_fxdb=true' }
//        );
//
//        post(resource, header, null, '{}');
//
//        if (isResponseCodeSuccess()) {
//            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
//            uuid = (String) results?.get('uuid');
//        }
//        return uuid;
//    }

    //The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
    //Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value
    //SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public void updateGroupRecalculateUserApi(EnvironmentIdentifier identifier, String body) {
//        String resource = String.format(
//                SPGeneralSettings.getInstance().getValue(
//                        Constants.USER_API_GROUP_RECALCULATE_ENDPOINT),
//                new List<String> {identifier.id, identifier.env}
//        );
//
//        patch(resource, header, null, body);
//
//        if (!isResponseCodeSuccess())
//            throw getError();
//    }

    Exception getError() {
        if (exceptionDetails != null)
            return exceptionDetails;
        else 
            return new ApplicationException(
                resp.getStatus() + ' ' + resp.getBody());
    }

    public class UserSearchQuery {
        public String username { get; set; }

        public String env { get; set; }

        public Integer pageSize { get; set; }

        public Integer pageNumber { get; set; }

        public String orderBy { get; set; }

        public Boolean ascending { get; set; }
    }
}