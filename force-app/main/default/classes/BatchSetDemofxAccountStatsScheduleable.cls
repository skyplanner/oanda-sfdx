public with sharing class BatchSetDemofxAccountStatsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable{
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private String setting;
	
	public static final Integer BATCH_SIZE = 200;
	public DateTime startDateTime = DateTime.now().addDays(-1); //default one day ago
	
	public static final String CRON_NAME = 'BatchSetDemofxAccountStatsScheduleable';
    
    
    public BatchSetDemofxAccountStatsScheduleable(DateTime start){
    	if(start != null){
    		startDateTime = start;
    	}
    	loadConfig('Default');
    	
    	query = 'select id, Account__c, Lead__c, CreatedDate, LastModifiedDate from fxAccount__c where RecordTypeId = \'' + RecordTypeUtil.getFxAccountPracticeId() + '\' and LastModifiedDate >= ' + SoqlUtil.getSoql(startDateTime);
    }
    
    
    public BatchSetDemofxAccountStatsScheduleable(String q){
    	loadConfig('Default');
    	query = q;
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'select id, Account__c, Lead__c, CreatedDate, LastModifiedDate from fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<fxAccount__c> fxAccounts = (List<fxAccount__c>)batch;
		
		fxAccountStatsUtil.loadConfig(setting);
		fxAccountStatsUtil.processPracticeStats(fxAccounts);
	}
	
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
	
	public void execute(SchedulableContext context) {
		executeBatch(null);
	}
	
	public static Id executeBatch(Integer batchSize, DateTime start) {
		return Database.executeBatch(new BatchSetDemofxAccountStatsScheduleable(start), batchSize);
	}
	
	public static Id executeBatch(DateTime start) {
		return executeBatch(BATCH_SIZE, start);
	}
	
	public void loadConfig(String settingName){
		setting = settingName;
	}
	
}