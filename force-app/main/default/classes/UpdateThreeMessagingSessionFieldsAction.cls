/**
 * @File Name          : UpdateThreeMessagingSessionFieldsAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/6/2023, 5:45:33 AM
**/
public without sharing class UpdateThreeMessagingSessionFieldsAction { 

	@InvocableMethod(label='Set Three Messaging Session Fields' callout=false)
	public static void setThreeMessagingSessionFields(List<UpdateInfo> updateInfoList) {    
		new MessagingSessionUpdateManager().processUpdate(updateInfoList);
	}

	// *******************************************************************

	public class UpdateInfo implements FieldValueSource {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'fieldName1' required = true)
		public String fieldName1;

		@InvocableVariable(label = 'fieldValue1' required = false)
		public String fieldValue1;

		@InvocableVariable(label = 'fieldName2' required = true)
		public String fieldName2;

		@InvocableVariable(label = 'fieldValue2' required = false)
		public String fieldValue2;

		@InvocableVariable(label = 'fieldName3' required = true)
		public String fieldName3;

		@InvocableVariable(label = 'fieldValue3' required = false)
		public String fieldValue3;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			Map<String, Object> result = new Map<String, Object>();
			result.put(fieldName1, fieldValue1);
			result.put(fieldName2, fieldValue2);
			result.put(fieldName3, fieldValue3);
			return result;
		}

	}

}