/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class ClassExtension implements Callable {
    Decimal getFuzzinessValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        Decimal fuzziness = details?.filters?.fuzziness;
        if (fuzziness != null) {
            return fuzziness * 100;
        }
        return fuzziness;
    }

    String getBirthYearValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        return String.valueOf(details?.filters?.birth_year);
    }

    Integer getTotalHitsValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        return details?.total_hits;
    }

    String getSearchProfileValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        return details?.search_profile?.slug;
    }

    String getCountryValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        List<String> countryCodes = details?.filters?.country_codes;

        if (countryCodes?.size() > 0) {
            for (String code : countryCodes) {
                Country_Setting__c setting = CustomSettings.getCountrySettingByCode(code.toLowerCase());
                String country = setting?.name;

                if (String.isNotBlank(country) && country == search?.fxAccount__r?.Country__c) {
                    return country;
                }
            
            }
        }

        return null;
    }

    String getNationalityValue(CASearchDetails details, Comply_Advantage_Search__c search) {
        List<String> countryCodes = details?.filters?.country_codes;

        if (countryCodes?.size() > 0) {
            for (String code : countryCodes) {
                Country_Setting__c setting = CustomSettings.getCountrySettingByCode(code.toLowerCase());
                String country = setting?.name;

                if (String.isNotBlank(country) && country == search?.fxAccount__r?.Country__c) {
                    return search.fxAccount__r.Citizenship_Nationality__c;
                }
            
            }
        }
        
        return null;
    }

    public Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'fuzziness' {
                return this.getFuzzinessValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when 'birthYear' {
                return this.getBirthYearValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when 'totalHits' {
                return this.getTotalHitsValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when 'searchProfile' {
                return this.getSearchProfileValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when 'country' {
                return this.getCountryValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when 'nationality' {
                return this.getNationalityValue(
                    (CASearchDetails) args.get('details'),
                    (Comply_Advantage_Search__c) args.get('search')
                );
            }
            when else {
                throw new ExtensionMalformedCallException(
                    'Method not implemented'
                );
            }
        }
    }

    public class ExtensionMalformedCallException extends Exception {
    }
}