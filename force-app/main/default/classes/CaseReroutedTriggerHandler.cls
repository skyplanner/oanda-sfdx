/**
 * @File Name          : CaseReroutedTriggerHandler.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/16/2022, 4:31:09 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/10/2022, 12:46:17 PM   acantero     Initial Version
**/
public without sharing class CaseReroutedTriggerHandler {

    //********************************************************

    public static final String OBJ_API_NAME = 'Case_Rerouted__e';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //********************************************************

    final List<Case_Rerouted__e> newList;

    public CaseReroutedTriggerHandler(
        List<Case_Rerouted__e> newList
    ) {
        this.newList = newList;
    }

    //********************************************************

    public void handleAfterInsert() {
        try {
            ExceptionTestUtil.execute();
            Set<ID> caseIdSet = new Set<ID>();
            for(Case_Rerouted__e event : newList) {
                caseIdSet.add(event.Case_ID__c);
            }
            CaseReroutingManager.getInstance().rerouteCasesUsingSkills(caseIdSet);
            //....
        } catch (LogException lex) {
            throw lex;
            //...
        } catch (Exception ex) {
            throw LogException.newInstance(
                CaseReroutedTriggerHandler.class.getName(),
                ex
            );
        }
    }
    
}