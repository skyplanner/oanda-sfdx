/**
 * @File Name          : BaseServiceRoutingInfo.cls
 * @Description        : 
 * Stores the information necessary to perform custom routing processes
 * (Base class)
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 1:57:03 AM
**/
public inherited sharing virtual class BaseServiceRoutingInfo {

	public final PendingServiceRouting routingObj;
	public String sourceName;
	public SObject workItem {get; set;}

	public BaseServiceRoutingInfo(
		PendingServiceRouting psrObj, 
		String sourceName,
		SObject workItem
	) {
		this.routingObj = psrObj;
		this.sourceName = sourceName;
		this.workItem = workItem;
	}
	
}