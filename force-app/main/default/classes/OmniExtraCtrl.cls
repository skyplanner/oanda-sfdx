/**
 * @File Name          : OmniExtraCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 12:39:21 PM
**/
public without sharing class OmniExtraCtrl {

    @AuraEnabled
    public static OmniExtraInfo getOmniExtraInfo() {
        OmniExtraInfo result = new OmniExtraInfo();
        OmnichanelSettings omniSettings = OmnichanelSettings.getRequiredDefault();
        result.maxPushAttempts = omniSettings.maxPushAttempts;
        result.pushTimeout = omniSettings.pushTimeout;
        if (result.pushTimeout > 0) {
            result.servPresenceStatusIdList = SPChatUtil.getServPresenceStatusIdList(
                omniSettings.chatChannelDevName
            );
        }
        result.noHvcParkPresentStatuses = omniSettings.noHvcParkPresentStatuses != null ?
                omniSettings.noHvcParkPresentStatuses :
                new List<String>();
        return result;
    }

    @AuraEnabled 
    public static void finishPendingCaseClosing() {
        String userId = UserInfo.getUserId();
        AgentCapacityHelper.finishPendingCaseClosing(userId);
    }

    @AuraEnabled
    public static List<AgentCapacityHelper.ChatWorkClosingInfo> getPendingChatWorks() {
        String userId = UserInfo.getUserId();
        return AgentCapacityHelper.getPendingChatWorks(userId);
    }

    @AuraEnabled
    public static void onAgentLogin(Decimal capacity) {
        String agentId = UserInfo.getUserId();
        Set<String> skills = SPChatUtil.getAgentActiveSkills(agentId);
        String skillsSummary = ServiceSkillManager.getSkillsSummary(skills);
        AgentInfoManager.updateCurrentAgentInfo(capacity, skillsSummary);
        //...
        new AgentExtraInfoManager(agentId).prepareAgentExtraInfo();
    }

    @AuraEnabled
    public static void updateAgentCapacity(Decimal capacity) {
        AgentInfoManager.updateCurrentAgentInfo(capacity, null);
    }

    @AuraEnabled
    public static void deleteCurrentAgentInfo() {
        AgentInfoManager.deleteCurrentAgentInfo();
    }

    @AuraEnabled
    public static PushAttemptInfo workCanBeDeclined(
        String workId,
        List<String> servicePresenceStatusIdList
    ) {
        if (
            String.isBlank(workId) ||
            (servicePresenceStatusIdList == null) ||
            servicePresenceStatusIdList.isEmpty()
        ) {
            return null;
        }
        //else...
        AgentWorkWrapper agentWorkW = new AgentWorkWrapper(workId);
        AgentBySkills agentsManager = new AgentBySkills(servicePresenceStatusIdList);
        String currentAgentId = UserInfo.getUserId();
        return workCanBeDeclined(
            agentWorkW, 
            agentsManager,
            currentAgentId
        );
    }

    @AuraEnabled
    public static void registerDeclinedChat(String chatId) {
        String agentId = UserInfo.getUserId();
        SLAViolationNotifiableFactory.SLAViolationNotifiableFactory(
            SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT,
            SLAConst.SLAViolationType.BOUNCED,
            new ChatSLAViolationNotifierDataReader(),
            agentId)
            ?.notifyViolation(
                new ChatSLAViolationNotificationData(new List<Id>{chatId})
            );
    }

    @AuraEnabled
    public static void freeCaseCapacity(
        String caseId
    ) {
        ChatCaseCapacityTimerCtrl.freeCaseCapacity(
            caseId,
            null
        );
    }

    @testVisible
    static PushAttemptInfo workCanBeDeclined(
        AgentWorkWrapper agentWorkW,
        AgentBySkills agentsManager,
        String currentAgentId
    ) {
        PushAttemptInfo result = new PushAttemptInfo();
        if (!agentWorkW.isAssigned()) {
            result.workIsValid = false;
            return result;
        }
        //else...
        result.workIsValid = true;
        String agentWorkSkillsSummary = agentWorkW.getSkillsSummary();
        result.canBeDeclined = agentsManager.existsCompatibleAgent(
            agentWorkSkillsSummary, 
            agentWorkW.capacityWeight, 
            currentAgentId
        );
        return result;
    }
    
    /**
	 * Method used for those Non-Hvc cases that are not parked and that are 
	 * assigned to the agent (and can be opened by the agent)
	 */
	@AuraEnabled
	public static Boolean registerAssignedNonHvcCase(ID nonHvcCaseId) {
		try {
			ExceptionTestUtil.execute();
			Set<ID> nonHvcCaseIdSet = new Set<ID> { nonHvcCaseId };
			return OnNonHvcCaseAssignedCmd.processNonHvcCases(nonHvcCaseIdSet);

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				OmniExtraCtrl.class.getName(),
				ex
			);
		}
		// } catch (Exception e) {
		// 	throw new AuraHandledException(e.getMessage());
		// }
	}

    @AuraEnabled
    public static NonHvcCaseAttemptInfo onNewNonHvcCase(
        String currentStatus
    ){
        return OmniNonHvcCaseCtrl.onNewNonHvcCase(currentStatus);
    }

    @AuraEnabled
    public static String onNonHvcCaseReleased(
        String currentStatus
    ){
        return OmniNonHvcCaseCtrl.onNonHvcCaseReleased(currentStatus);
    }

    @AuraEnabled
    public static void declineNonHvcCases(
        List<String> nonHvcCaseIdList
    ){
        OmniNonHvcCaseCtrl.declineNonHvcCases(nonHvcCaseIdList);
    }

    public class OmniExtraInfo {

        @AuraEnabled
        public Integer pushTimeout {get; set;}
        @AuraEnabled
        public Integer maxPushAttempts {get; set;}
        @AuraEnabled
        public List<String> servPresenceStatusIdList {get; set;}
        @AuraEnabled
        public List<String> noHvcParkPresentStatuses{get; set;}
        
    }

    public class PushAttemptInfo {

        @AuraEnabled
        public Boolean workIsValid {get; set;}
        @AuraEnabled
        public Boolean canBeDeclined {get; set;}
        
    }

}