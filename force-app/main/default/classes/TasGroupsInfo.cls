/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 01-03-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasGroupsInfo {
    public List<TasBaseGroup> commission_groups {get; set;}

    public List<TasBaseGroup> currency_conversion_groups {get; set;}

    public List<TasDivisionTradingGroup> division_trading_groups {get; set;}

    public List<TasBaseGroup> financing_groups {get; set;}

    public List<TasBaseGroup> pricing_groups {get; set;}
}