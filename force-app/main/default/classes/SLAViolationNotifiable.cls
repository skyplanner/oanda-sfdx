/**
 * @File Name          : SLAViolationNotifiable.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 1:57:39 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/16/2024, 3:50:23 PM   aniubo     Initial Version
**/
public interface SLAViolationNotifiable {
	void notifyViolation(SLAViolationNotificationData notificationData);
}