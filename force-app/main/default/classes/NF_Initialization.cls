/**
 * Created by mcasella on 6/19/20.
 */

global with sharing class NF_Initialization implements nfchat.AIProcessor {

    public static Map<String, String> buildContext(){

        Map<String, String> resultMap = new Map<String, String>();
        try{
            Chat_button_by_skill__mdt[] chatButtons = [SELECT Id, Skill__c, Button_Id__c
                                                       FROM Chat_button_by_skill__mdt];

            for(Chat_button_by_skill__mdt button : chatButtons){
                resultMap.put(button.Skill__c, button.Button_Id__c);
            }
        }catch(Exception ex){
            System.debug('>> Exception in NF_Initialization >  initialize : ' + ex.getMessage());
        }
        return resultMap;
    }
}