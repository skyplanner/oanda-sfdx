/*
 *	Author : Deepak Malkani.
 *	Created Date : July 11 2016
 *	Purpose : Common Utility Class used to perform RESTFul Callouts to any external services.
 *	Before invoking this class, make sure the end point is registered under Setup | Remote Site Settings 
 *
 *	Modified Date : Aug 31 2016
 *	Modification Reason : Modified the signature of RESTCallOutUtility to re-use the class for the newly added attribute params for Optimove
 *  Added provisioning for 5 input query params in the RESTCallout Utility Class.
 *
 *	Modified Date : Sep 1 2016
 *	Modification Reason : A lot of code re-factoring was done here. Repetative and unnecessary code has been deleted and moved to handler/helper classes 
*/


public with sharing class RESTCallOutUtility {

	public static String performRESTCallOut(String endPoint, String userName, String password, String callOutName, String authToken, String contentType, String RESTMethod, String JSONPOSTBody, String startDate, String endDate, Integer stop, Integer skip, String param1, String param2, String param3, String param4, String param5){

		HTTP h = new HTTP();
		HttpRequest request = new HttpRequest();
		HttpResponse response = new HttpResponse();
		String responseJSONBody;
		String ePoint;

		//Pattern used to dynamically change the end point URLs based on the callOutNames. This is used so that the same REST CalloutUtility can be re-used for multiple call out types
		if(callOutName == 'OptimoveAPILogin'){
			epoint = endPoint+'/general/login';
			JSONGenerator jsonMess = JSON.createGenerator(true);
			jsonMess.writeStartObject(); // this will produce a {
			jsonMess.writeStringField('Username', userName); //key value pair for username
			jsonMess.writeStringField('Password', password); //key value pair for password
			jsonMess.writeEndObject(); //this will produce a }
			request.setBody(jsonMess.getAsString());
		}
		else if(callOutName == 'OptimoveLastDate'){
			ePoint = endPoint+'/general/GetLastDataUpdate';
			request.setHeader('Authorization-Token', authToken.replace('\"', ''));
		}
		
		else if(callOutName == 'OptimoveCustChangers'){
			
			ePoint = endPoint+'/model/GetMicrosegmentChangers?startDate='+startDate+'&endDate='+endDate+'&$top='+stop+'&$skip='+skip+'&customerAttribute='+param1+'&CustomerAttributesDelimiter='+param2;
			request.setHeader('Authorization-Token', authToken.replace('\"', ''));
		}

		else if(calloutName == 'OptimoveMicroSSegList'){
			ePoint = endPoint+'/model/GetMicroSegmentList';
			request.setHeader('Authorization-Token', authToken.replace('\"', ''));
		}

		request.setEndpoint(ePoint);
		request.setHeader('Content-Type', contentType);
		request.setHeader('Accept', contentType);
		request.setMethod(RESTMethod);
		//Increased the Salesforce Timeout
		request.setTimeout(120000);
		//Make the call out
		try{
			//Check for Call out Limits -- in a batch total call out limit is 100 and we are just making 2 at a time
			if(Limits.getLimitCallouts() - Limits.getCallouts() > 2){
				response = h.send(request);
				responseJSONBody = response.getBody();
			}
			else{
				//Limits can be breached soon so register it in the Logs and dont process further
				//No point on retrying as this is a Gvernor Limit Exception
				responseJSONBody = 'null'+'|'+0+'|'+'LIMITS : Total Call Outs : '+Limits.getLimitCallouts()+' and Limits used so far : '+Limits.getCallouts()+'. Putting a stop at any more call outs since governor limits might get hit';
			}

			/* GENERATE A COMMON RESPONSE FOR OPTIMOVE EXCEPTIONS */

			//Callout exception handles when the service is down, however if the service is not down, but gives status code as 500, 400, 401 or 405, we capture it here
			if(response.getStatusCode() == 500 || response.getStatusCode() == 400 || response.getStatusCode() == 405)
				responseJSONBody = 'null'+'|'+response.getStatusCode()+'|'+'OPTIMOVE EXCEPTION : ERROR : '+response.getBody();
		}

		//Salesforce Call out Exception Block.
		catch(CalloutException callEx){

			/* FOR OPTIMOVE CALL OUTS ONLY. Generate JSON Response with common pattern for Salesforce Exceptions */

			if(calloutName == 'OptimoveAPILogin' || callOutName == 'OptimoveLastDate' || callOutName == 'OptimoveCustChangers' || callOutName == 'OptimoveMicroSSegList')
				responseJSONBody = 'null'+'|'+response.getStatusCode()+'|'+'SALESFORCE EXCEPTION : ERROR : '+callEx.getMessage();
		}

		return responseJSONBody;
	}

}