/**
 * Custom Lookup componenent to be used in any custom form.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0
 * @since 8/6/2021
 */
@isTest
private class LookupCtrlTest {

	/**
	 * public static List<SObject> doSearch(
	 *		String keyword, 
	 *		String objectName,
	 *		List<String> searchInFields)
	 */
	@isTest
	static void doSearch() {
		List<Account> accs;
		List<SObject> results;

		// we insert some accounts
		accs = new List<Account> {
			new Account(
				FirstName = 'First1',
				LastName='Last1',
				PersonEmail = 'test1@oanda.com',
				Account_Status__c = 'Active',
				PersonMailingCountry = 'Canada',
				RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
			),
			new Account(
				FirstName = 'First2',
				LastName='Last2',
				PersonEmail = 'test2@oanda.com',
				Account_Status__c = 'Active',
				PersonMailingCountry = 'Canada',
				RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
			),
			new Account(
				FirstName = 'First3',
				LastName='Last3',
				PersonEmail = 'test3@oanda.com',
				Account_Status__c = 'Active',
				PersonMailingCountry = 'Canada',
				RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
			),
			new Account(
				FirstName = 'First4',
				LastName='Last4',
				PersonEmail = 'test4@oanda.com',
				Account_Status__c = 'Active',
				PersonMailingCountry = 'Canada',
				RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
			),
			new Account(
				FirstName = 'First5',
				LastName='Last5',
				PersonEmail = 'test5@oanda.com',
				Account_Status__c = 'Active',
				PersonMailingCountry = 'Canada',
				RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
			)
		};
		insert accs;

		results = LookupCtrl.doSearch('First',
			'Account', new List<String> { 'FirstName' });
		System.assertEquals(5, results.size());

		results = LookupCtrl.doSearch('Last',
			'Account', new List<String> { 'LastName' });
		System.assertEquals(5, results.size());

		results = LookupCtrl.doSearch('test5@oanda.com',
			'Account', new List<String> { 'PersonEmail' });
		System.assertEquals(1, results.size());

		results = LookupCtrl.doSearch('XXX',
			'Account', new List<String> { 'Account_Status__c' });
		System.assertEquals(0, results.size());
	}

	/**
	 * public static SObject getCurrentRecord(
	 *		String recordId, 
	 *		String objectName,
	 *		List<String> searchInFields)
	 */
	@isTest
	static void getCurrentRecord() {
		Account acc;
		SObject record;

		acc = new Account(
			FirstName = 'First1',
			LastName='Last1',
			PersonEmail = 'test1@oanda.com',
			Account_Status__c = 'Active',
			PersonMailingCountry = 'Canada',
			RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
		);
		insert acc;

		record = LookupCtrl.getCurrentRecord(acc.Id,
			'Account', new List<String> { 'Name' });
		System.assertEquals('First1 Last1', ((Account)record).Name);
	}
}