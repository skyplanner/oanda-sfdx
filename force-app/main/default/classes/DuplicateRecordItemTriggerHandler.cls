/**
 * This class implements the logic corresponding to the trigger DuplicateRecordItemTrigger
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   25/02/2021
 */
public without sharing class DuplicateRecordItemTriggerHandler {

    public static final String ACCOUNT_SOBJECT_TYPE = 'Account';
    public static final String PERSON_ACCOUNT_NAME = 'PersonAccount';
    public static final String DIVISION_NAME_OGM = 'OANDA Global Markets';

    /**
     * This method handles the After Insert logic for the trigger DuplicateRecordItemTrigger
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   25/02/2021
     * @return: void
     * @param:  pNewList, the Trigger context variable Trigger.new
     */
    public void onAfterInsert(List<DuplicateRecordItem> pNewList){
        setDuplicateFlagInAccount(pNewList);
    }

    /**
     * Set the Possible Duplicate flag for the Person Accounts that the duplicate 
     * rules found as possible duplicates
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   25/02/2021
     * @return: void
     * @param:  pNewList, the list of accounts to process
     */
    private void setDuplicateFlagInAccount(List<DuplicateRecordItem> pNewList){
        Set<Id> accountIdSet = new Set<Id>();
        for(DuplicateRecordItem dri : [
            SELECT 
                Id, 
                RecordId, 
                Record.Name, 
                DuplicateRecordSet.DuplicateRule.SobjectType, 
                DuplicateRecordSet.DuplicateRule.SobjectSubtype 
            FROM 
                DuplicateRecordItem 
            WHERE Id IN: Trigger.newMap.keySet()])
        {
            if(dri.DuplicateRecordSet.DuplicateRule.SobjectType == ACCOUNT_SOBJECT_TYPE &&
                dri.DuplicateRecordSet.DuplicateRule.SobjectSubtype == PERSON_ACCOUNT_NAME)
            {
                accountIdSet.add(dri.RecordId);
            }
        }

        List<Account> accList = new List<Account>();
        for(Account acc : [
            SELECT 
                Id, Possible_Duplicate__c, CreatedDate 
            FROM 
                Account 
            WHERE Id IN: accountIdSet
            AND Possible_Duplicate__c = FALSE])
        {
            acc.Possible_Duplicate__c = true;
            if (acc.CreatedDate > System.now().addMinutes(-2) && acc.Possible_Duplicate__c) {
                acc.Possible_Duplicate_OB_check__c = Constants.ACCOUNT_OB_CHECK_MANUAL_REVIEW;
            }

            accList.add(acc);
        }

        if(!accList.isEmpty()){
            TriggersUtil.disableObjTriggers(
                TriggersUtil.Obj.ACCOUNT);
            
            update accList;
            
            TriggersUtil.enableObjTriggers(
                TriggersUtil.Obj.ACCOUNT);
        }
    }
}