/**
 * @File Name          : CaseHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/4/2024, 12:32:51 PM
**/
public inherited sharing class CaseHelper {

    public static List<Case> getRoutingInfoById(Set<ID> caseIdSet) {
        if (
            (caseIdSet == null) ||
            caseIdSet.isEmpty()
        ) {
            return new List<Case>();
        }
        //else...
        List<Case> result = [
            SELECT 
                Id,
                Is_HVC__c,
                Language,
                Language_Corrected__c,
                Agent_Capacity_Status__c,
                Routed__c
            FROM 
                Case 
            WHERE 
                Id IN :caseIdSet
        ];
        return result;
    }

    public static List<Case> getNotRoutedOrAssigned(Set<ID> caseIdSet) {
        if (
            (caseIdSet == null) ||
            caseIdSet.isEmpty()
        ) {
            return new List<Case>();
        }
        //else...
        List<Case> result = [
            SELECT
                Id
            FROM
                Case
            WHERE
                Id in :caseIdSet
            AND 
                Routed_And_Assigned__c = false
        ];
        return result;
    }

    public static Case getCloseInfoById(String caseId) {
        if (String.isBlank(caseId)) {
            return null;
        }
        //else...
        Case result = null;
        List<Case> caseList = [
            SELECT
                Id,
                Status,
                Origin,
                Inquiry_Nature__c,
                Live_or_Practice__c,
                Type__c,
                Subtype__c,
                Is_Marked_as_Important__c,
                Complaint__c,
                Is_HVC__c,
                SourceId
            FROM
                Case
            WHERE
                Id = :caseId
        ];
        if (!caseList.isEmpty()) {
            result = caseList[0];
        }
        return result;
    }

    public static Boolean isValidLanguageCorrectedValue(String newVal) {
        Boolean isValidValue = SPPicklistUtil.isValidValue(
            Case.Language_Corrected__c,
            newVal
        );
        return (isValidValue == true);
    }

    public static String getValidLanguageCorrected(
        String languageCorrected,
        String defaultLanguage
    ) {
        String result = defaultLanguage;
        if (isValidLanguageCorrectedValue(languageCorrected) == true) {
            result = languageCorrected;
        } 
        return result;
    }

    public static List<String> getClosedStatuses() {
        List<String> result = new List<String>();
        List<CaseStatus> caseStatusList = [
            SELECT 
                ApiName 
            FROM 
                CaseStatus 
            WHERE 
                IsClosed = true
        ];
        for(CaseStatus caseStatus : caseStatusList) {
            result.add(caseStatus.ApiName);
        } 
        return result;
    }
    
}