/**
 * Annual PIU processes. Daily job to trigger anual batch
 * @author Fernando Gomez
 * @version 1.0
 * @see https://oandacorp.atlassian.net/browse/SP-10406
 */
public without sharing class CurrentDeskPIUAnnualProcessJob implements Schedulable {
	private static final String CRON_EXP = '0 0 {0} * * ?';

	/**
	 * Schedules the job daily 
	 */
	public static void scheduleJob(Integer hour) {	
		String timeStr;

		if (hour == null || hour < 0 || hour > 23)
			throw new CurrentDeskPIUAnnualProcessException('Hour out of range');

		// we put the hours in the scheduker str
		timeStr = String.format(CRON_EXP, new List<String> { String.valueOf(hour) });
		System.schedule(
			'Annual PIU processes - ' + System.now(),
			timeStr,
			new CurrentDeskPIUAnnualProcessJob());
	}

	/**
	 * Automtically executed when job is picked up yb system
	 * @param SC
	 */
	public void execute(SchedulableContext SC) {
		Database.executebatch(new CurrentDeskPIUAnnualProcessBatch(), 1);
	}

	/**
	 * Throwable exception
	 */
	public class CurrentDeskPIUAnnualProcessException extends Exception { }
}