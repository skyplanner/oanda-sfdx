/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 18th, 2019
 */
global class HelpPortalWelcomeCtrl extends HelpPortalBase {
	global transient List<FAQ> mostVisited { get; private set; }
	global String division {get;set;}
	
	/**
	 * Main constructor
	 */
	global HelpPortalWelcomeCtrl() {
		super();
		String divisionURL = ApexPages.currentPage().getParameters().get('division');
		division = divisionURL;
	    mostVisited = getTopFaqsByDivision(division);
				
	}
}