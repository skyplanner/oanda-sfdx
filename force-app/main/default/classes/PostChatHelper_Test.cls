/**
 * @File Name          : PostChatHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/23/2021, 11:17:45 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 10:59:54 AM   acantero     Initial Version
**/
@isTest
private without sharing class PostChatHelper_Test {

    static final String CHAT_DETAILS_TEST_DATA = 'ChatDetailsTestData';

    @isTest
    static void test() {
        String chatDetailsJson = 
            SPTestUtil.loadStaticResource(CHAT_DETAILS_TEST_DATA);
        Map<String,String> params = new Map<String,String>
        {
            PostChatHelper.CHAT_DETAILS => chatDetailsJson
        };
        Test.startTest();
        PostChatHelper instance = new PostChatHelper(params);
        Test.stopTest();
        System.assert(String.isNotBlank(instance.language));
    }
    
    @isTest
    static void test2() {
        Test.startTest();
        PostChatHelper.ChatDetails chatDetailsObj = 
            new PostChatHelper.ChatDetails();
        PostChatHelper.CustomDetail customDetailObj = 
            new PostChatHelper.CustomDetail();
        chatDetailsObj.visitorId = 'fake visitorId';
        customDetailObj.entityMaps = new List<String>();
        customDetailObj.transcriptFields = new List<String>();
        customDetailObj.displayToAgent = true;
        Test.stopTest();
        System.assertNotEquals(null, chatDetailsObj);
        System.assertNotEquals(null, customDetailObj);
    }

}