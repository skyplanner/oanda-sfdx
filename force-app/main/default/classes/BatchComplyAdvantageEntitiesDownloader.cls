public with sharing class BatchComplyAdvantageEntitiesDownloader implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Database.AllowsCallouts
{
    public string additionalQueryFilters = '';
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public set<Id> searchIds;
    public static final Schema.SObjectField ENTITY_OBJ_UNIQUE_NUMBER_FIELD = Comply_Advantage_Search_Entity__c.Fields.Unique_ID__c;

    public set<string> complyAdvantageSearchFields = new set<string>{
        'id',
        'name',
        'case__c',
        'is_monitored__c',
        'match_status_override_reason__c',
        'match_status__c',
        'report_link__c',
        'search_id__c',
        'division_name__c',
        'search_parameter_birth_year__c',
        'search_parameter_citizenship_nationality__c',
        'search_parameter_mailing_country__c',
        'search_text__c',
        'fxaccount__c',
        'entity_contact__c',
        'is_alias_search__c',
        'search_reason__c',
        'case_management_link__c',
        'is_suspended__c',
        'search_reference__c',
        'total_hits__c',
        'is_secondary_search__c',
        'affiliate__c',
        'custom_division_name__c',
        'flagged_by_admin__c',
        'monitoring_turned_off_datetime__c',
        'monitoring_turned_on_datetime__c',
        'entities_download_status__c',
        'monitor_changes_acknowledged_date__c',
        'monitor_changes_acknowledged_by__c',
        'has_monitor_changes__c',
        'monitor_change_date__c',
        'fxAccount__r.Email_Formula__c'
    };
  
    public BatchComplyAdvantageEntitiesDownloader(set<Id> searchIds) 
    {
        this.searchIds = searchIds;

        query = 'select ' + SoqlUtil.getSoqlFieldList(complyAdvantageSearchFields)+ ' from Comply_Advantage_Search__c';
       
        if(!searchIds.isEmpty())
        {
            query +=  ' Where Id IN :searchIds';
        }
    }
    public BatchComplyAdvantageEntitiesDownloader(string filter) 
    {
        query = 'select ' + SoqlUtil.getSoqlFieldList(complyAdvantageSearchFields)+ ' from Comply_Advantage_Search__c';
       
        if(string.isNotBlank(filter))
        {
            query +=  ' Where ' + filter;
        }
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'select ' + SoqlUtil.getSoqlFieldList(complyAdvantageSearchFields)+ ' from Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) 
    {
        List<Comply_Advantage_Search_Entity__c> entitesToInsert = new List<Comply_Advantage_Search_Entity__c>{};
        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>{};

        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(scope);
        set<Id> searchIds = searchMap.keySet();
       
        Map<Id, set<string>> searchEntityUniqueIdsMap = new Map<Id, set<string>>{};
        Map<string, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId = new Map<string, Comply_Advantage_Search_Entity__c>{};

        //retrieve entities from Salesforce for the searches
        for(Comply_Advantage_Search_Entity__c entity : [select ID, Unique_ID__c, Comply_Advantage_Search__c
                                                        From Comply_Advantage_Search_Entity__c
                                                        Where Comply_Advantage_Search__c IN :searchIds])
        {
            
            set<string> entityUniqueIds = searchEntityUniqueIdsMap.get(entity.Comply_Advantage_Search__c);
            if(entityUniqueIds == null)
            {
                entityUniqueIds = new set<string>{};
            }
            entityUniqueIds.add(entity.Unique_ID__c);
            searchEntityUniqueIdsMap.put(entity.Comply_Advantage_Search__c, entityUniqueIds);

            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }

        //download entities for each search and sync with SF
        for(Comply_Advantage_Search__c search : scope)
        {
            ComplyAdvantageEntitiesDownloader helper = new ComplyAdvantageEntitiesDownloader(search);
            ComplyAdvantageEntitiesDownloader.EntityDownloadResult result = helper.getEntities();
            if(result.isSuccessful)
            {
                Comply_Advantage_Search_Entity__c[] caEntities = result.entities;

                set<string> sfSearchEntityIds = searchEntityUniqueIdsMap.get(search.Id);
                if(sfSearchEntityIds != null && !sfSearchEntityIds.isEmpty())
                {
                    caEntities = syncEntities(sfSearchEntityIds, sfBatchEntitiesByUniqueId, caEntities);
                }
                entitesToInsert.addAll(caEntities);
                
                search.Entities_Download_Status__c = 'Done';
                searchesToUpdate.add(search);
            }
            else 
            {
                search.Entities_Download_Status__c = 'Failed';
                searchesToUpdate.add(search);
            }
        }
        
        if(!entitesToInsert.isEmpty())
        {
            Database.upsert(entitesToInsert, ENTITY_OBJ_UNIQUE_NUMBER_FIELD, false);
        }
        if(!searchesToUpdate.isEmpty())
        {
            Database.update(searchesToUpdate, false);
        }
	} 
    
    //if any entity from Salesforce not present in CA result will be marked as 'Removed'
    private Comply_Advantage_Search_Entity__c[] syncEntities(set<string> sfSearchEntityIds,
                                                             Map<string, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId,
                                                             Comply_Advantage_Search_Entity__c[] caEntities)
    {
        Comply_Advantage_Search_Entity__c[] entities = caEntities;

        set<string> caEntityUniqueIds = new set<String>{};
        
        for(Comply_Advantage_Search_Entity__c caEntity : caEntities)
        {
            caEntityUniqueIds.add(caEntity.Unique_ID__c);
        }
        for(string sfEntityUniqueId : sfSearchEntityIds)
        {
            if(!caEntityUniqueIds.contains(sfEntityUniqueId))
            {
                Comply_Advantage_Search_Entity__c sfEntity = sfBatchEntitiesByUniqueId.get(sfEntityUniqueId);
                sfEntity.Is_Removed__c = true;
                entities.add(sfEntity);
            }
        }
        return entities;
    } 
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}