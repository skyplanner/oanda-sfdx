/**
 * Provides access to emai templates.
 * @author Fernando Gomez
 */
public without sharing class EmailTemplateUtil {

	/**
	 * @return the template marked for the specified language.
	 * The template's api name must start with the specified prefix.
	 * If none is found, null is returned. If more than one is found,
	 * the first occurrence is returned.
	 * @param namePrefix
	 * @param language
	 */
	public static EmailTemplate getTemplateByLanguage(
			String namePrefix, String language) {
		List<EmailTemplate> result;
		String nameLikeClause, descriptionLikeClause;

		nameLikeClause = namePrefix + '%';
		descriptionLikeClause = '%Language="' + language + '"%';

		// a template of a language will contain
		// the prefix in the api name and the language
		// will be in the description
		result = [
			SELECT Id, Name, DeveloperName
			FROM EmailTemplate
			WHERE DeveloperName LIKE :nameLikeClause
			AND Description LIKE :descriptionLikeClause
			ORDER BY DeveloperName
		];

		// it is possible we get many, but it should not happen..
		// in that case we return the first
		return result.isEmpty() ? null : result.get(0);
	}
}