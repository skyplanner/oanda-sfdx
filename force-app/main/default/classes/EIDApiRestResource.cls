/*
*  @author : Mikolaj Juras
*  Created Date : Feb 6 2024
*  Purpose : new class to handle DIT from User API, based on old logic
*/
@RestResource(urlMapping='/api/v1/user/*/eid_results')
global without sharing class EIDApiRestResource {
    private final static String PROVIDER_TRULLIO = 'TruliooUS';
    private final static String PROVIDER_EQUIFAX_DIT = 'EquifaxDIT';
    private static Map <String,String> equifaxDITDetails;
    // static Integer fxAccountUserId;

    @HTTPPost
    global static String createEID(
        String provider, 
        Boolean passed, 
        String eid_request, 
        String eid_response, 
        String eid_response_full, 
        Boolean documents_approved, 
        String tracking_number, 
        Long create_date,
        String watchlist_result,
        String provider_link)
    {        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        if(!CustomSettings.isAPIEnabled(CustomSettings.API_NAME_EID_RESULT)){
            return CDAAdapter.serialize(false, 'API ' + CustomSettings.API_NAME_EID_RESULT + ' is disabled');
        }
        try {
            String fxAccountTradeUserIdString = req.requestURI.remove('/eid_results').substring(req.requestURI.remove('/eid_results').lastIndexOf('/')+1);

            Integer fxAccountUserId = Integer.valueOf(fxAccountTradeUserIdString);

            //Now Query for an FX Account against the user Id provided and LIVE Fx Accounts
            List<fxAccount__c> fxAccntLst = [SELECT id, Name FROM fxAccount__c WHERE fxTrade_User_Id__c = : fxAccountUserId AND RecordTypeId = : RecordTypeUtil.getFxAccountLiveId() LIMIT 1];
            if(!fxAccntLst.isEmpty()){
                //If the fxAccount is found, we link the EID Result to the FX Account
                EID_Result__c eidResultObj = new EID_Result__c(
                    fxAccount__c = fxAccntLst[0].Id, 
                    fxTrade_User_Id__c = fxAccountUserId, 
                    Provider__c = provider, 
                    Reason__c = eid_response, 
                    EID_Response__c = eid_response_full, 
                    Tracking_Number__c = tracking_number, 
                    Date__c = DateTime.newInstance(create_date),
                    Watchlist_Result__c = watchlist_result,
                    Provider_Link_API__c = provider_link
                );
                
                //SP-13229 for Equifax DIT identiti provider set multiple fields from reposnse
                if (provider == PROVIDER_EQUIFAX_DIT) {
                    setEquifaxDTfields(eidResultObj, eid_response_full);
                }
                
                eidResultObj.Result__c = passed == true ? 'Passed' : 'Failed';

                insert eidResultObj;

                if(provider == PROVIDER_TRULLIO){
                    List<EIDResultJSON> json = EIDResultJSON.parse(eid_response_full);
                    List<EID_Result__c> RelatedTrulioRecords = new List<EID_Result__c>();
                    for(EIDResultJSON relatedEID :json){
                        EID_Result__c TrullioRecord = new EID_Result__c(
                            Trulliio_Agency__c  = eidResultObj.id, 
                            First_Name__c = relatedEID.Response.first_name,
                            Last_Name__c = relatedEID.Response.last_name,
                            DOB__c = relatedEID.Response.dob,
                            Unit_Number__c =  relatedEID.Response.UnitNumber,
                            Building_Number__c = relatedEID.Response.BuildingNumber,
                            StreetvName__c = relatedEID.Response.StreetName,
                            Street_Type__c = relatedEID.Response.StreetType,
                            City__c = relatedEID.Response.City,
                            Postal_Code__c = relatedEID.Response.PostalCode,
                            State_Province_Code__c = relatedEID.Response.StateProvinceCode,
                            Telephone__c = relatedEID.Response.telephone,
                            SSN__c = relatedEID.Response.ssn,
                            Fraud_Flag_Status__c  =relatedEID.Response.Fraud_flag.status,
                            Fraud_Flag_Code__c =relatedEID.Response.Fraud_flag.flags,
                            Agency_Name__c =relatedEID.name
                        );
                        RelatedTrulioRecords.add(TrullioRecord);
                    }
                    try{
                        insert RelatedTrulioRecords;
                        
                    }catch(Exception e){
                        System.debug('Exception caught'+e.getMessage());
                    }
                }
                return CDAAdapter.serialize(true, 'EID Result has been sucessfully created and its Id is ' + eidResultObj.Id);
            } else{
                return CDAAdapter.serialize(false, 'EID Result could not be created, since fxAccount does not exist for user id ' + fxAccountUserId);
            }
        }
        catch(Exception error){
            return CDAAdapter.serialize(false, 'The response returned an error from Salesforce '+error.getMessage() + ' and cause is ' + error.getCause() + error.getStackTraceString());
        }
    }

    @HTTPGet
    global static void getEID() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String fxAccountTradeUserIdString;
        Integer fxAccountUserId;

        List <EID_Result__c> EIDs;

        try {
            fxAccountTradeUserIdString = req.requestURI.remove('/eid_results').substring(req.requestURI.remove('/eid_results').lastIndexOf('/')+1);
            fxAccountTradeUserIdString = String.escapeSingleQuotes(fxAccountTradeUserIdString);
            fxAccountUserId = Integer.valueOf(fxAccountTradeUserIdString);
            String globalId = fxAccountTradeUserIdString + '+' + String.valueOf(RecordTypeUtil.getFxAccountLiveId()).left(15);
            EIDs = [SELECT Id, Name, fxAccount__r.fxTrade_Global_ID__c, CreatedDate,  EID_Response__c, Result__c, Provider__c, Tracking_Number__c, Reason__c
                    FROM EID_Result__c 
                    WHERE fxAccount__r.fxTrade_Global_ID__c =:globalId];
            List<EIDResponse> EIDresponses = new List<EIDResponse>();

            for(EID_Result__c eid : EIDs) {
                EIDresponses.add(getEidResponseFromResult(eid));
            }
            res.responseBody = Blob.valueOf(JSON.serialize(EIDresponses));
            res.statusCode = 200;
            return;
        } catch(Exception ex){
            String respBody = 'Something went wrong: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                'EIDApiRestResource',
                'Http get failed',
                res.statusCode + ' ' + respBody);
            return;
        }
    }

    private static void setEquifaxDTfields(EID_Result__c er, String json) {
        try{
            EquifaxDIT eqDIT = (EquifaxDIT)System.JSON.deserialize(json, EquifaxDIT.class);
            equifaxDITDetails = mapEquifaxItems(eqDIT.details);

            er.Transaction_Id__c = eqDIT.transactionId;
            er.Correlation_Id__c = eqDIT.correlationId;
            er.Reference_Transaction_Id__c = eqDIT.referenceTransactionId;
            er.Decision__c = eqDIT.decision;
            //to be checked regarding the time zone
            er.Time_Stamp__c = eqDIT?.timestamp != null ? Datetime.valueOf(eqDIT.timestamp.replace('T', ' ').left(19)) : null;
            er.Original_Transaction_Id__c = eqDIT.originalTransactionId;
            
            er.Address_Affiliation__c = getPayloadItemForField('addressAffiliation', true);
            er.Address_Affiliation_Reasons__c = getPayloadItemForField('addressAffiliationReason');
            er.Address_Insights__c = getPayloadItemForField('addressInsights', true);
            er.Address_Insights_Reason__c = getPayloadItemForField('addressInsightsReason');
            er.Address_Verification__c = getPayloadItemForField('addressVerification', true);
            er.Address_Verification_Reasons__c = getPayloadItemForField('addressVerificationReason');
            er.Address_Trust__c = getPayloadItemForField('addressTrust', true);
                        
            er.Device_Association__c = getPayloadItemForField('deviceAssociation', true);
            er.Device_Association_Reason__c = getPayloadItemForField('deviceAssociationReason');
            er.Device_Reputation__c = getPayloadItemForField('deviceReputation', true);
            er.Device_Reputation_Reason__c = getPayloadItemForField('deviceReputationReason');
            er.Device_Trust__c = getPayloadItemForField('deviceTrust', true);
        
        
            er.DOB_Affiliation__c = getPayloadItemForField('dobAffiliation', true);
            er.DOB_Affiliation_Reason__c = getPayloadItemForField('dobAffiliationReason');
            er.DOB_Insights__c = getPayloadItemForField('dobInsights', true);
            er.DOB_Insights_Reason__c = getPayloadItemForField('dobInsightsReason');
            er.DOB_Verification__c = getPayloadItemForField('dobVerification', true);
            er.DOB_Verification_Reason__c = getPayloadItemForField('dobVerificationReason');
            er.DOB_Trust__c = getPayloadItemForField('dobTrust', true);
        
            er.Email_Affiliation__c = getPayloadItemForField('emailAffiliation', true);
            er.Email_Affiliation_Reason__c = getPayloadItemForField('emailAffiliationReason');
            er.Email_Insights__c = getPayloadItemForField('emailInsights', true);
            er.Email_Insights_Reason__c = getPayloadItemForField('emailInsightsReason');
            er.Email_Verification__c = getPayloadItemForField('emailVerification', true);
            er.Email_Verification_Reason__c = getPayloadItemForField('emailVerificationReason');
            er.Email_Trust__c = getPayloadItemForField('emailTrust', true);
        
            er.Identity_Verification__c = getPayloadItemForField('identityVerification', true);
            er.Identity_Verification_Reason__c = getPayloadItemForField('identityVerificationReason');
            er.Identity_Trust__c = getPayloadItemForField('identityTrust', true);
            er.Identity_Risk__c = getPayloadItemForField('identityRisk', true);
            er.Identity_Risk_Reason__c = getPayloadItemForField('identityRiskReason');
            er.Identity_Resolution__c = getPayloadItemForField('identityResolution', true);
            er.Identity_Resolution_Reason__c = getPayloadItemForField('identityResolutionReason');
        
            er.Phone_Affiliation__c = getPayloadItemForField('phoneAffiliation', true);
            er.Phone_Affiliation_Reason__c = getPayloadItemForField('phoneAffiliationReason');
            er.Phone_Insights__c = getPayloadItemForField('phoneInsights', true);
            er.Phone_Insights_Reason__c = getPayloadItemForField('addressAffiliationReason');
            er.Phone_Verification__c = getPayloadItemForField('phoneVerification', true);
            er.Phone_Verification_Reason__c = getPayloadItemForField('phoneVerificationReason');
            er.Phone_Trust__c = getPayloadItemForField('phoneTrust', true);
        
            er.SSN_Affiliation__c = getPayloadItemForField('SSNAffiliation', true);
            er.SSN_Affiliation_Reason__c = getPayloadItemForField('SSNAffiliationReason');
            er.SSN_Insights__c = getPayloadItemForField('SSNInsights', true);
            er.SSN_Insights_Reason__c = getPayloadItemForField('SSNInsightsReason');
            er.SSN_Verification__c = getPayloadItemForField('SSNVerification', true);
            er.SSN_Verification_Reason__c = getPayloadItemForField('SSNVerificationReason');
            er.SSN_Trust__c = getPayloadItemForField('SSNTrust', true);
        } catch (JSONException e){
            System.debug('JSON mapping Exception EIDResultRestResource.setEquifaxDTfields'+e.getMessage());
        }
    }

    public static Map<String,String> mapEquifaxItems(List<EquifaxDITItem> equifaxDITItems) {
        Map <String,String> quifaxKeyValue = new Map<String,String>();
        for (EquifaxDITItem eqi : equifaxDITItems) {
            quifaxKeyValue.put(eqi.key, eqi.value);
        }
        return quifaxKeyValue;
    }

    public static String getPayloadItemForField(String key) {
        return equifaxDITDetails != null && equifaxDITDetails.containsKey(key) ? equifaxDITDetails.get(key) : '';
    }

    public static Boolean getPayloadItemForField(String key, Boolean isBoolean) {
        return equifaxDITDetails != null && equifaxDITDetails.containsKey(key) ? stringToBoolean(equifaxDITDetails.get(key)) : false;
    }

    public static Boolean stringToBoolean(String value) {
        return value == 'Y' || value == 'Yes' ? true : false;
    }

    public static EIDResponse getEidResponseFromResult(EID_Result__c e) {
        EIDResponse r = new EIDResponse();
        r.create_date = e.CreatedDate.getTime();
        r.eid_response = e.Reason__c;
        r.eid_response_full = e.EID_Response__c;
        r.passed = e.Result__c == 'Passed' ? true : false;
        r.provider = e.Provider__c;
        r.sf_id = e.Id;
        r.sf_name = e.Name;
        r.tracking_number = e.Tracking_Number__c;
        return r;
    }


    public class EquifaxDIT {
        String transactionId;
        String correlationId;
        String referenceTransactionId;
        String decision;
        String timestamp;
        String originalTransactionId;
        List<EquifaxDITItem> details;

    }
    public class EquifaxDITItem {
        public String key;
        public String value;
    }

    public class EIDResponse{
        public string sf_id { get; set; }
        public string sf_name { get; set; }
        public string provider { get; set; }
        public Boolean passed { get; set; }
        public string eid_response { get; set; }
        public string eid_response_full { get; set; }
        public string tracking_number { get; set; }
        public long create_date { get; set; }
    }
}