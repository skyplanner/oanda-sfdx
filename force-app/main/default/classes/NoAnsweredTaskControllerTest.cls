/**
 * Created by akajda on 17/05/2024.
 */

@IsTest
public with sharing class NoAnsweredTaskControllerTest {
    @IsTest
    static void completeUnansweredTaskTest(){
        Task task1 = new Task(Subject = 'Call');
        insert task1;

        Test.startTest();
        NoAnsweredTaskController.markTaskAsNotAnswered(task1.Id);
        update task1;
        Test.stopTest();

        Task taskAfter = [SELECT Status, No_answer__c FROM Task WHERE Id=:task1.Id LIMIT 1];
        System.assertEquals(true, taskAfter.No_answer__c);
        System.assertEquals('Completed', taskAfter.Status);
    }
}