@IsTest 
public class LeadTriggerHandlerTest {

    final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);

    @IsTest 
    static void insertNotesField01(){    	
        String note1 = 'Sample Entry';
        Lead lead = new Lead();
    	lead.FirstName = 'John';
    	lead.LastName = 'Doe';
    	lead.Email = 'testemail01@sptestemail.com';
        lead.Entry__c = note1;
        Test.startTest();
            insert lead;
        Test.stopTest();
        lead = [SELECT Id, Notes__c, Entry__c FROM Lead WHERE Id =: lead.Id];
        System.assert(String.isNotBlank(lead.Notes__c), 'The notes cannot be empty');
        System.assert(
            lead.Notes__c.contains(note1), 
            'The notes field does not contains the note ' + lead.Notes__c
        );
        System.assert(String.isBlank(lead.Entry__c), 'The entry must be empty');
    }

    @IsTest 
    static void updateNotesField01(){    	
        String note1 = 'Sample Entry 1';
        Lead lead = new Lead();
    	lead.FirstName = 'John';
    	lead.LastName = 'Doe';
    	lead.Email = 'testemail01@sptestemail.com';
        lead.Entry__c = note1;
        insert lead;
        Test.startTest();
            lead = [SELECT Id, Notes__c, Entry__c FROM Lead WHERE Id =: lead.Id];
            System.assert(String.isNotBlank(lead.Notes__c), 'The notes cannot be empty');
            System.assert(
                lead.Notes__c.contains(note1), 
                'The notes field does not contains the note ' + lead.Notes__c
            );            
            System.assert(String.isBlank(lead.Entry__c), 'The entry must be empty');
        Test.stopTest();
        String note2 = 'Sample Entry 2';
        lead.Entry__c = note2;
        update lead;        
        lead = [SELECT Id, Notes__c, Entry__c FROM Lead WHERE Id =: lead.Id];
        System.assert(String.isNotBlank(lead.Notes__c), 'The notes cannot be empty');
        System.assert(
            lead.Notes__c.contains(note1), 
            'The notes field does not contains the note ' + lead.Notes__c
        );
        System.assert(String.isBlank(lead.Entry__c), 'The entry must be empty');
    }

    @IsTest
    private static void checkLeadRollDownOnFxAccount() {

        Lead lead = new Lead(
                RecordTypeId = RecordTypeUtil.getLeadRetailId(),
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                Email = 'test@test.com',
                HasOptedOutOfEmail=false);
        insert lead;

        fxAccount__c fxAcc = new fxAccount__c(
                RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
                Account__c = null,
                Lead__c=lead.Id,
                OwnerId = UserUtil.getSystemUserId()
        );
        insert fxAcc;

        lead.fxAccount__c = fxAcc.Id;
        update lead;

        lead.Salutation = 'updated value';
        lead.FirstName = 'updated value';
        lead.MiddleName = 'updated value';
        lead.LastName = 'updated value';
        lead.Suffix = 'updated value';
        lead.Street = 'updated value';
        lead.City = 'updated value';
        lead.State = 'updated value';
        lead.PostalCode = '0000';
        lead.Email = 'updated@test.com';
        lead.Phone = '101101101';
        lead.Language_Preference__c = 'French';
        lead.Business_Name__c = 'updated value';
        lead.HasOptedOutOfEmail = true;
        update lead;

        fxAccount__c updatedAcc = [SELECT Business_Name__c, City__c, Country_Name__c, Email__c, First_Name__c,
                Has_Opted_Out_Of_Email__c, Language_Preference__c, Last_Name__c, Middle_Name__c, Phone__c,
                Postal_Code__c, State__c, Street__c, Suffix__c, Title__c FROM fxAccount__c WHERE Id=:fxAcc.Id];

        System.assertEquals(updatedAcc.Title__c, lead.Salutation);
        System.assertEquals(updatedAcc.First_Name__c, lead.FirstName);
        System.assertEquals(updatedAcc.Middle_Name__c, lead.MiddleName);
        System.assertEquals(updatedAcc.Last_Name__c, lead.LastName);
        System.assertEquals(updatedAcc.Suffix__c, lead.Suffix);
        System.assertEquals(updatedAcc.Street__c, lead.Street);
        System.assertEquals(updatedAcc.City__c, lead.City);
        System.assertEquals(updatedAcc.State__c, lead.State);
        System.assertEquals(updatedAcc.Postal_Code__c, lead.PostalCode);
        System.assertEquals(updatedAcc.Email__c, lead.Email);
        System.assertEquals(updatedAcc.Phone__c, lead.Phone);
        System.assertEquals(updatedAcc.Language_Preference__c, lead.Language_Preference__c);
        System.assertEquals(updatedAcc.Business_Name__c, lead.Business_Name__c);
        System.assertEquals(updatedAcc.Has_Opted_Out_Of_Email__c, lead.HasOptedOutOfEmail);

    }
    @IsTest
    private static void checkLeadRollDownForRegistrationUser() {

        Lead lead = new Lead(
                RecordTypeId = RecordTypeUtil.getLeadRetailId(),
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                Email = 'test@test.com',
                HasOptedOutOfEmail=false);
        insert lead;

        fxAccount__c fxAcc = new fxAccount__c(
                RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
                Account__c = null,
                Lead__c=lead.Id,
                Has_Opted_Out_Of_Email__c = false,
                OwnerId = UserUtil.getSystemUserId()
        );
        insert fxAcc;

        lead.fxAccount__c = fxAcc.Id;
        update lead;

        User testU = new User(
                LastName='test user',
                Username='testuser1@spoanda.com',
                Email='testuser@spoanda.com',
                Alias='testuser',
                ProfileId=UserUtil.getRegistrationProfileId(),
                TimeZoneSidKey='GMT',
                LocaleSidKey='en_US',
                EmailEncodingKey='ISO-8859-1',
                LanguageLocaleKey='en_US',
                IsActive=true);
        insert testU;

        Test.startTest();
        System.runAs(testU) {
            lead.HasOptedOutOfEmail = true;
            update lead;
        }

        Test.stopTest();
        fxAccount__c updatedAcc = [SELECT Business_Name__c, City__c, Country_Name__c, Email__c, First_Name__c,
                Has_Opted_Out_Of_Email__c, Language_Preference__c, Last_Name__c, Middle_Name__c, Phone__c,
                Postal_Code__c, State__c, Street__c, Suffix__c, Title__c FROM fxAccount__c WHERE Id=:fxAcc.Id];

        System.assertNotEquals(updatedAcc.Has_Opted_Out_Of_Email__c, lead.HasOptedOutOfEmail);

    }

    @IsTest
    private static void testCountryChangeToRecalcCRA() {

        Country_Setting__c can = new Country_Setting__c(Name='Canada', Group__c='Canada', ISO_Code__c='ca', Region__c='North America', Zone__c='Americas', Risk_Rating__c='LOW');
        insert can;

        Lead lead = new Lead(
                RecordTypeId = RecordTypeUtil.getLeadRetailId(),
                LastName = 'test',
                Phone = '1234567546',
                Email = 'test@test.com');                
        insert lead;

        fxAccount__c fxa = new fxAccount__c(
                Name = 'test',
                Lead__c = lead.Id,
                Funnel_Stage__c = FunnelStatus.AWAITING_CLIENT_INFO,
                RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
                Division_Name__c = 'OANDA Canada',
                Employment_Status__c = 'Unemployed',
                Citizenship_Nationality__c = 'Canada',
                Customer_Risk_Assessment_Text__c = null
        );
        insert fxa;

        lead.fxAccount__c = fxa.Id;
        update lead;

        fxa = [SELECT Customer_Risk_Assessment_Text__c FROM fxAccount__c where Id=:fxa.Id];
        Assert.areEqual(null, fxa.Customer_Risk_Assessment_Text__c);        

        Test.startTest();
        System.runAs(REGISTRATION_USER) {
            lead.Country = 'Canada';
            update lead;
        }
        Test.stopTest();

        fxa = [SELECT Customer_Risk_Assessment_Text__c FROM fxAccount__c where Id=:fxa.Id];

        Assert.areNotEqual(null, fxa.Customer_Risk_Assessment_Text__c);
    }
}