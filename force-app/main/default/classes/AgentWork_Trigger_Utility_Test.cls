/**
 * @File Name          : AgentWork_Trigger_Utility_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/12/2022, 12:56:40 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/16/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class AgentWork_Trigger_Utility_Test {

    // @testSetup
    // static void setup(){
    // }
    
    //after insert, agentWork linked to a Case
    @isTest
    static void execute1() {
        ID caseChannelId = OmnichanelRoutingTestDataFactory.getCaseChannelId();
        Case case1 = null;
        //ID queueId = OmnichanelRoutingTestDataFactory.getRequiredSkillsQueueId();
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0003');
        System.runAs(supervisor) {
            ID userId = UserInfo.getUserId();
            ID hvcAccountId = 
                OmnichanelRoutingTestDataFactory.createHvcAccount(
                    OmnichanelConst.OANDA_CORPORATION
                );
            case1 = new Case(
                Origin = 'Custom',
                AccountId = hvcAccountId,
                Subject = 'ACL Email ' + System.Now(),
                Priority = 'Normal'
            );
            insert case1;
            System.debug('case1: ' + case1.Id);
            System.debug('caseChannelId: ' + caseChannelId);
            PendingServiceRouting psr1 = new PendingServiceRouting(
                CapacityWeight = 1,
                IsReadyForRouting = false,
                RoutingModel  = 'MostAvailable',
                RoutingPriority = 1,
                RoutingType = 'SkillsBased',
                ServiceChannelId = caseChannelId,
                WorkItemId = case1.Id
            );
            //this is a fake psr, i do not want it take skills
            PendingServiceRouting_Trigger_Utility.enabled = false; //Important!!
            insert psr1;
            //...
            List<AgentWork> agentWorkList = new List<AgentWork> {
                new AgentWork(
                    ServiceChannelId = caseChannelId,
                    WorkItemId = case1.Id,
                    UserId = userId,
                    PendingServiceRoutingId = psr1.Id
                )
            };
            AgentWork_Trigger_Utility.assignedIsDefaultStatusInTest = true;
            AgentWork_Trigger_Utility handler = new AgentWork_Trigger_Utility(
                agentWorkList,
                null
            );
            handler.execute(
                true, //isAfterInsert
                false //isAfterUpdate
            );
        }
        Test.stopTest();
        case1 = [
            select Routed_And_Assigned__c, Agent_Capacity_Status__c
            from Case 
            where Id = :case1.Id
        ];
        System.debug(case1);
        System.assert(case1.Routed_And_Assigned__c);
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE, 
            case1.Agent_Capacity_Status__c
        );
    }

    @IsTest
    static void isEnabled() {
        Boolean error = false;
        Test.startTest();
        try {
            AgentWork_Trigger_Utility.enabled = false;
            //...
            AgentWork_Trigger_Utility.process(
                null, //newList
                null, //oldMap
                null, //isAfterInsert
                null //isAfterUpdate
            );
            //...
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @IsTest
    static void exception1() {
        Boolean exceptionOk = false;
        Test.startTest();
        try {
            ExceptionTestUtil.prepareDummyException();
            //...
            AgentWork_Trigger_Utility.process(
                null, //newList
                null, //oldMap
                null, //isAfterInsert
                null //isAfterUpdate
            );
            //...
        } catch (Exception ex) {
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    @IsTest
    static void exception2() {
        Boolean exceptionOk = false;
        Test.startTest();
        try {
            ExceptionTestUtil.exceptionInstance = 
                LogException.newInstance('abc', 'def');
            //...
            AgentWork_Trigger_Utility.process(
                null, //newList
                null, //oldMap
                null, //isAfterInsert
                null //isAfterUpdate
            );
            //...
        } catch (Exception ex) {
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

}