/**
 * @File Name          : SSkillRequirementInfo.cls
 * @Description        : 
 * Stores the information necessary to create a SkillRequirement record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/29/2024, 2:22:44 PM
**/
public inherited sharing class SSkillRequirementInfo {

	public final String skillName;

	public SkillRequirement skillReq {get; private set;}

	public SSkillRequirementInfo(
		String skillName, 
		Boolean isAdditional,
		Integer priority
	) {
		this.skillName = skillName;
		this.skillReq = new SkillRequirement(
			IsAdditionalSkill = false
		);
		if (isAdditional == true) {
			this.skillReq.IsAdditionalSkill = true;
			this.skillReq.SkillPriority = priority;
		}
		this.skillReq.SkillLevel = OmnichanelConst.DEFAULT_SKILL_LEVEL;
	}

	public SSkillRequirementInfo(
		String skillName, 
		Boolean isAdditional
	) {
		this(
			skillName, // skillName
			isAdditional, // isAdditional
			OmnichanelConst.DEFAULT_SKILL_PRIORITY // priority
		);
	}

}