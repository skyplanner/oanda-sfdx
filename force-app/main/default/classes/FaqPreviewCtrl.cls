/**
 * @File Name          : FaqPreviewCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/25/2020, 3:58:27 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/26/2020   acantero     Initial Version
**/
public with sharing class FaqPreviewCtrl {

    public static final String DEFAULT_LANGUAGE= 'en_US';

    @AuraEnabled
    public static String getArticleLanguage(String articleId) {
        if (String.isBlank(articleId)) {
            return null;
        }
        //else...
        String result = null;
        List<FAQ__kav> faqList = [
            SELECT 
                Language
            FROM 
                FAQ__kav
            WHERE 
                Id = :articleId
        ];
        if (!faqList.isEmpty()) {
            result = faqList[0].Language;
        }
        //else...
        return result;
    }

    @AuraEnabled
    public static String getArticleId(String urlName, String language) {
        if (String.isBlank(urlName)) {
            return null;
        }
        //else...
        if (String.isBlank(language)) {
            language = DEFAULT_LANGUAGE;
        }
        String result = getFaqId(urlName, language);
        if (
            (result == null) &&
            (language != DEFAULT_LANGUAGE)
        ) {
            result = getFaqId(urlName, DEFAULT_LANGUAGE);
        }
        return result;
    }

    static String getFaqId(String urlName, String language) {
        String result = null;
        List<FAQ__kav> faqList = [
			SELECT 
                Id
            FROM 
                FAQ__kav
            WHERE 
                PublishStatus = 'Online'
            AND 
                UrlName = :urlName
            AND 
                Language = :language
		];
        if (!faqList.isEmpty()) {
            result = faqList[0].Id;
        }
        //else...
        return result;
    }

}