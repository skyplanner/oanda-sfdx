/**
 * Tests: CaseConditionalUpdate
 * @author Fernando Gomez
 */
@isTest
private class CaseConditionalUpdateTest {
	/**
	 * public void execute_QI_Case_owner()
	 */
	@isTest
	static void execute_QI_Case_owner() {
		Case newCase1, oldCase1;
		CaseConditionalUpdate updater;
		
		newCase1 = new Case(
			Type__c = 'Chatbot',
			Subtype__c = '2FA',
			Chat_Division__c = 'APAC',
			Origin = 'Phone',
			OwnerId = UserInfo.getUserId(),
			Inquiry_Nature__c = 'Login',
			RecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId()
		);

		oldCase1 = new Case(
			Type__c = 'Chatbot',
			Subtype__c = '2FA',
			Chat_Division__c = 'APAC',
			Origin = 'Phone',
			OwnerId = UserInfo.getUserId(),
			Inquiry_Nature__c = 'Login',
			RecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId()
		);

		updater = new CaseConditionalUpdate(newCase1, null);
		updater.execute('WF_QI_Case_Owner');
		System.assertEquals(UserInfo.getUserId(), newCase1.QI_Case_Owner__c);
	}
}