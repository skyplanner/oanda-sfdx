/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-10-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasBaseGroup implements Comparable {
    public Integer id { get; set; }

    public String name { get; set; }
    
    public String description { get; set; }

    public transient String label { 
        get {
            return id + ' - ' + name;
        } 
    }

    public Integer compareTo(Object compareTo) {
        TasBaseGroup compareToGroup = (TasBaseGroup) compareTo;

        if (id > compareToGroup.id)
            return 1;
        else if (id < compareToGroup.id)
            return -1;
        return 0;
    }
}