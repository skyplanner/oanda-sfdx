/**
 * @File Name          : GetCasesToBeRoutedCmd.cls
 * @Description        : 
 * Returns the cases that should be routed
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/11/2024, 1:53:34 AM
**/
public inherited sharing class GetCasesToBeRoutedCmd {

	final List<Case> recList;
	final Map<Id, Case> oldMap;
	final List<Case> validRecList;

	public ID skillCasesQueueId {get; set;}


	public GetCasesToBeRoutedCmd(
		List<Case> recList,
		Map<Id, Case> oldMap
	) {
		this.recList = recList;
		this.oldMap = oldMap;
		this.validRecList = new List<Case>();
	}

	public List<Case> execute() {
		if (
			(recList == null) ||
			recList.isEmpty()
		) {
			return validRecList;
		}
		// else...
		
		for (Case rec : recList) {
			Case oldRec = oldMap?.get(rec.Id);
			
			if (checkChanges(rec, oldRec) == true) {
				validRecList.add(rec);
			}
		}
		return validRecList;
	}
	
	@TestVisible
	Boolean checkChanges(
		Case newRec,
		Case oldRec
	) {
		Boolean result = false;

		if (oldRec == null) {
			result = checkChangesOnInsertedRecord(newRec);
		}
		return result;
	}

	@TestVisible
	Boolean checkChangesOnInsertedRecord(Case newRec) {
		Boolean result = (
			(
				(
					(newRec.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK) ||
					(newRec.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_API) 
				) &&
				String.isBlank(newRec.Subject) &&
				String.isBlank(newRec.Description)
			) ||
			(
				(newRec.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL) &&
				(newRec.OwnerId == skillCasesQueueId)
			) 
		);
		return result;
	}
	
}