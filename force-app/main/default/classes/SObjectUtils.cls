/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-15-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class SObjectUtils {
	public static String getObjectNameById(String objectId) {
		String name;
		try {
			name = objectId != null
				? ((Id) objectId).getSObjectType().getDescribe().getName()
				: null;
		} catch (Exception ex) {
			name = null;
		}
		return name;
	}
	public static Schema.SObjectType getObjectTypeByName(String name) {
		Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
		return globalMap.containsKey(name) ? globalMap.get(name) : null;
	}
	public static Schema.SObjectField getObjectFieldByName(
		Schema.SObjectType objType,
		String fieldName
	) {
		Schema.DescribeSObjectResult objDescribeSObject = objType.getDescribe();
		Map<String, Schema.SObjectField> mapFields = objDescribeSObject.fields.getMap();
		return mapFields.containsKey(fieldName)
			? mapFields.get(fieldName)
			: null;
	}
}