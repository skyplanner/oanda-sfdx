/**
 * @File Name          : PL_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/27/2020, 2:57:19 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   acantero     Initial Version
**/
public class PL_MIFID_NID_Validator implements ExpressionValidator {

    public String validate(String val) {
        Integer result;
        Integer checksum;
        Integer C1 = Integer.valueOf(val.substring(0, 1));
        Integer C2 = Integer.valueOf(val.substring(1, 2));
        Integer C3 = Integer.valueOf(val.substring(2, 3));
        Integer C4 = Integer.valueOf(val.substring(3, 4));
        Integer C5 = Integer.valueOf(val.substring(4, 5));
        Integer C6 = Integer.valueOf(val.substring(5, 6));
        Integer C7 = Integer.valueOf(val.substring(6, 7));
        Integer C8 = Integer.valueOf(val.substring(7, 8));
        Integer C9 = Integer.valueOf(val.substring(8, 9));
        Integer valLength = val.length();
        if (valLength == 11) {
            Integer C10 = Integer.valueOf(val.substring(9, 10));
            checksum = Integer.valueOf(val.substring(10));
            result = 
                (C1 * 1) +
                (C2 * 3) +
                (C3 * 7) +
                (C4 * 9) +
                (C5 * 1) +
                (C6 * 3) +
                (C7 * 7) +
                (C8 * 9) +
                (C9 * 1) +
                (C10 * 3);
            Integer lastDigit = getLastDigit(result);
            result = 10 - lastDigit;
            //...
        } else if (valLength == 10) {
            checksum = Integer.valueOf(val.substring(9));
            result = 
                (C1 * 6) +
                (C2 * 5) +
                (C3 * 7) +
                (C4 * 2) +
                (C5 * 3) +
                (C6 * 4) +
                (C7 * 5) +
                (C8 * 6) +
                (C9 * 7);
            result = Math.mod(result, 11);
            //...
        } else {
            return null;
        }
        if (checksum != result) {
            return System.Label.MifidValPasspChecksumError;
        }
        //else...
        return null;
    }

    static Integer getLastDigit(Integer val) {
        String temp = String.valueOf(val);
        Integer result = Integer.valueOf(temp.substring(temp.length() - 1));
        return result;
    }
    
}