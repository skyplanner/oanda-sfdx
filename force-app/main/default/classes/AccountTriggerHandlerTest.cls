@isTest
public class AccountTriggerHandlerTest {

    private static Date birthday = System.today().addYears(-36);
    private static final Integer QTY = 100;

    @isTest
    private static void setBirthdateTest(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        insert liveAccount;
        Account acc1;
        Test.startTest();
            acc1 = new Account(
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                PersonEmail = 'person@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            );
            insert acc1;
        Test.stopTest();
        acc1 = [SELECT Id, Birthdate_DR__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            birthday.month() + '/' + 
                birthday.day() + '/' + 
                birthday.year(),
            acc1.Birthdate_DR__c,
            'The dates does not match');
    }

    @isTest
    private static void setBirthdateBulkTest(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        insert liveAccount;
        Test.startTest();
            List<Account> accList = new List<Account>();        
            for (Integer i = 0; i < QTY; i++) {
                accList.add(new Account(
                    FirstName = 'testname',
                    LastName = 'bulkLastName',
                    Phone = '7861234567',
                    PersonEmail = 'personbulk' + i + '@spemail.com',
                    RecordTypeId = personAccountRecordType.Id,
                    fxAccount__c = liveAccount.Id
                ));
            }
            insert accList;
        Test.stopTest();
        accList = [SELECT Id, Birthdate_DR__c FROM Account WHERE LastName = 'bulkLastName'];
        System.assertEquals(QTY, accList.size(), 'The account number does not match');
        for(Account acc : accList){
            System.assertEquals(
                birthday.month() + '/' + 
                    birthday.day() + '/' + 
                    birthday.year(),
                acc.Birthdate_DR__c,
                'The dates does not match');
        }
    } 

    @isTest
    private static void validateTaxIdTest(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        liveAccount.Government_ID__c = '12333321012';
        insert liveAccount;        
        Account acc1;
        Test.startTest();           
            acc1 = new Account(
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                PersonEmail = 'person@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            );
            insert acc1;
        Test.stopTest();
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            false,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');
    }

    @isTest
    private static void validateTaxIdInvalidCharRepetitionTest1(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        liveAccount.Government_ID__c = '11111223344';
        insert liveAccount;        
        Account acc1;
        Test.startTest();           
            acc1 = new Account(
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                PersonEmail = 'person@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            );
            insert acc1;
        Test.stopTest();
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            true,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');
    }

    @isTest
    private static void validateTaxIdInvalidTooShortTest1(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        liveAccount.Government_ID__c = '112345';
        insert liveAccount;        
        Account acc1;
        Test.startTest();           
            acc1 = new Account(
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                PersonEmail = 'person@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            );
            insert acc1;
        Test.stopTest();
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            true,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');
        //get a valid format onupdate
        liveAccount.Government_ID__c = '11223545';
        update liveAccount;
        update acc1;
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            false,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');
        //get an invalid format by empty on update
        liveAccount.Government_ID__c = '';
        update liveAccount;
        update acc1;
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            true,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');            
    }

    @isTest
    private static void validateTaxIdInvalidCustomMetadataTest1(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday;
        liveAccount.Government_ID__c = '0123456789';
        insert liveAccount;        
        Account acc1;
        Test.startTest();          
            acc1 = new Account(
                FirstName = 'testname1',
                LastName = 'abc',
                Phone = '3051234567',
                PersonEmail = 'person@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            );
            insert acc1;
        Test.stopTest();
        acc1 = [SELECT Id, Tax_ID_Format_Review__c FROM Account WHERE Id =: acc1.Id];
        System.assertEquals(
            true,
            acc1.Tax_ID_Format_Review__c,
            'The field was not checked properly');       
    }

    @isTest
    private static void setLeadConversionOwner() {
        Account acc1 = new Account(
            FirstName = 'testname1',
            LastName = 'abc',
            Phone = '3051234567');
        insert acc1;
        
        acc1 =
            [SELECT
                OwnerId,
                Lead_Conversion_Owner__c
            FROM Account
            WHERE Id =: acc1.Id];
        
        System.assert(
            String.isNotBlank(acc1.Lead_Conversion_Owner__c) &&
            acc1.OwnerId == acc1.Lead_Conversion_Owner__c,
            'The Lead_Conversion_Owner__c field was not set with OwnerId value');
    }

    @isTest
    private static void setDemoConversionScoreSnapshot() {
        Datetime yesterday = Datetime.now().addDays(-1);
        Datetime beforeYest = yesterday.addDays(-1);

        Account acc1 = new Account(
            FirstName = 'testname1',
            LastName = 'abc',
            Phone = '3051234567',
            First_Live_fxAccount_DateTime__c = yesterday,
            First_Practice_fxAccount_DateTime__c = beforeYest,
            Demo_Score__c = 15);
        insert acc1;

        acc1 =
            [SELECT
                Demo_Score__c,
                Demo_Conversion_Snapshot__c
            FROM Account
            WHERE Id =: acc1.Id];

        System.assert(
            acc1.Demo_Conversion_Snapshot__c == 15 &&
            acc1.Demo_Conversion_Snapshot__c == acc1.Demo_Score__c,
            'The Demo_Conversion_Snapshot__c field was not set with Demo_Score__c value');
    }
    

    @isTest
    private static void setOBcheckBasedOnKYCTest(){
  
        Account a = new Account(LastName='test account');
        insert a;
        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        liveAccount.Birthdate__c = birthday; //age 36
        insert liveAccount;
        Test.startTest();
        Account persAcc  = new Account(
                    FirstName = 'testname',
                    LastName = 'bulkLastName',
                    Phone = '7861234567',
                    PersonEmail = 'personbulk11111@spemail.com',
                    RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId(),
                    fxAccount__c = liveAccount.Id
                );
        
        insert persAcc;
        Account personAccTocheck1 = [SELECT Age_70__c,Age_70_OB_check__c, Name_Review__c, Name_Length_OB_check__c, fxAccount__c, RecordTypeId,
                                     Tax_ID_Format_Review__c, Tax_ID_Format_OB_check__c, Possible_Duplicate__c, Possible_Duplicate_OB_check__c
                                    FROM Account 
                                    WHERE id = :persAcc.Id ];

        // acc.Age_70_OB_check__c = !acc.Age_70__c ? automatedRev : manualRev;
        // acc.Name_Length_OB_check__c = !acc.Name_Review__c ? automatedRev : manualRev;
        // acc.Tax_ID_Format_OB_check__c = !acc.Tax_ID_Format_Review__c ? automatedRev : manualRev;
        // acc.Possible_Duplicate_OB_check__c = !acc.Possible_Duplicate__c ? automatedRev : manualRev;
        String automatedRev = 'Automated Review - Accepted';
        String manualRev = 'Automated Review - Manual Review Required';
        System.assertEquals(false, personAccTocheck1.Age_70__c, 'flag is not checked - automated review');
        System.assertEquals(automatedRev, personAccTocheck1.Age_70_OB_check__c, 'flag is not checked - automated review');

        System.assertEquals(false, personAccTocheck1.Name_Review__c, 'flag is not checked - automated review');
        System.assertEquals(automatedRev, personAccTocheck1.Name_Length_OB_check__c, 'flag is not checked - automated review');

        // System.assertEquals(true, personAccTocheck1.Tax_ID_Format_Review__c, 'flag is checked - manual review');
        // System.assertEquals(manualRev, personAccTocheck1.Tax_ID_Format_OB_check__c, 'flag is checked - manual review');

        System.assertEquals(false, personAccTocheck1.Possible_Duplicate__c, 'flag is not checked - automated review');
        System.assertEquals(automatedRev, personAccTocheck1.Possible_Duplicate_OB_check__c, 'flag is not checked - automated review');
        
    } 

    @isTest
    private static void checkForPhoneDuplicatesOnInsert(){
  
        Account acc1 = new Account(
            FirstName = 'testname1',
            LastName = 'abc',
            Phone = '5432154321');
        insert acc1;

        Account acc1After = [SELECT Phone_DR__c FROM Account WHERE Id=:acc1.Id];
        System.assertEquals('5432154321', acc1After.Phone_DR__c);
  
        Account acc2 = new Account(
            FirstName = 'testname2',
            LastName = 'abc',
            Phone = '+543-2154-321');
        insert acc2;

        Account acc2After = [SELECT Phone_DR__c FROM Account WHERE Id=:acc2.Id];
        System.assertEquals('5432154321', acc2After.Phone_DR__c);
        
    } 

    @isTest
    private static void checkForPhoneDuplicatesOnUpdate(){
        
        Account acc = new Account(
            FirstName = 'testname2',
            LastName = 'abc');
        insert acc;

        Account accAfterInsert = [SELECT Phone_DR__c FROM Account WHERE Id=:acc.Id];
        System.assertEquals(null, accAfterInsert.Phone_DR__c);

        acc.Phone = '+485432154321';
        update acc;

        Account accAfterUpdate = [SELECT Phone_DR__c FROM Account WHERE Id=:acc.Id];
        System.assertEquals('5432154321', accAfterUpdate.Phone_DR__c);
        
    } 

    @IsTest
    static void checkIntroducingBrokerChange()
    {
        
        Account introducingBroker = new TestDataFactory().createTestIntroducingBroker('test1', 'test1@test.com');

        Account account = new TestDataFactory().createTestAccount();
    
        fxAccount__c liveAccount1 = new fxAccount__c();
        liveAccount1.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount1.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount1.Account__c = account.Id;
        liveAccount1.Introducing_Broker__c = introducingBroker.Id;
        liveAccount1.fxTrade_One_Id__c = '123456';
        liveAccount1.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount1.Birthdate__c = birthday;

        fxAccount__c liveAccount2 = new fxAccount__c();
        liveAccount2.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount2.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount2.Account__c = account.Id;
        liveAccount2.Introducing_Broker__c = introducingBroker.Id;
        liveAccount1.fxTrade_One_Id__c = '1234567';
        liveAccount2.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount2.Birthdate__c = birthday;

        insert new List<fxAccount__c>{liveAccount1, liveAccount2};


        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        introducingBroker.Introducing_Broker_Name__c = 'test2';

        update introducingBroker;

        Test.getEventBus().deliver();
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
        for(AsyncApexJob job : jobs) {
            Assert.areEqual('Completed', job.Status, 'The job should be completed.');
            Assert.areEqual(0, job.NumberOfErrors, 'There should be no errors.');
        }
    } 
}