/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-23-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiPhone {
    public String TYPE {get; set;}

    public String phone {get; set;}

    public Integer phone_id {get; set;}

    public Integer priority {get; set;}
}