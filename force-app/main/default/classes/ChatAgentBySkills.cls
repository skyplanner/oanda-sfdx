/**
 * @File Name          : ChatAgentBySkills.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/25/2024, 12:50:13 AM
**/
public without sharing class ChatAgentBySkills {

    public static final Integer NO_CAPACITY = 0;

    final AgentBySkills agentsManager;

    public ChatAgentBySkills() {
        this(
            OmnichanelSettings.getRequiredDefault().chatChannelDevName
        );
    }

    public ChatAgentBySkills(String chatChannelDevName) {
        ServiceLogManager.getInstance().log(
			'ChatAgentBySkills -> ' + 
			'chatChannelDevName: ' + chatChannelDevName, // msg
			ChatAgentBySkills.class.getName() // category
		);
        List<String> servPresenceStatusIdList = 
            SPChatUtil.getServPresenceStatusIdList(chatChannelDevName);
        agentsManager = new AgentBySkills(servPresenceStatusIdList);
    }

    public Set<String> existsCompatibleAgents(
        Set<String> skillDevNameSet
    ) {
        return existsCompatibleAgents(skillDevNameSet, NO_CAPACITY);
    }

    public Set<String> existsCompatibleAgents(
        Set<String> skillDevNameSet, 
        Decimal capacityWeight
    ) {
        ServiceLogManager.getInstance().log(
			'existsCompatibleAgents -> ' +
			'capacityWeight: ' + capacityWeight +
			', skillDevNameSet: (details)', // msg
			ChatAgentBySkills.class.getName(), // category
			skillDevNameSet // details
		);
        Set<String> skillCodeSet = 
            ServiceSkillManager.getSkillCodeSetFromDevNames(skillDevNameSet);   
        Set<String> result = 
            agentsManager.existsCompatibleAgents(skillCodeSet, capacityWeight);
        ServiceLogManager.getInstance().log(
            'existsCompatibleAgents -> result', // msg
            ChatAgentBySkills.class.getName(), // category
            result // details
        );	
        return result;
    }
    
}