/**
 * Created by Agnieszka Kajda on 03/11/2023.
 */

public with sharing class RetryLockedRecordsQueueable implements Queueable{

    String method = '';
    List<Id> recs = new List<Id>();

    public RetryLockedRecordsQueueable(List<Id> recs, String method){
        this.method = method;
        this.recs = recs;
    }
    public void execute(QueueableContext context){
        //method cannot be generic due to different queries and functionalities it needs to perform

        switch on method{
            when 'updateCaseChatTranscriptCount' {
                List<LiveChatTranscript> liveChatTranscriptList = [SELECT Id, CaseId FROM LiveChatTranscript WHERE Id IN :recs];
                LiveChatTranscriptHandler h = new LiveChatTranscriptHandler();
                h.updateCaseChatTranscriptCount(liveChatTranscriptList);

            }
            when 'postprocessLeadConversion' {
                List<Lead> leads = [SELECT Id FROM Lead WHERE Id IN :recs];
                LeadUtil.postprocessLeadConversion(leads);
            }
        }

    }

}