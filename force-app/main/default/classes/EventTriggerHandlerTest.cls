@isTest
private class EventTriggerHandlerTest {

    private static final String EVENT_TYPE_NAME = 'TestValue';

    @isTest
    static void shouldGroupEventsBasedOnReplayCount() {
        Integer republishEventsCount = 1;
        Integer runEventsCount = 2000;

        List<SObject> events = new List<Sobject>();

        events.add(createApiEvent(EVENT_TYPE_NAME, 11));

        for(Integer i = 1; i <= (republishEventsCount + runEventsCount); i++) {
            events.add(createApiEvent(EVENT_TYPE_NAME, 0));
        }

        Map<EventTriggerHandler.EventExecutionAction, List<SObject>> grouppedEvents = 
            new EventTriggerHandler().groupEventsByDesiredAction(events);

        Assert.areEqual(runEventsCount, grouppedEvents.get(EventTriggerHandler.EventExecutionAction.RUN).size());
        Assert.areEqual(1, grouppedEvents.get(EventTriggerHandler.EventExecutionAction.TERMINATE).size());
        Assert.areEqual(republishEventsCount, grouppedEvents.get(EventTriggerHandler.EventExecutionAction.REPUBLISH).size());        
    }

    @isTest
    static void shouldRepublishEvents() {

        List<SObject> events = new List<Sobject>();
        events.add(createApiEvent(EVENT_TYPE_NAME, 0));

        new EventTriggerHandler().republishEvents(events);
    }

    @isTest
    static void shouldTerminateEvents() {

        List<SObject> events = new List<Sobject>();
        events.add(createApiEvent(EVENT_TYPE_NAME, 11));

        new EventTriggerHandler().terminateEvents(events);
    }

    private static Api_Event_Immediate__e createApiEvent(String typeName, Integer replayCount) {

        Api_Event_Immediate__e event = new Api_Event_Immediate__e();
        event.Handler_Type_Name__c = typeName;
        event.Replay_Counter__c = replayCount;

        return event;
    }
}