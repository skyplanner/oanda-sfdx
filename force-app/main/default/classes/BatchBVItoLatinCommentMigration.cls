/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchBVItoLatinCommentMigration implements
    Database.Batchable<sObject>, Database.AllowsCallouts, Database.RaisesPlatformEvents, BatchReflection {
    public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public String instanceName;
    public String action;

    public BatchBVItoLatinCommentMigration(String filter, String insName, String act) {
        this.query = 'SELECT Id, Name, ' +
            '    fxAccount__c, ' +
            '    fxAccount__r.Name, ' +
            '    Search_Reference__c, ' +
            '    Is_Alias_Search__c, ' +
            '    Search_Reason__c, ' +
            '    Search_Text__c, ' +
            '    Search_Id__c, ' +
            '    Is_Monitored__c, ' +
            '    Custom_Division_Name__c, ' +
            '    Search_Parameter_Birth_Year__c, ' +
            '    Search_Parameter_Citizenship_Nationality__c, ' +
            '    Search_Parameter_Mailing_Country__c, ' +
            '    Match_Status__c, ' +
            '    Total_Hits__c, ' +
            '    Report_Link__c, ' +
            '    Case_Management_Link__c, ' +
            '    Search_Reference_Before_OGM_Migration__c ' +
            'FROM Comply_Advantage_Search__c ' +
            'WHERE Is_Monitored__c = TRUE ' +
            (String.isNotBlank(filter) ? filter : '');
        this.instanceName = insName;
        this.action = act;
    }
    
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Name, fxAccount__c, fxAccount__r.Name, Search_Reference__c, Is_Alias_Search__c, Search_Reason__c, Search_Text__c, Search_Id__c, Is_Monitored__c, Custom_Division_Name__c, Search_Parameter_Birth_Year__c, Search_Parameter_Citizenship_Nationality__c, Search_Parameter_Mailing_Country__c, Match_Status__c, Total_Hits__c, Report_Link__c, Case_Management_Link__c, Search_Reference_Before_OGM_Migration__c FROM Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        System.debug('scope: ' + scope);
        ComplyAdvantageCommentsMigrationHelper migration = new ComplyAdvantageCommentsMigrationHelper(
            instanceName
        );

        migration.call(action, new Map<String, Object> {
            'records' => scope
        });
    }

    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (!Test.isRunningTest()) {
            try {
                EmailUtil.sendEmailForBatchJob(bc.getJobId());
            }
            catch (Exception e) {}
        }
	}

    
}