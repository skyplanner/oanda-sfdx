/**
 * @File Name          : RemoveNonHvcCasesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/19/2024, 12:01:38 AM
**/
public inherited sharing class RemoveNonHvcCasesCmd {

	public Boolean completed {get; private set;}
	public Integer count {get; private set;}

	public void execute() {
		System.debug('RemoveNonHvcCasesCmd -> execute');
		Integer recordsLimit = RemoveNonHvcCasesCmdSettings.recordsLimit;
		Datetime lastModifiedDate = 
			RemoveNonHvcCasesCmdSettings.lastModifiedDate;

		List<Non_HVC_Case__c> records = NonHvcCaseRepo.getModifiedBeforeDate(
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED, // status
			lastModifiedDate, // lastModifiedDate
			recordsLimit // queryLimit
		);
		count = records.size();
		completed = (records.size() < recordsLimit);

		delete records;
		
		ServiceLogManager.getInstance().log(
			'Removed: ' + records.size(), // msg
			RemoveNonHvcCasesCmd.class.getName() // category
		);
	}

}