/**
 * @File Name          : AuthenticationCodeEmailProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/8/2023, 3:49:54 AM
**/
public inherited sharing class AuthenticationCodeEmailProcess {

	public static final String EMAIL_ADDRESS = 
		Settings__c.getValues('Default')?.Auth_code_OrgWideEmail__c;

	final ID caseId;
	final Contact contactObj;
	final String languageCode;

	EmailTemplate template;
	String emailTemplateDevName;

	public AuthenticationCodeEmailProcess(
		ID caseId,
		Contact contactObj,
		String languageCode
	) {
		this.caseId = caseId;
		this.contactObj = contactObj;
		this.languageCode = languageCode;
	}

	public Boolean sendAuthenticationCodeEmail() {
		emailTemplateDevName = SPSettingsManager.getSettingOrDefault(
			MessagingConst.MSG_AUTH_EMAIL_TEMPLATE_SETTING, // settingName
			'Case_Authentication_Code' // defaultValue
		);
		prepareTemplate();
		Boolean result = sendEmail();
		return result;
	}

	@TestVisible
	void prepareTemplate() {
		firstTemplateSearch();

		if (template == null) {
			alternativeTemplateSearch();
		}
		
		if (template == null) {
			template = EmailTemplateRepo.getByDevName(emailTemplateDevName);
		}
	}

	@TestVisible
	void firstTemplateSearch() {
		String languageName = 
			ServiceLanguagesManager.getLanguage(languageCode);
		template = EmailTemplateUtil.getTemplateByLanguage(
			emailTemplateDevName, // namePrefix
			languageName // language
		);
	}

	@TestVisible
	void alternativeTemplateSearch() {
		String alternativeLangName = 
			ServiceLanguagesManager.getAlternativeName(languageCode);
		template = EmailTemplateUtil.getTemplateByLanguage(
			emailTemplateDevName, // namePrefix
			alternativeLangName // language
		);
	}

	@TestVisible
	Boolean sendEmail() {
		if (template == null) {
			Logger.error(
				'Email Template not found, Dev Name: ' + emailTemplateDevName, // msg
				AuthenticationCodeEmailProcess.class.getName() // category
			);
			return false;
		}
		// else...
		Boolean result = false;
		ID senderId = null;
		
		if (String.isNotBlank(EMAIL_ADDRESS)) {
			senderId = OrgWideEmailAddressRepo.getByAddress(EMAIL_ADDRESS)?.Id;
		}

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new List<String> { contactObj.Email });
		mail.setTargetObjectId(contactObj.Id);
		mail.setSaveAsActivity(true);
		mail.setWhatId(caseId);
		mail.setTemplateId(template.Id);

		if (senderId != null) {
			mail.setOrgWideEmailAddressId(senderId);
		}

		List<Messaging.SendEmailResult> emailResults = 
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		
		if (
			(emailResults != null) &&
			(emailResults.isEmpty() == false)
		) {
			// TEST DEBUG
			// Logger.info(
			// 	'sendAuthenticationCodeEmail -> result', // msg
			// 	AuthenticationCodeEmailProcess.class.getName(), // category
			// 	'isSuccess: ' + emailResults[0].isSuccess()
			// );
			
			result = emailResults[0].isSuccess();

			if (result == false) {
				Logger.error(
					'sendAuthenticationCodeEmail -> errors', // msg
					AuthenticationCodeEmailProcess.class.getName(), // category
					emailResults[0].getErrors() // details
				);
			}
		}
		
		return result;
	}

}