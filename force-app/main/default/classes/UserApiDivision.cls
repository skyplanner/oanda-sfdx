/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 07-28-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiDivision {
    public String abbreviation {get; set;}

    public String description {get; set;}

    public Integer division_id {get; set;}

    public Integer end_of_day {get; set;}

    public String end_of_day_tz {get; set;}

    public Decimal tax_deduction_percentage {get; set;}
}