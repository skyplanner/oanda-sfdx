/**
 * @File Name          : SPPicklistUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/17/2021, 11:25:09 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/17/2021, 11:23:54 AM   acantero     Initial Version
**/
public without sharing class SPPicklistUtil {

    public static List<String> getPicklistValues(SObjectField field) {
        Schema.DescribeFieldResult fieldMetadata = field.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldMetadata.getPicklistValues();
        List<String> picklistValues = new List<String>();
        for(Schema.PicklistEntry picklistEntry : picklistEntries) {
            picklistValues.add(picklistEntry.getValue());
        }
        return picklistValues;
    }

    public static Boolean isValidValue(SObjectField field, String picklistValue) {
        if (String.isBlank(picklistValue)) {
            return false;
        }
        //else...
        Schema.DescribeFieldResult fieldMetadata = field.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldMetadata.getPicklistValues();
        List<String> picklistValues = new List<String>();
        for(Schema.PicklistEntry picklistEntry : picklistEntries) {
            if (picklistValue == picklistEntry.getValue()) {
                return true;
            }
        }
        //else...
        return false;
    }


}