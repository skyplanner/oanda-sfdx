/**
 * @File Name          : SWorkItemUpdater.cls
 * @Description        : 
 * It is responsible for managing the changes to be made to the WorkItem during
 * the routing process.
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/23/2024, 3:07:03 PM
**/
public interface SWorkItemUpdater {

	List<SObject> getRecordsToUpdate(BaseServiceRoutingInfo routingInfo);

}