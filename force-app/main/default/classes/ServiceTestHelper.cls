/**
 * @File Name          : ServiceTestHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/12/2024, 2:37:31 PM
**/
@IsTest
public without sharing class ServiceTestHelper {

	public static String getChatKey(String letter) {
		String result = '';
		
		for (Integer i = 0; i < 200; i++) {
			result += letter;
		}
		return result;
	}

	public static ID getChatChannelId() {
		return getRequiredChannelId(
			Schema.LiveChatTranscript.SObjectType.getDescribe().getName()
		);
	}

	public static ID getRequiredChannelId(String relatedEntity) {
		return [
			SELECT Id 
			FROM ServiceChannel 
			WHERE RelatedEntity = :relatedEntity
			LIMIT 1
		].Id;
	}
	
}