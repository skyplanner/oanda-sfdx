/**
 * @File Name          : AgentWork_Trigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:10:30 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/15/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class AgentWork_Trigger_Test {

    // @testSetup
    // static void setup(){
    // }

    @isTest
    static void insert_test() {
        Boolean agentWorkTriggerFired = false;
        ID caseChannelId = OmnichanelRoutingTestDataFactory.getCaseChannelId();
        //ID queueId = OmnichanelRoutingTestDataFactory.getRequiredSkillsQueueId();
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0002');
        System.runAs(supervisor) {
            ID userId = UserInfo.getUserId();
            ID hvcAccountId = OmnichanelRoutingTestDataFactory.createHvcAccount(OmnichanelConst.OANDA_CORPORATION);
            Case case1 = new Case(
                Origin = 'Custom',
                AccountId = hvcAccountId,
                Subject = 'ACL Email ' + System.Now(),
                Priority = 'Normal'
            );
            insert case1;
            System.debug('case1: ' + case1.Id);
            System.debug('caseChannelId: ' + caseChannelId);
            PendingServiceRouting psr1 = new PendingServiceRouting(
                CapacityWeight = 1,
                IsReadyForRouting = false,
                RoutingModel  = 'MostAvailable',
                RoutingPriority = 1,
                RoutingType = 'SkillsBased',
                ServiceChannelId = caseChannelId,
                WorkItemId = case1.Id
            );
            //this is a fake psr, i do not want it take skills
            PendingServiceRouting_Trigger_Utility.enabled = false; //Important!!
            insert psr1;
            //...
            try {
                AgentWork work = new AgentWork();
                work.ServiceChannelId = caseChannelId;
                work.WorkItemId = case1.Id;
                work.UserId = userId;
                work.PendingServiceRoutingId = psr1.Id;
                insert work;
            } catch (Exception ex) {
                agentWorkTriggerFired = true;
            }
        }
        Test.stopTest();
        System.assert(agentWorkTriggerFired);
     }

}