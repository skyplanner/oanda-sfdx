public with sharing class CurrentDeskLiveUserCallOut implements Queueable,Database.AllowsCallouts
{
    private CurrentDeskSync.LiveUser liveUser;

    public CurrentDeskLiveUserCallOut(CurrentDeskSync.LiveUser userInfo)
    {
        liveUser = userInfo;          
    }
    
    public void execute(QueueableContext context)
    {
       Http http = new Http();
       HttpResponse response = http.send(getLiveRegistrationRequest());
       getLiveRegistrationResponse(response);
    }

    private HttpRequest getLiveRegistrationRequest() 
    { 
        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl + '/registration/client/individual');
        httpRequest.setMethod('POST');
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setTimeout(120000);

        string requestBody = getLiveRegistrationRequestBody();
		System.debug(requestBody);
        httpRequest.setBody(requestBody); 
        return httpRequest;
    }
   
    private string getLiveRegistrationRequestBody() 
    { 
        Map<String, Object> regRequestFields = new Map<String, Object>();
        regRequestFields.put('account-code-id', liveUser.accountCodeId);
        regRequestFields.put('account-language-id', liveUser.accountLanguageId);
        regRequestFields.put('account-password', StringUtil.replaceIfBlank(liveUser.accountPassword, 'N/A'));
        regRequestFields.put('account-phone-id', StringUtil.replaceIfBlank(liveUser.accountPhoneId, 'N/A'));
        regRequestFields.put('account-currency-id', liveUser.accountCurrencyId);
        regRequestFields.put('account-preference-id', liveUser.accountPreferenceId);
        regRequestFields.put('account-trading-platform-id', liveUser.accountTradingPlatformId);
		regRequestFields.put('account-live-status', true);

		// SP-9161: IF an account is RFF'd with Funding Limit flag checked,
		// then an API call is triggered to create an account in CurrentDesk
		// with "account-approved-status" = false, so that the account
		// status in CD is set to "Missing Documents".
		// IF and account if RFF'd with Funding Limit flag unchecked, 
		// then an API call is triggered to create an account in CurrentDesk with 
		// "account-approved-status" = True,
		// so that the account staus in CD is set to "Approved".
		regRequestFields.put('account-approved-status', !liveUser.isFundingLimited);

        regRequestFields.put('individual-address',
			StringUtil.replaceIfBlank(liveUser.individualAddress, 'N/A'));
        regRequestFields.put('individual-address-city',
			StringUtil.replaceIfBlank(liveUser.individualAddressCity, 'N/A'));
        regRequestFields.put('individual-address-country-id',
			liveUser.individualAddressCountryId);
        regRequestFields.put('individual-address-months',
			liveUser.individualAddressMonths);
        regRequestFields.put('individual-address-postal-code',
			StringUtil.replaceIfBlank(liveUser.individualAddressPostalCode, '00000'));
        regRequestFields.put('individual-birth-date',
			StringUtil.replaceIfBlank(liveUser.individualBirthDate, 'N/A'));
        regRequestFields.put('individual-citizenship-country-id',
			liveUser.individualCitizenshipCountryId);
        regRequestFields.put('individual-email',
			StringUtil.replaceIfBlank(liveUser.individualEmail, 'N/A'));
        regRequestFields.put('individual-employment-industry',
			StringUtil.replaceIfBlank(liveUser.individualEmploymentIndustry, 'N/A'));
        regRequestFields.put('individual-employment-status-id',
			liveUser.individualEmploymentStatusId);
        regRequestFields.put('individual-occupation',
			StringUtil.replaceIfBlank(liveUser.individualOccupation, 'N/A'));
        regRequestFields.put('individual-first-name',
			StringUtil.replaceIfBlank(liveUser.individualFirstName, 'N/A'));
        regRequestFields.put('individual-middle-name',
			StringUtil.replaceIfBlank(liveUser.individualMiddleName, 'N/A'));
        regRequestFields.put('individual-gender-id', liveUser.individualGenderId);
        regRequestFields.put('individual-identification-number',
			StringUtil.replaceIfBlank(liveUser.individualIdentificationNumber, 'N/A'));
        regRequestFields.put('individual-identification-type-id',
			liveUser.individualIdentificationTypeId);
        regRequestFields.put('individual-last-name',
			StringUtil.replaceIfBlank(liveUser.individualLastName, 'N/A'));
        regRequestFields.put('individual-mobile',
			StringUtil.replaceIfBlank(liveUser.individualMobile, 'N/A'));
        regRequestFields.put('individual-residence-country-id',
			liveUser.individualResidenceCountryId);
        regRequestFields.put('individual-telephone',
			StringUtil.replaceIfBlank(liveUser.individualTelephone, 'N/A'));
        regRequestFields.put('individual-title-id', liveUser.individualTitleId);

        regRequestFields.put('trading-futures-experience-id',
			liveUser.tradingFuturesExperienceId);
        regRequestFields.put('trading-options-experience-id',
			liveUser.tradingOptionsExperienceId);
        regRequestFields.put('trading-shares-experience-id',
			liveUser.tradingSharesExperienceId);
        regRequestFields.put('trading-spot-experience-id',
			liveUser.tradingSpotExperienceId);

        if (String.isNotBlank(liveUser.proxyDomain))
            regRequestFields.put('proxy-domain',
				StringUtil.replaceIfBlank(liveUser.proxyDomain, 'N/A'));

		if (liveUser.reqCustomParameters != null &&
				!liveUser.reqCustomParameters.isEmpty())
			regRequestFields.putAll(liveUser.reqCustomParameters);
		
            String lastName,firstName;
                if(!String.isBlank(liveUser.aliasName))
                {
                    lastName =   liveUser.aliasName.substringAfter(' ');
                    firstName =  liveUser.aliasName.subStringBefore(' ');
                }else{
                    lastName = liveUser.individualFirstName;
                    firstName = liveUser.individualLastName;
                }
                   
                regRequestFields.put('custom-parameter-value-03',firstName);
                regRequestFields.put('custom-parameter-value-04',lastName);
		return JSON.serializePretty(regRequestFields);
    }

    private void getLiveRegistrationResponse(HttpResponse response) { 
        //handle timeout error. retry one more time
        if(response.getStatusCode() != 200)
        {
            Http http = new Http();
            response = http.send(getLiveRegistrationRequest());
        }

        if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody())) {
            //handle succesfull registration
			Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
			integer clientId = Integer.valueOf(results.get('ClientId'));
			
			Id fxAccountId = liveUser.fxAccountId;
			fxAccount__c fxa = [Select CD_Client_ID__c From fxAccount__c Where Id=:fxAccountId Limit 1];
			fxa.CD_Client_ID__c = clientId;
			update fxa;
        }
        else 
        {
            // Build error info
            string subject = 'CurrentDesk Live Registraion failed for user : ' + liveUser.fxAccountDetails.Name;
            string emailBody = 'Repsonse: \n ' + response.getBody();
            emailBody +=  '\n\nSalesforce link: \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + liveUser.fxAccountId;
            emailBody += '\n\nRequest Details: \n' +  getLiveRegistrationRequestBody();
            
            // Save error log
            Logger.error(
                subject,
                Constants.LOGGER_CURRENT_DESK_CATEGORY,
                emailBody);

            // Send email
            EmailUtil.sendEmail('salesforce@oanda.com', subject, emailBody);
        }
    }
}