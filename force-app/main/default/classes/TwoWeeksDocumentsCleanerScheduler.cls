/**
 * Scheduler class for run the BatchDocumentsCleaner 
 * It covers the documents created two weeks ago
 * @author Ariel Cantero, Skyplanner LLC
 * @since 07/10/2019
 */
global without sharing class TwoWeeksDocumentsCleanerScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
		BatchDocumentsCleaner docCleanerBatch = new BatchDocumentsCleaner(true);
		if (docCleanerBatch.isValid) {
			Database.executeBatch(docCleanerBatch, BatchDocumentsCleaner.BATCH_EXECUTION_SCOPE);
		}
	}
	
}