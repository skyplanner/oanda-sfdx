/* Name: BatchReactivationUpdate
 * Description : Apex Class to update Reactivation Tick Box  as mentioned SP-9153
 * Author: Sahil Handa 
 * Date : 2021-05-17
 */
public class BatchReactivationUpdate implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable 
{
    
    private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public static final Integer BATCH_SIZE = 200;
    
    public BatchReactivationUpdate() 
    {
     /* //   execute below for  scheduling the AMR transfer
            BatchReactivationUpdate b=  new BatchReactivationUpdate('SELECT Id, Last_Reactivation_Date__c ,Eligible_for_Reactivation__c, Reactivation_Eligibility_Request__c  FROM Account where Eligible_for_Reactivation__c = true or Reactivation_Eligibility_Request__c = true') ;
            System.schedule('BatchReactivationUpdate', '0 1 0 L * ?', b);          
	*/
    }

    public BatchReactivationUpdate(String q) 
    {
        query = q;
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Last_Reactivation_Date__c ,Eligible_for_Reactivation__c, Reactivation_Eligibility_Request__c  FROM Account WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> batch) 
    {
        System.debug('batch ===> ' + batch);
        List<Account> accountList = (List<Account>) batch;
        List<Account> accToUpdate = new List<Account>();
        
        Integer sixthMonthBack = Date.today().addMonths(-6).month();
        Integer sixthYearBack = Date.today().addMonths(-6).year();
        
        for(Account acc :accountList){
            if( (acc.Last_Reactivation_Date__c).month() == sixthMonthBack &&  (acc.Last_Reactivation_Date__c).year() == sixthYearBack){
                acc.Eligible_for_Reactivation__c = false;
                acc.Reactivation_Eligibility_Request__c = false;
                accToUpdate.add(acc);
            }
        }
        if(accToUpdate.size() > 0)
        {
            try
            {
                update accToUpdate;
            }
            catch(Exception e)
            {   
                System.debug('Err'+e.getMessage());
            }
        }
    }
    
    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchReactivationUpdate(query), BATCH_SIZE);
    }

    public void finish(Database.BatchableContext BC) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
}