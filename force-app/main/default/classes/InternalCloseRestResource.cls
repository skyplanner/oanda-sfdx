/* Name: InternalCloseRestResource
* Description : New Apex Class to fxAccount close
* Author: Michal Piatek (mpiatek@oanda.com)
* Date : 2024 June 06
*/
@RestResource(urlMapping='/api/v1/users/internal_close')
global class InternalCloseRestResource {
    @HttpPost
    global static void doPost() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(req, res);

        try {
            Object payload = JSON.deserializeUntyped(context.getBodyAsString());
            PlatformEventDispatch.insertGenericAfter('FXA_INTERNAL_CLOSE', context.getBodyAsString());
            res.responseBody = Blob.valueOf('Records will be processed.');
            res.statusCode = 200;
            return;
        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }
}