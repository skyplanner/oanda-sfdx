/**
 * User API 'Grant API Access' action manager class
 */
public class UserApiActionGrantAPIAccess extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        Id fxAccountId = getfxAccountId(record);
        EnvironmentIdentifier identifier = getfxTradeUserId(record);
        UserApiUserPatchRequest.UserPatchRequestWrapper wrapper 
            = UserApiUserPatchRequest.getGrantApiAccessWrapper();
        UserCallout userCallout = new UserCallout();
        userCallout.updateUser(identifier, wrapper.user);
        String action = getActionLabel(record);
        auditTrailManager.saveAuditTrail(
            identifier.id, fxAccountId, action, wrapper.change);

        return action + ' action success!!!';
    }
    
    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return !user.user.apiEnabled;
    }
}