/**
 * Manager class for GBG Settings
 */
public class GbgSettings {
    
    // Key to identify general GBG setting record 
    // (not a for a particular division)
    private static final String INTEGRATION = 'Integration';
    // Division -> setting 
    private Map<String, GBG_Setting__mdt> rcdMap; 
    // Settings instance
    private static GbgSettings instance = null;

    /**
     * Constructor
     */
    private GbgSettings() {
        load();
    }

    /**
     * Get settings intance
     */
    public static GbgSettings getInstance() {
        if(instance == null) {
            instance = new GbgSettings();
        }
        return instance;
    }

    /**
     * Know what are the divisions
     * with GBG integration enabled
     */
    public List<String> getEnabledDivisions() {
        List<String> r = new List<String>();

        for(GBG_Setting__mdt rcd :rcdMap.values()) {
            if(rcd.Enabled__c) {
                if(String.isNotBlank(rcd.Division__c))
                    r.add(rcd.Division__c);
            }
        }

        return r;
    }

    /**
     * Know if GBG is enabled or not
     * (not according a particular division)
     */
    public Boolean isGbgEnabled() {
       return isDivisionEnabled(INTEGRATION);
    }

    /**
     * Know if GBG is enabled or not
     * (not according a particular division)
     */
    public Boolean isDivisionEnabled(String div) {
        if(!rcdMap.containsKey(div))
            return false;

        return rcdMap.get(div).Enabled__c;
    }

    /**
     * Load setting records
     */
    private void load() {
        rcdMap = new Map<String, GBG_Setting__mdt>();

        for(GBG_Setting__mdt rcd :
            [SELECT DeveloperName,
                Division__c, 
                Enabled__c
            FROM GBG_Setting__mdt
            LIMIT 1000])
        {
            rcdMap.put(
                String.isBlank(rcd.Division__c) ? 
                    rcd.DeveloperName :
                    rcd.Division__c,
                rcd);
        }
    }

}