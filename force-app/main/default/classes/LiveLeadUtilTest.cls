@isTest
private class LiveLeadUtilTest {

    static testMethod void checkConvertedLeadsTest() {
        Lead demoLead = TestDataFactory.getTestLead(false);
        Lead liveLead = TestDataFactory.getTestLead(true);
        
        insert demoLead;
        insert liveLead;
        
        fxAccount__c fxAccount = new fxAccount__c();
	    fxAccount.Account_Email__c = demoLead.Email;
	    fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    insert fxAccount;
        
        Live_Lead__c rc1 = new Live_Lead__c(LeadId__c = demoLead.Id, Email__c = demoLead.Email, fxTrade_User_Id__c = '1234', User_Name__c = 'testLead');
        Live_Lead__c rc2 = new Live_Lead__c(LeadId__c = liveLead.Id, Email__c = liveLead.Email, fxTrade_User_Id__c = '5678', User_Name__c = 'testLead1');
        
        List<Live_Lead__c> rcs = new List<Live_Lead__c>();
        rcs.add(rc1);
        rcs.add(rc2);
        
        insert rcs;
        
        List<Live_Lead__c> rcs1 = [select id, processed__c, Lead_Exists__c from Live_Lead__c where Id = :rc1.Id or Id = :rc2.Id];
        System.assertEquals(2, rcs1.size());
        
        for(Live_Lead__c liveLd : rcs1){
        	System.assertEquals(true, liveLd.processed__c);
        	System.assertEquals(true, liveLd.Lead_Exists__c);
        }
        
        //test lead is converted
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(liveLead.id);
        lc.setConvertedStatus('Hot');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        
        
        Live_Lead__c rc3 = new Live_Lead__c(LeadId__c = demoLead.Id, Email__c = demoLead.Email, fxTrade_User_Id__c = '1234', User_Name__c = 'testLead');
        Live_Lead__c rc4 = new Live_Lead__c(LeadId__c = liveLead.Id, Email__c = liveLead.Email, fxAccountId__c = fxAccount.Id, fxTrade_User_Id__c = '5678', User_Name__c = 'testLead1');
        
        List<Live_Lead__c> rcs2 = new List<Live_Lead__c>();
        rcs2.add(rc3);
        rcs2.add(rc4);
        
        insert rcs2;
        
        Live_Lead__c rc5 = [select id, processed__c, Lead_Exists__c from Live_Lead__c where Id = :rc4.Id];
        System.assertEquals(true, rc5.processed__c);
        System.assertEquals(false, rc5.Lead_Exists__c);
        
        fxAccount__c fxAccount1 = [select id, BI_Sync__c from fxAccount__c where Id = :fxAccount.Id limit 1];
        System.assertEquals(true, fxAccount1.BI_Sync__c);
    }
}