/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 02-27-2023
 * @last modified by  : Dianelys Velazquez
**/
public abstract class CryptoCallout extends Callout {
    protected Map<String, String> header;

    public CryptoCallout() {
        APISettings apiSettings;
        NamedCredentialsUtil.NamedCredentialData namedCredentialData;
        header = new  Map<String, String>();
        //The namedCredentialData should replace appSettings after fully implementation
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) {
            namedCredentialData = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
        } else {
            apiSettings = APISettingsUtil.getApiSettingsByName(Constants.USER_API_WRAPPER_SETTING_NAME);
        }

        if (apiSettings != null) {
            baseUrl = apiSettings.baseUrl;
            header.put('client_id', apiSettings.clientId);
            header.put('client_secret', apiSettings.clientSecret);
            header.put('Content-Type', 'application/json');
        }

        if (namedCredentialData != null) {
            baseUrl = namedCredentialData.credentialUrl;
            header.put('Content-Type', 'application/json');
        }
    }
}