public class ComplianceRecordsCtrl {
    
    /**
     * Get fxAccount's legal agreements
     */
    @AuraEnabled
    public static List<LegalAgreement> fetchAgreements(
        Id fxAccId)
    {
        try {
            return new LegalAgreementsUtil().getfxAccAgreements(fxAccId);
        } catch (LogException lex) {
            throw lex;
        } catch (Exception ex) {
            throw LogException.newInstance(
                Constants.LOGGER_LEGAL_AGREEMENT_CATEGORY,
                ex);
        }
    }
    
    /**
     * Get fxAccount's KIDs
     */
    @AuraEnabled
    public static ComplianceRecordsUtil.KIDResponse fetchKIDs(Id fxAccId)
    {
        try {
            return ComplianceRecordsUtil.getFxAccountsKIDs(fxAccId);
        } catch (LogException lex) {
            throw lex;
        } catch (Exception ex) {
            throw LogException.newInstance(
                Constants.LOGGER_COMPLIANCE_RECORD_CATEGORY,
                ex);
        }
    }

}