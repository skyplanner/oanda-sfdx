/**
 * @author Fernando Gomez
 * @version 2.0
 * @since March 28th, 2019
 */
@isTest
private class VfSearchControllerTestClass {
	@isTest
	static void redirect(){
		VfSearchController ctrl;
		PageReference pageRef, redirected;

		pageRef = Page.HelpAndSupport;
		Test.setCurrentPage(pageRef);

		ApexPages.currentPage().getParameters().put('categoryType_Division', 'OANDA_US');
		ApexPages.currentPage().getParameters().put('language', 'en_US');

		ctrl = new VfSearchController();
		redirected = ctrl.redirect();
		System.assertEquals('en_US', redirected.getParameters().get('language'));
	}
}