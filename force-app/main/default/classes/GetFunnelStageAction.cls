/**
 * @File Name          : GetFunnelStageAction.cls
 * @Description        : Returns the "Funnel Stage" corresponding
 *                       to a given account
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/14/2023, 1:52:15 PM
**/
public without sharing class GetFunnelStageAction { 

	@InvocableMethod(label='Get Funnel Stage' callout=false)
	public static List<String> getFunnelStage(List<ID> accountIdList) {    
		try {
			ExceptionTestUtil.execute();
			List<String> result = new List<String> { null };

			if (
				(accountIdList == null) ||
				accountIdList.isEmpty()
			) {
				return result;
			}
			// else...     
			Account accountObj = AccountRepo.getFxAccountInfo(accountIdList[0]);
			result[0] = accountObj.fxAccount__r.Funnel_Stage__c;
			return result;   

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetFunnelStageAction.class.getName(),
				ex
			);
		}
	} 

}