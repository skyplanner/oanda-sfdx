/*
 *	Author : Deepak Malkani.
 *	Created Date : June 23 2016
 *	Purpose : Purpose of this class is to act as a web service accepting requests from Request BI Sync Button and setting the Request BI Sync flag to true on Salesforce
 *  Important Note : The class has without sharing setting on purpose. This allows anyone having acess - Read access to FXaccount to send a request in terms of checking the flag to true.
 						Once the flag is set to true, BI will pick up this FX Account and update it with latest data. The entire process can take around 2-4 hours to complete.

 	Future Considerations : For future, BI Sync will be replaced by Real-Time sync and then the request will be sent to FX Trade Platform and CRM instantaneously.					 
*/

global without sharing class BISyncRequest {
	
	/*Web Service Method : Used to Update the BI Sycn Flag to true, so that BI will pick this record in future and Sync it back.*/
	webservice static String requestBISync(String FXAccountId){
		String result;
		if(FXAccountId != null && FXAccountId != ''){
			fxAccount__c fxAccountObj = new fxAccount__c(Id = FXAccountId);
			fxAccountObj.BI_Sync__c = true;
			try{
				update fxAccountObj;
				result = Label.BISyncRequestSuccess;
			}
			catch(DMLException dmlError){
				result = Label.BISyncRequestFailure+' '+dmlError.getMessage();
			}
		}
		return result;
	}

}