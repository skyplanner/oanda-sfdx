/**
 * @description       : The batch class is used to schedule the update of one ID field only
 * for demo fxAccounts associated with not converted demo Leads.
 * The class works with a direct User API, which means that if you include the old logic, the batch will not work.
 * @author            : OANDA
 * @group             :
 * @last modified on  : 12 June 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class UpdateDemoFxAccountOneIdBatch implements  Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {

    private final static Id FXACC_RT_DEMO = RecordTypeUtil.getFxAccountPracticeId();
    private final static Id LEAD_RT_DEMO = RecordTypeUtil.getLeadRetailPracticeId();

    private List<String> queryFields =  new List<String>
    {
            'Id',
            'Lead__r.Email',
            'fxTrade_One_Id__c'
    };
    private Date oldCreatedDate;
    @TestVisible
    private Map<String, List<String>> errorLogs = new Map<String, List<String>>();
    private final static String LOGGER_CATEGORY = 'UpdateDemoFxAccountOneIdBatch';
    private final static String LOGGER_MSG = 'Issue with updating one Id';
    private final static Integer BATCH_SIZE = 80;

    //By default, records no older than 6 months (180 days) are checked, this time can be reduced or increased before running the batch.
    private Integer DAYS_BEFORE_TODAY = 180;

    private Integer RECORDS_LIMIT = 0;

    //Schedule every Sunday at 1 AM
    public static String CRON_SCHEDULE = '0 0 1 ? * 1';

    public UpdateDemoFxAccountOneIdBatch() {
    }

    public UpdateDemoFxAccountOneIdBatch(Integer daysBeforeToday) {
        DAYS_BEFORE_TODAY = daysBeforeToday;
    }

    public UpdateDemoFxAccountOneIdBatch(Integer daysBeforeToday, Integer recordsLimit) {
        DAYS_BEFORE_TODAY = daysBeforeToday;
        RECORDS_LIMIT = recordsLimit;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT ' + String.join(queryFields,', ') + ' FROM fxAccount__c'
                + ' WHERE RecordTypeId = :FXACC_RT_DEMO'
                + ' AND (fxTrade_One_Id__c = NULL OR fxTrade_One_Id__c = \'\')'
                + ' AND Lead__r.Email != NULL'
                + ' AND Lead__c IN (SELECT Id FROM Lead WHERE RecordTypeId = :LEAD_RT_DEMO AND IsConverted = false)';

        if (DAYS_BEFORE_TODAY != null && DAYS_BEFORE_TODAY > 0) {
            oldCreatedDate = System.today().addDays(-DAYS_BEFORE_TODAY);
            query += ' AND CreatedDate >= :oldCreatedDate';
        }
        query += ' ORDER By CreatedDate DESC';
        if (RECORDS_LIMIT != null && RECORDS_LIMIT > 0) {
            query += ' LIMIT ' + RECORDS_LIMIT;
        }

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<SObject> records) {
        SimpleCallouts tempCallout = new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME);
        //UserCallout userCallout = new UserCallout();
        for(fxAccount__c fxAccount : (List<fxAccount__c>)records) {
            try {
                String uuid = tempCallout.postOneIdByEmail(fxAccount.Lead__r.Email);
//                String uuid = userCallout.postOneIdByEmail(fxAccount.Lead__r.Email);
                fxAccount.fxTrade_One_Id__c = uuid;
            } catch (Exception ex) {
                setErrorLogsMap(ex.getMessage().length() > 50 ? ex.getMessage().substring(0, 50) : ex.getMessage(), fxAccount.Id);
            }
        }

        TriggersUtil.disableTrigger(TriggersUtil.Triggers.TRIGGER_FXACCOUNT);
        TriggersUtil.disableTrigger(TriggersUtil.Triggers.TRIGGER_FXACCOUNT_AFTER);
        TriggersUtil.disableTrigger(TriggersUtil.Triggers.TRIGGER_FXACCOUNT_BEFORE);
        List<Database.SaveResult> updateResults = Database.update(records, false);
        for (Database.SaveResult updateResult : updateResults) {
            if (!updateResult.isSuccess()) {
                setErrorLogsMap('Error during fxAccount update', updateResult.getId());
            }
        }

        if (errorLogs.size() > 0) {
            Logger.error(
                    LOGGER_MSG,
                    LOGGER_CATEGORY,
                    JSON.serialize(errorLogs));
        }
    }

    public void finish(Database.BatchableContext context) {

    }

    //Schedule
    public void execute(SchedulableContext context) {
        Database.executeBatch(new UpdateDemoFxAccountOneIdBatch(DAYS_BEFORE_TODAY, RECORDS_LIMIT), BATCH_SIZE);
    }

    /**
     * @description Method to schedule the batch job with default time configuration
     */
    public static void schedule() {
        System.schedule('UpdateDemoFxAccountOneIdBatch', CRON_SCHEDULE, new UpdateDemoFxAccountOneIdBatch());
    }

    /**
     * @param cronExpression parameter indicating the value when it should be called by Schedule
     * @param daysBeforeToday parameter indicating the period for which records should be viewed
     * @description Method to schedule the batch job
     */
    public static void schedule(String cronExpression, Integer daysBeforeToday) {
        schedule(cronExpression, daysBeforeToday, 0);
    }

    /**
     * @param cronExpression parameter indicating the value when it should be called by Schedule
     * @param daysBeforeToday parameter indicating the period for which records should be viewed
     * @param recordsLimit parameter number of records to review
     * @description Method to schedule the batch job
     */
    public static void schedule(String cronExpression, Integer daysBeforeToday, Integer recordsLimit) {
        cronExpression = String.isNotBlank(cronExpression) ? cronExpression : CRON_SCHEDULE;
        System.schedule('UpdateDemoFxAccountOneIdBatch', cronExpression, new UpdateDemoFxAccountOneIdBatch(daysBeforeToday, recordsLimit));
    }

    private void setErrorLogsMap(String errorType, String fxAccountId) {
        if (errorLogs.containsKey(errorType)) {
            errorLogs.get(errorType).add(fxAccountId);
        } else {
            errorLogs.put(errorType, new List<String>{
                    fxAccountId
            });
        }
    }
}