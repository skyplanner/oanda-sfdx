/**
 * @File Name          : ExceptionTestUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2024, 4:29:13 AM
**/
public inherited sharing class ExceptionTestUtil {

	public static Exception exceptionInstance;

	public static void prepareDummyException() {
		exceptionInstance = 
			new DummyTestException('fake msg');
	}

	public static void prepareSpException() {
		exceptionInstance = 
			new SPException('fake msg');
	}

	public static void prepareLogException() {
		exceptionInstance = 
			new LogException('fake msg');
	}

	public static void execute() {
		if (
			Test.isRunningTest() &&
			(exceptionInstance != null)
		) {
			throw exceptionInstance;
		}

	}

	//************************************************************************ 

	public class DummyTestException extends Exception {
	}
}