public without sharing class LeadUtilWithoutSharing {
    
    public static void unassign(List<Lead> leads) {
    	List<Lead> leadsToUnassign = new List<Lead>();
    	for(Lead l : leads) {
    		if(l.Unassign__c==true) {
    			l.OwnerId = UserUtil.getSystemUserForTest().Id;
    			l.Unassign__c=false;
    		}
    	}
    }
}