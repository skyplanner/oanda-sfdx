public with sharing class ComplyAdvantageUpdateResults implements Queueable,Database.AllowsCallouts {
    
    Comply_Advantage_Search__c newSearchRecord;
    Comply_Advantage_Search__c oldSearchRecord;
    ComplyAdvantageSearch.ComplyAdvantageSettings complyAdvantageSetting;
    
    /**
     * Process advantage update results async if possible
     */
    public static Id processAsync(
        Comply_Advantage_Search__c searchDetails,
        Comply_Advantage_Search__c oldSearchDetails,
        ComplyAdvantageSearch.ComplyAdvantageSettings complyAdvantageSetting)
    {
        if(LimitsUtil.canEnqueueJob()) {
            Logger.debug(
                'Comply Advantage Check : Comply Advantage update queued',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY);

            return
                System.enqueueJob(
                    new ComplyAdvantageUpdateResults(
                        searchDetails, 
                        oldSearchDetails,
                        complyAdvantageSetting));
        } else {
            process(
                searchDetails, 
                oldSearchDetails,
                complyAdvantageSetting);
        }
        return null;
    }

    /**
     * Process advantage update results
     */
    public static void process(
        Comply_Advantage_Search__c searchDetails,
        Comply_Advantage_Search__c oldSearchDetails,
        ComplyAdvantageSearch.ComplyAdvantageSettings complyAdvantageSetting)
    {
        new ComplyAdvantageUpdateResults(
            searchDetails, 
            oldSearchDetails,
            complyAdvantageSetting).processing();
    }

    /**
     * Constructor
     */
    private ComplyAdvantageUpdateResults (Comply_Advantage_Search__c newSearchRec, 
                                         Comply_Advantage_Search__c oldSearchRec,
                                         ComplyAdvantageSearch.ComplyAdvantageSettings pComplyAdvantageSetting)
    {
        newSearchRecord = newSearchRec;
        oldSearchRecord = oldSearchRec;
        complyAdvantageSetting = pComplyAdvantageSetting;
    }
    
    public void execute(QueueableContext context) 
    { 
        processing();
    }

    public void processing() 
    { 
        if(newSearchRecord.Search_Id__c != null){
            
            Http http = new Http();
               
            //setup request
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ newSearchRecord.Search_Id__c +'?api_key='+ complyAdvantageSetting.apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
			request.setMethod('POST');
            
            string requestBody = '{"match_status" : "'+ newSearchRecord.Match_Status__c +'"}';
            request.setBody(requestBody); 

            //make service call
            HttpResponse response = http.send(request);
 
            //process response
            if (response.getStatusCode() == 200) 
            {
                Note statusOverrideReasonNote = new Note();
                statusOverrideReasonNote.ParentId = newSearchRecord.Id;
                statusOverrideReasonNote.Body =  newSearchRecord.Match_Status_Override_Reason__c;
                statusOverrideReasonNote.Title = 'Match Status Changed From ' + oldSearchRecord.Match_Status__c + ' to ' + newSearchRecord.Match_Status__c;
                Insert statusOverrideReasonNote;
        
                Comply_Advantage_Search__c rec = new Comply_Advantage_Search__c(Id = newSearchRecord.Id);
                rec.Match_Status_Override_Reason__c = '';
                ComplyAdvantageHelper.updateComplyAdvantageSearch(rec, false);  
       		}
            else{
                Logger.error(
                    'Error : while syncing Customer match status with comply Advantage',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response);
            }
        }   
    }
}