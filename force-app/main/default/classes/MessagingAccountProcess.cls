/**
 * @File Name          : MessagingAccountProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 7:56:34 PM
**/
public inherited sharing class MessagingAccountProcess {

	final MessagingSession session;

	public MessagingAccountProcess(MessagingSession session) {
		this.session = session;
	}

	public MessagingAccountCheckInfo checkLinkToAccountOrLead(
		String userEmail
	) {
		MessagingAccountCheckInfo result = 
			new MessagingAccountCheckInfo(false);
		if (
			String.isBlank(userEmail) ||
			(session == null)
		) {
			return result;
		}
		// else...
		MessagingSession sessionToUpdate = null;
		MessagingEndUser endUserToUpdate = null;
		Account accountObj = AccountRepo.getSummaryByEmail(userEmail);
		
		if (accountObj != null) {
			endUserToUpdate = new MessagingEndUser(
				Id = session.MessagingEndUserId,
				AccountId = accountObj.Id,
				ContactId = accountObj.PersonContactId
			);
			result.isLinkedToAccountOrLead = true;
			setFxAccount(
				result, // checkInfo
				accountObj.fxAccount__c, // fxAccountId,
				userEmail // userEmail 
			);
			
		} else {
			Lead leadObj = LeadRepo.getSummaryByEmail(userEmail);

			if (leadObj != null) {
				session.LeadId = leadObj.Id;
				sessionToUpdate = session;

				endUserToUpdate = new MessagingEndUser(
					Id = session.MessagingEndUserId,
					LeadId = leadObj.Id
				);
				result.isLinkedToAccountOrLead = true;
			}
		}
		
		if (result.isLinkedToAccountOrLead == true) {
			SPDataUtils.updateIfNoTesting(endUserToUpdate);
			SPDataUtils.updateIfNoTesting(sessionToUpdate);
		}
		return result;
	}

	public MessagingAccountCheckInfo checkLinkToFxAccount(
		String username
	) {
		MessagingAccountCheckInfo result = 
			new MessagingAccountCheckInfo(false);

		if (
			String.isBlank(username) ||
			(session == null)
		) {
			return result;
		}
		// else...
		fxAccount__c fxAccountObj = 
			FxAccountRepo.getAccountInfoByName(username);
		
		if (fxAccountObj != null) {
			result.isLinkedToAccountOrLead = true;
			setFxAccount(
				result, // checkInfo
				fxAccountObj.Id, // fxAccountId,
				fxAccountObj.Account__r.PersonEmail // userEmail 
			);
			MessagingEndUser endUser = new MessagingEndUser(
				Id = session.MessagingEndUserId,
				AccountId = fxAccountObj.Account__c
			);
			SPDataUtils.updateIfNoTesting(endUser);
		} 
		return result;
	}

	@TestVisible
	void setFxAccount(
		MessagingAccountCheckInfo checkInfo,
		ID fxAccountId,
		String userEmail
	) {
		if (
			(fxAccountId == null) ||
			String.isBlank(userEmail)
		) {
			return;
		}
		//else...
		
		// Only if there is a contact for the account 
		// is the fxAccount considered valid 
		// (the contact is necessary to send the authentication code)
		Contact contactObj = ContactRepo.getSummaryByEmail(userEmail);

		if (contactObj != null) {
			checkInfo.fxAccountId = fxAccountId;
			
		}
	}
	
}