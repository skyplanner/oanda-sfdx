/**
 * @File Name          : MsgSessionSRoutingInfoManager.cls
 * @Description        : 
 * Creates ServiceRoutingInfo records from PendingServiceRouting records
 * linked to MessagingSession object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 1:25:18 AM
**/
public inherited sharing class MsgSessionSRoutingInfoManager 
	extends CommonSRoutingInfoManager 
	implements SWorkItemUpdater, SRoutingLogCreator {

	AccountSegmentationManager segmentationManager;

	public MsgSessionSRoutingInfoManager() {
		super();
		this.segmentationManager = new AccountSegmentationManager();
	}

	protected override List<SObject> getWorkItems(Set<ID> workItemIdSet) {
		return [
			SELECT
				CaseId,
				Case_Language_Code__c,
				Case_Subtype__c,
				Case_Type__c,
				Inquiry_Nature__c,
				Language_Code__c,
				Live_or_Practice__c,
				New_Lead__c,
				Region__c,
				User_Email__c,
				Tier__c,
				EndUserAccount.Id, 
				EndUserAccount.PersonEmail,
				EndUserAccount.Primary_Division_Name__c,
				EndUserAccount.Language_Preference__pc,
				EndUserAccount.Is_High_Value_Customer__c,
				EndUserAccount.Funnel_Stage__pc,
				EndUserAccount.Last_Trade_Date__c,
				EndUserAccount.fxAccount__c,
				EndUserAccount.CreatedDate,
				Lead.Id,
				Lead.Email,
				Lead.fxAccount__c,
				Lead.Language_Preference__c,
				Lead.Division_from_Region__c,
				Lead.CreatedDate
			FROM MessagingSession 
			WHERE Id IN :workItemIdSet
		];
	}

	@TestVisible
	protected override void processWorkItems(List<SObject> workItems) {
		if (workItems.isEmpty()) {
			return;
		}
		// else...
		for (SObject rec : workItems) {
			MessagingSession msgSession = (MessagingSession) rec;
			ID fxAccountId = AccountSegmentationManager.getValidFxAccount(
				msgSession.EndUserAccount, // accountObj 
				msgSession.Lead // leadObj
			);
			segmentationManager.registerFxAccount(fxAccountId);
		}
	}

	@TestVisible
	protected override BaseServiceRoutingInfo getRoutingInfo(
		PendingServiceRouting routingObj, 
		SObject workItem
	) {
		MessagingSession msgSession = (MessagingSession) workItem;
		ID fxAccountId = AccountSegmentationManager.getValidFxAccount(
			msgSession.EndUserAccount, // accountObj 
			msgSession.Lead // leadObj
		);
		Segmentation__c segmentation = 
			segmentationManager.getSegmentation(fxAccountId);

		ServiceRoutingInfoFactory factory = 
			ServiceRoutingInfoFactory.getInstance();

		ServiceRoutingInfo result = factory.createNewRecord(
			routingObj, // psrObj
			ServiceRoutingInfo.Source.MESSAGING, // infoSource
			msgSession // workItem
		);
		result.workItemUpdater = this;
		result.routingLogCreator = this;
		result.dropAdditionalSkills = true;
		result.initTier(msgSession.Tier__c);

		// Segmentation must be assigned before setting up the account or lead
		result.segmentation = segmentation;
		
		// checkCustomerInfo = false because
		// checkCustomerType is called directly 
		// at the end of field initialization
		result.setRelatedLead(
			msgSession.Lead, // leadObj
			false // checkCustomerInfo
		);
		result.setRelatedAccount(
			msgSession.EndUserAccount, // accountObj
			false // checkCustomerInfo
		);
		result.email = msgSession.User_Email__c;
		setDivision(
			result, // sRoutingInfo
			msgSession.Region__c // chatRegion
		);
		result.accountType = msgSession.Live_or_Practice__c;
		result.language = msgSession.Case_Language_Code__c;
		result.inquiryNature = msgSession.Inquiry_Nature__c;
		result.type = msgSession.Case_Type__c;
		result.subtype = msgSession.Case_Subtype__c;
		// result.crypto = (msgSession.Spot_Crypto__c == true);
		result.checkCustomerType();

		ServiceLogManager.getInstance().log(
			'getRoutingInfo -> result', // msg
			MsgSessionSRoutingInfoManager.class.getName(), // category
			result // details
		);

		return result;
	}

	@TestVisible
	void setDivision(
		ServiceRoutingInfo sRoutingInfo,
		String chatRegion
	) {
		if (String.isBlank(sRoutingInfo.division)) {
			sRoutingInfo.division = chatRegion;
		}
	}

	public List<SObject> getRecordsToUpdate(BaseServiceRoutingInfo routingInfo) {
		ServiceRoutingInfo sRoutingInfo = (ServiceRoutingInfo) routingInfo;
		MessagingSession msgSession = (MessagingSession) sRoutingInfo.workItem;
		List<SObject> result = new List<SObject> {
			new MessagingSession(
				Id = sRoutingInfo.workItem.Id,
				ASA_Start_Time__c = DateTime.now(),
				Tier__c = sRoutingInfo.tier,
				Routing_Status__c = OmnichanelConst.ROUTING_STATUS_ROUTING
			)
		};

		if (msgSession.CaseId != null) {
			result.add(
				new Case(
					Id = msgSession.CaseId,
					Tier__c = sRoutingInfo.tier,
					Ready_For_Routing__c = true
				)
			);	
		}
		return result;
	}

	public SObject getRoutingLog(BaseServiceRoutingInfo routingInfo) {
		SObject result = 
			new RoutingLogManager(routingInfo).getLogForMsgSession();
		return result;
	}

}