/**
 * @description       : helper class to handle the Update_Token__e triggers events
 * @author            : OANDA
 * @group             :
 * @last modified on  : 16 May 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class UpdateTokenEnvTriggerHelper extends TriggerHandler {

    private static final Set<Integer> SUCCESS_STATUS_CODES = new Set<Integer>{200, 201};
    List<Update_Token__e> newList;

    /**
     * Constructor
     */
    public UpdateTokenEnvTriggerHelper() {
        this.newList = (List<Update_Token__e>) Trigger.new;
    }

    /**
    * @description Override afterInsert method.
    */
    public override void afterInsert() {
        System.enqueueJob(new UpdateTokenQueueable(this.newList));
    }

    private class UpdateTokenQueueable implements Queueable, Database.AllowsCallouts {
        private List<Update_Token__e> newRecords;

        UpdateTokenQueueable(List<Update_Token__e> newRecords) {
            this.newRecords = newRecords == null ?  new List<Update_Token__e>() : newRecords;
        }

        public void execute(QueueableContext context) {
            // In the normal case, the UpdateTokenEnvTriggerHelper trigger will only work on one record.
            // The "for" was added because triggers work with bulk records.
            for (Update_Token__e updateToken : this.newRecords) {
                HttpRequest request = TokenHelper.getUpdateNamedCredentialRequest(
                        updateToken.Request_Body__c,
                        updateToken.Is_Update_Credential__c
                );
                HttpResponse response = new Http().send(request);
                if (!SUCCESS_STATUS_CODES.contains(response.getStatusCode())) {
                    Logger.error('The token has not been updated.',
                            'UpdateTokenEnvTriggerHelper',
                            'Status code: ' + response.getStatusCode() + '. Body:' + response.getBody());
                }

            }
        }
    }
}