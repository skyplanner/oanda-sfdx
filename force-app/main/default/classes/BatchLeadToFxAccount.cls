/*
 * Take all the leads in the system, and migrate fxAccount data into the fxAccount object
 */
public with sharing class BatchLeadToFxAccount implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{

	public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchLeadToFxAccount() {
		// Select all leads that are Retail Practice or Retail Lead that do not have any associated fxAccount__c objects
		Id live = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		Id practice = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
//		query = 'SELECT Id, RecordTypeId, Traded__c, Username__c, Username_Practice__c, Funnel_Stage__c, Division_Name__c, Type__c FROM Lead WHERE lastname=\'levinia shareena cher\' and (RecordTypeId = \'' + live + '\' OR RecordTypeId = \''+ practice + '\') AND IsConverted=false AND (Username__c!=null OR Username_Practice__c!=null) AND Id NOT IN (SELECT Lead__c FROM fxAccount__c)';
		query = 'SELECT Id, RecordTypeId, Traded__c, Username__c, Username_Practice__c, Funnel_Stage__c, Email, Division_Name__c, Type__c FROM Lead WHERE (RecordTypeId = \'' + live + '\' OR RecordTypeId = \''+ practice + '\') AND IsConverted=false AND (Username__c!=null OR Username_Practice__c!=null) AND Id NOT IN (SELECT Lead__c FROM fxAccount__c)';
		system.debug('query: ' + query);
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, RecordTypeId, Traded__c, Username__c, Username_Practice__c, Funnel_Stage__c, Division_Name__c, Type__c FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
   	public Database.QueryLocator start(Database.BatchableContext bc) {
   		CustomSettings.setEnableLeadConversion(false);
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		
   		if (CustomSettings.isEnableLeadConversion()) {
   			throw new ApplicationException('Lead conversion must be disabled before migrating Leads to fxAccounts');
   		}
   		
    	fxAccount__c[] fxAccountsToInsert = new fxAccount__c[]{};
    	for(SObject s : batch) {
    		Lead lead = (Lead)s;
    		fxAccount__c migratedAccount = fxAccountUtil.getFxAccountFromLead(lead);
    		
    		if (migratedAccount != null) {
	    		fxAccountsToInsert.add(migratedAccount);
	    		lead.Funnel_Stage__c = migratedAccount.Funnel_Stage__c;
	    		lead.CRM_Legacy_Id__c = lead.Email;
    		}
    		
    	}
		insert fxAccountsToInsert;
		update batch;
//		Database.insert(fxAccountsToInsert, false);
//		Database.update(batch, false);
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		CustomSettings.setEnableLeadConversion(true);
		try {
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
		}
		catch (Exception e) {}
	}
	
	// BatchMergeLeads.executeBatch()
	public static Id executeBatch() {
		return Database.executeBatch(new BatchLeadToFxAccount());
	}
}