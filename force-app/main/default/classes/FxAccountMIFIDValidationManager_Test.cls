/**
 * @File Name          : FxAccountMIFIDValidationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-19-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/23/2020   acantero     Initial Version
**/
@isTest
private class FxAccountMIFIDValidationManager_Test {

    //invalid param values
    @isTest
    static void validatefxAccounts_test1() {
        Boolean error = false;
        Test.startTest();
        try {
            FxAccountMIFIDValidationManager.validatefxAccounts(null, null);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    //validable fxAcconts Id list
    @isTest
    static void validatefxAccounts_test2() {
        List<ID> idList = MIFIDValidationTestDataFactory.getValidableFxAccountsIdList();
        Test.startTest();
        FxAccountMIFIDValidationManager.validatefxAccounts(
            idList, 
            false
        );
        Test.stopTest();
        Integer validatedCount = [select count() from MiFID_Validation_Result__c  where Valid__c = true];
        System.assertNotEquals(0, validatedCount);
    }

    //invalid param values
    @isTest
    static void checkFxAccountMifidStatus_test1() {
        Boolean error = false;
        Test.startTest();
        try {
            FxAccountMIFIDValidationManager.checkFxAccountMifidStatus(null, null);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    //invalid param values
    @isTest
    static void mifidValStatusMustChange_test1() {
        Test.startTest();
        Boolean result = FxAccountMIFIDValidationManager.mifidValStatusMustChange(null, null, null);
        Test.stopTest();
        System.assertEquals(false, result);
    }

    @isTest
    static void checkFxAccountMifidStatus_test2() {
        fxAccount__c validatedFxAccount = MIFIDValidationTestDataFactory.createValidatedFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568'
        );
        List<fxAccount__c> fxAccountList = new List<fxAccount__c>{validatedFxAccount};
        fxAccount__c validatedFxAccountOld = new fxAccount__c(Passport_Number__c = '1234567', Citizenship_Nationality__c = 'Japan');
        
        Map<Id, fxAccount__c> oldMapFxAccount = new Map<Id, fxAccount__c>{validatedFxAccount.Id => validatedFxAccountOld};
        List<MiFID_Validation_Result__c> validationResultBefore = [select Id, Valid__c from MiFID_Validation_Result__c  where fxAccount__c =: validatedFxAccount.Id ];
        Test.startTest();
           FxAccountMIFIDValidationManager.checkFxAccountMifidStatus(fxAccountList, oldMapFxAccount);
        Test.stopTest();
        List<MiFID_Validation_Result__c> validationResultAfter = [select Id, Valid__c from MiFID_Validation_Result__c  where fxAccount__c =: validatedFxAccount.Id ];
        System.assertEquals(1, validationResultBefore.size());
        System.assertEquals(true, validationResultBefore[0].Valid__c);
        System.assertEquals(1, validationResultAfter.size());
        System.assertEquals(false, validationResultAfter[0].Valid__c);
    }

}