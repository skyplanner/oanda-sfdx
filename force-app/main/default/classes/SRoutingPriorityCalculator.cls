/**
 * @File Name          : SRoutingPriorityCalculator.cls
 * @Description        : 
 * Calculates the routing priority for a routing information record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 2:16:04 AM
**/
public interface SRoutingPriorityCalculator {

	Integer getRoutingPriority(BaseServiceRoutingInfo info);

}