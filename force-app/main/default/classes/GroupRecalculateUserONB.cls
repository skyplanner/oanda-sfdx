public with sharing class GroupRecalculateUserONB extends OutgoingNotificationBus{
    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        //After completely replacing the old logic, the validation fields can be moved here.

        if (String.isBlank(((fxAccount__c) newRecord).fxTrade_One_Id__c)) {
            addEventsError(newRecord.Id, 'GroupRecalculateUserONB - missing fxTrade_User_Id__c');
            return null;
        }
        if (String.isBlank(fxAccountUtil.getEnvVariable((fxAccount__c) newRecord))) {
            addEventsError(newRecord.Id, 'GroupRecalculateUserONB - RT do not match' + ((fxAccount__c) newRecord).RecordTypeId);
            return null;
        }
        return '{}';
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize(new Map<String, String> {
                'env' => fxAccountUtil.getEnvVariable((fxAccount__c)newRecord),
                'recordId' =>  ((fxAccount__c) newRecord).Id,
                'tradeOneId' => ((fxAccount__c) newRecord).fxTrade_One_Id__c
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME).updateGroupRecalculateUserApi(
                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeOneId')),
                body
        );
//        new UserCallout().updateGroupRecalculateUserApi(
//                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeOneId')),
//                body);
    }

    public override String getRecordId() {
        return recordId;
    }
}