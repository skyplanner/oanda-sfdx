/**
 * @File Name          : GetSameValueActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/19/2023, 9:34:29 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 9:33:53 AM   aniubo     Initial Version
 **/
@isTest
private class GetSameValueActionTest {
	@isTest
	private static void testTestCase() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<String> valueList = new List<String>{ '1', '2', '3', '4', '5' };
		List<String> strList = GetSameValueAction.getSameValue(valueList);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			valueList.size(),
			strList.size(),
			'The size of the list should be the same'
		);
	}
}