public with sharing class CurrentDeskUpdateDetailsCallout 
{
    public static CurrentDeskSync.LiveUser liveUser;

    public CurrentDeskUpdateDetailsCallout() 
    {

    }
    public static void updateCustomerDetails(CurrentDeskSync.LiveUser userInfo)
    {
        liveUser = userInfo;
        callService(getClientDetailUpdateRequest());
    }
    
    private static HttpRequest getClientDetailUpdateRequest() 
    { 
        string clientId =string.valueOf(liveUser.CurrentDeskClientId);
        string requestBody = getClientDetailUpdateRequestBody();

        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl + '/misc/client/updateclient' + '?id=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setHeader('id',clientId);
        httpRequest.setHeader('clientUpdateModel', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setTimeout(120000);

        httpRequest.setBody(requestBody); 

        return httpRequest;
    }
     private static string getClientDetailUpdateRequestBody() 
    { 
        string[] updateRequests = new string[]{};
        if(liveUser.accountReadOnly != null)
        {
           updateRequests.add('"client-readonly":' +  liveUser.accountReadOnly);
        }
        if(liveUser.lockAccount != null)
        {
            updateRequests.add('"client-enabled":' + !liveUser.lockAccount);
        }
        return '\n{\n' + String.join( updateRequests, ',\n') + '\n}\n';
    }

    private static void callService(HttpRequest req)
    {
        Http http = new Http();
        HttpResponse response = http.send(req);

        //timeout retry once
        if(response.getStatusCode() == 408)
        {
            http = new Http();
            response = http.send(req);
        }
        if(response.getStatusCode() != 200)
        {
            //send email
            string subject = 'CurrentDesk update failed for user : ' + liveUser.fxAccountDetails.Name;
            string emailBody = 'Repsonse: \n ' + response.getBody();
            emailBody +=  '\n\nSalesforce link: \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + liveUser.fxAccountId;
            emailBody += '\n\nRequest Details: \n' +  req.getBody();
            EmailUtil.sendEmail('salesforce@oanda.com', subject, emailBody);
        }
    }
}