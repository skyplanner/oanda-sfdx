/**
 * @File Name          : MilestonesNotificationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/8/2021, 11:58:37 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/6/2021, 4:52:53 PM   acantero     Initial Version
**/
public with sharing class MilestonesNotificationManager extends SLANotificationManager {

    public MilestonesNotificationManager(
        SLAConst.AgentSupervisorModel supervisorModel
    ) {
        super(supervisorModel);
    }

    public void sendNotifications(
        List<SLAMilestoneViolationInfo> violationInfoList
    ) {
        if ((violationInfoList == null) || (violationInfoList.isEmpty())) {
            return;
        }
        //else...
        Boolean existsSupervisors = findSupervisors(violationInfoList);
        if (existsSupervisors != true) {
            return;
        }
        //else...
        for(SLAMilestoneViolationInfo violationInfo : violationInfoList) {
            sendNotification(violationInfo);
        }
    }

    void sendNotification(SLAMilestoneViolationInfo violationInfo) {
        Set<String> supervisorIds = supervisorProv.getSupervisors(
            getSupervisorCriteria(violationInfo)
        );
        Milestone_Time_Settings__mdt milestoneSetting = violationInfo.milestoneSettings;
        if(
            (supervisorIds != null) && 
            (milestoneSetting != null) &&
            String.isNotBlank(milestoneSetting.Notification_Subject__c) &&
            String.isNotBlank(milestoneSetting.Notification_Body__c)
        ) {
            String validTimeTrigger = 
                SPTextUtil.getStringValueOrEmpty(milestoneSetting.Time_Trigger__c);
            String validOwnerName = 
                SPTextUtil.getValueOrEmpty(violationInfo.ownerName);
            String validCaseNumber = 
                SPTextUtil.getValueOrEmpty(violationInfo.caseNumber);
            String validInquiryNature = 
                SPTextUtil.getValueOrEmpty(violationInfo.inquiryNature);
            String validDivision = 
                SPTextUtil.getValueOrEmpty(violationInfo.division);
            
            Messaging.CustomNotification notification = new Messaging.CustomNotification();
            // Set the contents for the notification
            notification.setTitle(milestoneSetting.Notification_Subject__c);
            notification.setBody(
                milestoneSetting.Notification_Body__c
                    .replace(Label.MilestoneTimePlaceholder, validTimeTrigger)
                    .replace(Label.OwnerNamePlaceholder, validOwnerName)
                    .replace(Label.CaseNumberPlaceholder, validCaseNumber)
                    .replace(Label.InquiryNaturePlaceholder, validInquiryNature)
                    .replace(Label.DivisionPlaceholder, validDivision)
            );
            // Set the notification type and target
            notification.setNotificationTypeId(getSlaNotificationId());
            notification.setTargetId(violationInfo.caseId);
            // Send the notification
            notification.send(supervisorIds);
        }
    }

}