/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsAsset {
    public String public_url { get; set; }

    public String source { get; set; }

    public String type { get; set; }

    public String url { get; set; }
}