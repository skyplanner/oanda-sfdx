/**
 * Tests: AuthenticationCodeGenerator
 * @author Fernando Gomez
 */
@isTest
private class AuthenticationCodeGeneratorTest {
	/**
	 * public AuthenticationCode getCode(Boolean createNewIfInvalid)
	 */
	@isTest
	static void getCode() {
		AuthenticationCodeGenerator generator;
		AuthenticationCode code;
		Lead l;

		// test lead
		l = TestDataFactory.getTestLead(false);
		insert l;

		// for code and expriation fields we just need a string
		// and a datetime
		// Code field: Business_Name__c
		// Expiration: Info_Last_Updated__c
		generator = new AuthenticationCodeGenerator(
			l, 'Business_Name__c', 'Info_Last_Updated__c');

		// we get the code, by generatting a new one
		code = generator.getCode(true);
		l = [
			SELECT Business_Name__c,
				Info_Last_Updated__c
			FROM Lead
			WHERE Id = :l.Id
		];

		System.assertEquals(code.code, l.Business_Name__c);
		System.assertEquals(code.expiresOn, l.Info_Last_Updated__c);
	}

	/**
	 * public static SObject generateAuthenticationCodeAndSaveToRecord(
	 *		SObject record,
	 * 		String codeFieldApiName,
	 * 		String codeExpiresOnFieldApiName
	 */
	@isTest
	static void generateAuthenticationCodeAndSaveToRecord() {
		AuthenticationCode code;
		Lead l;

		// test lead
		l = TestDataFactory.getTestLead(false);
		insert l;

		AuthenticationCodeGenerator.generateAuthenticationCodeAndSaveToRecord(
			l, 'Business_Name__c', 'Info_Last_Updated__c');
			
		l = [
			SELECT Business_Name__c,
				Info_Last_Updated__c
			FROM Lead
			WHERE Id = :l.Id
		];

		System.assertNotEquals(null, l.Business_Name__c);
		System.assertNotEquals(null, l.Info_Last_Updated__c);
	}

	/**
	 * @test
	 * public static Boolean validateAuthenticationCode(
	 *	String codeToValidate,
	 *	Id recordId,
	 *	String codeFieldApiName,
	 *	String codeExpiresOnFieldApiName)
	 */
	@isTest
	static void validateAuthenticationCode() {
		AuthenticationCode code;
		Lead l;

		// test lead
		l = TestDataFactory.getTestLead(false);
		insert l;

		AuthenticationCodeGenerator.generateAuthenticationCodeAndSaveToRecord(
			l, 'Business_Name__c', 'Info_Last_Updated__c');
			
		System.assert(!AuthenticationCodeGenerator.validateAuthenticationCode(
			'XXXXXX', l.Id, 'Business_Name__c', 'Info_Last_Updated__c'));

		l = [
			SELECT Business_Name__c,
				Info_Last_Updated__c
			FROM Lead
			WHERE Id = :l.Id
		];

		System.assert(AuthenticationCodeGenerator.validateAuthenticationCode(
			l.Business_Name__c, l.Id, 'Business_Name__c', 'Info_Last_Updated__c'));
	}
}