/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ScheduledEmailController_Test {
	@TestSetup
	static void makeData() {
		Account acc = ScheduledEmailTestUtil.createAccount('Test Name');
		Contact ctc = ScheduledEmailTestUtil.createContact(
			acc.Id,
			'Test LastName',
			'me@test.com'
		);
		ScheduledEmailTestUtil.createCase(ctc);
	}
	@isTest
	private static void noDraftEmail() {
		// Test data setup

		// Actual test
		Test.startTest();
		try {
			ScheduledEmailManager.ScheduledEmailWrapper wrapper = createWrapper();

			ScheduledEmailController.saveScheduledEmail(getCaseId(), wrapper);
		} catch (Exception ex) {
			System.assertEquals(
				System.Label.Schedule_an_email_need_Draft,
				ex.getMessage(),
				'No Email Draft'
			);
		}
		Test.stopTest();

		// Asserts
	}
	@isTest
	private static void createScheduledEmail() {
		// Test data setup
		Id caseId = getCaseId();
		ScheduledEmailTestUtil.createDraftEmail(caseId);
		ScheduledEmailManager.ScheduledEmailWrapper wrapper = createWrapper();
		// Actual test
		Test.startTest();
		try {
			wrapper = ScheduledEmailController.saveScheduledEmail(
				caseId,
				wrapper
			);
		} catch (Exception ex) {
			System.assertEquals(
				System.Label.Schedule_an_email_need_Draft,
				ex.getMessage(),
				'No Email Draft'
			);
		}
		Test.stopTest();
		Scheduled_Email__c insertedScheduled = getScheduledEmail();
		System.assert(insertedScheduled != null, 'Not Null');
		System.assertEquals(
			wrapper.scheduledAmOrPm,
			insertedScheduled.AM_or_PM__c,
			'Can be AM'
		);
		System.assertEquals(wrapper.id, insertedScheduled.Id, 'same id');
		System.assertEquals(
			wrapper.scheduledTime,
			insertedScheduled.Time__c,
			'can be 4'
		);
		System.assertEquals(
			getScheduledDateTime(),
			insertedScheduled.Scheduled_Date_Time__c,
			'can be today at 4 pm'
		);

		// Asserts
	}
	@isTest
	private static void getScheduled() {
		// Test data setup
		Id caseId = getCaseId();
		ScheduledEmailTestUtil.createDraftEmail(caseId);
		ScheduledEmailManager.ScheduledEmailWrapper wrapper;
		// Actual test
		Test.startTest();
		try {
			wrapper = ScheduledEmailController.getScheduledEmail(caseId);
		} catch (Exception ex) {
			System.assertEquals(
				System.Label.Schedule_an_email_need_Draft,
				ex.getMessage(),
				'No Email Draft'
			);
		}
		Test.stopTest();
		Schedule_Mail_Setting__mdt settingMtd = ScheduleMailSettingRepo.getFirst();

		System.assertEquals(
			wrapper.scheduledAmOrPm,
			settingMtd.AM_or_PM__c,
			'default PM'
		);
		System.assert(
			wrapper.id == null,
			'Id Null. Scheduled Email not exists'
		);
		System.assertEquals(
			settingMtd.Time__c,
			wrapper.scheduledTime,
			'default 4'
		);

		// Asserts
	}

	@isTest
	private static void deleteScheduled() {
		// Test data setup
		Id caseId = getCaseId();
		ScheduledEmailTestUtil.createDraftEmail(caseId);
		ScheduledEmailManager.ScheduledEmailWrapper wrapper = createWrapper();
		// Actual test
		Test.startTest();
		try {
			wrapper = ScheduledEmailController.saveScheduledEmail(
				caseId,
				wrapper
			);
			wrapper = ScheduledEmailController.deleteScheduledEmail(wrapper.id);
		} catch (Exception ex) {
			System.assertEquals(
				System.Label.Schedule_an_email_need_Draft,
				ex.getMessage(),
				'No Email Draft'
			);
		}
		Test.stopTest();
		Schedule_Mail_Setting__mdt settingMtd = ScheduleMailSettingRepo.getFirst();

		System.assertEquals(
			wrapper.scheduledAmOrPm,
			settingMtd.AM_or_PM__c,
			'default PM'
		);
		System.assert(
			wrapper.id == null,
			'Id Null. Scheduled Email not exists'
		);
		System.assertEquals(
			settingMtd.Time__c,
			wrapper.scheduledTime,
			'default 4'
		);

		// Asserts
	}

	private static ScheduledEmailManager.ScheduledEmailWrapper createWrapper() {
		ScheduledEmailManager.ScheduledEmailWrapper wrapper = new ScheduledEmailManager.ScheduledEmailWrapper();
		wrapper.scheduledAmOrPm = 'AM';
		wrapper.scheduledDate = Date.today();
		wrapper.scheduledTime = '4';
		return wrapper;
	}
	private static Datetime getScheduledDateTime() {
		Date today = Date.today();
		return Datetime.newInstance(
			today.year(),
			today.month(),
			today.day(),
			4,
			0,
			0
		);
	}

	private static Id getCaseId() {
		return [SELECT Id FROM Case LIMIT 1].Id;
	}
	private static Scheduled_Email__c getScheduledEmail() {
		List<Scheduled_Email__c> schedules = [
			SELECT
				Id,
				AM_or_PM__c,
				Case__c,
				Time__c,
				Scheduled_Date__c,
				Email_Message__c,
				Scheduled_Date_Time__c
			FROM Scheduled_Email__c
			LIMIT 1
		];
		return schedules.isEmpty() ? null : schedules.get(0);
	}
}