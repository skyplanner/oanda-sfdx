public with sharing class CurrentDeskUpdateFlagsCallout implements Queueable,Database.AllowsCallouts
{
    public CurrentDeskSync.LiveUser liveUser;
    public CurrentDeskUpdateFlagsCallout(CurrentDeskSync.LiveUser userInfo) 
    {
        liveUser = userInfo;
    }

    public void execute(QueueableContext context) 
    {
        HttpRequest req = getUpdateFlagsRequest();
        callService(req);

        if(liveUser.reducePositionOnly)
        {
            CurrentDeskGetMT4Logins cdMT4 = new CurrentDeskGetMT4Logins(liveUser);
            System.enqueueJob(cdMT4);
        }
    }
    
    public HttpRequest getUpdateFlagsRequest() 
    { 
        string clientId = string.valueOf(liveUser.CurrentDeskClientId);

        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl + '/misc/client/updateflag'  + '?id=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setHeader('id', string.valueOf(liveUser.CurrentDeskClientId));
        httpRequest.setTimeout(120000);

        string requestBody = getUpdateFlagsRequestBody();
        httpRequest.setHeader('clientUpdateModel', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setBody(requestBody); 

        return httpRequest;
    }
    private string getUpdateFlagsRequestBody() 
    {
        string req = '';
        if(liveUser.reducePositionOnly != null)
        {
            req = '{' + '\n';
            req += '"flag-enabled": '+ liveUser.reducePositionOnly +',' + '\n';
            req += '"flag-id": 1' + '\n';
            req +='}';
        }
        return req;
    }

    private void callService(HttpRequest req)
    {
        Http http = new Http();
        HttpResponse response = http.send(req);
        if(response.getStatusCode() == 408)
        {
            //timeout retry once
            http = new Http();
            response = http.send(req);
        }

        //handle response
        if(response.getStatusCode() != 200)
        {
            //send email
            string subject = 'CurrentDesk update failed for user : ' + liveUser.fxAccountDetails.Name;
            string emailBody = 'Repsonse: \n ' + response.getBody();
            emailBody +=  '\n\nSalesforce link: \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + liveUser.fxAccountId;
            emailBody += '\n\nRequest Details: \n' +  req.getBody();
            EmailUtil.sendEmail('salesforce@oanda.com', subject, emailBody);
        }
    }
   
}