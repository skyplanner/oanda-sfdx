@isTest
private class BatchDocumentsCleaner_Test {

	@testSetup
	static void setup () {
	    ArticlesTestDataFactory.createArticlesWithDummyDocs();
		
	}

	@isTest
	static void BatchDocumentsCleaner_test() {
		Integer beforeCount = [select count() from Document];
		BatchDocumentsCleaner docCleanerBatch = new BatchDocumentsCleaner(false);
		Test.startTest();
		if (docCleanerBatch.isValid) {
			Database.executeBatch(docCleanerBatch, BatchDocumentsCleaner.BATCH_EXECUTION_SCOPE);
		}
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

	@isTest
	static void execute_test() {
		Integer beforeCount = [select count() from Document];
		List<Document> docList = [select Id from Document];
		BatchDocumentsCleaner docCleanerBatch = new BatchDocumentsCleaner(false);
		Test.startTest();
		docCleanerBatch.execute(null, docList);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

}