/* Name: FxAccountApiRestResourceTest
 * Description : test class for FxAccountApiRestResource
 * Author: Mikolaj Juras
 * Date : 2023 September 27
 */

@isTest
public with sharing class FxAccountApiRestResourceTest {

    private static string emptyRestUrl = '/services/apexrest/api/v1/fxAccount/';
    private static string restUrl = '/services/apexrest/api/v1/fxAccount/123123123';
    private static string postMethod = 'POST';
    private static string getMethod = 'GET';
    
    @isTest
    static void testDoPostNoUserId() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = emptyRestUrl;  
        req.httpMethod = postMethod;

        JSONGenerator generator = JSON.createGenerator(true); 
        generator.writeStartObject();
        generator.writeStringField('username', 'testusername');
        generator.writeStringField('email', 'test@oanda.com');
        generator.writeEndObject();
        
        req.requestBody = Blob.valueOf(generator.getAsString());
    
        RestContext.request = req;
        RestContext.response = res;
        
        FxAccountApiRestResource.doPost();
    
        Assert.areEqual(500, res.statusCode, 'fxAccountTradeUserId demo is required');
    }

    @isTest
    static void testCorrectRequest() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
        req.requestURI = restUrl;  
        req.httpMethod = postMethod;
        
        
        JSONGenerator generator = JSON.createGenerator(true); 
        generator.writeStartObject();
        generator.writeStringField('username', 'testusername');
        generator.writeStringField('email', 'test@oanda.com');
        generator.writeBooleanField('is_demo', false);
        generator.writeEndObject();
        
        req.requestBody = Blob.valueOf(generator.getAsString());
        
        RestContext.request = req;
        RestContext.response = res;
       
        FxAccountApiRestResource.doPost();
        Test.getEventBus().deliver();
        Assert.areEqual(200, res.statusCode, 'Event is created');      
    }
    
    @isTest
    static void getUserTest() {
        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 123123123;
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        insert fxAccount;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = getMethod;

        RestContext.request = req;
        RestContext.response = res;
       
        FxAccountApiRestResource.doGet();
        Assert.areEqual(200, res.statusCode, 'fxAccount is returned');

        Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.responseBody.toString());
        Map<String, Object> fxAccountPopulatedMap = fxAccount.getPopulatedFieldsAsMap();
        for (fx_Account_Api_Mapping__mdt mapping : fx_Account_Api_Mapping__mdt.getAll().values()) {
            if(mapping.Map_on_fxAcc__c && fxAccountPopulatedMap.containsKey(mapping.fxAcc_Field__c)) {
                Assert.isTrue(response.containsKey(mapping.DeveloperName), 'fxAccount format or required fields are not returned');
            }
        }
    }
    
    @isTest
    static void getUserNoUserTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl + '/demo';  
        req.httpMethod = getMethod;

        RestContext.request = req;
        RestContext.response = res;
       
        FxAccountApiRestResource.doGet();
        Assert.areEqual(404, res.statusCode, 'no fxAccount is returned');
    }

    @isTest
    static void getUserBadRequestTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = emptyRestUrl;  
        req.httpMethod = getMethod;

        RestContext.request = req;
        RestContext.response = res;
       
        FxAccountApiRestResource.doGet();
        Assert.areEqual(500, res.statusCode, 'no fxAccount is returned');
    }
}