/**
 * @File Name          : RelatedMessagingSessionsController.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 1:44:55 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 3:44:51 PM   aniubo     Initial Version
 **/
public with sharing class RelatedMessagingSessionsController {

	@testVisible
	private static IRelatedMessagingSessionsReader reader = new RelatedMessagingSessionsReader();
	@AuraEnabled
	public static Integer getCount(Id recordId) {
		try {
			ExceptionTestUtil.execute();

			return new RelatedMessagingSessionsService(
					recordId,
					reader
				)
				.getCount();
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
	@AuraEnabled
	public static List<MessagingSession> getRelatedMessagingSessions(
		Id recordId,
		List<String> fields,
		Integer pageSize,
		Integer pageNo
	) {
		try {
			ExceptionTestUtil.execute();

			return new RelatedMessagingSessionsService(
					recordId,
					reader
				)
				.getRelatedMessagingSessions(fields, pageSize, pageNo);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}