/**
 * @File Name          : SLAViolationBaseNotificationManager.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/11/2024, 2:36:47 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/16/2024, 5:22:38 PM   aniubo     Initial Version
 **/
public inherited sharing abstract class SLAViolationBaseNotificationManager extends SLANotificationManager implements SLAViolationNotificationManager {
	public SLAConst.SLAViolationType violationType { get; private set; }
	protected Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> notificationParamsByViolationTypeMap;
	protected Map<SLAConst.SLAViolationType, Boolean> notificationTierByViolationType;
	protected  ServiceLogManager logManager { get; set; }

	public SLAViolationBaseNotificationManager(
		SLAConst.AgentSupervisorModel supervisorModel,
		SLAConst.SLAViolationType violationType,
		Map<String, Milestone_Time_Settings__mdt> milestonesByType,
		SupervisorProviderCreator supervisorProviderCreator
	) {
		super(supervisorModel, supervisorProviderCreator);
		this.violationType = violationType;
		notificationParamsByViolationTypeMap = getNotificationParamsMap(
			milestonesByType
		);
		logManager = ServiceLogManager.getInstance();

		logManager.log(
			'SLAViolationBaseNotificationManager:ctr-> supervisorModel  ='+supervisorModel.name() , 
			'SLA',
			'ViolationType = ' + violationType.name()
		);
	}
	public virtual void sendNotifications(
		List<SLAViolationInfo> slaViolationInfos
	) {
		if (slaViolationInfos != null && !slaViolationInfos.isEmpty()) {
			Boolean existsSupervisors = findSupervisors(slaViolationInfos);
			if (existsSupervisors) {
				for (SLAViolationInfo info : slaViolationInfos) {
					sendNotification(info);
				}
			}
		}
	}
	public SLAConst.SLAViolationType getSLAViolationType() {
		return violationType;
	}

	protected virtual NotificationParams getNotificationParams(
		SLAViolationInfo info
	) {
		NotificationParams notParams = null;
		String channel = getChannelNameByObjectNotification(
			info.notificationObjectType
		);
		Map<string, NotificationParams> notificationParamsByTier = notificationParamsByViolationTypeMap.get(
				violationType
			)
			.get(channel);

		Boolean isByTier = notificationTierByViolationType.get(violationType);
		if (isByTier) {
			String tier = String.isBlank(info.tier)
				? OmnichannelCustomerConst.TIER_4
				: info.tier;
			notParams = notificationParamsByTier.get(tier);
		} else {
			notParams = notificationParamsByTier.get(violationType.name());
		}
		return notParams;
	}

	protected virtual String buildNotificationBody(
		SLAViolationNotificationBuilder violationNotificationBuilder,
		SLAViolationInfo info,
		NotificationParams notificationParams
	) {
		setTriggerTime(info, notificationParams);
		return violationNotificationBuilder.buildBody(
			notificationParams.body,
			info
		);
	}

	protected virtual String buildNotificationSubject(
		SLAViolationNotificationBuilder violationNotificationBuilder,
		SLAViolationInfo info,
		NotificationParams notificationParams
	) {
		setTriggerTime(info, notificationParams);
		return violationNotificationBuilder.buildSubject(
			notificationParams.subject,
			info
		);
	}

	protected void setTriggerTime(
		SLAViolationInfo info,
		NotificationParams notificationParams
	) {
		info.triggerTime = notificationParams.triggerTime != null
			? notificationParams.triggerTime.format()
			: null;
	}

	protected void sendNotification(SLAViolationInfo info) {
		NotificationParams notificationParams = getNotificationParams(info);
		if (
			notificationParams != null &&
			String.isNotBlank(notificationParams.body) &&
			String.isNotBlank(notificationParams.subject)
		) {
			Set<String> supervisorIds = supervisorProv.getSupervisors(
				getSupervisorCriteria(info)
			);
			logManager.log(
				'SLAViolationBaseNotificationManager:sendNotification->getSupervisors' , 
				'SLA',
			'	Supervisor Count =' +supervisorIds.size()
			);
			if (supervisorIds.isEmpty()) {
				return;
			}
			logManager.log(
				'SLAViolationBaseNotificationManager:sendNotification->getSupervisors' , 
				'SLA',
			'	Supervisors =' + String.join(supervisorIds, ',')
			);
			SLAViolationNotificationBuilder violationNotificationBuilder;
			violationNotificationBuilder = SLAViolationNotificationBuilderFactory.createNotificationBuilder(
				info.notificationObjectType,
				violationType
			);
			String body = buildNotificationBody(
				violationNotificationBuilder,
				info,
				notificationParams
			);
			logManager.log(
				'SLAViolationBaseNotificationManager:sendNotification' , 
				'SLA',
			'	Body =' + body
			);
			String subject = buildNotificationSubject(
				violationNotificationBuilder,
				info,
				notificationParams
			);
			
			logManager.log(
				'SLAViolationBaseNotificationManager:sendNotification' , 
				'SLA',
			'	Subject =' + body
			);

			Messaging.CustomNotification notification = new Messaging.CustomNotification();

			notification.setTitle(subject);
			notification.setBody(body);
			// Set the notification type and target
			setNotificationTargetId(notification, info);
			notification.setNotificationTypeId(getSlaNotificationId());
			// Send the notification
			notification.send(supervisorIds);
		}
		else{
			logManager.log(
			'SLAViolationBaseNotificationManager:sendNotification' , 
			'SLA',
			'String.isNotBlank(notificationParams.body) && String.isNotBlank(notificationParams.subject)'
		);
		}
	}

	protected abstract void setNotificationTargetId(
		Messaging.CustomNotification notification,
		SLAViolationInfo info
	);

	protected abstract Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> getNotificationParamsMap(
		Map<String, Milestone_Time_Settings__mdt> milestonesByType
	);

	protected virtual Map<String, Map<String, NotificationParams>> getNotificationParamFromSettings(
		Map<String, Milestone_Time_Settings__mdt> milestonesByType
	) {
		Map<String, Map<String, NotificationParams>> notificationParamsByTier = new Map<String, Map<String, NotificationParams>>();
		for (String key : milestonesByType.keySet()) {
			Milestone_Time_Settings__mdt setting = milestonesByType.get(key);
			NotificationParams params = new NotificationParams(
				setting.Violation_Type__c,
				setting.Notification_Subject__c,
				setting.Notification_Body__c
			);
			Boolean isAsa =
				setting.Violation_Type__c == SLAConst.ASA_EXCEEDED_VT;

			params.triggerTime = setting.Time_Trigger__c != null
				? isAsa
						? (setting.Time_Trigger__c * 60).intValue()
						: setting.Time_Trigger__c.intValue()
				: 0;

			Map<String, NotificationParams> byTier = notificationParamsByTier.get(
				setting.Channel__c
			);
			if (byTier == null) {
				byTier = new Map<String, NotificationParams>();
				notificationParamsByTier.put(setting.Channel__c, byTier);
			}
			byTier.put(getTierBySettingName(key), params);
		}
		return notificationParamsByTier;
	}

	protected virtual String getTierBySettingName(String settingName) {
		String tier = OmnichannelCustomerConst.TIER_4;
		if (settingName.contains('1')) {
			tier = OmnichannelCustomerConst.TIER_1;
		} else if (settingName.contains('2')) {
			tier = OmnichannelCustomerConst.TIER_2;
		} else if (settingName.contains('3')) {
			tier = OmnichannelCustomerConst.TIER_3;
		}
		return tier;
	}

	protected virtual String getViolationTypeString() {
		String violationTypeString = '';
		if (violationType == SLAConst.SLAViolationType.ASA) {
			violationTypeString = SLAConst.ASA_EXCEEDED_VT;
		} else if (violationType == SLAConst.SLAViolationType.AHT) {
			violationTypeString = SLAConst.AHT_EXCEEDED_VT;
		} else if (
			violationType == SLAConst.SLAViolationType.BOUNCED ||
			violationType == SLAConst.SLAViolationType.ANSWER
		) {
			violationTypeString = SLAConst.ALERT_NO_ANSWER_VT;
		}
		return violationTypeString;
	}

	protected virtual String getChannelNameByObjectNotification(
		SLAConst.SLANotificationObjectType notificationObjectType
	) {
		String channelName = '';
		switch on notificationObjectType {
			when CHAT_CASE_NOT, MESSAGING_NOT, LIVE_CHAT_NOT {
				channelName = 'Chat';
			}
			when EMAIL_CASE_NOT {
				channelName = 'Email';
			}
			when PHONE_CASE_NOT {
				channelName = 'Phone';
			}
		}
		return channelName;
	}

	public class NotificationParams {
		public NotificationParams(
			String violationType,
			String subject,
			String body
		) {
			this.violationType = violationType;
			this.subject = subject;
			this.body = body;
		}
		public String violationType { get; set; }
		public String subject { get; set; }
		public String body { get; set; }
		public Integer triggerTime { get; set; }
	}
}