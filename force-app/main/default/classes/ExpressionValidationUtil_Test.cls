/**
 * @File Name          : ExpressionValidationUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/24/2020, 3:18:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
@isTest
private class ExpressionValidationUtil_Test {

    //validate single char value
	@isTest
	static void validate_test1() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        valSettings.minLength = 1;
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        Test.startTest();
		ExpressionValidationUtil valUtil = new ExpressionValidationUtil(
            '1',
            fakeExpVal,
            false
        );
        Boolean isValid = valUtil.isValid;
		Test.stopTest();
		System.assert(isValid);
    }
    
    //first char is invalid
	@isTest
	static void validate_test2() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        Test.startTest();
		ExpressionValidationUtil valUtil = new ExpressionValidationUtil(
            ' AB12678H',
            fakeExpVal,
            false
        );
        Boolean isValid = valUtil.isValid;
		Test.stopTest();
		System.assertEquals(false, isValid);
		System.assert(valUtil.hasInvalidChar);
    }
    
    //valid value include separators
	@isTest
	static void validate_test3() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        fakeExpVal.separators = ',-_';
        Test.startTest();
		ExpressionValidationUtil valUtil = new ExpressionValidationUtil(
            'AB-12678_H',
            fakeExpVal,
            false
        );
        Boolean isValid = valUtil.isValid;
		Test.stopTest();
		System.assert(isValid);
    }
    
    //include lowercase letters (invalid)
	@isTest
	static void validate_test4() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        Test.startTest();
		ExpressionValidationUtil valUtil = new ExpressionValidationUtil(
            'Ab12678H',
            fakeExpVal,
            false
        );
        Boolean isValid = valUtil.isValid;
		Test.stopTest();
		System.assertEquals(false, isValid);
		System.assert(valUtil.hasInvalidChar);
    }

    //consecutive and repetive breaks (valid values)
	@isTest
	static void validate_test5() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        Test.startTest();
		ExpressionValidationUtil valUtil1 = new ExpressionValidationUtil(
            '12336895',
            fakeExpVal,
            false
        );
        ExpressionValidationUtil valUtil2 = new ExpressionValidationUtil(
            '11127960',
            fakeExpVal,
            false
        );
        Boolean isValid1 = valUtil1.isValid;
        Boolean isValid2 = valUtil2.isValid;
		Test.stopTest();
		System.assertEquals(true, isValid1);
		System.assertEquals(true, isValid2);
    }

    //letterCase = LOWER_CASE  (invalid value)
	@isTest
	static void validate_test6() {
        MiFIDExpValSettings valSettings = getRequiredSettings();
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        fakeExpVal.letterCase = MiFIDExpValConst.LetterCase.LOWER_CASE;
        Test.startTest();
		ExpressionValidationUtil valUtil1 = new ExpressionValidationUtil(
            'axRk',
            fakeExpVal,
            false
        );
        Boolean isValid1 = valUtil1.isValid;
		Test.stopTest();
		System.assertEquals(false, isValid1);
    }

    static MiFIDExpValSettings getRequiredSettings() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        valSettings.minInvalidNumber = 4;
        valSettings.minInvalidText = 3;
        valSettings.minLength = 4;
        valSettings.minPercentNumber = 50;
        return valSettings;
    }

}