/**
 * @File Name          : NF_getSessionCaseStatus.cls
 * @Description        : This class is used to determine if the case is created in the same session or not.
 * @Author             : Ashish Jethi
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    5/9/2019, 12:24:05 PM       Ashish Jethi              Initial Version
**/
global without sharing class NF_getSessionCaseStatus implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
        Map<String, String> eventParams = new Map<String, String>();
        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(aiServiceResponse);
        String sessionId = (String)m.get('sessionId');


        List<nfchat__Chat_Log__c> listChatLog = [SELECT Id
                                                 FROM nfchat__Chat_Log__c
                                                 WHERE nfchat__Session_Id__c=: sessionId
                                                 LIMIT 1];

            if(!listChatLog.isEmpty()){
                List<Case> listCases = [SELECT id,caseNumber
                                        FROM Case
                                        WHERE nfchat__Chat_Log__c =: listChatLog[0].id
                                        LIMIT 1];

                if(!listCases.isEmpty()){
                    eventParams.put('result',listCases[0].caseNumber);
                } else{
                    eventParams.put('result','false');
                }
            } else{
                eventParams.put('result','false');
            }
        return JSON.serialize(eventParams);
    }
}