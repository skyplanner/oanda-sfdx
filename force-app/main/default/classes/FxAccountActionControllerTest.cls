/**
 * fxAccount Action Controller Tests
 * 
 * @author Hing Chow
 */

@isTest
public class FxAccountActionControllerTest {
    @testSetup
    static void init(){
        TestDataFactory testHandler = new TestDataFactory();
        
        Account account = testHandler.createTestAccount();
        fxAccount__c fxaWithAccount = testHandler.createFXTradeAccount(account);
        
        Lead lead = testHandler.createLeads(1)[0];
        fxAccount__c fxaWithLead = testHandler.createFXTradeAccount(lead);
    }
    
    @isTest
    public static void createCaseWithAccountTest() {
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c, Contact__c FROM fxAccount__c WHERE Account__c != NULL][0];
        String token = '7df33';          // random 5-char hex string
        
        Test.startTest();
        FxAccountActionController.createCase(fxa.Id, token);
        Integer emailInvocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        Case cs = [SELECT Id, fxAccount__c, AccountId, Lead__c, ContactId, Status, Origin, Description FROM Case WHERE fxAccount__c = :fxa.Id][0];
        
        System.assertEquals(fxa.Id, cs.fxAccount__c);
        System.assertNotEquals(null, cs.AccountId);
        System.assertEquals(fxa.Account__c, cs.AccountId);
        System.assertEquals(fxa.Lead__c, cs.Lead__c);
        System.assertEquals(fxa.Contact__c, cs.ContactId);
        System.assertEquals('Closed', cs.Status);
        System.assertEquals('Password Reset', cs.Origin);
        System.assert(cs.Description.contains(token));
        System.assertEquals(1, emailInvocations);
    }
    
    @isTest
    public static void createCaseWithLeadTest() {
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c, Contact__c FROM fxAccount__c WHERE Lead__c != NULL][0];
        String token = '30975';          // random 5-char hex string
        
        Test.startTest();
        FxAccountActionController.createCase(fxa.Id, token);
        Integer emailInvocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        Case cs = [SELECT Id, fxAccount__c, AccountId, Lead__c, ContactId, Status, Origin, Description FROM Case WHERE fxAccount__c = :fxa.Id][0];
        
        System.assertEquals(fxa.Id, cs.fxAccount__c);
        System.assertNotEquals(null, cs.Lead__c);
        System.assertEquals(fxa.Account__c, cs.AccountId);
        System.assertEquals(fxa.Lead__c, cs.Lead__c);
        System.assertEquals(fxa.Contact__c, cs.ContactId);
        System.assertEquals('Closed', cs.Status);
        System.assertEquals('Password Reset', cs.Origin);
        System.assert(cs.Description.contains(token));
        System.assertEquals(1, emailInvocations);
    }
}