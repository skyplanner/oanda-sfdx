/**
* database access utility methods
*
* @author Gilbert Gao
*/
public with sharing class DatabaseUtil 
{
	
   public static Database.SaveResult[] insertRecords(SObject[] records,
                                                     boolean allOrNone,
                                                     boolean disableTrigger)
   {
      Database.SaveResult[] insertResult;

      if(records != null && records.size() > 0)
      {

         Schema.SObjectType sobjType = records[0].getSObjectType();

         if(disableTrigger)
         {
            TriggersUtil.disableTriggers(sobjType);
         }

         insertResult =  Database.insert(records, allOrNone);

         if(disableTrigger)
         {
            TriggersUtil.enableTriggers(sobjType);
         }
      }

      return insertResult;
   }

   public static Database.SaveResult[] updateRecords(SObject[] records,
                                                     boolean allOrNone,
                                                     boolean disableTrigger)
   {
      Database.SaveResult[] updateResult;

      if(records != null && records.size() > 0)
      {
         Schema.SObjectType sobjType = records[0].getSObjectType();

         if(disableTrigger)
         {
            TriggersUtil.disableTriggers(sobjType);
         }

         updateResult =  Database.update(records, allOrNone);

         if(disableTrigger)
         {
            TriggersUtil.enableTriggers(sobjType);
         }
      }

      return updateResult;
   }

    public static Database.upsertResult[] upsertRecords(SObject[] records,
                                                       boolean allOrNone,
                                                       boolean disableTrigger)
   {
      Database.upsertResult[] upsertResult;

      if(records != null && records.size() > 0)
      { 
         Schema.SObjectType sobjType = records[0].getSObjectType();
         
         if(disableTrigger)
         {
            TriggersUtil.disableTriggers(sobjType);
         }

         upsertResult =  Database.upsert(records, allOrNone);

         if(disableTrigger)
         {
            TriggersUtil.enableTriggers(sobjType);
         }
      }

      return upsertResult;
   }

	public static void processDatabaseResults(List<Database.SaveResult> srList){
		Integer successes = 0;
		Integer failures = 0;
		
		for (Database.SaveResult sr : srList) {

            if (sr.isSuccess()) {

               // Operation was successful
               successes++;
            }else {
               failures++;
               // Operation failed, so get all errors               
               for(Database.Error err : sr.getErrors()) {

                  System.debug('The following error has occurred.');                   
                  System.debug(err.getStatusCode() + ': ' + err.getMessage());
                  System.debug('fields that affected this error: ' + err.getFields());
               }

            }
	     }
	     
	     System.debug('Number of successes: ' + successes + ' ,number of failures: ' + failures);
	}
    
}