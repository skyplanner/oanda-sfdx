/**
 * Name: CQGAccountTriggerHandlerTest
 * Description : Test class for CQGAccountTriggerHandler
 * Author: ejung
 * Date : 2024-03-06
 */

@IsTest
private with sharing class CQGAccountTriggerHandlerTest {
    
    @IsTest
    private static void testBeforeInsertDataUpdateCqgAccount() {
        TestDataFactory testHandler = new TestDataFactory();
        Account a  = testHandler.createTestAccount();
        fxAccount__c fxa = new fxAccount__c(
            Name = 'test', 
            Account__c = a.Id, 
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE
        );
        insert fxa;

        Test.startTest();

        //insert CQG_Account__c record
        CQG_Account__c cqg = new CQG_Account__c(
            Account_Name__c = 'testCQG',
            fxAccount__c = fxa.Id
        );
        insert cqg;

        Test.stopTest();

        CQG_Account__c cqgAfterInsert = [
            SELECT Id, 
                Account__c
            FROM CQG_Account__c
            WHERE Id =: cqg.Id
            LIMIT 1];

        Assert.areEqual(a.Id, cqgAfterInsert.Account__c, 'CQG is successfully linked to the Account of the associated fxAccount');
    }
}