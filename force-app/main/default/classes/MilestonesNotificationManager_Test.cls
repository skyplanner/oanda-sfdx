/**
 * @File Name          : MilestonesNotificationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 10:48:44 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 4:56:08 PM   acantero     Initial Version
**/
@isTest
private without sharing class MilestonesNotificationManager_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        );
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        OmnichanelRoutingTestDataFactory.createHvcAccountTestCase();
    }

    @isTest
    static void sendNotifications() {
        String caseId = [select Id from Case limit 1].Id;
        String milestoneTypeName = 
            SLAViolationsTestDataFactory.getMilestoneTypeName();
        SLAMilestoneViolationInfo violationInfo = 
            SLAViolationsTestDataFactory.getSLAMilestoneViolationInfo(
                caseId, 
                milestoneTypeName
            );
        List<SLAMilestoneViolationInfo> violationInfoList = 
            new List<SLAMilestoneViolationInfo> {violationInfo};
        Boolean error = false;
        Test.startTest();
        try {
            MilestonesNotificationManager notifManager = 
                new MilestonesNotificationManager(
                    SLAConst.AgentSupervisorModel.ROLE
                );
            //fill supervisorProv with mock class instance
            notifManager.supervisorProv = 
                SLAViolationsTestDataFactory.getMockSupervisorByRoles();
            notifManager.sendNotifications(violationInfoList);
            //...
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
    
}