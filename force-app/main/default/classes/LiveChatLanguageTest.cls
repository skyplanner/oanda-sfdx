/**
 * @File Name          : LiveChatLanguageTest.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 09-16-2022
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0        07-06-2020                dmorales                  Initial Version
**/
@isTest
private class LiveChatLanguageTest {	
	@isTest 
	static void testLiveChatLanguageCtrl() {
		PageReference pageRef = Page.LiveChatLanguage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('LanguagePreferenceOffline', 'de');
        ApexPages.currentPage().getParameters().put('isRedirect', 'true');
        LiveChatLanguageCtrl ctr = new LiveChatLanguageCtrl();
		Boolean offlineHours = ctr.offlineHours;
		System.assertNotEquals(null, ctr.offlineHours, 'Invalid value');
		System.assertNotEquals(null, ctr.showOfflineWarning, 'Invalid value');
		System.assertNotEquals(null, ctr.fromLanguage, 'Invalid value');
		System.assertNotEquals(null, ctr.getNextUrl(), 'Invalid value');
		System.assertNotEquals(null, ctr.getRefreshUrl(), 'Invalid value');
		System.assertNotEquals(null, ctr.getWhen(), 'Invalid value');
	}

	@isTest
	static void checkLanguageAvailability() {
		Test.startTest();
		LiveChatLanguageCtrl ctrl = new LiveChatLanguageCtrl();
		ctrl.checkLanguageAvailability();
		Boolean english = ctrl.englishLangAvailable;
        Boolean chinese = ctrl.chineseLangAvailable;
        Boolean german = ctrl.germanLangAvailable;
        Boolean spanish = ctrl.spanishLangAvailable;
        Boolean russian = ctrl.russianLangAvailable;
        Boolean polish = ctrl.polishLangAvailable;
        Boolean french = ctrl.frenchLangAvailable;
        Boolean italian = ctrl.italianLangAvailable;
        Boolean chatAvailable = ctrl.chatAvailable;
		Test.stopTest();
		System.assertEquals(false, english, 'Invalid value');
        System.assertEquals(false, chinese, 'Invalid value');
        System.assertEquals(false, german, 'Invalid value');
        System.assertEquals(false, spanish, 'Invalid value');
        System.assertEquals(false, russian, 'Invalid value');
        System.assertEquals(false, polish, 'Invalid value');
        System.assertEquals(false, french, 'Invalid value');
        System.assertEquals(false, italian, 'Invalid value');
        System.assertEquals(false, chatAvailable, 'Invalid value');
	}

	@isTest
	static void getNextUrl() {
		String wizardUrl = Page.LiveChatWizard.getUrl();
		String eventMsgUrl = Page.LiveChatEventMessage.getUrl();
		Test.startTest();
		LiveChatLanguageCtrl ctrl = new LiveChatLanguageCtrl();
		String result1 = ctrl.getNextUrl(true);
		String result2 = ctrl.getNextUrl(false);
		Test.stopTest();
		System.assertEquals(
			wizardUrl, 
			result1,
			'If the EventMessage check is true (means no EventMessage) then navigate to LiveChatWizard'
		);
        System.assertEquals(
			eventMsgUrl, 
			result2,
			'If the EventMessage check is false (means there is an EventMessage) then it navigates to LiveChatEventMessage'
		);
	}
   //Commenting temporary for SP-13578
	/*--@isTest
	static void checkOfflineHoursRedirectShouldNotRedirectWhenCryptoEnableTest() {
		Settings__c settings = new Settings__c(
			Name = 'Default',
			Crypto_Enabled_For_Omni_Channel__c = true
		);
		insert settings;

		Test.startTest();

		LiveChatLanguageCtrl ctrl = new LiveChatLanguageCtrl();
		PageReference pr = ctrl.checkOfflineHoursRedirect();
		
		Test.stopTest();

		System.assertEquals(null, pr, 'Invalid value');
	}--*/
}