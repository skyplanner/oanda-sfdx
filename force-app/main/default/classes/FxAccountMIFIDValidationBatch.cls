/**
 * @File Name          : FxAccountMIFIDValidationBatch.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-19-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/22/2020   acantero     Initial Version
**/
global without sharing class FxAccountMIFIDValidationBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {

    global static final Integer BATCH_EXECUTION_SCOPE = 200;
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    
    public FxAccountMIFIDValidationBatch() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        query = 'SELECT Id';
        query += ' FROM fxAccount__c';
        query += ' WHERE RecordType.DeveloperName = \'' + MiFIDExpValConst.FXACCOUNT_REC_TYPE_LIVE + '\'';
        query += ' AND Type__c = \'' + MiFIDExpValConst.FXACCOUNT_TYPE_INDIVIDUAL + '\'';
        query += ' AND Account__c <> NULL';
        if (
            (valSettings.accountDivisionNames != null) &&
            (!valSettings.accountDivisionNames.isEmpty())
        ) {
            query += ' AND (';
            query = SPTextUtil.addValuesToText(
                query, 
                valSettings.accountDivisionNames, 
                'Division_Name__c = \'', //firstItemPrefix
                '\'', //firstItemSuffix
                ' OR Division_Name__c = \'', //othersItemPrefix
                '\'' //othersItemSuffix
            );
            query += ')';
        }
        query += ' AND (Funnel_Stage__c = \''+ MiFIDExpValConst.FXACCOUNT_READY_FOR_FUNDING + 
                '\' OR Funnel_Stage__c = \'' + MiFIDExpValConst.FXACCOUNT_FUNDED + 
                '\' OR Funnel_Stage__c = \'' + MiFIDExpValConst.FXACCOUNT_TRADED + '\')';
        query += ' AND Last_Trade_Date__c <> NULL';
        query += ' AND (Last_Trade_Date__c = THIS_MONTH OR Last_Trade_Date__c = LAST_N_MONTHS:5)';
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(query);
	}

    global void execute(Database.BatchableContext context, List<sObject> data) {
		List<fxAccount__c> recList = (List<fxAccount__c>) data;
        List<ID> idList = new List<ID>();
        for(fxAccount__c obj : recList) {
            idList.add(obj.Id);
        }

        FxAccountMIFIDValidationManager fxAccountMifidManager = new FxAccountMIFIDValidationManager(
                                                                        idList,
                                                                        true
                                                                );

        fxAccountMifidManager.executeValidation();
	}

    global void finish(Database.BatchableContext bc){
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

}