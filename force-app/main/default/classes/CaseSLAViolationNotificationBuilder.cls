/**
 * @File Name          : CaseSLAViolationNotificationBuilder.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/27/2024, 12:57:33 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2024, 10:24:44 AM   aniubo     Initial Version
**/
public inherited sharing class CaseSLAViolationNotificationBuilder extends SLAViolationNotificationBaseBuilder {
	public CaseSLAViolationNotificationBuilder(
		SLAConst.SLAViolationType violationType
	) {
		super(violationType);
	}
	public override void buildData() {
		fieldsByPlaceholder = new Map<String, String>{
			Label.MilestoneTimePlaceholder => 'triggerTime',
			Label.CaseNumberPlaceholder => 'caseNumber',
			Label.InquiryNaturePlaceholder => 'inquiryNature',
			Label.DivisionPlaceholder => 'division',
			Label.TierPlaceholder => 'tier',
			Label.OwnerNamePlaceholder => 'ownerName',
			Label.CaseOriginPlaceholder => 'caseOrigin'
		};
		placeholdersByViolationType = new Map<SLAConst.SLAViolationType, List<String>>{
			SLAConst.SLAViolationType.AHT => new List<String>{
				Label.CaseNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.OwnerNamePlaceholder,
				Label.CaseOriginPlaceholder,
				Label.MilestoneTimePlaceholder

			}
		};
	}
}