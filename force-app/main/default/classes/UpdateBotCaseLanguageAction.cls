/**
 * @File Name          : UpdateBotCaseLanguageAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/14/2024, 1:31:20 PM
**/
public without sharing class UpdateBotCaseLanguageAction { 

	@InvocableMethod(label='Set Bot Case Language' callout=false)
	public static List<String> setBotCaseLanguage(List<ActionInfo> infoList) { 
		try {
			ExceptionTestUtil.execute();
			List<String> result = new List<String> { null };

			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...
			ActionInfo info = infoList[0];
			MessagingChatbotProcess chatbotProcess = 
				new MessagingChatbotProcess(info.messagingSessionId);
			Boolean success = 
				chatbotProcess.updateMessagingCaseLanguage(info.languageCode);
				
			if (success == true) {
				result[0] = info.languageCode;
			}
			
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetUserAccountIdAction.class.getName(),
				ex
			);
		}
	}
		
	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'languageCode' required = true)
		public String languageCode;

	}

}