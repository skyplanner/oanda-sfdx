/**
 * @File Name          : IRelatedMessagingSessionsReader.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 2:03:14 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 1:17:29 PM   aniubo     Initial Version
 **/
public interface IRelatedMessagingSessionsReader {
	Integer getCount(
		Id currentSessionId,
		String filterFieldApiName,
		Set<Id> messagingEndUserIds
	);

	List<MessagingSession> getRelatedMessagingSessions(
		Set<Id> messagingEndUserIds,
		List<String> fields,
		String filterFieldApiName,
		Integer pageSize,
		Integer pageNo
	);

	MessagingSessionRelatedEntity getMessagingRelatedEntity(
		Id messagingSessionId
	);

	Set<Id> getMessagingEndUsers(Id accountId);
}