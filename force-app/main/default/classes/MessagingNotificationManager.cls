/**
 * @File Name          : MessagingNotificationManager.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 3:51:50 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/27/2024, 4:04:03 PM   aniubo     Initial Version
 **/
public without sharing class MessagingNotificationManager extends SLAViolationBaseNotificationManager {
	public MessagingNotificationManager(
		SLAConst.AgentSupervisorModel supervisorModel,
		SLAConst.SLAViolationType violationType,
		Map<String, Milestone_Time_Settings__mdt> milestonesByType,
		SupervisorProviderCreator supervisorProviderCreator
	) {
		super(
			supervisorModel,
			violationType,
			milestonesByType,
			supervisorProviderCreator
		);
	}
	protected override Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> getNotificationParamsMap(
		Map<String, Milestone_Time_Settings__mdt> milestonesByType
	) {
		Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> notifications;
		notifications = new Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>>();

		Map<String, NotificationParams> answerViolations = new Map<String, NotificationParams>{
			SLAConst.SLAViolationType.ANSWER.name() => new NotificationParams(
				SLAConst.ALERT_NO_ANSWER_VT,
				Label.MessagingMissedNotifBody,
				Label.MessagingMissedNotifSubject
			)
		};

		Map<String, NotificationParams> bouncedViolations = new Map<String, NotificationParams>{
			SLAConst.SLAViolationType.BOUNCED.name() => new NotificationParams(
				SLAConst.ALERT_NO_ANSWER_VT,
				Label.MessagingBouncedNotifSubject,
				Label.MessagingBouncedNotifBody
			)
		};
		notifications.put(
			SLAConst.SLAViolationType.ANSWER,
			new Map<String, Map<string, NotificationParams>>{
				'Chat' => answerViolations
			}
		);

		notifications.put(
			SLAConst.SLAViolationType.BOUNCED,
			new Map<String, Map<string, NotificationParams>>{
				'Chat' => bouncedViolations
			}
		);
		Map<String, Milestone_Time_Settings__mdt> milestonesMessagingByType = new Map<String, Milestone_Time_Settings__mdt>();
		for (String key : milestonesByType.keySet()) {
			if (key.contains('Messaging')) {
				milestonesMessagingByType.put(key, milestonesByType.get(key));
			}
		}
		// ASA notifications By Tiers
		Map<String, Map<string, NotificationParams>> asaByTiers = getNotificationParamFromSettings(
			milestonesMessagingByType
		);
		notifications.put(SLAConst.SLAViolationType.ASA, asaByTiers);

		this.notificationTierByViolationType = new Map<SLAConst.SLAViolationType, Boolean>{
			SLAConst.SLAViolationType.ANSWER => false,
			SLAConst.SLAViolationType.BOUNCED => false,
			SLAConst.SLAViolationType.ASA => true
		};
		return notifications;
	}

	protected override void setNotificationTargetId(
		Messaging.CustomNotification notification,
		SLAViolationInfo info
	) {
		SLAChatViolationInfo violationInfo = (SLAChatViolationInfo) info;
		notification.setTargetId(violationInfo.chatId);
	}
}