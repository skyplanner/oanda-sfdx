/*
 * 
 */
public with sharing class BatchAssignLeads implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{

	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchAssignLeads() {
		query = 'SELECT Id, RecordTypeId, Email, OwnerId FROM Lead WHERE RecordTypeId=\'' + RecordTypeUtil.getLeadRetailId() + '\' AND IsConverted = false';
	}

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, RecordTypeId, Email, OwnerId FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

   	public Database.QueryLocator start(Database.BatchableContext bc) {
		CustomSettings.setEnableLeadConversion(false);
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		
   		if (CustomSettings.isEnableLeadConversion()) {
   			throw new ApplicationException('Lead conversion must be disabled before assigning migrated leads');
   		}
    	LeadBatchUtil.assignLeadsWithActivities((Lead[]) batch);
	}

	public void finish(Database.BatchableContext bc) {
		CustomSettings.setEnableLeadConversion(true);
		try {
			BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
		}
		catch (Exception e) {}
	}
	
	// BatchAssignLeads.executeBatch()
	public static Id executeBatch() {
		return Database.executeBatch(new BatchAssignLeads());
	}
}