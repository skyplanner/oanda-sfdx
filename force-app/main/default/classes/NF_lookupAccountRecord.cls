/**
* Created by Neuraflash LLC on 03/01/19.
* Implementation : Chatbot
* Summary : Lookup Account records by searching Email  & Return Boolean
* Details : 1. Searches for an Account record by email provided.
*           2. Return Boolean if Account found/not found.
* 
*/

global without sharing class NF_lookupAccountRecord implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
            Map<String, String> eventParams = new Map<String, String>();
            String emailValue =(String)paramMap.get('email'); //get email from DF
            List<Account> listAccount = [SELECT Id FROM Account WHERE PersonEmail =: emailValue
                                         ORDER BY CreatedDate ASC NULLS FIRST LIMIT 1];
            if(!listAccount.isEmpty()){
                eventParams.put('result','Account');
            }
			else{
                List<Lead> listLead = [SELECT Id FROM Lead WHERE Email =: emailValue
                                         ORDER BY CreatedDate ASC NULLS FIRST LIMIT 1];
                if(!listLead.isEmpty()){
                    eventParams.put('result','Lead');
                }
                else{
                    eventParams.put('result','None');
                }
            }
            eventParams.put('email',emailValue);
            return JSON.serialize(eventParams);
    }
}