/* Name: FxAccountApiRestResource
 * Description : New Apex Class to handle fxAcc(User) updates from user-api
 * Author: Mikolaj Juras
 * Date : 2023 September 27
 */
@RestResource(urlMapping='/api/v1/fxAccount/*')
global class FxAccountApiRestResource {

    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try {

            String reqBody = '';
            if(req.requestBody != null){
                reqBody = req.requestBody.toString().trim();
            }
            
            String fxAccountTradeUserIdString = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            //as first step save the payload in SF
            
            LogEvt__e log = new LogEvt__e(
                                    Type__c = 'Info',
                                    Category__c = '/api/v1/fxAccount/*' + ' Input payload for '+ fxAccountTradeUserIdString,
                                    Message__c = 'User update/insert payload: '+ fxAccountTradeUserIdString,
                                    Details__c = reqBody,
                                    Id__c = (String)UserInfo.getUserId());
            EventBus.publish(log);
            //create 'user' payload event
            FxAccount_Upsert__e fxAccEvent = new FxAccount_Upsert__e();
            fxAccEvent.fxTrade_User_Id__c = Integer.valueOf(fxAccountTradeUserIdString);
            fxAccEvent.User_Payload__c = reqBody;
            fxAccEvent.Replay_Counter__c = 0;
            
            Database.SaveResult sr = EventBus.publish(fxAccEvent);
            if (!sr.isSuccess()) {
                LogEvt__e errLog = new LogEvt__e(
                Type__c = 'Info',
                Category__c = '/api/v1/fxAccount/*' + ' Input payload for '+ fxAccountTradeUserIdString,
                Message__c = 'Failed to publish event: '+ fxAccEvent,
                Details__c = reqBody,
                Id__c = (String)UserInfo.getUserId());
                EventBus.publish(errLog);
            }
            
            //provide response to user-api
            res.responseBody = Blob.valueOf('Record will be processed.');
            res.statusCode = 200;
            return;

        } catch (Exception ex) { 
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                FxAccountUpsertTriggerHandler.LOGGER_CATEGORY_URL,
                FxAccountUpsertTriggerHandler.LOGGER_CATEGORY,
                res.statusCode + ' ' + respBody);
                return;
        }
    }

    @HttpGet
    global static void doGet() {
        /*
        Accepted calls:
        '/api/v1/fxAccount/123123' - live
        '/api/v1/fxAccount/123123/demo' - demo 
        */
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String  fxAccountTradeUserIdString = '';
        
        try {
            Set<String> additionalFields = new Set<String>{'LastModifiedDate, City__c, Country__c, Street__c, State__c, Trading_Experience_Duration__c, Trading_Experience_Type__c'};
            String lastParam = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            String noDemoUrl ='';
            Boolean isDemo = lastParam == 'demo' ? true : false; 
            fxAccountTradeUserIdString = isDemo 
                                         ? req.requestURI.remove('/demo').substring(req.requestURI.remove('/demo').lastIndexOf('/')+1) 
                                         : lastParam;

            Set<String> fxAccountsFields = new Set<String>();
            for (fx_Account_Api_Mapping__mdt mapping : fx_Account_Api_Mapping__mdt.getAll().values()) {
                if(mapping.fxAcc_Field__c != null) {
                    fxAccountsFields.add(mapping.fxAcc_Field__c);
                }
            }
            fxAccountsFields.addAll(additionalFields);
            Id currentRecordTypeId = isDemo 
                                    ? Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get('Retail Practice').getRecordTypeId() 
                                    : Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get('Retail Live').getRecordTypeId();
            String query = 'SELECT Id, ';
            query += String.join(new List<String>(fxAccountsFields), ',');
            query += ' FROM fxAccount__c WHERE fxTrade_User_Id__c =';
            query += Integer.valueOf(fxAccountTradeUserIdString);
            query += ' AND RecordTypeId = \'';
            query += currentRecordTypeId;
            query += '\'';
            query += ' LIMIT 1';

            List<fxAccount__c> fxa = Database.query(query);
            if (fxa.isEmpty()) {
                String demoOrLive = isDemo ? ' demo' : ' live';
                res.responseBody = Blob.valueOf('No fxAccounts found for user Id:' + fxAccountTradeUserIdString + demoOrLive + ' user');
                res.statusCode = 404;
                return;
            }

            Map<String,Object> fxaWithEmptyVal = new Map<String,Object>();
            for (fx_Account_Api_Mapping__mdt mapping : fx_Account_Api_Mapping__mdt.getAll().values() ) {
                if(!mapping.Map_on_fxAcc__c) {
                    continue;
                }

                Object value = mapping.fxAcc_Field__c != null ? fxa[0].get(mapping.fxAcc_Field__c) : null;

                if(value == null && mapping.fxAcc_Field__c != null ) { 
                    continue;
                }
                switch on mapping.Field_Type__c {

                    when 'String' {
                        fxaWithEmptyVal.put(mapping.DeveloperName, String.valueOf(value));
                    }
                    when 'Date' {
                        fxaWithEmptyVal.put(mapping.DeveloperName, Date.valueOf(value));
                    }
                    when 'Datetime' {
                        fxaWithEmptyVal.put(mapping.DeveloperName, Datetime.valueOf(value));
                    }
                    when 'Integer' {
                        fxaWithEmptyVal.put(mapping.DeveloperName, Integer.valueOf(value));
                    }
                    when 'Decimal' {
                        fxaWithEmptyVal.put(mapping.DeveloperName, (Decimal) value);
                    }
                    when 'DIVISION_NAME_MAPPING' {
                        String keyValue = String.valueOf(value);
                        String mappingValue = Constants.REVERSED_DIVISION_NAME_MAPPING.get(keyValue);

                        fxaWithEmptyVal.put(mapping.DeveloperName, mappingValue);
                    }
                    when 'EMPLOYMENT_STATUS_MAPPING' {
                        String keyValue = String.valueOf(value);
                        String mappingValue = Constants.REVERSED_EMPLOYMENT_STATUS_MAPPING.get(keyValue);

                        fxaWithEmptyVal.put(mapping.DeveloperName, mappingValue);
                    }
                    when 'ADDRESS' {
                        FxAccountUpsertTriggerHandler.AddressDTO addr = new FxAccountUpsertTriggerHandler.AddressDTO();
                        addr.city = fxa[0].City__c;
                        addr.country = fxa[0].Country_Name__c; 
                        addr.postcode = fxa[0].Postal_Code__c;
                        addr.street1 = fxa[0].Street__c?.substringBeforeLast(' ');
                        addr.street2 = fxa[0].Street__c?.substringAfterLast(' ');
                        addr.region = fxa[0].State__c;   
                        fxaWithEmptyVal.put(mapping.DeveloperName, addr);           
                    }
                    when 'BOOL_STRING_TO_YES_NO' {
                        Boolean boolValue;
                        switch on String.valueOf(value) {
                            when 'Yes' {
                                boolValue = true;
                            }
                            when 'No' {
                                boolValue = false;
                            }
                        }
                        fxaWithEmptyVal.put(mapping.DeveloperName, boolValue);
                    }
                    when 'CUSTOM_BOOLEAN' {
                        String temp = String.valueOf(value);
                        fxaWithEmptyVal.put(mapping.DeveloperName, temp);
                    }
                    when 'TRADING_EXP' {
                        List<String> durations = fxa[0].Trading_Experience_Duration__c?.split(',');
                        List<String> types = fxa[0].Trading_Experience_Type__c?.split(',');

                        if(durations == null || types == null) {
                            continue;
                        }

                        List<TradingExperience> tradingExperiences = new List<TradingExperience>();

                        for(Integer i = 0; i < durations.size(); i++) {
                            TradingExperience singleTradingExp = new TradingExperience();
                            singleTradingExp.experience_duration = durations[i];
                            singleTradingExp.experience_type = types[i];
                            tradingExperiences.add(singleTradingExp);
                        }
                        fxaWithEmptyVal.put(mapping.DeveloperName, tradingExperiences);           
                    }
                    when else {
                        fxaWithEmptyVal.put(mapping.DeveloperName, value);
                    }
                }
            }

            res.responseBody = Blob.valueOf(JSON.serialize(fxaWithEmptyVal));
            res.statusCode = 200;
            res.addHeader('Content-Type', 'application/json');
            return;
        } catch (Exception ex) { 
            
            String respBody = 'Sth went wrong to get user : ' + fxAccountTradeUserIdString + ' failed. Err msg: ' + ex.getMessage() + ex.getLineNumber() + ex.getStackTraceString();
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                FxAccountUpsertTriggerHandler.LOGGER_CATEGORY_URL,
                FxAccountUpsertTriggerHandler.LOGGER_CATEGORY,
                respBody+ ex.getMessage() + ex.getStackTraceString());
            return;
        }
    }

    public class TradingExperience{
        public String experience_duration{ get; set; }
        public String experience_type{ get; set; }
    }
}