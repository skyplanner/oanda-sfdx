@IsTest
public with sharing class BatchLeadConversionScheduleableTest {
	
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    static testMethod void testBatchLeadConversionTraded() {
    	
    	System.runAs(UserUtil.getSystemUserForTest()) {
			Lead lead = new Lead();
			lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead.LastName = 'Schmoe';
			lead.Email = 'joe@example.org';
			
			insert lead;
			
			fxAccount__c liveAccount = new fxAccount__c();
			liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
			liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			liveAccount.Lead__c = lead.Id;
			
			insert liveAccount;
			
			liveAccount.Trigger_Lead_Assignment_Rules__c = false;
			update liveAccount;
		
			lead = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead.Id];
			
			System.assert(!lead.IsConverted);
			
			Test.startTest();
			
			BatchConvertLeadsScheduleable.executeBatch();
			
			Test.stopTest();
			
			lead = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead.Id];
			
			System.assert(lead.IsConverted);
    	}
    }
    
    static testMethod void testBatchLeadConversionNoEmailOnLead() {
    	
    	System.runAs(UserUtil.getSystemUserForTest()) {
    		
    		// Lead has no email
			Lead lead = new Lead();
			lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead.LastName = 'Schmoe';
			
			try {
				insert lead;
				system.assert(false);
			}
			catch (Exception e) {}
    	}
    }
    
    static testMethod void testBatchLeadConversionDuplicateLead() {
    	
    	System.runAs(UserUtil.getSystemUserForTest()) {
    		
			Lead lead1 = new Lead();
			lead1.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead1.LastName = 'Schmoe';
			lead1.Email = 'joe@example.org';
			
			insert lead1;
			
			
			fxAccount__c liveAccount1 = new fxAccount__c();
			liveAccount1.Funnel_Stage__c = FunnelStatus.TRADED;
			liveAccount1.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			liveAccount1.Lead__c = lead1.Id;
			
			insert liveAccount1;
			
			
			Lead lead2 = new Lead();
			lead2.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead2.LastName = 'Schmoe';
			lead2.Email = 'joe@example.org';
			
			insert lead2;
			
			
			
			fxAccount__c liveAccount2 = new fxAccount__c();
			liveAccount2.Funnel_Stage__c = FunnelStatus.TRADED;
			liveAccount2.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			liveAccount2.Lead__c = lead2.Id;
			
			insert liveAccount2;
			
			liveAccount1.Trigger_Lead_Assignment_Rules__c = false;
			liveAccount2.Trigger_Lead_Assignment_Rules__c = false;
		    List<fxAccount__c> fxas = new fxAccount__c[] {liveAccount1, liveAccount2};
		    update fxas;
		
		
			lead1 = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead1.Id];
			lead2 = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead2.Id];
			
			System.assert(!lead1.IsConverted);
			System.assert(!lead2.IsConverted);
			
			Test.startTest();
			
			BatchConvertLeadsScheduleable.executeBatch();
			
			Test.stopTest();
			
			lead1 = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead1.Id];
			lead2 = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead2.Id];
			
			System.assert(!lead1.IsConverted);
			System.assert(!lead2.IsConverted);
    	}
    }
    
    static testMethod void testBatchLeadConversionNoFxAccount() {
    	
    	System.runAs(UserUtil.getSystemUserForTest()) {
			Lead lead = new Lead();
			lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead.Funnel_Stage__c = FunnelStatus.TRADED;
			lead.LastName = 'Schmoe';
			lead.Territory__c = 'North America';
			lead.Email = 'joe@example.org';
			
			insert lead;
			
			BatchConvertLeadsScheduleable.executeInline();
			
			lead = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead.Id];
			
			System.assert(!lead.IsConverted);
			
			fxAccount__c liveAccount = new fxAccount__c();
			liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
			liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			liveAccount.Lead__c = lead.Id;
			
			insert liveAccount;
			
			liveAccount.Trigger_Lead_Assignment_Rules__c = false;
			update liveAccount;
			
			BatchConvertLeadsScheduleable.executeInline();
		
			lead = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead.Id];
			
			System.assert(lead.IsConverted);
    	}
    }
    
    static testMethod void testBatchLeadConversionAssigned() {
    	
    	System.runAs(UserUtil.getSystemUserForTest()) {
			Lead lead = new Lead();
			lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			lead.LastName = 'Schmoe';
			lead.Territory__c = 'North America';
			lead.Email = 'joe@example.org';
			
			insert lead;
			
			fxAccount__c liveAccount = new fxAccount__c();
			liveAccount.Funnel_Stage__c = FunnelStatus.FUNDED;
			liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			liveAccount.Lead__c = lead.Id;
			
			insert liveAccount;
			
			liveAccount.Trigger_Lead_Assignment_Rules__c = false;
			update liveAccount;
		
			lead = [SELECT Id, IsConverted FROM Lead WHERE Id = :lead.Id];
			
			System.assert(!lead.IsConverted);
			
			LeadUtil.useDefaultAssignment(lead);
			update lead;
			
			Test.startTest();
			
			BatchConvertLeadsScheduleable.executeBatch();
			
			Test.stopTest();
			
			lead = [SELECT Id, IsConverted, OwnerId FROM Lead WHERE Id = :lead.Id];
			
			System.assert(lead.IsConverted);
			//System.assert(lead.OwnerId != UserUtil.getSystemUserForTest().Id);
    	}
    }
    
    private static void insertFxtradeUser() {
    	System.runAs(SYSTEM_USER) {
    		
    		String uname = 'joe' + Math.round(Math.random() * 1000000);
    		
    		Lead lead = new Lead();
    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		lead.LastName = 'Schmoe';
    		lead.Email = uname + '@example.org';
    		lead.Territory__c = 'North America';
    		
    		insert lead;
    		
    		fxAccount__c practiceAccount = new fxAccount__c();
    		practiceAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    		practiceAccount.Name = uname;
    		practiceAccount.Lead__c = lead.Id;
    		practiceAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
    		
    		fxAccount__c fxAccount = new fxAccount__c();
    		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		fxAccount.Name = uname;
    		fxAccount.Lead__c = lead.Id;
    		fxAccount.Funnel_Stage__c = FunnelStatus.TRADED;
    		
    		insert fxAccount;
    	}
    }
}