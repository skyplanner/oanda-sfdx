/**
 * @Description  : Helper class for CaseTriggerHandler
 * Logic extracted from CaseTriggerHandler.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-22
 **/
public inherited sharing class CaseTriggerHandlerHelper {
    private static final String APPROVED_STATUS = 'Approved to Fund';
    public static final String CX_CASES_QUEUE = 'CX Cases';

    /**
     * @description Method populates a list of CaseConditionalUpdate objects and updates the Case Division field
     * @author Jakub Fik | 2024-05-22
     * @param newCases
     * @param oldCases
     **/
    public static void performConditionalUpdates(List<Case> newCases, List<Case> oldCases) {
        Case newCase;
        Case oldCase;
        List<CaseConditionalUpdate> updates;
        List<String> workflows;

        // we will perform these workflows
        workflows = new List<String>{ 'WF_QI_Case_Owner' };

        // we'll save updates
        updates = new List<CaseConditionalUpdate>();

        // we work for every record
        for (Integer i = 0; i < newCases.size(); i++) {
            newCase = newCases.get(i);
            oldCase = oldCases != null ? oldCases.get(i) : null;
            updates.add(new CaseConditionalUpdate(newCase, oldCase));

            // Update Case Division field based on Lead/Account Division field
            if (newCase.Lead_Account_Division2__c != null) {
                newCase.Case_Division__c = newCase.Lead_Account_Division2__c;
            }
        }

        // we then execute updates
        ConditionalUpdate.performConditionalUpdates(updates, workflows);
    }


    public static void changeRTToSupport(Case newCase){
            newCase.RecordTypeId = RecordTypeUtil.getSupportCaseTypeId();
            newCase.Inquiry_Nature__c = null;
            newCase.Type__c = null;
            newCase.Subtype__c = null;
    }

    /**
     * @description Calculate time between Case Created Date and Close Date considering TMS business hours
     * @author Agnieszka Kajda | 2024-07-04
     * @param newCase
     **/
    public static void setTMSResolutionTime(Case newCase){
        Decimal numberOfHours =
                (Decimal) BusinessHours.diff(BusinessHoursUtil.getTMSBusinessHoursId(), newCase.CreatedDate, System.now());
        newCase.TMS_Resolution_Time__c = numberOfHours.divide(3600000,2);

    }

    /**
     * @description Links cases to corresponding Account, Contact, or Lead based on email, preparing them for update.
     * Note: Actual database update is not performed within this method.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     **/
    public static void linkToAccountContactOrLead(List<Case> cases) {
        Map<String, List<Case>> casesToLinkByEmail = new Map<String, List<Case>>();
        for (Case c : cases) {
            if (
                c.Chat_Web_Email__c != null &&
                c.Chat_Web_Email__c != '' &&
                c.Lead__c == null &&
                c.AccountId == null &&
                c.ContactId == null &&
                ! RecordTypeUtil.isCaseOTMSRecordTypes(c.RecordTypeId)
            ) {
                List<Case> casesToLink = casesToLinkByEmail.get(c.Chat_Web_Email__c);
                if (casesToLink == null) {
                    casesToLink = new List<Case>();
                    casesToLinkByEmail.put(c.Chat_Web_Email__c, casesToLink);
                }
                casesToLink.add(c);
            }
        }

        if (casesToLinkByEmail.size() > 0) {
            List<Case> casesToUpdate = new List<Case>();
            Map<String, Contact> contactByEmail = new Map<String, Contact>();
            for (Contact con : [
                SELECT Id, AccountId, Email
                FROM Contact
                WHERE
                    Email IN :casesToLinkByEmail.keySet()
                    AND Account.RecordTypeId = :RecordTypeUtil.getPersonAccountRecordTypeId()
            ]) {
                contactByEmail.put(con.Email, con);
            }

            for (Contact con : contactByEmail.values()) {
                List<Case> casesToLink = casesToLinkByEmail.get(con.Email);
                if (casesToLink != null) {
                    for (Case c : casesToLink) {
                        c.ContactId = con.Id;
                        c.AccountId = con.AccountId;
                        casesToUpdate.add(c);
                    }
                }
                casesToLinkByEmail.remove(con.Email);
            }

            if (casesToLinkByEmail.size() > 0) {
                Map<String, Lead> leadByEmail = new Map<String, Lead>();
                for (Lead l : [
                    SELECT Id, Email
                    FROM Lead
                    WHERE
                        Email IN :casesToLinkByEmail.keySet()
                        AND IsConverted = FALSE
                        AND RecordTypeId = :RecordTypeUtil.getLeadMergeRecordTypeIds()
                ]) {
                    leadByEmail.put(l.Email, l);
                }
                for (Lead l : leadByEmail.values()) {
                    List<Case> casesToLink = casesToLinkByEmail.get(l.Email);
                    if (casesToLink != null) {
                        for (Case c : casesToLink) {
                            c.Lead__c = l.Id;
                            casesToUpdate.add(c);
                        }
                    }
                    casesToLinkByEmail.remove(l.Email);
                }
            }
            if (casesToUpdate.size() > 0) {
                // update casesToUpdate;
            }
        }
    }

    /**
     * @description Transfers ownership of cases to delegated owners based on provided mappings.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     * @param ownerUsersIdsByCase
     **/
    public static void transferCases(List<Case> cases, Map<Id, Id> ownerUsersIdsByCase) {
        Map<Id, User> ownerUsers = new Map<Id, User>(
            [
                SELECT Id, Delegated_Case_Owner__c
                FROM User
                WHERE Id IN :ownerUsersIdsByCase.values() AND Delegated_Case_Owner__c != NULL
            ]
        );
        for (Case c : cases) {
            if (ownerUsersIdsByCase.containsKey(c.Id) && ownerUsers.containsKey(ownerUsersIdsByCase.get(c.Id))) {
                c.OwnerId = ownerUsers.get(ownerUsersIdsByCase.get(c.Id)).Delegated_Case_Owner__c;
            }
        }
    }

    /**
     * @description Copies the Reason Failed value to a text field, truncating if necessary.
     * @author Jakub Fik | 2024-05-22
     * @param c
     **/
    public static void copyReasonFailed(Case c) {
        if (c.Reason_Failed__c == null) {
            c.Reason_Failed_text__c = null;
        } else if (c.Reason_Failed__c.length() > 255) {
            c.Reason_Failed_text__c = c.Reason_Failed__c.substring(0, 255);
        } else {
            c.Reason_Failed_text__c = c.Reason_Failed__c;
        }
    }

    /**
     * Gets the users to exclude in the First Owner from the custom metadata
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   26/04/2021
     * @return: Set<String>
     */
    public static Set<Id> getUsersToExclude() {
        Set<Id> result = new Set<Id>();
        String postFix = '';
        if ([SELECT IsSandbox FROM Organization].IsSandbox) {
            postFix = UserInfo.getUserName().substringAfterLast('.com');
        }
        List<String> userNames = new List<String>();
        for (First_Owner_Exclusion__mdt foe : [SELECT MasterLabel FROM First_Owner_Exclusion__mdt]) {
            userNames.add(foe.MasterLabel + postFix);
        }
        for (User usr : [SELECT Id FROM User WHERE Username IN :userNames]) {
            result.add(usr.Id);
        }
        return result;
    }

    /**
     * This method fills out the First Owner field from the OwnerId
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   06/04/2021
     * @return: void
     * @param:  pCases, the list of cases to process
     */
    public static void fillFirstOwnerUser(Set<Id> usersToExclude, Case cs) {
        if (
            String.isBlank(cs.First_Owner__c) &&
            String.isNotBlank(cs.OwnerId) &&
            cs.OwnerId.to15().startsWith('005') &&
            !usersToExclude.contains(cs.OwnerId)
        ) {
            cs.First_Owner__c = cs.OwnerId;
        }
    }

    /**
     * Find cases with Verification Code
     * Call method for re-insert Verification Code into Big Object
     **/
    public static void verificationCodeRelease(List<Case> cases) {
        List<Verification_Code_Setting__c> verifCode = new List<Verification_Code_Setting__c>();

        for (Case c : cases) {
            if (!String.isBlank(c.Verification_code__c)) {
                verifCode.add(
                    new Verification_Code_Setting__c(
                        Name = c.Verification_code__c,
                        Order__c = c.Verification_Code_Order__c
                    )
                );
            }
        }
        if (!Test.isRunningTest()) {
            System.enqueueJob(new VerificationCodeBigObjectJob(verifCode));
        }
    }

    /**
     * on CA case creation, mark fXAccount__C hasComplyAdvantageCaseFlag= true
     */
    public static void updateFxAcCACaseFlag(List<Case> newList) {
        String complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();

        fxAccount__c[] fxAccsToUpdate = new List<fxAccount__c>{};
        for (Case c : newList) {
            if (c.RecordTypeId == complianceCheckCaseRecordTypeId && String.isNotBlank(c.fxAccount__c)) {
                fxAccount__c fxa = new fxAccount__c(Id = c.fxAccount__c);
                fxa.Has_ComplyAdvantage_Case__c = true;
                fxAccsToUpdate.add(fxa);
            }
        }

        if (!fxAccsToUpdate.isEmpty()) {
            DatabaseUtil.updateRecords(fxAccsToUpdate, false, true);
        }
    }

    /**
     * @description Validates the approval of a case based on specific conditions.
     * Adds an error if the case does not meet the required risk tolerance criteria.
     * @author Jakub Fik | 2024-05-22
     * @param newCase
     * @param oldStatus
     **/
    public static void validateCaseApproval(Case newCase, String oldStatus) {
        if (
            CaseUtil.isOnBoardingType(newCase) &&
            newCase.fxAccount_Division_Name__c == 'OANDA Canada' &&
            newCase.Risk_Tolerance_CAD__c == null &&
            newCase.Application_Status__c == APPROVED_STATUS &&
            oldStatus != APPROVED_STATUS
        ) {
            newCase.addError('Error: RISK TOLERANCE AMOUNT HAS NOT BEEN SET.');
        }
    }

    /**
     * @description Determines and manages the transfer of case ownership based on specific conditions.
     * @author Jakub Fik | 2024-05-22
     * @param c
     * @param ownerUsersIdsByCase
     * @param oldCase
     **/
    public static void getCasesToTransfer(Case c, Map<Id, Id> ownerUsersIdsByCase, Case oldCase) {
        if ((oldCase != null && oldCase.OwnerId != c.OwnerId) && c.OwnerId.to15().startsWith('005')) {
            ownerUsersIdsByCase.put(c.Id, oldCase.OwnerId);
        } else if (
            (oldCase != null &&
            oldCase.Status != c.Status) &&
            c.Status == 'Closed' &&
            c.Origin == 'Phone' &&
            c.OwnerId == UserUtil.getQueueId('CX Cases')
        ) {
            c.OwnerId = UserInfo.getUserId();
        }
    }

    /**
     * @description Updates the 'Case_Last_Updated_Date__c' field for all 'Comply_Advantage_Search__c' records
     * associated with the provided case IDs.
     * @author Jakub Fik | 2024-05-22
     * @param caseIds
     **/
    public static void updateLastUpdateDateField(Set<Id> caseIds) {
        List<Comply_Advantage_Search__c> searches = [
            SELECT Id
            FROM Comply_Advantage_Search__c
            WHERE Case__c IN :caseIds
        ];
        for (Comply_Advantage_Search__c search : searches) {
            search.Case_Last_Updated_Date__c = Datetime.now();
        }

        if (!searches.isEmpty()) {
            update searches;
        }
    }
}