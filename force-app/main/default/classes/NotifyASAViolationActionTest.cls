/**
 * @File Name          : NotifyASAViolationActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:40:04 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/29/2024, 9:32:54 AM   aniubo     Initial Version
 **/
@isTest
private class NotifyASAViolationActionTest {
	@testSetup
	static void setup() {
		SLAViolationsTestDataFactory.createTestChats();
	}

	@isTest
	static void chatAsaViolation() {
		List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
		List<NotifyASAViolationAction.NotifyASAViolationData> violationsData = new List<NotifyASAViolationAction.NotifyASAViolationData>();
		for (Id chatId : chatIdList) {
			NotifyASAViolationAction.NotifyASAViolationData data = new NotifyASAViolationAction.NotifyASAViolationData();
			data.recordId = chatId;
			data.isLiveChat = true;
			data.violationType = 'ASA';
			violationsData.add(data);
		}
		Test.startTest();
		NotifyASAViolationAction.notifyASAViolation(violationsData);
		Test.stopTest();
		Integer count = [SELECT COUNT() FROM SLA_Violation__c];
		System.assertEquals(
			chatIdList.size(),
			count,
			'SLA Violation Count must be equal to chatIdList size'
		);
	}
	@isTest
	static void messagingAsaViolation() {
		List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
		List<NotifyASAViolationAction.NotifyASAViolationData> violationsData = new List<NotifyASAViolationAction.NotifyASAViolationData>();
		NotifyASAViolationAction.NotifyASAViolationData data = new NotifyASAViolationAction.NotifyASAViolationData();
			data.recordId = null;
			data.isLiveChat = false;
			violationsData.add(data);
		Test.startTest();
		NotifyASAViolationAction.notifyASAViolation(violationsData);
		Test.stopTest();

		System.assertEquals(
			0,
			0,
			'SLA Violation Count must be equal to Zero'
		);
	}
}