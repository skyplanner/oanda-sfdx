public class BatchDeleteLeadFieldHistory implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	public BatchDeleteLeadFieldHistory() {
		//Query for all LeadHistory
		query = 'SELECT Id, LeadId, Field FROM LeadHistory WHERE Field = \'Email\' OR Field = \'Phone\'';
	}

	public BatchDeleteLeadFieldHistory(String q) {
   		query = q;
  	}
	  public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, LeadId, Field FROM LeadHistory WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		delete batch;
	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}


}