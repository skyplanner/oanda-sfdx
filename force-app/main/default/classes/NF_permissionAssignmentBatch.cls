/**
* Created by Neuraflash LLC on 20/06/18.
* Implementation : Chatbot
* Summary : Assign Permission set to users
* Details :
*
*/
public class NF_permissionAssignmentBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{
    public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public NF_permissionAssignmentBatch(String query) {
        if (String.isEmpty(query)) {
            query = Label.permissionBatchQuery;
        }
        this.query = query;
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM User WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query); //fetch the users to assign permission set
    }

    public void execute(Database.BatchableContext BC,List<User> scope){
        set<ID> setofUserId = new set<ID>();
        map<ID,ID> mapofAssignment = new map<ID,ID>();
        for(User eachUser: scope){
            setofUserId.add(eachUser.id);
        }
        List<PermissionSetAssignment> setAssign = new list<PermissionSetAssignment>();
        List<PermissionSetAssignment> existingPerm = [select AssigneeId, PermissionSetId from permissionsetassignment where AssigneeId in: setofUserId];
        List<PermissionSet> listofPermission = [Select id from PermissionSet where name =: Label.NFChatUser_Custom or name =: Label.NFChat_User];
        for(PermissionSetAssignment eachPerm: existingPerm){
            mapofAssignment.put(eachPerm.PermissionSetId,eachPerm.AssigneeId);
        }
        for(User eachUser: scope){
            for(PermissionSet eachSet: listofPermission){
                if(eachUser.id != mapofAssignment.get(eachSet.id)){
                    setAssign.add(new PermissionSetAssignment(AssigneeId = eachUser.id,PermissionSetId =eachSet.id));
                }
            }
        }
        if(!setAssign.isEmpty()){
            Database.upsert(setAssign,false);
        }        
    }

    public void finish(Database.BatchableContext BC){
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }   
}