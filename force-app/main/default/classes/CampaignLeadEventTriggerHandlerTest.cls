@isTest
public with sharing class CampaignLeadEventTriggerHandlerTest {
    static String payloadTemplate = '{"first_name": "name" ,'
                                   + '"last_name": "surname",'
                                    + '"email":"name.surname@email.ff",'
                                    + '"phone":"+48512345678",'
                                    + '"country":"Canada",'
                                    + '"sf_campaign_names" : ["OEL Platform test"],'
                                    + '"division_id": "11",'
                                    + '"raf_first_name":"refname",'
                                    + '"raf_last_name":"refsurname",'
                                    + '"raf_email":"refame.refsurname@email.ff",'
                                    + '"raf_owner":"test",'
                                    + '"dummy":"test"'
                                    + '}';

    private static string restUrl = '/api/v1/campaignLeads/';
    private static string postMethod = 'POST';
                        
    @isTest
    static void testDoPostInsert() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(payloadTemplate);
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);
        CamapinLeadsApiRestResource.doPost();
        Test.getEventBus().deliver();
        Test.stopTest();
    
        Assert.areEqual(200, res.statusCode, 'record will be processed');
        Campaign_Lead__c campLeadAfter = [Select id, Email__c FROM Campaign_Lead__c LIMIT 1];
        Assert.areEqual('name.surname@email.ff', campLeadAfter.Email__c, 'Email should be updated');
        Lead leadAfter = [Select id, Email, FirstName, fxTrade_One_Id__c, RecordTypeId FROM Lead LIMIT 1 ];
        Assert.areEqual('name.surname@email.ff', leadAfter.Email, 'Email should be updated');
        Assert.areEqual('name', leadAfter.FirstName, 'Name should be updated');
        Assert.areEqual('aabb713d-2d70-11ef-951a-42010a7801d3', leadAfter.fxTrade_One_Id__c, 'Name should be updated');
        Assert.isTrue(LeadUtil.isStandardLead(leadAfter), 'should be standard lead');
    }

    @isTest
    static void testDoPostUpdateLead() {

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='name.surname@email.ff',
            RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
            
        insert lead1;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(payloadTemplate);
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
        Test.setMock(HttpCalloutMock.class, mock);

        CamapinLeadsApiRestResource.doPost();

        Test.getEventBus().deliver();
        Test.stopTest();
    
        Assert.areEqual(200, res.statusCode, 'record will be processed');
        Campaign_Lead__c campLeadAfter = [Select id, Email__c FROM Campaign_Lead__c ];
        Assert.areEqual('name.surname@email.ff', campLeadAfter.Email__c, 'Email should be updated');
        Lead leadAfter = [Select id, Email, LastName, RecordTypeId, RAF_Referrer_First_Name__c FROM Lead WHERE Id = :lead1.Id];
        Assert.areEqual('name.surname@email.ff', leadAfter.Email, 'Email should be updated');
        Assert.areEqual('test lead', leadAfter.LastName, 'LastName should remain');
        Assert.areEqual('refname', leadAfter.RAF_Referrer_First_Name__c, 'LastName should remain');
    }

    @isTest
    static void testDoPostUpdateContact() {

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='name.surname@email.ff',
            RecordTypeId = RecordTypeUtil.getLeadRetailId());
            
        insert lead1;
        LeadUtil.convertLeads(new List<Lead>{lead1});
        Lead leadbefore = [Select id, Email, LastName, RecordTypeId, RAF_Referrer_First_Name__c, IsConverted, ConvertedContactId FROM Lead WHERE Id = :lead1.Id];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(payloadTemplate);
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
        Test.setMock(HttpCalloutMock.class, mock);

        CamapinLeadsApiRestResource.doPost();
        Test.getEventBus().deliver();
        Test.stopTest();
    
        Assert.areEqual(200, res.statusCode, 'record will be processed');
        Campaign_Lead__c campLeadAfter = [Select id, Email__c FROM Campaign_Lead__c limit 1];
        Assert.areEqual('name.surname@email.ff', campLeadAfter.Email__c, 'Email should be updated');
        Lead leadAfter = [Select id, Email, LastName, RecordTypeId, RAF_Referrer_First_Name__c, IsConverted, ConvertedContactId FROM Lead WHERE Id = :lead1.Id];
        Assert.areEqual('name.surname@email.ff', leadAfter.Email, 'Email should be updated');
        Assert.areEqual('test lead', leadAfter.LastName, 'LastName should remain');
        Assert.areEqual(null, leadAfter.RAF_Referrer_First_Name__c, 'Raf should not be updated for converted lead ');
        Assert.isTrue(leadAfter.IsConverted, 'Lead is converted');
        Contact contactAfter = [Select RAF_Referrer_Last_Name__c, RAF_Referrer_Email_Address__c FROM Contact WHERE Id = :leadAfter.ConvertedContactId];
        Assert.areEqual('refsurname', contactAfter.RAF_Referrer_Last_Name__c, 'Raf should not be updated for converted lead ');
        Assert.areEqual('refame.refsurname@email.ff', contactAfter.RAF_Referrer_Email_Address__c, 'Raf should not be updated for converted lead ');
    }

    @isTest
    static void testDoPostCheckError() {

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='name.surname@email.ff',
            RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
            
        insert lead1;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = postMethod;
        //make JSON invalid
        String incorrectPaylod = payloadTemplate.replaceFirst('","', '+++');
        req.requestBody = Blob.valueOf(incorrectPaylod);
    
        RestContext.request = req;
        RestContext.response = res;
        try{
            Test.startTest();
            CalloutMock mock = new CalloutMock(
                200,
                '{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
                'OK',
                null
            );
            CamapinLeadsApiRestResource.doPost();
            Test.getEventBus().deliver();
            Test.stopTest();
        }catch (Exception ex) { 
            Assert.isTrue(String.isNotBlank(ex.getMessage()), 'Error is thrown due to invalid JOSN');
        }
    }
}