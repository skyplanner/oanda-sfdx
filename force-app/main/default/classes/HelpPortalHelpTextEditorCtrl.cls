/**
 * Controller for component: HelpPortalManagement
 * @author Fernando Gomez, Skyplanner LLC
 * @since 4/30/2019
 */
global  class HelpPortalHelpTextEditorCtrl {
	
	
	/**
	 * Main constructor
	 */
	global HelpPortalHelpTextEditorCtrl() {
		// TODO: ...
	}
    
    @AuraEnabled
    global static List<Division_Settings__mdt> getDivision(){
        try{
        return DivisionMetadataUtil.getDivisionVisible();
        }catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
    }
}