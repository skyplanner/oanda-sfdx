/**
 * @File Name          : SupervisorProviderFactory.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 3:48:00 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/29/2024, 3:48:00 PM   aniubo     Initial Version
**/
public with sharing class SupervisorProviderFactory implements SupervisorProviderCreator {
	public SupervisorProvider createSupervisorProvider(
		SLAConst.AgentSupervisorModel supervisorModel
	) {
		if (
			(supervisorModel != SLAConst.AgentSupervisorModel.DIVISION) &&
			(supervisorModel != SLAConst.AgentSupervisorModel.ROLE)
		) {
			throw new SupervisorProviderFactoryException(
				'supervisorModel has invalid value: ' + supervisorModel
			);
		}
		SupervisorProvider result = null;
		if (supervisorModel == SLAConst.AgentSupervisorModel.DIVISION) {
			result = new SupervisorByDivisions();
		} else if (supervisorModel == SLAConst.AgentSupervisorModel.ROLE) {
			result = new SupervisorByRoles();
		}
		return result;
	}

	public class SupervisorProviderFactoryException extends Exception {
	}
}