@isTest
public with sharing class FxAccountSelectorTest {

    public static final Integer VALID_USER_ID = 12345;
    public static final Integer INVALID_USER_ID = 00000;

    @TestSetup
    static void initData(){
 
         User testuser1 = UserUtil.getRandomTestUser();    
         TestDataFactory testHandler = new TestDataFactory();
         Account account = testHandler.createTestAccount();
         Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
         insert contact;
 
         fxAccount__c fxAccount = new fxAccount__c();
         fxAccount.Account__c = account.Id;
         fxAccount.Contact__c = contact.Id;
         fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
         fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
         fxAccount.Division_Name__c = 'OANDA Europe';
         fxAccount.Citizenship_Nationality__c = 'Canada';
         fxAccount.OwnerId = testuser1.Id; 
         fxAccount.fxTrade_User_ID__c = VALID_USER_ID;
         fxAccount.fxTrade_Global_ID__c = VALID_USER_ID + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
         fxAccount.Net_Worth_Value__c = 10000;
         fxAccount.Liquid_Net_Worth_Value__c = 5000;
         fxAccount.US_Shares_Trading_Enabled__c = false;
         fxAccount.Email__c = account.PersonEmail;
         fxAccount.Name = 'testusername';
         insert fxAccount;
    }

    @isTest
    public static void getFxAccountByGlobalId(){

        Test.startTest();
        fxAccount__c validAccount = FXAccountSelector.getLiveFxAccountByUserId(VALID_USER_ID);
        fxAccount__c invalidAccount = FXAccountSelector.getLiveFxAccountByUserId(INVALID_USER_ID);
        Test.stopTest();

        Assert.isNotNull(validAccount, 'FxAccount should not be null');
        Assert.isNull(invalidAccount, 'FxAccount should be null');
    }
}