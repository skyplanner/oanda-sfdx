/**
 * Manager class to search in SF
 */
public class UserApiSfSearchManager {
    UserApiAppSettings stts;
    UserApiSearch criteria;
    Integer limitSize;
    Integer offset;

    /**
     * Constructor
     */
    public UserApiSfSearchManager(
        UserApiSearch criteria,
        Integer limitSize,
        Integer offset
    ) {
        this.criteria = criteria;
        this.limitSize = limitSize;
        this.offset = offset;
        stts = UserApiAppSettings.getInstance();
    }

    /**
     * Search fxAccounts
     */
    public List<fxAccount__c> search() {
        String qWhere = getQueryCriteria();

        if (String.isBlank(qWhere)) {
            throw LogException.newInstance(
                'To search in Salesforce you must specify criteria.',
                Constants.LOGGER_USER_API_CATEGORY);
        }

        if (stts.sfQueryFields.isEmpty())
            throw LogException.newInstance(
                'There are not query fields configured.',
                Constants.LOGGER_USER_API_CATEGORY);

        return Database.query(
            ' SELECT Id, ' + 
                String.join(stts.sfQueryFields, ',') +
            ' FROM fxAccount__c ' +
            ' WHERE ' + getQueryCriteria() +
            ' LIMIT ' + limitSize +
            ' OFFSET ' + offset);
    }

    /**
     * Build query criteria
     */
    private String getQueryCriteria() {
        List<String> wArr = new List<String>();
        
        if (String.isNotBlank(criteria.email)) {
            wArr.add(' Email__c LIKE \'%' + criteria.email + '%\' ');
        }

        if (String.isNotBlank(criteria.accName)) {
            wArr.add(' Account__r.Name LIKE \'%' + criteria.accName + '%\' ');
        }

        if (String.isNotBlank(criteria.userId)) {
            wArr.add(' fxTrade_User_Id_Text__c LIKE \'%' + criteria.userId + '%\' ');
        }
        
        return String.join(wArr, 'AND');
    }
}