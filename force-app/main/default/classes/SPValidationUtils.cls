/**
 * @File Name          : SPValidationUtils.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/11/2020, 1:25:55 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   acantero     Initial Version
**/
public class SPValidationUtils {

    public static Boolean checkFlexibleFormat(
        String someValue,
        Integer lettersCount, 
        Integer digitsCount
    ) {
        if (
            (lettersCount == null) ||
            (digitsCount == null)
        ) {
            return null;
        }
        //else...
        if (
            String.isBlank(someValue) ||
            (someValue.length() != (lettersCount + digitsCount))
        ) {
            return false;
        }
        //else...
        Integer lettersCounter = 0;
        Integer digitsCounter = 0;
        for(Integer i  = 0; i < someValue.length(); i++) {
            String currentChar = someValue.substring(i, i + 1);
            if (currentChar.isNumeric()) {
                digitsCounter++;
                if (digitsCounter > digitsCount) {
                    return false;
                }
            } else if (currentChar.isAlpha()) {
                lettersCounter++;
                if (lettersCounter > lettersCount) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }
    
    public static Boolean verifyLuhnCheckDigit(String someValue) {
        CheckDigitInfo info = new CheckDigitInfo(someValue);
        if (info.isValid != true) {
            return null;
        }
        //else...
        Integer checkDigitCalc = calculateLuhnCheckDigit(info.otherDigitsStr);
        Boolean result = (info.checkDigit == checkDigitCalc );
        return result;
    }

    public static Integer calculateLuhnCheckDigit(String someValue) {
        if (
            String.isBlank(someValue) ||
            (!someValue.isNumeric())
        ) {
            return null;
        }
        //else...
        if(someValue.length()==11) someValue=someValue.substring(2, 11);
        Integer total = 0;
        Integer length = someValue.length();
        Boolean doubleValue = true;
        for(Integer i = length - 1; i >= 0; i--) {
            String currentChar = someValue.substring(i, i + 1);
            Integer num = Integer.ValueOf(currentChar);
            if (doubleValue) {
                num = num * 2;
                if (num > 9) {
                    num = num - 9;
                }
            }
            total += num;
            doubleValue = !doubleValue;
        }
        //else...
        Integer result = Math.mod((total * 9), 10);
        return result;
    }

    public static Boolean verifyModCheckDigit(
        String someValue, 
        Long modNumber, 
        Boolean useOnlyLastDigit
    ) {
        CheckDigitInfo info = new CheckDigitInfo(someValue);
        if (info.isValid != true) {
            return null;
        }
        //else...
        Long checkDigitCalc = calculateMod(info.otherDigitsStr, modNumber, useOnlyLastDigit);
        Boolean result = (info.checkDigit == checkDigitCalc );
        return result;
    }

    public static Long calculateMod(
        String someValue, 
        Long modNumber, 
        Boolean returnLastDigit
    ) {
        if (
            String.isEmpty(someValue) ||
            (!someValue.isNumeric()) ||
            (modNumber == null) ||
            (modNumber == 0)
        ) {
            return null;
        }
        //else...
        Long longVal = Long.valueOf(someValue);
        Long result = Math.mod(longVal, modNumber);
        if (
            (result > 9) && 
            (returnLastDigit == true)
        ) {
            result = 
                Long.valueOf(String.valueOf(result).right(1));
        }
        return result;
    }

    public class CheckDigitInfo {

        public final Boolean isValid;
        public final Integer checkDigit;
        public final String otherDigitsStr;

        public CheckDigitInfo(String someValue) {
            if (
                String.isBlank(someValue) ||
                (someValue.length() == 1) ||
                (!someValue.isNumeric())
            ) {
                isValid = false;
                return;
            }
            //else...
            Integer lastCharIndex = someValue.length() - 1;
            checkDigit = Integer.valueOf(someValue.substring(lastCharIndex));
            otherDigitsStr = someValue.substring(0, lastCharIndex);
            isValid = true;
        }

    }

}