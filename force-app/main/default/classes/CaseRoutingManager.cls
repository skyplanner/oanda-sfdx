/**
 * @File Name          : CaseRoutingManager.cls
 * @Description        : 
 * Allows cases to be routed through Omnichannel
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 4:09:24 AM
**/
public without sharing class CaseRoutingManager {

	static CaseRoutingManager instance;

	Set<ID> originalCasesQueueIdSet;
	ID skillCasesQueueId;
	ID skillNonHvcCasesQueueId;
	ID caseServiceChanelId;
	ID nonHvcCaseServiceChanelId;
	QueueRoutingConfig emailCasesRoutingConfig;
	QueueRoutingConfig nonHvcEmailCasesRoutingConfig;

	private CaseRoutingManager() {
	}

	public static CaseRoutingManager getInstance() {
		if (instance == null) {
			instance = new CaseRoutingManager();
		}
		return instance;
	}

	public Boolean routeUsingSkills(List<Case> caseList) {
		if (
			(caseList == null) || 
			caseList.isEmpty()
		) {
			return false;
		}
		//else...
		Map<String,Boolean> caseIdHvcMap = new Map<String,Boolean>();
		List<Case> toBeRoutedCaseList = new List<Case>();

		for (Case caseObj : caseList) {
			toBeRoutedCaseList.add(
				new Case(Id = caseObj.Id)
			);
			caseIdHvcMap.put(
				caseObj.Id, 
				caseObj.Is_HVC__c
			);
		}
		return routeUsingSkills(
			new CaseRoutingRequest(
				toBeRoutedCaseList, // caseList
				caseIdHvcMap, // caseIdHvcMap
				false, // routeOnlyHvc
				false // markAsRoutedAndUpdate
			)
		);
	}

	public void routeNonHvcCasesFromIds(Set<ID> caseIdSet) {
		List<Case> caseList = new List<Case>();
		for(ID caseId : caseIdSet) {
			caseList.add(
				new Case(Id = caseId)
			);
		}
		routeUsingSkills(
			new CaseRoutingRequest(
				caseList, // caseList
				null, // caseIdHvcMap
				false, // routeOnlyHvc
				false // markAsRoutedAndUpdate
			)
		);
	}

	public Boolean updateLanguageAndRoute(
		Set<ID> caseIdSet
	) {
		if (
			(caseIdSet == null) ||
			caseIdSet.isEmpty()
		) {
			return false;
		}
		// else...
		List<Case> caseList = CaseHelper.getRoutingInfoById(caseIdSet);
		Map<String,Boolean> caseIdHvcMap = new Map<String,Boolean>();
		String defaultLanguage = 
			ServiceLanguagesManager.getLanguage(ChatConst.DEFAULT_LANGUAGE);

		for (Case caseObj : caseList) {
			caseIdHvcMap.put(caseObj.Id, caseObj.Is_HVC__c);
			updateLanguageCorrected(
				caseObj,
				defaultLanguage
			);
		}
		
		return routeUsingSkills(
			new CaseRoutingRequest(
				caseList, // caseList
				caseIdHvcMap, // caseIdHvcMap
				false, // routeOnlyHvc
				true // markAsRoutedAndUpdate
			)
		);
	}

	public Boolean routeUsingSkills(CaseRoutingRequest req) {
		if (
			(req == null) ||
			(req.caseList == null) ||
			req.caseList.isEmpty()
		) {
			return false;
		}
		//else...
		List<Case> routedCaseList = new List<Case>();
		List<Non_HVC_Case__c> nonHvcCaseList = new List<Non_HVC_Case__c>();
		List<PendingServiceRouting> pendingRoutList = new List<PendingServiceRouting>();
		ID skillsNonHvcCasesQueueId = getSkillNonHvcCasesQueueId();

		for (Case caseObj : req.caseList) {
			// Boolean isHvc = (
			//     (req.caseIdHvcMap != null) &&
			//     (req.caseIdHvcMap.get(caseObj.Id) == true)
			// );
			// if (isHvc == true) {
			//     //is HVC
			//     pendingRoutList.add(
			//         createPSRforHvcCase(caseObj.Id)
			//     );
			//     routedCaseList.add(caseObj);
			//     //...
			// } else 
			// if (req.routeOnlyHvc != true) {
			//      //is Not HVC
			//     nonHvcCaseList.add( 
			//         new Non_HVC_Case__c(
			//             Case__c = caseObj.Id,
			//             OwnerId = skillsNonHvcCasesQueueId
			//         )
			//     );
			//     routedCaseList.add(caseObj);
			// }
			nonHvcCaseList.add( 
				new Non_HVC_Case__c(
					Case__c = caseObj.Id,
					OwnerId = skillsNonHvcCasesQueueId
				)
			);
			routedCaseList.add(caseObj);
		}

		if (!nonHvcCaseList.isEmpty()) {
			insert nonHvcCaseList;
			
			for (Non_HVC_Case__c nonHvcCase : nonHvcCaseList) {
				pendingRoutList.add(
					createPSRforNonHvcCase(nonHvcCase.Id)
				);
			}
		}
		//...
		//mark cases as routed and update them
		if (req.markAsRoutedAndUpdate == true) {
			updateRoutedCases(routedCaseList);
		}
		
		if (!pendingRoutList.isEmpty()) {
			insert pendingRoutList;

			for (PendingServiceRouting pendingRoutObj : pendingRoutList) {
				pendingRoutObj.IsReadyForRouting = true;
			}
			
			update pendingRoutList;
		}
		return true;
	}
	

	@testVisible
	void updateRoutedCases(
		List<Case> routedCaseList
	) {
		if (
			(routedCaseList != null) &&
			(!routedCaseList.isEmpty())
		) {
			for(Case caseObj : routedCaseList) {
				caseObj.Routed__c = true;
			}
			//disable SP trigger 
			//actually it is not necessary to disable it, is done only to optimize
			//SPCaseTriggerHandler.enabled = false;
			update routedCaseList;
			//enable trigger again
			//SPCaseTriggerHandler.enabled = true;
		}
	}

	@TestVisible
	Boolean updateLanguageCorrected(
		Case caseObj,
		String defaultLanguage
	) {
		if (caseObj == null) {
			return false;
		}
		//else...
		String languageCorrected = defaultLanguage;
		if (String.isNotBlank(caseObj.Language)) {
			String langCorrectedByFamily = 
				ServiceLanguagesManager.getLanguageCorrectedByFamily(
					caseObj.Language
				);
			languageCorrected = CaseHelper.getValidLanguageCorrected(
				langCorrectedByFamily, // languageCorrected
				defaultLanguage // defaultLanguage
			);
			caseObj.Predicted_Language__c = 
				ServiceLanguagesManager.getLanguageByFamily(caseObj.Language);
		}
		caseObj.Language_Corrected__c = languageCorrected;
		return true;
	}

	public PendingServiceRouting createPSRforHvcCase(ID caseId) {
		return createPSR(
			getEmailCasesRoutingConfig(),
			getCaseServiceChanelId(),
			caseId
		);
	}

	public PendingServiceRouting createPSRforNonHvcCase(ID nonHvcCaseId) {
		return createPSR(
			getNonEmailCasesRoutingConfig(),
			getNonHvcCaseServiceChanelId(),
			nonHvcCaseId
		);
	}

	public PendingServiceRouting createPSR(
		QueueRoutingConfig routingConfig,
		ID channelId,
		ID workItemId
	) {
		PendingServiceRouting result = new PendingServiceRouting(
			CapacityWeight = routingConfig.CapacityWeight,
			IsReadyForRouting = false,
			RoutingModel  = routingConfig.RoutingModel,
			RoutingType = OmnichanelConst.SKILLS_BASED_ROUTING_TYPE,
			ServiceChannelId = channelId,
			WorkItemId = workItemId
		);
		return result;
	}

	@testVisible
	QueueRoutingConfig getEmailCasesRoutingConfig() {
		if (emailCasesRoutingConfig == null) {
			emailCasesRoutingConfig = 
				QueueRoutingConfigHelper.getByDevName(
					OmnichanelConst.EMAIL_CASES_ROUTING_CONFIG,
					true
				);
		}
		return emailCasesRoutingConfig;
	}

	@testVisible
	QueueRoutingConfig getNonEmailCasesRoutingConfig() {
		if (nonHvcEmailCasesRoutingConfig == null) {
			nonHvcEmailCasesRoutingConfig = 
				QueueRoutingConfigHelper.getByDevName(
					OmnichanelConst.NON_HVC_EMAIL_CASES_ROUTING_CONFIG,
					true
				);
		}
		return nonHvcEmailCasesRoutingConfig;
	}

	@testVisible
	ID getCaseServiceChanelId() {
		if (caseServiceChanelId == null) {
			caseServiceChanelId = 
				ServiceChannelHelper.getByDevName(
					OmnichanelConst.CASE_SERVICE_CHANNEL,
					true
				).Id;
		}
		return caseServiceChanelId;
	}

	@testVisible
	ID getNonHvcCaseServiceChanelId() {
		if (nonHvcCaseServiceChanelId == null) {
			nonHvcCaseServiceChanelId = 
				ServiceChannelHelper.getByDevName(
					OmnichanelConst.NON_HVC_CASE_SERVICE_CHANNEL,
					true
				).Id;
		}
		return nonHvcCaseServiceChanelId;
	}

	public ID getSkillCasesQueueId() {
		if (skillCasesQueueId == null) {
			skillCasesQueueId = getRequiredSkillsQueueId();
		}
		return skillCasesQueueId;
	}

	@testVisible
	ID getSkillNonHvcCasesQueueId() {
		if (skillNonHvcCasesQueueId == null) {
			skillNonHvcCasesQueueId = 
				getRequiredSkillsNonHvcCasesQueueId();
		}
		return skillNonHvcCasesQueueId;
	}

	public static Set<ID> getOriginalCaseQueueIdSet() {
		OmnichanelSettings omniSettings = 
			OmnichanelSettings.getRequiredDefault();
		return SPQueueUtil.getQueueIdSetByDevNames(
			omniSettings.casesQueueDevNameList
		);
	}

	@testVisible
	public static ID getRequiredSkillsQueueId() {
		String result = OmnichanelUtil.getRequiredQueueId(
			OmnichanelConst.SKILL_CASES_QUEUE_DEVNAME,
			'Omnichanel "Skill Cases" queue not found'
		);
		return result;
	}

	@testVisible
	public static ID getRequiredSkillsNonHvcCasesQueueId() {
		String result = OmnichanelUtil.getRequiredQueueId(
			OmnichanelConst.SKILL_NON_HVC_CASES_QUEUE_DEVNAME,
			'Omnichanel "Skill Non-HVC Cases" queue not found'
		);
		return result;
	}

	// ************************************************************************

	public class CaseRoutingRequest {

		public List<Case> caseList {get; set;}
		public Map<String,Boolean> caseIdHvcMap {get; set;}
		public Boolean routeOnlyHvc {get; set;}
		public Boolean markAsRoutedAndUpdate {get; set;}

		public CaseRoutingRequest() {
		}

		public CaseRoutingRequest(
			List<Case> caseList,
			Map<String,Boolean> caseIdHvcMap,
			Boolean routeOnlyHvc,
			Boolean markAsRoutedAndUpdate
		) {
			this.caseList = caseList;
			this.caseIdHvcMap = caseIdHvcMap;
			this.routeOnlyHvc = routeOnlyHvc;
			this.markAsRoutedAndUpdate = markAsRoutedAndUpdate;
		}

	}

}