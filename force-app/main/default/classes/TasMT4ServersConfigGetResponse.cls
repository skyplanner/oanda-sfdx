/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-14-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasMT4ServersConfigGetResponse {
    public List<TasMT4Server> mt4_servers {get; set;}
}