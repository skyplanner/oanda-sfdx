/**
 * @File Name          : ServiceResourceHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/20/2021, 1:37:55 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/25/2021, 10:56:27 AM   acantero     Initial Version
**/
public without sharing class ServiceResourceHelper {

    public static Set<String> getAgentsIdSet(
        Set<String> serviceResourceIdSet
    ) {
        Set<String> result = new Set<String>();
        if (
            (serviceResourceIdSet == null) ||
            serviceResourceIdSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<ServiceResource> agentList = [
            SELECT 
                Id
            FROM 
                ServiceResource
            WHERE 
                Id IN :serviceResourceIdSet
            AND
                ResourceType = :OmnichanelConst.SERVICE_RES_AGENT
        ];
        for(ServiceResource agent : agentList) {
            result.add(agent.Id);
        }
        return result;
    }

    public static Set<String> getAgentsUserIdSet(
        Set<String> userIdSet
    ) {
        Set<String> result = new Set<String>();
        if (
            (userIdSet == null) ||
            userIdSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<ServiceResource> agentList = [
            SELECT 
                RelatedRecordId
            FROM 
                ServiceResource
            WHERE 
                RelatedRecordId IN :userIdSet
            AND
                ResourceType = :OmnichanelConst.SERVICE_RES_AGENT
        ];
        for(ServiceResource agent : agentList) {
            result.add(agent.RelatedRecordId);
        }
        return result;
    }
    
}