/**
 * @File Name          : MessagingAccountProcessTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 4:45:34 AM
**/
@IsTest
private without sharing class MessagingAccountProcessTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createLead1(initManager);
		initManager.storeData();
	}

	// test 1 : userEmail is null => isLinkedToAccountOrLead = false
	// test 2 : session is null => isLinkedToAccountOrLead = false
	@IsTest
	static void checkLinkToAccountOrLead1() {
		Test.startTest();
		// test 1
		MessagingAccountProcess instance1 = 
			new MessagingAccountProcess(new MessagingSession());
		MessagingAccountCheckInfo result1 = 
			instance1.checkLinkToAccountOrLead(null);
		// test 2
		MessagingAccountProcess instance2 = new MessagingAccountProcess(null);
		MessagingAccountCheckInfo result2 = instance2.checkLinkToAccountOrLead(
			ServiceTestDataKeys.ACCOUNT1_EMAIL
		);
		Test.stopTest();
		
		Assert.isFalse(result1.isLinkedToAccountOrLead, 'Invalid result');
		Assert.isFalse(result2.isLinkedToAccountOrLead, 'Invalid result');
	}

	// userEmail is linked to an account => isLinkedToAccountOrLead = true
	@IsTest
	static void checkLinkToAccountOrLead2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = 
			initManager.getObjectId(ServiceTestDataKeys.FX_ACCOUNT_1, true);

		MessagingAccountProcess instance = 
			new MessagingAccountProcess(new MessagingSession());
		
		Test.startTest();
		MessagingAccountCheckInfo result = instance.checkLinkToAccountOrLead(
			ServiceTestDataKeys.ACCOUNT1_EMAIL
		);
		Test.stopTest();
		
		Assert.isTrue(result.isLinkedToAccountOrLead, 'Invalid result');
		Assert.areEqual(fxAccount1Id, result.fxAccountId, 'Invalid result');
	}

	// userEmail is linked to a lead => isLinkedToAccountOrLead = true
	@IsTest
	static void checkLinkToAccountOrLead3() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		MessagingAccountProcess instance = 
			new MessagingAccountProcess(new MessagingSession());
		
		Test.startTest();
		MessagingAccountCheckInfo result = instance.checkLinkToAccountOrLead(
			ServiceTestDataKeys.LEAD1_EMAIL
		);
		Test.stopTest();
		
		Assert.isTrue(result.isLinkedToAccountOrLead, 'Invalid result');
		Assert.isNull(result.fxAccountId, 'Invalid result');
	}

	// test 1 : username is null => isLinkedToAccountOrLead = false
	// test 2 : session is null => isLinkedToAccountOrLead = false
	@IsTest
	static void checkLinkToFxAccount1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = 
			initManager.getObjectId(ServiceTestDataKeys.FX_ACCOUNT_1, true);
		fxAccount__c fxAccountObj = 
			ServiceTestDataHelper.getFxAccount(fxAccount1Id);

		Test.startTest();
		// test 1
		MessagingAccountProcess instance1 = 
			new MessagingAccountProcess(new MessagingSession());
		MessagingAccountCheckInfo result1 = 
			instance1.checkLinkToFxAccount(null);
		// test 2
		MessagingAccountProcess instance2 = new MessagingAccountProcess(null);
		MessagingAccountCheckInfo result2 = 
			instance2.checkLinkToFxAccount(fxAccountObj.Name);
		Test.stopTest();
		
		Assert.isFalse(result1.isLinkedToAccountOrLead, 'Invalid result');
		Assert.isFalse(result2.isLinkedToAccountOrLead, 'Invalid result');
	}

	// username is valid => isLinkedToAccountOrLead = true
	@IsTest
	static void checkLinkToFxAccount2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = 
			initManager.getObjectId(ServiceTestDataKeys.FX_ACCOUNT_1, true);
		fxAccount__c fxAccountObj = 
			ServiceTestDataHelper.getFxAccount(fxAccount1Id);

		Test.startTest();
		MessagingAccountProcess instance = 
			new MessagingAccountProcess(new MessagingSession());
		MessagingAccountCheckInfo result = 
			instance.checkLinkToFxAccount(fxAccountObj.Name);
		Test.stopTest();
		
		Assert.isTrue(result.isLinkedToAccountOrLead, 'Invalid result');
		Assert.areEqual(fxAccount1Id, result.fxAccountId, 'Invalid result');
	}

	// username is not valid => isLinkedToAccountOrLead = false
	@IsTest
	static void checkLinkToFxAccount3() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = 
			initManager.getObjectId(ServiceTestDataKeys.FX_ACCOUNT_1, true);
		fxAccount__c fxAccountObj = 
			ServiceTestDataHelper.getFxAccount(fxAccount1Id);

		Test.startTest();
		MessagingAccountProcess instance = 
			new MessagingAccountProcess(new MessagingSession());
		MessagingAccountCheckInfo result = 
			instance.checkLinkToFxAccount(fxAccountObj.Name + 'abcd');
		Test.stopTest();
		
		Assert.isFalse(result.isLinkedToAccountOrLead, 'Invalid result');
		Assert.isNull(result.fxAccountId, 'Invalid result');
	}
	
}