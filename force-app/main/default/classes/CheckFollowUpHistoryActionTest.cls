/**
 * @File Name          : CheckFollowUpHistoryActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 10:31:00 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 10:13:58 AM   aniubo     Initial Version
 **/
@isTest
private class CheckFollowUpHistoryActionTest {
	@isTest
	private static void testInfoListIsNull() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<CheckFollowUpHistoryAction.ActionResult> results = CheckFollowUpHistoryAction.checkFollowUpHistory(
			null
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(true, results != null, 'Results should not be null');
		Assert.areEqual(1, results.size(), 'Results should have 1 item');
		Assert.areEqual(
			true,
			String.isBlank(results[0].followUpHistory),
			'Follow up history should be blank'
		);
		Assert.areEqual(
			0,
			results[0].dialogFollowUpCounter,
			'Dialog follow up counter should be 0'
		);
	}

	@isTest
	private static void testFollowUpHistoryEmpty() {
		// Test data setup
		CheckFollowUpHistoryAction.ActionInfo info = createInfo(null, 'key');
		// Actual test
		Test.startTest();
		List<CheckFollowUpHistoryAction.ActionResult> results = CheckFollowUpHistoryAction.checkFollowUpHistory(
			new List<CheckFollowUpHistoryAction.ActionInfo>{ info }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(true, results != null, 'Results should not be null');
		Assert.areEqual(1, results.size(), 'Results should have 1 item');
		Map<String, Integer> expectedFollowUpHistory = new Map<String, Integer>{
			'key' => 1
		};
		Assert.areEqual(
			JSON.serialize(expectedFollowUpHistory),
			results[0].followUpHistory,
			'Follow up history must not be blank'
		);
		Assert.areEqual(
			1,
			results[0].dialogFollowUpCounter,
			'Dialog follow up counter should be 1'
		);
	}

	@isTest
	private static void testFollowUpHistory() {
		// Test data setup
		Map<String, Integer> expectedFollowUpHistory = new Map<String, Integer>{
			'key' => 1
		};

		CheckFollowUpHistoryAction.ActionInfo info = createInfo(
			JSON.serialize(expectedFollowUpHistory),
			'key'
		);
		// Actual test
		Test.startTest();
		List<CheckFollowUpHistoryAction.ActionResult> results = CheckFollowUpHistoryAction.checkFollowUpHistory(
			new List<CheckFollowUpHistoryAction.ActionInfo>{ info }
		);
		Test.stopTest();
		expectedFollowUpHistory.put('key', 2);
		// Asserts
		Assert.areEqual(true, results != null, 'Results should not be null');
		Assert.areEqual(1, results.size(), 'Results should have 1 item');
		Assert.areEqual(
			JSON.serialize(expectedFollowUpHistory),
			results[0].followUpHistory,
			'Follow up history must not be blank'
		);
		Assert.areEqual(
			2,
			results[0].dialogFollowUpCounter,
			'Dialog follow up counter should be 2'
		);
	}

	@isTest
	private static void testCheckFollowUpHistoryThrowError() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false; // Actual test
		Test.startTest();
		try {
			List<CheckFollowUpHistoryAction.ActionResult> results = CheckFollowUpHistoryAction.checkFollowUpHistory(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should be thrown');
	}

	private static CheckFollowUpHistoryAction.ActionInfo createInfo(
		String followUpHistory,
		String dialogFollowUpKey
	) {
		CheckFollowUpHistoryAction.ActionInfo info = new CheckFollowUpHistoryAction.ActionInfo();
		info.followUpHistory = followUpHistory;
		info.dialogFollowUpKey = dialogFollowUpKey;
		return info;
	}
}