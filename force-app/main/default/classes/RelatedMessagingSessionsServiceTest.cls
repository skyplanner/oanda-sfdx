/**
 * @File Name          : RelatedMessagingSessionsServiceTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 1:44:10 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/14/2024, 1:36:45 PM   aniubo     Initial Version
**/
@isTest
private class RelatedMessagingSessionsServiceTest {
	@isTest
	private static void testGetCount() {
		// Test data setup

		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsService service = getRelatedMessagingSessionsService(
			true
		);
		Integer count = service.getCount();
		Test.stopTest();
		System.assertEquals(1, count, 'Count should be 1');

		// Asserts
	}

	@isTest
	private static void testGetRelatedMessagingSessions() {
		// Test data setup

		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsService service = getRelatedMessagingSessionsService(
			false
		);
		List<MessagingSession> sessions = service.getRelatedMessagingSessions(new List<String>(), 0, 0);
		Test.stopTest();
		System.assertEquals(true, sessions.isEmpty(), 'Sessions should be empty');

		// Asserts
	}

	private static IRelatedMessagingSessionsService getRelatedMessagingSessionsService(
		Boolean isAccount
	) {
		return new RelatedMessagingSessionsService(
			userInfo.getUserId(),
			new RelatedMessagingSessionsReaderMock(isAccount)
		);
	}
}