/**
 * @File Name          : CloseChatCaseCtrl.cls
 * @Description        : ClosCloseChatCase component controller
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/7/2019, 4:53:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/7/2019, 4:52:07 PM   acantero     Initial Version
**/
public with sharing class CloseChatCaseCtrl {
    
    @AuraEnabled
    public static String getCaseIdFromChatTranscript(String chatTranscriptId) {
        if (String.isBlank(chatTranscriptId)) {
            return null;
        }
        //else...
        List<LiveChatTranscript> result = [
            select
                CaseId
            from
                LiveChatTranscript
            where 
                Id = :chatTranscriptId 
            limit 
                1
        ];
        if (result.isEmpty()) {
            return null;
        }
        // else...
        return result[0].CaseId;
    }

}