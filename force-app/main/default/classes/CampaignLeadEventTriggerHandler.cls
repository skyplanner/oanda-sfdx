public with sharing class CampaignLeadEventTriggerHandler {
    
    Map<String,Map<String,Endpoints_Mappings__mdt>> endpointMappings;
    private static String ENDPOINT_NAME = 'CampaignLeadApi';
    private final static String LOGGER_CATEGORY = 'POST-CampaignLead';
    private final static String LOGGER_CATEGORY_URL = '@RestResource(/api/v1/campaignLeads/)';

    /* SAMPLE CODE
    String payload = '{"first_name":"test_name","last_name":"test surname","email":
    "test3@test2.pl","phone":"+48512345678","country":"Canada","campaignName":"OGM Platform"
    ,"leadSource":"OGM Platform"}';


        CampaignLeadEventTriggerHandler clhandler  = new CampaignLeadEventTriggerHandler();
    clhandler.handleCamapignLeadInsert(new List<String>{payload});



    */

    public CampaignLeadEventTriggerHandler() {
        // endpointMappings = 

    }

    public void handleCamapignLeadInsert(List<String> CLInputstrings) {
        try {
            EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
            endpointMappings = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME);
            Map<String, Endpoints_Mappings__mdt> currentObjMapping = endpointMappings.get('Campaign_Lead__c');
            List<Campaign_Lead__c> campaignLeadsToInsert = new List<Campaign_Lead__c>();

            for (String jsonBody : CLInputstrings) {
                Map<String, Object> input = (Map<String, Object>)JSON.deserializeUntyped(jsonBody);
                Logger.error(ENDPOINT_NAME, 'input: '+ input, ' input ' + CLInputstrings);

                Campaign_Lead__c cl = new Campaign_Lead__c();
                for(String inputField : input.keySet()) {
                    setInputFieldtoSobject(cl, 
                                        inputField, 
                                        currentObjMapping.get(inputField)?.Sobject_Field_Name__c , 
                                        currentObjMapping.get(inputField)?.Data_Type__c, 
                                        input.get(inputField));
                }
                campaignLeadsToInsert.add(cl);
            }
            List<Database.SaveResult> sr = Database.insert(campaignLeadsToInsert, false);
        } catch (Exception ex) { 
            Logger.error(ENDPOINT_NAME, 'Error processing: '+ ENDPOINT_NAME, ' input ' + CLInputstrings);
                return;
        }
    }

    private void setInputFieldtoSobject(SObject currentSobject, String inputField, String fieldName, String dataType, Object fieldValue){

        try {
            switch on dataType {
                /*
                when 'Date' {
                    List <String> dateSpilted = String.valueOf(fieldValue).split('-');
                    if (Integer.valueOf(dateSpilted[0]) > 1900) {
                        currentSobject.put(fieldName, Date.newInstance(Integer.valueOf(dateSpilted[0]), Integer.valueOf(dateSpilted[1]), Integer.valueOf(dateSpilted[2])));
                    }
                }
                when 'Datetime' {
                    Datetime  currentDT = fieldValue instanceof Long ? Datetime.valueOf(fieldValue) : Datetime.valueOf(String.valueOf(fieldValue).replaceAll('T',' '));
                    currentSobject.put(fieldName, currentDT);
                }
                when 'Decimal' {
                    currentSobject.put(fieldName, Decimal.valueOf(String.valueOf(fieldValue)));
                }
                when 'Integer' {
                    currentSobject.put(fieldName, Integer.valueOf(fieldValue));
                }
                */
                when 'String' {
                    currentSobject.put(fieldName, String.valueOf(fieldValue));
                }
                when 'DIVISION_NAME_MAPPING' {
                    String tempDivision = Constants.DIVISION_NAME_MAPPING.containsKey((String)fieldValue) 
                                            ? Constants.DIVISION_NAME_MAPPING.get((String)fieldValue)
                                            : String.valueOf(fieldValue);
                    currentSobject.put(fieldName, tempDivision);
                }
                when else {
                    currentSobject.put(fieldName, fieldValue);
                }
            }
        } catch (Exception ex) {
            Logger.error(
                ENDPOINT_NAME,
                'Incorrect mapping',
                ' The following inputField/field name ' + inputField + '/' +fieldName 
                + ' field value:  ' + fieldValue + ' for object: ' + currentSobject 
                + ' was not mapped. Err msg: ' + ex.getMessage() 
                + ' Update performed by ' + UserInfo.getName());
        }
    }
}