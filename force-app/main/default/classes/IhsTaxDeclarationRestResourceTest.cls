/* Name: IhsTaxDeclarationRestResourceTest
 * Description : Test Class for ihs tax declaration endpoint
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 July 10
 */

 @isTest
 public class IhsTaxDeclarationRestResourceTest {
    public static final String VALID_USER_EMAIL = 'test@oanda.com';
    public static final String INVALID_USER_EMAIL = 'invalid@oanda.com';

    public static final Integer VALID_USER_ID = 12345;

    public static final String TREATY_COUNTRY_CODE = 'test - Treaty_Country_Code__c';
 
    public static final String REQUEST_BODY = '{\"account_review_status\":\"account_review_status 1\",\"business_name\":\"business_name 1\",\"country_of_birth\":\"country_of_birth 1\",\"crss_status\":1000,\"entity_type\":\"test\",\"fatca_status\":\"fatca_status 1\",\"first_name\":\"first_name 1\",\"form_validation_result_code\":\"3000\",\"ftin\":\"ftin 1\",\"ftin_country\":\"ftin_country 1\",\"giin\":\"giin 1\",\"giin_status\":\"giin_status 1\",\"is_in_queue\":5000,\"last_name\":\"last_name1 \",\"middle_name\":\"middle_name 1\",\"queue_code\":\"queue_code 1\",\"rejection_reason_code\":\"rejection_reason_code 1\",\"rejection_reason_type\":\"rejection_reason_type 1\",\"reporting_fi_jurisdiction\":\"reporting_fi_jurisdiction 1\",\"sponsoring_entity_name\":\"sponsoring_entity_name \",\"sp_rate\":6000,\"tax_address\":\"tax_address \",\"tax_address_2\":\"tax_address_2 \",\"tax_city\":\"tax_city \",\"tax_country_code\":\"tax_country_code \",\"tax_postal_code\":\"tax_postal_code \",\"tax_state\":\"tax_state \",\"tin\":\"tin \",\"tin_unavailable\":\"tin_unavailable \",\"treaty_country_code\":\"treaty_country_code \",\"validation_comments\":\"validation_comments \",\"date_of_birth\":\"2017-02-01\",\"date_validated\":\"2017-02-01\",\"formation_date\":\"2017-02-01\",\"form_expiration_date\":\"2017-02-01\",\"liquidation_or_bankruptcy_date\":\"2017-02-01\",\"name\":\"vangli1\"}';
    
    @TestSetup
    static void initData(){

        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id, Email = VALID_USER_EMAIL);
        insert contact;

        TAX_Declarations__c td = new TAX_Declarations__c();
        td.Treaty_Country_Code__c = TREATY_COUNTRY_CODE;
        insert td;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = VALID_USER_ID;
        fxAccount.fxTrade_Global_ID__c = VALID_USER_ID + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = VALID_USER_EMAIL;
        fxAccount.Account_Email__c = VALID_USER_EMAIL;
        fxAccount.Name = 'testusername';
        fxAccount.TAX_Declarations__c = td.Id;
        insert fxAccount;
    }  

    @isTest
    public static void testDoGetSuccess() {
        
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();

        RestContext.request.requestUri = '/api/v1/ihs/taxdeclaration?email='+  VALID_USER_EMAIL;
        RestContext.request.httpMethod = 'GET';
        RestContext.request.addParameter('email', VALID_USER_EMAIL);

        Test.startTest();
        IhsTaxDeclarationRestResource.doGet();
        Test.stopTest();

        Map<String, Object> response = (Map<String,Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());

        Assert.areEqual(TREATY_COUNTRY_CODE, response.get('TreatyCountryCode'), 'TreatyCountryCode invalid');
    }

    @isTest
    public static void testDoGetFailureNoEmail() {
        
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();

        RestContext.request.requestUri = '/api/v1/ihs/taxdeclaration?email=';
        RestContext.request.httpMethod = 'GET';

        Test.startTest();
        IhsTaxDeclarationRestResource.doGet();
        Test.stopTest();

        Map<String, Object> response = (Map<String,Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());

        Assert.areEqual(404, response.get('status'), 'Should return 404');
        Assert.isNotNull(response.get('message'), 'Message should be provided');
    }

    @isTest
    public static void testDoGetFailureInvalidEmail() {
        
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();

        RestContext.request.requestUri = '/api/v1/ihs/taxdeclaration?email=' + INVALID_USER_EMAIL;
        RestContext.request.httpMethod = 'GET';
        RestContext.request.addParameter('email', INVALID_USER_EMAIL);

        Test.startTest();
        IhsTaxDeclarationRestResource.doGet();
        Test.stopTest();

        Map<String, Object> response = (Map<String,Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());

        Assert.areEqual(404, response.get('status'), 'Should return 404');
        Assert.isNotNull(response.get('message'), 'Message should be provided');
    }

    @isTest
    public static void testDoPutSuccess() {
        
        RestContext.request = new RestRequest();
        RestContext.response = new RestResponse();

        RestContext.request.resourcePath = '/api/v1/ihs/taxdeclaration/*';
        RestContext.request.requestUri = '/api/v1/ihs/taxdeclaration/'+  VALID_USER_ID;
        RestContext.request.httpMethod = 'PUT';
        RestContext.request.requestBody = Blob.valueOf(REQUEST_BODY);

        FxAccount__c fxaBeforeUpdate = [SELECT Id, Tax_Declarations__c FROM FxAccount__c LIMIT 1];

        Test.startTest();
        IhsTaxDeclarationRestResource.doPut();
        Test.stopTest();

        FxAccount__c fxaAfterUpdate = [SELECT Id, Tax_Declarations__c FROM FxAccount__c LIMIT 1];
        Map<String, Object> response = (Map<String,Object>) JSON.deserializeUntyped(RestContext.response.responseBody.toString());

        Assert.areEqual(true, (Boolean) response.get('isSuccessful'), 'Response should be valid');
        Assert.areEqual(fxaAfterUpdate.Id, response.get('Id'), 'Response should be valid');
        Assert.areNotEqual(fxaAfterUpdate.Tax_Declarations__c, fxaBeforeUpdate.Tax_Declarations__c, 'Tax declaration should change');
    }
}