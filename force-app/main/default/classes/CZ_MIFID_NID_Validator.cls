/**
 * @File Name          : CZ_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/5/2020, 7:41:13 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020   acantero     Initial Version
**/
public class CZ_MIFID_NID_Validator implements ExpressionValidator {

    public static final String SEPARATOR = '/';
    public static final Integer VALIDABLE_LENGTH = 11;
    
    public String validate(String val) {
        if (
            String.isEmpty(val) ||
            (val.length() != VALIDABLE_LENGTH)
        ) {
            return null;
        }
        //else...
        val = val.replaceAll(SEPARATOR, '');
        if (
            (val.length() < 2) ||
            (!val.isNumeric())
        ) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        if (SPValidationUtils.verifyModCheckDigit(val, 11, true) != true) {
            return System.Label.MifidValPasspChecksumError;
        }
        //else...
        return null;
    }

}