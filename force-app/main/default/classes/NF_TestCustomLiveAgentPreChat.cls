/**
 * Created by mcasella on 9/11/18.
 */
 
@IsTest
private class NF_TestCustomLiveAgentPreChat {
    @IsTest
    static void testLiveAgentPreChat() {
        NF_CustomLiveAgentPreChat prechat = new NF_CustomLiveAgentPreChat();

        String chatLogId = '';

        Test.StartTest();

        nfchat__Chat_Log__c log = new nfchat__Chat_Log__c();
        log.nfchat__AI_Config_Name__c = 'test';
        log.nfchat__Session_Id__c = '123';
        insert log;
        chatLogId = log.id;
		nfchat__Chat_Log_Detail__c testLogDetail = NF_TestDataUtill.createChatLogDetail();
        testLogDetail.nfchat__Intent_Name__c ='Business.Client.Portal';
        testLogDetail.nfchat__Chat_Log__c = chatLogId;
        insert testLogDetail;
        Case c = new Case();
        c.nfchat__Chat_Log__c = chatLogId;
        insert c;

        String jsonStr = '{ "SessionId": "'+log.nfchat__Session_Id__c+'", "prechatDetails": [] }';
        nfchat.LiveAgentObject laObject = (nfchat.LiveAgentObject) JSON.deserialize(jsonStr , nfchat.LiveAgentObject.class);

        prechat.addPreChatDetails(laObject);

        Test.StopTest();

        System.assertEquals(chatLogId, c.nfchat__Chat_Log__c);
    }
}