@IsTest
private class CaseUtilTest {

    @IsTest
    static void testGetOnBoardingCaseByFxAccount() {
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxa = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxa.fxTrade_User_Id__c = 1234;
        fxa.Division_Name__c = 'OANDA Canada';
        update fxa;

        List<Id> fxaIds = new List<Id>();
        fxaIds.add(fxa.Id);

        Test.startTest();
        Map<Id, Case> cases1 = CaseUtil.getOnBoardingCaseByFxAccount(fxaIds, 'Document Upload', 'Open', false);
        Map<Id, Case> cases2 = CaseUtil.getOnBoardingCaseByFxAccount(fxaIds,
                'OGM Live fxAccount created', 'Open', 'OGM Live fxAccount created', false);
        Test.stopTest();

        Case testCase = cases1.get(fxa.Id);
        System.assertEquals(fxa.fxTrade_User_Id__c, testCase.fxTrade_User_Id__c);

        testCase = cases2.get(fxa.Id);
        System.assertEquals(fxa.fxTrade_User_Id__c, testCase.fxTrade_User_Id__c);
    }

    @IsTest
    static void setFxAccsToApprovedToFund() {
        Datetime dtOld = Datetime.now()-1;
        Datetime dtNew = Datetime.now();

        TestDataFactory testHandler = new TestDataFactory();
        Account acc = testHandler.createTestAccount();
        fxAccount__c fxa = testHandler.createFXTradeAccount(acc);
        Contact cont = [SELECT Id FROM Contact WHERE AccountId = :acc.Id];

        Case case1 = new Case(
                RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                Status = 'Open',
                fxAccount__c = fxa.Id,
                AccountId = fxa.Account__c,
                ContactId = cont.Id,
                Subject = 'Subject of the case');
        insert case1;

        // Retrieve fxAccount to check that the field is empty
        fxa = [SELECT Approved_to_Fund__c FROM fxAccount__c WHERE Id  = :fxa.Id];

        System.assertEquals(null,fxa.Approved_to_Fund__c,'fxAccount.Approved_to_Fund__c has value.');

        Test.startTest();

        // Update case
        case1.Date_of_CX_Lead_Approval__c = dtOld;
        update case1;

        // Retrieve fxAccount to check if got the right value
        fxa = [SELECT Approved_to_Fund__c FROM fxAccount__c WHERE Id  = :fxa.Id];

        // Check fxAccount got the right value
        System.assertEquals(dtOld,fxa.Approved_to_Fund__c,
                'fxAccount did not get the right Approved_to_Fund__c value.');

        // Update case again for second approval
        case1.Date_of_CX_Lead_Approval__c = dtNew;
        update case1;

        // Retrieve fxAccount again
        fxa = [SELECT Approved_to_Fund__c FROM fxAccount__c WHERE Id  = :fxa.Id];

        // Check fxAccount got the right value
        // should retain the old approved tp fimd date
        System.assertEquals(dtOld,fxa.Approved_to_Fund__c,
                'fxAccount did not get the right Approved_to_Fund__c value.');
        Test.stopTest();
    }
}