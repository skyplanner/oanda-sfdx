/**
 * @File Name          : RemoveNonHvcCasesSchedulable.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/19/2024, 12:04:10 AM
**/
public without sharing class RemoveNonHvcCasesSchedulable 
	implements Schedulable {

	public void execute(SchedulableContext ctx) {
		System.debug('RemoveNonHvcCasesSchedulable -> execute');
		SPEnqueueJobProcess enqueueJobProcess = 
			new SPEnqueueJobProcess(new RemoveNonHvcCasesJob());
		Boolean success = enqueueJobProcess.safeEnqueueJob(
			RemoveNonHvcCasesJob.class.getName() // jobStringKey
		);
		checkEnqueueActionResult(success);
	}

	@TestVisible
	Boolean checkEnqueueActionResult(Boolean success) {
		System.debug(
			'RemoveNonHvcCasesSchedulable -> checkEnqueueActionResult'
		);
		Boolean result = true;
		if (success == false) {
			Logger.error(
				'Attempt to enqueue job with duplicate queueable signature', // msg
				RemoveNonHvcCasesSchedulable.class.getName() // category
			);
			result = false;
		}
		return result;
	}
	
}