/**
 * /services/apexrest/api/v1/users/test@test.com/fxaccounts/crypto/
  Sample Payload
 {
    "mt5_client_id" : 12345,
    "paxos_user_id" : 12345,
    "paxos_summary_status" : "pending",
    "one_id" : 34355345
}
*/
@RestResource(urlMapping='/api/v1/users/*/fxaccounts/crypto/')
global without sharing class CryptoFxaccountRestResource 
{
    @HttpPost
    global static void doPost()
    {
        try 
        {
            RestRequest req = RestContext.request;
            RestResponse res = Restcontext.response;

            string requestBody = req.requestBody.toString();
            Logger.info('crypto-new-account-reqBody', Constants.LOGGER_CRYPTO_CATEGORY, requestBody);


            NewCryptoAccountRequest newCryptoAccountRequest = (NewCryptoAccountRequest)JSON.deserialize(requestBody, NewCryptoAccountRequest.class);

            string urlBeforeAccounts = req.requestURI.mid(0, req.requestURI.indexOf('/fxaccounts'));
            string[] urlFragments = urlBeforeAccounts.split('/');
            string emailId = urlFragments[urlFragments.size()-1];

            system.debug('New Spot Crypto Account Request for Email : ' + emailId);
            
            Account[] accounts = [select Id,
                                        fxAccount__r.Name,
                                        (select Id From fxAccounts__r where RecordTypeId=:RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID)
                                From Account
                                Where PersonEmail= :emailId Limit 50000];

            Lead[] leads = [select Id,
                                fxAccount__r.Name,
                                (select Id From fxAccounts__r where RecordTypeId=:RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID)
                            From Lead
                            Where Email= :emailId AND IsConverted = false Limit 50000];

            if (!accounts.isEmpty()) {
                Account account = accounts[0];
                Lead lead;
                string fxAccountName = account.fxAccount__r != null ? account.fxAccount__r.Name : '';
                fxAccount__c[] cryptoFxAccounts = account.fxAccounts__r;

                if (!leads.isEmpty()) {
                    lead = leads[0];
                }

                if (cryptoFxAccounts.isEmpty()) {
                    //create new crypto fxAccount
                    fxAccount__c fxa = new fxAccount__c();
                    fxa.name = fxAccountName;
                    fxa.RecordTypeId = RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID;
                    fxa.Account__c = account.Id;
                    fxa.Lead__c = lead != null ? lead.Id : null;
                    fxa.MT5_Client_Id__c = newCryptoAccountRequest.mt5_client_id;
                    fxa.fxTrade_One_Id__c = newCryptoAccountRequest.one_id;
                    fxa.fxTrade_User_Id__c = newCryptoAccountRequest.user_id;
                    Upsert fxa MT5_Client_Id__c;

                    if (fxa.Id != null) {
                        //create new Paxos Record
                        Paxos__c paxos = new Paxos__c();
                        paxos.Paxos_Account_Number__c = newCryptoAccountRequest.paxos_user_id;
                        paxos.Paxos_Status__c = newCryptoAccountRequest.paxos_summary_status;
                        paxos.Display_Name__c = newCryptoAccountRequest.display_name;
                        paxos.Paxos_Account__c = newCryptoAccountRequest.paxos_account;
                        paxos.Paxos_Profile__c = newCryptoAccountRequest.paxos_profile;
                        paxos.One_Id_External_Id__c = newCryptoAccountRequest.one_id;
                        paxos.fxAccount__c = fxa.Id;
                        Insert paxos;

                        //Link the new paxos record to fxaccount
                        if (paxos.Id != null) {
                            fxa.Paxos_Identity__c = paxos.Id;
                            update fxa;
                        }
                        //link the crypto fxaccount to account
                        if (account != null) {
                            account.Crypto_Fxaccount__c = fxa.Id;
                            account.One_Id__C = newCryptoAccountRequest.one_id;
                            update account;
                        }
                        // else if(lead != null)
                        // {
                        //     lead.Crypto_Fxaccount__c = fxa.Id;
                        //     lead.One_Id__C = newCryptoAccountRequest.one_id;
                        //     update lead;
                        // }
                    }
                }
                else {
                    res.statusCode = 409;
                    res.responseBody = Blob.valueOf('Crypto account exists already for customer');
                }
            }
            else {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf('Client not found with the email Id : ' + emailId);
            }
        } 
        catch (Exception ex) 
        {
            Logger.error('crypto-new-account', Constants.LOGGER_CRYPTO_CATEGORY, ex.getMessage());

            RestResponse res = Restcontext.response;
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(ex.getMessage());
        }
        
    }

    public class NewCryptoAccountRequest
    {
        public string mt5_client_id;
        public string paxos_user_id;
        public string paxos_summary_status;
        public string one_id;
        public string display_name;
        public string paxos_account;
        public string paxos_profile;
        public decimal user_id;
    }
}