/**
 * @File Name          : LiveChatBySkillsSelectionCon_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/2/2022, 4:24:45 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/27/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class LiveChatBySkillsSelectionCon_Test {

    @isTest
    private static void settings_test() {
        Test.setCurrentPage(Page.LiveChatAgent);
        ApexPages.currentPage().getParameters().put('LanguagePreference', 'English');
        Test.startTest();
        LiveChatBySkillsSelectionCon controller = new LiveChatBySkillsSelectionCon();
        String apiRoot = controller.getAPIRoot();
        String jsRoot = controller.getJSRoot();
        String deployParam1 = controller.getDeployParam1();
        String deployParam2 = controller.getDeployParam2();
        //...
        Boolean offlineHours = controller.offlineHours;
        //...
        Test.stopTest();
        System.assertNotEquals(null, apiRoot, 'Invalid value');
        System.assertNotEquals(null, jsRoot, 'Invalid value');
        System.assertNotEquals(null, deployParam1, 'Invalid value');
        System.assertNotEquals(null, deployParam2, 'Invalid value');
        System.assertNotEquals(null, offlineHours, 'Invalid value');
    }

    @isTest
    private static void checkLanguageIsAvailable() {
        String languageCode = 'es';
        Test.startTest();
        Boolean result = 
            LiveChatBySkillsSelectionCon.checkLanguageIsAvailable(languageCode);
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid value');
    }

}