/**
 * Wrapper for Comply advantage JSON: Search Comment
 * @author Fernando Gomez
 */
public class ComplyAdvantageUser {
	@AuraEnabled
	public Integer id { get; set; }

	@AuraEnabled
	public String email { get; set; }

	@AuraEnabled
	public String name { get; set; }

	@AuraEnabled
	public String phone { get; set; }

	@AuraEnabled
	public String updated_at { get; set; }

	@AuraEnabled
	public String created_at { get; set; }
}