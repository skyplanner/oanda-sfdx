@isTest
private class BatchApexErrorHandlerTest {
    @isTest
	static void triggerBatchFailure() {
        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;
        Account acc = new Account(
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            PersonEmail = 'a@b.com',
            FirstName = 'Bartolo',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            LastName = 'Colon',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'test opp',
            StageName = 'test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;
        try{
            Test.startTest();
            Id newBatchJobId = Database.executeBatch(new BatchConvertLeadsScheduleable('SELECT Id FROM Lead WHERE Id=\''+lead1.Id+'\''), 200);
            Test.getEventBus().deliver();
            Test.stopTest();
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        Test.getEventBus().deliver(); 
        List<BatchErrorEvent__c> errors = [SELECT Id FROM BatchErrorEvent__c LIMIT 100];
		System.assertEquals(1, errors.size());
		System.assertEquals('ok', BatchApexErrorHandler.checkBatchStatus(errors[0].Id));
        String scheduleRerun =BatchApexErrorHandler.rerunBatch(errors[0].Id, 200);
		System.assertEquals(true, scheduleRerun.contains('Rerun successfully scheduled'));

	}

    @isTest
	static void checkSuccessfullBatch() {
        try{
            Test.startTest();
            Id newBatchJobId = Database.executeBatch(new BatchConvertLeadsScheduleable('SELECT Id FROM Lead'), 200);
            Test.getEventBus().deliver();
            Test.stopTest();
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        Test.getEventBus().deliver(); 
        List<BatchErrorEvent__c> errors = [SELECT Id, Status__c FROM BatchErrorEvent__c];
		System.assertEquals(0, errors.size());

	}
}