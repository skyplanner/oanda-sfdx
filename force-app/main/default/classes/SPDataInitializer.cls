/**
 * @File Name          : SPDataInitializer.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/11/2023, 10:13:43 PM
**/
public interface SPDataInitializer {

	String getUniqueName(DescribeSObjectResult sObjectInfo);

	String getUniqueName(String objName);

	Integer getUniqueIndex(DescribeSObjectResult sObjectInfo);

	Integer getUniqueIndex(String objName);

	ID getObjectId(
		String objKey,
		Boolean required
	);

	ID insertObj(
		SObject newObject, 
		String objKey
	);

	void setObjectFieldValues(
		SObject newObject,
		String objKey
	);

	ID getRecordTypeId(
		DescribeSObjectResult sObjectInfo,
		String recTypeDevName
	);

	void setRecordTypeId(
		String objKey,
		DescribeSObjectResult sObjectInfo,
		String recTypeDevName
	);

	void setFieldValue(
		String objKey,
		Schema.SObjectField fieldToken,
		Object newValue
	);

	void setFieldValue(
		String objKey,
		String fieldApiName,
		Object newValue
	);

	void setFieldValues(
		String objKey,
		Map<String,Object> newObjValues
	);

	void setObjectOption(
		String objKey,
		String optionName,
		Object newValue
	);

	Object getObjectOption(
		String objKey,
		String optionName,
		Object defaultValue
	);

	Object getFieldValue(
		String objKey,
		Schema.SObjectField fieldToken,
		Boolean required
	);

	Object getFieldValue(
		String objKey,
		String fieldApiName,
		Boolean required
	);

	void checkRequiredFieldValue(
		String objKey,
		String fieldApiName
	);

	void checkRequiredFieldValue(
		String objKey,
		Schema.SObjectField fieldToken
	);

}