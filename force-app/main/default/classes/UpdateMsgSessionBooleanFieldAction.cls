/**
 * @File Name          : UpdateMsgSessionBooleanFieldAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/16/2023, 4:09:37 AM
**/
public without sharing class UpdateMsgSessionBooleanFieldAction { 

	@InvocableMethod(label='Set Messaging Session Boolean Field' callout=false)
	public static void setMsgSessionBooleanField(List<UpdateInfo> updateInfoList) { 
		new MessagingSessionUpdateManager().processUpdate(updateInfoList);
	}
		
	// *******************************************************************

	public class UpdateInfo implements FieldValueSource {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'fieldName' required = true)
		public String fieldName;

		@InvocableVariable(label = 'fieldValue' required = false)
		public Boolean fieldValue;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			Map<String, Object> result = new Map<String, Object>();
			result.put(fieldName, fieldValue);
			return result;
		}

	}

}