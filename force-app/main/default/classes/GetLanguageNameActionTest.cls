/**
 * @File Name          : GetLanguageNameActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 2:48:29 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 2:47:09 PM   aniubo     Initial Version
 **/
@isTest
private class GetLanguageNameActionTest {
	@isTest
	private static void tesLanguageCodesIsNullOrEmpty() {
		// Test data setup
		Boolean isError = false;
		List<String> customLabelNames;
		// Actual test
		Test.startTest();
		try {
			customLabelNames = GetLanguageNameAction.getLanguageName(null);
			Assert.areEqual(
				'',
				customLabelNames.get(0),
				'Should return empty string'
			);

			customLabelNames = null;

			customLabelNames = GetLanguageNameAction.getLanguageName(
				new List<ID>()
			);
			assert.areEqual(
				'',
				customLabelNames.get(0),
				'Should return empty string'
			);
		} catch (Exception ex) {
			isError = true;
		}
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}

	@isTest
	private static void testGetLanguageNameEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			List<String> stages = GetLanguageNameAction.getLanguageName(
				new List<String>()
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testGetLanguageName() {
		// Test data setup
		List<String> languageCodes = new List<String>{ 'de' };
		// Actual test
		Test.startTest();
		List<String> customLabelNames = GetLanguageNameAction.getLanguageName(
			languageCodes
		);
		Test.stopTest();
		Service_Language__mdt setting = ServiceLanguagesManager.getLanguageRecord(
			languageCodes.get(0)
		);
		// Asserts
		Boolean isUnknown = setting == null;
		Assert.areEqual(
			isUnknown,
			customLabelNames.get(0) == 'UNKNOWN',
			isUnknown
				? 'Should return UNKNOWN'
				: 'Should return Custom Label Value'
		);
	}
}