/**
 * @File Name          : UpdateBotCaseLanguageActionTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/14/2024, 1:52:53 PM
**/
@IsTest
private without sharing class UpdateBotCaseLanguageActionTest {

	/**
	 * Exception 
	 */
	@IsTest
	static void setBotCaseLanguage1() {
		Boolean error = false;

		UpdateBotCaseLanguageAction.ActionInfo info = 
			new UpdateBotCaseLanguageAction.ActionInfo();
		info.messagingSessionId = null;
		info.languageCode = OmnichanelConst.ENGLISH_LANG_CODE;

		List<UpdateBotCaseLanguageAction.ActionInfo> infoList = 
			new List<UpdateBotCaseLanguageAction.ActionInfo> { info };
		
		Test.startTest();
		ExceptionTestUtil.prepareDummyException();
		
		try {
			List<String> result = 
				UpdateBotCaseLanguageAction.setBotCaseLanguage(infoList);
				
		} catch(Exception ex) {
			error = true;
		}
		
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}    

	/**
	 * Given the difficulty in creating MessagingSession test records
	 * the messagingSessionId parameter always has a null value
	 * 
	 * test 1 : infoList is not empty 
	 * test 2 : infoList = null
	 * test 3 : infoList is empty
	 * 
	 */
	@IsTest
	static void setBotCaseLanguage2() {
		UpdateBotCaseLanguageAction.ActionInfo info = 
			new UpdateBotCaseLanguageAction.ActionInfo();
		info.messagingSessionId = null;
		info.languageCode = OmnichanelConst.ENGLISH_LANG_CODE;

		List<UpdateBotCaseLanguageAction.ActionInfo> infoList1 = 
			new List<UpdateBotCaseLanguageAction.ActionInfo> { info };

		List<UpdateBotCaseLanguageAction.ActionInfo> infoList2 = 
			new List<UpdateBotCaseLanguageAction.ActionInfo>();

		Test.startTest();
		// test 1 => result[0] = null
		List<String> result1 = 
			UpdateBotCaseLanguageAction.setBotCaseLanguage(infoList1);
		// test 2 => result[0] = null
		List<String> result2 = 
			UpdateBotCaseLanguageAction.setBotCaseLanguage(null);
		// test 3 => result[0] = null
		List<String> result3 = 
			UpdateBotCaseLanguageAction.setBotCaseLanguage(infoList2);
		Test.stopTest();
		
		Assert.isNull(result1[0], 'Invalid result');
		Assert.isNull(result2[0], 'Invalid result');
		Assert.isNull(result3[0], 'Invalid result');
	}
	
}