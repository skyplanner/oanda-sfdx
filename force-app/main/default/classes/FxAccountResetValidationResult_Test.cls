/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 08-19-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   08-19-2020   dmorales   Initial Version
**/
@isTest
private class FxAccountResetValidationResult_Test {
    @isTest
    static void test_invocable() {
        Boolean error = false;
        List<ID> fxAccountIdList = MIFIDValidationTestDataFactory.getValidableFxAccountsIdList();
        Test.startTest();
        try {
            FxAccountResetValidationResult.resetValidationResult(fxAccountIdList);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
}