/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-01-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class IHSIntegrationManager {
    final static String W8_W9_STATUS_FIELD_NAME = 'W8_W9_Status__c';
    final static String CONDITIONALLY_APPROVED_FIELD_NAME = 'Conditionally_Approved__c';
    final static String STATUS_SETTING = 'IHS_Interview_Updated_Statuses';
    public final static String CONDITIONALLY_APPROVED_DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss';

    public void updateConditionallyApprovedAfterPIU(
        List<fxAccount__c> newList, Map<Id,fxAccount__c> oldMap
    ) {
        Set<Id> taxDeclarationFxAccounts = getTaxDeclarations(newList);
        Set<String> piuChangeFieldNames = getPIUChangeFieldNames();
        List<String> statuses = getInterviewUpdateStatuses();

        for (fxAccount__c fxAcc : newList) {
            fxAccount__c fxAccountOld = oldMap.get(fxAcc.Id);

            //Check fxAccount has tax declaration
            if (fxAcc.TAX_Declarations__c == null && 
                !taxDeclarationFxAccounts.contains(fxAcc.Id))
                continue;

            if (SObjectUtil.isRecordFieldsChanged(
                fxAcc, fxAccountOld, CONDITIONALLY_APPROVED_FIELD_NAME)
            ) {
                fxAcc.Conditionally_Approved_Date__c =
                    fxAcc.Conditionally_Approved__c ? Datetime.now() : null;
                fxAcc.Conditionally_Approved_Date_Text__c = 
                    fxAcc.Conditionally_Approved_Date__c?.format(
                        CONDITIONALLY_APPROVED_DATE_FORMAT);
            }
                
            if ((SObjectUtil.isRecordFieldsChanged(
                fxAcc, fxAccountOld, W8_W9_STATUS_FIELD_NAME) &&
                String.isNotBlank(fxAcc.W8_W9_Status__c) &&
                isInterviewUpdateStatus(statuses, fxAcc.W8_W9_Status__c)) ||
                (!fxAcc.Conditionally_Approved__c &&  
                fxAcc.Conditionally_Approved_Date__c != null)
            ) {
                
                fxAcc.Conditionally_Approved_Date__c = null;
                fxAcc.Conditionally_Approved_Date_Text__c = null;
            }

            if (isPIUChange(piuChangeFieldNames, fxAcc, fxAccountOld) &&
                fxAcc.US_Shares_Trading_Enabled__c
            ) {
                fxAcc.Conditionally_Approved__c = true;
                fxAcc.Conditionally_Approved_Date__c = Datetime.now();
                fxAcc.Conditionally_Approved_Date_Text__c = 
                    fxAcc.Conditionally_Approved_Date__c?.format(
                        CONDITIONALLY_APPROVED_DATE_FORMAT);
            }
        }
    }

    Set<String> getPIUChangeFieldNames() {
        Set<String> result =  new Set<String>();

        for (IHS_Integration_Field__mdt f :
            IHS_Integration_Field__mdt.getAll().values())
            result.add(f.Field_Name__c);

        return result;
    }
    
    Boolean isPIUChange(Set<String> fieldNames,
        fxAccount__c newOne, fxAccount__c oldOne
    ) {
        for (String fieldName : fieldNames)
            if (SObjectUtil.isRecordFieldsChanged(newOne, oldOne, fieldName))
                return true;

        return false;
    }

    Boolean isInterviewUpdateStatus(
        List<String> statuses, String w8w9Status
    ) {
        return statuses.isEmpty() || 
            statuses.contains(w8w9Status.toLowerCase());
    }

    List<String> getInterviewUpdateStatuses() {
        List<String> statuses = new List<String>();        
        SP_General_Setting__mdt sett
            = SP_General_Setting__mdt.getInstance(STATUS_SETTING);

        if (sett == null || String.isBlank(sett.Value__c))
            return statuses;

        statuses = sett.Value__c.split(',');

        for (Integer i = 0; i < statuses.size(); i++)
            statuses[i] = statuses[i].trim().toLowerCase();

        return statuses;
    }

    Set<Id> getTaxDeclarations(List<fxAccount__c> fxAccList) {
        Set<Id> result = new Set<Id>();
        Set<Id> fxAccountIds = new Set<Id>();
        
        for (fxAccount__c fxAcc : fxAccList) {
            fxAccountIds.add(fxAcc.Id);
        }

        for (Tax_Declarations__c td : [SELECT fxAccount__c 
            FROM Tax_Declarations__c 
            WHERE fxAccount__c IN: fxAccountIds])
            if (td.fxAccount__c != null)
                result.add(td.fxAccount__c);

        return result;
    }
}