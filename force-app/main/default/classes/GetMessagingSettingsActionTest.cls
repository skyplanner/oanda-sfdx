/**
 * @File Name          : GetMessagingSettingsActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 2:57:38 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 2:56:17 PM   aniubo     Initial Version
 **/
@isTest
private class GetMessagingSettingsActionTest {
	@isTest
	private static void testGetMessagingSettingsEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			GetMessagingSettingsAction.getMessagingSettings();
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testGetMessagingSettings() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<GetMessagingSettingsAction.MessagingSettingsInfo> messagingSettings = GetMessagingSettingsAction.getMessagingSettings();

		Test.stopTest();
		Boolean debugMode = SPSettingsManager.checkSetting(
			MessagingConst.MSG_DEBUG_MODE_SETTING
		);
		String defaultLanguage = SPSettingsManager.getSetting(
			MessagingConst.MSG_DEFAULT_LANGUAGE_SETTING
		);
		String defaultRegion = SPSettingsManager.getSetting(
			MessagingConst.MSG_DEFAULT_REGION_SETTING
		);

		// Asserts
		Assert.areEqual(
			debugMode,
			messagingSettings.get(0).debugMode,
			'Debug mode should be equal' + debugMode
		);

		Assert.areEqual(
			defaultLanguage,
			messagingSettings.get(0).defaultLanguage,
			'Default Language should be equal' + defaultLanguage
		);

		Assert.areEqual(
			defaultRegion,
			messagingSettings.get(0).defaultRegion,
			'Default Region should be equal' + defaultRegion
		);
	}
}