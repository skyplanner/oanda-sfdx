@isTest
public with sharing class BatchSharesByOneIdTriggerRecalcTest {

    @TestSetup
    static void initData() {

        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 1234324;
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        insert fxAccount;
    }

    @IsTest
    static void triggerSharesRecalc(){
        fxAccount__c fxAccount = [SELECT Id, RecordTypeId, Email__c, US_Shares_Trading_Enabled__c, Division_Name__c from fxAccount__c limit 1];

        //set mocks
        String userBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) {
            userBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            userBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.USER_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String oneIdURL = userBaseUrl
                         + String.format(
                             CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),
                             new List<String> {fxAccount.Email__c});
        APISettings apiSettings = APISettingsUtil.getApiSettingsByName(Constants.GROUP_RECALCULATION_ENDPOINT);
        String sharesRecalcURL = apiSettings.baseUrl;

        Test.setMock(HttpCalloutMock.class, 
            CalloutMock.getMultiStaticResourceSuccessMock(
                new List<CalloutMock.MultiStaticResourceItem> {
                    new CalloutMock.MultiStaticResourceItem(
                        oneIdURL, 'TestGetOneIdByEmailOK'),
                    new CalloutMock.MultiStaticResourceItem(
                        sharesRecalcURL, 'TestTasGroupSharesRecalcOK')
                }));

        Test.startTest();
        fxAccount.US_Shares_Trading_Enabled__c = true;
        update fxAccount;
        Test.stopTest();

        Assert.areEqual(0, [SELECT Count() From Log__c WHERE Category__c = :BatchSharesByOneIdTriggerRecalc.LOGGER_CATEGORY], 'no logs should be present');
    }

}