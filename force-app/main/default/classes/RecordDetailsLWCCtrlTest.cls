/**
 * RecordDetailsLWCCtrl test class
 */
@isTest
public with sharing class RecordDetailsLWCCtrlTest {
    
    /**
     * Get case related account information
     */
    @isTest
    static  void fetchRelatedRecord() {
        Account acc1 = new Account(
            LastName = 'test account');
        insert acc1;

        Case case1 = new Case(
            Subject = 'Test',
            AccountId = acc1.Id);
        insert case1;

        Map<String, String> data =
            RecordDetailsLWCCtrl.fetchRelatedRecord(
                case1.Id, 'AccountId');

        System.assertEquals(data.get('id'), acc1.Id);
        System.assertEquals(data.get('objName'), 'Account');
    }
    
}