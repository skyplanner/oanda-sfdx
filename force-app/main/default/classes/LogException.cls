/**
 * Log execption to replace outer-most exceptions handlings.
 * The message should include the Id to the LOG containing
 * more information about the original exception.
 */
public class LogException extends Exception {
    
    private static final String MSG_PLACEHOLDER_LOG_ID = '[LOG_ID]';
	private static final String MSG_PLACEHOLDER_EXCEPTION_MSG = '[EXCEPTION_MSG]';
	
	private String source;
	private String details;

    /**
	 * @return The name of the original class 
	 * where the exception was caught.
	 */
	public String getSourceClassApiName() {
		return source;
	}

	/**
	 * @return The details section of the log generated 
	 * by this exception.
	 */
	public String getDetails() {
		return details;
	}

    /**
	 * Creates a new general exception ready to be re-throw.
	 * Before returning the new exception, the following
	 * actions are taken:<p>
	 * 1. A log is saved with information about the original
	 * exception.<p>
	 * 2. A short message with the new log id is set
	 * as the new exception message.
	 * @param sourceName 	name of the class or trigger in which the 
	 *						original exception was catched.
	 * @param originalException
	 *						Original exception that was catched
	 */
	public static LogException newInstance(
        String source,
        Exception originalException)
    {
      	return newInstance(
			originalException.getMessage(),
			source,
			getDetailsMessage(originalException)
		);
    }

	/**
	 * Creates a new general exception ready to be re-throw.
	 * Before returning the new exception, the following
	 * actions are taken:<p>
	 * 1. A log is saved with information about the original
	 * exception.<p>
	 * 2. A short message with the new log id is set
	 * as the new exception message.
	 */
	public static LogException newInstance(
        String msg,
		String source)
    {
        return newInstance(msg, source, '');
    }

	/**
	 * Creates a new general exception ready to be re-throw.
	 * Before returning the new exception, the following
	 * actions are taken:<p>
	 * 1. A log is saved with information about the original
	 * exception.<p>
	 * 2. A short message with the new log id is set
	 * as the new exception message.
	 */
	public static LogException newInstance(
        String msg,
		String source,
        String details)
    {
        // we create a new log and insert it
        Log l = Logger.exception(
            msg,
            source,
            details);

        return newInstance(msg, source, l);
    }

	/**
	 * Creates a new general exception ready to be re-throw.
	 * Before returning the new exception, the following
	 * actions are taken:<p>
	 * 1. A log is saved with information about the request and response.<p>
	 * 2. A short message with the new log id is set
	 * as the new exception message.
	 */
	public static LogException newInstance(
        String msg,
		String source,
		HttpRequest request,
        HttpResponse response)
    {
        // we create a new log and insert it
        Log l = Logger.exception(
            msg,
            source,
			request,
			response);

        return newInstance(msg, source, l);
    }

    /**
	 * Creates the details to be saved to the new log
	 * @param originalException
	 */
	@testVisible
	public static String getDetailsMessage(
        Exception originalException)
    {
		return
            'Exception type: ' + originalException.getTypeName() +
			'\nError message: ' + originalException.getMessage() + 
			'\nStack Trace:' + originalException.getStackTraceString();
	}

	/**
	 * Creates a new general exception ready to be re-throw.
	 * Before returning the new exception, the following
	 * actions are taken:<p>
	 * 1. A log was saved.<p>
	 * 2. A short message with the new log id is set
	 * as the new exception message.
	 */
	private static LogException newInstance(
        String msg,
		String source,
       	Log log)
    {
		// we create the exception with a new message
        // provide the original mesage with the log id...
        String message = getExceptionMessage(
            msg,
            log.Id);

        // we return the exception ready to get thrown (if necessary)
        LogException r = new LogException(message);
        r.source = source;
        r.details = log.details;
        return r;
	}

    /**
	 * Creates the details to be saved to the new log
	 * @param originalException
	 * @param logId
	 * @return the exception message
	 */
	private static String getExceptionMessage(
        String msg,
        String logId)
    {
        return Label.Log_Exception
            .replace(MSG_PLACEHOLDER_EXCEPTION_MSG,
                msg)
            .replace(MSG_PLACEHOLDER_LOG_ID, logId);
    }

}