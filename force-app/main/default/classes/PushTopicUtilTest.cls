/**
 * Test PushTopicUtil class
 * 
 * @author Gilbert Gao
 */
@isTest
private class PushTopicUtilTest {

    @isTest
	static void testCQGRequest()
	{
		
		Test.startTest();
		
		String topicName = PushTopicUtil.PUSH_TOPIC_CQG_REQUEST;
		
		//insert new topic
		System.debug('insert new CQG topic');
		PushTopicUtil.upsertTopic_CQGRequest_Name(topicName);
		
		List<PushTopic> cqgTopics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, cqgTopics[0].isActive);
		
		
		//update exiting topic
		System.debug('update existing CQG topic');
		cqgTopics[0].Query = 'select Id from CQG_Account__c where OPA_Request__c = true';
		update cqgTopics;

		PushTopicUtil.upsertTopic_CQGRequest();
		
		cqgTopics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, cqgTopics[0].isActive);
		
		Test.stopTest();
		
	}
	
	@isTest
	static void testFxAccntStream()
	{
		
		Test.startTest();
		
		String topicName = PushTopicUtil.PUSH_TOPIC_FXACCOUNT_UPDATE;
		
		//insert new topic
		System.debug('insert new fxAccount stream topic');
		PushTopicUtil.upsertTopic_fxAccnt_stream_updates_Name(topicName);
		
		List<PushTopic> topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		
		//update exiting topic
		System.debug('update existing fxAccount stream topic');
		PushTopicUtil.upsertTopic_fxAccnt_stream_updates();
		
		topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		Test.stopTest();
		
	}
	
	@isTest
	static void testFxAccntClientStatus()
	{
		
		Test.startTest();
		
		String topicName = PushTopicUtil.PUSH_TOPIC_FXACCOUNT_CLIENT_STATUS;
		
		//insert new topic
		System.debug('insert new fxAccount client status');
		PushTopicUtil.upsertTopic_fxAccnt_client_status_Name(topicName);
		
		List<PushTopic> topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		
		//update exiting topic
		System.debug('update existing fxAccount client status');
		PushTopicUtil.upsertTopic_fxAccnt_client_status();
		
		topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		Test.stopTest();
		
	}
	
	@isTest
	static void testTradeGroup()
	{
		
		Test.startTest();
		
		String topicName = PushTopicUtil.PUSH_TOPIC_FXACCOUNT_TRADE_GROUP;
		
		//insert new topic
		System.debug('insert new fxAccount trade group topic');
		PushTopicUtil.upsertTopic_fxAccnt_trade_group_Name(topicName);
		
		List<PushTopic> topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		
		//update exiting topic
		System.debug('update existing fxAccount trade group topic');
		PushTopicUtil.upsertTopic_fxAccnt_trade_group();
		
		topics = [select Id, Query, isActive from PushTopic where Name = :topicName];
		System.assertEquals(true, topics[0].isActive);
		
		Test.stopTest();
		
	}

	@isTest
	static void testupsertTopics() {

		Test.startTest();

		//insert new topics
		PushTopicUtil.upsertTopics();

		List<PushTopic> topics = [select Id, Query, isActive from PushTopic];
		Assert.areEqual(4, topics.size());

		Test.stopTest();

	}
	
}