/**
 * @File Name          : SLAViolationNotifiableFactory.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:12:29 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/19/2024, 2:39:19 PM   aniubo     Initial Version
 **/
public with sharing class SLAViolationNotifiableFactory {
	public static SLAViolationNotifiable SLAViolationNotifiableFactory(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader
	) {
		return SLAViolationNotifiableFactory(
			notificationObjectType,
			violationType,
			dataReader,
			null
		);
	}

	public static SLAViolationNotifiable SLAViolationNotifiableFactory(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		Id agentId
	) {
		SLAViolationCanNotifiable violationNotificationChecker = new SLAViolationCanNotifyChecker();
		SLAViolationNotifiable notifier;
		switch on notificationObjectType {
			when LIVE_CHAT_NOT {
				notifier = new ChatSLAViolationNotifier(
					notificationObjectType,
					violationType,
					dataReader,
					violationNotificationChecker,
					agentId
				);
			}
			when MESSAGING_NOT {
				notifier = new MessagingSLAViolationNotifier(
					notificationObjectType,
					violationType,
					dataReader,
					violationNotificationChecker
				);
			}
			when CHAT_CASE_NOT, EMAIL_CASE_NOT, PHONE_CASE_NOT {
				notifier = new CaseSLAViolationNotifier(
					notificationObjectType,
					violationType,
					dataReader,
					violationNotificationChecker
				);
			}
		}
		return notifier;
	}
}