/**
 * @File Name          : ChatSLAViolationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 4:02:51 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 11:28:04 AM   acantero     Initial Version
**/
@isTest
private without sharing class ChatSLAViolationManager_Test {

    @testSetup
    static void setup() {
        SLAViolationsTestDataFactory.createTestChats();
    }

    @isTest
    static void hvcWaitingViolation() {
        List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
        Test.startTest();
        ChatSLAViolationManager.hvcWaitingViolation(chatIdList);
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(chatIdList.size(), count);
    }

    @isTest
    static void hvcMissedViolation() {
        List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
        Test.startTest();
        ChatSLAViolationManager.hvcMissedViolation(chatIdList);
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(chatIdList.size(), count);
    }

    @isTest
    static void declinedViolation() {
        List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
        String chatId = chatIdList[0];
        String agentId = UserInfo.getUserId();
        Test.startTest();
        ChatSLAViolationManager.declinedViolation(
            chatId,
            agentId
        );
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(1, count);
    }
    
}