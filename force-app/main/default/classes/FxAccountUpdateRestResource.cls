/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Apex REST class designed to update FxAccount object fields
*  /services/apexrest/Users/
* Note : Fields configured on FX_Trade_User_Config__mdt only updated(custom meta data type)
* FX_Trade_User_Config__mdt holds the user friendly input field name gets passed onto the request and corresponding salesforce api field name
*
* HTTP Verbs Handled:my
* POST - handles upsert of FxAccount object fields
*
* Sample Request Format
* [{
*   "user_id" : 1,
*   "hvc_score" : 0.1
* },
* {
*   "user_id" : 2,
*   "hvc_score" : 0.2
* }]
*/
@RestResource(urlMapping='/Users/')
global class FxAccountUpdateRestResource{
    
    global static Map<string,string> ApiNamefieldNameMap = new Map<string,string>{};
    global static string fxAccountLiveId = RecordTypeUtil.getFxAccountLiveId();

    @HttpPost
    global static String doPost(){

        RecordResult[] saveResult = new RecordResult[]{}; 
        
        if(!CustomSettings.isAPIEnabled(CustomSettings.API_NAME_FX_ACCOUNT_UPDATE)){ 
            RecordResult recordResult = new RecordResult();
            recordResult.isSuccessful = false;
            recordResult.errorInfo =  'API ' + CustomSettings.API_NAME_FX_ACCOUNT_UPDATE + ' is disabled';
            saveResult.add(recordResult);         
            return JSON.serialize(saveResult);
        }
        
        if(!CustomSettings.isUserAllowedForAPI(UserInfo.getUserId(), CustomSettings.API_NAME_FX_ACCOUNT_UPDATE)){
            RecordResult recordResult = new RecordResult();
            recordResult.isSuccessful = false;
            recordResult.errorInfo =  'User is not allowed to call this API';
            saveResult.add(recordResult);         
            return JSON.serialize(saveResult);
        }
         
        fxAccount__c[] fxAccountsToUpdate = new fxAccount__c[]{};    
        String inputJsonStr = RestContext.request.requestBody.toString();
        List<Object> userObjects = (List<Object>) JSON.deserializeUntyped(inputJsonStr);
        
        //no user records
        if(userObjects == null || userObjects.size() == 0){
            RecordResult recordResult = new RecordResult();
            recordResult.isSuccessful = false;
            recordResult.errorInfo =  'No user records';
            saveResult.add(recordResult);         
            return JSON.serialize(saveResult);
        }
        
        if(ApiNamefieldNameMap.isEmpty()){
            //Fields passed on this service are configured on FX Trade User Config.     
            for(FX_Trade_User_Config__mdt fieldConfig : [SELECT Field_Name__c, API_Name__c FROM FX_Trade_User_Config__mdt]){
                ApiNamefieldNameMap.put(fieldConfig.API_Name__c, fieldConfig.Field_Name__c);
            }
        }
        
        //iterate user objects and generate fxAccount object 
        for(object userObject : userObjects){ 
           
            Map<String, object> userRecord = (Map<String, object>)userObject;       
            if(userRecord.get('user_id') != null)
            {      
                fxAccount__c fxAccount = new fxAccount__c();
                
                // convert fxAccount live id to 15 characters if its found to be 18 characters
                if(fxAccountLiveId != null && fxAccountLiveId.length() == 18)
                {
                   fxAccountLiveId = fxAccountLiveId.substring(0,15);
                }
                fxAccount.fxTrade_Global_ID__c = (integer)userRecord.get('user_id') + '+' + fxAccountLiveId;
                String objectName = 'fxAccount__c';
                // Iterate fields configured on FX_Trade_User_Config__mdt and update them only if the field exist on input payload
                for(string apiName : ApiNamefieldNameMap.keySet()){
                    string inputFieldName = ApiNamefieldNameMap.get(apiName);
                    if(inputFieldName != null && inputFieldName != '' && userRecord.containsKey(inputFieldName)){
                        SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
                        DescribeSObjectResult d = r.getDescribe();
                        if(String.valueOf(d.fields.getMap().get(apiName).getDescribe().getType()) == 'Date') {
                            fxAccount.put(apiName, Date.parse((String)userRecord.get(inputFieldName)));
                        }
                        else{
                            fxAccount.put(apiName, userRecord.get(inputFieldName));
                        }
                    }
                }
                fxAccountsToUpdate.add(fxAccount);
            }
        }

        if(fxAccountsToUpdate.size() > 0)
        {
           //allows partial update
           Database.UpsertResult[] saveResults = Database.upsert(fxAccountsToUpdate, fxAccount__c.fields.fxTrade_Global_ID__c, false);
           for(Database.UpsertResult result : saveResults){
                RecordResult recordResult = new RecordResult();
                recordResult.isSuccessful = result.isSuccess();
                recordResult.errorInfo =  result.getErrors();
                saveResult.add(recordResult);
            }
        }

        return JSON.serialize(saveResult);
    }
    
    class RecordResult
    {
        boolean isSuccessful;
        Object errorInfo;
    }
}