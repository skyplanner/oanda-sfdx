/**
 * @File Name          : DataCategoriesHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/17/2019, 5:11:23 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
public with sharing class DataCategoriesHelper {
	
	public static List<DataCategoryWrapper> getDataCategoriesByGroup(String sObjectName, String groupName) {
		return getDataCategoriesByGroup(
			sObjectName,
			groupName,
			true
		);
	}

	public static List<DataCategoryWrapper> getDataCategoriesByGroup(
		String sObjectName, 
		String groupName, 
		Boolean includeTopCat
	) {
		List<DataCategoryWrapper> result = new List<DataCategoryWrapper>();
		if (String.isBlank(sObjectName) || String.isBlank(groupName)) {
			return result;
		}
		//else...
		List<DataCategory> topLevelCategories = getTopCategories(sObjectName, groupName);
		if (topLevelCategories != null) {
			addCategoriesToList(result, topLevelCategories, 1, includeTopCat);
		}
		return result;
	}

	public static Map<String,String> getDataCategoriesMapByGroup(String sObjectName, String groupName) {
		Map<String,String> result = new Map<String,String>();
		if (String.isBlank(sObjectName) || String.isBlank(groupName)) {
			return result;
		}
		//else...
		List<DataCategory> topLevelCategories = getTopCategories(sObjectName, groupName);
		if (topLevelCategories != null) {
			addCategoriesToMap(result, topLevelCategories);
		}
		return result;
	}

	private static List<DataCategory> getTopCategories(String sObjectName, String groupName) {
		List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult = 
			getDescribeCategoryStructureResult(sObjectName, groupName);
		System.debug('describeCategoryStructureResult.size: ' + describeCategoryStructureResult.size());
		if (describeCategoryStructureResult.isEmpty()) {
			return null;
		}
		//else...
		DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0];
		//Get the top level categories
		List<DataCategory> topLevelCategories = singleResult.getTopCategories();
		System.debug('topLevelCategories.size: ' + topLevelCategories.size());
		return topLevelCategories;
	}

	private static List<DescribeDataCategoryGroupStructureResult> getDescribeCategoryStructureResult(String sObjectName, String groupName) {
		List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
		DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
		p.setSobject(sObjectName);
		p.setDataCategoryGroupName(groupName);
		pairs.add(p);
		List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult =
			Schema.describeDataCategoryGroupStructures(pairs, false);
		return describeCategoryStructureResult;
	}

	private static void addCategoriesToList(
		List<DataCategoryWrapper> catList, 
		List<DataCategory> categories, 
		Integer level,
		Boolean canBeIncluded
	) {
		for(DataCategory cat : categories) {
			if (canBeIncluded) {
				catList.add(
					new DataCategoryWrapper(
						cat.getName(),
						cat.getLabel(),
						level
					)
				);
			}
			List<DataCategory> childCategories = cat.getChildCategories();
			addCategoriesToList(catList, childCategories, level + 1, true);
		}
	}

	private static void addCategoriesToMap(Map<String, String> catMap, List<DataCategory> categories) {
		for(DataCategory cat : categories) {
			catMap.put(cat.getName(), cat.getLabel());
			List<DataCategory> childCategories = cat.getChildCategories();
			addCategoriesToMap(catMap, childCategories);
		}
	}

	public class DataCategoryWrapper {
		@AuraEnabled
		public String value {get; set;}
		@AuraEnabled
		public String label {get; set;}
		@AuraEnabled
		public Integer level {get; set;}

		public DataCategoryWrapper(String value, String label, Integer level) {
			this.value = value;
			this.label = label;
			this.level = level;
		}
	}
}