/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 08-17-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   08-17-2020   dmorales   Initial Version
**/
public class CaseMiFIDValidatedChange {
    @InvocableMethod(label='Case fxAccountSetValidationResult' description='')
    public static void fxAccountSetValidationResult(List<Id> caseList) {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        if (valSettings.isActive) {
            FxAccountMIFIDValidationManager.updateValidationResultFromCase(caseList);
        }
    }
}