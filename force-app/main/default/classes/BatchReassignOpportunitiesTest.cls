@isTest
private class BatchReassignOpportunitiesTest {

	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

	@testSetup static void init(){

		//Create test data
		
		TestDataFactory testHandler = new TestDataFactory();

		List<Account> accntList = testHandler.createTestAccounts(2);
	    List<Opportunity> opptyList = testHandler.createOpportunities(accntList);
		List<fxAccount__c> fxaList = testHandler.createFXTradeAccounts(accntList);

		for(Integer k = 0; k<2; k++){
			fxaList[k].RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
			fxaList[k].Opportunity__c = opptyList[k].Id;
			fxaList[k].Funnel_Stage__c = FunnelStatus.FUNDED;
			fxaList[k].Division_Name__c = 'OANDA Corporation';
			fxaList[k].hvc_score__c = 0.7;
		}

		update fxaList;


		for(Integer k = 0; k<2; k++){
			opptyList[k].RecordTypeId = RecordTypeUtil.getSalesOpportunityTypeId();
			opptyList[k].OwnerId = UserUtil.getSystemUserId();
			opptyList[k].StageName = FunnelStatus.FUNDED;
			opptyList[k].CloseDate = Date.today();
			opptyList[k].fxAccount__c = fxaList[k].Id;
		}

		update opptyList;

	}
	
	@isTest static void testReassignOpportunities() {

		Test.startTest();

    	String Query = 'SELECT Id, RecordTypeId,US_TO_RM_Run_Batch_To_Reassign__c , AccountId, OwnerId, Current_Deposit_USD_MTD__c, Opportunity_Sales_Division__c, Client_Value_Indicator_Score__c FROM Opportunity WHERE RecordTypeId != \'' + RecordTypeUtil.getRetentionOpportunityTypeId() + '\' AND OwnerId = \'' + UserUtil.getSystemUserId() + '\' AND Client_Value_Indicator_Score__c >= 0.5 ORDER BY Current_Deposit_USD_MTD__c DESC';
    	BatchReassignOpportunitiesSchedulable batch = new BatchReassignOpportunitiesSchedulable(query);
    	Database.executeBatch(batch);
    	
		Test.stopTest();

		List<Opportunity> oppsReassigned = [SELECT id, Name, OwnerId FROM Opportunity WHERE Client_Value_Indicator_Score__c >= 0.5];
    	
		/* system.assertNotEquals(UserUtil.getSystemUserId(), oppsReassigned[0].OwnerId);
		system.assertNotEquals(UserUtil.getSystemUserId(), oppsReassigned[1].OwnerId);
		system.assertNotEquals(oppsReassigned[0], oppsReassigned[1]); */
	}

	
}