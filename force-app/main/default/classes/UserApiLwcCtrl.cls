/**
 * UserApiLwc controller
 */
public class UserApiLwcCtrl {    
    /**
     * Init
     * - Load actions by settings
     */
    @AuraEnabled
    public static UserApiManager.Data init() {
        try {
            return new UserApiManager().loadData();
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Search external
     */
    @AuraEnabled
    public static List<Map<String, Object>> searchExternal(
        UserApiSearch criteria, Integer limitSize, Integer offset
    ) {
        try {
            return new UserApiExternalSearchManager(
                criteria, limitSize, offset).search();
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Search in Salesforce
     */
    @AuraEnabled
    public static List<fxAccount__c> searchSf(
        UserApiSearch criteria, Integer limitSize, Integer offset
    ) {
        try {
            return new UserApiSfSearchManager(
                criteria, limitSize, offset).search();
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Save to Salesforce
     */
    @AuraEnabled
    public static void saveSf(SObject record) {
        try {
            update record;
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Save to external API
     */
    @AuraEnabled
    public static void saveExternal(
        Map<String, Object> record, Map<String, Object> oldRecord
    ) {
        try {
            // For save other fields split this action,
            // or save all the audit trail at the end of two callouts.
            // UserApiManager userApiManager = new UserApiManager();
            // userApiManager.save(record, oldRecord);

            TasApiManager tasApiManager = new TasApiManager();
            tasApiManager.saveGroupsInfo(record, oldRecord);

        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Get permissions
     */
    @AuraEnabled
    public static UserApiPermissions.PermissionsWrapper getPermissions() {
        try {
            return UserApiPermissions.getPermissions();
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static List<TasV20AccsDetails> getV20AccsDetails(
        String env, String fxTradeUserId
    ) {
        try {            
            EnvironmentIdentifier id = 
                new EnvironmentIdentifier(env, fxTradeUserId);
            TasApiManager tasApiManager = new TasApiManager();
            TasUserAccountGetResponse v20Info =
                tasApiManager.getV20UserInfo(id);
            List<TasV20AccsDetails> v20Accounts = v20Info?.accounts?.v20;

            if (v20Accounts == null)
                return null;

            if (v20Accounts.isEmpty())
                return new List<TasV20AccsDetails>();

            List<TasDivisionTradingGroup> dtgList =
                tasApiManager.getDivisionTradingGroups(env,
                    v20Info.defaultAccountConfig?.v20?.
                    divisionTradingGroupID?.divisionID);
            List<TasMT4Server> mt4Servers = tasApiManager.getMT4Servers(env,
                v20Info.defaultAccountConfig?.v20?.pricingGroupOverrideIDs);

            if (dtgList == null || mt4Servers == null)
                return v20Accounts;

            dtgList.sort();
            mt4Servers.sort();

            for (TasV20AccsDetails d: v20Accounts) {
                d.dtgOptions = new List<TasV20AccsDetails.PicklistOption>();
                d.mt4ServerOptions = 
                    new List<TasV20AccsDetails.PicklistOption>();

                for (TasDivisionTradingGroup dtg : dtgList) {
                    TasV20AccsDetails.PicklistOption o =
                        new TasV20AccsDetails.PicklistOption();
                    o.label = dtg?.label;
                    o.value = dtg?.id?.tradingGroupID;
                    o.selected = o.value == d.divisionTradingGroupID;
                    d.dtgOptions.add(o);
                }

                for (TasMT4Server m : mt4Servers) {
                    TasV20AccsDetails.PicklistOption o =
                        new TasV20AccsDetails.PicklistOption();
                    Integer mt4ServerId = Integer.valueOf(m?.server_id);
                    o.label = m?.server_id + ' - ' +
                         m?.server_name;
                    o.value = mt4ServerId;
                    o.selected = o.value == d.mt4ServerID;
                    o.disabled = !m?.valid;
                    d.mt4ServerOptions.add(o);
                }
            }

            return v20Accounts;

        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static List<TasMT5AccsDetails> getMT5AccsDetails(String env, String fxTradeUserId, String email ) {
        try {
            TasApiManager tasApiManager = new TasApiManager();
            EnvironmentIdentifier ei = new EnvironmentIdentifier(env, fxTradeUserId);

            return tasApiManager.getMt5AccountsByOneId(ei, email);
        } catch (Exception ex) {   
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static List<Audit_Trail__c> getAuditTrails(String fxTradeUserId) {
        try {
            AuditTrailManager mng = new AuditTrailManager();
            return mng.getAuditTrails(fxTradeUserId);
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    /**
     * Call the action manager class
     */
    @AuraEnabled
    public static Object callAction(
        String actionName, String actionLabel,
        String actionManagerClass, Map<String, Object> record
    ) {
        try {
            if (String.isBlank(actionManagerClass)) {
                throw LogException.newInstance(
                    Constants.LOGGER_USER_API_CATEGORY,
                    'Missing manager class for action: ' + actionName);
            }

            // Create an action manager class instance
            Callable cls = (Callable) Type.forName(actionManagerClass)
                .newInstance();

            record.put('actionName', actionName);
            record.put('actionLabel', actionLabel);

            // Call the action
            return cls.call(
                UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static void updatev20Account(
        TasApiManager.Updatev20AccountWrapper request
    ) {
        try {
            TasApiManager tasApiManager = new TasApiManager();
            tasApiManager.updateV20Account(request);
        } catch (Exception ex) {
            throw getError(ex);
        }
    }
    
    @AuraEnabled
    public static UserInfoWrapper getUserInfo(
        String env, String fxTradeUserId, String fxTradeName
    ) {
        try {
            EnvironmentIdentifier id = new EnvironmentIdentifier(
                env, fxTradeUserId);
            UserApiManager mng = new UserApiManager();
            TasApiManager tasApiManager = new TasApiManager();
            UserInfoWrapper result = new UserInfoWrapper();
            UserApiUserGetResponse user = mng.getUser(id);
            TasUserAccountGetResponse v20Info =
                tasApiManager.getV20UserInfo(id);
            TasDefaultAccountConfig defaultAccConfig =
                v20Info?.defaultAccountConfig?.v20;
            result.status = mng.getStatus(user?.user_status);
            result.statuses = mng.getStatusList(user);
            result.actions = UserApiPermissions.getAvailableActions(
                fxTradeName, user, v20Info);
            result.record = user?.toMap();
            result.groups = tasApiManager.getGroups(
                env, user.division?.division_id);

            if (result.record != null && defaultAccConfig != null) {

            Integer pricingGroup;
                if (!Util.isListNullOrEmpty(defaultAccConfig.pricingGroupOverrideIDs)) {
                    for (String g : Constants.PRICING_GROUP_NUMBERS_TO_IGNORE) {
                        Integer groupToRemove = Integer.valueOf(g);
                        if (defaultAccConfig.pricingGroupOverrideIDs.contains(groupToRemove)) {
                            defaultAccConfig.pricingGroupOverrideIDs.remove(defaultAccConfig.pricingGroupOverrideIDs.indexOf(groupToRemove));
                        }
                    }
                }
                pricingGroup = Util.isListNullOrEmpty(defaultAccConfig.pricingGroupOverrideIDs) ? 0 : defaultAccConfig.pricingGroupOverrideIDs[0];

                result.record.put(Constants.API_FIELD_PRICING_GROUP, pricingGroup);
                result.record.put(Constants.API_FIELD_COMMISSION_GROUP,
                    defaultAccConfig.commissionGroupID != null ? defaultAccConfig.commissionGroupID : 0);
                result.record.put(Constants.API_FIELD_CURRENCY_CONVERSION_GROUP,
                    defaultAccConfig.currencyConversionGroupOverrideID != null ? defaultAccConfig.currencyConversionGroupOverrideID : 0);

                result.record.put(Constants.API_FIELD_FINANCING_GROUP,
                    (defaultAccConfig.financingGroupIDs != null 
                        && !defaultAccConfig.financingGroupIDs.isEmpty())
                        ? defaultAccConfig.financingGroupIDs[0] : 0); 

                result.record.put(Constants.API_FIELD_NEW_POSITIONS_LOCKED,
                    defaultAccConfig.newPositionsLocked);
            }

            return result;
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static void resendMT4Notification(
        TasApiManager.Updatev20AccountWrapper request
    ) {
        try {
            TasApiManager tasApiManager = new TasApiManager();
            tasApiManager.resendMT4Notification(request);
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    @AuraEnabled
    public static List<UserApiAuditLog> getHistoricalAuditLogs(
        String env, String fxTradeUserId
    ) {
        try {
            UserApiManager mng = new UserApiManager();

            return mng.getAuditLogs(
                new EnvironmentIdentifier(env, fxTradeUserId));
        } catch (Exception ex) {
            throw getError(ex);
        }
    }

    static Exception getError(Exception ex) {
        return ex instanceof LogException 
            ? ex 
            : LogException.newInstance(
                Constants.LOGGER_USER_API_CATEGORY, ex);
    }

    @AuraEnabled
    public static void lockUnlockMt5Acc(String mt5AccountId, String env, String action, String fxTradeUserId) {
        try {
            TasApiManager tasApiManager = new TasApiManager();
            EnvironmentIdentifier ei = new EnvironmentIdentifier(env, mt5AccountId);
            tasApiManager.lockUnlockMt5Acc(ei, action, fxTradeUserId);
        } catch (Exception ex) {   
            throw getError(ex);
        }
    }

    public class UserInfoWrapper {
        @AuraEnabled
        public String status { get; set; }

        @AuraEnabled
        public UserApiManager.StatusesWrapper statuses { get; set; }

        @AuraEnabled
        public List<UserApiAction> actions { get; set; }

        @AuraEnabled
        public Map<String, Object> record { get; set; }
        
        @AuraEnabled
        public Map<String, List<Map<String, String>>> groups { get; set; }
    }
}