/**
 * @File Name          : IsLinkedToFxAccountAction.cls
 * @Description        : Check if a username is linked to an fxAccount
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 5:57:21 AM
**/
public without sharing class IsLinkedToFxAccountAction {

	@InvocableMethod(label='Is Linked To fxAccount' callout=false)
	public static List<ActionResult> isLinkedToFxAccount(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<ActionResult> result = new List<ActionResult> { new ActionResult() };
		
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(infoList[0].messagingSessionId);
			result[0] = new ActionResult(
				process.checkLinkToFxAccount(infoList[0].username)
			);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				IsLinkedToFxAccountAction.class.getName(),
				ex
			);
		}   
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'username' required = true)
		public String username;
		
	}

	// *******************************************************************

	public class ActionResult {

		@InvocableVariable(label = 'isLinkedToAccountOrLead' required = false)
		public Boolean isLinkedToAccountOrLead;

		@InvocableVariable(label = 'fxAccountId' required = false)
		public ID fxAccountId;

		public ActionResult() {
			this.isLinkedToAccountOrLead = false;
		}

		public ActionResult(MessagingAccountCheckInfo accountCheckInfo) {
			this();
			if (accountCheckInfo != null) {
				this.isLinkedToAccountOrLead = 
					accountCheckInfo.isLinkedToAccountOrLead;
				this.fxAccountId = accountCheckInfo.fxAccountId;
			}
		}
		
	}
	
}