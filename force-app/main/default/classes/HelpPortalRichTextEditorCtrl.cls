/**
 * Controller for component: HelpPortalRichTextEditor
 * @author Fernando Gomez, Skyplanner LLC
 * @since 4/30/2019
 */
global without sharing class HelpPortalRichTextEditorCtrl {

	/**
	 * Main constructor
	 */
	global HelpPortalRichTextEditorCtrl() {
		
	}

	/**
	 * @param contentVersionId
	 * @return creates and return the url of a new doc
	 */
	@AuraEnabled
	global static String convertFileToDocument(Id contentVersionId) {
		DocumentManager manager;
		try {
			manager = new DocumentManager();
			return manager.convertFileToDocument(contentVersionId);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param documentPrefix
	 * @param languageCode
	 * @return a list of currently active language
	 */
	@AuraEnabled
    global static String getHtml(String languageCode, String documentPrefix, String divisionCode) {
		HelpPortalRichText s;
		try {
            System.debug('hhh');
            s = new HelpPortalRichText(languageCode, documentPrefix,divisionCode);
            System.debug('s'+s.getHtml());
			return s.getHtml();
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param documentPrefix
	 * @param languageCode
	 * @param html
	 */
	@AuraEnabled
	global static void saveHtml(String languageCode,
                                String documentPrefix, String html, String divisionCode) {
		HelpPortalRichText s;
		try {
			s = new HelpPortalRichText(languageCode, documentPrefix, divisionCode);
			s.saveHtml(html);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}
    
  
}