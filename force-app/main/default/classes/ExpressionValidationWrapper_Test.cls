/**
 * @File Name          : ExpressionValidationWrapper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/27/2020, 4:26:23 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
@isTest
private class ExpressionValidationWrapper_Test {

	@isTest
	static void getExpValMap_test() {
		Set<String> keys = null;
		Test.startTest();
		Map<String,ExpressionValidationWrapper> result = ExpressionValidationWrapper.getExpValMap(keys);
		Test.stopTest();
		System.assert(result.isEmpty());
	}

	@isTest
	static void setPrefixSize_test() {
		Test.startTest();
		ExpressionValidationWrapper expVal = new ExpressionValidationWrapper();
		expVal.setPrefixSize(MiFIDExpValConst.SEPARATOR_REMOVE_PREFIX + '3');
		Test.stopTest();
		System.assertEquals(3, expVal.prefixSize);
	}

}