/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
public with sharing class EmailMessage_Triggers_Test {
	@testSetup
	private static void testSetup() {
		//create custom setting
		Settings__c settings = new Settings__c(
			Name = 'Default'
		);
		insert settings;
		//...
		Account acc = ScheduledEmailTestUtil.createAccount('Test Name');
		Contact ctc = ScheduledEmailTestUtil.createContact(
			acc.Id,
			'Test LastName',
			'me@test.com'
		);
		ScheduledEmailTestUtil.createDraftEmail(
			ScheduledEmailTestUtil.createCase(ctc).Id
		);
	}
	@isTest
	private static void canDeleteNoScheduledMail() {
		// Test data setup
		Test.startTest();
		EmailMessage message = getEmailMessage();
		delete message;
		Test.stopTest();
		message = getEmailMessage();
		System.assertEquals(true, message == null, 'No Draft message');
		// Asserts
	}
	@isTest
	private static void canNotDelete() {
		ScheduledEmailTestUtil.createScheduledMessagesForDraftEmails(
			Date.today(),
			'4',
			'PM',
			Datetime.now()
		);
		// Test data setup
		Test.startTest();
		IScheduledEmailSettingManager settingManager = SpEmailUtil.getScheduledEmailSettingManager();
		settingManager.getSetting().setAllowDelete(false);
		EmailMessage message = getEmailMessage();
		try {
			delete message;
		} catch (Exception ex) {
			System.assertEquals(
				true,
				ex.getMessage()
					.contains(System.Label.Discard_Draft_Error_Message),
				'Can not delete because the draft have a scheduled'
			);
		}
		Test.stopTest();
		// Asserts
	}
	@isTest
	private static void canDelete() {
		ScheduledEmailTestUtil.createScheduledMessagesForDraftEmails(
			Date.today(),
			'4',
			'PM',
			Datetime.now()
		);
		// Test data setup
		Test.startTest();
		IScheduledEmailSettingManager settingManager = SpEmailUtil.getScheduledEmailSettingManager();
		settingManager.getSetting().setAllowDelete(true);
		EmailMessage message = getEmailMessage();
		System.debug(message);
		try {
			delete message;
		} catch (Exception ex) {
			System.assertEquals(
				true,
				ex.getMessage()
					.contains(System.Label.Discard_Draft_Error_Message),
				'Can not delete because the draft have a scheduled'
			);
		}
		Test.stopTest();
		message = getEmailMessage();
		System.debug(message);
		// System.assertEquals(
		// 	true,
		// 	message == null,
		// 	'Delete because mtd is config to TRUE'
		// );
		// Asserts
	}
	private static EmailMessage getEmailMessage() {
		List<EmailMessage> draftList = [
			SELECT Id
			FROM EmailMessage
			WHERE Status = :EmailMessageRepo.EMAIL_MESSAGE_STATUS_DRAFT
		];
		return draftList.isEmpty() ? null : draftList.get(0);
	}
	private static Id getCaseId() {
		return [SELECT Id FROM Case LIMIT 1].Id;
	}
}