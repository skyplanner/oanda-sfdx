/**
 * @File Name          : AgentExtraInfoManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/20/2021, 10:43:48 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/23/2021, 1:35:27 PM   acantero     Initial Version
**/
public inherited sharing class AgentExtraInfoManager {

    final String agentId;

    public AgentExtraInfoManager() {
        this(UserInfo.getUserId());
    }

    public AgentExtraInfoManager(String agentId) {
        this.agentId = agentId;
    }

    public void prepareAgentExtraInfo() {
        Boolean exists = 
            AgentExtraInfoHelper.checkByAgent(agentId);
        if (exists == false) {
            initAgentExtraInfo(0);
        }
    }

    public Integer getcNonHvcCasesByAgent() {
        Integer result  = 0;
        Agent_Extra_Info__c agentExtraInfo = 
            AgentExtraInfoHelper.getByAgent(
                agentId
            );
        if (agentExtraInfo != null) {
            result = agentExtraInfo.Non_HVC_Cases__c.intValue();
        }
        return result;
    }

    public void initAgentExtraInfo(Integer nonHVCCasesValue) {
        Agent_Extra_Info__c agentExtraInfo = 
            new Agent_Extra_Info__c(
                Agent_ID__c = agentId,
                Non_HVC_Cases__c = nonHVCCasesValue
            );
        insert agentExtraInfo;
    }
    
}