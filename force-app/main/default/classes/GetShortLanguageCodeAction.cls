/**
 * @File Name          : GetShortLanguageCodeAction.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/10/2024, 1:16:09 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/10/2024, 12:25:49 PM   aniubo     Initial Version
 **/
public without sharing class GetShortLanguageCodeAction {
	@testVisible
	private static final String LANGUAGE_IDENTIFIER_PREFIX = 'LANGUAGE_';
	@InvocableMethod(
		label='Get Language Code'
		description='Get the short language code from the language Identifier'
	)
	public static List<String> getLanguageCode(
		List<String> languageIdentifiers
	) {
		List<String> languageCodes;
		try {
			ExceptionTestUtil.execute();
			languageCodes = new List<String>{ '' };
			if (
				(languageIdentifiers == null) ||
				languageIdentifiers.isEmpty() ||
				String.isBlank(languageIdentifiers[0])
			) {
				return languageCodes;
			}
			String languageIdentifier = languageIdentifiers[0];
			if (languageIdentifier.startsWith(LANGUAGE_IDENTIFIER_PREFIX)) {
				languageCodes = new List<String>{
					languageIdentifier.substring(
						LANGUAGE_IDENTIFIER_PREFIX.length()
					)
				};
			}
			return languageCodes;
		} catch (Exception ex) {
			throw LogException.newInstance(
				CheckMsgAgentAvailabilityAction.class.getName(),
				ex
			);
		}
	}
}