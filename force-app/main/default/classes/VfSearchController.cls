/**
 * Controller for Visual force Page.
 * @author Fernando Gomez
 * @version 1.0
 * @since March 19th, 2019
 */
global class VfSearchController extends HelpPortalBase {

	global VfSearchController() {
		super();
	}

	/**
	 * Redirects to the new help portal:
	 * Expected source URL:
	 * /HelpAndSupport?categoryType_Division=OANDA_US&language=en_US
	 * @return a page reference to redirect to
	 */
	global PageReference redirect() {
		PageReference pr;

		// the page reference if always the home page
		pr = new PageReference(Site.getBaseUrl());
		pr.getParameters().put('language', language);
		pr.setRedirect(true);

		return pr;
	}
}