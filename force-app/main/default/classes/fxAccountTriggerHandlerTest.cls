@isTest(seeAllData=false)
public class fxAccountTriggerHandlerTest 
{
    final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);
    static final Id systemUserId = UserUtil.getUserIdByName('System User');

    @TestSetup
    static void initData()
    {
        User testuser1 = UserUtil.getRandomTestUser();    

        TestDataFactory testHandler = new TestDataFactory();

        Country_Setting__c[] can = new Country_Setting__c[]
        {
            new Country_Setting__c(Name='Canada', Group__c='Canada', ISO_Code__c='ca', Region__c='North America', Zone__c='Americas', Risk_Rating__c='LOW'),
            new Country_Setting__c(Name='Pakistan', Group__c='Asia', ISO_Code__c='pk', Region__c='Asia', Zone__c='Asia', Risk_Rating__c='PROHIBITED')
        };
        insert can;

        Settings__c settings = new Settings__c(Name='Default', Enable_CRA_Calculation__c = true);
        insert settings;

		Account account = testHandler.createTestAccount();
        Account introducingBroker = testHandler.createTestIntroducingBroker('test1', 'test1@test.com');
        Account introducingBroker2 = testHandler.createTestIntroducingBroker('test2','test2@test.com');
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;
        
        Opportunity opp = new Opportunity(
            Name = 'test opp',
            StageName = 'test stage',
            CloseDate = Date.today(),
            AccountId = account.Id,
        	OwnerId = testuser1.Id);
        insert opp;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Introducing_Broker__c = introducingBroker.Id;
      	fxAccount.Contact__c = contact.Id;
        fxAccount.Opportunity__c = opp.Id;
    	fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
    	fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
      	fxAccount.Division_Name__c = 'OANDA Asia Pacific';
      	fxAccount.Employment_Status__c = 'Unemployed';
      	fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.Singapore_Financial_Education__c = 'Management';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 1234324;
        fxAccount.fxTrade_One_Id__c = '123456';
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
    	insert fxAccount;
        
    }

    @IsTest
    static void validateClientStatusNotify()
    {
        fxAccount__c liveAccount = [Select Id, Approved_to_Fund__c FROM fxAccount__c LIMIT 1];
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        liveAccount.Approved_to_Fund__c = Datetime.now();
        update liveAccount;
        
        Test.getEventBus().deliver();
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
        for(AsyncApexJob job : jobs) {
            Assert.areEqual('Completed', job.Status, 'The job should be completed.');
            Assert.areEqual(0, job.NumberOfErrors, 'There should be no errors.');
        }
    } 

    @IsTest
    static void validateCustomerRiskAssessmentChangeTest()
    {
        fxAccount__c fxa = [SELECT Id, Citizenship_Nationality__c, Customer_Risk_Assessment_Text__c FROM fxAccount__c LIMIT 1];

        Test.startTest();
        fxa.Citizenship_Nationality__c = 'Pakistan';
		update fxa;
        Test.stopTest();

        Case[] craChangeCases = [Select Id From Case where fxAccount__c = :fxa.Id and subject = 'CRA Change'];
		system.assertEquals(1, craChangeCases.size());
    }
	
    @IsTest
    static void validateCustomerKnowledgeAssesmentTest()
    {
        fxAccount__c fxa = [SELECT Id,Singapore_Financial_Education__c,OAP_CKA_Result__c FROM fxAccount__c LIMIT 1];

        Test.startTest();
        fxa.Singapore_Financial_Education__c = 'none';
		update fxa;
        Test.stopTest();

        Case[] ckaCases = [Select Id From Case where fxAccount__c = :fxa.Id and subject = 'Non-resident PIU fail'];
		system.assertEquals(1, ckaCases.size());
    }
	

    @IsTest
    static void checkCoreFxAccountTest()
    {
        fxAccount__c fxa = [SELECT Id, Account__r.Date_Customer_Became_Core__c, Trade_Volume_USD_MTD__c FROM fxAccount__c LIMIT 1];
        system.assertEquals(null, fxa.Account__r.Date_Customer_Became_Core__c);
        
        Test.startTest();
        fxa.Trade_Volume_USD_MTD__c = 10000000;
		update fxa;
        Test.stopTest();
        
        Account ac = [SELECT Date_Customer_Became_Core__c FROM Account where fxAccount__c =:fxa.Id  LIMIT 1];
        system.assertEquals(Date.today(), ac.Date_Customer_Became_Core__c);
    }
	
    @IsTest
    static void checkRiskToleranceTest()
    {
        fxAccount__c liveAccount = [select id, Risk_Tolerance_CAD__c from fxAccount__c limit 1];

        System.runAs(REGISTRATION_USER) 
        {
            Test.startTest();

            liveAccount.Risk_Tolerance_CAD__c = 100000;
            update liveAccount;

            Test.stopTest();

            System.assertEquals(1, ActionUtil.lastPublished.size());
        }

    } 

    @IsTest
    static void checkIntroducingBrokerChange()
    {
        fxAccount__c liveAccount = [select id, Risk_Tolerance_CAD__c from fxAccount__c limit 1];
        Account newIntroducingBroker = [SELECT Id FROM Account WHERE Introducing_Broker_Name__c = 'test2'];
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        liveAccount.Introducing_Broker__c = newIntroducingBroker.Id;
        update liveAccount;

        Test.getEventBus().deliver();
        Test.stopTest();


        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
        for(AsyncApexJob job : jobs) {
            Assert.areEqual('Completed', job.Status, 'The job should be completed.');
            Assert.areEqual(0, job.NumberOfErrors, 'There should be no errors.');
        }
    } 

    @IsTest
    static void UnassignOppsForRejectedAppsTest()
    {
        fxAccount__c liveAccount = [select id,Opportunity__r.ownerId from fxAccount__c limit 1];
        system.assertNotEquals(systemUserId, liveAccount.Opportunity__r.ownerId);

                
        Test.startTest();    
        liveAccount.Division_Name__c ='OANDA Europe';
        liveAccount.Knowledge_Answer_1__c = 'Buying Euros while simultaneously selling US Dollars';
        liveAccount.Knowledge_Answer_2__c = 'A CFD reflects the market movements of an asset, but the actual underlying asset is never owned';
        liveAccount.Knowledge_Answer_3__c = 'Your positions are closed out';
        liveAccount.Knowledge_Answer_4__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.Knowledge_Answer_5__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.Knowledge_Answer_6__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.Knowledge_Answer_7__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.Knowledge_Answer_8__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.Knowledge_Answer_9__c = 'When the market "jumps1" from one price to another price, leaving a "gap"';
        liveAccount.CKA_Last_User_Update__c =  datetime.now();
        liveAccount.Traded_Products_50_Times__c=true;
        liveAccount.Trading_Experience_Type__c ='0,201,202,203,204';
        liveAccount.Trading_Experience_Duration__c ='0,1,2,3,40';
        liveAccount.Knowledge_Result__c = 'Reject';
        update liveAccount;
        
        Test.stopTest();
        
        liveAccount = [select Id, Opportunity__r.ownerId from fxAccount__c limit 1];
        system.assertEquals(systemUserId, liveAccount.Opportunity__r.ownerId);
    }

    @IsTest
    static void checkAffiliateMapping()
    {
        Affiliate__c[] affiliates = new Affiliate__c[]{
           new Affiliate__c(Username__c = 'username',
                            Firstname__c = 'John',
                            LastName__c = 'Doe',
                            Company__c = 'Oanda Corp',
                            Division_Name__c = 'OANDA Global Markets',
                            Country__c = 'Macao',
                            Alias__c = 'doer',
                            Email__c = 'aff1@email.com',
                            Cellxpert_Partner_ID__c = 1234),
            new Affiliate__c(Username__c = 'username',
                            Firstname__c = 'John',
                            LastName__c = 'Doe',
                            Company__c = 'Oanda Corp',
                            Division_Name__c = 'OANDA Global Markets',
                            Country__c = 'Macao',
                            Alias__c = 'doer',
                            Email__c = 'aff2@email.com',
                            Cellxpert_Partner_ID__c = 1235)
        };
		insert affiliates;
        
        Lead l = new Lead(LastName='test lead', Email='j@example.org');
        insert l;
    
        fxAccount__c fxa = new fxAccount__c(Name='test fxAccount',
                                            Lead__c=l.Id,
                                            Cellxpert_Partner_ID__c = 1234);
        insert fxa;

        fxa = [select Affiliate__c from fxAccount__c where Id = :fxa.Id];
        system.assertEquals(affiliates[0].Id, fxa.Affiliate__c);

        fxa.Cellxpert_Partner_ID__c = 1235;
        update fxa;

        fxa = [SELECT Id,Affiliate__c FROM fxAccount__c  where Id = :fxa.Id];
        System.assertEquals(affiliates[1].Id, fxa.Affiliate__c);
    } 

    @isTest
    static void createEIDResultTest()
    {
        fxAccount__c liveAccount = [select id, Singpass_Flow__c, MyInfo_Residential_Address_Confirmed__c from fxAccount__c limit 1];

        EID_Result__c[] eidBefore = [SELECT Id, fxAccount__c FROM EID_Result__c WHERE fxAccount__c = :liveAccount.Id];
        System.assertEquals(0, eidBefore.size());

        System.runAs(REGISTRATION_USER) 
        {
            Test.startTest();

            liveAccount.Singpass_Flow__c = true;
            liveAccount.MyInfo_Residential_Address_Confirmed__c = true;
            update liveAccount;

            Test.stopTest();
        }

        EID_Result__c[] eidAfter = [SELECT Id, fxAccount__c FROM EID_Result__c WHERE fxAccount__c = :liveAccount.Id];
        System.assertEquals(1, eidAfter.size());
    }

    @isTest
    static void createIHSDiscrepanciesCaseTest()
    {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = new TestDataFactory().createFXTradeAccount(acc);
        Tax_Declarations__c td = new Tax_Declarations__c(
            fxAccount__c = fxAcc.Id
        );
        insert td;

        fxAcc.Tax_Declarations__c = td.Id;
        fxAcc.US_Shares_Trading_Enabled__c = true;
        update fxAcc;

        Case[] cases = [SELECT Id FROM Case WHERE Subject = 'W8/W9 discrepancies vs. SF records'];
        System.assertEquals(0, cases.size());

        Test.startTest();

        fxAcc.First_Name__c = 'Bob';
        update fxAcc;
        
        Test.stopTest();
        cases = [SELECT Id FROM Case WHERE Subject = 'W8/W9 discrepancies vs. SF records'];
        System.assertEquals(1, cases.size());
    }

    @IsTest
    static void validateNetWorthUpdateTest()
    {
        fxAccount__c fxa = [SELECT Id, Net_Worth_Value__c, Liquid_Net_Worth_Value__c FROM fxAccount__c LIMIT 1];

        Test.startTest();
        fxa.Liquid_Net_Worth_Value__c = fxa.Net_Worth_Value__c + 1;
        try{
            update fxa;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Error: Liquid Net Worth Value cannot be greater than Net Worth Value') ? true : false;
			System.AssertEquals(true, expectedExceptionThrown);
        }
        Test.stopTest();
        fxa = [SELECT Id, Net_Worth_Value__c, Liquid_Net_Worth_Value__c FROM fxAccount__c LIMIT 1];
        System.assertNotEquals(fxa.Net_Worth_Value__c + 1, fxa.Liquid_Net_Worth_Value__c);
    }

    @isTest
    static void createCorporateTaxDeclarationReviewCaseTest() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = new TestDataFactory().createFXTradeAccount(acc);

        Case[] cases = [SELECT Id FROM Case WHERE Subject = 'Corporate Tax Declaration Review'];
        System.assertEquals(0, cases.size());

        Test.startTest();
        fxAcc.W8_W9_Status__c = 'interviewManualReview';
        update fxAcc;
        Test.stopTest();

        cases = [SELECT Id FROM Case WHERE Subject = 'Corporate Tax Declaration Review'];
        System.assertEquals(1, cases.size());
    }

    	
    @IsTest
    static void setAccountsToCloseOnlyTest()
    {
        fxAccount__c fxa = [SELECT Id,Conditionally_Approved__c,Division_Name__c FROM fxAccount__c LIMIT 1];
        Integer futuresNumber = Limits.getFutureCalls();
        Test.startTest();
        fxa.Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS;
        fxa.Conditionally_Approved__c = !fxa.Conditionally_Approved__c;
		update fxa;
        Integer futuresNumberAfter = Limits.getFutureCalls();
        Test.stopTest();
		system.assertEquals(1, futuresNumberAfter-futuresNumber);
    }

    @IsTest
    static void checkFxAccountUserFieldChangesTest() {
        fxAccount__c liveAccount = [
                SELECT Id, Birthdate__c, Is_2FA_Required__c, Last_Name__c, Language_Preference__c, Employment_Status__c,
                        Has_Opted_Out_Of_Email__c, PEFP__c, Risk_Tolerance_Amount__c, PEFP_Details__c, Introducing_Broker_Number__c,
                        Street__c, Phone__c, Industry_of_Employment__c, Income_Source_Details__c
                FROM fxAccount__c
                WHERE fxTrade_User_Id__c != null
                LIMIT 1];
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());
        liveAccount.Birthdate__c = Date.today().addDays(2);
        liveAccount.Is_2FA_Required__c = true;
        liveAccount.Last_Name__c = '';
        liveAccount.Language_Preference__c = 'English';
        liveAccount.Employment_Status__c = 'Employed';
        liveAccount.Has_Opted_Out_Of_Email__c = true;
        liveAccount.PEFP__c = true;
        liveAccount.Risk_Tolerance_Amount__c = 10.00;
        liveAccount.PEFP_Details__c = 'details';
        liveAccount.Introducing_Broker_Number__c = '1';
        liveAccount.Street__c = 'street';
        liveAccount.Phone__c = '1234';
        liveAccount.Industry_of_Employment__c = 'Agricultural';
        liveAccount.Income_Source_Details__c = 'Employment Income';

        Test.startTest();
            update liveAccount;
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
    }

    @IsTest
    static void checkGroupRecalculationTest() {
        fxAccount__c liveAccount = [
                SELECT Professional_Trader__c, Division_Name__c
                FROM fxAccount__c
                WHERE fxTrade_One_Id__c != null
                LIMIT 1];
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());
        fxAccountAfterTriggerHandler.isTestRun = true;

        liveAccount.Professional_Trader__c = true;
        liveAccount.Division_Name__c = 'OANDA Europe';

        Test.startTest();
            liveAccount.Funnel_Stage__c = Constants.FUNNEL_RFF;
            update liveAccount;
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
    }

    @IsTest
    static void checkGroupRecalculationTestError() {
        fxAccount__c liveAccount = [
                SELECT Professional_Trader__c, fxTrade_One_Id__c
                FROM fxAccount__c
                WHERE fxTrade_One_Id__c != null
                LIMIT 1];
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());
        fxAccountAfterTriggerHandler.isTestRun = true;

        liveAccount.Professional_Trader__c = true;
        liveAccount.fxTrade_One_Id__c = '';
        liveAccount.Division_Name__c = 'OANDA Europe';

        Test.startTest();
            update liveAccount;
            Test.getEventBus().deliver();
        Test.stopTest();

        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(0, jobs.size());
    }
}