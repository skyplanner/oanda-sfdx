/**
 * @description       : batch class use to schedule for updating name credential token
 * @author            : OANDA
 * @group             :
 * @last modified on  : 20 May 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class UpdateTokenBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {

    @TestVisible
    private Map<String, String> tokensToUpdate = new Map<String, String>();
    private Boolean isRerun = false;
    private String query;

    public  List<Map<String, String>> errorLogs = new List<Map<String, String>>();
    public Integer newRescheduleInMin;
    private Integer numberOfExecutedRecord = 0;

    private static final Integer MIN_BEFORE_TOKEN_EXPIRE_DEFAULT_VALUE = 120;
    private static final Integer RESCHEDULE_IN_MINUTES_ERROR_DEFAULT_VALUE = 10;

    private final static String LOGGER_CATEGORY = 'Update Token Batch';
    private final static String LOGGER_MSG = 'Issue with get/update token(s)';

    /**
    * @description if the batch will call without any params, the process will run for all credentials listed in this constructor
    */
    public UpdateTokenBatch() {
        String credentialSuffix = Util.getValidCredentialSuffix();
        tokensToUpdate = new Map<String, String>{
                (Constants.USER_API_NAMED_CREDENTIALS_NAME + credentialSuffix).toLowerCase() => Constants.USER_API_NAMED_CREDENTIALS_NAME,
                (Constants.TAS_API_NAMED_CREDENTIALS_NAME + credentialSuffix).toLowerCase() => Constants.TAS_API_NAMED_CREDENTIALS_NAME
        };
    }

    /**
    * @param localCredentialName Name of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
    * @description for running batch with only one named credential
    * @examples
    * System.scheduleBatch(new UpdateTokenBatch(Constants.Tas_API_NAMED_CREDENTIALS_NAME), 'Update tokens', 0);
    * Database.executeBatch(new UpdateTokenBatch(Constants.USER_API_NAMED_CREDENTIALS_NAME));
    */
    public UpdateTokenBatch(String localCredentialName) {
        String credentialSuffix = Util.getValidCredentialSuffix();
        tokensToUpdate = new Map<String, String>{
                (localCredentialName + credentialSuffix).toLowerCase() => localCredentialName
        };
    }

    /**
    * @param localCredentialNames Names of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
    * ex.: new List<String> {'Tas_API', 'User_API'}
    * @description for running batch with only one named credential
    * @examples
    * System.scheduleBatch(new UpdateTokenBatch(new List<String> {'Tas_API', 'User_API'}), 'Update tokens', 0));
    * Database.executeBatch(new UpdateTokenBatch(new List<String> {'Tas_API', 'User_API'}));
    */
    public UpdateTokenBatch(List<String> localCredentialNames) {
        String credentialSuffix = Util.getValidCredentialSuffix();
        for (String localCredentialName : localCredentialNames) {
            tokensToUpdate.put((localCredentialName + credentialSuffix).toLowerCase(), localCredentialName);
        }
    }

    public Database.QueryLocator start(Database.BatchableContext context) {
        String defaultQuery = 'SELECT Id, DeveloperName, Endpoint FROM NamedCredential WHERE DeveloperName IN (\'' + String.join(tokensToUpdate.keySet(), '\', \'') + '\')';
        query = String.isBlank(query) ? defaultQuery : query;
        return Database.getQueryLocator(query);
    }

    /**
    * @description the method get from the list of provided tokens ond check minutes between now and token expiration date,
     * and compere this value with minBeforeTokenExpire (based on value from Named Credentials custom metadata, or default value if it's empty),
     * if this value less then minBeforeTokenExpire the method make a callout to get new tokon for each named credential separatly.
     * Because the method can work with list of named credentials the method collect newRescheduleInMin (min named credentials between records in list)
     * to the clossest token experation date to rerun yourself before based on that time
    */
    public void execute(Database.BatchableContext context, List<Object> records) {
        Named_Credential_Configuration__mdt namedCredentialConfiguration = Util.namedCredentialConfiguration;
        Integer minBeforeTokenExpire = getCorrectTimeInMin(namedCredentialConfiguration?.UTB_min_before_expire__c, MIN_BEFORE_TOKEN_EXPIRE_DEFAULT_VALUE);
        Integer rescheduleInMinError = getCorrectTimeInMin(namedCredentialConfiguration?.UTB_reschedule_in_min_error__c, RESCHEDULE_IN_MINUTES_ERROR_DEFAULT_VALUE);

        //The callouts are made in a loop because each API has its own endpoint for retrieving the token
        for (NamedCredential namedCredential : (List<NamedCredential>)records) {
            numberOfExecutedRecord++;
            Integer minBetweenTokenExpiration = 0;
            Integer rescheduleInMin;

            Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(tokensToUpdate.get(namedCredential.DeveloperName.toLowerCase()), Constants.PRINCIPAL_NAME_CREDENTIALS);
            if ((String)credentialResultMap.get('status') == Constants.STATUS_ERROR || credentialResultMap.get('credential') == null) {
                setRescheduleInMin(rescheduleInMinError);
                errorLogs.add(new Map<String, String>{
                        'namedCredential' => namedCredential.DeveloperName,
                        'status' => (String)credentialResultMap.get('status'),
                        'message' => (String)credentialResultMap.get('message')
                });
                continue;
            }

            ConnectApi.Credential credential = (ConnectApi.Credential) credentialResultMap.get('credential');
            minBetweenTokenExpiration = TokenHelper.getMinutesBetweenNowAndTokenExpirationDate(credential);

            if ((minBetweenTokenExpiration - minBeforeTokenExpire) > minBeforeTokenExpire) {
                setRescheduleInMin(minBetweenTokenExpiration - minBeforeTokenExpire);
            } else {
                TokenHelper.TokenHelperResult tokenHelperResult = TokenHelper.manageNamedCredentialToken(tokensToUpdate.get(namedCredential.DeveloperName.toLowerCase()), credential, true);
                if (tokenHelperResult.isSuccess) {
                    setRescheduleInMin(tokenHelperResult.minToTokenExpire - minBeforeTokenExpire);
                } else {
                    setRescheduleInMin(rescheduleInMinError);
                    errorLogs.add(new Map<String, String>{
                            'namedCredential' => namedCredential.DeveloperName,
                            'status' => tokenHelperResult.status,
                            'message' => tokenHelperResult.message
                    });
                }
            }
        }
    }

    public void finish(Database.BatchableContext context) {
        List<String> errorsToEmail = new List<String>();
        BatchApexErrorHandler.checkBatchErrors(isRerun, context);
        if (errorLogs.size() > 0) {
            Logger.error(
                    LOGGER_MSG,
                    LOGGER_CATEGORY,
                    JSON.serialize(errorLogs));
            errorsToEmail.add('Errors during code execution: ' + JSON.serialize(errorLogs));
        }

        if (numberOfExecutedRecord != tokensToUpdate.keySet().size()) {
            String error = 'The number of batch records started and the number of records completed varies. Check the UpdateTokenBatch configuration and named credential settings (access, name, etc.).';
            Logger.error(
                    LOGGER_MSG,
                    LOGGER_CATEGORY,
                    error);

            errorsToEmail.add('Errors during code execution: ' + error);
        }

        if (errorsToEmail.size() > 0) {
            String body = 'Job Id: ' + context.getJobId() + '\n\n';
            body += String.join(errorsToEmail, '\n');
            EmailUtil.sendEmail('salesforce@oanda.com', 'UpdateTokenBatchError(s)', body);
        }

        if (!isRerun && !Test.isRunningTest()) {
            System.scheduleBatch(
                    tokensToUpdate.keySet().size() > 1 ? new UpdateTokenBatch() :  new UpdateTokenBatch(tokensToUpdate.values()[0]),
                    'Update token for ' + (tokensToUpdate.keySet().size() > 1 ? ' APIs' : tokensToUpdate.values()[0]),
                    newRescheduleInMin == null || newRescheduleInMin < 0 ? RESCHEDULE_IN_MINUTES_ERROR_DEFAULT_VALUE : newRescheduleInMin,
                    50);
        }
    }

    //******************** Helpers ******************************

    private Integer getCorrectTimeInMin(Decimal numberFromCustomMetadata, Integer constantValue) {
        Integer result = constantValue;
        if (numberFromCustomMetadata != null && numberFromCustomMetadata > 0) {
            result = (Integer) numberFromCustomMetadata;
        }
        return result;
    }

    private void setRescheduleInMin(Integer rescheduleInMin) {
        newRescheduleInMin = newRescheduleInMin != null && newRescheduleInMin < rescheduleInMin
                ? newRescheduleInMin
                : rescheduleInMin;
    }

}