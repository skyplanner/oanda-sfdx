@isTest
private class BatchgOnboardingEIDTest {
    
    public static testMethod void testBatchStatusController() {
        TestDataFactory factory = new TestDataFactory();
        Account a = factory.createTestAccount();
        fxACCOUNT__C FX = factory.createFXTradeAccount(a);
        FX.Division_Name__c = 'OANDA Corporation';
        FX.EID_Pass_Count__c = 1;
        FX.Employment_Status__c ='Student';
        FX.Annual_Income__c  ='25_35K' ;// ancial advisor'or Employment_Job_Title__c ='Account manager' or Employment_Job_Title__c ='Portfolio manager' or Employer_Details__c = 'Trader' or Employer_Details__c ='Consultant' or Employer_Details__c ='Financial advisor'or Employer_Details__c ='Account manager' or Employer_Details__c ='Portfolio manager' or Self_Employed_Details__c = 'Trader' or Self_Employed_Details__c ='Consultant' or Self_Employed_Details__c ='Financial advisor'or Self_Employed_Details__c ='Account manager' or Self_Employed_Details__c ='Portfolio manager'   ) or (Employer_Name__c = 'Bank of America' or  Employer_Name__c = 'Wells Fargo' or Employer_Name__c = 'Citi Bank' or Employer_Name__c = 'Chase' or Employer_Name__c = 'JP Morgan' or Works_for_a_CFTC_NFA_member_org__c = true)) and  createddate= Today';
        UPDATE FX;
        //BatchOnboardingEID_AMPCases controller = new BatchOnboardingEID_AMPCases();
        Test.startTest();
        String query = 'SELECT Id,Division_Name__c,EID_Pass_Count__c,Intermediary__c,Works_for_a_CFTC_NFA_member_org__c, Contact__c ,Account__c,AMP_Job_Title_Change__c ,AMP_Net_Worth_Changed__c , AMP_Street_Changed__c   FROM fxAccount__c WHERE Division_Name__c = \'OANDA Corporation\' and EID_Pass_Count__c = 1 and (((Employment_Status__c =\'Student\' or Employment_Status__c =\'Unemployed\') and Annual_Income__c  >=\'25_35K\' ) or (Employment_Status__c =\'Retired \' and Annual_Income__c  >=\'100_250K\' ) or (Age__c <30  and Net_Worth_Value__c > 250000 )or (Employment_Job_Title__c = \'Trader\' or Employment_Job_Title__c =\'Consultant\' or Employment_Job_Title__c =\'Financial advisor\'or Employment_Job_Title__c =\'Account manager\' or Employment_Job_Title__c =\'Portfolio manager\' or Employer_Details__c = \'Trader\' or Employer_Details__c =\'Consultant\' or Employer_Details__c =\'Financial advisor\'or Employer_Details__c =\'Account manager\' or Employer_Details__c =\'Portfolio manager\' or Self_Employed_Details__c = \'Trader\' or Self_Employed_Details__c =\'Consultant\' or Self_Employed_Details__c =\'Financial advisor\'or Self_Employed_Details__c =\'Account manager\' or Self_Employed_Details__c =\'Portfolio manager\'   ) or (Employer_Name__c = \'Bank of America\' or  Employer_Name__c = \'Wells Fargo\' or Employer_Name__c = \'Citi Bank\' or Employer_Name__c = \'Chase\' or Employer_Name__c = \'JP Morgan\' or Works_for_a_CFTC_NFA_member_org__c = true)) and  createddate= Today';
        
        Id batchJobId = Database.executeBatch(new BatchOnboardingEID_AMPCases(query), 200);
        
        Test.stopTest();
        List<Case> caseInserted = [SELECT id from Case];
        System.debug('caseInserted'+caseInserted + '√caseInsertedsize'+caseInserted);
        System.assertEquals(1, caseInserted.size());
    }
}