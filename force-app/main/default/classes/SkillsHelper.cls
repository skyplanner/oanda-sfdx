/**
 * @File Name          : SkillsHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/5/2024, 11:58:30 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/17/2020   acantero     Initial Version
**/
public class SkillsHelper {

	public static String getAccountTypeSkill(ServiceRoutingInfo info) {
		if (info.isPracticeAccount) {
			return OmnichanelConst.PRACTICE_SKILL;
		}
		//else...
		if (info.isLiveAccount) {
			return OmnichanelConst.LIVE_SKILL;
		}
		//else...
		return null;
	}

	public static String getLanguageSkill(ServiceRoutingInfo info) {
		if (
			info.createdFromChat &&
			(!OmnichanelSettings.existsDefaultChatButtonId()) 
		) {
			return null;
		}
		//else...
		String result = null;
		if (String.isNotBlank(info.language)) {
			result = getSkillFromLanguage(info.language);
		}
		if (
			String.isBlank(result) &&
			String.isNotBlank(info.alternativeLanguage)
		) {
			result = getSkillFromLanguage(info.alternativeLanguage);
		}
		if (String.isBlank(result)) {
			result = getSkillFromLanguage(OmnichanelConst.ENGLISH_LANG_CODE);
		}
		return result;
	}

	public static String getSkillFromLanguage(String language) {
		if (String.isBlank(language)) {
			return null;
		}
		//else...
		String result = ServiceLanguagesManager.getSkillByLangCode(language);
		if (result == null) {
			String langCode = ServiceLanguagesManager.getLanguageCode(language);
			if (langCode != language) {
				result = ServiceLanguagesManager.getSkillByLangCode(langCode);
			}
		}
		return result;
	}

	public static String getRegionSkill(ServiceRoutingInfo info) {
		if (String.isBlank(info.division)) {
			return null;
		}
		//else...
		ServiceDivisionsManager divisionsMgr 
			= ServiceDivisionsManager.getInstance();
		String result = divisionsMgr.getDivisionSkillDevName(info.division);
		return result;
	}

	public static String getCustomerTypeSkill(ServiceRoutingInfo info) {
		if (info.relatedCustomerType == null) {
			return null;
		}
		String result = getSeniorAgentSkill(info);
		//else...
		if (String.isNotBlank(result)) { 
			return  result;
		}
		//else...
		if (info.isNewCustomer) {
			return OmnichanelConst.NEW_CUSTOMER_SKILL;
		}
		//else...
		if (info.isActiveCustomer) {
			return OmnichanelConst.ACTIVE_CUSTOMER_SKILL;
		}
		//else...
		if (info.isCustomer) {
			return OmnichanelConst.CUSTOMER_SKILL;
		}
		//else...
		return null;
	}

	public static String getInquiryNatureSkill(ServiceRoutingInfo info) {
		if (String.isBlank(info.inquiryNature)) {
			if (info.createdFromCase) {
				return OmnichanelConst.OTHER_SKILL;
			}
			//else...
			return null;
		}
		//else...
		ServiceQuestionsMapper mapper = 
            ServiceQuestionsMapper.createFromMappedValues(
				info.isLiveAccount,
				info.inquiryNature, 
				info.type
			);
        return mapper.skill;
	}

	public static String getCryptoSkill(ServiceRoutingInfo info) {
		String result = null;
		if (info.crypto == true) {
			result = OmnichanelConst.CRYPTO_SKILL;
		}
		return result;
	}

	public static String getSeniorAgentSkill(ServiceRoutingInfo info) {
		String result = null;
		if (String.isNotBlank(info.tier) && info.tier == OmnichannelCustomerConst.TIER_1) {
			result = OmnichanelConst.SENIOR_AGENT_SKILL;
		}
		return result;
	}

}