/**
 * @File Name          : ServiceSkillManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/2/2024, 12:37:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/24/2021, 3:25:52 PM   acantero     Initial Version
**/
public without sharing class ServiceSkillManager {

    public static String getSkillsSummary(Set<String> skillDevNameSet) {
        String result = '';
        if (
            (skillDevNameSet == null) ||
            skillDevNameSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<Service_Skill__mdt> serviceSkillList = [
            SELECT
                Skill_Code__c
            FROM
                Service_Skill__mdt
            WHERE
                Skill_Dev_Name__c IN :skillDevNameSet
        ];
        for(Service_Skill__mdt serviceSkillObj : serviceSkillList) {
            result += (serviceSkillObj.Skill_Code__c + ' ');
        }
        return result;
    }

    public static Map<String,String> getSkillCodeMapFromIds(Set<String> skillIdSet) {
        Map<String,String> result = new Map<String,String>();
        if (
            (skillIdSet == null) ||
            skillIdSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<Skill> skillList = [
            SELECT
                Id,
                DeveloperName
            FROM
                Skill
            WHERE
                Id IN :skillIdSet
        ];
        Map<String,String> skillDevNameMap = new Map<String,String>();
        Set<String> skillDevNameSet = new Set<String>();
        for(Skill skillObj : skillList) {
            skillDevNameMap.put(
                skillObj.Id, 
                skillObj.DeveloperName
            );
            skillDevNameSet.add(skillObj.DeveloperName);
        }
        Map<String,String> skillCodeMapFromDevNames = 
            getSkillCodeMapFromDevNames(skillDevNameSet);
        for(String skillId : skillDevNameMap.keySet()) {
            String skillDevName = skillDevNameMap.get(skillId);
            String skillCode = skillCodeMapFromDevNames.get(skillDevName);
            if (String.isNotBlank(skillCode)) {
                result.put(
                    skillId, 
                    skillCode
                );
            }
        }
        return result;
    }

    public static Map<String,String> getSkillCodeMapFromDevNames(Set<String> skillDevNameSet) {
        Map<String,String> result = new Map<String,String>();
        if (
            (skillDevNameSet == null) ||
            skillDevNameSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<Service_Skill__mdt> serviceSkillList = 
            getServiceSkillList(skillDevNameSet);
        for(Service_Skill__mdt serviceSkillObj : serviceSkillList) {
            result.put(
                serviceSkillObj.Skill_Dev_Name__c, 
                serviceSkillObj.Skill_Code__c
            );
        }
        return result;
    }

    public static Set<String> getSkillCodeSetFromDevNames(Set<String> skillDevNameSet) {
        Set<String> result = new Set<String>();
        if (
            (skillDevNameSet == null) ||
            skillDevNameSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<Service_Skill__mdt> serviceSkillList = 
            getServiceSkillList(skillDevNameSet);
        for(Service_Skill__mdt serviceSkillObj : serviceSkillList) {
            result.add(
                serviceSkillObj.Skill_Code__c
            );
        }
        return result;
    }

    static List<Service_Skill__mdt> getServiceSkillList(Set<String> skillDevNameSet) {
        if (
            (skillDevNameSet == null) ||
            skillDevNameSet.isEmpty()
        ) {
            return new List<Service_Skill__mdt>();
        }
        //else...
        List<Service_Skill__mdt> result = [
            SELECT
                Skill_Dev_Name__c,
                Skill_Code__c
            FROM
                Service_Skill__mdt
            WHERE
                Skill_Dev_Name__c IN :skillDevNameSet
        ];
        return result;
    }

    public static String getSkillCode(String skillDevName) {
        String result = [
            SELECT
                Skill_Code__c
            FROM
                Service_Skill__mdt
            WHERE
                Skill_Dev_Name__c = :skillDevName
        ]?.DeveloperName;
        return result;
    }

    public static Map<String,Decimal> getSkillPriorityByDevNames(Set<String> skillDevNameSet) {
        Map<String,Decimal> sillPriorityByDevNames = new Map<String,Decimal>();
       for (Service_Skill__mdt skillMtd : [
            SELECT
                Id,
                DeveloperName,
                Skill_Priority__c
            FROM
            Service_Skill__mdt
            WHERE
                DeveloperName IN :skillDevNameSet
        ]){
            Decimal priority = skillMtd.Skill_Priority__c == null ? 
                        OmnichanelConst.DEFAULT_SKILL_LEVEL: 
                        skillMtd.Skill_Priority__c;
            sillPriorityByDevNames.put(skillMtd.DeveloperName, priority);
        }
        return sillPriorityByDevNames;
    }

}