/**
 * Case-updating logic is as follows:
 * 1. Find case associated with current chat session
 * 2. Ensure the code entered during this session matches that on the associated fxAccount
 * 3. If the codes match (secondary verification for safety), set the field to true
 * 4. Update the case
 */
        
 global without sharing class NF_SetAuthenticationOnCase  implements nfchat.AIMethodService{
    global void doInvoke(String chatId) {       

        // Find the chat log
        nfchat__Chat_Log__c currentChatLog = [
            SELECT nfchat__Session_Id__c, nfchat__Email__c, Auth_Answer__c
            FROM nfchat__Chat_Log__c
            WHERE Id =: chatId
        ];

        // Get email for account lookup, and code to check
        String chatSessionEmail = currentChatLog?.nfchat__Email__c;
        String chatSessionAuthAnswer = currentChatLog?.Auth_Answer__c;

        // Find fxAccount based on email
        List<fxAccount__c> fxAccounts = [
            SELECT Id, Name, Email__c, Account__r.PersonEmail, Authentication_Code__c, Authentication_Code_Expires_On__c  
            FROM fxAccount__c
            WHERE Account__r.PersonEmail = :chatSessionEmail
        ];
        System.debug('>> NF_SetAuthentationOnCase: ACCOUNT FOUND: ' + fxAccounts);

        // Get case to update
        Case caseToUpdate = [ 
            SELECT id, CaseNumber, User_Authenticated__c
            FROM case
            WHERE nfchat__Chat_Log__r.Id = :chatId 
            LIMIT 1
        ];
        System.debug('>> NF_SetAuthentationOnCase: CASE FOUND: ' + caseToUpdate);

        if (caseToUpdate == null) {
            System.debug('>> NF_SetAuthenticationOnCase: NO CASE FOUND - ABORTING');
            return;
        }

        // Check for the account
        if (fxAccounts.size() > 0) {
            // Compare code on account and chat log
            Boolean codeMatch = ( fxAccounts[0].Authentication_Code__c == chatSessionAuthAnswer );
            
            // Update the case
            caseToUpdate.User_Authenticated__c = codeMatch;
            update caseToUpdate;

            System.debug('>> NF_SetAuthentationOnCase: CASE UPDATED: ' + caseToUpdate);
        }
    }
}