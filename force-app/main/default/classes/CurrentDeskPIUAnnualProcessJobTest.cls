/**
 * Tests: CurrentDeskPIUAnnualProcessJob, CurrentDeskPIUAnnualProcessBatch
 */
@isTest
private class CurrentDeskPIUAnnualProcessJobTest {

	@isTest
	private static void scheduleJob() {
		Test.startTest();
		CurrentDeskPIUAnnualProcessJob.scheduleJob(10);
		Test.stopTest();
	}
}