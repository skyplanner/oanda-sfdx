/**
 * Util class to manage "Action" platform event
 */
public with sharing class ActionUtil {

    // Actions types
    @testVisible
    static final String SYNC_TYPE = 'sync';
    @testVisible
    static final String GBG_TYPE = 'gbg';
    @testVisible
    static final String IHS_TYPE = 'ihs';
    // Add more types here...

    // Actions to publish
    List<Action__e> actions;
    // Actions published successfully
    List<Action__e> published;
    // Actions no published
    List<Action__e> noPublished;

    // Last published events 
    @testVisible
    private static List<Action__e> lastPublished;

    /**
     * Constructor
     */
    public ActionUtil() {
        actions = new List<Action__e>();
        published = new List<Action__e>();
        noPublished = new List<Action__e>();
    }

    /**
     * Publish sync action platform events
     */
    public void publishSyncAccActions(
        Set<Decimal> fxTradeUserIds)
    {
        for(Decimal fxTradeUserId :fxTradeUserIds)
            actions.add(
                new Action__e(
                    fxTrade_User_Id__c =
                        String.valueOf(fxTradeUserId),
                    Type__c = SYNC_TYPE));

        publish(actions);
    }

    /**
     * Publish gbg action platform events
     */
    public Set<Id> publishGbgActions(
        List<Document__c> docs)
    {
        Set<Id> docIdsPublished =
            new Set<Id>();
        actions =
            new List<Action__e>();

        for(Document__c doc :docs)
            actions.add(
                new Action__e(
                    fxTrade_User_Id__c =
                        String.valueOf(doc.fxTrade_User_ID__c),
                    Type__c = GBG_TYPE,
                    Parameter_1__c = doc.Id));

        publish(actions);

        for(Action__e action :published){
            docIdsPublished.add(
                action.Parameter_1__c);
        }

        return docIdsPublished;
    }

    /**
     * Publish IHS action platform events
     */
    public Set<Id> publishIhsActions(
        List<Document__c> docs)
    {
        Set<Id> docIdsPublished = new Set<Id>();
        actions = new List<Action__e>();

        for(Document__c doc :docs)
            actions.add(new Action__e(fxTrade_User_Id__c = String.valueOf(doc.fxTrade_User_Id_Formula__c),
                                    Type__c = IHS_Type,
                                    Parameter_1__c = doc.Id));

        publish(actions);

        for(Action__e action :published) 
        {
            docIdsPublished.add(action.Parameter_1__c);
        }

        return docIdsPublished;
    }

    /**
     * Publish action platform event
     */
    public void publishAction(
        Decimal fxTradeUserId,
        String aType,
        String param1)
    {
        publishAction(
            String.valueOf(fxTradeUserId),
            aType,
            param1);
    }

    /**
     * Publish action platform event
     */
    public void publishAction(
        String fxTradeUserId,
        String aType,
        String param1)
    {
        publish(
            new List<Action__e>{
                new Action__e(
                    fxTrade_User_ID__c = fxTradeUserId,
                    Type__c = aType,
                    Parameter_1__c = param1)});
    }

    /**
     * Get succeeded actions
     */
    public List<Action__e> getPublishedActions() {
        return published;
    }

     /**
     * Get fail actions
     */
    public List<Action__e> getFailedActions() {
        return noPublished;
    }

    /**
     * Publish action platform event
     */
    public void publish(
        List<Action__e> actions)
    {
        List<Database.SaveResult> rs =
            EventBus.publish(
                actions);
        
        processResult(actions, rs);
    }

    /**
     * Process publishing results
     */
    private void processResult(
        List<Action__e> actions,
        List<Database.SaveResult> rs)
    {
        for(integer i = 0; i < rs.size(); i++) {
            processResult(actions[i], rs[i]);
        }
    }

    /**
     * Process publishing results
     */
    private void processResult(
        Action__e action,
        Database.SaveResult r)
    {
        if (r.isSuccess()) {
            published.add(action);
        } else {
            noPublished.add(action);

            /**
             * We should do something 
             * different with these errors...
             */
            System.debug('publishAction failed');
            for (Database.Error error : r.getErrors()) {
                System.debug('Error returned: ' +
                    error.getStatusCode() + ' - ' +
                    error.getMessage());
            }
        }

        System.debug('Actions published: ' + published);
        System.debug('Actions no published: ' + noPublished);

        // Save last published events
        lastPublished = published;
    }

}