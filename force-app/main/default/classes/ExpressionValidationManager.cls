/**
 * @File Name          : ExpressionValidationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/12/2020, 12:51:12 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/16/2020   acantero     Initial Version
**/
public class ExpressionValidationManager {

    static MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();

    String currentKey;
    List<Pattern> currentPatternList;
    
    public ExpValError validateExpression(
        String val, 
        ExpressionValidationWrapper expVal
    ){
        if (expVal == null) {
            return null;
        }
        //else...
        if (String.isBlank(val)) {
            if (expVal.canBeBlankValue()) {
                return null;
            }
            //else...
            Boolean isConditional = expVal.canBeBlankWithConditions();
            System.debug('validateExpression -> isConditional: ' + isConditional);
            return new ExpValError(System.Label.MifidValMissingError, isConditional);
        }
        //else...
        if (expVal.mustBeBlankValue()) {
            return new ExpValError(System.Label.MifidValShouldBeBlankError);
        }
        //else...
        if (expVal.canBeADefaultValue()) {
            if (isAValidDefaultValue(val)) {
                return null;
            }
            //else...
            if (expVal.mustBeADefaultValue()) {
                return new ExpValError(System.Label.MifidValInvalidValueError);
            }
        }
        //else...
        String uppercaseValue = val.toUpperCase();
        if (!validateFormat(uppercaseValue, expVal)) {
            return new ExpValError(System.Label.MifidValPasspFormatError);
        }
        //else...
        String error = validateConsecutiveRepetitive(uppercaseValue, expVal);
        if (error != null) {
            return new ExpValError(error);
        }
        //else...
        if (expVal.apexValidation == true) {
            return getExpValError(invokeApexValidation(uppercaseValue, expVal));
        }
        return null;
    }

    @testVisible 
    ExpValError getExpValError(String errorMsg) {
        if (String.isBlank(errorMsg)) {
            return null;
        }
        //else...
        return new ExpValError(errorMsg);
    }

    @testVisible
    Boolean validateFormat(
        String val, 
        ExpressionValidationWrapper expVal
    ) {
        if (
            String.isBlank(expVal.validation1) ||
            (expVal.validation1 == MiFIDExpValConst.FORMAT_WILDCARD)
        ) {
            return true;
        }
        //else...
        Boolean result = true;
        preparePatternList(expVal.key, expVal.validation1);
        for(Pattern patternObj : currentPatternList) {
            if (patternObj.matcher(val).matches()) {
                System.debug('ok --> format: ' + patternObj.pattern() + ', val: ' + val);
                result = true;
                break;
            } else {
                System.debug('error --> format: ' + patternObj.pattern() + ', val: ' + val);
                result = false;
            }
        }
        return result;
    }

    @testVisible
    Boolean isAValidDefaultValue(String val) {
        Boolean result = false;
        List<String> values = valSettings.defaultValues;
        if (values != null) {
            for(String defaultVal : values) {
                result = (valSettings.defaultValuesCaseSensitive == true)
                    ? defaultVal.equals(val)
                    : defaultVal.equalsIgnoreCase(val);
                if (result) {
                    return true;
                }
            }
        }
        return result;
    }

    @testVisible
    private String invokeApexValidation(
        String val, 
        ExpressionValidationWrapper expVal
    ) {
        String apexClassName = expVal.developerName + MiFIDExpValConst.APEX_VALIDATOR_SUFFIX;
        Type apexClassType = Type.forName(apexClassName);
        if (apexClassType == null) {
            throw new ExpressionValidatorException('APEX class "' + apexClassName + '" not found');
        }
        //else...
        ExpressionValidator validator = (ExpressionValidator)apexClassType.newInstance();
        try {
            return validator.validate(val);
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            return System.Label.MifidInternalValidationError;
        } 
    }

    @testVisible
    private String validateConsecutiveRepetitive(
        String val, 
        ExpressionValidationWrapper expVal
    ) {
        Boolean checkPrefix = true;
        String toValidate = val;
        if (expVal.checkOnlyValidFragments == true) {
            Boolean checkNumbers = 
                (expVal.checkConsecutiveNumbers == true) || 
                (expVal.checkRepetitiveNumber == true);
            Boolean checkLetters = 
                (expVal.checkConsecutiveLetters == true) || 
                (expVal.checkRepetitiveLetter == true);
            if (checkNumbers && (!checkLetters)) {
                toValidate = ExpressionValidationUtil.extractNumber(val, expVal.prefixSize);
                checkPrefix = false;
                //...
            } else if (checkLetters && (!checkNumbers)) {
                toValidate = ExpressionValidationUtil.extractLetters(val, expVal.prefixSize);
                checkPrefix = false;
            }
        } 
        ExpressionValidationUtil valUtil = new ExpressionValidationUtil(
            toValidate,
            expVal,
            checkPrefix
        );
        if (valUtil.isValid) {
            return null;
        }
        //else...
        if (valUtil.isConsecutive) {
            return System.Label.MifidValPasspConsecutiveError + ' (' + valUtil.invalidPart + ')';
        }
        if (valUtil.isRepetitive) {
            return System.Label.MifidValPasspRepetitiveError + ' (' + valUtil.invalidPart + ')';
        }
        if (valUtil.hasInvalidChar) {
            return System.Label.MifidValInvalidCharError + ' (' + valUtil.invalidPart + ')';
        }
        if (valUtil.hasInvalidLength) {
            return System.Label.MifidValInvalidLengthError;
        }
        //else...
        return System.Label.MifidValInvalidValueError;
    }

    @testVisible
    private void preparePatternList(String key, String expressions) {
        if (currentPatternList == null) {
            currentPatternList = new List<Pattern>(); 
        }
        String oldKey = currentKey;
        currentKey = key;
        if (oldKey == key) {
            return;
        }
        //else...
        if (!currentPatternList.isEmpty()) {
            currentPatternList.clear();
        }
        if (String.isNotBlank(expressions)) {
            List<String> expList = SPTextUtil.parseMultilineText(expressions);
            for(String exp : expList) {
                if (String.isNotBlank(exp)) {
                    Pattern patternObj = Pattern.compile(exp);
                    if (patternObj != null) {
                        currentPatternList.add(patternObj);
                    }
                }
            }
        }
    }

    public class ExpValError {

        public final String errorMsg;
        public final Boolean isConditional;

        public ExpValError(String errorMsg) {
            this(errorMsg, false);
        }

        public ExpValError(String errorMsg, Boolean isConditional) {
            this.errorMsg = errorMsg;
            this.isConditional = isConditional;
        }

    }

}