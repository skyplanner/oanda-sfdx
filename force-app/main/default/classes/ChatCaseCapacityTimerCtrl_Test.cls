/**
 * @File Name          : ChatCaseCapacityTimerCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/21/2021, 10:16:11 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/21/2021, 5:43:47 PM   acantero     Initial Version
**/
@isTest
private without sharing class ChatCaseCapacityTimerCtrl_Test {

    @testSetup
    static void setup() {
        /* OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        ); */
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        OmnichanelRoutingTestDataFactory.createHvcAccountTestCase();
        //disable LiveChatTranscript Trigger
        SPLiveChatTranscriptTriggerHandler.enabled = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        insert chatList;
    }

    @isTest
    static void getWrapUpTime() {
        Test.startTest();
        Integer result = ChatCaseCapacityTimerCtrl.getWrapUpTime();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void getCaseWorkClosingInfo() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        AgentCapacityHelper.beginCaseClosing(
            userId, 
            caseId
        );
        Test.startTest();
        ChatCaseCapacityTimerCtrl.WorkClosingInfo result = 
            ChatCaseCapacityTimerCtrl.getCaseWorkClosingInfo(caseId);
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void beginChatClosing() {
        String chatId = [select Id from LiveChatTranscript limit 1].Id;
        String workId = null;
        Test.startTest();
        String result = ChatCaseCapacityTimerCtrl.beginChatClosing(
            chatId,
            workId
        );
        Test.stopTest();
        System.assert(String.isNotBlank(result));
    }

    @isTest
    static void beginCaseClosing() {
        String caseId = [select Id from Case limit 1].Id;
        Test.startTest();
        String result = 
            ChatCaseCapacityTimerCtrl.beginCaseClosing(caseId);
        Test.stopTest();
        System.assert(String.isNotBlank(result));
    }

    @isTest
    static void endWorkClosing() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        String closingObjId = 
            AgentCapacityHelper.beginCaseClosing(
                userId, 
                caseId
            );
        Integer beforeCount = [select count() from Agent_Work_Closing__c];
        Test.startTest();
        ChatCaseCapacityTimerCtrl.endWorkClosing(closingObjId);
        Test.stopTest();
        Integer afterCount = [select count() from Agent_Work_Closing__c];
        System.assertEquals(1, beforeCount);
        System.assertEquals(0, afterCount);
    }

    @isTest
    static void freeCaseCapacity() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        String closingObjId = 
            AgentCapacityHelper.beginCaseClosing(
                userId, 
                caseId
            );
        Test.startTest();
        ChatCaseCapacityTimerCtrl.freeCaseCapacity(
            caseId,
            closingObjId
        );
        Test.stopTest();
        Integer afterCount = [select count() from Agent_Work_Closing__c];
        System.assertEquals(0, afterCount);
        Case caseObj = [select Id, Agent_Capacity_Status__c from Case where Id = :caseId];
        System.assertEquals(OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, caseObj.Agent_Capacity_Status__c);
    }
    
}