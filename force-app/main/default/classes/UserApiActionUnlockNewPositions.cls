/**
 * User API 'Unlock New Positions' action manager class
 */
public class UserApiActionUnlockNewPositions extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        Id fxAccountId = getfxAccountId(record);
        EnvironmentIdentifier identifier = getfxTradeUserId(record);
        TasUserAccountPatchRequest.TasUserAccountPatchRequestWrapper w
            = TasUserAccountPatchRequest.getUnlockNewPositionsWrapper();
        TasV20AccountCallout tasCallout = new TasV20AccountCallout();
        tasCallout.updateAccountInfo(identifier, w.request);
        String action = getActionLabel(record);
        auditTrailManager.saveAuditTrail(
            identifier.id, fxAccountId, action, w.change);

        return action + ' action success!!!';
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        TasUserAccountGetResponse defaultInfo = getV20DefaultInfo(record);

        return defaultInfo?.defaultAccountConfig?.v20?.newPositionsLocked == true;
    }
}