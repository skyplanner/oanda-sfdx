/**
 * @File Name          : UpdateMessagingSessionFieldAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/16/2023, 3:23:55 AM
**/
public without sharing class UpdateMessagingSessionFieldAction { 

	@InvocableMethod(label='Set Messaging Session Field' callout=false)
	public static List<String> setMessagingSessionField(List<UpdateInfo> updateInfoList) { 
        List<String> result = new List<String> { null };

		if (
			(updateInfoList == null) ||
			updateInfoList.isEmpty()
		) {
			return result;
		}
		// else...
		new MessagingSessionUpdateManager().processUpdate(updateInfoList);
        result[0] = updateInfoList[0].fieldValue;
        return result;
	}
		
	// *******************************************************************

	public class UpdateInfo implements FieldValueSource {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'fieldName' required = true)
		public String fieldName;

		@InvocableVariable(label = 'fieldValue' required = false)
		public String fieldValue;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			Map<String, Object> result = new Map<String, Object>();
			result.put(fieldName, fieldValue);
			return result;
		}

	}

}