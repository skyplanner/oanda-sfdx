/**
 * @File Name          : MessagingSLAViolationNotificationBuilder.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/5/2024, 3:57:45 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/27/2024, 4:16:46 PM   aniubo     Initial Version
**/
public inherited sharing class MessagingSLAViolationNotificationBuilder extends SLAViolationNotificationBaseBuilder {
	public MessagingSLAViolationNotificationBuilder(
		SLAConst.SLAViolationType violationType
	) {
		super(violationType);
	}
	public override void buildData() {
		fieldsByPlaceholder = new Map<String, String>{
			Label.MilestoneTimePlaceholder => 'triggerTime',
			Label.ChatNumberPlaceholder => 'chatNumber',
			Label.InquiryNaturePlaceholder => 'inquiryNature',
			Label.DivisionPlaceholder => 'division',
			Label.TierPlaceholder => 'tier',
			Label.AgentNamePlaceholder => 'ownerName',
			Label.AccountLeadNamePlaceholder => 'accountName',
			Label.AccountLeadPlaceholder => 'sourceType'
		};
		placeholdersByViolationType = new Map<SLAConst.SLAViolationType, List<String>>{
			SLAConst.SLAViolationType.ANSWER => new List<String>{
				Label.ChatNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.AgentNamePlaceholder,
				Label.AccountLeadNamePlaceholder
			},
			SLAConst.SLAViolationType.BOUNCED => new List<String>{
				Label.ChatNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.AgentNamePlaceholder,
				Label.AccountLeadNamePlaceholder
			},
			SLAConst.SLAViolationType.ASA => new List<String>{
				Label.MilestoneTimePlaceholder,
				Label.ChatNumberPlaceholder,
				Label.TierPlaceholder,
				Label.AccountLeadPlaceholder,
				Label.AccountLeadNamePlaceholder
			}
		};
	}
}