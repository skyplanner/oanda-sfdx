/**
* Created by Neuraflash LLC on 27/November/23.
* Implementation : Chat BroadCast Message
* Details : 1. Test Class for NF_GetBroadCast.
* 
*/
@isTest
public class NF_GetBroadcastTest {
    
    @testSetup
    static void createTestData() {
        
        //Creating Broadcast message        
        List<Chat_Broadcast__c> cbList = new List<Chat_Broadcast__c>();
        
        Chat_Broadcast__c cb = new Chat_Broadcast__c (
            Name  = 'BROADCAST_MESSAGE_Test',
            Message__c = 'This is my test Message',
            Priority__c = 10,
            Active__c  = true);
        
        Chat_Broadcast__c cb1 = new Chat_Broadcast__c (
            Name  = 'BROADCAST_MESSAGE_Test2',
            Message__c = 'Priority 10 message',
            Priority__c = 10,
            Active__c  = true);
        
        Chat_Broadcast__c cb2 = new Chat_Broadcast__c (
            Name  = 'BROADCAST_MESSAGE_Test2',
            Message__c = 'Priortity 2 message',
            Priority__c = 2,// This message should appear and not the above one
            Active__c  = true);     
        
        cbList.add(cb);
        cbList.add(cb1);
        cbList.add(cb2);
        
        try{
            insert cbList;           
        }
        catch(Exception e){
            System.debug('Exception caught :::' +e.getMessage() + 'stack :::'+e.getStackTraceString());
        }        
		        
    }
    
    //Test when no broadcast message is exist
    @isTest
    static void test_NoBroadCastMessageExist(){
        
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
        string payload = '{"prompt": "BROADCAST_MESSAGE","broadcastName": "BROADCAST_LABEL_FROM_SALESFORCE"}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        //caseGenerationInstance.ReturnStringResponse(data);
        
		//Call the method
        NF_GetBroadcast n = new NF_GetBroadcast();
        List<String> resp = n.ReturnStringResponse(data);
        System.debug('resp 1:::'+resp);
        Assert.areEqual(0,resp.size(),'List size has to be Zero since no BroadCast message exist with Name BROADCAST_LABEL_FROM_SALESFORCE');
    }
    
    //Test when broadcast message available with the broadcastName
    @isTest
    static void test_broadCastMessageExist(){
        
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
        string payload = '{"prompt": "BROADCAST_MESSAGE","broadcastName": "BROADCAST_MESSAGE_Test"}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        //caseGenerationInstance.ReturnStringResponse(data);
        
		//Call the method
        NF_GetBroadcast n = new NF_GetBroadcast();
        List<String> resp = n.ReturnStringResponse(data);
        System.debug('resp 2:::'+resp);
        Assert.areEqual(1,1,'Has to be 1 as BroadCast message exist with Name BROADCAST_MESSAGE_Test');

    }    
    
    //Test for Priority
    @isTest
    static void test_messagePriority(){
        
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
        string payload = '{"prompt": "BROADCAST_MESSAGE","broadcastName": "BROADCAST_MESSAGE_Test2"}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        //caseGenerationInstance.ReturnStringResponse(data);
        
        //Call the method
        NF_GetBroadcast n = new NF_GetBroadcast();
        List<String> resp = n.ReturnStringResponse(data);
        System.debug('resp 3:::'+resp);
        Assert.areEqual('Priortity 2 message',resp[0],'Wrong Broadcast Message displayed');
        Assert.areEqual(1,1,'Should return only 1 broadcast message according to the priority');// lower number High priority
        
    }
    
}