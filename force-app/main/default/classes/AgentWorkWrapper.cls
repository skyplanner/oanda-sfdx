/**
 * @File Name          : AgentWorkWrapper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2021, 1:47:19 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/7/2020   acantero     Initial Version
**/
public without sharing class AgentWorkWrapper {

    public final String workId;

    @testVisible
    public String status {get; private set;}

    @testVisible
    public Decimal capacityWeight {get; private set;}

    @testVisible
    String skillsSummary;

    public AgentWorkWrapper(String workId) {
        this.workId = workId;
        this.capacityWeight = 0;
        if (String.isBlank(workId)) {
            return;
        }
        //else...
        List<AgentWork> workList = [
            SELECT
                Status,
                CapacityWeight
            FROM
                AgentWork
            WHERE
                Id = :workId
        ];
        if (!workList.isEmpty()) {
            this.status = workList[0].Status;
            this.capacityWeight = workList[0].CapacityWeight;
        }
    }

    public Boolean isAssigned() {
        return (status == OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED);
    }

    public String getSkillsSummary() {
        if (
            (skillsSummary == null) &&
            String.isNotBlank(workId)
        ) {
            Set<String> agentWorkSkills = SPChatUtil.getAgentWorkSkills(workId);
            skillsSummary = ServiceSkillManager.getSkillsSummary(agentWorkSkills);
        }
        return skillsSummary;
    }
}