/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-23-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiForeignResidence {
    public Integer country_id {get; set;}
    
    public String tax_id {get; set;}
}