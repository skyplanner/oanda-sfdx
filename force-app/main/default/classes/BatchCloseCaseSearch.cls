/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCloseCaseSearch implements
    Database.Batchable<sObject>, Database.AllowsCallouts,Database.RaisesPlatformEvents, BatchReflection {
    public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public List<String> filterList;

    public BatchCloseCaseSearch(String filter) {
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Case__c, Total_Hits__c ' +
        'FROM Comply_Advantage_Search__c ' +
        'WHERE ' +
        (String.isNotBlank(filter) ? filter : '');
    }

    public BatchCloseCaseSearch(String filter, List<String> filterList) {
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Case__c, Total_Hits__c ' +
        'FROM Comply_Advantage_Search__c ' +
        'WHERE ' +
        (String.isNotBlank(filter) ? filter : '');

        this.filterList = filterList;

        System.debug('query: ' + query);
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Case__c, Total_Hits__c ' +
        'FROM Comply_Advantage_Search__c  WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        System.debug('scope: ' + scope);
        Map<Id, Case> caseToUpdate = new Map<Id, Case>();
        List<Comply_Advantage_Search__c> searchToUpdate = new List<Comply_Advantage_Search__c>();

        for (Comply_Advantage_Search__c search : scope) {
            if (search.Case__c != null) {
                Case c = new Case(
                    Id = search.Case__c,
                    Status = 'Closed'
                );
                caseToUpdate.put(c.Id, c);
            }
        }

        if (!caseToUpdate.isEmpty()) {
            update caseToUpdate.values();
        }
    }

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}