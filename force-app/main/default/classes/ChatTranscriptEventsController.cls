/**
 * Created by akajda on 17/11/2023.
 */

public with sharing class ChatTranscriptEventsController {
    @AuraEnabled
    public static List<SObject> getTranscriptEvents(String recordId) {
        return [
                SELECT Time, Id, Name, Agent.Name, AgentId, Type, Detail, CreatedDate, LastModifiedDate, LastViewedDate,
                        LiveChatTranscriptId
                FROM LiveChatTranscriptEvent
                WHERE LiveChatTranscriptId =: recordId
        ];
    }
}