/* Name: LeadApiRestResource
 * Description : upsert demo/marketing lead or throw error
 * Author: Mikolaj Juras
 * Date : 2024 May 27
 */
@RestResource(urlMapping='/api/v1/lead')
global without sharing class LeadApiRestResource {
    /* SAMPLE PAYLOAD
        {
            "division_name":"OANDA Global Markets",
            "first_name":"Landry",
            "last_name":"Michael",
            "country":"Taiwan",
            "phone":"16476873088",
            "email":"mlandry+1234567@oanda.com",
            "funnel_stage":"Incomplete Application",
            "lead_source":"9sf"
        }

    */
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqBody = '';
        try {
            
            if(req.requestBody != null){
                reqBody = req.requestBody.toString().trim();
            }

            InputClass inputLead = (InputClass)JSON.deserializeStrict(reqBody, InputClass.class);
            
            if(string.isBlank(inputLead.email) || String.isBlank(inputLead.last_name)) {
                String respBody = 'Bad request no email/last name in body: ' + reqBody;
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 400;
                Logger.error(
                    'LeadApiRestResource failed',
                    '/api/v1/lead no email in body',
                    res.statusCode + ' ' + respBody);
                    return;
                }
            


            List<Lead> leads= [SELECT id, IsConverted from Lead WHERE Email = :inputLead.email LIMIT 1];
            Boolean leadExists = leads.size() == 1;


            if(leadExists && leads[0].IsConverted) {
                String respBody = 'Lead is already converted';
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 400;
                Logger.error(
                    'LeadApiRestResource failed',
                    '/api/v1/lead lead is converted',
                    res.statusCode + ' ' + respBody);
                    return;
            }
            Lead upsertedLead;
            if(leadExists && !leads[0].IsConverted || !leadExists) {
                upsertedLead = createLead(inputLead, leadExists ? leads[0].Id : null);
            }
            res.responseBody = Blob.valueOf('Record is upserted ' + upsertedLead);
            res.statusCode = 200; 
            return;

        } catch (Exception ex) { 
            String respBody = 'Something went wrong, with lead creation/ update. Body: ' + reqBody + 'Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                'LeadApiRestResource failed',
                '/api/v1/lead',
                res.statusCode + ' ' + respBody);
                return;
        }
    }

    private static Lead createLead(InputClass input, Id leadId) {
        
        
        Lead l = new Lead();
        l.Id = leadId;
        l.Division_Name__c = input.division_name;
        l.Email = input.email;
        l.FirstName = input.first_name;
        l.LastName = input.last_name;
        l.Country = input.country;
        l.Phone = input.phone;
        l.Funnel_Stage__c = input.funnel_stage;
        l.LeadSource = input.lead_source;
        SimpleCallouts sc = new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME);
        l.fxTrade_One_Id__c = sc.postOneIdByEmail(input.email);

        upsert l;
        return l;
    }

    global class InputClass {
        public string division_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string funnel_stage { get; set; }
        public string lead_source { get; set; }
    }

}