/*
 *	@Author : Deepak Malkani.
 *	Created Date : Jan 17 2017.
 *	Purpose : EID Results controller extension, used to display EID Reason Codes and text based on the EID Result providers.
*/
public with sharing class EIDResultsCtrlExt {

    //Decalaring variables
    private static EID_Result__c EIDResultRec {get; set; }
    private List<String> codes;
    
    //Constructor
    public EIDResultsCtrlExt(ApexPages.StandardController controller) {
        EIDResultRec = [SELECT id, Provider__c, Reason__c FROM EID_Result__c WHERE id =: (Id) controller.getId() LIMIT 1];
    }
    
    //Getter method used to fetch Reason Codes
    public List<EID_Reason_Codes__c> getReasonCodes() {
        return [SELECT ActionText__c, ReasonCode__c, ReasonText__c, Provider__c FROM EID_Reason_Codes__c WHERE ReasonCode__c IN : this.getCodes() AND Provider__c = : EIDResultRec.Provider__c];
    }
    
    //Getter method used to parse Reason Code test field and out 2 character codes in a list
    public List<String> getCodes(){
        codes = new List<String>();
        String reasonCode = EIDResultRec.Reason__c;
        List<String> reasonCodeList = new List<String>();
        if(EIDResultRec.Provider__c =='TruliooUS' && reasonCode != null){
                reasonCodeList = reasonCode.split(',');
                    codes.addALL(reasonCodeList);
            }
        else if(reasonCode != null){
            for(Integer i=0; i<= reasonCode.length(); i++){
                if(math.mod(i,2) == 0 && i != 0){
                    String code = reasonCode.substring(i-2,i);
                    codes.add(code);
                }
            }
        }
        return codes;
    }
}