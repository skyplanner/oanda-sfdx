@isTest
private class CampaignLeadTest {

    static testMethod Campaign_Lead__c getTestCompaignLead() {
        return new Campaign_Lead__c (
										Last_Name__c = 'test',
										First_Name__c = 'test',
										Email__c = 'test@test.com',
										City__c = 'Toronto',
										Country__c = 'Canada',
										Phone__c = '416-555-5555',
										Campaign_Name__c = 'testcampaign'
									);
    }
    
    static testMethod Campaign_Lead__c getReactivationLead() {
    	Campaign_Lead__c cl = getTestCompaignLead();
    	cl.Lead_Source__c = 'Reactivation';
    	cl.Task_Subject__c = 'April Reactivation';
    	cl.Task_Due_Date__c = Date.newInstance(2014, 4, 20);
    	cl.Queue_for_Task_Round_Robin__c = 'Australia Task Queue';
    	return cl;
    }
    
    static testMethod void testOneCampaignLead() {
        insert getTestCompaignLead();
    }
    
    static testMethod void testOneReactivationLead() {
        insert getReactivationLead();
    }
    
    static testMethod void testExistingOwnedLead() {
    	User u = UserUtil.getTestUser();
    	insert u;
    	
    	Lead l = new Lead(LastName='test', Email='test@test.com', OwnerId=u.Id);
    	insert l;
    	
    	Campaign_Lead__c cl = getTestCompaignLead();
    	insert cl;
    	l = [SELECT OwnerId FROM Lead WHERE Id IN (SELECT Lead__c FROM Campaign_Lead__c WHERE Id=:cl.Id)][0];
    	CampaignMember[] cm = [SELECT LeadId FROM CampaignMember WHERE LeadId=:l.Id];
    	
    	System.assertEquals(1, cm.size());
    	System.assertEquals(u.Id, l.OwnerId);
    }
    
    static testMethod void testDuplicateCampaignMember() {
    	User u = UserUtil.getTestUser();
    	insert u;
    	
    	Lead l = new Lead(LastName='test', Email='test@test.com', OwnerId=u.Id);
    	insert l;
    	System.debug('l: ' + l);
    	Campaign_Lead__c cl1 = getTestCompaignLead();
    	Campaign_Lead__c cl2 = getTestCompaignLead();
    	Database.insert(new List<Campaign_Lead__c> {cl1, cl2}, false);
    	
    	CampaignMember[] cm = [SELECT LeadId FROM CampaignMember WHERE LeadId=:l.Id];
    	
    	System.assertEquals(1, cm.size());
    }
    /*
    static testMethod void testDuplicateCampaignMemberSeparate() {
    	User u = UserUtil.getTestUser();
    	insert u;
    	
    	Lead l = new Lead(LastName='test', Email='test@test.com', OwnerId=u.Id);
    	insert l;
    	
    	Campaign_Lead__c cl = getTestCompaignLead();
    	insert cl;
    	
    	l = [SELECT OwnerId FROM Lead WHERE Id IN (SELECT Lead__c FROM Campaign_Lead__c WHERE Id=:cl.Id)][0];
    	CampaignMember[] cm = [SELECT LeadId FROM CampaignMember WHERE LeadId=:l.Id];
    	
    	System.assertEquals(1, cm.size());
    	System.assertEquals(u.Id, l.OwnerId);
    	
    	try {
    		cl.Id=null;
    		insert cl;
	    	cm = [SELECT LeadId FROM CampaignMember WHERE LeadId=:l.Id];
	    	System.assertEquals(1, cm.size());
    	} catch(Exception e) {
    		System.debug('The following exception has occurred: ' + e.getMessage());
    	}
    }
    
    static testMethod void testExistingOwnedOpportunity() {
    	User u = UserUtil.getTestUser();
    	insert u;
    	
    	Lead l = new Lead(LastName='test', Email='test@test.com', OwnerId=u.Id);
    	insert l;
    	
    	Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(l.id);
		
		lc.setConvertedStatus(LeadUtil.CONVERTED_LEAD_STATUS);
		
		Database.LeadConvertResult lcr = Database.convertLead(lc);

    	Campaign_Lead__c cl = getTestCompaignLead();
    	insert cl;
    	
    	cl = [SELECT Contact__c FROM Campaign_Lead__c WHERE Id=:cl.Id];
    	l = [SELECT OwnerId, ConvertedContactId, ConvertedOpportunityId FROM Lead WHERE ConvertedContactId IN (SELECT Contact__c FROM Campaign_Lead__c WHERE Id=:cl.Id)][0];
    	
    	CampaignMember[] cm = [SELECT ContactId FROM CampaignMember WHERE ContactId=:cl.Contact__c];
    	Opportunity o = [SELECT OwnerId FROM Opportunity WHERE Id=:l.ConvertedOpportunityId][0];
    	
    	System.assertEquals(1, cm.size());
    	System.assertEquals(u.Id, o.OwnerId);
    }
    
    static testMethod void testLeadOwnedByInactiveUser() {
    	User u = UserUtil.getTestUser();
    	
    }*/
}