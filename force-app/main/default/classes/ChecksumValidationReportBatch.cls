public with sharing class ChecksumValidationReportBatch implements Database.Batchable<sObject>, Database.Stateful, Database.RaisesPlatformEvents, BatchReflection {
    
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    List<String> queriedFields = new List<String> {'Citizenship_Nationality__c'};
    List<String> includedCountries;
    Boolean isUserExcluded;
    String DivisionName;
    String fxAccountRecordType;
    String fxAccountType = MiFIDExpValConst.FXACCOUNT_TYPE_INDIVIDUAL;
    Map<ID, FxAccountValidationError> aggrgatedResult;
    
    public ChecksumValidationReportBatch(List<String> includedCountries, Boolean isUserExcluded, String fxAccountRecordType, String divisionName) {
        this.includedCountries = includedCountries;
        this.isUserExcluded = isUserExcluded;
        this.fxAccountRecordType = fxAccountRecordType;
        this.DivisionName = divisionName;
    }
    
    public static void runBatchWithDefaultValues() {
        Database.executeBatch(new ChecksumValidationReportBatch(new List<String> {'Cyprus', 'Czech Republic', 'Liechtenstein', 'Lithuania', 'Netherlands', 'Portugal', 'Romania', 'Slovakia', 'Malta'}, false, MiFIDExpValConst.FXACCOUNT_REC_TYPE_LIVE, Constants.DIVISIONS_OANDA_EUROPE));
    }


	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {

        String whereStatement = 'Division_Name__c =:DivisionName AND RecordType.DeveloperName =:fxAccountRecordType AND Citizenship_Nationality__c IN :includedCountries AND Is_Excluded_User__c = :isUserExcluded';
        whereStatement += ' AND Type__c = :fxAccountType';
        if(isRerun){
            whereStatement = 'Id IN :ids';
        }
        String query = 'SELECT Id, {0} FROM fxAccount__c WHERE {1}';
        String formattedQuery = String.format(query, new List<String> {String.join(queriedFields, ', '), whereStatement});
        aggrgatedResult = new Map<ID, FxAccountValidationError>();
        return Database.getQueryLocator(formattedQuery);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        Set<Id> fxAccountIdList = new Set<Id>();
        for(SObject s : scope) {
            fxAccountIdList.add(s.Id);
        }
        Map<Id, fxAccount__c> fxAccountsMap = new Map<Id, fxAccount__c>([ SELECT  Id, Name, National_Personal_ID__c, Passport_Number__c, Citizenship_Nationality__c, Last_Trade_Date__c, Account__c, Division_Name__c, Contact__c,
                                                    ( SELECT Id, Valid__c, fxAccount__c, LastModifiedDate FROM MiFID_Validation_Results__r ORDER BY LastModifiedDate DESC LIMIT 1 )
                                                    FROM fxAccount__c  WHERE Id IN :fxAccountIdList ORDER BY Citizenship_Nationality__c ASC ]);

        FxAccountMIFIDValidator validator =  new FxAccountMIFIDValidator( fxAccountsMap.values(), false);
        Map<ID, FxAccountValidationError> results = validator.validateOnly();
        aggrgatedResult.putAll(results);   
    }

    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        sendEmail();
    }

    private void sendEmail(){

        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String currentUserEmail = [SELECT Email FROM User WHERE Id =: UserInfo.getUserId()].Email;
        String[] toAddresses = new list<string> { currentUserEmail };
        String subject ='Checksum Validation Report Result';
        String body = 'This is the result of the checksum validation report. \nDate time run : ' + DateTime.now() + '\n' + 'Result :\n';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        if(aggrgatedResult.size() > 0){
            Map<Id, fxAccount__c> fxAccountsMap = getFxAccountExtraFields(aggrgatedResult.keySet());
            body += 'A total of ' + aggrgatedResult.size() + ' Fx Accounts with errors were found. \n\nFind attached the report with the details of the errors found.';
            string header = 'Fx Account Record Id, Name, Citizenship, Last Trade Date, Subject, fxTrade User Id, Passport, Personal ID, Country, Funnel Stage, Error Details\n';
            string finalstr = header ;
            for(Id resId: aggrgatedResult.keySet()){
                FxAccountValidationError res = aggrgatedResult.get(resId);
                fxAccount__c fxAcc = res.fxAccountObj;
                fxAccount__c fxAccExtraFields = fxAccountsMap.get(resId);
                String errorMSg = res.error;
                errorMSg = errorMSg.replace('\n', ' ');
                errorMSg = errorMSg.replace(',', ' '  );
                string recordString = resId + ',' + fxAcc.Name + ',' + fxAcc.Citizenship_Nationality__c + ',' + fxAccExtraFields.Last_Trade_Date__c + ',' + res.subject + ',' + fxAccExtraFields.fxTrade_User_Id_Text__c + ',' + fxAcc.Passport_Number__c + ',' + fxAcc.National_Personal_ID__c + ',' + fxAccExtraFields.Country__c + ',' + fxAccExtraFields.Funnel_Stage__c  + ',' + errorMSg + '\n';
                finalstr = finalstr +recordString;
            }

            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(finalstr);
            string csvname= 'Checksum Validation Report.xls';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        } else {
            body += 'No errors found';
        }
        body += '\nCheers! \n\nThe Checksum Validation Report Robot';
        email.setPlainTextBody(body);
        
        
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

    }

    private Map<Id, fxAccount__c> getFxAccountExtraFields(Set<Id> recordIds){
        return new Map<Id, fxAccount__c> ([SELECT Id, Last_Trade_Date__c, Country__c, Funnel_Stage__c, fxTrade_User_Id_Text__c  FROM fxAccount__c WHERE Id IN :recordIds]);
    }
}