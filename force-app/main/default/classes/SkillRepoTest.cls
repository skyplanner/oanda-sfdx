/**
 * @File Name          : SkillRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/18/2024, 12:43:03 PM
**/
@IsTest
private without sharing class SkillRepoTest {

	@IsTest
	static void getByDevNames() {
		String englishSkill = OmnichanelConst.ENGLISH_SKILL;
		Set<String> skillNameSet = new Set<String> { englishSkill };

		Test.startTest();
		List<Skill> result = SkillRepo.getByDevNames(skillNameSet);
		Test.stopTest();
		
		Assert.isFalse(
			result.isEmpty(), 
			'The record linked to the "English" skill should have been found.'
		);
	}
	
}