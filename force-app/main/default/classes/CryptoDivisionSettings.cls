/**
 * @File Name          : CryptoDivisionSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/16/2022, 5:10:22 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/12/2022, 12:32:45 AM   acantero     Initial Version
**/
public without sharing class CryptoDivisionSettings {

    public static Boolean exists(String divisionCode) {
        List<Division_Settings__mdt> recList = [
            SELECT 
                DeveloperName
            FROM 
                Division_Settings__mdt
            WHERE
                DeveloperName = :divisionCode
            AND
                Spot_Crypto__c = true
            LIMIT 1
        ];
        return (!recList.isEmpty());
    }

    public static List<String> getDivisionCodeList() {
        List<String> result = new List<String>();
        List<Division_Settings__mdt> recList = [
            SELECT 
                DeveloperName
            FROM 
                Division_Settings__mdt
            WHERE
                Spot_Crypto__c = true
        ];
        for(Division_Settings__mdt divisionSettingsObj : recList) {
            result.add(divisionSettingsObj.DeveloperName);
        }
        return result;
    }
    
}