/* Name: CaseApiRestResource
 * Description : New Apex Class to handle help desk legasy system cases
 * Author: Mikolaj Juras
 * Date : 2023 December 05
 */
@RestResource(urlMapping='/api/v1/user/*/hd-case')
global with sharing class CaseApiRestResource {
    
    @testVisible
    private static Id CASE_HD_RECORD_TYPE = Case.SobjectType.getDescribe().getRecordTypeInfosByName().get('Help Desk Ticket').getRecordTypeId();

    @HttpPut
    global static void createCase() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try {
            String reqBody = '';
            if(req.requestBody != null){
                reqBody = req.requestBody.toString().trim();
            }

            String[] urlParts = RestContext.request.requestURI.split('/');
            String emailId = urlParts[4]; // Adjust the index based on your URL structure

            if (String.isBlank(emailId)) {
                res.statusCode = 500;
                res.responseBody = Blob.valueOf('Incorrect Request, bad url: ' + urlParts.toString() );
                return;
            }
            CaseData cd = (CaseData)JSON.deserialize(reqBody, CaseData.class);
            List<Account> relatedAccount = [SELECT id, fxAccount__c, fxAccount__r.Contact__c,(select Id from Cases where RecordTypeId=:CASE_HD_RECORD_TYPE) FROM Account WHERE PersonEmail =:emailId];
            Account currentAcc = relatedAccount.size() > 0 ? relatedAccount[0] : null;
            Case existingHDCase = currentAcc != null && !currentAcc.Cases.isEmpty() ? currentAcc.Cases[0] : null;
            
            Case newCase = new Case(
                Subject = cd.emailSubject,
                Description = cd.emailBody,
                RecordTypeId = CASE_HD_RECORD_TYPE,
                Status = cd.Status,
                Department__c = 'FX Trade Funds',
                OwnerId = UserUtil.getSystemUserId(),
                HD_Submitted_By__c = 'portal@oanda.com',
                CCM_CX__c = true,
                HD_Request_Type__c = 'FXTrade - Funds Notification',
                Origin = 'Email Outbound'

            );
            newCase.Id = existingHDCase != null ? existingHDCase.Id : null;
            newCase.AccountId = currentAcc != null ? currentAcc.Id : null;
            newCase.ContactId = currentAcc != null && currentAcc?.fxAccount__r?.Contact__c != null ? currentAcc.fxAccount__r.Contact__c : null;
            upsert newCase;

            res.statusCode = 200;
            res.responseBody = Blob.valueOf('Case is created/updated. Case Id: ' + newCase.Id + ' Case details: ' + newCase) ;

            return;
        } catch (Exception e) {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Case is not created. Error: ' + e.getMessage());
            return;
        }
    }

    public class CaseData {
        public String emailAddress;
        public String emailSubject;
        public String emailBody;
        public String status;

    }
}
/*
    Client Email will be sent as parameter to API call, Salesforce will look on the account and tag the HD Tickets to it.

    Fields on the case to fill:

    Status - Closed

    Submitted by: The User whom notification is sent.

    Department : FX Trade Funds

    Request Type: FX Trade Fund Notification

    Subject : Funding Transaction Notification

    Owner: blank or System user./ integration user.

    Subject : Funding Transaction Notification
    {
    "emailAddress": "samarnath+14marb@oanda.com",
    "emailSubject": "qui nulla",
    "emailBody": "",
    "status": "Open"
    }
*/