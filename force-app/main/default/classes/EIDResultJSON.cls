public class EIDResultJSON {
    
    public class Response {
        public Fraud_flag fraud_flag;
        public String first_name;
        public String last_name;
        public String dob;
        public String UnitNumber;
        public String BuildingNumber;
        public String StreetName;
        public String StreetType;
        public String City;
        public String PostalCode;
        public String StateProvinceCode;
        public String telephone;
        public String ssn;
    }
    
    public String name;
    public Response response;
    
    public class Fraud_flag {
        public Boolean status;
        public String flags;
    }
    
    
    public static List<EIDResultJSON> parse(String json) {
        return (List<EIDResultJSON>) System.JSON.deserialize(json, List<EIDResultJSON>.class);
    }
}