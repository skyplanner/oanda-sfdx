/**
 * @File Name          : ServiceCustomerTierCalculatorTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/20/2024, 11:09:59 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/25/2024, 9:19:44 AM   aniubo     Initial Version
 **/
@isTest
private class ServiceCustomerTierCalculatorTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		// setup code
		ServiceTestDataFactory.createLead1(initManager);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createLeadCase1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	@isTest
	private static void testTier4IsLeadOrFxAccountIsBlank() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			null, // region 
			OmnichannelCustomerConst.SEGMENTATION_INACTIVE // segPL
		);
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(new SCustomerTypeInfo());

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4,
			tier,
			'Tier must be TIER_4 because fxAccountId is blank'
		);
		tier = new ServiceCustomerTierCalculator().getTier(customerTypeInfo);

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4,
			tier,
			'Tier must be TIER_4 because the customer is a lead'
		);
		Test.stopTest();
	}

	@isTest
	private static void testTier2_OGM_NoHVC() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_GLOBAL_MARKETS, // region
			OmnichannelCustomerConst.SEGMENTATION_INACTIVE // segPL
		);
		customerTypeInfo.isALead = false;

		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_2,
			tier,
			'Tier must be TIER_2 because the customer is a OGM'
		);
	}

	@isTest
	private static void testTier1_OGM_IsHVC() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_GLOBAL_MARKETS, // region
			OmnichannelCustomerConst.SEGMENTATION_INACTIVE // segPL
		);
		customerTypeInfo.isALead = false;
		customerTypeInfo.isHVC = true;
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_1,
			tier,
			'Tier must be TIER_1 because the customer is a OGM and isHVC'
		);
	}

	@isTest
	private static void testTier4_Inactive() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_INACTIVE // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4,
			tier,
			'Tier must be TIER_4 because segmentation.Seg_PL__c = Inactive'
		);
	}

	@isTest
	private static void testTier4_Blank() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_BLANK // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4,
			tier,
			'Tier must be TIER_4 because segmentation.Seg_PL__c = Blank'
		);
	}

	@isTest
	private static void testTier4_Seg_PL_IsNull() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			null // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4,
			tier,
			'Tier must be TIER_4 because segmentation.Seg_PL__c = Null'
		);
	}

	@isTest
	private static void testTier3_Seg_PL_Low() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_LOW // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_3,
			tier,
			'Tier must be TIER_3 because segmentation.Seg_PL__c = low'
		);
	}

	@isTest
	private static void testTier1_Seg_PL_High() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_HIGH // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_1,
			tier,
			'Tier must be TIER_1 because segmentation.Seg_PL__c = high'
		);
	}

	@isTest
	private static void testTier1_Seg_PL_HighPlus() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_HIGH_PLUS // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_1,
			tier,
			'Tier must be TIER_1 because segmentation.Seg_PL__c = high+'
		);
	}

	@isTest
	private static void testTier2_Seg_PL_Mid() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SCustomerTypeInfo customerTypeInfo = createCustomerTypeInfo(
			initManager,
			OmnichanelConst.OANDA_CANADA_AB, // region
			OmnichannelCustomerConst.SEGMENTATION_MID // segPL
		);
		customerTypeInfo.isALead = false;
		
		Test.startTest();

		String tier = new ServiceCustomerTierCalculator()
			.getTier(customerTypeInfo);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.TIER_2,
			tier,
			'Tier must be TIER_1 because segmentation.Seg_PL__c = Mid'
		);
	}

	private static SCustomerTypeInfo createCustomerTypeInfo(
		SPDataInitManager initManager,
		String region,
		String segPL
	) {
		SCustomerTypeInfo customerTypeInfo = new SCustomerTypeInfo();

		customerTypeInfo.isALead = true;
		customerTypeInfo.fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		customerTypeInfo.division = region;
		customerTypeInfo.segPL = segPL;

		return customerTypeInfo;
	}
}