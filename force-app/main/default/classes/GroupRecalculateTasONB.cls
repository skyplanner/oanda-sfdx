public with sharing class GroupRecalculateTasONB extends OutgoingNotificationBus{
    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        //After completely replacing the old logic, the validation fields can be moved here.

        if (String.isBlank(((fxAccount__c) newRecord).fxTrade_One_Id__c)) {
            addEventsError(newRecord.Id, 'GroupRecalculateTasONB - missing fxTrade_User_Id__c');
            return null;
        }
        if (String.isBlank(fxAccountUtil.getEnvVariable((fxAccount__c) newRecord))) {
            addEventsError(newRecord.Id, 'GroupRecalculateTasONB - RT do not match' + ((fxAccount__c) newRecord).RecordTypeId);
            return null;
        }
        return '{}';
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize(new Map<String, String> {
                'env' => fxAccountUtil.getEnvVariable((fxAccount__c)newRecord),
                'recordId' =>  ((fxAccount__c) newRecord).Id,
                'tradeOneId' => ((fxAccount__c) newRecord).fxTrade_One_Id__c
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        new SimpleCallouts(Constants.TAS_API_NAMED_CREDENTIALS_NAME).updateGroupRecalculateTasApi(
                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeOneId')),
                body);
//        new TasMt5AccountCallout().updateGroupRecalculateTasApi(
//                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeOneId')),
//                body);
    }

    public override String getRecordId() {
        return recordId;
    }
}