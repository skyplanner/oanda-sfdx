/**
 * @File Name          : MessagingSessionRelatedEntity.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/8/2024, 1:54:07 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 1:53:13 PM   aniubo     Initial Version
**/
public inherited sharing class MessagingSessionRelatedEntity {
	public Id recordId { get; set; }
	public Boolean isAccountType { get; set; }
	public MessagingSessionRelatedEntity(id recordId, Boolean isAccountType) {
		this.recordId = recordId;
		this.isAccountType = isAccountType;
	}
}