/**
 * @File Name          : LiveChatCustomWindowCtrl.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/31/2021, 2:27:58 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    5/8/2020   dmorales     Initial Version
 **/
public without sharing class LiveChatCustomWindowCtrl {

	public Boolean isEnglish {set; get;}
	public String chatLanguage {set; get;}

	// public Long avgWaitTime {
	// 	set;
	// 	get{
    // 		return generateWaitTime();
	// 	}
	// }

public LiveChatCustomWindowCtrl() {
	isEnglish = false;
	Cookie chatLanguageCookie = ApexPages.currentPage().getCookies().get(
		ChatConst.CHAT_LANGUAGE_COOKIE
	);
	if (chatLanguageCookie != null) {
		chatLanguage = chatLanguageCookie.getValue();	
		isEnglish = (chatLanguage == ChatConst.DEFAULT_LANGUAGE);
	}
	else {
		chatLanguage = ChatConst.DEFAULT_LANGUAGE;
	}
}

// public Long generateWaitTime()
// {
//     Integer limitQuery = 250;
// 	OmnichanelSettings omniSettings = OmnichanelSettings.getRequiredDefault(); 
	
//     if (!omniSettings.initializationFails) {
// 		limitQuery = omniSettings.limitAvgWaitTime > 0 ?
// 		             omniSettings.limitAvgWaitTime : 250; 
//     }

// 	List<AgentWork> agentWorkList = [SELECT Id, SpeedToAnswer, AssignedDateTime, AcceptDateTime
// 	                                 FROM AgentWork
// 	                               //  WHERE AcceptDateTime != NULL	                                 
// 	                                 WHERE SpeedToAnswer != NULL	                                 
// 	                                 AND SpeedToAnswer < 10000	                                 
// 	                                 ORDER BY CreatedDate DESC
// 	                                 LIMIT :limitQuery];

// 	Integer count = 0;
// 	Long waitTimeTotal = 0;
    

// 	for(AgentWork aw : agentWorkList)
// 	{
// 		waitTimeTotal += aw.SpeedToAnswer;
//     	//Long substract = (aw.AcceptDateTime.getTime() - aw.AssignedDateTime.getTime())/1000;
// 		//waitTimeTotal += substract;
// 		count++;
// 	}

// 	Long avgWaitTime = 0;
// 	if( count > 0 )
// 	{
// 		avgWaitTime = waitTimeTotal/count;
// 	}
//     return avgWaitTime;
//  }
 
}