/* Name: BatchOnboardingAMPFollowUp
* Description : Batch Class to create cases  for Onboarding AMP follow up 
* Documentation: Please, use this document as a reference on how to run this batch
*   https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
* Author: Sahil Handa
* */
public class BatchOnboardingAMPFollowUp implements
    Database.Batchable<sObject>,Database.RaisesPlatformEvents, BatchReflection,
    database.stateful, Schedulable{
    
    // Query to pickup the right fxAccounts
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    
    public static final Integer BATCH_SIZE = 200;
	public static final String CRON_SCHEDULE = '0 0 9 * * ?';
    public static final String CRON_NAME =
        'BatchOnboardingEID_AMPCases';
    private static final String QUEUE_NAME =
        'OC_AMP_follow_ups';
    @testVisible
    private static final String CASE_SUBJECT =
        'OANDA fxtrade account: Information required';
    @testVisible
    private static final String CASE_DESCRIPTION =
        'OANDA fxtrade account: Information required';
    @testVisible
    private static final String SUBTYPE_ADDRESS_CONFIRMATION =
        'Address Confirmation';
    @testVisible
    private static final String SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS =
        'Employment/Financial details';

    /**
     * Constructor
     */
    public BatchOnboardingAMPFollowUp(String q) {
        query = q;
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id,Division_Name__c,EID_Pass_Count__c,Contact__c,Intermediary__c,Works_for_a_CFTC_NFA_member_org__c,Account__c,Account__r.Id,AMP_Job_Title_Change__c ,AMP_Net_Worth_Changed__c , AMP_Street_Changed__c, AMP_Industry_Employment_Changed__c FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    /****************** Schedulable ******************/

    
    /**
     * Schedulable schedule method 
     */
    public static void schedule(String q) {
    	System.schedule(
            CRON_NAME,
            CRON_SCHEDULE,
            new BatchOnboardingEID_AMPCases(q));
  	}

    /**
     * Schedulable execute method
     */
    public void execute(SchedulableContext context) {
    	Database.executeBatch(
            new BatchOnboardingAMPFollowUp(query),
            BATCH_SIZE);
  	}


    /****************** BATCHABLE ******************/


    /**
     * Batch start method
     */
    public Database.querylocator start(
        Database.BatchableContext BC)
    {
        return
            Database.getQueryLocator(query);
    }
    
    /**
     * Batch execute method
     */
    public void execute(
        Database.BatchableContext BC,
        List<fxAccount__c> fxas)
    {
        try {
            process(fxas);
        } catch(Exception ex) {
            Logger.exception(
                Constants.LOGGER_ONBOARDING_AMP_FOLLOWUP_CATEGORY,
                ex);
        }
    }

    /**
     * Batch finish method
     */
    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
    

    /****************** PRIVATE ******************/


    /**
     * Process to do when batch run
     */
    private void process(
        List<fxAccount__c> fxas)
    {
        Group queue =
            SPQueueUtil.getQueueByDevName(
                QUEUE_NAME,
                true);

        List<Case> caseToBeInserted =
            new List<Case>();
        
        for(fxAccount__c fxAccount :fxas) {
            if(fxAccount.AMP_Street_Changed__c) {
                caseToBeInserted.add(
                    createCase(
                        queue,
                        fxAccount,
                        SUBTYPE_ADDRESS_CONFIRMATION));
            }
            
            if(fxAccount.AMP_Net_Worth_Changed__c) {
                caseToBeInserted.add(
                    createEmploymentFinancialCase(
                        queue,
                        fxAccount));
            }

            if(fxAccount.AMP_Job_Title_Change__c) {
                caseToBeInserted.add(
                    createEmploymentFinancialCase(
                        queue,
                        fxAccount));
            }   
            
            if(fxAccount.AMP_Industry_Employment_Changed__c) {
                caseToBeInserted.add(
                    createEmploymentFinancialCase(
                        queue,
                        fxAccount));
            }
        }
        
        // Insert cases
        DataBase.SaveResult[] saveResults =
            Database.insert(
                caseToBeInserted,
                false);

        // Log dml errors
        Logger.error(
            Constants.LOGGER_ONBOARDING_AMP_FOLLOWUP_CATEGORY,
            saveResults);
    }

      /**
     * Create AMP Employment Financial FollowUp case
     */
    private Case createEmploymentFinancialCase(
        Group queue,
        fxAccount__c fxAccount)
    {
        return createCase(
            queue,
            fxAccount,
            SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS);
    }

    /**
     * Create AMP FollowUp case
     */
    private Case createCase(
        Group queue,
        fxAccount__c fxAccount,
        String sub)
    {
        return
            new Case(
                fxAccount__c = fxAccount.ID, 
                Subject = CASE_SUBJECT,
                Description = CASE_DESCRIPTION,
                Live_or_Practice__c = Constants.CASE_LIVE,
                Inquiry_Nature__c  = Constants.CASE_INQUIRY_NATURE_REGISTRATION,
                Type__c = Constants.CASE_TYPE_AMP_FOLLOWUP,
                Subtype__c = sub,
                ownerId = queue.id,
                AccountId = fxAccount.Account__c,
                ContactId = fxAccount.Contact__c);
    }

}