/**
 * @description       : Utilities methods for EID Results.
 * @author            : Gilbert Gao
 * @date			  : Jan 26, 2017
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public without sharing class EIDUtil {
	static final String EID_PASSED = 'Passed';
    
	//Gilbert: create onboarding cases for new EID result if the cases don't exist
	public static void processEIDForCase(List<EID_Result__c> eIds){
		
		Set<Id> integrationProfileIDs = UserUtil.getIntegrationProfileIds();
  	  
  	    if(integrationProfileIDs.contains(UserInfo.getProfileId()) == false){
  	  	   //only integration user should create onboarding cases
  	  	   return;
  	    }
		
		//find onboarding cases
		List<Id> fxaIds = new List<Id>();
		
		for(EID_Result__c eId : eIds){
			if(eId.fxAccount__c != null){
				fxaIds.add(eId.fxAccount__c);
			}
			
		}
		
		System.debug('fxAccounts: ' + fxaIds.size());
		
		//We should never re-open a closed case when an EID result arrives. 
		Map<Id, Case> caseByfxaId = CaseUtil.getOnBoardingCaseByFxAccount(fxaIds, 'EID Verification', 'Closed', false);
		if(caseByfxaId.size() == 0){
			return;
		}
		
		//attach EID Result to case
		for(EID_Result__c eId : eIds){
			Case theCase = caseByfxaId.get(eId.fxAccount__c);
			
			if(theCase != null){
				eId.Case__c = theCase.Id; 
			}
		}
	}

	public static void updateEidData(List<EID_Result__c> eids) {
		Map<Id, Id> fxAccountMapByEid =  new Map<Id, Id>();
		Map<Id, Id> accountMapByEid =  new Map<Id, Id>();
		Map<Id, Id> leadMapByEid =  new Map<Id, Id>();

		for (EID_Result__c eid : eids) {
			if (eid.fxAccount__c != null) {
				fxAccountMapByEid.put(eid.Id, eid.fxAccount__c);
			}
			else if (eid.Account__c != null) {
				accountMapByEid.put(eid.Id, eid.Account__c);
			}
			else if (eid.Lead__c != null) {
				leadMapByEid.put(eid.Id, eid.Lead__c);
			}
		}

		Map<Id, fxAccount__c> fxAccountMap =  new Map<Id, fxAccount__c>([
			SELECT Id,
				First_Name__c,
				Last_Name__c,
				Birthdate__c,
				Street__c,
				City__c,
				State__c,
				Postal_Code__c,
				Country_Name__c,
				Account__c,
				Lead__c
			FROM fxAccount__c
			WHERE Id IN :fxAccountMapByEid.values()
		]);
		Map<Id, Account> accountMap =  new Map<Id, Account>([
			SELECT Id,
				FirstName,
				LastName,
				PersonMailingStreet,
				PersonMailingCity,
				PersonMailingState,
				PersonMailingPostalCode,
				PersonMailingCountry
			FROM Account
			WHERE Id IN :accountMapByEid.values()
		]);
		Map<Id, Lead> leadMap =  new Map<Id, Lead>([
			SELECT Id,
				FirstName,
				LastName,
				Street,
				City,
				State,
				PostalCode,
				Country
			FROM Lead
			WHERE Id IN :leadMapByEid.values()
		]);

		for (EID_Result__c eid : eids) {
			if (eid.fxAccount__c != null) {
				fxAccount__c parent = fxAccountMap.get(eid.fxAccount__c);
				eid.First_Name_PD__c = parent.First_Name__c;
				eid.Last_Name_PD__c = parent.Last_Name__c;
				eid.Birthdate_PD__c = parent.Birthdate__c;
				eid.Street_PD__c = parent.Street__c;
				eid.City_PD__c = parent.City__c;
				eid.State_PD__c = parent.State__c;
				eid.Postal_Code_PD__c = parent.Postal_Code__c;
				eid.Country_PD__c = parent.Country_Name__c;
				eid.Account__c = parent.Account__c;
				eid.Lead__c = parent.Lead__c;
			}
			else if (eid.Account__c != null) {
				Account parent = accountMap.get(eid.Account__c);
				eid.First_Name_PD__c = parent.FirstName;
				eid.Last_Name_PD__c = parent.LastName;
				eid.Street_PD__c = parent.PersonMailingStreet;
				eid.City_PD__c = parent.PersonMailingCity;
				eid.State_PD__c = parent.PersonMailingState;
				eid.Postal_Code_PD__c = parent.PersonMailingPostalCode;
				eid.Country_PD__c = parent.PersonMailingCountry;
			}
			else if (eid.Lead__c != null) {
				Lead parent = leadMap.get(eid.Lead__c);
				eid.First_Name_PD__c = parent.FirstName;
				eid.Last_Name_PD__c = parent.LastName;
				eid.Street_PD__c = parent.Street;
				eid.City_PD__c = parent.City;
				eid.State_PD__c = parent.State;
				eid.Postal_Code_PD__c = parent.PostalCode;
				eid.Country_PD__c = parent.Country;
			}
		}
	}
	
    public static void setEIDPassCount(List<EID_Result__c> eIds) {
    	Set<Id> fxaIds = new Set<Id>();
    	
        //Find associated fxAccount IDs
    	for(EID_Result__c eId : eIds) {
    		if(eId.fxAccount__c != null) {
    			fxaIds.add(eId.fxAccount__c);
    		}
    	}
        
        //Find associated fxAccounts
    	List<fxAccount__c> fxas = [SELECT Id FROM fxAccount__c WHERE Id IN :fxaIds];
        
        //Create a map from fxAccount ID to number of EID passes
        Map<Id, Integer> counts = new Map<Id, Integer>();
    	List<AggregateResult> ars = [SELECT fxAccount__c, COUNT(Id) Pass_Count__c FROM EID_Result__c WHERE fxAccount__c IN :fxaIds AND Result__c = :EID_PASSED GROUP BY fxAccount__c];
        for(AggregateResult ar : ars) {
            counts.put((Id) ar.get('fxAccount__c'), (Integer) ar.get('Pass_Count__c'));
        }
        
        // Update each fxAccount with the new EID pass count
        for(fxAccount__c fxa : fxas) {
            if(counts.containsKey(fxa.Id)) {
                fxa.EID_Pass_Count__c = counts.get(fxa.Id);
            } else {
                fxa.EID_Pass_Count__c = 0;
            }
        }
        
        update fxas;
    }
    public static void reopenCase(List<EID_Result__c> eidResults) {
    	Set<Id> fxaIds = new Set<Id>();
    	Set<Id> caseIds = new Set<Id>();
		List<EID_Result__c> linkedEidResults = new List<EID_Result__c>();

        //Find associated fxAccount Ids and Case Ids
    	for(EID_Result__c eid : eidResults) {
    		if(eid.fxAccount__c != null && eid.Case__c!=null) {
    			fxaIds.add(eid.fxAccount__c);
				caseIds.add(eid.Case__c);
				linkedEidResults.add(eid);
    		}
    	}

        //Find associated fxAccounts
    	Map<Id,fxAccount__c> fxas = new Map<Id,fxAccount__c>([SELECT Id, Division_Name__c FROM fxAccount__c WHERE Id IN :fxaIds AND Division_Name__c=:Constants.DIVISIONS_OANDA_AUSTRALIA]);
        //Find associated Cases
    	Map<Id,Case> cases = new Map<Id,Case>([SELECT Id, Status FROM Case WHERE Id IN :caseIds AND Status='Closed']);

		List<Case> casesToReopen = new List<Case>();

		for(EID_Result__c eid : linkedEidResults) {
			if(fxas.containsKey(eid.fxAccount__c) && cases.containsKey(eid.Case__c)) {
				casesToReopen.add(new Case(Id=eid.Case__c, Status='Open'));
			}
		}
        update casesToReopen;
    }
}