public without sharing class ComplyAdvantageReport implements Queueable,Database.AllowsCallouts 
{
    ComplyAdvantageSearch complyAdvantageSearch;
    public ComplyAdvantageReport(ComplyAdvantageSearch caSearchInfo)
    {
        complyAdvantageSearch = caSearchInfo;
    }
    public void execute(QueueableContext context) 
    { 
        if(complyAdvantageSearch.caSearchId != null)
        {    
            Http http = new Http(); 
               
            //setup request
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ complyAdvantageSearch.caSearchId +'/certificate?api_key='+ complyAdvantageSearch.settings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('GET');
           
            //make service call
            HttpResponse response = http.send(request);

            //process response
            if (response.getStatusCode() == 200) 
            {
                try 
                {   
                    Document__c doc = new Document__c();
                    doc.RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_DOCUMENT_CHECK, 'Document__c');
                    string documentPrefix = (complyAdvantageSearch.reportPrefix != null) ? complyAdvantageSearch.reportPrefix : 'Comply Advantage Report: ';
                    string documentName = documentPrefix + complyAdvantageSearch.searchText;
                    if(documentName.length() > 80 )
                    {
                        documentName = documentName.substring(0, 79);
                    }

                    doc.name = documentName;
                    doc.Notes__c = complyAdvantageSearch.searchReason;
                    doc.Document_Type__c = 'Comply Advantage';
                    doc.fxAccount__c = complyAdvantageSearch.fxAccountId;
                    doc.Case__c = complyAdvantageSearch.caseId;
                    doc.Account__c = complyAdvantageSearch.AccountId;
                    doc.Comply_Advantage_Search__c = complyAdvantageSearch.sfSearchRecordId;
                    Database.SaveResult docSaveResult =  Database.insert(doc);             
                 
                    Attachment attach = new Attachment();
                    attach.Body = response.getBodyAsBlob(); 
                    attach.Name = documentName;
                    attach.ParentId = docSaveResult.getId();
                    attach.ContentType = 'application/pdf';
                    attach.IsPrivate = false;			
                    insert attach;
                    
                    //query for previous monitored search
                    Comply_Advantage_Search__c[] previousSearches = new Comply_Advantage_Search__c[]{};
                    for(Comply_Advantage_Search__c s : [select Id,
                                                               Is_Monitored__c,
                                                               Search_Id__c 
                                                        From Comply_Advantage_Search__c 
                                                        where Case__c =:complyAdvantageSearch.caseId and 
                                                              Entity_Contact__c =: complyAdvantageSearch.entityContactId and 
                                                              Is_Alias_Search__c =: complyAdvantageSearch.IsAliasSearch and 
                                                              Is_Monitored__c = true])
                    {
                        if(s.Search_Id__c != complyAdvantageSearch.caSearchId)
                        {
                            previousSearches.add(s);
                        }
                    }
                    
                     //disable old search monitoring
                     if(previousSearches.size() > 0 && !Test.isRunningTest())
                     {
                        Logger.debug(
                            'Comply Advantage, Disabling previous monitored search' + previousSearches[0].Search_Id__c,
                            Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY);

                         System.enqueueJob(new ComplyAdvantageMonitorOff(previousSearches[0].Search_Id__c,
                                                                         complyAdvantageSearch.settings.apiKey,
                                                                         false)); 
                     }
                } 
                catch (DMLException e) 
                { 
                    Logger.exception(
                        Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                        e);
                }
       		}  else {
                Logger.error(
                    'Comply advantage report error',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response);
            }
        }   
      
    }
}