/**
 * Created by akajda on 21/04/2023.
 */

public with sharing class BatchApexErrorHandler {
    
    public static void upsertErrorLogs(List<BatchApexErrorEvent> triggerNew){
        Set<Id> asyncApexJobIds = new Set<Id>();
        for(BatchApexErrorEvent evt:triggerNew){
            asyncApexJobIds.add(evt.AsyncApexJobId);
        }

        Map<Id,AsyncApexJob> jobs = new Map<Id,AsyncApexJob>(
                [SELECT Id, ApexClass.Name,CompletedDate, LastProcessed, MethodName, JobType, Status, CronTriggerId, ParentJobId FROM AsyncApexJob WHERE Id IN :asyncApexJobIds]
        );
        //additional query for errors triggered from BatchApexWorker and not BatchApex itself
        Set<Id> parents = new Set<Id>();
        for(AsyncApexJob job: jobs.values()){
            if(job.ParentJobId!=null){
                parents.add(job.ParentJobId);
                asyncApexJobIds.add(job.ParentJobId);
            }
        }
        Map<Id,AsyncApexJob> parentJobs = new Map<Id,AsyncApexJob>(
                [SELECT Id, ApexClass.Name,CompletedDate, LastProcessed, MethodName, JobType, Status, CronTriggerId, ParentJobId FROM AsyncApexJob WHERE Id IN :parents]
        );

        Map<Id,BatchErrorEvent__c> errorEvents = new Map<Id,BatchErrorEvent__c>();

        for(BatchErrorEvent__c errorEvent: [SELECT Id, Rerun_Job_Id__c, Parent_BatchErrorEvent__c, Stack_Trace__c FROM BatchErrorEvent__c WHERE Rerun_Job_Id__c IN :asyncApexJobIds]){
            errorEvents.put(errorEvent.Rerun_Job_Id__c, errorEvent);
        }

        List<BatchErrorEvent__c> records = new List<BatchErrorEvent__c>();
        for(BatchApexErrorEvent evt:triggerNew){
            AsyncApexJob job = jobs.get(evt.AsyncApexJobId);  
            if(parents!=null && !parents.isEmpty()) job = parentJobs.get(job.ParentJobId);
            BatchErrorEvent__c a = new BatchErrorEvent__c(
                    Name = evt.AsyncApexJobId, 
                    Stack_Trace__c = evt.StackTrace, 
                    Message__c=evt.Message, 
                    Phase__c=evt.Phase, 
                    Job_Id__c=evt.AsyncApexJobId, 
                    Exception_Type__c=evt.ExceptionType,
                    Status__c='Failed', 
                    Job_Scope__c=evt.JobScope,
                    Class_Name__c=job.ApexClass.Name, 
                    Job_Type__c=job.JobType, 
                    Completion_Date__c = job.CompletedDate
            );
            if(errorEvents.containsKey(evt.AsyncApexJobId)){
                if(errorEvents.get(evt.AsyncApexJobId).Stack_Trace__c==evt.StackTrace){
                    a.Id=job.Id;
                }else{
                    a.Parent_BatchErrorEvent__c=job.Id;
                }
            } 
            if(errorEvents.containsKey(job.Id)){
                if(errorEvents.get(job.Id).Stack_Trace__c==evt.StackTrace){
                    a.Id=errorEvents.get(job.Id).Id;
                }else{
                    a.Parent_BatchErrorEvent__c=errorEvents.get(job.Id).Id;
                }
            } 
            records.add(a);

        }
        upsert records;
    }
    @AuraEnabled (cacheable=false)
    public static String checkBatchStatus(Id recordId){
        try{
            BatchErrorEvent__c errorEvent = [SELECT Id, Job_Scope__c,Job_Id__c, Status__c, Class_Name__c, Parent_BatchErrorEvent__c FROM BatchErrorEvent__c WHERE Id=:recordId];
            if(errorEvent.Parent_BatchErrorEvent__c!=null){
                BatchErrorEvent__c parentEvent = [SELECT Id, Job_Scope__c,Job_Id__c, Status__c, Class_Name__c, Parent_BatchErrorEvent__c 
                FROM BatchErrorEvent__c WHERE Id=:errorEvent.Parent_BatchErrorEvent__c];
                errorEvent=parentEvent;
            }
            if(errorEvent.Status__c!='Failed'){
                return 'Batch job is already in the following status: '+errorEvent.Status__c;
            }
            return 'ok';
        }catch(Exception error){
            return 'Unexpected error: '+error.getMessage();
        }
    }

    //to handle quickaction rerun, to be called i.e. from LWC
    @AuraEnabled (cacheable=false)
    public static String rerunBatch(Id recordId, Integer batchSize){
        try{
            BatchErrorEvent__c errorEvent = [SELECT Id, Job_Scope__c,Job_Id__c, Status__c, Class_Name__c, Parent_BatchErrorEvent__c FROM BatchErrorEvent__c WHERE Id=:recordId];
            if(errorEvent.Parent_BatchErrorEvent__c!=null){
                BatchErrorEvent__c parentEvent = [SELECT Id, Job_Scope__c,Job_Id__c, Status__c, Class_Name__c, Parent_BatchErrorEvent__c 
                FROM BatchErrorEvent__c WHERE Id=:errorEvent.Parent_BatchErrorEvent__c];
                errorEvent=parentEvent;
            }
            if(errorEvent.Status__c!='Failed'){
                return 'Batch job is already in the following status: '+errorEvent.Status__c;
            }
            if(errorEvent.Job_Scope__c!=null && errorEvent.Job_Scope__c!=''){
                List<String> jobIds = errorEvent.Job_Scope__c.split(',');
                BatchReflection br = getBatchInstance(errorEvent.Class_Name__c);
                Id newBatchJobId = br.rerunSetup(jobIds, batchSize);
                AsyncApexJob job = [SELECT Id, ApexClass.Name,CompletedDate, LastProcessed, MethodName, JobType, Status, ParentJobId FROM AsyncApexJob WHERE Id=:newBatchJobId];
                errorEvent.Rerun_Job_Id__c=job.Id; 
                errorEvent.Status__c=job.Status;
                update errorEvent;
                return 'Rerun successfully scheduled, current status: '+job.Status;
            }else{
                return 'Missing record ids!';
            }

        }catch(Exception e){
            return 'Error ocurred: '+e.getMessage();
        }

    }

    //method called after succesfull rerun
    public static void updateBatchErrorLogs(Id recordId){
        try{
            BatchErrorEvent__c errorEvent = [SELECT Id, Rerun_Job_Id__c, Parent_BatchErrorEvent__c FROM BatchErrorEvent__c WHERE Rerun_Job_Id__c =:recordId];
            List<BatchErrorEvent__c> records = new List<BatchErrorEvent__c>();
            BatchErrorEvent__c a = new BatchErrorEvent__c( Id = errorEvent.Id, Status__c='Completed');
            records.add(a);
            List<BatchErrorEvent__c> childEvents = [SELECT Id, Rerun_Job_Id__c, Parent_BatchErrorEvent__c FROM BatchErrorEvent__c WHERE Parent_BatchErrorEvent__c =:errorEvent.Id];
            for(BatchErrorEvent__c evt:childEvents){
                records.add(new BatchErrorEvent__c( Id = errorEvent.Id, Status__c='Completed'));
            }
                upsert records;

        }catch(Exception e){
            System.debug(e.getMessage());
        }

    }

    public static void checkBatchErrors(Boolean isRerun, Database.BatchableContext bc){
        if(isRerun){
			AsyncApexJob job = [SELECT Id, NumberOfErrors FROM AsyncApexJob WHERE Id = :bc.getJobId()];
			if(job.NumberOfErrors==0) BatchApexErrorHandler.updateBatchErrorLogs(bc.getJobId());
		} 
    }

    public static BatchReflection getBatchInstance(String batchName) {
        Type t = Type.forName(batchName);
        object batchInstnce = t.newInstance();
        BatchReflection interfaceInstance = (BatchReflection) batchInstnce;
        return interfaceInstance;
    }
}