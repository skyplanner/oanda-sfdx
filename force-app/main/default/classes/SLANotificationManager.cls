/**
 * @File Name          : SLANotificationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 4:57:48 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/6/2021, 4:50:37 PM   acantero     Initial Version
**/
public without sharing abstract class SLANotificationManager {

    static ID slaNotificationId;

    protected final SLAConst.AgentSupervisorModel supervisorModel;
    @testVisible
    protected SupervisorProvider supervisorProv;
    protected SupervisorProviderCreator supervisorProviderCreator;

    public SLANotificationManager(
        SLAConst.AgentSupervisorModel supervisorModel,
        SupervisorProviderCreator supervisorProviderCreator
    ) {
        this.supervisorModel = supervisorModel;
        this.supervisorProviderCreator = supervisorProviderCreator;
    }

    protected Boolean findSupervisors(List<SLAViolationInfo> violationInfoList) {
        if (supervisorProv == null) {
            supervisorProv = supervisorProviderCreator.createSupervisorProvider(supervisorModel);
        }
        for(SLAViolationInfo violationInfo : violationInfoList) {
            supervisorProv.addCriteria(
                getSupervisorCriteria(violationInfo)
            );
        }
        supervisorProv.getSupervisors();
        Boolean result = !supervisorProv.isEmpty();
        return result;
    }

    public static ID getSlaNotificationId() {
        if (slaNotificationId == null) {
            slaNotificationId = [
                SELECT 
                    Id
                FROM 
                    CustomNotificationType
                WHERE 
                    DeveloperName = :SLAConst.SLA_NOTIFICATION_DEV_NAME
            ]?.Id;
            if (slaNotificationId == null) {
                throw new SLANotifManagerException(
                    'Sla Notification type not found, DevName: ' + SLAConst.SLA_NOTIFICATION_DEV_NAME
                );
            }
        }
        return slaNotificationId;
    }

    protected String getSupervisorCriteria(SLAViolationInfo violationInfo) {
        String result = null;
        if (supervisorModel == SLAConst.AgentSupervisorModel.DIVISION) {
            result = violationInfo.divisionCode;
            //...
        } else if (supervisorModel == SLAConst.AgentSupervisorModel.ROLE) {
            result = violationInfo.ownerId;
            //...
        }
        return result;
    }

    //*************************************************************************** */

    public class SLANotifManagerException extends Exception {
    }

}