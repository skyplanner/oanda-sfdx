/**
 * @File Name          : ChatTranscriptRepo.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/19/2024, 1:52:01 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/19/2024, 1:51:48 PM   aniubo     Initial Version
**/
public inherited sharing class ChatTranscriptRepo {
	public static List<LiveChatTranscript> getChatTranscriptsForSLANotification(
		List<Id> ids
	) {
		List<LiveChatTranscript> result = [
			SELECT
				Id,
				Name,
				Tier__c,
				Language__c,
				Chat_Division__c,
				Inquiry_Nature__c,
				Division_Name__c,
				Chat_Type_of_Account__c,
				Account.Name,
				Account.Primary_Division_Name__c
			FROM LiveChatTranscript
			WHERE Id IN :ids
		];
		return result;
	}
}