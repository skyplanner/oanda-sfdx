/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-16-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class RelatedListFieldSettingMDT implements Comparable {

    public Related_List_Field_Setting__mdt fieldSetting;
    
    // Constructor
    public RelatedListFieldSettingMDT(Related_List_Field_Setting__mdt fs) {
    	// Guard against wrapping a null 
    	if(fs == null) {
    		Exception ex = new NullPointerException();
    		ex.setMessage('Related List Field Setting Medatada argument cannot be null'); 
    		throw ex;
    	}
        fieldSetting = fs;
    }
    
    public Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        RelatedListFieldSettingMDT compareToFs = (RelatedListFieldSettingMDT)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (fieldSetting.Order__c == null && compareToFs.fieldSetting.Order__c == null) {
            returnValue = 0;
        } else if (fieldSetting.Order__c == null && compareToFs.fieldSetting.Order__c != null){
            // nulls-last implementation
            returnValue = 1;
        } else if (fieldSetting.Order__c != null && compareToFs.fieldSetting.Order__c == null){
            // nulls-last implementation
            returnValue = -1;
        } else if (fieldSetting.Order__c > compareToFs.fieldSetting.Order__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (fieldSetting.Order__c < compareToFs.fieldSetting.Order__c) {
            // Set return value to a negative value.
            returnValue = -1;
        } 
        return returnValue;
    }
}