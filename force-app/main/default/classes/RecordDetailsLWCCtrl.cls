/**
 * RecordDetailsLWC ctrl class
 */
public with sharing class RecordDetailsLWCCtrl {
    
    /**
     * Get related record information
     * - Id 
     * - API Name
     */
    @AuraEnabled
    public static Map<String, String> fetchRelatedRecord(
        Id recordId,
        String lookup)
    {
        try {
            // Build query to fetch related record info
            String q =
                ' SELECT ' + lookup + 
                ' FROM ' + SObjectUtil.getObjName(recordId) + 
                ' WHERE Id = \'' + recordId + '\'';

            System.debug(
                'fetchRelatedRecord-query: ' +
                q);

            // Get related record's id
            Id relatedId =
                (Id)(((SObject)Database.query(q)).get(lookup));

            if(String.isBlank(relatedId))
                return null;

            // Return related record information
            // (Obj API Name and Id)
            return new Map<String, String>{
                'id' => relatedId,
                'objName' => SObjectUtil.getObjName(relatedId)};
        } catch (Exception ex) {
            throw new AuraHandledException(
                ex.getMessage() + ex.getStackTraceString());
        }
    }

}