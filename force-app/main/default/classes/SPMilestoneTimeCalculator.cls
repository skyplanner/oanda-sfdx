/**
 * @File Name          : SPMilestoneTimeCalculator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/15/2020, 2:17:41 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/31/2020   acantero     Initial Version
**/
global class SPMilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator {

    global static final Integer DEFAULT_TIME = 1;

    global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId) {
        Integer result = DEFAULT_TIME;
        List<Milestone_Time_Settings__mdt> settings = [
            SELECT
                Time_Trigger__c
            FROM
                Milestone_Time_Settings__mdt
            WHERE
                Milestone_Type_ID__c = :milestoneTypeId
        ];
        if (!settings.isEmpty()) {
            result = Integer.valueOf(settings[0].Time_Trigger__c);
        }
        return result;
    }

}