public without sharing class ComplyAdvantageMonitorUpdateEntites implements Queueable,Database.AllowsCallouts
{
    public static final Schema.SObjectField ENTITY_OBJ_UNIQUE_NUMBER_FIELD = Comply_Advantage_Search_Entity__c.Fields.Unique_ID__c;

    Comply_Advantage_Search__c searchRecord;
    ComplyAdvantageMonitorNotification.MonitorData monitorInfo;

    private set<string> entityIds = new set<String>{};

    private set<string> addedEntityIds = new set<String>{};
    private set<string> updatedEntityIds = new set<String>{};
    private set<string> removedEntityIds = new set<String>{};

    private Comply_Advantage_Search_Entity__c[] caSearchEntities;
    private Map<Id, Comply_Advantage_Search_Entity__c> caSearchEntitiesMap;

    public ComplyAdvantageMonitorUpdateEntites() 
    {

    }
    public ComplyAdvantageMonitorUpdateEntites(Comply_Advantage_Search__c searchRecord,
                                               ComplyAdvantageMonitorNotification.MonitorData monitorInfo) 
    {
        this.searchRecord = searchRecord;
		this.monitorInfo = monitorInfo;
        
        if(monitorInfo != null)
        {
            if(monitorInfo.added != null && !monitorInfo.added.isEmpty())
            {
                addedEntityIds = formatEntityIds(monitorInfo.added);
                entityIds.addAll(addedEntityIds);
            }
            if(monitorInfo.updated != null && !monitorInfo.updated.isEmpty())
            {
                updatedEntityIds = formatEntityIds(monitorInfo.updated);
                entityIds.addAll(updatedEntityIds);
            }
            if(monitorInfo.removed != null && !monitorInfo.removed.isEmpty())
            {
                removedEntityIds = formatEntityIds(monitorInfo.removed);
                entityIds.addAll(removedEntityIds);
            }
        }
	}
    
    public void execute(QueueableContext context) 
    {  
        execute();
    }

    public void execute()
	{
        //download Entities first
        downloadEntities();

        //update Flags
        updateMonitorFlags();
      
    }
    
    public void updateMonitorFlags()
	{      
        Comply_Advantage_Search_Entity__c[] caSearchEntitiesToUpdate = new  Comply_Advantage_Search_Entity__c[]{};

        if(entityIds != null && !entityIds.isEmpty())
        {
            for(Comply_Advantage_Search_Entity__c caSearchEntity :  [select Id,
                                                                            Unique_ID__c,
                                                                            Monitor_Change_Status__c,
                                                                            Is_Removed__c 
                                                                     From Comply_Advantage_Search_Entity__c 
                                                                     where Unique_ID__c IN :entityIds])
            {
                if(addedEntityIds.contains(caSearchEntity.Unique_ID__c))
                {
                    caSearchEntity.Monitor_Change_Status__c = 'Added';
                    caSearchEntity.Is_Removed__c = false;
                    caSearchEntitiesToUpdate.add(caSearchEntity);
                }
                if(updatedEntityIds.contains(caSearchEntity.Unique_ID__c))
                {
                    caSearchEntity.Monitor_Change_Status__c = 'Updated';
                    caSearchEntity.Is_Removed__c = false;
                    caSearchEntitiesToUpdate.add(caSearchEntity);
                }
                if(removedEntityIds.contains(caSearchEntity.Unique_ID__c))
                {
                    caSearchEntity.Monitor_Change_Status__c = 'Removed';
                    caSearchEntity.Is_Removed__c = false;
                    caSearchEntitiesToUpdate.add(caSearchEntity);
                }
            }

            if(!caSearchEntitiesToUpdate.isEmpty())
            {
                update caSearchEntitiesToUpdate;
            }
        }
	} 

	private set<string> formatEntityIds(List<string> eIds)
    {
        set<string> formattedEntityIds = new set<string>{};
        for(string eid:eIds)
        {
            formattedEntityIds.add(monitorInfo.search_id +':' + eid);
        }
        return formattedEntityIds;
	} 

	private void downloadEntities()
    {
        ComplyAdvantageEntitiesDownloader downloader = new ComplyAdvantageEntitiesDownloader(searchRecord);

        ComplyAdvantageEntitiesDownloader.EntityDownloadResult result = downloader.getEntities();

        List<Comply_Advantage_Search_Entity__c> entitesToInsert = result.entities;

        if(!entitesToInsert.isEmpty())
        {
            Database.upsert(entitesToInsert, ENTITY_OBJ_UNIQUE_NUMBER_FIELD, false);
        }
	} 
}