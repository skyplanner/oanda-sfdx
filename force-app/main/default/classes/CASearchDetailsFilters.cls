/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsFilters {
    public Integer birth_year { get; set; }
    
    public List<String> country_codes { get; set; }

    public String entity_type { get; set; }

    public Boolean exact_match { get; set; }

    public Decimal fuzziness { get; set; }

    public Integer remove_deceased { get; set; }

    public List<String> types { get; set; }
}