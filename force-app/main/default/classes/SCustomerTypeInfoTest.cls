/**
 * @File Name          : SCustomerTypeInfoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 3:46:43 PM
**/
@isTest
private class SCustomerTypeInfoTest {

	// test 1 : create from Account
	// test 2 : create from Lead
	@IsTest
	static void test() {
		Account accountObj = new Account();
		Lead leadObj = new Lead();

		Test.startTest();
		// test 1
		SCustomerTypeInfo instance1 = new SCustomerTypeInfo(accountObj);
		// test 2
		SCustomerTypeInfo instance2 = new SCustomerTypeInfo(leadObj);
		Test.stopTest();
		
		Assert.isNotNull(instance1, 'Invalid result');
		Assert.isNotNull(instance2, 'Invalid result');
	}
	
}