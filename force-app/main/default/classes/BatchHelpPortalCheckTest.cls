/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@IsTest
private class BatchHelpPortalCheckTest {
    @isTest
	static void test500() {
        CalloutMock mock = new CalloutMock(
			503,
			'{}',
			'Fail',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.StartTest();

        BatchHelpPortalCheck b = new BatchHelpPortalCheck();
        Database.executeBatch(b);

		Test.stopTest();

		List<AsyncApexJob> aj = [SELECT Id
			FROM AsyncApexJob
			WHERE ApexClass.Name = 'BatchHelpPortalCheck'
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		System.assertEquals(true, aj.size() > 0);
	}

    @isTest
	static void test200() {
        CalloutMock mock = new CalloutMock(
			200,
			'{}',
			'Fail',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.StartTest();

        BatchHelpPortalCheck b = new BatchHelpPortalCheck();
        Database.executeBatch(b);

		Test.stopTest();

		List<AsyncApexJob> aj = [SELECT Id
			FROM AsyncApexJob
			WHERE ApexClass.Name = 'BatchHelpPortalCheck'
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		System.assertEquals(true, aj.size() > 0);
	}
}