/**
 * @File Name          : LiveChatBySkillsSelectionCon.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/2/2022, 4:26:06 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/23/2020   acantero     Initial Version
**/
public without sharing class LiveChatBySkillsSelectionCon {

    public static final String AUTOMATIC_CONNECTION = 'automaticConnection';

    public static final String DIVISION = 'Division';
    public static final String DIVISION_NAME = 'DivisionName';
    public static final String ACCOUNT_TYPE = 'AccountType';
    public static final String INQUIRY_NATURE = 'InquiryNature';
    public static final String SPECIFIC_QUESTION = 'SpecificQuestion';

    
    public Boolean oneChatButtonForAll {get; private set;}
    public String defaultChatButtonId {get; private set;}
    public Boolean isAPostchatRedirect {get; private set;}
    public Boolean automaticConnection {get; private set;}
    public Boolean agentsAvailable {get; private set;}
    public Boolean isEnglish {get; private set;}
    public String languagePreference {get; private set;}
    public String languageLabel {get; private set;}
    public String languageCorrected {get; private set;}
    public String divisionName {get; private set;}
    public String defaultLanguage {get; private set;}
    public String defaultLanguageLabel {get; private set;}
    public String inquiryNatureMapped {get; private set;}
    public String serviceType {get; private set;}
    public String serviceSubtype {get; private set;}

    public String chatLangSelectionUrl {get; private set;}
    public String refreshUrl {get; private set;}

    public String unableToConnectMsg {get; private set;}
    public String unableToConnectMsgDefaultLang {get; private set;}

    public Boolean offlineHours {get; private set;}

    public LiveChatBySkillsSelectionCon() {
        chatLangSelectionUrl = Page.LiveChatLanguage.getUrl();
        refreshUrl = Page.LiveChatAgent.getUrl();
        offlineHours = BaseLiveChatCtrl.checkOfflineHours();
        //...
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        initLanguageProperties(pageParams);
        initChatProperties(pageParams);
        //...
        //save in a cookie because LiveChatCustomWindow page will retrieve it. 
        String cookieLanguage = (isAPostchatRedirect == true)
            ? defaultLanguage
            : languagePreference;
        Cookie chatLanguage = 
            new Cookie(
                ChatConst.CHAT_LANGUAGE_COOKIE, 
                cookieLanguage, 
                null, 
                611040000, 
                false
            ); 
        ApexPages.currentPage().setCookies(new Cookie[]{chatLanguage});
    }

    void initLanguageProperties(Map<String,String> pageParams) {
        defaultLanguage = ChatConst.DEFAULT_LANGUAGE;
        defaultLanguageLabel = ServiceLanguagesManager.getLanguage(defaultLanguage);
        languagePreference = pageParams.get(ChatConst.LANGUAGE_PREFERENCE);
        if (String.isBlank(languagePreference)) {
            languagePreference = ChatConst.DEFAULT_LANGUAGE;
        }
        languageLabel = ServiceLanguagesManager.getLanguage(languagePreference);
        String langCorrectedByFamily = 
            ServiceLanguagesManager.getLanguageCorrectedByFamily(
                languagePreference
            );
        languageCorrected = CaseHelper.getValidLanguageCorrected(
            langCorrectedByFamily, // languageCorrected
            defaultLanguageLabel // defaultLanguage
        );
        isEnglish = (languagePreference == ChatConst.DEFAULT_LANGUAGE);
        unableToConnectMsg = (isEnglish)
            ? Label.Unable_To_Connect_Chat
            : Label.Chat_Not_Available_For_Language;
        unableToConnectMsgDefaultLang = Label.Unable_To_Connect_Chat;
    }

    public static String getValidLanguageCorrected(
        String languageCorrectedVal,
        String defaultLanguageLabelVal
    ) {
        String result = defaultLanguageLabelVal;
        if (CaseHelper.isValidLanguageCorrectedValue(languageCorrectedVal) == true) {
            result = languageCorrectedVal;
        } 
        return result;
    }

    void initChatProperties(Map<String,String> pageParams) {
        String isAPostchatRedirectStr = pageParams.get(ChatConst.POSTCHAT_REDIRECT);
        isAPostchatRedirect = (isAPostchatRedirectStr != null) && Boolean.valueOf(isAPostchatRedirectStr);
        if (isAPostchatRedirect == true) {
            automaticConnection = true;
            agentsAvailable = false;
            setDefaultChatButtonId(defaultLanguage);
            return;
        } 
        //else...
        String automaticConnectionStr = pageParams.get(AUTOMATIC_CONNECTION);
        automaticConnection = (automaticConnectionStr != null) && Boolean.valueOf(automaticConnectionStr);
        agentsAvailable = ChatLanguagesManager.languageIsAvailable(languagePreference);
        setDefaultChatButtonId(languagePreference);
        initServiceProperties(pageParams);
    }

    void initServiceProperties(Map<String,String> pageParams) {
        String division = pageParams.get(DIVISION);
        divisionName = pageParams.get(DIVISION_NAME);
        if (
            String.isBlank(divisionName) &&
            String.isNotBlank(division)
        ) {
            divisionName = 
                ServiceDivisionsManager.getInstance().getDivisionName(
                    division
                );
        }
        String accountType = pageParams.get(ACCOUNT_TYPE);
        String inquiryNature = pageParams.get(INQUIRY_NATURE);
        String specificQuestion = pageParams.get(SPECIFIC_QUESTION);
        Boolean isLiveAccountType = (accountType == OmnichanelConst.LIVE);
        ServiceQuestionsMapper mapper = 
            ServiceQuestionsMapper.createFromUnmappedValues(
                isLiveAccountType,
                inquiryNature,
                specificQuestion
            );
        inquiryNatureMapped = mapper.inquiryNatureMapped;
        serviceType = mapper.serviceType;
        serviceSubtype = mapper.serviceSubtype;
    }

    void setDefaultChatButtonId(String languageCode){
        oneChatButtonForAll = true;
        defaultChatButtonId = OmnichanelSettings.getDefaultChatButtonId();
        if (String.isBlank(defaultChatButtonId)) {
            oneChatButtonForAll = false;
            defaultChatButtonId = 
                ServiceLanguagesManager.getChatButtonIdByLangCode(
                    languageCode
                );
        }
        if (defaultChatButtonId == null) {
            defaultChatButtonId = '';
        }
    }

    public String getAPIRoot(){
    	return CustomSettings.getLiveAgentAPIRoot();
    }
    
    public String getJSRoot(){
    	return CustomSettings.getLiveAgentJSRoot();
    }
    
    public String getDeployParam1(){
    	return CustomSettings.getLiveAgentDeployParam1();
    }
    
    public String getDeployParam2(){
    	return CustomSettings.getLiveAgentDeployParam2();
    }

    @RemoteAction
	public static Boolean checkLanguageIsAvailable(String languageCode) {
        Boolean result = ChatLanguagesManager.languageIsAvailable(languageCode);
		return result;
	}

}