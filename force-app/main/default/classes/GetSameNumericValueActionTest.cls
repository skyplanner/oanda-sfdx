/**
 * @File Name          : GetSameNumericValueActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/19/2023, 9:31:56 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 9:31:56 AM   aniubo     Initial Version
 **/
@isTest
private class GetSameNumericValueActionTest {
	@isTest
	private static void getSameNumericValue() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<Decimal> valueList = new List<Decimal>{ 1, 2, 3, 4, 5 };
		List<Decimal> decimals = GetSameNumericValueAction.getSameNumericValue(
			valueList
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			valueList.size(),
			decimals.size(),
			'The size of the list should be the same'
		);
	}
}