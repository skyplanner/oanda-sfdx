public with sharing class BatchStatusController {
	public String error {get; set;}
	
	Boolean checkForOtherJobs() {
		error = '';
		List<AsyncApexJob> openJobs = [select Id from AsyncApexJob where Status = 'Processing' OR Status = 'Queued']; 
	 
		if(openJobs.size() < 5){
		    return true;
		}else{
			error = 'Only five batch jobs at a time.';
			return false;
		}
	}
	
	public void startBatchLeadToFxAccount() {
		if(checkForOtherJobs()) {
			ID batchprocessid = BatchLeadToFxAccount.executeBatch();
		}
	}

	public void startBatchMergeLeads() {
		if(checkForOtherJobs()) {
			ID batchprocessid = BatchMergeLeads.executeBatch();
		}
	}

	public void startBatchAssignLeads() {
		if(checkForOtherJobs()) {
			ID batchprocessid = BatchAssignLeads.executeBatch();
		}
	}

	public void startBatchConvertLeads() {
		if(checkForOtherJobs()) {
			ID batchprocessid = BatchConvertLeads.executeBatch();
		}
	}
}