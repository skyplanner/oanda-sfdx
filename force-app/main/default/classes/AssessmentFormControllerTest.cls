/**
 * Assessment Form Controller tests
 * 
 * @author Gilbert Gao
 */
@isTest
private class AssessmentFormControllerTest {

	@testSetup
	static void createTestData() {
		RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
		
		Account acc = new Account(LastName = 'abc', RecordTypeId = personAccountRecordType.Id);
    	insert acc;
    	
    	fxAccount__c liveAccount = new fxAccount__c();
    	liveAccount.name = 'abc';
	    liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
		liveAccount.Account__c = acc.Id;
		liveAccount.Contact__c = acc.PersonContactId;

		liveAccount.Knowledge_Answer_1__c = 'abc';
		liveAccount.Knowledge_Answer_2__c = 'bcd';
		liveAccount.Knowledge_Result__c = 'Pass';

		insert liveAccount;
		
	}


	@isTest static void showAssessmentFormTest() {

		fxAccount__c fxa = [select id, name, Knowledge_Result__c, Knowledge_Answer_1__c, Knowledge_Answer_2__c from fxAccount__c limit 1];

        Test.startTest();
		fxAccount__c fxa1 = AssessmentFormController.getFxAccount(fxa.Id);

		System.assertEquals(fxa.Name, fxa1.name);
		System.assertEquals(fxa.Knowledge_Result__c, fxa1.Knowledge_Result__c);


		List<AssessmentFormController.AssessmentQuestion> questions = AssessmentFormController.getAssessmentQuestions(fxa.Id);
        
        //since we only initialize two knowledge anwer fields, so there should be only two records.
		System.assertEquals(2, questions.size());

        fxa.Knowledge_Answer_1__c = null;
		fxa.Knowledge_Answer_2__c = null;
		update fxa;

		questions = AssessmentFormController.getAssessmentQuestions(fxa.Id);
		System.assertEquals(0, questions.size());

		Test.stopTest();

	}

}