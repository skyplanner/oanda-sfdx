/**
 * @File Name          : NonHVCCasesStatusPairManager.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 4:20:40 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/6/2024, 3:07:10 PM   aniubo     Initial Version
**/
public with sharing class NonHVCCasesStatusPairManager {
	public NonHVCCasesStatusPairSettings getByStatusAllowing(
		String currentStatus
	) {
		Non_HVC_Cases_Status_Pair__mdt settings = NonHVCCasesStatusPairHelper.getByStatusAllowing(
			currentStatus
		);
		return getByStatus(settings, true);
	}

	public NonHVCCasesStatusPairSettings getByStatusNotAllowing(
		String currentStatus
	) {
		Non_HVC_Cases_Status_Pair__mdt settings = NonHVCCasesStatusPairHelper.getByStatusNotAllowing(
			currentStatus
		);
		return getByStatus(settings, false);
	}

	@testVisible
	private NonHVCCasesStatusPairSettings getByStatus(
		Non_HVC_Cases_Status_Pair__mdt settings,
		Boolean isAllowing
	) {
		return isAllowing
			? NonHVCCasesStatusPairSettings.createFromByStatusAllowing(settings)
			: NonHVCCasesStatusPairSettings.createFromByStatusNotAllowing(
					settings
			  );
	}
}