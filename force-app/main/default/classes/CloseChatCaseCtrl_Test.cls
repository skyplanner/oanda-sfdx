/**
 * @File Name          : CloseChatCaseCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/9/2019, 4:44:55 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/9/2019, 4:37:27 PM   acantero     Initial Version
**/
@isTest
private class CloseChatCaseCtrl_Test {

	@testSetup
	static void setup () {
        User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
        System.runAs(SYSTEM_USER) {
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id, Origin='Chat');
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now());
			insert t;
		}
	}

	@isTest
	static void getCaseIdFromChatTranscript_test() {
        LiveChatTranscript chatTranscriptObj = [select Id, CaseId from LiveChatTranscript limit 1];
		String chatTranscriptId = chatTranscriptObj.Id;
		Test.startTest();
		String result = CloseChatCaseCtrl.getCaseIdFromChatTranscript(chatTranscriptId);
		Test.stopTest();
		System.assertEquals(chatTranscriptObj.CaseId, result);
	}

    @isTest
	static void getCaseIdFromChatTranscript_test2() {
		String chatTranscriptId = null;
		Test.startTest();
		String result = CloseChatCaseCtrl.getCaseIdFromChatTranscript(chatTranscriptId);
		Test.stopTest();
		System.assertEquals(null, result);
	}

    @isTest
	static void getCaseIdFromChatTranscript_test3() {
        LiveChatTranscript chatTranscriptObj = [select Id, CaseId from LiveChatTranscript limit 1];
		String chatTranscriptId = chatTranscriptObj.CaseId;
		Test.startTest();
		String result = CloseChatCaseCtrl.getCaseIdFromChatTranscript(chatTranscriptId);
		Test.stopTest();
		System.assertEquals(null, result);
	}

}