/**
 * @File Name          : LeadRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/14/2023, 3:03:00 AM
**/
public inherited sharing class LeadRepo {

	public static Lead getSummaryByEmail(String email) {
		List<Lead> recList = [
			SELECT Name
			FROM Lead
			WHERE Email = :email
			LIMIT 1
		];
		Lead result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

}