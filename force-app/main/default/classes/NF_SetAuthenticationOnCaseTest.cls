/**
 * Created by samuel on 12/3/21.
 */
@IsTest
public with sharing class NF_SetAuthenticationOnCaseTest {
    @TestSetup
    static void createTestData() {
        //NF_TestDataUtill.createQueue();
        nfchat__Chat_Log__c chatLog = new nfchat__Chat_Log__c();
        chatLog.nfchat__First_Name__c = 'Joy';
        chatLog.nfchat_Case_Description__c = 'Description';
        chatLog.nfchat_Account_Type__c = 'Live';
        chatLog.nfchat__Last_Name__c = 'Smith';
        chatLog.nfchat__Phone__c = '1234567890';
        chatLog.nfchat_Username__c = 'joy@test.in';
        chatLog.nfchat__Email__c = 'joySmith@test.in';
        chatLog.nfchat__Company__c = 'Smith Tech';
        chatLog.nfchat__AI_Config_Name__c = 'AIConfig Test';
        chatLog.nfchat__Session_Id__c = '123456789';
        chatLog.Auth_Answer__c = '123456';
        chatlog.Annual_Income__c = '123';
        chatlog.Authenticated__c = 'true';
        chatlog.Current_Employer__c = 'NF';
        chatlog.Income_Source__c = 'Inherited';
        chatlog.Industry__c = 'Test';
        chatlog.Job_Title__c = 'Dev';
        chatlog.Language_Preference__c = 'English';
        chatlog.Net_Worth__c = '1233456';
        chatlog.Job_Title__c = 'Dev';
        chatlog.EmailCheck__c = 'true';
        chatlog.Case_Type__c = 'Live';
        chatlog.Case_Sub_Type__c = 'Individual';
        insert chatLog;

        nfchat__Chat_Log_Detail__c chatLogDetail = new nfchat__Chat_Log_Detail__c();
        chatLogDetail.nfchat__Intent_Name__c = 'business.account.registration';
        chatLogDetail.nfchat__Chat_Log__c = chatLog.id;
        insert chatLogDetail;

        TestDataFactory testHandler = new TestDataFactory();
        Account acc;
        fxAccount__c fxa;
        Case sampleCase;
        Contact theConttact;

        testHandler = new TestDataFactory();
        acc = testHandler.createTestAccount();
        acc.RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId();
        acc.PersonEmail = 'joySmith@test.in';
        acc.Do_Not_Mail__c = false;
        acc.Marketing_Email_Opt_In__c = false;
        acc.Authentication_Code__c = '123456';
        update acc;

        fxa = testHandler.createFXTradeAccount(acc);
        fxa.Email__c = 'joySmith@test.in';  // Update to match the chat log
        fxa.Lead_Score__c = 3;              // Setting this arbitrarily
        fxa.Account__c = acc.Id;
        update fxa;

        theConttact = [
                SELECT Id
                FROM Contact
                WHERE AccountId = :acc.Id
        ];

        sampleCase = new Case(
                recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                Status = 'Open',
                fxAccount__c = fxa.Id,
                AccountId = fxa.Account__c,
                ContactId = theConttact.Id,
                Subject = 'Subject of the case',
                nfchat__Chat_Log__c = chatLog.Id,
                Session_Id__c = chatLog.Id
        );
        insert sampleCase;
    }

    @IsTest
    public static void testSetAuth() {
        String chatId = [SELECT Id FROM nfchat__Chat_Log__c LIMIT 1].Id;
        NF_SetAuthenticationOnCase setter = new NF_SetAuthenticationOnCase();
        setter.doInvoke(chatId);
    }
}