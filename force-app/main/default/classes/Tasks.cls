public without sharing class Tasks 
{
    //for updating Task related records
    private static Map<Id,Lead> relatedLeadsMap = new Map<Id, Lead>{};
    private static Map<Id,Account> relatedAccountsMap = new Map<Id,Account>{};
    private static Map<Id,Opportunity> relatedOppsMap = new Map<Id,Opportunity>{};
    private static Map<Id, Case> relatedCasesMap = new Map<Id, Case>{};
   
    public static Lead getLead(Id leadId)
    {
        Lead l = relatedLeadsMap.get(leadId);
        if(l == null)
        {
            l = new Lead(Id = leadId);
        }
        return l;
    }
    public static void updateLead(Lead lead)
    {
        relatedLeadsMap.put(lead.Id, lead);
    }
    
    public static Account getAccount(Id accId)
    {
        Account acc = relatedAccountsMap.get(accId);
        if(acc == null)
        {
            acc = new Account(Id = accId);
        }    
        return acc;
    }
    public static void updateAccount(Account acc)
    {
        relatedAccountsMap.put(acc.Id, acc);
    }
    
    public static Opportunity getOpportunity(Id oppId)
    {
        Opportunity op = relatedOppsMap.get(oppId);
        if(op == null)
        {
            op = new Opportunity(Id = oppId);
        }     
        return op;
    }
    public static void updateOpportunity(Opportunity opp)
    {
       relatedOppsMap.put(opp.Id, opp);
    }
	
    public static Case getCase(Id caseId)
    {
        Case c = relatedCasesMap.get(caseId);
        if(c == null)
        {
            c = new Case(Id = caseId);
        }       
        return c;
    }
    public static void updateCase(Case c)
    {
       relatedCasesMap.put(c.Id, c);
    }
    

    public static void commitAfterTriggerUpdates()
    {
        if(relatedLeadsMap.size() > 0)
        update relatedLeadsMap.values();

        if(relatedAccountsMap.size() > 0)
        update relatedAccountsMap.values();

        if(relatedOppsMap.size() > 0)
        update relatedOppsMap.values();
        
        if(relatedCasesMap.size() > 0)
        update relatedCasesMap.values();
    }  
}