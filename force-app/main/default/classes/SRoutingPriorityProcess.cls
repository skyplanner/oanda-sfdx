/**
 * @File Name          : SRoutingPriorityProcess.cls
 * @Description        : 
 * Calculates the routing priority for a ServiceRoutingInfo record
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 12:58:35 PM
**/
public inherited sharing class SRoutingPriorityProcess 
	implements SRoutingPriorityCalculator {

	public static final Integer PRIORITY_15_POINTS = 15;
	public static final Integer PRIORITY_11_POINTS = 11;
	public static final Integer PRIORITY_10_POINTS = 10;
	public static final Integer PRIORITY_5_POINTS = 5;
	public static final Integer PRIORITY_2_POINTS = 2;

	public Integer getRoutingPriority(BaseServiceRoutingInfo info) {
		ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) info;
		Integer total = 0;

		if (routingInfo.createdFromChat) {
			total += PRIORITY_2_POINTS;
		}

		if (routingInfo.relatedCustomerType != null) {
			// if (routingInfo.isHVC) {
			// 	total += PRIORITY_10_POINTS;
			// 	//...
			// } else 
			if (routingInfo.isNewCustomer) {
				total += PRIORITY_5_POINTS;
				//...
			} else if (routingInfo.isActiveCustomer) {
				total += PRIORITY_2_POINTS;
				//...
			}
		}

		if (routingInfo.isAComplaint) {
			total += PRIORITY_15_POINTS;
		}

		if (routingInfo.priority == OmnichanelConst.CASE_PRIORITY_CRITICAL) {
			total += PRIORITY_15_POINTS;
			
		} else if (routingInfo.priority == OmnichanelConst.CASE_PRIORITY_HIGH) {
			total += PRIORITY_11_POINTS;
		}

		if (routingInfo.ccmCx) {
			total += PRIORITY_11_POINTS;
		}

		if (routingInfo.deposit || routingInfo.trade) {
			total += PRIORITY_5_POINTS;
		}

		Integer finalPriority = 
			OmnichanelPriorityConst.DEFAULT_PRIORITY - (total + getTierWeight(routingInfo.tier));
		return getValidRoutingPriority(finalPriority);
	}

	public static Integer getValidRoutingPriority(Integer newValue) {
		Integer result = 
			(newValue > OmnichanelPriorityConst.FINAL_PRIORITY_MIN_VALUE)
			? newValue
			: OmnichanelPriorityConst.FINAL_PRIORITY_MIN_VALUE;
		return result;
	} 

	public static Integer getTierWeight(String tier){
		Integer weight = OmnichanelPriorityConst.PRIORITY_TIER_4_WEIGHT;
		if(String.isNotBlank(tier)){
			if(tier == OmnichannelCustomerConst.TIER_1){
				weight = OmnichanelPriorityConst.PRIORITY_TIER_1_WEIGHT;
			}
			else if(tier == OmnichannelCustomerConst.TIER_2){
				weight = OmnichanelPriorityConst.PRIORITY_TIER_2_WEIGHT;
			}
			else if(tier == OmnichannelCustomerConst.TIER_3){
				weight = OmnichanelPriorityConst.PRIORITY_TIER_3_WEIGHT;
			}
		}
		return weight;
	}
	
}