/**
 * @File Name          : OnMsgSessionAssignedOrOpenedCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/1/2024, 1:09:56 PM
**/
@IsTest
private without sharing class OnMsgSessionAssignedOrOpenedCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	/**
	 * test 1
	 * The parameters correspond to a MessagingSession record that
	 * has been assigned
	 * test 2
	 * The parameters correspond to a MessagingSession record that
	 * has been opened
	 * * test 2
	 * The parameters correspond to a Non_HVC_Case__c record that
	 * has been assigned
	 */
	@IsTest
	static void checkRecord() {
		ID workItemId = null;

		Test.startTest();
		OnMsgSessionAssignedOrOpenedCmd instance = 
			new OnMsgSessionAssignedOrOpenedCmd();
		// test 1 => return true
		Boolean result1 = instance.checkRecord(
			Schema.MessagingSession.SObjectType, //workItemSObjType
			OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED, //agentWorkStatus
			workItemId // workItemId
		);
		// test 2 => return true
		Boolean result2 = instance.checkRecord(
			Schema.MessagingSession.SObjectType, //workItemSObjType
			OmnichanelConst.AGENT_WORK_STATUS_OPENED, //agentWorkStatus
			workItemId // workItemId
		);
		// test 3 => return false
		Boolean result3 = instance.checkRecord(
			Schema.Non_HVC_Case__c.SObjectType, //workItemSObjType
			OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED, //agentWorkStatus
			workItemId // workItemId
		);
		Test.stopTest();

		Assert.isTrue(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
	}

	/**
	 * test 1 : msgSessionIdSet is empty
	 * test 2 : msgSessionIdSet is not empty
	 */
	@IsTest
	static void execute() {
		Test.startTest();
		OnMsgSessionAssignedOrOpenedCmd instance = 
			new OnMsgSessionAssignedOrOpenedCmd();
		// test 1 => return false
		Boolean result1 = instance.execute();
		// test 2 => return true
		instance.msgSessionIds.add(null);
		Boolean result2 = instance.execute();
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
	}

	@IsTest
	static void processMessagingSessions() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		List<MessagingSession> messagingSessions = new List<MessagingSession> {
			new MessagingSession()
		};
		Test.startTest();
		OnMsgSessionAssignedOrOpenedCmd instance = 
			new OnMsgSessionAssignedOrOpenedCmd();
		// msgSession is only in memory
		// so Id = null and we need to do this trick
		instance.assignedMsgSessionIds.add(null); // trick
		instance.processMessagingSessions(messagingSessions);
		Test.stopTest();
		
		Assert.isFalse(instance.recordsToUpdate.isEmpty(), 'Invalid result');
	}

	/**
	 * test 1 : assigned = true, CaseId != null
	 * test 2 : assigned = true, CaseId = null
	 * test 3 : opened = true
	 */
	@IsTest
	static void processMessagingSession() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		
		Test.startTest();
		OnMsgSessionAssignedOrOpenedCmd instance1 = 
			new OnMsgSessionAssignedOrOpenedCmd();
		// test 1 => recordsToUpdate.size = 2
		MessagingSession msgSession1 = new MessagingSession (
			CaseId = case1Id
		);
		instance1.processMessagingSession(
			msgSession1, // msgSession
			true, // assigned
			false // opened
		);
		// test 2 => recordsToUpdate.size = 1
		OnMsgSessionAssignedOrOpenedCmd instance2 = 
			new OnMsgSessionAssignedOrOpenedCmd();
		MessagingSession msgSession2 = new MessagingSession();
		instance2.processMessagingSession(
			msgSession2, // msgSession
			true, // assigned
			false // opened
		);
		// test 3 => recordsToUpdate.size = 1
		OnMsgSessionAssignedOrOpenedCmd instance3 = 
			new OnMsgSessionAssignedOrOpenedCmd();
		MessagingSession msgSession3 = new MessagingSession();
		instance3.processMessagingSession(
			msgSession3, // msgSession
			false, // assigned
			true // opened
		);
		Test.stopTest();
		
		Assert.areEqual(2, instance1.recordsToUpdate.size(), 'Invalid result');
		Assert.areEqual(1, instance2.recordsToUpdate.size(), 'Invalid result');
		Assert.areEqual(1, instance3.recordsToUpdate.size(), 'Invalid result');
	}
	
}