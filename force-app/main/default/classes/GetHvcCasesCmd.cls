/**
 * @File Name          : GetHvcCasesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/17/2021, 11:25:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/17/2021, 11:11:23 PM   acantero     Initial Version
**/
public inherited sharing class GetHvcCasesCmd {

    final List<Case> caseList;
    
    List<Case> accountCaseList;
    List<Case> unknowChatCaseList;
    Set<String> accountIdSet;
    Set<String> emailSet;

    List<Case> hvcCaseList = new List<Case>();

    public GetHvcCasesCmd(List<Case> caseList) {
        this.caseList = caseList;
    }

    public static List<Case> getHvcCases(List<Case> caseList) {
        return new GetHvcCasesCmd(caseList).execute();
    }

    public List<Case> execute() {
        if (
            (caseList == null) || 
            caseList.isEmpty()
        ) {
            return hvcCaseList;
        }
        //else...
        accountCaseList = new List<Case>();
        unknowChatCaseList = new List<Case>();
        accountIdSet = new Set<String>();
        emailSet = new Set<String>();
        //...
        for(Case caseObj  : caseList) {
            if (String.isNotBlank(caseObj.AccountId)) {
                accountCaseList.add(caseObj);
                accountIdSet.add(caseObj.AccountId);
                continue;
            } 
            //else...
            //AccountId is null
            if (String.isNotBlank(caseObj.Lead__c)) {
                continue;
            }
            //else...
            checkChatCase(caseObj);
        }
        checkAccounts();
        checkEmails();
        return hvcCaseList;
    }

    Boolean checkChatCase(Case caseObj) {
        Boolean result = false;
        if (
            (caseObj.Origin == OmnichanelConst.CASE_ORIGIN_CHAT) ||
            (caseObj.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE)
        ) {
            result = true;
            if (caseObj.Chat_Is_HVC__c != null) {
                if (caseObj.Chat_Is_HVC__c == OmnichanelConst.CHAT_IS_HVC_YES) {
                    hvcCaseList.add(caseObj);
                }
                //...
            } else if (String.isNotBlank(caseObj.Chat_Email__c)){
                unknowChatCaseList.add(caseObj);
                emailSet.add(caseObj.Chat_Email__c);
            }
        } 
        return result;
    }

    void checkAccounts() {
        if (!accountIdSet.isEmpty()) {
            Set<String> hvcAccountIdSet = OmnichanelUtil.getHVCAccountIdSet(accountIdSet);
            if (!hvcAccountIdSet.isEmpty()) {
                for(Case caseObj : accountCaseList) {
                    if (hvcAccountIdSet.contains(caseObj.AccountId)) {
                        hvcCaseList.add(caseObj);
                    }
                }
            }
        }
    }

    void checkEmails() {
        if (!emailSet.isEmpty()) {
            Set<String> hvcEmailSet = OmnichanelUtil.getHVCEmailSet(emailSet);
            if (!hvcEmailSet.isEmpty()) {
                for(Case caseObj : unknowChatCaseList) {
                    if (hvcEmailSet.contains(caseObj.Chat_Email__c)) {
                        hvcCaseList.add(caseObj);
                    }
                }
            }
        }
    }
    
}