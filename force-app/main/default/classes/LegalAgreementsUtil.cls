/**
 * Legal Agreements util class
 */
public class LegalAgreementsUtil {
    
    Map<String, Legal_Agreement_Mapping__c> codeAgreeMap;

    /**
     * Constructor
     */
    public LegalAgreementsUtil() {
        // Load mapping settngs
        loadMappingSettings();
    }

    /**
     * Request legal agreements
     * Actions:
     * - Do callout
     * - Update fxAccount with response
     * - Adjust agreements from settings
     * - Build agreements relationship to
     *      present to the UI
     */
    public List<LegalAgreement> getfxAccAgreements(
        Id fxAccId)
    {
        // Retrieve the fxAccount record for update
        fxAccount__c fxAcc = getFxAcc(fxAccId);

        if(fxAcc == null) {
            return new List<LegalAgreement>();
        }

        // Do callout to get JSON legalAgreements
        String strAgs =
            new LegalAgreementCallout().getLegalAgreements(
                fxAcc.fxTrade_User_Id_Text__c);
       
        // Build legal agreements from str data 
        List<LegalAgreement> ags =
            getAgreements(strAgs);

        Logger.debug('ags', '', ags);

        // Return agreements with relationship
        return getAgreementsRelationship(ags);
    }

    /**
     * Retrieve the fxAccount record
     */
    private fxAccount__c getFxAcc(Id fxAccId) {
        List<fxAccount__c> fxas = 
            [SELECT Id,
                    fxTrade_User_Id_Text__c
                FROM fxAccount__c
                WHERE Id = :fxAccId
                AND fxTrade_User_Id__c != null
                FOR UPDATE];

        return fxas.isEmpty() ? null : fxas[0];
    }

    /**
     * Retrieve legal aggrement list from fxAccount
     */
    private List<LegalAgreement> getAgreements(
        String strAgs)
    {
        return String.isBlank(strAgs) ? 
            new List<LegalAgreement>() :
            (List<LegalAgreement>)JSON.deserialize(
                strAgs,
                List<LegalAgreement>.class);
    }

    /**
     * Build (parent/children) structure according
     * legal aggreement name, where the parent is 
     * the last version 
     */
    private List<LegalAgreement> getAgreementsRelationship(
        List<LegalAgreement> las)
    {
        if(las == null || las.isEmpty())
            return new List<LegalAgreement>();

        Map<String, LegalAgreement> agMap =
            new Map<String, LegalAgreement>();

        for(LegalAgreement la :las) {
            if(la == null)
                continue;
            
            // Adjust agreement from settings
            adjustAgreementFromStts(la);
                
            if(agMap.containsKey(la.name)) {
                // Get current parent
                LegalAgreement currentParent =
                    agMap.get(la.name);
                
                // If the loop agreement version is more
                // recent, it should be the new parent
                if(la.version > currentParent.version) {
                    // Define the new parent
                    LegalAgreement newParent = la;
                    // Add current parent as new parent's child
                    newParent.addChild(currentParent);
                    // Move current parent children to the new parent
                    newParent.addChildren(currentParent.children);
                    // Set the new parent in the map
                    agMap.put(newParent.name, newParent);
                } else {
                    // Add legal agreement as child
                    // to the current parent agreement
                    currentParent.addChild(la);
                }
            } else {
                agMap.put(la.name, la);
            }
        }

        return agMap.values();
    }

    /**
     * Set right agreement properties from settings 
     * mapping to present in UI
     */
    private void adjustAgreementFromStts(
        LegalAgreement ag)
    {
        // Adjust name
        ag.name = codeAgreeMap.containsKey(ag.code) ?
            codeAgreeMap.get(ag.code).Display_Name__c :
            ag.code;
        // Adjust language
        ag.language = (ag.language != null ?
            CustomSettings.getLanguageByCode(ag.language) :
            null);
        // Adjust country
        ag.country = (ag.country != null ?
            CustomSettings.getCountrySettingByCode(ag.country).Name :
            null);             
        
        // Add more here...
    }

    /**
     * Load settings
     */
    private void loadMappingSettings() {
        codeAgreeMap = new Map<String, Legal_Agreement_Mapping__c>();
        
        for(Legal_Agreement_Mapping__c ag : Legal_Agreement_Mapping__c.getAll().values()){
            codeAgreeMap.put(String.valueOf(ag.Code__c.intValue()), ag);
        }
    }

}