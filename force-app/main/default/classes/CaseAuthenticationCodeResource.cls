/**
 * @author Fernando Gomez
 * @since 5/7/2021
 */
public without sharing class CaseAuthenticationCodeResource {
	private Case aCase;
	private AuthenticationCodeGenerator generator;

	private static final String DEFAULT_TEMPLATE_NAME = 'Case_Authentication_Code';
    private static final String EMAIL_ADDRESS = Settings__c.getValues('Default')?.Auth_code_OrgWideEmail__c;

	/**
	 * Main constuctor
	 */
	public CaseAuthenticationCodeResource(Id caseId) {
		SObject record;
		aCase = getCase(caseId);

		if (aCase.fxAccount__r != null)
			record = aCase.fxAccount__r;
		else
			record = aCase.Account;

		generator = new AuthenticationCodeGenerator(
			record,
			'Authentication_Code__c',
			'Authentication_Code_Expires_On__c');
	}

	/**
	 * @param sendCodeByEmail
	 * @return an existing and valid,
	 * or a newly generated auth code
	 * linked to the case
	 */
	public AuthenticationCode generateCode(Boolean sendCodeByEmail) {
		AuthenticationCode code;
		
		// we retrieve or generate the code
		code = generator.getCode(true);

		// we send the email if needed
		if (sendCodeByEmail)
			sendEmail();

		return code;
	}

	/**
	 * Send the current auth code in the case
	 * to the case's contact
	 */
	public void sendEmail() {
		EmailTemplate template;

		template = EmailTemplateUtil.getTemplateByLanguage(
			DEFAULT_TEMPLATE_NAME,
			aCase.Account.Language_Preference__pc);
		
		EmailUtil.sendEmailByTemplate(aCase.ContactId,
			// we use the default template
			template != null ? template.DeveloperName : DEFAULT_TEMPLATE_NAME,
			aCase.Id,
			true,
			EMAIL_ADDRESS);
	}

	/**
	 * SP-9521
	 * Send a Client Authentication Code to the specified Case's
	 * current contact.
	 * @param caseId
	 */
	@AuraEnabled
	public static void sendAuthenticationCodeEmail(Id caseId) {
		CaseAuthenticationCodeResource resource;
		try {
			resource = new CaseAuthenticationCodeResource(caseId);
			resource.sendEmail();
		} catch (Exception ex) {
			System.debug(ex);
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @return the case in question
	 */
	private Case getCase(Id caseId) {
		return [
			SELECT Id,
				ContactId,
				Account.Language_Preference__pc,
				fxAccount__r.Id,
				fxAccount__r.Authentication_Code__c,
				fxAccount__r.Authentication_Code_Expires_On__c,
				Account.Id,
				Account.Authentication_Code__c,
				Account.Authentication_Code_Expires_On__c
			FROM Case
			WHERE Id = :caseId
		];
	}
}