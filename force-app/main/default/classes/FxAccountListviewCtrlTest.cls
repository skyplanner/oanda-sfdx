@IsTest
private class FxAccountListviewCtrlTest {
    @isTest
    private static void validateFxAccountTest(){
        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];
        Account acc1;
        acc1 = new Account(
            FirstName = 'testname1',
            LastName = 'abc',
            Phone = '3051234567',
            PersonEmail = 'person@spemail.com',
            RecordTypeId = personAccountRecordType.Id
        );
        insert acc1;
        Account a = new Account(
            LastName='test account', 
            Parent_Account__c = acc1.Id
        );
        insert a;
        fxAccount__c liveAccount1 = new fxAccount__c();
        liveAccount1.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount1.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount1.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount1.Account__c = a.Id;
        liveAccount1.Government_ID__c = '12333321012';
        insert liveAccount1;
        Account a2 = new Account(
            LastName='test account', 
            Parent_Account__c = acc1.Id
        );
        insert a2;
        fxAccount__c liveAccount2 = new fxAccount__c();
        liveAccount2.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount2.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount2.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount2.Account__c = a2.Id;
        liveAccount2.Government_ID__c = '12333321012';
        insert liveAccount2;
        Test.startTest();
            List<FxAccountListviewCtrl.AccountWrapper> fxAccounts = FxAccountListviewCtrl.getFxAccounts(acc1.Id);
            System.assert(!fxAccounts.isEmpty(), 'There must be 2 fxAccounts');
        Test.stopTest();
    }

    @isTest
    private static void validateFxAccountFailedTest(){
        Test.startTest();
            Boolean thrownException = false;
            try{
                List<FxAccountListviewCtrl.AccountWrapper> fxAccounts = FxAccountListviewCtrl.getFxAccounts(null);
            }
            catch(AuraHandledException ex){
                thrownException = true;
            }
            System.assert(thrownException, 'The exception was not thrown.');
        Test.stopTest();
    }
}