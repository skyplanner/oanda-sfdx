/**
 * @File Name          : FxAccountMIFIDHelper.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   acantero     Initial Version
**/
public without sharing class FxAccountMIFIDHelper {

    public static Boolean expValHasChanged(
        MiFID_Validation_Result__c fxAccountValidResult,
        Map<String,ExpressionValidationWrapper> expValMap
    ) {
        Datetime expValLastModifiedDate = 
            getExpValLastModifiedDate(expValMap);
        Boolean result =  (
            (fxAccountValidResult == null) ||
            (
                (expValLastModifiedDate != null) &&
                (fxAccountValidResult.LastModifiedDate <= expValLastModifiedDate)
            )
        );
        
        return result;
    }

    public static Datetime getExpValLastModifiedDate(
        Map<String,ExpressionValidationWrapper> aExpValMap
    ) {
        if (
            (aExpValMap == null) || 
            (aExpValMap.isEmpty())
        ) {
            return null;
        }
        //else...
        Datetime result = null;
        for(ExpressionValidationWrapper expVal : aExpValMap.values()) {
            if (
                (result == null) ||
                (
                    (expVal.lastModifiedDate != null) &&
                    (result < expVal.lastModifiedDate)
                )
            ) {
                result = expVal.lastModifiedDate;
            }
        }
        return result;
    }

    public static String getSubjectForValError(
        String currentSubject, 
        MiFIDExpValConst.MiFIDValType valType,
        Boolean previuosErrorWasConditional,
        Boolean currentErrorIsConditional
    ) {
        if (
            (currentSubject != null) &&
            (previuosErrorWasConditional != true) &&
            (currentErrorIsConditional == true)
        ) {
            return currentSubject;
        }
        //else...
        String result = null;
        //conditionals trick
        if (
            (currentSubject != null) &&
            (previuosErrorWasConditional == true) &&
            (currentErrorIsConditional == false)
        ) {
            //clean subject created by conditional error
            currentSubject = null; 
        }
        //i can check now
        if (currentSubject != null) {
            result = System.Label.MifidValCaseSubjectAll;
            //...
        } else if (valType == MiFIDExpValConst.MiFIDValType.NATIONAL_ID) {
            result = System.Label.MifidValCaseSubject;
            //...
        } else if (valType == MiFIDExpValConst.MiFIDValType.PASSPORT) {
            result = System.Label.MifidValCaseSubjectPassport;
        }
        return result;
    }

    public static String addErrorMessage(
        String currentError, 
        String newValError, 
        MiFIDExpValConst.MiFIDValType valType,
        Boolean previuosErrorWasConditional,
        Boolean currentErrorIsConditional
    ) {
        if (
            (currentError != null) &&
            (previuosErrorWasConditional != true) &&
            (currentErrorIsConditional == true)
        ) {
            return currentError;
        }
        //else...
        String fieldLabel;
        if (valType == MiFIDExpValConst.MiFIDValType.NATIONAL_ID) {
            fieldLabel = System.Label.MifidValNatPersonalId;
            //...
        } else if (valType == MiFIDExpValConst.MiFIDValType.PASSPORT) {
            fieldLabel = System.Label.MifidValPassport;
        }
        newValError = fieldLabel + ' : ' + newValError;
        if (
            (currentError == null) ||
            (
                (previuosErrorWasConditional == true) &&
                (currentErrorIsConditional == false)
            )
        ) {
            return newValError;
        }
        //else...
        String result = (currentError + ', \n' + newValError);
        return result;
    }

    public static Boolean checkDivision(String divisionName){
        if(String.isBlank(divisionName))
          return false;

        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        if (
            (valSettings.accountDivisionNames != null) &&
            (!valSettings.accountDivisionNames.isEmpty())
        ) {
           for(String divisionSett : valSettings.accountDivisionNames){
               if(divisionSett == divisionName)
                 return true;
           }
           //if not included
           return false;
        }
        //else
        return true;
    }

}