/**
 * @File Name          : RemoveNonHvcCasesCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/17/2024, 1:26:15 PM
**/
@IsTest
private without sharing class RemoveNonHvcCasesCmdTest {

	/**
	 * Three Non_HVC_Case__c records are created
	 * with status = Released
	 */
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_1, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_2, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_3, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase2(initManager);
		ServiceTestDataFactory.createNonHVCCase3(initManager);
		initManager.storeData();
	}

	/**
	 * 3 records match the filters
	 * recordsLimit = 2
	 * => completed = false
	 * => count = 2
	 * => there is 1 record of Non_HVC_Case__c left
	 */
	@IsTest
	static void execute1() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		Test.startTest();
		RemoveNonHvcCasesCmdSettings.recordsLimit = 2;
		RemoveNonHvcCasesCmdSettings.lastModifiedDate = Datetime.now();
		RemoveNonHvcCasesCmd instance = new RemoveNonHvcCasesCmd();
		instance.execute();
		Test.stopTest();

		Integer nonHvcCount = [SELECT count() FROM Non_HVC_Case__c];
		
		Assert.isFalse(instance.completed, 'Invalid value');
		Assert.areEqual(2, instance.count, 'Invalid value');
		Assert.areEqual(1, nonHvcCount, 'Invalid value');
	}

	/**
	 * 3 records match the filters
	 * recordsLimit = 5
	 * => completed = true
	 * => count = 3
	 * => there are no records left of Non_HVC_Case__c 
	 */
	@IsTest
	static void execute2() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		Test.startTest();
		RemoveNonHvcCasesCmdSettings.recordsLimit = 5;
		RemoveNonHvcCasesCmdSettings.lastModifiedDate = Datetime.now();
		RemoveNonHvcCasesCmd instance = new RemoveNonHvcCasesCmd();
		instance.execute();
		Test.stopTest();

		Integer nonHvcCount = [SELECT count() FROM Non_HVC_Case__c];
		
		Assert.isTrue(instance.completed, 'Invalid value');
		Assert.areEqual(3, instance.count, 'Invalid value');
		Assert.areEqual(0, nonHvcCount, 'Invalid value');
	}

	/**
	 * 0 records match the filters
	 * recordsLimit = 5
	 * => completed = true
	 * => count = 0
	 * => there are 3 Non_HVC_Case__c records left 
	 */
	@IsTest
	static void execute3() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		Test.startTest();
		RemoveNonHvcCasesCmdSettings.recordsLimit = 5;
		RemoveNonHvcCasesCmdSettings.lastModifiedDate = 
			Datetime.now().addDays(-1);
		RemoveNonHvcCasesCmd instance = new RemoveNonHvcCasesCmd();
		instance.execute();
		Test.stopTest();

		Integer nonHvcCount = [SELECT count() FROM Non_HVC_Case__c];
		
		Assert.isTrue(instance.completed, 'Invalid value');
		Assert.areEqual(0, instance.count, 'Invalid value');
		Assert.areEqual(3, nonHvcCount, 'Invalid value');
	}
	
}