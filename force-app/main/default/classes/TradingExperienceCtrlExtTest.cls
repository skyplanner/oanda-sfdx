/*
 * @Author : Deepak Malkani
 * @Created Date : March 23 2017
 * @Purpose : Test class for Visual Force Page Controller Extension : TradingExperienceCtrlExt
*/

@isTest
private class TradingExperienceCtrlExtTest
{
	@testSetup
	static void init(){

		TestDataFactory.createTradingExpDurnCodesSett();
		TestDataFactory.createTradingExpTypeCodesSett();
		TestDataFactory testHandler = new TestDataFactory();
		Account accnt = testHandler.createTestAccount();
		fxAccount__c fxAccnt = testHandler.createFXTradeAccount(accnt);
	}

	@isTest
	static void validate_ExperiencePageLoad_1()
	{
		
		//Start Assertion before VF Page Runs
		System.assertEquals(1, [SELECT Count() FROM fxAccount__c]);
		fxAccount__c fxAccnt = [SELECT Id, Name FROM fxAccount__c][0];
		fxAccnt.Trading_Experience_Duration__c = '1,2';
		fxAccnt.Trading_Experience_Type__c = '1,2';
		Test.startTest();
		update fxAccnt;
		PageReference pg = Page.TradingExperienceVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(fxAccnt);
		TradingExperienceCtrlExt contHandler = new TradingExperienceCtrlExt(stdCtrl);
		Test.stopTest();

		System.assertEquals(2, contHandler.getTradingExperience().size());
		System.assertEquals('Test1', contHandler.getTradingExperience()[0].expDuration);
		System.assertEquals('Test2', contHandler.getTradingExperience()[1].expDuration);

	}

	@isTest
	static void validate_ExperiencePageLoad_2()
	{
		
		//Start Assertion before VF Page Runs
		System.assertEquals(1, [SELECT Count() FROM fxAccount__c]);
		fxAccount__c fxAccnt = [SELECT Id, Name FROM fxAccount__c][0];
		fxAccnt.Trading_Experience_Duration__c = '3,4';
		fxAccnt.Trading_Experience_Type__c = '5,6';
		Test.startTest();
		update fxAccnt;
		PageReference pg = Page.TradingExperienceVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(fxAccnt);
		TradingExperienceCtrlExt contHandler = new TradingExperienceCtrlExt(stdCtrl);
		Test.stopTest();

		System.assertEquals(2, contHandler.getTradingExperience().size());
		System.assertEquals('Test3', contHandler.getTradingExperience()[0].expDuration);
		System.assertEquals('Test4', contHandler.getTradingExperience()[1].expDuration);
		System.assertEquals('Test Type 5', contHandler.getTradingExperience()[0].expType);
		System.assertEquals('Test Type 6', contHandler.getTradingExperience()[1].expType);
		System.assertEquals(true, contHandler.getHasTradingExperience());
	}

	@isTest
	static void validate_ExperiencePageLoad_negative(){

		System.assertEquals(1, [SELECT Count() FROM fxAccount__c]);
		fxAccount__c fxAccnt = [SELECT Id, Name FROM fxAccount__c][0];
		Test.startTest();
		PageReference pg = Page.TradingExperienceVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(fxAccnt);
		TradingExperienceCtrlExt contHandler = new TradingExperienceCtrlExt(stdCtrl);
		Test.stopTest();
		System.assertEquals(false, contHandler.getHasTradingExperience());

	}

	@isTest
	static void validate_ExperiencePageLoad_200_Level() {
		System.assertEquals(1, [SELECT Count() FROM fxAccount__c]);
		fxAccount__c fxAccnt = [SELECT Id, Name FROM fxAccount__c][0];
		fxAccnt.Trading_Experience_Duration__c = '101,201';
		fxAccnt.Trading_Experience_Type__c = '101,201';
		Test.startTest();
		update fxAccnt;
		PageReference pg = Page.TradingExperienceVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(fxAccnt);
		TradingExperienceCtrlExt contHandler = new TradingExperienceCtrlExt(stdCtrl);
		Test.stopTest();

		System.assertEquals(1, contHandler.getTradingExperience().size());
		System.assertEquals(null, contHandler.getTradingExperience()[0].expDuration);
		System.assertEquals('201', contHandler.getTradingExperience()[0].expVolume);
	}
}