/**
 * @File Name          : ServiceQuestionsMapper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/2/2021, 12:56:33 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 1:37:00 PM   acantero     Initial Version
**/
@isTest
private without sharing class ServiceQuestionsMapper_Test {

    //live
    @isTest
    static void createFromUnmappedValues() {
        Service_Questions_Mapping__mdt mappingObj = [
            select 
                Inquiry_Nature__c, Specific_Question__c,
                Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c,
                Type__r.Type__c,
                Subtype__r.Subtype__c
            from Service_Questions_Mapping__mdt
            where Inquiry_Nature__c <> null
            and Specific_Question__c <> null
            and Live__c = true
            limit 1
        ];
        String inquiryNatureMapped = 
            mappingObj.Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c;
        String serviceType = mappingObj.Type__r.Type__c;
        String serviceSubtype = mappingObj.Subtype__r.Subtype__c;
        //...
        String inquiryNature = [
            select Inquiry_Nature__c 
            from Service_Inquiry_Nature__mdt
            where Id = :mappingObj.Inquiry_Nature__c
        ].Inquiry_Nature__c;
        System.debug('inquiryNature: ' + inquiryNature);
        //...
        String specificQuestion = [
            select Question__c
            from Service_Specific_Question__mdt
            where Id = :mappingObj.Specific_Question__c
        ].Question__c;
        System.debug('specificQuestion: ' + specificQuestion);
        //...
        Boolean live = true;
        Test.startTest();
        ServiceQuestionsMapper instance = 
            ServiceQuestionsMapper.createFromUnmappedValues(
                live,
                inquiryNature,
                specificQuestion
            );
        Test.stopTest();
        System.assertEquals(inquiryNatureMapped, instance.inquiryNatureMapped);
        System.assertEquals(serviceType, instance.serviceType);
        System.assertEquals(serviceSubtype, instance.serviceSubtype);
    }

    //practice, search by Inquiry_Nature_Mapped and type
    @isTest
    static void createFromMappedValues1() {
        Service_Questions_Mapping__mdt mappingObj = [
            select 
                Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c, 
                Type__r.Type__c, 
                Skill__r.Skill_Dev_Name__c
            from Service_Questions_Mapping__mdt
            where Inquiry_Nature_Mapped__r.Has_a_Skill__c = false
            and Type__c <> null
            and Practice__c = true
            limit 1
        ];
        String inquiryNatureMapped = 
            mappingObj.Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c;
        System.debug('inquiryNatureMapped: ' + inquiryNatureMapped);
        //...
        String skillDevName = 
            mappingObj.Skill__r.Skill_Dev_Name__c;
        System.debug('skillDevName: ' + skillDevName);
        //...
        String serviceType = mappingObj.Type__r.Type__c;
        System.debug('serviceType: ' + serviceType);
        //...
        Boolean live = false;
        Test.startTest();
        ServiceQuestionsMapper instance = 
            ServiceQuestionsMapper.createFromMappedValues(
                live,
                inquiryNatureMapped,
                serviceType
            );
        Test.stopTest();
        System.assertEquals(skillDevName, instance.skill);
    }

    //live, search only by Inquiry_Nature_Mapped 
    @isTest
    static void createFromMappedValues2() {
        Service_Questions_Mapping__mdt mappingObj = [
            select 
                Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c,  
                Skill__r.Skill_Dev_Name__c
            from Service_Questions_Mapping__mdt
            where Inquiry_Nature_Mapped__r.Has_a_Skill__c = true
            and Live__c = true
            limit 1
        ];
        String inquiryNatureMapped = 
            mappingObj.Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c;
        System.debug('inquiryNatureMapped: ' + inquiryNatureMapped);
        //...
        String skillDevName = 
            mappingObj.Skill__r.Skill_Dev_Name__c;
        System.debug('skillDevName: ' + skillDevName);
        //...
        Boolean live = true;
        Test.startTest();
        ServiceQuestionsMapper instance = 
            ServiceQuestionsMapper.createFromMappedValues(
                live,
                inquiryNatureMapped,
                null //serviceType
            );
        Test.stopTest();
        System.assertEquals(skillDevName, instance.skill);
    }

    //practice, invalid Inquiry_Nature_Mapped
    @isTest
    static void createFromMappedValues3() {
        Boolean live = true;
        Test.startTest();
        ServiceQuestionsMapper instance = 
            ServiceQuestionsMapper.createFromMappedValues(
                live,
                'fake inquiry nature mapped',
                null //serviceType
            );
        Test.stopTest();
        System.assertEquals(null, instance.skill);
    }

    @isTest
    static void getSubtypeByAgentList() {
        Test.startTest();
        List<SubtypeByAgentInfo> result = 
            ServiceQuestionsMapper.getSubtypeByAgentList();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void getInquiryNatureId() {
        Boolean throwException = false;
        String result1, result2;
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        result1 = instance.getInquiryNatureId(null);
        try {
            result2 = instance.getInquiryNatureId('fake inquiry nature');
            //...
        } catch (ServiceQuestionsMapper.ServiceQuestionsMapperException ex) {
            throwException = true;
        }
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(true, throwException);
    }

    @isTest
    static void getSpecificQuestionId() {
        Boolean throwException = false;
        String result1, result2;
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        result1 = instance.getSpecificQuestionId(null);
        try {
            result2 = instance.getSpecificQuestionId('fake specific question');
            //...
        } catch (ServiceQuestionsMapper.ServiceQuestionsMapperException ex) {
            throwException = true;
        }
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(true, throwException);
    }

    @isTest
    static void getInquiryNatureMappedObj() {
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        Service_Inquiry_Nature_Mapped__mdt result1 = instance.getInquiryNatureMappedObj(null);
        Service_Inquiry_Nature_Mapped__mdt result2 = instance.getInquiryNatureMappedObj('fake inquiry nature mapped');
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
    }

    @isTest
    static void getTypeId() {
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        String result1 = instance.getTypeId(null);
        String result2 = instance.getTypeId('fake service type');
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
    }

    //defaultSkill exists
    @isTest
    static void processMappedValues1() {
        String fakeSkillDevName= 'fakeSkillDevName';
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        instance.inquiryNatureMappedHasSkill = false;
        instance.defaultSkill = fakeSkillDevName;
        instance.processMappedValues(null);
        Test.stopTest();
        System.assertEquals(fakeSkillDevName, instance.skill);
    }

    //live, valid Inquiry_Nature_Mapped (no skill) and invalid type
    @isTest
    static void processMappedValues2() {
        Service_Questions_Mapping__mdt mappingObj = [
            select Inquiry_Nature_Mapped__c
            from Service_Questions_Mapping__mdt
            where Inquiry_Nature_Mapped__r.Has_a_Skill__c = false
            and Type__c <> null
            and Live__c = true
            limit 1
        ];
        //...
        Boolean live = true;
        Test.startTest();
        ServiceQuestionsMapper instance = new ServiceQuestionsMapper(true);
        instance.inquiryNatureMappedHasSkill = false;
        instance.defaultSkill = null;
        instance.inquiryNatureMappedId = mappingObj.Inquiry_Nature_Mapped__c;
        instance.processMappedValues('fake type');
        Test.stopTest();
        System.assertEquals(null, instance.skill);
    }
    
}