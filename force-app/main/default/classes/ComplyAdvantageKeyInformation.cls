/**
 * Wrapper for Comply advantage JSON: key_information
 * @author Fernando Gomez
 */
public class ComplyAdvantageKeyInformation {
	@AuraEnabled
	public String name { get; set; }

	@AuraEnabled
	public String entity_type { get; set; }
	
	@AuraEnabled
	public List<String> types { get; set; }
	
	@AuraEnabled
	public List<String> match_types { get; set; }
	
	@AuraEnabled
	public List<String> sources { get; set; }

	@AuraEnabled
	public String notes { get; set; }
	
	@AuraEnabled
	public String address { get; set; }
	
	@AuraEnabled
	public String phone { get; set; }
	
	@AuraEnabled
	public String email { get; set; }

	@AuraEnabled
	public String user_defined_id { get; set; }
		
	@AuraEnabled
	public String date_of_birth { get; set; }

	@AuraEnabled
	public Integer age { get; set; }
		
	@AuraEnabled
	public String match_status { get; set; }
	
	@AuraEnabled
	public String risk_level { get; set; }
	
	@AuraEnabled
	public Boolean is_whitelisted { get; set; }
	
	@AuraEnabled
	public String primary_country { get; set; }
	
	@AuraEnabled
	public String country_names { get; set; }

	@AuraEnabled
	public List<Map<String, String>> aka { get; set; }
}