/**
 * @File Name          : CreateNonHvcCaseManagerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/5/2024, 12:06:22 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/7/2024, 12:02:22 PM   aniubo     Initial Version
 **/
@isTest
private class CreateNonHvcCaseManagerTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_1,
			'Status__c',
			OmnichanelConst.NON_HVC_CASE_STATUS_PARKED
		);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		ServiceTestDataFactory.createUser1(initManager);
		initManager.storeData();
	}
	@isTest
	private static void testNonHvcCaseReleased() {
		// Test data setup
		CreateNonHvcCaseManagerWrapper wrapper = intiTestData(null);
		String newStatusId = ServicePresenceStatusHelper.getByDevName(
				wrapper.statusPair.Status_not_allow__c,
				true
			)
			.Id;
		NonHvcCaseAttemptInfo info;
		Test.startTest();
		System.runAs(wrapper.testUser) {
			CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();
			info = manager.newNonHvcCase(
				wrapper.statusPair.Status_allowing__c
			);
		}
		Test.stopTest();
		System.assertEquals(
			false,
			info.canBeAssigned,
			'Agent can no longer be assigned cases'
		);
		System.assertEquals(newStatusId, info.newStatusId, 'Same status id');
		// Asserts
	}

	@IsTest
	static void testOnNonHvcCaseReleased() {
		CreateNonHvcCaseManagerWrapper wrapper = intiTestData(0);
		String newStatusId = ServicePresenceStatusHelper.getByDevName(
				wrapper.statusPair.Status_allowing__c,
				true
			)
			.Id;
		String result;
		Test.startTest();
		System.runAs(wrapper.testUser) {
			CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();
			result = manager.nonHvcCaseReleased(
				wrapper.statusPair.Status_not_allow__c
			);
		}
		Test.stopTest();
		System.assertEquals(newStatusId, result, 'Same status id');
	}

	@IsTest
	static void testDeclineNonHvcCases() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id nonHvcCaseId = initManager.getObjectId(
			ServiceTestDataKeys.NON_HVC_CASE_1,
			true
		);
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Test.startTest();
		List<String> nonHvcCaseIdList = new List<String>{ nonHvcCaseId };
		CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();
		manager.declineNonHvcCases(nonHvcCaseIdList);
		Test.stopTest();
		List<Non_HVC_Case__c> oldNonHvCaseList = [
			SELECT Id
			FROM Non_HVC_Case__c
			WHERE Id = :nonHvcCaseId
			LIMIT 1
		];
		System.assert(
			oldNonHvCaseList.isEmpty(),
			'Non HVC Case Must be deleted'
		);
		//...
		List<Non_HVC_Case__c> newNonHvCaseList = [
			SELECT Id
			FROM Non_HVC_Case__c
			WHERE Case__c = :caseId
			LIMIT 1
		];
		System.assertEquals(
			false,
			newNonHvCaseList.isEmpty(),
			'on HVC Case Must be deleted'
		);
	}

	private static Non_HVC_Cases_Status_Pair__mdt getStatusPair() {
		return [
			SELECT Status_allowing__c, Status_not_allow__c
			FROM Non_HVC_Cases_Status_Pair__mdt
			WHERE Bidirectional__c = TRUE AND 
			Max_Number_Cases__c != null AND
			Max_Number_Cases__c > 0 AND 
			Min_Occupancy_Percentage__c != null AND
			Min_Occupancy_Percentage__c >0
			LIMIT 1
		];
	}
	private static CreateNonHvcCaseManagerWrapper intiTestData(
		Integer nonHvcCases
	) {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Non_HVC_Cases_Status_Pair__mdt statusPair = getStatusPair();
		NonHVCCasesStatusPairManager statusPairManager = new NonHVCCasesStatusPairManager();
		NonHVCCasesStatusPairSettings settings = statusPairManager.getByStatusNotAllowing(
			statusPair.Status_not_allow__c
		);
		Integer maxNonHvcCases = settings != null &&  settings.maxNonHvcCases > 0 
		? settings.maxNonHvcCases 
		: CreateNonHvcCaseManager.MAX_NON_HVC_CASES;
		initManager.setFieldValue(
			ServiceTestDataKeys.AGENT_EXTRA_INFO_1,
			'Non_HVC_Cases__c',
			nonHvcCases != null ? nonHvcCases : maxNonHvcCases
		);
		
		ServiceTestDataFactory.createAgentExtraInfo1(initManager);
		Id userId = initManager.getObjectId(ServiceTestDataKeys.USER_1, true);
		// Actual test
		User testUser = new User(Id = userId);
		return new CreateNonHvcCaseManagerWrapper(statusPair, testUser);
	}
	public class CreateNonHvcCaseManagerWrapper {
		public CreateNonHvcCaseManagerWrapper(
			Non_HVC_Cases_Status_Pair__mdt statusPair,
			User testUser
		) {
			this.statusPair = statusPair;
			this.testUser = testUser;
		}
		public Non_HVC_Cases_Status_Pair__mdt statusPair { get; set; }
		public User testUser { get; set; }
	}
}