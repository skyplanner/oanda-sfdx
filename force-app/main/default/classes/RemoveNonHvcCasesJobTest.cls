/**
 * @File Name          : RemoveNonHvcCasesJobTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/18/2024, 11:45:54 PM
**/
@IsTest
private without sharing class RemoveNonHvcCasesJobTest {

	/**
	 * Three Non_HVC_Case__c records are created
	 * with status = Released
	 */
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		LoggerTestDataFactory.enableLogSettings();
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_1, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_2, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_3, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase2(initManager);
		ServiceTestDataFactory.createNonHVCCase3(initManager);
		initManager.storeData();
	}

	/**
	 * 3 records match the filters
	 * recordsLimit = 2
	 * => count = 2
	 * => there is 1 record of Non_HVC_Case__c left
	 */
	@IsTest
	static void execute1() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		Test.startTest();
		RemoveNonHvcCasesCmdSettings.recordsLimit = 2;
		RemoveNonHvcCasesCmdSettings.lastModifiedDate = Datetime.now();
		RemoveNonHvcCasesJob instance = new RemoveNonHvcCasesJob();
		System.enqueueJob(instance);
		Test.stopTest();

		Assert.isTrue(true, 'Invalid value');

		Integer nonHvcCount = [SELECT count() FROM Non_HVC_Case__c];
		Boolean areErrorsOrExceptions = 
			LoggerTestDataFactory.areErrorsOrExceptions();
		
		Assert.isFalse(areErrorsOrExceptions, 'Invalid value');
		Assert.areEqual(1, nonHvcCount, 'Invalid value');
	}

	/**
	 * exception
	 */
	@IsTest
	static void execute2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Boolean error = false;

		try {
			Test.startTest();
			RemoveNonHvcCasesCmdSettings.recordsLimit = 2;
			RemoveNonHvcCasesCmdSettings.lastModifiedDate = Datetime.now();
			RemoveNonHvcCasesJob instance = new RemoveNonHvcCasesJob();
			ExceptionTestUtil.prepareSpException();
			System.enqueueJob(instance);
			Test.stopTest();

		} catch (SPException ex) {
			error = true;
		}

		Integer nonHvcCount = [SELECT count() FROM Non_HVC_Case__c];
		
		Assert.isTrue(error, 'Invalid value');
		Assert.areEqual(3, nonHvcCount, 'Invalid value');
	}
	
}