/**
 * Wrapper for Comply advantage JSON: Search Details - DOC
 * @author Fernando Gomez
 */
public class ComplyAdvantageSearchDoc {
	@AuraEnabled
	public List<String> keywords { get; set; }

	@AuraEnabled
	public List<Map<String, String>> media { get; set; }

	@AuraEnabled
	public List<Map<String, String>> assets { get; set; }
}