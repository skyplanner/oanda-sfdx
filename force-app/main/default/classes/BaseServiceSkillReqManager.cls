/**
 * @File Name          : BaseServiceSkillReqManager.cls
 * @Description        : 
 * Returns the list of SkillRequirements corresponding to 
 * routing information records
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 5:18:58 PM
**/
public inherited sharing abstract class BaseServiceSkillReqManager 
	implements SSkillRequirementsManager {

	@TestVisible
	protected Set<String> globalSkillNameSet;

	@TestVisible
	protected abstract List<SSkillRequirementInfo> getSkillsInfo(
		BaseServiceRoutingInfo routingInfo
	);

	public List<SkillRequirement> getSkillRequirements(
		List<BaseServiceRoutingInfo> infoList
	) {
		List<SkillRequirement> result = new List<SkillRequirement>();
		List<SSkillRequirementInfo> allSkillsInfo = 
			new List<SSkillRequirementInfo>();
		globalSkillNameSet = new Set<String>();

		for (BaseServiceRoutingInfo info : infoList) {
			List<SSkillRequirementInfo> skillsInfoList = getSkillsInfo(info);
			
			if (!skillsInfoList.isEmpty()) {
				allSkillsInfo.addAll(skillsInfoList);
			}
		}

		if (allSkillsInfo.isEmpty()) {
			return result;
		}
		// else...
		setSkillsPriorities(allSkillsInfo);
		Map<String, ID> skillMap = getSkillMap(globalSkillNameSet);

		ServiceLogManager.getInstance().log(
			'getSkillRequirements -> allSkillsInfo', // msg
			BaseServiceSkillReqManager.class.getName(), // category
			allSkillsInfo // details
		);

		for (SSkillRequirementInfo skillInfo : allSkillsInfo) {
			ID skillId = skillMap.get(skillInfo.skillName);

			if (skillId != null) {
				SkillRequirement skillReq = skillInfo.skillReq;
				skillReq.SkillId = skillId;
				result.add(skillReq);
			}
		}

		ServiceLogManager.getInstance().log(
			'getSkillRequirements -> result', // msg
			BaseServiceSkillReqManager.class.getName(), // category
			result // details
		);

		return result;
	}

	protected virtual void setSkillsPriorities(
		List<SSkillRequirementInfo> skillsInfo
	) {
		// To be overridden in child classes
	}

	@TestVisible
	Map<String, ID> getSkillMap(Set<String> skillNameSet) {
		Map<String, ID> skillMap = new Map<String, ID>();
		List<Skill> skillList = [
			SELECT DeveloperName
			FROM Skill
			WHERE DeveloperName IN :skillNameSet
		];

		for (Skill skillObj : skillList) {
			skillMap.put(skillObj.DeveloperName, skillObj.Id);
		}
		return skillMap;
	}
	
}