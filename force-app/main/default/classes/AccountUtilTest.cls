/**
 */
@isTest
private class AccountUtilTest {
    
    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    static testMethod void testUpdateFxAccountOwner() {
        
        System.runAs(SYSTEM_USER) {
            
            Account a = new Account(LastName='test account');
            insert a;
            
            Opportunity o = new Opportunity(Name='test opp', StageName='test stage', CloseDate=Date.today(), AccountId=a.Id);
            insert o;
        
            Lead l = new Lead(LastName='test lead', Email='j@example.org');
            insert l;
        
            fxAccount__c fxa = new fxAccount__c(Name='test fxAccount', Account__c=a.Id, Opportunity__c=o.Id, Lead__c=l.Id);
            insert fxa;
        
            User u = UserUtil.getTestUser();
            insert u;
            
            Test.startTest();
            a.OwnerId = u.Id;
            checkRecursive.SetOfIDs = new Set<Id>();
            update a;
            Test.stopTest();
            
            System.assertEquals(u.Id, [SELECT OwnerId FROM fxAccount__c WHERE Id =: fxa.Id][0].OwnerId);
        }
    }
    
    static testMethod void testUpdateFxAccountOwner2() {
        User u = UserUtil.getTestUser();
        insert u;
        
        Account a = new Account(LastName='test account', OwnerId=u.Id);
        insert a;
        
        Opportunity o = new Opportunity(Name='test opp', StageName='test stage', CloseDate=Date.today(), AccountId=a.Id);
        insert o;
        
        Lead l1 = new Lead(LastName='test lead', Email='j@example.org');
        Lead l2 = new Lead(LastName='test lead', Email='j2@example.org', OwnerId=u.Id);
        insert new Lead[] {l1, l2};
        
        
        Test.startTest();
        fxAccount__c fxa1 = new fxAccount__c(Name='test fxAccount', Account__c=a.Id, Opportunity__c=o.Id, Lead__c=l1.Id);
        fxAccount__c fxa2 = new fxAccount__c(Name='test fxAccount', Lead__c=l2.Id);
        insert new fxAccount__c[] {fxa1, fxa2};
        Test.stopTest();
        
        System.assertEquals(u.Id, [SELECT OwnerId FROM fxAccount__c WHERE Id =: fxa1.Id][0].OwnerId);
        System.assertEquals(u.Id, [SELECT OwnerId FROM fxAccount__c WHERE Id =: fxa2.Id][0].OwnerId);
    }

    static testMethod void testUpdateCRAFromAccount(){
        TestDataFactory testHandler =
            new TestDataFactory();

        insert
            new Country_Setting__c(
                Name='Canada',
                Group__c='Canada',
                ISO_Code__c='ca',
                Region__c='North America',
                Zone__c='Americas',
                Risk_Rating__c='LOW');

        Account account = new Account(
            FirstName = 'First1',
            LastName='Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active',
            PersonMailingCountry = 'Cameroon');
        insert account;

        Contact contact = new Contact(
            MailingCountry = 'Canada',
            LastName ='test',
            Account = account);
        insert contact;

        insert new fxAccount__c(
            Account__c = account.Id,
            Contact__c = contact.Id,
            Funnel_Stage__c = FunnelStatus.AWAITING_CLIENT_INFO,
            RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
            Division_Name__c = 'OANDA Corporation',
            Employment_Status__c = 'Unemployed',
            Citizenship_Nationality__c = 'Canada',
            Customer_Risk_Assessment_Text__c = null);

        fxAccount__c fxa =
            [SELECT Id,
                    Customer_Risk_Assessment_Text__c
                FROM fxAccount__c
                WHERE Account__c = :account.Id
                LIMIT 1];

        System.assertEquals(
            fxa.Customer_Risk_Assessment_Text__c,
            null);

        Test.startTest();

        checkRecursive.SetOfIDs = new Set<Id>();

        account.PersonMailingCountry = 'Canada';
        update account;

        Test.stopTest();

        fxa =
            [SELECT Id,
                    Customer_Risk_Assessment_Text__c
                FROM fxAccount__c
                WHERE Account__c = :account.Id
                LIMIT 1];

        System.assertNotEquals(
            fxa.Customer_Risk_Assessment_Text__c,
            null);
        System.assertNotEquals(
            'Incomplete Data',
            fxa.Customer_Risk_Assessment_Text__c);
    }

	/**
	 * public static Boolean isDeletionAllowed()
	 */
	@isTest
	static void isDeletionAllowed() {
		System.assert(AccountUtil.isDeletionAllowed() != null);
	}

	/**
	 * public static void preventDeletion(List<Account> accounts)
	 */
	@isTest
	static void preventDeletion() {
		Account account = new Account(
			FirstName = 'First1',
			LastName='Last1',
			PersonEmail = 'test1@oanda.com',
			Account_Status__c = 'Active',
			PersonMailingCountry = 'Cameroon');
		insert account;
		AccountUtil.preventDeletion(new List<Account> { account });
	}

}