/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-27-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class AuditTrailManager {
    public static final Map<String, String> UPDATE_FIELD_MAP 
        = new Map<String, String> {
            Constants.API_FIELD_EMAIL 
                => System.Label.Audit_Trail_Email_Field,
            Constants.API_FIELD_NAME 
                => System.Label.Audit_Trail_Name_Field,
            Constants.API_FIELD_CITIZENSHIP 
                => System.Label.Audit_Trail_Citizenship_Field,
            Constants.API_FIELD_API_ACCESS 
                => System.Label.Audit_Trail_Api_Access_Field,
            Constants.API_FIELD_PREMIUM_ACCESS 
                => System.Label.Audit_Trail_Premium_Access_Field,
            Constants.API_FIELD_NEW_POSITIONS_LOCKED
                => System.Label.Audit_Trail_New_Positions_Locked_Field,
            UserApiUserPatchRequest.FIELD_NAME_BALANCE_INTEREST_ENABLED
                => System.Label.Audit_Trail_Balance_Interest_Enabled_Field,
            UserApiStatus.FIELD_NAME_ACCOUNT_LOCKED
                => System.Label.Audit_Trail_Account_Locked_Field,
            UserApiStatus.FIELD_NAME_TRADING_LOCKED
                => System.Label.Audit_Trail_Trading_Locked_Field,
            UserApiStatus.FIELD_NAME_DEPOSITS_LOCKED
                => System.Label.Audit_Trail_Deposits_Locked_Field,
            UserApiStatus.FIELD_NAME_WITHDRAWALS_LOCKED
                => System.Label.Audit_Trail_Withdrawals_Locked_Field,
            UserApiStatus.FIELD_NAME_TPA_AUTHORIZATION_LOCKED
                => System.Label.Audit_Trail_TPA_Authorization_Locked_Field,
            UserApiStatus.FIELD_NAME_EMAIL_VALIDATED
                => System.Label.Audit_Trail_Email_Validated_Field,
            UserApiStatus.FIELD_NAME_DOCUMENTS_APPROVED
                => System.Label.Audit_Trail_Documents_Approved_Field,
            UserApiStatus.FIELD_NAME_REGISTRATION_COMPLETED
                => System.Label.Audit_Trail_Registration_Completed_Field,
            UserApiStatus.FIELD_NAME_ACCOUNT_CLOSED
                => System.Label.Audit_Trail_Account_Closed_Field,
            UserApiStatus.FIELD_NAME_INTEREST
                => System.Label.Audit_Trail_All_Interest_Disabled_Field,
            UserApiStatus.FIELD_NAME_USER_INFO_LAST_UPDATED
                => System.Label.Audit_Trail_PIU_Outdated_Field,
            TasApiManager.FIELD_NAME_MT4_SERVER
                => System.Label.Audit_Trail_V20_Account_MT4_Server_Field,
            TasApiManager.FIELD_NAME_DIVISION_TRADING_GROUP
                => System.Label.Audit_Trail_V20_Account_Division_Trading_Group_Field,
            Constants.MT5_IS_DISABLED => Constants.MT5_IS_DISABLED,
            Constants.API_FIELD_PRICING_GROUP => Constants.API_FIELD_PRICING_GROUP,
            Constants.API_FIELD_COMMISSION_GROUP => Constants.API_FIELD_COMMISSION_GROUP,
            Constants.API_FIELD_CURRENCY_CONVERSION_GROUP => Constants.API_FIELD_CURRENCY_CONVERSION_GROUP,
            Constants.API_FIELD_FINANCING_GROUP => Constants.API_FIELD_FINANCING_GROUP
        };

    public void saveAuditTrail(String fxTradeUserId, Id fxAccountId,
        String action, AuditTrailChange change
    ) {
        saveAuditTrail(fxTradeUserId, fxAccountId, null, action,
            new List<AuditTrailChange> {change});
    }

    public void saveAuditTrail(String fxTradeUserId, Id fxAccountId,
        String v20AccountId, String action, AuditTrailChange change
    ) {
        saveAuditTrail(fxTradeUserId, fxAccountId, 
            v20AccountId, action, new List<AuditTrailChange> {change});
    }

    public void saveAuditTrail(String fxTradeUserId, Id fxAccountId,
        String action, List<AuditTrailChange> changes
    ) {
        saveAuditTrail(fxTradeUserId, fxAccountId, null, action, changes);
    }

    public void saveAuditTrail(
        String fxTradeUserId, Id fxAccountId, String v20AccountId,
        String action, List<AuditTrailChange> changes
    ) {
        List<Audit_Trail__c> trails = new List<Audit_Trail__c>();
        DateTime now =  DateTime.now();
        Id userId = UserInfo.getUserId();

        for (AuditTrailChange ch : changes) {
            Audit_Trail__c trail = new Audit_Trail__c(
                External_User_ID__c = fxTradeUserId,
                External_v20_Account_ID__c = v20AccountId,
                fxAccount__c = fxAccountId,
                Action__c = action,
                Field__c = ch.field,
                New_Value__c = ch.newValue,
                Original_Value__c =  ch.oldValue,
                Timestamp__c = now,
                User__c = userId
            );

            trails.add(trail);
        }

        insert trails;
    }

    public void saveAuditTrail(String fxTradeUserId,
        Map<String, Object> record, Map<String, Object> oldRecord
    ) {
        List<AuditTrailChange> changes
            = new List<AuditTrailChange>();
        String fxAccountId = UserApiActionBase.getfxAccountId(record);

        for (String key : record.keyset()) {
            if (UPDATE_FIELD_MAP.containsKey(key)) {
                AuditTrailChange change = new AuditTrailChange(
                    key, oldRecord.get(key), record.get(key));
                changes.add(change);
            }
        }

        saveAuditTrail(fxTradeUserId, fxAccountId, 'Update User', changes);
    }

    public List<Audit_Trail__c> getAuditTrails(String fxTradeUserId) {
        return [SELECT Timestamp__c, Field__c, New_Value__c, Action__c,
                Original_Value__c, User_Url__c,  User_Full_Name__c
            FROM Audit_Trail__c
            WHERE External_User_ID__c =: fxTradeUserId
            ORDER BY Timestamp__c DESC];
    }

    public class AuditTrailChange {
        public String field { get; set; }

        public String oldValue { get; set; }

        public String newValue { get; set; }

        public AuditTrailChange() {}

        public AuditTrailChange(
            String fieldKey, Object originalValue, Object newValue
        ) {
            field = UPDATE_FIELD_MAP.get(fieldKey);
            oldValue = String.valueOf(originalValue);
            this.newValue = String.valueOf(newValue);
        }
    }
}