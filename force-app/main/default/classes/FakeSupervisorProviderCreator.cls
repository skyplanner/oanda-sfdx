/**
 * @File Name          : FakeSupervisorProviderCreator.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 9:43:03 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/4/2024, 9:43:03 AM   aniubo     Initial Version
**/
@isTest
public class FakeSupervisorProviderCreator implements SupervisorProviderCreator {
	public SupervisorProvider createSupervisorProvider(
		SLAConst.AgentSupervisorModel supervisorModel
	) {
		return new FakeSupervisorProvider();
	}

	public class FakeSupervisorProvider implements SupervisorProvider {
		public void addCriteria(String criteria) {
		}

		public void getSupervisors() {
		}

		public Set<String> getSupervisors(String criteria) {
			return new Set<String>{ UserInfo.getUserId() };
		}

		public Boolean isEmpty() {
			return false;
		}
	}
}