/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 01-19-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserPrivilege {
    public String name { get; set; }

    public Integer value { get; set; }
    
    public transient Boolean enabled {
        get {
            return  value != null && value == 1;
        }
    }

    public UserApiUserPrivilege(String name, Integer value) {
        this.name = name;
        this.value = value;
    }
}