/**
 * @File Name          : OnAgentWorkStatusChangesCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/1/2024, 2:12:19 PM
**/
@IsTest
private without sharing class OnAgentWorkStatusChangesCmd_Test {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	/**
	 * No status change => return false
	 */
	@IsTest
	static void checkRecord1() {
		AgentWork rec = new AgentWork();
		AgentWork oldRec = new AgentWork();
	
		Test.startTest();
		OnAgentWorkStatusChangesCmd instance = 
			new OnAgentWorkStatusChangesCmd();
		Boolean result = instance.checkRecord(
			rec, // rec
			oldRec // oldRec
		);
		Test.stopTest();
		
		Assert.isFalse(result, 'Invalid result');
	}

	/**
	 * test 1
	 * status changed, new status = ASSIGNED => return true
	 * 
	 * test 2
	 * status changed, new status <> ASSIGNED or OPENED => return false
	 */
	@IsTest
	static void checkRecord2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		AgentWork rec = new AgentWork(
			WorkItemId = case1Id
		);
		AgentWork oldRec = new AgentWork(
			WorkItemId = case1Id
		);
	
		Test.startTest();
		OnAgentWorkStatusChangesCmd instance = 
			new OnAgentWorkStatusChangesCmd();
		// test 1 => return true
		Boolean result1 = instance.checkRecord(
			rec, // rec
			oldRec, // oldRec
			true, // statusChange
			OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED // newStatus
		);
		// test 2 => return false
		Boolean result2 = instance.checkRecord(
			rec, // rec
			oldRec, // oldRec
			true, // statusChange
			'Closed' // newStatus
		);
		Test.stopTest();
		
		Assert.isTrue(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
	}

	@IsTest
	static void execute() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		AgentWork rec = new AgentWork(
			WorkItemId = case1Id
		);
		AgentWork oldRec = new AgentWork(
			WorkItemId = case1Id
		);

		Test.startTest();
		OnAgentWorkStatusChangesCmd instance = new OnAgentWorkStatusChangesCmd();
		Boolean validRecord = instance.checkRecord(
			rec, // rec
			oldRec, // oldRec
			true, // statusChange
			OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED // newStatus
		);
		instance.execute();
		Test.stopTest();
		
		Assert.isTrue(validRecord, 'Invalid result');
		Assert.areEqual(1, instance.recordsToUpdate.size(), 'Invalid value');
	}
	
}