/**
 * Controller for page: FeedbackHome
 * @author Fernando Gomez
 * @version 1.0
 * @since Apr 10th
 */
public without sharing class FeedbackHomeCtrl extends SurveyRedirectorBase {
	public Boolean isBadParameter { get; private set; }
	public Boolean isNotAvailable { get; private set; }
	public Boolean isAlreadyTaken { get; private set; }
	public Boolean isExpired { get; private set; }
	public Boolean isAllGood { get; private set; }

	/**
	 * Main constructor. Build up link to actual survey,
	 * or reports any errors to the user.
	 */
	public FeedbackHomeCtrl() {
		Info i;
		String p, surveyName;
		Integer expireInDays;
		Survey__c currentSurvey;
		Case currentCase;
		Datetime dnow = System.now();

		// we clear all error flags
		isBadParameter = false;
		isNotAvailable = false;
		isAlreadyTaken = false;
		isAllGood = false;

		// the info is in the parameters,
		// case id, and when the link was sent....
		p = ApexPages.currentPage().getParameters().get('survey');

		// we need to watch for problems in the value,
		// since the link might be corrupted
		try {
			i = (Info)JSON.deserialize(p, Info.class);
			caseId = i.c;
		} catch(Exception ex) {
			isBadParameter = true;
			return;
		}

		// we extract the language
		language = getLanguageCode(i.l);

		if (String.isBlank(caseId) ||
				caseId.getSobjectType().getDescribe().getName() != 'Case')
			isBadParameter = true;
		else if ((currentCase = getCase(caseId)) == null ||
				String.isBlank(surveyName = getSurveyName()) ||
					(currentSurvey = getSurvey(surveyName)) == null)
			isNotAvailable = true;
		else {
			expireInDays = getExpireDays();
			
			// if the link has expired, we also show an error....
			if ((currentCase.FeedbackSentOn__c != null &&
					currentCase.FeedbackSentOn__c + expireInDays < dnow) ||
					(currentCase.FeedbackReminderSentOn__c != null &&
					currentCase.FeedbackReminderSentOn__c + expireInDays < dnow))
				isExpired = true;
			else {
				surveyId = currentSurvey.Id;
				contactId = currentCase.ContactId;
				leadId = currentCase.Lead__c;

				if (checkIfAlreadyTaken()) 
					isAlreadyTaken = true;
				else
					// is all good now, 
					// we are ready to redirect to the survey
					isAllGood = true;
			}
		}
	}

	/**
	 * Double checks if the survey has already been taken
	 */
	public Boolean checkIfAlreadyTaken() {
		return [
			SELECT COUNT()
			FROM SurveyTaker__c 
			WHERE Survey__c = :surveyId 
			AND Case__c = :caseId
			AND (
				(Contact__c != NULL AND Contact__c = :contactId)
				OR (Lead__c != NULL AND Lead__c = :leadId)
			)
		] > 0;
	}

	/**
	 * Wrapper with info about case receiving the feedback.
	 */
	private class Info {
		// caseId
		Id c { get; private set; }
		// suervey sent on
		String s { get; private set; }
		// language
		String l { get; private set; }
	}

	/**
	 * @return the amount of days should 
	 * pass before a lnk expires...
	 */
	private Integer getExpireDays() {
		return (SurveySettings__c.getOrgDefaults().LinkExpiresInDays__c == null ?
			7 : SurveySettings__c.getOrgDefaults().LinkExpiresInDays__c).intValue();
	}
}