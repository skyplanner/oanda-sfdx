/**
 * Class for calling new endpoints
 **/
public with sharing class SimpleCallouts extends Callout {
    protected Map<String, String> header;

    public SimpleCallouts(String namedCredentialsName) {
        header = new  Map<String, String>();
        NamedCredentialsUtil.NamedCredentialData namedCredentialData = NamedCredentialsUtil.getOrgDefinedNamedCredential(namedCredentialsName, Constants.PRINCIPAL_NAME_CREDENTIALS, '');
        baseUrl = namedCredentialData.credentialUrl;
        header.put('Content-Type', 'application/json');
    }

    public void updateUserIntroducingBroker(String oneId,
            UserApiUserPatchRequest user
    ) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.USER_API_INTRODUCING_BROKER_ENDPOINT),
                new List<String> {oneId}
        );

        patch(resource, header, null, Json.serialize(user, true));


        if (!isResponseCodeSuccess())
            throw getError();
    }

    public void updateUser(EnvironmentIdentifier identifier, String body
    ) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.USER_API_GET_USER_ENDPOINT),
                new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, body);

        if (!isResponseCodeSuccess())
            throw getError();
    }

    public void updateAddress(EnvironmentIdentifier identifier, String body) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.USER_API_UPD_ADDRESS_ENDPOINT),
                new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, body);

        if (!isResponseCodeSuccess())
            throw getError();
    }

    public void updateContactPhone(EnvironmentIdentifier identifier, String body) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.USER_API_CONTACT_PHONE_ENDPOINT),
                new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, body);

        if (!isResponseCodeSuccess())
            throw getError();
    }

    public String postOneIdByEmail(String email, Boolean isErrorCatch) {
        String uuid;
        String resource = String.format(
                CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),
                new List<String> { email + '?skip_fxdb=true' }
        );

        post(resource, header, null, '{}');

        if (isResponseCodeSuccess()) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
            uuid = (String) results?.get('uuid');
        }  else if (isErrorCatch) {
            throw getError();
        }
        return uuid;
    }

    public String postOneIdByEmail(String email) {
        return postOneIdByEmail(email, false);
    }

    public void updateStatus(EnvironmentIdentifier Identifier, UserApiStatus wrapper) {

        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_STATUS_ENDPOINT),
            new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, Json.serialize(wrapper, true));

        if (!isResponseCodeSuccess()) {
                throw getError();
        }
    }

    public void updateGroupRecalculateTasApi(EnvironmentIdentifier identifier, String body) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.TAS_API_GROUP_RECALCULATE_ENDPOINT),
                new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, body);

        if (!isResponseCodeSuccess())
            throw getError();
    }

    public void updateGroupRecalculateUserApi(EnvironmentIdentifier identifier, String body) {
        String resource = String.format(
                SPGeneralSettings.getInstance().getValue(
                        Constants.USER_API_GROUP_RECALCULATE_ENDPOINT),
                new List<String> {identifier.id, identifier.env}
        );

        patch(resource, header, null, body);

        if (!isResponseCodeSuccess())
            throw getError();
    }

    Exception getError() {
        if (exceptionDetails != null) {
            return exceptionDetails;
        }

        return new ApplicationException(
                resp.getStatus() + ' ' + resp.getBody());
    }
}