/*
*	@author : Deepak Malkani
*	Created Date : Feb 07 2016
*	Purpose : Test class to test the Rest Resource for creating EDI Results
*/

@isTest
private class EIDResultRestResourceTest
{
    //Gilbert: initialize api control custom setting
    @testSetup static void init(){
        API_Access_Control__c control1 = new API_Access_Control__c(name = CustomSettings.API_NAME_EID_RESULT, Enabled__c = true);
        insert control1;
    }
    
    
    @isTest
    static void createEIDResults_wo_fxaccnt()
    {
        String responseTxt;
        // Make the REST Call out
        Test.startTest();
        responseTxt = EIDResultRestResource.create_eid_Results(
            123, 
            'EqifaxUS', 
            true, 
            '', 
            '0102', 
            '01020304', 
            true, 
            '', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        Test.stopTest();
        
        //Perform assertions
        system.assertEquals(true, responseTxt.contains('FAILURE'));
        
    }
    
    @isTest
    static void createEIDResults_wt_fxaccnt()
    {
        //Create Test data
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 1234;
        update fxaccntObj;
        String responseTxt;
        // Make the REST Call out
        Test.startTest();
        responseTxt = EIDResultRestResource.create_eid_Results(
            1234, 
            'EqifaxUS', 
            true, 
            '', 
            '102', 
            '[{\"name\":\"Credit Agency 2\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Enhanced Credit 1\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Utility\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}}]',			true, 
            '95991919111451654', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        Test.stopTest();
        
        //Perform assertions
        system.assertEquals(true, responseTxt.contains('SUCCESS'));
        
        EID_Result__c res = [
            SELECT Id, 
            fxTrade_User_Id__c,
            Provider__c,
            Reason__c, 
            EID_Response__c,
            Tracking_Number__c, 
            Date__c,
            Watchlist_Result__c,
            Provider_Link_API__c
            FROM EID_Result__c where Trulliio_Agency__c = null];
        
        System.assertEquals('watchlistSample', res.Watchlist_Result__c, res);
        System.assertEquals('providerlinksample', res.Provider_Link_API__c, res);
    }
    
    @isTest
    static void createEIDResults_wt_fxaccnt_2()
    {
        //Create Test data
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 1234;
        update fxaccntObj;
        String responseTxt;
        // Make the REST Call out
        Test.startTest();
        responseTxt = EIDResultRestResource.create_eid_Results(
            1234, 
            'EqifaxUS', 
            false, 
            '', 
            '102', 
            '[{\"name\":\"Credit Agency 2\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Enhanced Credit 1\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Utility\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}}]',			false, 
            '95991919111451654', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        
        EID_Result__c result = [SELECT fxTrade_User_Id__c, Result__c, Tracking_Number__c FROM EID_Result__c where Trulliio_Agency__c = null];
        Test.stopTest();
        
        //Perform assertions
        system.assertEquals(true, responseTxt.contains('SUCCESS'));
        system.assertEquals(1234, result.fxTrade_User_Id__c);
        system.assertEquals('Failed', result.Result__c);
        system.assertEquals('95991919111451654', result.Tracking_Number__c);
    }

    @isTest
    static void createEIDResults_wt_EquifaxDIT()
    {
        //Create Test data
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 123456;
        update fxaccntObj;
        String responseTxt;
        // Make the REST Call out
        Test.startTest();
        
        responseTxt = EIDResultRestResource.create_eid_Results(
            123456, 
            'EquifaxDIT', 
            false, 
            '', 
            '105', 
            '{\"transactionId\": \"11261f29-12de-43cb-84dc-6dc7d8ced8cb\", \"correlationId\": \"25d002df-f42c-42ce-984c-48fcac661b57\", \"referenceTransactionId\": \"reference-1\", \"decision\": \"Deny\", \"timestamp\": \"2021-10-25T07:40:22.0131732+00:00\", \"details\": [{\"key\": \"phoneTrust\", \"value\": \"N\"}, {\"key\": \"phoneVerification\", \"value\": \"N\"}, {\"key\": \"phoneVerificationReason\", \"value\": \"phoneNotValid,phoneTypeNotMobile\"}, {\"key\": \"phoneAffiliation\", \"value\": \"N\"}, {\"key\": \"phoneAffiliationReason\", \"value\": \"\"}, {\"key\": \"phoneInsights\", \"value\": \"N\"}, {\"key\": \"phoneInsightsReason\", \"value\": \"\"}, {\"key\": \"emailTrust\", \"value\": \"N\"}, {\"key\": \"emailVerification\", \"value\": \"N\"}, {\"key\": \"emailVerificationReason\", \"value\": \"emailExists\"}, {\"key\": \"emailAffiliation\", \"value\": \"N\"}, {\"key\": \"emailAffiliationReason\", \"value\": \"\"}, {\"key\": \"emailInsights\", \"value\": \"N\"}, {\"key\": \"emailInsightsReason\", \"value\": \"accountDomainRisk\"}, {\"key\": \"addressTrust\", \"value\": \"N\"}, {\"key\": \"addressVerification\", \"value\": \"N\"}, {\"key\": \"addressVerificationReason\", \"value\": \"\"}, {\"key\": \"addressAffiliation\", \"value\": \"N\"}, {\"key\": \"addressAffiliationReason\", \"value\": \"\"}, {\"key\": \"addressInsights\", \"value\": \"N\"}, {\"key\": \"addressInsightsReason\", \"value\": \"\"}, {\"key\": \"identityVerification\", \"value\": \"N\"}, {\"key\": \"identityVerificationReason\", \"value\": \"\"}, {\"key\": \"identityResolution\", \"value\": \"Y\"}, {\"key\": \"identityResolutionReason\", \"value\": \"identityStrongCorroboration\"}, {\"key\": \"identityRisk\", \"value\": \"N\"}, {\"key\": \"identityRiskReason\", \"value\": \"identityRiskLow\"}, {\"key\": \"identityTrust\", \"value\": \"N\"}, {\"key\": \"SSNTrust\", \"value\": \"N\"}, {\"key\": \"SSNVerification\", \"value\": \"N\"}, {\"key\": \"SSNVerificationReason\", \"value\": \"\"}, {\"key\": \"SSNAffiliation\", \"value\": \"N\"}, {\"key\": \"SSNAffiliationReason\", \"value\": \"No Data Found\"}, {\"key\": \"SSNInsights\", \"value\": \"N\"}, {\"key\": \"SSNInsightsReason\", \"value\": \"\"}, {\"key\": \"dobVerification\", \"value\": \"N\"}, {\"key\": \"dobVerificationReason\", \"value\": \"DOB Not Verified\"}, {\"key\": \"dobAffiliation\", \"value\": \"N\"}, {\"key\": \"dobAffiliationReason\", \"value\": \"No Data Found\"}, {\"key\": \"dobInsights\", \"value\": \"N\"}, {\"key\": \"dobInsightsReason\", \"value\": \"DOB is in Future\"}, {\"key\": \"dobTrust\", \"value\": \"N\"}, {\"key\": \"deviceTrust\", \"value\": \"N\"}, {\"key\": \"deviceAssociation\", \"value\": \"N\"}, {\"key\": \"deviceAssociationReason\", \"value\": \"\"}, {\"key\": \"deviceReputation\", \"value\": \"N\"}, {\"key\": \"deviceReputationReason\", \"value\": \"\"}], \"originalTransactionId\": \"11261f29-12de-43cb-84dc-6dc7d8ced8cb\"}',
            true,
            '95991919111451654',
            1493901511000L,
            'watchlistSample',
            'providerlinksample');     

        EID_Result__c result = [SELECT fxTrade_User_Id__c, Result__c, Tracking_Number__c, Transaction_Id__c, Correlation_Id__c, 
                                        Phone_Trust__c, Phone_Verification_Reason__c, Time_Stamp__c
                                FROM EID_Result__c 
                                WHERE Trulliio_Agency__c = null];
        Test.stopTest();
        
        //Perform assertions
        System.assertEquals(true, responseTxt.contains('SUCCESS'));
        System.assertEquals(123456, result.fxTrade_User_Id__c);
        System.assertEquals('Failed', result.Result__c);
        System.assertEquals('95991919111451654', result.Tracking_Number__c);
        System.assert(!result.Phone_Trust__c, 'Phone trust should be true');
        System.assertEquals('phoneNotValid,phoneTypeNotMobile', result.Phone_Verification_Reason__c, 'Phone Not valid msg should be present');
        System.assert(result.Time_Stamp__c != null, 'Date should be valid');
    }
}