global with sharing class SP9437MassUpdateBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    global SP9437MassUpdateBatch() {
        query='SELECT Id, CaseId FROM LiveChatTranscript WHERE CaseId != null';
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, CaseId FROM LiveChatTranscript WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    	// Batchable interface
	global Iterable<SObject> start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}

    global void execute(Database.BatchableContext bc, List<LiveChatTranscript> sobjects) {
        LiveChatTranscriptHandler h = new LiveChatTranscriptHandler();
        h.handleAfterInsert(sobjects);
	}

    global void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
}