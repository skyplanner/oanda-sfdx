/**
 * Controller for component: fxAccountViewer
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 5/2/2019
 */
@isTest
private class FxAccountViewerCtrlTest {

	/**
	 * Tests: 
	 * global static List<UsernameWrapper> 
	 * 		getFxAccounts(Id accountId, Boolean isLive)
	 */
	@isTest
	static void getFxAccounts() {
		Account acc;

		// we need to insert a new account
		acc = new Account(
			FirstName = 'John',
			LastName = 'Doe',
			PersonEmail = 'johndoe@gmail.com'
		);
		insert acc;

		System.assertEquals(0, FxAccountViewerCtrl.getFxAccounts(acc.Id, true).size());
		System.assertEquals(0, FxAccountViewerCtrl.getFxAccounts(acc.Id, false).size());

		// we now insert a few fx accounts
		insert new List<fxAccount__c> {
			new fxAccount__c(
				Name = 'acb123',
				Account__c = acc.Id,
				RecordTypeId = 
					Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get(
						FxAccountViewerCtrl.LIVE_RECORD_TYPE).getRecordTypeId()
			),
			new fxAccount__c(
				Name = 'cde456',
				Account__c = acc.Id,
				RecordTypeId = 
					Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get(
						FxAccountViewerCtrl.PRACTICE_RECORD_TYPE).getRecordTypeId()
			),
			new fxAccount__c(
				Name = 'cde456',
				Account__c = acc.Id,
				RecordTypeId = 
					Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get(
						FxAccountViewerCtrl.PRACTICE_RECORD_TYPE).getRecordTypeId()
			)
		};

		System.assertEquals(1, FxAccountViewerCtrl.getFxAccounts(acc.Id, true).size());
		System.assertEquals(1, FxAccountViewerCtrl.getFxAccounts(acc.Id, false).size());
		System.assertEquals(2,
			FxAccountViewerCtrl.getFxAccounts(acc.Id, false)[0].accounts.size());
	}
}