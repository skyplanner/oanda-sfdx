@isTest
private class BatchStatusControllerTest {

    public static testMethod void testBatchStatusController() {
        BatchStatusController controller = new BatchStatusController();
      
        // Instantiate a new controller with all parameters in the page
        controller.startBatchLeadToFxAccount();
        controller.startBatchMergeLeads();
        controller.startBatchAssignLeads();
        controller.startBatchConvertLeads();
    }
}