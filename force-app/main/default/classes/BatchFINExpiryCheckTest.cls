@isTest (seeAllData = false)
public class BatchFINExpiryCheckTest {

    public static Lead lead;
    public static fxAccount__c fxa;
    
    @testSetup
    static void init()
    {
        lead = new Lead();
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        lead.LastName = 'Last1';
        lead.Email = 'test@abc.com';
        insert lead;
        
        fxa = new fxAccount__c();
        fxa.Funnel_Stage__c = FunnelStatus.TRADED;
        fxa.Ready_for_Funding_Date__c = Date.today().addDays(-1);
        fxa.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxa.Lead__c = lead.Id;
        fxa.Type__c = 'Individual';
        fxa.Singpass_Flow__c = true;
        fxa.FIN_Expiry_Date__c = Date.today();
        fxa.Government_ID__c = 'G123456';
        insert fxa;
    }
    
    static testMethod void testCaseCreation1()
    {
        fxAccount__c fxa = [select id from fxAccount__c limit 1];
        
        Test.startTest();

        String query = 'SELECT Id, Name, Type__c, Singpass_Flow__c, FIN_Expiry_Date__c, Government_ID__c, Account__c, fxTrade_User_Id__c, Contact__c, Division_Name__c, lead__c FROM fxAccount__c WHERE Type__c = \'Individual\' and Singpass_Flow__c = true and FIN_Expiry_Date__c = TODAY and (Government_ID__c LIKE \'G%\' or Government_ID__c LIKE \'F%\' or Government_ID__c LIKE \'M%\') and Is_Closed__c = false and Ready_for_Funding_DateTime__c != null and RecordType.Name = \'Retail Live\'';
        BatchFINExpiryCheck batch = new BatchFINExpiryCheck(query);
        Database.executeBatch(batch);

        Test.stopTest();

        Case[] finExpiryNotificationCases = [Select Id,OwnerId From Case where fxAccount__c = :fxa.Id and subject = 'Document Expiry Notification: FIN'];
        
        // Case should be created
        system.assertEquals(True, finExpiryNotificationCases.size() == 1);   
    }

    static testMethod void testCaseCreation2()
    {
        fxAccount__c fxa = [select id from fxAccount__c limit 1];
        fxa.Government_ID__c = 'A123456';
        update fxa;
        
        Test.startTest();

        String query = 'SELECT Id, Name, Type__c, Singpass_Flow__c, FIN_Expiry_Date__c, Government_ID__c, Account__c, fxTrade_User_Id__c, Contact__c, Division_Name__c, lead__c FROM fxAccount__c WHERE Type__c = \'Individual\' and Singpass_Flow__c = true and FIN_Expiry_Date__c = TODAY and (Government_ID__c LIKE \'G%\' or Government_ID__c LIKE \'F%\' or Government_ID__c LIKE \'M%\') and Is_Closed__c = false and Ready_for_Funding_DateTime__c != null and RecordType.Name = \'Retail Live\'';
        BatchFINExpiryCheck batch = new BatchFINExpiryCheck(query);
        Database.executeBatch(batch);

        Test.stopTest();

        Case[] finExpiryNotificationCases = [Select Id,OwnerId From Case where fxAccount__c = :fxa.Id and subject = 'Document Expiry Notification: FIN'];
        
        // Case should NOT be created since the government ID does not start with G, F or M
        system.assertEquals(True, finExpiryNotificationCases.size() == 0);   
    }
}