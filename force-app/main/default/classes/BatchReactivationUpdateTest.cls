/* Name: BatchReassignAccountsSchedulableTest 
 * Description : Apex Test Class for the BatchReactivationUpdateTest 
 * Author: Sahil Handa 
 * Date : 2021-05-17
 */
@isTest(seeAllData=false)
public class BatchReactivationUpdateTest 
{

    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    @testSetup static void init()
    {

        //Create test data
        List<Account> accntInsList = new List<Account>();
        for(Integer i=0; i<4;i++)
        {
            Account accnt = new Account();
            accnt.FirstName = 'TestFirst'+i;
            accnt.LastName = 'TestLast' +i;
            accnt.PersonEmail = 'test'+i+'@oanda.com';
            accnt.Account_Status__c = 'Active';
            accnt.Reactivation_Eligibility_Request__c = true;
            accnt.Eligible_for_Reactivation__c = true;
                accnt.PersonMailingCountry = 'Canada'; 
                accnt.Division_Name__c = 'OANDA Canada';  
            
            accntInsList.add(accnt);
        }

        if(!accntInsList.isEmpty())
        {
            try
            {
            insert accntInsList;
            }
            catch(Exception e)
            {
            }
        }
        
        List<fxAccount__c> fxaList = new List<fxAccount__c>();
        for(Integer k =0; k<4 ; k++)
        {
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'j1@example.org'+k;
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            tradeAccount.First_Trade_Date__c = Datetime.now();
            tradeAccount.Last_Trade_Date__c = DateTime.now();
            tradeAccount.Balance_USD__c = 12000;
            tradeAccount.Account__c = accntInsList[k].Id;
            tradeAccount.Division_Name__c='OANDA Europe';

            tradeAccount.Last_Reactivation_Date__c = Date.today()-183;
            System.debug(tradeAccount.Last_Reactivation_Date__c );
            fxaList.add(tradeAccount);
        }
        if(! fxaList.isEmpty())
        {
            try
            {
            insert fxaList;
            }
            catch(Exception e)
            {
            }
        }
        for(Integer k =0; k<4 ; k++)
        {
            accntInsList[k].fxAccount__c = fxaList[k].Id;
        }
        try
        {
        update accntInsList;
        }
        catch(Exception e)
        {
        }
       
    }
    
    @isTest static void testReactivationSchedulable1() 
    {
        // executing the AMR batch job under test context
        Test.startTest();
        CheckRecursive.setOfIDs = new Set<Id>();
        System.debug('update start');
        BatchReactivationUpdate b=  new BatchReactivationUpdate('SELECT Id, Last_Reactivation_Date__c ,Eligible_for_Reactivation__c, Reactivation_Eligibility_Request__c  FROM Account where Eligible_for_Reactivation__c = true or Reactivation_Eligibility_Request__c = true') ;
        String Query = 'SELECT Id, Last_Reactivation_Date__c ,Eligible_for_Reactivation__c, Reactivation_Eligibility_Request__c  FROM Account where Eligible_for_Reactivation__c = true or Reactivation_Eligibility_Request__c = true';
        BatchReactivationUpdate batch = new BatchReactivationUpdate(query);
        Database.executeBatch(batch);
        System.debug('update stop');
        Test.stopTest();
        
    }

}