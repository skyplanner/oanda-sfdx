/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class pdfGBGID3Cntlr {
    private final Id eidId;
    public String errorMessage { get; set; }
    public String authenticationID { get; set; }
    public String profileID { get; set; }
    public String profileName { get; set; }
    public String profileVersion { get; set; }
    public String score { get; set; }
    public String bandText { get; set; }
    public String timestamp { get; set; }
    public String customerRef { get; set; }
    public EID_Result__c eid { get; set; }
    public Map<String, Object> jsonMap { get; set; }
    public List<Map<String, Object>> globalItemCheckResultCodes { get; set; }
    
    public pdfGBGID3Cntlr(ApexPages.StandardController stdController) {
        eid = (EID_Result__c) stdController.getRecord();
        System.debug(eid);
        System.debug(eid.EID_Response__c);
        eid.Identity_Risk__c=!eid.Identity_Risk__c;
        try {
            jsonMap = (Map<String, Object>) JSON.deserializeUntyped(
                    eid.EID_Response__c
            );
            System.debug(jsonMap);
            System.debug(jsonMap.get('AuthenticationID'));
            authenticationID = (String) jsonMap.get('AuthenticationID');
            profileID = (String) jsonMap.get('ProfileID');
            profileName = (String) jsonMap.get('ProfileName');
            profileVersion = (String) jsonMap.get('ProfileVersion');
            score = (String) jsonMap.get('Score');
            bandText = (String) jsonMap.get('BandText');
            timestamp = (String) jsonMap.get('Timestamp');
            customerRef = (String) jsonMap.get('CustomerRef');
        }
        catch (Exception ex) {
            errorMessage = 'JSON no converted: ' + ex.getMessage();
            System.debug(errorMessage);
            return;
        }
        try {
            if (jsonMap.get('ResultCodes') != null) {
                Map<String, Object> resultCodes = (Map<String, Object>) JSON.deserializeUntyped(
                        JSON.serialize(jsonMap.get('ResultCodes'))
                );

                if (resultCodes.get('GlobalItemCheckResultCodes') != null) {
                    List<Object> globalItemCheckResultCodesTemp = (List<Object>) JSON.deserializeUntyped(
                            JSON.serialize(resultCodes.get('GlobalItemCheckResultCodes'))
                    );

                    globalItemCheckResultCodes = new List<Map<String, Object>>();
                    for(Object o : globalItemCheckResultCodesTemp) {
                        Map<String, Object> code = (Map<String, Object>) JSON.deserializeUntyped(
                                JSON.serialize(o)
                        );
                        for(String key : code.keySet()) {
                            if (key == 'Comment' || key == 'Match' || key == 'Warning') {
                                Map<String, Object> codeInner = (Map<String, Object>) JSON.deserializeUntyped(
                                        JSON.serialize(code.get(key))
                                );
                                List<Object> codeInfo = (List<Object>) JSON.deserializeUntyped(
                                        JSON.serialize(codeInner.get('GlobalItemCheckResultCode'))
                                );
                                List<Map<String, Object>> info = new List<Map<String, Object>>();
                                for(Object obj : codeInfo) {
                                    info.add(
                                            (Map<String, Object>) JSON.deserializeUntyped(
                                                    JSON.serialize(obj)
                                            )
                                    );
                                }
                                code.put(key, info);
                            }
                        }
                        if (code.get('Comment') == null) {
                            code.put('Comment', new List<Map<String, Object>>());
                        }
                        if (code.get('Match') == null) {
                            code.put('Match', new List<Map<String, Object>>());
                        }
                        if (code.get('Warning') == null) {
                            code.put('Warning', new List<Map<String, Object>>());
                        }
                        globalItemCheckResultCodes.add(code);
                    }
                }
            }
        }
        catch (Exception ex) {
            errorMessage = 'JSON no converted: ' + ex.getMessage();
            System.debug(errorMessage);
            return;
        }
    }
}