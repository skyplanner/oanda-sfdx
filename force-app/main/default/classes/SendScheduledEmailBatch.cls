public with sharing class SendScheduledEmailBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    Datetime beginDatetime = Datetime.now().addHours(-1);
    Datetime endDatetime = Datetime.now();
    public SendScheduledEmailBatch() {
        query = 'SELECT Id, Email_Message__c FROM Scheduled_Email__c  WHERE Scheduled_Date_Time__c > :beginDatetime AND Scheduled_Date_Time__c <= :endDatetime';
        
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Email_Message__c FROM Scheduled_Email__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    public Database.Querylocator start(Database.BatchableContext info) {
        return Database.getQueryLocator(query);
    } 

    public void execute(Database.BatchableContext context, List<Scheduled_Email__c> scope) {
        ScheduleMailSender sender = new ScheduleMailSender();
        sender.processScheduledEmails(scope);
    }
    public void finish(Database.BatchableContext bc) { 
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    } 
}