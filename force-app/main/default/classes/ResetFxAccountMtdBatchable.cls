global with sharing class ResetFxAccountMtdBatchable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection,  Database.Stateful{
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	Decimal numberOfRecords;
	
	ResetFxAccountMtdBatchable() {
		numberOfRecords = 0;
		query = 
			'SELECT Id ' +
			' FROM fxAccount__c ' + 
			' WHERE Trade_Volume_USD_MTD__c <> 0 OR ' + 
			' OANDA_P_L_USD_MTD__c <> 0 OR ' + 
			' P_and_L_USD_MTD__c <> 0';
	}

	// Batchable interface
	global Iterable<SObject> start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	global void execute(Database.BatchableContext bc, List<fxAccount__c> sobjects) {

		numberOfRecords += sobjects.size();

		Map<Id, fxAccount__c> fxasMap = new Map<Id, fxAccount__c>(sobjects);
		List<fxAccount__c> toUpdate = new List<fxAccount__c>();
		for(fxAccount__c f : 
			[SELECT 
				Id, 
				Trade_Volume_USD_MTD__c, 
				OANDA_P_L_USD_MTD__c, 
				P_and_L_USD_MTD__c
			FROM 
				fxAccount__c
			WHERE 
				Id IN: fxasMap.keySet()
			FOR UPDATE])
		{
			f.Trade_Volume_USD_MTD__c = 0;
			f.OANDA_P_L_USD_MTD__c = 0;
			f.P_and_L_USD_MTD__c = 0;
			toUpdate.add(f);
		}
		update toUpdate;
	} 

	global void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		List<AsyncApexJob> apexJobList = [
			SELECT Id, 
				JobItemsProcessed, 
				CreatedDate, 
				TotalJobItems 
			FROM AsyncApexJob 
			WHERE Id =: bc.getJobId()];

		if(!apexJobList.isEmpty()){
			String subject = 'ResetFxAccountMtdBatchable completed';
			String body = 'Job started: ' + apexJobList[0].CreatedDate;
			body += '\nJob ended: ' + Datetime.now();
			body += '\nNumber of batches: ' + apexJobList[0].TotalJobItems;
			body += '\nNumber of records: ' + numberOfRecords;
			EmailUtil.sendEmail('salesforce@oanda.com', subject, body);
		}
	}

	// static methods
	
	// ResetFxAccountMtdBatchable.executeBatch();
	public static Id executeBatch() {
		return Database.executeBatch(new ResetFxAccountMtdBatchable());
	}
}