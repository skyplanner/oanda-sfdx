/**
 * @File Name          : AccountSegmentationManager.cls
 * @Description        : Manages Segmentation__c records linked to fxAccounts
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 3:14:06 AM
**/
public inherited sharing class AccountSegmentationManager {

	Set<ID> fxAccountIds;
	protected Map<ID, Segmentation__c> segmentationsByFxAccountId;

	public AccountSegmentationManager() {
		this.fxAccountIds = new Set<ID>();
	}

	public static ID getValidFxAccount(
		Account accountObj,
		Lead leadObj
	) {
		ID result = (accountObj != null)
			? accountObj.fxAccount__c
			: leadObj?.fxAccount__c;
		return result;
	}

	public Boolean registerFxAccount(ID fxAccountId) {
		Boolean result = false;

		if (fxAccountId != null) {
			fxAccountIds.add(fxAccountId);
			segmentationsByFxAccountId = null;
			result = true;
			
			ServiceLogManager.getInstance().log(
				'registerFxAccount -> fxAccountId: ' + fxAccountId, // msg
				AccountSegmentationManager.class.getName() // category
			);
		}
		
		return result;
	}

	public Segmentation__c getSegmentation(ID fxAccountId) {
		Segmentation__c result = null;

		if (fxAccountId != null) {
			result = getSegmentationMap().get(fxAccountId);
		}

		ServiceLogManager.getInstance().log(
			'getSegmentation -> fxAccountId: ' + fxAccountId  +
			', result.Id: ' + result?.Id, // msg
			AccountSegmentationManager.class.getName() // category
		);

		return result;
	}

	public Map<ID, Segmentation__c> getSegmentationMap() {
		if (segmentationsByFxAccountId != null) {
			return segmentationsByFxAccountId;
		}
		// else...
		segmentationsByFxAccountId = new Map<ID, Segmentation__c>();

		if (fxAccountIds.isEmpty() == true) {
			return segmentationsByFxAccountId;
		}
		// else...
		List<Segmentation__c> segmentations = 
			SegmentationRepo.getActiveByFxAccount(fxAccountIds);

		for (Segmentation__c segmentation : segmentations) {
			segmentationsByFxAccountId.put(
				segmentation.fxAccount__c, // key
				segmentation // value
			);
		}
		
		return segmentationsByFxAccountId;
	}

}