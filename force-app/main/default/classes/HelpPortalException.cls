/**
 * @File Name          : HelpPortalException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2020, 11:30:45 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/28/2020   acantero     Initial Version
**/
public class HelpPortalException extends Exception {
}