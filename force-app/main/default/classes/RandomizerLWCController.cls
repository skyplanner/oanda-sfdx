/**
 * Controller to serve to RandomizerLWC
 * @author:	Michel Carrillo (SkyPlanner)
 * @since:	01/07/2021
 * @version 1.0
 */
public with sharing class RandomizerLWCController {

    private static final Integer NUMBER_OF_CASES = 100;

    /**
     * Retrieve the initial values to initialize the form
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @return:	InitValuesWrapper, contains all the required values to start the component 
     */
    @AuraEnabled(cacheable=false)
    public static InitialValuesWrapper getInitialValues(){
        try{
            InitialValuesWrapper result = new InitialValuesWrapper();
            result.caseOriginValues = getPickListValues(Case.Origin, false);
            result.inquireNatureValues = getPickListValues(Case.Inquiry_Nature__c, true);
            result.typeValues = getPickListValues(Case.Type__c, true);
            result.subTypeValues = getPickListValues(Case.Subtype__c, true);
            result.recordTypeValues = getRecordTypes();
            return result;
        } 
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     * Retrieve the random cases based on the given filters
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @return:	List<Case>, The random cases
     */
    @AuraEnabled(cacheable=false)
    public static List<Case> getRandomCases(FiltersWrapper pFilters){

        try {
            System.debug('pFilters ===> ' + pFilters);
            List<Case> result = new List<Case>();
            String query = buildQuery(pFilters);
            String[] originFilters = pFilters.caseOriginFilter;
            List<Case> tmpList = Database.query(query);
            if(pFilters.numberCasesFilter < tmpList.size()){
                result = getRandomCasesList(tmpList, pFilters.numberCasesFilter);
            }
            else{
                result = tmpList;
            }
            return result;        
        } 
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     * Generate the query string from the given filters wrappers.
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/12/2021
     * @return:	String, The genrated query
     * @param:	FiltersWrapper, the filters wrapper
     */
    private static String buildQuery(FiltersWrapper pFilters){

        String resQuery =  'SELECT Id, ' +
                '       CaseNumber, ' +
                '       Owner.Name, ' +
                '       First_Owner__r.Name, ' +
                '       Account.Name, ' +
                '       RecordType.Name, ' +
                '       Type__c, ' +
                '       CreatedDate, ' +
                '       Case_Channel__c, ' + 
                '       Chat_Division__c, ' + 
                '       Country__c, ' +
                '       Origin, ' +
                '       Account_Division_Name__c, ';

            if(pFilters.chatDurationFilter != null){
                resQuery += ' (SELECT ChatDuration FROM LiveChatTranscripts WHERE ChatDuration > ' + pFilters.chatDurationFilter + ') ';
            }
            else {
                resQuery += ' (SELECT ChatDuration FROM LiveChatTranscripts) ';
            }

            resQuery += ' FROM   Case ' +
                    'WHERE Id != NULL AND RecordType.Id != NULL ';

            if(String.isNotEmpty(pFilters.caseOwnerFilter))
                resQuery += 'AND Owner.Name LIKE \'%' + pFilters.caseOwnerFilter + '%\' ';

            if(String.isNotEmpty(pFilters.caseOwnerRoleFilter))
                resQuery += 'AND Owner.UserRole.Name LIKE \'%' + pFilters.caseOwnerRoleFilter + '%\' ';

            if(String.isNotEmpty(pFilters.accountDivisionFilter))
                resQuery += 'AND Account_Division_Name__c LIKE \'%' + pFilters.accountDivisionFilter + '%\' ';

            if(String.isNotEmpty(pFilters.chatDivisionFilter))
                resQuery += 'AND Chat_Division__c LIKE \'%' + pFilters.chatDivisionFilter + '%\' ';

            if(pFilters.caseOriginFilter != null && !pFilters.caseOriginFilter.isEmpty())
                resQuery += 'AND Origin IN :originFilters ';

            if(String.isNotEmpty(pFilters.inquireNatureFilter))
                resQuery += 'AND Inquiry_Nature__c = \'' + pFilters.inquireNatureFilter + '\' ';

            if(String.isNotEmpty(pFilters.typeFilter))
                resQuery += 'AND Type__c = \'' + pFilters.typeFilter + '\' ';

            if(String.isNotEmpty(pFilters.subTypeFilter))
                resQuery += 'AND Subtype__c = \'' + pFilters.subTypeFilter + '\' ';

            /********/
            if(pFilters.fromDateFilter != null)
                resQuery += 'AND DAY_ONLY(CreatedDate) >= ' + 
                    Datetime.newInstance(
                        pFilters.fromDateFilter.year(), 
                        pFilters.fromDateFilter.month(), 
                        pFilters.fromDateFilter.day())
                        .format('yyyy-MM-dd') + ' ';

            if(pFilters.toDateFilter != null){
                resQuery += 'AND DAY_ONLY(CreatedDate) <= ' + 
                    Datetime.newInstance(
                        pFilters.toDateFilter.year(), 
                        pFilters.toDateFilter.month(), 
                        pFilters.toDateFilter.day())
                        .format('yyyy-MM-dd') + ' ';
            }

            if(String.isNotEmpty(pFilters.recordTypeFilter))
                resQuery += 'AND RecordType.Id = \'' + pFilters.recordTypeFilter + '\' ';

            if(String.isNotEmpty(pFilters.channelFilter))
                resQuery += 'AND Case_Channel__c LIKE \'%' + pFilters.channelFilter + '%\' ';

            if(String.isNotEmpty(pFilters.languagePreferenceFilter))
                resQuery += 'AND Chat_Language_Preference__c LIKE \'%' + pFilters.languagePreferenceFilter + '%\' ';

            if(String.isNotEmpty(pFilters.chatLanguageFilter))
                resQuery += 'AND Language_Preference__c LIKE \'%' + pFilters.chatLanguageFilter + '%\' ';

            if(String.isNotEmpty(pFilters.countryOfResidencyFilter))
                resQuery += 'AND Country__c LIKE \'%' + pFilters.countryOfResidencyFilter + '%\' ';

            if(String.isNotEmpty(pFilters.agentTenureFilter))
                resQuery += getTenureFilter(pFilters.agentTenureFilter) + ' ';

            if(String.isNotEmpty(pFilters.qiReviewedFilter))
                resQuery += 'AND QI_Reviewed__c = ' + pFilters.qiReviewedFilter + ' ';

            if(String.isNotEmpty(pFilters.isComplaintFilter))
                resQuery += 'AND Complaint__c = ' + pFilters.isComplaintFilter + ' ';

			if (String.isNotEmpty(pFilters.caseQIOwnerFilter))
				resQuery +=
					'AND QI_Case_Owner__r.Name LIKE \'%' +
					String.escapeSingleQuotes(pFilters.caseQIOwnerFilter) +
					'%\' ';

			if (String.isNotEmpty(pFilters.isHVCFilter))
				resQuery += 'AND Is_HVC__c = ' + pFilters.isHVCFilter + ' ';

            if(pFilters.chatDurationFilter != null)
                resQuery += ' AND Id IN (SELECT CaseId FROM LiveChatTranscript WHERE ChatDuration > ' + pFilters.chatDurationFilter + ') ';

            if(pFilters.numberCasesFilter != null)
                resQuery += ' LIMIT ' + pFilters.numberCasesFilter * 10;
            else
                resQuery += ' LIMIT ' + NUMBER_OF_CASES * 10;

            System.debug('QUERY ===> ' + resQuery);
            return resQuery;
    }

    /**
     * Return pQuantity random cases from the pCaseList list.
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/12/2021
     * @return:	List<Case>, The random list of cases
     * @param:	List<Case>, List of cases to get the random cases from.
     * @param:	Integer, The amount of random cases to be selected.
     */
    private static List<Case> getRandomCasesList(List<Case> pCaseList, Integer pQuantity){
        
        if(pQuantity == null)
            pQuantity = NUMBER_OF_CASES;

        if(pQuantity >= pCaseList.size())
            return pCaseList;

        List<Case> resultCases = new List<Case>();
        Integer randomIndex;
        for(Integer i = 0; i < pQuantity; i++){
            randomIndex = getUniqueInteger(pCaseList.size());
            resultCases.add(pCaseList.get(randomIndex));
        }

        return resultCases;
    }

    private static Set<Integer> generatedIntegers = new Set<Integer>();
    /**
     * Return a random number just once from 0 to pTop parameter
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/12/2021
     * @return:	Integer, The random number.
     * @param:	Integer, Maximum number in the random range.
     */
    private static Integer getUniqueInteger(Integer pTop) {
        Integer result = Integer.valueof((Math.random() * pTop));
        while(generatedIntegers.contains(result) || generatedIntegers.size() >= pTop)
            result = Integer.valueof((Math.random() * pTop));
        generatedIntegers.add(result);
        return result;
    }

    /**
     * Generate the filter statement to the tenure value
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/12/2021
     * @return:	String, The tenure value filter
     * @param:	String, the given value of the filter
     */
    private static String getTenureFilter(String pTenureFilter){

        String dateStr;
        String result = ' AND Case_Tenure__c ';

        if(pTenureFilter == '< 0.5 Year')// < 182 Days
            dateStr = '< 182 ';

        if(pTenureFilter == '< 1 Year')// 182 < d < 365
            dateStr = '< 365 ';

        if(pTenureFilter == '> 1 Year') // <= 365
            dateStr = '>= 365 ';

        if(String.isNotBlank(dateStr))
            return result + dateStr;

        return '';
    }

    /**
     * Retrieve the recordTypes 
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @return:	List<PicklistValuesWrapper>
     */
    private static List<PicklistValuesWrapper> getRecordTypes(){
        List<PicklistValuesWrapper> result = new List<PicklistValuesWrapper>();
        result.add(new PicklistValuesWrapper('All', ''));
        for(RecordType rt : [SELECT 
                    Name, 
                    DeveloperName, 
                    NamespacePrefix, 
                    Description, 
                    BusinessProcessId, 
                    SobjectType, 
                    IsActive 
                FROM RecordType
                WHERE SobjectType = 'Case'
                AND IsActive = True])
            result.add(new PicklistValuesWrapper(rt.Name, rt.Id));
        return result;
    }

    /**
     * Retrieve the picklist values 
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @return:	List<PicklistValuesWrapper>
     * @param:	Schema.SObjectField to retrieve the picklist values from 
     */
	private static List<PicklistValuesWrapper> getPickListValues(Schema.SObjectField field, Boolean pIncludesAll){
		List<PicklistValuesWrapper> result = new List<PicklistValuesWrapper>();
		Schema.DescribeFieldResult fieldResult = field.getDescribe();
        Schema.PicklistEntry[] values = fieldResult.getPicklistValues();
        if(pIncludesAll)        
            result.add(new PicklistValuesWrapper('All', ''));
		for( Schema.PicklistEntry pValue : values) {
			result.add(new PicklistValuesWrapper(pValue.getLabel(), pValue.getValue()));
		}
		return result;
    }
    
    /**
     * Represents the filters defined by the client
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @version 1.0
     */
    public class FiltersWrapper {
        @AuraEnabled
        public String caseOwnerFilter {get; set;}
        @AuraEnabled
        public String caseOwnerRoleFilter {get; set;}
        @AuraEnabled
        public String accountDivisionFilter {get; set;}
        @AuraEnabled
        public String chatDivisionFilter {get; set;}
        @AuraEnabled
        public List<String> caseOriginFilter {get; set;}
        @AuraEnabled
        public String inquireNatureFilter {get; set;}
        @AuraEnabled
        public String typeFilter {get; set;}
        @AuraEnabled
        public String subTypeFilter {get; set;}
        @AuraEnabled
        public Date fromDateFilter {get; set;}
        @AuraEnabled
        public Date toDateFilter {get; set;}
        @AuraEnabled
        public String recordTypeFilter {get; set;}
        @AuraEnabled
        public String channelFilter {get; set;}
        @AuraEnabled
        public String languagePreferenceFilter {get; set;}
        @AuraEnabled
        public String chatLanguageFilter {get; set;}
        @AuraEnabled
        public Integer chatDurationFilter {get; set;}
        @AuraEnabled
        public String countryOfResidencyFilter {get; set;}
        @AuraEnabled
        public String agentTenureFilter {get; set;}
        @AuraEnabled
        public Integer numberCasesFilter {get; set;}
        @AuraEnabled
        public String qiReviewedFilter {get; set;}
        @AuraEnabled
        public String isComplaintFilter {get; set;}
        @AuraEnabled
        public String caseQIOwnerFilter {get; set;}
        @AuraEnabled
        public String isHVCFilter {get; set;}
    }

    /**
     * Class that represents the initial values of the LWC
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @version 1.0
     */
    public class InitialValuesWrapper{
        @AuraEnabled
        public List<PicklistValuesWrapper> caseOriginValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> inquireNatureValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> typeValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> subTypeValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> recordTypeValues {get; set;}
    }

    /**
     * Class that represents the picklist entries
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/07/2021
     * @version 1.0
     */
    public class PicklistValuesWrapper{
        public PicklistValuesWrapper(String pLabel, String pValue){
            this.label = pLabel;
            this.value = pValue;
        }
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String value {get; set;}
    }
}