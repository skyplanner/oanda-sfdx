/**
 * @File Name          : ServiceRoutingContextTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/7/2024, 6:08:12 PM
**/
@IsTest
private without sharing class ServiceRoutingContextTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	@IsTest
	static void getRoutingHelper() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		PendingServiceRouting psr = new PendingServiceRouting();
		psr.WorkItemId = case1Id;
		psr.RoutingType = OmnichanelConst.SKILLS_BASED_ROUTING_TYPE;
		List<PendingServiceRouting> routingObjList = 
			new List<PendingServiceRouting>{ psr };

		Test.startTest();
		ServiceRoutingContext instance = 
			new ServiceRoutingContext(routingObjList);
		SRoutingHelper result = instance.getRoutingHelper();
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	@IsTest
	static void registerRoutingInfoManager() {
		Test.startTest();
		ServiceRoutingContext instance = 
			new ServiceRoutingContext(
				new List<PendingServiceRouting> {
					new PendingServiceRouting()
				}
			);
		Boolean result = instance.registerRoutingInfoManager(
			null, //routingSObjectType
			null // routingInfoManager 
		);
		Test.stopTest();
		
		Assert.isFalse(result, 'Invalid result');
	}

	// test 1 : psr records are equals
	// test 2: psr records are diferent
	// test 3: psr record lists are diferent (diferent size)
	@IsTest
	static void getInstance() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		
		PendingServiceRouting psr1 = new PendingServiceRouting();
		psr1.WorkItemId = case1Id;
		List<PendingServiceRouting> routingObjList1 = 
			new List<PendingServiceRouting>{ psr1 };

		PendingServiceRouting psr2 = new PendingServiceRouting();
		psr2.WorkItemId = case1Id;
		List<PendingServiceRouting> routingObjList2 = 
			new List<PendingServiceRouting>{ psr2 };

		PendingServiceRouting psr3 = new PendingServiceRouting();
		psr3.WorkItemId = accountId;
		List<PendingServiceRouting> routingObjList3 = 
			new List<PendingServiceRouting>{ psr3 };

		List<PendingServiceRouting> routingObjList4 = 
			new List<PendingServiceRouting>{ psr1, psr2 };

		Test.startTest();
		// test 1
		ServiceRoutingContext instance1 = 
			ServiceRoutingContext.getInstance(routingObjList1);
		ServiceRoutingContext instance2 = 
			ServiceRoutingContext.getInstance(routingObjList2);
		// test 2
		ServiceRoutingContext instance3 = 
			ServiceRoutingContext.getInstance(routingObjList3);
		// test 3
		ServiceRoutingContext instance4 = 
			ServiceRoutingContext.getInstance(routingObjList4);
		Test.stopTest();
		
		Assert.isTrue(
			(instance1 == instance2), 
			'Both calls should return the same instance'
		);
		Assert.isFalse(
			(instance1 == instance3), 
			'Both calls should return different instances'
		);
		Assert.isFalse(
			(instance3 == instance4), 
			'Both calls should return different instances'
		);
	}
	
}