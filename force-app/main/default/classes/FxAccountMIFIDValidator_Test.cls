/**
 * @File Name          : FxAccountMIFIDValidator_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   acantero     Initial Version
**/
@istest
private class FxAccountMIFIDValidator_Test {

    //test validation using Expression Validation metadata
    @isTest
	static void validate_test() {
        Boolean error = false;
        List<fxAccount__c> objList = MIFIDValidationTestDataFactory.createValidableFxAccounts();
        FxAccountMIFIDValidator obj1 = new FxAccountMIFIDValidator(null, null);
        FxAccountMIFIDValidator obj2 = new FxAccountMIFIDValidator(objList, false);
        Test.startTest();
        try {
            obj1.validate();
		    obj2.validate();
        } catch (Exception ex) {
            error = true;
        }
		Test.stopTest();
        System.assertEquals(false, error);
    }
    
    //set expresion validation to run posible validations
    //run twice the same fxAccount list, check cases are created only once
    @isTest
	static void validate_test2() {
        List<fxAccount__c> objList = MIFIDValidationTestDataFactory.createValidableFxAccounts();
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator obj1 = new FxAccountMIFIDValidator(objList, false, expValMap);
        obj1.validate();
        Integer firstCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        FxAccountMIFIDValidator obj2 = new FxAccountMIFIDValidator(objList, false, expValMap);
        obj2.validate();
		Test.stopTest();
        Integer finalCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        //Integer finalCaseCount = MIFIDValidationTestDataFactory.showValidationErrors();
        System.assertEquals(MIFIDValidationTestDataFactory.FXACCOUNT_INVALID_NOVALIDATED_COUNT, firstCaseCount);
        System.assertEquals(MIFIDValidationTestDataFactory.FXACCOUNT_INVALID_NOVALIDATED_COUNT, finalCaseCount);
    }

    //set expresion validation to run posible validations
    //run twice the same fxAccount list, check cases are created only once and updated
    @isTest
	static void validate_test3() {
        List<fxAccount__c> objList = MIFIDValidationTestDataFactory.createValidableFxAccounts();
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator obj1 = new FxAccountMIFIDValidator(objList, false, expValMap);
        obj1.validate();
        Map<ID,Case> firstValCaseMap = new Map<ID,Case>(
            [select Id, LastModifiedDate from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE]
        );
        FxAccountMIFIDValidator obj2 = new FxAccountMIFIDValidator(objList, true, expValMap);
        obj2.validate();
		Test.stopTest();
        List<Case> finalValCaseList = [select Id, LastModifiedDate from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        System.assertEquals(MIFIDValidationTestDataFactory.FXACCOUNT_INVALID_NOVALIDATED_COUNT, firstValCaseMap.size());
        System.assertEquals(MIFIDValidationTestDataFactory.FXACCOUNT_INVALID_NOVALIDATED_COUNT, finalValCaseList.size());
     /*   for(Case caseObj : finalValCaseList) {
            Case oldRecord = firstValCaseMap.get(caseObj.Id);
            System.assert(caseObj.LastModifiedDate > oldRecord.LastModifiedDate);
        }*/
    }
 

    //validate fxAccount with invalid passport (this create a 'MiFID Review' Case)
    //then update fxAccount with valid passport
    //run validation again and check if the case is declared 'obsolete'
    @isTest
	static void validate_test5() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        //invalid fxAccount
        fxAccount__c fxAccount1 = MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1234567'
        );
        List<fxAccount__c> objList = new List<fxAccount__c>{ fxAccount1 };
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
        String firstValCaseSubject = [
            SELECT
                Subject
            FROM 
                Case 
            WHERE
                fxAccount__c = :fxAccount1.Id
            AND 
                Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE
            AND
                Status <> :MiFIDExpValConst.CASE_CLOSED_STATUS
        ][0].Subject;
        //This is done so that the validation is not triggered by the modification 
        //in that case the system would use the metadata information
        valSettings.isActive = false; 
        fxAccount1.Passport_Number__c = '1224568'; //valid value
        update fxAccount1;
        //restore validation
        valSettings.isActive = true;
        objList = [
            select 
                Id,
                Name,
                National_Personal_ID__c,
                Passport_Number__c,
                Citizenship_Nationality__c,
                Last_Trade_Date__c,
                Account__c,
                Division_Name__c
            from fxAccount__c 
            where Id = :fxAccount1.Id
        ];
        valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
		Test.stopTest();
        String secondValCaseSubject = [
            SELECT
                Subject
            FROM 
                Case 
            WHERE
                fxAccount__c = :fxAccount1.Id
            AND 
                Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE
            AND
                Status <> :MiFIDExpValConst.CASE_CLOSED_STATUS
        ][0].Subject;
        System.assertEquals(System.Label.MifidValCaseSubjectPassport, firstValCaseSubject);
        System.assertEquals(System.Label.MifidObsoleteCase, secondValCaseSubject);
    }

    //validate fxAccount with 'empty conditional' values
    //both NID and Passport are empty => fxAccount is invalid
    @isTest
	static void validate_test6() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        //invalid fxAccount
        fxAccount__c fxAccount1 = MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Bucanero', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MALTA_COUNTRY, 
            null, 
            null
        );
        List<fxAccount__c> objList = new List<fxAccount__c>{ fxAccount1 };
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
		Test.stopTest();
        Integer finalCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        System.assertEquals(1, finalCaseCount);
    }

    //validate fxAccount with 'empty conditional' values
    //NID is empty and Passport is ok => fxAccount is valid
    @isTest
	static void validate_test7() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        //invalid fxAccount
        fxAccount__c fxAccount1 = MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Bucanero', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MALTA_COUNTRY, 
            null, 
            '1122334'
        );
        List<fxAccount__c> objList = new List<fxAccount__c>{ fxAccount1 };
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
		Test.stopTest();
        Integer finalCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        System.assertEquals(0, finalCaseCount);
    }

    //validate fxAccount with 'empty conditional' values
    //NID is ok and Passport is empty => fxAccount is valid
    @isTest
	static void validate_test8() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        //invalid fxAccount
        fxAccount__c fxAccount1 = MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Bucanero', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MALTA_COUNTRY, 
            '1122334M',
            null
        );
        List<fxAccount__c> objList = new List<fxAccount__c>{ fxAccount1 };
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
		Test.stopTest();
        Integer finalCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        System.assertEquals(0, finalCaseCount);
    }

    //validate fxAccount with 'empty conditional' values
    //NID is invalid and Passport is empty => fxAccount is invalid
    @isTest
	static void validate_test9() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        //invalid fxAccount
        fxAccount__c fxAccount1 = MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Bucanero', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MALTA_COUNTRY, 
            '112',
            null
        );
        List<fxAccount__c> objList = new List<fxAccount__c>{ fxAccount1 };
        Map<String,ExpressionValidationWrapper> expValMap = 
            MIFIDValidationTestDataFactory.getValFxAccountsExpValMap();
        Test.startTest();
        FxAccountMIFIDValidator valManager = new FxAccountMIFIDValidator(objList, true, expValMap);
        valManager.validate();
		Test.stopTest();
        Integer finalCaseCount = [select count() from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        System.assertEquals(1, finalCaseCount);
    }




}