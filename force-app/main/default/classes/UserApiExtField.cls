/**
 * User API external field
 */
public class UserApiExtField extends UserApiField {
    public UserApiExtField(User_API_Ext_Field__mdt f) {
        label = f.label;
        name = f.DeveloperName;
        showOnList = f.Show_on_List__c;
        showOnDetails = f.Show_on_Details__c;
        readOnly = f.Read_Only__c;
        order = f.Order__c.intValue();
        isText = f.Type__c == 'Text';
        isCheck = f.Type__c == 'Checkbox';
        isEmail = f.Type__c == 'Email';
        isNumber = f.Type__c == 'Number';
        isDate = f.Type__c == 'Date';
        isDateTime = f.Type__c == 'Datetime';
        isPicklist = f.Type__c == 'Picklist';
    }
}