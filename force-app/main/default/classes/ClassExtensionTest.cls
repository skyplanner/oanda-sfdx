/**
 * @description       : The rest of the code of ClassExtension is tested in the test class CASearchDetailsManagerTest
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class ClassExtensionTest {
    @isTest
	static void callingException() {
        Callable extension = (Callable) Type.forName('ClassExtension')
            .newInstance();

        try {
            Object result = extension.call(
                'testing not implemented method',
                new Map<String, Object>{
                    'details' => new CASearchDetails()
                }
            );
        } catch (Exception ex) {
            System.assertEquals('Method not implemented', ex.getMessage());
        }
	}
}