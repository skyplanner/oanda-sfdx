/**
 * @File Name          : MiFIDValCasesHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2020, 8:35:33 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   acantero     Initial Version
**/
@isTest
private class MiFIDValCasesHelper_Test {

    //invalid values
    @isTest
    static void createOrUpdateCases_test1() {
        Boolean error = false;
        Test.startTest();
        try {
            MiFIDValCasesHelper obj = new MiFIDValCasesHelper(null, null);
            obj.createOrUpdateCases();
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
    
    //invalid params
    @isTest
    static void getFxAccountsWithMiFIDCaseOpen_test() {
        Test.startTest();
        List<ID> result = MiFIDValCasesHelper.getFxAccountsWithMiFIDCaseOpen(null);
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    //invalid params
    @isTest
    static void getFxAccountsOpenCaseList_test() {
        Test.startTest();
        List<Case> result = MiFIDValCasesHelper.getFxAccountsOpenCaseList(null);
        Test.stopTest();
        System.assert(result.isEmpty());
    }
}