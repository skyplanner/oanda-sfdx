@isTest
private class LegalAgreementListControllerTest {
	
	@testSetup
	static void createTestData() {
		RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
		
		Account acc = new Account(LastName = 'abc', RecordTypeId = personAccountRecordType.Id);
    	insert acc;
    	
    	fxAccount__c liveAccount = new fxAccount__c();
    	liveAccount.name = 'abc';
	    liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
		liveAccount.Account__c = acc.Id;
		liveAccount.Contact__c = acc.PersonContactId;

		liveAccount.Legal_Consent__c = '[{"version":"2013-05-06","update_datetime":"2017-09-13T17:55:11","create_datetime":"2017-09-13T17:55:11","name":"risk_disclosure_agreement_ca","country":"CA","language":"EN"},{"version":"2016-03-23","update_datetime":"2017-09-13T17:55:11","create_datetime":"2017-09-13T17:55:11","name":"fxtrade_customer_agreement_ca","country":"CA","language":"EN"}]';

		insert liveAccount;

		Legal_Agreement_Mapping__c agr1 = new Legal_Agreement_Mapping__c(name = 'risk_disclosure_agreement_ca', Code__c = 11, Display_Name__c = 'Risk Disclosure Agreement (OCAN)');
	    Legal_Agreement_Mapping__c agr2 = new Legal_Agreement_Mapping__c(name = 'fxtrade_customer_agreement_ca', Code__c = 9, Display_Name__c = 'FXTrade Customer Agreement (OCAN)');
	    insert new List<Legal_Agreement_Mapping__c>{agr1, agr2};
		
        Country_Setting__c cs = new Country_Setting__C(Name='Canada', EEA__c=false, Group__c='Canada', ISO_Code__c='ca', Prohibited__c=false, Region__c='North America', Zone__c='Americas');
        insert cs;
	}

	@isTest static void showLegalAgreementsTest() {
		fxAccount__c fxa = [select id, name from fxAccount__c limit 1];

		String fxaName = LegalAgreementListController.getFxAccountName(fxa.id);
		System.assertEquals(fxa.Name, fxaName);

		List<LegalAgreementListController.LegalAgreement> agreements = LegalAgreementListController.getAgreements(fxa.Id);
		System.assertEquals(2, agreements.size());
	}
	
	//test when legal agreements is null
	@isTest static void emptyLegalAgreementsTest() {
		fxAccount__c fxa = [select id, name, Legal_Consent__c from fxAccount__c limit 1];

		fxa.Legal_Consent__c = null;
		update fxa;

		String fxaName = LegalAgreementListController.getFxAccountName(fxa.id);
		System.assertEquals(fxa.Name, fxaName);

		List<LegalAgreementListController.LegalAgreement> agreements = LegalAgreementListController.getAgreements(fxa.Id);
		System.assertEquals(0, agreements.size());
	}
	
}