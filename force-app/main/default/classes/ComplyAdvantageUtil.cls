/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-06-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class ComplyAdvantageUtil 
{
    public static List<Comply_Advantage_Division_Setting__c> DIVISION_SETTINGS = getAllDivisionSettings();
    public static Map<string, Comply_Advantage_Division_Setting__c> DIVISION_SETTINGS_MAP = getDivisionSettinsMap();

    public static final Boolean COMPLY_ADVANTAGE_ENABLED = isComplyAdvantageEnabled();

    public static final set<string> COMPLY_ADVANTAGE_SEARCH_FEILDS = new set<string>{
        'id',
        'name',
        'case__c',
        'is_monitored__c',
        'match_status_override_reason__c',
        'match_status__c',
        'report_link__c',
        'search_id__c',
        'division_name__c',
        'search_parameter_birth_year__c',
        'search_parameter_citizenship_nationality__c',
        'search_parameter_mailing_country__c',
        'search_text__c',
        'fxaccount__c',
        'fxAccount__r.Name',
        'entity_contact__c',
        'is_alias_search__c',
        'search_reason__c',
        'case_management_link__c',
        'is_suspended__c',
        'search_reference__c',
        'total_hits__c',
        'is_secondary_search__c',
        'affiliate__c',
        'custom_division_name__c',
        'flagged_by_admin__c',
        'monitoring_turned_off_datetime__c',
        'monitoring_turned_on_datetime__c',
        'entities_download_status__c',
        'monitor_changes_acknowledged_date__c',
        'monitor_changes_acknowledged_by__c',
        'has_monitor_changes__c',
        'monitor_change_date__c',
        'Search_Profile__c',
        'Fuzziness__c'
    };

    public ComplyAdvantageUtil() 
    {

    }

    public static Comply_Advantage_Search__c getSearchBySalesforceId(string complyAdvantageSFRecordId)
    {
        string query = 'select ' + SoqlUtil.getSoqlFieldList(COMPLY_ADVANTAGE_SEARCH_FEILDS)+ ' from Comply_Advantage_Search__c where Id =:complyAdvantageSFRecordId';
        Comply_Advantage_Search__c[] caSearches =  (Comply_Advantage_Search__c[]) Database.Query(query);
        
        return caSearches.isEmpty() ? null : caSearches[0];
    }

    private static Comply_Advantage_Division_Setting__c[] getAllDivisionSettings()
    {
        return [SELECT Name,
                       Search_Profile__c,
                       Secondary_Search_Profile__c,
                       Fuzziness__c,
                       Comply_Advantage_Instance_Setting__r.Name,
                       Comply_Advantage_Instance_Setting__r.API_Key__c
                FROM Comply_Advantage_Division_Setting__c];
    }

    private static Map<string, Comply_Advantage_Division_Setting__c> getDivisionSettinsMap() {
        Map<string, Comply_Advantage_Division_Setting__c> divisionSettings = 
            new Map<string, Comply_Advantage_Division_Setting__c>{};

        for (Comply_Advantage_Division_Setting__c divSettings : DIVISION_SETTINGS) {
                divisionSettings.put(divSettings.Name, divSettings);
        }  
        return divisionSettings;
	} 

    public static String getApiKey(Comply_Advantage_Search__c search) 
    {
        string divisionName = getDivisionName(search);

        return getAPiKey(divisionName, search.Search_Parameter_Mailing_Country__c);
    }

    public static String getAPiKey(String divisionName, String country) {
		Comply_Advantage_Division_Setting__c temp = DIVISION_SETTINGS_MAP.get(divisionName);
        return temp?.Comply_Advantage_Instance_Setting__r?.API_Key__c;
	}

    public static Comply_Advantage_Division_Setting__c getDivisionSettingByName(
        String divisionName,
        String country
    ) {
		Comply_Advantage_Division_Setting__c temp = DIVISION_SETTINGS_MAP.get(divisionName);
        
        return temp;
	}

    public static String getDivisionName(Comply_Advantage_Search__c search) 
    {
        return String.isNotBlank(search.Custom_Division_Name__c) ?  
                                    search.Custom_Division_Name__c :
                                        (String.isNotBlank(search.Division_Name__c) ? 
                                              search.Division_Name__c : '');
    }

	private static Boolean isComplyAdvantageEnabled()
    {
		return Settings__c.getValues('Default') != null && 
                    Settings__c.getValues('Default').Comply_Advantage_Enabled__c;
	}

    public static String getSearchProfile(String searchId, String apiKey) {
        HttpResponse response = getSearchDetailsCallout(searchId, apiKey);
        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>) results.get('content');
            Map<String, Object> responseData = (Map<String, Object>) resWrapper.get('data');   
            Map<String, Object> searchProfile = (Map<String, Object>) responseData.get('search_profile');

            return (String) searchProfile?.get('name');
        }
        
        return null;
    }

    public static HttpResponse getSearchDetailsCallout(String searchId, String apiKey) {
        String serviceURL = String.format(
            'https://api.complyadvantage.com/searches/{0}/details?api_key={1}&share_url=1',
            new List<String> { searchId, apiKey }
        );

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(serviceURL);
        request.setMethod('GET');
        request.setTimeout(120000);

        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 200) {
            Logger.error(
                'Comply Advantage Search Details',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response
            );
        }
        return response;
    }

    public static HttpResponse getMonitoredInfo(String searchId, String apiKey) {
        String serviceURL = String.format(
            'https://api.complyadvantage.com/searches/{0}/monitors?api_key={1}',
            new List<String> { searchId, apiKey }
        );

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(serviceURL);
        request.setMethod('GET');
        request.setTimeout(120000);

        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 200) {
            Logger.error(
                'Comply Advantage Search Details',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response
            );
        }
        return response;
    }

    public static HttpResponse acknowledgeMonitorChange(String searchId, String apiKey) {
        String serviceURL = SPGeneralSettings.getInstance().getValue('CA_Endpoint') + 
                            String.format(
                                SPGeneralSettings.getInstance().getValue('CA_Endpoint_Acknowledge') + '?api_key={1}',
                                new List<String> { searchId, apiKey });

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(serviceURL);
        request.setMethod('POST');
        request.setTimeout(120000);

        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 204) {
            Logger.error(
                'Comply Advantage Search Details',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response
            );
        }
        return response;
    }

    public static HttpResponse updateSearchProfileCallout(String searchId, String searchProfile, String apiKey) {
        if (String.isNotBlank(searchId) &&
            String.isNotBlank(searchProfile) &&
            String.isNotBlank(apiKey)) {
            String serviceURL = String.format(
                'https://api.complyadvantage.com/searches/{0}/search-profile?api_key={1}',
                new List<String> { searchId, apiKey }
            );
    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(serviceURL);
            request.setMethod('PATCH');
            request.setTimeout(120000);
            String requestBody = '{ "search_profile": { "id": "' + searchProfile + '" } }';
            request.setBody(requestBody); 
    
            HttpResponse response = http.send(request);
            if (response.getStatusCode() != 200) {
                Logger.error(
                    'Comply Advantage Search Profile',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response
                );
            }
            return response;
        }

        return null;
    }

    public static HttpResponse downloadReportCallout(String searchId, String apiKey) {
        if (String.isNotBlank(searchId) &&
            String.isNotBlank(apiKey)) {
            String serviceURL = String.format(
                'https://api.complyadvantage.com/searches/{0}/certificate?api_key={1}',
                new List<String> { searchId, apiKey }
            );
    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(serviceURL);
            request.setMethod('GET');
            request.setTimeout(120000);
    
            HttpResponse response = http.send(request);
            if (response.getStatusCode() != 200) {
                Logger.error(
                    'Comply Advantage Search Certificate',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response
                );
            }
            return response;
        }

        return null;
    }

    public static Document__c getDocument(Comply_Advantage_Search__c search) {
        Document__c doc = new Document__c();
        doc.RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_DOCUMENT_CHECK, 'Document__c');
        String documentPrefix = 'Comply Advantage Report: ';
        String documentName = documentPrefix + search.Search_Text__c;
        if (documentName.length() > 80 ) {
            documentName = documentName.substring(0, 79);
        }

        doc.Name = documentName;
        doc.Notes__c = search.Search_Reason__c;
        doc.Document_Type__c = 'Comply Advantage';
        doc.fxAccount__c = search.fxAccount__c;
        doc.Case__c = search.Case__c;
        doc.Account__c = search.fxAccount__r?.Account__c;
        doc.Affiliate__c = search.Affiliate__c;
        doc.Comply_Advantage_Search__c = search.Id;
        
        return doc;
    }

    public static Attachment getAttachment(HttpResponse response, string documentName, Id parentId) {
        Attachment attach = new Attachment();
        attach.Body = response.getBodyAsBlob(); 
        attach.Name = documentName;
        attach.ParentId = parentId;
        attach.ContentType = 'application/pdf';
        attach.IsPrivate = false;           
        return attach;
    }
}