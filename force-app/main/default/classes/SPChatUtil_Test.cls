/**
 * @File Name          : SPChatUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:13:45 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/9/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class SPChatUtil_Test {

    static final String FAKE_AGENT_ID = '0050B0000081XTXQA2';
    static final String FAKE_WORK_ID = '0Bz3C0000000CvkSAE';
    static final String FAKE_SERVICE_PRESENCE_STATUS_ID = '0N53C00000000WUSAY';

    @istest
    static void getServPresenceStatusIdList_test() {
        Test.startTest();
        List<String> result1 = SPChatUtil.getServPresenceStatusIdList(null);
        List<String> result2 = SPChatUtil.getServPresenceStatusIdList('SomChannelDevName');
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertNotEquals(null, result2);
    }

    @istest
    static void getAgentActiveSkills_test() {
        Set<String> result1 = null;
        Set<String> result2 = null;
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0008');
        System.runAs(supervisor) {
            OmnichanelRoutingTestDataFactory.createAgentServiceResWithSkill(
                UserInfo.getUserId(), 
                UserInfo.getUserName(),
                OmnichanelConst.OCAN_SKILL
            );
            String agentId1 = null;
            String agentId2 = UserInfo.getUserId();
            result1 = SPChatUtil.getAgentActiveSkills(agentId1);
            result2 = SPChatUtil.getAgentActiveSkills(agentId2);
        }
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertEquals(1, result2.size());
    }

    @istest
    static void getAgentWorkSkills_test() {
        Test.startTest();
        Set<String> result1 = SPChatUtil.getAgentWorkSkills(null);
        Set<String> result2 = SPChatUtil.getAgentWorkSkills(FAKE_WORK_ID);
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertNotEquals(null, result2);
    }

    @istest
    static void getActiveAgents_test() {
        Test.startTest();
        List<String> result1 = SPChatUtil.getActiveAgents(null);
        List<String> result2 = SPChatUtil.getActiveAgents(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertNotEquals(null, result2);
    }

    @istest
    static void getOtherActiveAgents_test() {
        Test.startTest();
        List<String> result1 = SPChatUtil.getOtherActiveAgents(
            null,
            null
        );
        List<String> result2 = SPChatUtil.getOtherActiveAgents(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID},
            FAKE_AGENT_ID
        );
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertNotEquals(null, result2);
    }

    @istest
    static void checkActiveAgents_test() {
        Test.startTest();
        Set<String> result1 = SPChatUtil.checkActiveAgents(null);
        Set<String> result2 = SPChatUtil.checkActiveAgents(
            new Set<String> {FAKE_AGENT_ID}
        );
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assert(result2.isEmpty());
    }

    @istest
    static void agentIsActive_test() {
        Test.startTest();
        Boolean result1 = SPChatUtil.agentIsActive(null);
        Boolean result2 = SPChatUtil.agentIsActive(FAKE_AGENT_ID);
        Test.stopTest();
        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
    }

    @istest
    static void getChatCapacityWeight_test() {
        Boolean error = false;
        Test.startTest();
        try {
            Integer result = SPChatUtil.getChatCapacityWeight();
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
}