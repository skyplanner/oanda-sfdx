/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-28-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiAddress {
    public String TYPE {get; set;}

    public String address1 {get; set;}

    public String address2 {get; set;}

    public Integer address_id {get; set;}

    public String city {get; set;}

    public String country {get; set;}

    public String postcode {get; set;}

    public String region {get; set;}
}