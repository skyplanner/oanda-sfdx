public with sharing class BatchFixFxdbName implements Database.Batchable<sObject>{
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchFixFxdbName() {
		this.query = 'SELECT id, FXDB_Name__c, First_Name__c, Middle_Name__c, Last_Name__c, Suffix__c from fxAccount__c where Middle_Name__c = null AND FXDB_Name__c like \'%  %\' AND RecordTypeId  = \'012U00000005NQFIA2\' order by LastModifiedDate desc LIMIT 50000';
	}

    public BatchFixFxdbName(String qry) {
		this.query = qry;
	}


   	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		List<fxAccount__c> fxAccounts = (List<fxAccount__c>)batch;
   		for (fxAccount__c fxa : fxAccounts) {
            fxAccountUtil.setFxdbName(fxa, null);
   		}
   		update fxAccounts;
	}

	public void finish(Database.BatchableContext bc) {}
	
	public static Id executeBatch(String qry, Integer batchSize) {
		return Database.executeBatch(new BatchFixFxdbName(qry),batchSize);
	}
}