/**
 * Tests: ComplyAdvantageConditionalUpdate
 * @author Fernando Gomez
 * @see ComplyAdvantageConditionalUpdate
 */
@isTest
private class ComplyAdvantageConditionalUpdateTest {

	/**
	 * public override void execute(String workflowName)
	 */
	@isTest
	private static void execute() {
		Comply_Advantage_Search__c newCASearch, oldCASearch;
		ComplyAdvantageConditionalUpdate updater;

		newCASearch = new Comply_Advantage_Search__c(
			Is_Monitored__c = true
		);

		updater = new ComplyAdvantageConditionalUpdate(newCASearch, null);
		updater.execute('WF_Monitoring_Turned_On_Off_Datetime');
		System.assert(System.now() >= newCASearch.Monitoring_Turned_On_Datetime__c);

		newCASearch.Is_Monitored__c = false;
		oldCASearch = new Comply_Advantage_Search__c(
			Is_Monitored__c = true
		);

		updater = new ComplyAdvantageConditionalUpdate(newCASearch, oldCASearch);
		updater.execute('WF_Monitoring_Turned_On_Off_Datetime');
		System.assert(System.now() >= newCASearch.Monitoring_Turned_Off_Datetime__c);
	}
}