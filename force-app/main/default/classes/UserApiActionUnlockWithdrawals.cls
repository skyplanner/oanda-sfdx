/**
 * User API 'Unlock Withdrawals' action manager class
 */
public class UserApiActionUnlockWithdrawals extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.unlockWithdrawals();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.withdrawalsLocked;
    }
}