@IsTest
private class OutgoingNotificationBusTest {
    @IsTest
    static void outgoingNotificationBusTest() {
        List<Outgoing_Notification__e> newEvents = new List<Outgoing_Notification__e>{
                new Outgoing_Notification__e(
                        EventName__c = 'OutgoingNotificationBus',
                        Key__c = JSON.serialize(new Map<String, String>{'recordId' => '12345', 'env' => 'live'}),
                        Body__c = JSON.serialize(new Map<String, String>{'field1' => 'value', 'field1' => 'value'})
                ),
                new Outgoing_Notification__e(
                        EventName__c = 'WrongClassName',
                        Key__c = JSON.serialize(new Map<String, String>{'recordId' => '12345', 'env' => 'live'}),
                        Body__c = JSON.serialize(new Map<String, String>{'field1' => 'value', 'field1' => 'value'})
                )
        };
        OutgoingNotificationBus outgoingNotification = new OutgoingNotificationBus();
        outgoingNotification.addEventRecord(OutgoingNotificationBus.class, new fxAccount__c(), new fxAccount__c());

        outgoingNotification.outgoingNotifications = newEvents;
        outgoingNotification.addEventsError('testId', 'message1');
        outgoingNotification.addEventsError('testId', 'message2');

        Test.startTest();
            outgoingNotification.publishPlatformEvents();
        Test.stopTest();
        List<AsyncApexJob> jobs = [SELECT Id, Status, NumberOfErrors FROM AsyncApexJob WHERE JobType = 'Queueable' AND ApexClass.Name = 'OutgoingNotificationQueueable'];
        Assert.areEqual(1, jobs.size());
    }
}