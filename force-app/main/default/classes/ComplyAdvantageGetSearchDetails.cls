/*
 *  ComplyAdvantageGetSearchDetails
 * 
 *  This Queable method is called from ComplyAdvantageMonitor class when there is a change in Monitored results
 *  This will get required information to create support case and download the PDF report and attach it to support case
*/
public without sharing class ComplyAdvantageGetSearchDetails implements Queueable,Database.AllowsCallouts 
{
    ComplyAdvantageSearch complyAdvantageSearch;
    
    public static final string CASE_STATUS_OPEN = 'Open';
    public static final string CASE_STATUS_CLOSED = 'Closed';
    public static final string CASE_STATUS_VERIFIED_CLOSED = 'Verified and Closed';
    
    public static final set<string> activeFunnelStages = new set<string>
    {
        'Ready For Funding',
        'Funded',
        'Traded'
    };
    public ComplyAdvantageGetSearchDetails (ComplyAdvantageSearch searchInfo)
    {
        complyAdvantageSearch = searchInfo;
    }
    public void execute(QueueableContext context) 
    {
        if(complyAdvantageSearch.caSearchId != null)
        {
            Http http = new Http();
               
            //setup request
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ complyAdvantageSearch.caSearchId +'?api_key='+ complyAdvantageSearch.settings.apiKey + '&share_url=1';
            request.setEndpoint(serviceURL);
            request.setMethod('GET');
           
            //make service call
            HttpResponse response = http.send(request);

            //process response
            if (response.getStatusCode() == 200) 
            {             
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
       		    Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
       			Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');   
                string matchStatus = (string)responseData.get('match_status');
                string shareUrl = (string)responseData.get('share_url');
                
                //update the comply Advantage Search with latest Results
                Comply_Advantage_Search__c activeSearch = complyAdvantageSearch.currentSearch;
                activeSearch.Match_Status__c = matchStatus;
                activeSearch.Report_Link__c = shareUrl;
                ComplyAdvantageHelper.updateComplyAdvantageSearch(activeSearch, false);  

                //retrieve report PDF and attach to the case
                if (!Test.isRunningTest()) 
                {
                    complyAdvantageSearch.searchReason = 'Monitored Status Update';
                    system.enqueueJob(new ComplyAdvantageReport(complyAdvantageSearch));
                }
       		} else {
                Logger.error(
                    'Comply advantage search details error',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response);
            }
        }         
     }
}