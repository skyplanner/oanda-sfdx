/**
* Test class for CXNoteListController
* 
* @author Gilbert Gao
*/
@isTest
private class CXNoteListControllerTest {

	@testSetUp
	static void prepareTestData(){
		TestDataFactory factory = new TestDataFactory();

		Account acct = factory.createTestAccount();
		fxAccount__c fxa = factory.createFXTradeAccount(acct);
	}
	
	@isTest static void test_getCXNotes() {
		
		fxAccount__c fxa = [select id, Notes__c, Account__c from fxAccount__c limit 1];
		fxa.Notes__c = '[{"author":"ggao3","timestamp":1493901511000,"subject":null,"comment":"hello world5"},{"author":"ggao4","timestamp":1493901512000,"subject":null,"comment":"hello world6 abc"}]';

		update fxa;

		Test.startTest();

		List<CXNoteListController.CXNote> notes = CXNoteListController.getCXNotes(fxa.Id);
		System.assertEquals(2, notes.size());

		List<fxAccount__c> fxas = CXNoteListController.getLiveFxAccounts(fxa.Account__c);
		System.assertEquals(1, fxas.size());


		Test.stopTest();
	}

	@isTest static void test_addNoteEntry(){

		fxAccount__c fxa = [select id, Notes__c, Account__c from fxAccount__c limit 1];

		Test.startTest();

        List<CXNoteListController.CXNote> notes = CXNoteListController.addNoteEntry(fxa.Id, 'Hello World');
        System.assertEquals(1, notes.size());


        notes = CXNoteListController.addNoteEntry(fxa.Id, 'Hello World');
        System.assertEquals(2, notes.size());


		Test.stopTest();

	}

	@isTest static void test_addRFFNoteEntry(){

		fxAccount__c fxa = [select id, Notes__c, Account__c from fxAccount__c limit 1];

		List<ID> fxaIds = new List<ID>();
		fxaIds.add(fxa.Id);

		Test.startTest();

        CXNoteListController.addRFFNoteEntry(fxaIds);
        System.assertNotEquals(null, [select id, Notes__c, Account__c from fxAccount__c where id =: fxa.Id].Notes__c);

		Test.stopTest();

	}	
}