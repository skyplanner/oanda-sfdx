public with sharing class ComplyAdvantageSearchMigrationUId implements Callable
{
    public static final Schema.SObjectField SEARCH_ID_FIELD = Comply_Advantage_Search__c.Fields.Search_Id__c;
    public static final string SEARCH_REASON = 'CA-MIGRATION';

    public static final string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    public static final string onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public static final Id complyAdvantageCaseQueueId = UserUtil.getQueueId('Comply Advantage Cases');
    public static final string apiKey =  SPGeneralSettings.getInstance().getValue('CA_Migration_Key');
    
    public static Map<string, string> countryNameByISOMap =  getCountryCodes();    

    List<Comply_Advantage_Migration_Record__c> searchesToMigrate;

    Map<string, ComplyAdvantageSearchRecord> existingSearchesBySearchRef = new Map<string, ComplyAdvantageSearchRecord>{};
    Map<string, ComplyAdvantageSearchRecord> complyAdvantageSearchBySearchRef = new Map<string, ComplyAdvantageSearchRecord>{};
    Map<string, FxAccountModel> fxAccountDetailsByUserId = new Map<string, FxAccountModel>{};
    Map<string, string> searchRefClientRefMap = new Map<string, string>{};

    Map<Id, string> fxacUserIdByAccountId = new Map<Id, string>{};

    set<Decimal> userIds = new set<Decimal>{};
    set<Id> accountIds = new set<Id>{};
    set<String> existingSearchReferences = new set<String>{};

    public Object call(String action, Map<String, Object> args) 
    {
        switch on action 
        {
            when 'migrate' 
            {
                migrate((List<Comply_Advantage_Migration_Record__c>)args.get('records'));       
            }
            when 'checkMigrationRecords' 
            {
                checkMigrationRecords((List<Comply_Advantage_Migration_Record__c>)args.get('records'));       
            }
        }
        return null;
    }

    public ComplyAdvantageSearchMigrationUId() 
    {

    }
    
    
    public void checkMigrationRecords(List<Comply_Advantage_Migration_Record__c> checkMigrationRecords)
    {
        set<string> clientRefUserNames = new set<string>{};
        Set<Decimal> clientRefUserIds = new Set<Decimal>{};

        Map<string, Comply_Advantage_Migration_Record__c[]> recsByClientRef = new Map<string, Comply_Advantage_Migration_Record__c[]>{};

        for(Comply_Advantage_Migration_Record__c mr : checkMigrationRecords)
        {
            if(String.isNotBlank(mr.Client_Reference__c) && !mr.Client_Reference__c.isNumeric())
            {
                clientRefUserNames.add(mr.Client_Reference__c.toLowerCase());
                
                Comply_Advantage_Migration_Record__c[] records = recsByClientRef.get(mr.Client_Reference__c.toLowerCase());
                if(records == null)
                {
                    records = new Comply_Advantage_Migration_Record__c[]{};
                }
                records.add(mr);
                recsByClientRef.put(mr.Client_Reference__c.toLowerCase(), records);
            }
            if(String.isNotBlank(mr.Client_Reference__c) && mr.Client_Reference__c.isNumeric())
            {
                clientRefUserIds.add(Decimal.valueOf(mr.Client_Reference__c));
                clientRefUserNames.add(mr.Client_Reference__c);

                Comply_Advantage_Migration_Record__c[] records = recsByClientRef.get(mr.Client_Reference__c);
                if(records == null)
                {
                    records = new Comply_Advantage_Migration_Record__c[]{};
                }
                records.add(mr);
                recsByClientRef.put(mr.Client_Reference__c, records);
            }
        }

        for(fxAccount__c fxa : [Select Id,fxTrade_User_Id__c, User_Name_Lower_Case__c
                                From fxAccount__c 
                                where (fxTrade_User_Id__c IN :clientRefUserIds OR User_Name_Lower_Case__c IN :clientRefUserNames)
                                      AND RecordType.Name = 'Retail Live'])
        {
            if(clientRefUserIds.contains(fxa.fxTrade_User_Id__c))
            {
                string userId = string.valueOf(fxa.fxTrade_User_Id__c);
                
                Comply_Advantage_Migration_Record__c[] mrRecords = recsByClientRef.get(userId);
                if(mrRecords != null)
                {
                    for(Comply_Advantage_Migration_Record__c mr : mrRecords)
                    {
                        mr.Is_Client_Ref_User_Id__c = true;
                        mr.FxAccount_Matching_UserId__c = fxa.Id;
                    }
                }
            }
            if(clientRefUserNames.contains(fxa.User_Name_Lower_Case__c))
            {
                Comply_Advantage_Migration_Record__c[] mrRecords = recsByClientRef.get(fxa.User_Name_Lower_Case__c);
                if(mrRecords != null)
                {
                    for(Comply_Advantage_Migration_Record__c mr : mrRecords)
                    {
                        mr.Is_Client_Ref_User_Name__c = true;
                        mr.FxAccount_Matching_Username__c = fxa.Id;
                    }
                }
            }
        }

        Comply_Advantage_Migration_Record__c[] updateRecords = new Comply_Advantage_Migration_Record__c[]{};
        for(Comply_Advantage_Migration_Record__c[] migrateRecords : recsByClientRef.values())
        {
            updateRecords.addAll(migrateRecords);
        }
        Database.update(updateRecords, false);
    }

    public void migrate(List<Comply_Advantage_Migration_Record__c> searchesToMigrate)
    {
       this.searchesToMigrate = searchesToMigrate;

       queryCASearchRecords();

       setupData();

       saveCACases();

       saveCASearchRecords();

       updateMigrationStatus();
    }

    private void setupData()
    {
        parseSearchRecords();

        getFxAccounts();

        getEntityContacts();

        getSearchDetailsFromCA();

        downloadReportsFromCA();
    }

    private void queryCASearchRecords()
    {
        set<string> searchReferences = new set<String>{};
        for(Comply_Advantage_Migration_Record__c mr : searchesToMigrate)
        {
            if(String.isNotBlank(mr.Search_Reference__c))
            {
                searchReferences.add(mr.Search_Reference__c);
            }
        }
        for(Comply_Advantage_Search__c search :  [select Id,Search_Reference__c From Comply_Advantage_Search__c Where Search_Reference__c IN :searchReferences])
        {
            existingSearchReferences.add(search.Search_Reference__c);
        }

    }
    private void parseSearchRecords()
    {
        for(Comply_Advantage_Migration_Record__c migrateRecord : searchesToMigrate)
        {
            ComplyAdvantageSearchRecord searchRec = new ComplyAdvantageSearchRecord(migrateRecord);

            if(existingSearchReferences.contains(searchRec.searchReference))
            {
                existingSearchesBySearchRef.put(searchRec.searchReference, searchRec);
            }
            else if(searchRec.clientRef.isNumeric() && String.isNotBlank(searchRec.searchReference))
            {
                userIds.add(Decimal.valueOf(searchRec.clientRef));

                searchRefClientRefMap.put(searchRec.searchReference, searchRec.clientRef);

                complyAdvantageSearchBySearchRef.put(searchRec.searchReference, searchRec);
            }
        }
    }

   private void getFxAccounts()
   {
        string[] caseRecordTypeIds= new string[]{onboardingCaseRecordTypeId,complianceCheckCaseRecordTypeId};

        system.debug('userIds ' + userIds );
        if(!userIds.isEmpty())
        {
            fxAccount__c[] fxAccounts = [SELECT Id,
                                                fxTrade_User_Id__c,
                                                Name,
                                                User_Name_Lower_Case__c,
                                                Birthdate__c,
                                                Citizenship_Nationality__c,
                                                Division_Name__c,
                                                Contact__c,
                                                Account__c,
                                                Account__r.Name,
                                                Account__r.PersonMailingCountry,
                                                Account__r.Nickname__pc,
                                                (select Id, OwnerId,RecordTypeId from Cases__r where RecordTypeId IN :caseRecordTypeIds)
                                        FROM fxAccount__c 
                                        WHERE RecordType.Name = 'Retail Live' AND
                                              fxTrade_User_Id__c IN :userIds];

            for(fxAccount__c fxa : fxAccounts)
            {
                FxAccountModel fxaDetails = new FxAccountModel(fxa);

                if(fxa.Account__c != null)
                {
                    accountIds.add(fxa.Account__c);
                    fxacUserIdByAccountId.put(fxa.Account__c, string.valueOf(fxa.fxTrade_User_Id__c));
                }
                fxAccountDetailsByUserId.put(string.valueOf(fxa.fxTrade_User_Id__c), fxaDetails);
            }    
        }
        system.debug('fxAccountDetailsByUserId :' + fxAccountDetailsByUserId);                 
    }

    private void getEntityContacts()
    {
        for(Entity_Contact__c ec : [select Id,
                                           First_Name__c,
                                           Last_Name__c,
                                           Citizenship__c,
                                           Nationality__c,
                                           Country__c,
                                           Birthdate__c, 
                                           Account__c
                                    From Entity_Contact__c 
                                    Where Account__c IN :accountIds AND RecordType.Name = 'General'])
        {
            if(fxacUserIdByAccountId.containsKey(ec.Account__c))
            {
                string fxacUserId = fxacUserIdByAccountId.get(ec.Account__c);
                if(fxAccountDetailsByUserId.containsKey(fxacUserId))
                {
                    EntityContactModel ecm = new EntityContactModel(ec); 
                    FxAccountModel fxaDetails = fxAccountDetailsByUserId.get(fxacUserId);
                    fxaDetails.entityContacts.add(ecm);

                    string entityContactName = ComplyAdvantageHelper.getEntityContactName(ec);
                    fxaDetails.entityContactNames.add(entityContactName);
                }
            }
        }
    }

    private void getSearchDetailsFromCA()
    {
        for(ComplyAdvantageSearchRecord searchRec : complyAdvantageSearchBySearchRef.values())
        {
            try 
            {           
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndpoint('https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'?api_key='+ apiKey + '&share_url=1');
                request.setMethod('GET');
            
                HttpResponse response = http.send(request);

                if (response.getStatusCode() == 200) 
                {             
                    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
                    Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                    Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');   
                    
                    searchRec.searchId = (integer)responseData.get('id');
                    searchRec.matchStatus = (string)responseData.get('match_status');
                    searchRec.shareUrl = (string)responseData.get('share_url');
                    searchRec.totalHits = (integer)responseData.get('total_matches');
                }
            } 
            catch (Exception ex) 
            {
                Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
            }
        }
    }

    private void downloadReportsFromCA()
    {
        for(ComplyAdvantageSearchRecord searchRec : complyAdvantageSearchBySearchRef.values())
        {
            try 
            {    
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndpoint('https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'/certificate?api_key='+ apiKey);
                request.setMethod('GET');
                request.setTimeout(120000);
                HttpResponse response = http.send(request);

                if (response.getStatusCode() == 200) 
                { 
                    FxAccountModel fxa = fxAccountDetailsByUserId.get(searchRec.clientRef);
                    searchRec.reportDownloadResponse = response;
                }
            } 
            catch (Exception ex) 
            {
                Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
            }
        }
    }

    private void saveCACases()
    {
        try 
        {
            Map<string, Case> caseByFxaUsername = new Map<string, Case>{};
            for(ComplyAdvantageSearchRecord sr :  complyAdvantageSearchBySearchRef.values())
            {
                string fxAccUserId = sr.clientRef;
                if(fxAccountDetailsByUserId.containsKey(fxAccUserId))
                {
                    FxAccountModel fxacDetails = fxAccountDetailsByUserId.get(fxAccUserId);
                    if(!caseByFxaUsername.containsKey(fxAccUserId) && fxacDetails.complyAdvatageCaseId == null)
                    {
                        Case caCase = getCACaseObject(fxAccUserId);
                        fxacDetails.complyAdvatageCase = caCase;
                        fxAccountDetailsByUserId.put(fxAccUserId, fxacDetails);

                        caseByFxaUsername.put(fxAccUserId, caCase);
                    }
                }
            }
            if(!caseByFxaUsername.isEmpty())
            {
                case[] newCACases = caseByFxaUsername.values();
                Database.SaveResult[] result = Database.insert(newCACases, false);
            }
        } 
        catch (Exception ex) 
        {
            Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
        }
	} 

	private void saveCASearchRecords()
    {
        try 
        {
            Comply_Advantage_Search__c[] migratedSearches = new Comply_Advantage_Search__c[]{};
            for(ComplyAdvantageSearchRecord sr :  complyAdvantageSearchBySearchRef.values())
            {
                if(searchRefClientRefMap.containsKey(sr.searchReference))
                {
                    string clientRef = searchRefClientRefMap.get(sr.searchReference);

                    if(fxAccountDetailsByUserId.containsKey(clientRef))
                    {
                        Comply_Advantage_Search__c searchRecord = parseSearchRequest(sr, fxAccountDetailsByUserId.get(clientRef));
                        
                        if(searchRecord.Case__c != null)
                        {
                            migratedSearches.add(searchRecord);
                        }
                        sr.searchRecord = searchRecord;
                    }
                }
            }
            if(!migratedSearches.isEmpty())
            {
                Database.UpsertResult[] upsertResult =  Database.upsert(migratedSearches, SEARCH_ID_FIELD, false);
                system.debug('Search Records Insert Result : ' + upsertResult);

                saveReports(migratedSearches);
            }
        } 
        catch (Exception ex) 
        {
            Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
        }
	} 

    private void saveReports(Comply_Advantage_Search__c[] migratedSearches)
    {
        try 
        {
            
            Document__c[] reports = new Document__c[]{};
            Map<Id, Comply_Advantage_Search__c> searchRecordsMap = new Map<Id,Comply_Advantage_Search__c>{};

            for(Comply_Advantage_Search__c search :  migratedSearches)
            {
                if(search.Id != null)
                {
                    Document__c docRecord = getDocumentObject(search);
                    reports.add(docRecord);

                    ComplyAdvantageSearchRecord casr =  complyAdvantageSearchBySearchRef.get(search.Search_Reference__c);
                    casr.report = docRecord;

                    searchRecordsMap.put(search.Id, search);
                }
            }
            if(!reports.isEmpty())
            {
                Database.SaveResult[] docSaveResult =  Database.insert(reports, false);
                system.debug('Documents Insert Result : ' + docSaveResult);

                saveAttachments(reports, searchRecordsMap);
            }
        }
        catch(Exception ex)
        {
            Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
        }
    }

    private void saveAttachments(Document__c[] reports, Map<Id,Comply_Advantage_Search__c> searchRecordsMap)
    {
        try 
        {        
            Attachment[] attachments = new Attachment[]{};

            for(Document__c doc : reports)
            {
                if(doc.Id != null)
                {
                    Comply_Advantage_Search__c search = searchRecordsMap.get(doc.Comply_Advantage_Search__c);
                    ComplyAdvantageSearchRecord casr = complyAdvantageSearchBySearchRef.get(search.Search_Reference__c);

                    Attachment attachment = getAttachmentObject(casr.reportDownloadResponse, doc.name, doc.Id);
                    attachments.add(attachment);
                
                    casr.attachment = attachment;
                }
            }
            if(!attachments.isEmpty())
            {
                Database.SaveResult[] attachSaveResult =  Database.insert(attachments, false);
                system.debug('Attachment Insert Result : ' + attachSaveResult);
            }
        }
        catch(Exception ex)
        {
            Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
        }
    }

    private void updateMigrationStatus()
    {
        try
        {
            Comply_Advantage_Migration_Record__c[] migRecords = new Comply_Advantage_Migration_Record__c[]{};
            
            for(ComplyAdvantageSearchRecord sr :  existingSearchesBySearchRef.values())
            {
                Comply_Advantage_Migration_Record__c migRecord = sr.migrationRecord;
                migRecord.Migration_Status__c = 'Exists';
                migRecords.add(migRecord);
            }
            for(ComplyAdvantageSearchRecord sr :  complyAdvantageSearchBySearchRef.values())
            {
                string migrationStatus;
                boolean caseCreated = false;
                boolean searchCreated = false;
                boolean reportCreated = false;
                boolean attachmentCreated = false;

                Comply_Advantage_Migration_Record__c migRecord = sr.migrationRecord;
                FxAccountModel fxa = fxAccountDetailsByUserId.get(sr.clientRef);

                if(fxa == null)
                {
                    migrationStatus = 'No Client';
                }
                else 
                {
                    caseCreated = fxa.complyAdvatageCase != null && fxa.complyAdvatageCase.Id != null;
                    searchCreated = sr.searchRecord  != null && sr.searchRecord.Id != null;
                    reportCreated =  sr.report != null && sr.report.Id != null;
                    attachmentCreated = sr.attachment != null && sr.attachment.Id != null;
                    
                    boolean done =  caseCreated && searchCreated && reportCreated && attachmentCreated;
                    migrationStatus = done ? 'Done' : 'Failed';
                }
              
                migRecord.CA_Case_Created__c = caseCreated;
                migRecord.Case__c = caseCreated ? fxa.complyAdvatageCase.Id : null;

                migRecord.CA_Search_Record_Created__c = searchCreated;
                migRecord.Comply_Advantage_Search__c = searchCreated ? sr.searchRecord.Id : null;

                migRecord.Report_Document_Created__c = reportCreated;
                migRecord.Document__c =  reportCreated ? sr.report.Id : null;

                migRecord.Report_Attachment_Created__c = attachmentCreated;
               
                migRecord.Migration_Status__c = migrationStatus;
              
                migRecords.add(migRecord);
            }

            if(!migRecords.isEmpty())
            {
                update migRecords;
            }
        } 
        catch (Exception ex) 
        {
            Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
        }
    }
  
    
    private Comply_Advantage_Search__c parseSearchRequest(ComplyAdvantageSearchRecord searchRec , FxAccountModel fxac)
    {
        Comply_Advantage_Search__c newComplyAdvantageSearch = new Comply_Advantage_Search__c();
      
        newComplyAdvantageSearch.Search_Reason__c = SEARCH_REASON;
        newComplyAdvantageSearch.Name = searchRec.searchText.length() > 80 ?  searchRec.searchText.substring(0, 79) :  searchRec.searchText;
        newComplyAdvantageSearch.Search_Reference__c = searchRec.searchReference;
        newComplyAdvantageSearch.Case__c = fxac.complyAdvatageCase.Id;
        newComplyAdvantageSearch.fxAccount__c = fxac.recordId;
        newComplyAdvantageSearch.Custom_Division_Name__c = fxac.divisionName;
        newComplyAdvantageSearch.Is_Monitored__c = true;
        newComplyAdvantageSearch.Case_Management_Link__c = 'https://app.complyadvantage.com/#/case-management/search/' + searchRec.searchReference;
        newComplyAdvantageSearch.Search_Text__c = searchRec.searchText ;
        newComplyAdvantageSearch.Search_Parameter_Birth_Year__c = String.valueOf(searchRec.yearOfBirth);

        newComplyAdvantageSearch.Search_Id__c = String.valueOf(searchRec.searchId);
        newComplyAdvantageSearch.Match_Status__c = searchRec.matchStatus;
        newComplyAdvantageSearch.Report_Link__c = searchRec.shareUrl;
        newComplyAdvantageSearch.Total_Hits__c = searchRec.totalHits;
        
        newComplyAdvantageSearch.Is_Alias_Search__c = (searchRec.searchText == fxac.alias);
        if(searchRec.searchText == fxac.alias || searchRec.searchText == fxac.accountName)
        {
            if(searchRec.countries.contains(fxac.citizenship))
            newComplyAdvantageSearch.Search_Parameter_Citizenship_Nationality__c = fxac.citizenship;

            if(searchRec.countries.contains(fxac.mailingCountry))
            newComplyAdvantageSearch.Search_Parameter_Mailing_Country__c = fxac.mailingCountry;
        }
        else if(fxac.entityContactNames.contains(searchRec.searchText))
        {
            EntityContactModel ec = filterEntityContactsByName(searchRec , fxac.entityContacts);
            if(ec != null)
            {
                newComplyAdvantageSearch.Entity_Contact__c  = ec.recId;
                newComplyAdvantageSearch.Search_Parameter_Citizenship_Nationality__c = ec.citizenship;
                newComplyAdvantageSearch.Search_Parameter_Mailing_Country__c = ec.mailingCountry;
            }
        }
        
        return newComplyAdvantageSearch;
    }

    private EntityContactModel filterEntityContactsByName(ComplyAdvantageSearchRecord searchRec , EntityContactModel[] entityContacts)
    {
        EntityContactModel[] contactsMatchingName = new EntityContactModel[]{};
        for(EntityContactModel ec: entityContacts)
        {
            if(ec.name.trim().toLowerCase() == searchRec.searchText.trim().toLowerCase())
            {
                contactsMatchingName.add(ec);
            }
        }
        if(contactsMatchingName.size() == 1)
        {
            return contactsMatchingName[0];
        }

        //if we have more than one ECs with mathcing name, compare all parameters
        for(EntityContactModel ec: contactsMatchingName)
        {
            if(ec.yearOfBirth == searchRec.yearOfBirth && searchRec.countries.contains(ec.citizenship) &&  searchRec.countries.contains(ec.mailingCountry))
            {
                return ec;
            }
        }
        return null;
    }

    private static Map<string, string> getCountryCodes()
    {
        Map<string, string> countryNamesMap = new Map<string, string>{};
        for(Country_Setting__c countrySetting : Country_Setting__c.getAll().values()) 
        {
            String countryName = countrySetting.Long_Name__c != null ? countrySetting.Long_Name__c : countrySetting.Name;  
            countryNamesMap.put(countrySetting.ISO_Code__c.toUpperCase(), countryName);
        }
        return countryNamesMap;
	} 

	private Case getCACaseObject(String fxAccUserName)
    {
        FxAccountModel fxa = fxAccountDetailsByUserId.get(fxAccUserName);
        return new Case
        (
            RecordTypeId = complianceCheckCaseRecordTypeId,  
            Status = 'Closed', 
            Origin = 'Migration',
            OwnerId = complyAdvantageCaseQueueId,
            fxAccount__c = fxa.recordId,
            AccountId = fxa.accountId,
            ContactId = fxa.contactId,
            ParentId = fxa.onboardingCaseId,
            Subject = 'Comply Advantage Case for ' + fxa.accountName
        );
	}
    private Document__c getDocumentObject(Comply_Advantage_Search__c searchRec)
    {
        Document__c doc = new Document__c();
        doc.RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_DOCUMENT_CHECK, 'Document__c');

        string documentName = 'Comply Advantage Report: ' + searchRec.Search_Text__c;
        if(documentName.length() > 80 )
        {
            documentName = documentName.substring(0, 79);
        }
        doc.name = documentName;
        doc.Notes__c = SEARCH_REASON;
        doc.Document_Type__c = 'Comply Advantage';
        doc.fxAccount__c = searchRec.fxAccount__c;   
        doc.Case__c = searchRec.Case__c;    
        doc.Comply_Advantage_Search__c = searchRec.Id;
        return doc;
    }

    private Attachment getAttachmentObject(HttpResponse response, string documentName, Id parentId)
    {
        Attachment attach = new Attachment();
        attach.Body = response.getBodyAsBlob(); 
        attach.Name = documentName;
        attach.ParentId = parentId;
        attach.ContentType = 'application/pdf';
        attach.IsPrivate = false;           
        return attach;
    }
    public class ComplyAdvantageSearchRecord
    {
        Comply_Advantage_Migration_Record__c migrationRecord;

        string clientRef;    
        string searchText;
        string searchReference;
        string countryCodes;
        set<string> countries;
        integer yearOfBirth;

        integer searchId;
        string matchStatus;
        string shareUrl;
        integer totalHits;

        HttpResponse reportDownloadResponse;

        Document__c report;
        Attachment attachment;
        Comply_Advantage_Search__c searchRecord;

        public ComplyAdvantageSearchRecord(Comply_Advantage_Migration_Record__c rec)
        {
            this.migrationRecord = rec;

            this.clientRef = rec.Client_Reference__c.toLowerCase();
            this.searchReference = rec.Search_Reference__c;
            this.searchText = rec.Search_Term__c;
            this.countryCodes = rec.Country_Codes__c;
            this.yearOfBirth = rec.Birth_Year__c != null ? integer.valueOf(rec.Birth_Year__c) : null;

            this.countries = new set<string>{};
            if(String.isNotBlank(countryCodes))
            {
                string[] countryCodes = rec.Country_Codes__c.split(',');
                for(string countryCode : countryCodes)
                {
                    string isoCode = countryCode.trim().toUpperCase();
                    if(countryNameByISOMap.containsKey(isoCode))
                    {
                        this.countries.add(countryNameByISOMap.get(isoCode));
                    }
                }
            }
        }
    }
    
    public class EntityContactModel
    {
        Id recId;
        string name;
        string citizenship;
        string mailingCountry;
        integer yearOfBirth;

        public EntityContactModel(Entity_Contact__c ec)
        {
            this.recId = ec.Id;
            this.name = ComplyAdvantageHelper.getEntityContactName(ec);
            this.citizenship = ec.Citizenship__c != null ? ec.Citizenship__c : (ec.Nationality__c);
            this.mailingCountry = ec.Country__c;
            this.yearOfBirth = ec.Birthdate__c != null ? ec.Birthdate__c.Year() : null;
        }
    }
    public class FxAccountModel
    {
        Id recordId;

        Id accountId;
        Id onboardingCaseId;
        Id complyAdvatageCaseId;
        Id contactId;

        string userName;
        Decimal fxTradeUserId;

        string citizenship;
        string mailingCountry;
        integer yearOfBirth;
        string accountName;
        string alias;
        string divisionName;

        Map<string ,EntityContactModel> entityContactNameMap = new Map<string, EntityContactModel>{};
        EntityContactModel[] entityContacts = new EntityContactModel[]{};
        set<string> entityContactNames = new set<string>{};

        Case onboardingCase;
        Case complyAdvatageCase;

        public FxAccountModel(fxAccount__c fxa)
        {
            this.recordId = fxa.Id;

            this.userName = fxa.Name;
            this.fxTradeUserId =fxa.fxTrade_User_Id__c;
            this.citizenship = fxa.Citizenship_Nationality__c;
            this.yearOfBirth = fxa.Birthdate__c != null ? fxa.Birthdate__c.year() : null;
            this.accountId = fxa.Account__c;
            this.divisionName = fxa.Division_Name__c;

            this.contactId = fxa.Contact__c;

            if(fxa.Account__c != null)
            {
                this.accountName = fxa.Account__r.Name;
                this.alias = fxa.Account__r.Nickname__pc;
                this.mailingCountry = fxa.Account__r.PersonMailingCountry;
            }
            if(fxa.Cases__r != null)
            {
                for(Case ca : fxa.Cases__r)
                {
                    if(ca.RecordTypeId == onboardingCaseRecordTypeId)
                    {
                        this.onboardingCaseId = ca.Id;
                        this.onboardingCase = ca;
                    }
                    if(ca.RecordTypeId == complianceCheckCaseRecordTypeId)
                    {
                        this.complyAdvatageCaseId = ca.Id;
                        this.complyAdvatageCase = ca;
                    }
                }
            }
        }      
    }

}