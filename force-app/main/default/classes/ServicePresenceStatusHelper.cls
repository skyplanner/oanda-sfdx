/**
 * @File Name          : ServicePresenceStatusHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/24/2021, 2:37:09 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/24/2021, 2:27:38 PM   acantero     Initial Version
**/
public inherited sharing class ServicePresenceStatusHelper {

    public static ServicePresenceStatus getByDevName(
        String devName, 
        Boolean required
    ) {
        if (String.isBlank(devName)) {
            throw new ServPresStatusHelperException('devName is blank');
        }
        //else...
        ServicePresenceStatus result = null;
        List<ServicePresenceStatus> recList = [
            SELECT 
                Id 
            FROM 
                ServicePresenceStatus 
            WHERE 
                DeveloperName = :devName
            LIMIT 1
        ];
        if (!recList.isEmpty()) {
            result = recList[0];
        }
        checkRequired(
            result,
            required,
            'devName: ' + devName
        );
        return result;
    }

    @TestVisible
    static void checkRequired(
        ServicePresenceStatus rec,
        Boolean required,
        String criteria
    ) {
        if (
            (rec == null) &&
            (required == true)
        ) {
            throw new ServPresStatusHelperException(
                'ServicePresenceStatus not found, ' + criteria
            );
        }
    }

    //**************************************************************

    public class ServPresStatusHelperException extends Exception {
    }

}