/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 18th, 2019
 */
global class HelpPortalArticleCtrl extends HelpPortalBase {
	global transient FAQ article { get; private set; }

	/**
	 * Standard constructor
	 * @param ctrl
	 */
	global HelpPortalArticleCtrl(
			ApexPages.KnowledgeArticleVersionStandardController ctrl) {
		super();                
		// we need the record so we add all necessary fields
		FAQ__kav record = (FAQ__kav)ctrl.getRecord();
		// we retrieve the article as wrapper	
        String idParameter = ApexPages.currentPage().getParameters().get('articleId');        
		//article = FAQ.doWrap(getFaq(record.Id, true));
        article = FAQ.doWrap(getFaq(Id.valueof(idParameter), true));
	}

	/**
	 * Standard constructor
	 * @param ctrl
	 */
	global HelpPortalArticleCtrl(Id articleId) {
		super();	
    	// we retrieve the article as wrapper		
		article = FAQ.doWrap(getFaq(articleId, false));
	}

	/**
	 * Standard constructor
	 * @param ctrl
	 */
	global HelpPortalArticleCtrl(Id articleId, String language) {
		super(language);	
    	// we retrieve the article as wrapper		
		article = FAQ.doWrap(getFaq(articleId, false));
	}

	/**
	 * Returns the faq
	 * @param articleId
	 */
	private FAQ__kav getFaq(Id articleId, Boolean updateStat) {
		return updateStat ? [
				SELECT
					Id,
					KnowledgeArticleId,
					Title,
					Answer__c,
					Answer_2__c,
					AllowFeedback__c
				FROM FAQ__kav
				WHERE Id = :articleId
				UPDATE VIEWSTAT
			] : [
				SELECT
					Id,
					KnowledgeArticleId,
					Title,
					Answer__c,
					Answer_2__c,
					AllowFeedback__c
				FROM FAQ__kav
				WHERE Id = :articleId
			];
	}  

	/**
	 * Save a rating for the current article
	 * @param overallRating
	 * @param ranking
	 * @param comments
	 */
	global void submitRating(String overallRating, String ranking, String comments) {   
		insert new FaqRating__c(
			FAQ__c = article.faqId,
			OverallRating__c = overallRating,
			Ranking__c = ranking,
			Comments__c = comments
		);
	}
}