@isTest
private class EventMessageUtilTest {

    public static final String TEST_MSG_TYPE = 'Chat Form';
    public static final Boolean TEST_MSG_ACTVE = true;
    public static final String TEST_MSG_ENG1 = 'Message1';
    public static final String TEST_MSG_ENG2 = 'Message2';
    public static final String TEST_MSG_SPA = 'Message in Spanish';
    public static final String TEST_LANGUAGE_ENGLISH = 'en_US';
    public static final String TEST_LANGUAGE_SPANISH = 'es';

    @testSetup
    static void setup() {
      Event_Message__c msg1 = new Event_Message__c(
         Type__c = TEST_MSG_TYPE, 
         Active__c = TEST_MSG_ACTVE,
         Message_English__c = TEST_MSG_ENG1,
         Message_Spanish__c = TEST_MSG_SPA
      );
      insert msg1;
    }

	@isTest
	static void testDeactivateExistingMessages() {
        List<Event_Message__c> eventMessagesBefore = [SELECT Type__c, Active__c, Message_English__c FROM Event_Message__c WHERE Active__c = true AND Type__c = :TEST_MSG_TYPE];
        System.assertEquals(eventMessagesBefore.size(), 1);
        System.assertEquals(eventMessagesBefore[0].Message_English__c, TEST_MSG_ENG1);
		
        Event_Message__c msg2 = new Event_Message__c(
            Type__c = TEST_MSG_TYPE, 
            Active__c = TEST_MSG_ACTVE,
            Message_English__c = TEST_MSG_ENG2
        );
        insert msg2;

        List<Event_Message__c> eventMessagesAfter = [SELECT Type__c, Active__c, Message_English__c FROM Event_Message__c WHERE Active__c = true AND Type__c = :TEST_MSG_TYPE];
		System.assertEquals(eventMessagesAfter.size(), 1);
        System.assertEquals(eventMessagesAfter[0].Message_English__c, TEST_MSG_ENG2);
	}

    @isTest
	static void testGetActiveEventMessageLang1() {
        String msg1 = EventMessageUtil.getActiveEventMessageLang(TEST_LANGUAGE_ENGLISH, TEST_MSG_TYPE);
        System.assertEquals(msg1, TEST_MSG_ENG1);

        String msg2 = EventMessageUtil.getActiveEventMessageLang(TEST_LANGUAGE_SPANISH, TEST_MSG_TYPE);
        System.assertEquals(msg2, TEST_MSG_SPA);

	}
}