/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-10-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasGroupsInfoGetResponse {
    public TasGroupsInfo groups {get; set;}    

    public List<Map<String, String>> getPricingGroups() {
        return getGroupItems(groups?.pricing_groups);
    }

    public List<Map<String, String>> getCommissionGroups() {
        return getGroupItems(groups?.commission_groups);
    }

    public List<Map<String, String>> getCurrencyConversionGroups() {
        return getGroupItems(groups?.currency_conversion_groups);
    }

    public List<Map<String, String>> getFinancingGroups() {
        return getGroupItems(groups?.financing_groups);
    }

    List<Map<String, String>> getGroupItems(List<TasBaseGroup> items) {
        List<Map<String, String>> result = new List<Map<String, String>>();

        if (items != null) {
            items.sort();
            
            for (TasBaseGroup g : items) {
                result.add(new Map<String, String> {
                    'label' => g.label,
                    'value' => string.valueOf(g.id)});
            }
        }

        return result;
    }
}