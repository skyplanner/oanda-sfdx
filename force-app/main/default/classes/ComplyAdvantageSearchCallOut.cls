/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-03-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public without sharing class ComplyAdvantageSearchCallOut  implements Queueable,Database.AllowsCallouts
{
    private string caseClosedStatus = 'Closed';
    private string caseVerifiedClosedStatus = 'Verified and Closed';
    public string onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public Integer currentYear = System.Today().year();
    public set<string> positiveStatusList =new set<string>{'potential_match','true_positive','unknown'};   
    private static boolean run = true;

    ComplyAdvantageSearch complyAdvantageSearch;

    Comply_Advantage_Search__c nameSearch;
    Comply_Advantage_Search__c aliasSearch;
	Comply_Advantage_Search__c primaryNameSearch = new Comply_Advantage_Search__c();
    Comply_Advantage_Search__c primaryAliasSearch = new Comply_Advantage_Search__c();

    public List<Map<String, Object>> searchesResponse;
    
    /**
     * Do search callout async if possible
     */
    public static Id processAsync(
        ComplyAdvantageSearch caSearchInfo)
    {
        System.debug('processAsync-caSearchInfo: ' + caSearchInfo);

        if(LimitsUtil.canEnqueueJob()) {
            System.debug('Comply Advantage Check : Comply Advantage call queued');
            return
                System.enqueueJob(
                    new ComplyAdvantageSearchCallOut(
                        caSearchInfo));
        } else {
            process(caSearchInfo);
        }
        return null;
    }

    /**
     * Do search callout
     */
    public static void process(
        ComplyAdvantageSearch caSearchInfo)
    {
        new ComplyAdvantageSearchCallOut(
            caSearchInfo).processing();
    }

    /**
     * Constructor
     */
    public ComplyAdvantageSearchCallOut(ComplyAdvantageSearch searchInfo) 
    {
        complyAdvantageSearch = searchInfo;
        searchesResponse = new List<Map<String, Object>>();
    }

    public void execute(QueueableContext context) {
        processing();
    }

    public void processing() {
        HttpResponse nameSearchResponse,secondarynameSearchResponse;
        HttpResponse aliasSearchResponse,secondaryaliasSearchResponse;
        HttpResponse nameSearchReportResponse,secondarynameSearchReportResponse;
        HttpResponse aliasSearchReportResponse,secondaryaliasSearchReportResponse;
        HttpResponse disableOldNameSearchResponse,disablePrimaryNameSearchResponse;
        HttpResponse disableOldAliasSearchResponse,disablePrimaryAliasSearchResponse;
        Boolean isNameSecondarySearch= false;
        Boolean isAliasSecondarySearch = false;

        if(string.isNotBlank(complyAdvantageSearch.searchText))
        {
            nameSearchResponse = newSearchCallOut(complyAdvantageSearch.searchText,false);
            if(nameSearchResponse != null && nameSearchResponse.getStatusCode() == 200)
            {
                nameSearchReportResponse = downloadReportCallout(nameSearchResponse);
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(nameSearchResponse.getBody());    
                Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
                if(Integer.valueOf(responseData.get('total_matches')) >= 100)
                {
                    secondarynameSearchResponse = newSearchCallOut(complyAdvantageSearch.searchText,true);
                }
                if(secondarynameSearchResponse != null && secondarynameSearchResponse.getStatusCode() == 200)
                {
                    isNameSecondarySearch= true;
                    secondarynameSearchReportResponse = downloadReportCallout(secondarynameSearchResponse);
                    primaryNameSearch.Search_Id__c= string.valueOf((integer) responseData.get('id'));
                    disablePrimaryNameSearchResponse = disablePreviousSearchCallout(primaryNameSearch);
                }
                disableOldNameSearchResponse = disablePreviousSearchCallout(complyAdvantageSearch.previousNameSearch);
            }
            
        }
        if(string.isNotBlank(complyAdvantageSearch.aliasSearchText))
        {
            aliasSearchResponse = newSearchCallOut(complyAdvantageSearch.aliasSearchText,false);
            if(aliasSearchResponse != null && aliasSearchResponse.getStatusCode() == 200)
            {
                aliasSearchReportResponse = downloadReportCallout(aliasSearchResponse);
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(aliasSearchResponse.getBody());    
                Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
                if(Integer.valueOf(responseData.get('total_matches')) >= 100)
                {
                    secondaryaliasSearchResponse = newSearchCallOut(complyAdvantageSearch.aliasSearchText,true);
                }
                if(secondaryaliasSearchResponse != null && secondaryaliasSearchResponse.getStatusCode()==200)
                {
                    isAliasSecondarySearch= true;
                    secondaryaliasSearchReportResponse = downloadReportCallout(secondaryaliasSearchResponse);
                    primaryAliasSearch.Search_Id__c= string.valueOf((integer) responseData.get('id'));
                    disablePrimaryAliasSearchResponse = disablePreviousSearchCallout(primaryAliasSearch);
                }
                disableOldAliasSearchResponse = disablePreviousSearchCallout(complyAdvantageSearch.previousAliasNameSearch);
            }
            
        }

        upsert complyAdvantageSearch.caseInfo;
        complyAdvantageSearch.caseId = complyAdvantageSearch.caseInfo.Id;

        processResponse(nameSearchResponse,aliasSearchResponse,nameSearchReportResponse,aliasSearchReportResponse,disableOldNameSearchResponse,disableOldAliasSearchResponse,false,false);
        if(isNameSecondarySearch || isAliasSecondarySearch)
        {
            processResponse(secondarynameSearchResponse,secondaryaliasSearchResponse,secondarynameSearchReportResponse,secondaryaliasSearchReportResponse,null,null,isNameSecondarySearch,isAliasSecondarySearch);
        }
    }

    public HttpResponse newSearchCallOut(string searchText,boolean isSecondarySearch)
    {
        try
        {
            Http http = new Http();

            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches?api_key='+ complyAdvantageSearch.settings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('POST');
            request.setTimeout(120000);
            
            string requestBody = getNewSearchRequestBody(searchText,isSecondarySearch);
            System.debug(requestBody);
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request); 
            System.debug(response.getBody());
            return response;
        }
        catch(Exception ex)
        {
            Logger.exception(
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                ex);
            return null;
        }
    }
    public HttpResponse downloadReportCallout(HttpResponse searchResponse)
    {
        try
        {
            string searchId;
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(searchResponse.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
            Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data'); 
            if(responseData.get('id') != null)
            {
                searchId = string.valueOf((integer) responseData.get('id'));
            }

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchId +'/certificate?api_key='+ complyAdvantageSearch.settings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('GET');
            request.setTimeout(120000);

            HttpResponse response = http.send(request);
            return response;
        }
        catch(Exception ex)
        {
            Logger.exception(
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                ex);
            return null;
        }
    }
   
    private HttpResponse disablePreviousSearchCallout(Comply_Advantage_Search__c searchToDisableMonitoring)
    {
        HttpResponse response;
        if(searchToDisableMonitoring != null && string.isNotBlank(searchToDisableMonitoring.Search_Id__c))
        {
            try 
            {
                Http http = new Http();

                HttpRequest request = new HttpRequest();
                string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchToDisableMonitoring.Search_Id__c +'/monitors?api_key='+ complyAdvantageSearch.settings.apiKey;
                request.setEndpoint(serviceURL);
                request.setHeader('X-HTTP-Method-Override','PATCH');
                request.setMethod('POST');
                request.setTimeout(120000);
                
                string requestBody = '{"is_monitored" :'+ false +'}';
                request.setBody(requestBody); 

                response = http.send(request);
                return response;
            } 
            catch (Exception ex) 
            {
                Logger.exception(
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                ex);
                return null;
            }
        }
        return null;    
    }

    private void processResponse(HttpResponse nameSearchResponse,
                                  HttpResponse aliasSearchResponse,
                                  HttpResponse nameSearchReportResponse,
                                  HttpResponse aliasSearchReportResponse,
                                  HttpResponse disableOldNameSearchResponse,
                                  HttpResponse disableOldAliasSearchResponse,
                                  Boolean isNameSecondarySearch,
                                  Boolean isAliasSecondarySearch)
    {
        try {
            Comply_Advantage_Search__c[] oldCASearches;

            nameSearch = getComplyAdvantageSearchRecord(nameSearchResponse, false ,isNameSecondarySearch);
            aliasSearch = getComplyAdvantageSearchRecord(aliasSearchResponse, true,isAliasSecondarySearch);
            
            Attachment[] attachments = new Attachment[]{};
            
            if(nameSearch != null)
            {
            Database.SaveResult nameSearchInsertResponse = Database.insert(nameSearch);     
            if(nameSearchReportResponse != null && nameSearchReportResponse.getStatusCode() == 200)
            {
                    Document__c nameSearchReport = getDocument(complyAdvantageSearch.searchText, nameSearchInsertResponse.getId());
                    Database.SaveResult nameSearchReportInsertResponse = Database.insert(nameSearchReport);

                    Attachment nameReportAttachment = getAttachment(nameSearchReportResponse, nameSearchReport.name, nameSearchReportInsertResponse.Id);
                    attachments.add(nameReportAttachment);
            }
            }
            if(aliasSearch != null)
            {
                Database.SaveResult aliasSearchInsertResponse = Database.insert(aliasSearch);
                if(aliasSearchReportResponse != null && aliasSearchReportResponse.getStatusCode() == 200)
                {
                    Document__c aliasSearchReport = getDocument(complyAdvantageSearch.aliasSearchText, aliasSearchInsertResponse.getId());
                    Database.SaveResult aliasSearchReportInsertResponse = Database.insert(aliasSearchReport);
        
                    Attachment aliasReportAttachment = getAttachment(aliasSearchReportResponse, aliasSearchReport.name, aliasSearchReportInsertResponse.getId());
                    attachments.add(aliasReportAttachment);
                }
            }
            if(attachments.size() > 0)
            {
                Insert attachments;        
            }

            Comply_Advantage_Search__c[] previousSearchesToDisable = new Comply_Advantage_Search__c[]{};
            if(disableOldNameSearchResponse != null && disableOldNameSearchResponse.getStatusCode() == 200)
            {
                previousSearchesToDisable.add(new Comply_Advantage_Search__c(Id =complyAdvantageSearch.previousNameSearch.Id,Is_Monitored__c = false));    
            }
            if(disableOldAliasSearchResponse != null && disableOldAliasSearchResponse.getStatusCode() == 200)
            {
                previousSearchesToDisable.add(new Comply_Advantage_Search__c(Id =complyAdvantageSearch.previousAliasNameSearch.Id,Is_Monitored__c = false));
            }
        
            if(previousSearchesToDisable.size() > 0)
            {
                Database.SaveResult[] updateResult = Database.update(previousSearchesToDisable,false);
            }

            updateCase();
        } catch (Exception ex) {
            Logger.exception(
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                ex);
        }
    }
    
    private void updateCase()
    {
        boolean complyAdvantageCaseUpdated = false;
        if(complyAdvantageSearch.caseInfo != null)
        {
            if((nameSearch != null && nameSearch.Total_Hits__c != 0 && positiveStatusList.contains(nameSearch.Match_Status__c)) || 
            (aliasSearch != null && aliasSearch.Total_Hits__c != 0 && positiveStatusList.contains(aliasSearch.Match_Status__c)))
            {
                complyAdvantageSearch.caseInfo.Priority = 'Critical';

                if(complyAdvantageSearch.caseInfo.status == caseClosedStatus)
                {
                    complyAdvantageSearch.caseInfo.status = 'Open';
                }
                
                if(complyAdvantageSearch.caseInfo.status == caseVerifiedClosedStatus)
                {
                    complyAdvantageSearch.caseInfo.status = 'Re-opened';
                    
                    CaseComment comment = new CaseComment();
                    comment.CommentBody = 'Case Reopened because of New Comply Advantage Search (' + complyAdvantageSearch.searchReason + ')';
                    comment.ParentId = complyAdvantageSearch.caseInfo.Id;
                    Insert comment;
                }
                complyAdvantageCaseUpdated = true;
            }
            if(!complyAdvantageSearch.caseInfo.Comply_Advantage_Search_Completed__c)
            {
                complyAdvantageSearch.caseInfo.Comply_Advantage_Search_Completed__c = true;
                complyAdvantageCaseUpdated = true;
            }
            if(nameSearch != null && nameSearch.Entity_Contact__c == null)
            {
                string caseName = 'Comply Advantage Case for ' +  nameSearch.Name;
                if(complyAdvantageSearch.caseInfo.Subject != caseName)
                {
                    complyAdvantageSearch.caseInfo.Subject = caseName;
                    complyAdvantageCaseUpdated = true;
                }
            }               
            if(complyAdvantageCaseUpdated)
            {
                update complyAdvantageSearch.caseInfo; 
            }
        } 
    }
    private string getNewSearchRequestBody(string searchText,boolean isSecondarySearch)
    {
        NewSearchRerquestBody reqBody = new NewSearchRerquestBody();
        reqBody.search_term = searchText;
        reqBody.client_ref = complyAdvantageSearch.searchReference;
        reqBody.search_profile = isSecondarySearch ? complyAdvantageSearch.settings.secondarySearchProfile : complyAdvantageSearch.settings.searchProfileName;
        reqBody.fuzziness =  complyAdvantageSearch.settings.fuzziness;
        if(isSecondarySearch )
        {
        reqBody.exact_match = true;
        }
        reqBody.share_url =  1;

        reqBody.filters = new NewSearchFilters();
        if(complyAdvantageSearch.birthDate != null &&  
           complyAdvantageSearch.birthDate.year() > 1900 &&
           complyAdvantageSearch.birthDate.year() <= currentYear )
        {
            reqBody.filters.birth_year = String.valueOf(complyAdvantageSearch.birthDate.year());
        }
        if(complyAdvantageSearch.citizenshipNationality != null || complyAdvantageSearch.mailingCountry != null)
        {
            Country_Setting__c cnCountrySetting =  CustomSettings.getCountrySettingByName(complyAdvantageSearch.citizenshipNationality);
            Country_Setting__c mcCountrySetting = CustomSettings.getCountrySettingByName(complyAdvantageSearch.mailingCountry);
            
            reqBody.filters.country_codes = new string[]{};
            if(cnCountrySetting != null && cnCountrySetting.ISO_Code__c != null)
            {
                reqBody.filters.country_codes.add(cnCountrySetting.ISO_Code__c);
            }
            if(mcCountrySetting != null && mcCountrySetting.ISO_Code__c != null)
            {
                reqBody.filters.country_codes.add(mcCountrySetting.ISO_Code__c);
            }
         }
        
        return JSON.serialize(reqBody);
    }


    //---------Handling Responses -------------------------
    
    private Comply_Advantage_Search__c getComplyAdvantageSearchRecord(HttpResponse nameSearchResponse, Boolean isAliasSearch, Boolean isSecondarySearch)
    {
        Comply_Advantage_Search__c newComplyAdvantageSearch;
        //process response
        if (nameSearchResponse != null && nameSearchResponse.getStatusCode() == 200) 
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(nameSearchResponse.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
            Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
            searchesResponse.add(responseData);

            //get total_matches, share_url 
            if(responseData.get('id') != null)
            {
                complyAdvantageSearch.caSearchId = string.valueOf((integer) responseData.get('id'));
                string reference = (string)responseData.get('ref');
                integer totalHits = (integer)responseData.get('total_matches');
                string matchStatus = (string)responseData.get('match_status');
                string shareUrl = (string)responseData.get('share_url');
                Datetime createdDateTime = Datetime.valueOf((string)responseData.get('created_at')); 
                
                newComplyAdvantageSearch = new Comply_Advantage_Search__c();
                newComplyAdvantageSearch.Name = isAliasSearch ? complyAdvantageSearch.aliasSearchText : complyAdvantageSearch.searchText ;
                newComplyAdvantageSearch.Search_Id__c = complyAdvantageSearch.caSearchId;
                newComplyAdvantageSearch.Case__c = complyAdvantageSearch.caseInfo.Id;
                newComplyAdvantageSearch.Match_Status__c = matchStatus; 
                newComplyAdvantageSearch.Report_Link__c = shareUrl;
                newComplyAdvantageSearch.fxAccount__c = complyAdvantageSearch.fxAccountId;
                newComplyAdvantageSearch.Custom_Division_Name__c = complyAdvantageSearch.divisonName;
                newComplyAdvantageSearch.Affiliate__c = complyAdvantageSearch.affiliateId;
                
                newComplyAdvantageSearch.Is_Monitored__c = (primaryAliasSearch != null && primaryAliasSearch.Search_Id__c==newComplyAdvantageSearch.Search_Id__c) || (primaryNameSearch != null && primaryNameSearch.Search_Id__c==newComplyAdvantageSearch.Search_Id__c) ? false : true;
                newComplyAdvantageSearch.Entity_Contact__c  = complyAdvantageSearch.entityContactId;
                newComplyAdvantageSearch.Search_Reference__c = reference;
                newComplyAdvantageSearch.Case_Management_Link__c = 'https://app.complyadvantage.com/#/case-management/search/' + reference;
                newComplyAdvantageSearch.Total_Hits__c = totalHits;

                newComplyAdvantageSearch.Search_Text__c = isAliasSearch ? complyAdvantageSearch.aliasSearchText : complyAdvantageSearch.searchText ;
                if(complyAdvantageSearch.birthDate != null && 
                    complyAdvantageSearch.birthDate.year() > 1900 &&
                    complyAdvantageSearch.birthDate.year() <= currentYear)
                {
                    newComplyAdvantageSearch.Search_Parameter_Birth_Year__c = complyAdvantageSearch.birthDate != null ? string.valueOf(complyAdvantageSearch.birthDate.year()) : null;
                }
                newComplyAdvantageSearch.Search_Parameter_Citizenship_Nationality__c = complyAdvantageSearch.citizenshipNationality;
                newComplyAdvantageSearch.Search_Parameter_Mailing_Country__c = complyAdvantageSearch.mailingCountry;
                newComplyAdvantageSearch.Search_Reason__c = complyAdvantageSearch.searchReason;
                newComplyAdvantageSearch.Search_Profile__c = isSecondarySearch ? complyAdvantageSearch.settings.secondarySearchProfile : complyAdvantageSearch.settings.searchProfileName;
                newComplyAdvantageSearch.Fuzziness__c = complyAdvantageSearch.settings.fuzziness * 100;

                newComplyAdvantageSearch.Is_Alias_Search__c = isAliasSearch;
                if(isSecondarySearch)
                {
                    newComplyAdvantageSearch.Is_Secondary_Search__c = true;
                }
            }
        }
        return newComplyAdvantageSearch;
    }

    private Document__c getDocument(string searchText, Id parentId)
    {
        Document__c doc = new Document__c();
        doc.RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_DOCUMENT_CHECK, 'Document__c');
        string documentPrefix = (complyAdvantageSearch.reportPrefix != null) ? complyAdvantageSearch.reportPrefix : 'Comply Advantage Report: ';
        string documentName = documentPrefix + searchText;
        if(documentName.length() > 80 )
        {
            documentName = documentName.substring(0, 79);
        }

        doc.name = documentName;
        doc.Notes__c = complyAdvantageSearch.searchReason;
        doc.Document_Type__c = 'Comply Advantage';
        doc.fxAccount__c = complyAdvantageSearch.fxAccountId;
        doc.Case__c = complyAdvantageSearch.caseId;
        doc.Account__c = complyAdvantageSearch.AccountId;
        doc.Affiliate__c = complyAdvantageSearch.affiliateId; //Added. @Skyplanner 02/12/2021
        doc.Comply_Advantage_Search__c = parentId;
        
        return doc;
    }

    private Attachment getAttachment(HttpResponse response, string documentName, Id parentId)
    {
        Attachment attach = new Attachment();
        attach.Body = response.getBodyAsBlob(); 
        attach.Name = documentName;
        attach.ParentId = parentId;
        attach.ContentType = 'application/pdf';
        attach.IsPrivate = false;           
        return attach;
    }
    class NewSearchRerquestBody
    {
        string search_term;
        string client_ref;
        Decimal fuzziness;
        string search_profile;
        integer share_url;
        NewSearchFilters filters;
        Boolean exact_match;
    }
    class NewSearchFilters
    {
        string birth_year;
        string entity_type;
        string[] country_codes;
    }    

    public static boolean runOnce() 
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
}