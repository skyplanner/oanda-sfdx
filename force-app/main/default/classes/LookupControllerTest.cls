@IsTest
public class LookupControllerTest {
    
	@TestSetup static void makeData(){
      	Account acc1 = TestDataFactory.createPersonAccount('Test Account 1', 'testemail1@test.com', false);
		Account acc2 = TestDataFactory.createPersonAccount('Test Account 2', 'testemail2@test.com', false);
		Account acc3 = TestDataFactory.createPersonAccount('Test Account 3', 'testemail3@test.com', false);
		Account acc4 = TestDataFactory.createPersonAccount('Other Name', 'testemail4@test.com', false);
		insert new List<Account>{acc1, acc2, acc3, acc4};
	}

	@IsTest static void test_findRecordsSuccess() {        
			Test.startTest();
			List<Account> fetched =  (List<Account>)LookupController.findRecords('Test Account', 'Account', new List<String>{'Language_Preference__pc'}, null);
			Test.stopTest();

			System.assertEquals(3, fetched.size(), 'The number of records found is incorrect');
			System.assert(fetched[0].Name.contains('Test Account'), 'The Name should contain the search key.');
			System.assert(fetched[0].Language_Preference__pc != null, 'The Language_Preference__pc field should be returned');
		
	}

	@IsTest static void test_findRecordsNoExtraFields() {
       
			Test.startTest();
			List<Account> fetched =  (List<Account>)LookupController.findRecords('Test Account', 'Account', null, null);
			Test.stopTest();

			System.assertEquals(3, fetched.size(), 'The number of records found is incorrect');
			System.assert(fetched[0].Name.contains('Test Account'), 'The Name should contain the search key.');
		
	}

	@IsTest static void test_getRecentlyViewed() {
		User u = new User(Id = UserUtil.getSystemUserId());
		System.runAs(u){
			Test.startTest();
			List<Account> viewed = [SELECT Id, Name from Account Limit 2 FOR VIEW ];
			List<Account> fetched =  (List<Account>)LookupController.getRecentlyViewed('Account', new List<String>{'Language_Preference__pc', 'LastViewedDate'});
			Test.stopTest();

			System.assertEquals(2, fetched.size(), 'The number of records found is incorrect');
			System.assert(fetched[0].LastViewedDate != null, 'The LastViewedDate should not be null.');
			System.assert(fetched[0].Language_Preference__pc != null, 'The Language_Preference__pc field should be returned');
		}
		
	}

	@IsTest 
    static void test_getRecentlyViewedNoExtraFields() {
		
		User u = new User(Id = UserUtil.getSystemUserId());
		System.runAs(u){
			Test.startTest();
			List<Account> viewed = [SELECT Id, Name from Account Limit 2 FOR VIEW ];
			List<Account> fetched =  (List<Account>)LookupController.getRecentlyViewed('Account', null);
			Test.stopTest();

			System.assertEquals(2, fetched.size(), 'The number of records found is incorrect');

		}
		
	}

	@IsTest static void testsearchRecords() {
		User u = new User(Id = UserUtil.getSystemUserId());
		System.runAs(u){
			Test.startTest();
			Map<String, List<sobject>> fetched =  LookupController.searchRecords('Test Account');
			Test.stopTest();

		}
		
	}

}