/**
 * Wrapper for Comply advantage JSON: Search Details
 * @author Fernando Gomez
 */
public class ComplyAdvantageSearchListingInfo {
	@AuraEnabled
	public String source { get; set; }
	@AuraEnabled
	public String name { get; set; }
	@AuraEnabled
	public String url { get; set; }
	@AuraEnabled
	public Long count { get; set; }
	@AuraEnabled
	public Boolean disclose_sources { get; set; }
	
	@AuraEnabled
	public List<String> aml_types { get; set; }
	
	@AuraEnabled
	public List<Map<String, String>> data { get; set; }

	@AuraEnabled
	public String listing_started_utc { get; set; }
	@AuraEnabled
	public String listing_ended_utc { get; set; }
}