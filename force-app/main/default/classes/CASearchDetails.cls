/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-16-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetails {
    public Integer errorCode { get; set; }

    public String errorMessage { get; set; }
    
    public String id { get; set; }

    public String ref { get; set; }

    public Integer searcher_id { get; set; }

    public Integer assignee_id { get; set; }

    public CASearchDetailsFilters filters { get; set; }

    public String match_status { get; set; }

    public String risk_level { get; set; }

    public String search_term { get; set; }

    public String submitted_term { get; set; }

    public String client_ref { get; set; }

    public Integer total_hits { get; set; }

    public Integer total_matches { get; set; }

    public String updated_at { get; set; }

    public String created_at { get; set; }

    // public Map<String, String> tags { get; set; }

    // public List<Map<String, String>> labels { get; set; }

    public CASearchDetailsBlacklistFilters blacklist_filters { get; set; }

    public Integer total_blacklist_hits { get; set; }

    public CASearchDetailsProfile search_profile { get; set; }

    public Integer limitt { get; set; }

    public Integer offset { get; set; }

    public CASearchDetailsSearcher searcher { get; set; }

    public CASearchDetailsAssignee assignee { get; set; }

    public List<CASearchDetailsHits> hits { get; set; }

    public List<CASearchDetailsHits> blacklist_hits { get; set; }

    public String share_url { get; set; }
}