/**
 * @File Name          : ChatLanguagesManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/27/2022, 11:05:44 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/16/2020   acantero     Initial Version
**/
@isTest
private class ChatLanguagesManager_Test {

    @isTest
    static void checkLanguageAvailability_test() {
        Test.startTest();
        ChatLanguagesManager obj = ChatLanguagesManager.getInstance();
        Boolean english = obj.englishLangAvailable;
        Boolean englishEnabled = obj.englishLangEnabled;
        //...
        Boolean chinese = obj.chineseLangAvailable;
        Boolean chineseEnabled = obj.chineseLangEnabled;
        //...
        Boolean german = obj.germanLangAvailable;
        Boolean germanEnabled = obj.germanLangEnabled;
        //...
        Boolean spanish = obj.spanishLangAvailable;
        Boolean spanishEnabled = obj.spanishLangEnabled;
        //...
        Boolean russian = obj.russianLangAvailable;
        Boolean russianEnabled = obj.russianLangEnabled;
        //...
        Boolean polish = obj.polishLangAvailable;
        Boolean polishEnabled = obj.polishLangEnabled;
        //...
        Boolean french = obj.frenchLangAvailable;
        Boolean frenchEnabled = obj.frenchLangEnabled;
        //...
        Boolean italian = obj.italianLangAvailable;
        Boolean italianEnabled = obj.italianLangEnabled;
        //...
        Test.stopTest();
        System.assertNotEquals(null, obj.langSkillEnabledSet);
        System.assertNotEquals(null, obj.langSkillCodeAvailableSet);
        System.assertEquals(false, english);
        System.assertNotEquals(null, englishEnabled);
        System.assertEquals(false, chinese);
        System.assertNotEquals(null, chineseEnabled);
        System.assertEquals(false, german);
        System.assertNotEquals(null, germanEnabled);
        System.assertEquals(false, spanish);
        System.assertNotEquals(null, spanishEnabled);
        System.assertEquals(false, russian);
        System.assertNotEquals(null, russianEnabled);
        System.assertEquals(false, polish);
        System.assertNotEquals(null, polishEnabled);
        System.assertEquals(false, french);
        System.assertNotEquals(null, frenchEnabled);
        System.assertEquals(false, italian);
        System.assertNotEquals(null, italianEnabled);
    }
    
}