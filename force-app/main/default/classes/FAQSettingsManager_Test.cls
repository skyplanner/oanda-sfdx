@isTest
private class FAQSettingsManager_Test {

	@testSetup
	static void setup () {
	    //put test data initialization code here
	}

	@isTest
	static void getInstance_test() {
		Test.startTest();
		FAQSettingsManager result = FAQSettingsManager.getInstance();
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void getInstance_test2() {
		String settName = 'SomeInvalidTestName';
		Test.startTest();
		FAQSettingsManager result = FAQSettingsManager.getInstance(settName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void init_test() {
		FAQSettingsManager obj = FAQSettingsManager.getInstance();
		List<FAQ_Setting__mdt> settings = new List<FAQ_Setting__mdt>{
			new FAQ_Setting__mdt()
		};
		Test.startTest();
		obj.init(settings);
		Test.stopTest();
		System.assertEquals(FAQSettingsManager.DEFAULT_CAT_TOP_ART_COUNT, obj.categoryTopArticlesCount);
	}

}