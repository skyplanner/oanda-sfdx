/*
 *  @author : Deepak Malkani
 *  Created Date : Feb 3 2017
 *  Purpose : APEX Rest Resource used to create EID Results, which will be linked to a corsponding fxAccount
*/

@isTest
private class OnboardingAttachmentRestResourceTest
{

	@testSetup static void init(){

		List<Document_Codes__c> doccdsLst = new List<Document_Codes__c>();
		//Preload Test with some test data
		for(Integer i=0; i<20; i++){
			Document_Codes__c doccodes = new Document_Codes__c(Name = 'Test' + i, DocId__c = i, Doc_Type__c = 'Testing' + i);
			doccdsLst.add(doccodes);
		}
		
		//Add Document Code for MyNumber document
		Document_Codes__c doccodesMyNumber = new Document_Codes__c(Name = 'TestMyNumber', DocId__c = 100, Doc_Type__c = 'My Number');
		doccdsLst.add(doccodesMyNumber);
		
		insert doccdsLst;

		API_Access_Control__c control1 = new API_Access_Control__c(name = CustomSettings.API_NAME_ONBOARDING_DOC, Enabled__c = true);
        insert control1;
	}

	@isTest
	static void create_attachment_wo_fxAccount()
	{
		String responseTxt;
		Test.startTest();
		responseTxt = OnboardingAttachmentRestResource.createonBoardingAttachments(1234,01, 'testattach', 'this is a blob',  'text/plain');
		Test.stopTest();
		system.assertEquals(true, responseTxt.contains('FAILURE'));
	}

	@isTest
	static void create_attachment_wt_fxAccount()
	{

		//Create Test data
		TestDataFactory testHandler = new TestDataFactory();
		fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
		fxaccntObj.fxTrade_User_Id__c = 1234;
		update fxaccntObj;
		String responseTxt;
		Test.startTest();
		responseTxt = OnboardingAttachmentRestResource.createonBoardingAttachments(1234, 1, 'testattach', 'this is a blob',  'text/plain');
		Test.stopTest();
		system.assertEquals(true, responseTxt.contains('SUCCESS'));
		
		Document__c newDoc = [SELECT fxAccount__c, fxTrade_User_Id__c, Name, Name__c, Document_Type__c, Is_My_Number__c FROM Document__c WHERE fxAccount__c = :fxaccntObj.Id];
		system.assertEquals(1234, newDoc.fxTrade_User_Id__c);
		system.assertEquals('testattach', newDoc.Name);
		system.assertEquals('testattach', newDoc.Name__c);
		system.assertEquals(false, newDoc.Is_My_Number__c);
	}
	
	@isTest
	static void create_attachment_wt_fxAccount_MyNumber()
	{

		//Create Test data
		TestDataFactory testHandler = new TestDataFactory();
		fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
		fxaccntObj.fxTrade_User_Id__c = 1234;
		update fxaccntObj;
		String responseTxt;
		Test.startTest();
		responseTxt = OnboardingAttachmentRestResource.createonBoardingAttachments(1234, 100, 'testattach', 'this is a blob',  'text/plain');
		Test.stopTest();
		system.assertEquals(true, responseTxt.contains('Attachment has been created'));

		Document__c newDoc = [SELECT Is_My_Number__c FROM Document__c WHERE fxAccount__c =: fxaccntObj.Id];
		system.assertEquals(true, newDoc.Is_My_Number__c);
	}
	
	@isTest
	static void create_doc_wo_fxAccount()
	{
		String responseTxt;
		Test.startTest();
		responseTxt = OnboardingDocRestResource.createonBoardingDocument(1234, 1, 'testattach');
		Test.stopTest();
		system.assertEquals(true, responseTxt.contains('FAILURE'));
	}

	@isTest
	static void create_doc_wt_fxAccount()
	{

		//Create Test data
		TestDataFactory testHandler = new TestDataFactory();
		fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
		fxaccntObj.fxTrade_User_Id__c = 1234;
		update fxaccntObj;
		String responseTxt;
		Test.startTest();
		responseTxt = OnboardingDocRestResource.createonBoardingDocument(1234, 1, 'testattach');
		Test.stopTest();
		system.assertEquals(true, responseTxt.contains('SUCCESS'));
		
		Document__c newDoc = [SELECT fxAccount__c, fxTrade_User_Id__c, Name, Name__c, Document_Type__c, Is_My_Number__c FROM Document__c WHERE fxAccount__c = :fxaccntObj.Id];
		system.assertEquals(1234, newDoc.fxTrade_User_Id__c);
		system.assertEquals('testattach', newDoc.Name);
		system.assertEquals('testattach', newDoc.Name__c);
		system.assertEquals(false, newDoc.Is_My_Number__c);
	}

}