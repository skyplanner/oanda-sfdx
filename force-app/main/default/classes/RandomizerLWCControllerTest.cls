@isTest
private class RandomizerLWCControllerTest {
    @testSetup 
    static void dataSetup() {
        Integer qty = 10;

        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }

        Test.startTest();

        Account accnt = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com', 
            Account_Status__c = 'Active', 
            PersonMailingCountry = 'Canada');
        insert accnt;

        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'test01@example.org';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
        tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
        tradeAccount.Total_Deposits_USD__c = 2000;
        tradeAccount.Division_Name__c = 'Trade Group';
        tradeAccount.Account__c = accnt.Id;
        insert tradeAccount;

        List<Case> caseList = new List<Case>();
        for(Integer i = 0; i< qty; i++){
            caseList.add(
                new Case(
                    AccountId = accnt.Id,
                    Type__c = 'Chatbot',
                    Subtype__c = '2FA',
                    Chat_Division__c = 'APAC',
                    Origin = 'Phone',
                    OwnerId = testU.Id,
                    Inquiry_Nature__c = 'Login',
                    RecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId()
                ));
            caseList.add(
                new Case(
                    AccountId = accnt.Id,
                    Type__c = 'Web',
                    Subtype__c = '2FA',
                    Chat_Division__c = 'APAC',
                    Origin = 'Phone',
                    OwnerId = testU.Id,
                    Inquiry_Nature__c = 'Login',
                    RecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId()
                ));
        }
        insert caseList;

        Test.stopTest();
    }

    @isTest
    static void filtersTest1(){
        RandomizerLWCController.FiltersWrapper fw = 
            new RandomizerLWCController.FiltersWrapper();

        fw.caseOwnerRoleFilter = [SELECT UserRole.Name
            FROM User 
            WHERE Username = 'sptestusername@spoanda.com'].UserRole.Name;
        fw.accountDivisionFilter = 'Trade';
        fw.typeFilter = 'Chatbot';
        fw.agentTenureFilter = '< 0.5 Year';
        fw.numberCasesFilter = 5;

        Test.startTest();

            List<Case> result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(5, result.size(), 'The filters are not working properly');

        Test.stopTest();
    }

    @isTest
    static void filtersTest2(){
        RandomizerLWCController.FiltersWrapper fw = 
            new RandomizerLWCController.FiltersWrapper();

        fw.caseOwnerRoleFilter = [SELECT UserRole.Name 
            FROM User 
            WHERE Username = 'sptestusername@spoanda.com'].UserRole.Name;
        fw.accountDivisionFilter = 'Trade';
        fw.chatDivisionFilter = null;
        fw.caseOriginFilter = new List<String>();
        fw.inquireNatureFilter = null;
        fw.subTypeFilter = null;
        fw.fromDateFilter = System.today();
        fw.toDateFilter = System.today().adddays(1);
        fw.recordTypeFilter = RecordTypeUtil.getSupportOBCaseTypeId();
        fw.channelFilter = null;
        fw.languagePreferenceFilter = null;
        fw.chatLanguageFilter = null;
        fw.chatDurationFilter = 2500;
        fw.countryOfResidencyFilter = null;
        fw.agentTenureFilter = '> 1 Year';
        fw.numberCasesFilter = 100;
        fw.qiReviewedFilter = 'true';

        Test.startTest();

            List<Case> result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(0, result.size(), 'The filters are not working properly');

        Test.stopTest();
    }    

    @isTest
    static void filtersTest3(){
        RandomizerLWCController.FiltersWrapper fw = 
            new RandomizerLWCController.FiltersWrapper();

        fw.caseOwnerRoleFilter = [SELECT UserRole.Name 
            FROM User 
            WHERE Username = 'sptestusername@spoanda.com'].UserRole.Name;
        fw.accountDivisionFilter = 'Trade';
        fw.chatDivisionFilter = 'Chatbot';
        fw.caseOriginFilter = new String[]{'Chatbot'};
        fw.inquireNatureFilter = 'Chatbot';
        fw.subTypeFilter = 'Chatbot';
        fw.fromDateFilter = System.today();
        fw.toDateFilter = System.today().adddays(1);
        fw.recordTypeFilter = RecordTypeUtil.getSupportOBCaseTypeId();
        fw.channelFilter = 'Chatbot';
        fw.languagePreferenceFilter = 'Chatbot';
        fw.chatLanguageFilter = 'Chatbot';
        fw.chatDurationFilter = 2500;
        fw.countryOfResidencyFilter = 'Chatbot';
        fw.agentTenureFilter = '> 1 Year';
        fw.numberCasesFilter = 100;
        fw.qiReviewedFilter = 'true';
        fw.caseQIOwnerFilter = 'Chester';

        Test.startTest();

            List<Case> result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(0, result.size(), 'The filters are not working properly');

        Test.stopTest();
    }

    @isTest
    static void filtersTest4(){
        RandomizerLWCController.FiltersWrapper fw = 
            new RandomizerLWCController.FiltersWrapper();

        fw.caseOwnerRoleFilter = [SELECT UserRole.Name 
            FROM User 
            WHERE Username = 'sptestusername@spoanda.com'].UserRole.Name;
        fw.accountDivisionFilter = 'Trade';
        fw.typeFilter = 'Chatbot';
        fw.agentTenureFilter = '< 0.5 Year';
        fw.numberCasesFilter = null;

        Test.startTest();

            List<Case> result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(10, result.size(), 'The filters are not working properly');

            fw.numberCasesFilter = 200;
            result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(10, result.size(), 'The filters are not working properly');

            fw.caseOwnerFilter = 'unknownUser';
            result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(0, result.size(), 'The filters are not working properly');

        Test.stopTest();
    }

    @isTest
    static void filtersTest5(){
        RandomizerLWCController.FiltersWrapper fw = 
            new RandomizerLWCController.FiltersWrapper();

        fw.caseOwnerRoleFilter = [SELECT UserRole.Name 
            FROM User 
            WHERE Username = 'sptestusername@spoanda.com'].UserRole.Name;
        fw.accountDivisionFilter = 'Trade';
        fw.typeFilter = 'Chatbot';
        fw.subTypeFilter = '2FA';
        fw.caseOriginFilter = new String[]{'Phone'};
        fw.agentTenureFilter = '< 0.5 Year';
        fw.numberCasesFilter = 200;

        Test.startTest();

            List<Case> result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(10, result.size(), 'The filters are not working properly');

            fw.typeFilter = 'web';
            result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(10, result.size(), 'The filters are not working properly');

            fw.typeFilter = '';
            result = RandomizerLWCController.getRandomCases(fw);
            System.assertEquals(20, result.size(), 'The filters are not working properly');

        Test.stopTest();
    }

    @isTest
    static void getInitialValuesTest(){

        Test.startTest();

            RandomizerLWCController.InitialValuesWrapper initWrapper = 
                RandomizerLWCController.getInitialValues();
            
            System.assert(!initWrapper.caseOriginValues.isEmpty(), 'There is no values retrieved');
            Schema.DescribeFieldResult fieldResult = Case.Origin.getDescribe();
            Schema.PicklistEntry[] originValues = fieldResult.getPicklistValues();
            System.assertEquals(
                originValues.size(), 
                initWrapper.caseOriginValues.size(),
                'The expected values are not matching');

            System.assert(!initWrapper.inquireNatureValues.isEmpty(), 'There is no values retrieved');
            fieldResult = Case.Inquiry_Nature__c.getDescribe();
            Schema.PicklistEntry[] picklistValues = fieldResult.getPicklistValues();
            System.assertEquals(
                picklistValues.size(), 
                initWrapper.inquireNatureValues.size() - 1, //Including 'All'
                'The expected values are not matching');

            System.assert(!initWrapper.typeValues.isEmpty(), 'There is no values retrieved');
            fieldResult = Case.Type__c.getDescribe();
            picklistValues = fieldResult.getPicklistValues();
            System.assertEquals(
                picklistValues.size(), 
                initWrapper.typeValues.size() - 1, //Including 'All'
                'The expected values are not matching');

            System.assert(!initWrapper.recordTypeValues.isEmpty(), 'There is no values retrieved');
            List<RecordType> rtList = [SELECT 
                    Name, 
                    DeveloperName, 
                    NamespacePrefix, 
                    Description, 
                    BusinessProcessId, 
                    SobjectType, 
                    IsActive 
                FROM RecordType
                WHERE SobjectType = 'Case'
                AND IsActive = True];
            System.assertEquals(
                rtList.size(), 
                initWrapper.recordTypeValues.size() - 1, //Including 'All'
                'The expected values are not matching');
        
        Test.stopTest();
    }
}