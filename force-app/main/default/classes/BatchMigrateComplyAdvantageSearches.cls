/*
 * One time batch job, which will reparent all comply advantage searches and the Comply Advantage reports to new Comply Advantage Case 
 */
public class BatchMigrateComplyAdvantageSearches  implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
    
    public static final Integer BATCH_SIZE = 100;    
    public List<String> errs = new List<String>();
    public static final string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    public static set<string> positiveStatusList =new set<string>{'potential_match','true_positive','unknown'};
    
    public static final string CASE_STATUS_OPEN = 'Open';
    public static final string CASE_STATUS_CLOSED = 'Closed';
    public static final string CASE_STATUS_VERIFIED_CLOSED = 'Verified and Closed';
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchMigrateComplyAdvantageSearches(){
        query = 'Select Id, fxAccount__c,AccountId,Lead__c,Status, OwnerId, Priority, Subject, Comply_Advantage_Search_Completed__c, ' +
         		       		   '(select Id, Case__c From Documents__r Where Name like \'%Comply Advantage Report%\' and Document_Type__c=\'Comply Advantage\'), ' +
       		                   '(select Id From Cases Where RecordTypeId=:complianceCheckCaseRecordTypeId), ' +
                               '(select Id, Case__c,Is_Monitored__c, Match_Status__c From Comply_Advantage_Searches__r) ' +
                       'From Case Where RecordType.Name = \'Onboarding\' AND Comply_Advantage_Search_Completed__c = true';
        
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'Select Id, fxAccount__c,AccountId,Lead__c,Status, OwnerId, Priority, Subject, Comply_Advantage_Search_Completed__c, ' +
         		       		   '(select Id, Case__c From Documents__r Where Name like \'%Comply Advantage Report%\' and Document_Type__c=\'Comply Advantage\'), ' +
       		                   '(select Id From Cases Where RecordTypeId=:complianceCheckCaseRecordTypeId), ' +
                               '(select Id, Case__c,Is_Monitored__c, Match_Status__c From Comply_Advantage_Searches__r) ' +
                       'From Case WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<Case> scope)
    { 
        Case[] complyAdvantageCases = new Case[]{};
        Comply_Advantage_Search__c[] reparentedCASearches = new Comply_Advantage_Search__c[]{};
        Document__c[] reparentedDocuments = new Document__c[]{};
        
        Map<Id,Case> fxAccountOBCaseMap = new Map<Id,Case>{};
        for(Case obCase : scope)
        {
            Comply_Advantage_Search__c[] caSearches = obCase.Comply_Advantage_Searches__r;
            Case[] childCACases = obCase.Cases;
            if(caSearches != null && caSearches.size() >0 && childCACases.size() == 0)
            {
                Case caCase = new Case();
                string sub = obCase.Subject;
                caCase.Subject = sub.replace('Application Review for', 'Comply Advantage Case for');
                caCase.Status = getStatus(obCase);
                caCase.Priority = caCase.Status == CASE_STATUS_OPEN ? 'Critical' : 'Normal';
                caCase.ownerId =  UserUtil.getQueueId('Comply Advantage Cases');
                caCase.fxAccount__c = obCase.fxAccount__c;
                caCase.Lead__c = obCase.Lead__c;
                caCase.AccountId = obCase.AccountId;
                caCase.Comply_Advantage_Search_Completed__c = obCase.Comply_Advantage_Search_Completed__c;
                caCase.ParentId = obCase.Id;
                caCase.RecordTypeId = complianceCheckCaseRecordTypeId;
                fxAccountOBCaseMap.put(obCase.fxAccount__c, obCase);
                complyAdvantageCases.add(caCase);
            }
        }
        
        if(complyAdvantageCases.size() > 0)
        {
            Database.SaveResult[] caseCreationResults = Database.Insert(complyAdvantageCases,false);  
            for(Case caCase : complyAdvantageCases)
            {
                Case obCase = fxAccountOBCaseMap.get(caCase.fxAccount__c);
                for(Comply_Advantage_Search__c caSearch : obCase.Comply_Advantage_Searches__r) 
                {
                    caSearch.Case__c = caCase.Id;
                    reparentedCASearches.add(caSearch);
                }
                for(Document__c doc : obCase.Documents__r) 
                {
                    doc.Case__c = caCase.Id;
                    reparentedDocuments.add(doc);
                }
            }   		
            if(reparentedCASearches.size() > 0)
            {
            	Database.SaveResult[] reparentedSearchUpdateResults = Database.update(reparentedCASearches,false); 
            }
            if(reparentedDocuments.size() >0)
            {
            	Database.SaveResult[] reparentedDocumentUpdateResults = Database.update(reparentedDocuments,false); 
            }
        }
    } 
    
    private static string getStatus(Case obCase)
    {
        boolean hasPositiveMonitoredSearch = false;
        
        if(obCase.Status == CASE_STATUS_CLOSED)
        {
             return CASE_STATUS_VERIFIED_CLOSED;
        }
        else
        {
            for(Comply_Advantage_Search__c caSearch : obCase.Comply_Advantage_Searches__r) 
            {
               if(caSearch.Is_Monitored__c && positiveStatusList.contains(caSearch.Match_Status__c))
               {
                   hasPositiveMonitoredSearch = true;
               }
            }   
           return hasPositiveMonitoredSearch ? CASE_STATUS_OPEN : CASE_STATUS_CLOSED;
        }
    }
        
    public static Id executeBatch(Integer batchSize)
    {
		return Database.executeBatch(new BatchMigrateComplyAdvantageSearches(), batchSize);
	}
    
    public void finish(Database.BatchableContext bc)
    {  
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if(!Test.isRunningTest())
        {
           // EmailUtil.sendEmailForBatchJob(bc.getJobId());
        }
    } 
}