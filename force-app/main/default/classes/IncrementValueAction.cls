/**
 * @File Name          : IncrementValueAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/21/2023, 4:03:29 PM
**/
public without sharing class IncrementValueAction { 

	@InvocableMethod(label='Increment a Value' callout=false)
	public static List<Decimal> incrementValue(List<Decimal> valueList) {    
		try {
			ExceptionTestUtil.execute();
			List<Decimal> result = new List<Decimal> { 0 };

			if (
				(valueList == null) ||
				valueList.isEmpty()
			) {
				return result;
			}
			// else...        
			result[0] = valueList[0] + 1;
			System.debug('result: ' + result);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				IncrementValueAction.class.getName(),
				ex
			);
		}
	} 

}