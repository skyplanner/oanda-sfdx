/**
 * @File Name          : SE_MIFID_NID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/5/2020, 2:13:55 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   acantero     Initial Version
**/
@istest
private class SE_MIFID_NID_Validator_Test {
    
    @istest
    static void validate_test() {
        Integer currentYear = Date.today().year();
        Integer tooYoung = currentYear - (SE_MIFID_NID_Validator.MIN_AGE -1);
        Integer tooOld = currentYear - (SE_MIFID_NID_Validator.MAX_AGE + 1);
        Test.startTest();
        SE_MIFID_NID_Validator validator = new SE_MIFID_NID_Validator();
        String result1 = validator.validate(null); //nothing to validate
        String result2 = validator.validate('199'); //invalid lenght
        String result3 = validator.validate('A199967'); //invalid number
        String result4 = validator.validate(tooYoung + '0101'); //invalid year of bith
        String result5 = validator.validate(tooOld + '0101');  //invalid year of bith
        String result6 = validator.validate('22000101'); //invalid year of bith (in the future)
        String result7 = validator.validate('197707241231'); //invalid check digit
        String result8 = validator.validate('197707241233'); //valid
        String result9 = validator.validate('199701047921'); //valid
        String result10 = validator.validate('197503021482'); //valid
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(System.Label.MifidValPasspFormatError, result2);
        System.assertEquals(System.Label.MifidValPasspFormatError, result3);
        System.assertEquals(System.Label.MifidValPasspFormatError, result4);
        System.assertEquals(System.Label.MifidValPasspFormatError, result5);
        System.assertEquals(System.Label.MifidValPasspFormatError, result6);
        System.assertEquals(System.Label.MifidValPasspChecksumError, result7);
        System.assertEquals(null, result8);
        System.assertEquals(null, result9);
        System.assertEquals(null, result10);
    }

}