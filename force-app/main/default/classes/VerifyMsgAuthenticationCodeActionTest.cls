/**
 * @File Name          : VerifyMsgAuthenticationCodeActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:15:01 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 1:05:30 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class VerifyMsgAuthenticationCodeActionTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}

	@isTest
	private static void testValueListIsNullOrEmpty() {
		// Test data setup
		List<Boolean> verifications;
		// Actual test
		Test.startTest();

		verifications = VerifyMsgAuthenticationCodeAction.verifyMsgAuthenticationCode(
			null
		);
		Assert.areEqual(false, verifications.get(0), 'Should return false');

		verifications = null;

		verifications = VerifyMsgAuthenticationCodeAction.verifyMsgAuthenticationCode(
			new List<VerifyMsgAuthenticationCodeAction.ActionInfo>()
		);
		Assert.areEqual(false, verifications.get(0), 'Should return false');
		Test.stopTest();
		// Asserts
	}

	@isTest
	private static void testVerifyMsgAuthenticationCodeEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			VerifyMsgAuthenticationCodeAction.verifyMsgAuthenticationCode(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testVerifyMsgAuthenticationCode() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		VerifyMsgAuthenticationCodeAction.ActionInfo actionInfo = new VerifyMsgAuthenticationCodeAction.ActionInfo();
		actionInfo.messagingSessionId = null;
		actionInfo.fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		actionInfo.code = '123456';
		// Actual test
		Test.startTest();
		List<Boolean> verifications = VerifyMsgAuthenticationCodeAction.verifyMsgAuthenticationCode(
			new List<VerifyMsgAuthenticationCodeAction.ActionInfo>{ actionInfo }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			verifications.get(0),
			'Should return false. Session is not valid'
		);
	}
}