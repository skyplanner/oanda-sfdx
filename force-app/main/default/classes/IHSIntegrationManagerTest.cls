/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 11-27-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@IsTest
public with sharing class IHSIntegrationManagerTest {
    @TestSetup
    static void init() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = new TestDataFactory().createFXTradeAccount(acc);
        TAX_Declarations__c td = new TAX_Declarations__c(
            fxAccount__c = fxAcc.Id
        );
        insert td;

        fxAcc.Conditionally_Approved__c = false;
        fxAcc.US_Shares_Trading_Enabled__c = true;
        fxAcc.TAX_Declarations__c = td.Id;

        update fxAcc;
    }

    @IsTest
    static void firstNameChangeShouldNotUpdateConditionallyApprovedIfUSShareNotEnabledTest() {
        fxAccount__c fxAcc = [SELECT Id FROM fxAccount__c];

        Test.startTest();

        fxAcc.First_Name__c = 'Lisa';
        fxAcc.US_Shares_Trading_Enabled__c = false;
        update fxAcc;

        Test.stopTest();

        fxAcc = [SELECT Conditionally_Approved__c, 
                Conditionally_Approved_Date__c
            FROM fxAccount__c];

        System.assertEquals(false, fxAcc.Conditionally_Approved__c,
            'Conditionally Approved should NOT be true');
        System.assertEquals(null, fxAcc.Conditionally_Approved_Date__c,
            'Conditionally Approved Date should be empty');
    }

    @IsTest
    static void firstNameChangeShouldUpdateConditionallyApprovedTest() {
        fxAccount__c fxAcc = [SELECT Id FROM fxAccount__c];

        Test.startTest();

        fxAcc.First_Name__c = 'Lisa';
        update fxAcc;

        Test.stopTest();

        fxAcc = [SELECT Conditionally_Approved__c, 
                Conditionally_Approved_Date__c
            FROM fxAccount__c];

        System.assert(fxAcc.Conditionally_Approved__c,
            'Conditionally Approved should be true');
        System.assertNotEquals(null, fxAcc.Conditionally_Approved_Date__c,
            'Conditionally Approved Date should not be empty');
    }

    @IsTest
    static void w8w9StatusChangeShouldEmptyConditionallyApprovedDateTest() {
        fxAccount__c fxAcc = [SELECT Id FROM fxAccount__c];       

        Test.startTest();

        // ygutierrez - 11/27/2022
        // every time the field W8_W9_Status__c is updated, the account related going
        //  to be updated also (SP-12199). If the account is updated then, the fxAccount
        //  going to be synchronized with some of the account fields (only if changed):
        //  first, last and middle name, etc.
        //  [class AccountRollDownUtil called from AccountTriggerHandler]
        fxAcc.First_Name__c = 'Lisa';
        fxAcc.W8_W9_Status__c = 'testing';
        update fxAcc;

        fxAcc.W8_W9_Status__c = 'interviewValid';
        update fxAcc;

        Test.stopTest();

        fxAcc = [SELECT Conditionally_Approved__c, 
                Conditionally_Approved_Date__c
            FROM fxAccount__c];

        System.assert(fxAcc.Conditionally_Approved__c,
            'Conditionally Approved should be true');
        System.assertEquals(null, fxAcc.Conditionally_Approved_Date__c,
            'Conditionally Approved Date should be empty');
    }
}