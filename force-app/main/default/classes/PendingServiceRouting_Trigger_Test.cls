/**
 * @File Name          : PendingServiceRouting_Trigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2022, 11:12:29 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/27/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class PendingServiceRouting_Trigger_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createTestAccounts();
        OmnichanelRoutingTestDataFactory.createTestLeads();
    }

    //PendingServiceRouting related to Cases
    @isTest
    static void trigger_test1() {
        ID caseChannelId = OmnichanelRoutingTestDataFactory.getCaseChannelId();
        Set<ID> caseIdSet = OmnichanelRoutingTestDataFactory.getCreatedTestCasesIds();
        List<PendingServiceRouting> psrList = 
            OmnichanelRoutingTestDataFactory.getNewPendingServiceRList(caseChannelId, caseIdSet);
        Test.startTest();
        insert psrList;
        Test.stopTest();
        Integer count = [select count()  from PendingServiceRouting];
        System.assertEquals(caseIdSet.size(), count);
    }

    //PendingServiceRouting related to Chats
    @isTest
    static void trigger_test2() {
        ID chatChannelId = OmnichanelRoutingTestDataFactory.getChatChannelId();
        List<LiveChatVisitor> chatVisitorList = new List<LiveChatVisitor>();
        LiveChatVisitor chatVisitor1 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor1);
        LiveChatVisitor chatVisitor2 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor2);
        LiveChatVisitor chatVisitor3 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor3);
        LiveChatVisitor chatVisitor4 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor4);
        LiveChatVisitor chatVisitor5 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor5);
        LiveChatVisitor chatVisitor6 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor6);
        insert chatVisitorList;
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_ASIA_PACIFIC_AB,
                Chat_Type_of_Account__c = OmnichanelConst.PRACTICE,
                Chat_Language_Preference__c = OmnichanelConst.RUSSIAN_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_LOGIN,
                Type__c = OmnichanelConst.TYPE_FORGOT_OANDA_USERNAME,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor1.Id
            )
        );
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.LEAD_CUSTOMER_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_EUROPE_AB,
                Chat_Type_of_Account__c = OmnichanelConst.PRACTICE,
                Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_FUNDING_WITHDRAWAL,
                Type__c = OmnichanelConst.TYPE_DEPOSIT,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor2.Id
            )
        );
        //accounts
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.HVC_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_CANADA_AB,
                Chat_Type_of_Account__c = OmnichanelConst.LIVE,
                Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_FUNDING_WITHDRAWAL,
                Type__c = OmnichanelConst.TYPE_WITHDRAWAL,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor3.Id
            )
        );
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.NEW_CUSTOMER_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_AUSTRALIA_AB,
                Chat_Type_of_Account__c = OmnichanelConst.LIVE,
                Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
                Type__c = null,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor4.Id
            )
        );
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.ACTIVE_CUSTOMER_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_CORPORATION_AB,
                Chat_Type_of_Account__c = OmnichanelConst.LIVE,
                Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_REGISTRATION,
                Type__c = null,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor5.Id
            )
        );
        chatList.add(
            new LiveChatTranscript(
                Chat_Email__c = OmnichanelRoutingTestDataFactory.CUSTOMER_EMAIL,
                Chat_Division__c = OmnichanelConst.OANDA_CORPORATION_AB,
                Chat_Type_of_Account__c = OmnichanelConst.LIVE,
                Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_ACCOUNT,
                Type__c = null,
                Subtype__c = null,
                LiveChatVisitorId = chatVisitor6.Id
            )
        );
        insert chatList;
        Map<ID,LiveChatTranscript> chatMap = new Map<ID,LiveChatTranscript>(chatList);
        Set<ID> chatIdSet = chatMap.keySet();
        List<PendingServiceRouting> psrList = 
            OmnichanelRoutingTestDataFactory.getNewPendingServiceRList(chatChannelId, chatIdSet);
        Test.startTest();
        insert psrList;
        Test.stopTest();
        Integer count = [select count()  from PendingServiceRouting];
        System.assertEquals(chatList.size(), count);
    }
    
    // ok
    @isTest
    static void assignRoutingPriorityBeforeInsert1() {
        Boolean logExceptionIsThrown = false;
        Boolean exceptionIsThrown = false;
        ID caseChannelId = OmnichanelRoutingTestDataFactory.getCaseChannelId();
        Set<ID> caseIdSet = OmnichanelRoutingTestDataFactory.getCreatedTestCasesIds();
        List<PendingServiceRouting> psrList = 
            OmnichanelRoutingTestDataFactory.getNewPendingServiceRList(caseChannelId, caseIdSet);
        Test.startTest();
        PendingServiceRouting_Trigger_Utility handler = 
            new PendingServiceRouting_Trigger_Utility(psrList, null);
        handler.assignRoutingPriorityBeforeInsert();
        Test.stopTest();
        for(PendingServiceRouting psrObj : psrList) {
            System.assert(psrObj.RoutingPriority > 0);
        }
    }

    // exceptions
    @isTest
    static void assignRoutingPriorityBeforeInsert2() {
        Boolean logExceptionIsThrown = false;
        Boolean exceptionIsThrown = false;
        List<PendingServiceRouting> psrList = new List<PendingServiceRouting>();
        Test.startTest();
        PendingServiceRouting_Trigger_Utility handler = 
            new PendingServiceRouting_Trigger_Utility(psrList, null);
        // LogException
        try {
            ExceptionTestUtil.exceptionInstance = 
                LogException.newInstance('abc', 'def');
            handler.assignRoutingPriorityBeforeInsert();
        } catch (LogException lex) {
            logExceptionIsThrown = true;
        }
        // Exception
        try {
            ExceptionTestUtil.prepareDummyException();
            handler.assignRoutingPriorityBeforeInsert();
        } catch (Exception ex) {
            exceptionIsThrown = true;
        }
        Test.stopTest();
        System.assertEquals(true, logExceptionIsThrown, 'Invalid result');
        System.assertEquals(true, exceptionIsThrown, 'Invalid result');
    }

    // empty values
    @isTest
    static void assignRoutingPriorityBeforeInsert3() {
        List<PendingServiceRouting> psrList = new List<PendingServiceRouting>();
        Test.startTest();
        PendingServiceRouting_Trigger_Utility handler = 
            new PendingServiceRouting_Trigger_Utility(psrList, null);
        handler.assignRoutingPriorityBeforeInsert();
        Test.stopTest();
        System.assertEquals(0, psrList.size());
    }

    // empty values
    @isTest
    static void assignSkillsAfterInsert1() {
        List<PendingServiceRouting> psrList = new List<PendingServiceRouting>();
        Test.startTest();
        PendingServiceRouting_Trigger_Utility handler = 
            new PendingServiceRouting_Trigger_Utility(psrList, null);
        handler.assignSkillsAfterInsert();
        Test.stopTest();
        System.assertEquals(0, psrList.size());
    }

    // exceptions
    @isTest
    static void assignSkillsAfterInsert2() {
        Boolean logExceptionIsThrown = false;
        Boolean exceptionIsThrown = false;
        List<PendingServiceRouting> psrList = new List<PendingServiceRouting>();
        Test.startTest();
        PendingServiceRouting_Trigger_Utility handler = 
            new PendingServiceRouting_Trigger_Utility(psrList, null);
        // LogException
        try {
            ExceptionTestUtil.exceptionInstance = 
                LogException.newInstance('abc', 'def');
            handler.assignSkillsAfterInsert();
        } catch (LogException lex) {
            logExceptionIsThrown = true;
        }
        // Exception
        try {
            ExceptionTestUtil.prepareDummyException();
            handler.assignSkillsAfterInsert();
        } catch (Exception ex) {
            exceptionIsThrown = true;
        }
        Test.stopTest();
        System.assertEquals(true, logExceptionIsThrown, 'Invalid result');
        System.assertEquals(true, exceptionIsThrown, 'Invalid result');
    }

    @isTest
    static void setDefaultPriority() {
        List<PendingServiceRouting> psrList = new List<PendingServiceRouting>();
        PendingServiceRouting psr1 = new PendingServiceRouting(
            RoutingPriority = 7
        );
        psrList.add(psr1);
        PendingServiceRouting psr2 = new PendingServiceRouting(
        );
        psrList.add(psr2);
        Test.startTest();
        PendingServiceRouting_Trigger_Utility.setDefaultPriority(true, psrList, 5);
        Test.stopTest();
        System.assertEquals(7, psr1.RoutingPriority);
        System.assertEquals(5, psr2.RoutingPriority);
    }
}