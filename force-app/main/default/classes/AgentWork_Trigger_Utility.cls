/**
 * @File Name          : AgentWork_Trigger_Utility.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/12/2022, 1:07:34 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/2/2020   acantero     Initial Version
**/
public without sharing class AgentWork_Trigger_Utility {

    //this method exists because the status field is not creteable or updateable in test
    public static Boolean assignedIsDefaultStatusInTest = false;

    //********************************************************

    public static final String OBJ_API_NAME = 'AgentWork';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //********************************************************

    final List<AgentWork> newList;
    final Map<ID,AgentWork> oldMap;

    public AgentWork_Trigger_Utility(
        List<AgentWork> newList,
        Map<ID,AgentWork> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    public static void process(
        List<AgentWork> newList,
        Map<ID,AgentWork> oldMap,
        Boolean isAfterInsert,
        Boolean isAfterUpdate
    ) {
        try {
            if (!isEnabled()) {
                return;
            }
            //else..
            ExceptionTestUtil.execute();
            //...
            AgentWork_Trigger_Utility handler = 
                new AgentWork_Trigger_Utility(
                    newList,
                    oldMap
                );
            handler.execute(
                isAfterInsert,
                isAfterUpdate
            );
            //...
        } catch (LogException lex) {
            throw lex;
            //...
        } catch (Exception ex) {
            throw LogException.newInstance(
                AgentWork_Trigger_Utility.class.getName(),
                ex
            );
        }
    }

    //********************************************************

    //this method exists because the imposibility of create "right" test classes for AgentWork object
    public void execute(
        Boolean isAfterInsert, 
        Boolean isAfterUpdate
    ) {
        if (
            isAfterInsert || 
            isAfterUpdate
        ) {
            checkAssignedOrOpenedCases();
        }
    }

    public void checkAssignedOrOpenedCases() {
        OnAgentWorkStatusChangesCmd.assignedIsDefaultStatusInTest = 
            assignedIsDefaultStatusInTest;
        OnAgentWorkStatusChangesCmd onStatusChanges = 
            new OnAgentWorkStatusChangesCmd();
        for (AgentWork agentWorkObj : newList) {
            AgentWork oldAgentWork = oldMap?.get(agentWorkObj.Id);
            onStatusChanges.checkRecord(
                agentWorkObj, 
                oldAgentWork
            );
        }
        onStatusChanges.execute();
    }
    
}