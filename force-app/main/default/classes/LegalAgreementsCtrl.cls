public class LegalAgreementsCtrl {
    
    /**
     * Get fxAccount's legal agreements
     */
    @AuraEnabled
    public static List<LegalAgreement> fetchAgreements(
        Id fxAccId)
    {
        try {
            return new LegalAgreementsUtil().getfxAccAgreements(fxAccId);
        } catch (LogException lex) {
            throw lex;
        } catch (Exception ex) {
            throw LogException.newInstance(
                Constants.LOGGER_LEGAL_AGREEMENT_CATEGORY,
                ex);
        }
    }

}