@isTest
private class SandboxRefreshTest {

	@isTest static void testRunApexClass(){

		Lead lead = new Lead();
		lead.LastName = 'TestLead';
		lead.Email = 'testlead@abc.com';
		lead.Phone = '111-111-1234';
		insert lead;

		Account account = new Account();
		account.LastName = 'TestAccount';
		account.PersonEmail = 'testaccount@abc.com';
		account.Phone = '222-222-2345';
		insert account;

	    Test.startTest();

		BatchFormatAccountInfo b = new BatchFormatAccountInfo();
		database.executebatch(b);

        Test.stopTest();
    	
        Lead l = [SELECT Id, Phone, Email FROM Lead WHERE LastName = 'TestLead' LIMIT 1];
        system.assertEquals('testlead@example.com', l.Email);
        system.assertEquals('111-111-5555', l.Phone);

		Account a = [SELECT Id, Phone, PersonEmail FROM Account WHERE Lastname = 'TestAccount' LIMIT 1];
        system.assertEquals('testaccount@example.com', a.PersonEmail);
        system.assertEquals('222-222-5555', a.Phone);

    }	
}