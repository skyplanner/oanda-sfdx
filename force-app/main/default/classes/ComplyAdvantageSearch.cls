/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-06-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public class ComplyAdvantageSearch
{
    public string sfSearchRecordId;
    
    public string name;

    public string caSearchId;
    public string caAliasSearchId;
  
    public string searchText;
    public string aliasSearchText;
    public string citizenshipNationality;
    public string mailingCountry;
    public Date birthDate;

    /**
	 * Fernando @Skyplanner, 12/10/2020
	 * Addition: Fields to support aAffilliates
	 */
	public Id affiliateId { get; set; }
	public Affiliate__c affiliate { get; set; }
	public string username { get; set; }
	public string company { get; set; }
	public string alias { get; set; }

    public string searchReason;
    public set<string> changedFields;
    
    public url reportLink;
    public string matchStatus;
    public string matchStatusOverrideReason;
    
    //fxAccount Name
    public string searchReference;
    public boolean IsMonitored = false;
    public boolean IsAliasSearch = false; 
    
    public fxAccount__c fxAccount;
    public Case caseInfo;
    
    public Id fxAccountId;
    public Id accountId;
    public Id contactId;
    public Id caseId; 
    public Id entityContactId; 
    
    public string divisonName;
    public string caInstanceName;
    
    public Comply_Advantage_Search__c currentSearch;
    public Comply_Advantage_Search__c previousSearch;
    
    public Comply_Advantage_Search__c previousNameSearch;
    public Comply_Advantage_Search__c previousAliasNameSearch;
    
    public string reportPrefix; 
    
    public ComplyAdvantageSettings settings
    {
       get 
       {
           ComplyAdvantageSettings s;
           if(this.divisonName != null)
           {
              s = getComplyAdvantageSettingByDivision(this.divisonName,this.mailingCountry);
           }
           return s; 
       }
   }
   
   public Class ComplyAdvantageSettings
   {
       public string divisionName;
       public string apiKey;
       public string instanceName;
       public string searchProfileName;
       public string secondarySearchProfile;
       public decimal fuzziness = 0.5; 
   }

   public static ComplyAdvantageSettings getcomplyAdvantageSettingByDivision(string divisonName , string mailingCountry)
   { 
	   ComplyAdvantageSettings settings = new ComplyAdvantageSettings();
	   if(string.isNotBlank(divisonName))
	   {
		   Comply_Advantage_Division_Setting__c[] divisionSettings = [SELECT Name, 
																			 Search_Profile__c,
																			 Fuzziness__c,Secondary_Search_Profile__c,
																			 Comply_Advantage_Instance_Setting__r.Name,
																			 Comply_Advantage_Instance_Setting__r.API_Key__c
																	  FROM Comply_Advantage_Division_Setting__c
																	  WHERE Name =:divisonName];
		   if(divisionSettings.size() >0)
		   {
			   Comply_Advantage_Division_Setting__c temp = divisionSettings[0];
				settings.divisionName = temp.Name;
				settings.searchProfileName=temp.Search_Profile__c;
				settings.secondarySearchProfile = temp.Secondary_Search_Profile__c;
				if(temp.Fuzziness__c != null && temp.Fuzziness__c != 0)
				{
					 settings.fuzziness =temp.Fuzziness__c / 100;
				}
				if(temp.Comply_Advantage_Instance_Setting__r != null)
				{
				   settings.apiKey = temp.Comply_Advantage_Instance_Setting__r.API_Key__c;
				   settings.instanceName =temp.Comply_Advantage_Instance_Setting__r.Name;
				}
		   }
	   }

	   return settings;         
   	}
}