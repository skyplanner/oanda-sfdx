/**
 * @File Name          : GetSameNumericValueAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/21/2023, 3:46:48 PM
**/
public without sharing class GetSameNumericValueAction { 

	@InvocableMethod(label='Get same numeric value' callout=false)
	public static List<Decimal> getSameNumericValue(List<Decimal> valueList) {    
		return valueList;
	} 

}