/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-31-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoAccountSettingsMdt {
	private static CryptoAccountSettingsMdt instance;
	private Map<String, Crypto_Account_Setting__mdt> values;

    private CryptoAccountSettingsMdt() {
        values = new Map<String, Crypto_Account_Setting__mdt>();

		for (Crypto_Account_Setting__mdt stt : [
            SELECT
                QualifiedApiName,
                Value__c,
                Env__c,
                Check_Balance__c,
                USER_api__c,
                TAS_api__c
            FROM Crypto_Account_Setting__mdt
        ]) {
            values.put(stt.QualifiedApiName, stt);
        }
    }

    public static CryptoAccountSettingsMdt getInstance() {
		if (instance == null)
			return new CryptoAccountSettingsMdt();

		return instance;
	}

    public Crypto_Account_Setting__mdt getMdt(String settingApiName) {
		return values.get(settingApiName);
	}    

    public String getValue(String settingApiName) {
		return values.get(settingApiName).Value__c;
	}
}