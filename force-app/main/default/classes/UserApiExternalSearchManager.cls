/**
 * Manager class to search external
 */
public class UserApiExternalSearchManager {
    UserCallout userCallout;
    TasV20AccountCallout v20AccountCallout;
    UserApiSearch criteria;
    Integer limitSize;
    Integer offset;

    public UserApiExternalSearchManager(
        UserApiSearch criteria, Integer limitSize, Integer offset
    ) {
        this.criteria = criteria;
        this.limitSize = limitSize;
        this.offset = offset;
        userCallout = new UserCallout();
        v20AccountCallout = new TasV20AccountCallout();
    }

    /**
     * Callout to search
     */
    public List<Map<String, Object>> search() {
        if (String.isNotBlank(criteria.userName))
            return searchByUserName(criteria.userName, criteria.userNameEnv);

        //v20 Account Id
        if (String.isNotBlank(criteria.v20AccId))
            return searchByV20AccountId(new EnvironmentIdentifier(
                criteria.v20AccEnv, criteria.v20AccId));

        return new List<Map<String, Object>>();
    }

    List<Map<String, Object>> searchByUserName(
        String username, String userNameEnv
    ) {
        UserCallout.UserSearchQuery q = new UserCallout.UserSearchQuery();
        q.username = username;
        q.env = userNameEnv;
        q.pageSize = limitSize;
        q.pageNumber = offset / limitSize + 1;
        UserApiSearchResponse r = userCallout.search(q);
        
        return r.toMapList();
    }

    List<Map<String, Object>> searchByV20AccountId(EnvironmentIdentifier v20AccountId) {
        List<Map<String, Object>> result = new List<Map<String, Object>>();
        TasV20AccsDetails v20Account 
            = v20AccountCallout.getV20AccsDetails(v20AccountId);

        if (v20Account?.userId != null) {
            UserApiUserGetResponse user = userCallout.getUser(
                new EnvironmentIdentifier(v20AccountId.env, v20Account.userId));
            result.add(user.toMap());
        }
        
        return result;
    }
}