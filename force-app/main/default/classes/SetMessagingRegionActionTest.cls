/**
 * @File Name          : SetMessagingRegionActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 5:31:40 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/29/2024, 5:17:28 PM   aniubo     Initial Version
 **/
@isTest
private class SetMessagingRegionActionTest {
	@isTest
	private static void testSetMessagingRegionEmpty() {
		// Test data setup

		// Actual test
		Test.startTest();
		SetMessagingRegionAction.ActionInfo actionInfo = new SetMessagingRegionAction.ActionInfo();
		List<SetMessagingRegionAction.ActionResult> results = SetMessagingRegionAction.setMessagingRegion(
			new List<SetMessagingRegionAction.ActionInfo>()
		);
		Test.stopTest();

		System.assertEquals(
			true,
			results != null,
			'Results should not be null'
		);
		// Asserts
	}

	@isTest
	private static void testSetMessagingRegion() {
		// Test data setup

		// Actual test
		Test.startTest();
		SetMessagingRegionAction.ActionInfo actionInfo = new SetMessagingRegionAction.ActionInfo();
		actionInfo.messagingSessionId = null;
		actionInfo.countryCode = 'en_US';
		List<SetMessagingRegionAction.ActionResult> results = SetMessagingRegionAction.setMessagingRegion(
			new List<SetMessagingRegionAction.ActionInfo>{ actionInfo }
		);
		Test.stopTest();

		System.assertEquals(
			true,
			results != null,
			'Results should not be null'
		);
		// Asserts
	}

	@isTest
	private static void testSetMessagingRegionEX() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean hasError = false;
		// Actual test
		Test.startTest();
		SetMessagingRegionAction.ActionInfo actionInfo = new SetMessagingRegionAction.ActionInfo();
		actionInfo.messagingSessionId = null;
		actionInfo.countryCode = 'en_US';
		try {
			List<SetMessagingRegionAction.ActionResult> results = SetMessagingRegionAction.setMessagingRegion(
				new List<SetMessagingRegionAction.ActionInfo>{ actionInfo }
			);
		} catch (Exception ex) {
			hasError = true;
		}

		Test.stopTest();

		System.assertEquals(true, hasError, 'Exception should be thrown');
		// Asserts
	}

	@isTest
	private static void testPrepareActionResult() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingChatbotProcess.MessagingCountryInfo countryInfo = new MessagingChatbotProcess.MessagingCountryInfo();
		countryInfo.region = 'OC';
		countryInfo.countryName = 'US';
		SetMessagingRegionAction.ActionResult ar = SetMessagingRegionAction.prepareActionResult(
			countryInfo
		);
		Test.stopTest();

		System.assertEquals(
			countryInfo.countryName,
			ar.countryName,
			'Country name should be the same'
		);
		System.assertEquals(
			countryInfo.region,
			ar.region,
			'Region should be the same'
		);
		System.assertEquals(
			true,
			ar.success,
			'Success should be true'
		);
		// Asserts
	}
}