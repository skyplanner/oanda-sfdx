/**
 * @File Name          : AgentCapacityHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/21/2021, 4:16:48 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/21/2021, 12:44:30 PM   acantero     Initial Version
**/
@isTest
private without sharing class AgentCapacityHelper_Test {

    @testSetup
    static void setup() {
        /* OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        ); */
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        OmnichanelRoutingTestDataFactory.createHvcAccountTestCase();
    }

    @isTest
    static void beginChatClosing() {
        //disable LiveChatTranscript Trigger
        SPLiveChatTranscriptTriggerHandler.enabled = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        insert chatList;
        String userId = UserInfo.getUserId();
        String chatId = chatList[0].Id;
        System.debug('chatId: ' + chatId);
        String workId = null;
        Test.startTest();
        AgentCapacityHelper.beginChatClosing(
            userId, 
            chatId, 
            workId
        );
        Test.stopTest();
        Integer count = [select count() from Agent_Work_Closing__c];
        System.assertEquals(1, count);
    }

    //test beginCaseClosing, getCaseClosing and endWorkClosing 
    @isTest
    static void test() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        Test.startTest();
        AgentCapacityHelper.beginCaseClosing(
            userId, 
            caseId
        );
        Integer beforeCount = [select count() from Agent_Work_Closing__c];
        Agent_Work_Closing__c result = AgentCapacityHelper.getCaseClosing(
            caseId,
            userId
        );
        ID workClosingId = result.Id;
        AgentCapacityHelper.endWorkClosing(workClosingId);
        Test.stopTest();
        Integer afterCount = [select count() from Agent_Work_Closing__c];
        System.assertEquals(1, beforeCount);
        System.assertEquals(0, afterCount);
        System.assert(String.isNotBlank(workClosingId));
    }

    @isTest
    static void finishPendingCaseClosing() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        Test.startTest();
        AgentCapacityHelper.beginCaseClosing(
            userId, 
            caseId
        );
        AgentCapacityHelper.finishPendingCaseClosing(userId);
        Test.stopTest();
        Integer afterCount = [select count() from Agent_Work_Closing__c];
        System.assertEquals(0, afterCount);
        Case caseObj = [select Id, Agent_Capacity_Status__c from Case where Id = :caseId];
        System.assertEquals(OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, caseObj.Agent_Capacity_Status__c);
    }

    @isTest
    static void getPendingChatWorks1() {
        String userId = UserInfo.getUserId();
        Test.startTest();
        List<AgentCapacityHelper.ChatWorkClosingInfo> result = 
            AgentCapacityHelper.getPendingChatWorks(
                userId
            );
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void getPendingChatWorks2() {
        //disable LiveChatTranscript Trigger
        SPLiveChatTranscriptTriggerHandler.enabled = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        insert chatList;
        String userId = UserInfo.getUserId();
        String chatId = chatList[0].Id;
        String workId = null;
        Test.startTest();
        String chatClosingId = AgentCapacityHelper.beginChatClosing(
            userId, 
            chatId, 
            workId
        );
        Agent_Work_Closing__c agentWorkClosing = [
            SELECT Id, Case__c, Chat__c, Agent__c, Agent_Work__c	
            FROM Agent_Work_Closing__c
            WHERE Id = :chatClosingId
        ];
        System.debug('userId: ' + userId);
        System.debug('chatId: ' + chatId);
        System.debug('agentWorkClosing: ' + agentWorkClosing);
        List<AgentCapacityHelper.ChatWorkClosingInfo> result = 
            AgentCapacityHelper.getPendingChatWorks(
                userId
            );
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void chatWorkClosingInfo() {
        Test.startTest();
        AgentCapacityHelper.ChatWorkClosingInfo info = 
            new AgentCapacityHelper.ChatWorkClosingInfo(
                'fake workId',
                'fake closingId'
            );
        String workId = info.workId;
        String closingId = info.closingId;
        Test.stopTest();
        System.assert(String.isNotBlank(workId));
        System.assert(String.isNotBlank(closingId));
    }

}