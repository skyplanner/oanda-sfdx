/**
 * @File Name          : CreateMessagingCommentActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:10:38 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 1:34:26 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class CreateMessagingCommentActionTest {
	@isTest
	private static void testCommentsIsNullOrEmpty() {
		// Test data setup
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			CreateMessagingCommentAction.createMessagingComment(null);
			CreateMessagingCommentAction.createMessagingComment(
				new List<CreateMessagingCommentAction.CommentInfo>()
			);
		} catch (Exception ex) {
			isError = true;
		}
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}
	@isTest
	private static void testCreateMessagingCommentThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			CreateMessagingCommentAction.createMessagingComment(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testCreateMessagingComment() {
		// Test data setup
		CreateMessagingCommentAction.CommentInfo commentInfo = new CreateMessagingCommentAction.CommentInfo();
		commentInfo.messagingSessionId = null;
		commentInfo.comment = 'Test comment';
		commentInfo.key = 'Test key';
		commentInfo.subject = 'Test subject';
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			CreateMessagingCommentAction.createMessagingComment(
				new List<CreateMessagingCommentAction.CommentInfo>{
					commentInfo
				}
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}
}