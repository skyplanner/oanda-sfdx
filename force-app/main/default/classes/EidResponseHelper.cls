/**
 * @description       : 
 * @author            : Oanda
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public class EidResponseHelper {
    public List<EID_Result__c > eidResultList {get;set;}
    public EidResponseHelper(){
        this.eidResultList = new List<EID_Result__c >();
    }

    @AuraEnabled(cacheable=true)
    public static list<EID_Result__c> getGBGID3Results(string searchKey){
        System.debug('searchKey'+searchKey);
        
        List<EID_Result__c> lst = new List<EID_Result__c>([
            SELECT  Id,
                    First_Name_PD__c,
                    Last_Name_PD__c,
                    Gender_PD__c,
                    Birthdate_PD__c,
                    Building_PD__c,
                    Street_PD__c,
                    City_PD__c,
                    Postal_Code_PD__c,
                    State_PD__c,
                    Country_PD__c,
                    EID_Response__c
            FROM EID_Result__c
            WHERE Id = :searchKey ]);
        System.debug('lst'+lst.size());
        return lst;
    }
            
    @AuraEnabled(cacheable=true)
    public static list<EID_Result__c> getEIDResults(string searchKey){
        System.debug('searchKey'+searchKey);
        
        List<EID_Result__c> lst = new List<EID_Result__c>([SELECT id,Agency_Name__c, First_Name__c, Fraud_Flag_Status__c, Fraud_Flag_Code__c,
                                                           Last_Name__c, DOB__c, Unit_Number__c, Building_Number__c, StreetvName__c, Street_Type__c,
                                                           City__c, Postal_Code__c,State_Province_Code__c, Telephone__c, SSN__c from EID_Result__c
                                                           WHERE Trulliio_Agency__c = :searchKey ]);
        System.debug('lst'+lst.size());
        return lst;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<EID_Reason_Codes__c> getFlagList(string searchKey){
        List<EID_Result__c> lst = new List<EID_Result__c>([SELECT Fraud_Flag_Code__c from EID_Result__c
                                                           WHERE Trulliio_Agency__c = :searchKey ]);
        Set<String> setOfFlag = new Set<String>();
        for(EID_Result__c eid :lst){
            String flagsFromJSon = eid.Fraud_Flag_Code__c;
            
            flagsFromJSon = flagsFromJSon.replaceAll('\\[', '');
            flagsFromJSon = flagsFromJSon.replaceAll('\\]', '');
            setOfFlag.addAll(flagsFromJSon.split(','));
        }
        System.debug('setOfFlag'+setOfFlag);
        //Map<String,String> codeToReasonMap = new Map<String,String>();
        List<EID_Reason_Codes__c> ReasonCodeList = [SELECT ActionText__c, ReasonCode__c, ReasonText__c, Provider__c FROM EID_Reason_Codes__c
                                                    WHERE ReasonCode__c IN : setOfFlag AND Provider__c = 'TruliooUS'];
        
        //codeToReasonMap.put('101','Trullioo failed 101');
        //codeToReasonMap.put('102','Trullioo failed 101');
        return ReasonCodeList ;
    }
    
    class Response{
        String fieldName;
        String fieldValue;
    }

    class ResponseGBGID3 {
        EID_Result__c eid;
        String domain;
    }
}