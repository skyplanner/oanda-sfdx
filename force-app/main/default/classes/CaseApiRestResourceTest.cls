@IsTest
private class CaseApiRestResourceTest {

    @IsTest
    static void testCreateCase() {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        CaseApiRestResource.CaseData testData = new CaseApiRestResource.CaseData();
        testData.emailAddress = 'test@example.com';
        testData.emailSubject = 'Test Subject';
        testData.emailBody = 'Test Body';
        testData.status = 'Open';

        String requestBody = JSON.serialize(testData);

        request.requestURI = '/api/v1/user/test@example.com/hd-case';
        request.requestBody = Blob.valueOf(requestBody);
        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();
        CaseApiRestResource.createCase();
        Test.stopTest();

        System.assertEquals(200, response.statusCode);

    }

    @IsTest 
    static void testWithAccount(){

        String testEmail = 'tests@example.org';

        Account acc1 = new Account(
            PersonEmail = testEmail,
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            FirstName = 'Bartolo',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            LastName = 'Colon',
            MiddleName = 'Iglesias',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155',
            Suffix = 'Suff1',
            Salutation = 'Salut1',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId());
        insert acc1;

        fxAccount__c fxAcc1 = new fxAccount__c(
            Account__c = acc1.Id,
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Government_ID__c = '0123456789');
        insert fxAcc1;

        Case c = new Case(
            RecordTypeId = CaseApiRestResource.CASE_HD_RECORD_TYPE,
            AccountId = acc1.Id
        );
        insert c;

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        CaseApiRestResource.CaseData testData = new CaseApiRestResource.CaseData();
        testData.emailAddress = testEmail;
        testData.emailSubject = 'Test Subject';
        testData.emailBody = 'Test Body';
        testData.status = 'Open';
    
        String requestBody = JSON.serialize(testData);

        request.requestURI = '/api/v1/user/' + testEmail + '/hd-case';
        request.requestBody = Blob.valueOf(requestBody);
        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();
        CaseApiRestResource.createCase();
        Test.stopTest();

        Assert.areEqual(200, response.statusCode);
        Assert.areEqual('FXTrade - Funds Notification', [SELECT HD_Request_Type__c from Case WHERE ID =: c.Id].HD_Request_Type__c, 'HD_Request_Type__c should be set up');
    }
}