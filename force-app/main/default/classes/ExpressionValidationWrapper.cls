/**
 * @File Name          : ExpressionValidationWrapper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2020, 3:34:43 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
public without sharing class ExpressionValidationWrapper extends ExpressionValidationInfo {

    @testVisible
    public String developerName {get; private set;}
    @testVisible
    public String key {get; private set;}
    @testVisible
    public String validation1 {get; private set;}
    @testVisible
    public Boolean apexValidation {get; private set;}
    @testVisible
    public Datetime lastModifiedDate {get; private set;}
    
    @testVisible
    private ExpressionValidationWrapper() {
        //default value
        letterCase = MiFIDExpValConst.LetterCase.UPPER_CASE;
    } 
    
    public ExpressionValidationWrapper(Expression_validation__mdt expVal) {
        //default value
        letterCase = MiFIDExpValConst.LetterCase.UPPER_CASE;
        //...
        this.developerName = expVal.DeveloperName;
        this.key = expVal.Key__c;
        this.validation1 = expVal.Validation_1__c;
        this.apexValidation = expVal.APEX_validation__c;
        this.canBeDefaultValue = expVal.Can_be_default_value__c;
        this.canBeBlank = expVal.Can_be_blank__c;
        this.checkConsecutiveNumbers = expVal.Consecutive_numbers__c;
        this.checkRepetitiveNumber = expVal.Repetitive_number__c;
        this.checkConsecutiveLetters = expVal.Consecutive_letters__c;
        this.checkRepetitiveLetter = expVal.Repetitive_letter__c;
        this.checkOnlyValidFragments = expVal.Only_valid_fragments__c;
        if (!setPrefixSize(expVal.Separators__c)) {
            this.separators = expVal.Separators__c;
        }
        this.lastModifiedDate = expVal.Last_Modified_Date2__c;
    }

    @testVisible
    Boolean setPrefixSize(String prefixSizeStr) {
        prefixSize = 0;
        if (
            String.isNotBlank(prefixSizeStr) &&
            prefixSizeStr.startsWith(MiFIDExpValConst.SEPARATOR_REMOVE_PREFIX)
        ) {
            Integer sepRemovePrefixLenght = MiFIDExpValConst.SEPARATOR_REMOVE_PREFIX.length();
            if (prefixSizeStr.length() > sepRemovePrefixLenght) {
                String temp = prefixSizeStr.substring(sepRemovePrefixLenght);
                if (temp.isNumeric()) {
                    prefixSize = Integer.valueOf(temp);
                }
            }
            return true;
        }
        //else...
        return false;
        
    }

    public static Map<String,ExpressionValidationWrapper> getExpValMap(Set<String> keys) {
        Map<String,ExpressionValidationWrapper> expValMap = new Map<String,ExpressionValidationWrapper>();
        if ((keys == null) || keys.isEmpty()) {
            return expValMap;
        }
        //else...
        List<Expression_validation__mdt> expValList = [
            SELECT
                DeveloperName,
                Key__c,
                Validation_1__c,
                Consecutive_numbers__c,
                Consecutive_letters__c,
                Repetitive_letter__c,
                Repetitive_number__c,
                Only_valid_fragments__c,
                Separators__c,
                APEX_validation__c,
                Can_be_default_value__c,
                Can_be_blank__c,
                Last_Modified_Date2__c
            FROM
                Expression_validation__mdt
            WHERE
                Key__c IN :keys
        ];
        for(Expression_validation__mdt expVal : expValList) {
            expValMap.put(expVal.Key__c, new ExpressionValidationWrapper(expVal));
        }
        return expValMap;
    }
    
}