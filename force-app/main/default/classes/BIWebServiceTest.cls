@isTest
private class BIWebServiceTest {

    static testMethod void liveFxTradeQueryTest() {
    	loadBIWebServiceSettings();
    	
    	Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();

        mock.setStaticResource('BIWebServiceLiveSubAccounts');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        BIWebService bi = new BIWebService();
        List<BIWebService.SubAccount> subAccounts = bi.queryLiveFxTrade('513619');
        
        System.assertEquals(2, subAccounts.size());
        System.assertNotEquals(null, subAccounts[0].account_number);
        System.assertNotEquals(null, subAccounts[1].account_number);
        
        Test.stopTest();
    }
    
    static testMethod void demoFxTradeQueryTest() {
    	loadBIWebServiceSettings();
    	
    	 Test.startTest();

         StaticResourceCalloutMock mock = new StaticResourceCalloutMock();

         mock.setStaticResource('BIWebServiceDemoSubAccounts');
         mock.setStatusCode(200);
         mock.setHeader('Content-Type', 'application/json');
        
         // Set the mock callout mode
         Test.setMock(HttpCalloutMock.class, mock);

         BIWebService bi = new BIWebService();
         List<BIWebService.SubAccount> subAccounts = bi.queryDemoFxTrade('1879979');
         System.assertEquals(1, subAccounts.size());
         System.assertNotEquals(null, subAccounts[0].account_number);
         Test.stopTest();
        
    }
    
    static void loadBIWebServiceSettings(){
    	BIWebServiceSetting__c setting = new BIWebServiceSetting__c(name = 'BI Web Services');
    	setting.User_Name__c = 'app-salesforce';
    	setting.Password__c = 'abc';
    	setting.Live_Account_End_Point__c = 'https://biapi.oanda.com:3443/biapi-salesforce/get-game-subaccount-data-by-fxtrade-user-id';
    	setting.Demo_Account_End_Point__c = 'https://biapi.oanda.com:3443/biapi-salesforce/get-live-subaccount-data-by-fxtrade-user-id';
    	
    	insert setting;
    }
}