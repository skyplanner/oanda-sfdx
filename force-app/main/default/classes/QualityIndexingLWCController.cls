/**
 * Controller to serve to QualityIndexingLWC component
 * @author:	Michel Carrillo (SkyPlanner)
 * @since:	01/18/2021
 * @version 1.0
 */
public with sharing class QualityIndexingLWCController {

    public static final String TENURE_LESS_HALF_YEAR = 'Tenure_0_5_year__c';
    public static final String TENURE_LESS_FULL_YEAR = 'Tenure_1_year__c';
    public static final String TENURE_GREATER_FULL_YEAR = 'Tenure_greater_1_year__c';

    public static final String PD_AVERAGE_SPEED_TO_ANSWER = 'Average Speed to Answer';
    public static final String PD_ACCURACY = 'Accuracy';
    public static final String PD_SELF_SERVICE_METHOD = 'Self Service Method';
    public static final String PD_TONE_MANNERISM = 'Tone/Mannerism';
    public static final String PD_LITERACY_GRAMMAR = 'Literacy/Grammar';
    public static final String PD_CASE_TAGGING = 'Case Tagging';
    public static final String PD_FIRST_RESPONSE_TIME = 'First Response Time';

    public static final String ORIGIN_TYPE_CHAT = 'Chat';
    public static final String ORIGIN_TYPE_EMAIL = 'Email';
    public static final String ORIGIN_TYPE_CALL = 'Call';

    /**
     * Retrieve the initial values to initialize the form
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/18/2021
     * @return:	InitValuesWrapper, contains all the required values to start the component 
     */
    @AuraEnabled(cacheable=false)
    public static InitialValuesWrapper getInitialValues(Id pCaseId){
        try{
            InitialValuesWrapper result = new InitialValuesWrapper();
            Case currentCase = getCurrentCase(pCaseId);
            if(currentCase == null){
                String message = 'We cannot find a Case for the Id: ' + pCaseId;
                AuraHandledException ahe = new AuraHandledException(message);
                ahe.setMessage(message);
                throw ahe;
            }

            result.caseType = getCaseType(currentCase);
            QIParameters qiParam = new QIParameters(
                result.caseType,
                getTenureTier(Integer.valueOf(currentCase.Case_Tenure__c))
            );

            result.isTradingAdvice = currentCase.Is_Trading_Advice__c;

            String key = PD_AVERAGE_SPEED_TO_ANSWER;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.speedToAnswerValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_ACCURACY;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.accuracyValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_TONE_MANNERISM;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.toneValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_LITERACY_GRAMMAR;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.literacyValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_CASE_TAGGING;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.taggingValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_FIRST_RESPONSE_TIME;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.firstResponseValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            key = PD_SELF_SERVICE_METHOD;
            if(qiParam.valuesByDeductionParameter.containsKey(key))
                result.selfServiceValues = 
                    toPicklistValuesWrapper(qiParam.valuesByDeductionParameter.get(key));

            map<String, Quality_Index__c> qiMap = getQIMap(pCaseId);

            if(qiMap.containsKey(PD_AVERAGE_SPEED_TO_ANSWER))
                result.speedToAnswer = qiMap.get(PD_AVERAGE_SPEED_TO_ANSWER).Deduction_Point__c;

            if(qiMap.containsKey(PD_ACCURACY))
                result.accuracy = qiMap.get(PD_ACCURACY).Deduction_Point__c;

            if(qiMap.containsKey(PD_SELF_SERVICE_METHOD))
                result.selfService = qiMap.get(PD_SELF_SERVICE_METHOD).Deduction_Point__c;
                
            if(qiMap.containsKey(PD_TONE_MANNERISM))
                result.tone = qiMap.get(PD_TONE_MANNERISM).Deduction_Point__c;

            if(qiMap.containsKey(PD_LITERACY_GRAMMAR))
                result.literacy = qiMap.get(PD_LITERACY_GRAMMAR).Deduction_Point__c;

            if(qiMap.containsKey(PD_CASE_TAGGING))
                result.tagging = qiMap.get(PD_CASE_TAGGING).Deduction_Point__c;

            if(qiMap.containsKey(PD_FIRST_RESPONSE_TIME))
                result.firstResponse = qiMap.get(PD_FIRST_RESPONSE_TIME).Deduction_Point__c;

            result.score = currentCase.QI_Score__c == null ? 5 : currentCase.QI_Score__c;

			result.qiCaseOwnerId = currentCase.QI_Case_Owner__c;
            return result;
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     * Update the Quality Index values for the given Case
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/19/2021
     * 
     * @return:	void 
     */
    @AuraEnabled(cacheable=false)
    public static Decimal updateQI(Id pCaseId, FormWrapper pFromWrapper) {
        try{
            map<String, Quality_Index__c> qiMap = getQIMap(pCaseId);
            String key;
            Decimal totalScore = 0;
            if(pFromWrapper.speedToAnswer != null)
                setParameter(pCaseId, qiMap, PD_AVERAGE_SPEED_TO_ANSWER, pFromWrapper.speedToAnswer);
            if(pFromWrapper.accuracy != null)
                setParameter(pCaseId, qiMap, PD_ACCURACY, pFromWrapper.accuracy);
            if(pFromWrapper.tone != null)
                setParameter(pCaseId, qiMap, PD_TONE_MANNERISM, pFromWrapper.tone);
            if(pFromWrapper.literacy != null)
                setParameter(pCaseId, qiMap, PD_LITERACY_GRAMMAR, pFromWrapper.literacy);
            if(pFromWrapper.tagging != null)
                setParameter(pCaseId, qiMap, PD_CASE_TAGGING, pFromWrapper.tagging);
            if(pFromWrapper.firstResponse != null)
                setParameter(pCaseId, qiMap, PD_FIRST_RESPONSE_TIME, pFromWrapper.firstResponse);
            if(pFromWrapper.selfService != null)
                setParameter(pCaseId, qiMap, PD_SELF_SERVICE_METHOD, pFromWrapper.selfService);

            for(Quality_Index__c v : qiMap.values())
                totalScore += v.Deduction_Point__c;

            if(totalScore > 5)
                throw new CustomException('The sum of all values cannot be greater than 5');           

            upsert qiMap.values();

            Case c = new Case();
            c.Id = pCaseId;
            c.QI_Reviewed__c = true;
            c.QI_Score__c = 5 - totalScore;
            c.QI_Case_Owner__c = pFromWrapper.qiCaseOwnerId;

            if(pFromWrapper.isTradingAdvice != null)
                c.Is_Trading_Advice__c = pFromWrapper.isTradingAdvice;
            update c;

            return c.QI_Score__c;
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static map<String, Quality_Index__c> getQIMap(Id pCaseId){
        map<String, Quality_Index__c> qiMap = new map<String, Quality_Index__c>();
        for(Quality_Index__c qi : [SELECT Id, 
                                    Case__c, 
                                    Deduction_Point__c,
                                    Parameter_Name__c 
                                FROM Quality_Index__c
                                WHERE Case__c =: pCaseId
                                Order By CreatedDate DESC
                                LIMIT 100]){ //At most should exist 7
            if(!qiMap.containsKey(qi.Parameter_Name__c))
                qiMap.put(qi.Parameter_Name__c, qi);
        }
        return qiMap;
    }

    private static void setParameter(Id pCaseId, map<String, Quality_Index__c> pQiMap, String pKey, Decimal pValue){

        if(pQiMap.containsKey(pKey))
            pQiMap.get(pKey).Deduction_Point__c = pValue;
        else{
            pQiMap.put(pKey, new Quality_Index__c(
                Case__c = pCaseId,
                Parameter_Name__c = pKey,
                Deduction_Point__c = pValue
            ));
        }
    }

    private static String getTenureTier(Integer pTenure){
        if(pTenure <= 182)//Less than 182 days
            return TENURE_LESS_HALF_YEAR;
        if(pTenure <= 365)//Less than 365 days
            return TENURE_LESS_FULL_YEAR;
        return TENURE_GREATER_FULL_YEAR;
    }

	private static List<PicklistValuesWrapper> toPicklistValuesWrapper(List<Decimal> pDecimalList){
		List<PicklistValuesWrapper> result = new List<PicklistValuesWrapper>();
        for(Decimal d : pDecimalList)
            result.add(new PicklistValuesWrapper(d.toPlainString(), d.toPlainString()));
		return result;
    }

    private static String getCaseType(Case pCaseType){
        // if(pCaseType.Origin == 'Chatbot'){
        //     if(pCaseType.LiveChatTranscripts != NULL &&
        //             !pCaseType.LiveChatTranscripts.isEmpty())
        //         return ORIGIN_TYPE_CHAT;
        //     else
        //         return ORIGIN_TYPE_EMAIL;
        // }
        return pCaseType.QI_Classification__c;
    }

    private static Case getCurrentCase(Id pCaseId){
        if(String.isBlank(pCaseId))
            return null;
        return [SELECT 
                    Origin, 
                    CreatedDate, 
                    QI_Score__c, 
                    Case_Tenure__c, 
                    Is_Trading_Advice__c,
                    QI_Classification__c,
                    QI_Case_Owner__c,
                    (SELECT Id FROM LiveChatTranscripts)
                FROM 
                    Case 
                WHERE Id =: pCaseId];
    }

    public class FormWrapper{
        @AuraEnabled
        public Decimal speedToAnswer {get; set;}
        @AuraEnabled
        public Decimal accuracy {get; set;}
        @AuraEnabled
        public Decimal tone {get; set;}
        @AuraEnabled
        public Decimal literacy {get; set;}
        @AuraEnabled
        public Decimal tagging {get; set;}
        @AuraEnabled
        public Decimal firstResponse {get; set;}
        @AuraEnabled
        public Decimal selfService {get; set;}
        @AuraEnabled
        public Boolean isTradingAdvice {get; set;}

		@AuraEnabled
        public String qiCaseOwnerId {get; set;}
    }

    /**
     * Class that represents the picklist entries
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/18/2021
     * @version 1.0
     */
    public class PicklistValuesWrapper{
        public PicklistValuesWrapper(String pLabel, String pValue){
            this.label = pLabel;
            this.value = pValue;
        }
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String value {get; set;}
    }

    /**
     * Class that represents the initial values of the LWC
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	01/18/2021
     * @version 1.0
     */
    public class InitialValuesWrapper{
        @AuraEnabled
        public List<PicklistValuesWrapper> speedToAnswerValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> accuracyValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> toneValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> literacyValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> taggingValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> firstResponseValues {get; set;}
        @AuraEnabled
        public List<PicklistValuesWrapper> selfServiceValues {get; set;}
        @AuraEnabled
        public String caseType {get; set;}
        @AuraEnabled
        public Decimal score {get; set;}
        @AuraEnabled
        public Decimal speedToAnswer {get; set;}
        @AuraEnabled
        public Decimal accuracy {get; set;}
        @AuraEnabled
        public Decimal tone {get; set;}
        @AuraEnabled
        public Decimal literacy {get; set;}
        @AuraEnabled
        public Decimal tagging {get; set;}
        @AuraEnabled
        public Decimal firstResponse {get; set;}
        @AuraEnabled
        public Decimal selfService {get; set;}
        @AuraEnabled
        public Boolean isTradingAdvice {get; set;}
        
        @AuraEnabled
        public String qiCaseOwnerId {get; set;}
        
        public InitialValuesWrapper(){
            speedToAnswer = 0;
            accuracy = 0;
            tone = 0;
            literacy = 0;
            tagging = 0;
            firstResponse = 0;
            selfService = 0;
        }
    }

    public class CustomException extends Exception{}
}