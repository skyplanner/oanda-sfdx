/**
 * @File Name          : MessagingAccountCheckInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/3/2023, 5:07:11 AM
**/
public inherited sharing class MessagingAccountCheckInfo {

	public Boolean isLinkedToAccountOrLead {get; set;}

	public ID fxAccountId {get; set;}

	public MessagingAccountCheckInfo(Boolean isLinkedToAccountOrLead) {
		this.isLinkedToAccountOrLead = isLinkedToAccountOrLead;
	}
	
}