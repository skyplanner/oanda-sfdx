/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-23-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiTradingExpirience {
    public Integer experience_duration {get; set;}
    
    public Integer experience_type {get; set;}
}