/**
 * @description       : SFDC Platform event handlers
 * @author            : Michal Piatek (mpiatek@oanda.com)
 * @group             : 
 * @last modified on  : 04-06-2024
 * @last modified by  : Michal Piatek
**/
public without sharing class PlatformEventsHandlersImplementations {

    public without sharing class HandleKnowledgeAssesmentUpdate implements PlatformEventDispatch.IPlatformEventHandlerClass {
        
        private final String ENDPOINT_NAME = 'knowledgeAssesment';
        private final String LOGGER_CATEGORY_URL = '@RestResource(/api/v1/user/*/assessment)';

        public void handle(List<SObject> events) {  
            try{
                Map<String,Map<String,Endpoints_Mappings__mdt>> endpointMappings;
                EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
                endpointMappings = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME);
                Map<String, Endpoints_Mappings__mdt> currentObjMapping = endpointMappings.get('fxAccount__c');

                List<fxAccount__c> fxAccountsToUpdate = new List<fxAccount__c>();

                for(SObject evt : events) { 
                    String jsonBody = (String) evt.get('Request_Body__c');
                    String fxaId = (String) evt.get('Context_Variables__c');

                    Map<String, Object> payload = (Map<String, Object>)JSON.deserializeUntyped(jsonBody);
                    fxAccount__c fxa = new fxAccount__c(Id = fxaId);

                    ApiMappingService mapper = new ApiMappingService();
                    mapper.loggerCategory = ENDPOINT_NAME;
                    mapper.loggerCategoryUrl = LOGGER_CATEGORY_URL;

                    for(String inputField : payload.keySet()) {
                        if(!currentObjMapping.containsKey(inputField)) {
                            continue;
                        }
                        mapper.setInputFieldtoSobject(
                            fxa, 
                            currentObjMapping.get(inputField), 
                            payload
                        );
                    }

                    fxAccountsToUpdate.add(fxa);
                }

                List<Database.SaveResult> saveResults = Database.update(fxAccountsToUpdate, false);

                //Log unsuccessful save results
                Logger.error(LOGGER_CATEGORY_URL, saveResults);

            } catch (Exception ex){
                Logger.exception(LOGGER_CATEGORY_URL, ex);
            }
        }
    }

    public without sharing class HandleInternalCLose implements PlatformEventDispatch.IPlatformEventHandlerClass {
        
        private final String ENDPOINT_NAME = 'internal_close';
        private final String LOGGER_CATEGORY_URL = '@RestResource(/api/v1/users/internal_close)';

        public void handle(List<SObject> events) {  
            try{

                Map<String,Map<String,Endpoints_Mappings__mdt>> endpointMappings;
                EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
                endpointMappings = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME);
                Map<String, Endpoints_Mappings__mdt> currentObjMapping = endpointMappings.get('fxAccount__c');

                List<fxAccount__c> fxAccountsToUpsert = new List<fxAccount__c>();

                Set<String> fxTradeGlobalIdSet = new Set<String>();

                for(SObject evt : events) { 
                    String jsonBody = (String) evt.get('Request_Body__c');

                    Object deserialized = JSON.deserializeUntyped(jsonBody);

                    List<Object> deserializedFxaToProcess = new List<Object>();

                    if(deserialized instanceOf List<Object>) {
                        deserializedFxaToProcess = (List<Object>) deserialized;
                    } else {
                        deserializedFxaToProcess.add(deserialized);
                    }

                    for(Object ob : deserializedFxaToProcess) { 
                        Map<String, Object> payload = (Map<String, Object>) ob;
                        
                        Integer userId = (Integer) payload.get('user_id');
                        Boolean isLive = (Boolean) payload.get('is_live');

                        String userGlobalId = fxAccountUtil.constructGlobalTradeId(userId, isLive);
                        Logger.info(userGlobalId);
                        fxAccount__c fxa = new fxAccount__c(fxTrade_Global_Id__c = userGlobalId);

                        ApiMappingService mapper = new ApiMappingService();
                        mapper.loggerCategory = ENDPOINT_NAME;
                        mapper.loggerCategoryUrl = LOGGER_CATEGORY_URL;
    
                        for(String inputField : payload.keySet()) {
                            if(!currentObjMapping.containsKey(inputField)) {
                                continue;
                            }
                            mapper.setInputFieldtoSobject(
                                fxa, 
                                currentObjMapping.get(inputField), 
                                payload
                            );
                        }
                        fxAccountsToUpsert.add(fxa);
                    }
                }
                List<Database.UpsertResult> upsertResults = Database.upsert(fxAccountsToUpsert, fxAccount__c.Fields.fxTrade_Global_ID__c, false);
                
                //Log unsuccessful save results
                Logger.error(LOGGER_CATEGORY_URL, upsertResults);
            } catch (Exception ex){
                Logger.exception(LOGGER_CATEGORY_URL, ex);
            }
        }

    }
}