public with sharing class BatchSetLivefxAccountStatsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable{
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private boolean runPracticeStats = false; 
	
	public static final Integer BATCH_SIZE = 200;
	public static DateTime TWO_DAYS_AGO = DateTime.now().addDays(-2);
	
	public static final String CRON_NAME = 'BatchSetLivefxAccountStatsScheduleable';
    
    public static final String CRON_SCHEDULE_Daily = '0 30 1 * * ?';
	
	public BatchSetLivefxAccountStatsScheduleable(boolean runPractice){
		runPracticeStats = runPractice;
		
		query ='select id, Account__c, Lead__c, CreatedDate, LastModifiedDate from fxAccount__c where RecordTypeId = \'' + RecordTypeUtil.getFxAccountLiveId() + '\' and CreatedDate > ' + SoqlUtil.getSoql(TWO_DAYS_AGO);
	}
	
	public BatchSetLivefxAccountStatsScheduleable(boolean runPractice, String q){
		runPracticeStats = runPractice;
		
		query = q;
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'select id, Account__c, Lead__c, CreatedDate, LastModifiedDate from fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<fxAccount__c> fxAccounts = (List<fxAccount__c>)batch;
		
		fxAccountStatsUtil.processLiveCreationTime(fxAccounts);
	}
	
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		if(runPracticeStats){
			BatchSetDemofxAccountStatsScheduleable.executeBatch(TWO_DAYS_AGO);
		}
	}
	
	public static void schedule() {
		System.schedule(CRON_NAME, CRON_SCHEDULE_Daily, new BatchSetLivefxAccountStatsScheduleable(true));
	}
	
	public void execute(SchedulableContext context) {
		executeBatch();
	}
	
	public static Id executeBatch(Integer batchSize, boolean runPractice) {
		return Database.executeBatch(new BatchSetLivefxAccountStatsScheduleable(runPractice), batchSize);
	}
	
	public static Id executeBatch() {
		return executeBatch(BATCH_SIZE, true);
	}
    
}