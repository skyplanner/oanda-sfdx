@isTest
public with sharing class DocumentLinkUtilTest {
   
    @TestSetup
    static void makeData()
    {
        
    }

    /**
     * Test Scenes: 1 and 2
     */
    static testMethod void test1()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        System.runAs(getIntegrationUser()) {
            UserUtil.refreshCurrentUser();

            Document__c doc = new Document__c(
                name = 'Passport',
                RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
                Document_Type__c = 'Passport',
                Expiry_Date__c = Date.today(),
                fxAccount__c = fxAcc.Id);
            insert doc;

            doc = 
                [SELECT Lead__c,
                        Account__c,
                        Show_on_Profile__c
                    FROM Document__c 
                    WHERE Id = :doc.Id];

            // Test Scene 1
            
            System.assertEquals(
                doc.Account__c, fxAcc.Account__c);
            System.assertEquals(
                doc.Lead__c, fxAcc.Lead__c);

            // Test Scene 2
            System.assertEquals(
                doc.Show_on_Profile__c, true);
        }
    }

    /**
     * Test Scene 3
     */
    static testMethod void test2()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = false);
        insert doc;

        doc = 
            [SELECT Account__c,
                    Show_on_Profile__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 3
        System.assertEquals(
            doc.Show_on_Profile__c, true);
    }

    /**
     * Test Scene 4
     */
    static testMethod void test3()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org',
            RecordTypeId = RecordTypeUtil.getLeadStandardId());
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        Case case1 = new Case(
            Subject = 'Case 1',
            fxAccount__c = fxAcc.Id,
            Lead__c = lead1.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        insert case1;

        Test.startTest();

        // Update lead rt because it was changed in fxAcc trigger
        lead1.RecordTypeId = RecordTypeUtil.getLeadStandardId();
        update lead1;

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id);
        insert doc;

        doc = 
            [SELECT Lead__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 4
        System.assertEquals(
            doc.Lead__c,
            case1.Lead__c);
    }

    /**
     * Test Scene 5
     */
    static testMethod void test4()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        Case case1 = new Case(
            Subject = 'Case 1',
            AccountId = acc.Id,
            fxAccount__c = fxAcc.Id,
            Lead__c = lead1.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        insert case1;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id,
            Lead__c = lead1.Id,
            fxAccount__c = fxAcc.Id);
        insert doc;

        doc = 
            [SELECT Show_on_Profile__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 5
        System.assertEquals(
            doc.Show_on_Profile__c,
            false);
    }

    /**
     * Test Scene 6
     */
    static testMethod void test5()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        Case case1 = new Case(
            Subject = 'Case 1',
            AccountId = acc.Id,
            fxAccount__c = fxAcc.Id,
            Lead__c = lead1.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        insert case1;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id);
        insert doc;

        doc = 
            [SELECT Account__c,
                    fxAccount__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 6

        System.assertEquals(
            doc.Account__c,
            case1.AccountId);

        System.assertEquals(
            doc.fxAccount__c,
            case1.fxAccount__c);
    }

    /**
     * Test Scene 7
     */
    static testMethod void test6()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Case case1 = new Case(
            Subject = 'Case 1',
            AccountId = acc.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        insert case1;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id);
        insert doc;

        doc = 
            [SELECT Account__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 7

        System.assertEquals(
            doc.Account__c,
            case1.AccountId);
    }

    /**
     * Test Scene 8
     */
    static testMethod void test7()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        Case case1 = new Case(
            Subject = 'Case 1',
            fxAccount__c = fxAcc.Id,
            Lead__c = lead1.Id);
        insert case1;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id);
        insert doc;

        doc = 
            [SELECT Lead__c,
                    fxAccount__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 8

        System.assertEquals(
            doc.Lead__c,
            case1.Lead__c);

        System.assertEquals(
            doc.fxAccount__c,
            case1.fxAccount__c);
    }

    /**
     * Test Scene 9
     */
    static testMethod void test8()
    {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org',
            RecordTypeId = RecordTypeUtil.getLeadRetailId());
        insert lead1;

        Case case1 = new Case(
            Subject = 'Case 1',
            Lead__c = lead1.Id);
        insert case1;

        Test.startTest();

        Document__c doc = new Document__c(
            name = 'Passport',
            RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
            Document_Type__c = 'Passport',
            Expiry_Date__c = Date.today(),
            Show_on_Profile__c = true,
            Case__c = case1.Id);
        insert doc;

        doc = 
            [SELECT Lead__c
                FROM Document__c 
                WHERE Id = :doc.Id];

        // Test Scene 9

        System.assertEquals(
            doc.Lead__c,
            case1.Lead__c);
    }

    /**
     * Get a registration user to run test code
     */
    private static User getIntegrationUser() {
        List<User> users = 
            [SELECT Id
                FROM User 
                WHERE UserName = 'regUser@testemail.com'];

        if(!users.isEmpty())
            return users[0];
        
        List<Profile> regProfiles =
            [SELECT Id
                FROM Profile 
                WHERE Name = :UserUtil.NAME_INTEGRATION_PROFILE
                LIMIT 1];

        User regUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = regProfiles[0].Id,
            UserName = 'regUser@testemail.com');
        
        insert regUser;
        return regUser;
    }

}