/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-28-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public with sharing class ScheduledEmailManager {
	private IScheduledEmailSettingManager settingManager;

	public ScheduledEmailManager(IScheduledEmailSettingManager settingManager) {
		this.settingManager = settingManager;
	}

	public ScheduledEmailWrapper getScheduleEmail(String caseId) {
		Scheduled_Email__c scheduledEmail = ScheduledEmailRepo.getByCaseAndOwner(
			caseId,
			UserInfo.getUserId()
		);
		System.debug(this.convertFromSObject(scheduledEmail));
		return this.convertFromSObject(scheduledEmail);
	}
  
	public ScheduledEmailWrapper saveScheduleEmail(
		String caseId,
		ScheduledEmailWrapper wrapper
	) {
		Scheduled_Email__c scheduledEmail = this.convertFromWrapper(wrapper);
		if (scheduledEmail.Id == null) {
			scheduledEmail.Case__c = caseId;
			EmailMessage message = EmailMessageRepo.getDraftByParentId(caseId);
			if (message == null) {
				AuraException noDraftException = new AuraException(
					System.Label.Schedule_an_email_need_Draft
				);
				noDraftException.setMessage(
					System.Label.Schedule_an_email_need_Draft
				);
				throw noDraftException;
			}
			scheduledEmail.Email_Message__c = message.Id;
		}
		upsert scheduledEmail;
		return this.convertFromSObject(scheduledEmail);
	}

	public ScheduledEmailWrapper deleteScheduleEmail(String id) {
		Scheduled_Email__c scheduledEmail = new Scheduled_Email__c(Id = id);
		delete scheduledEmail;
		return this.convertFromSObject(null);
	}

	public void deleteByEmailMessage(List<Id> emailMessageIds) {
		List<Scheduled_Email__c> deleteScheduledEmailList = ScheduledEmailRepo.getByEmailMessages(emailMessageIds);
		if (!deleteScheduledEmailList.isEmpty()) {
			delete deleteScheduledEmailList;
		}
	}

	private ScheduledEmailWrapper convertFromSObject(
		Scheduled_Email__c scheduledEmail
	) {
		ScheduledEmailWrapper wrapper = new ScheduledEmailWrapper();
		wrapper.scheduledAmOrPm = scheduledEmail != null
			? scheduledEmail.AM_or_PM__c
			: settingManager.getSetting().getAMOrPM();
		wrapper.scheduledDate = scheduledEmail != null
			? scheduledEmail.Scheduled_Date__c
			: settingManager.getSetting().getDate();
		wrapper.scheduledTime = scheduledEmail != null
			? scheduledEmail.Time__c
			: settingManager.getSetting().getTime();
		wrapper.id = scheduledEmail != null ? scheduledEmail.Id : null;
		wrapper.emailMessageId = scheduledEmail != null
			? scheduledEmail.Email_Message__c
			: null;
		return wrapper;
	}
	private Scheduled_Email__c convertFromWrapper(
		ScheduledEmailWrapper wrapper
	) {
		Scheduled_Email__c scheduledEmail = new Scheduled_Email__c();
		scheduledEmail.AM_or_PM__c = wrapper.scheduledAmOrPm;
		scheduledEmail.Time__c = wrapper.scheduledTime;
		scheduledEmail.Scheduled_Date__c = wrapper.scheduledDate;
		scheduledEmail.Id = wrapper.id;
		scheduledEmail.Scheduled_Date_Time__c = createFromWrapper(wrapper);
		return scheduledEmail;
	}

	private Datetime createFromWrapper(ScheduledEmailWrapper wrapper) {
		Integer hours = to24Hours(
			wrapper.scheduledTime,
			wrapper.scheduledAmOrPm
		);
		return Datetime.newInstance(
			wrapper.scheduledDate.year(),
			wrapper.scheduledDate.month(),
			wrapper.scheduledDate.day(),
			hours,
			0,
			0
		);
	}
	private Integer to24Hours(String strTime, String amOrPm) {
		Integer hour = Integer.valueOf(strTime);
		if (strTime == '12') {
			hour = 0;
		}
		return amOrPm == 'PM' ? hour + 12 : hour;
	}
	public class ScheduledEmailWrapper {
		@AuraEnabled
		public String id { get; set; }
		@AuraEnabled
		public Date scheduledDate { get; set; }
		@AuraEnabled
		public String scheduledTime { get; set; }
		@AuraEnabled
		public String scheduledAmOrPm { get; set; }
		@AuraEnabled
		public String emailMessageId { get; set; }
	}
}