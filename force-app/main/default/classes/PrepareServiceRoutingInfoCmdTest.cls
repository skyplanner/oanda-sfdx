/**
 * @File Name          : PrepareServiceRoutingInfoCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/12/2024, 12:58:56 AM
**/
@IsTest
private without sharing class PrepareServiceRoutingInfoCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createLead1(initManager);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createSegmentation1(initManager);
		initManager.storeData();
	}

	// infoList is null or empty
	@IsTest
	static void execute1() {
		Test.startTest();
		PrepareServiceRoutingInfoCmd instance = 
			new PrepareServiceRoutingInfoCmd(null);
		Boolean result = instance.execute();
		Test.stopTest();
		
		Assert.isFalse(result, 'Invalid result');
	}

	// completeAccountInformation
	@IsTest
	static void execute2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		sRoutingInfo.email = ServiceTestDataKeys.ACCOUNT1_EMAIL;
		sRoutingInfo.infoIsIncomplete = true;
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> {sRoutingInfo };
	
		Test.startTest();
		PrepareServiceRoutingInfoCmd instance = 
			new PrepareServiceRoutingInfoCmd(infoList);
		Boolean result = instance.execute();
		Test.stopTest();
		
		Assert.isTrue(result, 'Invalid result');
	}

	// completeLeadInformation
	@IsTest
	static void execute3() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		sRoutingInfo.email = ServiceTestDataKeys.LEAD1_EMAIL;
		sRoutingInfo.infoIsIncomplete = true;
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> {sRoutingInfo };
	
		Test.startTest();
		PrepareServiceRoutingInfoCmd instance = 
			new PrepareServiceRoutingInfoCmd(infoList);
		Boolean result = instance.execute();
		Test.stopTest();
		
		Assert.isTrue(result, 'Invalid result');
	}
	
}