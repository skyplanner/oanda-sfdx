/**
 * Controller associated with pages rendering the survey.
 * Used by SurveyPage, ResultsPage, TakeSurvey
 */
global without sharing class ViewSurveyController extends HelpPortalBase {
	public Boolean isMobile { get; private set; }
	public String surveyId { get; set; }

	public List<SFQuestion> allQuestions { get; set; }
	public Map<Id, List<SFQuestion>> dependantQuestions { get; set; }

	public String caseId { get; private set; }
	public String contactId { get; private set; }
	public String leadId { get; private set; }
	public String chatKey { get; private set; }

	public Boolean thankYouRendered { get; set; }
	public Boolean showTrustPilot { get; set; }
	public Integer currentQuestionIndex { get; set; }
	public SFQuestion currentQuestion { get; private set; }
	public Boolean readyToSubmit { get; private set; }

	public Boolean isPreview { get; private set; }
	public Boolean isPostChat { get; private set; }
	public Boolean hasErrors { get; private set; }
	public String errorMessage { get; private set; }

	public String headerHtml { get; private set; }
	public String footerHtml { get; private set; }

	public static final Set<string> fullScores = new Set<string> {
		'Extremely',
		'Very easy',
		'Yes', 
		'Excellent'
	};
	
	public ViewSurveyController(ApexPages.StandardController stdController) {
		Map<String, String> params;
		// Get url parameters
		params = Apexpages.currentPage().getParameters();
		caseId   = params.get('caId');
		contactId = params.get('cId');
		leadId = params.get('lId');
		chatKey = params.get('chatKey');
		isPreview = params.get('isPreview') == 'true';

		// props
		surveyId = stdController.getId();
		currentQuestionIndex = null;
		currentQuestion = null;
		readyToSubmit = false;
		isMobile = isMobile();

		allQuestions = new List<SFQuestion>();
		dependantQuestions = new Map<Id, List<SFQuestion>>();

		// if the survey if not present, we can't do anything
		if (String.isBlank(surveyId)) {
			hasErrors = true;
			errorMessage = 'noSurveyError';
		} else if (!isPreview && hasTakenSurvey()) {
			hasErrors = true;
			errorMessage = 'surveyTakenError';
		} else {
			isPostChat = String.isNotBlank(chatKey);
			thankYouRendered = false;
			hasErrors = false;
			errorMessage = null;

			// we setup question (standalone and dependant)
			setupQuestions();

			if (isMobile && !allQuestions.isEmpty()) {
				currentQuestionIndex = 0;
				currentQuestion = allQuestions[0];
				readyToSubmit = allQuestions.size() == 1;
			}
		}
	}
	
	/** 
	 * Fills up the List of questions to be 
	 * displayed on the Visualforce page
	 */
	private void setupQuestions() {
		SFQuestion question;

		for (Survey_Question__c q : [
					SELECT 
						Type__c,
						Id,
						Survey__c, 
						Required__c,
						Question__c, 
						OrderNumber__c,
						Name,
						Choices__c,
						AnswersToTriggerExtraFeedback__c,
						Parent_Question__c,
						Parent_Question_Answers__c, (
							SELECT TranslatedQuestion__c, 
								TranslatedChoices__c,
								TransalatedExtraFeedbackAnswers__c
							FROM SurveyQuestionTranslations__r
							WHERE Language__c = :getLanguageLabel(language)
						)
					FROM Survey_Question__c
					WHERE Survey__c =: surveyId 
					ORDER BY OrderNumber__c
				]) {
			question = new SFQuestion(q);

			// if the question is dependant, we place it in a map
			// so it ca be easily found and inserted if any of
			// the trigger questions is selected
			if (question.isQuestionDependant) {
				// we either add to an existent key
				if (dependantQuestions.containsKey(question.parentQuestionId))
					dependantQuestions.get(question.parentQuestionId).add(question);
				// or create the new key and list, with the question
				else
					dependantQuestions.put(question.parentQuestionId,
						new List<SFQuestion> { question }); 
			} 
			// if this is a standalone, we directly
			// add it to the inital list
			else
				allQuestions.add(question);
		}
	}

	/**
	 * Moves on to the next question
	 */
	public Pagereference nextQuestion() {
		// we only move on if question is good
		if (isQuestionGood(currentQuestion)) {
			if (currentQuestionIndex < allQuestions.size() - 1) {
				currentQuestionIndex++;
				currentQuestion = allQuestions[currentQuestionIndex];
			}

			if (currentQuestionIndex == allQuestions.size() - 1)
				readyToSubmit = true;
		}

		return null;
	}

	/**
	 * Moves on to the previous question
	 */
	public Pagereference previousQuestion() {
		// we only move on if question is good
		if (currentQuestionIndex > 0) {
			currentQuestionIndex--;
			currentQuestion = allQuestions[currentQuestionIndex];
		}

		return null;
	}
	
	/**
	 * Proceeds to submit all results.
	 */
	public void submitResults() {
		List<SurveyQuestionResponse__c> sqrList;
		SurveyQuestionResponse__c sqr;
		SurveyTaker__c taker;
		Case c;

		if (hasTakenSurvey()) {
			hasErrors = true;
			errorMessage = 'surveyTakenError';
			return;
		} 

		hasErrors = false;
		errorMessage = null;
		sqrList = new List<SurveyQuestionResponse__c>();
		showTrustPilot = true;

		for (SFQuestion q : allQuestions) {
			// we check if the question is good
			if (isQuestionGood(q)) {
				sqr = new SurveyQuestionResponse__c(
					Survey_Question__c = q.id,
					Response__c = q.getResponse()
				);

				if(q.questionType != 'Free Text' && !fullScores.contains(sqr.Response__c))
					showTrustPilot = false;

				q.wasChecked = true;
				q.checkIfFeedbackIsNeeded();
				if (q.needsExtraFeedback && String.isNotBlank(q.extraFeedback))
					sqr.ExtraFeedback__c = q.extraFeedback;
				
				sqrList.add(sqr);
			} else {
				hasErrors = true;
				errorMessage = 'submittingError';
			}
		}

		if (!hasErrors)
			try {
				taker = getSurveyTaker();
				insert taker;

				for (SurveyQuestionResponse__c sq : sqrList)
					sq.SurveyTaker__c = taker.Id;

				insert sqrList;

				if (String.isNotBlank(caseId)) {
					c = getCase();
					c.FeedbackReceived__c = true;
					c.FeedbackReceivedOn__c = System.now();
					update c;
				}

				thankYouRendered = true;
			} catch (Exception e) {
				hasErrors = true;
				errorMessage = 'savingError';
			}
	}
		
	/**
	 * Analyzes if the curret question requires extra feedback.
	 */
	public void checkSelection() {
		SFQuestion q, removed;
		Set<Id> addedQuestionIds, toRemove;
		Integer index = currentQuestionIndex;
		List<Integer> indexesToRemove;

		// we need the ids of all question that are 
		// currently being show
		addedQuestionIds = getQuestionIds();
		toRemove = new Set<Id>();
		
		// we need both the current question and a relation
		// of the currently displayed dependant questions
		// as they maight need removal..
		q = allQuestions.get(index);

		// initially, we need to check for the feedback extra field
		q.checkIfFeedbackIsNeeded();

		// the we need to see if there is a dependant question
		// that should be inserted based on the answer..ApexPages
		if (dependantQuestions.containsKey(q.id)) {
			// we iterate to add new answers,
			// and keep a record of answers we need to remove
			for (SFQuestion dp : dependantQuestions.get(q.id))
				// if of the selected answers should trigger,
				// we add the question after the current one..
				// but we can only add it if it hasn't been added already
				if (dp.isDependantAnswer(q)) {
					if (!addedQuestionIds.contains(dp.id)) {
						addedQuestionIds.add(dp.id);
						if (++index >= allQuestions.size())
							allQuestions.add(dp);
						else
							allQuestions.add(index, dp);
					}
				// if the answer is not selected as a trigger,
				// but the question was previoslu added,
				// we need to remove it
				} else if (addedQuestionIds.contains(dp.id))
					toRemove.add(dp.id);

			// if we have any questyion to remove...
			// we them remove them
			if (!toRemove.isEmpty()) {
				indexesToRemove = new List<Integer>();
				index = 0;
				for (SFQuestion rq : allQuestions) {
					if (toRemove.contains(rq.id))
						indexesToRemove.add(index);
					// we need to increse to keep the index in sync
					index++;
				}

				// if we came up with any index to remove
				// we do it in reverse order
				indexesToRemove.sort();
				for (Integer i = indexesToRemove.size() - 1; i >= 0; i--) {
					removed = allQuestions.remove(indexesToRemove.get(i));
					// also need to clear the answes
					removed.clearResponse();
				}
			}
		}
	}

	/** 
	 * Returns true if the user has already taken the survey
	 * @return the id of the new survey taker instance
	 */
	private Boolean hasTakenSurvey() {
		return String.isNotBlank(caseId) &&
			(String.isNotBlank(contactId) || String.isNotBlank(leadId)) && 
			[
				SELECT COUNT()
				FROM SurveyTaker__c 
				WHERE Survey__c = :surveyId 
				AND Case__c = :caseId
				AND (
					(Contact__c != NULL AND Contact__c = :contactId)
					OR (Lead__c != NULL AND Lead__c = :leadId)
				)
			] > 0;
	}
	
	/** 
	 * Adds the current user (in internal) as a survey taker.
	 * @return the id of the new survey taker instance
	 */	
	private SurveyTaker__c getSurveyTaker() {
		return new SurveyTaker__c(
			Contact__c = contactId,
			Survey__c = surveyId,
			Taken__c = 'false',
			Case__c = caseId,
			Lead__c = leadId,
			User__c = UserInfo.getUserId(),
			LiveChatTranscript__c = getChatTranscriptId()
		);
	}

	/** 
	 * Adds a post chat record linked to the current
	 */
	private Id getChatTranscriptId() {
		String difName = chatKey;
		if (String.isBlank(difName))
			return null;
		else
			try {
				return [
					SELECT Id
					FROM LiveChatTranscript
					WHERE ChatKey = :difName
					LIMIT 1
				].Id;
			} catch (Exception ex) {
				return null;
			}
	}

	/**
	 * @return the case with the specified id
	 */
	private Case getCase() {
		return [
			SELECT Id
			FROM Case
			WHERE Id = :caseId
		];
	}

	/**
	 * @return 
	 */
	private Set<Id> getQuestionIds() {
		Set<Id> result;

		result = new Set<Id>();
		for (SFQuestion q : allQuestions)
			result.add(q.id);

		return result;
	}

	/**
	 * Validates the specified question
	 * @param question
	 */
	private Boolean isQuestionGood(SFQuestion q) {
		// we clear the error at the beginning
		q.hasError = false;
		q.errorMessage = null;

		// radio (row)
		if (q.renderSelectRow == 'true')
			if (q.required && String.isBlank(q.selectedOption)) {
				q.hasError = true;
				q.errorMessage = 'fieldRequired';
			}
		// radio (row or column)
		else if (q.renderSelectRadio == 'true')
			if (q.required && String.isBlank(q.selectedOption)) {
				q.hasError = true;
				q.errorMessage = 'fieldRequired';
			}
		// free text
		else if (q.renderFreeText == 'true')
			if (q.required && String.isBlank(q.choices)) {
				q.hasError = true;
				q.errorMessage = 'fieldRequired';
			}
		// check boxes
		else if (q.renderSelectCheckboxes == 'true')
			if (q.required && 
					(q.selectedOptions == null || 
						q.selectedOptions.isEmpty())) {
				q.hasError = true;
				q.errorMessage = 'fieldRequired';
			}

		return !q.hasError;
	}
}