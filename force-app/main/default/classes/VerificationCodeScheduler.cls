/**
 * @File Name          : VerificationCodeScheduler.cls
 * @Description        : Fill Verification Code Custom Setting with first 1000 record from big object
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : ejung
 * @Last Modified On   : 14/05/2023
 * @Modification Log   : modified to reload Verification Code when it dips below the threashold
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   dmorales     Initial Version
**/
global without sharing class VerificationCodeScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
        
		List<Verification_Code_Setting__c> verifCodeRemain = Verification_Code_Setting__c.getAll().values();
        Integer countConfig = VerificationCodeConfigManager.getInstance().countToLoad;
        if(verifCodeRemain.size() < countConfig){
            Integer countToLoad = 1000 - verifCodeRemain.size();
            System.enqueueJob(new VerificationCodeJob(countToLoad));
        }
	}
	
}