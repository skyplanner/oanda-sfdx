/**
 * Tests:
 * SPGeneralSettings.cls
 */
@isTest
private class SPGeneralSettingsTest {
	
	/**
	 * public static SPGeneralSettings getInstance()
	 */
	@isTest
	static void getInstance() {
		System.assertNotEquals(null, SPGeneralSettings.getInstance());
		System.assertNotEquals(null, SPGeneralSettings.getInstance());
	}

	/**
	 * public String getValue(String settingApiName)
	 */
	@isTest
	static void getValue() {
		SPGeneralSettings instance = SPGeneralSettings.getInstance();
		System.assertEquals(null, instance.getValue('unexistent_settingApiName'));
	}

	/**
	 * public Boolean getValueAsBoolean(String settingApiName)
	 */
	@isTest
	static void getValueAsBoolean() {
		SPGeneralSettings instance = SPGeneralSettings.getInstance();
		System.assertEquals(null,
			instance.getValueAsBoolean('unexistent_settingApiName'));
	}

	/**
	 * public List<String> getValueAsList(String settingApiName)
	 */
	@isTest
	static void getValueAsList() {
		SPGeneralSettings instance = SPGeneralSettings.getInstance();
		System.assert(!instance.getValueAsList(
			'CD_Annual_PIU_Process_Divisions').isEmpty());
	}

	/**
	 * public List<Integer> getValueAsIntegerList(String settingApiName)
	 */
	/* @isTest
	static void getValueAsIntegerList() {
		SPGeneralSettings instance = SPGeneralSettings.getInstance();
		System.assert(!instance.getValueAsIntegerList(
			'Annual_PIU_Preference_Mapping_From_72').isEmpty());
	} */

	/**
	 * public static Map<String, String> getGeneralSettingsValues()
	 */
	@isTest
	static void getGeneralSettingsValues() {
		System.assertNotEquals(null, SPGeneralSettings.getGeneralSettingsValues());
	}
}