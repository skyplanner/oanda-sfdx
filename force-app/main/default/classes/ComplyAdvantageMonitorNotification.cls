public with sharing class ComplyAdvantageMonitorNotification
{
    public String event { get; set; }
    public MonitorData data { get; set; }
   

    public class SearchStatusChangeFields{
        public String match_status { get; set; }
        public String risk_level { get; set; }
    }
    public class SearchStatusChanges{
        public SearchStatusChangeFields before { get; set; }
        public SearchStatusChangeFields after { get; set; }
    }

    public class MonitorData
	{
        // monitored_search_updated,search_status_updated,match_status_updated
        public String search_id { get; set; }

        // search_status_updated,match_status_updated
        public String client_ref { get; set; }

        //search reference - search_status_updated
        public String ref { get; set; }

        // monitored_search_updated
        public String[] updated { get; set; }
		public String[] added { get; set; }
		public String[] removed { get; set; }	
		public boolean is_suspended { get; set; }

        // search_status_updated
        public SearchStatusChanges changes  { get; set; }

		//match_status_updated - for Entities
        public String entity_id { get; set; }
		public String entity_match_status { get; set; }
		public Boolean is_whitelisted { get; set; }
		public Map<String, String> tags { get; set; }
        
	}
}