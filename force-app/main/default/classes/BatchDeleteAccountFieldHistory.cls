public class BatchDeleteAccountFieldHistory implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	public BatchDeleteAccountFieldHistory() {
		//Query for all AccountHistory
		query = 'SELECT Id, AccountId, Field FROM AccountHistory WHERE Field = \'PersonEmail\' OR Field = \'Phone\'';
	}

	public BatchDeleteAccountFieldHistory(String q) {
   		query = q;
  	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, AccountId, Field FROM AccountHistory WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		delete batch;
	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		BatchFormatLeadInfo b = new BatchFormatLeadInfo();
		database.executebatch(b);
	}


}