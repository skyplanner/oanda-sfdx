/**
 * @File Name          : AgentBySkills_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/16/2020, 10:52:46 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/9/2020   acantero     Initial Version
**/
@istest
private class AgentBySkills_Test {

    static final String FAKE_AGENT_ID = '0050B0000081XTXQA2';
    static final String FAKE_SERVICE_PRESENCE_STATUS_ID = '0N53C00000000WUSAY';

    @isTest
    static void existsCompatibleAgent_test() {
        Test.startTest();
        AgentBySkills agentsManager = new AgentBySkills(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        Boolean result = agentsManager.existsCompatibleAgent(
            'LE CC OME', 
            3, 
            FAKE_AGENT_ID
        );
        Test.stopTest();
        System.assertEquals(false, result);
    }

    @isTest
    static void existsCompatibleAgents_test1() {
        Test.startTest();
        AgentBySkills agentsManager = new AgentBySkills(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        Set<String> skillsCodes = new Set<String>
        {
            OmnichanelConst.ENGLISH_SKILL_CODE,
            OmnichanelConst.SPANISH_SKILL_CODE
        };
        Set<String> result = agentsManager.existsCompatibleAgents(
            skillsCodes, 
            3
        );
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void existsCompatibleAgents_test2() {
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = FAKE_AGENT_ID,
            Name = FAKE_AGENT_ID,
            Capacity__c = 3,
            Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
        );
        insert agentInfo;
        Test.startTest();
        AgentBySkills agentsManager = new AgentBySkills(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        agentsManager.activeAgents = new List<String> {FAKE_AGENT_ID};
        Set<String> skillsCodes = new Set<String>
        {
            OmnichanelConst.ENGLISH_SKILL_CODE,
            OmnichanelConst.SPANISH_SKILL_CODE
        };
        Set<String> result = agentsManager.existsCompatibleAgents(
            skillsCodes, 
            3
        );
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void existsCompatibleAgents_test3() {
        Test.startTest();
        AgentBySkills agentsManager = new AgentBySkills(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        Set<String> result = agentsManager.existsCompatibleAgents(
            null, 
            3
        );
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

}