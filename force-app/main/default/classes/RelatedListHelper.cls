/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class RelatedListHelper {
    public static RelatedListResponse getRelatedListQuery(String recordId, String mdtDevName) {
        Related_List_Setting__mdt mainOjMdt = RelatedListMDT.getInstance().getMdt(mdtDevName);
        String objApiName = mainOjMdt.Object__r.QualifiedApiName;
        Map<String, Schema.SObjectField> fields = getFields(objApiName);
        RelatedListResponse result = getFieldNames(objApiName, mainOjMdt, fields);
        String q = String.format(
            'SELECT Id{0} FROM {1} WHERE {2} = "{3}"{4}',
            new List<String> {
                (result.fields?.size() > 0 ? (', ' + String.join(result.fields, ', ')) : ''),
                objApiName,
                mainOjMdt.Parent_Id_Field__r.QualifiedApiName,
                recordId,
                String.isNotBlank(mainOjMdt.Where_Condition__c)
                    ? (' AND ' + mainOjMdt.Where_Condition__c)
                    : ''
            }
        );

        result.wrapper.mdtName = mainOjMdt.MasterLabel;
        result.query = q.replaceAll('"', '\'');

        return result;
    }

    private static RelatedListResponse getFieldNames(String objApiName, Related_List_Setting__mdt parentOjMdt, Map<String, Schema.SObjectField> fields) {
        List<String> fieldNames = new List<String>();
        RelatedListWrapper wrapper = new RelatedListWrapper();

        List<RelatedListFieldSettingMDT> fieldsSettings = sortRelatedListFieldSett(parentOjMdt.Related_List_Fields_Setting__r);
        for (RelatedListFieldSettingMDT childWrapper : fieldsSettings) {
            Related_List_Field_Setting__mdt child = childWrapper.fieldSetting;
            System.debug('child: ' + child.Order__c + ' ' + child.Object__r.QualifiedApiName);

            if (child.Child_Related__c != null) {
                RelatedListResponse res = getChildInfo(parentOjMdt.Object__r.QualifiedApiName, child.Child_Related__r.DeveloperName);
                if (res != null) {
                    wrapper.childrenWrapper.add(res.wrapper);
                    fieldNames.add(res.query);
                }
            } else {
                wrapper.objApiName = objApiName;
                wrapper.apiNames.add(child.Field__r.QualifiedApiName);
                wrapper.fieldLabels.add(child.MasterLabel);
                wrapper.types.add(fields.get(child.Field__r.QualifiedApiName).getDescribe().getType());
                wrapper.linkToRecord.add(child.Link_to_Record__c);

                fieldNames.add(child.Field__r.QualifiedApiName);
            }
        }

        RelatedListResponse result = new RelatedListResponse();
        result.fields = fieldNames;
        result.wrapper = wrapper;
        return result;
    }

    private static List<RelatedListFieldSettingMDT> sortRelatedListFieldSett(List<Related_List_Field_Setting__mdt> toSort) {
        List<RelatedListFieldSettingMDT> result = new List<RelatedListFieldSettingMDT>();
        for (Related_List_Field_Setting__mdt child : toSort) {
            result.add(new RelatedListFieldSettingMDT(child));
        }
        result.sort();

        return result;
    }
    
    private static RelatedListResponse getChildInfo(String parentApiName, String childMdtDevName) {
        Related_List_Setting__mdt childOjMdt = RelatedListMDT.getInstance().getMdt(childMdtDevName);
        if (childOjMdt == null) {
            return null;
        }
        String childApiName = childOjMdt.Object__r.QualifiedApiName;
        Map<String, Schema.SObjectField> fields = getFields(childApiName);
        RelatedListResponse res = getFieldNames(childApiName, childOjMdt, fields);
        String objApiName = getChildRelationshipName(parentApiName, childApiName);

        String q = String.format(
            '(SELECT Id{0} FROM {1}{2})',
            new List<String> {
                (res.fields?.size() > 0 ? (', ' + String.join(res.fields, ', ')) : ''),
                objApiName,
                String.isNotBlank(childOjMdt.Where_Condition__c)
                    ? (' WHERE ' + childOjMdt.Where_Condition__c)
                    : ''
            }
        );

        res.wrapper.childRelationshipName = objApiName;
        res.wrapper.mdtName = childOjMdt.MasterLabel;
        res.query = q;

        return res;
    }

    private static String getChildRelationshipName(String parentApiName, String childApiName) {
        String objApiName;
        List<Schema.ChildRelationship> childRelationship = Schema.getGlobalDescribe().get(parentApiName).getDescribe().getChildRelationships();
        for (Schema.ChildRelationship cr: childRelationship) {
            if (cr.getChildSObject().getDescribe().getName() == childApiName) {
                objApiName = cr.getRelationshipName();
                break;
            }
        }

        return objApiName;
    }

    public static List<SObject> getSObjects(String query) {
        return Database.query(query);
    }

    public static RelatedListWrapper getWrapper(List<SObject> objs, RelatedListWrapper wrapper) {
        if (objs?.size() > 0) {
            for (SObject obj : objs) {
                RelatedListWrapper.RelatedListRecord r = new RelatedListWrapper.RelatedListRecord();
                r.id = obj.Id;
                for (Integer i = 0; i < wrapper.apiNames.size(); i++) {
                    RelatedListWrapper.RelatedListField f = new RelatedListWrapper.RelatedListField();
                    f.apiName = wrapper.apiNames[i];
                    f.value = obj.get(f.apiName);
                    f.label = wrapper.fieldLabels[i];
                    f.type = getType(wrapper, i, obj);
                    r.fields.add(f);
                }

                if (wrapper.childrenWrapper?.size() > 0) {
                    for (RelatedListWrapper childWrapper : wrapper.childrenWrapper) {
                        List<SObject> childObjs = obj.getSObjects(childWrapper.childRelationshipName);
                        RelatedListWrapper child = getWrapper(childObjs, (RelatedListWrapper) JSON.deserializeStrict(
                            JSON.serialize(childWrapper),
                            RelatedListWrapper.class
                        ));
                        r.children.add(child);
                    }
                }

                wrapper.records.add(r);
            }
        }

        return wrapper;
    }

    private static Map<String, Schema.SObjectField> getFields(String objApiName) {
        SObjectType r = ((SObject) (Type.forName('Schema.' + objApiName).newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        return d.fields.getMap();
    }

    private static Map<String, Object> getType(RelatedListWrapper wrapper, Integer i, SObject obj) {
        Map<String, Object> typeMap = new Map<String, Object> {
            'url' => new Map<String, Object> {
                'show' => false
            },
            'email' => new Map<String, Object> {
                'show' => false
            },
            'currency' => new Map<String, Object> {
                'show' => false
            },
            'percent' => new Map<String, Object> {
                'show' => false
            },
            'phone' => new Map<String, Object> {
                'show' => false
            },
            'date' => new Map<String, Object> {
                'show' => false
            },
            'datetime' => new Map<String, Object> {
                'show' => false
            },
            'boolean' => new Map<String, Object> {
                'show' => false
            },
            'text' => new Map<String, Object> {
                'show' => false
            }
        };
        Schema.DisplayType type = wrapper.types[i];
        String apiName = wrapper.apiNames[i];
        Object value = obj.get(apiName);

        switch on type {
            when URL {
                ((Map<String, Object>) typeMap.get('url')).put('show', true);
            }
            when EMAIL {
                ((Map<String, Object>) typeMap.get('email')).put('show', true);
            }
            when CURRENCY {
                ((Map<String, Object>) typeMap.get('currency')).put('show', true);
            }
            when PERCENT {
                ((Map<String, Object>) typeMap.get('percent')).put('show', true);
            }
            when PHONE {
                ((Map<String, Object>) typeMap.get('phone')).put('show', true);
            }
            when DATE {
                ((Map<String, Object>) typeMap.get('date')).put('show', true);
            }
            when DATETIME {
                ((Map<String, Object>) typeMap.get('datetime')).put('show', true);
            }
            when BOOLEAN {
                ((Map<String, Object>) typeMap.get('boolean')).put('show', true);
            }
            when STRING {
                String val = (String) value;
                if (val.contains('http')) {
                    ((Map<String, Object>) typeMap.get('url')).put('show', true);
                    //<a href="https://fmsadmin.svc.oanda.com/admin/users/" target="_blank">Find in FMS5</a>
                    val = val.replace('</a>', '');
                    List<String> l = val.split('>');
                    ((Map<String, Object>) typeMap.get('url')).put('label', l[1]);
                    ((Map<String, Object>) typeMap.get('url')).put('value', l[0].split('"')[1]);
                } else if (wrapper.linkToRecord[i]) {
                    ((Map<String, Object>) typeMap.get('url')).put('show', true);
                    ((Map<String, Object>) typeMap.get('url')).put('label', value);
                    ((Map<String, Object>) typeMap.get('url')).put('value',  '/lightning/r/' + wrapper.objApiName + '/' + obj.Id +'/view');
                }
                else {
                    ((Map<String, Object>) typeMap.get('text')).put('show', true);
                }
            }
            when else {
                ((Map<String, Object>) typeMap.get('text')).put('show', true);
            }
        }
        return typeMap;
    }

    public class RelatedListResponse {
        public String query { get; set; }

        public List<String> fields { get; set; }

        public RelatedListWrapper wrapper { get; set; }
    }
}