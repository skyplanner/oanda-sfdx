/**
 * Controller for page: ArticleList
 * @author Ariel Cantero
 * @since 6/1/2019
 */
global with sharing class ArticleListCtrl {

    @AuraEnabled
	public static Integer countArticles(ArticleListFilterCriteria criterias) {
		return new ArticleListHelper(criterias).count();
	}

	@AuraEnabled
	public static List<FAQ__kav> fetchArticles(
			ArticleListFilterCriteria criterias, 
			Integer pageSize, 
			Integer offset, 
			String sortBy, 
			Boolean isDescending) {
		//...
		return new ArticleListHelper(criterias, pageSize, offset, sortBy, isDescending).search();
	}

	@AuraEnabled
	public static List<DataCategoriesHelper.DataCategoryWrapper> getCategories() {
		return DataCategoriesHelper.getDataCategoriesByGroup(DataCategoriesConstants.ARTICLE_SOBJECT_NAME, DataCategoriesConstants.DAT_CAT_CATEGORIES);
	}

	@AuraEnabled
	public static List<DataCategoriesHelper.DataCategoryWrapper> getAudiences() {
		return DataCategoriesHelper.getDataCategoriesByGroup(DataCategoriesConstants.ARTICLE_SOBJECT_NAME, DataCategoriesConstants.DAT_CAT_AUDIENCE);
	}

	@AuraEnabled
	public static List<DataCategoriesHelper.DataCategoryWrapper> getDivisions() {
		return DataCategoriesHelper.getDataCategoriesByGroup(DataCategoriesConstants.ARTICLE_SOBJECT_NAME, DataCategoriesConstants.DAT_CAT_DIVISION);
	}
}