/**
 *
 */
@isTest
private class RoundRobinAssignmentTest {
	
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	final static Integer BATCH_SIZE = 100;
	final static Id NA_QUEUE_ID = UserUtil.getQueueId('Retail - Americas Leads');
	
	static testMethod void testCampaignMemberAddTasks() {
		
		Group queue = [Select Id from Group where type='Queue' and Name='Retail - EMEA Leads' limit 1]; 
		
		List<GroupMember> members = [Select UserOrGroupId From GroupMember where GroupId =: queue.id];
		
		
		List<ID> userIds = new List<ID>();
		for(GroupMember gMember : members){
			userIds.add(gMember.UserOrGroupId);
		}
		
	    //get active users
		List<User> activeUsers = [SELECT Id FROM User WHERE Id IN :userIds AND IsActive = true];
		System.debug('Total number of active queue users: ' +  members.size());
		
		Round_Robin_Queue__c queueSetting = new Round_Robin_Queue__c();
		queueSetting.name = queue.id;
		queueSetting.Last_Assigned_User_Id__c = activeUsers[0].id;
		
		insert queueSetting;
		
		
        Campaign c = new Campaign(Name='test campaign', Is_Auto_Create_Task__c=true);
        c.Task_Round_Robin_Queue_Name__c = 'Retail - EMEA Leads';
        insert c;
        
        List<Lead> leadList = new List<Lead>();
        for(Integer i=0; i<BATCH_SIZE; i++){
            Lead l = new Lead(LastName='test'+i, Email='test' + i + '@oanda.com');
            leadList.add(l);
        }
       
        insert leadList;
        
        test.startTest();
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        for(Integer i =0; i<BATCH_SIZE; i++ ){
        	CampaignMember cm = new CampaignMember(CampaignId=c.Id, LeadId=leadList.get(i).id);
        	campaignMembers.add(cm);
        }
        
        insert campaignMembers;
        test.stopTest();
        
        //System.assertEquals(BATCH_SIZE, [SELECT Id FROM Task WHERE WhoId=:l.Id].size());
    }
    
    static testMethod void testRoundRobinOperations(){
    	Group queue = [Select Id from Group where type='Queue' and Name='Retail - EMEA Leads' limit 1]; 
		
		List<GroupMember> members = [Select UserOrGroupId From GroupMember where GroupId =: queue.id];
		
		
		List<ID> userIds = new List<ID>();
		for(GroupMember gMember : members){
			userIds.add(gMember.UserOrGroupId);
		}
		
	    //get active users
		List<User> activeUsers = [SELECT Id FROM User WHERE Id IN :userIds AND IsActive = true];
		System.debug('Total number of active queue users: ' +  activeUsers.size());
		
		//put the first user into the custom setting
		Round_Robin_Queue__c queueSetting = new Round_Robin_Queue__c();
		queueSetting.name = queue.id;
		queueSetting.Last_Assigned_User_Id__c = activeUsers[0].id;
		insert queueSetting;
		
		test.startTest();
		
		List<ID> assignedUsers = new List<ID>();
		
		Integer activeUsersCount = activeUsers.size();
		
		for(Integer i =0; i<activeUsersCount; i++){ 
			assignedUsers.add(RoundRobinAssignment.getNextOwnerId(queue.id));
		}
		
		RoundRobinAssignment.commitAssignments();
		
		//check if a different user is assigned in each iteration
		boolean foundDoubleAssgin = false;
		for(Integer i=0; i<activeUsersCount; i++){
			
			for(Integer j=i+1; j< activeUsersCount; j++){
				if(assignedUsers[i] == assignedUsers[j]){
					foundDoubleAssgin = true;
					break;
				}
			}
			
			if(foundDoubleAssgin){
				break;
			}
		}
		
		System.assertEquals(false, foundDoubleAssgin);
		
		if(activeUsersCount > 1)
		{
			//This user id should be different from the initial user id
			ID tempId = RoundRobinAssignment.getNextOwnerId(queue.id);
			RoundRobinAssignment.commitAssignments();
			
			System.assertNotEquals(queueSetting.Last_Assigned_User_Id__c, tempId);
		}
		
		test.stopTest();
    }
    
    static testMethod void testMassLeadAssginment(){
    	final Integer NUMBER_OF_NA_LEADS = 200;
    	
    	Map<Id, Set<Id>> userIdsByQueueId = UserUtil.getQueueUsers();
    	
    	Set<Id> userIds = userIdsByQueueId.get(NA_QUEUE_ID);
    	
    	System.assert(userIds.size() > 0);
    	
    	List<Lead> toAssign = new List<Lead>();
    	for (Integer i = 0; i < NUMBER_OF_NA_LEADS; i++) {
    		Lead lead = new Lead();
    		lead.LastName = 'Schmoe';
    		lead.OwnerId = NA_QUEUE_ID;
    		lead.Email = 'randNA_' + i + '@example.org';
    		
    		toAssign.add(lead);
    	}
    	
    	insert toAssign;
    	
    	Set<Id> leadIds = new Set<Id>();
    	for (Lead lead : toAssign) {
    		leadIds.add(lead.Id);
    	}
    	
    	List<Lead> leadOwners = [SELECT OwnerId FROM Lead WHERE Id IN :leadIds];
    	
    	System.assertEquals(NUMBER_OF_NA_LEADS, leadOwners.size());
    }
    
    static testMethod void testAssignLead() {
    	
    	CustomSettings.setEnableLeadConversion(true);
    	CustomSettings.setEnableLeadMerging(true);
    	
    	insertFxtradeUser();
    	
    	List<fxAccount__c> fxAccounts = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c];
    	
    	for (fxAccount__c fxAccount : fxAccounts) {
    		fxAccount.Trigger_Lead_Assignment_Rules__c = true;
    	}
    	
    	test.startTest();
    	update fxAccounts;
    	
    	
    	
    	Database.executeBatch(new BatchTriggerLeadAssignment(), BATCH_SIZE);
    	test.stopTest();
    	
    	List<Lead> leads = [SELECT Id, OwnerId, IsConverted FROM Lead];
    	
    	System.assertEquals(BATCH_SIZE, leads.size());
    	
    	//for (Lead lead : leads) {
    	//	System.assert(SYSTEM_USER.Id != lead.OwnerId);
    	//}
    }
    
    private static void insertFxtradeUser() {
    	List<String> unameList = new List<String>();
    	
    	for(Integer i = 0; i < BATCH_SIZE; i++){
    		unameList.add('joe_' + i);
    	}
  
    	insertFxtradeUser(unameList);
    }
    
    private static void insertFxtradeUser(List<String> unameList) {
    	
    	Map<String, Lead> leadByUname = new Map<String, Lead>();
    	List<fxAccount__c> fxAccountList = new List<fxAccount__c>();
    	
    	
    	Lead lead;
    	System.runAs(SYSTEM_USER) {
    		
    		for(String uname : unameList){
    			
    			lead = new Lead();
	    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		lead.LastName = 'Schmoe';
	    		lead.Email = uname + '@example.org';
	    		lead.Territory__c = 'North America';
    			
    			leadByUname.put(uname, lead);
    		}
    		
    		
    		insert leadByUname.values();
    		
    		for(String uname : unameList){
    			
    			fxAccount__c fxAccount = new fxAccount__c();
	    		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		fxAccount.Name = uname;
	    		
	    		lead = leadByUname.get(uname);
	    		fxAccount.Lead__c = lead.Id;
    			
    			fxAccountList.add(fxAccount);
    		}
    		
    		
    		insert fxAccountList;
    		
    	}
		
    }

}