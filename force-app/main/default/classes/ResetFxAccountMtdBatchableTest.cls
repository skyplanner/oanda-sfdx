@isTest
public class ResetFxAccountMtdBatchableTest {

    @isTest
    static void test01(){
        Account a = new Account(LastName='test');
    	insert a;    	
        fxAccount__c f = new fxAccount__c(
            Name='test', 
            Trade_Volume_USD_MTD__c = 1,
            OANDA_P_L_USD_MTD__c = 1,
            P_and_L_USD_MTD__c = 1,
            Account__c = a.Id);
        insert f;

        Test.startTest();

            ResetFxAccountMtdBatchable.executeBatch();

        Test.stopTest();

        f = [
            SELECT 
                Id, 
                Trade_Volume_USD_MTD__c, 
                OANDA_P_L_USD_MTD__c, 
                P_and_L_USD_MTD__c 
            FROM fxAccount__c
                WHERE Id =: f.Id];

        System.assertEquals(0, f.Trade_Volume_USD_MTD__c);
        System.assertEquals(0, f.OANDA_P_L_USD_MTD__c);
        System.assertEquals(0, f.P_and_L_USD_MTD__c);
    }
}