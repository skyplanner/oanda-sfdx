/**
 * @File Name          : SPDataInitProxyImp.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/11/2023, 10:16:40 PM
**/
public inherited sharing class SPDataInitProxyImp implements SPDataInitProxy {
	
	final SPDataInitializer dataInit;
	final String objKey;
	final DescribeSObjectResult sObjectInfo;

	public SPDataInitProxyImp(
		SPDataInitializer dataInit,
		String objKey,
		DescribeSObjectResult sObjectInfo
	) {
		System.assert(dataInit != null);
		System.assert(String.isNotBlank(objKey));
		System.assert(sObjectInfo != null);

		this.dataInit = dataInit;
		this.objKey = objKey;
		this.sObjectInfo = sObjectInfo;
	}

	public String getObjectKey() {
		return objKey;
	}

	public String getUniqueName() {
		return dataInit.getUniqueName(sObjectInfo);
	}

	public Integer getUniqueIndex() {
		return dataInit.getUniqueIndex(sObjectInfo);
	}

	public ID insertObj(SObject newObject) {
		return dataInit.insertObj(newObject, objKey);
	}

	public ID getRecordTypeId(String recTypeDevName) {
		return dataInit.getRecordTypeId(sObjectInfo, recTypeDevName);
	}

	public void setRecordTypeId(String recTypeDevName) {
		dataInit.setRecordTypeId(objKey, sObjectInfo, recTypeDevName);
	}

	public void setFieldValue(
		Schema.SObjectField fieldToken,
		Object newValue
	) {
		dataInit.setFieldValue(objKey, fieldToken, newValue);
	}

	public void setFieldValues(Map<String,Object> newObjValues) {
		dataInit.setFieldValues(objKey, newObjValues);
	}

    public void setObjectOption(
		String optionName,
		Object newValue
	) {
        dataInit.setObjectOption(objKey, optionName, newValue);
    }

	public Object getObjectOption(
		String optionName,
		Object defaultValue
	) {
        return dataInit.getObjectOption(objKey, optionName, defaultValue);
    }

	public Object getFieldValue(
		Schema.SObjectField fieldToken,
		Boolean required
	) {
		return dataInit.getFieldValue(objKey, fieldToken, required);
	}

	public void checkRequiredFieldValue(Schema.SObjectField fieldToken) {
		dataInit.checkRequiredFieldValue(objKey, fieldToken);
	}
	
}