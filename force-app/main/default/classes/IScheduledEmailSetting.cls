/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
public interface IScheduledEmailSetting {
	Integer getScope();
	Date getDate();
	String getTime();
	String getAMOrPM();
	Boolean getAllowDelete();
	void setAllowDelete(Boolean value);
}