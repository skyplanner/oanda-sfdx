/**
 * Handles the validation and generation of Authentication Codes.
 * @author Fernando Gomez
 * @since 5/7/2021
 */
public without sharing class AuthenticationCodeGenerator {
	private final Integer numberOfDigits;
	private SObject record;
	private String codeFieldApiName;
	private String codeExpiresOnFieldApiName;

	AuthenticationCode code;

	/**
	 * Main construictor
	 * @param record
	 * @param codeFieldApiName
	 * @param codeExpiresOnFieldApiName
	 */
	public AuthenticationCodeGenerator(
			SObject record,
			String codeFieldApiName,
			String codeExpiresOnFieldApiName) {
		this(record, codeFieldApiName, codeExpiresOnFieldApiName, 6);
	}

	/**
	 * @param record
	 * @param codeFieldApiName
	 * @param codeExpiresOnFieldApiName
	 * @param numberOfDigits
	 */
	public AuthenticationCodeGenerator(
			SObject record,
			String codeFieldApiName,
			String codeExpiresOnFieldApiName,
			Integer numberOfDigits) {
		this.record = record;
		this.codeFieldApiName = codeFieldApiName;
		this.codeExpiresOnFieldApiName = codeExpiresOnFieldApiName;
		this.numberOfDigits = numberOfDigits;

		code = new AuthenticationCode(
			String.valueOf(record.get(codeFieldApiName)),
			Datetime.valueOf(record.get(codeExpiresOnFieldApiName)));
	}

	/**
	 * A code and expiration date is returned if
	 * there is currenty a valid code in the record.
	 * If createNewIfInvalid is false and there is no code or if the code
	 * has expired, NULL is returned.
	 * If createNewIfInvalid is true and there is no code or if the code
	 * has expired, a new code is generated, saved, and returned.
	 * @param createNewIfInvalid
	 * @return AuthenticationCode
	 * @see AuthenticationCode
	 */
	public AuthenticationCode getCode(Boolean createNewIfInvalid) {
		// we only the existing code if is valid
		if (code.isCodeValid())
			return code;

		// if we need to generate a new code, we do it
		// and return the new one
		if (createNewIfInvalid) {
			generateNewCodeAndSaveToRecord();
			return code;
		}

		return null;
	}

	/**
	 * A new code will be randomly generated and saved
	 * to the specified record.
	 * @param record
	 * @param codeFieldApiName
	 * @param codeExpiresOnFieldApiName
	 */
	public void generateNewCodeAndSaveToRecord() {
		generateNew();
		record.put(codeFieldApiName, code.code);
		record.put(codeExpiresOnFieldApiName, code.expiresOn);
		update record;
	}

	/**
	 * A new code will be randomly generated and saved
	 * to the specified record.
	 * @return the same object with the new fields populated
	 * @param record
	 * @param codeFieldApiName
	 * @param codeExpiresOnFieldApiName
	 */
	@AuraEnabled
	public static SObject generateAuthenticationCodeAndSaveToRecord(
			SObject record, String codeFieldApiName,
			String codeExpiresOnFieldApiName) {
		AuthenticationCodeGenerator generator;
		try {
			generator = new AuthenticationCodeGenerator(
				record, codeFieldApiName, codeExpiresOnFieldApiName);
			generator.generateNewCodeAndSaveToRecord();
			return record;
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @return true if the code is valid for the record
	 * @param record
	 * @param codeFieldApiName
	 * @param codeExpiresOnFieldApiName
	 */
	@AuraEnabled
	public static Boolean validateAuthenticationCode(
			String codeToValidate,
			Id recordId,
			String codeFieldApiName,
			String codeExpiresOnFieldApiName) {
		AuthenticationCodeGenerator generator;
		DateTime nowDt;
		String objectName;

		try {
			nowDt = System.now();
			objectName = recordId.getSobjectType().getDescribe().getName();

			return Database.countQuery(
				String.format(
					'SELECT COUNT() ' +
					'FROM {0} ' +
					'WHERE Id = :recordId ' +
					'AND {1} = :codeToValidate ' +
					'AND {2} >= :nowDt',
					new List<String> {
						String.escapeSingleQuotes(objectName),
						String.escapeSingleQuotes(codeFieldApiName),
						String.escapeSingleQuotes(codeExpiresOnFieldApiName)
					}
				)
			) > 0;
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * Creates and saves a new code
	 */
	private void generateNew() {
		code = new AuthenticationCode(
			// new numeric code such as 009878, 124300...
			String.valueOf(
				Math.floor(
					Math.random() *
					Math.pow(10, numberOfDigits)
				).intValue()
			).leftPad(numberOfDigits, '0'),
			// expiration date is in one daye from now
			getNewExpirationDate()
		);
	}

	/**
	 * @return the datetime a new code should expire
	 */
	private DateTime getNewExpirationDate() {
		String expireInMinutes = SPGeneralSettings.getInstance().getValue(
			'SP_Authenticatio_Code_Expires_In_Minutes');
		return System.now().addMinutes(
			expireInMinutes != null && expireInMinutes.isNumeric() ?
			Integer.valueOf(expireInMinutes) :
			15
		);
	}
}