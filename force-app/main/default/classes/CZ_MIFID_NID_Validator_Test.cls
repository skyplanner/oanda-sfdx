/**
 * @File Name          : CZ_MIFID_NID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/10/2020, 4:19:34 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/5/2020   acantero     Initial Version
**/
@istest
private class CZ_MIFID_NID_Validator_Test {

    @istest
    static void validate_test() {
        Test.startTest();
        CZ_MIFID_NID_Validator validator = new CZ_MIFID_NID_Validator();
        String result1 = validator.validate(null); //nothing to validate
        String result2 = validator.validate('123456/789'); //no validable lenght
        String result3 = validator.validate('1//////////'); //invalid lenght after replace /
        String result4 = validator.validate('A23456/7890'); //invalid value (not number)
        String result5 = validator.validate('999999/9990'); //invalid check digit
        String result6 = validator.validate('999999/9999'); //valid
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(System.Label.MifidValPasspFormatError, result3);
        System.assertEquals(System.Label.MifidValPasspFormatError, result4);
        System.assertEquals(System.Label.MifidValPasspChecksumError, result5);
        System.assertEquals(null, result6);

    }

}