/**
 * @File Name          : GetUserAccountIdActionTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 1:04:37 PM
**/
@IsTest
private without sharing class GetUserAccountIdActionTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		initManager.storeData();
	}

	// exception
	@IsTest
	static void getUserAccountId1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Boolean error = false;
		List<String> userEmailList = new List<String>{ 'fakeemail@a.com' };
		
		Test.startTest();
		ExceptionTestUtil.prepareDummyException();
		
		try {
			List<ID> result = GetUserAccountIdAction.getUserAccountId(userEmailList);
		} catch(Exception ex) {
			error = true;
		}
		
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}    

	// test 1 : valid email => result[0] = accountId
	// test 2 : invalid email => result[0] = null
	// test 3 : userEmailList = null => result[0] = null
	// test 4 : userEmailList is empty => result[0] = null
	@IsTest
	static void getUserAccountId2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID account1Id = 
			initManager.getObjectId(ServiceTestDataKeys.ACCOUNT_1, true);
		List<String> userEmailList1 = 
			new List<String>{ ServiceTestDataKeys.ACCOUNT1_EMAIL };
		List<String> userEmailList2 = 
			new List<String>{ 'superfakeemail@a.com' };

		Test.startTest();
		List<ID> result1 = 
			GetUserAccountIdAction.getUserAccountId(userEmailList1);
		List<ID> result2 = 
			GetUserAccountIdAction.getUserAccountId(userEmailList2);
		List<ID> result3 = 
			GetUserAccountIdAction.getUserAccountId(null);
		List<ID> result4 = 
			GetUserAccountIdAction.getUserAccountId(new List<String>());
		Test.stopTest();
		
		Assert.areEqual(account1Id, result1[0], 'Invalid result');
		Assert.isNull(result2[0], 'Invalid result');
		Assert.isNull(result3[0], 'Invalid result');
		Assert.isNull(result4[0], 'Invalid result');
	}    
	
}