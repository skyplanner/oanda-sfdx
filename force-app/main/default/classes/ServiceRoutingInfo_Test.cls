/**
 * @File Name          : ServiceRoutingInfo_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 5:26:17 PM
**/
@isTest
private class ServiceRoutingInfo_Test {

	@isTest
	static void test() {
		Test.startTest();
		ServiceRoutingInfo instance = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		Boolean booleanResult = instance.infoIsIncomplete;
		String stringResult = instance.email;
		stringResult = instance.language;
		stringResult = instance.alternativeLanguage;
		stringResult = instance.accountType;
		OmnichannelCustomerConst.CustomerType aCustomerType = 
			instance.relatedCustomerType;
		stringResult = instance.subtype;
		booleanResult = instance.isAComplaint;
		booleanResult = instance.deposit;
		booleanResult = instance.trade;
		booleanResult = instance.isOELDivision;
		booleanResult = instance.crypto;
		stringResult = instance.priority;
		booleanResult = instance.ccmCx;
		booleanResult = instance.dropAdditionalSkills;
		SObject aWorkItem = instance.workItem;
		Account aRelatedAccount = instance.relatedAccount;
		Lead aRelatedLead = instance.relatedLead;
		booleanResult = instance.relatedToAnAccount;
		booleanResult = instance.relatedToALead;
		booleanResult = instance.createdFromChat;
		booleanResult = instance.createdFromCase;
		booleanResult = instance.createdFromMessagingChat;
		booleanResult = instance.isHVC;
		booleanResult = instance.isNewCustomer;
		booleanResult = instance.isActiveCustomer;
		booleanResult = instance.isCustomer;
		booleanResult = instance.isPracticeAccount;
		booleanResult = instance.isLiveAccount;
		Test.stopTest();
		Assert.isNotNull(instance, 'Invalid Result');
	}

	@isTest
	static void trade() {
		Test.startTest();
		ServiceRoutingInfo instance = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		instance.inquiryNature = OmnichanelConst.INQUIRY_NAT_TRADE;
		Boolean trade = instance.trade;
		Test.stopTest();
		Assert.isTrue(trade, 'Invalid Result');
	}

	@isTest
	static void deposit() {
		Test.startTest();
		ServiceRoutingInfo instance = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		instance.inquiryNature = 
			OmnichanelConst.INQUIRY_NAT_FUNDING_WITHDRAWAL;
		instance.type = OmnichanelConst.TYPE_DEPOSIT;
		Boolean deposit = instance.deposit;
		Test.stopTest();
		Assert.isTrue(deposit, 'Invalid Result');
	}

	@isTest
	static void isOELDivision() {
		Test.startTest();
		ServiceRoutingInfo instance = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		instance.division = OmnichanelConst.OANDA_EUROPE;
		Boolean isOELDivision = instance.isOELDivision;
		Test.stopTest();
		Assert.isTrue(isOELDivision, 'Invalid Result');
	}

	// test 1 : createdFromChat and live
	// test 2 : createdFromChat and practice
	// test 3 : createdFromCase and relatedToALead
	// test 4 : createdFromCase and relatedToAnAccount
	@isTest
	static void checkAccountType() {
		Test.startTest();
		// test 1
		ServiceRoutingInfo instance1 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CHAT, // infoSource
			null // workItem
		);
		instance1.accountType = OmnichanelConst.LIVE;
		Boolean live1 = instance1.isLiveAccount;
		// test 2
		ServiceRoutingInfo instance2 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CHAT, // infoSource
			null // workItem
		);
		instance2.accountType = OmnichanelConst.PRACTICE;
		Boolean practice2 = instance2.isPracticeAccount;
		// test 3
		ServiceRoutingInfo instance3 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		instance3.setRelatedLead(new Lead());
		Boolean practice3 = instance3.isPracticeAccount;
		// test 4
		ServiceRoutingInfo instance4 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		instance4.setRelatedAccount(new Account());
		Boolean live4 = instance4.isLiveAccount;
		Test.stopTest();

		Assert.isTrue(live1, 'Invalid Result');
		Assert.isTrue(practice2, 'Invalid Result');
		Assert.isTrue(practice3, 'Invalid Result');
		Assert.isTrue(live4, 'Invalid Result');
	}
	
}