/**
 * @File Name          : FxAccountValidationError.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2020, 4:53:26 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/24/2020   acantero     Initial Version
**/
public class FxAccountValidationError {

    public final fxAccount__c fxAccountObj;
    public final String subject;
    public final String error;

    public FxAccountValidationError(
        fxAccount__c fxAccountObj, 
        String subject,
        String error
    ) {
        this.fxAccountObj = fxAccountObj;
        this.subject = subject;
        this.error = error;
    }
}