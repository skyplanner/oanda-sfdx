/**
 * @File Name          : RegisterRoutingSkillCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/29/2024, 4:38:08 PM
**/
@IsTest
private without sharing class RegisterRoutingSkillCmdTest {

	// test 1 : skillName is blank
	// test 2 : skillName is not blank, the skill is added for the first time
	// test 3 : skillName is not blank, the skill is added again
	@IsTest
	static void registerSkill1() {
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		); 
		
		Test.startTest();
		RegisterRoutingSkillCmd instance = 
			new RegisterRoutingSkillCmd(sRoutingInfo);
		// test 1 
		SSkillRequirementInfo result1 = instance.registerSkill(null, false);
		// test 2
		SSkillRequirementInfo result2 = instance.registerSkill(
			OmnichanelConst.LOGIN_SKILL, // skillName
			false // isAditional
		);
		// test 3
		SSkillRequirementInfo result3 = instance.registerSkill(
			OmnichanelConst.LOGIN_SKILL, // skillName
			true // isAditional
		);
		Test.stopTest();
		
		Assert.isNull(result1, 'Invalid result');
		Assert.isNotNull(result2, 'Invalid result');
		Assert.isNull(result3, 'Invalid result');
	}

	// test 1 : skillInfo = null
	// test 2 : skillInfo is valid
	// test 3 : skillInfo is valid, the skill was already added before
	@IsTest
	static void registerSkill2() {
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		); 
		SSkillRequirementInfo skillReqInfo = new SSkillRequirementInfo(
			OmnichanelConst.LOGIN_SKILL, // skillName
			true // isAdditionalSkill
		);
		
		Test.startTest();
		RegisterRoutingSkillCmd instance = 
			new RegisterRoutingSkillCmd(sRoutingInfo);
		// test 1 
		Boolean result1 = instance.registerSkill(null);
		// test 2
		Boolean result2 = instance.registerSkill(skillReqInfo);
		// test 3
		Boolean result3 = instance.registerSkill(skillReqInfo);
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
	}
	
}