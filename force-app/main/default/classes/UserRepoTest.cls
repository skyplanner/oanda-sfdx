/**
 * @File Name          : UserRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/10/2024, 3:13:19 PM
**/
@IsTest
private without sharing class UserRepoTest {

	@IsTest
	static void getSummaryByName() {
		User result = null;

		Test.startTest();
		System.runAs(new User(Id = UserInfo.getUserId())) {
			String name = UserInfo.getName();
			result = UserRepo.getSummaryByName(name);
		}
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	@IsTest
	static void getSummaryForStandardUsers() {
		List<User> result = null;

		Test.startTest();
		System.runAs(SPTestUtil.createUser('u0009')) {
			Set<ID> userIds = new Set<ID> { UserInfo.getUserId() };
			result = UserRepo.getSummaryForStandardUsers(userIds);
		}
		Test.stopTest();
		
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}
	
}