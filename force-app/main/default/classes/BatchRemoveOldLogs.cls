/**
 * @Description  : Batch witch scheduler. Created to remove old logs.
 * @Author       : Jakub Fik
 * @Date         : 2024-04-04
**/
public with sharing class BatchRemoveOldLogs implements Database.Batchable<SObject>, Schedulable {
	private static final String QUERY_BEGINING = 'SELECT Id FROM ';
	private static final String QUERY_MIDDLE = ' WHERE CreatedDate < ';

    private final String QUERY;

    // Constructor
    public BatchRemoveOldLogs(String objectName, Integer monthsAgo) {
        this.QUERY = createQuery(objectName, monthsAgo);
    }

    // Batchable.start
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.QUERY);
    }

    // Batchable.execute
    public void execute(Database.BatchableContext bc, List<SObject> records) {
        delete records;
    }

    // Batchable.finish
    public void finish(Database.BatchableContext bc) {
    }

    // Schedulable.execute
    public void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }

    /**
    * @description Query creator. Creates query depends on Object api name and Integer (how old logs should be removed).
    * @author Jakub Fik | 2024-04-04 
    * @param objectName 
    * @param monthsAgo 
    * @return String 
    **/
    private static String createQuery(String objectName, Integer monthsAgo) {
        Integer negativeValue = -1 * monthsAgo;
        DateTime dateTillRemove = DateTime.now().addMonths(negativeValue);
        return QUERY_BEGINING +
            objectName +
            QUERY_MIDDLE +
            SoqlUtil.getSoql(dateTillRemove);
    }
}