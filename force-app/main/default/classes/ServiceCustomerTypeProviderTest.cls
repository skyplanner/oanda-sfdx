/**
 * @File Name          : ServiceCustomerTypeProviderTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/2/2024, 3:02:04 PM
**/
@IsTest
private without sharing class ServiceCustomerTypeProviderTest {

	// test 1 : HVC (related to Account)
	// test 2 : NEW_CUSTOMER (related to Account)
	// test 3 : ACTIVE (related to Account)
	// test 4 : CUSTOMER (related to Account)
	// test 5 : NEW_CUSTOMER (related to Lead)
	// test 6 : CUSTOMER (related to Lead)
	@isTest
	static void getCustomerType() {
		ServiceCustomerTypeProvider instance = 
			new ServiceCustomerTypeProvider();
		String accountId = 'fake Id';

		Test.startTest();
		// test 1
		SCustomerTypeInfo customerInfo1 = new SCustomerTypeInfo();
		customerInfo1.accountId = accountId;
		customerInfo1.isHVC = true;
		OmnichannelCustomerConst.CustomerType result1 = 
			instance.getCustomerType(customerInfo1);
		// test 2
		SCustomerTypeInfo customerInfo2 = new SCustomerTypeInfo();
		customerInfo2.accountId = accountId;
		customerInfo2.funnelStage = 'Legal';
		customerInfo2.createdDate = Datetime.now();
		OmnichannelCustomerConst.CustomerType result2 = 
			instance.getCustomerType(customerInfo2);
		// test 3
		SCustomerTypeInfo customerInfo3 = new SCustomerTypeInfo();
		customerInfo3.accountId = accountId;
		customerInfo3.funnelStage = OmnichanelConst.FUNNEL_STAGE_TRADED;
		customerInfo3.lastTradeDate = Datetime.now();
		OmnichannelCustomerConst.CustomerType result3 = 
			instance.getCustomerType(customerInfo3);
		// test 4
		SCustomerTypeInfo customerInfo4 = new SCustomerTypeInfo();
		customerInfo4.accountId = accountId;
		customerInfo4.funnelStage = OmnichanelConst.FUNNEL_STAGE_TRADED;
		customerInfo4.lastTradeDate = Date.today().addMonths(-7);
		OmnichannelCustomerConst.CustomerType result4 = 
			instance.getCustomerType(customerInfo4);
		// test 5
		SCustomerTypeInfo customerInfo5 = new SCustomerTypeInfo();
		customerInfo5.isALead = true;
		customerInfo5.createdDate = Date.today();
		OmnichannelCustomerConst.CustomerType result5 = 
			instance.getCustomerType(customerInfo5);
		// test 6
		SCustomerTypeInfo customerInfo6 = new SCustomerTypeInfo();
		customerInfo6.isALead = true;
		customerInfo6.createdDate = Date.today().addDays(-91);
		OmnichannelCustomerConst.CustomerType result6 = 
			instance.getCustomerType(customerInfo6);
		Test.stopTest();

		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.HVC, 
			result1, 
			'Invalid Result'
		);
		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER, 
			result2, 
			'Invalid Result'
		);
		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.ACTIVE, 
			result3, 
			'Invalid Result'
		);
		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.CUSTOMER, 
			result4, 
			'Invalid Result'
		);
		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER, 
			result5, 
			'Invalid Result'
		);
		Assert.areEqual(
			OmnichannelCustomerConst.CustomerType.CUSTOMER, 
			result6, 
			'Invalid Result'
		);
	}
	
}