/**
 * @File Name          : InternalErrorManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/20/2021, 4:02:22 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/27/2020   acantero     Initial Version
**/
public class InternalErrorManager {

    public static final Integer TEXT_MAX_LENGTH = 255;

    public static final Integer LONG_TEXT_MAX_LENGTH = 131072;
    
    public static void safeLogException(String category, Exception ex) {
        if (ex == null) {
            return;
        }
        //else...
        try {
            String exMsg = getValidText(ex.getMessage(), TEXT_MAX_LENGTH);
            String stackTraceString = getSafeStackTraceString(ex);
            System.debug('stackTraceString: ' + stackTraceString);
            String exDetail = getValidText(stackTraceString, LONG_TEXT_MAX_LENGTH);
            if (String.isNotBlank(exMsg)) {
                insert new Internal_Error__c(
                    Category__c = category,
                    Error__c = exMsg,
                    Detail__c = exDetail
                ); 
            }
        } catch (Exception ex2) {
            //silence inner exception
            System.debug(LoggingLevel.ERROR, ex2.getMessage());
        } 
    }

    @testVisible
    static String getValidText(String text, Integer maxLenght) {
        String result = ((text != null) && (text.length() > maxLenght))
            ? text.substring(0, maxLenght)
            : text;
        return result;
    }

    static String getSafeStackTraceString(Exception source) {
        String result = null;
        try {
            result = source.getStackTraceString();
            //...
        } catch (Exception ex) {
        }
        return result;
    }

}