/**
 * @File Name          : FAQSettingsManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/4/2019, 3:37:49 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/18/2019   acantero     Initial Version
**/
global without sharing class FAQSettingsManager {

	public static Integer DEFAULT_CAT_TOP_ART_COUNT = 5;
	public static final String DEFAULT_SETTING_NAME = 'DefaultSettings';
	public static Boolean DEFAULT_RESTRICT_TOP_ART = false;

    private static FAQSettingsManager instance = null;

    global Integer categoryTopArticlesCount {get; private set;}
	@TestVisible
    global List<String> faqCategories {get; private set;}
    global Boolean restrictTopArticles {get; private set;}
    global String commonDivision {get; private set;}

    private FAQSettingsManager(String settName) {
    	List<FAQ_Setting__mdt> settings = getSettings(settName);
    	init(settings);
    }

	global static FAQSettingsManager getInstance() {
		if (instance == null) {
			instance = new FAQSettingsManager(DEFAULT_SETTING_NAME);
		}
		return instance;
	}

	global static FAQSettingsManager getInstance(String settName) {
		if (instance == null) {
			String validName = String.isBlank(settName) ? DEFAULT_SETTING_NAME : settName;
			instance = new FAQSettingsManager(validName);
		}
		return instance;
	}

	@TestVisible
	private void init(List<FAQ_Setting__mdt> settings) {
		if (settings.isEmpty()) {
			categoryTopArticlesCount = DEFAULT_CAT_TOP_ART_COUNT;
			faqCategories = new List<String>();
			restrictTopArticles = DEFAULT_RESTRICT_TOP_ART;
		} else {
			categoryTopArticlesCount = (settings[0].Category_Top_Articles_Count__c != null) 
				? settings[0].Category_Top_Articles_Count__c.intValue()
				: DEFAULT_CAT_TOP_ART_COUNT;
			if (settings[0].FAQ_Categories__c != null) {
				faqCategories = settings[0].FAQ_Categories__c.split(',');
				Integer length = faqCategories.size();
				for(Integer i = 0 ; i < length; i++) {
					faqCategories[i] = faqCategories[i].trim();
				}
			} else {
				faqCategories = new List<String>();
			}
			restrictTopArticles = settings[0].Restrict_Top_Articles__c;
			commonDivision = settings[0].Common_Division__c;
		}
	}

	@TestVisible
	private static List<FAQ_Setting__mdt> getSettings(String settName) {
		List<FAQ_Setting__mdt> settings = [
			SELECT 
				Category_Top_Articles_Count__c, 
				FAQ_Categories__c, 
				Restrict_Top_Articles__c,
				Common_Division__c
			FROM 
				FAQ_Setting__mdt 
			WHERE 
				QualifiedApiName = :settName
			LIMIT 1
		];
		return settings;
	}

}