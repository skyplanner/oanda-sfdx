/**
 * Custom Lookup componenent to be used in any custom form.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0
 * @since 8/6/2021
 */
public class LookupCtrl {
	private String objectName;
	private List<String> searchInFields;
	private final String QUERY;

	private List<String> filters;

	/**
	 * Main constructor
	 * @param keyword
	 * @param objectName
	 * @param searchInFields
	 */
	private LookupCtrl(String objectName,
			List<String> searchInFields,
			Boolean isGetRecord) {
		List<String> selectFields, filters;
		Set<String> mandatoryFields;
		String escapedSearchField;

		this.objectName = objectName;
		this.searchInFields = searchInFields;

		// we need filters
		mandatoryFields = new Set<String> { 'Name' };
		selectFields = new List<String>();
		selectFields.addAll(mandatoryFields);
		filters = new List<String>();

		if (isGetRecord)
			filters.add('Id = :recordId');
		else
			for (String searchField : searchInFields) {
				escapedSearchField = String.escapeSingleQuotes(searchField);

				// we add the field to the select clause
				if (!mandatoryFields.contains(escapedSearchField))
					selectFields.add(escapedSearchField);

				// the search fields must be compared against the keyword
				filters.add(escapedSearchField + ' LIKE :keywordClause');
			}

		QUERY = String.format(
			'SELECT {0} FROM {1} WHERE {2} ORDER BY {3} LIMIT 100',
			new List<String> {
				// fields, always Id and name
				String.join(selectFields, ', '),
				// object api name
				String.escapeSingleQuotes(objectName),
				// filters
				String.join(filters, ' OR '),
				// ordering
				String.join(selectFields, ', ')
			});
	}

	/**
	 * @param keyword
	 * @return the resulty of the search by the current keyword
	 */
	private List<SObject> search(String keyword) {
		String keywordClause = '%' + String.escapeSingleQuotes(keyword) + '%';
		return Database.query(QUERY);
	}

	/**
	 * @param recordId
	 * @return the resulty of the search by the current keyword
	 */
	private SObject getCurrent(Id recordId) {
		return Database.query(QUERY);
	}

	/**
	 * @param keyword
	 * @param objectName
	 * @param searchInFields
	 */
	@AuraEnabled(cacheable=true)
	public static List<SObject> doSearch(
			String keyword, 
			String objectName,
			List<String> searchInFields) {
		try {
			LookupCtrl ctrl = new LookupCtrl(objectName, searchInFields, false);
			return ctrl.search(keyword);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage() + '\n' +
				e.getStackTraceString());
		}
	}

	/**
	 * @param recordId
	 * @param objectName
	 * @param searchInFields
	 */
	@AuraEnabled(cacheable=true)
	public static SObject getCurrentRecord(
			String recordId, 
			String objectName,
			List<String> searchInFields) {
		try {
			LookupCtrl ctrl = new LookupCtrl(objectName, searchInFields, true);
			return ctrl.getCurrent(recordId);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage() + '\n' +
				e.getStackTraceString());
		}
	}
}