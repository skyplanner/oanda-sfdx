/* Name: InternalCloseRestResourceTest
 * Description : Test Class to for internal close API
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 July 09
 */

 @isTest
 public class CustomerRiskAssesmentRestResourceTest {
     
   public static final string REQUEST_BODY = '{\"division\":\"OANDA Europe Markets\",\"employmentStatus\":\"Employed\",\"industryofEmployment\":\"Financial\",\"countryOfResidence\":\"Germany\",\"citizenshipNationality\":\"United Kingdom\",\"incomeSourceDetails\":\"Employment Income\"}';
   
   public static final string REQUEST_URI = '/api/v1/customer/riskassessment';
 
   @isTest
    public static void testDoPostSuccess() {
      
        RestContext.request = createRequest(REQUEST_BODY);
        RestContext.response = new RestResponse();

        Test.startTest();
        CustomerRiskAssesmentRestResource.doPost();
        Test.stopTest();

        //assert success 
        Assert.areEqual(200, RestContext.response.statusCode, 'Should return risk value succesfuly');
    }

    @isTest
    public static void testDoPostFail() {
      
        RestContext.request = createRequest('');
        RestContext.response = new RestResponse();

        Test.startTest();
        CustomerRiskAssesmentRestResource.doPost();
        Test.stopTest();

        //assert fail 
        Assert.areNotEqual(200, RestContext.response.statusCode, 'Should not return risk value succesfuly');
    }

    private static RestRequest createRequest(String requestBody) {
        RestRequest request = new RestRequest();

        request.requestUri = REQUEST_URI;
        request.resourcePath = REQUEST_URI;
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(requestBody);

        return request;
    }
}