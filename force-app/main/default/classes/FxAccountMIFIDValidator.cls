/**
 * @File Name          : FxAccountMIFIDValidator.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/27/2020, 1:34:28 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   acantero     Initial Version
**/
public without sharing class FxAccountMIFIDValidator {

    List<fxAccount__c> fxAccList;
    final Boolean updateExistingOpenCases;
    final Map<String,ExpressionValidationWrapper> totalExpValMap;

    List<ID> fxAccountsWithMiFIDCaseOpen;
    Map<ID,FxAccountValidationError> errorMap;
    
    List<ID> validFxAccountsIdList;

    Map<Id, MiFID_Validation_Result__c> fxAccountResultMap;
    List<MiFID_Validation_Result__c> toUpdate;

    ExpressionValidationManager expValManager;
    ExpressionsManager expressionsMan;

    static MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();

    public FxAccountMIFIDValidator(
        List<fxAccount__c> fxAccList,
        Boolean updateExistingOpenCases
    ) {
        this(
            fxAccList, 
            updateExistingOpenCases,
            null //totalExpValMap
        );
    }

    public FxAccountMIFIDValidator(
        List<fxAccount__c> fxAccList,
        Boolean updateExistingOpenCases,
        Map<String,ExpressionValidationWrapper> totalExpValMap
    ) {
        
        this.fxAccList = fxAccList;
        this.updateExistingOpenCases = (updateExistingOpenCases == true);
        this.expressionsMan = new ExpressionsManager(totalExpValMap);
    }

    public Map<ID,FxAccountValidationError> validateOnly() {
        if ((fxAccList == null) || fxAccList.isEmpty()) {
            return null;
        }
        //else...
        if (updateExistingOpenCases == false) {
            fxAccountsWithMiFIDCaseOpen = 
                MiFIDValCasesHelper.getFxAccountsWithMiFIDCaseOpen(fxAccList);
        }
        generateFxAccountResultMap();
        toUpdate = new List<MiFID_Validation_Result__c>();
        errorMap = new Map<ID,FxAccountValidationError>();
        validFxAccountsIdList = new List<ID>();
        expValManager = new ExpressionValidationManager();
        for(fxAccount__c fxAccountObj : fxAccList) {    
            MiFID_Validation_Result__c  fxAccountValidResult = fxAccountResultMap.get(fxAccountObj.Id);
            if (String.isBlank(fxAccountObj.Citizenship_Nationality__c)) {
                handleBlankCitizenshipNationality(fxAccountObj, fxAccountValidResult);
                continue;
            }
            //else...
            Map<String,ExpressionValidationWrapper> validExpValMap = 
                expressionsMan.getExpValMap(fxAccountObj.Citizenship_Nationality__c);
            System.debug('validExpValMap: ' + validExpValMap);
            if (
                (updateExistingOpenCases == false) &&
                (fxAccountValidResult != null)
            ) {
                Boolean expValChanged = 
                    FxAccountMIFIDHelper.expValHasChanged(
                        fxAccountValidResult, 
                        validExpValMap
                    );
                if (notNeedValidation(fxAccountObj, expValChanged, fxAccountValidResult)) {
                    continue;
                }
            }
            //else...
            Boolean isValid = validateFxAccount(fxAccountObj, validExpValMap);
            System.debug('fxAccountObj.Name: ' + fxAccountObj.Name + ', isValid: ' + isValid);
            
        }
        return errorMap;
    }

    public void validate() {
        if ((fxAccList == null) || fxAccList.isEmpty()) {
            return;
        }
        //else...
        if (updateExistingOpenCases == false) {
            fxAccountsWithMiFIDCaseOpen = 
                MiFIDValCasesHelper.getFxAccountsWithMiFIDCaseOpen(fxAccList);
        }
        generateFxAccountResultMap();
        toUpdate = new List<MiFID_Validation_Result__c>();
        errorMap = new Map<ID,FxAccountValidationError>();
        validFxAccountsIdList = new List<ID>();
        expValManager = new ExpressionValidationManager();
        for(fxAccount__c fxAccountObj : fxAccList) {    
            MiFID_Validation_Result__c  fxAccountValidResult = fxAccountResultMap.get(fxAccountObj.Id);
            if (String.isBlank(fxAccountObj.Citizenship_Nationality__c)) {
                handleBlankCitizenshipNationality(fxAccountObj, fxAccountValidResult);
                continue;
            }
            //else...
            Map<String,ExpressionValidationWrapper> validExpValMap = 
                expressionsMan.getExpValMap(fxAccountObj.Citizenship_Nationality__c);
                
            if (
                (updateExistingOpenCases == false) &&
                (fxAccountValidResult != null)
            ) {
                Boolean expValChanged = 
                    FxAccountMIFIDHelper.expValHasChanged(
                        fxAccountValidResult, 
                        validExpValMap
                    );
                if (notNeedValidation(fxAccountObj, expValChanged, fxAccountValidResult)) {
                    continue;
                }
            }
            //else...
            Boolean isValid = validateFxAccount(fxAccountObj, validExpValMap);
            System.debug('fxAccountObj.Name: ' + fxAccountObj.Name + ', isValid: ' + isValid);
            setMiFIDValidated(fxAccountObj, isValid, fxAccountValidResult);
            if (isValid) {
                validFxAccountsIdList.add(fxAccountObj.Id);
            }
        }
        MiFIDValCasesHelper casesHelper = 
            new MiFIDValCasesHelper(
                errorMap,
                validFxAccountsIdList
            );
        casesHelper.createOrUpdateCases();
        if (!toUpdate.isEmpty()) {        
            upsert toUpdate;
        }
    }

    @testVisible
    void handleBlankCitizenshipNationality(fxAccount__c fxAccountObj, MiFID_Validation_Result__c fxAccountValidResult) {
        Boolean errorMustBeReported = reportError(
            fxAccountObj, 
            System.Label.MifidValCitNatEmpty,
            System.Label.MifidValCitNatMissingError,
            false
        );
        Boolean fxAccountValidated = (fxAccountValidResult != null) 
            ? fxAccountValidResult.Valid__c 
            : false;
        if (
            errorMustBeReported ||
            (fxAccountValidated == true)
        ) {
            setMiFIDValidated(fxAccountObj, false, fxAccountValidResult);
        }
    }

    @testVisible
    Boolean notNeedValidation(
        fxAccount__c fxAccountObj, 
        Boolean expValChanged,
        MiFID_Validation_Result__c fxAccountValidResult
    ) {
        Boolean result = false;
        
        Boolean fxAccountValidated = fxAccountValidResult != null ? 
                                     fxAccountValidResult.Valid__c :
                                     false;
        if (
            (            
                (fxAccountValidated == true) &&
                (expValChanged == false)
            ) ||
            (           
                (fxAccountValidated == false) &&
                (fxAccountsWithMiFIDCaseOpen != null) &&
                fxAccountsWithMiFIDCaseOpen.contains(fxAccountObj.Id)
            )
        ) {
            result = true;
        }
        return result;
    }

    @testVisible
    void setMiFIDValidated(
        fxAccount__c fxAccountObj, 
        Boolean newVal,   
        MiFID_Validation_Result__c fxAccountValidResult
    ) {
        MiFID_Validation_Result__c fxAccountResult = (fxAccountValidResult != null) 
            ? fxAccountValidResult 
            : new MiFID_Validation_Result__c(
                fxAccount__c = fxAccountObj.Id
            );
        fxAccountResult.Valid__c = newVal;
        toUpdate.add(fxAccountResult);  
    }

    @testVisible
    Boolean validateFxAccount(
        fxAccount__c fxAccountObj,
        Map<String,ExpressionValidationWrapper> expValMap
    ) {
        String error  = null;
        String subject = null;
        Boolean errorIsConditional = null;
        for(String key : expValMap.keySet()) {
            ExpressionValidationWrapper expVal = expValMap.get(key);
            String fieldValue;
            MiFIDExpValConst.MiFIDValType valType;
            if (key.endsWith(valSettings.natPersIDSuffix)) {
                valType = MiFIDExpValConst.MiFIDValType.NATIONAL_ID;
                fieldValue = fxAccountObj.National_Personal_ID__c;
                //...
            } else if (key.endsWith(valSettings.passportSuffix)) {
                valType = MiFIDExpValConst.MiFIDValType.PASSPORT;
                fieldValue = fxAccountObj.Passport_Number__c;
            }
            ExpressionValidationManager.ExpValError valError = 
                expValManager.validateExpression(
                    fieldValue, 
                    expVal
                );
            Boolean existsPreviousError = (error != null);
            if (valError != null) {
                error = 
                    FxAccountMIFIDHelper.addErrorMessage(
                        error,
                        valError.errorMsg, 
                        valType,
                        errorIsConditional,
                        valError.isConditional
                    );
                subject = 
                    FxAccountMIFIDHelper.getSubjectForValError(
                        subject,
                        valType,
                        errorIsConditional,
                        valError.isConditional
                    );
                System.debug('validateFxAccount -> valType: ' + valType);
                System.debug('validateFxAccount -> valError.isConditional: ' + valError.isConditional);
                System.debug('validateFxAccount -> errorIsConditional (before): ' + errorIsConditional);
                errorIsConditional = (
                    (existsPreviousError != true) && 
                    (valError.isConditional == true)
                );
                System.debug('validateFxAccount -> errorIsConditional (after): ' + errorIsConditional);
            }
        }
        System.debug('validateFxAccount -> errorIsConditional: ' + errorIsConditional + ', error: ' + error);
        if (
            (error != null) &&
            (errorIsConditional != true)
        ) {
            reportError(fxAccountObj, subject, error, true);
            return false;
        }
        //else...
        return true;
    }



    @testVisible
    private Boolean reportError(
        fxAccount__c fxAccountObj, 
        String subject,
        String error,
        Boolean requireUpdateCase
    ) {
        Boolean result = false;
        if (
            (requireUpdateCase == true) ||
            (updateExistingOpenCases == true) ||
            (fxAccountsWithMiFIDCaseOpen == null) ||
            (!fxAccountsWithMiFIDCaseOpen.contains(fxAccountObj.Id))
        ) {
            FxAccountValidationError errorObj = 
                new FxAccountValidationError(
                    fxAccountObj, 
                    subject,
                    error
                );
            errorMap.put(fxAccountObj.Id, errorObj);
            result = true;
        }
        return result;
    }

    /** dmorales: new, create a map to manage new custom object Mifid Validation Results associated to fxAccount **/
    void generateFxAccountResultMap(){
        
        fxAccountResultMap = new Map<Id, MiFID_Validation_Result__c>();

        for(fxAccount__c fxAccountObj: fxAccList){
            List<MiFID_Validation_Result__c> relatedResults = fxAccountObj.MiFID_Validation_Results__r;
            if(relatedResults != null && relatedResults.size() > 0)
               fxAccountResultMap.put(fxAccountObj.Id, relatedResults[0]);//Because each fxAccount must have only one MiFIDValidationResult object
        }
    }
    
}