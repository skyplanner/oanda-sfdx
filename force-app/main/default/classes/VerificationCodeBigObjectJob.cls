/**
 * @File Name          : VerificationCodeBigObjectJob.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/12/2019, 3:04:13 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   dmorales     Initial Version
**/
public with sharing class VerificationCodeBigObjectJob implements Queueable {

    private List<Verification_Code_Setting__c> codes;
   
    public VerificationCodeBigObjectJob(List<Verification_Code_Setting__c> codes) {
           this.codes = codes;         
    }

    public void execute(QueueableContext context) {
        List<Verification_Code__b> boCodes = new List<Verification_Code__b>();
      
        for(Verification_Code_Setting__c code: codes){
           Verification_Code__b bo = new Verification_Code__b();
               /*  Decimal order;
                if(code.Order__c + 999999 >= 2*999999) 
                    order = code.Order__c - 999999;
                else     
                    order = code.Order__c + 999999;
                bo.Order__c = order; */
           bo.Order__c = code.Order__c;
           bo.Code__c = code.Name;
           boCodes.add(bo);
        }          
       System.debug(boCodes);
       if(!Test.isRunningTest())
         Database.insertImmediate(boCodes);
       
    }
}