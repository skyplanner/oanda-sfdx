@isTest
public with sharing class EmailComponentControllerTest {
    
    @isTest
    private static void testGetEmailTemplates(){
        TestDataFactory factory = new TestDataFactory();
        Account acc = factory.createTestAccount();
        Opportunity opp = factory.createOpportunity(acc);

        Test.startTest();
        List<EmailTemplate> templates = EmailComponentController.getEmailTemplates(opp.Id, new List<String>());
        EmailComponentController.getRecentEmailTemplates(opp.Id);
        EmailComponentController.getRecordEmail(opp.Id, null, null);
        Test.stopTest();
        System.assert(templates.size() > 0);
    }
    
    @isTest
    private static void testGetTranslatedTemplate(){
        TestDataFactory factory = new TestDataFactory();
        Account acc = factory.createTestAccount();
        Opportunity opp = factory.createOpportunity(acc);

        Test.startTest();
        List<EmailTemplate> templates = EmailComponentController.getTranslatedTemplate(opp.Id, 'Unfound Template', 'French');

        Test.stopTest();
        System.assertEquals(0, templates.size());
    }

    @isTest
    private static void testGetRecentEmailTemplateByName(){
        TestDataFactory factory = new TestDataFactory();
        Account acc = factory.createTestAccount();
        Opportunity opp = factory.createOpportunity(acc);

        Test.startTest();
        List<EmailTemplate> templates = EmailComponentController.getRecentEmailTemplateByName(opp.Id, 'Unfound Template');

        Test.stopTest();
        System.assertEquals(0, templates.size());
    }

    @isTest
    private static void testGetTemplateDetails(){
        TestDataFactory factory = new TestDataFactory();
        Account acc = factory.createTestAccount();
        Opportunity opp = factory.createOpportunity(acc);
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Title';
        cv.PathOnClient = 'Test Path';
        cv.VersionData = Blob.valueOf('Test Body');
        insert cv;

        Test.startTest();
        List<EmailTemplate> templates = EmailComponentController.getEmailTemplates(opp.Id, new List<String>());
        EmailComponentController.EmailMsg msg = EmailComponentController.getTemplateDetails(templates[0].Id, null, opp.Id);
        EmailComponentController.getRelatedRecordId(opp.Id, 'Opportunity', 'Account.Email');
        EmailComponentController.parseInlineImages('Test bodyyy' + cv.Id, new List<String>{cv.Id});

        Test.stopTest();
        System.assertEquals(templates[0].Subject, msg.Subject);
    }

    @isTest
    private static void testSendAnEmailMsg(){
        TestDataFactory factory = new TestDataFactory();
        Account acc = factory.createTestAccount();
        Opportunity opp = factory.createOpportunity(acc);

        Test.startTest();

		GlobalStaticVar.stopTrigger();
        List<EmailTemplate> templates = EmailComponentController.getEmailTemplates(opp.Id, new List<String>());
        EmailComponentController.sendAnEmailMsg('test@test.com', 'testrecipient@test.com', null, null, templates[0].subject, 
        null, opp.Id, null, 'Test body', 'Test Sender', new List<String>(),
        new List<String>(),true, new List<String>(), null);

        Test.stopTest();
    }
    
}