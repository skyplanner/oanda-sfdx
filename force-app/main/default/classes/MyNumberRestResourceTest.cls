@isTest
private class MyNumberRestResourceTest {

    public static final string REST_RESOURCE_PATH = '/api/v1/my_number/*';

    @TestSetup
    static void makeData(){
        Test.loadData(My_Number__c.sObjectType, 'MyNumberTestSetup');
    }

    @isTest
    static void shouldDeleteMyNumber() {

        My_Number__c myNum = [SELECT Id, fxTrade_User_Id__c FROM My_Number__c LIMIT 1];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/api/v1/my_number/' + myNum.fxTrade_User_Id__c;  
        req.httpMethod = 'DELETE';
        req.resourcePath = REST_RESOURCE_PATH;

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        MyNumberRestResource.doDelete();

        Test.stopTest();

        List<My_Number__c> createdMyNums = [
            SELECT Id, My_Number__c, fxTrade_User_Id__c 
            FROM My_Number__c 
            WHERE fxTrade_User_Id__c = :myNum.fxTrade_User_Id__c
        ];

        Assert.areEqual(0, createdMyNums.size(), 'Client additional My Numbers Should be deleted');
    }

    @isTest
    static void shouldStoreMyNumber() {
        StaticResource sr = [SELECT Id,Name, Body FROM StaticResource WHERE Name = 'PostMyNumberTestJson' LIMIT 1];
        String jsonString = String.valueOf(sr.Body.toString());

        Map<String,Object> requestObject = 
            (Map<String,Object>) JSON.deserializeUntyped(jsonString);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/api/v1/my_number';  
        req.httpMethod = 'POST';
        req.resourcePath = REST_RESOURCE_PATH;
        req.requestBody = Blob.valueOf(jsonString);
        
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        MyNumberRestResource.doPost();
        Test.stopTest();

        List<My_Number__c> createdMyNums = [
            SELECT Id, My_Number__c, fxTrade_User_Id__c 
            FROM My_Number__c 
            WHERE fxTrade_User_Id__c = : String.valueOf(requestObject.get('fxTrade_User_Id__c'))
        ];

        Assert.areEqual(1, createdMyNums.size(), 'Client additional My Numbers Should be created');
        Assert.areEqual(createdMyNums[0].fxTrade_User_Id__c, String.valueOf(requestObject.get('fxTrade_User_Id__c')), 'Client Id mismatch');
        Assert.areEqual(createdMyNums[0].My_Number__c, String.valueOf(requestObject.get('My_Number__c')), 'My_Number__c field mismatch');
    }

    @isTest
    static void shouldGetMyNumber() {
        My_Number__c myNum = [SELECT fxTrade_User_Id__c, My_Number__c FROM My_Number__c LIMIT 1];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/api/v1/my_number/' + myNum.fxTrade_User_Id__c;  
        req.httpMethod = 'GET';
        req.resourcePath = REST_RESOURCE_PATH;

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        MyNumberRestResource.doGet();

        Test.stopTest();

        Map<String,Object> respObject = (Map<String,Object>) JSON.deserializeUntyped(String.valueOf(res.responseBody.toString()));

        Assert.areEqual(myNum.fxTrade_User_Id__c, (String) respObject.get('fxTrade_User_Id__c'), 'Client Id mismatch');
        Assert.areEqual(myNum.My_Number__c, (String) respObject.get('My_Number__c'), 'My_Number__c field mismatch');
    }

    @isTest
    static void shouldGetMyNumberOj() {
        My_Number__c myNum = [SELECT OJ_Client_Id__c, My_Number__c FROM My_Number__c LIMIT 1];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/api/v1/my_number/oj_id/' + myNum.OJ_Client_Id__c;  
        req.httpMethod = 'GET';
        req.resourcePath = REST_RESOURCE_PATH;

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        MyNumberRestResource.doGet();

        Test.stopTest();

        Map<String,Object> respObject = (Map<String,Object>) JSON.deserializeUntyped(res.responseBody.toString());

        Assert.areEqual(myNum.OJ_Client_Id__c, (String) respObject.get('OJ_Client_ID__c'), 'Client Id mismatch');
        Assert.areEqual(myNum.My_Number__c, (String) respObject.get('My_Number__c'), 'My_Number__c field mismatch');

    }
}