/**
 * Tests: ComplyAdvantageUsersDownloader
 * @author Fernando Gomez
 * @since 11/15/2021
 */
@isTest
private class ComplyAdvantageUsersDownloaderTest {

	/**
	 * Tests:
	 * public List<ComplyAdvantageUser> getUsers()
	 */
	@isTest
	private static void getUsers_good() {
		CalloutMock mock;
		ComplyAdvantageUsersDownloader d;
		List<ComplyAdvantageUser> users;

		mock = new CalloutMock(
			200,
			'{' +
				'"code": 200,' +
				'"status": "success",' +
				'"content": {' +
					'"data": [' +
						'{' +
							'"id": 9145,' +
							'"email": "fnazari@oanda.com",' +
							'"name": "Fareshta Nazari",' +
							'"updated_at": "2020-03-27 20:29:07",' +
							'"created_at": "2020-03-26 06:34:29"' +
						'}' +
					']' +
				'}' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		d = new ComplyAdvantageUsersDownloader('APLAOSIJDFOSJIAOSF');
		users = d.getUsers();
		System.assertEquals(9145, users[0].id);
	}

	/**
	 * Tests:
	 * public List<ComplyAdvantageUser> getUsers()
	 */
	@isTest
	private static void constructor() {
		ComplyAdvantageUsersDownloader d;
		d = new ComplyAdvantageUsersDownloader('div', 'US');
	}

	/**
	 * Tests:
	 * public List<ComplyAdvantageUser> getUsers()
	 */
	@isTest
	private static void getUsers_fail() {
		CalloutMock mock;
		ComplyAdvantageUsersDownloader d;
		List<ComplyAdvantageUser> users;

		mock = new CalloutMock(
			400,
			'{' +
				'"code": 400,' +
				'"status": "fail"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		d = new ComplyAdvantageUsersDownloader('APLAOSIJDFOSJIAOSF');
		try {
			users = d.getUsers();
			System.assert(false, 'Exception should be thrown');
		} catch (Exception ex) {
			// all good
		}
	}
}