/**
 * @File Name          : SCustomerTierCalculator.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/1/2024, 3:26:28 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/1/2024, 3:26:28 PM   aniubo     Initial Version
**/
public interface SCustomerTierCalculator {
	String getTier(SCustomerTypeInfo customerInfo);
}