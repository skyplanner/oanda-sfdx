@isTest(seeAllData=false)
public class TrustedContactPersonUtilTest 
{
    @isTest
    static void lead_tcp() 
    {
        Lead lead = new Lead(FirstName = 'Robert Gabriel', 
                             LastName='Mugabe', 
                             Email = 'test1@oanda.com',
                             Has_Trusted_Contact_Person__c = true);
        insert lead;
        
        fxAccount__c fxAccount = new fxAccount__c(Account_Email__c = 'test1@oanda.com', 
                                                  RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                                                  Lead__c = lead.Id,
                                                  Division_Name__c = 'OANDA Canada'); 
        insert fxAccount; 
      
        Entity_Contact__c entityContact =new Entity_Contact__c(First_Name__c =  'test',
                                                                Last_Name__c = 'test', 
                                                                Lead__c = lead.Id, 
                                                                Type__c= 'Trusted Contact Person',
                                                                RecordTypeId = RecordTypeUtil.getRecordTypeId('General', 'Entity_Contact__c'));
        insert entityContact;

        Id entityContactID = entityContact.Id;

        Entity_Contact__c[] ecs_before = [select Id from Entity_Contact__c where Type__c= 'Trusted Contact Person' and Lead__c = :lead.Id];
        system.assertEquals(1, ecs_before.size());

        lead.Has_Trusted_Contact_Person__c = false;
        update lead;

        Entity_Contact__c[] ecs_after = [select Id from Entity_Contact__c where Type__c= 'Trusted Contact Person' and Lead__c = :lead.Id];
        system.assertEquals(0, ecs_after.size());

    }

    @isTest
    static void account_tcp() 
    {
        Lead lead = new Lead(FirstName = 'Robert Gabriel', 
                             LastName='Mugabe', 
                             Email = 'test1@oanda.com',
                             Has_Trusted_Contact_Person__c = true);
        insert lead;

        fxAccount__c fxAccount = new fxAccount__c(Account_Email__c = 'test1@oanda.com', 
                                                  RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                                                  Lead__c = lead.Id,
                                                  Division_Name__c = 'OANDA Canada'); 
        insert fxAccount; 
        
        Account account = new Account(FirstName = 'Robert Gabriel',
                                      LastName = 'Mugabe',
                                      PersonMailingCountry = 'Canada',
                                      fxAccount__c = fxAccount.Id,
                                      Has_Trusted_Contact_Person__c = true);
        insert account; 

        Entity_Contact__c entityContact = new Entity_Contact__c(First_Name__c =  'test',
                                                                Last_Name__c = 'test', 
                                                                Account__c = account.Id, 
                                                                Type__c= 'Trusted Contact Person',
                                                                RecordTypeId = RecordTypeUtil.getRecordTypeId('General', 'Entity_Contact__c'));
        insert entityContact;

        Entity_Contact__c[] ecs_before = [select Id from Entity_Contact__c where Type__c= 'Trusted Contact Person' and Account__c = :account.Id];
        system.assertEquals(1, ecs_before.size());

        Test.startTest();
        checkRecursive.SetOfIDs = new Set<Id>();
        account.Has_Trusted_Contact_Person__c = false;
        update account;
        Test.stopTest();

        Entity_Contact__c[] ecs_after = [select Id from Entity_Contact__c where Type__c= 'Trusted Contact Person' and Account__c = :account.Id];
        system.assertEquals(0, ecs_after.size());
    }
   
}