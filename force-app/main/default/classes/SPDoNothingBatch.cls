/**
 * @File Name          : SPDoNothingBatch.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2023, 1:58:03 AM
**/
public with sharing class SPDoNothingBatch implements Database.Batchable<SObject> {

    public List<Log__c> start(
		Database.BatchableContext bc
	){
        System.debug('SPDoNothingBatch.start');
        List<Log__c> items = [
            select Id 
            from Log__c 
            limit 1
        ];
        return items;
	}

    public void execute(
		Database.BatchableContext bc, 
		List<Log__c> items
	){
        System.debug('SPDoNothingBatch.execute');
    }

    public void finish(
		Database.BatchableContext bc
	) {
        System.debug('SPDoNothingBatch.finish');
    }

}