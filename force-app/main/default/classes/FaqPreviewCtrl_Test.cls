/**
 * @File Name          : FaqPreviewCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/25/2020, 5:56:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/25/2020   acantero     Initial Version
**/
@istest
private class FaqPreviewCtrl_Test {

    //invalid param
    @istest
    static void getArticleLanguage_test1() {
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleLanguage(null);
        Test.stopTest();
        System.assertEquals(null, result);
    }

    //valid param
    @istest
    static void getArticleLanguage_test2() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.SPANISH_LANGUAGE
        );
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleLanguage(artId);
        Test.stopTest();
        System.assertEquals(FaqPreviewTestDataFactory.SPANISH_LANGUAGE, result);
    }

    //invalid param
    @istest
    static void getArticleId_test1() {
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleId(null, null);
        Test.stopTest();
        System.assertEquals(null, result);
    }

    //valid params
    @istest
    static void getArticleId_test2() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.SPANISH_LANGUAGE
        );
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleId(
            FaqPreviewTestDataFactory.ART_URL_NAME,
            FaqPreviewTestDataFactory.SPANISH_LANGUAGE
        );
        Test.stopTest();
        System.assertEquals(artId, result);
    }

    //valid urlName, empty language
    @istest
    static void getArticleId_test3() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.DEFAULT_LANGUAGE
        );
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleId(
            FaqPreviewTestDataFactory.ART_URL_NAME,
            null
        );
        Test.stopTest();
        System.assertEquals(artId, result);
    }

    //valid urlName, invalid language
    @istest
    static void getArticleId_test4() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.DEFAULT_LANGUAGE
        );
        Test.startTest();
        String result = FaqPreviewCtrl.getArticleId(
            FaqPreviewTestDataFactory.ART_URL_NAME,
            FaqPreviewTestDataFactory.SPANISH_LANGUAGE
        );
        Test.stopTest();
        System.assertEquals(artId, result);
    }


}