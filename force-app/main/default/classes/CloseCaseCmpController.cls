/**
 * @File Name          : CloseCaseCmpController.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/21/2021, 12:24:26 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/14/2020   dmorales     Initial Version
**/
public with sharing class CloseCaseCmpController {

	@AuraEnabled
	public static Case getCaseById(String caseId){
		try {
			return CaseHelper.getCloseInfoById((ID)caseId);
			//...
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
    public static Map<Object,List<String>> getDependencies(String field){
    	Map<Object,List<String>> dependentPicklistValues = 
    	   new Map<Object,List<String>>();
       try{
         	List<String> splitString = field.split('\\.');
			// store the object/field names
			String objectName = splitString[0];
			String fieldName = splitString[1];
			// get the SObjectType
			Schema.SObjectType objectType = 
			Schema.getGlobalDescribe().get(objectName);
			// get the fields on the object
			Map<String, SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
			// The key to the map is the api name of the field
			Schema.SobjectField theField = fieldMap.get(fieldName);	    
	    	dependentPicklistValues = PicklistUtil.getDependentPicklistValues(theField);		
       }
       catch(Exception e){
           return null;		 
       }
		return dependentPicklistValues; 
	}

	@AuraEnabled
    public static List<SubtypeByAgentInfo> getSubtypeByAgentList() {
		return ServiceQuestionsMapper.getSubtypeByAgentList();
	}
	
}