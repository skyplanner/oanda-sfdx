public class FxAccountActionController {
    
     @AuraEnabled
    public static String getSessionId() {
        return UserInfo.getSessionId();
    }
    
    @AuraEnabled
    public static String getUserId() {
        return UserInfo.getUserId();
    }
    
    @AuraEnabled
    public static void publishAction(Decimal fxTradeUserId, String actionType, String parameter_1) {
        new ActionUtil().publishAction(fxTradeUserId, actionType, parameter_1);
    }
    
    @AuraEnabled
    public static fxAccount__c getFxAccount(ID fxaId) {
        fxAccount__c fxa = [SELECT Name, fxTrade_User_Id__c FROM fxAccount__c WHERE ID = :fxaId LIMIT 1];
        
        return fxa;
    }
    
    @AuraEnabled
    public static void createCase(ID fxaId, String token) {
        final String EMAIL_TEMPLATE = 'OANDA_fxTrade_Reset_Your_Account_Password';
        
        List<fxAccount__c> fxas = [SELECT Account__c, Lead__c, Contact__c, Account__r.PersonContactId FROM fxAccount__c WHERE Id = :fxaId];
        
        if (fxas.isEmpty())
            return;
        
        // Create a case
        Case cs = new Case();
        cs.OwnerId = getUserId();
        cs.fxAccount__c = fxaId;
        cs.AccountId = fxas[0].Account__c;
        cs.Lead__c = fxas[0].Lead__c;
        cs.ContactId = fxas[0].Contact__c;
        cs.Status = 'Closed';
        cs.Subject = 'OANDA fxTrade - Reset Your Account Password';
        cs.Origin = 'Password Reset';
        cs.RecordTypeId = RecordTypeUtil.getSupportCaseTypeId();    // Support Record Type
        cs.Description ='To securely reset your OANDA fxTrade account password, please click the link below and enter your username and the unique code provided by our Customer Service Representative\n\n'
            + 'https://www.oanda.com/account/password-reset/confirm-identity\n\n'
            + 'If you did not request this email, please contact us at fxtrade@oanda.com. Your account is safe, as the link above requires entry of additional information for verification.\n'
            + 'If you have any further questions or comments, please do not hesitate to contact us at fxtrade@oanda.com.\n\n'
            + token;
        
        // Insert the case
        insert cs;
        
        // Send an email
        Id targetObjectId = fxas[0].Account__r.PersonContactId != null ? fxas[0].Account__r.PersonContactId : fxas[0].Lead__c;
        EmailUtil.sendEmailByTemplate(targetObjectId, EMAIL_TEMPLATE, cs.Id);
    }
}