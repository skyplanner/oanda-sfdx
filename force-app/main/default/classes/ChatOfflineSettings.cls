/**
 * @File Name          : ChatOfflineSettings.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 07-14-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/17/2020   dmorales     Initial Version
 **/
public without sharing class ChatOfflineSettings {

public static final String DEFAULT_SETTINGS = 'DefaultSetting';

private static ChatOfflineSettings instance;

private String key;
public Boolean initializationFails {get; private set;}

public String fromDay {get; private set;}
public String toDay {get; private set;}
//    public Integer fromHour {get; private set;}
public String fromHourMinute {get; private set;}
public String toHourMinute {get; private set;}
//    public Integer toHour {get; private set;}
public Boolean isActive {get; private set;}
public String defaultDivision {get; private set;}
public String redirectDivision {get; private set;}
public Boolean isOffline {get; private set;}

public static Map<String,Integer> mapDays = new Map<String,Integer> {
	'Monday' => 0,
	'Tuesday' => 1,
	'Wednesday' => 2,
	'Thursday' => 3,
	'Friday' => 4,
	'Saturday' => 5,
	'Sunday' => 6
};

@TestVisible
private ChatOfflineSettings(String fromDay, String toDay, String fromHourMinute, String toHourMinute, Boolean isActive){
	 this.fromDay = fromDay;
	 this.toDay = toDay;
	 this.fromHourMinute = fromHourMinute;
	 this.toHourMinute = toHourMinute;
	 this.isActive = isActive;
	 this.defaultDivision = 'OC';
	 this.redirectDivision = 'Default';
	 this.initializationFails = false;
	 this.isOffline = checkOfflineHours();
}

private ChatOfflineSettings(String key) {
	this.key = key;
	load();
	this.isOffline = checkOfflineHours();
}

public static ChatOfflineSettings getInstance(String key) {
	if ((instance == null) || (instance.key != key)) {
		instance = new ChatOfflineSettings(key);
	}
	return instance;
}

public static ChatOfflineSettings getDefaultInstance() {
	ChatOfflineSettings result = getInstance(DEFAULT_SETTINGS);
	return result;
}


void load() {
	initializationFails = true;
	List<Chat_Schedule_Offline__mdt> settingsList = [
		SELECT
		From_Day__c,
		//    From_Hour__c,
		From_Hour_Minute__c,
		To_Day__c,
		//   To_Hour__c,
		To_Hour_Minute__c,
		Is_Active__c,
		Default_Division__c,
		Re_direct_Division__c
		FROM
		Chat_Schedule_Offline__mdt
		WHERE
		DeveloperName = :key
	];
	if (!settingsList.isEmpty()) {
		initializationFails = false;
		Chat_Schedule_Offline__mdt settings = settingsList[0];
		//   fromHour = Integer.valueOf(settings.From_Hour__c);
		fromHourMinute = settings.From_Hour_Minute__c;
		toHourMinute = settings.To_Hour_Minute__c;
		//    toHour = Integer.valueOf(settings.To_Hour__c);
		fromDay = settings.From_Day__c;
		toDay = settings.To_Day__c;
		isActive = settings.Is_Active__c;
		defaultDivision = settings.Default_Division__c;
		redirectDivision = settings.Re_direct_Division__c;
	}
}

Boolean checkOfflineHours(){
   
	Datetime nowDate = System.now();

	String dayTodayString = nowDate.format('EEEE', 'US/Eastern');
	Integer dayToday = mapDays.get(dayTodayString);
	Integer hourNow = Integer.valueOf(nowDate.format('HH','US/Eastern'));
	Integer minutesNow = nowDate.minute();


	//find reference schedule in custom metadata
	if(initializationFails && !Test.isRunningTest())
		return false;
	if(!isActive && !Test.isRunningTest())
		return false;

	Integer fromDay = mapDays.get(fromDay);
	Integer toDay = mapDays.get(toDay);

	String[] fromTimeSplit = fromHourMinute.split(':');
	Integer fromHourNew = 0;
	Integer fromMinuteNew = 0;

	String[] toTimeSplit = toHourMinute.split(':');
	Integer toHourNew = 0;
    Integer toMinuteNew = 0;
  
    try{
      fromHourNew = Integer.valueOf(fromTimeSplit[0]);
      fromMinuteNew = Integer.valueOf(fromTimeSplit[1]);
    
      toHourNew = Integer.valueOf(toTimeSplit[0]);
      toMinuteNew = Integer.valueOf(toTimeSplit[1]);
    }
    catch(Exception e){
      return false;
    }

	System.debug('dayToday ' + dayToday);
	System.debug('hourNow ' + hourNow);
	System.debug('minuteNow ' + minutesNow);
	System.debug('fromDay ' + fromDay);
	System.debug('fromHour ' + fromHourNew);
	System.debug('fromMinute ' + fromMinuteNew);
	System.debug('toDay ' + toDay);
	System.debug('toHour ' + toHourNew);
	System.debug('toMinute ' + toMinuteNew);

	Boolean offline = false;

	if(toDay == fromDay && dayToday == toDay) {
		System.debug('(toDay == fromDay && dayToday == toDay)');
		//if(hourNow >= fromHour && hourNow < toHour)
		if(hourNow > fromHourNew && hourNow < toHourNew) {
			System.debug('(hourNow > fromHourNew && hourNow < toHourNew)');
			return true;
		}

		if(toHourNew == fromHourNew && hourNow == fromHourNew) {
			System.debug('(toHourNew == fromHourNew && hourNow == fromHourNew)');
			if(minutesNow >= fromMinuteNew && minutesNow < toMinuteNew)
				return true;
			//else
			return false;
		}

		if(hourNow == fromHourNew && minutesNow >= fromMinuteNew) {
			System.debug('(hourNow == fromHourNew && minutesNow >= fromMinuteNew)');
			return true;
		}

		if(hourNow == toHourNew && minutesNow < toMinuteNew) {
			System.debug('(hourNow == toHourNew && minutesNow < toMinuteNew)');
			return true;
		}
		// else
		return false;
	}

	if(dayToday == fromDay) {
		if(hourNow > fromHourNew)
			return true;
		if(hourNow == fromHourNew && minutesNow >= fromMinuteNew)
			return true;
		//else
		return false;
	}

	if(dayToday == toDay) {
		if(hourNow < toHourNew)
			return true;
		if(hourNow == toHourNew && minutesNow < toMinuteNew)
			return true;
		//else
		return false;
	}


	Boolean inverse = (fromDay > toDay);
	System.debug('Inverse ' + inverse);

	if(fromDay < dayToday && dayToday < toDay) {
		offline = true;
	}

	if(inverse)
		return !offline;
	else
		return offline;
}


}