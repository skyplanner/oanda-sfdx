/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@IsTest
private class BatchChatFormCheckTest {
    @isTest
	static void test500() {
        CalloutMock mock = new CalloutMock(
			503,
			'{}',
			'Fail',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.StartTest();

        BatchChatFormCheck b = new BatchChatFormCheck();
        Database.executeBatch(b);

		Test.stopTest();

		List<AsyncApexJob> aj = [SELECT Id
			FROM AsyncApexJob
			WHERE ApexClass.Name = 'BatchChatFormCheck'
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		System.assertEquals(true, aj.size() > 0);
	}

    @isTest
	static void test200() {
        CalloutMock mock = new CalloutMock(
			200,
			'{}',
			'Fail',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

        Test.StartTest();

        BatchChatFormCheck b = new BatchChatFormCheck();
        Database.executeBatch(b);

		Test.stopTest();

		List<AsyncApexJob> aj = [SELECT Id
			FROM AsyncApexJob
			WHERE ApexClass.Name = 'BatchChatFormCheck'
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		System.assertEquals(true, aj.size() > 0);
	}
}