/**
 * @File Name          : CountryCodeMetadataUtil.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/8/2024, 2:47:00 AM
 * @Modification Log   : 
 * Ver    Date         Author     Modification
 * 1.0    10/15/2019   dmorales   Initial Version
 * 1.1    2/15/2021    fgomez     new Field added to metadata query
**/
public without sharing class CountryCodeMetadataUtil {
   
    public static List<Country_Code__mdt> getPortalCountries() {
       List<Country_Code__mdt> countries = [SELECT 
              DeveloperName, 
              Country__c,
              Alternative_Name__c, 
              Region__c, 
              Default_Language__c,
              Geographical_Region__c,
              Help_Portal_Order__c,
              Is_Prohibited__c
   			FROM Country_Code__mdt 
            WHERE Visible__c = true 
            ORDER BY Geographical_Region__c, Help_Portal_Order__c, Alternative_Name__c, Country__c];
        return countries;       
    }

    public static Country_Code__mdt getCountryByCode(String countryCode){
      
        List<Country_Code__mdt> countries = [SELECT 
              DeveloperName, 
              Country__c,
              Alternative_Name__c, 
              Region__c, 
              Default_Language__c, 
              Geographical_Region__c,
              Is_Prohibited__c,
              Is_TMS__c
   			FROM Country_Code__mdt 
            WHERE DeveloperName =: countryCode ]; 

		if(! countries.isEmpty()){
			return countries[0];
		}		    
        
		return null;      
    }

    public static List<String> getCountryMapInRegion(List<String> regions) {
        List<String> result = new List<String>();
          List<Country_Code__mdt> countryCodeList = [
              SELECT
                DeveloperName, 
                Country__c,            
                Region__c,
                Is_Prohibited__c
             FROM 
                Country_Code__mdt 
             WHERE Region__c in :regions ];
  
           if (countryCodeList.isEmpty()) {
              return result;
          }
          //else...
          for(Country_Code__mdt countryCodeObj : countryCodeList) {
              result.add(countryCodeObj.Country__c);
          }
          return result;
      }

    public static List<Geographical_Region_Setting__mdt> getGeographicalRegionOrder(){
      
        List<Geographical_Region_Setting__mdt> geoRegion = [SELECT Geographical_Region__c, Order__c 
                                                            FROM Geographical_Region_Setting__mdt 
                                                            ORDER BY Order__c ]; 
        System.debug('Region ' + geoRegion);  
		return geoRegion;      
    }

    public static Country_Code__mdt getRegionInfoByAlpha2Code(
        String alpha2Code
    ) {
        Country_Code__mdt result = null;
        List<Country_Code__mdt> recList = [ 
            SELECT
                Country__c,
                Alternative_Name__c, 
                Region__c
            FROM Country_Code__mdt 
            WHERE DeveloperName = :alpha2Code 
        ];

		if (recList.isEmpty() == false) {
			result = recList[0];
		}		    
		return result;
    }

    public static Country_Code__mdt getRegionInfoByAlpha3Code(
        String alpha3Code
    ) {
        Country_Code__mdt result = null;
        List<Country_Code__mdt> recList = [ 
            SELECT
                Country__c,
                Alternative_Name__c, 
                Region__c
            FROM Country_Code__mdt 
            WHERE ALPHA_3__c = :alpha3Code 
        ];

		if (recList.isEmpty() == false) {
			result = recList[0];
		}		    
		return result;
    }

    public static Country_Code__mdt getRegionInfoByCountryCode(
        String countryCode
    ) {
        Country_Code__mdt result = null;
        if (String.isBlank(countryCode)) {
			return result;
		}
		// else...
		if (countryCode.length() == 3) {
			result = 
                CountryCodeMetadataUtil.getRegionInfoByAlpha3Code(countryCode);
		
		} else {
			result = 
                CountryCodeMetadataUtil.getRegionInfoByAlpha2Code(countryCode);
		}
        return result;
    }

}