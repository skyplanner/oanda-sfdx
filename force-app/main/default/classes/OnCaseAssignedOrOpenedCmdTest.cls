/**
 * @File Name          : OnCaseAssignedOrOpenedCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/1/2024, 1:15:14 PM
**/
@IsTest
private without sharing class OnCaseAssignedOrOpenedCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	/**
	 * test 1 : workItemSObjType = Case
	 * test 2 :workItemSObjType = MessagingSession
	 */
	@IsTest
	static void checkRecord() {
		ID workItemId = null;

		Test.startTest();
		OnCaseAssignedOrOpenedCmd instance = 
			new OnCaseAssignedOrOpenedCmd();
		// test 1 => return true
		Boolean result1 = instance.checkRecord(
			Schema.Case.SObjectType, //workItemSObjType
			workItemId // workItemId
		);
		// test 2 => return false
		Boolean result2 = instance.checkRecord(
			Schema.MessagingSession.SObjectType, //workItemSObjType
			workItemId // workItemId
		);
		Test.stopTest();

		Assert.isTrue(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
	}

	/**
	 * test 1 : caseIds is empty
	 * test 2 : caseIds is not empty
	 */
	@IsTest
	static void execute() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);

		Test.startTest();
		// test 1 => return false
		OnCaseAssignedOrOpenedCmd instance1 = 
			new OnCaseAssignedOrOpenedCmd();
		Boolean result1 = instance1.execute();
		// test 2 => return true
		OnCaseAssignedOrOpenedCmd instance2 = 
			new OnCaseAssignedOrOpenedCmd();
		instance2.checkRecord(
			Schema.Case.SObjectType, // workItemSObjType, 
			case1Id // workItemId
		);
		Boolean result2 = instance2.execute();
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
	}
	
}