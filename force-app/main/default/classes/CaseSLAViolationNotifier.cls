/**
 * @File Name          : CaseSLAViolationNotifier.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:11:30 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2024, 3:56:48 PM   aniubo     Initial Version
 **/
public inherited sharing class CaseSLAViolationNotifier extends SLAViolationBaseNotifier {
	private Map<Id, Id> ownersByCaseId { get; set; }
	private Set<Id> owners { get; set; }
	private List<SLAMilestoneViolationInfo> assignedCases = new List<SLAMilestoneViolationInfo>();
	private List<SLAMilestoneViolationInfo> unAssignedCases = new List<SLAMilestoneViolationInfo>();
	private SLAViolationNotificationManager notificationManagerDiv { get; set; }

	public CaseSLAViolationNotifier(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		SLAViolationCanNotifiable violationNotificationChecker
	) {
		super(
			notificationObjectType,
			violationType,
			dataReader,
			violationNotificationChecker
		);
		ownersByCaseId = new Map<Id, Id>();
		owners = new Set<Id>();
	}

	protected override void setSLAViolationNotificationManager() {
		if (!assignedCases.isEmpty()) {
			fillOwnerName(assignedCases, owners);
			notificationManager = SLAViolationNotificationManagerFactory.createNotificationManager(
				SLAConst.SLANotificationObjectType.CHAT_CASE_NOT,
				this.violationType,
				SLAConst.AgentSupervisorModel.ROLE,
				this.settingsByViolationType,
				new SupervisorProviderFactory()
			);
		}
		if (!unAssignedCases.isEmpty()) {
			notificationManagerDiv = SLAViolationNotificationManagerFactory.createNotificationManager(
				SLAConst.SLANotificationObjectType.CHAT_CASE_NOT,
				this.violationType,
				SLAConst.AgentSupervisorModel.DIVISION,
				this.settingsByViolationType,
				new SupervisorProviderFactory()
			);
		}
	}

	public override void notifyViolation(
		SLAViolationNotificationData notificationData
	) {
		List<SObject> records = getData(notificationData);
		if (records != null && !records.isEmpty()) {
			List<SLAViolationInfo> violationInfos = getViolationInfos(records);
			if (
				this.violationNotificationChecker.canNotify(
					this.violationType,
					this.notificationObjectType
				)
			) {
				setSLAViolationNotificationManager();
				if (
					assignedCases != null &&
					!assignedCases.isEmpty() &&
					notificationManager != null
				) {
					notificationManager.sendNotifications(assignedCases);
				}
				if (
					unAssignedCases != null &&
					!unAssignedCases.isEmpty() &&
					notificationManagerDiv != null
				) {
					notificationManagerDiv.sendNotifications(unAssignedCases);
				}
			}
			saveViolationInfo(violationInfos);
		}
	}

	private List<SObject> getData(
		SLAViolationNotificationData notificationData
	) {
		CaseSLAViolationNotificationData caseData = (CaseSLAViolationNotificationData) notificationData;
		List<Case> cases = this.violationDataReader.getData(notificationData);
		for (Case current : cases) {
			Milestone_Time_Settings__mdt setting = this.settingsByViolationType.get(
				current.Milestone_Violated__c
			);
			Id caseOwner = setting?.Triggered_by_owner_change__c
				? caseData.oldMap.get(current.Id).OwnerId
				: current.OwnerId;
			ownersByCaseId.put(current.Id, caseOwner);
		}
		return cases;
	}

	protected override SLAViolationInfo createViolationInfo(SObject record) {
		Case caseObj = (Case) record;
		Milestone_Time_Settings__mdt setting = this.settingsByViolationType.get(
			caseObj.Milestone_Violated__c
		);
		SLAMilestoneViolationInfo result = new SLAMilestoneViolationInfo(
			caseObj.Id,
			caseObj.CaseNumber,
			setting
		);
		result.isPersistent = true;
		result.division = getDivision(caseObj);
		result.inquiryNature = caseObj.Inquiry_Nature__c;
		result.Tier = caseObj.Tier__c;
		result.language = String.isNotBlank(caseObj.Chat_Language_Preference__c)
			? caseObj.Chat_Language_Preference__c
			: caseObj.Language_Preference__c;
		result.liveOrPractice = caseObj.Live_or_Practice__c;
		result.caseOrigin = getCaseOrigin(caseObj.Origin);
		result.notificationObjectType = getNotificationObjectType(
			caseObj.Origin
		);
		Boolean caseIsAssigned = !SPQueueUtil.ownerIsAQueue(
			ownersByCaseId.get(caseObj.Id)
		);
		if (caseIsAssigned) {
			result.ownerId = ownersByCaseId.get(caseObj.Id);
			assignedCases.add(result);
			owners.add(result.ownerId);
		} else {
			checkUnassignedViolationInfo(result, caseObj);
		}
		return result;
	}

	protected override Map<String, Milestone_Time_Settings__mdt> getMilestoneTimeSettingsByViolationType() {
		return MilestoneUtils.getSettingsFilterByViolationType(
			new List<String>{
				SLAConst.AHT_EXCEEDED_VT,
				SLAConst.ASA_EXCEEDED_VT
			}
		);
	}

	private String getDivision(Case caseObj) {
		if (
			String.isNotBlank(caseObj.Chat_Division__c) &&
			String.isNotBlank(caseObj.Division__c)
		) {
			return caseObj.Division__c;
		}
		//else...
		return caseObj.Account_Division_Name__c;
	}

	private void checkUnassignedViolationInfo(
		SLAMilestoneViolationInfo violationInfo,
		Case caseObj
	) {
		if (String.isNotBlank(caseObj.Chat_Division__c)) {
			violationInfo.divisionCode = caseObj.Chat_Division__c;
			//...
		} else if (String.isNotBlank(caseObj.Account_Division_Name__c)) {
			violationInfo.divisionCode = ServiceDivisionsManager.getInstance()
				.getDivisionCode(caseObj.Account_Division_Name__c);
		}
		if (String.isNotBlank(violationInfo.divisionCode)) {
			unAssignedCases.add(violationInfo);
		}
	}

	private SLAConst.SLANotificationObjectType getNotificationObjectType(
		String caseOrigin
	) {
		SLAConst.SLANotificationObjectType notificationObjectType;
		if (
			caseOrigin == SLAConst.CASE_ORIGIN_EMAIL_API ||
			caseOrigin == SLAConst.CASE_ORIGIN_FRONT_DESK
		) {
			notificationObjectType = SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT;
		} else if (
			caseOrigin == SLAConst.CASE_ORIGIN_CHATBOT_CASE ||
			caseOrigin == SLAConst.CASE_ORIGIN_CHAT ||
			caseOrigin == SLAConst.CASE_ORIGIN_CHATBOT_EMAIL_CASE
		) {
			notificationObjectType = SLAConst.SLANotificationObjectType.CHAT_CASE_NOT;
		} else if (caseOrigin == SLAConst.CASE_ORIGIN_PHONE) {
			notificationObjectType = SLAConst.SLANotificationObjectType.PHONE_CASE_NOT;
		}
		return notificationObjectType;
	}
	private String getCaseOrigin(String caseOrigin) {
		String channelName;
		if (
			caseOrigin == SLAConst.CASE_ORIGIN_EMAIL_API ||
			caseOrigin == SLAConst.CASE_ORIGIN_FRONT_DESK ||
			caseOrigin == SLAConst.CASE_ORIGIN_PHONE
		) {
			channelName = caseOrigin;
		} else {
			channelName = 'Chatbot';
		}
		return channelName;
	}

	private void fillOwnerName(
		List<SLAMilestoneViolationInfo> violationInfos,
		Set<Id> ownerIds
	) {
		Map<String, String> userNameMap = new Map<String, String>();
		List<User> userList = [
			SELECT Name
			FROM User
			WHERE Id IN :ownerIds
		];
		for (User userObj : userList) {
			userNameMap.put(userObj.Id, userObj.Name);
		}
		for (SLAMilestoneViolationInfo info : violationInfos) {
			if (String.isNotBlank(info.ownerId)) {
				info.ownerName = userNameMap.get(info.ownerId);
			}
		}
	}
}