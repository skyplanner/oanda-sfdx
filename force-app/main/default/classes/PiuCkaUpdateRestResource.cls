/* Name: PiuCkaUpdateRestResource
 * Description : New Apex Class to handle Personal Information Update from user-api
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 21
 */

@RestResource(urlMapping='/api/v1/user/*/piu_cka_update')
global class PiuCkaUpdateRestResource {

    public static final List<String> POST_PARAMS = new List<String>{'user_id'};

    @HttpPost
    global static void doPost() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, POST_PARAMS);

        try {
            Integer userId = Integer.valueOf(context.getPathParam('user_id'));
            
            FxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

            if(fxa == null) {
                String respBody = 'Could not find user with given Id';
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 404;
                return;
            }

            Piu_Cka_Update__e piuCkaEvent = new Piu_Cka_Update__e();
            piuCkaEvent.fxTrade_User_Id__c = fxa.Id;
            piuCkaEvent.User_Payload__c = context.getBodyAsString();
            Database.SaveResult sr = EventBus.publish(piuCkaEvent);

            if (!sr.isSuccess()) {
                LogEvt__e errLog = new LogEvt__e(
                Type__c = 'Error',
                    Category__c = '/api/v1/user/*/piu_cka_update' + ' Input payload for '+ userId,
                    Message__c = 'Failed to publish event: '+ piuCkaEvent,
                    Details__c = context.getBodyAsString(),
                    Id__c = (String)UserInfo.getUserId());
                EventBus.publish(errLog);
            }
            
            //provide response to user-api
            res.responseBody = Blob.valueOf('Record will be processed.');
            res.statusCode = 200;
            return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

    
}