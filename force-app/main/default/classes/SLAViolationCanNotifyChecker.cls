/**
 * @File Name          : SLAViolationCanNotifyChecker.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 12:49:37 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2024, 11:29:21 AM   aniubo     Initial Version
 **/
public with sharing class SLAViolationCanNotifyChecker implements SLAViolationCanNotifiable {
	@testVisible
	private final static String SP_GENERAL_SETTING_SLA_NO_NOTIFICATION_NAME = 'Do_not_Send_SLA_Notification';
	private SPGeneralSettings settings { get; set; }

	@testVisible
	private Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>> disableNotificationsByObjectType {
		get;
		set;
	}

	public SLAViolationCanNotifyChecker() {
		settings = SPGeneralSettings.getInstance();
	}

	public Boolean canNotify(
		SLAConst.SLAViolationType violationType,
		SLAConst.SLANotificationObjectType notificationObjectType
	) {
		Boolean notify = true;
		if (disableNotificationsByObjectType == null) {
			initDisableNotificationsByType();
		}
		if (!disableNotificationsByObjectType.isEmpty()) {
			List<SLAConst.SLAViolationType> violationTypes = disableNotificationsByObjectType.get(
				notificationObjectType
			);
			if (
				violationTypes != null && violationTypes.contains(violationType)
			) {
				notify = false;
			}
		}
		return notify;
	}

	@testVisible
	private void initDisableNotificationsByType() {
		disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = settings.getValueAsList(
			SP_GENERAL_SETTING_SLA_NO_NOTIFICATION_NAME
		);
		initDisableNotificationsByType(disables);
	}

	@testVisible
	private void initDisableNotificationsByType(List<String> disables) {
		if (disables != null || !disables.isEmpty()) {
			for (String disableValue : disables) {
				SLAConst.SLAViolationType violationType = null;
				List<SLAConst.SLANotificationObjectType> notificationObjectTypes = new List<SLAConst.SLANotificationObjectType>();
				if (
					String.isNotBlank(disableValue) &&
					disableValue.contains('_')
				) {
					List<String> disableParts = disableValue.split('_');
					String objName = disableParts[0];
					switch on objName {
						when 'MESSAGING' {
							violationType = parseViolationType(disableParts[1]);
							notificationObjectTypes.add(
								SLAConst.SLANotificationObjectType.MESSAGING_NOT
							);
						}
						when 'CHAT' {
							violationType = parseViolationType(disableParts[1]);
							notificationObjectTypes.add(
								SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
							);
						}
						when 'CASE' {
							Integer violationTypeIdx = 1;
							if (disableParts.size() > 2) {
								SLAConst.SLANotificationObjectType notificationObjectType = null;
								String caseType = disableParts[1];
								if (caseType == 'CHAT') {
									notificationObjectType = SLAConst.SLANotificationObjectType.CHAT_CASE_NOT;
								} else if (caseType == 'EMAIL') {
									notificationObjectType = SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT;
								} else if (caseType == 'PHONE') {
									notificationObjectType = SLAConst.SLANotificationObjectType.PHONE_CASE_NOT;
								}
								if (notificationObjectType != null)
									notificationObjectTypes.add(
										notificationObjectType
									);
								violationTypeIdx = 2;
							} else {
								notificationObjectTypes.add(
									SLAConst.SLANotificationObjectType.CHAT_CASE_NOT
								);
								notificationObjectTypes.add(
									SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT
								);
								notificationObjectTypes.add(
									SLAConst.SLANotificationObjectType.PHONE_CASE_NOT
								);
							}
							violationType = parseViolationType(
								disableParts[violationTypeIdx]
							);
						}
					}
					if (
						!notificationObjectTypes.isEmpty() &&
						violationType != null
					) {
						addDisableNotification(
							notificationObjectTypes,
							violationType
						);
					}
				}
			}
		}
	}
	private SLAConst.SLAViolationType parseViolationType(String violationType) {
		SLAConst.SLAViolationType type = null;
		if (String.isNotBlank(violationType)) {
			try {
				type = SLAConst.SLAViolationType.valueOf(violationType);
			} catch (Exception ex) {
				type = null;
			}
		}
		return type;
	}

	private void addDisableNotification(
		List<SLAConst.SLANotificationObjectType> notificationObjectTypes,
		SLAConst.SLAViolationType violationType
	) {
		for (
			SLAConst.SLANotificationObjectType notificationObjectType : notificationObjectTypes
		) {
			if (
				!disableNotificationsByObjectType.containsKey(
					notificationObjectType
				)
			) {
				disableNotificationsByObjectType.put(
					notificationObjectType,
					new List<SLAConst.SLAViolationType>()
				);
			}
			List<SLAConst.SLAViolationType> violationTypes = disableNotificationsByObjectType.get(
				notificationObjectType
			);
			if (!violationTypes.contains(violationType)) {
				violationTypes.add(violationType);
			}
		}
	}
}