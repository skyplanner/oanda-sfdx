/**
 * @File Name          : MessagingSLAViolationNotifierTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:16:47 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/29/2024, 11:31:51 AM   aniubo     Initial Version
 **/
@isTest
private class MessagingSLAViolationNotifierTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();

		initManager.setFieldValues(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			new Map<String, Object>{
				'Division_Name__c' => Constants.DIVISIONS_OANDA_CANADA
			}
		);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		initManager.setFieldValues(
			ServiceTestDataKeys.LEAD_1,
			new Map<String, Object>{ 'fxAccount__c' => fxAccountId }
		);
		ServiceTestDataFactory.createLead1(initManager);
		initManager.storeData();
		Id accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);

		Account account = new Account(
			Id = accountId,
			fxAccount__c = fxAccountId
		);
		update account;
	}

	@isTest
	private static void TestNotifyViolation() {
		// Test data setup

		// Actual test
		Test.startTest();
		TestSLAViolationDataReader reader = new TestSLAViolationDataReader(
			false
		);
		ChatSLAViolationNotificationData notificationData = new ChatSLAViolationNotificationData(
			new List<Id>()
		);
		MessagingSLAViolationNotifier notifier = new MessagingSLAViolationNotifier(
			SLAConst.SLANotificationObjectType.MESSAGING_NOT,
			SLAConst.SLAViolationType.ASA,
			new TestSLAViolationDataReader(false),
			new TestSLAViolationCanNotifiable()
		);
		notifier.notifyViolation(notificationData);
		Test.stopTest();
		Integer count = getCountViolations();
		Assert.areEqual(1, count, 'Should have 1 violation');

		// Asserts
	}
	@isTest
	private static void TestNotifyViolationLead() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingSLAViolationNotifier notifier = new MessagingSLAViolationNotifier(
			SLAConst.SLANotificationObjectType.MESSAGING_NOT,
			SLAConst.SLAViolationType.ASA,
			new TestSLAViolationDataReader(true),
			new TestSLAViolationCanNotifiable()
		);
		notifier.notifyViolation(
			new ChatSLAViolationNotificationData(new List<Id>())
		);

		Test.stopTest();
		Integer count = getCountViolations();
		Assert.areEqual(1, count, 'Should have 1 violation');

		// Asserts
	}
	@IsTest
	static void testDivisionNameAccount() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		Account acc = TestSLAViolationDataReader.getAccount(accountId);
		Test.startTest();
		MessagingSLAViolationNotifier notifier = new MessagingSLAViolationNotifier(
			SLAConst.SLANotificationObjectType.MESSAGING_NOT,
			SLAConst.SLAViolationType.ASA,
			new TestSLAViolationDataReader(true),
			new TestSLAViolationCanNotifiable()
		);
		String divisionName = notifier.getAccountDivisionName(acc);
		Test.stopTest();
		Assert.areEqual(
			true,
			String.isNotBlank(divisionName),
			'Should have division name'
		);
	}
	@IsTest
	static void testDivisionNameLead() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id leadId = initManager.getObjectId(ServiceTestDataKeys.LEAD_1, true);
		Lead lead1 = TestSLAViolationDataReader.getLead(leadId);
		Test.startTest();
		MessagingSLAViolationNotifier notifier = new MessagingSLAViolationNotifier(
			SLAConst.SLANotificationObjectType.MESSAGING_NOT,
			SLAConst.SLAViolationType.ASA,
			new TestSLAViolationDataReader(true),
			new TestSLAViolationCanNotifiable()
		);
		String divisionName = notifier.getLeadDivisionName(lead1);
		Test.stopTest();
		Assert.areEqual(
			true,
			String.isNotBlank(divisionName),
			'Should have division name'
		);
	}

	private static Integer getCountViolations() {
		return [
			SELECT COUNT()
			FROM SLA_Violation__c
		];
	}

}