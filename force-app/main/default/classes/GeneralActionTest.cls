/**
 * GeneralAction test class
 */
@isTest
public class GeneralActionTest implements Callable {
    
    @isTest
    static void publish() {
        insert new Lead(
            LastName = 'Colon',
            Email = 'test@example.com',
            Company = 'Companuti LLC');

        Test.startTest();

        new GeneralAction(
            'GeneralActionTest',
            'call',
            new Map<String, Object>{
                'Bartolo' => 'Super Colon'}).publish();

        Test.stopTest();

        System.assertEquals(
            [SELECT LastName FROM Lead][0].LastName,
            'Super Colon',
            'The last name was not updated.');
    }

    public Object call(
        String action, 
        Map<String, Object> args) 
    {
        Lead l = [SELECT LastName FROM Lead][0];
        l.LastName = (String)args.get('Bartolo');
        update l;

        return null;
    }

}