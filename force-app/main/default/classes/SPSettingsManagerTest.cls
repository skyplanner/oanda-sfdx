/**
 * @File Name          : SPSettingsManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/17/2023, 8:17:28 PM
**/
@IsTest
private without sharing class SPSettingsManagerTest {

	public static final String SP_FAKE_ONLY_FOR_TEST_SETTING_NAME = 'SP_FAKE_ONLY_FOR_TEST_SETTING_NAME';

	@IsTest
	static void test() {
		SP_General_Setting__mdt firstValidSetting = getFirstValidSetting();
		String validDevName = firstValidSetting.DeveloperName;
		String validValue = firstValidSetting.Value__c;
		Test.startTest();
		// getSetting
		String result1 = 
			SPSettingsManager.getSetting(validDevName);
		String result2 = 
			SPSettingsManager.getSetting(SP_FAKE_ONLY_FOR_TEST_SETTING_NAME);
		// getSettingOrDefault
		String result3 = 
			SPSettingsManager.getSettingOrDefault(validDevName, 'SP_FAKE_VLAUE_ONLY_FOR_TEST');
		String result4 = 
			SPSettingsManager.getSettingOrDefault(SP_FAKE_ONLY_FOR_TEST_SETTING_NAME, 'abcd');
		// checkSetting
		Boolean result5 = 
			SPSettingsManager.checkSetting(SP_FAKE_ONLY_FOR_TEST_SETTING_NAME);
		// getSettingAsSet (also test getSettingAsList)
		Set<String> result6 = 
			SPSettingsManager.getSettingAsSet(validDevName);
		Set<String> result7 = 
			SPSettingsManager.getSettingAsSet(SP_FAKE_ONLY_FOR_TEST_SETTING_NAME);
		Integer result8 = 
			SPSettingsManager.getSettingAsInt(SP_FAKE_ONLY_FOR_TEST_SETTING_NAME);
		Test.stopTest();
		// getSetting results
		System.assertEquals(validValue, result1, 'Invalid value');
		System.assertEquals(null, result2, 'Invalid value');
		// getSettingOrDefault results
		System.assertEquals(validValue, result3, 'Invalid value');
		System.assertEquals('abcd', result4, 'Invalid value');
		// checkSetting results
		System.assertEquals(false, result5, 'Invalid value');
		// check getSettingAsSet results
		System.assertEquals(false, result6.isEmpty(), 'Invalid value');
		System.assertEquals(true, result7.isEmpty(), 'Invalid value');
		// check getSettingAsInt result
		System.assertEquals(0, result8, 'Invalid value');
	}

	static List<SP_General_Setting__mdt> getFirstSetting() {
		List<SP_General_Setting__mdt> generalSettingsList = [
			SELECT 
				DeveloperName, 
				Value__c 
			FROM SP_General_Setting__mdt
			LIMIT 1
		];
		return generalSettingsList;
	}

	static SP_General_Setting__mdt getFirstValidSetting() {
		List<SP_General_Setting__mdt> generalSettingsList = getFirstSetting();
		System.assertEquals(
			false, 
			generalSettingsList.isEmpty(), 
			'At least one record in SP_General_Setting__mdt is needed to test this class'
		);
		SP_General_Setting__mdt firstSetting = generalSettingsList[0];
		// check is the setting has an "org version"
		String orgSettingKey = 
			SPSettingsManager.getOrgSettingKey(firstSetting.DeveloperName);
		SP_General_Setting__mdt orgSetting = 
			SP_General_Setting__mdt.getInstance(orgSettingKey);
		if (orgSetting != null) {
			return orgSetting;
		}
		// else...
		return firstSetting;
	}
	
}