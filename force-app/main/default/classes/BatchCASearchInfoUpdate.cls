/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCASearchInfoUpdate implements
    Database.Batchable<sObject>, Database.AllowsCallouts,Database.RaisesPlatformEvents, BatchReflection {
    public String query;
    public List<String> filterList;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchCASearchInfoUpdate(String filter) {
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c, Division_Name__c, ' +
        '   Search_Profile__c, Fuzziness__c, Search_Parameter_Birth_Year__c, Total_Hits__c, ' +
        '   Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c, ' +
        '   fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c ' +
        'FROM Comply_Advantage_Search__c ' +
        'WHERE ' +
        (String.isNotBlank(filter) ? filter : '');
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c, Division_Name__c, ' +
        '   Search_Profile__c, Fuzziness__c, Search_Parameter_Birth_Year__c, Total_Hits__c, ' +
        '   Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c, ' +
        '   fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c ' +
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public BatchCASearchInfoUpdate(String filter, List<String> filterList) {
        query = 
        'SELECT Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c, Division_Name__c, ' +
        '   Search_Profile__c, Fuzziness__c, Search_Parameter_Birth_Year__c, Total_Hits__c, ' +
        '   Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c, ' +
        '   fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c ' +
        'FROM Comply_Advantage_Search__c ' +
        'WHERE ' +
        (String.isNotBlank(filter) ? filter : '');

        this.filterList = filterList;

        System.debug('query: ' + query);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        System.debug('scope: ' + scope);

        CASearchDetailsManager manager = new CASearchDetailsManager(scope);
        manager.startUpdating();
    }

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}