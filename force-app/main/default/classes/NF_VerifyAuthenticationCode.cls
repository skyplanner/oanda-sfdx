/**
 * Created by NeuraFlash on 11/18/21
 * Summary: Checks the fxAccount for the provided code
 * Details:
 *  1. Code provided from nfchat__Bot_Session__c
 *  2. Code checked AGAINST fxAccount__c
 *  3. Current time checked against fxAccount__c.Authorization_Code_Expires_On__c
 */

global without sharing class NF_VerifyAuthenticationCode  implements nfchat.DataAccessClass{
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
        Map<String, String> eventParams = new Map<String, String>();

        //try {
            Map<String, Object> mapSession = (Map<String, Object>) JSON.deserializeUntyped(aiServiceResponse);
            String chatSessionId = (String)mapSession.get('sessionId');  // Get Session ID for case lookup
            
            nfchat__Chat_Log__c currentChatLog = [
                SELECT nfchat__Session_Id__c, nfchat__Email__c, Auth_Answer__c
                FROM nfchat__Chat_Log__c
                WHERE nfchat__Session_Id__c =: chatSessionId
            ];

            // Get email for account lookup, and code to check
            String chatSessionEmail = currentChatLog?.nfchat__Email__c;
            String chatSessionAuthAnswer = currentChatLog?.Auth_Answer__c;

            // Find fxAccount based on email
            List<fxAccount__c> fxAccounts = [
                SELECT Id, Name, Email__c, Account__r.PersonEmail, Authentication_Code__c, Authentication_Code_Expires_On__c  
                FROM fxAccount__c
                WHERE Account__r.PersonEmail = :chatSessionEmail
            ];
            

            if (fxAccounts.size() > 0) {
                fxAccount__c accountToCheck = fxAccounts[0];
                
                System.debug('>> NF_VerifyAuthenticationCode: Found account= ' + fxAccounts);
                
                // Get case to update
                Case[] caseToUpdate = [ 
                    SELECT id, CaseNumber, User_Authenticated__c
                    FROM case
                    WHERE nfchat__Chat_Log__r.nfchat__Session_Id__c = :chatSessionId 
                ];

                System.debug('>> NF_VerifyAuthenticationCode: Searched for cases= ' + caseToUpdate);
                
                Boolean isCodeValid = AuthenticationCodeGenerator.validateAuthenticationCode(
                                        chatSessionAuthAnswer, 
                                        accountToCheck.Id, 
                                        'Authentication_Code__c', 
                                        'Authentication_Code_Expires_On__c');               
                
                if (isCodeValid) {
                    System.debug('>> NF_VerifyAuthenticationCode: CODE IS VALID!');
                    eventParams.put('result', 'AUTH_RESULT_AUTH');      // send event to dialogflow
                } else {
                    System.debug('>> NF_VerifyAuthenticationCode: CODE HAS BEEN REJECTED');
                    eventParams.put('result', 'AUTH_RESULT_UNAUTH');    // send event to dialogflow
                }
                
                if (caseToUpdate.size() > 0) {
                    System.debug('>> NF_VerifyAuthenticationCode: Setting code on case');
                    //updateCase(caseToUpdate[0].CaseNumber, isCodeValid); - do not update case, this will be done later in NF_SetAuthenticationOnCase
                }
                else {
                    System.debug('>> NF_VerifyAuthenticationCode: NO CASE TO UPDATE');
                }
            }
        // }
        // catch(Exception ex){
        //     System.debug('>> Exception in NF_VerifyAuthenticationCode > processRequest : ' + ex.getMessage());
        //     System.debug('>>        at Line Number: ' + ex.getLineNumber());
        //     System.debug('>>        stack trace:    ' + ex.getStackTraceString());
        //     return null;
        // }
        return JSON.serialize(eventParams);
    }
    
    private static void updateCase (String selectedCaseNumber, Boolean authValue) {
        Case caseToUpdate = [ SELECT User_Authenticated__c FROM Case WHERE CaseNumber =: selectedCaseNumber];
        caseToUpdate.User_Authenticated__c = authValue;
        update caseToUpdate;
    }
}