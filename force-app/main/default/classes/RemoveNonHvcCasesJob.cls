/**
 * @File Name          : RemoveNonHvcCasesJob.cls
 * @Description        :
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/18/2024, 11:58:24 PM
**/
public without sharing class RemoveNonHvcCasesJob implements Queueable {

	public void execute(QueueableContext context) {
		System.debug('RemoveNonHvcCasesJob -> execute');
		System.attachFinalizer(new JobFinalizer());
		ExceptionTestUtil.execute();
		RemoveNonHvcCasesCmd removeCmd = new RemoveNonHvcCasesCmd();
		removeCmd.execute();
		nextIteration(removeCmd.completed);
	}

	@TestVisible
	Boolean nextIteration(Boolean completed) {
		System.debug('RemoveNonHvcCasesJob -> nextIteration');
		Boolean result = false;

		if (completed == false) {
			SPEnqueueJobProcess enqueueJobProcess = 
				new SPEnqueueJobProcess(new RemoveNonHvcCasesJob());
			enqueueJobProcess.enqueueIfNoTesting();
			result = true;
		}

		return result;
	}

	// *************************************************************

	public class JobFinalizer extends SPBaseFinalizer {

		public override void onError(Exception ex) {
			System.debug('RemoveNonHvcCasesJob.JobFinalizer -> onError');
			Boolean isLogException = (ex instanceof LogException);
			if (isLogException == false) {
				Logger.exception(
					JobFinalizer.class.getName(), // category
					ex // ex
				);
			}
		}
	}

}