/**
 * Test class: HelpPortalHomeCtrl
 * @author Fernando Gomez
 * @version 1.0
 * @since April 1st, 2019
 */
@isTest
private class HelpPortalHomeCtrlTest {
	/**
	 * global HelpPortalHomeCtrl()
	 */
	@isTest 
	static void construction() {
		HelpPortalHomeCtrl c = new HelpPortalHomeCtrl();
		System.assert( c != null);
	}

	/**
	 * global static List<FAQ> search(String language, String searchText)
	 */
	@isTest 
	static void search() {
		System.assertEquals(0, HelpPortalHomeCtrl.search('en_US', 'some text', 'OANDA_Canada').size());
	}

	/**
	 * global static void submitRating(Id articleId,
	 *		String overallRating, String ranking, String comments)
	 */
	@isTest 
	static void submitRating() {
		FAQ__kav f;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question'
		);
		insert f;

		HelpPortalHomeCtrl.submitRating(f.Id, 'Like', '5', null, 'en_US');
		System.assertEquals(1, [
			SELECT COUNT()
			FROM FaqRating__c
		]);
	}

	@isTest 
	static void testCheckCountry() {
	   	PageReference prHome = Page.HelpPortalHome;
		Test.setCurrentPage(prHome);
	    Test.startTest();
		HelpPortalHomeCtrl ctrl = new HelpPortalHomeCtrl();
		PageReference redirectPage = ctrl.checkCountry();
		Test.stopTest();	
		System.assertEquals(null, redirectPage);
		System.assertEquals(false, ctrl.countrySel);
	}

	@isTest 
	static void testCheckInvalidCountry() {
		PageReference prHome = Page.HelpPortalHome;
		Cookie cookie_country = new Cookie('prefered_country', 'XXXXXXXXXXXX', null, -1, false);
		Cookie cookie_language = new Cookie('prefered_language', 'fr', null, -1, false);
		prHome.setCookies(new Cookie[]{cookie_country,cookie_language});
		Test.setCurrentPage(prHome);
		Boolean exceptionFired = false;
		Test.startTest();
		HelpPortalHomeCtrl ctrl = new HelpPortalHomeCtrl();
		PageReference redirectPage = null;
		try {
			redirectPage = ctrl.checkCountry();
		} catch (HelpPortalException ex) {
			exceptionFired = true;
		}
		Test.stopTest();	
		System.assertEquals(null, redirectPage);
		System.assertEquals(true, exceptionFired);
	}

	@isTest 
	static void testUsingCookies() {
	   	PageReference prHome = Page.HelpPortalHome;
		Cookie cookie_country = new Cookie('prefered_country', 'CA', null, -1, false);
		Cookie cookie_language = new Cookie('prefered_language', 'fr', null, -1, false);
		prHome.setCookies(new Cookie[]{cookie_country,cookie_language});
		Test.setCurrentPage(prHome);
	    Test.startTest();
		HelpPortalHomeCtrl ctrl = new HelpPortalHomeCtrl();
		PageReference redirectPage = ctrl.checkCountry();
		Test.stopTest();	
		System.assertEquals(null, redirectPage);
		System.assertEquals(true, ctrl.countrySel);
		System.assertEquals('fr', ctrl.language);
	}

	@isTest 
	static void testUsingCookiesRegion() {
	   	PageReference prHome = Page.HelpPortalHome;
		Cookie cookie_country = new Cookie('prefered_country', 'CA', null, -1, false);
		Cookie cookie_region = new Cookie('prefered_region', 'FL', null, -1, false); //Wrong Region
		Cookie cookie_language = new Cookie('prefered_language', 'fr', null, -1, false);
		prHome.setCookies(new Cookie[]{cookie_country,cookie_language,cookie_region});
		Test.setCurrentPage(prHome);
	    Test.startTest();
		HelpPortalHomeCtrl ctrl = new HelpPortalHomeCtrl();
		PageReference redirectPage = ctrl.checkCountry();
		Test.stopTest();	
		System.assertEquals(null, redirectPage);
		System.assertEquals(true, ctrl.countrySel);
		System.assertEquals('fr', ctrl.language);
	}


	@isTest 
	static void testWelcome() {
	   	PageReference prHome = Page.HelpPortalWelcome;
		   prHome.getParameters().put('division', 'OANDA_Canada');		 
		Test.setCurrentPage(prHome);
	    Test.startTest();
		HelpPortalWelcomeCtrl ctrl = new HelpPortalWelcomeCtrl();
		Test.stopTest();	
		System.assert(ctrl.mostVisited != null);
	}
}