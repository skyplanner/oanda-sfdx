/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 01-23-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiAuditLog {
    @AuraEnabled
    public String action {get; set;}

    @AuraEnabled
    public Integer changeset_id {get; set;}
    
    @AuraEnabled
    public String createdOn {get; set;} //datetime
    
    @AuraEnabled
    public String description {get; set;}

    @AuraEnabled
    public String field {get; set;}

    @AuraEnabled
    public String new_value {get; set;}

    @AuraEnabled
    public String old_value {get; set;}

    @AuraEnabled
    public String table {get; set;}

    @AuraEnabled
    public Integer timestamp {get; set;}
    
    @AuraEnabled
    public String user {get; set;}
    
    @AuraEnabled
    public String tableName {
        get {
            return String.isBlank(table)
                ? table
                : table.toLowerCase().remove('fxschema.');
        }
    }
}