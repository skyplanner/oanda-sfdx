/**
 * Tests: ComplyAdvantageEntitiesDownloader
 * @author Fernando Gomez
 * @since 11/15/2021
 */
@isTest
private class ComplyAdvantageEntitiesDownloaderTest {
	/**
	 * Tests:
	 * public void upload(
	 *		List<String> entityIds,
	 *		ComplyAdvantageKeyInformation template)
	 */
	@isTest
	private static void upload_good() {
		CalloutMock mock;
		ComplyAdvantageEntitiesDownloader d;
		ComplyAdvantageKeyInformation template;

		Comply_Advantage_Search__c ca = new Comply_Advantage_Search__c(
			Search_Id__c = '123456',
			Search_Reference__c = 'savasv_snaladkvcsv'
		);
		insert ca;

		mock = new CalloutMock(
			200,
			'{' +
				'"code": 200,' +
				'"status": "error"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageEntitiesDownloader(ca);

		template = new ComplyAdvantageKeyInformation();
		template.match_status = 'match_status';
		template.risk_level = 'risk_level';
		template.is_whitelisted = true;
		
		d.upload(new List<String> { '123456', '5649879' }, template);
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public void upload(
			List<String> entityIds,
			ComplyAdvantageKeyInformation template)
	 */
	@isTest
	private static void upload_fail() {
		CalloutMock mock;
		ComplyAdvantageEntitiesDownloader d;
		ComplyAdvantageKeyInformation template;

		Comply_Advantage_Search__c ca = new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);
		insert ca;
		
		mock = new CalloutMock(
			400,
			'{' +
				'"code": 400,' +
				'"status": "error"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageEntitiesDownloader(ca);

		try {
			template = new ComplyAdvantageKeyInformation();
			d.upload(new List<String> { '123456', '5649879' }, template);
			System.assert(false, 'should fail');
		} catch (Exception ex) {
			// all good
		}
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public void download()
	 */
	@isTest
	private static void download_good() {
		CalloutMock mock;
		ComplyAdvantageEntitiesDownloader d;
		ComplyAdvantageKeyInformation template;
		Comply_Advantage_Search__c cas;

		cas = new Comply_Advantage_Search__c(
			Search_Id__c = '123456',
			Search_Reference__c = 'savasv_snaladkvcsv'
		);
		insert cas;

		mock = new CalloutMock(
			200,
			'{' +
				'"code": 200,' +
				'"status": "success",' +
				'"content": [' +
					'{' +
						'"id": "X1I12UGFJPGXCAW",' +
						'"last_updated_utc": "2021-09-19T15:38:52Z",' +
						'"created_utc": "2018-08-31T13:06:59Z",' +
						'"key_information": {' +
							'"name": "Fernán Gómez",' +
							'"entity_type": "person",' +
							'"types": [' +
								'"adverse-media",' +
								'"adverse-media-violent-crime",' +
								'"adverse-media-general"' +
							'],' +
							'"match_types": [' +
								'"fuzzy_match"' +
							'],' +
							'"sources": [' +
								'"complyadvantage-adverse-media"' +
							'],' +
							'"aka": [' +
								'{"name": "Fernán Gómez"},' +
								'{"name": "Fernan Gomez"}' +
							'],' +
							'"date_of_birth": null,' +
							'"dates_of_birth": [],' +
							'"match_status": "potential_match",' +
							'"risk_level": null,' +
							'"is_whitelisted": false,' +
							'"primary_country": null,' +
							'"country_names": "Brazil, Colombia, Spain"' +
						'},' +
						'"uncategorized": {' +
							'"keywords": [],' +
							'"assets": [],' +
							'"match_types": [' +
								'"fuzzy_match"' +
							']' +
						'}' +
					'}' +
				']' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageEntitiesDownloader(cas);
		ComplyAdvantageEntitiesDownloader.EntityDownloadResult result = d.getEntities();
		System.assertEquals(true, result.entities.size() > 0);
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public ComplyAdvantageSearchHit getEntity(String entityId)
	 */
	@isTest
	private static void getEntity_fail() {
		CalloutMock mock;
		ComplyAdvantageEntitiesDownloader d;
		ComplyAdvantageKeyInformation template;
		ComplyAdvantageSearchHit entity;

		Comply_Advantage_Search__c ca = new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);
		insert ca;

		mock = new CalloutMock(
			400,
			'{' +
				'"code": 400,' +
				'"status": "error"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageEntitiesDownloader(ca);

		try {
			template = new ComplyAdvantageKeyInformation();
			entity = d.getEntity('5649879');
			System.assert(false, 'should fail');
		} catch (Exception ex) {
			// all good
		}
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public ComplyAdvantageSearchHit getEntity(String entityId)
	 */
	@isTest
	private static void getEntity_good() {
		CalloutMock mock;
		ComplyAdvantageEntitiesDownloader d;
		ComplyAdvantageKeyInformation template;
		ComplyAdvantageSearchHit entity;

		Comply_Advantage_Search__c ca = new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);
		insert ca;

		mock = new CalloutMock(
			200,
			'{' +
				'"code": 200,' +
				'"status": "success",' +
				'"content": [' +
					'{' +
						'"id": "UU1D2RIC4HSUVYI",' +
						'"key_information": {' +
							'"name": "Maria Fernanda Gómez",' +
							'"entity_type": "person",' +
							'"types": [' +
								'"pep"' +
							'],' +
							'"match_types": [' +
								'"fuzzy_match"' +
							'],' +
							'"aka": [' +
								'{' +
									'"name": "Fernanda Gomez"' +
								'}' +
							'],' +
							'"dates_of_birth": [],' +
							'"match_status": "false_positive",' +
							'"risk_level": null,' +
							'"is_whitelisted": false,' +
							'"country_names": "Colombia, Honduras, Mexico",' +
							'"designation": "N",' +
							'"associates": []' +
						'},' +
						'"uncategorized": {' +
							'"keywords": [],' +
							'"media": [' +
								'{' +
									'"date": "2021-06-03T00:00:00Z",' +
									'"snippet": "No obstante,",' +
									'"title": "srvadv",' +
									'"url": "https"' +
								'}' +
							'],' +
							'"match_types": [' +
								'"fuzzy_match"' +
							'],' +
							'"related": [],' +
							'"fields": []' +
						'},' +
						'"full_listing": {' +
							'"pep": {' +
								'"complyadvantage": {' +
									'"source": "complyadvantage",' +
									'"aml_types": [' +
										'"pep"' +
									'],' +
									'"name": "ComplyAdvantage PEP Data",' +
									'"url": "",' +
									'"count": 1000000,' +
									'"disclose_sources": false,' +
									'"data": [' +
										'{' +
											'"name": "Country",' +
											'"source": "complyadvantage",' +
											'"value": "Colombia"' +
										'}' +
									'],' +
									'"listing_started_utc": null,' +
									'"listing_ended_utc": null' +
								'}' +
							'}' +
						'}' +
					'}' +
				']' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageEntitiesDownloader(ca);
		template = new ComplyAdvantageKeyInformation();
		entity = d.getEntity('UU1D2RIC4HSUVYI');
		System.assertEquals(1, entity.uncategorized.media.size());
		Test.stopTest();
	}
}