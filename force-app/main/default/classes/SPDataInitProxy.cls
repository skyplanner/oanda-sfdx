/**
 * @File Name          : SPDataInitProxy.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/11/2023, 10:13:23 PM
**/
public interface SPDataInitProxy {

	String getObjectKey();

	String getUniqueName();

	Integer getUniqueIndex();

	ID insertObj(SObject newObject);

	ID getRecordTypeId(String recTypeDevName);

	void setRecordTypeId(String recTypeDevName);

	void setFieldValue(
		Schema.SObjectField fieldToken,
		Object newValue
	);

	void setFieldValues(Map<String,Object> newObjValues);

	void setObjectOption(
		String optionName,
		Object newValue
	);

	Object getObjectOption(
		String optionName,
		Object defaultValue
	);

	Object getFieldValue(
		Schema.SObjectField fieldToken,
		Boolean required
	);

	void checkRequiredFieldValue(Schema.SObjectField fieldToken);

}