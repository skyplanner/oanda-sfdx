/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 07-08-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class ComplyAdvantageSearchAfterTrgHandler extends ComplyAdvantageSearchTriggerHandler
{
    public ComplyAdvantageSearchAfterTrgHandler(boolean trgInsert,
                                                boolean trgUpdate,
                                                list<Comply_Advantage_Search__c> triggerNew,
                                                Map<Id, Comply_Advantage_Search__c> triggerOldMap) 
    {
        super(trgInsert,trgUpdate,false, true, triggerNew, triggerOldMap);

        populateRelatedFxaccounts();
    }
	public void onAfterInsert()
    {
        for(Comply_Advantage_Search__c search : trgNew)
        {
            markFxAcCAMonitorFlagOnInsert(search);
            populateMatchStatus(search, null);
        }

        downloadReport();
	} 
	public void onAfterUpdate()
    {
        for(Comply_Advantage_Search__c searchAfterUpdate : trgNew)
        {
            Comply_Advantage_Search__c searchBeforeUpdate = trgOldMap.get(searchAfterUpdate.Id);

            markFxAcCAMonitorFlagOnUpdate(searchAfterUpdate, searchBeforeUpdate);

            populateMatchStatus(searchAfterUpdate, searchBeforeUpdate);
        }
	} 
	private void markFxAcCAMonitorFlagOnInsert(Comply_Advantage_Search__c search)
    {
        if(search.Is_Monitored__c &&
           string.isBlank(search.Entity_Contact__c) && 
           string.isNotBlank(search.fxAccount__c) && 
           relatedFxAccountsMap.containsKey(search.fxAccount__c))
        {
            fxAccount__c fxa = relatedFxAccountsMap.get(search.fxAccount__c);

            if(search.Is_Alias_Search__c)
            {
                fxa.Is_CA_Alias_Search_Monitored__c = true;
                triggerDataHandler.updateRecord(fxa);
            }
            if(!search.Is_Alias_Search__c)
            {
                fxa.Is_CA_Name_Search_Monitored__c = true;
                triggerDataHandler.updateRecord(fxa);
            }
        }
	} 
    private void markFxAcCAMonitorFlagOnUpdate(Comply_Advantage_Search__c searchAfterUpdate, Comply_Advantage_Search__c searchBeforeUpdate)
    {
        if(string.isBlank(searchAfterUpdate.Entity_Contact__c) && 
           string.isNotBlank(searchAfterUpdate.fxAccount__c) && 
           relatedFxAccountsMap.containsKey(searchAfterUpdate.fxAccount__c))
        {
            fxAccount__c fxa = relatedFxAccountsMap.get(searchAfterUpdate.fxAccount__c);

            if(searchAfterUpdate.Is_Monitored__c != searchBeforeUpdate.Is_Monitored__c)
            {
                // Yaneivys Gutierrez - fixing SP-11536 - 07-08-2022
                if (searchAfterUpdate.Is_Monitored__c) {
                    if (searchAfterUpdate.Is_Alias_Search__c) {
                        fxa.Is_CA_Alias_Search_Monitored__c = true;
                        triggerDataHandler.updateRecord(fxa);
                    }
                    else {
                        fxa.Is_CA_Name_Search_Monitored__c = true;
                        triggerDataHandler.updateRecord(fxa);
                    }
                }
                else { // Updated to not monitored
                    boolean hasMonitoredNameSearch = false;
                    boolean hasMonitoredAliasSearch = false;

                    //check all other searches
                    for (Comply_Advantage_Search__c monitoredSearch : fxa.Comply_Advantage_Searches__r) {
                        if (monitoredSearch.Entity_Contact__c == null && monitoredSearch.Is_Monitored__c) {
                            if (monitoredSearch.Is_Alias_Search__c) {
                                hasMonitoredAliasSearch = true;
                            } else {
                                hasMonitoredNameSearch = true;
                            }
                        }
                    }
                    if (fxa.Is_CA_Name_Search_Monitored__c != hasMonitoredNameSearch) {
                        fxa.Is_CA_Name_Search_Monitored__c = hasMonitoredNameSearch;
                        triggerDataHandler.updateRecord(fxa);
                    }
                    if (fxa.Is_CA_Alias_Search_Monitored__c != hasMonitoredAliasSearch) {
                        fxa.Is_CA_Alias_Search_Monitored__c = hasMonitoredAliasSearch;
                        triggerDataHandler.updateRecord(fxa);
                    }
                }
            }
        }
	}

    private void populateMatchStatus(Comply_Advantage_Search__c searchAfterUpdate, Comply_Advantage_Search__c searchBeforeUpdate){
        if(searchAfterUpdate.fxAccount__c!=null && (searchBeforeUpdate==null || (searchBeforeUpdate.fxAccount__c != searchAfterUpdate.fxAccount__c ||
            searchBeforeUpdate.Match_Status__c != searchAfterUpdate.Match_Status__c) && relatedFxAccountsMap.containsKey(searchAfterUpdate.fxAccount__c))) {

            fxAccount__c fxa = relatedFxAccountsMap.get(searchAfterUpdate.fxAccount__c);
            if (String.isBlank(fxa.CA_Match_Status__c)) {
                fxa.CA_Match_Status__c = searchAfterUpdate.Match_Status__c;
                triggerDataHandler.updateRecord(fxa);
            }
        }
    }

    private void downloadReport()
    {

	} 

    public void commitTriggerChanges()
    {
        triggerDataHandler.commitTriggerChanges();
    }

}