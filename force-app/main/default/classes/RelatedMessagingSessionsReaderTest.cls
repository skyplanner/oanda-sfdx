/**
 * @File Name          : RelatedMessagingSessionsReaderTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 3:49:48 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/14/2024, 12:34:35 PM   aniubo     Initial Version
 **/
@isTest
@SuppressWarnings('PMD.AvoidHardcodingId ')
private class RelatedMessagingSessionsReaderTest {
	@isTest
	private static void testConvertToInQuery() {
		// Test data setup

		String id = UserInfo.getUserId();
		String id2 = UserInfo.getProfileId();
		// Actual test
		Test.startTest();
		RelatedMessagingSessionsReader reader = getReader();
		String query = reader.convertToInQuery(new Set<Id>{ id, id2 });
		String pattern = '\'\'{0}\'\',\'\'{1}\'\'';

		System.assertEquals(
			String.format(pattern, new List<String>{ id, id2 }),
			query,
			'Should convert to in query'
		);

		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testGetCount() {
		// Test data setup

		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsReader reader = getReader();
		Integer count = reader.getCount(UserInfo.getUserId() ,'OwnerId', new Set<Id>());
		Test.stopTest();
		System.assertEquals(0, count, 'Should return 0');
		// Asserts
	}

	@isTest
	private static void testGetRelatedMessagingSessions() {
		// Test data setup

		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsReader reader = getReader();
		List<MessagingSession> sessions = reader.getRelatedMessagingSessions(
			new Set<Id>(),
			new List<String>{ 'EndUserAccountId' },
			'EndUserAccountId',
			10,
			1
		);
		Test.stopTest();
		System.assertEquals(
			sessions.size(),
			0,
			'No Sessions should be returned'
		);
		// Asserts
	}

	@isTest
	private static void testGetMessagingRelatedEntity() {
		// Test data setup

		Id idx;
		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsReader reader = getReader();
		MessagingSessionRelatedEntity relatedEntity = reader.getMessagingRelatedEntity(
			idx
		);

		Test.stopTest();
		System.assertEquals(null, relatedEntity, 'Should return null');
		// Asserts
	}

	@isTest
	private static void testGetMessagingEndUsers() {
		// Test data setup

		// Actual test
		Test.startTest();
		IRelatedMessagingSessionsReader reader = getReader();
		Set<Id> endUserIds = reader.getMessagingEndUsers(null);

		Test.stopTest();
		System.assertEquals(
			endUserIds.size(),
			0,
			'No MessagingEndUsers Ids should be returned'
		);
		// Asserts
	}

	@isTest
	private static void testGetMessagingRelatedEntity2() {
		// Test data setup

		// Actual test
		Test.startTest();
		RelatedMessagingSessionsReader reader = getReader();
		MessagingSession messagingSession = new MessagingSession();
		MessagingSessionRelatedEntity relatedEntity = reader.getMessagingRelatedEntity(
			messagingSession
		);
		System.assertEquals(null, relatedEntity, 'Should return null');
		messagingSession.LeadId = '00Q8F000004biGCUAY';
		relatedEntity = reader.getMessagingRelatedEntity(messagingSession);
		System.assertEquals(
			true,
			relatedEntity != null,
			'relatedEntity Must not be null'
		);
		System.assertEquals(
			messagingSession.LeadId,
			relatedEntity.recordId,
			'EndUserAccountId Must be equal to relatedEntity.recordId'
		);
		System.assertEquals(
			false,
			relatedEntity.isAccountType,
			'relatedEntity.isAccountType must be false'
		);
		Test.stopTest();

		// Asserts
	}
	private static RelatedMessagingSessionsReader getReader() {
		return new RelatedMessagingSessionsReader();
	}
}