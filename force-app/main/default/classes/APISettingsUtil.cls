/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 06-09-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class APISettingsUtil {

    private final static String LOGGER_CATEGORY = 'no credential in org';
    private final static String LOGGER_MSG = 'credential do not exist in org!!!!';

    public static APISettings getApiSettingsByName(string apiSettingsName) {
        String credentialSuffix = Util.getValidCredentialSuffix();

        String localApiSettingName = apiSettingsName + credentialSuffix;

        API_Settings__mdt apiSettingsMtd = API_Settings__mdt.getInstance(localApiSettingName);

        if (apiSettingsMtd == null) {
            String errMsg = 'Current credential metadata do not exist: '
                    + localApiSettingName
                    + ' please reach to admins to review the integration credentials and configuration';
            Logger.error(
                    LOGGER_MSG,
                    LOGGER_CATEGORY,
                    errMsg);
            throw new MissingCredentialException(errMsg);
        }

        APISettings apiSettingDTO = new APISettings(apiSettingsMtd);

        return apiSettingDTO;
    }
    
    public class MissingCredentialException extends Exception {} 
}