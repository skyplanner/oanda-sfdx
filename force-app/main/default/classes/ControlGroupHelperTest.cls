@isTest(seeAllData=false)
public class ControlGroupHelperTest 
{
     static TestMethod void testControlGroup() 
     {
        User SYSTEM_USER = UserUtil.getSystemUserForTest();
        System.runAs(SYSTEM_USER) 
        {
            String orgId = UserInfo.getOrganizationId();
            String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;

		    User retentionHouseUser = new User(
		   		LastName='Retention House User', 
		   		Username= uniqueName + 'rhouseUser@oanda.com', 
		   		Email='systemuser.fxtrade@oanda.com', 
		   		Alias='rhous', 
		   		ProfileId=[SELECT Id FROM Profile WHERE Name = 'Forex Specialist Team Lead'].Id,
		   		TimeZoneSidKey='GMT', 
		   		LocaleSidKey='en_US', 
		   		EmailEncodingKey='ISO-8859-1', 
		   		LanguageLocaleKey='en_US',
		   		IsActive=true);
		    	
		    insert retentionHouseUser;
            
            Settings__c settings = new Settings__c(Name='Default', Enable_Control_Group__c=true);
            insert settings;
            
            Country_Setting__c[] countrySettings= new Country_Setting__c[]
            {
				new Country_Setting__c(Name='Canada', Group__c='Canada', ISO_Code__c='ca', Region__c='North America', Control_Group_Region__c='Americas', Zone__c='Americas', Risk_Rating__c='LOW'),
                new Country_Setting__c(Name='Anguilla', Group__c='United States', ISO_Code__c='ai', Region__c='Latin America & Caribbean', Control_Group_Region__c='Americas', Zone__c='Americas', Risk_Rating__c='LOW'),
                new Country_Setting__c(Name='Bahamas', Group__c='United States', ISO_Code__c='bs', Region__c='Latin America & Caribbean', Control_Group_Region__c='Americas', Zone__c='Americas', Risk_Rating__c='LOW'),
                new Country_Setting__c(Name='Thailand', Group__c='Rest of Asia', ISO_Code__c='sg', Region__c='East Asia & Pacific',Control_Group_Region__c='Rest of Asia', Zone__c='Americas', Risk_Rating__c='LOW'),
                new Country_Setting__c(Name='Botswana', Group__c='Europe', ISO_Code__c='bw', Region__c='Sub-Saharan Africa', Control_Group_Region__c='Rest of EMEA', Zone__c='EMEA', Risk_Rating__c='LOW')
            };
            insert countrySettings;
            
            Control_Group_Settings_New__c[] cgSettings = new Control_Group_Settings_New__c[]
            {
                new Control_Group_Settings_New__c(Name='Americas', Lead_Counter__c =0,  Lead_Allotment_Number__c = 2, Is_Enabled__c = true, Is_NTA__c =false),
                new Control_Group_Settings_New__c(Name='Thailand', Lead_Counter__c =0, Lead_Allotment_Number__c = 3, Is_Enabled__c = true, Is_NTA__c =false)    
            };
            insert cgSettings;

            Lead[] newLeads= new Lead[]
            {
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='user1@example.org', LastName='user1', Country = 'India'),
                    
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe1@example.org', LastName='joe1Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe2@example.org', LastName='joe2Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe3@example.org', LastName='joe3Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe4@example.org', LastName='joe4Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe5@example.org', LastName='joe5Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe6@example.org', LastName='joe6Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe7@example.org', LastName='joe7Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe8@example.org', LastName='joe8Schmoe', Country = 'Thailand'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe9@example.org', LastName='joe9Schmoe', Country = 'Thailand'),
                    
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='roe1@example.org', LastName='roe1Schmoe', Country = 'Canada'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='roe2@example.org', LastName='roe2Schmoe', Country = 'Anguilla'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='roe3@example.org', LastName='roe3Schmoe', Country = 'Bahamas'),
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='roe4@example.org', LastName='roe4Schmoe', Country = 'Botswana'),
                    
                new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='roe5@example.org', LastName='roe4Schmoe', Country = 'Malaysia')     
            };
            insert newLeads;
			
            Lead joe1 = [select OwnerId from Lead where Email='joe1@example.org' Limit 1];
            Lead joe3 = [select OwnerId from Lead where Email='joe3@example.org' Limit 1];
            Lead joe5 = [select OwnerId from Lead where Email='joe5@example.org' Limit 1];
            Lead joe6 = [select OwnerId from Lead where Email='joe6@example.org' Limit 1];
            Lead roe1 = [select OwnerId from Lead where Email='roe1@example.org' Limit 1];
            Lead roe2 = [select OwnerId from Lead where Email='roe2@example.org' Limit 1];
            Lead roe3 = [select OwnerId from Lead where Email='roe3@example.org' Limit 1];
            Lead roe4 = [select OwnerId from Lead where Email='roe2@example.org' Limit 1];
            Lead roe5 = [select OwnerId from Lead where Email='roe5@example.org' Limit 1];
            
            System.assertEquals(SYSTEM_USER.Id, joe1.OwnerId);
            System.assertEquals(retentionHouseUser.Id, joe3.OwnerId);
            System.assertEquals(SYSTEM_USER.Id, joe5.OwnerId);
            System.assertEquals(retentionHouseUser.Id, joe6.OwnerId);
            
            System.assertEquals(SYSTEM_USER.Id, roe1.OwnerId);
            System.assertEquals(retentionHouseUser.Id, roe2.OwnerId);
            System.assertEquals(SYSTEM_USER.Id, roe3.OwnerId);
            System.assertEquals(retentionHouseUser.Id, roe4.OwnerId);
           
            System.assertEquals(SYSTEM_USER.Id, roe5.OwnerId);
            
        }
     }
         
     static TestMethod void testSingaporeControlGroup() 
     {
        TestsUtil.forceJobsSync = true;
        
        User SYSTEM_USER = UserUtil.getSystemUserForTest();
        System.runAs(SYSTEM_USER) 
        {
            String orgId = UserInfo.getOrganizationId();
            String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;

		    User retentionHouseUser = new User(
		   		LastName='Retention House User', 
		   		Username= uniqueName + 'rhouseUser@oanda.com', 
		   		Email='systemuser.fxtrade@oanda.com', 
		   		Alias='rhous', 
		   		ProfileId=[ SELECT Id FROM Profile WHERE Name = 'Forex Specialist Team Lead' ].Id,
		   		TimeZoneSidKey='GMT', 
		   		LocaleSidKey='en_US', 
		   		EmailEncodingKey='ISO-8859-1', 
		   		LanguageLocaleKey='en_US',
		   		IsActive=true);	    	
		    insert retentionHouseUser;
            
            Settings__c settings = new Settings__c(Name='Default', Enable_Control_Group__c=true);
            insert settings;
            
            Control_Group_Settings_New__c[] cgSettings = new Control_Group_Settings_New__c[]
            {
                new Control_Group_Settings_New__c(Name='Singapore', Lead_Counter__c =0, Lead_Allotment_Number__c = 2, Is_Enabled__c = true, Is_NTA__c =true)    
            };
            insert cgSettings;
            
            Account[] newAccounts= new Account[]
            {
                new Account(LastName='test1', PersonMailingCountry = 'Singapore'),
                new Account(LastName='test2', PersonMailingCountry = 'Singapore')    
            };
            insert newAccounts;
            
            Opportunity[] newOpportunities= new Opportunity[]
            {
                new Opportunity(Name='test1', AccountId=newAccounts[0].Id, StageName='Before you begin', CloseDate=Date.today()),
                new Opportunity(Name='test2', AccountId=newAccounts[1].Id, StageName='Before you begin', CloseDate=Date.today())    
            };
            insert newOpportunities;
                       
            fxAccount__c[] newfxAccounts= new fxAccount__c[]
            {
                new fxAccount__c(Name='test1', RecordTypeId=RecordTypeUtil.getFxAccountLiveId(), Account__c=newAccounts[0].Id, 
                                 Opportunity__c=newOpportunities[0].Id, Division_Name__c='OANDA Corporate', Funnel_Stage__c = 'Traded', First_Trade_Date__c = Datetime.now().addDays(-1)),
                new fxAccount__c(Name='test2', RecordTypeId=RecordTypeUtil.getFxAccountLiveId(), Account__c=newAccounts[1].Id, 
                                 Opportunity__c=newOpportunities[1].Id, Division_Name__c='OANDA Corporate', Funnel_Stage__c = 'Traded', First_Trade_Date__c = Datetime.now().addDays(-1))    
            };
            insert newfxAccounts;
            
            ControlGroupHelper.assignToSingaporeControlGroup();
            
            Account[] cgAccounts = [select Id from Account where OwnerId = :retentionHouseUser.Id];
            
            system.assertEquals(1, cgAccounts.size());
            
            
        }
    }
}