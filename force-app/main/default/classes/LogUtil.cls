/**
 * Logs util class
 */
public class LogUtil {
    
    // Settings
    private LogSettings stts;

    /**
     * Constructor
     */
    public LogUtil() {
        stts = LogSettings.getInstance();
    }

    /**
     * Create a log platform event
     */
    public void createLog(Log l)
    {
        if(isCreateActive(l)) {
            // Create platform event
            EventBus.publish(l.getEvent());
        }
    }

    /**
     * Process log events
     * - Create logs
     * - Delete old logs from settings
     */
    public void processLogEvents(
        List<LogEvt__e> lEvents)
    {
        // Create logs
        createLogs(lEvents);
        // Delete old logs
        deleteOld();
    }

    /**
     * Get string details from
     * request and response
     */
    public String getDetails(
        HttpRequest request,
        HttpResponse response)
    {
        Map<String, Map<String, String>> details = 
            new Map<String, Map<String, String>>();

        if(response != null) {
            details.put(
                'response',
                new Map<String, String>{
                    'body' => response.getBody(),
                    'statusCode' =>
                        String.valueOf(response.getStatusCode()),
                    'status' => response.getStatus()
            });
        }

        if(request != null) {
            details.put(
                'request',
                new Map<String, String>{
                    'body' => request.getBody(),
                    'method' => request.getMethod(),
                    'endpoint' => request.getEndpoint()
            });
        }

        return Utilities.serialize(
            details, false);
    }

    /**
     * Get string details from
     * Database.Error
     */
    public String getDetails(
        String recordInfo, 
        Database.Error err)
    {
        Map<String, String> details = 
            new Map<String, String> {
                'message' => err.getMessage(),
                'statusCode' => String.valueOf(err.getStatusCode())};

        List<String> fields = err.getFields();

        if(fields != null && !fields.isEmpty()) {
            details.put('fields', String.join(fields, ','));
        }

        if(String.isNotBlank(recordInfo))
            details.put('recordId', recordInfo);

        return Utilities.serialize(
            details, false);
    }

    /**
     * Create a log from platform event published
     */
    private void createLogs(
        List<LogEvt__e> lEvents)
    {
        // Know if logs active
        if(!stts.isActive()) {
            return;
        }

        List<Log__c> logs =
            new List<Log__c>();

        // Create logs from platform events

        for(LogEvt__e evt :lEvents) {
            logs.add(
                new Log(evt).getRecord());
        }

        if(!logs.isEmpty()) {
            insert logs;
        }
    }

    /**
     * Delete old logs according settings
     */
    private void deleteOld() {
        List<Log__c> oldLogs =
            [SELECT Id 
                FROM Log__c
                WHERE CreatedDate < :stts.getOldDate()
                LIMIT 5000];
        
        if(!oldLogs.isEmpty()) {
            Database.delete(oldLogs, false);
        }
    }

    /**
     * Know if it is ok to create the log
     */
    private Boolean isCreateActive(Log l) {
        // Know if logs are active
        if(!stts.isActive()) {
            return false;
        }

        // If it is a debug and debug logs 
        // are deactivated
        if(l.isDebug() && !stts.isDebugActive()) {
            return false;
        }

        return true;
    }

}