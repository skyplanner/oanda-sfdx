/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-23-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchUpdAccFromFxAccTest {
    @isTest
    static void updateAccounts() {
        Account acc = TestDataFactory.getPersonAccount(true);
		fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);

        fxAcc.W8_W9_Status__c = 'testing';
        fxAcc.US_Shares_Trading_Enabled__c = true;
        update fxAcc;

        acc.W8_W9_Status__c = null;
        acc.US_Shares_Trading_Enabled__c = false;
        update acc;

        Test.StartTest();

        BatchUpdAccFromFxAcc b = new BatchUpdAccFromFxAcc(null);
        Id batchProcessId = Database.executeBatch(b);

        Test.stopTest();

        AsyncApexJob aaj = [
            SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
            FROM AsyncApexJob
            WHERE Id =: batchProcessId ];

        acc = [SELECT Id, W8_W9_Status__c, US_Shares_Trading_Enabled__c FROM Account WHERE Id = :acc.Id];

        System.assertEquals(true, aaj != null);
        System.assertEquals(1, aaj.TotalJobItems);
        System.assertEquals('testing', acc.W8_W9_Status__c);
        System.assertEquals(true, acc.US_Shares_Trading_Enabled__c);
    }
}