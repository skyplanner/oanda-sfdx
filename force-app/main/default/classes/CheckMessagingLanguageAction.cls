/**
 * @File Name          : CheckMessagingLanguageAction.cls
 * @Description        : Check if a language is a valid language 
 *                       for the Chatbot
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/14/2023, 2:55:09 PM
**/
public without sharing class CheckMessagingLanguageAction { 

	@InvocableMethod(label='Check Messaging Language' callout=false)
	public static List<Boolean> languageIsAvailable(List<String> languageCodes) {    
		try {
			ExceptionTestUtil.execute();
			List<Boolean> result = new List<Boolean>{ false };

			if (
				(languageCodes == null) ||
				languageCodes.isEmpty()
			) {
				return result;
			}
			// else...
			String languageCode = languageCodes[0];
			result[0] = 
				MessagingChatbotProcess.isLangAvailableForChatbot(languageCode);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CheckMessagingLanguageAction.class.getName(),
				ex
			);
		}
	} 

}