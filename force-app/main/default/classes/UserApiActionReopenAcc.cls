/**
 * User API 'Reopen Account' action manager class
 */
public class UserApiActionReopenAcc extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.reopenAccount();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiAction action = getAction(record);
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.accountClosed;
    }
}