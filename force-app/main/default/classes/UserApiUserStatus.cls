/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 03-03-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserStatus {
    public Integer account_locked {get; set;}

    public Integer agreements_outdated {get; set;}

    public Integer closed {get; set;}

    public Integer deposits_locked {get; set;}

    public Integer documents_approved {get; set;}

    public Integer email_validated {get; set;}

    public Integer interest_exempt {get; set;}

    public Integer leverage_locked {get; set;}

    public Integer password_expired {get; set;}

    public Integer piu_outdated {get; set;}

    public Integer registration_completed {get; set;}

    public Integer snail_mail_validated {get; set;}

    public Integer too_many_failed_logins {get; set;}

    public Integer tpa_authorization_locked {get; set;}

    public Integer trading_locked {get; set;}

    public Integer update_timestamp {get; set;}

    public String user_info_last_updated {get; set;}

    public Integer user_info_outdated {get; set;}

    public Integer withdrawals_locked {get; set;}

    public transient Boolean accountLocked {
        get {
            return account_locked == 1;
        }
    }
    
    public transient Boolean tradingLocked {
        get {
            return trading_locked == 1;
        }
    }
    
    public transient Boolean depositsLocked {
        get {
            return deposits_locked == 1;
        }
    }
    
    public transient Boolean withdrawalsLocked {
        get {
            return withdrawals_locked == 1;
        }
    }
    
    public transient Boolean tpaAuthorizationLocked {
        get {
            return tpa_authorization_locked == 1;
        }
    }
    
    public transient Boolean emailValidated {
        get {
            return email_validated == 1;
        }
    }
    
    public transient Boolean documentsApproved {
        get {
            return documents_approved == 1;
        }
    }
    
    public transient Boolean registrationCompleted {
        get {
            return registration_completed == 1;
        }
    }
    
    public transient Boolean accountClosed {
        get {
            return closed == 1;
        }
    }
    
    public transient Boolean allInterestEnabled {
        get {
            return interest_exempt == 0;
        }
    }
    
    public transient Boolean tooManyFailedLogins {
        get {
            return too_many_failed_logins == 1;
        }
    }
    
    public transient Boolean passwordExpired {
        get {
            return password_expired == 1;
        }
    }
    
    public transient Boolean agreementsOutdated {
        get {
            return agreements_outdated == 1;
        }
    }
    
    public transient Boolean leverageLocked {
        get {
            return leverage_locked == 1;
        }
    }
    
    public transient Boolean piuOutdated {
        get {
            return piu_outdated == 1;
        }
    }
    
    public transient Boolean snailMailValidated {
        get {
            return snail_mail_validated == 1;
        }
    }
}