/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-23-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchUpdAccFromFxAcc implements Database.RaisesPlatformEvents, BatchReflection,
    Database.Batchable<sObject>, Database.AllowsCallouts {
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchUpdAccFromFxAcc(String filter) {
        query = 'SELECT Id, W8_W9_Status__c, US_Shares_Trading_Enabled__c, Account__c, Account__r.W8_W9_Status__c, Account__r.US_Shares_Trading_Enabled__c ' +
        'FROM fxAccount__c ' +
        'WHERE W8_W9_Status__c != NULL OR US_Shares_Trading_Enabled__c = TRUE ' +
        (String.isNotBlank(filter) ? filter : '');
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, W8_W9_Status__c, US_Shares_Trading_Enabled__c, Account__c, Account__r.W8_W9_Status__c, Account__r.US_Shares_Trading_Enabled__c ' +
        'FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('start method: ' + Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope) {
        System.debug('scope: ' + scope);

        Set<Id> accIds = new Set<Id>();
        for (fxAccount__c fx : scope) {

            if (fx.Account__c != null &&
                (fx.W8_W9_Status__c != fx.Account__r.W8_W9_Status__c ||
                 fx.US_Shares_Trading_Enabled__c != fx.Account__r.US_Shares_Trading_Enabled__c)) {
                accIds.add(fx.Account__c);
            }
        }

        Map<Id, Account> accMap = new Map<Id, Account>([
            SELECT Id, W8_W9_Status__c, US_Shares_Trading_Enabled__c
            FROM Account
            WHERE Id IN :accIds
        ]);

        for (fxAccount__c fx : scope) {
            if (fx.Account__c != null &&
                (fx.W8_W9_Status__c != fx.Account__r.W8_W9_Status__c ||
                 fx.US_Shares_Trading_Enabled__c != fx.Account__r.US_Shares_Trading_Enabled__c)) { 
                    Account acc = accMap.get(fx.Account__c);
                    acc.W8_W9_Status__c = fx.W8_W9_Status__c;
                    acc.US_Shares_Trading_Enabled__c = fx.US_Shares_Trading_Enabled__c;
            }
        }

        if (!accMap.isEmpty()) {
            update accMap.values();
        }
    }

    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (!Test.isRunningTest()) {
            try {
                EmailUtil.sendEmailForBatchJob(bc.getJobId());
            }
            catch (Exception e) {}
        }
	}
}