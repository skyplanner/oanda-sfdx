/**
 * User API action
 */
public class UserApiAction {
    public static transient final String ACTION_NAME_CRM_PROFILE
        = 'CRM_Profile';

    @AuraEnabled
    public String action { get; set; }

    @AuraEnabled
    public String env { get; set; }

    @AuraEnabled
    public String name { get; set; }

    @AuraEnabled
    public String label { get; set; }

    @AuraEnabled
    public String custPermission { get; set; }

    @AuraEnabled
    public String managerClass { get; set; }

    @AuraEnabled
    public String icon { get; set; }

    @AuraEnabled
    public integer order { get; set; }

    @AuraEnabled
    public String link { get; set; }

    /**
     * Constructor
     */
    public UserApiAction(User_API_Action__mdt actionStt) {
        action = actionStt.Action__c;
        label = actionStt.Label;
        icon = actionStt.Icon__c;
        order = actionStt.Order__c.intValue();
        name = actionStt.DeveloperName;
        custPermission = actionStt.Custom_Permission__c;
        managerClass = actionStt.Manager_Class_Name__c;
        link = actionStt.Link__c;
        env = actionStt.Environment__c;
    }

    /**
     * Know if this action is available for the current user
     */
    public Boolean isAvailableForCurrentUser() {
        return String.isBlank(custPermission) ||
            FeatureManagement.checkPermission(custPermission);
    }    

    public Boolean isLinkAction(String fxTradeName) {
        if (name == ACTION_NAME_CRM_PROFILE) {
            link = String.format(link, new List<String> {fxTradeName});
            return true;
        }

        return String.isNotBlank(link);
    }
}