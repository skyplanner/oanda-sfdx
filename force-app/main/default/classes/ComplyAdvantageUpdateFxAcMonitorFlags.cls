public with sharing class ComplyAdvantageUpdateFxAcMonitorFlags implements Callable
{
    public ComplyAdvantageUpdateFxAcMonitorFlags() 
    {

    }

    public Object call(String action, Map<String, Object> args) 
    {
        switch on action 
        {
            when 'updateFxAcCAMonitorFlags' 
            {
                updateFxAcCAMonitorFlags((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
        }
        return null;
    }

    public void updateFxAcCAMonitorFlags(List<Comply_Advantage_Search__c> searches)
    {
        Map<Id, fxAccount__c> fxAccountsToUpdate = new  Map<Id, fxAccount__c>{};
        for(Comply_Advantage_Search__c search : searches)
        {
            fxAccount__c fxa = fxAccountsToUpdate.get(search.fxAccount__c);
            if(fxa == null)
            {
                fxa = new fxAccount__c(Id = search.fxAccount__c);
            }

            if(search.Is_Alias_Search__c)
            {
                fxa.Is_CA_Alias_Search_Monitored__c = true;
                fxa.Has_ComplyAdvantage_Case__c = true;
            }
            else 
            {
                fxa.Is_CA_Name_Search_Monitored__c = true;  
                fxa.Has_ComplyAdvantage_Case__c = true;  
            }
            fxAccountsToUpdate.put(fxa.ID, fxa);
        }
        
        if(fxAccountsToUpdate.values().size() > 0)
        {
            Database.SaveResult[] result = DatabaseUtil.updateRecords(fxAccountsToUpdate.values(), false, true);
            system.debug('result : ' + result);
        }
    }   
}