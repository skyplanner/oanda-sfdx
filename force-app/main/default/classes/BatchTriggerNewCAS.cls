/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 08-17-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchTriggerNewCAS implements
    Database.Batchable<sObject>, Database.AllowsCallouts, Database.RaisesPlatformEvents, BatchReflection {
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchTriggerNewCAS(String filter) {
        query = 'SELECT Id, Division_Name__c, Lead__c, Lead__r.Name, Account__c, Account__r.Name, ' +
            '   Has_ComplyAdvantage_Case__c, Trigger_Comply_Advantage_Search__c, ' +
            '   (SELECT Id FROM Comply_Advantage_Searches__r WHERE Is_Monitored__c = TRUE) '+
            'FROM fxAccount__c ' +
            'WHERE Is_Closed__c = FALSE ' +
            (String.isNotBlank(filter) ? filter : '');
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Division_Name__c, Lead__c, Lead__r.Name, Account__c, Account__r.Name, ' +
            '   Has_ComplyAdvantage_Case__c, Trigger_Comply_Advantage_Search__c, ' +
            '   (SELECT Id FROM Comply_Advantage_Searches__r WHERE Is_Monitored__c = TRUE) '+
            'FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope) {
        System.debug('scope: ' + scope);
        for (fxAccount__c fx : scope) {
            if ((fx.Comply_Advantage_Searches__r == null || fx.Comply_Advantage_Searches__r.size() <= 0) &&
                (fx.Lead__c != null && fx.Lead__r.Name != 'default default') ||
                (fx.Account__c != null && fx.Account__r.Name != 'default default')) {
                ComplyAdvantageHelper.initialComplyAdvantageSearch(fx);
            }
        }
    }

    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (!Test.isRunningTest()) {
            try {
                EmailUtil.sendEmailForBatchJob(bc.getJobId());
            }
            catch (Exception e) {}
        }
	}
}