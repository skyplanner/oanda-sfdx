/**
 * @author Fernando Gomez
 * @since 5/7/2021
 */
@RestResource(urlMapping='/v1/fx-account/authentication-code/*')
global without sharing class FxAccountAuthenticationCodeResource {
	private Long fxTradeUserId;
	private AuthenticationCodeGenerator generator;

	global fxAccount__c fxAccount { get; private set; }
	global Boolean isValid { get; private set; }
	global Boolean sendEmailNotification { get; private set; }

	private RequestOptions options;
    private static final String EMAIL_ADDRESS = Settings__c.getValues('Default')?.Auth_code_OrgWideEmail__c;

	/**
	 * Main constructor
	 */
	private FxAccountAuthenticationCodeResource() {
		fxTradeUserId = getUserId();

		if (fxTradeUserId == null) {
			setError(400, 'BAD_REQUEST', 'Live User ID not found in URI');
			return;
		}
		
		try {
			options = getOptions();
		} catch (JSONException ex) {
			setError(400, 'BAD_REQUEST', ex.getMessage());
			return;
		}

		// we need a valid user Id and there should be
		// an user with that Id, otherwise 404
		fxAccount = getFxAccount();

		if (fxAccount == null) {
			setError(404, 'NOT_FOUND',
				'Live User with ID "' + fxTradeUserId + '" not found');
			return;
		}
		
		generator = new AuthenticationCodeGenerator(
			fxAccount,
			'Authentication_Code__c',
			'Authentication_Code_Expires_On__c');
		isValid = true;
	}

	/**
	 * Returns an existing and valid code and expiration date.
	 * If no valid code available, NULL is returned in stead.
	 */
	@HttpGet
	global static void doGet() {
		FxAccountAuthenticationCodeResource resource;
		resource = new FxAccountAuthenticationCodeResource();

		// if request was good, 200
		if (resource.isValid)
			resource.setResponse(200, resource.generator.getCode(false));
	}

	/**
	 * Returns an existing and valid code and expiration date.
	 * If no valid code available, a new one is created
	 */
	@HttpPost
	global static void doPost() {
		FxAccountAuthenticationCodeResource resource;
		resource = new FxAccountAuthenticationCodeResource();

		// if request was good
		if (resource.isValid) {
			resource.setResponse(200, resource.generator.getCode(true));

			if (resource.getSendEmailNotificationFlag())
				resource.sendAuthenticationCodeEmail();
		}
	}

	/*
	@HttpDelete
	global static void doDelete() {
		// Add your code
	}

	@HttpPut
	global static void doPut() {
		// Add your code
	}

	@HttpPatch
	global static void doPatch() {
		// Add your code
	}
	*/

	/**
	 * @return the username that should be specified
	 * in the request URI
	 */
	private Long getUserId() {
		try {
			List<String> uriElements = RestContext.request.requestURI.split('/');
			return Long.valueOf(uriElements.get(uriElements.size() - 1));
		} catch (Exception ex) {
			System.debug(ex.getMessage());
			return null;
		}
	}
	
	/**
	 * @return the username that should be specified
	 * in the request URI
	 */
	private RequestOptions getOptions() {
		String body;

		if (RestContext.request.requestBody != null) {
			body = RestContext.request.requestBody.toString();
			System.debug(body);
			return String.isEmpty(body) ?
				new RequestOptions() :
				(RequestOptions)JSON.deserialize(body, RequestOptions.class);
		} else 
			return new RequestOptions();
	}
	
	/**
	 * @return the flag that specifies if we should send a notification
	 * with the authentication code.
	 */
	private Boolean getSendEmailNotificationFlag() {
		return options.sendEmail == true;
	}

	/**
	 * @return the fxAccount containing the
	 * fxTrade User Id specified in the request URI
	 */
	private fxAccount__c getFxAccount() {
		List<fxAccount__c> accounts;

		accounts = [
			SELECT Id,
				Authentication_Code__c,
				Authentication_Code_Expires_On__c,
				Email_Formula__c,
				Language_Preference__c
			FROM fxAccount__c
			WHERE fxTrade_User_Id__c = :fxTradeUserId
			AND RecordTypeId = :RecordTypeUtil.getFxAccountLiveId()
		];

		if (accounts.isEmpty() || accounts.size() > 1)
			return null;

		return accounts[0];
	}

	/**
	 * SP-10182
	 * Send a Client Authentication Code to the specified
	 * fxAccount. Account's email.
	 */
	private void sendAuthenticationCodeEmail() {
		String defaultTemplateName;
		EmailTemplate template;
		
		// base template name, also prefix of languaged templates
		defaultTemplateName = 'fxAccount_Authentication_Code';

		// we then extract the template and contact
		template = EmailTemplateUtil.getTemplateByLanguage(
			defaultTemplateName,
			fxAccount.Language_Preference__c);
		
		EmailUtil.sendEmailRenderingTemplate(
			new List<String> { fxAccount.Email_Formula__c },
			// we use the default template
			template != null ? template.DeveloperName : defaultTemplateName,
			fxAccount.Id,
			null,
			(Test.isRunningTest() ? 'donotreply@oanda.com' : EMAIL_ADDRESS));
	}

	/**
	 * Places a response in the response
	 * @param statusCode
	 * @param obj
	 */
	private void setResponse(Integer statusCode, Object obj) {
		RestContext.response.statusCode = statusCode;
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(obj));
		RestContext.response.headers.put('Content-Type', 'application/json');
	}

	/**
	 * Places an error in the response
	 * @param statusCode
	 * @param errorCode
	 * @param message
	 */
	private void setError(Integer statusCode, String errorCode, String message) {
		isValid = false;
		setResponse(statusCode,
			new List<Map<String, Object>> {
				new Map<String, Object> {
					'errorCode' => errorCode,
					'message' => message
				}
			});
	}

	/**
	 * Options whe submitting the request
	 */
	private class RequestOptions {
		Boolean sendEmail;
	}
}