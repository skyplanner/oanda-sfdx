/**
 * @File Name          : AuthenticationCodeProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 5:36:29 AM
**/
public inherited sharing class AuthenticationCodeProcess {

	public static final String AUTHENTICATION_CODE_FIELD = 
		'Authentication_Code__c';

	public static final String AUTHENTICATION_CODE_EXPIRES_ON_FIELD = 
		'Authentication_Code_Expires_On__c';

	final ID fxAccountId;
	final ID caseId;

	fxAccount__c fxAccountObj;
	Account accountObj;

	public AuthenticationCodeProcess(
		ID fxAccountId,
		ID caseId
	) {
		this.fxAccountId = fxAccountId;
		this.caseId = caseId;
	}

	public Boolean verifyAuthenticationCode(String code) {
		Boolean result = 
			AuthenticationCodeGenerator.validateAuthenticationCode(
				code, // codeToValidate 
				fxAccountId, // recordId
				AUTHENTICATION_CODE_FIELD, // codeFieldApiName
				AUTHENTICATION_CODE_EXPIRES_ON_FIELD // codeExpiresOnFieldApiName
			);
		updateCaseAuthenticationInfo(result);
		return result;
	}

	@TestVisible
	Boolean updateCaseAuthenticationInfo(Boolean userAuthenticated) {
		Boolean result = false;

		if (caseId != null) {
			Case caseObj = new Case(
				Id = caseId,
				User_Authenticated__c = userAuthenticated
			);
			update caseObj;
			result = true;
		}
		
		return result;
	}

	public Boolean sendAuthenticationCode(String languageCode) {
		if (
			(fxAccountId == null) ||
			(caseId == null)
		) {
			return false;
		}
		// else...
		fxAccountObj = 
			FxAccountRepo.getAuthenticationInfoById(fxAccountId);
		accountObj = 
			AccountRepo.getAuthenticationInfoById(fxAccountObj.Account__c);
		Contact contactObj = 
			ContactRepo.getSummaryByEmail(accountObj.PersonEmail);
		Boolean result = sendAuthenticationCode(
			contactObj, // contactObj
			languageCode // languageCode
		);
		return result;
	}

	@TestVisible
	Boolean sendAuthenticationCode(
		Contact contactObj,
		String languageCode
	) {
		if (contactObj == null) {
			return false;
		}
		// else...
		AuthenticationCodeGenerator codeGen = new AuthenticationCodeGenerator(
			fxAccountObj, // record
			AUTHENTICATION_CODE_FIELD, // codeFieldApiName
			AUTHENTICATION_CODE_EXPIRES_ON_FIELD // codeExpiresOnFieldApiName
		);
		codeGen.generateNewCodeAndSaveToRecord();
		// copy Authentication values to Account fields
		accountObj.Authentication_Code__c = 
			fxAccountObj.Authentication_Code__c;
		accountObj.Authentication_Code_Expires_On__c = 
			fxAccountObj.Authentication_Code_Expires_On__c;
		update accountObj;

		AuthenticationCodeEmailProcess emailProcess = 
			new AuthenticationCodeEmailProcess(
				caseId, // caseId
				contactObj, // contactObj
				languageCode // languageCode
			);
		Boolean result = emailProcess.sendAuthenticationCodeEmail();
		return result;
	}
	
}