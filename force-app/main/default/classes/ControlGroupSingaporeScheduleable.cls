/*
 *  ControlGroupSingaporeScheduleable
 *  Run a batch job daily to query all opportunities (country = Singapore) that became Traded within the past 24 hrs, 
 *  randomly select one user from the list and reassign the user to the control group.
*/
public class ControlGroupSingaporeScheduleable implements Schedulable 
{  
    public static final String CRON_SCHEDULE = '0 0 10 * * ?';

    public static void execute(SchedulableContext context) 
    {
         if (Settings__c.getValues('Default') != null && 
             Settings__c.getValues('Default').Enable_Control_Group__c)
        {
        	ControlGroupHelper.assignToSingaporeControlGroup();
        }
	}
     public static void execute() 
    {
         if (Settings__c.getValues('Default') != null && 
             Settings__c.getValues('Default').Enable_Control_Group__c)
        {
        	ControlGroupHelper.assignToSingaporeControlGroup();
        }
	}
    
    public static void schedule(string cronExpression) 
    {
        if(string.isBlank(cronExpression))
        {
            cronExpression = CRON_SCHEDULE;
        }
        System.schedule('ControlGroupSingaporeScheduleable', cronExpression, new ControlGroupSingaporeScheduleable());
    }
}