/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-01-2023
 * @last modified by  : Dianelys Velazquez
**/
@isTest
public with sharing class CondApprovedNotificationJobTest {
    @isTest
    public static void scheduleTest() {
        Test.StartTest();

        CondApprovedNotificationJob.schedule();

		Test.stopTest();

		System.assert(true, 'Scheduling issue.');
    }
}