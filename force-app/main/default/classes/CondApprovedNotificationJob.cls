/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-01-2023
 * @last modified by  : Dianelys Velazquez
**/
public class CondApprovedNotificationJob implements Schedulable {
    final static String FLOW_NAME =
        'IHS_Notify_Bizops_Conditionally_Approved_fxAccounts';
    final static String SCHEDULE_NAME =
        'Notify Bizops Cond. Approved fxAccounts Job';
    final static String FLOW_SCHEDULE_NAME =
        'Notify Bizops Cond. Approved fxAccounts Flow';
    final static String NOTIFICATION_ENABLED_KEY =
        'IHS_Cond_Appr_Notif_Enabled';

	public void execute(SchedulableContext scon) {
        SPGeneralSettings settings = SPGeneralSettings.getInstance();
        if (!settings.getValueAsBoolean(NOTIFICATION_ENABLED_KEY))
            return;

        DateTime yesterday = DateTime.now() - 1;
        String yesterdayText = yesterday.format(
            IHSIntegrationManager.CONDITIONALLY_APPROVED_DATE_FORMAT);        

        List<fxAccount__c> condAppList = [SELECT Id
            FROM fxAccount__c
            WHERE Conditionally_Approved_Date_Text__c >= :yesterdayText
            LIMIT 1];

        if (condAppList.isEmpty())
            return;

        Datetime sysTime = System.now().addSeconds(30);
        String cron = '' + sysTime.second() + ' ' + 
            sysTime.minute() + ' ' + sysTime.hour() + ' ' + 
            sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        FlowSchedulable.schedule(FLOW_SCHEDULE_NAME, FLOW_NAME, cron);
	}

    public static void schedule() {
        String dateText = DateTime.now().format(
            IHSIntegrationManager.CONDITIONALLY_APPROVED_DATE_FORMAT);
        System.schedule(SCHEDULE_NAME + ' ' + dateText,
            '0 0 22 * * ?', new CondApprovedNotificationJob());
	}
}