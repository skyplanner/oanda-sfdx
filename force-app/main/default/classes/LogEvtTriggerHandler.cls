/**
 * This class handles the LogEvt__e triggers events
 */
public class LogEvtTriggerHandler {
    
    List<LogEvt__e> newList;

    // Util class to manage logs
    private LogUtil util;

    /**
     * Constructor
     */
    public LogEvtTriggerHandler(
        List<LogEvt__e> newList)
    {
        this.newList = newList;
        util = new LogUtil();
    }

     /**
     * Handles the after insert event for
     * the LogEvt__e trigger
     */
    public void onAfterInsert(){
        // Process platform events
        util.processLogEvents(newList);
    }

}