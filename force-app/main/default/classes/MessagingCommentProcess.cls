/**
 * @File Name          : MessagingCommentProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/30/2023, 12:00:11 AM
**/
public without sharing class MessagingCommentProcess {

	public static final String CASE_REASON_KEY = 'CASE_REASON';

	final ID messagingSessionId;
	final String key;

	public MessagingCommentProcess(
		ID messagingSessionId
	) {
		this.messagingSessionId = messagingSessionId;
	}

	public MessagingCommentProcess(
		ID messagingSessionId,
		String key
	) {
		this.messagingSessionId = messagingSessionId;
		this.key = key;
	}

	public Integer createCaseComments(String caseId) {
		List<Messaging_Comment__c> msgComments = 
			MessagingCommentRepo.getByMessagingSession(messagingSessionId);
		Integer result = createCaseComments(
			caseId, // caseId
			msgComments // msgComments
		);
		return result;
	}

	@TestVisible
	Integer createCaseComments(
		String caseId,
		List<Messaging_Comment__c> msgComments
	) {
		List<CaseComment> caseComments = new List<CaseComment>();
		String caseCommentBody = '';

		for (Messaging_Comment__c msgComment : msgComments) {

			if (msgComment.Name == CASE_REASON_KEY) {
				caseComments.add(
					new CaseComment(
						ParentId = caseId,
						CommentBody = msgComment.Body__c
					)
				);

			} else {
				caseCommentBody += (msgComment.Body__c + '\n');
			}
		}

		if (String.isNotBlank(caseCommentBody)) {
			caseComments.add(
				new CaseComment(
					ParentId = caseId,
					CommentBody = caseCommentBody
				)
			);
		}

		SPDataUtils.insertIfNoTesting(caseComments);
		SPDataUtils.deleteIfNoTesting(msgComments);
		return caseComments.size();
	}

	public Boolean upsertComment(
		String subject,
		String body
	) {
		if (String.isBlank(body)) {
			return false;
		}
		// else....
		
		Messaging_Comment__c comment =
			MessagingCommentRepo.getByMessagingSessionAndName(
				messagingSessionId, // messagingSessionId
				key // name
			);
		upsertComment(
			comment, // comment
			subject, // subject
			body // body
		);
		return true;
	}

	@TestVisible
	void upsertComment(
		Messaging_Comment__c comment,
		String subject,
		String body
	) {
		if (comment == null) {
			comment = new Messaging_Comment__c(
				Messaging_Session__c = messagingSessionId,
				Name = key
			);
		}
		comment.Body__c = getValidBody(
			subject, // subject
			body // body
		);
		SPDataUtils.upsertIfNoTesting(comment);
	}

	@TestVisible
	String getValidBody(
		String subject,
		String body
	) {
		if (String.isNotBlank(subject)) {
			return subject + ': ' + body;
		}
		//else...
		return body; 
	}

}