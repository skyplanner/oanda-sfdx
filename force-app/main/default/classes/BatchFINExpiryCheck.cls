public with sharing class BatchFINExpiryCheck implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Schedulable {

    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static final Integer BATCH_SIZE = 200;   
    //schedule everyday at 1 AM
    public static final String CRON_SCHEDULE = '0 0 1 ? * * *';   
    public List<String> errs = new List<String>();
    public set<string> divisions_OAP = new set<string>
    {
        'OANDA Asia Pacific',
        'OANDA Asia Pacific CFD'
    };
    public static final string adhocOAPOAUQueueId = UserUtil.getQueueId('Ad Hoc (OAP/OAU) Cases');
	public static final Id supportOBCaseRecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId();
    public static date checkDate = Date.today();

    public BatchFINExpiryCheck() 
    {
        string[] queryFields =  new string[]
        {
          'Id',
          'Name',
          'Type__c',
          'Singpass_Flow__c',
          'FIN_Expiry_Date__c',
          'Government_ID__c',
          'Account__c',
          'fxTrade_User_Id__c',
          'Contact__c',
          'Division_Name__c',
          'lead__c'
        };
        query = 'Select ' + String.join(queryFields,',') + ' From fxAccount__c ';
        query += 'where Type__c = \'Individual\' and ';
        query += 'Division_Name__c = :divisions_OAP and ';
        query += 'Singpass_Flow__c = true and ';
        query += 'FIN_Expiry_Date__c != null and ';
        query += 'FIN_Expiry_Date__c = :checkDate and ';
        query += '(Government_ID__c LIKE \'G%\' or Government_ID__c LIKE \'F%\' or Government_ID__c LIKE \'M%\') and ';
        query += 'Is_Closed__c = false and ';
        query += 'Ready_for_Funding_DateTime__c != null and ';
      	query += 'RecordType.Name = \'Retail Live\'';
    }

    public BatchFINExpiryCheck(String q) 
    {
		query = q;
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        string[] queryFields =  new string[]
        {
          'Id',
          'Name',
          'Type__c',
          'Singpass_Flow__c',
          'FIN_Expiry_Date__c',
          'Government_ID__c',
          'Account__c',
          'fxTrade_User_Id__c',
          'Contact__c',
          'Division_Name__c',
          'lead__c'
        };
        query = 'Select ' + String.join(queryFields,',') + ' From fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {       
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope)
    {
        case[] FINExpiryNotificationCases = new case[]{};
        List<fxAccount__c> fxaList = (List<fxAccount__c>)scope;
        for(fxAccount__c fxa : fxaList)
        {
            case FINExpiryNotificationCase = new case();
            FINExpiryNotificationCase.AccountId = fxa.Account__c;  
            FINExpiryNotificationCase.fxAccount__c = fxa.Id;
            FINExpiryNotificationCase.Lead__c = fxa.Lead__c;
            FINExpiryNotificationCase.ContactId = fxa.Contact__c;
            FINExpiryNotificationCase.OwnerId = adhocOAPOAUQueueId;
            FINExpiryNotificationCase.Inquiry_Nature__c = 'Account'; 
            FINExpiryNotificationCase.Type__c = 'Expired docs';
            FINExpiryNotificationCase.Live_or_Practice__c = 'Live';
            FINExpiryNotificationCase.Origin = 'Internal';
            FINExpiryNotificationCase.RecordTypeId = supportOBCaseRecordTypeId;
            FINExpiryNotificationCase.Subject = 'Document Expiry Notification: FIN';
            FINExpiryNotificationCase.Status = 'Waiting on Date';
            FINExpiryNotificationCase.Waiting_for_Date__c = fxa.FIN_Expiry_Date__c.addDays(30);
            FINExpiryNotificationCase.Expiry_Document_Due_Date__c = fxa.FIN_Expiry_Date__c.addDays(30);        
            FINExpiryNotificationCase.Description = 'FIN will expire for fxAccount ';
            FINExpiryNotificationCase.Description += fxa.Name  + ' (fxTrade User Id: ' +fxa.fxTrade_User_Id__c+ ') on ';
            FINExpiryNotificationCase.Description += fxa.FIN_Expiry_Date__c.year() + '-' + fxa.FIN_Expiry_Date__c.month() + '-' + fxa.FIN_Expiry_Date__c.day();
            
            FINExpiryNotificationCases.add(FINExpiryNotificationCase);
            
        system.debug('FINExpiryNotificationCase :' + FINExpiryNotificationCase);
        }
        
        Database.SaveResult[] caseCreationResults = Database.Insert(FINExpiryNotificationCases , false);
        for (Database.SaveResult res : caseCreationResults) 
        {
            if(!res.isSuccess())
            {
                for (Database.Error error : res.getErrors()) 
                {
                   System.debug('Error returned: ' + error.getStatusCode() +' - '+ error.getMessage());
                }
            }
        }
    }

    public void execute(SchedulableContext context) 
    {
		Database.executeBatch(new BatchFINExpiryCheck(), BATCH_SIZE);
    }
    
    public static void execute() 
    {
        Database.executeBatch(new BatchFINExpiryCheck(), BATCH_SIZE);
    }
    
    public static void schedule(string cronExpression) 
    { 
         if(String.isNotBlank(cronExpression) && String.isNotEmpty(cronExpression))
         {
			System.schedule('BatchFINExpiryCheck', cronExpression, new BatchFINExpiryCheck());
         }
	}
    
    //schedule the batch job
    public static void schedule() 
    {      
        schedule(CRON_SCHEDULE);
	}
    
    public void finish(Database.BatchableContext bc)
    {  
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if(!Test.isRunningTest())
        {
        	EmailUtil.sendEmailForBatchJob(bc.getJobId());
        }
    }
}