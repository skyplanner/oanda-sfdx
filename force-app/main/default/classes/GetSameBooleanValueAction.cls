/**
 * @File Name          : GetSameBooleanValueAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 11:45:44 AM
**/
public without sharing class GetSameBooleanValueAction { 

	@InvocableMethod(label='Get same Boolean value' callout=false)
	public static List<Boolean> getSameValue(List<String> valueList) {   
		try {
			ExceptionTestUtil.execute(); 
			List<Boolean> result = new List<Boolean>();

			if (valueList != null) {
				
				for (String value : valueList) {
					Boolean booleanValue = false;

					if (String.isNotBlank(value)) {
						String validValue = value.toLowerCase();
						booleanValue = (
							(validValue == 'true') ||
							(validValue == '1') ||
							(validValue == 'yes')
						);
					}
					
					result.add(booleanValue);
				}
			}

			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetSameBooleanValueAction.class.getName(),
				ex
			);
		}
	} 

}