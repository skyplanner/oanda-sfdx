@isTest
private class NF_GetCaseCreationMatrixTest {
	
	@isTest static void this_should_get_non_fallback_routing() {
		
		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.PersonEmail = 'testAccount@nf.com';
		insert testAccount;

		fxAccount__c testFx = NF_TestDataUtill.createfxAccount();
		testFx.Account__c = testAccount.Id;
		insert testFx;

		nfchat__Chat_Log__c testChatLog = NF_TestDataUtill.createChatLog();
		testChatLog.nfchat_Account_Type__c = 'demo';
		testChatLog.nfchat__Session_Id__c = 'testSession';
		insert testChatLog;

		NF_GetCaseCreationMatrix obj = new NF_GetCaseCreationMatrix();
		Map<String, String> paramMap = new Map<String, String>();
		//paramMap.put('sessionId', testChatLog.nfchat__Session_Id__c);
		String session = '{"sessionId":"' + testChatLog.nfchat__Session_Id__c + '"}';
		paramMap.put('email', testAccount.PersonEmail);
		String routingConfig = obj.processRequest(paramMap, session, null);
		System.debug('>> routingConfig : ' + routingConfig);
	}

	@isTest
	static void getChatButton(){
		String skill = 'German';

		Chat_button_by_skill__mdt retrieved = NF_GetCaseCreationMatrix.getChatButtonMetadataBySkill(skill);
		Chat_button_by_skill__mdt languageSkill = [SELECT Id, Button_Id__c FROM Chat_button_by_skill__mdt WHERE Skill__c = :skill];

		System.assertEquals(languageSkill.Button_Id__c, retrieved.Button_Id__c);
	}

	@isTest
	static void getDefaultChatButton(){
		String skill = 'English';

		Chat_button_by_skill__mdt retrieved = NF_GetCaseCreationMatrix.getChatButtonMetadataBySkill(null);

		Chat_button_by_skill__mdt defaultSkill = [SELECT Id, Button_Id__c FROM Chat_button_by_skill__mdt WHERE Skill__c = :skill];

		System.assertEquals(defaultSkill.Button_Id__c, retrieved.Button_Id__c);
	}
}