/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Test class to test FxAccountUpdateRestResource
*
*/
@isTest(SeeAllData = false)
public class FxAccountUpdateRestResourceTest {
    
   final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    // Test 1 : Disable the API on API_Access_Control__c Custom settings
    public static testMethod void test_disable_api() {
        
        System.runAs(SYSTEM_USER) {
            
            Account acc = new Account(LastName='test account 1');
            insert acc;

            Opportunity o = new Opportunity(Name='test opp 1', StageName='test stage', CloseDate=Date.today(), AccountId=acc.Id);
            insert o;
       
            Lead lead = new Lead(LastName='test lead 1', Email='lead1@example.org');
            insert lead;
        
            fxAccount__c fxacc = new fxAccount__c(Name='test fxAccount 1', Account__c=acc.Id, fxTrade_User_Id__c = 1, fxTrade_Global_ID__c = '1+' +RecordTypeUtil.getFxAccountLiveId(), Opportunity__c=o.Id, Lead__c=lead.Id);
            insert fxacc;   
          
            User u = UserUtil.getTestUser();
            insert u;
            
             API_Access_Control__c control = new API_Access_Control__c(name = CustomSettings.API_NAME_FX_ACCOUNT_UPDATE, User_Allowed__c = SYSTEM_USER.Id, Enabled__c = false );
             insert control;
            
             RestRequest request = new RestRequest();
             request.requestUri = '/services/apexrest/Users/';
             request.httpMethod = 'POST';
             request.addHeader('Content-Type', 'application/json');
             request.requestBody = Blob.valueOf('[{"user_id" : 1,"hvc_score" : 0.1},{"user_id" : 2,"hvc_Score" : 0.2}]');
             RestContext.request = request;
           
             string response = FxAccountUpdateRestResource.doPost();
             List<object> res = (List<object>) JSON.deserializeUntyped(response);
             Map<string,object> resMap = (Map<string,object>)res[0];
            
             System.assertEquals(false, (boolean)resMap.get('isSuccessful'));     
             System.assertEquals((string)resMap.get('errorInfo'), 'API FxAccountUpdate is disabled');
        }
    }

    // Test 2 : Call the service as someone who is not authorised on API_Access_Control__c Custom settings
    public static testMethod void test_disable_user() {
        
            System.runAs(SYSTEM_USER) {
            
            Account acc = new Account(LastName='test account 1');
            insert acc;

            Opportunity o = new Opportunity(Name='test opp 1', StageName='test stage', CloseDate=Date.today(), AccountId=acc.Id);
            insert o;
       
            Lead lead = new Lead(LastName='test lead 1', Email='lead1@example.org');
            insert lead;
        
            fxAccount__c fxacc = new fxAccount__c(Name='test fxAccount 1', Account__c=acc.Id, fxTrade_User_Id__c = 1, fxTrade_Global_ID__c = '1+' +RecordTypeUtil.getFxAccountLiveId(), Opportunity__c=o.Id, Lead__c=lead.Id);
            insert fxacc;   
          
            User u = UserUtil.getTestUser();
            insert u;
            
             API_Access_Control__c control = new API_Access_Control__c(name = CustomSettings.API_NAME_FX_ACCOUNT_UPDATE, User_Allowed__c = u.Id, Enabled__c = true );
             insert control;
            
             RestRequest request = new RestRequest();
             request.requestUri = '/services/apexrest/Users/';
             request.httpMethod = 'POST';
             request.addHeader('Content-Type', 'application/json');
             request.requestBody = Blob.valueOf('[{"user_id" : 1,"hvc_score" : 0.1}]');
             RestContext.request = request;
           
             string response = FxAccountUpdateRestResource.doPost();
             List<object> res = (List<object>) JSON.deserializeUntyped(response);
             Map<string,object> resMap = (Map<string,object>)res[0];
            
             System.assertEquals(false, (boolean)resMap.get('isSuccessful'));     
             System.assertEquals((string)resMap.get('errorInfo'), 'User is not allowed to call this API');
        }
    }
    
    // Test 3 : Pass the existing FxAccount's user id and hvc score. Update should be successful
     public static testMethod void test_update_HVC_Score() {
        
            System.runAs(SYSTEM_USER) {
            
            Account acc = new Account(LastName='test account 1');
            insert acc;

            Opportunity o = new Opportunity(Name='test opp 1', StageName='test stage', CloseDate=Date.today(), AccountId=acc.Id);
            insert o;
       
            Lead lead = new Lead(LastName='test lead 1', Email='lead1@example.org');
            insert lead;
        	
            string fxAccountLiveId = RecordTypeUtil.getFxAccountLiveId();
            if(fxAccountLiveId != null && fxAccountLiveId.length() == 18)
            {
                fxAccountLiveId = fxAccountLiveId.substring(0,15);
            }    
            fxAccount__c fxacc = new fxAccount__c(Name='test fxAccount 1',
                                                  Account__c=acc.Id, 
                                                  fxTrade_User_Id__c = 1, 
                                                  fxTrade_Global_ID__c = '1+' +fxAccountLiveId,
                                                  Opportunity__c=o.Id,
                                                  Lead__c=lead.Id,
                                                  hvc_score__c = 0.4
                                                  );
            insert fxacc;   
          
            User u = UserUtil.getTestUser();
            insert u;
            
             API_Access_Control__c control = new API_Access_Control__c(name = CustomSettings.API_NAME_FX_ACCOUNT_UPDATE, User_Allowed__c = SYSTEM_USER.Id, Enabled__c = true );
             insert control;
            
             RestRequest request = new RestRequest();
             request.requestUri = '/services/apexrest/Users/';
             request.httpMethod = 'POST';
             request.addHeader('Content-Type', 'application/json');
             request.requestBody = Blob.valueOf('[{"user_id" : 1,"hvc_score" : 0.1}]');
             RestContext.request = request;
           
             string response = FxAccountUpdateRestResource.doPost();        
             List<object> res = (List<object>) JSON.deserializeUntyped(response);
             Map<string,object> resMap = (Map<string,object>)res[0];
                
             system.debug('output' + resMap);
             System.assertEquals(true, (boolean)resMap.get('isSuccessful')); 
             
             fxacc = [select hvc_score__c from fxAccount__c where Id = :fxacc.Id Limit 1];
             System.assertEquals(0.1 , fxacc.hvc_score__c);
        }
    }
    
    // Test 4 : Pass the non existing FxAccount user id and hvc score. Update should be unsuccessful
     public static testMethod void test_update_non_existing_user() {
        
            System.runAs(SYSTEM_USER) {
            
            Account acc = new Account(LastName='test account 1');
            insert acc;

            Opportunity o = new Opportunity(Name='test opp 1', StageName='test stage', CloseDate=Date.today(), AccountId=acc.Id);
            insert o;
       
            Lead lead = new Lead(LastName='test lead 1', Email='lead1@example.org');
            insert lead;
        
            fxAccount__c fxacc = new fxAccount__c(Name='test fxAccount 1',
                                                  Account__c=acc.Id, 
                                                  fxTrade_User_Id__c = 1, 
                                                  fxTrade_Global_ID__c = '1+' +RecordTypeUtil.getFxAccountLiveId(),
                                                  Opportunity__c=o.Id,
                                                  Lead__c=lead.Id,
                                                  hvc_score__c = 0.4
                                                  );
            insert fxacc;   
          
            User u = UserUtil.getTestUser();
            insert u;
            
             API_Access_Control__c control = new API_Access_Control__c(name = CustomSettings.API_NAME_FX_ACCOUNT_UPDATE, User_Allowed__c = SYSTEM_USER.Id, Enabled__c = true );
             insert control;
            
             RestRequest request = new RestRequest();
             request.requestUri = '/services/apexrest/Users/';
             request.httpMethod = 'POST';
             request.addHeader('Content-Type', 'application/json');
             request.requestBody = Blob.valueOf('[{"user_id" : 2,"hvc_score" : 0.1}]');
             RestContext.request = request;
           
             string response = FxAccountUpdateRestResource.doPost();
             List<object> res = (List<object>) JSON.deserializeUntyped(response);
             Map<string,object> resMap = (Map<string,object>)res[0];
             System.assertEquals(false, (boolean)resMap.get('isSuccessful'));
        }
     }
}