/**
 * @File Name          : MessagingSessionRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 9:49:40 AM
**/
@IsTest
private without sharing class MessagingSessionRepoTest {

	/**
	 * Testing is performed without records created given the difficulties
	 * in creating MessagingSession test records
	 */
	@IsTest
	static void test() {
		Test.startTest();
		MessagingSession result1 = MessagingSessionRepo.getSummaryById(null);
		MessagingSession result2 = MessagingSessionRepo.getLeadInfoById(null);
		MessagingSession result3 = MessagingSessionRepo.getCaseInfoById(null);
		MessagingSession result4 = 
			MessagingSessionRepo.getAuthenticationInfoById(null);
		MessagingSession result5 = MessagingSessionRepo.getInitInfoById(null);
		List<MessagingSession> result6 = 
			MessagingSessionRepo.getCaseInfo(new Set<ID>{ null });
		List<MessagingSession> result7 = 
			MessagingSessionRepo.getMessagingSessions(new List<ID>{ null });
		Assert.isNull(result1, 'Invalid result');
		Assert.isNull(result2, 'Invalid result');
		Assert.isNull(result3, 'Invalid result');
		Assert.isNull(result4, 'Invalid result');
		Assert.isNull(result5, 'Invalid result');
		Assert.isTrue(result6.isEmpty(), 'Invalid result');
		Assert.isTrue(result7.isEmpty(), 'Invalid result');
	}
	
}