/**
 * @File Name          : RegisterMessagingLanguageAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 12:08:35 PM
**/
public without sharing class RegisterMessagingLanguageAction { 

	@InvocableMethod(label='Register messaging language' callout=false)
	public static void registerLanguage(List<ActionInfo> infoList) {   
		try {
			ExceptionTestUtil.execute();   
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return;
			}
			// else...     
			ActionInfo info = infoList[0];
			MessagingChatbotProcess chatbotProcess = 
				new MessagingChatbotProcess(info.messagingSessionId);
			chatbotProcess.setLanguageAndInquiryNature(
				info.endUserId, // endUserId
				info.languageCode, // languageCode
				null // inquiryNature
			);
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				RegisterMessagingLanguageAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'endUserId' required = true)
		public String endUserId;

		@InvocableVariable(label = 'languageCode' required = true)
		public String languageCode;

	}

}