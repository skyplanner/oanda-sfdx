/**
 * @File Name          : RemoveNonHvcCasesDynamicSchedulable.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2024, 1:13:00 PM
**/
public without sharing class RemoveNonHvcCasesDynamicSchedulable 
	extends DynamicSchedulable implements Schedulable {

	public RemoveNonHvcCasesDynamicSchedulable() {
		super('RemoveNonHvcCasesSchedulable');
	}

}