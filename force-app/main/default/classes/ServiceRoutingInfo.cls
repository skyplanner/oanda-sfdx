/**
 * @File Name          : ServiceRoutingInfo.cls
 * @Description        : 
 * Stores the information necessary to perform custom routing processes
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:13:43 AM
**/
public class ServiceRoutingInfo extends BaseServiceRoutingInfo {

	// The word CASES appears in the plural because CASE is a reserved word
	public enum Source {CHAT, CASES, MESSAGING} 

	Source infoSource;
	Boolean practice;
	Boolean live;

	public Boolean infoIsIncomplete {get; set;}

	public String email {get; set;}

	public String language {get; set;}

	public String alternativeLanguage {get; set;}

	public String accountType {get; set;}

	public OmnichannelCustomerConst.CustomerType relatedCustomerType {get; set;}

	public String subtype {get; set;}

	public Boolean isAComplaint{get; set;}

	public Boolean deposit {get; set;}

	public Boolean trade {get; set;}

	public Boolean isOELDivision {get; set;}

	public Boolean crypto {get; set;}

	public String priority {get; set;}

	public Boolean ccmCx {get; set;}

	public Boolean dropAdditionalSkills {get; set;}

	public SCustomerTypeProvider customerTypeProvider {get; set;}

	public SCustomerTierCalculator customerTierCalculator {get; set;}

	public String tier {get; private set;}

	public Boolean tierHasChanged {get; private set;}

	public Segmentation__c segmentation {get; set;}

	public SWorkItemUpdater workItemUpdater {get; set;}

	public SRoutingLogCreator routingLogCreator {get; set;}

	//DO NOT REMOVE
	//public Boolean isEmpty {get; set;}

	//*****************************************************

	public Account relatedAccount {get; private set;}

	public Lead relatedLead {get; private set;}

	//*****************************************************
	
	public String inquiryNature {
		get; 
		set {
			inquiryNature = value;
			if (
				(inquiryNature == OmnichanelConst.INQUIRY_NAT_TRADE) ||
				(inquiryNature == OmnichanelConst.INQUIRY_NAT_FEES_CHARGES)
			) {
				trade = true;
			}
			checkDeposit();
		}
	}

	public String type {
		get; 
		set {
			type = value;
			checkDeposit();
		}
	}

	public String division {
		get;
		set {
			division = value;
			isOELDivision = (
				(division == OmnichanelConst.OANDA_EUROPE_AB) ||
				(division == OmnichanelConst.OANDA_EUROPE)
			);
		}
	}
	
	public Boolean relatedToAnAccount {
		get {
			return (relatedAccount != null);
		}
	}

	public Boolean relatedToALead {
		get {
			return (relatedLead != null);
		}
	}

	public Boolean createdFromChat {
		get {
			return (infoSource == Source.CHAT);
		}
	}

	public Boolean createdFromCase {
		get {
			return (infoSource == Source.CASES);
		}
	}

	public Boolean createdFromMessagingChat {
		get {
			return (infoSource == Source.MESSAGING);
		}
	}

	public Boolean isHVC {
		get {
			return (
				relatedCustomerType == 
				OmnichannelCustomerConst.CustomerType.HVC
			);
		}
	}

	public Boolean isNewCustomer {
		get {
			return (
				relatedCustomerType == 
				OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER
			);
		}
	}

	public Boolean isActiveCustomer {
		get {
			return (
				relatedCustomerType == 
				OmnichannelCustomerConst.CustomerType.ACTIVE
			);
		}
	}

	public Boolean isCustomer {
		get {
			return (
				relatedCustomerType == 
				OmnichannelCustomerConst.CustomerType.CUSTOMER
			);
		}
	}

	public Boolean isPracticeAccount {
		get {
			if (practice == null) {
				checkAccountType();
			}
			return practice;
		}
	}

	public Boolean isLiveAccount {
		get {
			if (live == null) {
				checkAccountType();
			}
			return live;
		}
	}

	public void checkAccountType() {
		practice = false;
		live = false;
		if (createdFromChat) {
			if (String.isNotBlank(accountType)) {
				if (
					(accountType == OmnichanelConst.LIVE) ||
					(accountType == OmnichanelConst.NEW_ACCOUNT) 
				) {
					live = true;
					//...
				} else  {
					practice = true;
				}
			}
			//...
		} else if (createdFromCase) {
			if (relatedToALead) {
				practice = true;
				//...
			} else if (relatedToAnAccount) {
				live = true;
			}
		}
	}
	
	public ServiceRoutingInfo(
		PendingServiceRouting psrObj, 
		Source infoSource,
		SObject workItem
	) {
		super(
			psrObj, // psrObj
			infoSource?.name(), // sourceName
			workItem // workItem
		);
		this.infoSource = infoSource;
		this.dropAdditionalSkills = false;
		this.infoIsIncomplete = false;
		this.isAComplaint = false;
		this.deposit = false;
		this.trade = false;
		this.isOELDivision = false;
		this.practice = null;
		this.live = null;
		this.relatedCustomerType = null;
		this.crypto = false;
		this.priority = null;
		this.ccmCx = false;
		this.tierHasChanged = false;
	}
	
	public void setRelatedAccount(Account accountObj) {
		setRelatedAccount(
			accountObj, // accountObj
			true // checkCustomerInfo
		);
	}

	public void setRelatedAccount(
		Account accountObj,
		Boolean checkCustomerInfo
	) {
		ServiceLogManager.getInstance().log(
			'setRelatedAccount -> account.Id: ' + accountObj?.Id, // msg
			ServiceRoutingInfo.class.getName() // category
		);
		if (accountObj == null) { 
			return;
		}
		//else...
		relatedCustomerType = null;
		infoIsIncomplete = false;
		relatedAccount = accountObj;

		if (String.isBlank(division)) {
			division = accountObj.Primary_Division_Name__c;
		}

		if (String.isBlank(language)) {
			language = accountObj.Language_Preference__pc;
		}
		
		if (checkCustomerInfo == true) {
			checkCustomerType();
		}
	}

	public void setRelatedLead(Lead leadObj) {
		setRelatedLead(
			leadObj, // leadObj
			true // checkCustomerInfo
		);
	}

	public void setRelatedLead(
		Lead leadObj,
		Boolean checkCustomerInfo
	) {
		ServiceLogManager.getInstance().log(
			'setRelatedLead -> lead.Id: ' + leadObj?.Id, // msg
			ServiceRoutingInfo.class.getName() // category
		);

		if (leadObj == null) { 
			return;
		}
		//else...
		relatedCustomerType = null;
		infoIsIncomplete = false;
		relatedLead = leadObj;

		if (String.isBlank(division)) {
			division = leadObj.Division_from_Region__c;
		}

		if (String.isBlank(language)) {
			language = leadObj.Language_Preference__c;
		}

		if (checkCustomerInfo == true) {
			checkCustomerType();
		}
	}

	public void checkCustomerType() {
		SCustomerTypeInfo customerInfo = null;
		
		if (relatedAccount != null) {
			customerInfo = new SCustomerTypeInfo(relatedAccount);

		} else if (relatedLead != null) {
			customerInfo = new SCustomerTypeInfo(relatedLead);
		}
		String newTier = OmnichannelCustomerConst.TIER_4;

		if (customerInfo != null) {
			customerInfo.division = division;
			customerInfo.segPL = segmentation?.Seg_PL__c;
			relatedCustomerType = 
				customerTypeProvider?.getCustomerType(customerInfo);

			if (customerTierCalculator != null) {
				newTier = customerTierCalculator.getTier(customerInfo);
			}
		}

		updateTier(newTier);
	}

	/**
	 * This method should only be used for Chat Transcript sessions linked to
	 * new leads
	 */
	public void calculateTierForNewLead() {
		String newTier = OmnichannelCustomerConst.TIER_4;
		SCustomerTypeInfo customerInfo = SCustomerTypeInfo.getInfoForNewLead();

		if (customerTierCalculator != null) {
			newTier = customerTierCalculator.getTier(customerInfo);
		}

		updateTier(newTier);
	}

	public void initTier(String tier) {
		this.tier = tier;
		tierHasChanged = false;
	}

	public Boolean updateTier(String newTier) {
		Boolean result = false;
		if (tier != newTier) {
			tier = newTier;
			tierHasChanged = true;
			result = true;
		}
		return result;
	}
	
	@TestVisible
	void checkDeposit() {
		if (
			(type == OmnichanelConst.TYPE_DEPOSIT) && 
			(inquiryNature == OmnichanelConst.INQUIRY_NAT_FUNDING_WITHDRAWAL)
		) {
			deposit = true;
		}
	}
	
}