@isTest
public class LegalAgreementsCtrlTest {
    
    static testMethod void fetchAgreements() {

        insert new LegalAgreement_Settings__c(
            Get_Agreements_Resource__c = '/{user_id}/legal-agreements',
            Client_Id__c = '500d599d11cb4af99f9cdacaa3d59a99',
            Client_Secret__c = 'Ab9D1DBFF9e549e4a812E7548AEdBF8c');

        insert new List<Legal_Agreement_Mapping__c>{
            new Legal_Agreement_Mapping__c(
                Name = 'fxtrade_customer_agreement',
                Code__c = 17,
                Display_Name__c = 'fxtrade customer agreement'),
            new Legal_Agreement_Mapping__c(
                Name = 'fxtrade_additional_disclaimer',
                Code__c = 18,
                Display_Name__c = 'fxtrade additional disclaimer'),
            new Legal_Agreement_Mapping__c(
                Name = '8sf_terms',
                Code__c = 19,
                Display_Name__c = '8sf terms')
        };

        String strLegalAgs =
        '[' +
        '   {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2020-08-24",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"17"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2020-08-25",' +
        '       "update_Datetime":"2021-08-09T22:50:17",' +
        '       "create_datetime":"2021-08-09T22:50:17",' +
        '       "code":"17"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2017-05-08",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"18"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2014-10-29",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"19"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2014-10-30",' +
        '       "update_Datetime":"2021-08-10T21:50:17",' +
        '       "create_datetime":"2021-08-10T21:50:17",' +
        '       "code":"19"' +
        '    }' +
        ' ]';

        // Set test mock with right response
		Test.setMock(
            HttpCalloutMock.class,
            new CalloutMock(
                200,
                strLegalAgs,
                'success',
                null));

        Lead l = new Lead(
            LastName='test',
            Email='test@oanda.com');
        insert l;
        
        fxAccount__c fxAcc = new fxAccount__c(
            Name = 'test',
            RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
            Lead__c = l.Id,
            Division_Name__c = 'OANDA Corporate',
            fxTrade_User_Id__c = 123456);
        insert fxAcc;

        Test.startTest();

        // Retrieve legal agreements
        List<LegalAgreement> legalAgs =
            LegalAgreementsCtrl.fetchAgreements(fxAcc.Id);

        System.assertEquals(
            legalAgs.size(),
            3,
            'Should have 3 legal agreements');

        for(LegalAgreement la :legalAgs) {
            if(la.name == 'fxtrade customer agreement') {
                System.assertEquals(
                    la.version.day(),
                    25,
                    'Have not the right parent. Should be the last version');
                System.assertEquals(
                    la.children.size(),
                    1,
                    'Do not have only one child');
                System.assertEquals(
                    la.children[0].version.day(),
                    24,
                    'Have not the right child. Should be the older version');
            } else if(la.name == 'fxtrade additional disclaimer') {
                System.assertEquals(
                    la.version.day(),
                    8,
                    'Have not the right version day.');
                System.assert(
                    la.children == null,
                    'Should not have children.');
            } else if(la.name == '8sf terms'){
                System.assertEquals(
                    la.version.day(),
                    30,
                    'Have not the right parent. Should be the last version');
                System.assertEquals(
                    la.children.size(),
                    1,
                    'Do not have only one child');
                System.assertEquals(
                    la.children[0].version.day(),
                    29,
                    'Have not the right child. Should be the older version');
            }
        }

        Test.stopTest();
    }

}