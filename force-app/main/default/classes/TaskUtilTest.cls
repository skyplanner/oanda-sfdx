@IsTest
private class TaskUtilTest {
	
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	
	static testMethod void testCheckForWeekendActivityDate()
	{
		Date SAT_OCT_19_2013 = Date.newInstance(2013, 10, 19);
		Date MON_OCT_21_2013 = Date.newInstance(2013, 10, 21);
		
		Task t = new Task(ActivityDate=SAT_OCT_19_2013);
		insert t;
		
		Date activityDate = [SELECT ActivityDate FROM Task WHERE Id=:t.Id].ActivityDate;
		/* TODO System.assert(MON_OCT_21_2013.isSameDay(activityDate)); */
	}
	
	static testMethod void testTaskForOppWithDifferentOwner() {
		
		Opportunity o;
		User u1 = UserUtil.getTestUser('testuser1@oanda.com');
		User u2 = UserUtil.getTestUser('testuser2@oanda.com');
		
		System.runAs(SYSTEM_USER) {
			insert new List<User>{u1, u2};
			
			o = new Opportunity(Name='test opp', OwnerId=u1.Id, StageName='test stage', CloseDate=Date.today());
			insert o;
		}
		
		System.runAs(u2) {
			Task t = new Task(Subject='test task', WhatId=o.Id);
			
			try {
				insert t;
				System.assert(false);
			}
			catch (Exception e) {}
		}
	}
	
	static testMethod void testUpdateLeadStatusForOpp() {
		Opportunity o = new Opportunity(Name='test', StageName='test stage', CloseDate=Date.today());
		insert o;
		
		Task t = new Task(Update_Lead_Status__c='test', WhatId=o.Id);
		insert t;
		
		o = [SELECT Id, Status__c FROM Opportunity WHERE Id=:o.Id][0];
		System.assertEquals('test', o.Status__c);
	}
	
	static testMethod void testUpdateLeadStatusForLead() {
		Lead l = new Lead(LastName='test', Email='test@oanda.com');
		insert l;
		
		Task t = new Task(Update_Lead_Status__c='test', WhoId=l.Id);
		insert t;
		
		l = [SELECT Id, Status FROM Lead WHERE Id=:l.Id][0];
		System.assertEquals('test', l.Status);
	}

	static testMethod void testUpdateLeadStatusForOppWithMultipleTasks() {
		Opportunity o = new Opportunity(Name='test', StageName='test stage', CloseDate=Date.today());
		insert o;
		
		List<Task> taskList = new List<Task>();

		Task t1 = new Task(Update_Lead_Status__c='test', WhatId=o.Id);
		taskList.add(t1);

		Task t2 = new Task(Update_Lead_Status__c='test', WhatId=o.Id);
		taskList.add(t2);

		insert taskList;
		
		o = [SELECT Id, Status__c FROM Opportunity WHERE Id=:o.Id][0];
		System.assertEquals('Not contacted', o.Status__c);
	}
	
	static testMethod void testUpdateLeadStatusForLeadWithMultipleTasks() {
		Lead l = new Lead(LastName='test', Email='test@oanda.com');
		insert l;

		List<Task> taskList = new List<Task>();

		Task t1 = new Task(Update_Lead_Status__c='test', WhoId=l.Id);
		taskList.add(t1);

		Task t2 = new Task(Update_Lead_Status__c='test', WhoId=l.Id);
		taskList.add(t2);

		insert taskList;
		
		l = [SELECT Id, Status FROM Lead WHERE Id=:l.Id][0];
		System.assertEquals('Not Contacted (default)', l.Status);
	}
	
	static testMethod void testInsertPersonaTag() {
		Account a = new Account(Name='test');
		insert a;
		
		Opportunity o = new Opportunity(Name='test', AccountId=a.Id, StageName='test stage', CloseDate=Date.today());
		insert o;
		
		Task t = new Task(Persona__c='test', WhatId=o.Id);
		insert t;
		
		Persona_Tag__c pt = [SELECT Id, Persona__c FROM Persona_Tag__c WHERE Account__c=:a.Id][0];
		System.assertEquals('test', pt.Persona__c);
	}
	
	static testMethod void testInsertPersonaTagAccount() {
		Account a = new Account(Name='test');
		insert a;
		
		Task t = new Task(Persona__c='test', WhatId=a.Id);
		insert t;
		
		Persona_Tag__c pt = [SELECT Id, Persona__c FROM Persona_Tag__c WHERE Account__c=:a.Id][0];
		System.assertEquals('test', pt.Persona__c);
	}
	
	static testMethod void testInsertPersonaTagLead() {
		Lead l = new Lead(LastName='test', Email='test@oanda.com');
		insert l;
		
		Task t = new Task(Persona__c='test', WhoId=l.Id);
		insert t;
		
		Persona_Tag__c pt = [SELECT Id, Persona__c FROM Persona_Tag__c WHERE Lead__c=:l.Id][0];
		System.assertEquals('test', pt.Persona__c);
	}

	//Deepak Malkani : Added few more tests for Lead and Opportunity Status change on Task Inserts + Updates - JIRA Story # SP-2941 and SP-2980

	static testMethod void testLeadStatusChange_onTaskIns(){
		
		Test.startTest();

		//PREPARE TEST DATA
		Lead ldObj = TestDataFactory.createStdLead();
		Task tkObj = TestDataFactory.createTasksforLead(ldObj);
		
		//reset the Task canRun flag as it may have been set to false
        GlobalStaticVar.canRun = true;

		//VALIDATE AND ASSERTIONS
		System.assertEquals('Warm', [SELECT Status FROM Lead WHERE id =: ldObj.Id].Status);
		tkObj.Update_Lead_Status__c = 'Hot';
		update tkObj;
		System.assertEquals('Hot', [SELECT Status FROM Lead WHERE id = : ldObj.Id].Status);
		
		Test.stopTest();
	}
/*
	static testMethod void testLeadStatusBulk_onTaskUp(){

		Test.startTest();

		//PREPARE TEST DATA
		List<Lead> leadInsList = TestDataFactory.createStdLeadsBulk(2);
		List<Task> tskInsList = TestDataFactory.createTasksforLeadBulk(leadInsList);
		List<Task> tskUpdList = new List<Task>();
        
        //reset the Task canRun flag as it may have been set to false
        GlobalStaticVar.canRun = true;
        
		//VALIDATE AND ASSERTIONS
		System.assertEquals(2, [SELECT Count() FROM Lead WHERE Status =: 'Warm']);
		for(Integer i=0; i< tskInsList.size(); i++){
			Task taskUpdObj = new Task(Id = tskInsList[i].Id, Update_Lead_Status__c = 'Hot');
			tskUpdList.add(taskUpdObj);
		}
		System.debug('canRun: ' + GlobalStaticVar.canRun);
		if(!tskUpdList.isEmpty())
			update tskUpdList;
			
		
		System.assertEquals(2, [SELECT Count() FROM Lead WHERE Status =: 'Hot']);

		Test.stopTest();
	}

	static testMethod void testOpptyStatusBulk_onTaskUpd(){

		Test.startTest();

		//PREPARE TEST DATA
		List<Opportunity> opptyInsList = TestDataFactory.createOpptyBulk(200);
		List<Task> tskInsList = TestDataFactory.createTasksforOpptyBulk(opptyInsList);
		List<Task> tskUpdList = new List<Task>();
		
		//reset the Task canRun flag as it may have been set to false
        GlobalStaticVar.canRun = true;

        System.debug('canRun: ' + GlobalStaticVar.canRun);
		//VALIDATE AND ASSERTIONS
		System.assertEquals(200, [SELECT Count() FROM Opportunity WHERE Status__c =: 'Warm']);
		for(Integer i=0; i< tskInsList.size(); i++){
			Task taskUpdObj = new Task(Id = tskInsList[i].Id, Update_Lead_Status__c = 'Hot');
			tskUpdList.add(taskUpdObj);
		} 
		if(!tskUpdList.isEmpty())
			update tskUpdList;
			
	    System.debug('canRun: ' + GlobalStaticVar.canRun);
	    
		System.assertEquals(200, [SELECT Count() FROM Opportunity WHERE Status__c =: 'Hot']);

		Test.stopTest();
	}
*/	
	static testMethod void testGlobalStaticVars(){
		Test.startTest();
		Boolean runTriggerFlag;
		runTriggerFlag = GlobalStaticVar.canIRun();
		system.assertEquals(true, runTriggerFlag);
		GlobalStaticVar.stopTrigger();
		system.assertEquals(false, GlobalStaticVar.canIRun());
		Test.stopTest();
	}

	@IsTest
	static void updateTaskToMeaningfulTest(){
		Account a = new Account(LastName='Test Account');
		insert a;

		Opportunity o = new Opportunity(Name='test opp', StageName='New', AccountId=a.Id, First_Trade_Date__c=Date.today().addDays(-2),
				Initial_Deposit_DateTime__c=System.now(), CloseDate=System.today()+2);
		insert o;
		Test.setCreatedDate(o.Id, Date.today().addDays(-2));

		fxAccount__c fxa = new fxAccount__c(Name='test fxAccount', Account__c=a.Id, Opportunity__c=o.Id, fxTrade_Created_Date__c=Date.today().addDays(-2));
		insert fxa;

		Task task = new Task();
		task.WhatId = o.Id;
		task.Subject = 'Call';
		insert task;

		Task taskAfter = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task.Id LIMIT 1];
		Opportunity oppAfter = [SELECT Meaningful_Interaction__c FROM Opportunity WHERE Id=:o.Id];
		System.assertEquals(false, taskAfter.Meaningful_Interactions__c);
		System.assertEquals(false, oppAfter.Meaningful_Interaction__c);
		System.assertEquals(false, taskAfter.First_Meaningful_Interaction__c);

		task.Meaningful_Interactions__c=true;
		update task;

		Task taskAfter2 = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task.Id LIMIT 1];
		oppAfter = [SELECT Meaningful_Interaction__c FROM Opportunity WHERE Id=:o.Id];
		System.assertEquals(true, taskAfter2.Meaningful_Interactions__c);
		System.assertEquals(true, oppAfter.Meaningful_Interaction__c);
		System.assertEquals(true, taskAfter2.First_Meaningful_Interaction__c);
		System.assertNotEquals(null, taskAfter2.First_MI_DateTime__c);

		Task task2 = new Task();
		task2.WhatId = o.Id;
		task2.Subject = 'Call';
		insert task2;

		Task task2After = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task2.Id LIMIT 1];
		System.assertEquals(false, task2After.Meaningful_Interactions__c);
		System.assertEquals(false, task2After.First_Meaningful_Interaction__c);
		System.assertEquals(null, task2After.First_MI_DateTime__c);

		task2.CallDurationInSeconds=120;
		update task2;


		Task task2After2 = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task2.Id LIMIT 1];
		System.assertEquals(true, task2After2.Meaningful_Interactions__c);
		System.assertEquals(false, task2After2.First_Meaningful_Interaction__c);
		System.assertEquals(null, task2After2.First_MI_DateTime__c);



	}


	@IsTest
	static void insertTaskMeaningfulTest(){
		Account a = new Account(LastName='Test Account');
		insert a;

		Opportunity o = new Opportunity(Name='test opp', StageName='New', AccountId=a.Id, First_Trade_Date__c=Date.today().addDays(-2),
				Initial_Deposit_DateTime__c=System.now(), CloseDate=System.today()+2);
		insert o;
		Test.setCreatedDate(o.Id, Date.today().addDays(-2));

		fxAccount__c fxa = new fxAccount__c(Name='test fxAccount', Account__c=a.Id, Opportunity__c=o.Id, fxTrade_Created_Date__c=Date.today().addDays(-2));
		insert fxa;

		Task task = new Task();
		task.WhatId = o.Id;
		task.Subject = 'Call';
		task.Meaningful_Interactions__c=true;
		insert task;

		Task taskAfter = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task.Id LIMIT 1];
		Opportunity oppAfter = [SELECT Meaningful_Interaction__c FROM Opportunity WHERE Id=:o.Id];
		System.assertEquals(true, taskAfter.Meaningful_Interactions__c);
		System.assertEquals(true, oppAfter.Meaningful_Interaction__c);
		System.assertEquals(true, taskAfter.First_Meaningful_Interaction__c);
		System.assertNotEquals(null, taskAfter.First_MI_DateTime__c);

		Task task2 = new Task();
		task2.WhatId = o.Id;
		task2.Subject = 'Call';
		task2.Meaningful_Interactions__c=true;
		insert task2;

		Task taskAfter2 = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task2.Id LIMIT 1];
		System.assertEquals(true, taskAfter2.Meaningful_Interactions__c);
		System.assertEquals(false, taskAfter2.First_Meaningful_Interaction__c);
		System.assertEquals(null, taskAfter2.First_MI_DateTime__c);
		
		Task task3 = new Task();
		task3.WhatId = o.Id;
		task3.Subject = 'Call';
		task3.Meaningful_Interactions__c=false;
		insert task3;

		Task taskAfter3 = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task3.Id LIMIT 1];
		System.assertEquals(false, taskAfter3.Meaningful_Interactions__c);
		System.assertEquals(false, taskAfter3.First_Meaningful_Interaction__c);
		System.assertEquals(null, taskAfter3.First_MI_DateTime__c);
		
		Task task4 = new Task();
		task4.WhatId = o.Id;
		task4.Subject = 'Call';
		task4.CallDurationInSeconds=121;
		insert task4;

		Task taskAfter4 = [SELECT Id, Meaningful_Interactions__c, MI_Included_Client__c, First_Meaningful_Interaction__c,
				First_MI_DateTime__c, MI_Included_Client_DateTime__c FROM Task WHERE Id=:task4.Id LIMIT 1];
		System.assertEquals(true, taskAfter4.Meaningful_Interactions__c);
		System.assertEquals(false, taskAfter4.First_Meaningful_Interaction__c);
		System.assertEquals(null, taskAfter4.First_MI_DateTime__c);

	}

	@IsTest
    static void successSyncCallResultServiceTest(){
        Task testTask1 = new Task(CallDisposition = 'No Answer');
        Task testTask2 = new Task(CallDisposition = 'Test');
        Task testTask3 = new Task();
        
        Test.startTest();
        TaskUtil.syncCallResultService(new List<Task> {testTask1, testTask2, testTask3});
        Test.stopTest();

        System.assertEquals(testTask1.CallDisposition, testTask1.Call_Result__c, '');
        System.assertEquals(null, testTask2.Call_Result__c, '');
        System.assertEquals(null, testTask3.Call_Result__c, '');
    }

	@IsTest
    static void completeUnansweredTaskTest(){
        Task task1 = new Task(Subject = 'Call');
		insert task1;

        Test.startTest();
        task1.No_answer__c=true;
		update task1;
        Test.stopTest();

		Task taskAfter = [SELECT Status, Update_Lead_Status__c FROM Task WHERE Id=:task1.Id LIMIT 1];
        System.assertEquals('No Answer - Call attempt 1', taskAfter.Update_Lead_Status__c);
        System.assertEquals('Completed', taskAfter.Status);
    }


}