public with sharing class StringUtil {
	
	public static boolean isUpperCaseCharacter(String c) {
		System.debug('c: ' + c);
		if(c.length()<>1) {
			System.debug('false');
			return false;
		}
		return c.toUpperCase().equals(c);
	}
	
	public static boolean isLowerCaseCharacter(String c) {
		System.debug('c: ' + c);
		if(c.length()<>1) {
			System.debug('false');
			return false;
		}
		return c.toLowerCase().equals(c);
	}
	
	// useful for csv files
	// this method replaces commas with semicolon, if the character after the comma is uppercase
	public static String replaceCommaWithSemicolon(String s) {
		if(s==null) {
			return null;
		}
		integer indexPrevComma = 0;
		integer indexComma = s.indexOf(', ', indexPrevComma);
		while(indexComma>0) {		
			integer indexNextCharacter = indexComma + 2;
			// only replace ',' with ';' if the next character is uppercase
			if(isUpperCaseCharacter(s.substring(indexNextCharacter, indexNextCharacter+1))) {
				s = s.substring(0, indexComma) + ';' + s.substring(indexNextCharacter);
			}
			indexPrevComma = indexNextCharacter;
			indexComma = s.indexOf(', ', indexPrevComma);
		}
		
		s = s.replaceAll(' ;', ';');
		return s;
	}

	/**
	 * @return if input is null or empty, returns the replacement,
	 * otherwise returns input
	 * @param input
	 * @param replacement
	 */
	public static string replaceIfBlankWithQuotes(String input, String replacement) {
		return '"' + replaceIfBlank(input, replacement) + '"';
	}

	/**
	 * @return if input is null or empty, returns the replacement,
	 * otherwise returns input
	 * @param input
	 * @param replacement
	 */
	public static string replaceIfBlank(String input, String replacement) {
		return String.isBlank(input) ? replacement : input;
	}

	/**
	 * @return string replacing {0} by replacement1.
	 * @param input
	 * @param replacement1
	 */
	public static String format(
			String input,
			String replacement1) {
		return String.isBlank(input) ? '' :
			String.format(input, new List<String> { replacement1 });
	}

	/**
	 * @return string replacing {0} by replacement1.
	 * @param input
	 * @param replacement1
	 */
	public static String format(
			String input,
			Object replacement1) {
		return format(input, String.valueOf(replacement1));
	}

	/**
	 * @return string replacing {0} by replacement.
	 * @param input
	 * @param replacement1
	 * @param replacement2
	 */
	public static String format(
			String input,
			String replacement1,
			String replacement2) {
		return String.isBlank(input) ? '' :
			String.format(input, new List<String> {
				replacement1, replacement2 });
	}

	/**
	 * @return give [{k1: 'v1', k2: 'v2'}, {k3: 'v3', k4: 'v4'}]
	 * returns "v1, {v2, v3, v4". Returns an empty String if no values
	 * @param input
	 * @param separator
	 */
	public static String joinValues(List<Map<String, String>> input, String separator) {
		List<String> values;

		if (input == null || input.isEmpty())
			return '';

		values = new List<String>();
		for (Map<String, String> item : input)
			values.addAll(item.values());

		return String.join(values, separator);
	}

}