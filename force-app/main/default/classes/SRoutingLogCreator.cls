/**
 * @File Name          : SRoutingLogCreator.cls
 * @Description        : 
 * Returns the routing log corresponding to the BaseServiceRoutingInfo record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/27/2024, 8:06:39 PM
**/
public interface SRoutingLogCreator {

	SObject getRoutingLog(BaseServiceRoutingInfo routingInfo);

}