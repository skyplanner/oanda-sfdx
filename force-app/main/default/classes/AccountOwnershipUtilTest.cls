@isTest
public with sharing class AccountOwnershipUtilTest {
    
    @testSetup
	static void setup () {
		Account acc1;
        String accField;
        fxAccount__c fxAcc1;

        RecordType personAccountRecordType =
            [SELECT Id 
                FROM RecordType 
                WHERE DeveloperName = 'PersonAccount' 
                AND SObjectType = 'Account'];

        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.ACCOUNT);
        
        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAcc1 = new fxAccount__c(
            Lead__c = lead1.Id,
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Government_ID__c = '0123456789');
        insert fxAcc1;
        
        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.ACCOUNT);

        acc1 = new Account(
            fxAccount__c = fxAcc1.Id, 
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            PersonEmail = 'a@b.com',
            FirstName = 'Bartolo',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            LastName = 'Colon',
            MiddleName = 'Iglesias',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155',
            Suffix = 'Suff1',
            Salutation = 'Salut1',
            RecordTypeId = personAccountRecordType.Id);
        insert acc1;

        // Not 'Owner Changed' task created when inserting
        System.assert([SELECT Id FROM Task LIMIT 1].isEmpty());
	}

    /**
     * Create a user with right conditions
     * and the task should be created
     */
    @isTest
    private static void createTaskOwnerChanged() {
        User tUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            UserName = 'testusername01@testemail.com');
        insert tUser;

        Account acc1 =
            [SELECT Id,
                    OwnerId
                FROM Account];

        // Update account owner
        acc1.OwnerId = tUser.Id;
        update acc1;

        Task taskOwnerChaged =
            [SELECT WhatId,
                    OwnerId,
                    Subject,
                    Status,
                    Priority
                FROM Task
                WHERE WhatId = :acc1.Id
                LIMIT 1];

        // Verify if 'Owner Change' task 
        // was created properly

        System.system.assertEquals(
            taskOwnerChaged.Subject,
            'New Account Owner');

        System.system.assertEquals(
            taskOwnerChaged.OwnerId,
            tUser.Id);

        System.system.assertEquals(
            taskOwnerChaged.Status,
            TaskUtil.STATUS_NOT_STARTED);
        
        System.system.assertEquals(
            taskOwnerChaged.Priority,
            TaskUtil.PRIORITY_NORMAL);
    }

    /**
     * Create a user with LastName = 'House User
     * and the task should not be created
     */
    @isTest
    private static void noCreateTaskOwnerChanged() {
        User tUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            FirstName = 'Retention',
            LastName = 'House User', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            UserName = 'testusername01@testemail.com');
        insert tUser;

        Account acc1 =
            [SELECT Id,
                    OwnerId
                FROM Account
                WHERE FirstName = 'Bartolo'];

        // Update account owner
        acc1.OwnerId = tUser.Id;
        update acc1;

        // Not 'Owner Changed' task should be created
        System.assert(
            [SELECT Id
                FROM Task 
                WHERE WhatId = :acc1.Id
                LIMIT 1].isEmpty());
    }

}