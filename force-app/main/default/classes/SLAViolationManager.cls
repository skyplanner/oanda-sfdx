/**
 * @File Name          : SLAViolationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/8/2021, 10:27:40 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/8/2021, 10:27:40 AM   acantero     Initial Version
**/
public without sharing abstract class SLAViolationManager {

    public static void saveViolationInfo(List<SLAViolationInfo> violationInfoList) {
        if (
            (violationInfoList == null) ||
            violationInfoList.isEmpty()
        ) {
            return;
        }
        //else...
        List<SLA_Violation__c> slaViolationList = 
            new List<SLA_Violation__c>();
        for(SLAViolationInfo info : violationInfoList) {
            if (info.isPersistent == true) {
                slaViolationList.add(
                    new SLA_Violation__c(
                        Agent__c = info.ownerId,
                        Case__c = info.caseId,
                        Channel__c = info.channel,
                        Division__c = info.division,
                        Inquiry_Nature__c = info.inquiryNature,
                        Is_HVC__c = info.isHVC,
                        Language__c = info.language,
                        Live_or_Practice__c = info.liveOrPractice,
                        Violation_Type__c = info.violationType
                    )
                );
            }
        }
        if (!slaViolationList.isEmpty()) {
            insert slaViolationList;
        }
    }

}