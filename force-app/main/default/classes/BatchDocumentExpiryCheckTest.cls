@isTest (seeAllData = false)
public class BatchDocumentExpiryCheckTest {

    final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);
    
    public static Lead lead;
    public static Document__c docObj;
    public static Account accnt;
    public static fxAccount__c fxa;
    
    @testSetup
    static void init()
    {
        lead = new Lead();
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        lead.LastName = 'Last1';
        lead.Email = 'test@abc.com';
        insert lead;
        
        fxa = new fxAccount__c();
        fxa.Funnel_Stage__c = FunnelStatus.TRADED;
        fxa.Ready_for_Funding_Date__c = Date.today().addDays(-1);
        fxa.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxa.Lead__c = lead.Id;
        insert fxa;
        
		docObj = new Document__c();
        docObj.name = 'Passport';
        docObj.Lead__c = lead.Id;
        docObj.fxAccount__c = fxa.Id;
        docObj.Document_Type__c = 'Passport';
        docObj.Expiry_Date__c = Date.today();
        insert docObj;
    }
    
    static testMethod void testCaseCreation()
    {
        fxAccount__c fxa = [select id from fxAccount__c limit 1];
        
        Test.startTest();

        String query = 'select Id, Name, Document_Type__c, Expiry_Date__c, Account__c, fxAccount__c, fxAccount__r.Name, fxAccount__r.fxTrade_User_Id__c, fxAccount__r.Contact__c, fxAccount__r.Division_Name__c,lead__c From Document__c where Expiry_Date__c != null and Expiry_Date__c = TODAY and fxAccount__c != null and fxAccount__r.Is_Closed__c = false and fxAccount__r.Ready_for_Funding_DateTime__c != null and fxAccount__r.RecordType.Name = \'Retail Live\'';
        BatchDocumentExpiryCheck batch = new BatchDocumentExpiryCheck(query);
        Database.executeBatch(batch);
        
        Test.stopTest();
        
        Case[] documentExpiryNotificationCases = [Select Id,OwnerId From Case where fxAccount__c = :fxa.Id and subject = 'Document Expiry Notification'];
        
        system.assertEquals(True, documentExpiryNotificationCases.size() == 1);    
    }
}