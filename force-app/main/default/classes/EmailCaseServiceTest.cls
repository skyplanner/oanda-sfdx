/**
 * @Description  : Test class for EmailCaseService
 * @Author       : Jakub Fik
 * @Date         : 2024-05-31
**/
@isTest
public with sharing class EmailCaseServiceTest {

    /**
     * @description Null test for reopenCases method.
     * @author Jakub Fik | 2024-05-29
     **/
    @IsTest
    static void nullTest() {
        Boolean isError = false;
        Test.startTest();
        try {
            new EmailCaseService().reopenCases(null);
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();
        System.assertEquals(false, isError, 'Method should not throw an error.');
    }

    /**
     * @description Test for the reopenCases method when query requirements are not met.
     * @author Jakub Fik | 2024-05-29
     **/
    @IsTest
    static void noMatchingCasesTest() {
        TriggerHandler.bypass('CaseTriggerHandler');
        Case testCase = new Case(
            Status = Constants.CASE_STATUS_PENDING
        );
        insert testCase;
        Boolean isError = false;
        Test.startTest();
        try {
            new EmailCaseService().reopenCases(new Set<Id>{ testCase.Id });
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();
        Case updatedCase = [SELECT Id, Status FROM Case WHERE id = :testCase.Id];

        System.assertEquals(false, isError, 'Method should not throw an error.');
        System.assertEquals(testCase.Status, updatedCase.Status, 'Status of the case should not be updated.');
    }

    /**
     * @description Success test for reopenCases method.
     * @author Jakub Fik | 2024-05-29
     **/
    @IsTest
    static void successTest() {
        TriggerHandler.bypass('CaseTriggerHandler');
        Case testCase = new Case(
            Status = Constants.CASE_STATUS_WAITING_ON_DATE,
            Waiting_for_Date__c = date.today()
        );
        insert testCase;
        Test.startTest();
        new EmailCaseService().reopenCases(new Set<Id>{ testCase.Id });
        Test.stopTest();
        Case updatedCase = [SELECT Id, Status FROM Case WHERE id = :testCase.Id];

        System.assertEquals(Constants.CASE_STATUS_RE_OPENED, updatedCase.Status, 'Case status should be changed into re-opened.');
    }

    /**
    * @description Success test for manageOtmsNotificationCases method.
    * @author Jakub Fik | 2024-07-18 
    **/
    @IsTest
    static void manageOtmsNotificationCasesTest() {
        String testSubject = 'New document uploaded - test@email.com';
        Case testCase = new Case (
            Subject = testSubject,
            RecordTypeId = RecordTypeUtil.getOTMSOnboardingCaseRecordTypeId()
        );
        insert testCase;

        EmailMessage testEmail = new EmailMessage(
            Subject = testSubject,
            ParentId = testCase.Id
        );
        insert testEmail;
        Test.startTest();
        Case newTestCase = testCase.clone();
        insert newTestCase;
        EmailMessage newTestEmail = new EmailMessage(
            Subject = testSubject,
            ParentId = newTestCase.Id
        );
        insert newTestEmail; 
        Test.stopTest();

        EmailMessage resignParentEmail = [SELECT Id, ParentId FROM EmailMessage where id = :newTestEmail.Id];
        Integer deletedCase = [SELECT COUNT() FROM CASE WHERE ID = :newTestCase.Id];

        System.assertEquals(testCase.Id, resignParentEmail.ParentId, 'Case should be reassign into first case with same subject.');
        System.assertEquals(0, deletedCase, 'Second case with same Subject should be removed.');
    }
}