/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-23-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiLanguage {
    public String abbr {get; set;}

    public String active {get; set;}

    public Integer id {get; set;}

    public Integer modified_timestamp {get; set;}

    public String name {get; set;}
}