/**
 * @File Name          : LiveChatWizardCtrl.cls
 * @Description        : 
 * @Author             : Daymi Morales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 09-16-2022
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/01/2019   Daymi Morales     Initial Version
**/
public without sharing class LiveChatWizardCtrl extends BaseLiveChatCtrl {

	transient String previuosPageUrl;

	public String jsonTypeAccountDepend {get;set;}  
	public String jsonInquiryNatureDepend {get;set;}  
	public String jsonPickListAccountValues {get;set;}
	public String jsonPickListInquiryValues {get;set;}
	public String jsonPickListQuestionValues {get;set;}
	public String jsonPickListDivision {get;set;}

	public Integer initialStep {get; private set;}
	public Integer canJumpToStep {get; private set;}
	public Boolean isAnAccount {get; private set;}
	public Boolean accountIsHVC {get; private set;}
	public Boolean isCryptoValid {get; private set;}
	public Boolean isCryptoEnabled {get; private set;}
	public String accountDivision {get; private set;}
	public String accountDivisionName {get; private set;}
	public String fxAccount {get; private set;}
	public String ldRecordTypeId {get; private set;}
	public String cryptoDivisions {get; private set;}

	public List<CountryInfo> countryList {get; private set;}
	
	public LiveChatWizardCtrl() {
		super();
		Map<String,String> pageParams = ApexPages.currentPage().getParameters();
		countryList = getCountryList();
		preparePicklistFieldsInfo();
		checkAccountRelatedParams(pageParams);
		cryptoDivisions = getCryptoDivisionsStr();
	}

	public String getBackUrl() {
		return Page.LiveChatLanguage.getUrl();
	}

	public String getNextUrl() {
		return Page.LiveChatAgent.getUrl();
	}

	public String getOfflineUrl() {
		return Page.LiveChatOfflineHours.getUrl();
	}

	void checkAccountRelatedParams(Map<String,String> pageParams) {
		//defuault values
		isAnAccount = false;
		accountIsHVC = false;
		isCryptoValid = false;
		fxAccount = '';

		String lastName = pageParams.get(ChatConst.CUSTOMER_LAST_NAME);
		String email = pageParams.get(ChatConst.CUSTOMER_EMAIL);
		String accountType = pageParams.get(ChatConst.ACCOUNT_TYPE);

		Boolean isValidEmail = SPEmailUtil.checkEmail(email);
		Boolean isValidAccountType = SPPicklistUtil.isValidValue(
			Chat_Extension__c.Live_or_Practice_Account__c, 
			accountType
		);
		
		if (isValidEmail) {
			checkAccount(email);
		}

		isCryptoEnabled = CustomSettings.isCryptoEnabledForOmniChannel();
		initialStep = isCryptoEnabled ? 1 : 2;
		canJumpToStep = isCryptoEnabled ? 2 : 3;

		if (String.isNotBlank(lastName) && isValidEmail) {
			canJumpToStep = isAnAccount && isValidAccountType ? 4 : 3;
		}
	}
	
	void checkAccount(String email) {
		ChatClientInfo clientInfo = 
			OmnichanelUtil.getClientInfoByEmail(email);
		if (clientInfo != null) {
			isAnAccount = clientInfo.isAnAccount;
			fxAccount = clientInfo.fxAccount;
			if (isAnAccount == true) {
				accountIsHVC = clientInfo.isHVC;
				isCryptoValid = clientInfo.isCryptoValid;
				accountDivision = clientInfo.division;
				accountDivisionName = clientInfo.divisionName;
			}
		}
	}

	void preparePicklistFieldsInfo() {
		jsonTypeAccountDepend = getDependentPicklistValues(
			Chat_Extension__c.Inquiry_Nature__c
		);
				
		jsonInquiryNatureDepend = getDependentPicklistValues(
			Chat_Extension__c.Specific_Question__c
		);
				
		jsonPickListAccountValues = getPicklistValuesAndLabels(
			Chat_Extension__c.Live_or_Practice_Account__c
		);

		jsonPickListInquiryValues =  getPicklistValuesAndLabels(
			Chat_Extension__c.Inquiry_Nature__c
		); 

		jsonPickListQuestionValues =  getPicklistValuesAndLabels(
			Chat_Extension__c.Specific_Question__c
		);

		jsonPickListDivision =  getPicklistValuesAndLabels(
			Chat_Extension__c.Chat_Division__c
		);          
	}

	public static String getDependentPicklistValues(
		Schema.sObjectField dependToken
	){
		Map<Object,List<String>> dependentPicklistValues =
		Utilities.getDependentPicklistValues(dependToken);
		return JSON.serialize(dependentPicklistValues); 
	}

	public static String getPicklistValuesAndLabels(
		Schema.sObjectField pickListName
	){
		Map<String, String> pickList = new Map<String, String>();
		Schema.DescribeFieldResult fieldResult = pickListName.getDescribe();  
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();  
		for (Schema.PicklistEntry f : ple) {
			pickList.put(f.getLabel(),f.getValue());         
		} 
		return JSON.serialize(pickList);   
	} 

	@RemoteAction
	public static ChatClientInfo getClientInfoByEmail(String email) {
		return OmnichanelUtil.getClientInfoByEmail(email);
	}

	@testVisible
	List<CountryInfo> getCountryList() {
		List<CountryInfo> result = new List<CountryInfo>();
		List<Country_Code__mdt> portalCountries = CountryCodeMetadataUtil.getPortalCountries();
		for(Country_Code__mdt countryMdt : portalCountries) {
			result.add(
				new CountryInfo(
					countryMdt.Country__c,
					countryMdt.Region__c,
					countryMdt.DeveloperName
				)
			);
		}
		return result;
	}

	@testVisible
	String getCryptoDivisionsStr() {
		List<String> divisionCodeList = 
			CryptoDivisionSettings.getDivisionCodeList();
		String cryptoDivisionsStr = String.join(divisionCodeList, ',');
		String result = ',' + cryptoDivisionsStr + ',';
		return result;
	}

	//*************************************************************************** */

	public class CountryInfo {

		public String country {get; set;}
		public String region {get; set;}
		public String devName {get; set;}

		public CountryInfo(
			String country, 
			String region,
			String devName
		) {
			this.region = region;
			this.country = country;
			this.devName = devName;
		}
		
	}

    
}