public without sharing class LiveChatEventMessageCtrl extends HelpPortalBase {

	public String langPref {get; set;}

	public String getActiveEventMessageEmail {
		get {
			String activeEventMessage = EventMessageUtil.getActiveEventMessageLang(langPref, 'Email');
		    return activeEventMessage;
		}
		set;
	}

	public String getActiveEventMessageChat {
		get {
			String activeEventMessage = EventMessageUtil.getActiveEventMessageLang(language, 'Chat Form');
		    return activeEventMessage;
		}
		set;
	}

	public String getNextUrl() {
		return Page.LiveChatWizard.getUrl();
	}
}