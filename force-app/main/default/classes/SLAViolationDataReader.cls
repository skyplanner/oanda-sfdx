/**
 * @File Name          : SLAViolationDataReader.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 4:24:33 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 4:24:33 PM   aniubo     Initial Version
**/
public interface SLAViolationDataReader {
	List<SObject> getData(SLAViolationNotificationData notificationData);
}