/**
 * AccountSyncUtil test class
 */
@isTest
public class AccountSyncUtilTest {
    
    /**
     * When update acc.fxAccount__c and:
     *  - acc.PersonEmail with Oanda domain
     *  - acc pointing to a LIVE fxAccount
     *  - acc.fxAccount with "fxTrade_User_Id__c" filled
     * Then, a 'SYNC' action platform event should be published
     */
    @isTest
    private static void publishSyncAccActionsFromUpdate() {
        Lead lead = new Lead(
            LastName='test lead',
            Email='test@test.com');
        insert lead;

        fxAccount__c fxAcc1 = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM,
            Birthdate__c = System.today().addYears(-36),
            Government_ID__c = '0123456789',
            fxTrade_User_Id__c = 8897,
            Lead__c = lead.Id);

        fxAccount__c fxAcc2 = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM,
            Birthdate__c = System.today().addYears(-36),
            Government_ID__c = '999555',
            fxTrade_User_Id__c = 9659,
            Lead__c = lead.Id);
        
        TriggersUtil.disableObjTriggers(
            TriggersUtil.Obj.FXACCOUNT);
        
        insert new List<fxAccount__c>{
            fxAcc1, fxAcc2};   
        
        TriggersUtil.enableObjTriggers(
            TriggersUtil.Obj.FXACCOUNT); 

        // Create acount pointing to fxAcc1

        TriggersUtil.disableObjTriggers(
            TriggersUtil.Obj.ACCOUNT);
        
        Account acc = new Account(
            LastName = 'test account',
            PersonEmail = '123@test.com',
            fxAccount__c = fxAcc1.Id);
        
        insert acc;
        
        TriggersUtil.enableObjTriggers(
            TriggersUtil.Obj.ACCOUNT); 

        // No SYNC action published yet because trigger were disabled
        System.assert(
            ActionUtil.lastPublished == null);

        // Update acc.fxAccount__c to point to fxAcc2
        acc.fxAccount__c = fxAcc2.Id;
        update acc;

        // Should be published a 'SYNC' platform event
        System.assertEquals(
            ActionUtil.lastPublished.size(), 1,
            'Sync action was not published');

        Action__e syncAction =
            ActionUtil.lastPublished[0];

        // Review platform event fields

        System.assertEquals(
            syncAction.Type__c,
            ActionUtil.SYNC_TYPE,
            'Sync action has wrong type');

        System.assertEquals(
            String.valueOf(syncAction.fxTrade_User_Id__c),
            '9659',
            'Sync action has wrong fxTradeUserId');
    }

}