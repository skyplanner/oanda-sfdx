/**
 * @File Name          : ServiceSkillManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/24/2021, 1:08:06 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 12:05:34 PM   acantero     Initial Version
**/
@isTest
private without sharing class ServiceSkillManager_Test {

    @isTest
    static void test() {
        /* Service_Skill__mdt skillInfo = 
            ServiceResourceTestDataFactory.getValidSkillInfoFromSettings();
        Skill skillObj = 
            ServiceResourceTestDataFactory.getSkill(skillInfo.Skill_Dev_Name__c);
        Set<String> skillDevNameSet = new Set<String> {skillObj.DeveloperName};
        Set<String> skillIdSet = new Set<String> {skillObj.Id};
        Test.startTest();
        String result1 = ServiceSkillManager.getSkillsSummary(skillDevNameSet);
        String result2 = ServiceSkillManager.getSkillsSummary(null);
        Map<String,String> result3 = ServiceSkillManager.getSkillCodeMapFromIds(skillIdSet);
        Map<String,String> result4 = ServiceSkillManager.getSkillCodeMapFromIds(null);
        Map<String,String> result5 = ServiceSkillManager.getSkillCodeMapFromDevNames(skillDevNameSet);
        Map<String,String> result6 = ServiceSkillManager.getSkillCodeMapFromDevNames(null);
        Set<String> result7 = ServiceSkillManager.getSkillCodeSetFromDevNames(skillDevNameSet);
        Set<String> result8 = ServiceSkillManager.getSkillCodeSetFromDevNames(null);
        Test.stopTest();
        System.assert(String.isNotBlank(result1));
        System.assertEquals('', result2);
        System.assertEquals(1, result3.size());
        System.assertEquals(0, result4.size());
        System.assertEquals(1, result5.size());
        System.assertEquals(0, result6.size());
        System.assertEquals(1, result7.size());
        System.assertEquals(0, result8.size()); */
    }
    
}