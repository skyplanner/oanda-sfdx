/**
 * @File Name          : SPMilestoneTimeCalculator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/16/2020, 7:23:27 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/16/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class SPMilestoneTimeCalculator_Test {

    //valid milestone id
    @isTest
    static void calculateMilestoneTriggerTime_test1() {
        Milestone_Time_Settings__mdt settings = [
            SELECT 
                Time_Trigger__c, 
                Milestone_Type_ID__c 
            FROM 
                Milestone_Time_Settings__mdt 
            LIMIT
                 1
        ];
        Integer timeTrigger = Integer.valueOf(settings.Time_Trigger__c);
        Test.startTest();
        Integer result = 
            new SPMilestoneTimeCalculator()
                .calculateMilestoneTriggerTime(
                    null, 
                    settings.Milestone_Type_ID__c
                );
        Test.stopTest();
        System.assertEquals(timeTrigger, result);
    }

    //invalid milestone id
    @isTest
    static void calculateMilestoneTriggerTime_test2() {
        Test.startTest();
        Integer result = 
            new SPMilestoneTimeCalculator()
                .calculateMilestoneTriggerTime(
                    null, 
                    'invalid id'
                );
        Test.stopTest();
        System.assertEquals(1, result);
    }
}