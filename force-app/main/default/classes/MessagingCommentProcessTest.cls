/**
 * @File Name          : MessagingCommentProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/21/2023, 12:26:49 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/21/2023, 12:26:24 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class MessagingCommentProcessTest {
	static Id MessagingSessionId = '0MwDU000000020N0AQ';
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		ServiceTestDataFactory.createLead1(initManager);
		initManager.storeData();
	}
	@isTest
	private static void testMessagingCommentProcessContractors() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingCommentProcess process = new MessagingCommentProcess(
			MessagingSessionId
		);
		Assert.areEqual(true, process != null, 'process is not null');
		process = new MessagingCommentProcess(MessagingSessionId, 'Key');
		Assert.areEqual(true, process != null, 'process is not null');
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testCreateCaseComments() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		// Actual test
		Test.startTest();
		MessagingCommentProcess process = new MessagingCommentProcess(
			MessagingSessionId
		);
		Integer commentNumber = process.createCaseComments(caseId);
		Test.stopTest();

		Assert.areEqual(
			0,
			commentNumber,
			'commentNumber is 0 because session is not exists'
		);

		// Asserts
	}
	@isTest
	private static void testCreateCaseComments1() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		List<Messaging_Comment__c> msgComments = new List<Messaging_Comment__c>{
			new Messaging_Comment__c(Body__c = 'test'),
			new Messaging_Comment__c(
				Body__c = 'test',
				Name = MessagingCommentProcess.CASE_REASON_KEY
			)
		};
		// Actual test
		Test.startTest();
		MessagingCommentProcess process = new MessagingCommentProcess(
			MessagingSessionId
		);
		Integer commentNumber = process.createCaseComments(caseId, msgComments);
		Test.stopTest();

		Assert.areEqual(2, commentNumber, 'commentNumber  must be 2');

		// Asserts
	}
	@isTest
	private static void testUpsertComment() {
		// Test data setup
		// Actual test
		Test.startTest();
		MessagingCommentProcess process = new MessagingCommentProcess(
			MessagingSessionId
		);
		Boolean isSuccess = process.upsertComment('subject', 'body');
		Test.stopTest();

		Assert.areEqual(true, isSuccess, 'isSuccess must be upsert ok');

		// Asserts
	}
	@isTest
	private static void testUpsertCommentBodyIsBlank() {
		// Test data setup
		// Actual test
		Test.startTest();
		MessagingCommentProcess process = new MessagingCommentProcess(
			MessagingSessionId
		);
		Boolean isSuccess = process.upsertComment('subject', null);
		Test.stopTest();

		Assert.areEqual(
			false,
			isSuccess,
			'isSuccess must be false because body is blank'
		);

		// Asserts
	}
}