/**
 * @File Name          : OmnichanelUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/17/2022, 10:45:15 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/17/2020   acantero     Initial Version
**/
public without sharing class OmnichanelUtil {

    public static ID getRequiredQueueId(String queueDevName, String errorMsg) {
        List<Group> queueList = [
            SELECT
                Id
            FROM
                Group
            WHERE 
                DeveloperName = :queueDevName
        ];
        if (!queueList.isEmpty()) {
            return queueList[0].Id;
        }
        //else...
        if (String.isBlank(errorMsg)) {
            errorMsg = 'Queue "' + queueDevName + '" not found';
        }
        throw new OmnichanelRoutingException(errorMsg);
    }

    public static Set<String> getHVCEmailSet(Set<String> emailSet) {
        List<Account> accountList = [
            SELECT 
                Id, 
                Is_High_Value_Customer__c,
                PersonEmail
            FROM
                Account 
            WHERE
                PersonEmail IN :emailSet
            AND
                PersonEmail <> NULL
        ];
        Set<String> result = new Set<String>();
        for(Account accountObj : accountList) {
            if (accountObj.Is_High_Value_Customer__c) {
                result.add(accountObj.PersonEmail);
            }
        }
        return result;
    }

    public static Set<String> getHVCAccountIdSet(Set<String> accountIdSet) {
        List<Account> accountList = [
            SELECT 
                Id, 
                Is_High_Value_Customer__c
            FROM
                Account 
            WHERE
                Id IN :accountIdSet
        ];
        Set<String> result = new Set<String>();
        for(Account accountObj : accountList) {
            if (accountObj.Is_High_Value_Customer__c) {
                result.add(accountObj.Id);
            }
        }
        return result;
    }

    public static ChatClientInfo getClientInfoByEmail(String email) {
        ChatClientInfo clientInfo = getAccountInfoByEmail(email);
        if (clientInfo == null) {
            clientInfo = getLeadInfoByEmail(email);
        }
        return clientInfo;
    }

    public static ChatClientInfo getAccountInfoByEmail(String email) {
        ChatClientInfo result = null;
        if (String.isBlank(email)) {
            return result;
        }
        //else...
        List<Account> accountList = [
            SELECT 
                Id, 
                Is_High_Value_Customer__c,
                Primary_Division_Name__c,
                fxAccount__c
            FROM
                Account 
            WHERE
                PersonEmail = :email  
            AND
                PersonEmail <> NULL
            LIMIT 1
        ];
        if (!accountList.isEmpty()) {
            Account accountObj = accountList[0];
            String divisionCode = 
                ServiceDivisionsManager.getInstance().getDivisionCode(
                    accountObj.Primary_Division_Name__c
                );
            result = ChatClientInfo.getAccountInfo(
                accountObj.Is_High_Value_Customer__c,
                divisionCode,
                accountObj.fxAccount__c
            );
            result.divisionName = accountObj.Primary_Division_Name__c;
            result.isCryptoValid = 
                CryptoDivisionSettings.exists(divisionCode);
        }
        return result; 
    }

    public static ChatClientInfo getLeadInfoByEmail(String email) {
        ChatClientInfo result = null;
        if (String.isBlank(email)) {
            return result;
        }
        //else...
        List<Lead> leadList = [
            SELECT 
                Id, 
                fxAccount__c, 
                RecordTypeId
            FROM
                Lead 
            WHERE
                Email = :email  
            AND
                Email <> NULL
            AND
                IsConverted = false
            LIMIT 1
        ];
        if (!leadList.isEmpty()) {
            Lead leadObj = leadList[0];
            result = ChatClientInfo.getLeadInfo(
                leadObj.fxAccount__c,
                leadObj.RecordTypeId
            );
        }
        return result; 
    }

}