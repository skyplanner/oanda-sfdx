public with sharing class fxAccountViewCon {
	
	public final static String SOBJECT_NAME_ACCOUNT = 'Account';
	public List<fxAccount__c> records {get; set;}
	
	private ApexPages.StandardController stdCon;
	private Schema.SObjectType parentType;
	private Account parentAccount;
	private Lead parentLead;
	
	
	public  fxAccountViewCon(ApexPages.StandardController stdController){
		
		stdCon = stdController;
		
		SObject theParent = stdController.getRecord();
		
		parentType = theParent.getSObjectType();
		
		loadFxAccounts(theParent);
		
		sortEmptyFxAccountDown();
	}
	
	private void loadFxAccounts(SObject theParent){
		String query =generateQuerySelectClause();
		
		if(parentType == Account.sObjectType){
			parentAccount = (Account)theParent;
			//records = [select id, name, recordTypeId, recordType.name, Funnel_Stage__c, LastModifiedDate, Traded__c, Last_Trade_Date__c, CRM_Link__c from fxAccount__c where Account__c = :parentAccount.Id];
			
			query = query + 'Id, name from fxAccount__c where Account__c = \'' + parentAccount.Id + '\' order by ' + SubAccountViewCon.SORT_FIELD_NAME_INIT + ' ' + SubAccountViewCon.SORT_ORDER_INIT;
			
		}else{
			parentLead = (Lead)theParent;
			//records = [select id, name, recordTypeId, recordType.name, Funnel_Stage__c, LastModifiedDate, Traded__c, Last_Trade_Date__c, CRM_Link__c from fxAccount__c where Lead__c = :parentLead.Id];
			
			query = query + 'Id, name from fxAccount__c where Lead__c = \'' + parentLead.Id + '\' order by ' + SubAccountViewCon.SORT_FIELD_NAME_INIT + ' ' + SubAccountViewCon.SORT_ORDER_INIT;
			
		}
		
		try{
			System.debug('Query to use: ' + query);
			
			records = Database.query(query);
		}catch(Exception e){
			System.debug(e);
			
			ApexPages.addMessages(e);
		}
	}
	
	private List<Schema.FieldSetMember> getfxAccountFields() {

    	return SObjectType.fxAccount__c.FieldSets.fxAccount_Related_List.getFields();

    }
    
    private String generateQuerySelectClause(){
    	String querySelect = 'SELECT ';

        for(Schema.FieldSetMember f : getfxAccountFields()) {
            querySelect += f.getFieldPath() + ', ';

        }
        
        return querySelect;
    	
    } 
    
    //If the sort field is empty, we should put the record to the end of the list
    private void sortEmptyFxAccountDown(){
    	
    	if(records == null || records.size() == 0){
    		//nothing to sort
    		return;
    	}
    	
    	List<fxAccount__c> results = new List<fxAccount__c>();
    	
    	//copy the records of non-null field
    	for(fxAccount__c fxa : records){
    		Object val = fxa.get(SubAccountViewCon.SORT_FIELD_NAME_INIT);
    		
    		if(val != null){
    			results.add(fxa);
    		}
    	}
    	
    	//copy the records of null field
    	for(fxAccount__c fxa : records){
    		Object val = fxa.get(SubAccountViewCon.SORT_FIELD_NAME_INIT);
    		
    		if(val == null){
    			results.add(fxa);
    		}
    	}
    	
    	records = results;
    }
	
    
}