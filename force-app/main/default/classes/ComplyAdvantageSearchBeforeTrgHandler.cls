public with sharing class ComplyAdvantageSearchBeforeTrgHandler extends ComplyAdvantageSearchTriggerHandler
{
    public ComplyAdvantageSearchBeforeTrgHandler(boolean trgInsert,
                                                 boolean trgUpdate,
                                                 list<Comply_Advantage_Search__c> triggerNew,
                                                 Map<Id, Comply_Advantage_Search__c> triggerOldMap) 
    {
        super(trgInsert,trgUpdate,false, true, triggerNew, triggerOldMap);
    }
	public void onBeforeInsert()
    {

	} 
	public void onBeforeAfterUpdate()
    {

	} 
}