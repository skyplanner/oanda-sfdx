/**
 * Handles each one of the workflow updates for the Case object.
 * Most of these updates will happen on a before-trigger. There is
 * a method for each conditional update and a mapping to call the update
 * dynamically based on the workflow name.
 * @author Fernando Gomez
 */
public class CaseConditionalUpdate extends ConditionalUpdate {
	private Case newCase;
	private Case oldCase;

	/**
	 * @param newCase
	 * @param oldCase 
	 */
	public CaseConditionalUpdate(Case newCase, Case oldCase) {
		super(newCase, oldCase);
		this.newCase = newCase;
		this.oldCase = oldCase;
	}

	/**
	 * Performs the action in the workflow with the specified name
	 * @param workflowName
	 */
	public override void execute(String workflowName) {
		switch on workflowName {
			when 'WF_QI_Case_Owner' {
				execute_QI_Case_owner();
			}
			when else {
				// do nothing as update is not supported
			}
		}
	}

	/**
	 * Performs the action in the workflow:
	 * QI_Case_owner
	 */
	public void execute_QI_Case_owner() {
		/*
		OR(QI_Reviewed__c = null, QI_Reviewed__c = false)
		*/
		if (newCase.QI_Reviewed__c == null || newCase.QI_Reviewed__c == false) {
			// on compaint cases, default to first owner
			if (newCase.Complaint__c == true)
				newCase.QI_Case_Owner__c = newCase.First_Owner__c;
			// defaults to Case Owner
			// if it's not a compaint case
			// and, of course, owner must be a User
			else if (UserUtil.isOwnedByUser(newCase))
				newCase.QI_Case_Owner__c = newCase.OwnerId;
		}
	}
}