/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-31-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoLockTradingController {
    @InvocableMethod 
    public static List<String> lockTrading(List<Id> fxAccounts) {
        if (fxAccounts == null || fxAccounts.isEmpty()) {
            return new List<String>();
        }

        Map<Id, Note> successNoteMap = new Map<Id, Note>();
        Map<Id, Note> failNoteMap = new Map<Id, Note>();

        for (Id fxaId : fxAccounts) {
            successNoteMap.put(fxaId, new Note(
                ParentId = fxaId,
                Body = String.format(Label.Lock_Note_Body_Success,
                    new List<String> {UserUtil.currentUser.Name}),
                Title = System.Label.Lock_Note_Title_Success
            ));
            failNoteMap.put(fxaId, new Note(
                ParentId = fxaId,
                Body = String.format(Label.Lock_Note_Body_Fail,
                    new List<String> {UserUtil.currentUser.Name}),
                Title = System.Label.Lock_Note_Title_Fail
            ));
        }

        CryptoAccountActionsCallout.CryptoAccountStatusRequest request =
            new CryptoAccountActionsCallout.CryptoAccountStatusRequest();
        request.isDisabled = true;

        Crypto_Account_Setting__mdt mdt = CryptoAccountSettingsMdt.getInstance().getMdt('Lock_Action_Info');
        
        CryptoAccountParams params = new CryptoAccountParams();
        params.fxaccountIds = fxAccounts;
        params.action = 0;
        params.cryptoAccountAction = Constants.CRYPTO_ACCOUNT_LOCK_ACTION;
        //Leave Enable_Disable_Endpoint after fully implementation of User API
        params.calloutResourcePathUser = (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) ? 'Enable_Disable_Endpoint' : 'Lock_Endpoint';
        params.calloutResourcePathTas = 'Lock_Endpoint_TAS';
        params.requestBody = JSON.serializePretty(request.body);
        params.successNoteMap = successNoteMap;
        params.failNoteMap = failNoteMap;
        params.addFxAccountDataResult = false;
        params.useTasApi = mdt.TAS_api__c;
        params.useUserApi = mdt.USER_api__c;

        CryptoAccountActionsCallout accLockTradingCallout = new CryptoAccountActionsCallout(
            params
        );

        return new List<String> { accLockTradingCallout.execute() };
    }
}