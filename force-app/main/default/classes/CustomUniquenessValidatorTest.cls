/**
 * Tests: CustomUniquenessValidator
 * @author: Fernando Gomez, SkyPlanner
 * @since 9/29/2021
 */
@isTest 
private class CustomUniquenessValidatorTest {

	/**
	 * public List<SObject> getNotUnique()
	 */
	@isTest 
	private static void getNotUnique() {
		List<Account> accounts;
		CustomUniquenessValidator validator;

		accounts = new List<Account> {
			new Account(
				PersonEmail = '1@email.com'
			),
			new Account(
				PersonEmail = '1@email.com'
			),
			new Account(
				PersonEmail = '2@email.com'
			),
			new Account(
				PersonEmail = '2@email.com'
			),
			new Account(
				PersonEmail = '3@email.com'
			),
			new Account(
				PersonEmail = '4@email.com'
			)
		};

		System.assertEquals(4,
			new CustomUniquenessValidator(accounts, 'PersonEmail')
				.collect()
				.getNotUnique()
				.size());
	}

	/**
	 * public List<SObject> getUnique()
	 */
	@isTest 
	private static void getUnique() {
		List<Account> accounts;
		CustomUniquenessValidator validator;

		accounts = new List<Account> {
			new Account(
				PersonEmail = '0@email.com'
			),
			new Account(
				PersonEmail = '0@email.com'
			),
			new Account(
				PersonEmail = '1@email.com'
			),
			new Account(
				PersonEmail = '2@email.com'
			),
			new Account(
				PersonEmail = '3@email.com'
			),
			new Account(
				PersonEmail = '4@email.com'
			)
		};

		System.assertEquals(4,
			new CustomUniquenessValidator(accounts, 'PersonEmail')
				.collect()
				.getUnique()
				.size());
	}
}