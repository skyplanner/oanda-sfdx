public with sharing class UserFieldsChangedONB extends OutgoingNotificationBus {
    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        Map<String, Object> wrapper = new Map<String, Object>();

        for (String field : OutgoingNotificationBusConstants.userInfoFields.keySet()) {
            if (newRecord.get(field) != oldRecord.get(field)) {
                UserApiUserPatchRequest.setUserFieldToMap(
                        wrapper,
                        false,
                        field,
                        OutgoingNotificationBusConstants.userInfoFields.get(field),
                        newRecord.get(field));
            }
        }

        for (String field : OutgoingNotificationBusConstants.auxUserInfoFields.keySet()) {
            if (newRecord.get(field) != oldRecord.get(field)) {
                UserApiUserPatchRequest.setUserFieldToMap(
                        wrapper,
                        true,
                        field,
                        OutgoingNotificationBusConstants.auxUserInfoFields.get(field),
                        newRecord.get(field));
            }
        }

        if (!wrapper.isEmpty() && ((fxAccount__c) newRecord).fxTrade_User_Id__c == null) {
            addEventsError(newRecord.Id, 'UserFieldsChangedONB - missing fxTrade_User_Id__c');
            return null;
        }

        if (!wrapper.isEmpty()) {
           return JSON.serialize(wrapper);
        }
        return null;
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize(new Map<String, String> {
            'env' => 'live',
            'recordId' =>  ((fxAccount__c) newRecord).Id,
            'tradeUserId' => String.valueOf((Integer)((fxAccount__c) newRecord).fxTrade_User_Id__c)
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME).updateUser(
                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
                body);
//        new UserCallout().updateUser(
//                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
//                body);
    }

    public override String getRecordId() {
        return recordId;
    }
}