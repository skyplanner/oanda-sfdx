/**
 * @File Name          : RelatedMessagingSessionsControllerTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 1:50:50 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/14/2024, 1:50:50 PM   aniubo     Initial Version
**/
@isTest
private class RelatedMessagingSessionsControllerTest {
	@isTest
	private static void testGetCount() {
		// Test data setup
		RelatedMessagingSessionsController.reader = new RelatedMessagingSessionsReaderMock(
			true
		);
		// Actual test
		Test.startTest();
		Integer count = RelatedMessagingSessionsController.getCount(
			UserInfo.getUserId()
		);

		Test.stopTest();
		System.assertEquals(1, count, 'Count should be 1');
		// Asserts
	}

	@isTest
	private static void testGetCountEx() {
		// Test data setup
		RelatedMessagingSessionsController.reader = new RelatedMessagingSessionsReaderMock(
			true
		);
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		ExceptionTestUtil.prepareDummyException();
		try {
			Integer count = RelatedMessagingSessionsController.getCount(
				UserInfo.getUserId()
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();
		System.assertEquals(
			true,
			exceptionThrown,
			'Exception should be thrown'
		);
		// Asserts
	}

	@isTest
	private static void testGetRelatedMessagingSessions() {
		// Test data setup
		RelatedMessagingSessionsController.reader = new RelatedMessagingSessionsReaderMock(
			true
		);
		// Actual test
		Test.startTest();
		List<MessagingSession> sessions = RelatedMessagingSessionsController.getRelatedMessagingSessions(
			UserInfo.getUserId(),
			new List<String>(),
			0,
			0
		);

		Test.stopTest();
		System.assertEquals(true, sessions.isEmpty(), 'Sessions should be empty');
		// Asserts
	}

    @isTest
	private static void testGetRelatedMessagingSessionsEx() {
		// Test data setup
		RelatedMessagingSessionsController.reader = new RelatedMessagingSessionsReaderMock(
			true
		);
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		ExceptionTestUtil.prepareDummyException();
		try {
            List<MessagingSession> sessions = RelatedMessagingSessionsController.getRelatedMessagingSessions(
                UserInfo.getUserId(),
                new List<String>(),
                0,
                0
            );
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();
		System.assertEquals(
			true,
			exceptionThrown,
			'Exception should be thrown'
		);
		// Asserts
	}
}