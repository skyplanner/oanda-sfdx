/**
 * @File Name          : AIUpdateRecordEventHandler_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/20/2021, 1:43:18 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    9/30/2021, 4:48:04 PM   acantero     Initial Version
**/
@IsTest
private without sharing class AIUpdateRecordEventHandler_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createTestAccounts();
    }

    //valid cases, predicted and routed
    @IsTest
    static void test1() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        Set<ID> originalCasesQueueIdSet = 
            CaseRoutingManager.getOriginalCaseQueueIdSet();
        String originalCasesQueueId = new List<ID>(originalCasesQueueIdSet)[0];
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        List<Case> caseList = new List<Case>();
        //IMPORTANT
        //The origin field is not filled to prevent einstein from 
        //taking the case and allowing the test by sending the events directly
        //HVC
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        Case hvcCase1 = new Case(
            AccountId = hvcId,
            Subject = 'HVC Case 1',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            OwnerId = originalCasesQueueId
        );
        caseList.add(hvcCase1);
        //non hvc case
        ID activeCustomerAccountId = accountMap.get(OmnichanelRoutingTestDataFactory.ACTIVE_CUSTOMER_EMAIL);
        Case nonHvcCase = new Case(
            AccountId = activeCustomerAccountId,
            Subject = 'Active Customer Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY
        );
        caseList.add(nonHvcCase);
        //...
        insert caseList;
        //..
        Test.startTest();
        List<AIUpdateRecordEvent> eventList = new List<AIUpdateRecordEvent>();
        eventList.add(
            new AIUpdateRecordEvent(
                RecordId = hvcCase1.Id
            )
        );
        eventList.add(
            new AIUpdateRecordEvent(
                RecordId = nonHvcCase.Id
            )
        );
        // Call method to publish events
        EventBus.publish(eventList);
        Test.stopTest();
        Integer psrCount = [select count() from PendingServiceRouting];
        //At the moment all cases that receive recommendations from Einstein are routed
        //then PendingServiceRouting is created for both cases (hvc and non hvc)
        System.assertEquals(2, psrCount);
    }

    //valid hvc case, no event -> not predicted and no routed
    @IsTest
    static void test2() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        Set<ID> originalCasesQueueIdSet = 
            CaseRoutingManager.getOriginalCaseQueueIdSet();
        String originalCasesQueueId = new List<ID>(originalCasesQueueIdSet)[0];
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        //IMPORTANT
        //The origin field is not filled to prevent einstein from 
        //taking the case
        //...
        //HVC
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        Case hvcCase1 = new Case(
            AccountId = hvcId,
            Subject = 'HVC Case 1',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            OwnerId = originalCasesQueueId
            //Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        Test.startTest();
        insert hvcCase1;
        Test.stopTest();
        Integer psrCount = [select count() from PendingServiceRouting];
        System.assertEquals(0, psrCount);
    }

    //valid hvc case, event fired but trigger disabled -> not predicted and no routed
    @IsTest
    static void test3() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        Set<ID> originalCasesQueueIdSet = 
            CaseRoutingManager.getOriginalCaseQueueIdSet();
        String originalCasesQueueId = new List<ID>(originalCasesQueueIdSet)[0];
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        List<Case> caseList = new List<Case>();
        //IMPORTANT
        //The origin field is not filled to prevent einstein from 
        //taking the case and allowing the test by sending the events directly
        //HVC
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        Case hvcCase1 = new Case(
            AccountId = hvcId,
            Subject = 'HVC Case 1',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            OwnerId = originalCasesQueueId
            //Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        caseList.add(hvcCase1);
        //...
        insert caseList;
        //..
        Test.startTest();
        List<AIUpdateRecordEvent> eventList = new List<AIUpdateRecordEvent>();
        eventList.add(
            new AIUpdateRecordEvent(
                RecordId = hvcCase1.Id
            )
        );
        //disable trigger and then call method to publish events
        AIUpdateRecordEventHandler.enabled = false;
        EventBus.publish(eventList);
        Test.stopTest();
        Integer psrCount = [select count() from PendingServiceRouting];
        System.assertEquals(0, psrCount);
    }
    
}