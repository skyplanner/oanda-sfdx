/**
 * Utill class for acccount roll down process
 *  - fxAccounts
 *  - Opportunities
 */
public without sharing class AccountRollDownUtil {
    
    // Account map
    Map<Id, Account> accMap;

    //******* fxAccount Structures *******

    // fxAccount -> account fields mapping
    @testVisible
    static final Map<String, String> FXACC_ACC_FIELDS_MAP =
        new Map<String, String>{
            'Business_Name__c' => 'Business_Name__c',
            'City__c' => 'PersonMailingCity',
            'Country_Name__c' => 'PersonMailingCountry',
            'Email__c' => 'PersonEmail',
            'First_Name__c' => 'FirstName',
            'Has_Opted_Out_Of_Email__c' => 'PersonHasOptedOutOfEmail',
            'Language_Preference__c' => 'Language_Preference__pc',
            'Last_Name__c' => 'LastName',
            'Middle_Name__c' => 'MiddleName',
            'Phone__c' => 'Phone',
            'Postal_Code__c' => 'PersonMailingPostalCode',
            'State__c' => 'PersonMailingState',
            'Street__c' => 'PersonMailingStreet',
            'Suffix__c' => 'Suffix',
            'Title__c' => 'Salutation'
            // Add more fields here to roll down...
        };
    
    // Accounts ids to roll down to fxAccounts
    Set<Id> accIdsForFxAcc;

    // fxAccount fields
    @testVisible
    static final Set<String> FXACC_FIELDS =
       FXACC_ACC_FIELDS_MAP.keySet();
    
    // Account fields from fxAccount mapping
    static final List<String> ACC_FIELDS_FROM_FXACC =
        FXACC_ACC_FIELDS_MAP.values();

    //******* Opportunity Structures *******

    // opportunity -> account fields mapping
    @testVisible
    static final Map<String, String> OPP_ACC_FIELDS_MAP =
        new Map<String, String>{
            'Account_Phone_Callable__c' => 'Phone'
            // Add more fields here to roll down...
        };
    
    // Accounts ids to roll down to opportunities
    Set<Id> accIdsForOpp;

    // Opportunity fields
    @testVisible
    static final Set<String> OPP_FIELDS =
        OPP_ACC_FIELDS_MAP.keySet();

    // Account fields from opportunity mapping
    static final List<String> ACC_FIELDS_FROM_OPP =
        OPP_ACC_FIELDS_MAP.values();


    /**
     * Constructor
     */
    public AccountRollDownUtil() {
        // Init helper structures
        accMap = new Map<Id, Account>();
        accIdsForFxAcc = new Set<Id>();
        accIdsForOpp = new Set<Id>();
    }

    /**
     * Know if roll down is needed
     *  - for fxAccount
     *  - for opportunity
     */
    public void verify(
        Account acc)
    {
        Boolean needed = false;

        // Know if roll down to fxAccount is needed 
        if(isRollDownNeededForFxAcc(acc)) {
            accIdsForFxAcc.add(acc.Id);
            needed = true;
        }

        // Know if roll down to opportunity is needed 
        if(isRollDownNeededForOpp(acc)) {
            accIdsForOpp.add(acc.Id);
            needed = true;
        }

        // Add account to helper map
        if(needed) {
            accMap.put(acc.Id, acc);
        }
    }

    /**
     * Know if roll down is needed
     *  - for fxAccount
     *  - for opportunity
     */
    public void verify(
        Account acc,
        Account oldAcc)
    {
        Boolean needed = false;

        // Know if roll down to fxAccount is needed 
        if(isRollDownNeededForFxAcc(acc, oldAcc)) {
            accIdsForFxAcc.add(acc.Id);
            needed = true;
        }

        // Know if roll down to opportunity is needed 
        if(isRollDownNeededForOpp(acc, oldAcc)) {
            accIdsForOpp.add(acc.Id);
            needed = true;
        }

        // Add account to helper map
        if(needed) {
            accMap.put(acc.Id, acc);
        }
    }

    /**
     * Roll down process
     *  - fxAccounts
     *  - Opportunities
     */
    public void rollDown() {
        // Roll down fxAccounts
        rollDownFxAccs();
        // Roll down opportunities
        rollDownOpps();
    }

    /**
     *  Roll down fxAccounts
     */
    private void rollDownFxAccs() {
        Account acc;

        if(accIdsForFxAcc.isEmpty()) {
            return;
        }

        System.debug(
            'AccountRollDownUtil-accIdsForFxAcc: ' +
            accIdsForFxAcc);

        List<fxAccount__c> fxAccsToUpd =
            [SELECT Id,
                    Account__c
                FROM fxAccount__c
                WHERE Account__c IN :accIdsForFxAcc
                AND Account__c IN :accMap.keySet()
                AND RecordTypeId = :fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE
                FOR UPDATE];

        if(fxAccsToUpd.isEmpty()) {
            return;
        }

        // fill fxAccounts with values from account
        for(fxAccount__c fxAcc :fxAccsToUpd)
        {
            fillFxAcc(fxAcc);
        }

        System.debug(
            'AccountRollDownUtil-fxAccsToUpd: ' +
            fxAccsToUpd);

        // Update fxAccounts
        update fxAccsToUpd;
    }

     /**
     *  Roll down opportunities
     */
    private void rollDownOpps() {
        Account acc;

        if(accIdsForOpp.isEmpty()) {
            return;
        }

        System.debug(
            'AccountRollDownUtil-accIdsForOpp: ' +
            accIdsForOpp);

        List<Opportunity> oppsToUpd =
            [SELECT Id,
                    AccountId
                FROM Opportunity
                WHERE AccountId IN :accIdsForOpp
                AND AccountId IN :accMap.keySet()
                FOR UPDATE];

        if(oppsToUpd.isEmpty()) {
            return;
        }

        // Retrieve/fill related opportunities
        for(Opportunity opp :oppsToUpd)
        {
            // Fill opp from account
            fillOpp(opp);
        }

        System.debug(
            'AccountRollDownUtil-oppsToUpd: ' +
            oppsToUpd);

        // Update opportunities with
        // values from account
        update oppsToUpd;
    }

    /**
     * Know if roll down to fxAccounts is needed
     */
    private Boolean isRollDownNeededForFxAcc(
        Account acc,
        Account oldAcc)
    {
        // Know if account.fxAccount__c was changed
        Boolean changedFxAccount =
            isPointedfxAccChanged(
                acc,
                oldAcc);

        // Know if some account monitoring field changed
        Boolean monitoringFieldChanged =
            SObjectUtil.isRecordFieldsChanged(
                acc,
                oldAcc,
                ACC_FIELDS_FROM_FXACC);
        
        return
            isRollDownNeededForFxAcc(acc) &&
            !changedFxAccount &&
            monitoringFieldChanged;
    }

    /**
     * Know if roll down is needed for fxAccount
     */
    private Boolean isRollDownNeededForFxAcc(
        Account acc)
    {
        return
            acc != null &&
            acc.fxAccount__c != null &&
            !UserUtil.isIntegrationUser();
    }

    /**
     * Know if roll down is needed for opportunity
     */
    private Boolean isRollDownNeededForOpp(
        Account acc,
        Account oldAcc)
    {
        // Know if account.fxAccount__c was changed
        Boolean changedFxAccount =
            isPointedfxAccChanged( 
                acc,
                oldAcc);

        // Know if some account monitoring field changed
        Boolean monitoringFieldChanged =
            SObjectUtil.isRecordFieldsChanged(
                acc,
                oldAcc,
                ACC_FIELDS_FROM_OPP);
        
        return
            isRollDownNeededForOpp(acc) &&
            !changedFxAccount &&
            monitoringFieldChanged;
    }

    /**
     * Know if roll down is needed for opportunity
     */
    private Boolean isRollDownNeededForOpp(
        Account acc)
    {        
        return
            acc != null &&
            acc.fxAccount__c != null;
    }

    /**
     * Update fxAccounts
     */
    private void fillFxAcc(
        fxAccount__c fxAcc)
    {
        fill(
            fxAcc,
            fxAcc.Account__c,
            FXACC_FIELDS,
            FXACC_ACC_FIELDS_MAP);
    }

    /**
     * Fill the opportunity from account
     */
    private void fillOpp(
        Opportunity opp)
    {
        fill(
            opp,
            opp.AccountId,
            OPP_FIELDS,
            OPP_ACC_FIELDS_MAP);
    }

    /**
     * Fill target object according 
     * the fields mapping
     */
    private void fill(
        SObject obj,
        Id accId,
        Set<String> fields,
        Map<String, String> fieldsMapping)
    {
        String accField;
        // Get account
        Account acc =
            accMap.get(accId);
        
        // Iterate all opportunity fields from mapping
        for(String field :fields) {
            // Get account field name
            accField = fieldsMapping.get(field);
            // Set target field value from account (using fields mapping)
            obj.put(field, acc.get(accField));
        }
    }

    /**
     * Know if account.fxAccount__c was changed
     */
    private Boolean isPointedfxAccChanged(
        Account acc,
        Account oldAcc)
    {
        return
            SObjectUtil.isRecordFieldsChanged(
                acc,
                oldAcc,
                'fxAccount__c');
    }

}