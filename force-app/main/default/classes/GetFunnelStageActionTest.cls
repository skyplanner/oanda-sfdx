/**
 * @File Name          : GetFunnelStageActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 2:26:53 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 2:26:08 PM   aniubo     Initial Version
 **/
@isTest
private class GetFunnelStageActionTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		initManager.storeData();
	}
	@isTest
	private static void testAccountIdListIsNullOrEmpty() {
		// Test data setup
		Boolean isError = false;
		List<String> stages;
		// Actual test
		Test.startTest();
		try {
			stages = GetFunnelStageAction.getFunnelStage(null);
			Assert.areEqual(null, stages.get(0), 'Should return null');

			stages = null;

			stages = GetFunnelStageAction.getFunnelStage(new List<ID>());
			assert.areEqual(null, stages.get(0), 'Should return null');
		} catch (Exception ex) {
			isError = true;
		}
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}

	@isTest
	private static void testGetFunnelStageEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			List<String> stages = GetFunnelStageAction.getFunnelStage(
				new List<String>()
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testGetFunnelStage() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		// Actual test
		Test.startTest();
		List<String> stages = GetFunnelStageAction.getFunnelStage(
			new List<String>{ accountId }
		);
		Test.stopTest();

		// Asserts Constants.FUNNEL_BEFORE_YOU_BEGIN
		Assert.areEqual(
			Constants.FUNNEL_BEFORE_YOU_BEGIN,
			stages.get(0),
			'Should return ' + Constants.FUNNEL_BEFORE_YOU_BEGIN
		);
	}
}