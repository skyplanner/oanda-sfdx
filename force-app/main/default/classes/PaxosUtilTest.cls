/* Name: PaxosUtilTest
 * Description : 
 * Author: Eugene
 * Date : 2022-01-25
 */

@isTest
public with sharing class PaxosUtilTest 
{
    @TestSetup
    static void initData()
    {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxa = TestDataFactory.createFXTradeCryptoAccount(acc, true);
    }

    @isTest
    static void updateCryptoFxaFunnelStageTest()
    {
        Test.startTest();
        fxAccount__c fxa = [SELECT Id, Funnel_Stage__c FROM fxAccount__c LIMIT 1];

        //test updating crypto fxAccount funnel stage on Paxos insert
        Paxos__c px = new Paxos__c(
            fxAccount__c = fxa.Id,
            Paxos_Status__c = Constants.PAXOS_STATUS_PENDING
        );
        insert px;

        fxAccount__c fxaWithPaxos = [SELECT Id, Funnel_Stage__c FROM fxAccount__c LIMIT 1];
        System.assertEquals(Constants.CRYPTO_FUNNEL_PENDING, fxaWithPaxos.Funnel_Stage__c);

        //test updating crypto fxAccount funnel stage on Paxos insert
        px.Paxos_Status__c = Constants.PAXOS_STATUS_APPROVED;
        update px;
        Test.stopTest();

        fxAccount__c fxaWithPaxosApproved = [SELECT Id, Funnel_Stage__c FROM fxAccount__c LIMIT 1];
        System.assertEquals(Constants.FUNNEL_RFF, fxaWithPaxosApproved.Funnel_Stage__c);   
    }
}