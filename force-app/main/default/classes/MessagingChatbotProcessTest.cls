/**
 * @File Name          : MessagingChatbotProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/14/2024, 2:58:30 PM
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class MessagingChatbotProcessTest {
	static Id MessagingSessionId = '0MwDU000000020N0AQ';

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		initManager.setFieldValue(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			fxAccount__c.Authentication_Code__c,
			'811109'
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			fxAccount__c.Authentication_Code_Expires_On__c,
			Datetime.now().addDays(1)
		);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		ServiceTestDataFactory.createLead1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}

	@isTest
	private static void testCheckLinkToAccountOrLead() {
		Boolean throwException = false;
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		MessagingAccountCheckInfo info = process.checkLinkToAccountOrLead(
			'email@server.com'
		);

		Test.stopTest();

		Assert.areEqual(
			false,
			info.isLinkedToAccountOrLead,
			'Must not be linked'
		);
	}

	@isTest
	private static void testCheckLinkToFxAccount() {
		Boolean throwException = false;
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		MessagingAccountCheckInfo info = process.checkLinkToFxAccount(
			'email@server.com'
		);

		Test.stopTest();

		Assert.areEqual(
			false,
			info.isLinkedToAccountOrLead,
			'Must not be linked'
		);
	}

	@isTest
	private static void testCreateLeadFromMsgSession() {
		Boolean throwException = false;
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Id leadId = process.createLeadFromMsgSession();

		Test.stopTest();

		Assert.areEqual(null, leadId, 'Lead must not be created');
	}

	@isTest
	private static void testCreateCaseFromMsgSession() {
		Boolean throwException = false;
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Case createdCase = process.createCaseFromMsgSession();

		Test.stopTest();

		Assert.areEqual(null, createdCase, 'Case must not be created');
	}

	@isTest
	private static void testUpdateMessagingCaseOrigin() {
		Boolean throwException = false;
		Test.startTest();
		try {
			MessagingChatbotProcess process = new MessagingChatbotProcess(
				MessagingSessionId
			);
			process.updateMessagingCaseOrigin();
		} catch (Exception ex) {
			throwException = true;
		}
		Test.stopTest();

		Assert.areEqual(false, throwException, 'Exception must not be thrown');
	}

	@isTest
	private static void testSendAuthenticationCode() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Test.startTest();
		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Boolean wasSuccessful = process.sendAuthenticationCode(fxAccountId);

		Test.stopTest();

		Assert.areEqual(false, wasSuccessful, 'Session not exists');
	}

	@isTest
	private static void testSendAuthenticationCode1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		MessagingSession session = new MessagingSession(
			CaseId = caseId,
			Language_Code__c = 'es'
		);
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Boolean wasSuccessful = process.sendAuthenticationCode(
			fxAccountId,
			session
		);

		Test.stopTest();

		Assert.areEqual(
			true,
			wasSuccessful,
			'sendAuthenticationCode must be successful'
		);
	}

	@isTest
	private static void testVerifyAuthenticationCode() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Boolean wasSuccessful = process.verifyAuthenticationCode(
			fxAccountId,
			'811109'
		);

		Test.stopTest();

		Assert.areEqual(false, wasSuccessful, 'Session not exists');
	}

	@isTest
	private static void testVerifyAuthenticationCode2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		MessagingSession session = new MessagingSession(
			CaseId = caseId,
			Language_Code__c = 'es'
		);
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Boolean wasSuccessful = process.verifyAuthenticationCode(
			fxAccountId,
			'811109',
			session
		);

		Test.stopTest();

		Assert.areEqual(true, wasSuccessful, 'Authentication code is valid');
	}

	@isTest
	private static void testRegisterLeadCreation() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Id leadId = initManager.getObjectId(ServiceTestDataKeys.LEAD_1, true);
		MessagingSession session = new MessagingSession(
			Language_Code__c = 'es'
		);
		Test.startTest();

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		Lead newLead = new Lead(Id = leadId);
		process.registerLeadCreation(session, newLead);

		Test.stopTest();

		Assert.areEqual(
			newLead.Id,
			session.LeadId,
			'session.LeadId must be set'
		);
		Assert.areEqual(
			true,
			session.New_Lead__c,
			'New_Lead__c must be set to true'
		);
	}

	@isTest
	private static void testUpdateMsgSessionLookup() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseID = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		MessagingSession session = new MessagingSession(
			Language_Code__c = 'es'
		);
		Test.startTest();
		Case newCase = new Case(Id = caseID);

		MessagingChatbotProcess process = new MessagingChatbotProcess(
			MessagingSessionId
		);
		process.updateMsgSessionLookup(session, newCase, 'CaseId');

		Test.stopTest();

		Assert.areEqual(caseID, session.CaseId, 'session.CaseId must be set');
	}

	@isTest
	private static void testCheckMsgAgentAvailability() {
		// Test data setup

		// Actual test
		Test.startTest();
		Boolean isAgentAvailable = MessagingChatbotProcess.checkMsgAgentAvailability(
			'es'
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(false, isAgentAvailable, 'Agent must not be available');
	}

	@isTest
	private static void testIsLangAvailableForChatbot() {
		// Test data setup

		// Actual test
		Test.startTest();

		Boolean isAgentAvailable = MessagingChatbotProcess.isLangAvailableForChatbot(
			'es'
		);
		Test.stopTest();
		Service_Language__mdt languageRec = ServiceLanguagesManager.getLanguageRecord(
			'es'
		);
		// Asserts
		Assert.areEqual(
			(languageRec?.Available_for_Chatbot__c == true),
			isAgentAvailable,
			'Language must be available for chatbot. If LanguageRec?.Available_for_Chatbot__c == true'
		);
	}

	@isTest
	private static void testGetLanguageName() {
		// Test data setup

		// Actual test
		Test.startTest();
		String language = MessagingChatbotProcess.getLanguageName('es');
		Test.stopTest();
		String languageCustomLabel = ServiceLanguagesManager.getLangCustomLabel(
			'es'
		);
		// Asserts
		Assert.areEqual(
			String.isNotBlank(languageCustomLabel),
			language != 'UNKNOWN',
			'Language must be different from UNKNOWN if languageCustomLabel is not blank'
		);
	}

	/**
	 * test 1 : countryCode is blank
	 * test 2 : valid countryCode (countryCode = US)
	 */
	@IsTest
	static void setRegionFromCountryCode() {
		Test.startTest();
		MessagingChatbotProcess instance = new MessagingChatbotProcess(null);
		// test 1
		MessagingChatbotProcess.MessagingCountryInfo result1 = 
			instance.setRegionFromCountryCode(null);
		// test 2
		MessagingChatbotProcess.MessagingCountryInfo result2 = 
			instance.setRegionFromCountryCode(
				'US', // countryCode
				new MessagingSession() // session
			);
		Test.stopTest();
		
		Assert.isNull(result1, 'Invalid result');
		Assert.isNotNull(result2, 'Invalid result');
	}

	/**
	 * test 1 : session is null (messagingSessionId = null)
	 * test 2 : languageCode is blank
	 * test 3 : session.CaseId = null
	 * test 4 : session.CaseId <> null
	 */
	@IsTest
	static void setLanguageAndInquiryNature() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		String messagingSessionId = null;
		String endUserId = null;
		String languageCode = OmnichanelConst.SPANISH_LANG_CODE;
		String inquiryNature = OmnichanelConst.INQUIRY_NAT_LOGIN;
		MessagingSession session = new MessagingSession();
		
		Test.startTest();
		MessagingChatbotProcess instance = 
			new MessagingChatbotProcess(messagingSessionId);
		// test 1 => return false
		Boolean result1 = instance.setLanguageAndInquiryNature(
			endUserId, // endUserId
			languageCode, // languageCode
			inquiryNature // inquiryNature
		);
		// test 2 => return false
		Boolean result2 = instance.setLanguageAndInquiryNature(
			endUserId, // endUserId
			null, // languageCode
			inquiryNature, // inquiryNature
			session // session
		);
		// test 3 => return true, Case_Language_Code__c is updated
		session.CaseId = null;
		session.Case_Language_Code__c = OmnichanelConst.ENGLISH_LANG_CODE;
		Boolean result3 = instance.setLanguageAndInquiryNature(
			endUserId, // endUserId
			languageCode, // languageCode
			inquiryNature, // inquiryNature
			session // session
		);
		String caseLanguageCode3 = session.Case_Language_Code__c;
		// test 4 => return true, Case_Language_Code__c maintains its value
		session.CaseId = case1Id;
		session.Case_Language_Code__c = OmnichanelConst.ENGLISH_LANG_CODE;
		Boolean result4 = instance.setLanguageAndInquiryNature(
			endUserId, // endUserId
			languageCode, // languageCode
			inquiryNature, // inquiryNature
			session // session
		);
		String caseLanguageCode4 = session.Case_Language_Code__c;
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
		Assert.isTrue(result3, 'Invalid result');
		Assert.isTrue(result4, 'Invalid result');
		Assert.areEqual(
			OmnichanelConst.SPANISH_LANG_CODE, 
			caseLanguageCode3, 
			'Invalid result'
		);
		Assert.areEqual(
			OmnichanelConst.ENGLISH_LANG_CODE, 
			caseLanguageCode4, 
			'Invalid result'
		);
	}

	/**
	 * test 1 : session is null (messagingSessionId = null)
	 * test 2 : languageCode is blank
	 * test 3 : session.CaseId <> null
	 * test 4 : session.CaseId = null
	 */
	@IsTest
	static void updateMessagingCaseLanguage() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		String messagingSessionId = null;
		String languageCode = OmnichanelConst.SPANISH_LANG_CODE;
		MessagingSession session = new MessagingSession();
		
		Test.startTest();
		MessagingChatbotProcess instance = 
			new MessagingChatbotProcess(messagingSessionId);
		// test 1 => return false
		Boolean result1 = instance.updateMessagingCaseLanguage(languageCode);
		// test 2 => return false
		session.CaseId = null;
		Boolean result2 = instance.updateMessagingCaseLanguage(
			null, // languageCode
			session // session
		);
		// test 3 => return false
		session.CaseId = case1Id;
		session.Case_Language_Code__c = OmnichanelConst.ENGLISH_LANG_CODE;
		Boolean result3 = instance.updateMessagingCaseLanguage(
			languageCode, // languageCode
			session // session
		);
		// test 4 => return true, Case_Language_Code__c is updated
		session.CaseId = null;
		session.Case_Language_Code__c = OmnichanelConst.ENGLISH_LANG_CODE;
		Boolean result4 = instance.updateMessagingCaseLanguage(
			languageCode, // languageCode
			session // session
		);
		String caseLanguageCode4 = session.Case_Language_Code__c;
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
		Assert.isTrue(result4, 'Invalid result');
		Assert.areEqual(
			OmnichanelConst.SPANISH_LANG_CODE, 
			caseLanguageCode4, 
			'Invalid result'
		);
	}

}