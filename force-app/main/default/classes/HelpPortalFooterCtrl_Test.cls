/**
 * @File Name          : HelpPortalFooterCtrl_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/6/2020, 10:59:37 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   dmorales     Initial Version
**/
@isTest
private class HelpPortalFooterCtrl_Test {
    @isTest
    static void getInfoOC() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OC';
      
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;
         String urlImage = c.urlImage;
         Map<String, String> regionList = c.regionList;
         
         System.assert(c != null);
         System.assert(productLink == 'https://www.oanda.com/us-en/trading/');
         System.assert(platformLink == 'https://www.oanda.com/us-en/trading/platforms/');
         System.assert(toolsLink == 'https://www.oanda.com/us-en/trading/tools/');
         System.assert(urlImage == '');
         System.assert(regionList.get('OANDA') == 'OANDA TEST');
    }
    @isTest
    static void getInfoOEL() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OEL';
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;

         System.assert(c != null);
         System.assert(productLink == 'https://www.oanda.com/uk-en/trading/');
         System.assert(platformLink == 'https://www.oanda.com/uk-en/trading/platforms/');
         System.assert(toolsLink == 'https://www.oanda.com/uk-en/trading/tools/');
    }
    @isTest
    static void getInfoOME() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OME';
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;

         System.assert(c != null);
         System.assert(productLink == 'https://www1.oanda.com/forex-trading/markets/');
         System.assert(platformLink == 'https://www1.oanda.com/forex-trading/platform/');
         System.assert(toolsLink == 'https://www1.oanda.com/forex-trading/tools/');
    }
    @isTest
    static void getInfoOCAN() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OCAN';
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;

         System.assert(c != null);
         System.assert(productLink == 'https://www1.oanda.com/forex-trading/markets/');
         System.assert(platformLink == 'https://www1.oanda.com/forex-trading/platform/');
         System.assert(toolsLink == 'https://www1.oanda.com/forex-trading/tools/');
    }
    @isTest
    static void getInfoOAP() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OAP';
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;

         System.assert(c != null);
         System.assert(productLink == 'https://www1.oanda.com/forex-trading/markets/');
         System.assert(platformLink == 'https://www1.oanda.com/forex-trading/platform/');
         System.assert(toolsLink == 'https://www1.oanda.com/forex-trading/tools/');
    }
    @isTest
    static void getInfoOAU() {
         HelpPortalFooterCtrl c = new HelpPortalFooterCtrl();
         c.regionCode = 'OAU';
         String productLink = c.productLink;
         String platformLink = c.platformLink;
         String toolsLink = c.toolsLink;

         System.assert(c != null);
         System.assert(productLink == 'https://www1.oanda.com/forex-trading/markets/');
         System.assert(platformLink == 'https://www1.oanda.com/forex-trading/platform/');
         System.assert(toolsLink == 'https://www1.oanda.com/forex-trading/tools/');
    }   
}