/**
 * @File Name          : GeneralAction.cls
 * @Description        : 
 * General platform event without logic.
 * It will call some callable class to do the process 
 * passing the required data to do it.
 * @Author             : skyp
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/5/2024, 6:34:33 PM
**/

public without sharing class GeneralAction {

    /**
 * General platform event without logic.
 * It will call some callable class to do the process 
 * passing the required data to do it.
 */
    
    // Class to call to process the data
    private String className { get; set; }
    // Method to call to process the data
    private String methodName { get; set; }
    // Data to process
    private String data { get; set; }

    public GeneralAction(
        String className,
        String methodName,    
        Object data)
    {
        this.className = className;
        this.methodName = methodName;
        this.data =
            Utilities.serialize(
                data, false);
    }

    public GeneralAction(
        String className,
        String methodName,    
        String data)
    {
        this.className = className;
        this.methodName = methodName;
        this.data = data;
    }

    /**
     * Publish the event
     */
    public void publish() {
        EventBus.publish(getEvent());
    }

    /**
     * Call methods in the 
     * proccessing class
     */
    public static void process(
        List<GeneralAction__e> gas)
    {
        try {
            for(GeneralAction__e ga :gas) {
                // Create new class instance
                Callable cls = 
                    (Callable)Type.forName(ga.ClassName__c).newInstance();
                
                // Get data
                Map<String, Object> data =
                    (Map<String, Object>)JSON.deserializeUntyped(
                        ga.Data__c);

                // Call method to process the data
                cls.call(
                    ga.MethodName__c, 
                    data); 
            }
        } catch (Exception ex) {
            // Log the exception...
        }
    }

    /**
     * Get new event obj
     */
    private GeneralAction__e getEvent() {
        return
            new GeneralAction__e(
                ClassName__c = className,
                MethodName__c = methodName,
                Data__c = data);
    }

}