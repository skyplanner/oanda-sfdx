/**
 * @File Name          : OnNonHVCCaseStatusChangesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/3/2021, 7:21:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/25/2021, 2:42:49 PM   acantero     Initial Version
**/
public inherited sharing class OnNonHVCCaseStatusChangesCmd {

    final NonHVCCaseRegister register = new NonHVCCaseRegister();

    public void checkRecord(
        Non_HVC_Case__c rec, 
        Non_HVC_Case__c oldRec
    ) {
        if (rec.Status__c != oldRec.Status__c) {
            if (rec.Status__c == OmnichanelConst.NON_HVC_CASE_STATUS_PARKED) {
                register.onCaseParked(rec);
                //...
            } 
            // else if (
            //     (rec.Status__c == OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED) &&
            //     (oldRec.Status__c == OmnichanelConst.NON_HVC_CASE_STATUS_PARKED)
            // ) {
            //     register.onCaseReleased(rec);
            // }
        } 
    }

    public void execute() {
        register.process();
    }

}