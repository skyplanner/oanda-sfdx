/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class fxAccountAfterTriggerHandler extends fxAccountTriggerHandler
{
    @TestVisible
    private Set<Id> fxAccIdsForRecalculaition = new Set<Id>();

    @TestVisible
    private DivisionSettingsUtil dsu;


    @TestVisible
    private List<List<Id>> fxAccIdsToSetCloseOnly = new List<List<Id>>();
    @TestVisible
    private List<List<Id>>  fxAccIdsToUnsetCloseOnly = new List<List<Id>> ();

    // public static Set<Id> fxAccIdsToFireCAS = new Set<Id> ();
    public Map<Id, fxAccount__c> oldFxMapforCAS = new Map<Id, fxAccount__c>();

    public static Boolean CASbatchfired = false;

    public static  set<string> craCheckDivisions = new Set<String>{
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD
    };
    public static  set<string> craCheckFunnelStages = new set<String>{
        Constants.FUNNEL_RFF, 
        Constants.FUNNEL_FUNDED,
        Constants.FUNNEL_TRADED
    };
    public static  set<string> obCaseApplicationCompletedkDivisions = new Set<String>{
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD
    };
    public static  Map<string, set<string>> craRiskChangesMap = new Map<string, set<string>>{
        'Low' => new Set<String>{'Medium','High','Prohibited'},
        'Medium' => new Set<String>{'High','Prohibited'},
        'Incomplete' => new set<String>{'Medium','High'},
        'Incomplete Data' => new set<String>{'Medium','High'}
    };
    public static set<string> ckaCheckDivisions = new Set<String>{
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD
    };
    public static set<string> myInfoCheckDivisions = new Set<String>{
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD
    };
    public static set<string> ckaCheckFunnelStages = new set<String>{
        Constants.FUNNEL_RFF, 
        Constants.FUNNEL_FUNDED,
        Constants.FUNNEL_TRADED
    }; 

    private static final String IHS_CASE_SUBJECT_SUBTYPE = 'W8/W9 discrepancies vs. SF records';
    private static final String CASE_CORPORATE_REVIEW_SUBJECT = 'Corporate Tax Declaration Review';

    private static List<External_Notification_Event__e> externalNotificationEvents;

    private final static Date DUMMY_BIRTHDATE = Date.newInstance(1900, 1, 1);

    //Created to save test coverage for BatchSharesByOneIdTriggerRecalc SR-3117
    public static Boolean isTestRun = false;

    public fxAccountAfterTriggerHandler(boolean trgInsert,
                                        boolean trgUpdate,
                                        list<fxAccount__c> triggerNew,
                                        Map<Id, fxAccount__c> triggerOldMap) 
    {
        super(trgInsert,trgUpdate,false,true,triggerNew,triggerOldMap);

        if(isUpdate)
        {
            //retrieve records related to fxaccount like Account, lead, opportunity, Case...
            populateRelatedAccounts();

            populateRelatedOpportunities();

            populateRelatedCases();

            populateRelatedEIDs();

            
        }

    }

    public void onAfterInsert() {
        // Update lead record type based on newly inserted fxAccount
        System.debug('TRG Insert fxAccount_After: fxAccountUtil.setLeadRecordType');
        fxAccountUtil.setLeadRecordType(trgNew);

        // SP-9218
        fxAccountUtil.processOnboardingCaseCreation(trgNew);

        // SP-12199 updating Accounts
        fxAccountUtil.updateAccounts(trgNew, trgOldMap);

        fxAccountRollupQueueable.doFullRollup(trgNew, trgOldMap, true);
        
        fxAccountUtil.linkFromOpp(trigger.new);
        for(fxAccount__c fxAccount : trgNew) {
            //SP-13397 create case when W8/W9 equal to interviewManualReview
            createCorporateTaxDeclarationReviewCase(fxAccount);
            checkOutgoingNotificationOnAfterInsert(fxAccount);
        }

        publishOutgoingNotificationEvents();
    }

    public void onAfterUpdate() {
        if (trigger.isUpdate && CurrentDeskSync.isCurrentDeskEnabled()) {
            CurrentDeskSync.syncCustomerRecords(trgNew, trgOldMap.values());
        }
        fxAccountRollupQueueable.doFullRollup(trgNew, trgOldMap, false);
        
        fxAccountUtil.linkFromOpp(trgNew);

        fxAccount_Trigger_Utility.mifidValidationProcess(new Map<Id, fxAccount__c>(trgNew), trgOldMap);
        /**
         * SP-7630
         * For those accounts that were jus closed or locked,
         * we need to turn all searches monitoring off,
         * among other actions yet to define.
         * Doing as async job if conditions met and job limits are fine.
         */
        fxAccountUtil.processNewlyClosedAccounts(trgNew, trgOldMap.values());

        // SP-12199 updating Accounts
        fxAccountUtil.updateAccounts(trgNew, trgOldMap);
        //SP-12928 enably CA search for bulk updates
        if(Settings__c.getValues('Default') != null 
        && Settings__c.getValues('Default').Comply_Advantage_Enabled__c
        && !CASbatchfired
        //&& trigger.new.size() == 1
        ) {
            for(fxAccount__c newFxA : trgNew) {
                if (fxAccountUtil.isLiveAccount(newFxA)) {
                    Boolean addFxIdForCASReopen = false;
                    fxAccount__c oldFxA = trgOldMap.get(newFxA.Id);
                    // fxAccount__c newOldFxAcc = new fxAccount__c(Id = newFxA.Id);
                    Set<string> changedFields = ComplyAdvantageHelper.compareRecords(ComplyAdvantageHelper.fxAccountFields, newFxA, oldFxA);
                    if (changedFields.size() > 0) {
                        fxAccIdsToFireCAS.add(newFxA.Id);
                        oldFxMapforCAS.put(oldFxA.Id, oldFxA);
                    }
                }
            }
        }
        //iterate records once..have all processing under this.
        //initialize divisiion setting
        dsu = DivisionSettingsUtil.getInstance();
        for(fxAccount__c fxAccount : trgNew) {

            //check Customer Risk Assesment
            validateCustomerRiskAssessmentChange(fxAccount);
                
            //check OAP CKA Fail
            validateCustomerKnowledgeAssesment(fxAccount);
        
            //check if user Has Exceeded 10M Trade Vol.if so set core date
            checkCoreFxAccount(fxAccount);
            
            //check if risk tolerance changed and notify other systems
            checkRiskTolerance(fxAccount);

            //check if net worth update timestamp changed and notify other systems
            checkNWUpdateDateTimeChanges(fxAccount);

            //Unassign Opportunity for Rejected applications
            UnassignOppsForRejectedApps(fxAccount);

            //create Onboarding cases for customers from OAP, OAP CFD if the application funnel stage is 'Application Completed'
            createOnboardingCase(fxAccount);

            //create EID result for customers from OAP, OAP CFD if any of the MyInfo checkboxes are checked
            createEIDResult(fxAccount);

            //create W8/W9 discrepancies cases for customers who have discrepancies between IHS data and SF
            createIHSDiscrepanciesCase(fxAccount);

            //SP-13397 create case when W8/W9 equal to interviewManualReview
            createCorporateTaxDeclarationReviewCase(fxAccount);

            setAccountsToCloseOnly(fxAccount);

            //SP-13417 publish event when Funnel Stage changes for Optimove integration
            checkFunnelStageChangeAndNotify(fxAccount);

            checkOutgoingNotificationOnAfterUpdate(fxAccount, trgOldMap.get(fxAccount.Id));
        }
        for(List<Id> idList: fxAccIdsToSetCloseOnly) {
            ConditionalApprovalForSharedServices.setClosedAttr(idList, true);
        }

        for(List<Id> idList: fxAccIdsToUnsetCloseOnly) {
            ConditionalApprovalForSharedServices.setClosedAttr(idList, false);
        }
        triggerSharesRecalcBatch();
        triggerCASValidateBatcg();
        publishOutgoingNotificationEvents();
    }

    private void checkFunnelStageChangeAndNotify(fxAccount__c newFxAccount) {
        fxAccount__c oldFxac = trgOldMap.get(newFxAccount.Id);
        if(newFxAccount.Funnel_Stage__c != oldFxac.Funnel_Stage__c || newFxAccount.US_Shares_Trading_Enabled__c != oldFxac.US_Shares_Trading_Enabled__c) {
            String origin = newFxAccount.Funnel_Stage__c != oldFxac.Funnel_Stage__c ? ExternalNotificationEventService.ORIGIN_FXACCOUNT_FUNNELSTAGE_CHANGE : ExternalNotificationEventService.ORIGIN_FXACCOUNT_USSHARESTRADINGENABLED_CHANGE;
            fxaTrgDataHandler.addExternalNotificationEvent(ExternalNotificationEventService.createEvent(origin, newFxAccount));
        }
    }

    public void checkOutgoingNotificationOnAfterUpdate(fxAccount__c newFxAccount, fxAccount__c oldFxAccount) {
         if (Test.isRunningTest() ||
                (Util.isSentChangeNotificationAvailable(
                        Constants.FX_ACCOUNT_INTRODUCING_BROKER_CM,
                        newFxAccount.Division_Name__c,
                        newFxAccount.Email__c,
                        null
                ))) {
             outgoingNotification.addEventRecord(IntroducingBrokerChangedONB.class, newFxAccount, oldFxAccount);
         }

         Boolean isLiveAccount = fxAccountUtil.isLiveAccount(newFxAccount);
         if ((isLiveAccount && Test.isRunningTest())
                 || (!Test.isRunningTest()
                 && isLiveAccount
                 && Util.isSentChangeNotificationAvailable(Constants.FX_ACCOUNT_STREAM_UPDATES_CM, newFxAccount.Division_Name__c, newFxAccount.Email__c, newFxAccount.LastModifiedById))) {
             outgoingNotification.addEventRecord(UserFieldsChangedONB.class, newFxAccount, oldFxAccount);
             outgoingNotification.addEventRecord(ContactInfoFieldsChangedONB.class, newFxAccount, oldFxAccount);
             outgoingNotification.addEventRecord(PhoneInfoFieldsChangedONB.class, newFxAccount, oldFxAccount);
         }

         if (Test.isRunningTest() || 
             Util.isSentChangeNotificationAvailable(
                 Constants.FX_ACCOUNT_CLIENT_STATUS_CM,
                 newFxAccount.Division_Name__c,
                 newFxAccount.Email__c,
                 newFxAccount.LastModifiedById)
         ) {
             outgoingNotification.addEventRecord(FxAccountClientStatusONB.class, newFxAccount, oldFxAccount);
         }

        if (callEndpointForSharesRecalculation(newFxAccount, oldFxAccount)) {
            if ((Test.isRunningTest() && isTestRun)
                    || (!Test.isRunningTest()
                        && Util.isSentChangeNotificationAvailable(
                            Constants.FX_ACCOUNT_GROUP_RECALCULATE_CM,
                            newFxAccount.Division_Name__c,
                            newFxAccount.Email__c,
                            newFxAccount.LastModifiedById))) {
                outgoingNotification.addEventRecord(GroupRecalculateTasONB.class, newFxAccount, oldFxAccount);
                outgoingNotification.addEventRecord(GroupRecalculateUserONB.class, newFxAccount, oldFxAccount);
            } else {
                fxAccIdsForRecalculaition.add(newFxAccount.Id);
            }
        }

    }

    public void checkOutgoingNotificationOnAfterInsert(fxAccount__c newFxAccount) {
        if (Test.isRunningTest() ||
                (Util.isSentChangeNotificationAvailable(
                        Constants.FX_ACCOUNT_INTRODUCING_BROKER_CM,
                        newFxAccount.Division_Name__c,
                        newFxAccount.Email__c,
                        null
                ))) {
            outgoingNotification.addEventRecord(IntroducingBrokerChangedONB.class, newFxAccount, null);
        }
    }

     // <-- CRA Change  -->
     public void validateCustomerRiskAssessmentChange(fxAccount__c newFxAccount)
     {  
         string cra_new = newFxAccount.Customer_Risk_Assessment_Text__c;    
         string cra_old = trgOldMap.get(newFxAccount.Id).Customer_Risk_Assessment_Text__c;
          
         if(cra_old != cra_new &&
            fxAccountUtil.isLiveAccount(newFxAccount) &&
            craCheckDivisions.contains(newFxAccount.Division_Name__c) &&
            craCheckFunnelStages.contains(newFxAccount.Funnel_Stage__c))
         {       
             set<string> craCriticalChangeList = craRiskChangesMap.get(cra_old);
             if(craCriticalChangeList != null && craCriticalChangeList.contains(cra_new))
             { 
                createCustomerRiskAssessmentCase(newFxAccount);
             }
         }
     }
     private void createCustomerRiskAssessmentCase(fxAccount__c fxa)
     {
        Case c = new Case(
                Subject = 'CRA Change',
                Type = 'PIU',
                Live_or_Practice__c = 'Live',
                Origin = 'Internal',
                Inquiry_Nature__c = 'Account',
                OwnerId = UserUtil.getQueueId('Ad Hoc (OAP/OAU) Cases'),
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Support').getRecordTypeId(),
                fxAccount__c = fxa.Id,
                Lead__c = fxa.Lead__c,
                AccountId = fxa.Account__c,
                ContactId = fxa.Contact__c
        );
        fxaTrgDataHandler.insertRecord(c);
     }
     //  <--- End of CRA Related Changes -->
 
     // validateCustomerKnowledgeAssesment
     public void validateCustomerKnowledgeAssesment(fxAccount__c newFxAccount)
     {    
         fxAccount__c oldFxac = trgOldMap.get(newFxAccount.Id);
 
         boolean singaproreFlagCheck = (newFxAccount.Singapore_Financial_Education__c != oldFxac.Singapore_Financial_Education__c ||
                                         newFxAccount.Singapore_Professional_Qualification__c != oldFxac.Singapore_Professional_Qualification__c ||
                                             newFxAccount.Singapore_Financial_Work_Experience__c !=oldFxac.Singapore_Financial_Work_Experience__c);
         
         boolean funnelStageCheck = (ckaCheckFunnelStages.contains(newFxAccount.Funnel_Stage__c) || 
                                         (newFxAccount.Funnel_Stage__c == FunnelStatus.FUNNEL_STAGE_NAME_FUNDING_PROHIBITED &&
                                             newFxAccount.First_Trade_Date_New__c != null));
 		
        if(fxAccountUtil.isLiveAccount(newFxAccount) &&
           newFxAccount.OAP_CKA_Result__c == 'Fail' &&
           singaproreFlagCheck &&  funnelStageCheck &&   
           ckaCheckDivisions.contains(newFxAccount.Division_Name__c) &&
           newFxAccount.Country__c != 'Singapore')
        {          
           createCustomerKnowledgeAssesmentFailCase(newFxAccount);
        }
     }
     private void createCustomerKnowledgeAssesmentFailCase(fxAccount__c fxa)
     {
        Case c = new Case(
            Subject = 'Non-resident PIU fail',
            Live_or_Practice__c = 'Live',
            Origin = 'Internal',
            OwnerId = oapEscalationQueueId,
            RecordTypeId = supportCaseRecordTypeId,
            fxAccount__c = fxa.Id,
            AccountId = fxa.Account__c,
            ContactId = fxa.Contact__c
        );
        fxaTrgDataHandler.insertRecord(c);
     }
 
     // check if a customer's trade volume exceeds 10M, then mark account as core.  
     public void checkCoreFxAccount(fxAccount__c newFxAccount)
     {       
        if(newFxAccount.Id != null && 
           newFxAccount.Account__c != null && 
           newFxAccount.Trade_Volume_USD_MTD__c != null && 
           newFxAccount.Trade_Volume_USD_MTD__c >= 10000000)
        {
            if(newFxAccount.Trade_Volume_USD_MTD__c >= 10000000){
                Account ac = fxAccIdAccountMap.get(newFxAccount.Id);
                if(ac != null && ac.Date_Customer_Became_Core__c  == null)
                {
                    Account coreAccount = (Account) fxaTrgDataHandler.getRecord(ac.Id);
                    coreAccount.Date_Customer_Became_Core__c = Date.today();
                    fxaTrgDataHandler.updateRecord(coreAccount);
                }    
            }
            // SP-12067 Set Date Customer Became VIP 
            if(newFxAccount.Trade_Volume_USD_MTD__c >= 200000000){
                Account ac = fxAccIdAccountMap.get(newFxAccount.Id);
                if(ac != null && ac.Date_Customer_Became_VIP__c  == null)
                {
                    Account coreAccount = (Account) fxaTrgDataHandler.getRecord(ac.Id);
                    coreAccount.Date_Customer_Became_VIP__c = Date.today();
                    fxaTrgDataHandler.updateRecord(coreAccount);
                }    
            }              
        }
     }
 
     public void checkRiskTolerance(fxAccount__c newFxAccount)
     {
         fxAccount__c oldFxac = trgOldMap.get(newFxAccount.Id);
        
         if(newFxAccount.Risk_Tolerance_CAD__c != oldFxac.Risk_Tolerance_CAD__c &&
            fxAccountUtil.isLiveAccount(newFxAccount) &&
            currentUserProfileId == registraionProfileID)
         {
            publishRiskToleranceChanges(newFxAccount);
         }
     }
     public void publishRiskToleranceChanges(fxAccount__c fxa)
     {
        string riskToleranceCad = String.valueOf(fxa.Risk_Tolerance_CAD__c);
        string fxTradeUserId = String.valueOf(fxa.fxTrade_User_Id__c);

        Action__e platformEventAction = new Action__e(fxTrade_User_ID__c= fxTradeUserId, 
                                                       Type__c= 'object',
                                                       Parameter_1__c= riskToleranceCad,
                                                       Object_Name__c = 'fxAccount__c',
                                                       Object_Field_Name__c = 'Risk_Tolerance_CAD__c');
        fxaTrgDataHandler.addActionPlatformEvent(platformEventAction);
     }

    public void checkNWUpdateDateTimeChanges(fxAccount__c newFxAccount)
    {
        fxAccount__c oldFxac = trgOldMap.get(newFxAccount.Id);
        Map<String, Datetime> fieldNameValueMap = new Map<String, Datetime>();

        if(!UserUtil.getIntegrationProfileIDs().contains(UserInfo.getProfileId()))
        {
            if(newFxAccount.Net_Worth_Update_DateTime__c != oldFxac.Net_Worth_Update_DateTime__c &&
            fxAccountUtil.isLiveAccount(newFxAccount))
            {
                fieldNameValueMap.put('Net_Worth_Update_DateTime__c', newFxAccount.Net_Worth_Update_DateTime__c);
            }

            if(newFxAccount.Liquid_Net_Worth_Update_DateTime__c != oldFxac.Liquid_Net_Worth_Update_DateTime__c &&
            fxAccountUtil.isLiveAccount(newFxAccount))
            {
                fieldNameValueMap.put('Liquid_Net_Worth_Update_DateTime__c', newFxAccount.Liquid_Net_Worth_Update_DateTime__c);
            }

            if(newFxAccount.Savings_And_Investments_Update_DateTime__c != oldFxac.Savings_And_Investments_Update_DateTime__c &&
            fxAccountUtil.isLiveAccount(newFxAccount))
            {
                fieldNameValueMap.put('Savings_And_Investments_Update_DateTime__c', newFxAccount.Savings_And_Investments_Update_DateTime__c);
            }

            if(newFxAccount.Risk_Tolerance_Update_DateTime__c != oldFxac.Risk_Tolerance_Update_DateTime__c &&
            fxAccountUtil.isLiveAccount(newFxAccount))
            {
                fieldNameValueMap.put('Risk_Tolerance_Update_DateTime__c', newFxAccount.Risk_Tolerance_Update_DateTime__c);
            }
        }

        if(!fieldNameValueMap.isEmpty())
        {
            publishNWUpdateDateTimeChanges(newFxAccount, fieldNameValueMap);
        }
    }
    public void publishNWUpdateDateTimeChanges(fxAccount__c fxa, map<String, Datetime> params)
    {
       String fxTradeUserId = String.valueOf(fxa.fxTrade_User_Id__c);
       List<Action__e> platformEventActions = new List<Action__e>();

       for(String field: params.keySet())
       {
            Action__e action = new Action__e(fxTrade_User_ID__c= fxTradeUserId, 
                                                      Type__c= 'object',
                                                      Parameter_1__c= SoqlUtil.getSoqlGMT(params.get(field)),
                                                      Object_Name__c = 'fxAccount__c',
                                                      Object_Field_Name__c = field);
            platformEventActions.add(action);
       }

       fxaTrgDataHandler.addActionPlatformEvents(platformEventActions);
    }

     public void UnassignOppsForRejectedApps(fxAccount__c fxa)
     {
        fxAccount__c oldFxac = trgOldMap.get(fxa.Id);

        if(fxa.Opportunity__c != null && 
           fxa.Knowledge_Result__c == 'Reject' &&
           fxa.OwnerId != systemUserId)
        {
            Opportunity opp = (Opportunity) fxaTrgDataHandler.getRecord(fxa.Opportunity__c);
            opp.OwnerId = systemUserId;
            if(!System.isBatch() && !System.isFuture()){ 
                updatefxList(fxa.Id);
            }
            fxaTrgDataHandler.updateRecord(opp);
        }
     }

     @Future
     private static void updatefxList(Id fxaId) {
        List<CXNoteListController.CXNote> notes = CXNoteListController.getCXNotes(fxaId);
        CXNoteListController.CXNote note = new CXNoteListController.CXNote();
        note.timestamp = (Datetime.now().getTime());
        note.comment = System.Label.Unassign_Opps_Rejected_Apps;
        note.author = UserInfo.getName();
        notes.add(note);
        fxAccount__c fxa = new fxAccount__c(
            Id = fxaId,
            Notes__c = JSON.serialize(notes)
        );

        update fxa;
     }

     public void createOnboardingCase(fxAccount__c newFxAccount)
     {  
         string appFunnelStage_new = newFxAccount.Application_Form_Funnel_Stages__c;    
         string appFunnelStage_old  = trgOldMap.get(newFxAccount.Id).Application_Form_Funnel_Stages__c;
          
         if(fxAccountUtil.isLiveAccount(newFxAccount) &&
            appFunnelStage_old != appFunnelStage_new &&
            appFunnelStage_new == Constants.APP_FUNNEL_APPLICATION_COMPLETED &&
            obCaseApplicationCompletedkDivisions.contains(newFxAccount.Division_Name__c))
         {       
            if(!fxAccIdOBCaseMap.containsKey(newFxAccount.Id))
            {
                Case c = new Case(
                    recordTypeId = RecordTypeUtil.onboardingCaseRecordTypeId,
					Status = 'Open',
					fxAccount__c = newFxAccount.Id, 
					AccountId = newFxAccount.Account__c,
					Lead__c = newFxAccount.Lead__c,
					ContactId = newFxAccount.Contact__c,
					fxTrade_User_Id__c = newFxAccount.fxTrade_User_Id__c,
					Subject = 'Application Review', //temp
					Origin = 'Internal'
                );
                c.setOptions(CaseUtil.caseAssignmentDmlOpts);
                fxaTrgDataHandler.insertRecord(c);
            }
         }
     }

     public void createEIDResult(fxAccount__c newFxAccount)
     {
        fxAccount__c oldFxac = trgOldMap.get(newFxAccount.Id);
 
        boolean myInfoFlagCheck = (newFxAccount.Singpass_Flow__c != oldFxac.Singpass_Flow__c || 
                                    newFxAccount.MyInfo_Residential_Address_Confirmed__c != oldFxac.MyInfo_Residential_Address_Confirmed__c) &&
                                        (newFxAccount.Singpass_Flow__c || newFxAccount.MyInfo_Residential_Address_Confirmed__c);

        if(fxAccountUtil.isLiveAccount(newFxAccount) && 
            currentUserProfileId == registraionProfileID &&
            myInfoFlagCheck &&
            myInfoCheckDivisions.contains(newFxAccount.Division_Name__c))
        {
            if(!fxAccIdEIDMap.containsKey(newFxAccount.Id))
            {
                EID_Result__c eid = new EID_Result__c(
                    fxAccount__c = newFxAccount.Id,
                    Account__c = newFxAccount.Account__c,
                    Lead__c = newFxAccount.Lead__c,
                    Provider__c = 'Singpass',
                    Date__c = System.now()
                );
                fxaTrgDataHandler.insertRecord(eid);
            }
        }
     }

    public void createIHSDiscrepanciesCase(fxAccount__c newFxac)
    {
        fxAccount__c oldFxac;
        Map<String, String> piuFieldsMap;
        Map<String, String> piuChangedFieldsMap;
        String caseDescription;

        //check if the live fxAccount has TAX_Declarations__c 
        if(newFxac.TAX_Declarations__c != null &&
            fxAccountUtil.isLiveAccount(newFxac) &&
            //SP-12730 And check if US Shares Trading is enabled
            newFxac.US_Shares_Trading_Enabled__c
            )
        {
            oldFxac = trgOldMap.get(newFxac.Id);
            piuFieldsMap = getPIUFields();
            piuChangedFieldsMap = getPIUChangedFields(piuFieldsMap, newFxac, oldFxac);

            //check if the PIU fields are updated and create a case
            if(!piuChangedFieldsMap.isEmpty()) 
            {
            caseDescription = getIHSDiscrepanciesCaseDescription(piuChangedFieldsMap);
            Case c = new Case(
                Subject = IHS_CASE_SUBJECT_SUBTYPE,
                Description = caseDescription,
                Inquiry_Nature__c = Constants.CASE_INQUIRY_NATURE_ACCOUNT,
                Type__c = Constants.CASE_TYPE_TAX_DECLARATION,
                Subtype__c = IHS_CASE_SUBJECT_SUBTYPE,
                Live_or_Practice__c = Constants.CASE_LIVE,
                Origin = Constants.CASE_ORIGIN_INTERNAL,
                OwnerId = UserUtil.getQueueId('On Boarding Cases'),
                RecordTypeId = supportCaseRecordTypeId,
                fxAccount__c = newFxac.Id,
                Lead__c = newFxac.Lead__c,
                AccountId = newFxac.Account__c,
                ContactId = newFxac.Contact__c
            );
            fxaTrgDataHandler.insertRecord(c);
            }
        }
    }

    private Map<String, String> getPIUFields() 
    {
        Map<String, String> result =  new Map<String, String>();
        for (IHS_Integration_Field__mdt f :
            IHS_Integration_Field__mdt.getAll().values())
            result.put(f.Field_Name__c, f.Label);
        return result;
    }

    private Map<String, String> getPIUChangedFields(Map<String, String> fieldNamesMap, fxAccount__c newFxa, fxAccount__c oldFxa) 
    {
        Map<String, String> changedFieldsValuesMap = new Map<String, String>();
        for (String f : fieldNamesMap.keySet())
            if (oldFxa.get(f) != newFxa.get(f))
            {
                changedFieldsValuesMap.put(fieldNamesMap.get(f), 'changed from "' + oldFxa.get(f) + '" to "' + newFxa.get(f) + '"');
            }
        return changedFieldsValuesMap;
    }

    private String getIHSDiscrepanciesCaseDescription(Map<String, String> changedFieldsMap)
    {
         String caseDescription = System.Label.IHS_Discrepancies_Case_Description + '\n' + '\n';
         for (String d : changedFieldsMap.keySet())
         {
             caseDescription += '- ' + d + ' ' + changedFieldsMap.get(d) + '\n';
         }
         return caseDescription;
    }

    public void createCorporateTaxDeclarationReviewCase(fxAccount__c newFxac) {
        String caseStatus = 'interviewManualReview';
        if((trgOldMap == null && newFxac.W8_W9_Status__c == caseStatus)
            || (trgOldMap != null && newFxac.W8_W9_Status__c == caseStatus 
                && trgOldMap.containsKey(newFxac.Id) 
                && trgOldMap.get(newFxac.Id).W8_W9_Status__c != newFxac.W8_W9_Status__c)) {
                
            Case c = new Case(
                Subject = CASE_CORPORATE_REVIEW_SUBJECT,
                Type__c = Constants.CASE_TYPE_TAX_DECLARATION,
                Subtype__c = CASE_CORPORATE_REVIEW_SUBJECT,
                OwnerId = UserUtil.getQueueId('AML Team'),
                RecordTypeId = supportCaseRecordTypeId,
                fxAccount__c = newFxac.Id,
                Division__c = newFxac.Division_Name__c,
                AccountId = newFxac.Account__c
                );
                
            fxaTrgDataHandler.insertRecord(c);
        }        
    }


    public void setAccountsToCloseOnly (fxAccount__c newFxac){
        if (trgOldMap.containsKey(newFxac.Id) 
            && trgOldMap.get(newFxac.Id).Conditionally_Approved__c != newFxac.Conditionally_Approved__c
            && trgOldMap.get(newFxac.Id).Division_Name__c!=Constants.DIVISIONS_OANDA_GLOBAL_MARKETS){
            if(newFxac.Conditionally_Approved__c){
                if(fxAccIdsToSetCloseOnly.size()==0 || fxAccIdsToSetCloseOnly[fxAccIdsToSetCloseOnly.size()-1].size()==50){
                    fxAccIdsToSetCloseOnly.add(new List<Id> {newFxac.Id});
                }else{
                    List<Id> tempList = fxAccIdsToSetCloseOnly[fxAccIdsToSetCloseOnly.size()];
                    tempList.add(newFxac.Id);
                    fxAccIdsToSetCloseOnly[fxAccIdsToSetCloseOnly.size()]=tempList;
                }
            }else{
                if(fxAccIdsToUnsetCloseOnly.size()==0 || fxAccIdsToUnsetCloseOnly[fxAccIdsToUnsetCloseOnly.size()-1].size()==50){
                    fxAccIdsToUnsetCloseOnly.add(new List<Id> {newFxac.Id});
                }else{
                    List<Id> tempList = fxAccIdsToUnsetCloseOnly[fxAccIdsToUnsetCloseOnly.size()];
                    tempList.add(newFxac.Id);
                    fxAccIdsToUnsetCloseOnly[fxAccIdsToUnsetCloseOnly.size()]=tempList;
                }
            }
        }
    }

     public void commitTriggerChanges()
     {
         fxaTrgDataHandler.commitTriggerChanges();
     }
    
    
    public Boolean callEndpointForSharesRecalculation(fxAccount__c newFxAcc, fxAccount__c oldFxAcc){
        Boolean result = false;
        Boolean divisionEnabledRecalc = (String.isNotBlank(newFxAcc.Division_Name__c)
                                            && dsu.getDivisionSettingByDivisionName(newFxAcc.Division_Name__c)?.Enable_Shares_Recalculation__c == true);
        /* trigger group recalculation for:
         * SP-12309 US_Shares_Trading_Enabled__c is edited
         * SP-13551 Professional_Trader__c field is edited
         * SI- 35 trigger also for funnel update to RFF
        */
        if ((oldFxAcc.US_Shares_Trading_Enabled__c != newFxAcc.US_Shares_Trading_Enabled__c
                || oldFxAcc.Professional_Trader__c != newFxAcc.Professional_Trader__c
                || (oldFxAcc.Funnel_Stage__c != newFxAcc.Funnel_Stage__c
                    && craCheckFunnelStages.contains(newFxAcc.Funnel_Stage__c)
                    && !craCheckFunnelStages.contains(oldFxAcc.Funnel_Stage__c)
                    && fxAccountUtil.isLiveAccount(newFxAcc)))
            && divisionEnabledRecalc) {
            result = true;
        }

        return result;
    }

    //Can be removed after fully implementation
    private void triggerSharesRecalcBatch() {
        if(!fxAccIdsForRecalculaition.isEmpty()) {
            BatchSharesByOneIdTriggerRecalc.executeBatch(fxAccIdsForRecalculaition);
        }
    }

    private void triggerCASValidateBatcg() {
        try {
            Set<Id> fxAccIdsToFireCASCurrent = Utilities.findDiff(fxAccIdsToFireCAS, fxAccIdsToFireCASUsed);
            if(!fxAccIdsToFireCASCurrent.isEmpty()) {
                ComplyAdvantageValidateFxAccsBatch.executeBatch(fxAccIdsToFireCASCurrent, oldFxMapforCAS);
                fxAccIdsToFireCASUsed.addAll(fxAccIdsToFireCASCurrent);
            }
        } catch (Exception ex) {
            Logger.error(
                'Error calling ComplyAdvantageValidateFxAccsBatch from fxAccount trigger', // msg
                'fxAccountAfterTriggerHandler', // category
                ex.getMessage() // details
            );
        }
    }
}