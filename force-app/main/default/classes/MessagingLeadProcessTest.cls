/**
 * @File Name          : MessagingLeadProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/4/2024, 11:46:55 AM
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class MessagingLeadProcessTest {

	@isTest
	private static void testCreateLeadFromMsgSession() {
		// Test data setup
		String caseRecordType = SPSettingsManager.getSetting(
			MessagingConst.CASE_RECORD_TYPE_DEV_NAME_SETTING
		);
		MessagingSession session = new MessagingSession(
			Id =  null,
			Case_Record_Type__c = caseRecordType,
			Last_User_Intent__c = MessagingConst.ACCOUNT_DELETE_INTENT,
			Escalate_to_Agent__c = true,
			Inquiry_Nature__c = 'Trade',
			Case_Type__c = 'Close a Trade for Client',
			Region__c = 'Asia',
			User_Email__c = 'user@email-server.com',
			Live_or_Practice__c = 'Practice',
			User_Authenticated__c = true,
			Language_Code__c = 'de'
		);
		// Actual test
		Test.startTest();
		MessagingLeadProcess process = new MessagingLeadProcess(session);
		Lead newLead = process.createLeadFromMsgSession();
		Test.stopTest();
		Assert.areEqual(true, newLead != null, 'Lead should be created');

		// Asserts
	}

	@IsTest
	static void getLeadLanguage1() {
		Test.startTest();
		MessagingLeadProcess instance = 
			new MessagingLeadProcess(new MessagingSession());
		String languageCode = OmnichanelConst.ENGLISH_LANG_CODE;
		String result = instance.getLeadLanguage(
			languageCode // languageCode
		);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	/**
	 * Test 1
	 * languageCode = en_US, languagesWithAlternativeName = null
	 * Test 2
	 * languageCode = zh_CN, languagesWithAlternativeName = {zh_CN}
	 */
	@IsTest
	static void getLeadLanguage2() {
		Test.startTest();

		// Test 1 -> return "English"
		MessagingLeadProcess instance1 = 
			new MessagingLeadProcess(new MessagingSession());
		String languageCode1 = OmnichanelConst.ENGLISH_LANG_CODE;
		Set<String> languagesWithAlternativeName1 = null;
		String result1 = instance1.getLeadLanguage(
			languageCode1, // languageCode
			languagesWithAlternativeName1 // languagesWithAlternativeName
		);

		// Test 2 -> return "Chinese Simplified"
		MessagingLeadProcess instance2 = 
			new MessagingLeadProcess(new MessagingSession());
		String languageCode2 = OmnichanelConst.CHINESE_SIMP_LANG_CODE;
		Set<String> languagesWithAlternativeName2 = new Set<String> {
			OmnichanelConst.CHINESE_SIMP_LANG_CODE
		};
		String result2 = instance2.getLeadLanguage(
			languageCode2, // languageCode
			languagesWithAlternativeName2 // languagesWithAlternativeName
		);

		Test.stopTest();
		
		// Test 1
		Assert.isNotNull(result1, 'Invalid result');
		// Test 2
		Assert.isNotNull(result2, 'Invalid result');
	}

}