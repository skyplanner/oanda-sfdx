/**
 * Tests: FxAccountAuthenticationCodeResource
 * @author Fernando Gomez
 */
@isTest
private class FxAccountAuthenticationCodeResourceTest {
	/**
	 * error uri
	 */
	@isTest
	static void testErrorUri0() {
		RestRequest req;
		RestResponse res;
		
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/';
		req.httpMethod = 'POST';

		RestContext.request = req;
		RestContext.response = res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doGet();
		Test.stopTest();

		System.assertEquals(400, RestContext.response.statusCode);
	}

	/**
	 * error uri
	 */
	@isTest
	static void testErrorUri1() {
		RestRequest req;
		RestResponse res;
		
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/XXXXXX';
		req.httpMethod = 'POST';

		RestContext.request = req;
		RestContext.response = res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doGet();
		Test.stopTest();

		System.assertEquals(400, RestContext.response.statusCode);
	}

	/**
	 * error uri
	 */
	@isTest
	static void testErrorUri2() {
		RestRequest req;
		RestResponse res;
		
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/123456';
		req.httpMethod = 'POST';

		RestContext.request = req;
		RestContext.response = res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doGet();
		Test.stopTest();

		System.assertEquals(404, RestContext.response.statusCode);
	}

	/**
	 * error body
	 */
	@isTest
	static void testErrorBody0() {
		RestRequest req;
		RestResponse res;
		
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/123456';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf('{bad jason:} NO');

		RestContext.request = req;
		RestContext.response = res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doGet();
		Test.stopTest();

		System.assertEquals(400, RestContext.response.statusCode);
	}

	/**
	 * global static void doGet()
	 */
	@isTest
	static void doGet() {
		Account acc;
		fxAccount__c fxAccount;
		RestRequest req;
		RestResponse res;
		TestDataFactory tdf;

		tdf = new TestDataFactory();
		acc = tdf.createTestAccount();
		fxAccount = tdf.createFXTradeAccount(acc);

		// we need to set the fx trade user id
		fxAccount.fxTrade_User_Id__c = 123456.0;
		update fxAccount;

		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/123456';
		req.httpMethod = 'POST';

		RestContext.request = req;
		RestContext.response= res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doGet();
		Test.stopTest();

		fxAccount = [
			SELECT Authentication_Code__c,
				Authentication_Code_Expires_On__c
			FROM fxAccount__c
			WHERE Id = :fxAccount.Id
		];

		System.assertEquals(200, RestContext.response.statusCode);
		System.assertEquals(null,
			(AuthenticationCode)
			JSON.deserialize(
				RestContext.response.responseBody.toString(),
				AuthenticationCode.class
			));
	}

	/**
	 * global static void doGet()
	 */
	@isTest
	static void doPost() {
		Account acc;
		fxAccount__c fxAccount;
		RestRequest req;
		RestResponse res;
		TestDataFactory tdf;
		AuthenticationCode code;

		tdf = new TestDataFactory();
		acc = tdf.createTestAccount();
		fxAccount = tdf.createFXTradeAccount(acc);

		// we need to set the fx trade user id
		fxAccount.fxTrade_User_Id__c = 123456.0;
		fxAccount.Contact__c = [SELECT Id FROM Contact WHERE AccountId = :acc.Id].Id;
		update fxAccount;

		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = '/services/apexrest/v1/fx-account/authentication-code/123456';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf('{"sendEmail": true}');

		RestContext.request = req;
		RestContext.response= res;

		Test.startTest();
		FxAccountAuthenticationCodeResource.doPost();
		Test.stopTest();

		fxAccount = [
			SELECT Authentication_Code__c,
				Authentication_Code_Expires_On__c
			FROM fxAccount__c
			WHERE Id = :fxAccount.Id
		];

		System.assertEquals(200, RestContext.response.statusCode);

		code = (AuthenticationCode)JSON.deserialize(
			RestContext.response.responseBody.toString(),
			AuthenticationCode.class);

		System.assertEquals(code.code, fxAccount.Authentication_Code__c);
		System.assertEquals(code.expiresOn,
			fxAccount.Authentication_Code_Expires_On__c);
	}
}