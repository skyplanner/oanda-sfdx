/**
 * Test class: HelpPortalArticleCtrl
 * @author Fernando Gomez
 * @version 1.0
 * @since April 1st, 2019
 */
@isTest
private class HelpPortalArticleCtrlTest {
	/**
	 * global HelpPortalArticleCtrl(
	 *		ApexPages.KnowledgeArticleVersionStandardController ctrl)
	 */
	@isTest
	static void constructor1() {
		FAQ__kav f;
		ApexPages.KnowledgeArticleVersionStandardController c;
		HelpPortalArticleCtrl ctrl;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question'
		);
		insert f;

		c = new ApexPages.KnowledgeArticleVersionStandardController(f);
        PageReference p = Page.HelpPortalArticle;
        p.getParameters().put('articleId', f.Id);
        Test.setCurrentPage(p);
		ctrl = new HelpPortalArticleCtrl(c);
		System.assertEquals('This is a question', ctrl.article.title);
	}

	/**
	 * global HelpPortalArticleCtrl(Id articleId)
	 */
	@isTest
	static void constructor2() {
		FAQ__kav f;
		HelpPortalArticleCtrl ctrl;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question'
		);
		insert f;

		ctrl = new HelpPortalArticleCtrl(f.Id);
		System.assertEquals('This is a question', ctrl.article.title);
	}

	/**
	 * global void submitRating(String overallRating, String ranking, String comments)
	 */
	@isTest
	static void submitRating() {
		FAQ__kav f;
		HelpPortalArticleCtrl ctrl;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question'
		);
		insert f;

		ctrl = new HelpPortalArticleCtrl(f.Id);
		ctrl.submitRating('Like', '5', null);
		System.assertEquals(1, [
			SELECT COUNT()
			FROM FaqRating__c
		]);
	}
}