/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 02-27-2023
 * @last modified by  : Dianelys Velazquez
**/
@isTest
class UserApiLwcCtrlTest {
    @TestSetup
    static void makeData( ) {
        Lead lead1 = new Lead(
            LastName = 'test lead',
            Email = 'j@example.org');
        insert lead1;

        insert new fxAccount__c(
            Lead__c = lead1.Id,
            Funnel_Stage__c =
                FunnelStatus.TRADED,
            RecordTypeId =
                fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c =
                Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Government_ID__c = '0123456789',
            Email__c = 'j@example.org');
    }

    @isTest
    static void initTestWhenProfileIsDifferentThanCX() {
        UserApiManager.Data data = UserApiLwcCtrl.init();

        List<User_API_Ext_Field__mdt> extFields =
            [SELECT Id FROM User_API_Ext_Field__mdt];

        List<User_API_Sf_Field__mdt> sfFields =
            [SELECT Id FROM User_API_Sf_Field__mdt];
            
        System.assertEquals(extFields.size(), data.extFields.size());

        System.assertEquals(sfFields.size(), data.sfFields.size());
        
        System.assertEquals(false, data.isBizOpsL2Profile);
    }

    @isTest
    static void initTestWhenProfileIsCX() {

        Profile bizOpsProfile = [SELECT Id FROM Profile WHERE Name = :UserApiManager.BIZ_OPS_TECH_SUPPORT_L2];
        
        TestDataFactory tdf = new TestDataFactory();
        User cxUser = tdf.createUser(bizOpsProfile);

        System.runAs(cxUser) {
            UserApiManager.Data data = UserApiLwcCtrl.init();
        

            List<User_API_Ext_Field__mdt> extFields =
                [SELECT Id FROM User_API_Ext_Field__mdt];

            List<User_API_Sf_Field__mdt> sfFields =
                [SELECT Id FROM User_API_Sf_Field__mdt];
                
            System.assertEquals(extFields.size(), data.extFields.size());

            System.assertEquals(sfFields.size(), data.sfFields.size());
            
            System.assertEquals(true, data.isBizOpsL2Profile);
        }
    }

    @isTest
    static void searchExternalEmptyCriteriaReturnsEmptyListTest() {
        UserApiSearch s = new UserApiSearch();

        Test.startTest();

        List<Map<String, Object>> result = 
            UserApiLwcCtrl.searchExternal(s, null, null);

        Test.stopTest();

        System.assertEquals(0, result.size(), 'Invalid result size.');
    }

    @isTest
    static void searchExternalByUserNameOkTest() {
        Test.setMock(HttpCalloutMock.class, 
            CalloutMock.getStaticResourceSuccessMock('TestUserAPISearchOk'));
        UserApiSearch s = new UserApiSearch();       
        s.userName = 'test';
        s.userNameEnv = 'all';

        Test.startTest();

        List<Map<String, Object>> result = 
            UserApiLwcCtrl.searchExternal(s, 50, 0);

        Test.stopTest();

        System.assertEquals(2, result.size(), 'Invalid result size.');
    }

    @isTest
    static void searchExternalByV20AccountIdOkTest() {
        SPGeneralSettings genSettings = SPGeneralSettings.getInstance();

        String tasBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            tasBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            tasBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.TAS_API_WRAPPER_SETTING_NAME).baseUrl;
        }

        String v20AccResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_V20_ACCOUNT_ENDPOINT),
            new List<String> {'v20AccountId', 'live'});
        String userBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) {
            userBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            userBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.USER_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String getUserResource = userBaseUrl + String.format(
            genSettings.getValue(Constants.USER_API_GET_USER_ENDPOINT),
            new List<String> {'10', 'live'}
        );
        Test.setMock(HttpCalloutMock.class, 
            CalloutMock.getMultiStaticResourceSuccessMock(
                new List<CalloutMock.MultiStaticResourceItem> {
                    new CalloutMock.MultiStaticResourceItem(
                        v20AccResource, 'TestTasApiV20AccountGetOk'),
                    new CalloutMock.MultiStaticResourceItem(
                        getUserResource, 'TestUserAPIGetOk')
                }));
        UserApiSearch s = new UserApiSearch();
        s.v20AccId = 'v20AccountId';
        s.v20AccEnv = 'live';

        Test.startTest();

        List<Map<String, Object>> result = 
            UserApiLwcCtrl.searchExternal(s, null, null);

        Test.stopTest();

        System.assertEquals(1, result.size(), 'Invalid result size.');
    }

    @isTest
    static void searchExternalCalloutReturnsExternalServerErrorShouldThrowExceptionTest() {
        Boolean throwException = false;
        Test.setMock(HttpCalloutMock.class, CalloutMock.getErrorMock());
        UserApiSearch s = new UserApiSearch();
        s.userName = 'test';
        s.userNameEnv = 'all';

        Test.startTest();

        try {
            List<Map<String, Object>> result = 
                UserApiLwcCtrl.searchExternal(s, null, null);
        } catch (Exception ex) {
            throwException = true;
        }

        Test.stopTest();

        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    static void searchSfByEmailMatchTest() {
        UserApiSearch criteria = new UserApiSearch();
        criteria.email = 'example';

        List<fxAccount__c> fxAccs = UserApiLwcCtrl.searchSf(criteria, 100, 0);
        
        System.assertEquals(1, fxAccs.size(), 'Invalid result size.');
        System.assertEquals('j@example.org', fxAccs[0].Email__c, 'Email not match.');
    }

    @isTest
    static void searchSfByEmailNoMatchShouldReturnEmptyListTest() {
        UserApiSearch criteria = new UserApiSearch();
        criteria.email = '.com';

        List<fxAccount__c> fxAccs = UserApiLwcCtrl.searchSf(criteria, 100, 0);

        System.assert(fxAccs.isEmpty());
    }

    @isTest
    static void searchSfByNotMatchingAccountNameAndUserIdShouldReturnEmptyListTest() {
        UserApiSearch criteria = new UserApiSearch();
        criteria.accName = 'Test Accpunt';
        criteria.userId = 'fxTradeUserId';
        
        List<fxAccount__c> fxAccs = UserApiLwcCtrl.searchSf(criteria, 100, 0);

        System.assert(fxAccs.isEmpty());
    }

    @isTest
    static void searchSfEmptyCriteriaShouldThrowExceptionTest() {
        Boolean throwException = false;
        fxAccount__c fxAcc1 = [SELECT Email__c FROM fxAccount__c][0];
        UserApiSearch criteria = new UserApiSearch();

        try {
            List<fxAccount__c> fxAccs = UserApiLwcCtrl.searchSf(criteria, 100, 0);            
        } catch (Exception ex) {
            throwException = true;
        }
        
        System.assert(throwException, 'Should throw exception.');
    }

    
    @isTest
    static void saveSfTest() {
        fxAccount__c fxAcc1 = [SELECT Email__c FROM fxAccount__c][0];

        fxAcc1.Email__c = 'info@a.com';

        UserApiLwcCtrl.saveSf(fxAcc1);

        System.assertEquals([SELECT Email__c FROM fxAccount__c][0].Email__c,
            'info@a.com', 'The fxAccount email was not updated correctly.');
    }

    @isTest
    static void saveExternalCalloutReturnsInternalServerErrorShouldThrowExceptionTest() {
        Boolean throwException = false;
        Test.setMock(HttpCalloutMock.class, CalloutMock.getErrorMock());
        
        Map<String, Object> oldRecord = new Map<String, Object> {
            'fxTrade_User_Id__c' => 'fxTradeUserId',
            'Email' => 'test1@testemail.com',
            'Name' => 'Test Name 1',
            'Citizenship' => '',
            'ApiAccess' => false,
            'PricingGroup' => 1,
            'CommissionGroup' => 2,
            'CurrencyConversionGroup' => 3,
            'FinancingGroup' => 4,
            'Env' => 'demo'
        };

        Map<String, Object> record = new Map<String, Object> {
            'fxTrade_User_Id__c' => 'fxTradeUserId',
            'Email' => 'test@testemail.com',
            'Name' => 'Test Name',
            'Citizenship' => 'United States',
            'ApiAccess' => true,
            'PricingGroup' => 2,
            'CommissionGroup' => 3,
            'CurrencyConversionGroup' => 4,
            'FinancingGroup' => 5,
            'Env' => 'demo'
        };

        Test.startTest();

        try {
            UserApiLwcCtrl.saveExternal(record, oldRecord);
        } catch (Exception ex) {
            throwException = true;
        }
        
        Test.stopTest();

        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    static void saveExternalOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());
        Map<String, Object> oldRecord = new Map<String, Object> {
            'fxTrade_User_Id__c' => 'fxTradeUserId',
            'Email' => 'test1@testemail.com',
            'Name' => 'Test Name 1',
            'Citizenship' => '',
            'ApiAccess' => false,
            'PricingGroup' => 1,
            'CommissionGroup' => 2,
            'CurrencyConversionGroup' => 3,
            'FinancingGroup' => 4,
            'Env' => 'demo'
        };

        Map<String, Object> record = new Map<String, Object> {
            'fxTrade_User_Id__c' => 'fxTradeUserId',
            'Email' => 'test@testemail.com',
            'Name' => 'Test Name',
            'Citizenship' => 'United States',
            'ApiAccess' => true,
            'PricingGroup' => 2,
            'CommissionGroup' => 3,
            'CurrencyConversionGroup' => 4,
            'FinancingGroup' => 5,
            'Env' => 'demo'
        };

        Test.startTest();

        UserApiLwcCtrl.saveExternal(record, oldRecord);
        
        Test.stopTest();

        System.assert(true, 'Error saving external.');
    }

    @isTest
    static void getPermisssionsTest() {
        Test.startTest();

        UserApiPermissions.PermissionsWrapper result =
            UserApiLwcCtrl.getPermissions();

        Test.stopTest();

        System.assertEquals(5, result.live.size(),
            'Invalid live permissions size.');
        System.assertEquals(5, result.demo.size(),
            'Invalid demo permissions size.');     
    }
    
    @isTest
    static void callActionOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Map<String, Object> record = UserApiActionTest.getValidRecord();

        Test.startTest();

        String result = (String) UserApiLwcCtrl.callAction(
            'Close_Account', 'Close Account', 'UserApiActionCloseAcc', record);
        
        Test.stopTest();

        System.assertEquals('Close Account action success!!!', result,
            'Error on calling user action.');
    }    
    
    @isTest
    static void callActionWithoutManagerClassShouldThrowExceptionTest() {
        Boolean throwException = false;

        Test.startTest();

        try {
            Object result = UserApiLwcCtrl.callAction(null, null, null, null);
        } catch (Exception e) {
            throwException = true;
        }
        
        Test.stopTest();

        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    static void getV20AccsDetailsOkTest() {
        SPGeneralSettings genSettings = SPGeneralSettings.getInstance();
        String tasBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            tasBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            tasBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.TAS_API_WRAPPER_SETTING_NAME).baseUrl;
        }

        String userV20AccResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_USER_ACCOUNT_ENDPOINT),
            new List<String> {'10', 'live'});

        String groupsResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_GROUPS_ENDPOINT),
            new List<String> {'live','1'});
        
        String mt4ServersResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_MT4_SERVERS_ENDPOINT),
            new List<String> {'live', '0'});

        Test.setMock(HttpCalloutMock.class,
            CalloutMock.getMultiStaticResourceSuccessMock(
                new List<CalloutMock.MultiStaticResourceItem> {
                    new CalloutMock.MultiStaticResourceItem(
                        userV20AccResource, 'TestTasApiV20AccountsByUserOk'),
                    new CalloutMock.MultiStaticResourceItem(
                        groupsResource, 'TestTasApiGroupsGetOk'),
                    new CalloutMock.MultiStaticResourceItem(
                        mt4ServersResource, 'TestTasApiMT4ServersGetOk')
        }));

        Test.startTest();

        List<TasV20AccsDetails> result = UserApiLwcCtrl.getV20AccsDetails(
            'live', '10');

        Test.stopTest();

        System.assertEquals(1, result.size(), 'Invalid result size.');
    }

    @isTest
    static void getAuditTrailsTest() {
        Test.startTest();

        List<Audit_Trail__c> result = UserApiLwcCtrl.getAuditTrails(
            'fxTradeUserId');

        Test.stopTest();

        System.assertEquals(0, result.size(), 'Invalid result size.');
    }

    @isTest
    static void getMT5AccsTest() {
        String userBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) {
            userBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            userBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.USER_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String oneIdurl = userBaseUrl
                         + String.format(
                             CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),
                             new List<String> {'testOanda@testOanda.com'});

        String tasBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            tasBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            tasBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.TAS_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String mt5AccsUrl = tasBaseUrl
                            + String.format(
                                SPGeneralSettings.getInstance().getValue(Constants.TAS_API_MT5_ENDPOINT),
                                new List<String> {'2xx22222-22x2-22xx-x22x-22222xx2222', 'demo' + '&mt5_details=true', 'testOanda@testOanda.com'});

            Test.setMock(HttpCalloutMock.class, 
            CalloutMock.getMultiStaticResourceSuccessMock(
                new List<CalloutMock.MultiStaticResourceItem> {
                    new CalloutMock.MultiStaticResourceItem(
                        oneIdurl, 'TestGetOneIdByEmailOK'),
                    new CalloutMock.MultiStaticResourceItem(
                        mt5AccsUrl, 'TestTasApiMT5AccsGetOK')
                }));

        Test.startTest();
        //just to get coverage over DTO
        TasMT5AccsDetails dummyMT5AddDetail  = new TasMT5AccsDetails();
        
        List<TasMT5AccsDetails> result = UserApiLwcCtrl.getMT5AccsDetails('demo', 'fxTradeUserId', 'testOanda@testOanda.com');

        Test.stopTest();

        System.assertEquals(2, result.size(), 'two mt5 Accs returned.');
    }

    @isTest
    static void testLockUnlockMT5Acc() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());
        
        Test.startTest();

        UserApiLwcCtrl.lockUnlockMt5Acc('11111', 'demo', 'testOanda@testOanda.com', '112233');

        Test.stopTest();

        System.assert(true, 'Error saving external.');

    }

    @isTest
    static void resendMT4NotificationOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        TasApiManager.Updatev20AccountWrapper request 
            = new TasApiManager.Updatev20AccountWrapper();
        request.env = 'demo';
        request.fxTradeUserId = 'fxTradeUserId';
        request.v20AccountId = 'v20AccountId';
        
        Test.startTest();

        UserApiLwcCtrl.resendMT4Notification(request);

        Test.stopTest();

        System.assert(true, 'Error saving external.');
    }

    @isTest
    static void updatev20AccountOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        TasApiManager.Updatev20AccountWrapper request 
            = new TasApiManager.Updatev20AccountWrapper();
        request.env = 'demo';
        request.fxTradeUserId = 'fxTradeUserId';
        request.v20AccountId = 'v20AccountId';
        request.divisionTradingGroupId = 2;
        request.prevDivisionTradingGroupId = 1;
        request.mt4ServerId = 2;
        request.prevMt4ServerId = 1;
        
        Test.startTest();

        UserApiLwcCtrl.updatev20Account(request);

        Test.stopTest();

        System.assert(true, 'Error saving external.');
    }

    @isTest
    static void resendMT4NotificationErrorTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getErrorMock());
        Boolean throwException = false;

        TasApiManager.Updatev20AccountWrapper request 
            = new TasApiManager.Updatev20AccountWrapper();
        request.env = 'demo';
        request.fxTradeUserId = 'fxTradeUserId';
        request.v20AccountId = 'v20AccountId';

        Test.startTest();

        try {
            UserApiLwcCtrl.resendMT4Notification(request);
        } catch (Exception e) {
            throwException = true;
        }
        
        Test.stopTest();

        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    static void getUserInfoOkTest() {
        SPGeneralSettings genSettings = SPGeneralSettings.getInstance();
        String tasBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            tasBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            tasBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.TAS_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String groupsResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_GROUPS_ENDPOINT),
            new List<String> {'live','1'});
        String userV20AccResource = tasBaseUrl + String.format(
            genSettings.getValue(Constants.TAS_API_USER_ACCOUNT_ENDPOINT),
            new List<String> {'10', 'live'});
        String userBaseUrl = '';
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) {
            userBaseUrl = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, '', '').credentialUrl;
        } else {
            userBaseUrl = APISettingsUtil.getApiSettingsByName(
                    Constants.USER_API_WRAPPER_SETTING_NAME).baseUrl;
        }
        String getUserResource = userBaseUrl + String.format(
            genSettings.getValue(Constants.USER_API_GET_USER_ENDPOINT),
            new List<String> {'10', 'live'}
        );
        Test.setMock(HttpCalloutMock.class,
            CalloutMock.getMultiStaticResourceSuccessMock(
                new List<CalloutMock.MultiStaticResourceItem> {
                    new CalloutMock.MultiStaticResourceItem(
                        groupsResource, 'TestTasApiGroupsGetOk'),
                    new CalloutMock.MultiStaticResourceItem(
                        userV20AccResource, 'TestTasApiV20AccountsByUserOk'),
                    new CalloutMock.MultiStaticResourceItem(
                        getUserResource, 'TestUserAPIGetOk')
                }));
        
        Test.startTest();

        UserApiLwcCtrl.UserInfoWrapper result =
            UserApiLwcCtrl.getUserInfo('live', '10', '');

        Test.stopTest();

        System.assertEquals('Active', result.status, 'Invalid Status.');
        System.assertEquals(16, result.statuses.userStatuses.size(),
            'Invalid userStatuses size.');
        System.assertEquals(4, result.groups.keySet().size(),
            'Invalid groups size.');
    }

    @isTest
    static void getAudtiLogsOkTest() {
        Test.setMock(HttpCalloutMock.class, 
            CalloutMock.getStaticResourceSuccessMock(
                'TestUserAPIAuditLogOk'));

        Test.startTest();

        List<UserApiAuditLog> result = UserApiLwcCtrl.getHistoricalAuditLogs(
            'demo', 'fxTradeUserId');

        Test.stopTest();

        System.assertEquals(9, result.size(), 'Invalid result size.');
    }

    static User getUser() {
        List<SetupEntityAccess> customPermissions
            = new List<SetupEntityAccess>();
        Set<Id> assignedCustomPermissions = new Set<Id>();
        Profile p = [SELECT Id 
            FROM Profile 
            WHERE Name = 'System Administrator'];
        Id permissionSetId = [SELECT Id
            FROM PermissionSet 
            WHERE Profile.Name = 'System Administrator'][0].Id;
        
        for (SetupEntityAccess sea : [SELECT SetupEntityId 
            FROM SetupEntityAccess 
            WHERE ParentId =: permissionSetId])
            assignedCustomPermissions.add(sea.SetupEntityId);

        for (CustomPermission cP : [SELECT DeveloperName
            FROM CustomPermission 
            WHERE DeveloperName LIKE 'UAPI_%']
        ) {
            SetupEntityAccess objSEA = new SetupEntityAccess();
            objSEA.SetupEntityId = cP.Id;
            objSEA.ParentId = permissionSetId;

            if (!assignedCustomPermissions.contains(cP.Id))
                customPermissions.add(objSEA);
        }

        insert customPermissions;

        User u = new User(
            Alias = 'standt',
            Email ='user1234@actiontest.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = 'user1234@actiontest.com');

        insert u;

        return u;
    }

    @isTest
    static void testThrowErrForNoCredentialExists() {
        
        try {
            Test.startTest(); 
            APISettingsUtil.getApiSettingsByName('NOT_EXISTS');
            Test.stopTest();
        } catch(Exception e) {
            System.assert(e.getMessage().contains('NOT_EXISTS'));
        }
    }
}