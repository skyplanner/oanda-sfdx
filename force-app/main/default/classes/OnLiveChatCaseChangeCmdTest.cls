/**
 * @File Name          : OnLiveChatCaseChangeCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/1/2024, 10:23:00 AM
**/
@IsTest
private without sharing class OnLiveChatCaseChangeCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	/**
	 * test 1 : recList is null => return false
	 * test 2 : recList is empty => return false
	 * test 3 : oldMap is null => return false
	 * test 4 : recList is not empty, oldMap is not null => return true
	 */
	@IsTest
	static void execute() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		LiveChatTranscript newRec = new LiveChatTranscript(
			CaseId = case1Id,
			Tier__c = OmnichannelCustomerConst.TIER_4
		);
		LiveChatTranscript oldRec = new LiveChatTranscript();
		List<LiveChatTranscript> emptyList = new List<LiveChatTranscript>();
		Map<ID, LiveChatTranscript> oldMap = new Map<ID, LiveChatTranscript>{
			null => oldRec
		};
		List<LiveChatTranscript> okList = new List<LiveChatTranscript> { newRec };
		
		Test.startTest();
		// test 1 => return false
		OnLiveChatCaseChangeCmd instance = 
			new OnLiveChatCaseChangeCmd(null, oldMap);
		Boolean result1 = instance.execute();
		// test 2 => return false
		instance = new OnLiveChatCaseChangeCmd(emptyList, oldMap);
		Boolean result2 = instance.execute();
		// test 3 => return false
		instance = new OnLiveChatCaseChangeCmd(okList, null);
		Boolean result3 = instance.execute();
		// test 4 => return true
		instance = new OnLiveChatCaseChangeCmd(okList, oldMap);
		Boolean result4 = instance.execute();
		Test.stopTest();
		
		Case caseObj = [
			SELECT 
				Tier__c,
				Ready_For_Routing__c
			FROM Case
			WHERE Id = :case1Id
		];
		Assert.isFalse(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
		Assert.isTrue(result4, 'Invalid result');
		Assert.isTrue(caseObj.Ready_For_Routing__c, 'Invalid value');
		Assert.areEqual(
			OmnichannelCustomerConst.TIER_4, 
			caseObj.Tier__c, 
			'Invalid value'
		);
	}

	/**
	 * test 1 : CaseId change => return true
	 * test 2 : CaseId = null => return false
	 * test 3 : CaseId does not change  => return false
	 */
	@IsTest
	static void checkRecord() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		OnLiveChatCaseChangeCmd instance = 
			new OnLiveChatCaseChangeCmd(null, null);
			
		Test.startTest();
		// test 1 => return true
		LiveChatTranscript newRec1 = new LiveChatTranscript(
			CaseId = case1Id
		);
		LiveChatTranscript oldRec1 = new LiveChatTranscript();
		Boolean result1 = instance.checkRecord(
			newRec1, // newRec
			oldRec1 // oldRec
		);
		// test 2 => return false
		LiveChatTranscript newRec2 = new LiveChatTranscript();
		LiveChatTranscript oldRec2 = new LiveChatTranscript();
		Boolean result2 = instance.checkRecord(
			newRec2, // newRec
			oldRec2 // oldRec
		);
		// test 3 => return false
		LiveChatTranscript newRec3 = new LiveChatTranscript(
			CaseId = case1Id
		);
		LiveChatTranscript oldRec3 = new LiveChatTranscript(
			CaseId = case1Id
		);
		Boolean result3 = instance.checkRecord(
			newRec3, // newRec
			oldRec3 // oldRec
		);
		Test.stopTest();
		
		Assert.isTrue(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
	}
	
}