/**
 * @File Name          : HelpPortalCoutnrySelectionCtrl_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10/23/2019, 11:35:45 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   dmorales     Initial Version
**/
@isTest
private class HelpPortalCoutnrySelectionCtrl_Test {

	@isTest 
	static void construction() {
		HelpPortalCountrySelectionCtrl c = new HelpPortalCountrySelectionCtrl();
		System.assert( c != null);
	}

    @isTest 
	static void setCookie() {
		HelpPortalCountrySelectionCtrl c = new HelpPortalCountrySelectionCtrl();
        c.countrySelected = 'US';
		PageReference pageRedirect = c.setCookies();
        Cookie country = ApexPages.currentPage().getCookies().get('prefered_country');
        System.assert(pageRedirect != null);
        System.assert(country != null);
	}
}