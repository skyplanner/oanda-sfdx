/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 07-06-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   07-06-2020   dmorales   Update test class cause LiveChatSelectionCon controller was change.
**/
@isTest
private class LiveChatSelectionConTest {

   /*  static testMethod void salesChatTest() {
        //createChatConfigs();
        
        PageReference pageRef = Page.LiveChatSelection;

        Test.setCurrentPage(pageRef);
        
        //chat with sales on new account
        ApexPages.currentPage().getParameters().put('LanguagePreference', 'English');
        ApexPages.currentPage().getParameters().put('Division', 'OC');
        ApexPages.currentPage().getParameters().put('Department', LiveChatSelectionCon.TOPIC_NEW_ACCOUNT);
        
        LiveChatSelectionCon con = new LiveChatSelectionCon();
        String buttonId = con.getChatButtonId();
        
        System.assertEquals('5737A00000000Az', buttonId);
        
        //chat with sales on new account, but from a new region
        ApexPages.currentPage().getParameters().put('Division', 'OAU');
        LiveChatSelectionCon con1 = new LiveChatSelectionCon();
        buttonId = con1.getChatButtonId();
        
        System.assertEquals('573U0000000Ch7n', buttonId);
        
        //chat with CX
        ApexPages.currentPage().getParameters().put('Department', LiveChatSelectionCon.TOPIC_CX_OTHER);
        LiveChatSelectionCon con2 = new LiveChatSelectionCon();
        buttonId = con2.getChatButtonId();
        System.assertEquals('573U0000000Ch7n', buttonId);
        
        buttonId = con.getDefaultChatButtonId();
        System.assertEquals('573U0000000Ch7n', buttonId);
    }
    
    @testSetup
    static void createChatConfigs(){
    	List<Live_Chat_Config__c> configs = new List<Live_Chat_Config__c>();
    	
    	Live_Chat_Config__c config1 = new Live_Chat_Config__c(name = 'OANDA_Customer_Service', Chat_Button_ID__c='573U0000000Ch7n', Is_Default__c = true);
    	insert config1;
    	//configs.add(new Live_Chat_Config__c(name = 'Oanda_Sales_OC', Chat_Button_ID__c='5737A00000000Az', Is_Default__c = false, Region__c = 'OC', Topic__c=LiveChatSelectionCon.TOPIC_NEW_ACCOUNT));
    	Live_Chat_Config__c config = new Live_Chat_Config__c(name = 'Oanda_Sales_OC', Chat_Button_ID__c='5737A00000000Az', Is_Default__c = false);
    	config.Region__c = 'OC';
    	config.Topic__c=LiveChatSelectionCon.TOPIC_NEW_ACCOUNT;
    	insert config;
    	
    	//create onboarding chat config
    	Live_Chat_Config__c config2 = new Live_Chat_Config__c(name = 'Onboarding', Chat_Button_ID__c='5737A0000004CF1', Is_Default__c = false);
    	insert config2;
    }
    
    
    static testMethod void chatDeployConTest(){
    	PageReference pageRef = Page.LiveChatOnboarding;

        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('LanguagePreference', 'cns');
        ApexPages.currentPage().getParameters().put('CustomerEmail', 'test@abc.com');
		ApexPages.currentPage().getParameters().put('ButtonName', 'Onboarding');
		
		ChatDeployCon con = new ChatDeployCon();
		String chatId = con.getChatButtonId();
		
		System.assertEquals('5737A0000004CF1', chatId);
		System.assertEquals(false, con.getIsContact());
		
		String apiRoot = con.getAPIRoot();
		String jsRoot = con.getJSRoot();
		String deployP = con.getDeployParam1();
		String deployP2 = con.getDeployParam2();
        System.assert(apiRoot != null);
        System.assert(jsRoot != null);
        System.assert(deployP != null);
        System.assert(deployP2 != null);
    } */
    @isTest
    static  void testConstructor(){
        
        PageReference pageRef = Page.LiveChatSelection;

        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('LanguagePreference', 'de');
        
        LiveChatSelectionCon ctr = new LiveChatSelectionCon();
        String langPref = ctr.langPref;
        Boolean isOffline = ctr.isOffline;
        ctr.redirect();
		
        System.assert(langPref == 'de');
        System.assert(isOffline != null);
    }
}