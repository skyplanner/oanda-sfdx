/**
 * @File Name          : MiFIDValCasesHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-17-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/22/2020   acantero     Initial Version
**/
public without sharing class MiFIDValCasesHelper {

    static MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();

    static ID newCaseQueueId;
    static ID newCaseDefaultRecTypeId;

    final Map<ID,FxAccountValidationError> errorMap;
    final List<ID> validFxAccountsIdList;
    final List<Case> newCaseList = new List<Case>();
    final List<Case> updateCaseList = new List<Case>();

    public MiFIDValCasesHelper(
        Map<ID,FxAccountValidationError> errorMap,
        List<ID> validFxAccountsIdList
    ) {
        this.errorMap = errorMap;
        this.validFxAccountsIdList = validFxAccountsIdList;
    }

    public void createOrUpdateCases() {
        System.debug('errorMap: ' + errorMap);
        System.debug('validFxAccountsIdList: ' + validFxAccountsIdList);
        newCaseList.clear();
        updateCaseList.clear();
        Boolean errorMapOk = (errorMap != null) && (!errorMap.isEmpty());
        Boolean validFxAccountsIdListOk = 
            (validFxAccountsIdList != null) && (!validFxAccountsIdList.isEmpty());
        if (
            (errorMapOk == false) &&
            (validFxAccountsIdListOk == false)
        ) {
            return;
        }
        //else...
        Set<ID> fxAccountCheckCaseIdSet = new Set<ID>();
        if (errorMapOk) {
            fxAccountCheckCaseIdSet.addAll(errorMap.keySet());
        }
        if (validFxAccountsIdListOk) {
            fxAccountCheckCaseIdSet.addAll(validFxAccountsIdList);
        }
        Map<ID,Case> nonClosedCaseMap = 
            MiFIDValCasesHelper.getFxAccountsOpenCaseMap(fxAccountCheckCaseIdSet);
        if (errorMapOk) {
            processErrors(nonClosedCaseMap);
        }
        if (validFxAccountsIdListOk) {
            processOkValidations(nonClosedCaseMap);
        }
        if (!updateCaseList.isEmpty()) {
            update updateCaseList;
        }
        if (!newCaseList.isEmpty()) {
            insert newCaseList;
        }
    }

    void processErrors(
        Map<ID,Case> nonClosedCaseMap
    ) {
        List<FxAccountValidationError> errorList = errorMap.values();
        for(FxAccountValidationError errorObj : errorList) {
            Case existingCase = (nonClosedCaseMap != null) 
                ? nonClosedCaseMap.get(errorObj.fxAccountObj.Id)
                : null;
            if (existingCase != null) {
                MiFIDValCasesHelper.updateCase(
                    existingCase, 
                    errorObj
                );
                updateCaseList.add(existingCase);
                //...
            } else {
                Case newCase = MiFIDValCasesHelper.createCase(
                    errorObj
                );
                newCaseList.add(newCase);
            }
        }
    }

    void processOkValidations(
        Map<ID,Case> nonClosedCaseMap
    ) {
        for(ID fxAccountId : validFxAccountsIdList) {
            Case existingCase = (nonClosedCaseMap != null) 
                ? nonClosedCaseMap.get(fxAccountId)
                : null;
            if (
                (existingCase != null) &&
                (
                    (existingCase.Subject != System.Label.MifidObsoleteCase) ||
                    (existingCase.MiFID_validated__c == false)
                )
            ) {
                existingCase.Subject = System.Label.MifidObsoleteCase;
                existingCase.MiFID_validated__c = true;
                updateCaseList.add(existingCase);
            }
        }
    }

    public static List<ID> getFxAccountsWithMiFIDCaseOpen(
        List<fxAccount__c> fxAccList 
    ) {
        List<ID> result = new List<ID>();
        if (
            (fxAccList == null) ||
            fxAccList.isEmpty()
        ) {
            return result;
        }
        //else...
        Set<ID> fxAccountIdSet = new Set<ID>();
        for(fxAccount__c fxAccountObj : fxAccList) {
            fxAccountIdSet.add(fxAccountObj.Id);
        }
        for(List<Case> caseList : [
            SELECT
                fxAccount__c
            FROM 
                Case 
            WHERE
                fxAccount__c IN :fxAccountIdSet
            AND 
                Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE
            AND
                Status <> :MiFIDExpValConst.CASE_CLOSED_STATUS
        ]) {
            for(Case caseObj: caseList) {
                result.add(caseObj.fxAccount__c);
            }
        }
        return result;
    }

    public static Case createCase(
        FxAccountValidationError info
    ) {
        ID recTypeId = getMiFIDCaseRecTypeId();
        Case caseObj = new Case(
            Status = MiFIDExpValConst.CASE_OPEN_STATUS,
            RecordTypeId =  recTypeId,
            Subject = info.subject,
            Origin = MiFIDExpValConst.CASE_INTERNAL_ORIGIN,
            Priority = MiFIDExpValConst.CASE_NORMAL_PRIORITY,
            Inquiry_Nature__c = MiFIDExpValConst.CASE_ACCOUNT_INQUIRY_NATURE,
            Type__c = MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE,
            Division__c = info.fxAccountObj.Division_Name__c,
            Description = info.error,
            Live_or_Practice__c = MiFIDExpValConst.CASE_LIVE,
            AccountId = info.fxAccountObj.Account__c,
            fxAccount__c = info.fxAccountObj.Id,
            ContactId = info.fxAccountObj.Contact__c
        );
        ID queueId = getMiFIDCaseQueueId();
        if (queueId != null) {
            caseObj.OwnerId = queueId;
        }
        return caseObj;
    }

    public static void updateCase(
        Case caseObj,
        FxAccountValidationError info
    ) {
        caseObj.Subject = info.subject;
        caseObj.Division__c = info.fxAccountObj.Division_Name__c;
        caseObj.Description = info.error;
        caseObj.MiFID_validated__c = false;
    }

    @testVisible
    static ID getMiFIDCaseQueueId() {
        if (
            (newCaseQueueId == null) &&
            String.isNotBlank(valSettings.caseQueue)
        ) {
            List<Group> queueList = [
                SELECT 
                    Id 
                FROM
                    Group 
                WHERE
                    Type = 'Queue' 
                AND 
                    DeveloperNAME = :valSettings.caseQueue
            ];
            if (!queueList.isEmpty()) {
                newCaseQueueId = queueList[0].Id;
            }
        }
        return newCaseQueueId;
    }

    @testVisible
    static ID getMiFIDCaseRecTypeId() {
        if (newCaseDefaultRecTypeId == null) {
            newCaseDefaultRecTypeId = 
                Schema.SObjectType.Case
                    .getRecordTypeInfosByDeveloperName()
                    .get(valSettings.caseRecordType)
                    .getRecordTypeId();
        }
        return newCaseDefaultRecTypeId;
    }

    public static List<Case> getFxAccountsOpenCaseList(Set<ID> fxAccountIdSet) {
        if (
            (fxAccountIdSet == null) ||
            fxAccountIdSet.isEmpty()
        ) {
            return new List<Case>();
        }
        //else...
        List<Case> nonClosedCaseList = [
            SELECT
                Id,    
                fxAccount__c,
                Subject,
                MiFID_validated__c
            FROM 
                Case 
            WHERE
                fxAccount__c IN :fxAccountIdSet
            AND 
                Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE
            AND
                Status <> :MiFIDExpValConst.CASE_CLOSED_STATUS
        ];
        return nonClosedCaseList;
    }

    public static Map<ID,Case> getFxAccountsOpenCaseMap(Set<ID> fxAccountIdSet) {
        List<Case> fxAccountsOpenCaseList = 
            getFxAccountsOpenCaseList(fxAccountIdSet);
        Map<ID,Case> result = new Map<ID,Case>();
        for(Case caseObj : fxAccountsOpenCaseList) {
            result.put(
                caseObj.fxAccount__c, 
                caseObj
            );
        }
        return result;
    }
    
}