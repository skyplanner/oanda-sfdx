/**
 * @File Name          : SPInvalidFieldValueException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/17/2022, 1:29:54 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/17/2022, 1:10:25 PM   acantero     Initial Version
**/
public inherited sharing class SPInvalidFieldValueException extends Exception {

    public static SPInvalidFieldValueException getNewForRequired(
        String objectName,
        String fieldName
    ) {
        return new SPInvalidFieldValueException(
            'Field "' + objectName + '.' + fieldName + '" is null or empty'
        );
    }

}