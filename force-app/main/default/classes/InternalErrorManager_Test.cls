/**
 * @File Name          : InternalErrorManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/20/2021, 4:00:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/27/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class InternalErrorManager_Test {

    //exception message length > 255
    @isTest
    private static void safeLogException_test() {
        Exception testException;
        String msg = '012345678900123456789001234567890012345678900123456789001234567890' + 
        '01234567890012345678900123456789001234567890012345678900123456789001234567890' + 
        '01234567890012345678900123456789001234567890012345678900123456789001234567890' +
        '01234567890012345678900123456789001234567890012345678900123456789001234567890';
        Test.startTest();
        try {
            throw new OmnichanelRoutingException(msg);
        } catch (Exception ex) {
            testException = ex;
        }
        InternalErrorManager.safeLogException('test', testException);
        Test.stopTest();
        Integer count = [select count() from Internal_Error__c];
        System.assertEquals(1, count);
    }

    //exception is null
    @isTest
    private static void safeLogException_test2() {
        Test.startTest();
        InternalErrorManager.safeLogException(null, null);
        Test.stopTest();
        Integer count = [select count() from Internal_Error__c];
        System.assertEquals(0, count);
    }

    //exception is null
    @isTest
    private static void safeLogException_test3() {
        Test.startTest();
        try {
            Account invalidAccount = new Account();
            insert invalidAccount;
        } catch (Exception ex) {
            InternalErrorManager.safeLogException('test', ex);
        }
        Test.stopTest();
        Integer count = [select count() from Internal_Error__c];
        System.assertEquals(1, count);
    }
}