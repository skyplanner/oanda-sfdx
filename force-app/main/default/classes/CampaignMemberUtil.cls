public with sharing class CampaignMemberUtil {
	static final String CAMPAIGN_TYPE_REACTIVATION = 'Reactivation';
	
	public static void addTasks(CampaignMember[] campaignMembers) {
		Set<Id> campaignIds = new Set<Id>();
		for(CampaignMember cm : campaignMembers) {
			campaignIds.add(cm.CampaignId);
		}
		
		Map<Id, Campaign> campaignById = new Map<Id, Campaign>([SELECT Id, Name, OwnerId, Type, ParentId, Task_Round_Robin_Queue_Name__c, Task_Record_Type__c FROM Campaign WHERE Is_Auto_Create_Task__c = true AND Id IN :campaignIds]);
		
		List<Task> tasksToInsert = new List<Task>();
		Map<CampaignMember, Task> taskByCampaignMember = new Map<CampaignMember, Task>();
		for(CampaignMember cm : campaignMembers) {
			try {
				Campaign c = campaignById.get(cm.CampaignId);
				if(c!=null) {
					Task t = TaskUtil.getTask(getOwnerId(cm, c), (cm.ContactId==null?cm.LeadId:cm.ContactId), c.Type + ' followup', getTaskRecordTypeId(c));
					taskByCampaignMember.put(cm, t);
					tasksToInsert.add(t);
				}
			} catch(Exception ex) {
				cm.addError(ex);
			}
		}
		
		//commit the round robin queue custom setting
		RoundRobinAssignment.commitAssignments();
		
		if(tasksToInsert.size()>0) {
			insert tasksToInsert;
		}
		
		List<Lead> leadsToUpdate = new List<Lead>();
		List<Contact> contactsToUpdate = new List<Contact>();
		for(CampaignMember cm : taskByCampaignMember.keySet()) {
			Task t = taskByCampaignMember.get(cm);
			// update campaign member with task id
			cm.Associated_Task_Owner__c = t.OwnerId;
			
			boolean isReactivationCampaign = false;
			if(campaignById.get(cm.CampaignId).Type == CAMPAIGN_TYPE_REACTIVATION) {
				isReactivationCampaign = true;
			}
			// update lead/contact with campaign, task id
			if(LeadUtil.isLeadId(t.WhoId)) {
				Lead l = new Lead(Id=t.WhoId, Most_Recent_Campaign__c=cm.CampaignId);
				if(isReactivationCampaign) {
					l.Last_Reactivation_Campaign_Date__c = Date.today();
				}
				leadsToUpdate.add(l);
			} else {
				Contact c = new Contact(Id=t.WhoId, Most_Recent_Campaign__c=cm.CampaignId);
				if(isReactivationCampaign) {
					c.Last_Reactivation_Campaign_Date__c = Date.today();
				}
				contactsToUpdate.add(c);
			}
		}
		if(leadsToUpdate.size()>0) {
			update leadsToUpdate;
		}
		if(contactsToUpdate.size()>0) {
			update contactsToUpdate;
		}
	}
	
	public class QueueNameException extends Exception {}
	
	private static Id getTaskRecordTypeId(Campaign c) {
		return RecordTypeUtil.getRecordTypeId(c.Task_Record_Type__c, 'Task');
	}
	
	private static Id getOwnerId(CampaignMember cm, Campaign c) {
		if(c.Task_Round_Robin_Queue_Name__c!=null&&c.Task_Round_Robin_Queue_Name__c!='') {
			Id queueId = UserUtil.getQueueId(c.Task_Round_Robin_Queue_Name__c);
			if(queueId!=null) {
				return RoundRobinAssignment.getNextOwnerId(queueId);
			}
			throw new QueueNameException('Queue name "' + c.Task_Round_Robin_Queue_Name__c + '" for campaign "' + c.Name + '" is invalid');
		} else if(c.OwnerId!=UserUtil.getSystemUserId()) {
			return c.OwnerId;
		}
		return null;
	}
	
	public static void getLastTradeDateFromFxAccount(List<CampaignMember> cms) {
		System.debug('cms: ' + cms);
		Set<Id> conIds = new Set<Id>();
		Map<Id, List<CampaignMember>> campaignMembersByContactId = new Map<Id, List<CampaignMember>>();
		for(CampaignMember cm : cms) {
			if(cm.ContactId!=null) {
				List<CampaignMember> campaignMembers = campaignMembersByContactId.get(cm.ContactId);
				if(campaignMembers==null) {
					campaignMembers = new List<CampaignMember>();
					campaignMembersByContactId.put(cm.ContactId, campaignMembers);
				}
				campaignMembers.add(cm);
			}
		}
		System.debug('campaignMembersByContactId: ' + campaignMembersByContactId);
		if(campaignMembersByContactId.size()>0) {
			for(fxAccount__c fxa : [SELECT Last_Trade_Date__c, Contact__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Contact__c IN :campaignMembersByContactId.keySet()]) {
				for(CampaignMember cm : campaignMembersByContactId.get(fxa.Contact__c)) {
					if(cm.Last_Trade_DateTime_Before_Campaign__c==null || cm.Last_Trade_DateTime_Before_Campaign__c < fxa.Last_Trade_Date__c) {
						cm.Last_Trade_DateTime_Before_Campaign__c = fxa.Last_Trade_Date__c;
					}
				}
			}
		}
	}
	
	public static List<CampaignMember> getMostRecentReactivationCampaignMembers(Set<Id> contactIds) {
		if (contactIds.size() == 0) {
			return new List<CampaignMember>();
		}
		// get the reactivation campaign members, ordered by created date, newest first
		List<CampaignMember> cms = [SELECT Id, CreatedDate, CampaignId, ContactId, Status, HasResponded FROM CampaignMember WHERE ContactId IN :contactIds AND CampaignId IN (SELECT Id FROM Campaign WHERE Type=:CampaignUtil.CAMPAIGN_TYPE_REACTIVATION) ORDER BY CreatedDate DESC];
	
		Map<Id, CampaignMember> cmById = new Map<Id, CampaignMember> (cms);
		Set<Id> reactivationContactIds = new Set<Id>();
		
		for(CampaignMember cm : cms) {
			// check to see if there was a CM for this contact (ie. contact was added to multiple reactivation campaigns)
			if(reactivationContactIds.contains(cm.ContactId)) {
				// if there was, this is an older CM (ie. a previous reactivation campaign)
				cmById.remove(cm.Id);
			} else {
				reactivationContactIds.add(cm.ContactId);
			}
		}
		return cmById.values();
	}
}