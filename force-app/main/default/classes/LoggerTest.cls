/**
 * @File Name          : LoggerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/18/2024, 5:52:42 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/18/2024, 10:14:29 AM   acantero     Initial Version
**/
@isTest
public class LoggerTest {
    
    @testSetup
	static void setup () {
        LoggerTestDataFactory.enableLogSettings();
    }

    /*************** INFO ***************/

    @isTest
    public static void info1() {
        Test.startTest();
        Logger.info('msg1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, null);
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void info2() {
        Test.startTest();
        Logger.info('msg1', 'category1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void info3() {
        Test.startTest();
        Logger.info('msg1', 'category1', 'details1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, 'details1');
    }

    @isTest
    public static void info4() {
        Test.startTest();
        
        Map<String, String> map1 =
            new Map<String, String>{
                'key1' => 'value1'};
        
        Logger.info(
            'msg1',
            'category1',
            map1);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(map1, true));
    }

    @isTest
    public static void info5() {
        Test.startTest();
        
        Account acc =
            new Account(
                LastName = 'Smith');
        
        Logger.info(
            'msg1',
            'category1',
            acc);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(acc, true));
    }

    @isTest
    public static void info6() {
        Test.startTest();
        
        List<Account> accList =
            new List<Account>{
                new Account(
                    LastName = 'Smith')};
        
        Logger.info(
            'msg1',
            'category1',
            accList);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Info');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(accList, true));
    }

    @isTest
    public static void info7() {
        // Deactivate logs
        LogSettings stts = LogSettings.getInstance();
        stts.setActive(false);
        
        Test.startTest();
        Logger.info('msg1');
        Test.stopTest();

        // The log should not been created
        System.assert(
            [SELECT Id FROM Log__c].isEmpty());
    }

    /*************** WARNING ***************/

    @isTest
    public static void warning1() {
        Test.startTest();
        Logger.warning('msg1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, null);
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void warning2() {
        Test.startTest();
        Logger.warning('msg1', 'category1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void warning3() {
        Test.startTest();
        Logger.warning('msg1', 'category1', 'details1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, 'details1');
    }

    @isTest
    public static void warning4() {
        Test.startTest();
        
        Map<String, String> map1 =
            new Map<String, String>{
                'key1' => 'value1'};
        
        Logger.warning(
            'msg1',
            'category1',
            map1);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(map1, true));
    }

    @isTest
    public static void warning5() {
        Test.startTest();
        
        Account acc =
            new Account(
                LastName = 'Smith');
        
        Logger.warning(
            'msg1',
            'category1',
            acc);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(acc, true));
    }

    @isTest
    public static void warning6() {
        Test.startTest();
        
        List<Account> accList =
            new List<Account>{
                new Account(
                    LastName = 'Smith')};
        
        Logger.warning(
            'msg1',
            'category1',
            accList);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Warning');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(accList, true));
    }

    /*************** DEBUG ***************/

    @isTest
    public static void debug1() {
        Test.startTest();
        Logger.debug('msg1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, null);
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void debug2() {
        Test.startTest();
        Logger.debug('msg1', 'category1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void debug3() {
        Test.startTest();
        Logger.debug('msg1', 'category1', 'details1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, 'details1');
    }

    @isTest
    public static void debug4() {
        Test.startTest();
        
        Map<String, String> map1 =
            new Map<String, String>{
                'key1' => 'value1'};
        
        Logger.debug(
            'msg1',
            'category1',
            map1);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(map1, true));
    }

    @isTest
    public static void debug5() {
        Test.startTest();
        
        Account acc =
            new Account(
                LastName = 'Smith');
        
        Logger.debug(
            'msg1',
            'category1',
            acc);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(acc, true));
    }

    @isTest
    public static void debug6() {
        Test.startTest();
        
        List<Account> accList =
            new List<Account>{
                new Account(
                    LastName = 'Smith')};
        
        Logger.debug(
            'msg1',
            'category1',
            accList);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Debug');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(accList, true));
    }

    @isTest
    public static void debug7() {
        // Deactivate debugs
        LogSettings stts = LogSettings.getInstance();
        stts.setDebug(false);
        
        Test.startTest();
        Logger.debug('msg1');
        Test.stopTest();

        // The log should not been created
        System.assert(
            [SELECT Id FROM Log__c].isEmpty());
    }

    /*************** ERROR ***************/

    @isTest
    public static void error1() {
        Test.startTest();
        Logger.error('msg1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, null);
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void error2() {
        Test.startTest();
        Logger.error('msg1', 'category1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, null);
    }

    @isTest
    public static void error3() {
        Test.startTest();
        Logger.error('msg1', 'category1', 'details1');
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, 'details1');
    }

    @isTest
    public static void error4() {
        Test.startTest();
        
        Map<String, String> map1 =
            new Map<String, String>{
                'key1' => 'value1'};
        
        Logger.error(
            'msg1',
            'category1',
            map1);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(map1, true));
    }

    @isTest
    public static void error5() {
        Test.startTest();
        
        Account acc =
            new Account(
                LastName = 'Smith');
        
        Logger.error(
            'msg1',
            'category1',
            acc);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(acc, true));
    }

    @isTest
    public static void error6() {
        Test.startTest();
        
        List<Account> accList =
            new List<Account>{
                new Account(
                    LastName = 'Smith')};
        
        Logger.error(
            'msg1',
            'category1',
            accList);

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(
            log.Details__c,
            Utilities.serialize(accList, true));
    }

    @isTest
    public static void error7() {
        Test.startTest();

        HttpRequest req = new HttpRequest();
        req.setBody('Request information.');
        req.setMethod('GET');
        req.setEndpoint('www.google.com');

        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(500);
        resp.setBody('Response test error.');

        Logger.error('msg1', 'category1', req, resp);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        
        Map<String, Map<String, String>> detailsMap =
            (Map<String, Map<String, String>>)JSON.deserialize(
                log.Details__c, Map<String, Map<String, String>>.class);

        Map<String, String> reqMap = detailsMap.get('request');
        Map<String, String> respMap = detailsMap.get('response');

        System.assertEquals(reqMap.get('method'), 'GET');
        System.assertEquals(reqMap.get('endpoint'), 'www.google.com');
        System.assertEquals(reqMap.get('body'), 'Request information.');
        System.assertEquals(respMap.get('statusCode'), '500');
        System.assertEquals(respMap.get('body'), 'Response test error.');
    }

    @isTest
    public static void error8() {
        Test.startTest();
        
        Database.SaveResult[] srs =
            Database.insert(
                new List<fxAccount__c>{
                    new fxAccount__c()},
                false);

        Logger.error(
            'category1',
            srs);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
        System.assertEquals(log.Category__c, 'category1');
        System.assert(
            String.isNotBlank(log.Details__c));
    }

    @isTest
    public static void error9() {
        Test.startTest();
        
        Database.UpsertResult[] srs =
            Database.upsert(
                new List<fxAccount__c>{
                    new fxAccount__c()},
                false);

        Logger.error(
            'category1',
            srs);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Error');
        System.assertEquals(log.Message__c, 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
        System.assertEquals(log.Category__c, 'category1');
        System.assert(
            String.isNotBlank(log.Details__c));
    }

    /*************** EXCEPTION ***************/

    @isTest
    public static void exception1() {
        Test.startTest();

        Exception originalException;

        try{
            throw new HelpPortalException('error1');
        } catch(Exception ex) {
            originalException = ex;
            Logger.exception('from test method', ex);
        }

        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Except');
        System.assertEquals(log.Message__c, 'error1');
        System.assertEquals(log.Category__c, 'from test method');
        System.assertEquals(
            log.Details__c,
            LogException.getDetailsMessage(originalException));
    }

    @isTest
    public static void exception2() {
        Test.startTest();

        Logger.exception('msg1', 'category1', 'details1');
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Except');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        System.assertEquals(log.Details__c, 'details1');
    }

    @isTest
    public static void exception3() {
        Test.startTest();

        HttpRequest req = new HttpRequest();
        req.setBody('Request information.');
        req.setMethod('GET');
        req.setEndpoint('www.google.com');

        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(500);
        resp.setBody('Response test error.');

        Logger.exception('msg1', 'category1', req, resp);
        
        Test.stopTest();

        Log__c log = getLog();

        System.assertEquals(log.Type__c, 'Except');
        System.assertEquals(log.Message__c, 'msg1');
        System.assertEquals(log.Category__c, 'category1');
        
        Map<String, Map<String, String>> detailsMap =
            (Map<String, Map<String, String>>)JSON.deserialize(
                log.Details__c, Map<String, Map<String, String>>.class);

        Map<String, String> reqMap = detailsMap.get('request');
        Map<String, String> respMap = detailsMap.get('response');

        System.assertEquals(reqMap.get('method'), 'GET');
        System.assertEquals(reqMap.get('endpoint'), 'www.google.com');
        System.assertEquals(reqMap.get('body'), 'Request information.');
        System.assertEquals(respMap.get('statusCode'), '500');
        System.assertEquals(respMap.get('body'), 'Response test error.');
    }

    /**
     * Retrieve log from db
     */
    private static Log__c getLog() {
        return
            [SELECT Type__c,
                Message__c,
                Category__c,
                Details__c
            FROM Log__c];
    }
    
}