public with sharing class fxAccountTriggerDataHandler extends TriggerDataHandler
{

    public static final Schema.SObjectType CASE_SOBJECT_TYPE = Schema.Case.getSObjectType();
    public static final Schema.SObjectType ACCOUNT_SOBJECT_TYPE = Schema.Account.getSObjectType();
    public static final Schema.SObjectType OPPORTUNITY_SOBJECT_TYPE = Schema.Opportunity.getSObjectType();
    public static final Schema.SObjectType EID_SOBJECT_TYPE = Schema.EID_Result__c.getSObjectType();

    protected list<Action__e> platformEventActions;
    protected list<External_Notification_Event__e> externalNotificationEvents;

    public fxAccountTriggerDataHandler()
    {
        newRecordsByType = new  Map<Schema.SObjectType,  list<SObject>>
        {
            CASE_SOBJECT_TYPE => new list<Case>{},
            EID_SOBJECT_TYPE => new list<EID_Result__c>{}
        };
        updateRecordsByType = new  Map<Schema.SObjectType,  Map<Id, SObject>>
        {
            ACCOUNT_SOBJECT_TYPE => new Map<Id, Account>{},
            CASE_SOBJECT_TYPE => new Map<Id, Case>{},
            OPPORTUNITY_SOBJECT_TYPE => new Map<Id,Opportunity>{}
        };

        platformEventActions = new  list<Action__e>{};
    }
    //  public override void insertRecord(SObject rec)
    //  public override SObject getRecord(Id recordId)
    //  public override void updateRecord(SObject rec)
    
    public void addActionPlatformEvent(Action__e actionEvent)
    {
        platformEventActions.add(actionEvent);
    }

    public void addActionPlatformEvents(List<Action__e> actionEvents)
    {
        platformEventActions.addAll(actionEvents);
    }

    public void addExternalNotificationEvent(External_Notification_Event__e event)
    {
        if(externalNotificationEvents == null) {
            externalNotificationEvents = new List<External_Notification_Event__e>();
        }
        externalNotificationEvents.add(event);
        
    }
    
    private void publishPlatformEvents()
    {
        if(platformEventActions.size() > 0)
        {
            ActionUtil util = new ActionUtil();
            util.publish(platformEventActions);
        }
        if(externalNotificationEvents != null && !externalNotificationEvents.isEmpty()) {
            ExternalNotificationEventService.publishEvents(externalNotificationEvents);
            externalNotificationEvents = null;
        }
    }

    public void commitTriggerChanges()
    {
        commitRecords(ACCOUNT_SOBJECT_TYPE, false);

        commitRecords(OPPORTUNITY_SOBJECT_TYPE, false);

        commitRecords(CASE_SOBJECT_TYPE, false);

        commitRecords(EID_SOBJECT_TYPE, false);

        publishPlatformEvents();

        reset();
    } 

    public override void reset()
    {
        newRecordsByType = new  Map<Schema.SObjectType,  list<SObject>>
        {
            CASE_SOBJECT_TYPE => new list<Case>{}
        };
        updateRecordsByType = new  Map<Schema.SObjectType,  Map<Id, SObject>>
        {
            ACCOUNT_SOBJECT_TYPE => new Map<Id, Account>{},
            CASE_SOBJECT_TYPE => new Map<Id, Case>{},
            OPPORTUNITY_SOBJECT_TYPE => new Map<Id,Opportunity>{}
        }; 
        
        platformEventActions = new  list<Action__e>{};

    }

    public void resetDataBySobjectType(Schema.SObjectType sobjType)
    {
        if(sobjType == ACCOUNT_SOBJECT_TYPE )
        {
            updateRecordsByType.put(ACCOUNT_SOBJECT_TYPE, new Map<Id,Account>{});
        }
        else if(sobjType == OPPORTUNITY_SOBJECT_TYPE)
        {
            updateRecordsByType.put(OPPORTUNITY_SOBJECT_TYPE, new Map<Id, Opportunity>{});
        }
        else if(sobjType == CASE_SOBJECT_TYPE)
        {
            newRecordsByType.put(CASE_SOBJECT_TYPE, new List<Case>{});        
            updateRecordsByType.put(CASE_SOBJECT_TYPE, new Map<Id, Case>{});
        }
    }

}