/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-28-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private without sharing class JobStatusHelper_Test {
	@testSetup
	static void setup() {
		JobStatusTriggerHandler.enabled = false;
		ScheduledEmailTestUtil.createStatusJob(
			SendScheduledEmailJob.SEND_EMAIL_JOB,
			JobStatusHelper.ACTIVE_STATUS
		);
	}

	//valid job name
	@isTest
	static void getJobCurrentRequestDate1() {
		Test.startTest();
		Datetime result = JobStatusHelper.getJobCurrentRequestDate(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		Test.stopTest();
		System.assertNotEquals(null, result, 'result can not be null');
	}

	//invalid job name
	@isTest
	static void getJobCurrentRequestDate2() {
		Boolean error = false;
		Test.startTest();
		try {
			Datetime result = JobStatusHelper.getJobCurrentRequestDate(
				'some fake name'
			);
			//...
		} catch (JobStatusHelper.JobStatusException ex) {
			error = true;
		}
		Test.stopTest();
		System.assertEquals(true, error, 'Job name not exist');
	}

	//valid job name
	@isTest
	static void getJobStatusForUpdate1() {
		Test.startTest();
		Job_Status__c result = JobStatusHelper.getJobStatusForUpdate(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		Test.stopTest();
		System.assertNotEquals(null, result, 'result can not be null');
	}

	//invalid job name
	@isTest
	static void getJobStatusForUpdate2() {
		Boolean error = false;
		Test.startTest();
		try {
			Job_Status__c result = JobStatusHelper.getJobStatusForUpdate(
				'some fake name'
			);
			//...
		} catch (JobStatusHelper.JobStatusException ex) {
			error = true;
		}
		Test.stopTest();
		System.assertEquals(true, error, 'Job name not exist');
	}

	//valid data
	@isTest
	static void deactivateJob() {
		Test.startTest();
		JobStatusTriggerHandler.enabled = false;
		Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		JobStatusHelper.deactivateJob(jobStatus);
		Test.stopTest();
		String status = [
			SELECT Status__c
			FROM Job_Status__c
			WHERE Name = :SendScheduledEmailJob.SEND_EMAIL_JOB
			LIMIT 1
		]
		.Status__c;
		System.assertEquals(
			JobStatusHelper.INACTIVE_STATUS,
			status,
			'Job is deactivate'
		);
	}
}