/**
 * @Description  : Class creates High Risk Review Due cases.
 * @Author       : Jakub Fik
 * @Date         : 2024-06-27
**/
public with sharing class HighRiskReviewCaseBatch implements Database.Batchable<SObject>, Schedulable {
    private final static String OANDA_CORPORATION = Constants.DIVISIONS_OANDA_CORPORATION;
    private final static String QUERY = 'SELECT Id FROM fxAccount__c WHERE High_Risk_account_review_due_date__c = TODAY AND Division_Name__c = :OANDA_CORPORATION';
    private final static Case CASE_TEMPLATE = new Case(
        Subject = Constants.CASE_SUBJECT_OC_HIGH_RISK_REVIEW_DUE,
        Status = Constants.CASE_STATUS_OPEN,
        Origin = Constants.CASE_ORIGIN_INTERNAL,
        Live_or_Practice__c = Constants.CASE_LIVE,
        Inquiry_Nature__c = Constants.CASE_INQUIRY_NATURE_ACCOUNT,
        Type__c = Constants.CASE_TYPE_GENERAL_ACCOUNT_QUESTIONS,
        Subtype__c = Constants.CASE_SUBTYPE_OTHER_UNKNOWN,
        OwnerId = UserUtil.getQueueId(Constants.QUEUE_NAME_OC_HIGHT_RISK_REVIEW_DUE),
        RecordTypeId = RecordTypeUtil.getSupportComplianceCaseTypeId()
    );

    // Batchable.start
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(QUERY);
    }

    // Batchable.execute
    public void execute(Database.BatchableContext bc, List<SObject> records) {
        
        List<Case> cases = new List<Case>();
        for (fxAccount__c loopFxAccount : (List<fxAccount__c>)records) {
            Case newCase = CASE_TEMPLATE.clone();
            newCase.fxAccount__c = loopFxAccount.Id;
            cases.add(newCase);
        }
        Database.insert(cases);
    }

    // Batchable.finish
    public void finish(Database.BatchableContext bc) {
    }

    // Schedulable.execute
    public void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }
}