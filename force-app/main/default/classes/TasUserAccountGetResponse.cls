/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-09-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasUserAccountGetResponse {
    public TasUserAccounts accounts {get; set;}

    public TasV20DefaultAccountConfig defaultAccountConfig {get; set;}

    public class TasUserAccounts {
        public List<TasV20AccsDetails> v20 {get; set;}
    }
    
    public class TasV20DefaultAccountConfig {
        public TasDefaultAccountConfig v20 {get; set;}
    }
}