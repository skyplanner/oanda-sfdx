@isTest
public class QualityIndexingLWCControllerTest {

    private static void createQIParams(){
        List<QI_Parameter__c> qiList = new List<QI_Parameter__c>();
        // CHAT
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_AVERAGE_SPEED_TO_ANSWER,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_ACCURACY,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_SELF_SERVICE_METHOD,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_TONE_MANNERISM,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_LITERACY_GRAMMAR,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_CASE_TAGGING,
            Deduction_Point__c = 'Chat',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 0.25,
            Tenure_0_5_year__c = 0.5,
            Tenure_greater_1_year__c = 0.75
        ));
        //Email
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_FIRST_RESPONSE_TIME,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_ACCURACY,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_SELF_SERVICE_METHOD,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_TONE_MANNERISM,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_LITERACY_GRAMMAR,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_CASE_TAGGING,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 1.25,
            Tenure_0_5_year__c = 1.5,
            Tenure_greater_1_year__c = 1.75
        ));
        //Call
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_AVERAGE_SPEED_TO_ANSWER,
            Deduction_Point__c = 'Call',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 2.25,
            Tenure_0_5_year__c = 2.5,
            Tenure_greater_1_year__c = 2.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_ACCURACY,
            Deduction_Point__c = 'Call',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 2.25,
            Tenure_0_5_year__c = 2.5,
            Tenure_greater_1_year__c = 2.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_TONE_MANNERISM,
            Deduction_Point__c = 'Call',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 2.25,
            Tenure_0_5_year__c = 2.5,
            Tenure_greater_1_year__c = 2.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_LITERACY_GRAMMAR,
            Deduction_Point__c = 'Call',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 2.25,
            Tenure_0_5_year__c = 2.5,
            Tenure_greater_1_year__c = 2.75
        ));
        qiList.add(new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_CASE_TAGGING,
            Deduction_Point__c = 'Call',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 2.25,
            Tenure_0_5_year__c = 2.5,
            Tenure_greater_1_year__c = 2.75
        ));
        insert qiList;
    }

    @testSetup static void testSetup() {
        createQIParams();
    }

    @isTest
    static void testMethod1(){

        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }

        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Chat',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);

            System.assertEquals(
                QualityIndexingLWCController.ORIGIN_TYPE_CHAT,
                iValues.caseType,
                'The case type does not match'
            );

            System.assertEquals(
                1, 
                iValues.speedToAnswerValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.speedToAnswerValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.accuracyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.accuracyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.toneValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.toneValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.taggingValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.taggingValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.selfServiceValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.selfServiceValues[0].value,
                'The speed to answer option is not correct');

            System.assert(
                iValues.firstResponseValues == null, 
                'The options are not empty');

            String errorMessage;
            try{
                iValues = QualityIndexingLWCController.getInitialValues(null);
            }
            catch(Exception ex){
                errorMessage = 'Error thrown';
            }

            System.assert(
                String.isNotBlank(errorMessage),
                'The errror was not fired');

            System.assertEquals(
                'Error thrown', errorMessage,
                'The error message is not correct');

        Test.stopTest();
    }

    @isTest
    static void testMethod2(){
        
        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }

        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Email',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);
            
            System.assertEquals(
                QualityIndexingLWCController.ORIGIN_TYPE_EMAIL,
                iValues.caseType,
                'The case type does not match'
            );

            String errorMessage;
            try{
                iValues = QualityIndexingLWCController.getInitialValues(null);
            }
            catch(Exception ex){
                errorMessage = 'Error thrown';
            }

            System.assert(
                String.isNotBlank(errorMessage),
                'The errror was not fired');
            System.assertEquals(
                'Error thrown', errorMessage,
                'The error message is not correct');

            System.assertEquals(
                1, 
                iValues.firstResponseValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.firstResponseValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.accuracyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.accuracyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.toneValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.toneValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.taggingValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.taggingValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.selfServiceValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.selfServiceValues[0].value,
                'The speed to answer option is not correct');

            System.assert(
                iValues.speedToAnswerValues == null, 
                'The options are not empty');                

        Test.stopTest();
    }

    @isTest
    static void testMethod3(){
        
        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }        
        
        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Phone',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);
            
            System.assertEquals(
                QualityIndexingLWCController.ORIGIN_TYPE_CALL,
                iValues.caseType,
                'The case type does not match'
            );

            String errorMessage;
            try{
                iValues = QualityIndexingLWCController.getInitialValues(null);
            }
            catch(Exception ex){
                errorMessage = 'Error thrown';
            }

            System.assert(
                String.isNotBlank(errorMessage),
                'The errror was not fired');
            System.assertEquals(
                'Error thrown', errorMessage,
                'The error message is not correct');

            System.assertEquals(
                1, 
                iValues.accuracyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '2.50', 
                iValues.accuracyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.toneValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '2.50', 
                iValues.toneValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '2.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.taggingValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '2.50', 
                iValues.taggingValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '2.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assert(
                iValues.firstResponseValues == null, 
                'The options are not empty');

            System.assert(
                iValues.selfServiceValues == null, 
                'The options are not empty');

        Test.stopTest();
    }

    @isTest
    static void testMethod4(){
        
        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }        

        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Chat',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);

            System.assertEquals(
                QualityIndexingLWCController.ORIGIN_TYPE_CHAT,
                iValues.caseType,
                'The case type does not match'
            );

            System.assertEquals(
                1, 
                iValues.speedToAnswerValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.speedToAnswerValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.accuracyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.accuracyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.toneValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.toneValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.taggingValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.taggingValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.selfServiceValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '0.50', 
                iValues.selfServiceValues[0].value,
                'The speed to answer option is not correct');

            System.assert(
                iValues.firstResponseValues == null, 
                'The options are not empty');
            
            QualityIndexingLWCController.FormWrapper fWrapper 
                = new QualityIndexingLWCController.FormWrapper();
            fWrapper.speedToAnswer = 0.25;
            fWrapper.accuracy = 0.25;
            fwrapper.tone = 0.25;
            fWrapper.literacy = 0.25;
            fWrapper.tagging = 0.25;
            fWrapper.selfService = 0.25;

            QualityIndexingLWCController.updateQI(c.Id, fWrapper);

            iValues = QualityIndexingLWCController.getInitialValues(c.Id);

            System.assertEquals(
                fWrapper.speedToAnswer, 
                iValues.speedToAnswer,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.accuracy, 
                iValues.accuracy,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.tone, 
                iValues.tone,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.literacy, 
                iValues.literacy,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.tagging, 
                iValues.tagging,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.selfService, 
                iValues.selfService,
                'The values don\'t match');

        Test.stopTest();
    }

    @isTest
    static void testMethod5(){

		User testU = new User(
	   		LastName='test user', 
	   		Username='sptestusername@spoanda.com', 
	   		Email='testuser@spoanda.com', 
	   		Alias='testuser', 
	   		ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
	   		TimeZoneSidKey='GMT', 
	   		LocaleSidKey='en_US', 
	   		EmailEncodingKey='ISO-8859-1', 
	   		LanguageLocaleKey='en_US',
	   		IsActive=true);
        insert testU;

        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Email',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;
        Datetime createdDate = System.now().addMonths(-7);
        Test.setCreatedDate(c.Id, createdDate);  

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);

            System.assertEquals(
                1, 
                iValues.firstResponseValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.firstResponseValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.accuracyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.accuracyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.toneValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.toneValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.literacyValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.literacyValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.taggingValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.taggingValues[0].value,
                'The speed to answer option is not correct');

            System.assertEquals(
                1, 
                iValues.selfServiceValues.size(), 
                'The amount of options must be 1 for the created dataset');
            System.assertEquals(
                '1.50', 
                iValues.selfServiceValues[0].value,
                'The speed to answer option is not correct');

            System.assert(
                iValues.speedToAnswerValues == null, 
                'The options are not empty');

            QualityIndexingLWCController.FormWrapper fWrapper 
                = new QualityIndexingLWCController.FormWrapper();
            fWrapper.firstResponse = 0.5;
            fWrapper.accuracy = 0.5;
            fwrapper.tone = 0.5;
            fWrapper.literacy = 0.5;
            fWrapper.tagging = 0.5;
            fWrapper.selfService = 0.5;

            QualityIndexingLWCController.updateQI(c.Id, fWrapper);

            iValues = QualityIndexingLWCController.getInitialValues(c.Id);            

            System.assertEquals(
                fWrapper.firstResponse, 
                iValues.firstResponse,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.accuracy, 
                iValues.accuracy,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.tone, 
                iValues.tone,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.literacy, 
                iValues.literacy,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.tagging, 
                iValues.tagging,
                'The values don\'t match');
            System.assertEquals(
                fWrapper.selfService, 
                iValues.selfService,
                'The values don\'t match');

        Test.stopTest();
    }

    @isTest
    static void testMethod6(){

        User testU;

        System.runAs(new User(id = UserInfo.getUserId())){

            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;

            testU = new User(
                LastName='test user', 
                Username='sptestusername@spoanda.com', 
                Email='testuser@spoanda.com', 
                Alias='testuser', 
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT', 
                LocaleSidKey='en_US', 
                EmailEncodingKey='ISO-8859-1', 
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                UserRoleId = r.Id,
                IsActive=true);
            insert testU;
        }

        Case c = new Case(
            Status = 'Open',
            Subject = 'test subject',
            Origin = 'Email',
            Inquiry_Nature__c = 'Other',
            Live_or_Practice__c = 'Practice',
            OwnerId = testU.Id
        );
        insert c;
        Test.setCreatedDate(c.Id, System.now().addMonths(-7));

        insert new QI_Parameter__c(
            Deduction_Parameter__c = QualityIndexingLWCController.PD_FIRST_RESPONSE_TIME,
            Deduction_Point__c = 'Email',
            Active_Inactive__c = 'Yes',
            Tenure_1_year__c = 3.25,
            Tenure_0_5_year__c = 3.5,
            Tenure_greater_1_year__c = 3.75
        );

        Test.startTest();

            QualityIndexingLWCController.InitialValuesWrapper iValues = 
                QualityIndexingLWCController.getInitialValues(c.Id);

            System.assertEquals(
                2, 
                iValues.firstResponseValues.size(), 
                'The amount of options must be 1 for the created dataset');

        Test.stopTest();
    }
}