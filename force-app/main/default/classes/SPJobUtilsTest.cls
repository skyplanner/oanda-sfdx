/**
 * @File Name          : SPJobUtilsTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2023, 2:02:12 AM
**/
@IsTest
private without sharing class SPJobUtilsTest {

	@TestSetup
	static void setup() {
		// Log__c is used by SPDoNothingBatch
		insert new Log__c(
			Id__c = 'abcd'
		);
	}

	@IsTest
	static void executeBatchIfNoTesting() {
		Test.startTest();
		Boolean result = SPJobUtils.executeBatchIfNoTesting(
			new SPDoNothingBatch(), // batch
			1 // scope
		);
		Test.stopTest();
		System.assertEquals(
			false, 
			result, 
			'In a test context the batch should not be executed'
		);
	}

	@IsTest
	static void executeBatchIfCondition() {
		Test.startTest();
		Boolean result = SPJobUtils.executeBatchIfCondition(
			new SPDoNothingBatch(), // batch
			1, // scope
			true // condition
		);
		Test.stopTest();
		System.assertEquals(
			true, 
			result, 
			'If the condition is met, the batch must be executed'
		);
	}

	@IsTest
	static void isJobRunning() {
		Test.startTest();
		Boolean result = SPJobUtils.isJobRunning('SPDoNothingJob');
		Test.stopTest();
		System.assertNotEquals(null, result, 'Invalid result');
	}

	@IsTest
	static void abortJob() {
		Test.startTest();
		Boolean result = SPJobUtils.abortJob('SPDoNothingJob');
		Test.stopTest();
		System.assertNotEquals(null, result, 'Invalid result');
	}

	@IsTest
	static void getLastJobExecutionInfo() {
		Test.startTest();
		Database.executeBatch(new SPDoNothingBatch(), 1);
		Test.stopTest();
		AsyncApexJob result1 = SPJobUtils.getLastJobExecutionInfo(
			SPDoNothingBatch.class.getName(), // className
			SPJobUtils.BATCH_APEX_JOB_TYPE  // jobType
		);
		AsyncApexJob result2 = SPJobUtils.getLastJobExecutionInfo(
			'SuperFakeJobName', // className
			SPJobUtils.QUEUEABLE_JOB_TYPE // jobType
		);
		System.assertNotEquals(null, result1, 'Invalid result');
		System.assertEquals(null, result2, 'Invalid result');
	}
	
}