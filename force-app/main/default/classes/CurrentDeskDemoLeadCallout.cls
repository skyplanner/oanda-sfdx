public with sharing class CurrentDeskDemoLeadCallout implements Queueable,Database.AllowsCallouts
{
    private CurrentDeskSync.DemoUser demoUser;
	
    public CurrentDeskDemoLeadCallout(CurrentDeskSync.DemoUser userInfo)
    {
        demoUser = userInfo;
    }
    
    public void execute(QueueableContext context)
    {
        Http http = new Http();
        HttpResponse response = http.send(getHttpRequest());
        handleHttpResponse(response);
    }

    private HttpRequest getHttpRequest() 
    { 
        HttpRequest request = new HttpRequest();
        request.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl + '/registration/lead');
        request.setMethod('POST');
        request.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        request.setHeader('Content-Type', 'application/json');
        request.setTimeout(120000);

        string requestBody = getRequestBody();
        request.setBody(requestBody); 

        return request;
    }
    private void handleHttpResponse(HttpResponse response) 
    { 
        if(response.getStatusCode() == 200 && response.getBody() != '')
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Integer cdLeadId = Integer.valueOf(results.get('LeadId'));
            
            Id fxAccountId = demoUser.fxAccountId;
            fxAccount__c fxa = [Select CD_Demo_Lead_Id__c From fxAccount__c Where Id=:fxAccountId Limit 1];
            fxa.CD_Demo_Lead_Id__c = cdLeadId;
            update fxa;
        }
        else   
        {
            // Build error info
            string subject = 'CurrentDesk Demo Registraion failed for user : ' + demoUser.fxAccountDetails.Name;
            string emailBody = 'Repsonse: \n ' + response.getBody();
            emailBody +=  '\n\nSalesforce link: \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + demoUser.fxAccountId;
            emailBody += '\n\nRequest Details: \n' +  getRequestBody();
            
            // Save error log
            Logger.error(
                subject,
                Constants.LOGGER_CURRENT_DESK_CATEGORY,
                emailBody);
            
            // Send email
            EmailUtil.sendEmail('salesforce@oanda.com', subject, emailBody);
        }
    }

	private string getRequestBody() { 
        Map<String, Object> regRequestFields = new Map<String, Object>();

        regRequestFields.put('lead-title-id', demoUser.titleId);
        regRequestFields.put('lead-first-name', demoUser.firstName);
        regRequestFields.put('lead-last-name', demoUser.lastName);
        regRequestFields.put('lead-email', demoUser.email);
        regRequestFields.put('lead-telephone', demoUser.telephone);
        regRequestFields.put('lead-residence-country-id', demoUser.residenceCountryId);
        regRequestFields.put('lead-language-id', demoUser.languageId);
        regRequestFields.put('lead-english-status', demoUser.accountEnglishStatus);
        regRequestFields.put('account-demo-status', demoUser.accountDemoStatus);
        regRequestFields.put('account-category-id', demoUser.accountCategoryId);
        regRequestFields.put('account-trading-platform-id', demoUser.accountTradingPlatformId);
        regRequestFields.put('trading-average-deal-size-id', demoUser.tradingAverageDealSizeId);
        regRequestFields.put('account-currency-id', demoUser.accountCurrencyId);
        regRequestFields.put('account-preference-id', demoUser.accountPreferenceId);
        regRequestFields.put('financial-initial-deposit-id', demoUser.initialDepositId);
        regRequestFields.put('trading-experience-id', demoUser.tradingExpereinceId); 

        if(string.isNotBlank(demoUser.proxyDomain))
            regRequestFields.put('proxy-domain', demoUser.proxyDomain);

		if (demoUser.reqCustomParameters != null &&
				!demoUser.reqCustomParameters.isEmpty())
			regRequestFields.putAll(demoUser.reqCustomParameters);

		return JSON.serializePretty(regRequestFields);
    }
}