/**
 * @File Name          : OmnichanelRoutingTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/26/2024, 9:23:47 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/26/2020   acantero     Initial Version
**/
@isTest
public class OmnichanelRoutingTestDataFactory {

    public static final String EMAIL_FRONTDESK_MIL_SETT_DEVNAME = 'Email_Case_Tier_4_Resolution_Time';

    public static final String HVC_EMAIL = 'hvc@a.com';
    public static final String NEW_CUSTOMER_EMAIL = 'newcustomer@a.com';
    public static final String ACTIVE_CUSTOMER_EMAIL = 'activecustomer@a.com';
    public static final String CUSTOMER_EMAIL = 'customer@a.com';

    public static final String LEAD_NEW_CUSTOMER_EMAIL = 'newcustomer@l.com';
    public static final String LEAD_CUSTOMER_EMAIL = 'customer@l.com';

    //Entitlements
    public static final String CHAT_SLA = 'chat';
    public static final String PHONE_SLA = 'phone';
    public static final String EMAIL_FRONTDESK_SLA = 'email frontdesk';
    public static final String EMAIL_API_SLA = 'email api';
    public static final String EMAIL_CHATBOT_SLA = 'email chatbot';

    public static final String WEB_SUPPORT = 'Web Support';
    public static final String PHONE_SUPPORT = 'Phone Support';

    public static void createEntitlementData() {
        List<SlaProcess> slaProcessList = [
            select Id, Name
            from SlaProcess 
            where IsActive = true
        ];
        Map<String,SlaProcess> slaProcessMap = new Map<String,SlaProcess>();
        for(SlaProcess slaProcessObj : slaProcessList) {
            slaProcessMap.put(slaProcessObj.Name.toUpperCase(), slaProcessObj);
        }
        ID chatEntProcessId = slaProcessMap.get(CHAT_SLA.toUpperCase()).Id;
        ID phoneEntProcessId = slaProcessMap.get(PHONE_SLA.toUpperCase()).Id;
        ID emailFrontdeskEntProcessId = slaProcessMap.get(EMAIL_FRONTDESK_SLA.toUpperCase()).Id;
        ID emailApiEntProcessId = slaProcessMap.get(EMAIL_API_SLA.toUpperCase()).Id;
        ID emailChatbotEntProcessId = slaProcessMap.get(EMAIL_CHATBOT_SLA.toUpperCase()).Id;
        //...
        String businessAccountRecTypeId = 
            Schema.SObjectType.Account
                .getRecordTypeInfosByDeveloperName()
                .get('Business_Account')
                .getRecordTypeId();
        Account entitlementsAccount = new Account( 
            Name = 'Entitlements Account', 
            RecordTypeId = businessAccountRecTypeId
        );
        insert entitlementsAccount;
        //...
        List<Entitlement> entitlementList = new List<Entitlement>();
        //...chat
        Entitlement chatEntitlementObj = new Entitlement(
            Name = CHAT_SLA,
            Type = WEB_SUPPORT,
            AccountId = entitlementsAccount.Id,
            SlaProcessId = chatEntProcessId,
            StartDate = System.today().addDays(-1)
        );
        entitlementList.add(chatEntitlementObj);
        //...phone
        Entitlement phoneEntitlementObj = new Entitlement(
            Name = PHONE_SLA,
            Type = PHONE_SUPPORT,
            AccountId = entitlementsAccount.Id,
            SlaProcessId = phoneEntProcessId,
            StartDate = System.today().addDays(-1)
        );
        entitlementList.add(phoneEntitlementObj);
        //...email frontdesk
        Entitlement emailFrontdeskEntitlementObj = new Entitlement(
            Name = EMAIL_FRONTDESK_SLA,
            Type = WEB_SUPPORT,
            AccountId = entitlementsAccount.Id,
            SlaProcessId = emailFrontdeskEntProcessId,
            StartDate = System.today().addDays(-1)
        );
        entitlementList.add(emailFrontdeskEntitlementObj);
        //...email api
        Entitlement emailApiEntitlementObj = new Entitlement(
            Name = EMAIL_API_SLA,
            Type = WEB_SUPPORT,
            AccountId = entitlementsAccount.Id,
            SlaProcessId = emailApiEntProcessId,
            StartDate = System.today().addDays(-1)
        );
        entitlementList.add(emailApiEntitlementObj);
        //...email chatbot
        Entitlement emailChatbotEntitlementObj = new Entitlement(
            Name = EMAIL_CHATBOT_SLA,
            Type = WEB_SUPPORT,
            AccountId = entitlementsAccount.Id,
            SlaProcessId = emailChatbotEntProcessId,
            StartDate = System.today().addDays(-1)
        );
        entitlementList.add(emailChatbotEntitlementObj);
        //...
        insert entitlementList;
    }

    public static void prepareEntitlementSettings() {
        Set<String> entitlementNameSet = new Set<String>
        {
            CHAT_SLA, 
            PHONE_SLA, 
            EMAIL_FRONTDESK_SLA,
            EMAIL_API_SLA,
            EMAIL_CHATBOT_SLA
        };
        List<Entitlement> entitlementList = [
            select Id, Name
            from Entitlement
            where Name in :entitlementNameSet
        ];
        Map<String,Entitlement> entitlementMap = new Map<String,Entitlement>();
        for(Entitlement entitlementObj : entitlementList) {
            entitlementMap.put(entitlementObj.Name, entitlementObj);
        }
        EntitlementSettings.chatEntitlementId = entitlementMap.get(CHAT_SLA).Id;
        EntitlementSettings.phoneEntitlementId = entitlementMap.get(PHONE_SLA).Id;
        EntitlementSettings.emailFrontdeskEntitlementId = entitlementMap.get(EMAIL_FRONTDESK_SLA).Id;
        EntitlementSettings.emailApiEntitlementId = entitlementMap.get(EMAIL_API_SLA).Id;
        EntitlementSettings.emailChatbotEntitlementId = entitlementMap.get(EMAIL_CHATBOT_SLA).Id;
    } 
    
    public static void createTestAccounts() {
        prepareTestAccounts(false);
    }

    public static void prepareTestAccounts(Boolean justHvc) {
        Account newCustomerAccount;
        Account activeCustomerAccount;
        Account customerAccount;
        List<Account> accountList = new List<Account>();
        //HVC
        Account hvcAccount = new Account( 
            FirstName = 'Jhon', 
            LastName = 'Canada', 
            PersonEmail = HVC_EMAIL, 
            Account_Status__c = 'Active',
            Language_Preference__pc = OmnichanelConst.ENGLISH_LANG,
            Date_Customer_Became_Core__c =  System.today().addDays(-2),
            Advanced_Trader_Program_Tier__pc = 'Tier 1'
        );
        accountList.add(hvcAccount);
        if (justHvc != true) {
            //New Customer
            newCustomerAccount = new Account( 
                FirstName = 'Jhon', 
                LastName = 'Auestralia', 
                PersonEmail = NEW_CUSTOMER_EMAIL, 
                Account_Status__c = 'Active',
                Language_Preference__pc = OmnichanelConst.CHINESE_SIMP_LANG
            );
            accountList.add(newCustomerAccount);
            //Active Customer
            activeCustomerAccount = new Account( 
                FirstName = 'Jhon', 
                LastName = 'Usa', 
                PersonEmail = ACTIVE_CUSTOMER_EMAIL, 
                Account_Status__c = 'Active',
                Language_Preference__pc = OmnichanelConst.SPANISH_LANG
            );
            accountList.add(activeCustomerAccount);
            //Customer
            customerAccount = new Account( 
                FirstName = 'Jhon', 
                LastName = 'Germany', 
                PersonEmail = CUSTOMER_EMAIL, 
                Account_Status__c = 'Active',
                Language_Preference__pc = OmnichanelConst.GERMAN_LANG
            );
            accountList.add(customerAccount);
        }
        insert accountList;
        if (customerAccount != null) {
            Test.setCreatedDate(customerAccount.Id, DateTime.now().addMonths(-7));
        }
        //create fxAccounts
        List<fxAccount__c> fxAccountList = new List<fxAccount__c>();
        String fxAccountRecTypeId = 
            Schema.SObjectType.fxAccount__c
                .getRecordTypeInfosByDeveloperName()
                .get('Retail_Live')
                .getRecordTypeId();
        //HVC
        fxAccount__c hvcFxAccount = new fxAccount__c(
            Name = hvcAccount.Name,
            //Account_Email__c = hvcAccount.PersonEmail,
            RecordTypeId = fxAccountRecTypeId,
            Account__c = hvcAccount.Id,
            Division_Name__c = OmnichanelConst.OANDA_CANADA
        );
        fxAccount__c newCustomerFxAccount;
        fxAccount__c activeCustomerFxAccount;
        fxAccount__c customerFxAccount;
        if (justHvc != true) {
            //New Customer
            newCustomerFxAccount = new fxAccount__c(
                Name = newCustomerAccount.Name,
                //Account_Email__c = newCustomerAccount.PersonEmail,
                Funnel_Stage__c = 'Legal',
                RecordTypeId = fxAccountRecTypeId,
                Account__c = newCustomerAccount.Id,
                Division_Name__c = OmnichanelConst.OANDA_AUSTRALIA
            );
            fxAccountList.add(newCustomerFxAccount);
            //Active Customer
            activeCustomerFxAccount = new fxAccount__c(
                Name = activeCustomerAccount.Name,
                //Account_Email__c = activeCustomerAccount.PersonEmail,
                Funnel_Stage__c = 'Ready For Funding',
                RecordTypeId = fxAccountRecTypeId,
                Account__c = activeCustomerAccount.Id,
                Division_Name__c = OmnichanelConst.OANDA_CORPORATION,
                Last_Trade_Date__c = DateTime.now()
            );
            fxAccountList.add(activeCustomerFxAccount);
            //Customer
            customerFxAccount = new fxAccount__c(
                Name = customerAccount.Name,
                //Account_Email__c = customerAccount.PersonEmail,
                Funnel_Stage__c = 'Legal',
                RecordTypeId = fxAccountRecTypeId,
                Account__c = customerAccount.Id,
                Division_Name__c = OmnichanelConst.OANDA_EUROPE
            );
            fxAccountList.add(customerFxAccount);
        }
        insert fxAccountList;
        //update accounts
        Set<Id> accountIdSet = new Set<Id> {
            hvcAccount.Id
        };
        if (justHvc != true) {
            accountIdSet.add(newCustomerAccount.Id);
            accountIdSet.add(activeCustomerAccount.Id);
            accountIdSet.add(customerAccount.Id);
        }
        Map<ID,Account> accMap = new Map<ID,Account> (
            [select Id, fxAccount__c from Account where Id IN :accountIdSet]
        );
        hvcAccount = accMap.get(hvcAccount.Id);
        hvcAccount.fxAccount__c = hvcFxAccount.Id;
        //...
        if (justHvc != true) {
            newCustomerAccount = accMap.get(newCustomerAccount.Id);
            newCustomerAccount.fxAccount__c = newCustomerFxAccount.Id;
            //...
            activeCustomerAccount = accMap.get(activeCustomerAccount.Id);
            activeCustomerAccount.fxAccount__c = activeCustomerFxAccount.Id;
            //...
            customerAccount = accMap.get(customerAccount.Id);
            customerAccount.fxAccount__c = customerFxAccount.Id;
            //
        }
        List<Account> accToUpd = new List<Account> {
            hvcAccount
        };
        if (justHvc != true) {
            accToUpd.add(newCustomerAccount);
            accToUpd.add(activeCustomerAccount);
            accToUpd.add(customerAccount);
        }
        update accToUpd;
    }

    public static ID createHvcAccount(String division) {
        // List<String> validDivisions = new List<String>
        // {
        //     'OC',
        //     'OANDA Corporation',
        //     'OCAN',
        //     'OANDA Canada'
        // };
        //  System.assert(
        //     validDivisions.contains(division), 
        //     'Invalid division name for HVC Account: ' + division
        // );
        Account hvcAccount = new Account( 
            FirstName = 'Jhon', 
            LastName = 'Canada', 
            PersonEmail = HVC_EMAIL, 
            //Account_Status__c = 'Active',
            Language_Preference__pc = OmnichanelConst.ENGLISH_LANG,
            Date_Customer_Became_Core__c =  System.today().addDays(-2),
            Advanced_Trader_Program_Tier__pc = 'Tier 1'
        );
        insert hvcAccount;
        String fxAccountRecTypeId = 
            Schema.SObjectType.fxAccount__c
                .getRecordTypeInfosByDeveloperName()
                .get('Retail_Live')
                .getRecordTypeId();
        fxAccount__c hvcFxAccount = new fxAccount__c(
            Name = hvcAccount.Name,
            //Account_Email__c = hvcAccount.PersonEmail,
            RecordTypeId = fxAccountRecTypeId,
            Account__c = hvcAccount.Id,
            Division_Name__c = division,
            Type__c = 'Individual'
        );
        insert hvcFxAccount;
        Account accObj = [select Id, fxAccount__c from Account where Id = :hvcAccount.Id];
        accObj.fxAccount__c = hvcFxAccount.Id; 
        update accObj;
        return accObj.Id;
    }

    public static ID getHvcAccountId() {
        ID result = [select Id from Account where PersonEmail = :HVC_EMAIL limit 1].Id;
        return result;
    }

    public static Lead getNewCustomerLead() {
        //New Customer
        Lead newCustomerLead = new Lead( 
            FirstName = 'Tom', 
            LastName = 'Corea', 
            Email = LEAD_NEW_CUSTOMER_EMAIL, 
            Language_Preference__c = OmnichanelConst.RUSSIAN_LANG,
            Division_Name__c = OmnichanelConst.OANDA_ASIA_PACIFIC
        );
        return newCustomerLead;
    }

    public static ID createNewCustomerLead() {
        Lead newCustomerLead = getNewCustomerLead();
        insert newCustomerLead;
        return newCustomerLead.Id;
    }

    public static void createTestLeads() {
        List<Lead> leadList = new List<Lead>();
        leadList.add(getNewCustomerLead());
        //Customer
        Lead customerLead = new Lead( 
            FirstName = 'Tom', 
            LastName = 'Germany', 
            Email = LEAD_CUSTOMER_EMAIL, 
            Language_Preference__c = OmnichanelConst.ENGLISH_LANG,
            Division_Name__c = OmnichanelConst.OANDA_EUROPE
        );
        leadList.add(customerLead);
        insert leadList;
        Test.setCreatedDate(customerLead.Id, DateTime.now().addMonths(-7));
    }

    public static Map<String, ID> getAccountMap() {
        Map<String, ID> result = new Map<String, ID>();
        List<Account> accountList = [
            SELECT
                Id,
                PersonEmail
            FROM
                Account
        ];
        for(Account accountObj : accountList) {
            result.put(accountObj.PersonEmail, accountObj.Id);
        }
        return result;
    }

    public static Map<String, ID> getLeadMap() {
        Map<String, ID> result = new Map<String, ID>();
        List<Lead> leadList = [
            SELECT
                Id,
                Email
            FROM
                Lead
        ];
        for(Lead leadObj : leadList) {
            result.put(leadObj.Email, leadObj.Id);
        }
        return result;
    }

    public static List<Case> getNewTestCases(
        Boolean creteLeadCases, 
        Boolean justForHvcAccount
    ) {
        Map<String, ID> accountMap = getAccountMap();
        List<Case> caseList = new List<Case>();
        //HVC
        ID hvcId = accountMap.get(HVC_EMAIL);
        caseList.add(new Case(
            AccountId = hvcId,
            Subject = 'HVC Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            Tier__c = OmnichannelCustomerConst.TIER_1,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        ));
        if (justForHvcAccount != true) {
            //New Customer
            ID newCustomerAccountId = accountMap.get(NEW_CUSTOMER_EMAIL);
            caseList.add(new Case(
                AccountId = newCustomerAccountId,
                Subject = 'New Customer Case',
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_LOGIN
            ));
            //Active Customer
            ID activeCustomerAccountId = accountMap.get(ACTIVE_CUSTOMER_EMAIL);
            caseList.add(new Case(
                AccountId = activeCustomerAccountId,
                Subject = 'Active Customer Case',
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY
            ));
            //Customer
            ID customerAccountId = accountMap.get(CUSTOMER_EMAIL);
            caseList.add(new Case(
                AccountId = customerAccountId,
                Subject = 'Customer Case',
                Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER
            ));
        }
        if (creteLeadCases == true) {
            caseList.addAll(
                getNewTestCasesForLeads()
            );
        }
        return caseList;
    }

    public static List<Case> getNewTestCasesForLeads() {
        List<Case> result = new List<Case>();
        Map<String, ID> leadMap = getLeadMap();
        //New Customer (Lead)
        ID newCustomerLeadId = leadMap.get(LEAD_NEW_CUSTOMER_EMAIL);
        result.add(new Case(
            Lead__c = newCustomerLeadId,
            Subject = 'New Customer Case (Lead)'
        ));
        //Customer (Lead)
        ID customerLeadId = leadMap.get(LEAD_CUSTOMER_EMAIL);
        result.add(new Case(
            Lead__c = customerLeadId,
            Subject = 'Customer Case (Lead)'
        ));
        return result;
    }

    public static List<Case> createHvcAccountTestCase() {
        List<Case> caseList = getNewTestCases(false, true);
        insert caseList;
        return caseList;
    }

    public static List<Case> createTestCases() {
        List<Case> caseList = getNewTestCases(true, false);
        insert caseList;
        return caseList;
    }

    public static List<Case> createTestCasesForLeads() {
        List<Case> caseList = getNewTestCasesForLeads();
        insert caseList;
        return caseList;
    }

    public static Set<ID> getCreatedTestCasesIds() {
        Map<ID,Case> caseMap = new Map<ID,Case>(createTestCases());
        return caseMap.keySet();
    }

    public static List<PendingServiceRouting> getNewPendingServiceRList(ID channelId, Set<ID> workItemIdSet) {
        List<PendingServiceRouting> result = new List<PendingServiceRouting>();
        QueueRoutingConfig emailCasesRoutingConfig = 
            getRequiredRoutingConfig(OmnichanelConst.EMAIL_CASES_ROUTING_CONFIG);

        for(ID workItemId : workItemIdSet) {
            result.add(getNewPendingServiceR(channelId, emailCasesRoutingConfig, workItemId));
        }
        return result;
    }

    public static PendingServiceRouting getNewPendingServiceR(ID channelId, QueueRoutingConfig routingConfig, ID workItemId) {
        return new PendingServiceRouting(
            CapacityWeight = routingConfig.CapacityWeight,
            IsReadyForRouting = false,
            RoutingModel  = routingConfig.RoutingModel,
            //RoutingPriority = emailCasesRoutingConfig.RoutingPriority,
            RoutingType = OmnichanelConst.SKILLS_BASED_ROUTING_TYPE,
            ServiceChannelId = channelId,
            WorkItemId = workItemId
            //PushTimeout = 0
        );
    }

    public static QueueRoutingConfig getRequiredRoutingConfig(String devName) {
        return [
            SELECT
                RoutingModel,
                RoutingPriority,
                CapacityWeight
            FROM
                QueueRoutingConfig
            WHERE
                DeveloperName = :devName
        ];
    }

    public static ID getNonHvcCaseChannelId() {
        return getRequiredChannelId(
            Schema.Non_HVC_Case__c.SObjectType.getDescribe().getName()
        );
    }

    public static ID getCaseChannelId() {
        return getRequiredChannelId(
            Schema.Case.SObjectType.getDescribe().getName()
        );
    }

    public static ID getChatChannelId() {
        return getRequiredChannelId(
            Schema.LiveChatTranscript.SObjectType.getDescribe().getName()
        );
    }

    public static ID getRequiredChannelId(String relatedEntity) {
        return [
            SELECT
                Id 
            FROM
                ServiceChannel 
            WHERE
                RelatedEntity = :relatedEntity
            LIMIT
                1
        ].Id;
    }

    public static String getChatKey(String letter) {
        String result = '';
        for(Integer i = 0; i < 200; i++) {
            result += letter;
        }
        return result;
    }

    public static List<LiveChatTranscript> getNewChats(
        String division,
        String inquiryNature
    ) {
        createHvcAccount(division);
        return getNewChatsForHVCAccount(
            division,
            inquiryNature
        );
    }

    public static List<LiveChatTranscript> getNewChatsForHVCAccount(
        String division,
        String inquiryNature
    ) {
        ServiceDivisionsManager divisionsManager = ServiceDivisionsManager.getInstance();
        ID chatChannelId = getChatChannelId();
        List<LiveChatVisitor> chatVisitorList = new List<LiveChatVisitor>();
        LiveChatVisitor chatVisitor1 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor1);
        LiveChatVisitor chatVisitor2 = new LiveChatVisitor(
        );
        chatVisitorList.add(chatVisitor2);
        insert chatVisitorList;
        List<LiveChatTranscript> chatList = new List<LiveChatTranscript>();
        LiveChatTranscript chat1 = new LiveChatTranscript(
            ChatKey = OmnichanelRoutingTestDataFactory.getChatKey('A'),
            Chat_Email__c = OmnichanelRoutingTestDataFactory.HVC_EMAIL,
            Chat_Type_of_Account__c = OmnichanelConst.PRACTICE,
            Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
            Inquiry_Nature__c = inquiryNature,
            Type__c = null,
            Subtype__c = null,
            LiveChatVisitorId = chatVisitor1.Id
        );
        chatList.add(chat1);
        LiveChatTranscript chat2 = new LiveChatTranscript(
            ChatKey = OmnichanelRoutingTestDataFactory.getChatKey('B'),
            Chat_Email__c = OmnichanelRoutingTestDataFactory.HVC_EMAIL,
            Chat_Division__c = divisionsManager.getDivisionCode(division),
            Is_HVC__c = OmnichanelConst.CHAT_IS_HVC_YES,
            Chat_Type_of_Account__c = OmnichanelConst.PRACTICE,
            Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
            Inquiry_Nature__c = inquiryNature,
            Type__c = null,
            Subtype__c = null,
            LiveChatVisitorId = chatVisitor2.Id
        );
        chatList.add(chat2);
        return chatList;
    }

    public static User createChatSupervisor(String userKey) {
        OmnichanelSettings omniSettings = OmnichanelSettings.getRequiredDefault();
        ID profileId = [select Id from Profile where Name = 'System Administrator'][0].Id;
        ID roleId = [select Id from UserRole where DeveloperName in :omniSettings.supervisorRoleDevNameList][0].Id;
        String lastName = 'skpchatsupervisor' + userKey;
        User usrObj = new User(
            LastName = lastName, 
            ProfileId = profileId, 
            UserRoleId = roleId,
            Alias = userKey, 
            LanguageLocaleKey = 'en_US', 
            Email = lastName + '@oanda.com', 
            EmailEncodingKey='UTF-8', 
            isActive = true, 
            LocaleSIDKey='en_US', 
            TimeZoneSidKey = 'America/New_York', 
            Username = lastName + '@oanda.com'
        );
        return usrObj;
    }

    public static ServiceResource createAgentServiceResWithSkill(ID userId, String resName, String skillDevName) {
        ServiceResource res = new ServiceResource(
            Name = resName,
            ResourceType = OmnichanelConst.SERVICE_RES_AGENT,
            IsActive = true,
            RelatedRecordId = userId
        );
        insert res;
        //...
        ID skillId = [select Id from Skill where DeveloperName = :skillDevName][0].Id;
        ServiceResourceSkill resSkil = new ServiceResourceSkill(
            ServiceResourceId = res.Id,
            SkillId = skillId,
            EffectiveStartDate = System.today().addDays(-1)
        );
        insert resSkil;
        return res;
    }

    public static String getMilestoneName(String milSettingDevName) {
        String result = [
            SELECT
                Milestone_Type_Name__c
            FROM
                Milestone_Time_Settings__mdt
            WHERE
                DeveloperName = :milSettingDevName
        ][0].Milestone_Type_Name__c;
        return result;
    }

    public static String getEmailFrontdeskMilestoneName() {
        return getMilestoneName(EMAIL_FRONTDESK_MIL_SETT_DEVNAME);
    }

    public static ID getRequiredSkillsQueueId() {
        ID result = 
            [SELECT Id FROM Group WHERE DeveloperName = :OmnichanelConst.SKILL_CASES_QUEUE_DEVNAME][0].Id;
        return result;
    }

    public static void setupAllForNonHvcEmailFrontdeskCases(
        List<Case> caseList,
        Boolean setupLanguagesFields
    ) {
        setupAllForEmailFrontdeskCases(
            null, // hvcCaseList
            caseList, // nonHvcCaseList
            setupLanguagesFields
        );
    }

    public static void setupAllForHvcEmailFrontdeskCases(
        List<Case> caseList,
        Boolean setupLanguagesFields
    ) {
        setupAllForEmailFrontdeskCases(
            caseList, // hvcCaseList
            null, // nonHvcCaseList
            setupLanguagesFields
        );
    }

    public static void setupAllForEmailFrontdeskCases(
        List<Case> hvcCaseList,
        List<Case> nonHvcCaseList,
        Boolean setupLanguagesFields
    ) {
        List<Case> caseList = new List<Case>();
        String defaultLanguage = 
            ServiceLanguagesManager.getLanguage(ChatConst.DEFAULT_LANGUAGE);
        if (hvcCaseList != null) {
            ID hvcAccountId = 
                OmnichanelRoutingTestDataFactory.createHvcAccount(OmnichanelConst.OANDA_CORPORATION);
            for(Case hvcCaseObj : hvcCaseList) {
                hvcCaseObj.AccountId = hvcAccountId;
            }
            caseList.addAll(hvcCaseList);
        }
        if (nonHvcCaseList != null) {
            ID newCustomerLeadId = OmnichanelRoutingTestDataFactory.createNewCustomerLead();
            for(Case nonHvcCaseObj : nonHvcCaseList) {
                nonHvcCaseObj.Lead__c = newCustomerLeadId;
            }
            caseList.addAll(nonHvcCaseList);
        }
        setupAllForEmailFrontdeskCases(caseList, setupLanguagesFields);
    }

    public static void setupAllForEmailFrontdeskCases(
        List<Case> caseList,
        Boolean setupLanguagesFields
    ) {
        String defaultLanguage = 
            ServiceLanguagesManager.getLanguage(ChatConst.DEFAULT_LANGUAGE);
        String defaultLanguageCode = 
            ServiceLanguagesManager.getLanguageCode(ChatConst.DEFAULT_LANGUAGE);
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        for(Case caseObj : caseList) {
            caseObj.Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER;
            caseObj.Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK;
            if (setupLanguagesFields == true) {
                caseObj.Language = defaultLanguageCode;
                caseObj.Language_Corrected__c = defaultLanguage;
            }
        }
        setupAllForTestCases(caseList);
    }
    public static void setupAllForChatbotEmailCases(
        List<Case> caseList,
        Boolean setupLanguagesFields
    ) {
        String defaultLanguage = 
            ServiceLanguagesManager.getLanguage(ChatConst.DEFAULT_LANGUAGE);
        String defaultLanguageCode = 
            ServiceLanguagesManager.getLanguageCode(ChatConst.DEFAULT_LANGUAGE);
        for(Case caseObj : caseList) {
            caseObj.Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER;
            if (setupLanguagesFields == true) {
                caseObj.Language = defaultLanguageCode;
                caseObj.Language_Corrected__c = defaultLanguage;
            }
        }
        setupAllForTestCases(caseList);
    }

    public static void setupAllForTestCases(
        List<Case> caseList
    ) {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        //...
        insertTestCasesByPassingTriggers(caseList);
    }

    public static List<Case> insertTestCasesBypassingTriggers(
        List<Case> caseList
    ){
        //disable trigger to avoid automatic routing
        SPCaseTriggerHandler.enabled = false;
        //...
        insert caseList;
        //enable trigger again
        SPCaseTriggerHandler.enabled = true;
        return caseList;
    }

}