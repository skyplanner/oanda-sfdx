/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 01-18-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiSearchResponse {
    public Integer live_count {get; set;}

    public Integer demo_count {get; set;}
    
    public List<UserApiSearchItem> demo {get; set;}
    
    public List<UserApiSearchItem> live {
        get {
            if (live != null)
                for (UserApiSearchItem u: live)
                    u.isLive = true;

            return live;
        } set;
    }

    public transient Integer pageCount {
        get {
            return Math.max(demo_count, live_count);
        }
    }

    public transient Boolean containsDemoUsers {
        get {
            return demo != null && !demo.isEmpty();
        }
    }

    public transient Boolean containsLiveUsers {
        get {
            return live != null && !live.isEmpty();
        }
    }

    public List<Map<String, Object>> toMapList() {
        List<Map<String, Object>> result = new List<Map<String, Object>>();

        if (containsDemoUsers)
            for (UserApiSearchItem u : demo)
                result.add(u.toMap());

        if (containsLiveUsers)
            for (UserApiSearchItem u : live)
                result.add(u.toMap());

        return result;
    }
}