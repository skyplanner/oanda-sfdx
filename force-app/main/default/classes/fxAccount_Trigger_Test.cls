/**
 * @File Name          : fxAccount_Trigger_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/26/2020   acantero     Initial Version
**/
@isTest
private class fxAccount_Trigger_Test {

    @isTest
    static void beforeDelete_test(){
        fxAccount__c validatedFxAccount = MIFIDValidationTestDataFactory.createValidatedFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568'
        );
        List<MiFID_Validation_Result__c>  validResultBefore = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        Test.startTest();
          delete validatedFxAccount;
        Test.stopTest();
        List<MiFID_Validation_Result__c> validResultAfter = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        System.assertEquals(1 , validResultBefore.size());
        System.assertEquals(0 , validResultAfter.size());
    }

    @isTest
    static void afterUpdateTradeDate(){
        fxAccount__c validatedFxAccount = MIFIDValidationTestDataFactory.createValidatedFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568'
        );
        List<MiFID_Validation_Result__c>  validResultBefore = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        validatedFxAccount.Last_Trade_Date__c = System.now().addSeconds(2);
        Test.startTest();
          update validatedFxAccount;
        Test.stopTest();
        List<MiFID_Validation_Result__c> validResultAfter = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        System.assertEquals(1 , validResultBefore.size());
        System.assertEquals(1 , validResultAfter.size());
    }

    @isTest
    static void afterUpdatePersonalInfo(){
        fxAccount__c validatedFxAccount = MIFIDValidationTestDataFactory.createValidatedFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568'
        );
        List<MiFID_Validation_Result__c>  validResultBefore = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        validatedFxAccount.Passport_Number__c = '123456';
        Test.startTest();
          update validatedFxAccount;
        Test.stopTest();
        List<MiFID_Validation_Result__c> validResultAfter = [select Id, Valid__c from MiFID_Validation_Result__c where fxAccount__c = :validatedFxAccount.Id];
        System.assertEquals(1 , validResultBefore.size());
        System.assertEquals(1 , validResultAfter.size());
        System.assertEquals(true , validResultBefore[0].Valid__c);
        System.assertEquals(false , validResultAfter[0].Valid__c);
    }
    
    
}