/* Name: BatchReassignAccountsSchedulable
 * Description : Apex Class to Reassign the accounts to the Queue members as mentioned SP-8028 SP-8035 , SP-8196.
 *              using Round Robin way to the respective queues 
 * Author: Sri 
 * Date : 2020-05-28
 */
public class BatchReassignAccountsSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable 
{
    
    private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public static final Integer BATCH_SIZE = 200;
    
    public BatchReassignAccountsSchedulable() 
    {
     /* //   execute below for scheduling the AMR transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable('SELECT Id, OwnerId,AMR_Transfer_Criteria__c FROM Account WHERE AMR_Transfer_Criteria__c = true') ;
            System.schedule('BatchReassignAccountsSchedulable-AMR_Transfer', '0 30 22 * * ?', b);
        //   execute below for scheduling the OEL transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable('SELECT Id, OwnerId,OEL_Transfer_Criteria__c FROM Account WHERE OEL_Transfer_Criteria__c = true') ;
            System.schedule('BatchReassignAccountsSchedulable-OEL_Transfer', '0 20 22 * * ?', b);
        //   execute below for scheduling the singapore  transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable('SELECT Id, OwnerId, Singapore_RM_Transfer_Criteria__c FROM Account WHERE Singapore_RM_Transfer_Criteria__c = true') ;
            System.schedule('BatchReassignAccountsSchedulable-Singapore_Transfer', '0 10 22 * * ?', b);
        //   exceute below for scheduling the singapore referral RM transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable('SELECT Id, OwnerId, Singapore_RM_Referral_Criteria__c,FXS_RM_ID__c FROM Account WHERE Singapore_RM_Referral_Criteria__c = true') ;
            System.schedule('BatchReassignAccountsSchedulable-Singapore_RefTransfer', '0 0 20 * * ?', b);
        //   exceute below for scheduling the singapore OAP sales transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable(
                'SELECT OwnerId, Singapore_OAP_Sales_Transfer_Criteria__c FROM Account WHERE Singapore_OAP_Sales_Transfer_Criteria__c = true');
            System.schedule('BatchReassignAccountsSchedulable-Singapore_OAPSalesTransfer', '0 0 23 * * ?', b);
        //   exceute below for scheduling the OGM transfer
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable(
                'SELECT OwnerId, OGM_Transfer_Criteria__c FROM Account WHERE OGM_Transfer_Criteria__c = true');
            System.schedule('BatchReassignAccountsSchedulable-OGM_Transfer', '0 10 23 * * ?', b);
        //   exceute below for scheduling the OGM South African transfer 
            BatchReassignAccountsSchedulable b = new BatchReassignAccountsSchedulable(
                'SELECT OwnerId, OGM_South_African_Transfer_Criteria__c FROM Account WHERE OGM_South_African_Transfer_Criteria__c = true');
            System.schedule('BatchReassignAccountsSchedulable-OGM_SouthAfrican_Transfer', '0 15 23 * * ?', b);
        */
    }

    public BatchReassignAccountsSchedulable(String q) 
    {
        query = q;
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT OwnerId, Singapore_OAP_Sales_Transfer_Criteria__c, OGM_Transfer_Criteria__c, OGM_South_African_Transfer_Criteria__c FROM Account WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> batch) 
    {
        System.debug('batch ===> ' + batch);
        List<Account> accs = (List<Account>) batch;
        Map<Id, Id> fxMap = getFxMapByAcc(batch);
        Id transferQueueId ;
        List<Id> currentQueueUsers = new List<Id>();        
        List<fxAccount__c> updatefxList = new List<fxAccount__c>();        
        Map<String, Object> fieldsToValue ;
        if(accs.size()>0)
        {
            fieldsToValue= accs.get(0).getPopulatedFieldsAsMap();
            if(fieldsToValue.keySet().contains('AMR_Transfer_Criteria__c') && accs.get(0).AMR_Transfer_Criteria__c == true)
            {
                transferQueueId = UserUtil.getQueueId('Retail - Americas Accounts');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(UserUtil.getQueueId('Retail - Americas Accounts')));
            }
            else if(fieldsToValue.keySet().contains('OEL_Transfer_Criteria__c') && accs.get(0).OEL_Transfer_Criteria__c == true)
            {
                transferQueueId = UserUtil.getQueueId('OEL Premium Accounts');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(UserUtil.getQueueId('Retail - Americas Accounts')));
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(UserUtil.getQueueId('OEL Premium Accounts')));
            }
            else if(fieldsToValue.keySet().contains('Singapore_RM_Transfer_Criteria__c') && accs.get(0).Singapore_RM_Transfer_Criteria__c == true)
            {
                transferQueueId = UserUtil.getQueueId('Singapore RMs for Account Transfer');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(UserUtil.getQueueId('Singapore RMs for Account Transfer')));
            }
            else if(fieldsToValue.keySet().contains('Singapore_RM_Referral_Criteria__c') && accs.get(0).Singapore_RM_Referral_Criteria__c == true)
            {
                transferQueueId = null; // this is null because we have to transfer the account to FXS_RM_ID__c value within that account but not a queue
            }
            else if(fieldsToValue.keySet().contains('ROA_RM_Transfer_Criteria__c') && accs.get(0).ROA_RM_Transfer_Criteria__c == true)//SP-9064
            {
                transferQueueId = UserUtil.getQueueId('ROA RMs for Account Transfer');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(transferQueueId));
            } 
            else if (fieldsToValue.keySet().contains('Singapore_OAP_Sales_Transfer_Criteria__c') &&
                accs.get(0).Singapore_OAP_Sales_Transfer_Criteria__c)
            {
                transferQueueId = UserUtil.getQueueId('Singapore OAP for Account Transfer');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(transferQueueId));
            }
            else if (fieldsToValue.keySet().contains('OGM_Transfer_Criteria__c') &&
                accs.get(0).OGM_Transfer_Criteria__c)
            {
                transferQueueId = UserUtil.getQueueId('OGM for Account Transfer');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(transferQueueId));
            }
            else if (fieldsToValue.keySet().contains('OGM_South_African_Transfer_Criteria__c') &&
                accs.get(0).OGM_South_African_Transfer_Criteria__c)
            {
                transferQueueId = UserUtil.getQueueId('OGM South African');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(transferQueueId));
            }
            else if(fieldsToValue.keySet().contains('OGM_Transfer_Criteria_HVC__c') && accs.get(0).OGM_Transfer_Criteria_HVC__c == true)
            {
                transferQueueId = UserUtil.getQueueId('OGM Premium Accounts');
                currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(UserUtil.getQueueId('OGM Premium Accounts')));
            }
            else
            {
                return;
            }
        }
        
        List<Account> accountsToReassign = new List<Account>();
        for(Account var: accs)
        {
            Boolean ownerUpdate = false;
            if(fieldsToValue.keySet().contains('Singapore_RM_Referral_Criteria__c') && var.Singapore_RM_Referral_Criteria__c == true)
            {
                System.debug('First if ===>');
                var.ownerid = var.FXS_RM_ID__c;
                if(var.OwnerId != null)
                {
                    accountsToReassign.add(var);
                    ownerUpdate = true;
                }
            }
            else if(currentQueueUsers.contains(var.OwnerId) == false && transferQueueId != null)
            {
                System.debug('Second if ===> ');
                var.OwnerId = RoundRobinAssignment.getNextOwnerId(transferQueueId);
                if(var.OwnerId != null)
                {
                    accountsToReassign.add(var);
                    ownerUpdate = true;
                }
            }
            
            if (ownerUpdate) {
                Id fxAccId = fxMap.get(var.Id);
                List<CXNoteListController.CXNote> notes = CXNoteListController.getCXNotes(fxAccId);
				CXNoteListController.CXNote note = new CXNoteListController.CXNote();
				note.timestamp = (Datetime.now().getTime());
				note.comment = System.Label.Batch_Reassign_Account_Sched;
				note.author = UserInfo.getName();
				notes.add(note);
				fxAccount__c fxa = new fxAccount__c(
					Id = fxAccId,
					Notes__c = JSON.serialize(notes)
				);
				updatefxList.add(fxa);
            }
        }
        if(accountsToReassign.size() > 0)
        {
            try
            {
                update accountsToReassign;
                RoundRobinAssignment.commitAssignments(); 
            }
            catch(Exception e)
            {   
                System.debug('Err'+e.getMessage());
            }
        }
        if(updatefxList.size() > 0)
        {
            try
            {
                update updatefxList;
            }
            catch(Exception e)
            {   
                System.debug('Err'+e.getMessage());
            }
        }
    }
    
    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchReassignAccountsSchedulable(query), BATCH_SIZE);
    }

    private Map<Id, Id> getFxMapByAcc(List<Account> batch) {
        Map<Id, Id> fxMap = new Map<Id, Id>();
        Set<Id> accIds = (new Map<Id,Account>(batch)).keySet();

        for (Account acc : [
            SELECT Id, fxAccount__c
            FROM Account
            WHERE Id IN :accIds
        ]) {
            fxMap.put(acc.Id, acc.fxAccount__c);
        }

        return fxMap;
    }

    public void finish(Database.BatchableContext BC) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
}