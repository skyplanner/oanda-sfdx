/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-25-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoAccountTasActionsCallout extends TasCallout {
    public CryptoAccountTasActionsCallout() {
        super();
    }

    public Boolean patchMethod(String resource, Map<String, String> headers, Map<String, String> params, String body) {
		patch(resource, header, params, body);

        return isResponseCodeSuccess();
	}
}