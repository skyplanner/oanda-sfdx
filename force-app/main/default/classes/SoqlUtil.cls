public with sharing class SoqlUtil {
	public static String getSoqlList(Set<String> strings) {
		String result = '';
		for(String s : strings) {
			result += '\'' + String.escapeSingleQuotes(s) + '\',';
		}
		if (result.length()>0) {
        	result = result.substring(0,result.lastIndexOf(','));
    	}
    	return '(' + result + ')';
	}
	
	public static String getSoqlList(Set<Id> strings) {
		String result = '';
		for(String s : strings) {
			result += '\'' + s + '\',';
		}
		if (result.length()>0) {
        	result = result.substring(0,result.lastIndexOf(','));
    	}
    	return '(' + result + ')';
	}
	public static String getSoqlMergeLeadRecordTypeIds() {
		return ' AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN ' + SoqlUtil.getSoqlList(RecordTypeUtil.NAMES_LEAD_RECORDTYPE_FOR_MERGE) + ' AND SobjectType=\'Lead\')';
	}

	public static String getSoqlMergeLeadRecordTypeIdsWithOTMS() {
		Set<String> recordTypeNameList = new Set<String>(RecordTypeUtil.NAMES_LEAD_RECORDTYPE_FOR_MERGE);
		recordTypeNameList.add(RecordTypeUtil.NAME_LEAD_RECORDTYPE_OTMS);
		return ' AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN ' + SoqlUtil.getSoqlList(recordTypeNameList) + ' AND SobjectType=\'Lead\')';
	}
	
	public static String getSoqlFieldList(Set<String> fieldNames) {
		String result = '';
		for(String s : fieldNames) {
			result += s + ',';
		}
		if (result.length()>0) {
        	result = result.substring(0,result.lastIndexOf(','));
    	}
    	return result;
	}
	
	public static String getSoqlMergeAccountRecordTypeIds() {
		return ' AND RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN ' + SoqlUtil.getSoqlList(RecordTypeUtil.NAMES_ACCOUNT_RECORDTYPE_FOR_MERGE) + ' AND SobjectType=\'Account\')';
	}
	
	// YYYY-MM-DD
	public static String getSoql(Date d) {
		return DateTime.newInstance(d, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
	}
	
	// YYYY-MM-DDThh:mm:ssZ
	public static String getSoql(DateTime dt) {
		return dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
	}


	// YYYY-MM-DDTHH:mm:ssZ
	public static String getSoqlGMT(DateTime dt) {
		return dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
	}
}