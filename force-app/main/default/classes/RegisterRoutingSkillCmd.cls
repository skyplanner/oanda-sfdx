/**
 * @File Name          : RegisterRoutingSkillCmd.cls
 * @Description        : 
 * Manages SSkillRequirementInfo records linked to a routing information record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 2:37:40 AM
**/
public inherited sharing class RegisterRoutingSkillCmd {

	final BaseServiceRoutingInfo routingInfo;
	final Set<String> skillNameSet;
	
	public List<SSkillRequirementInfo> skillLInfoList {get; private set;}

	public Set<String> globalSkillNameSet {get; set;}

	public RegisterRoutingSkillCmd(BaseServiceRoutingInfo routingInfo) {
		this.routingInfo = routingInfo;
		this.skillNameSet = new Set<String>();
		this.skillLInfoList = new List<SSkillRequirementInfo>();
	}

	public SSkillRequirementInfo registerSkill(
		String skillName,
		Boolean isAditional
	) {
		SSkillRequirementInfo result = null;

		if (String.isNotBlank(skillName)) {
			SSkillRequirementInfo skillInfo = new SSkillRequirementInfo(
				skillName, // skillName
				isAditional // isAditional
			);

			if (registerSkill(skillInfo) == true) {
				result = skillInfo;
			}
		}
		return result;
	}

	public Boolean registerSkill(
		SSkillRequirementInfo skillInfo
	) {
		if (
			(skillInfo == null) ||
			String.isBlank(skillInfo.skillName) ||
			(skillNameSet.add(skillInfo.skillName) == false) //the skill was already added before for this item
		) {
			return false;
		}
		//else...
		skillInfo.skillReq.RelatedRecordId = routingInfo.routingObj.Id;
		skillLInfoList.add(skillInfo);
		globalSkillNameSet?.add(skillInfo.skillName);
		return true;
	}
	
}