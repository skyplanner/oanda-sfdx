/**
 * @File Name          : ChatSLAViolationNotificationManagerTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 4:14:17 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/4/2024, 9:54:57 AM   aniubo     Initial Version
**/
@isTest
private class ChatSLAViolationNotificationManagerTest {
	@testSetup
	private static void testSetup() {
		SLAViolationsTestDataFactory.createTestChats();
	}
	@isTest
	private static void testChatNotification() {
		// Test data setup

		List<LiveChatTranscript> liveChats = getChatTranscriptsForSLANotification();
		Map<String, Milestone_Time_Settings__mdt> milestones = MilestoneUtils.getSettingsFilterByViolationType(
			new List<String>{ SLAConst.AHT_EXCEEDED_VT}
		);
		Boolean hasError = false;
		// Actual test
		Test.startTest();
		try {
			LiveChatTranscript chatObj = liveChats.get(0);
			SLAViolationNotificationManager notificationManager = SLAViolationNotificationManagerFactory.createNotificationManager(
				SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT,
				SLAConst.SLAViolationType.ASA,
				SLAConst.AgentSupervisorModel.DIVISION,
				milestones,
				new FakeSupervisorProviderCreator()
			);
			SLAChatViolationInfo result = new SLAChatViolationInfo(
				chatObj.Id,
				chatObj.Name,
				SLAConst.ASA_EXCEEDED_VT
			);
			result.isNotifiable = true;
			result.isPersistent = true;
			result.division = 'Oanda Canada';
			result.divisionCode = 'OC';
			result.inquiryNature = chatObj.Inquiry_Nature__c;
			result.Tier = chatObj.Tier__c;
			result.language = chatObj.Language__c;
			result.liveOrPractice = chatObj.Chat_Type_of_Account__c;
			result.notificationObjectType = SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT;
			result.accountName = 'Test Account';
			result.ownerId = UserInfo.getUserId();
			notificationManager.sendNotifications(
				new List<SLAChatViolationInfo>{ result }
			);
		} catch (Exception ex) {
			hasError = true;
		}

		Test.stopTest();
		Assert.areEqual(
			false,
			hasError,
			'No Error occurred while sending notification'
		);
		// Asserts
	}
	public static List<LiveChatTranscript> getChatTranscriptsForSLANotification() {
		List<LiveChatTranscript> result = [
			SELECT
				Id,
				Name,
				Tier__c,
				Language__c,
				Chat_Division__c,
				Inquiry_Nature__c,
				Division_Name__c,
				Chat_Type_of_Account__c,
				Account.Name,
				Account.Primary_Division_Name__c
			FROM LiveChatTranscript
		];
		return result;
	}
}