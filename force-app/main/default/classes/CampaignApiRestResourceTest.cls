@isTest 
public with sharing class CampaignApiRestResourceTest {
    private static string emptyRestUrl = '/services/apexrest/api/v1/campaigns/';
    private static string restUrl = '/services/apexrest/api/v1/campaigns/123123123';
    private static string getMethod = 'GET';

    @isTest
    static void testDoGetNoUserId() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = emptyRestUrl;  
        req.httpMethod = getMethod;

    
        RestContext.request = req;
        RestContext.response = res;
        
        CampaignApiRestResource.doGet();
    
        Assert.areEqual(400, res.statusCode, 'fxAccountTradeUserId is required');
    }

    @isTest
    static void testDoGetNoUser() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = getMethod;

    
        RestContext.request = req;
        RestContext.response = res;
        
        CampaignApiRestResource.doGet();
    
        Assert.areEqual(200, res.statusCode, 'empty list returned');
    }

    @isTest
    static void testDoGetUser2() {
        Lead liveLead = TestDataFactory.getTestLead(true);
        
        insert liveLead;
        
        fxAccount__c fxAccount = new fxAccount__c();
	    fxAccount.Account_Email__c = liveLead.Email;
	    fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxAccount.fxTrade_User_Id__c = 123123123;
	    insert fxAccount;
        
        
        //test lead is converted
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(liveLead.id);
        lc.setConvertedStatus('Hot');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());

        Campaign c = new Campaign(Name='TEST NEW CAMPAIGN');
        insert c;

        Account acc = [SELECT id  FROM ACCOUNT limit 1];
        acc.fxAccount__c = fxAccount.Id;
        update acc;

        Contact con = [SElect id,account.fxAccount__r.fxTrade_User_Id__c, Contact.name from Contact limit 1];

        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=con.Id);
        insert cm;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = restUrl;  
        req.httpMethod = getMethod;

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);
        CampaignApiRestResource.doGet();
        Test.stopTest();
        Assert.areEqual(200, res.statusCode);
        Assert.isTrue(res.responseBody.toString().contains('TEST NEW CAMPAIGN'));
    }

}