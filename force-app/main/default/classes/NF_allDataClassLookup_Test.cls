/**
 * @File Name          : NF_allDataClassLookup_Test.cls
 * @Description        : 
 * @Author             : Ashish Jethi
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/9/2019, 3:14:21 PM   	  Ashish Jethi     			Initial Version
**/
@isTest
public class NF_allDataClassLookup_Test {

	/*test cases start for controller -NF_lookupAccountRecord */
	static testMethod void testlookUpAccount(){
		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.PersonEmail = 'test@class.com';
		insert testAccount;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('email',testAccount.PersonEmail);
		test.startTest();
		NF_lookupAccountRecord obj = new NF_lookupAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
        system.assertEquals('test@class.com',(string)returnResult.get('email'));
	}

	static testMethod void inValidtestlookUpAccount(){
		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.PersonEmail = 'test@class.com';
		insert testAccount;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('email','Invalidtest@class.com');
		test.startTest();
		NF_lookupAccountRecord obj = new NF_lookupAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,Object> returnResult = (Map<String,Object>)ResultObj;
        system.assertEquals('None',(string)returnResult.get('result'));
	}
	/*test cases end for controller -NF_lookupAccountRecord */

	/*test cases start for controller -NF_lookupfxAccountRecord */
	static testmethod void testValidFxAccount(){
		Account testAccount = NF_TestDataUtill.createAccount();
		insert testAccount;
		fxAccount__c testFXAccount = NF_TestDataUtill.createfxAccount();
		testFXAccount.Account__c = testAccount.Id;
		insert testFXAccount;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('email',testFXAccount.name);
		testResult.put('username',testFXAccount.name);
		test.startTest();
		NF_lookupfxAccountRecord obj = new NF_lookupfxAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
        system.assertEquals('true',(string)returnResult.get('Result'));
	}
	static testmethod void testInValidFxAccount(){
		Account testAccount = NF_TestDataUtill.createAccount();
		insert testAccount;
		fxAccount__c testFXAccount = NF_TestDataUtill.createfxAccount();
		testFXAccount.Account__c = testAccount.Id;
		insert testFXAccount;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('email',testFXAccount.name);
		testResult.put('username','InvalidAcc');
		test.startTest();
		NF_lookupfxAccountRecord obj = new NF_lookupfxAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
        system.assertEquals('false',(string)returnResult.get('Result'));
	}
	/*test cases end for controller -NF_lookupfxAccountRecord */
	static testmethod void testValidDepositAccount(){
		Lead testLead = NF_TestDataUtill.createLead();
		insert testLead;
		fxAccount__c testFXAccount = NF_TestDataUtill.createfxAccount();
		testFXAccount.Funnel_Stage__c = 'Documents Uploaded';
		testFXAccount.Division_Name__c ='OANDA Europe';
		testFXAccount.Lead__c = testLead.id;
		insert testFXAccount;
		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.fxAccount__c = testFXAccount.id;
		testAccount.PersonEmail ='test@class.com';
		insert testAccount;
		Map<String,String> testResult = new Map<String,String>();
		testResult.put('email','test@class.com');
        testResult.put('result','value');
		testResult.put('funnelStage','value');
		testResult.put('oemClient','value');
		NF_lookupDepositStatusAccountRecord obj = new NF_lookupDepositStatusAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.startTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
        system.assertEquals('true',(string)returnResult.get('result'));
	}
	static testmethod void testInValidDepositAccount(){
		Lead testLead = NF_TestDataUtill.createLead();
		insert testLead;
		fxAccount__c testFXAccount = NF_TestDataUtill.createfxAccount();
		testFXAccount.Funnel_Stage__c = 'Documents Uploaded';
		testFXAccount.Division_Name__c ='OANDA Europe';
		testFXAccount.Lead__c = testLead.id;
		insert testFXAccount;
		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.fxAccount__c = testFXAccount.id;
		testAccount.PersonEmail ='test@test.com';
		insert testAccount;
		Map<String,String> testResult = new Map<String,String>();
		testResult.put('email','test@class.com');
        testResult.put('result','value');
		testResult.put('count','0');
		NF_lookupDepositStatusAccountRecord obj = new NF_lookupDepositStatusAccountRecord();
		String Result = obj.processRequest(testResult,null,null);
		test.startTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
        system.assertEquals('false',(string)returnResult.get('result'));
	}
	static testmethod void testValidNameDOB(){
		TestsUtil.forceJobsSync = true;
		
		nfchat__Chat_Log__c testChatLog = NF_TestDataUtill.createChatLog();
		testChatLog.nfchat__First_Name__c = 'Test Account';
		testChatLog.Date_Of_Birth__c = '01/01/2001';
		insert testChatLog;

		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.PersonEmail = testChatLog.nfchat__Email__c;
		insert testAccount;

		fxAccount__c testFx = NF_TestDataUtill.createfxAccount();
		Date myDate = Date.newInstance(2001,01,01);
		testFx.BirthDate__c = myDate;
		testFx.Account__c = testAccount.Id;
		insert testFx;

		Map<String,String> testResult = new Map<String,String>();
		testResult.put('sessionId',testChatLog.nfchat__Session_Id__c);
		String sessionSTR = JSON.serialize(testResult);
		test.startTest();
		NF_lookupAccountByNameDOB obj = new NF_lookupAccountByNameDOB();
		String Result = obj.processRequest(testResult,sessionSTR,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
		Map<String,object> returnResult = (Map<String,object>)ResultObj;
		system.assertEquals('true',(string)returnResult.get('result'));
	}
	static testmethod void testInvalidNameDOB(){
		nfchat__Chat_Log__c testChatLog = NF_TestDataUtill.createChatLog();
		testChatLog.nfchat__First_Name__c = 'Test Acct';
		testChatLog.Date_Of_Birth__c = '02/02/2002';
		insert testChatLog;

		Account testAccount = NF_TestDataUtill.createPersonAccount();
		testAccount.PersonEmail = testChatLog.nfchat__Email__c;
		insert testAccount;

		fxAccount__c testFx = NF_TestDataUtill.createfxAccount();
		Date myDate = Date.newInstance(2001,01,01);
		testFx.BirthDate__c = myDate;
		testFx.Account__c = testAccount.Id;
		insert testFx;

		Map<String,String> testResult = new Map<String,String>();
		testResult.put('sessionId',testChatLog.nfchat__Session_Id__c);
		String sessionSTR = JSON.serialize(testResult);
		test.startTest();
		NF_lookupAccountByNameDOB obj = new NF_lookupAccountByNameDOB();
		String Result = obj.processRequest(testResult,sessionSTR,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
		Map<String,object> returnResult = (Map<String,object>)ResultObj;
		system.assertEquals('false',(string)returnResult.get('result'));
	}
	/*test cases start for controller -NF_getSessionCaseStatus */
	static testMethod void testgetCaseStatus(){
		nfchat__Chat_Log__c testChatLog = NF_TestDataUtill.createChatLog();
		insert testChatLog;
		Case testCase = NF_TestDataUtill.getCase();
		testCase.nfchat__Chat_Log__c = testChatLog.id;
		insert testCase;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('sessionId',testChatLog.nfchat__Session_Id__c);
		String sessionSTR = JSON.serialize(testResult);
		test.startTest();
		NF_getSessionCaseStatus obj = new NF_getSessionCaseStatus();
		String Result = obj.processRequest(testResult,sessionSTR,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
		system.assert((string)returnResult.get('result') != 'false');
	}
    static testMethod void testgetCaseStatus_2(){
		nfchat__Chat_Log__c testChatLog = NF_TestDataUtill.createChatLog();
		insert testChatLog;
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('sessionId',testChatLog.nfchat__Session_Id__c);
		String sessionSTR = JSON.serialize(testResult);
		test.startTest();
		NF_getSessionCaseStatus obj = new NF_getSessionCaseStatus();
		String Result = obj.processRequest(testResult,sessionSTR,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
		system.assert((string)returnResult.get('result') == 'false');
	}
    static testMethod void testgetCaseStatus_3(){
		Map<String,String> testResult = new Map<String,String>();
        testResult.put('sessionId','652gadgf2563rdghw');
		String sessionSTR = JSON.serialize(testResult);
		test.startTest();
		NF_getSessionCaseStatus obj = new NF_getSessionCaseStatus();
		String Result = obj.processRequest(testResult,sessionSTR,null);
		test.stopTest();
		object ResultObj = json.deserializeUntyped(Result);
        Map<String,object> returnResult = (Map<String,object>)ResultObj;
		system.assert((string)returnResult.get('result') == 'false');
	}

}