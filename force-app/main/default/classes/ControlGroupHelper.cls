/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public class ControlGroupHelper 
{
    public static Id retentionHouseUserId = UserUtil.getUserIdByName(UserUtil.NAME_RETENTION_HOUSE_USER);
    public static Id demoLeadRecordTypeID = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    public static Id systemUserId = UserUtil.getSystemUserId();
    
    public static void assignToControlGroup(List<Lead> leads, Map<Id, Lead> oldMap)
    {
        //control group settings
        set<Id> controlGroupSettingLeadCountUpdateIds = new set<Id>{};
        Map<string, Control_Group_Settings_New__c> controlGroupSettingsMap = new Map<string, Control_Group_Settings_New__c>{};
        Map<Id, Control_Group_Settings_New__c> controlGroupSettingsMapById = new Map<Id, Control_Group_Settings_New__c>{};
      
        //country settings    
        Map<string,Country_Setting__c> cs_countrySettingsMap = new Map<string,Country_Setting__c>{};     
        Map<string, set<string>> cs_regionCountriesMap = new Map<string, set<string>>{};
        Map<string, string> cs_countryRegionMap = new Map<string, string>{};     
		
        set<string> lead_controlGroup_countryNames = new set<string>{};
        set<string> lead_controlGroup_region_countryNames = new set<string>{};
            
        Lead[] potentialControlGroupLeads = new Lead[]{}; 
        Lead[] controlGroupLeads = new Lead[]{};     
        Lead[] nonControlGroupLeads = new Lead[]{}; 

        //get country settings
        for(Country_Setting__c countrySetting : Country_Setting__c.getAll().values()) 
        {
            String countryName = countrySetting.Long_Name__c != null ? countrySetting.Long_Name__c : countrySetting.Name;  
            cs_countrySettingsMap.put(countryName, countrySetting);
            cs_countryRegionMap.put(countryName, countrySetting.Control_Group_Region__c);
            
            set<string> regionCountries = cs_regionCountriesMap.get(countrySetting.Control_Group_Region__c);
            if(regionCountries == null)
            {
                regionCountries = new set<string>{};
            }
            regionCountries.add(countryName);
            cs_regionCountriesMap.put(countrySetting.Control_Group_Region__c, regionCountries);
        }
    
        //get control group settings
        for(Control_Group_Settings_New__c controlGrpSetting : Control_Group_Settings_New__c.getAll().values())
        {
            if(controlGrpSetting.Is_NTA__c == false && controlGrpSetting.Is_Enabled__c == true)
            {
            	controlGroupSettingsMap.put(controlGrpSetting.Name, controlGrpSetting); 
            	controlGroupSettingsMapById.put(controlGrpSetting.Id, controlGrpSetting);   
            }
        }
        
        //process leads and group them
        for(Lead leadInfo : leads)
        {
            if(leadInfo.country != null &&
               leadInfo.RecordTypeId == LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE &&
               cs_countrySettingsMap.containsKey(leadInfo.country))
            {
                string countryName = leadInfo.country;
                Country_Setting__c countrySetting = cs_countrySettingsMap.get(countryName);               
                string controlGroupRegionName = countrySetting.Control_Group_Region__c;
                
                Control_Group_Settings_New__c controlGroupSetting_Country = controlGroupSettingsMap.get(countryName);
                Control_Group_Settings_New__c controlGroupSetting_Region = controlGroupSettingsMap.get(controlGroupRegionName);
         
                if(controlGroupSetting_Country != null || controlGroupSetting_Region != null)
                {
                    potentialControlGroupLeads.add(leadInfo);
                } 
                else
                {
                    //country/region not listed on Control group settings..so send to round robin candidate list
                    nonControlGroupLeads.add(leadInfo);
                }
            }
            else
            {
                //failed in basic check..so send to regular round robin candidate list
                nonControlGroupLeads.add(leadInfo);
            }
        }

        // now assign every N'th lead to control group
        for(Lead potentialControlGroupLead :  potentialControlGroupLeads)
        {
            string countryName = potentialControlGroupLead.country;
            Country_Setting__c countrySetting = cs_countrySettingsMap.get(countryName);			
            string key = controlGroupSettingsMap.containsKey(countryName) ? countryName : countrySetting.Control_Group_Region__c;
            
            Control_Group_Settings_New__c controlGroupSetting = controlGroupSettingsMap.get(key);
            integer leadAllotmentNumber = Integer.valueOf(controlGroupSetting.Lead_Allotment_Number__c);
            integer leads_count = Integer.valueOf(controlGroupSetting.Lead_Counter__c);
            
            if(math.mod(leads_count +1 , leadAllotmentNumber) == 0)
            {
                potentialControlGroupLead.OwnerId = retentionHouseUserId;
                controlGroupSetting.Lead_Counter__c = 0;
            }
            else
            {
                controlGroupSetting.Lead_Counter__c += 1;
                nonControlGroupLeads.add(potentialControlGroupLead);
            }          
            controlGroupSettingLeadCountUpdateIds.add(controlGroupSetting.Id);
        }

        //assign the rest to round robin        
        if(nonControlGroupLeads.size() > 0)
        {
            LeadUtil.automaticLeadAssignment(nonControlGroupLeads, oldMap, true);
        }
        
        //update the counte
        if(controlGroupSettingLeadCountUpdateIds.size() >0)
        {
            Control_Group_Settings_New__c[] cgSettingsToUpdate = new Control_Group_Settings_New__c[]{};
            for(Id cgSettingId : controlGroupSettingLeadCountUpdateIds)
            {
                Control_Group_Settings_New__c s = controlGroupSettingsMapById.get(cgSettingId);
                cgSettingsToUpdate.add(s);
            }
            update cgSettingsToUpdate;
        }
    }
    
    /*
     *  Run a batch job daily to query all opportunities (country = Singapore) that became Traded within the past 24 hrs, 
     */
    public static void assignToSingaporeControlGroup()
    {
        Control_Group_Settings_New__c controlGroupSetting = Control_Group_Settings_New__c.getValues('Singapore');
        
        if(controlGroupSetting.Is_Enabled__c)
        {
            integer initalCount = Integer.valueOf(controlGroupSetting.Lead_Counter__c);
            Account[] changedAccounts = new Account[]{};
        
            Account[] accs = [SELECT Id, OwnerId, fxAccount__c
                              FROM Account 
                              WHERE fxAccount__r.RecordType.Name = 'Retail Live' AND
                                    fxAccount__r.Funnel_Stage__c = 'Traded' AND
                                    PersonMailingCountry = 'Singapore' AND
                                    fxAccount__r.First_Trade_Date__c = YESTERDAY AND
                                    RAF_Referrer_Username__pc = null AND 
                                    RAF_Referrer_Email_Address__pc = null AND
                                    Eligible_for_Rebate__pc = false AND
                                    Eligible_for_RM_Referral__pc = false];
             
            if(accs.size() > 0)
            {
           		List<fxAccount__c> updatefxList = new List<fxAccount__c>();

                // now assign every N'th lead to control group
                for(Account ac :  accs)
                {
                    integer leadAllotmentNumber = Integer.valueOf(controlGroupSetting.Lead_Allotment_Number__c);
                    integer leads_count = Integer.valueOf(controlGroupSetting.Lead_Counter__c);
                    
                    if(math.mod(leads_count +1 , leadAllotmentNumber) == 0)
                    {
                        ac.OwnerId = retentionHouseUserId;
                        List<CXNoteListController.CXNote> notes = CXNoteListController.getCXNotes(ac.fxAccount__c);
                        CXNoteListController.CXNote note = new CXNoteListController.CXNote();
                        note.timestamp = (Datetime.now().getTime());
                        note.comment = System.Label.Control_Group_Helper;
                        note.author = UserInfo.getName();
                        notes.add(note);
                        fxAccount__c fxa = new fxAccount__c(
                            Id = ac.fxAccount__c,
                            Notes__c = JSON.serialize(notes)
                        );
                        updatefxList.add(fxa);
                        changedAccounts.add(ac);
                        
                        controlGroupSetting.Lead_Counter__c = 0;
                    }
                    else
                    {
                        controlGroupSetting.Lead_Counter__c += 1;
                    }          
                }

                //updating the notes
                if(updatefxList.size() > 0){
                    update updatefxList;
                }
                
                if(initalCount != controlGroupSetting.Lead_Counter__c)
                {
                    update controlGroupSetting;
                }
                if(changedAccounts.size() > 0)
                {
                    update changedAccounts;
                }
            }
        }
    }
}