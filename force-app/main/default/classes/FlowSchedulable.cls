/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-22-2022
 * @last modified by  : Dianelys Velazquez
**/
public class FlowSchedulable implements Schedulable {
    String flowName;

    public FlowSchedulable(String flowName) {
        this.flowName = flowName;
    }

	public void execute(SchedulableContext scon) {
		Flow.Interview myFlow = Flow.Interview.createInterview(
            flowName, new Map<String, Object>());
        myFlow.start();
	}

    public static String schedule(
        String scheduleName, String flowName, String cron
    ) {
        FlowSchedulable s = new FlowSchedulable(flowName); 
        return System.schedule(scheduleName, cron, s);
    }

    public static void scheduleOmniChannelOnlineAgentFlow() {
        schedule('Omni-Channel Online Agent Warning 00',
            'Omni_Channel_Online_Agents',
            '0 0 * * * ?'
        );

        schedule('Omni-Channel Online Agent Warning 30',
            'Omni_Channel_Online_Agents',
            '0 30 * * * ?'
        );
    }

    public static String scheduleOmniChannelRoutingFlow() {
        return schedule(
            'Omni-Channel Routing Warning',
            'Omni_Channel_Routing_Check',
            '0 0 * * * ?'
        );
    }
}