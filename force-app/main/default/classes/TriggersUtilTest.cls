/*
TriggersUtil test class
*/
@isTest
public class TriggersUtilTest {
    
    @isTest 
    static void disableEnableByTriggerTest() {
        TriggersUtil.disableTrigger(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE);
        System.assert(!TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE));

        TriggersUtil.enableTrigger(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE);
        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE));

        TriggersUtil.disableTriggers(
            new List<TriggersUtil.Triggers>{
                TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE,
                TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER});
        
        System.assert(!TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE));
        System.assert(!TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER));

        TriggersUtil.enableTriggers(
            new List<TriggersUtil.Triggers>{
                TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE,
                TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER});
        
        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE));
        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER));

        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_FXACCOUNT_AFTER));
    }

    @isTest 
    static void disableEnableByObjTest() {
        TriggersUtil.disableObjTriggers(
                TriggersUtil.Obj.ACCOUNT);

        System.assert(!TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE));

        System.assert(!TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER));

        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_FXACCOUNT_BEFORE));

        System.assert(TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_FXACCOUNT_AFTER));        
    }
    
}