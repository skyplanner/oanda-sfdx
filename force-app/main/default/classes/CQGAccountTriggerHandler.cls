/**
 * Name: CQGAccountTriggerHandler
 * Description : Trigger Hanlder class for CQG_Account__c
 * Author: ejung
 * Date : 2024-03-06
 */
public with sharing class CQGAccountTriggerHandler extends TriggerHandler {

    List<CQG_Account__c> cqgAccountList;

    /**
     * Constructor
     */
    public CQGAccountTriggerHandler() {
        this.cqgAccountList = (List<CQG_Account__c>) Trigger.new;
    }

    /**
     * Handles the before insert event
     */
    public override void beforeInsert() {
        updateCqgData(cqgAccountList);
    }

    public void updateCqgData(List<CQG_Account__c> cqgAccounts) {
        if(cqgAccounts == null || cqgAccounts.size() <1) return;

        Map<Id, Id> fxaMapByCQG =  new Map<Id, Id>();

        for(CQG_Account__c cqg : cqgAccounts) {
            if(cqg.fxAccount__c != null) {
                fxaMapByCQG.put(cqg.Id, cqg.fxAccount__c);
            }
        }

        Map<Id, fxAccount__c> fxaMap = new Map<Id, fxAccount__c>([
            SELECT Id, 
                Account__c 
            FROM fxAccount__c 
            WHERE Id IN : fxaMapByCQG.values() 
        ]);

        for(CQG_Account__c cqg : cqgAccounts) {
            if(cqg.fxAccount__c != null) {
                cqg.Account__c = fxaMap.get(cqg.fxAccount__c).Account__c;
            }
        }
    }
}