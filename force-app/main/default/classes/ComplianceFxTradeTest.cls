@isTest
private class ComplianceFxTradeTest {
    
    static testMethod void testCreateCasesForStudent(){
    	
    	testCreateCase('Student');
    }
    
    static testMethod void testCreateCasesForRetired(){
    	
    	testCreateCase('Retired');
    }
  
    static testMethod void testCreateCasesForUnemployed(){
    	
    	testCreateCase('Unemployed');
    }
    
    static testMethod void testCreateCasesForSelfEmployed(){
    	
    	testCreateCase('Self_Employed');
    }
    
    static testMethod void testCreateCasesForEmployed(){
    	
    	testCreateCase('Employed');
    }
    
    private static void testCreateCase(String employStatus){
    	fxAccount__c liveAccount = createFxAccount();
    	
    	Compliance_FxTrade__c input = new Compliance_FxTrade__c();
    	input.fxTrade_UserName__c = liveAccount.name;
    	input.Household_Income__c = '15_25K';
    	input.Net_Worth__c = 12600;
    	input.Income_Source__c = 'abc';
    	input.Age__c = 31;
    	input.Bankruptcy_Status__c = true;
    	input.INTERMEDIARY__c = true;
    	input.Division_ID__c = '1';
    	input.Employment_Status__c = employStatus;
    	input.Case_Subject__c = 'Test Case';
    	input.Case_Type__c = 'Governance';
    	input.Case_Subtype__c = 'eID follow up';
    	input.Email__c = '123@abc.com';
    	
    	insert input;
    	
    	List<Case> cases = [select id, Subject, Type, SubType__c, AccountId, ContactId from Case where Subject = 'Test Case'];
    	
    	System.assertEquals(1, cases.size());
    	System.assertEquals('Test Case', cases[0].Subject);
    	System.assertEquals('Governance', cases[0].Type);
    	System.assertEquals('eID follow up', cases[0].SubType__c);
    	System.assertEquals(liveAccount.Account__c, cases[0].AccountId);
    	System.assertEquals(liveAccount.Contact__c, cases[0].ContactId);
    }
    
    private static fxAccount__c createFxAccount(){
    	fxAccount__c liveAccount = [select name, recordTypeId, Funnel_Stage__c, Account__c, Contact__c from fxAccount__c where name = 'abc' limit 1];
		
    	return liveAccount;
    }
    
    @testSetup
	static void createTestData() {
		RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
		
		Account acc = new Account(LastName = 'abc', RecordTypeId = personAccountRecordType.Id);
    	insert acc;
    	
    	fxAccount__c liveAccount = new fxAccount__c();
    	liveAccount.name = 'abc';
	    liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
		liveAccount.Account__c = acc.Id;
		liveAccount.Contact__c = acc.PersonContactId;
		insert liveAccount;
		
	}
}