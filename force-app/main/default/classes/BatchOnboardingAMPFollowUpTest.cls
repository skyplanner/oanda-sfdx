/**
 * BatchOnboardingAMPFollowUp test class
 * Documentation: Please, use this document as a reference on how to run this batch
 *   https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
 */
@isTest
public class BatchOnboardingAMPFollowUpTest {
    
    /**
     * All changes and verify all cases are created properly
     */
    @isTest
    public static void testCreateAllCases() {
        
        TestDataFactory factory = new TestDataFactory();
        Account a = factory.createTestAccount();
        
        fxAccount__c fxAcc = factory.createFXTradeAccount(a);
       
        fxAcc.Division_Name__c = Constants.DIVISIONS_OANDA_CORPORATION;
        fxAcc.Funnel_Stage__c = Constants.FUNNEL_FUNDED;
        fxAcc.Street__c = 'PO BOX 123';
        fxAcc.Employment_Job_Title__c = 'Trader';
        fxAcc.Net_Worth_Value__c = 500000;
        fxAcc.Industry_of_Employment__c = AMPUtils.INDUSTRY_EMP_VALUES[0];
        update fxAcc;
        
        // No cases created yet
        System.assertEquals(
            [SELECT Id
                FROM Case
                WHERE Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT].size(),
            0,
            'Was');

        Test.startTest();
        runBatch();
        Test.stopTest();
        
        // Must be no errors
        System.assert(
            [SELECT Id FROM Log__c].isEmpty(),
            'We got some error.');

        // 4 cases must have been created
        System.assertEquals(
            [SELECT Id
                FROM Case
                WHERE Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT].size(),
            4,
            'The 4 cases were not created.');

        // 1 case must have been created with subtype
        // SUBTYPE_ADDRESS_CONFIRMATION
        System.assertEquals(
            [SELECT Id
                FROM Case
                WHERE Type__c = :Constants.CASE_TYPE_AMP_FOLLOWUP
                AND Subtype__c = 
                    :BatchOnboardingAMPFollowUp.SUBTYPE_ADDRESS_CONFIRMATION].size(),
            1,
            'The address confirmation case was not created.');

        // 3 case must have been created with subtype
        // SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS
        System.assertEquals(
            [SELECT Id
                FROM Case
                WHERE Type__c = 
                    :Constants.CASE_TYPE_AMP_FOLLOWUP
                AND Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT
                AND Subtype__c = 
                    :BatchOnboardingAMPFollowUp.SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS].size(),
            3,
            'The employment financial details cases were not created.');
    }

    /**
     * No cases created becase the money change was not BIG
     */
    @isTest
    public static void noCreateCaseFromSmallAnnualIncomeChange() {
        TestDataFactory factory = new TestDataFactory();
        Account a = factory.createTestAccount();
        
        fxAccount__c fxAcc = factory.createFXTradeAccount(a);
       
        fxAcc.Division_Name__c = Constants.DIVISIONS_OANDA_CORPORATION;
        fxAcc.Funnel_Stage__c = Constants.FUNNEL_FUNDED;
        fxAcc.Annual_Income__c  ='25_35K';
        
        update fxAcc;
        
        // No cases created yet
        System.assertEquals(
            [SELECT Id 
                FROM Case 
                WHERE Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT].size(),
            0,
            'Some case was created by mistake.');

        Test.startTest();
        runBatch();
        Test.stopTest();

        // No cases created becase the money change was not BIG
        System.assertEquals(
            [SELECT Id 
                FROM Case 
                WHERE Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT].size(),
            0,
            'Some case was created by mistake with not big money change.');
    }

    /**
     * Big money change and a case must be created
     */
    @isTest
    public static void createCaseFromBigAnnualIncomeChange() {
        TestDataFactory factory = new TestDataFactory();
        Account a = factory.createTestAccount();
        
        fxAccount__c fxAcc = factory.createFXTradeAccount(a);
       
        fxAcc.Division_Name__c = Constants.DIVISIONS_OANDA_CORPORATION;
        fxAcc.Funnel_Stage__c = Constants.FUNNEL_FUNDED;
        fxAcc.Annual_Income__c  ='130_200K';
        update fxAcc;

        Test.startTest();
        runBatch();
        Test.stopTest();
        
        // Must be no errors
        System.assert(
            [SELECT Id FROM Log__c].isEmpty(),
            'We got some error.');

        // 1 case must have been created with subtype
        // SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS
        System.assertEquals(
            [SELECT Id
                FROM Case
                WHERE Type__c = 
                    :Constants.CASE_TYPE_AMP_FOLLOWUP
                AND Subject = 
                    :BatchOnboardingAMPFollowUp.CASE_SUBJECT
                AND Subtype__c = 
                    :BatchOnboardingAMPFollowUp.SUBTYPE_EMPLOYMENT_FINANCIAL_DETAILS].size(),
            1,
            'The employment financial details case was not created.');
    }

    /**
     * Run batch where the cases are created
     */
    private static void runBatch() {
        // This is the same query posted in the documentation of how run the batch
        // https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
        String query = 'SELECT Id,Division_Name__c,EID_Pass_Count__c,Contact__c,Intermediary__c,Works_for_a_CFTC_NFA_member_org__c,Account__c,Account__r.Id,AMP_Job_Title_Change__c ,AMP_Net_Worth_Changed__c , AMP_Street_Changed__c,AMP_Industry_Employment_Changed__c FROM fxAccount__c WHERE Division_Name__c = \'OANDA Corporation\' AND (Funnel_Stage__c = \'Traded\' OR Funnel_Stage__c = \'Ready For Funding\' OR Funnel_Stage__c = \'Funded\') AND (AMP_Street_Changed__c  = true  OR AMP_Job_Title_Change__c = true OR AMP_Net_Worth_Changed__c = true OR AMP_Industry_Employment_Changed__c = true) AND lastmodifieddate = TODAY';
        
        Database.executeBatch(
            new BatchOnboardingAMPFollowUp(query),
            200);
    }

}