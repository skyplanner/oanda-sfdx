/* Name: ComplianceRecordKidRestResourceTest
 * Description : Test Class to handle compliance records crud
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 July 26
 */

 @isTest
 public class ComplianceRecordKidRestResourceTest {
    
    public static final String POST_REQUEST_BODY = '{\"document_name\":\"Indices CFD Doc\",\"mt5_order\":\"mt5oder\",\"mt5_account\":\"mt5Acc\",\"document_version\":\"doc\",\"date_sent\":\"2024-12-12\"}';
    public static final String INVALID_POST_REQUEST_BODY = '{\"document_name\":\"invalid CFD Doc\",\"mt5_order\":\"mt5oder\",\"mt5_account\":\"mt5Acc\",\"document_version\":\"doc\",\"date_sent\":\"2024-12-12\"}';
    @TestSetup
    static void initData(){
 
        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 12345678;
        fxAccount.fxTrade_Global_ID__c = 12345678 + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        insert fxAccount;

        List<Schema.PicklistEntry> allowedDocNames
            = Compliance_Record__c.Document_Name__c.getDescribe().getPickListValues();

        TestDataFactory.createComplianceRecordKid(fxAccount.Id, allowedDocNames[0].getValue(), '1.0', Datetime.now());
        TestDataFactory.createComplianceRecordKid(fxAccount.Id, allowedDocNames[0].getValue(), '1.1', Datetime.now().addDays(1));
        TestDataFactory.createComplianceRecordKid(fxAccount.Id, allowedDocNames[0].getValue(), '1.2', Datetime.now().addDays(2));
    }

    @isTest
    static void testDoGetSuccess200() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/12345678';
        req.httpMethod = 'GET';
        
        Test.startTest();
        ComplianceRecordKidRestResource.doGet();
        Test.stopTest();

        List<Compliance_Record__c> expectedKids = [
            SELECT 
                Document_Name__c,
                MT5_Account_Number__c,
                MT5_Order_Number__c,
                Document_Version__c,
                Date_Sent__c
            FROM Compliance_Record__c
            WHERE fxAccount__r.FxTrade_Global_Id__c = :fxAccountUtil.constructGlobalTradeId(12345678, true)
            AND RecordType.DeveloperName = 'KID'
        ];

        List<Compliance_Record__c> complianceRecords = (List<Compliance_Record__c>) JSON.deserialize(res.responseBody.toString(), List<Compliance_Record__c>.class);

        Assert.areEqual(3, complianceRecords.size(), 'There should be 3 compliance records');
        for(Integer i = 0; i< complianceRecords.size(); i++) {
            Assert.areEqual(expectedKids[i].Document_Name__c, complianceRecords[i].Document_Name__c, 'Document name mismatch');
            Assert.areEqual(expectedKids[i].MT5_Account_Number__c, complianceRecords[i].MT5_Account_Number__c, 'Account number mismatch');
            Assert.areEqual(expectedKids[i].MT5_Order_Number__c, complianceRecords[i].MT5_Order_Number__c, 'Order number mismatch');
            Assert.areEqual(expectedKids[i].Document_Version__c, complianceRecords[i].Document_Version__c, 'Document version mismatch');
            Assert.areEqual(expectedKids[i].Date_Sent__c, complianceRecords[i].Date_Sent__c, 'Date mimsatch');
        }
    }

    @isTest
    static void testDoGetFail404() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/1234567';
        req.httpMethod = 'GET';
        
        Test.startTest();
        ComplianceRecordKidRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(404, res.statusCode, 'Request should fail with code 404');
    }

    @isTest
    static void testDoGetFail500onBadRequest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid';
        req.httpMethod = 'GET';
        
        Test.startTest();
        ComplianceRecordKidRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(500, res.statusCode, 'Request should fail with code 500');
    }

    @isTest
    static void testDoPostSuccess200() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/12345678';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(POST_REQUEST_BODY);
        
        Test.startTest();
        ComplianceRecordKidRestResource.doPost();
        Test.stopTest();

        Assert.areEqual(200, res.statusCode, 'Request should be successful');

        Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.responseBody.toString());

        ApiMappingService mapper = new ApiMappingService(complianceRecordKidRestResource.ENDPOINT_NAME);
        Compliance_Record__c cr = new Compliance_Record__c();

        mapper.setInputFieldstoSobject(cr, (Map<String,Object>)JSON.deserializeUntyped(POST_REQUEST_BODY));

        for(String field : cr.getPopulatedFieldsAsMap().keySet()) {
            Assert.areEqual(
                JSON.serialize(cr.get(field)), 
                JSON.serialize(response.get(field)),
                'Invalid field value'
            );
        }
    }

    @isTest
    static void testDoPostFail400() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/12345678';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(INVALID_POST_REQUEST_BODY);
        
        Test.startTest();
        ComplianceRecordKidRestResource.doPost();
        Test.stopTest();

        Assert.areEqual(400, res.statusCode, 'Request should not be successful');
    }

    @isTest
    static void testDoPostFail404() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/123';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(INVALID_POST_REQUEST_BODY);
        
        Test.startTest();
        ComplianceRecordKidRestResource.doPost();
        Test.stopTest();

        Assert.areEqual(404, res.statusCode, 'Request should not be successful');
    }

    @isTest
    static void testDoPostFail500() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/api/v1/user/kid/*';
        req.requestURI = '/api/v1/user/kid/' + null;
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(INVALID_POST_REQUEST_BODY);
        
        Test.startTest();
        ComplianceRecordKidRestResource.doPost();
        Test.stopTest();

        Assert.areEqual(500, res.statusCode, 'Request should not be successful');
    }
}