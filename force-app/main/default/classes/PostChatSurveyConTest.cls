/**
 * Test: PostChatSurveyCon
 * @author Fernando Gomez
 * @version 1.0
 * @since Apr 10th
 */
@isTest
private class PostChatSurveyConTest {
	/**
	 * public PageReference tryRedirecting()
	 */
	@isTest 
	static void tryRedirecting() {
		PageReference pr = Page.PostChatSurvey;
		PostChatSurveyCon ctrl;
		Case c;

		pr.getParameters().putAll(new Map<String, String> {
			'chatKey' => 'rf45-mdls-22edo-qe34',
			'windowLanguage' => 'en_US'
		});
		// Test.setCurrentPage(pr);

		ctrl = new PostChatSurveyCon();
		System.assert(ctrl.isError);

		Test.setCurrentPage(pr);
		ctrl = new PostChatSurveyCon();
		System.assert(ctrl.isError);

		pr.getParameters().put('attachedRecords', '____');
		ctrl = new PostChatSurveyCon();
		System.assert(ctrl.isError);

		pr.getParameters().put('attachedRecords', '{"CaseId":"5000t000005LOVvAAO"}');
		ctrl = new PostChatSurveyCon();
		System.assert(ctrl.isError);

		insert new SurveySettings__c(CurrentSurveyName__c = 'Sample');
		insert new Survey__c(Name = 'Sample');
		ctrl = new PostChatSurveyCon();
		System.assert(ctrl.isError);

		c = new Case(
			Subject = 'sample',
			Origin = 'Phone',
			Live_or_Practice__c = 'Live',
			Priority = 'Normal',
			Inquiry_Nature__c = 'Registration',
			Status = 'Open',
			RecordTypeId = 
				Schema.SObjectType.Case.getRecordTypeInfosByName().get(
					'Support').getRecordTypeId(),
			FeedbackReminderSentOn__c = System.now()
		);
		insert c;
		pr.getParameters().put('attachedRecords', '{"CaseId":"' + c.Id + '"}');
	    String chatDetails = '{"visitorId":"1","customDetails":[{"label":"LanguagePreference","value":"en_US","entityMaps":[],"displayToAgent":true,"transcriptFields":[]}]}';
		pr.getParameters().put('chatDetails', chatDetails);
		ctrl = new PostChatSurveyCon();
		System.assert(!ctrl.isError);
		System.assertNotEquals(null, ctrl.tryRedirecting());
	}
}