/**
 * Base member able to redirect to a survey
 * @author Fernando Gomez
 * @version 1.0
 * @since Apr 10th
 */
public abstract class SurveyRedirectorBase extends HelpPortalBase {
	public Id caseId { get; protected set; }
	public Id surveyId { get; protected set; }
	public Id contactId { get; protected set; }
	public Id leadId { get; protected set; }

	/**
	 * Base constructor
	 */
	public SurveyRedirectorBase() {
		super();
	}

	/**
	 * Takes the user to the specified survey
	 * @return a page reference to the following page
	 */
	public PageReference goToSurvey() {
		PageReference result = Page.TakeSurvey;
		result.getParameters().putAll(
			new Map<String, String> {
				'id' => surveyId,
				'caId' => caseId,
				'cId' => contactId,
				'lId' => leadId,
				'language' => language,
				'datetime' => String.valueOf(System.now().getTime())
			});
		result.setRedirect(false);
		return result;
	}

	/**
	 * @return the case witrh the specified id. 
	 * Only relevant info is queried.
	 */
	protected Case getCase(Id caseId) {
		try {
			return [
				SELECT Id,
					CaseNumber,
					Subject,
					ContactId,
					Lead__c,
					FeedbackReceived__c,
					FeedbackSentOn__c,
					FeedbackReminderSentOn__c
				FROM Case
				WHERE Id = :caseId
			];
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @return the survey that should be taken
	 */
	protected Survey__c getSurvey(String surveyName) {
		try {
			return [
				SELECT Id, Name
				FROM Survey__c
				WHERE Name = :surveyName
			];
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @return the current survey
	 */
	protected String getSurveyName() {
		return SurveySettings__c.getOrgDefaults().CurrentSurveyName__c;
	}
}