/* Name: Client4GRequestHandler
 * Description : Apex Class to download account,case,chat Information and all the documents for particular customer.
 *               Documents will be emailed to the Id mentioned in global settings
 * Author: vanglirajan kalimuthu
 * Date : 2020-06-03
 */
global with sharing Class Client4GRequestHandler
{
    public static Map<Id, string> documentsToPrint = new Map<Id, string>{};    
    public static set<Id> documentIds;
    
    webservice static void emailClientRecords(Id accountId)
    {
        set<Id> relatedIds = new set<Id>{accountId};
        Attachment[] documents = new Attachment[]{};
		
        Account ac = [select Name, fxAccount__c from Account where Id =: accountId Limit 1];
        Id fxaccountId = ac.fxAccount__c;
        
        Map<Id,Case> caseMap = new Map<Id,Case>([select Id, 
                                                        Subject,
                                                        (Select Id FROM LiveChatTranscripts)
                                                 From Case
                                                 Where AccountId =:accountId]);
        
       
        documentsToPrint.put(accountId,'Account Details');
        getCaseRecords(caseMap.values());
        getChatTranscriptRecords(caseMap.values());
      	documentIds = getdocumentIds(accountId, fxaccountId,caseMap.keySet());
        
        Map<string, Map<Id, string>> batchDocumentsToProcess = new  Map<string, Map<Id, string>>{};
        Map<Id, string> documentsToProcess = new Map<Id, string>{};
		
        integer marker = 1;
        for (String relatedParentId : documentsToPrint.keySet())
        {
            documentsToProcess.put(relatedParentId, documentsToPrint.get(relatedParentId));
            if(documentsToProcess.size() == 100 || marker == documentsToPrint.size())
            {
                string key = 'Batch_' + batchDocumentsToProcess.size() +1;
                batchDocumentsToProcess.put(key, documentsToProcess);
                documentsToProcess = new Map<Id, string>{};
            }
            marker +=1;
        }
        
        for (String batchId : batchDocumentsToProcess.keySet())
        {
           	Map<Id, string> docsToPrint = batchDocumentsToProcess.get(batchId);
            Client4GRequestEmailQueueable q = new Client4GRequestEmailQueueable(ac.Name, docsToPrint, null);
            system.enqueueJob(q);
        }       
        if(documentIds.size() > 0)
        {
            Client4GRequestEmailQueueable q = new Client4GRequestEmailQueueable(ac.Name, null, documentIds);
            system.enqueueJob(q);
        }
    }

    public static void getCaseRecords(Case[] cases)
    {
        Map<Id, string> caseRecords = new Map<Id, string>{};
        for(integer i=0; i < cases.size(); i++)
        {     
            documentsToPrint.put(cases[i].Id, 'Case_' + (i+1) + '_' + cases[i].Subject);
        }
    }
    public static void getChatTranscriptRecords(Case[] cases)
    {
        Map<Id, string> liveChatTranscriptRecords = new Map<Id, string>{};
        integer chatTransCounter = 1;
        for(integer i=0; i < cases.size(); i++)
        { 
            if(cases[i].LiveChatTranscripts != null && cases[i].LiveChatTranscripts.size() >0)
            {
                documentsToPrint.put(cases[i].LiveChatTranscripts[0].Id, 'ChatTranscripts_' + (chatTransCounter++));
            }
    	}
    }

    public static set<Id> getdocumentIds(Id accountId, Id fxaccountId, set<Id> caseIds)
    {
        Attachment[] attchDocs = new Attachment[]{};
            
        Map<Id,Document__c> docsMap = new Map<Id,Document__c>([select Id 
                                                               from Document__c 
                                                               where Account__C =:accountId OR 
                                                                     fxAccount__c=:fxaccountId OR
                                                                     Case__c IN :caseIds]);
        return docsMap.keyset();
    }

    public class AttachmentWrapper 
    {
         public String attEncodedBody {get; set;}
         public String attName {get; set;}
     }
}