/**
 * This class provides general functionalities to the Org's triggers
 */
public class TriggersUtil {

    // Objects to manage triggers
    public enum Obj {
        LEAD,
        ACCOUNT,
        FXACCOUNT,
        DOCUMENT,
        XCASE,
        ATTACHMENT,
        LIVECHATTRANSCRIPT
    }

    // Triggers
    public enum Triggers {
        TRIGGER_LEAD_BEFORE,
        TRIGGER_LEAD_AFTER,
        TRIGGER_ACCOUNT_BEFORE,
        TRIGGER_ACCOUNT_AFTER,
        TRIGGER_FXACCOUNT,
        TRIGGER_FXACCOUNT_BEFORE,
        TRIGGER_FXACCOUNT_AFTER,
        TRIGGER_DOCUMENT_BEFORE,
        TRIGGER_DOCUMENT_AFTER,
        TRIGGER_CASE_BEFORE,
        TRIGGER_CASE_AFTER,
        TRIGGER_ATTACHMENT_BEFORE,
        TRIGGER_ATTACHMENT_AFTER,
        TRIGGER_LIVECHATTRANSCRIPT_BEFORE,
        TRIGGER_LIVECHATTRANSCRIPT_AFTER,
        TRIGGER_TASK_BEFORE,
        TRIGGER_TASK_AFTER
        /* Add more here...*/}

    // Trigger by objects
    public static Map<Obj, List<Triggers>> objTriggersMap =
        new Map<Obj, List<Triggers>>{
            Obj.LEAD => new List<Triggers>{
                Triggers.TRIGGER_LEAD_BEFORE,
                Triggers.TRIGGER_LEAD_AFTER
            },
            Obj.ACCOUNT => new List<Triggers>{
                Triggers.TRIGGER_ACCOUNT_BEFORE,
                Triggers.TRIGGER_ACCOUNT_AFTER
            },
            Obj.FXACCOUNT => new List<Triggers>{
                Triggers.TRIGGER_FXACCOUNT,
                Triggers.TRIGGER_FXACCOUNT_BEFORE,
                Triggers.TRIGGER_FXACCOUNT_AFTER
            },
            Obj.DOCUMENT => new List<Triggers>{
                Triggers.TRIGGER_DOCUMENT_BEFORE,
                Triggers.TRIGGER_DOCUMENT_AFTER
            },
            Obj.XCASE => new List<Triggers>{
                Triggers.TRIGGER_CASE_BEFORE,
                Triggers.TRIGGER_CASE_AFTER
            },
            Obj.ATTACHMENT => new List<Triggers>{
                Triggers.TRIGGER_ATTACHMENT_BEFORE,
                Triggers.TRIGGER_ATTACHMENT_AFTER
            },
            Obj.LIVECHATTRANSCRIPT => new List<Triggers>{
                Triggers.TRIGGER_LIVECHATTRANSCRIPT_BEFORE,
                Triggers.TRIGGER_LIVECHATTRANSCRIPT_AFTER
            }
            /* Add more here...*/
        };
    
      // Trigger by objectType
      public static Map<Schema.SObjectType, List<Triggers>> objTypeTriggersMap =
      new Map<Schema.SObjectType, List<Triggers>>{
        Schema.fxAccount__C.getSObjectType()  => new List<Triggers>{
            Triggers.TRIGGER_FXACCOUNT,
            Triggers.TRIGGER_FXACCOUNT_BEFORE,
            Triggers.TRIGGER_FXACCOUNT_AFTER
        },
        Schema.Lead.getSObjectType()  => new List<Triggers>{
            Triggers.TRIGGER_LEAD_BEFORE,
            Triggers.TRIGGER_LEAD_AFTER
        },
        Schema.Account.getSObjectType() => new List<Triggers>{
              Triggers.TRIGGER_ACCOUNT_BEFORE,
              Triggers.TRIGGER_ACCOUNT_AFTER
        },
        Schema.Case.getSObjectType()  => new List<Triggers>{
            Triggers.TRIGGER_CASE_BEFORE,
            Triggers.TRIGGER_CASE_AFTER
        },
        Schema.Document__c.getSObjectType() => new List<Triggers>{
            Triggers.TRIGGER_DOCUMENT_BEFORE,
            Triggers.TRIGGER_DOCUMENT_AFTER
        },
        Schema.Attachment.getSObjectType() => new List<Triggers>{
            Triggers.TRIGGER_ATTACHMENT_BEFORE,
            Triggers.TRIGGER_ATTACHMENT_AFTER
        },
        Schema.LiveChatTranscript.getSObjectType() => new List<Triggers>{
            Triggers.TRIGGER_LIVECHATTRANSCRIPT_BEFORE,
            Triggers.TRIGGER_LIVECHATTRANSCRIPT_AFTER
        }
          /* Add more here...*/
      };

    // Disabled triggers
    private static Set<Triggers> disabledTriggers =
        new Set<Triggers>();

    /**
     * This determine if the given trigger is enabled
     * for the current transaction
     */
    public static Boolean isTriggerEnabled(Triggers t){
        return !disabledTriggers.contains(t) &&
            areEnabledForUser();
    }

     /**
     * This method disable the given triggers
     * for the current transaction
     */
    public static void disableTriggers(List<Triggers> ts){
        for(Triggers t: ts)
            disableTrigger(t);
    }

    /**
     * This method disable all triggers for the given
     * object for the current transaction
     */
    public static void disableObjTriggers(Obj obj){
        for(Triggers t: objTriggersMap.get(obj))
            disableTrigger(t);
    }

      /**
     * This method disable all triggers for the given
     * object for the current transaction
     */
    public static void disableTriggers(Schema.SObjectType sobjType){
        for(Triggers t: objTypeTriggersMap.get(sobjType))
            disableTrigger(t);
    }

    /**
     * This method disable the given trigger for the
     * current transaction and on
     */
    public static void disableTrigger(Triggers t){
        disabledTriggers.add(t);
    }

    /**
     * This method enable the given triggers for 
     * the current transaction
     */
    public static void enableTriggers(List<Triggers> ts){
        for(Triggers t :ts)
            enableTrigger(t);
    }

    /**
     * This method enable all triggers for the given
     * object for the current transaction
     */
    public static void enableObjTriggers(Obj obj){
        for(Triggers t: objTriggersMap.get(obj))
            enableTrigger(t);
    }

    public static void enableTriggers(Schema.SObjectType sobjType){
        for(Triggers t: objTypeTriggersMap.get(sobjType))
            enableTrigger(t);
    }

    /**
     * This method enables the given trigger for
     * the current transaction and on
     */
    public static void enableTrigger(Triggers t){
        if(disabledTriggers.contains(t)){
            disabledTriggers.remove(t);
        }
    }

    /**
	 * Know if triggers are enabled for current user
	 */
	public static Boolean areEnabledForUser() {
		return UserUtil.areTriggersEnabledForUser();
	}

	/**
	 * Know if triggers are enabled for some user
	 */
	public static Boolean areEnabledForUser(
        User usr)
    {
		return
            UserUtil.areTriggersEnabledForUser(usr);
	}
   
}