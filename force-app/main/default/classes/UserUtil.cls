public with sharing class UserUtil {
	static final String USERNAME_SYSTEM_USER = 'system.user@oanda.com.dev1';
	public static final String NAME_SYSTEM_USER = 'System User';
	public static final String NAME_MARKETO_USER = 'Marketo User';
	public static final String NAME_REGISTRATION_USER = 'Registration User';
	public static final String NAME_INTEGRATION_USER = 'Integration User';
	public static final String NAME_RETENTION_HOUSE_USER = 'Retention House User';
	public static final String USER_ID_PREFIX = '005';
	
	public static final String NAME_FXS_PROFILE = 'Forex Specialist';
	public static final String NAME_OB_PROFILE = 'OB';
	public static final String NAME_OB_ALL_SEC_PROFILE = 'OB - All Sec';
	public static final String NAME_AML_PROFILE = 'AML';
	public static final String NAME_SU_PROFILE = 'System Admin - Integration';
	public static final String NAME_SA_PROFILE = 'System Administrator';
	public static final String NAME_RETENTION_PROFILE = 'Retention';
	public static final String NAME_INTEGRATION_PROFILE = 'Integration';
	public static final String NAME_INTEGRATION_OJ_PROFILE = 'Integration - OJ';
	public static final String NAME_REGISTRATION_PROFILE = 'Registration';
	public static final String NAME_INTEGRATION_ALLSEC_PROFILE = 'Integration - All Sec';
	public static final String NAME_INTEGRATION_MARKETO_PROFILE = 'Integration - Marketo';
	
	static Id SystemUserId;
	static Map<String, Id> profileIds;
	static SeT<Id> integrationProfileIds;
	
	static Map<String, User> userByUsername = new Map<String, User>();
	static Map<String, User> userByName = new Map<String, User>();
	static Map<Id, User> userById;
	
	static Set<Group> queueGroups;
	static List<User> allUsers;
	static string currentUserRoleName;
	
	public static User currentUser
    {
       get 
       {
        	if(currentUser == null) {
				refreshCurrentUser();
           	}
           
			return currentUser;
       }
	   set;
    }
    
	public static Id getUserId(String username)
	{
		User u = getUser(username);
		
		if (u == null) {
			return null;
		}
		
		return u.Id;
	}
	
	public static Id getUserIdByName(String fullname)
	{
		User u = getUserByName(fullname);
		
		if (u == null) {
			return null;
		}
		
		return u.Id;
	}
	
	public static User getUserByName(String fullname) {
		User u = userByName.get(fullname);
		
		if (u == null) {
			User[] users  = [SELECT Id, Username, Name FROM User WHERE Name = :fullname order by CreatedDate Desc];
			if(users.size() > 0)
			{
				u = users[0];
				userByUsername.put(u.Username, u);
				userByName.put(u.Name, u);
				return u;
			}
			return null;
		}
		
		return u;
	}
	
	public static User getUser(String username) {
		User u = userByUsername.get(username);
		
		if (u == null) {
			u = [SELECT Id, Username, Name FROM User WHERE Username = :username];
			if(u != null)
			{
				userByUsername.put(username, u);
				userByName.put(u.Name, u);
				return u;
			}
			return null;
		}
		
		return u;
	}

	public static User getSystemUserForTest() {
		User u = getUserByName(NAME_SYSTEM_USER);
		if(u==null) {
			u = new User(Username=USERNAME_SYSTEM_USER, LastName=NAME_SYSTEM_USER);
			insert u;
		}
		return u;
	}

	public static User getSystemUserWithPermissionForTest(String lastName, String userName, String permissionSetName) {
		User u = getUserByName(lastName);
		if(u==null) {
			u = new User(
					LastName=lastName,
					Username=username,
					Email='testuser@oanda.com',
					Alias='testuser',
					ProfileId=[ SELECT Id FROM Profile WHERE Name = 'System Administrator' ].Id,
					TimeZoneSidKey='GMT',
					LocaleSidKey='en_US',
					EmailEncodingKey='ISO-8859-1',
					LanguageLocaleKey='en_US',
					IsActive=true);
			insert u;
			PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = :permissionSetName LIMIT 1];
			PermissionSetAssignment newAssignment = new PermissionSetAssignment(
					AssigneeId = u.Id,
					PermissionSetId = permissionSet.Id
			);
			insert newAssignment;
		}
		return u;
	}
	
	public static User getTestUser() {
		return getTestUser('testuser@oanda.com');
	}
	
	public static User getRandomTestUser() {
		Integer random = Math.round(Math.random() * 1000000);
		User u = getTestUser('user' + random + '@oanda.com');
		insert u;
		return u;
	}

	public static User getTestUser(String username) {
		return new User(
	   		LastName='test user', 
	   		Username=username, 
	   		Email='testuser@oanda.com', 
	   		Alias='testuser', 
	   		ProfileId=[ SELECT Id FROM Profile WHERE Name = 'Forex Specialist' ].Id,
	   		TimeZoneSidKey='GMT', 
	   		LocaleSidKey='en_US', 
	   		EmailEncodingKey='ISO-8859-1', 
	   		LanguageLocaleKey='en_US',
	   		IsActive=true);
	}
	
	public static Id getSystemUserId() {
		return getUserIdByName(NAME_SYSTEM_USER);
	}
	
	public static Id getMarketoUserId() {
		return getUserIdByName(NAME_MARKETO_USER);
	}

	public static Id getRegistrationUserId() {
		return getUserIdByName(NAME_REGISTRATION_USER);
	}

	public static Id getIntegrationUserId() {
		return getUserIdByName(NAME_INTEGRATION_USER);
	}

	public static Id getRetentionUserId() {
		return getUserIdByName(NAME_RETENTION_HOUSE_USER);
	}

	public static boolean isIntegrationUserId(Id i) {
		return getIntegrationUserIds().contains(i);
	}
	
	public static Set<Id> getIntegrationUserIds() {
		return new Set<Id> {getSystemUserId(), getMarketoUserId(), getRegistrationUserId(), getIntegrationUserId()};
	}
	
	public static Boolean isUserId(Id userOrQueueId) {
		return ((String) userOrQueueId).startsWith(USER_ID_PREFIX);
	}
	
	private static Set<Group> getQueueGroups() {
		if (queueGroups == null) {
			queueGroups = new Set<Group>();
			for (Group queue : [SELECT Id, Name FROM Group WHERE Type = 'queue']) {
				queueGroups.add(queue);
			}
		}
		
		return queueGroups;
	}

	public static Boolean isQueueUser(Id userId) {
		if(String.isBlank(userId))
			return false;
		return ((String)userId).startsWith('00G');

		// for (Group queue : getQueueGroups()) {
		// 	if (queue.Id == userId) {
		// 		return true;
		// 	}
		// }
		// return false;
	}
	
	public static Boolean isQueueMember(Id queueId, Id userId) {
		Map<Id, Set<Id>> userIdsByQueueId = getQueueUsers();

		for (Id queueUserId : userIdsByQueueId.get(queueId)) {
			if (queueUserId == userId) {
				return true;
			}
		}
		
		return false;
	}

	public static Id getQueueId(String queueName) {
		for (Group queue : getQueueGroups()) {
			if (queue.Name == queueName) {
				return queue.Id;
			}
		}
		
		return null;
	}
	
	public static String getQueueName(Id queueId) {
		for (Group queue : getQueueGroups()) {
			if (queue.Id == queueId) {
				return queue.Name;
			}
		}
		
		return null;
	}
	
	public static Set<Id> getActiveUserIds() {
		if (allUsers == null) {
			allUsers = [SELECT Id, IsActive FROM User];
		}
		
		Set<Id> activeUserIds = new Set<Id>();
		for (User u : allUsers) {
			if (u.IsActive) {
				activeUserIds.add(u.Id);
			}
		}
		
		return activeUserIds;
	}
	
	// Cache the result so we only need to query once per execution context
	static Map<Id, Set<Id>> userIdsByQueueId;
	
	public static Map<Id, Set<Id>> getQueueUsers() {
		
		if (userIdsByQueueId != null) {
			return userIdsByQueueId;
		}
		
		
		userIdsByQueueId = new Map<Id, Set<Id>>();
		
		List<GroupMember> groupMembers = [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN (SELECT Id FROM Group WHERE Type = 'queue')];
		
		Set<Id> userIds = new Set<Id>();
		for (GroupMember groupMember : groupMembers) {
			userIds.add(groupMember.UserOrGroupId);
		}
		
		// Query for only those active users
		Map<Id, User> activeUsersById = new Map<Id, User>([SELECT Id FROM User WHERE Id IN :userIds AND IsActive = true]);
		
		for (GroupMember groupMember : groupMembers) {
			
			// Only add active users to the mapping
			if (!activeUsersById.containsKey(groupMember.UserOrGroupId)) {
				continue;
			}
			
			Set<Id> userIdsInQueue = userIdsByQueueId.get(groupMember.GroupId);
			if (userIdsInQueue == null) {
				userIdsInQueue = new Set<Id>();
				userIdsByQueueId.put(groupMember.GroupId, userIdsInQueue);
			}
			userIdsInQueue.add(groupMember.UserOrGroupId);
		}
		
		return userIdsByQueueId;
	}
	
	static Map<Id, User> getUserById() {
		if(userById==null) {
			userById = new Map<Id, User>([SELECT Id, Username, Name, IsActive, ProfileId FROM User WHERE IsActive=true]);
		}
		return userById;
	}
	
	public static Boolean isActive(Id userId) {
		User u = getUserById().get(userId);
		return (u==null ? false : u.IsActive);
	}
	
	private static void loadProfileIds() {
		if (profileIds != null) {
			return;
		}
		
		// Profiles names
		Set<String> profileNames = new Set<String>{
			NAME_FXS_PROFILE,
			NAME_OB_PROFILE,
			NAME_OB_ALL_SEC_PROFILE,
			NAME_AML_PROFILE,
			NAME_SU_PROFILE,
			NAME_SA_PROFILE,
			NAME_RETENTION_PROFILE,
			NAME_INTEGRATION_PROFILE,
			NAME_INTEGRATION_OJ_PROFILE,
			NAME_REGISTRATION_PROFILE,
			NAME_INTEGRATION_ALLSEC_PROFILE,
			NAME_INTEGRATION_MARKETO_PROFILE};

		List<Profile> profiles =
			[SELECT Id,
					Name
				FROM Profile
				WHERE Name IN :profileNames];
		
		profileIds = new Map<String, Id>();
		
		for (Profile profile : profiles) {
			profileIds.put(profile.Name, profile.Id);
		}
	}
	
	public static Id getForexSpecialistProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_FXS_PROFILE);
	}
	
	public static Id getOBProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_OB_PROFILE);
	}

	public static Id getOBAllSecProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_OB_ALL_SEC_PROFILE);
	}

	public static Id getAMLProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_AML_PROFILE);
	}

	public static Id getSystemUserProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_SU_PROFILE);
	}
	
	public static Id getRegistrationProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_REGISTRATION_PROFILE);
	}
	
	public static Id getSystemAdministratorProfileId() {
		loadProfileIds();
		return profileIds.get(NAME_SA_PROFILE);
	}
	
	public static boolean isSystemAdministratorProfile(Id userId) {
		User u = getUserById().get(userId);
		return u.ProfileId == getSystemAdministratorProfileId();
	}
	
	public static Set<Id> getAdminUserIds() {
		System.debug([SELECT Username FROM User]);
		Map<Id, User> users = new Map<Id, User>([SELECT Id FROM User WHERE Username IN ('rfarr@oanda.com', 'ahwang@oanda.com') AND IsActive = true]);
		Set<Id> ids = new Set<Id>(users.keySet());
		ids.add(getSystemUserId());
		
		return ids;
	}
	
	//check if the user is a retention user based on profile
	public static boolean isRetentionUser(Id userId){
		User u = getUserById().get(userId);
		
		loadProfileIds();
		ID retenProfileId = profileIds.get(NAME_RETENTION_PROFILE);
		
		boolean result = false;
		if(u != null && u.ProfileId == retenProfileId){
			result = true;
		}
		
		return result;
	}
	
	/**
	 * Get integration profile ids 
	 *
	 */
	public static set<Id> getIntegrationProfileIds(){
		loadProfileIds();
		
		if(integrationProfileIds != null)
			return integrationProfileIds;

		integrationProfileIds = new set<Id>();
		
		integrationProfileIds.add(profileIds.get(NAME_INTEGRATION_PROFILE));
		integrationProfileIds.add(profileIds.get(NAME_INTEGRATION_OJ_PROFILE));
		integrationProfileIds.add(profileIds.get(NAME_REGISTRATION_PROFILE));
		integrationProfileIds.add(profileIds.get(NAME_INTEGRATION_ALLSEC_PROFILE));
		integrationProfileIds.add(profileIds.get(NAME_INTEGRATION_MARKETO_PROFILE));
		
		return integrationProfileIds;
	}

	/**
	 * Know if triggers are enabled for current user
	 */
	public static Boolean areTriggersEnabledForUser() {
        return
			areTriggersEnabledForUser(currentUser);
    }

	/**
	 * Know if triggers are enabled for some user
	 */
	public static Boolean areTriggersEnabledForUser(
		User usr)
	{
        return
			usr != null &&
            !usr.Triggers_Disabled__c;
    }

	/**
	 * Know if user is an "integration" one
	 */
	public static Boolean isRegistrationUser() {
        return
			currentUser != null && 
            currentUser.ProfileId ==
				getRegistrationProfileId();
    }

	/**
	 * Know if user is an "integration" one
	 */
	public static Boolean isIntegrationUser() {
        return
			currentUser != null &&
			isIntegrationUser(
				currentUser.ProfileId);
    }

	/**
	 * Know if user is an "integration" one
	 */
	public static Boolean isIntegrationUser(
		Id profileId)
	{
        return getIntegrationProfileIds().contains(
            profileId);
    }
    
    public static string getCurrentUserRole()
    {
        if(string.isBlank(currentUserRoleName))
        {
            Id userId = Userinfo.getUserId();
            User u = [select Id, name, userRole.name  from user where Id = :userId Limit 1];
            currentUserRoleName = u.userRole.name != null ? u.userRole.name : '';
        }
        return currentUserRoleName;
    }

	/**
	 * @return true if the specified record is
	 * owner by a User (not a User Group or Queue).
	 */
	public static Boolean isOwnedByUser(SObject record) {
		return ((Id)record.get('OwnerId'))
			.getSObjectType()
			.getDescribe()
			.getName() == 'User';
	}

	/**
	 * Refresh current user.
	 * Note: It is testVisible because sometime
	 * when using ' System.runAs()' in test methods
	 * you could need to refresh the current user
	 * to the new one running now.
	 */
	@testVisible
	private static void refreshCurrentUser() {
		List<User>	usrs =
			[SELECT Id, 
					Name, 
					ProfileId,
					UserRole.name,
					Triggers_Disabled__c,
					Process_Builder_Disabled__c
				FROM User 
				WHERE Id = :Userinfo.getUserId()
				LIMIT 1];

		if(!usrs.isEmpty())
			currentUser = usrs[0];
	}

	public static Boolean canUserModifyFeedComments(){
		Id userId =UserInfo.getUserId();
		List <Feed_Comment__mdt> feedCommentsMdt = [SELECT User_ID__c FROM Feed_Comment__mdt WHERE User_ID__c =:userId];
		if(feedCommentsMdt.isEmpty()) return false;
		return true;
	}
}