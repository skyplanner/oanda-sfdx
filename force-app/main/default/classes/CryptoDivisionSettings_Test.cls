/**
 * @File Name          : CryptoDivisionSettings_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/13/2022, 5:27:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/13/2022, 5:23:09 PM   acantero     Initial Version
**/
@IsTest
private without sharing class CryptoDivisionSettings_Test {

    @IsTest
    static void exists() {
        String divisionCode = 'OC';
        Test.startTest();
        Boolean result = 
            CryptoDivisionSettings.exists(divisionCode);
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid result');
    }

    @IsTest
    static void getDivisionCodeList() {
        Test.startTest();
        List<String> result = 
            CryptoDivisionSettings.getDivisionCodeList();
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid result');
    }
    
}