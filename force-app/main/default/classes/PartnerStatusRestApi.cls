/* Name: LeadApiRestResource
 * Description : upsert demo/marketing lead or throw error
 * Author: Mikolaj Juras
 * Date : 2024 Aug 2
 */
@RestResource(urlMapping='/api/v1/partners/*/status')
global without sharing class PartnerStatusRestApi {


    private static final String ENDPOINT_NAME = 'partner_status';
    private static final String LOGGER_CATEGORY_URL = '/api/v1/partners/*/status';
    private static final String PARTNER_ID_FIELD = 'partner_id';
    public static final List<String> GET_PARAMS = new List<String>{PARTNER_ID_FIELD};
    private static final String AFFILIATE_ID_PREFIX = 'a1z';
    private static final String BROKER_NUMBER_FIELD = 'Broker_Number__c';
    private static final String ID_FIELD = 'Id';
    


    @HttpGet
    global static void doGet() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, GET_PARAMS);
        Boolean isAffilieateId;
        String partnerId;
        String query;
        List<Affiliate__c> affiliates;

        try {
            partnerId = String.escapeSingleQuotes(String.valueOf(context.getPathParam('partner_id')));
            isAffilieateId = partnerId.startsWith(AFFILIATE_ID_PREFIX) && partnerId.length() >= 15;
            String whereField = isAffilieateId ? ID_FIELD : BROKER_NUMBER_FIELD;
            query = 'SELECT Id, Status__c from Affiliate__c WHERE ' + whereField + ' =:partnerId';
            affiliates = Database.query(query);
            
            if(affiliates.isEmpty()) {
            context.setResponse(404, 'Could not find Affiliate with provided partner Id: ' + partnerId);
            return;
            }
            PartnerStatus ps = new PartnerStatus();
            ps.partner_id = affiliates[0].Id;
            ps.status = affiliates[0].Status__c;

            context.setResponse(200, ps);

            return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString() +' Partner Id: '+ partnerId;
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            System.debug('Mj err: ' + respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

    global class PartnerStatus {
        public string partner_id { get; set; }
        public string status { get; set; }
    }
}