public with sharing class ApiMappingService {

    public ApiMappingService(String endpointName) {
        this.endpointName = endpointName;
        this.endpointMappings = 
            new EndpointsMappingsUtil().getEnpointsMappingByEndpoint(endpointName);
    }
    
    public ApiMappingService() {}

    private String endpointName;

    @TestVisible
    private Map<String,Map<String,Endpoints_Mappings__mdt>>  endpointMappings;

    public Map<String,List<SObject>> newObjects = new Map<String, List<SObject>>();

    public String loggerCategory;
    
    public String loggerCategoryUrl;

    public void setInputFieldstoSobject(SObject currentSobject, Map<String, Object> payload) {

        Map<String, Endpoints_Mappings__mdt> currentObjMapping = 
            this.endpointMappings.get(currentSobject.getSobjectType().getDescribe().getName());

        Map<String,Object> payloadCopy = new Map<String,Object>(payload);

        for(String payloadField : currentObjMapping.keySet()) {
            Endpoints_Mappings__mdt currentFieldMapping = currentObjMapping.get(payloadField);

            setInputFieldtoSobject(currentSobject, currentFieldMapping, payload);
            payloadCopy.remove(payloadField);
        }

        for(String field : payloadCopy.keySet()) {
            Logger.error(
                this.loggerCategoryUrl ?? this.endpointName,
                this.loggerCategory ?? 'ApiMappingService',
                ' The following field name ' + field 
                + ' field value:  ' + payloadCopy.get(field) + ' for object: ' + currentSobject 
                + ' was not mapped' +
                + ' Update performed by ' + UserInfo.getName());
        }
    }

    public void setInputFieldtoSobject(SObject currentSobject, SObject metadataMapping, Map<String, Object> payload ) {
        
        String inputField;
        String dataType;
        String sfFieldName;
        Object value;

        try {
            inputField = (String) metadataMapping.get('Input_Field_Name__c');
            dataType = (String) metadataMapping.get('Data_Type__c');
            sfFieldName = (String) metadataMapping.get('Sobject_Field_Name__c');
            value = payload.get(inputField);

            switch on dataType {
                when 'Integer' {
                    currentSobject.put(sfFieldName, Integer.valueOf(value));
                }
                when 'String' {
                    currentSobject.put(sfFieldName, String.valueOf(value));
                }
                when 'NORMALIZE_DATE' {
                    String dataSeparatorRegex = '\\/|\\.'; // match to replace slash and dot
                    String rawDate = String.valueOf(value);
                    currentSobject.put(sfFieldName, Date.valueOf(rawDate.replaceAll(dataSeparatorRegex, '-')));
                }
                when else {
                    currentSobject.put(sfFieldName, value);
                }
            }
        } catch (Exception ex) {
            Logger.error(
                this.loggerCategoryUrl,
                this.loggerCategory,
                ' The following field name ' + sfFieldName 
                + ' field value:  ' + value + ' for object: ' + currentSobject 
                + ' was not mapped. Err msg: ' + ex.getMessage() 
                + ' Update performed by ' + UserInfo.getName());
        }
    }
}