/**
 * Created by mcasella on 2019-06-06.
 */

@isTest
private class NF_UpdateCaseTest {

    @isTest
    static void testUpdateCase() {
        Test.startTest();

        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;

        LiveChatTranscript tx = new LiveChatTranscript(
            chatKey = '123',
            liveChatVisitorId = visitor.id
        );

        insert tx;

        Test.stopTest();

        system.assertEquals(true, true);
    }
}