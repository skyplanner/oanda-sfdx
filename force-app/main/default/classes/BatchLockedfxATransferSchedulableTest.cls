@isTest
private class BatchLockedfxATransferSchedulableTest {

  final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

  @testSetup static void init(){

    //Create test data
    
    TestDataFactory testHandler = new TestDataFactory();

    List<Account> accntList = testHandler.createTestAccounts(1);
      List<Opportunity> opptyList = testHandler.createOpportunities(accntList);
    List<fxAccount__c> fxaList = testHandler.createFXTradeAccounts(accntList);

    for(Integer k = 0; k<1; k++){
      fxaList[k].RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
      fxaList[k].Opportunity__c = opptyList[k].Id;
      fxaList[k].Funnel_Stage__c = FunnelStatus.FUNDED;
      fxaList[k].Division_Name__c = 'OANDA Corporation';
      fxaList[k].Deposit_USD_MTD__c = 1000000;
      fxaList[k].Account_Locked__c = true;
    }

    update fxaList;

    for(Integer k = 0; k<1; k++){
      opptyList[k].RecordTypeId = RecordTypeUtil.getSalesOpportunityTypeId();
      opptyList[k].StageName = FunnelStatus.FUNDED;
      opptyList[k].CloseDate = Date.today();
      opptyList[k].fxAccount__c = fxaList[k].Id;
    }

    update opptyList;

  }
  
  @isTest static void testlockaccountTransfer() 
  {
    Test.startTest();
    Id suid= UserUtil.getSystemUserId();
    BatchLockedfxATransferSchedulable batch=  new BatchLockedfxATransferSchedulable('Select Id,FXAIDonRelatedOpp__c,Opportunity__c from fxAccount__c where Account_Locked__c = true AND  ownerid <> \''+suid+'\'') ;

    Database.executeBatch(batch,20);
    Test.stopTest();
    List<fxAccount__c> fxsReassigned = [Select Id,OwnerId from fxAccount__c where Account_Locked__c = true ];
    
    system.assertEquals(suid, fxsReassigned[0].OwnerId);

  }

}