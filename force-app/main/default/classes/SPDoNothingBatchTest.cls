/**
 * @File Name          : SPDoNothingBatchTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2023, 1:59:22 AM
**/
@IsTest
private without sharing class SPDoNothingBatchTest {

    @TestSetup
    static void setup() {
        // Log__c is used by SPDoNothingBatch
        insert new Log__c(
            Id__c = '1'
        );
    }

    @IsTest
    static void test() {
        Test.startTest();
        Database.executeBatch(new SPDoNothingBatch(), 1);
        Test.stopTest();
        System.assert(true, 'Error');
    }
    
}