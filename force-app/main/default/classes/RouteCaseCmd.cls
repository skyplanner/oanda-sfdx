/**
 * @File Name          : RouteCaseCmd.cls
 * @Description        : Allows you to start routing a case
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/6/2024, 3:18:43 AM
**/
public without sharing class RouteCaseCmd implements Callable {

	public static final String CASE_ID_KEY = 'CASE_ID';
	public static final String CALL_METHOD = 'call';

	public static void invokeWithEvent(ID caseId) {
		new GeneralAction(
			RouteCaseCmd.class.getName(), // className
			CALL_METHOD, // methodName
			new Map<String, Object> {
				CASE_ID_KEY => caseId
			} // data
		).publish();
	}

	public Object call(
		String action, 
		Map<String, Object> args
	) {
		if (
			(args == null) ||
			args.isEmpty()
		) {
			return false;
		}
		// else...
		ID caseId = (ID) args.get(CASE_ID_KEY);
		execute(caseId);
		return true;
	}

	public Boolean execute(ID caseId) {
		if (String.isBlank(caseId)) {
			return false;
		}
		// else...
		Case caseObj = CaseRepo.getRoutingInfoById(caseId);
		List<Case> caseList = new List<Case> {caseObj};
		CaseRoutingManager routingManager = CaseRoutingManager.getInstance();
		routingManager.routeUsingSkills(caseList);
		return true;
	}

}