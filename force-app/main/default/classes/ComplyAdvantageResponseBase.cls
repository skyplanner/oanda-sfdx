/**
 * Wrapper for Comply advantage JSON:
 * Response JSON from CA
 * @author Fernando Gomez
 * @version 1.0
 */
public abstract class ComplyAdvantageResponseBase {
	public Integer code { get; set; }
	public String status { get; set; }

	/**
	 * Returns all entities in the CA search
	 * @param caSearchId
	 * @param searchId
	 * @param hits
	 */
	public List<Comply_Advantage_Search_Entity__c> getEntities(
			Id caSearchId,
			String searchId,
			List<ComplyAdvantageSearchHit> hits) 
	{
		return getEntities(caSearchId,searchId, false, hits);
	}

	public List<Comply_Advantage_Search_Entity__c> getEntities(
			Id caSearchId,
			String searchId,
			Boolean isInternalList,
			List<ComplyAdvantageSearchHit> hits) 
	{
		List<Comply_Advantage_Search_Entity__c> result = new List<Comply_Advantage_Search_Entity__c>();

		if (hits != null && !hits.isEmpty())
		{
			for (ComplyAdvantageSearchHit hit : hits)
			{
				hit.isInternalHit = isInternalList;
				result.add(hit.getSalesforceRecord(caSearchId, searchId));
			}
		}
		return result;
	}
}