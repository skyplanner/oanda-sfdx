public without sharing class EmailComponentController {
    
    
    @AuraEnabled
    public static List<Folder> getFolders(){
        return new List<Folder>([SELECT Id, Name, DeveloperName FROM Folder WHERE Type = 'Email' OR Type = 'EmailTemplate']);
       
    }

    @AuraEnabled 
    public static List<EmailTemplate> getEmailTemplates(Id recordId, List<String> folderNames){

        String objectName = recordId.getSobjectType().getDescribe().getName();
        
        if(Schema.sObjectType.Attachment.isAccessible()){
            // If folder name is included, filter by folder name, else search in all template folders
            if(folderNames != null && folderNames.size() > 0){
                return new List<EmailTemplate>([
                SELECT Subject,
                        Id,  
                        Name,
                        Description,
                        DeveloperName, 
                        FolderId, 
                        Folder.DeveloperName, 
                        Folder.Name,
                	   (Select Id,Name from Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom','text','html') AND Folder.Name IN :folderNames AND (RelatedEntityType = :objectName OR RelatedEntityType = null)
                ORDER BY FolderId, DeveloperName]);
            } else {
                return new List<EmailTemplate>([
                SELECT Subject,
                        Id,  
                        Name,
                        Description,
                        DeveloperName, 
                        FolderId, 
                        Folder.DeveloperName, 
                        Folder.Name,
                	   (Select Id,Name from Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom','text','html') AND Folder.Name <> null AND (RelatedEntityType = :objectName OR RelatedEntityType = null)
                ORDER BY FolderId, DeveloperName]);
            }

            
        }
        return new EmailTemplate[]{};
    }

    @AuraEnabled 
    public static List<EmailTemplate> getTranslatedTemplate(Id recordId, String templateName, String language){

        String objectName = recordId.getSobjectType().getDescribe().getName();
        
        if(Schema.sObjectType.Attachment.isAccessible()){
            return new List<EmailTemplate>([
                SELECT Subject,
                        Id,  
                        Name,
                        Description,
                        DeveloperName, 
                        FolderId, 
                        Folder.DeveloperName, 
                        Folder.Name,
                	   (Select Id,Name from Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom','text','html') AND (RelatedEntityType = :objectName OR RelatedEntityType = null) 
                    AND Name = :templateName AND Description = :language
                ORDER BY LastUsedDate DESC Limit 50]);
        }
        return new EmailTemplate[]{};
    }

    @AuraEnabled 
    public static List<EmailTemplate> getRecentEmailTemplates(Id recordId){

        String objectName = recordId.getSobjectType().getDescribe().getName();
        
        if(Schema.sObjectType.Attachment.isAccessible()){
            return new List<EmailTemplate>([
                SELECT Subject,
                        Id,  
                        Name,
                        Description,
                        DeveloperName, 
                        FolderId, 
                        Folder.DeveloperName, 
                        Folder.Name,
                	   (Select Id,Name from Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom','text','html') AND (RelatedEntityType = :objectName OR RelatedEntityType = null) 
                        AND LastUsedDate != null 
                ORDER BY LastUsedDate DESC Limit 50]);
        }
        return new EmailTemplate[]{};
    }

    @AuraEnabled 
    public static List<EmailTemplate> getRecentEmailTemplateByName(Id recordId, String templateName){

        String objectName = recordId.getSobjectType().getDescribe().getName();
        templateName = '%'+templateName+'%';
        
        if(Schema.sObjectType.Attachment.isAccessible()){
            return new List<EmailTemplate>([
                SELECT Subject,
                        Id,  
                        Name,
                        Description,
                        DeveloperName, 
                        FolderId, 
                        Folder.DeveloperName, 
                        Folder.Name,
                	   (Select Id,Name from Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom','text','html') AND (RelatedEntityType = :objectName OR RelatedEntityType = null) 
                        AND Name LIKE :templateName 
                ORDER BY LastUsedDate DESC Limit 50]);
        }
        return new EmailTemplate[]{};
    }
    
    @AuraEnabled 
    public static EmailMsg getTemplateDetails(string templateId, String whoId, String whatId){
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, whoId, WhatId,Messaging.AttachmentRetrievalOption.METADATA_ONLY);
        EmailMsg msg = new EmailMsg();
        msg.subject = email.getSubject();
        msg.body = email.getHtmlBody();

        List<Messaging.EmailFileAttachment> attachmentList = email.fileAttachments;

        List<FileAttachmentWrapper> fawList = new List<FileAttachmentWrapper>();
        for(Messaging.EmailFileAttachment efa : attachmentList){
           FileAttachmentWrapper faw = new FileAttachmentWrapper();
            faw.attachId = efa.id;
            faw.filename = efa.filename;
            faw.isContentDocument=false;
            fawList.add(faw);
        }
        for(ContentDocumentLink cdl : [Select ContentDocument.Id, ContentDocument.title, ContentDocument.fileExtension
                                       from contentdocumentlink
                                       where linkedEntityId=:templateId]){
            FileAttachmentWrapper faw = new FileAttachmentWrapper();
            faw.attachId = cdl.ContentDocument.id;
            faw.isContentDocument = true;                               
            faw.filename = cdl.ContentDocument.title+'.'+cdl.contentdocument.fileextension;
            fawList.add(faw);
        }
        msg.fileattachments = fawList;
        
        if(String.isblank(msg.body)){
	        msg.body = email.getPlainTextBody();
            if(String.isNotBlank(msg.body)){
                msg.body = msg.body.replace('\n', '<br/>');
            }
        }
        return msg;   
    }
    
    @AuraEnabled  
    public static void deleteFiles(string sdocumentId){ 
        if (Schema.sObjectType.ContentDocument.isDeletable()) {
        	delete [SELECT Id,Title,FileType from ContentDocument WHERE id=:sdocumentId]; 
        }
    }  
    
    @AuraEnabled
    public static void sendAnEmailMsg(string fromAddress, string toAddressesStr, string ccAddressesStr, string bccAddressesStr, string subject, 
                                      string whoId, string whatId, string secondaryWhatId,  string body, String senderDisplayName, List<String> contentDocumentIds,
                                      List<String> attachmentIds,Boolean createActivity, List<String> contentVersionIds, String threadId){ 
        System.debug('fromAddress: ' + fromAddress);
        System.debug('toAddressesStr: ' + toAddressesStr);
        System.debug('ccAddressesStr: ' + ccAddressesStr);
        System.debug('bccAddressesStr: ' + bccAddressesStr);
        System.debug('whoId: ' + whoId);
        System.debug('whatId: ' + whatId);
        System.debug('createActivity: ' + createActivity);
        System.debug('contentDocumentIds: ' + contentDocumentIds);
        System.debug('attachmentIds: ' + attachmentIds);
        System.debug('contentVersionIds: ' + contentVersionIds);
        System.debug('secondaryWhatId: ' + secondaryWhatId);
        System.debug('threadId: ' + threadId);
        try{                                   
            if(String.isNotblank(toAddressesStr) && Schema.sObjectType.ContentVersion.isAccessible()){
                String[] toAddresses = toAddressesStr.split(',');
                String[] fileIds = new String[]{};
                String[] ccAddresses = String.isNotBlank(ccAddressesStr) ? ccAddressesStr.split(',') : new String[]{};
                String[] bccAddresses = String.isNotBlank(bccAddressesStr) ? bccAddressesStr.split(',') : new String[]{};
                List<String> cvIds = new String[]{};
                for(ContentVersion cv : [SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId
                                        FROM ContentVersion
                                        WHERE isLatest = true AND ContentDocumentId IN :contentDocumentIds]){
                    cvIds.add(cv.id);
                }   
                if(contentVersionIds.size() > 0) {
                    body = parseInlineImages(body, contentVersionIds);
                }

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                if(String.isNotBlank(fromAddress)){
                    OrgWideEmailAddress[] owea = new List<OrgWideEmailAddress>([select Id
                                                                                from OrgWideEmailAddress
                                                                                where Address = :fromAddress]);
                    if ( owea.size() > 0 ) {
                        email.setOrgWideEmailAddressId(owea.get(0).Id);
                        email.setUseSignature(false);
                    }
                }
                email.setToAddresses(toAddresses);
                email.setCCAddresses(ccAddresses);
                email.setBCCAddresses(bccAddresses);
                email.setSubject(subject);
                email.sethtmlBody(body);
                email.setUseSignature(false);
                if(String.isNotBlank(senderDisplayName)){
                    email.setSenderDisplayName(senderDisplayName);
                } 
                if(cvIds !=null && !(cvIds.isEmpty())){
                    fileIds.addAll(cvIds);
                }
                if(attachmentIds !=null && !(attachmentIds.isEmpty())){
                    fileIds.addAll(attachmentIds);
                } 
                if(!(fileIds.isEmpty())){
                    email.setEntityAttachments(fileIds);
                }
                email.saveAsActivity = true;
                
                List<Messaging.SendEmailResult> ser =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                if((ser != null && ser.size() >0 && ser[0].isSuccess()) || Test.isRunningTest()){
                    if(createActivity){
                        String fromAddrlog = String.isNotBlank(fromAddress) ? 'From: ' + fromAddress + '\n' : '';
                        String toAddrlog = String.isNotBlank(toAddressesStr) ? 'To: ' + toAddressesStr + '\n': '';
                        String ccAddrlog = String.isNotBlank(ccAddressesStr) ? 'Cc: ' + ccAddressesStr + '\n': '';
                        String bccAddrlog = String.isNotBlank(bccAddressesStr) ? 'BCc: ' + bccAddressesStr + '\n': '';
                        String addressSeparator = '=================================================================='+'\n';
                        List<Task> emailActivities = new List<Task>();
                        Task emailActivity =  new Task(Subject = subject, 
                                                        WhoId = String.isNotBlank(whoId) ? whoId : null, 
                                                        WhatId = String.isNotBlank(whatId) ? whatId : null,
                                                        TaskSubtype = 'Email',
                                                        Status = 'Completed',
                                                        Description = fromAddrlog + toAddrlog +ccAddrlog + bccAddrlog+addressSeparator+
                                                                    (String.isNotBlank(body)? body.stripHtmlTags() : ''),
                                                        ActivityDate = Date.today()
                                                        );
                        emailActivities.add(emailActivity);
                        if(secondaryWhatId != null && secondaryWhatId !=  '' && whatId != secondaryWhatId){
                            Task emailActivityClone = emailActivity.clone(false, true, false, false);
                            if(secondaryWhatId.startsWith('00Q')){
                                emailActivityClone.WhoId = secondaryWhatId;
                                emailActivityClone.WhatId = null;
                            } else {
                                emailActivityClone.WhatId = secondaryWhatId;
                            }
                            emailActivities.add(emailActivityClone);
                        }
                        
                        System.debug('emailActivities: ' + emailActivities);
                        Database.DMLOptions dmo = new Database.DMLOptions();
                        dmo.allowFieldTruncation = true;
                        for(Task t : emailActivities){
                            t.setOptions(dmo);
                        }
                        if(!Test.isRunningTest()){
                            insert emailActivities;
                        }
                    
                    }
                }else{
                    List<Messaging.SendEmailError> errors = ser[0].getErrors();
                    system.debug(JSON.serialize(errors));
                }
            }
        }catch(Exception e){
            throw new AuraHandledException('Something went wrong: ' +e.getMessage());
        }
    }

    public static String parseInlineImages(String body, List<String> versionIds){
        List<ContentDistribution> toInsert = new List<ContentDistribution>();
        for(String versionId : versionIds) {
            toInsert.add(createContentDistribution(versionId));
        }
        if(toInsert.size() > 0) {
            insert toInsert;
        }
        Map<String, ContentDistribution> mapDistributionsByVersion = getMapDistributionsByVersion(versionIds);
        for(String version : versionIds){
            if(mapDistributionsByVersion.get(version) != null && mapDistributionsByVersion.get(version).ContentDownloadUrl != null){
                body = body.replace(version, mapDistributionsByVersion.get(version).ContentDownloadUrl);
            }
            
        }
        return body;
    }

    public static ContentDistribution createContentDistribution(String contentVersionId){
        ContentDistribution cd = new ContentDistribution();
        cd.Name = contentVersionId;
        cd.ContentVersionId = contentVersionId;
        cd.PreferencesAllowViewInBrowser= true;
        cd.PreferencesLinkLatestVersion=true;
        cd.PreferencesNotifyOnVisit=false;
        cd.PreferencesPasswordRequired=false;
        cd.PreferencesAllowOriginalDownload= true;
        return cd;
    }

    public static Map<String, ContentDistribution> getMapDistributionsByVersion(List<String> versionIds){
        Map<String, ContentDistribution> ret = new Map<String, ContentDistribution>();
        List<ContentDistribution> distributions = [SELECT Id, ContentVersionId, ContentDownloadUrl FROM ContentDistribution WHERE ContentVersionId IN :versionIds];
        for(ContentDistribution cd : distributions){
            ret.put(String.valueOf(cd.ContentVersionId).substring(0,15), cd);
        }
        return ret;
    }

    public class FileAttachmentWrapper{
        @AuraEnabled public String attachId;
        @AuraEnabled public String fileName;
        @AuraEnabled public boolean isContentDocument;
    }

    public class EmailMsg{
        @AuraEnabled public string subject;
        @AuraEnabled public string body;        
        @AuraEnabled public List<FileAttachmentWrapper> fileAttachments;
       // @AuraEnabled publi List<String> attach
    }


    @AuraEnabled
    public static RecordWrapper getRecordEmail(Id recordId, String relatedField, String ccField) {
        System.debug('recordId: ' + recordId);
        If(recordId == null) {
            return null;
        }
        String objectName = recordId.getSobjectType().getDescribe().getName();
        if(relatedField != null && relatedField != null) {
            recordId = getRelatedRecordId(recordId, objectName, relatedField);
        }

        objectName = recordId.getSobjectType().getDescribe().getName();
        String ccFieldName = 'Owner.Name';
        String ccFieldMail = 'Owner.Email';
        String emailField = 'Email';
        String preferedLanguageField = 'Account.Language_Preference__pc';
        String divisionField = 'Account.Primary_Division_Name__c';
        String nameField = 'Name';
        String accField = 'AccountId';
        if (objectName == Opportunity.getSObjectType().getDescribe().getName()) {
            emailField = 'Account_Email__c';
        }
        if (objectName == Account.getSObjectType().getDescribe().getName()) {
            emailField = 'PersonEmail';
            preferedLanguageField = 'Language_Preference__pc';
            divisionField = 'Primary_Division_Name__c';
            accField  = null;
        } 
        if (objectName == Case.getSObjectType().getDescribe().getName()) {
            emailField = 'Account.PersonEmail,Lead__r.Email';
            preferedLanguageField = 'Account.Language_Preference__pc,Lead__r.Language_Preference__c';
            nameField = 'CaseNumber';
            divisionField = 'Account.Primary_Division_Name__c,Lead__r.Division_Name__c';
            accField = 'AccountId,Lead__c';
        } 
        if (objectName == Lead.getSObjectType().getDescribe().getName()) {
            accField  = null;
            preferedLanguageField = 'Language_Preference__c';
            divisionField = 'Division_Name__c';
        } 
        if(ccField == 'Account.Owner'){
            if(objectName == Account.getSObjectType().getDescribe().getName()){
                ccFieldName = 'Owner.Name';
                ccFieldMail = 'Owner.Email';
            } else if(objectName == Case.getSObjectType().getDescribe().getName()){
                ccFieldName = 'Account.Owner.Name, Lead__r.Owner.Name';
                ccFieldMail = 'Account.Owner.Email, Lead__r.Owner.Email';
            } else {
                ccFieldName = 'Account.Owner.Name';
                ccFieldMail = 'Account.Owner.Email';
            }
        }
        
        List<String> fields = new List<String>{emailField, preferedLanguageField, divisionField, nameField, 'EmailThreadId__c'};
        if(ccField != null && ccField != '') {
            fields.add(ccFieldName);
            fields.add(ccFieldMail);
        }

        if(accField != null) {
            fields.add(accField);
        }
        List<String> params = new List<String>{ String.join(fields, ','), objectName};
        System.debug('params: ' + params);
        SObject rec = Database.query(String.format('SELECT Id, {0} FROM {1} WHERE Id =:recordId', params));
        String ccNameValue = '';
        String ccEmailValue = '';
        if(ccField == 'Account.Owner'){
            if(objectName != Account.getSObjectType().getDescribe().getName()){
                if(objectName == Case.getSObjectType().getDescribe().getName()){
                    if(rec.getSobject('Account') != null && rec.getSobject('Account').getSobject('Owner') != null){
                        ccNameValue = (String)rec.getSobject('Account').getSobject('Owner').get('Name');
                        ccEmailValue = (String)rec.getSobject('Account').getSobject('Owner').get('Email');
                    } else if(rec.getSobject('Lead__r') != null && rec.getSobject('Lead__r').getSobject('Owner') != null){
                        ccNameValue = (String)rec.getSobject('Lead__r').getSobject('Owner').get('Name');
                        ccEmailValue = (String)rec.getSobject('Lead__r').getSobject('Owner').get('Email');
                    }
                    
                } else {
                    ccNameValue = (String)rec.getSobject('Account').getSobject('Owner').get('Name');
                    ccEmailValue = (String)rec.getSobject('Account').getSobject('Owner').get('Email');
                }
                
            } else {
                ccNameValue = (String)rec.getSobject('Owner').get('Name');
                ccEmailValue = (String)rec.getSobject('Owner').get('Email');
            }
        } else if(ccField != null && ccField != '') {
            ccNameValue =  (String)rec.getSobject('Owner').get('Name');
            ccEmailValue = (String)rec.getSobject('Owner').get('Email');
        }
        String preferedLang; 
        String divisionValue; 
        String emailValue; 
        String accIdValue;
        if(objectName == Case.getSObjectType().getDescribe().getName()){
            preferedLang = rec.getSobject('Account') != null ? (String)rec.getSobject('Account').get('Language_Preference__pc') : rec.getSobject('Lead__r') != null?  (String)rec.getSobject('Lead__r').get('Language_Preference__c') : null;
            divisionValue = rec.getSobject('Account') != null ? (String)rec.getSobject('Account').get('Primary_Division_Name__c') : rec.getSobject('Lead__r') != null?  (String)rec.getSobject('Lead__r').get('Division_Name__c') : null; 
            emailValue = rec.getSobject('Account') != null ? (String)rec.getSobject('Account').get('PersonEmail') : rec.getSobject('Lead__r') != null?  (String)rec.getSobject('Lead__r').get('Email') : null;
            accIdValue = (String)rec.get('AccountId') != null ? (String)rec.get('AccountId') : (String)rec.get('Lead__c') != null ? (String)rec.get('Lead__c') : null;
        } else {
            preferedLang = preferedLanguageField == 'Language_Preference__pc' || preferedLanguageField == 'Language_Preference__c' ? (String)rec.get(preferedLanguageField) : (String)rec.getSobject('Account').get('Language_Preference__pc');
            divisionValue = divisionField == 'Primary_Division_Name__c' || divisionField == 'Division_Name__c' ?  (String)rec.get(divisionField) : (String)rec.getSobject('Account').get('Primary_Division_Name__c');
            emailValue = emailField == 'Email' ||  emailField == 'Account_Email__c' ? (String)rec.get(emailField) : objectName == Account.getSObjectType().getDescribe().getName() || objectName == Lead.getSObjectType().getDescribe().getName()?  (String)rec.get(emailField)  : (String)rec.getSobject('Account').get('PersonEmail');
            accIdValue = accField != null ? (String)rec.get(accField) : null;
        }
        String threadIdValue = rec.get('EmailThreadId__c') != null ? (String)rec.get('EmailThreadId__c') : null;
        Email_Component_Config__mdt config = getComponentConfig(objectName, divisionValue);
        return new RecordWrapper(rec.Id, objectName, (String)rec.get(nameField), emailValue, preferedLang, ccNameValue, ccEmailValue, divisionValue, accIdValue, threadIdValue, config);
    }

    public static Id getRelatedRecordId(Id recordId, String objectName, String relatedField){
        try{
            List<String> params = new List<String>{relatedField, objectName};
            SObject rec = Database.query(String.format('SELECT Id, {0} FROM {1} WHERE Id =:recordId', params));
            return (id)rec.get(relatedField);

        } catch(Exception e){
            return recordId;
        }
    }

    public class RecordWrapper {
        public RecordWrapper(String pRecordId, String pSObjectType, String pName, String pEmail, String langPreference, String pCcName, String pCcEmail, String pDivision, String pAccId, String pThreadId, Email_Component_Config__mdt pConfig){
            recordId = pRecordId;
            sObjectType = pSObjectType;
            name = pName;
            email = pEmail;
            languagePreference = langPreference;
            ccName = pCcName;
            ccEmail = pCcEmail;
            division = pDivision;
            accId = pAccId;
            threadId = pThreadId;
            config = pConfig;
        }
        @AuraEnabled
        public String recordId{get;set;}
        @AuraEnabled
        public String ccName{get;set;}
        @AuraEnabled
        public String ccEmail{get;set;}
        @AuraEnabled
        public String sObjectType{get;set;}
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        public String email{get;set;}
        @AuraEnabled
        public String languagePreference{get;set;}
        @AuraEnabled
        public String division{get;set;}
        @AuraEnabled
        public String accId{get;set;}
        @AuraEnabled
        public String threadId{get;set;}
        @AuraEnabled
        public Email_Component_Config__mdt config{get;set;}
    }

    public static Email_Component_Config__mdt getComponentConfig(String objectType, String division){
        String userProfile = UserInfo.getProfileId();
        List<Email_Component_Config__mdt> configs = Email_Component_Config__mdt.getAll().values();
        Map<String, Email_Component_Config__mdt> configMap = new Map<String, Email_Component_Config__mdt>();
        for(Email_Component_Config__mdt config : configs){
            String key = config.SObject__c;
            if(config.Profile__c != null){
                key += '-' + config.Profile__c;
            }
            if(config.Division__c != null){
                key += '-' + config.Division__c;
            }
            configMap.put(key, config);
        }
        System.debug('configMap: ' + configMap);
        System.debug('configMap keys: ' + configMap.keySet());
        if(configMap.get(objectType + '-' + userProfile + '-' + division) != null){
            return configMap.get(objectType + '-' + userProfile + '-' + division);
        } else if(configMap.get(objectType + '-' + userProfile) != null){
            return configMap.get(objectType + '-' + userProfile);
        } else if(configMap.get(objectType + '-' + division) != null){
            return configMap.get(objectType + '-' + division);
        } else if(configMap.get(objectType) != null){
            return configMap.get(objectType);
        }
        return null;
    }

}