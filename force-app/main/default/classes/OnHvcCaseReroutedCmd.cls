/**
 * @File Name          : OnHvcCaseReroutedCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/16/2022, 3:31:13 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/10/2022, 11:54:46 AM   acantero     Initial Version
**/
public inherited sharing class OnHvcCaseReroutedCmd {

    @TestVisible
    final Set<ID> reroutedCaseIdSet = new Set<ID>();

    public void checkRecord(
        Case rec, 
        Case oldRec
    ) {
        if (
            (rec.Is_HVC__c == true) &&
            (rec.Routed__c == true) &&
            (rec.Agent_Capacity_Status__c != oldRec.Agent_Capacity_Status__c) &&
            (rec.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE)
        ) {
            reroutedCaseIdSet.add(rec.Id);
        } 
    }

    public Boolean execute() {
        if (reroutedCaseIdSet.isEmpty()) {
            return false;
        }
        //else...
        return CaseReroutingManager.getInstance().fireRerouteEventForCases(
            reroutedCaseIdSet
        );
    }
}