public with sharing class BatchAssignPracticeLeadsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private Integer delayInHours;
	private List<String> assignDivisions;
	
	public static final Integer BATCH_SIZE = 50;
	
	public static final String CRON_NAME = 'BatchAssignPracticeLeadsScheduleable';
	public static final String CRON_SCHEDULE_1X35 = '0 35 * * * ?';
	
	
	public static void schedule() {
		System.schedule(CRON_NAME + '_1X35', CRON_SCHEDULE_1X35, new BatchAssignPracticeLeadsScheduleable());
	}
	
	public BatchAssignPracticeLeadsScheduleable() {
		Datetime queryDate = Datetime.now().addDays(-1);
		// Query for all unassigned practice accounts in the last day
		query = 'SELECT Id, Email FROM Lead WHERE RecordTypeId = \'' + LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE + '\' AND OwnerId = \'' + UserUtil.getSystemUserId() + '\' AND fxTrade_Created_Date__c > ' + SoqlUtil.getSoqlGMT(queryDate);

		System.debug('Batch query: ' + query);
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Email FROM Lead WHERE WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    
    //if divisions is null, then the assignment will be for all the divisions
	public BatchAssignPracticeLeadsScheduleable(List<String> divisions, Integer delayHours) {
        delayInHours = delayHours;
        assignDivisions = divisions;

        Datetime cutoffDate = Datetime.now().addMinutes(delayHours * -1);
		Datetime queryDate = cutoffDate.addDays(-1);

		// Query for all unassigned practice accounts in the last day
		query = 'SELECT Id, Email FROM Lead WHERE RecordTypeId = \'' + LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE + '\' AND OwnerId = \'' + UserUtil.getSystemUserId() + '\' AND fxTrade_Created_Date__c > ' + SoqlUtil.getSoqlGMT(queryDate) + ' and fxTrade_Created_Date__c <= ' + SoqlUtil.getSoqlGMT(cutoffDate);

        // add division condition if necessary
		if(divisions != null && divisions.size() > 0){
			query += ' and Primary_Division_Name__c in (';
   
            Integer counter = 1;
			for(String div : divisions){
				query += '\'' + div + '\'';

				if(counter < divisions.size()){
					query += ',';
				}

				counter++;
			}

			query += ')';
		}

		System.debug('Batch query: ' + query);
	}

	public BatchAssignPracticeLeadsScheduleable(String q){
		query = q;

		System.debug('Batch query: ' + query);
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<Lead> leads = (List<Lead>)batch;
		LeadUtil.assignPracticeLeads(leads);
	}
	
	public void execute(SchedulableContext context) {

		if(this.delayInHours > 0){

			Database.executeBatch(new BatchAssignPracticeLeadsScheduleable(assignDivisions, delayInHours), BATCH_SIZE);

		}else{

			executeBatch();

		}
		
	}

	public void finish(Database.BatchableContext bc) {
		try {
			BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
		}
		catch (Exception e) {}
	}
	
	public static Id executeBatch() {
		return executeBatch(BATCH_SIZE);
	}
	
	public static Id executeBatch(Integer batchSize) {
		return Database.executeBatch(new BatchAssignPracticeLeadsScheduleable(), batchSize);
	}
	
	// Executes lead conversion in same EC
	public static void executeInline() {
		List<Lead> leads = Database.query(new BatchAssignPracticeLeadsScheduleable().query);
		LeadUtil.assignPracticeLeads(leads);
	}
}