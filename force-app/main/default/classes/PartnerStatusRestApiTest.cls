@isTest
public  class PartnerStatusRestApiTest {

    @TestSetup
    static void initData(){
        Affiliate__c aff = new Affiliate__c(Username__c = 'username',
                    Firstname__c = 'John',
                    LastName__c = 'Doe',
                    Company__c = 'Oanda Corp',
                    Division_Name__c = 'OANDA Global Markets',
                    Country__c = 'Macao',
                    Alias__c = 'doer',
                    Broker_Number__c= '12345',
                    Email__c = 'aff1@email.com',
                    Status__c = 'Approved',
                    Cellxpert_Partner_ID__c = 1234);
        insert aff;
    }

    @isTest
    static void getAffiliateById() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;
        req.resourcePath = '/api/v1/partners/*/status';

        Id affId = [SELECT Id FROM Affiliate__c LIMIT 1].Id;
    
        req.requestURI = '/api/v1/partners/' + affId + '/status';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
       
        PartnerStatusRestApi.doGet();
        Assert.areEqual(200, res.statusCode, 'Affiliate id and status are returned');
    }

    @isTest
    static void getAffiliateByBrokerNumber() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;
        req.resourcePath = '/api/v1/partners/*/status';
    
        req.requestURI = '/api/v1/partners/12345/status';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
       
        PartnerStatusRestApi.doGet();
        Assert.areEqual(200, res.statusCode, 'Affiliate id and status are returned');
    }

    @isTest
    static void getAffiliateByBrokerNumberNotFound() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;
        req.resourcePath = '/api/v1/partners/*/status';
    
        req.requestURI = '/api/v1/partners/122225/status';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
       
        PartnerStatusRestApi.doGet();
        Assert.areEqual(404, res.statusCode, 'Affiliate not found');
    }

    @isTest
    static void getAffiliateInvalidRequest() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;
        //req.resourcePath = '/api/v1/partners/*/status';
    
        req.requestURI = '/api/v1/partners/1234/status';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
       
        PartnerStatusRestApi.doGet();
        //lack of resource path throw err
        Assert.areEqual(500, res.statusCode, 'Affiliate endpoint throw error related logic throw null pointer');
    }
}