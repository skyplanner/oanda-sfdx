/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 12-15-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class EnvironmentIdentifier {
    public String id {get; set;}

    public String env {get; set;}

    public EnvironmentIdentifier(String env, String id) {
        this.env = env;
        this.id = id;
    }
}