@isTest
private class DataCategoriesHelper_Test {

	@isTest
	static void getDataCategoriesByGroup_test() {
		String sObjectName = DataCategoriesConstants.ARTICLE_SOBJECT_NAME;
		String groupName = DataCategoriesConstants.DAT_CAT_CATEGORIES;
		Test.startTest();
		List<DataCategoriesHelper.DataCategoryWrapper> result = DataCategoriesHelper.getDataCategoriesByGroup(sObjectName, groupName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getDataCategoriesByGroup_test2() {
		String sObjectName = null;
		String groupName = null;
		Test.startTest();
		List<DataCategoriesHelper.DataCategoryWrapper> result = DataCategoriesHelper.getDataCategoriesByGroup(sObjectName, groupName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void getDataCategoriesMapByGroup_test() {
		String sObjectName = DataCategoriesConstants.ARTICLE_SOBJECT_NAME;
		String groupName = DataCategoriesConstants.DAT_CAT_CATEGORIES;
		Test.startTest();
		Map<String,String> result = DataCategoriesHelper.getDataCategoriesMapByGroup(sObjectName, groupName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getDataCategoriesMapByGroup_test2() {
		String sObjectName = null;
		String groupName = null;
		Test.startTest();
		Map<String,String> result = DataCategoriesHelper.getDataCategoriesMapByGroup(sObjectName, groupName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

}