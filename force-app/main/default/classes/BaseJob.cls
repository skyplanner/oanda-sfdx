/**
 * @File Name          : BaseJob.cls
 * @Description        :
 * @Author             : acantero
 * @Group              :
 * @Last Modified By   : Ariel Niubo
 * @Last Modified On   : 07-28-2022
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/23/2021, 3:12:14 PM   acantero     Initial Version
 **/
public without sharing abstract class BaseJob implements Queueable {
	public enum ActionType {
		EXECUTE,
		CLOSING
	}

	protected final String jobName;

	@testVisible
	ActionType action;

	@testVisible
	Datetime requestDate;

	protected BaseJob(String jobName) {
		this.jobName = jobName;
		this.action = ActionType.EXECUTE;
	}

	public void execute(QueueableContext context) {
		System.attachFinalizer(new JobFinalizer(jobName));
		//...
		BaseJob nextJob = null;
		if (action == ActionType.EXECUTE) {
			nextJob = executeAction(context);
			//...
		} else if (action == ActionType.CLOSING) {
			nextJob = tryToClose(context);
		}
		if (nextJob != null) {
			enqueueNextJob(nextJob);
		}
	}

	void enqueueNextJob(BaseJob nextJob) {
		if (!Test.isRunningTest()) {
			System.enqueueJob(nextJob);
		}
	}

	@testVisible
	BaseJob tryToClose(QueueableContext context) {
		BaseJob nextJob = null;
		Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
			jobName
		);
		if (jobStatus.Request_Date__c > requestDate) {
			//somebody make a new request
			nextJob = getNextJobInstance();
			nextJob.action = ActionType.EXECUTE;
			//...
		} else {
			JobStatusHelper.deactivateJob(jobStatus);
		}
		return nextJob;
	}

	@testVisible
	BaseJob executeAction(QueueableContext context) {
		Datetime jobRequestDate = JobStatusHelper.getJobCurrentRequestDate(
			jobName
		);
		Boolean jobIsDone = doAction(context);
		BaseJob nextJob = getNextJobInstance();
		if (jobIsDone == true) {
			nextJob.action = ActionType.CLOSING;
			nextJob.requestDate = jobRequestDate;
			//...
		} else {
			nextJob.action = ActionType.EXECUTE;
		}
		return nextJob;
	}

	public abstract BaseJob getNextJobInstance();

	/**
    * @description  
    * @author acantero | 6/23/2021
    * @param QueueableContext context
    * @return Boolean 
        Returns true if the action finished its work and does not need any more iterations. Returns false otherwise.
    */
	public abstract Boolean doAction(QueueableContext context);

	public class JobFinalizer implements Finalizer {
		protected final String jobName;

		public JobFinalizer(String jobName) {
			this.jobName = jobName;
		}

		public void execute(FinalizerContext ctx) {
			Boolean error = (ctx.getResult() ==
			ParentJobResult.UNHANDLED_EXCEPTION);
			Exception ex = (error == true) ? ctx.getException() : null;
			checkError(error, ex);
		}

		public void checkError(Boolean error, Exception ex) {
			if (error == true) {
				Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
					jobName
				);
				JobStatusHelper.deactivateJob(jobStatus);
			}
			if (ex != null) {
				InternalErrorManager.safeLogException(jobName, ex);
			}
		}
	}
}