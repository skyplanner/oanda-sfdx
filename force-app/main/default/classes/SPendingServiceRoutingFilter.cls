/**
 * @File Name          : SPendingServiceRoutingFilter.cls
 * @Description        : 
 * Determines whether a PendingServiceRouting record should be included
 * in the custom routing process
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 1:26:02 AM
**/
public interface SPendingServiceRoutingFilter {

	void prepareForFilter(List<PendingServiceRouting> pendingRoutingList);
	
	Boolean isValidForRouting(PendingServiceRouting psr);

}