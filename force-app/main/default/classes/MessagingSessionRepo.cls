/**
 * @File Name          : MessagingSessionRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/15/2024, 12:03:31 AM
**/
public inherited sharing class MessagingSessionRepo {

	public static MessagingSession getSummaryById(ID recordId) {
		List<MessagingSession> recList = [
			SELECT 
				CaseId,
				EndUserAccountId,
				EndUserContactId,
				MessagingEndUserId
			FROM MessagingSession
			WHERE Id = :recordId
		];
		MessagingSession result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static MessagingSession getLeadInfoById(ID recordId) {
		List<MessagingSession> recList = [
			SELECT
				Language_Code__c,
				MessagingEndUserId,
				Region__c,
				User_Email__c
			FROM MessagingSession
			WHERE Id = :recordId
		];
		MessagingSession result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static MessagingSession getCaseInfoById(ID recordId) {
		List<MessagingSession> recList = [
			SELECT
				CaseId,
				Case_Language_Code__c,
				Case_Queue__c,
				Case_Record_Type__c,
				Case_Subtype__c,
				Case_Type__c,
				EndUserAccountId,
				EndUserContactId,
				Escalate_to_Agent__c,
				Inquiry_Nature__c,
				Language_Code__c,
				Last_User_Intent__c,
				LeadId,
				Live_or_Practice__c,
				New_Lead__c,
				Region__c,
				User_Authenticated__c,
				User_Email__c
			FROM MessagingSession
			WHERE Id = :recordId
		];
		MessagingSession result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static MessagingSession getAuthenticationInfoById(ID recordId) {
		List<MessagingSession> recList = [
			SELECT 
				CaseId,
				Language_Code__c
			FROM MessagingSession
			WHERE Id = :recordId
		];
		MessagingSession result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static MessagingSession getInitInfoById(ID recordId) {
		List<MessagingSession> recList = [
			SELECT
				CaseId,
				Case.CaseNumber,
				Case.IsClosed,
				ChannelType, 
				Extra_Info__c,
				Initialized__c,
				MessagingEndUser.Country_Code__c,
				MessagingEndUser.IsoCountryCode,
				Region__c,
				User_Email__c
			FROM MessagingSession
			WHERE Id = :recordId
		];
		MessagingSession result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static List<MessagingSession> getCaseInfo(Set<ID> recordIds) {
		return [
			SELECT 
				CaseId,
				OwnerId
			FROM MessagingSession
			WHERE Id IN :recordIds
		];
	}
	
	public static List<MessagingSession> getMessagingSessions(
		List<Id> recordIds
	) {
		return [
			SELECT
				Id,
				Name,
				Tier__c,
				Language_Code__c,
				Case.Division__c,
				MessagingEndUser.Account.Division_Name__c,
				Lead.Division_Name__c,
				Inquiry_Nature__c,
				MessagingEndUser.Account.Primary_Division_Name__c,
				MessagingEndUser.Account.Name,
				Lead.Primary_Division_Name__c,
				Lead.Name,
				OwnerId,
				Owner.Name,
				Live_or_Practice__c ,
				Case.Chat_Division__c,
				Case.Chat_Language_Preference__c,
				Case.Language_Preference__c,
				AgentType,
				Routing_Status__c 
			FROM MessagingSession
			WHERE Id IN :recordIds
		];
	}

}