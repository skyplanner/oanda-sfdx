/**
 * @File Name          : ExpressionsManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2020, 1:45:01 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   acantero     Initial Version
**/
public without sharing class ExpressionsManager {

    @testVisible
    static MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
    @testVisible
    static Map<String,ExpressionValidationWrapper> defaultExpValMap;

    @testVisible
    final Map<String,ExpressionValidationWrapper> totalExpValMap;

    @testVisible
    Map<String,ExpressionValidationWrapper> expValMap;
    @testVisible
    String currentCitizenNationality;

    public ExpressionsManager(
        Map<String,ExpressionValidationWrapper> totalExpValMap
    ) {
        this.totalExpValMap = totalExpValMap;
    }

    public Map<String,ExpressionValidationWrapper> getExpValMap(
        String citizenshipNationality
    ) {
        if (
            (currentCitizenNationality != citizenshipNationality)
        ) {
            currentCitizenNationality = citizenshipNationality;
            String natPerIdKey = currentCitizenNationality + valSettings.natPersIDSuffix;
            String passportKey = currentCitizenNationality + valSettings.passportSuffix;
            Set<String> keys = new Set<String> {
                    natPerIdKey,
                    passportKey
            };
            expValMap = getExpValMapByKeys(keys);
        }
        if (expValMap.isEmpty()) {
            return getDefaultExpValMap();
        }
        //else...
        return expValMap;
    }

    public Map<String,ExpressionValidationWrapper> getDefaultExpValMap() {
        if (defaultExpValMap == null) {
            Set<String> keys = new Set<String> {
                MiFIDExpValConst.DEFAULT_MIFID + valSettings.natPersIDSuffix,
                MiFIDExpValConst.DEFAULT_MIFID + valSettings.passportSuffix
            };
            defaultExpValMap = getExpValMapByKeys(keys);
        }
        return defaultExpValMap;
    }

    public Map<String,ExpressionValidationWrapper> getExpValMapByKeys(
        Set<String> keys
    ) {
        if (totalExpValMap == null) {
            return ExpressionValidationWrapper.getExpValMap(keys);
        }
        //else
        Map<String,ExpressionValidationWrapper> result = 
            new Map<String,ExpressionValidationWrapper>();
        for(String key : keys) {
            ExpressionValidationWrapper expVal = totalExpValMap.get(key);
            if (expVal != null) {
                result.put(key, expVal);
            }
        }
        return result;
    }

}