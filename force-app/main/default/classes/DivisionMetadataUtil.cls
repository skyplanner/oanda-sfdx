/**
 * @File Name          : DivisionMetadataUtil.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/2/2020, 11:39:53 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/2/2020   dmorales     Initial Version
**/
public without sharing class DivisionMetadataUtil {
    public static List<Division_Settings__mdt> getDivisionVisible(){
        return [
            SELECT 
                DeveloperName, QualifiedApiName, Division_Data_Category__c, Division_Acronyms__c, Division_Label__c, Visible__c  
            FROM 
                Division_Settings__mdt 
            WHERE 
                Visible__c = TRUE           
        ]; 
    }
}