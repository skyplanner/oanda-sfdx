/* Name: MetadataCodesDisplayController
 * Description : Apex Class to get specific codes from metadata with very large volume of data - therefore standard SOQL is apllied instead of methods.
 * Author: Mikolaj Juras
 * Date : 2023 June 09
 */
public without sharing class MetadataCodesDisplayController {
    @AuraEnabled(Cacheable=true)
    public static List<Object> getMetadataRecords(String metadataName, String metadataFields, String errorCodes) {
        if (String.isBlank(metadataName) || String.isBlank(errorCodes)) {
            return new List<Object>();
        }
        metadataFields = String.isNotBlank(metadataFields) ? metadataFields : 'DeveloperName, Description__c';
        String safeMtdName = String.escapeSingleQuotes(metadataName);
        Set<String> errorCodesSet = new Set<String>();
        errorCodesSet.addAll(errorCodes.remove(' ').split(','));
        String queryString = 'SELECT Id,' + metadataFields + ' FROM ' + metadataName + ' WHERE DeveloperName IN :errorCodesSet ORDER BY DeveloperName ASC';
        List<Object> metadataRecords = new List<Object>();
        try {
            metadataRecords = Database.query(queryString);
        } catch (QueryException qe) {
            System.debug('ERROR: ' +qe.getMessage());
        } 
        return metadataRecords;
    }
}