/**
 * @File Name          : GetAgentOwnedMsgSessionsCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/10/2024, 9:35:53 PM
**/
@IsTest
private without sharing class GetAgentOwnedMsgSessionsCmdTest {

	// test 1 : recList is null => return empty list
	// test 2 : recList is empty => return empty list
	// test 3 : recList is not empty => return not empty list
	@IsTest
	static void execute() {
		List<MessagingSession> emptyList = new List<MessagingSession>();
		List<MessagingSession> okList = new List<MessagingSession>();
		
		Test.startTest();
		// test 1
		GetAgentOwnedMsgSessionsCmd instance = 
			new GetAgentOwnedMsgSessionsCmd(null);
		List<MessagingSession> result1 = instance.execute();

		// test 2
		instance = new GetAgentOwnedMsgSessionsCmd(emptyList);
		List<MessagingSession> result2 = instance.execute();
		
		// test 3
		List<MessagingSession> result3 = null;
		System.runAs(new User(Id = UserInfo.getUserId())) {
			okList.add(
				new MessagingSession(
					OwnerId = UserInfo.getUserId()
				)
			);
			instance = new GetAgentOwnedMsgSessionsCmd(okList);
			result3 = instance.execute();
		}
	
		
		Test.stopTest();
		
		Assert.isTrue(result1.isEmpty(), 'Invalid result');
		Assert.isTrue(result2.isEmpty(), 'Invalid result');
		Assert.isFalse(result3.isEmpty(), 'Invalid result');
	}
	
}