/**
 * @File Name          : ChatAgentBySkills_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/21/2021, 5:40:34 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/21/2021, 4:28:47 PM   acantero     Initial Version
**/
@isTest
private without sharing class ChatAgentBySkills_Test {
    
    @isTest
    static void existsCompatibleAgents() {
        Set<String> skillDevNameSet = new Set<String>
        {
            OmnichanelConst.HVC_SKILL, OmnichanelConst.OCAN_SKILL
        };
        Test.startTest();
        ChatAgentBySkills obj = new ChatAgentBySkills();
        Set<String> result = obj.existsCompatibleAgents(skillDevNameSet);
        Test.stopTest();
        System.assertNotEquals(null, result);
    }
}