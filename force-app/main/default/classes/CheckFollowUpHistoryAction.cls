/**
 * @File Name          : CheckFollowUpHistoryAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/13/2023, 12:05:50 AM
**/
public without sharing class CheckFollowUpHistoryAction {

	@InvocableMethod(label='Check Follow-Up History' callout=false)
	public static List<ActionResult> checkFollowUpHistory(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<ActionResult> result = new List<ActionResult> { new ActionResult() };
			
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			String followUpHistory = 
				String.isNotBlank(infoList[0].followUpHistory)
				? infoList[0].followUpHistory
				: '{}';
			Map<String, Integer> followUpHistoryMap = 
				(Map<String, Integer>)JSON.deserialize(
					followUpHistory, 
					Map<String, Integer>.class
				);
			Integer currentCounter = 
				followUpHistoryMap.get(infoList[0].dialogFollowUpKey);

			if (currentCounter == null) {
				currentCounter = 0;
			}
			currentCounter++;
			followUpHistoryMap.put(
				infoList[0].dialogFollowUpKey,
				currentCounter
			);
			result[0].dialogFollowUpCounter = currentCounter;
			result[0].followUpHistory = JSON.serialize(followUpHistoryMap);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CheckFollowUpHistoryAction.class.getName(),
				ex
			);
		}
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'followUpHistory' required = false)
		public String followUpHistory;

		@InvocableVariable(label = 'dialogFollowUpKey' required = true)
		public String dialogFollowUpKey;
		
	}

	// *******************************************************************

	public class ActionResult {

		@InvocableVariable(label = 'followUpHistory' required = false)
		public String followUpHistory;

		@InvocableVariable(label = 'dialogFollowUpCounter' required = false)
		public Decimal dialogFollowUpCounter;

		public ActionResult() {
			this.followUpHistory = '';
			this.dialogFollowUpCounter = 0;
		}
		
	}
	
}