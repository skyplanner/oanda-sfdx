/**
 * @File Name          : CloseCaseCmpController_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/21/2021, 5:25:05 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/14/2020   dmorales     Initial Version
**/
@isTest
private class CloseCaseCmpController_Test {
    @isTest
    static void getCaseById() {
       Case testCase = new Case(SuppliedEmail='test@oanda.com', 
        SuppliedName = 'test', Status = 'Open' );
       insert testCase;       
       Case testCaseGet = CloseCaseCmpController.getCaseById(testCase.Id);
        system.assertEquals(testCase.Status, testCaseGet.Status);
    }

    @isTest
    static void getDependencies() {
        Map<Object,List<String>> mapCase = CloseCaseCmpController.getDependencies('Case.Type__c');
        System.assert(mapCase !=null);
    }

    @IsTest
    static void getSubtypeByAgentList() {
        Test.startTest();
        List<SubtypeByAgentInfo> result = CloseCaseCmpController.getSubtypeByAgentList();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getCaseById2() {
        Boolean error = false;
        Test.startTest();
        try {
            Case result = CloseCaseCmpController.getCaseById('fakeid');
            //...
        } catch (AuraHandledException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
}