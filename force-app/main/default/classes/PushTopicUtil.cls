/**
 * Create or Update push topic or platform events
 * 
 * @author Gilbert Gao
 */
public class PushTopicUtil {
	
	public static final double API_VERSION = 38.0;
	public static final String PUSH_TOPIC_CQG_REQUEST = 'CGQ_request' + (Test.isRunningTest() ? '_T' : '');
	public static final String PUSH_TOPIC_FXACCOUNT_UPDATE = 'fxAccnt_stream_updates' + (Test.isRunningTest() ? '_T' : '');
	public static final String PUSH_TOPIC_FXACCOUNT_CLIENT_STATUS = 'fxAccnt_client_status' + (Test.isRunningTest() ? '_T' : '');
	public static final String PUSH_TOPIC_FXACCOUNT_TRADE_GROUP = 'fxAccnt_trade_group' + (Test.isRunningTest() ? '_T' : '');
	
	
	public static void upsertTopics(){
		upsertTopic_CQGRequest_Name(PUSH_TOPIC_CQG_REQUEST);
		upsertTopic_fxAccnt_stream_updates_Name(PUSH_TOPIC_FXACCOUNT_UPDATE);
		upsertTopic_fxAccnt_client_status_Name(PUSH_TOPIC_FXACCOUNT_CLIENT_STATUS);
		upsertTopic_fxAccnt_trade_group_Name(PUSH_TOPIC_FXACCOUNT_TRADE_GROUP);
		
	}
	
	public static void upsertTopic_CQGRequest(){
		upsertTopic_CQGRequest_Name(PUSH_TOPIC_CQG_REQUEST);
	}
	
	public static void upsertTopic_fxAccnt_stream_updates(){
		upsertTopic_fxAccnt_stream_updates_Name(PUSH_TOPIC_FXACCOUNT_UPDATE);
	}
	
	public static void upsertTopic_fxAccnt_client_status(){
		upsertTopic_fxAccnt_client_status_Name(PUSH_TOPIC_FXACCOUNT_CLIENT_STATUS);
	}
	
	
	public static void upsertTopic_fxAccnt_trade_group(){
		upsertTopic_fxAccnt_trade_group_Name(PUSH_TOPIC_FXACCOUNT_TRADE_GROUP);
	}
	
	public static void upsertTopic_CQGRequest_Name(String topicName){
		//generate topic query
		String query = 'select Id, Account_Name__c, fxTrade_User_Id__c, Home_Currency__c from CQG_Account__c where OPA_Request__c = true';
		

		PushTopic pTopic = new PushTopic(); 
        pTopic.NotifyForOperationCreate = true; 
        pTopic.NotifyForOperationUpdate = true; 
        pTopic.NotifyForOperationDelete = false; 
        pTopic.NotifyForOperationUndelete = false; 
        pTopic.NotifyForFields = 'Where'; 
        pTopic.Description = 'Request CQG Account Id through OPA';
		
		upsertTopicQuery(topicName, query, pTopic);
	}
	
	public static void upsertTopic_fxAccnt_stream_updates_Name(String topicName){
		//generate topic query
		//NOTE: 1300 chars is max query length
		//it is around 1260 at the moment with all divisions enabled, and @oanda.comemail domain filter
		String query = 'SELECT id,fxTrade_User_Id__c,LastModifiedDate,Registration_Source__c,Birthdate__c,Government_ID__c,Employment_Status__c,Employer_Name__c,Employment_Job_Title__c,Annual_Income__c,Net_Worth_Value__c,Is_2FA_Required__c,Industry_of_Employment__c,Title__c,Risk_Tolerance_Amount__c,IIROC_Member__c,Citizenship_Nationality__c,Income_Source_Details__c,Risk_Tolerance_CAD__c,Phone__c,Street__c,City__c,State__c,Country_Name__c,Postal_Code__c,First_Name__c,Middle_Name__c,Last_Name__c,Suffix__c,Language_Preference__c,FXDB_Name__c,Business_Name__c,Has_Opted_Out_Of_Email__c,Email__c,Introducing_Broker_Number__c,Net_Worth_Range_Start__c,Net_Worth_Range_End__c,Self_Employed_Details__c,PEFP__c,PEFP_Details__c,Intermediary__c,Relationship_to_OANDA__c,Liquid_Net_Worth_Value__c,Liquid_Net_Worth_Start__c,Liquid_Net_Worth_End__c,Savings_and_Investments__c FROM fxAccount__c WHERE RecordTypeId = \'012U00000005NQFIA2\' and LastModifiedById NOT IN( \'005U0000005nM3FIAU\',\'005U0000000LbY2IAK\',\'005U0000006Am4BIAS\',\'005U0000003JPLMIA4\',\'005U0000000UuCrIAK\')';

		//prepare a new empty topic
		PushTopic pTopic = new PushTopic(); 
        pTopic.NotifyForOperationCreate = false; 
        pTopic.NotifyForOperationUpdate = true; 
        pTopic.NotifyForOperationDelete = false; 
        pTopic.NotifyForOperationUndelete = false; 
        pTopic.NotifyForFields = 'Select'; 
        pTopic.Description = 'Not notify for changes made by integration user'; 
        
		upsertTopicQuery(topicName, query, pTopic);
	}
	
	public static void upsertTopic_fxAccnt_client_status_Name(String topicName){
		//topic query
		String query = 'select id, name, fxTrade_User_Id__c, LastModifiedById from fxAccount__c where recordTypeId = \'012U00000005NQFIA2\' and Approved_to_Fund__c != null';
		
		//prepare a new empty topic
		PushTopic pTopic = new PushTopic(); 
		pTopic.NotifyForOperationCreate = false; 
        pTopic.NotifyForOperationUpdate = true; 
        pTopic.NotifyForOperationDelete = false; 
        pTopic.NotifyForOperationUndelete = false; 
        pTopic.NotifyForFields = 'Where';
        pTopic.Description = 'Notify when the Approved To Fund datetime is changed for live fxAccount';
        
        
        upsertTopicQuery(topicName, query, pTopic);
	}
	
	public static void upsertTopic_fxAccnt_trade_group_Name(String topicName){
		//topic query
		String query = 'SELECT id, Name, fxTrade_User_Id__c, Division_Name__c, Trade_Group__c FROM fxAccount__c WHERE recordTypeId = \'012U00000005NQFIA2\' AND Trade_Group__c != null';
		
		//prepare a new empty topic
		PushTopic pTopic = new PushTopic(); 
		pTopic.NotifyForOperationCreate = false; 
        pTopic.NotifyForOperationUpdate = true; 
        pTopic.NotifyForOperationDelete = false; 
        pTopic.NotifyForOperationUndelete = false; 
        pTopic.NotifyForFields = 'Where';
        pTopic.Description = 'Notify when the Trade Group is changed for live fxAccount';
        
        
		upsertTopicQuery(topicName, query, pTopic);
	}

	private static void upsertTopicQuery(String topicName, String query, PushTopic newTopic){
		System.debug('Push Topic Name: ' + topicName);
		System.debug('Push Topic Query: ' + query);

		Notification_about_object_changes__mdt mc = Notification_about_object_changes__mdt.getInstance(topicName);
		Boolean newValueForIsActive;
		if (mc != null && !Test.isRunningTest()) {
			if (mc.Use_new_logic_instead_PushTopic__c == true) {
				newTopic.IsActive = false;
				newValueForIsActive = false;
			} else {
				newValueForIsActive = true;
				query += addDivisionFilter(mc);
				query += addEmailFilter(mc);
			}
		}


		System.debug('Push Topic Query With Filter Statements: ' + query);

		//check if the push topic exists
		List<PushTopic> topics = [select Id, Query, IsActive from PushTopic where Name = :topicName];
		
		if(topics != null && topics.size() > 0){
			System.debug('Push topic ' + topicName + ' exists');
			Boolean isTopicUpdated = false;
			
			if (topics[0].Query != query) {
				topics[0].Query = query;
				isTopicUpdated = true;
			}
			if (newValueForIsActive != null && topics[0].IsActive != newValueForIsActive) {
				topics[0].IsActive = newValueForIsActive;
				isTopicUpdated = true;
			}

			if (isTopicUpdated) {
				update topics[0];
				
				System.debug('Push topic ' + topicName + ' updated');
				System.debug('New Query: ' + query);
				System.debug('Push Topic Active: ' + topics[0].IsActive);
			}
			
		} else {
			//insert a new push topic
			newTopic.Name = topicName; 
            newTopic.Query = query;
            newTopic.ApiVersion = API_VERSION; 
            
            insert newTopic;
            
            System.debug('Push topic ' + topicName + ' created');
			System.debug('New Query: ' + query);
		}
	}

	//Logic based on Notification_about_object_changes__mdt can be removed after fully pushtopic replacement
	private static String addEmailFilter(Notification_about_object_changes__mdt mc) {
		return mc.Enable_For_Oanda_Email_Domain_Only__c ? ' AND Email_Domain__c != \'oanda.com\'' : '';
	}

	private static String addDivisionFilter(Notification_about_object_changes__mdt mc) {
		String divisionQueryFilter = '';

		Set<String> divisionNames =  String.isBlank(mc.Division_Names__c)
				? new Set<String>()
				: new Set<String>(mc.Division_Names__c.split(','));

		if (!divisionNames.isEmpty()) {
			divisionQueryFilter = ' AND Division_Name__c NOT IN (\''+ String.join(divisionNames,'\',\'') +'\')';
		}

		return divisionQueryFilter;
	}
}