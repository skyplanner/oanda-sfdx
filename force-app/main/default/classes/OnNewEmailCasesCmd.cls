/**
 * @File Name          : OnNewEmailCasesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/21/2021, 4:46:03 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/19/2021, 12:52:08 PM   acantero     Initial Version
**/
public inherited sharing class OnNewEmailCasesCmd {

    List<Case> checkQueueList = new List<Case>();

    public void checkRecord(
        Case rec
    ) {
        if (rec.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK) {
            rec.EntitlementId = 
                EntitlementSettings.getEmailFrontdeskEntitlementId();
            checkQueueList.add(rec);
            //...
        } else if (rec.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_API) {
            rec.EntitlementId = 
                EntitlementSettings.getEmailApiEntitlementId();
            checkQueueList.add(rec);
            //....
        } else if (rec.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL) {
            rec.EntitlementId = 
                EntitlementSettings.getEmailChatbotEntitlementId();
            checkQueueList.add(rec);
        }
    }

    public void execute() {
        if (checkQueueList.isEmpty()) {
            return;
        }
        //else...
        Set<ID> caseQueueIdSet = 
            CaseRoutingManager.getOriginalCaseQueueIdSet();
        ID skillsQueueId = 
            CaseRoutingManager.getRequiredSkillsQueueId();
        for(Case caseObj : checkQueueList) {
            if (caseQueueIdSet.contains(caseObj.OwnerId)) {
                caseObj.OwnerId = skillsQueueId;
            }
        }
    }

    
    
}