/*
 *	@author : Deepak Malkani.
 *	CreatedDate : Jan 20 2017
 *	Purpose : Test class for EID Results Controller Extension
*/
@isTest
private class EIDResultsCtrlExtTest
{
	@isTest
	static void loadEIDRespCodesforEquiFaxUS()
	{
		//Prepare Test Data
		TestDataFactory testHandler = new TestDataFactory();
		Account accnt = testHandler.createTestAccount();
		fxAccount__c fxAccnt = testHandler.createFXTradeAccount(accnt);
		EID_Result__c eidResults = TestDataFactory.createEIDResult_EquifaxUS(fxAccnt);
		TestDataFactory.createResponseCodesCustSett();
		Test.startTest();
		PageReference pg = Page.EIDReasonCodeVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(eidResults);
		EIDResultsCtrlExt contHandler = new EIDResultsCtrlExt(stdCtrl);
		system.assertEquals(2, contHandler.getReasonCodes().size());
		Test.stopTest();
	}

	@isTest
	static void loadEIDRespCodesforEquiFaxUK()
	{
		//Prepare Test Data
		TestDataFactory testHandler = new TestDataFactory();
		Account accnt = testHandler.createTestAccount();
		fxAccount__c fxAccnt = testHandler.createFXTradeAccount(accnt);
		EID_Result__c eidResults = TestDataFactory.createEIDResult_EquifaxUK(fxAccnt);
		TestDataFactory.createResponseCodesCustSett();
		Test.startTest();
		PageReference pg = Page.EIDReasonCodeVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(eidResults);
		EIDResultsCtrlExt contHandler = new EIDResultsCtrlExt(stdCtrl);
		system.assertEquals(5, contHandler.getReasonCodes().size());
		Test.stopTest();
	}
	
	@isTest
	static void loadEIDRespCodesforIovation()
	{
		//Prepare Test Data
		TestDataFactory testHandler = new TestDataFactory();
		Account accnt = testHandler.createTestAccount();
		fxAccount__c fxAccnt = testHandler.createFXTradeAccount(accnt);
		EID_Result__c eidResults = TestDataFactory.createEIDResult_EquifaxUK(fxAccnt);
		//Changing the EID Result Provider to Iovation.
		eidResults.Provider__c = 'Iovation';
		update eidResults;
		TestDataFactory.createResponseCodesCustSett();
		Test.startTest();
		PageReference pg = Page.EIDReasonCodeVFPage;
		Test.setCurrentPage(pg);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(eidResults);
		EIDResultsCtrlExt contHandler = new EIDResultsCtrlExt(stdCtrl);
		system.assertEquals(0, contHandler.getReasonCodes().size());
		Test.stopTest();
	}
	
}