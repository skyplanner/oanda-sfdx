public class BatchFormatAccountInfo implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	public BatchFormatAccountInfo() {
		//Query for all Accounts
		query = 'SELECT Id, PersonEmail, Phone FROM Account';
	}

	public BatchFormatAccountInfo(String q) {
   		query = q;
  	}
	
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, PersonEmail, Phone FROM Account WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		AccountUtil.formatAccountInfo((Account[]) batch);
	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		BatchDeleteAccountFieldHistory b = new BatchDeleteAccountFieldHistory();
		database.executebatch(b);
	}


}