/**
 * @File Name          : ScheduledEmailCheckerManager_Test.cls
 * @Description        : 
 * @Author             : Ariel Niubo
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/5/2022, 5:39:22 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         ???                      Ariel Niubo               Initial Version
**/

@isTest
private class ScheduledEmailCheckerManager_Test {

	@IsTest
	static void start() {
		Test.startTest();
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.start();
		List<CronTrigger> cronTriggerList = getCronTrigger(
			ScheduledEmailCheckerManager.JOB_NAME
		);
		System.assertEquals(
			1,
			cronTriggerList.size(),
			'One Schedule job running'
		);
		System.assertEquals(
			ScheduledEmailCheckerManager.CRON_EXP_EVERY_ONE_HOUR,
			cronTriggerList.get(0).CronExpression,
			'Run every one hour'
		);
		Test.stopTest();
		List<Job_Status__c> jobStatusList = getJobStatus();
		System.assertEquals(1, jobStatusList.size(), 'Just One');
		// System.assertEquals(
		// 	JobStatusHelper.ACTIVE_STATUS,
		// 	jobStatusList.get(0).Status__c,
		// 	'Status must be active'
		// );
	}

	@IsTest
	static void stop() {
		Test.startTest();
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.start();
		manager.stop();
		List<CronTrigger> cronTriggerList = getCronTrigger(
			ScheduledEmailCheckerManager.JOB_NAME
		);
		System.assertEquals(
			0,
			cronTriggerList.size(),
			'No Schedule job running'
		);
		Test.stopTest();
		List<Job_Status__c> jobStatusList = getJobStatus();
		System.assertEquals(1, jobStatusList.size(), 'Just One');
		System.assertEquals(
			JobStatusHelper.INACTIVE_STATUS,
			jobStatusList.get(0).Status__c,
			'Status must be inactive'
		);
	}

	@isTest
	private static void checkJobStatus() {
		Test.startTest();
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.checkJobStatus();
		Test.stopTest();
		List<Job_Status__c> jobStatusList = getJobStatus();
		System.assertEquals(
			1,
			jobStatusList.size(),
			'Inserted the job because not exists'
		);
	}

	@isTest
	private static void checkJobStatusNoInsert() {
		ScheduledEmailTestUtil.createStatusJob(
			SendScheduledEmailJob.SEND_EMAIL_JOB,
			JobStatusHelper.INACTIVE_STATUS
		);
		Test.startTest();
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.checkJobStatus();
		Test.stopTest();
		List<Job_Status__c> jobStatusList = getJobStatus();
		System.assertEquals(
			1,
			jobStatusList.size(),
			'Must not Insert the job because it exists'
		);
		// Asserts
	}
	
	@IsTest
	static void checkScheduledEmailChecker() {
		Test.startTest();
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.checkJobStatus();
		manager.checkScheduledEmailChecker();
		List<CronTrigger> cronTriggerList = getCronTrigger(
			ScheduledEmailCheckerManager.JOB_NAME
		);
		System.assertEquals(
			1,
			cronTriggerList.size(),
			'One Schedule job running'
		);
		System.assertEquals(
			ScheduledEmailCheckerManager.CRON_EXP_EVERY_ONE_HOUR,
			cronTriggerList.get(0).CronExpression,
			'Run every one hour'
		);
		Test.stopTest();
		List<Job_Status__c> jobStatusList = getJobStatus();
		System.assertEquals(1, jobStatusList.size(), 'Just One');
		// System.assertEquals(
		// 	JobStatusHelper.ACTIVE_STATUS,
		// 	jobStatusList.get(0).Status__c,
		// 	'Status must be active'
		// );
	}
	
	@IsTest
	static void checkScheduledEmailCheckerCronExp() {
		// Test.startTest();
		// System.schedule(
		// 	ScheduledEmailCheckerManager.JOB_NAME,
		// 	'0 0 23 * * ?',
		// 	new ScheduledEmailCheckerJob()
		// );
		// ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		// manager.checkJobStatus();
		// manager.checkScheduledEmailChecker();
		// List<CronTrigger> cronTriggerList = getCronTrigger(
		// 	ScheduledEmailCheckerManager.JOB_NAME
		// );
		// System.assertEquals(
		// 	1,
		// 	cronTriggerList.size(),
		// 	'One Schedule job running'
		// );
		// System.assertEquals(
		// 	ScheduledEmailCheckerManager.CRON_EXP_EVERY_ONE_HOUR,
		// 	cronTriggerList.get(0).CronExpression,
		// 	'Run every one hour'
		// );
		// Test.stopTest();
		// List<Job_Status__c> jobStatusList = getJobStatus();
		// System.assertEquals(1, jobStatusList.size(), 'Just One');
		// System.assertEquals(
		// 	JobStatusHelper.ACTIVE_STATUS,
		// 	jobStatusList.get(0).Status__c,
		// 	'Status must be active'
		// );
	}

	@isTest
	private static void scheduleNextTwoMinutes() {
		ScheduledEmailCheckerManager manager = new ScheduledEmailCheckerManager();
		manager.checkJobStatus();
		Test.startTest();
		String result = 
			ScheduledEmailCheckerManager.scheduleNextTwoMinutes();
		Test.stopTest();
		System.assert(
			String.isNotBlank(result),
			'Invalid result'
		);
	}
	
	private static List<Job_Status__c> getJobStatus() {
		return [
			SELECT Id, Status__c
			FROM Job_Status__c
			WHERE Name = :SendScheduledEmailJob.SEND_EMAIL_JOB
		];
	}

	private static List<CronTrigger> getCronTrigger(String jobName) {
		List<CronTrigger> cronTriggerList = [
			SELECT Id, CronExpression, CronJobDetail.Name
			FROM CronTrigger
			WHERE
				CronJobDetail.JobType = :CronTriggerRepo.SCHEDULE_JOB_TYPE
				AND CronJobDetail.Name = :jobName
		];
		return cronTriggerList;
	}
}