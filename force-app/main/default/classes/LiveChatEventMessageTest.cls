@isTest
private class LiveChatEventMessageTest {

    public static final String TEST_MSG_TYPE = 'Chat Form';
    public static final Boolean TEST_MSG_ACTVE = true;
    public static final String TEST_MSG_ENG = 'Message in English';

    @testSetup
    static void setup() {
      Event_Message__c msg = new Event_Message__c(
         Type__c = TEST_MSG_TYPE, 
         Active__c = TEST_MSG_ACTVE,
         Message_English__c = TEST_MSG_ENG
      );
      insert msg;    
    }

    @isTest
    private static void testGetNextUrl() {
       LiveChatEventMessageCtrl instance = new LiveChatEventMessageCtrl();
       String result = instance.getNextUrl();
       System.assertNotEquals(null, result);
    }

    @isTest
    private static void testGetActiveEventMessageChat() {
        LiveChatEventMessageCtrl instance = new LiveChatEventMessageCtrl();
        String result = instance.getActiveEventMessageChat;
        System.assertNotEquals(null, result);
       
    }
    
}