/**
 * @File Name          : OmniExtraCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 5:25:42 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/9/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class OmniExtraCtrl_Test {

    static final String FAKE_AGENT_ID = '0050B0000081XTXQA2';
    static final String FAKE_WORK_ID = '0Bz3C0000000CvkSAE';
    static final String FAKE_SERVICE_PRESENCE_STATUS_ID = '0N53C00000000WUSAY';

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CANADA
        );
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        OmnichanelRoutingTestDataFactory.createHvcAccountTestCase();
    }

    @istest
    static void getOmniExtraInfo_test1() {
        Test.startTest();
        OmniExtraCtrl.OmniExtraInfo result = OmniExtraCtrl.getOmniExtraInfo();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    //set chatChannelDevName and pushTimeout to ensure that the servPresenceStatusIdList attribute is populated
    @istest
    static void getOmniExtraInfo_test2() {
        OmnichanelSettings omniSettings = OmnichanelSettings.getRequiredDefault();
        omniSettings.chatChannelDevName = 'sfdc_liveagent';
        omniSettings.maxPushAttempts = 4;
        omniSettings.pushTimeout = 5;
        Test.startTest();
        OmniExtraCtrl.OmniExtraInfo result = OmniExtraCtrl.getOmniExtraInfo();
        Test.stopTest();
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.servPresenceStatusIdList);
    }

    @istest
    static void onAgentLogin_test() {
        String agentId = null;
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0005');
        System.runAs(supervisor) {
            OmnichanelRoutingTestDataFactory.createAgentServiceResWithSkill(
                UserInfo.getUserId(), 
                UserInfo.getUserName(),
                OmnichanelConst.OCAN_SKILL
            );
            agentId = UserInfo.getUserId();
            OmniExtraCtrl.onAgentLogin(3);
        }
        Test.stopTest();
        Agent_Info__c info = [
            select Capacity__c, Skills__c 
            from Agent_Info__c
            where Agent_ID__c = :agentId
        ];
        Agent_Extra_Info__c extraInfo = [
            select Non_HVC_Cases__c
            from Agent_Extra_Info__c
            where Agent_ID__c = :agentId
        ];
        System.assertEquals(3, info.Capacity__c);
        System.assertNotEquals(null, info.Skills__c);
        //...
        System.assertEquals(0, extraInfo.Non_HVC_Cases__c);
    }

    @istest
    static void updateAgentCapacity_test() {
        String agentId = null;
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0006');
        System.runAs(supervisor) {
            OmnichanelRoutingTestDataFactory.createAgentServiceResWithSkill(
                UserInfo.getUserId(), 
                UserInfo.getUserName(),
                OmnichanelConst.OCAN_SKILL
            );
            agentId = UserInfo.getUserId();
            OmniExtraCtrl.updateAgentCapacity(3);
        }
        Test.stopTest();
        Agent_Info__c info = [
            select Capacity__c 
            from Agent_Info__c
            where Agent_ID__c = :agentId
        ];
        System.assertEquals(3, info.Capacity__c);
    }

    @istest
    static void workCanBeDeclined_test1() {
        Test.startTest();
        OmniExtraCtrl.PushAttemptInfo result = OmniExtraCtrl.workCanBeDeclined(
            null,
            null
        );
        Test.stopTest();
        System.assertEquals(null, result);
    }

    //fakes ids
    @istest
    static void workCanBeDeclined_test2() {
        Test.startTest();
        OmniExtraCtrl.PushAttemptInfo result = OmniExtraCtrl.workCanBeDeclined(
            FAKE_WORK_ID,
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        Test.stopTest();
        System.assertEquals(false, result.workIsValid);
    }
    
    @istest
    static void workCanBeDeclined_test3() {
        AgentWorkWrapper agentWorkW = new AgentWorkWrapper(FAKE_WORK_ID);
        agentWorkW.status = OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED;
        agentWorkW.capacityWeight = 3;
        agentWorkW.skillsSummary = 'LE CC OME';
        AgentBySkills agentsManager = new AgentBySkills(
            new List<String> {FAKE_SERVICE_PRESENCE_STATUS_ID}
        );
        agentsManager.activeAgents = new List<String> {FAKE_AGENT_ID};
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = FAKE_AGENT_ID,
            Name = FAKE_AGENT_ID,
            Capacity__c = 3,
            Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
        );
        insert agentInfo;
        Test.startTest();
        OmniExtraCtrl.PushAttemptInfo result = OmniExtraCtrl.workCanBeDeclined(
            agentWorkW,
            agentsManager,
            FAKE_AGENT_ID
        );
        Test.stopTest();
        System.assertEquals(false, result.canBeDeclined);
    }

    @istest
    static void deleteCurrentAgentInfo_test() {
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0007');
        System.runAs(supervisor) {
            String agentId = UserInfo.getUserId();
            Agent_Info__c agentInfo = new Agent_Info__c(
                Agent_ID__c = agentId,
                Name = agentId,
                Capacity__c = 3,
                Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
            );
            insert agentInfo;
            //..
            OmniExtraCtrl.deleteCurrentAgentInfo();
        }
        Test.stopTest();
        Integer count = [select count() from Agent_Info__c];
        System.assertEquals(0, count);
    }

    @isTest
    static void finishPendingCaseClosing() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        AgentCapacityHelper.beginCaseClosing(
            userId, 
            caseId
        );
        Test.startTest();
        OmniExtraCtrl.finishPendingCaseClosing();
        Test.stopTest();
        Integer afterCount = [select count() from Agent_Work_Closing__c];
        System.assertEquals(0, afterCount);
        Case caseObj = [select Id, Agent_Capacity_Status__c from Case where Id = :caseId];
        System.assertEquals(OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, caseObj.Agent_Capacity_Status__c);
    }

    @isTest
    static void getPendingChatWorks() {
        Test.startTest();
        List<AgentCapacityHelper.ChatWorkClosingInfo> result = 
            OmniExtraCtrl.getPendingChatWorks();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void registerDeclinedChat() {
        List<String> chatIdList = SLAViolationsTestDataFactory.createTestChatsForHVCAccount(
            OmnichanelConst.OANDA_CANADA,
            OmnichanelConst.INQUIRY_NAT_LOGIN
        );
        //...
        String chatId = chatIdList[0];
        Test.startTest();
        OmniExtraCtrl.registerDeclinedChat(
            chatId
        );
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(1, count);
    }

    @isTest
    static void freeCaseCapacity() {
        String userId = UserInfo.getUserId();
        String caseId = [select Id from Case limit 1].Id;
        String closingObjId = 
            AgentCapacityHelper.beginCaseClosing(
                userId, 
                caseId
            );
        Test.startTest();
        OmniExtraCtrl.freeCaseCapacity(caseId);
        Test.stopTest();
        Case caseObj = [select Id, Agent_Capacity_Status__c from Case where Id = :caseId];
        System.assertEquals(OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, caseObj.Agent_Capacity_Status__c);
    }

    @isTest
    static void onNewNonHvcCase() {
        String currentStatus = 'fakeStatusName';
        Test.startTest();
        NonHvcCaseAttemptInfo result = 
            OmniExtraCtrl.onNewNonHvcCase(currentStatus);
        Test.stopTest();
        System.assertEquals(false, result.canBeAssigned);
        System.assertEquals(null, result.newStatusId);
    }

    @isTest
    static void onNonHvcCaseReleased() {
        String currentStatus = 'fakeStatusName';
        Test.startTest();
        String result = OmniExtraCtrl.onNonHvcCaseReleased(currentStatus);
        Test.stopTest();
        System.assertEquals(null, result);
    }

    @IsTest
    static void declineNonHvcCases() {
        Boolean error = false;
        Test.startTest();
        try {
            OmniExtraCtrl.declineNonHvcCases(null);
            //...
        } catch (Exception e) {
            error = true;
        } 
        Test.stopTest();
        System.assertEquals(false, error);
    }
 

}