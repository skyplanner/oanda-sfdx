/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-18-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ComboPicklistController_Test {
	@isTest
	private static void objectNameNull() {
		try {
			ComboPicklistController.getPicklistInfo(null, 'field');
		} catch (AuraHandledException ex) {
			System.assertEquals(
				System.Label.Scheduled_email_ctrl_ObjectName_null,
				ex.getMessage(),
				'Object Name cannot be Null or Empty'
			);
		}
	}
	@isTest
	private static void fieldNameNull() {
		try {
			ComboPicklistController.getPicklistInfo('object', '');
		} catch (AuraHandledException ex) {
			System.assertEquals(
				System.Label.Scheduled_email_ctrl_FieldName_null,
				ex.getMessage(),
				'Object Name cannot be Null or Empty'
			);
		}
	}
	@isTest
	private static void objectNameNotExist() {
		try {
			ComboPicklistController.getPicklistInfo('No_VALID', 'field');
		} catch (AuraHandledException ex) {
			System.assertEquals(
				System.Label.Scheduled_email_ctrl_invalid_object,
				ex.getMessage(),
				'Object Name cannot be Null or Empty'
			);
		}
	}
	@isTest
	private static void fieldNameNotExist() {
		try {
			ComboPicklistController.getPicklistInfo(
				'Scheduled_Email__C',
				'No_VALID'
			);
		} catch (AuraHandledException ex) {
			System.assertEquals(
				String.format(
					System.Label.Scheduled_email_ctrl_invalid_field,
					new List<String>{ 'Scheduled_Email__C', 'No_VALID' }
				),
				ex.getMessage(),
				'Object Name cannot be Null or Empty'
			);
		}
	}
	@isTest
	private static void getPicklistInfo() {
		try {
			List<ComboPicklistController.PicklistInfoWrapper> infoList = ComboPicklistController.getPicklistInfo(
				'Scheduled_Email__C',
				'AM_or_PM__C'
			);
			System.assertEquals(
				true,
				infoList.size() > 0,
				'The list cannot be empty'
			);
		} catch (AuraHandledException ex) {
			System.assertEquals(
				System.Label.Scheduled_email_ctrl_invalid_field,
				ex.getMessage(),
				'Object Name cannot be Null or Empty'
			);
		}
	}
}