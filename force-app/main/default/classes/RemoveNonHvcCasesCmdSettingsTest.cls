/**
 * @File Name          : RemoveNonHvcCasesCmdSettingsTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/18/2024, 10:51:41 AM
**/
@IsTest
private without sharing class RemoveNonHvcCasesCmdSettingsTest {

	@IsTest
	static void recordsLimit() {
		Test.startTest();
		Integer result = RemoveNonHvcCasesCmdSettings.recordsLimit;
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	@IsTest
	static void lastModifiedDate() {
		Test.startTest();
		Datetime result = RemoveNonHvcCasesCmdSettings.lastModifiedDate;
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}
	
}