/**
 * @File Name          : OmnichanelPriorityConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/25/2024, 1:02:19 PM
**/
public inherited sharing class OmnichanelPriorityConst {

	public static final Integer DEFAULT_PRIORITY = 400;
	public static final Integer FINAL_PRIORITY_MIN_VALUE = 1;
	public static final Integer PRIORITY_TIER_1_WEIGHT = 300;
	public static final Integer PRIORITY_TIER_2_WEIGHT = 200;
	public static final Integer PRIORITY_TIER_3_WEIGHT = 100;
	public static final Integer PRIORITY_TIER_4_WEIGHT = 0;
	

}