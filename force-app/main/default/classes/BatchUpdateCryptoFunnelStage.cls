/* Name: BatchUpdateCryptoFunnelStage
 * Description : Batch to update Crypto Funnel Stage on fxAccount based on Paxos Status
 * Author: Eugene
 * Created Date : 2022-01-31
 */
public with sharing class BatchUpdateCryptoFunnelStage implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection 
{
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchUpdateCryptoFunnelStage(String filter) {
        query = 'SELECT Id, fxAccount__r.Funnel_Stage__c, Paxos_Status__c, Admin_Disabled__c ' +
        'FROM Paxos__c ' +
        'WHERE Paxos_Status__c != NULL AND fxAccount__r.Funnel_Stage__c = NULL ' +
        (String.isNotBlank(filter) ? filter : '');
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, fxAccount__r.Funnel_Stage__c, Paxos_Status__c, Admin_Disabled__c ' +
        'FROM Paxos__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('start: ' + Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Paxos__c> scope) {
        System.debug('scope: ' + scope);
        List<fxAccount__c> fxasToUpdate = new List<fxAccount__c>();

        for(Paxos__c px : scope)
        {
            fxAccount__c fxa = new fxAccount__c(Id = px.fxAccount__c);

            if(px.Paxos_Status__c == Constants.PAXOS_STATUS_DISABLED)
            {
                fxa.Funnel_Stage__c = px.Admin_Disabled__c ? Constants.CRYPTO_FUNNEL_DISABLED_BY_PAXOS : Constants.CRYPTO_FUNNEL_CLOSED;
            }
            else if(PaxosUtil.PaxosStatusFunnelStageMap.keySet().contains(px.Paxos_Status__c))
            {
                fxa.Funnel_Stage__c = PaxosUtil.PaxosStatusFunnelStageMap.get(px.Paxos_Status__c);
            }
            
            fxasToUpdate.add(fxa);
        }

        if(fxasToUpdate.size() > 0)
        {
            update fxasToUpdate;
        }
    }

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}