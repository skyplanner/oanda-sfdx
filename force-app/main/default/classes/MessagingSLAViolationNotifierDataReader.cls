/**
 * @File Name          : MessagingSLAViolationNotifierDataReader.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 9:45:36 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 4:47:57 PM   aniubo     Initial Version
**/
 public with sharing class MessagingSLAViolationNotifierDataReader implements SLAViolationDataReader{
    public List<SObject> getData(
		SLAViolationNotificationData notificationData
	) {
		ChatSLAViolationNotificationData chatNotificationData = (ChatSLAViolationNotificationData) notificationData;
		return MessagingSessionRepo.getMessagingSessions(
			chatNotificationData.recordIds
		);
	}
}