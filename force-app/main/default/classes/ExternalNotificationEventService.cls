public with sharing class ExternalNotificationEventService {
    
    public static final String ORIGIN_FXACCOUNT_FUNNELSTAGE_CHANGE = 'FXAccountFunnelStageChange';
    public static final String ORIGIN_FXACCOUNT_PAXOSSTATUS_CHANGE = 'FXAccountPaxosStatusChange';
    public static final String ORIGIN_FXACCOUNT_USSHARESTRADINGENABLED_CHANGE = 'FXAccountUsSharesTradingEnabledChange';

    public static External_Notification_Event__e createEvent(String origin, String body, String relatedId, String sObj){
        External_Notification_Event__e ev = new External_Notification_Event__e();
        ev.Origin__c = origin;
        ev.RelatedRecordId__c = relatedId;
        ev.MessageBody__c = body;
        ev.SObject__c = sObj;
        return ev;
    }

    public static External_Notification_Event__e createEvent(String origin, SObject record){

        return createEvent(origin, Utilities.serialize(record, true), record.Id, Utilities.getSObjectTypeFromId(record.Id));
    }

    public static void publishEvent(External_Notification_Event__e ev){
        EventBus.publish(ev);
    }

    public static void publishEvents(List<External_Notification_Event__e> events){
        EventBus.publish(events);
    }

    public static void createAndPublishEvent(String origin, String body, String relatedId, String sObj){
        External_Notification_Event__e ev = createEvent(origin, body, relatedId, sObj);
        publishEvent(ev);
    }

    public static void createAndPublishEvent(String origin, SObject record){
        External_Notification_Event__e ev = createEvent(origin, record);
        publishEvent(ev);
    }
}