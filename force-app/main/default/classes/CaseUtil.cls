public without sharing class CaseUtil {
	
	public static final String NAME_CASE = 'Case';
	private static AssignmentRule onBoardingAssignmentRule;
	public static Database.DMLOptions caseAssignmentDmlOpts = getDMLOptionFromCaseAssignmentRule();

	private static final Set<String> SUBJECTS_TO_CLOSE = new Set<String> {
        '[TMS] Experienced Clients Alert',
        '[TMS] Alert: Monitorowanie ważności dowodów osobistych / Monitorowanie ważności analiz ryzyka (DOK_E',
		'[TMS] Alert: Monitorowanie ważności dowodów osobistych / Monitorowanie ważności analiz ryzyka (DOK_P',
        '[TMS] Salda ujemne',
        'Pierwsza wpłata na rachunek'
    };

	// Gilbert created on Jan 25, 2017
	//
	// create or search onboarding case by fxAccount ID, and return a mapping between fxAccount Id and onBoarding case
	// As this method updates and create cases, don't run it in a loop.
	public static Map<Id, Case> getOnBoardingCaseByFxAccount(List<Id> fxaIds,
			String caseOrigin, String caseStatus, Boolean reOpenCase) {
		return getOnBoardingCaseByFxAccount(fxaIds, caseOrigin,
				caseStatus, null, reOpenCase);
	}

	/**
     * Gilbert created on Jan 25, 2017
	 * create or search onboarding case by fxAccount ID, and return a mapping 
	 * between fxAccount Id and onBoarding case
	 * As this method updates and create cases, don't run it in a loop.
	 * @param fxaIds
	 * @param caseOrigin
	 * @param caseStatus
	 * @param caseSubject
	 * @param reOpenCase
	 */
    public static Map<Id, Case> getOnBoardingCaseByFxAccount(
			List<Id> fxaIds, String caseOrigin, String caseStatus,
			String caseSubject, Boolean reOpenCase) {
    	Map<Id, Case> caseByfxaId = new Map<Id, Case>();
		Database.DMLOptions dmlOpts;
		Client_Onboarding_Setting__mdt setting;
		Map<String, Client_Onboarding_Setting__mdt> settingByDivision;
		List<fxAccount__c> fxas;
		List<Id> filteredFxaIds;
		List<Case> casesToUpsert;
    	
    	//get all the fxAccount records
    	if (fxaIds == null || fxaIds.isEmpty()) return caseByfxaId;
		
    	//get onboarding case setting, and filter fxAccounts by division
    	settingByDivision = CustomSettings.getOnboardingSettingByDivision();
    	fxas = new List<fxAccount__c>();
    	filteredFxaIds = new List<Id>();

    	for (fxAccount__c fxa : [
					SELECT Id,
						Name,
						Account__c,
						Lead__c,
						Contact__c,
						Division_Name__c,
						fxTrade_User_Id__c,
						Is_MyInfo_STP_Flow__c
					FROM fxAccount__c
					WHERE Id IN :fxaIds
				]) {
    		setting = settingByDivision.get(fxa.Division_Name__c);
            if (setting != null && setting.Create_Onboarding_Case__c) {
            	fxas.add(fxa);
            	filteredFxaIds.add(fxa.Id);
            }
    	}

    	if (fxas.isEmpty()) {
    		System.debug('No valid fxAccounts for onboarding cases');
    		return caseByfxaId;
    	}
    	 
    	// check if there are existing onboarding cases
    	casesToUpsert = new List<Case>();
		for (Case ca : [
					SELECT Id,
						RecordTypeId,
						Status,
						fxAccount__c,
						AccountId,
						Lead__c,
						ContactId,
						fxTrade_User_Id__c
					FROM Case
					WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId()
					AND fxAccount__c IN :filteredFxaIds
					ORDER BY CreatedDate ASC
				]){
			caseByfxaId.put(ca.fxAccount__c, ca);
		}
    	
    	//map fxAccount with Case and create new cases if not exist
		dmlOpts = getDMLOptionFromCaseAssignmentRule();

    	for (fxAccount__c fxa : fxas) {
    		Case theCase = caseByfxaId.get(fxa.Id);
    		
    		if (theCase == null) {
    			// give a dummy case name,
				// workflow rule will change it afterwards
				//SP-14725 - fxAccounts without STP flow marked must be set to 'Open' for OAP
    			theCase = new Case(
					RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
					Status = (!fxa.Is_MyInfo_STP_Flow__c && Constants.DIVISIONS_ASIA_LIST.contains(fxa.Division_Name__c))?
						'Open' : caseStatus,
					fxAccount__c = fxa.Id, 
					AccountId = fxa.Account__c,
					Lead__c = fxa.Lead__c,
					ContactId = fxa.Contact__c,
					fxTrade_User_Id__c = fxa.fxTrade_User_Id__c,
					Subject = String.isNotBlank(caseSubject) ? caseSubject : null,
					Origin = caseOrigin
				);
				theCase.setOptions(dmlOpts);

				// should be inserted
				casesToUpsert.add(theCase);
				caseByfxaId.put(fxa.Id, theCase);
			} else if (reOpenCase == true && theCase.Status == 'Closed') {
				theCase.Status = 'Re-opened';
				// should then be updated
				casesToUpsert.add(theCase);
			}
		}
		
		if (!casesToUpsert.isEmpty()) {
			System.debug('Cases upserted: ' + casesToUpsert.size());
			upsert casesToUpsert;
		}

		return caseByfxaId;
	}
	
	public static Map<Id, Case> createorphanOnboardingCase(String caseOrigin, String caseStatus, String emailAddr, String emailSubject, String fromName){
		Map<Id, Case> caseMap = new Map<Id, Case>();
		Case caseObj = new Case(
			RecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId(),
			Status = caseStatus,
			Origin = caseOrigin,
			Subject = emailSubject,
			SuppliedEmail = emailAddr,
			SuppliedName = fromName
		);
		caseObj.setOptions(caseAssignmentDmlOpts);
		insert caseObj;
		caseMap.put(caseObj.Id, caseObj);
		return caseMap;
	}
    
    private static Database.DMLOptions getDMLOptionFromCaseAssignmentRule(){
    	
    	if(onBoardingAssignmentRule == null){
	    	//Fetching the assignment rules on case
	        onBoardingAssignmentRule = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Name = 'Onboarding Assignment' LIMIT 1];
        
    	}
    	
    	Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= onBoardingAssignmentRule.Id;
        
        return dmlOpts;
    	
    }

	/**
   * Know if the doc is 'Scan' type
   */
	public static Boolean isOnBoardingType(Case c){
		return c.RecordTypeId ==
				RecordTypeUtil.getOnBoardingCaseTypeId();
	}

	/**
	* @description Method checks if case contains the subject which should be closed.
	* @author Jakub Fik | 2024-07-01 
	* @param subject 
	* @return Boolean 
	**/
	public static Boolean isSubjectToClose(String subject) {
		if(String.isBlank(subject))  {
			return false;
		}
		for(String loopSubject : SUBJECTS_TO_CLOSE) {
			if(subject.contains(loopSubject)) {
				return true;
			}
		}
		return false;
	}

	/**
	* @description Method return email address from subject form New Document Deployed alert.
	* @author Jakub Fik | 2024-07-22 
	* @param subject 
	* @return String 
	**/
	public static String getEmailFromSubject(String subject) {
		String emailAddress;
		if(String.isNotBlank(subject) && subject.contains(Constants.NEW_DOCUMENT_UPLOADED)) {
			emailAddress = subject.split(' - ')[1];
		}
		return emailAddress;
	}

}