/**
 * @File Name          : SPLiveChatTranscriptCtrl.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/21/2023, 11:34:56 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/15/2019, 11:28:34 AM   dmorales     Initial Version
 **/
public without sharing class SPLiveChatTranscriptCtrl {
	@AuraEnabled
	public static Account getAccountFromChatTranscript(String Id) {
		LiveChatTranscript chatTranscript = (LiveChatTranscript) Database.query(
			'Select AccountId ' + 'from LiveChatTranscript  where Id = :Id'
		);
		if (chatTranscript != null && chatTranscript.AccountId != null) {
			String idAccount = chatTranscript.AccountId;
			Account accountRecord = (Account) Database.query(
				'Select Id, fxAccount__c ' +
				'from Account where Id = :idAccount'
			);
			return accountRecord;
		} else
			return null;
	}

	@AuraEnabled
	public static Account getAccount(String Id) {
		Account accountRecord = (Account) Database.query(
			'Select Id, fxAccount__c ' + 'from Account where Id = :Id'
		);
		return accountRecord;
	}

	@AuraEnabled
	public static Case getCaseFromChatTranscript(String Id) {
		LiveChatTranscript chatTranscript = (LiveChatTranscript) Database.query(
			'Select CaseId ' + 'from LiveChatTranscript  where Id = :Id'
		);
		if (chatTranscript != null && chatTranscript.CaseId != null) {
			String idCase = chatTranscript.CaseId;
			Case caseRecord = (Case) Database.query(
				'Select Is_HVC__c,' +
					'Date_Customer_Became_Core__c ' +
					'from Case where Id = :idCase'
			);
			return caseRecord;
		} else
			return null;
	}
	@AuraEnabled
	public static Account getAccountDynamic(String Id, String sObjectName) {
		Account accountRecord;
		if (sObjectName == 'ChatTranscript') {
			accountRecord = readAccountIdFromChatTranscript(Id);
		} else if (sObjectName == 'MessagingSession') {
			accountRecord = readAccountIdFromMessagingSession(Id);
		} else if (sObjectName == 'Account') {
			accountRecord = readAccount(Id);
		}
		return accountRecord;
	}

	private static Account readAccountIdFromChatTranscript(Id recordId) {
		List<LiveChatTranscript> transcripts = [
			SELECT Account.Id, Account.fxAccount__c
			FROM LiveChatTranscript
			WHERE Id = :recordId
		];
		return transcripts.isEmpty() ? null : transcripts[0].Account;
	}

	private static Account readAccountIdFromMessagingSession(Id recordId) {
		List<MessagingSession> sessions = [
			SELECT EndUserAccount.Id, EndUserAccount.fxAccount__c
			FROM MessagingSession
			WHERE Id = :recordId
		];
		return sessions.isEmpty() ? null : sessions[0].EndUserAccount;
	}

	private static Account readAccount(Id accountId) {
		List<Account> accounts = [
			SELECT Id, fxAccount__c
			FROM Account
			WHERE Id = :accountId
		];
		return accounts.isEmpty() ? null : accounts[0];
	}
}