@isTest
public class ComplianceRecordsCtrlTest {
    @IsTest
    static void fetchAgreements() {

        insert new LegalAgreement_Settings__c(
            Get_Agreements_Resource__c = '/{user_id}/legal-agreements',
            Client_Id__c = '500d599d11cb4af99f9cdacaa3d59a99',
            Client_Secret__c = 'Ab9D1DBFF9e549e4a812E7548AEdBF8c');

        insert new List<Legal_Agreement_Mapping__c>{
            new Legal_Agreement_Mapping__c(
                Name = 'fxtrade_customer_agreement',
                Code__c = 17,
                Display_Name__c = 'fxtrade customer agreement'),
            new Legal_Agreement_Mapping__c(
                Name = 'fxtrade_additional_disclaimer',
                Code__c = 18,
                Display_Name__c = 'fxtrade additional disclaimer'),
            new Legal_Agreement_Mapping__c(
                Name = '8sf_terms',
                Code__c = 19,
                Display_Name__c = '8sf terms')
        };

        String strLegalAgs =
        '[' +
        '   {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2020-08-24",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"17"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2020-08-25",' +
        '       "update_Datetime":"2021-08-09T22:50:17",' +
        '       "create_datetime":"2021-08-09T22:50:17",' +
        '       "code":"17"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2017-05-08",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"18"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2014-10-29",' +
        '       "update_Datetime":"2021-08-09T21:50:17",' +
        '       "create_datetime":"2021-08-09T21:50:17",' +
        '       "code":"19"' +
        '    },' +
        '    {' +
        '       "country":null,' +
        '       "language":null,' +
        '       "version":"2014-10-30",' +
        '       "update_Datetime":"2021-08-10T21:50:17",' +
        '       "create_datetime":"2021-08-10T21:50:17",' +
        '       "code":"19"' +
        '    }' +
        ' ]';

        // Set test mock with right response
		Test.setMock(
            HttpCalloutMock.class,
            new CalloutMock(
                200,
                strLegalAgs,
                'success',
                null));

        Lead l = new Lead(
            LastName='test',
            Email='test@oanda.com');
        insert l;
        
        fxAccount__c fxAcc = new fxAccount__c(
            Name = 'test',
            RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
            Lead__c = l.Id,
            Division_Name__c = 'OANDA Corporate',
            fxTrade_User_Id__c = 123456);
        insert fxAcc;

        Test.startTest();

        // Retrieve legal agreements
        List<LegalAgreement> legalAgs =
            ComplianceRecordsCtrl.fetchAgreements(fxAcc.Id);

        System.assertEquals(
            legalAgs.size(),
            3,
            'Should have 3 legal agreements');

        for(LegalAgreement la :legalAgs) {
            if(la.name == 'fxtrade customer agreement') {
                System.assertEquals(
                    la.version.day(),
                    25,
                    'Have not the right parent. Should be the last version');
                System.assertEquals(
                    la.children.size(),
                    1,
                    'Do not have only one child');
                System.assertEquals(
                    la.children[0].version.day(),
                    24,
                    'Have not the right child. Should be the older version');
            } else if(la.name == 'fxtrade additional disclaimer') {
                System.assertEquals(
                    la.version.day(),
                    8,
                    'Have not the right version day.');
                System.assert(
                    la.children == null,
                    'Should not have children.');
            } else if(la.name == '8sf terms'){
                System.assertEquals(
                    la.version.day(),
                    30,
                    'Have not the right parent. Should be the last version');
                System.assertEquals(
                    la.children.size(),
                    1,
                    'Do not have only one child');
                System.assertEquals(
                    la.children[0].version.day(),
                    29,
                    'Have not the right child. Should be the older version');
            }
        }

        Test.stopTest();
    }

    @IsTest
    public static void fetchKIDsTestNotAllowed() {
        TestDataFactory tdf = new TestDataFactory();
        fxAccount__c fxA = tdf.createFXTradeAccount(tdf.createTestAccount());
        fxA.Division_Name__c = 'Test invalid';
        update fxA;

        Test.startTest();
        ComplianceRecordsUtil.KIDResponse response =  ComplianceRecordsCtrl.fetchKIDs(fxA.Id);
        Test.stopTest();
        
        System.assertEquals(false, response.isAllowed);

    }

    @IsTest
    public static void fetchKIDsTestAllowed() {
        
        TestDataFactory tdf = new TestDataFactory();
        fxAccount__c fxA = tdf.createFXTradeAccount(tdf.createTestAccount());
        
        List<Schema.PicklistEntry> allowedDocNames
            = Compliance_Record__c.Document_Name__c.getDescribe().getPickListValues();

        TestDataFactory.createComplianceRecord(fxA.Id, allowedDocNames[0].getValue(), '1.0', Datetime.now());
        TestDataFactory.createComplianceRecord(fxA.Id, allowedDocNames[0].getValue(), '1.1', Datetime.now().addDays(1));
        TestDataFactory.createComplianceRecord(fxA.Id, allowedDocNames[0].getValue(), '1.2', Datetime.now().addDays(2));
        TestDataFactory.createComplianceRecord(fxA.Id, allowedDocNames[1].getValue(), '1.0', Datetime.now());

        String allowedDivisionsString = SPGeneralSettings.getInstance().getValue('KID_Allowed_Divisions');
        List<String> allowedDivisions = String.isBlank(allowedDivisionsString) 
            ? new List<String>() : allowedDivisionsString.split(';');


        if (!allowedDivisions.isEmpty()) {
            fxA.Division_Name__c = allowedDivisions[0];
            update fxA;

            Test.startTest();
            ComplianceRecordsUtil.KIDResponse response =  ComplianceRecordsCtrl.fetchKIDs(fxA.Id);
            Test.stopTest();

            System.assertEquals(true, response.isAllowed);
            System.assertEquals(2, response.results.size());
            System.assertEquals(2, response.results[0].children.size());
        } else {
            Test.startTest();
            ComplianceRecordsUtil.KIDResponse response =  ComplianceRecordsCtrl.fetchKIDs(fxA.Id);
            Test.stopTest();

            System.assertEquals(false, response.isAllowed);
            System.assertEquals(null, response.results);
        }
    }

    @IsTest
    public static void fetchKIDsTestException() {

        TestDataFactory tdf = new TestDataFactory();
        fxAccount__c fxA = tdf.createFXTradeAccount(tdf.createTestAccount());
        String fxAccountId = fxA.Id;

        delete fxA;

        LogException ex;

        Test.startTest();
        try {
            //not existing id
            ComplianceRecordsUtil.KIDResponse response =  ComplianceRecordsCtrl.fetchKIDs(fxAccountId);
        } catch (LogException lex) {
            ex = lex;
        }
        Test.stopTest();
        
        System.assertNotEquals(null, ex, 'Expected excepion type is LogException');

    }
}