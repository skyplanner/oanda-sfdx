@isTest
private class BatchTriggerLeadAssignmentTest {
    
    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
    
    
    static testMethod void testAssignLead() {
        
        CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
        
        for (Integer i = 0; i < BatchTriggerLeadAssignment.BATCH_SIZE; i++) {
            insertFxtradeUser();
        }
        
        List<fxAccount__c> fxAccounts = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c];
        
        for (fxAccount__c fxAccount : fxAccounts) {
            fxAccount.Trigger_Lead_Assignment_Rules__c = true;
        }
        
        update fxAccounts;
        
        
        test.startTest();
        BatchTriggerLeadAssignment.executeBatch();
        test.stopTest();
        
        List<Lead> leads = [SELECT Id, OwnerId, IsConverted FROM Lead];
        
        System.assertEquals(BatchTriggerLeadAssignment.BATCH_SIZE, leads.size());
        
        //for (Lead lead : leads) {
        //	System.assert(SYSTEM_USER.Id != lead.OwnerId);
        //}
    }
    
    
    static testMethod void testDuplicateWithPreviousEmail() {
        
        CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
        
        //create existing converted lead
        Lead ld = TestDataFactory.getTestLead(LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, 'test@example.org', 'Awaiting Client Info');
        insert ld;
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(ld.id);
        lc.setConvertedStatus('Hot');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        ld.Email = 'testa@example.com';
        update ld;
        
        //create new lead with previous email
        Id lead1Id = insertFxtradeUser('test');
        
        fxAccount__c fxa = [select id, Trigger_Lead_Assignment_Rules__c from fxAccount__c where Lead__c = :lead1Id limit 1];
        fxa.Trigger_Lead_Assignment_Rules__c = true;
        update fxa;
        
        test.startTest();
        BatchTriggerLeadAssignment.executeBatch();
        test.stopTest();
        
        
        //since there are no duplicated non-converted leads, lead1 should be assigned
        fxa = [select id, Trigger_Lead_Assignment_Rules__c from fxAccount__c where Lead__c = :lead1Id limit 1];
        System.assertEquals(false, fxa.Trigger_Lead_Assignment_Rules__c);
        
    }
    
    //TO DO COMPLETE REFACTOR!!!!

    static testMethod void testUnmergedLeads() {
        TestsUtil.forceJobsSync = true;
        
        CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
        
        System.runAs(SYSTEM_USER) {
            Id l1 = insertFxtradeUser('foo1');
                        checkRecursive.SetOfIDs = new Set<Id>();

            Id l2 = insertFxtradeUser('foo1');
                        checkRecursive.SetOfIDs = new Set<Id>();

            Id l3 = insertFxtradeUser('foo2');
                        checkRecursive.SetOfIDs = new Set<Id>();

            System.debug('001 - Inserted fxtrade users');
            
            List<Lead> leads = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l1 OR Id =: l2 OR Id =: l3];
            //Lead lead2 = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l2];
            //Lead lead3 = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l3];
            
            System.assertEquals(SYSTEM_USER.Id, leads[0].OwnerId);
            System.assertEquals(SYSTEM_USER.Id, leads[1].OwnerId);
            System.assertEquals(SYSTEM_USER.Id, leads[2].OwnerId);
                        checkRecursive.SetOfIDs = new Set<Id>();

            BatchTriggerLeadAssignment.executeInline();
            
            System.debug('002 - BatchTriggerLeadAssignment complete');
            
            fxAccount__c fx1 = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: l1];
            //fxAccount__c fx2 = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: l2];
            fxAccount__c fx3 = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: l3];
            
            fx1.Trigger_Lead_Assignment_Rules__c = true;
            fx3.Trigger_Lead_Assignment_Rules__c = true;
            
            System.debug('003 - Updating fxAccounts...');
            update fx1;
            checkRecursive.SetOfIDs = new Set<Id>();

            update fx3;
            System.debug('004 - Updated fxAccounts');
            checkRecursive.SetOfIDs = new Set<Id>();
            Test.startTest();
            checkRecursive.SetOfIDs = new Set<Id>();
            
            BatchTriggerLeadAssignment.executeInline();
            System.debug('005 - BatchTriggerLeadAssignment');
            
            Lead lead1 = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l1];
            Lead lead2 = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l2];
            //lead3 = [SELECT Id, Email, OwnerId FROM Lead WHERE Id =: l3];
            
            System.assertEquals(SYSTEM_USER.Id, lead1.OwnerId);
            System.assertEquals(SYSTEM_USER.Id, lead2.OwnerId);
            //System.assert(SYSTEM_USER.Id != lead3.OwnerId);
            
            //fx1 = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: l1];
            //fx2 = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: l2];
            checkRecursive.SetOfIDs = new Set<Id>();
            
            System.assert(fx1.Trigger_Lead_Assignment_Rules__c);
            //	    	System.assert(!fx2.Trigger_Lead_Assignment_Rules__c);
            
            System.debug('006 - About to trigger batch jobs...');
            checkRecursive.SetOfIDs = new Set<Id>();
            
            BatchMergeLeadsScheduleable.executeInline();
            System.debug('007 - BatchMergeLeadsScheduleable complete');
            checkRecursive.SetOfIDs = new Set<Id>();
            
            BatchTriggerLeadAssignment.executeInline();
            System.debug('008 - BatchTriggerLeadAssignment complete');
            
            lead1 = [SELECT Id, OwnerId FROM Lead WHERE Email =: lead1.Email];
            
            //System.assert(SYSTEM_USER.Id != lead1.OwnerId);
            
            List<fxAccount__c> fxs = [SELECT Id, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Lead__c =: lead1.Id];
            
            System.assertEquals(2, fxs.size());
            System.assert(!fxs.get(0).Trigger_Lead_Assignment_Rules__c);
            System.assert(!fxs.get(1).Trigger_Lead_Assignment_Rules__c);
            
            Test.stopTest();
        }
        
        
        
    }
    
    
    private static Id insertFxtradeUser() {
        String uname = 'joe' + Math.round(Math.random() * 1000000);
        return insertFxtradeUser(uname);
    }
    
    private static Id insertFxtradeUser(String uname) {
        
        Lead lead;
        System.runAs(SYSTEM_USER) {
            
            lead = new Lead();
            lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            lead.LastName = 'Schmoe';
            lead.Email = uname + '@example.org';
            lead.Territory__c = 'North America';
            
            insert lead;
            checkRecursive.SetOfIDs = new Set<Id>();

            fxAccount__c fxAccount = new fxAccount__c();
            fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            fxAccount.Name = uname;
            fxAccount.Lead__c = lead.Id;
            
            insert fxAccount;
            
        }
        return lead.Id;
    }
    
    static testMethod void testRunOnDayofWeek() {
        List<String> daysOfWeek = new List<String> {'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'};
            
            Datetime dt = System.now();
        String curDayOfWeek = dt.format('EEE').toUpperCase();
        
        BatchTriggerLeadAssignment a = new BatchTriggerLeadAssignment(150);
        a.runOnDaysOfWeek = '';
        
        for(String day : daysOfWeek){
            if(day != curDayOfWeek){
                a.runOnDaysOfWeek = a.runOnDaysOfWeek + ',' + day;
            }
        }
        
        //the current day is not included
        System.assertEquals(false, a.shouldRun());
        
        //current day is included
        a.runOnDaysOfWeek = curDayOfWeek;
        System.assertEquals(true, a.shouldRun());
    }
}