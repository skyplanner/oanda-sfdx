public with sharing class FunnelStatus {
	
	public static final String NOT_STARTED = 'Not Started';
	public static final String FUNNEL_STAGE_NAME_FUNDING_PROHIBITED = 'Funding Prohibited';
	public static final String BEFORE_YOU_BEGIN = 'Before You Begin';
	public static final String PERSONAL_AND_EMPLOYMENT = 'Personal And Employment';
	public static final String FINANCIAL = 'Financial';
	public static final String LEGAL = 'Legal';
	public static final String REVIEWED_AND_SUBMITTED = 'Reviewed And Submitted';
	public static final String AWAITING_CLIENT_INFO = 'Awaiting Client Info';
	public static final String DOCUMENTS_UPLOADED = 'Documents Uploaded';
	public static final String MORE_INFO_REQUIRED = 'More Info Required';
	public static final String READY_FOR_FUNDING = 'Ready For Funding';
	public static final String FUNDING_FAILED = 'Funding Failed';
	public static final String FUNDED = 'Funded'; 
	public static final String TRADED = 'Traded';
	public static final String DEMO_REGISTERED = 'Demo Registered';
	public static final String DEMO_TRADED = 'Demo Traded';
	
	public static final String EST_PHONE_VALIDATED = '8ST - Phone Validated';
	public static final String EST_FUNDED = '8ST - Funded';
	public static final String EST_DOCUMENTS_UPLOADED = '8ST - Documents Uploaded';
	public static final String EST_MORE_INFO_REQUIRED = '8ST - More Info Required';
	public static final String EST_ID_VERIFIED = '8ST - ID Verified';
	public static final String EST_CLOSED_WON = '8ST - Closed Won';

	public static final String RETENTION_NEW = 'New';
	public static final String RETENTION_ATTEMPTED = 'Attempted';
	public static final String RETENTION_CONTACTED = 'Contacted';
	public static final String RETENTION_CLOSED_WON = 'Closed Won';
	public static final String RETENTION_CLOSED_LOST = 'Closed Lost';
	public static final String RETENTION_SUFFIX_NO_CONTACT = ' - No Contact';
	public static final String RETENTION_CLOSED_WON_NO_CONTACT = RETENTION_CLOSED_WON + RETENTION_SUFFIX_NO_CONTACT;
	public static final String RETENTION_CLOSED_LOST_NO_CONTACT = RETENTION_CLOSED_LOST + RETENTION_SUFFIX_NO_CONTACT;
	public static final String RETENTION_CLOSED_LOST_DO_NOT_CONTACT = 'Closed Lost - Do Not Contact';
	
	public static final Set<String> RETENTION_CLOSED = new Set<String> { RETENTION_CLOSED_WON, RETENTION_CLOSED_WON_NO_CONTACT, RETENTION_CLOSED_LOST_NO_CONTACT, RETENTION_CLOSED_LOST, RETENTION_CLOSED_LOST_DO_NOT_CONTACT };
	public static final Set<String> RETENTION_DONT_CREATE_OPP = new Set<String> { RETENTION_NEW, RETENTION_ATTEMPTED, RETENTION_CONTACTED };
	public static final Set<String> RETENTION_CREATE_OPP_SAME_OWNER = new Set<String> { RETENTION_CLOSED_WON };
	public static final Set<String> RETENTION_CREATE_OPP_DIFF_OWNER = new Set<String> { RETENTION_CLOSED_WON_NO_CONTACT, RETENTION_CLOSED_LOST_NO_CONTACT, RETENTION_CLOSED_LOST };
	
	public static final Set<String> CLOSED_WON_STAGES = new Set<String> { TRADED, EST_CLOSED_WON };
	
	public static final List<String> FUNNEL_STATUS_LIST = new List<String>();
	static {
		FUNNEL_STATUS_LIST.add(NOT_STARTED);
		FUNNEL_STATUS_LIST.add(DEMO_REGISTERED);
		FUNNEL_STATUS_LIST.add(DEMO_TRADED);
		FUNNEL_STATUS_LIST.add(FUNNEL_STAGE_NAME_FUNDING_PROHIBITED);
		FUNNEL_STATUS_LIST.add(BEFORE_YOU_BEGIN);
		FUNNEL_STATUS_LIST.add(PERSONAL_AND_EMPLOYMENT);
		FUNNEL_STATUS_LIST.add(FINANCIAL);
		FUNNEL_STATUS_LIST.add(LEGAL);
		FUNNEL_STATUS_LIST.add(REVIEWED_AND_SUBMITTED);
		FUNNEL_STATUS_LIST.add(AWAITING_CLIENT_INFO);
		FUNNEL_STATUS_LIST.add(DOCUMENTS_UPLOADED);
		FUNNEL_STATUS_LIST.add(MORE_INFO_REQUIRED);
		FUNNEL_STATUS_LIST.add(READY_FOR_FUNDING);
		FUNNEL_STATUS_LIST.add(FUNDING_FAILED);
		FUNNEL_STATUS_LIST.add(FUNDED);
		FUNNEL_STATUS_LIST.add(TRADED);
	}
		
	public static final Map<String, Integer> FUNNEL_STATUS_ORDER = new Map<String, Integer>();
	static {
		FUNNEL_STATUS_ORDER.put(NOT_STARTED.toLowerCase(), 0);
		FUNNEL_STATUS_ORDER.put(DEMO_REGISTERED.toLowerCase(), 1000);
		FUNNEL_STATUS_ORDER.put(DEMO_TRADED.toLowerCase(), 1100);
		FUNNEL_STATUS_ORDER.put(BEFORE_YOU_BEGIN.toLowerCase(), 1200);
		FUNNEL_STATUS_ORDER.put(FUNNEL_STAGE_NAME_FUNDING_PROHIBITED.toLowerCase(), 1250);
		FUNNEL_STATUS_ORDER.put(PERSONAL_AND_EMPLOYMENT.toLowerCase(), 1300);
		FUNNEL_STATUS_ORDER.put(FINANCIAL.toLowerCase(), 1400);
		FUNNEL_STATUS_ORDER.put(LEGAL.toLowerCase(), 1500);
		FUNNEL_STATUS_ORDER.put(REVIEWED_AND_SUBMITTED.toLowerCase(), 1600);
		FUNNEL_STATUS_ORDER.put(AWAITING_CLIENT_INFO.toLowerCase(), 1700);
		FUNNEL_STATUS_ORDER.put(DOCUMENTS_UPLOADED.toLowerCase(), 1800); // DOCUMENTS_UPLOADED and MORE_INFO_REQUIRED are at same level since they happen concurrently
		FUNNEL_STATUS_ORDER.put(MORE_INFO_REQUIRED.toLowerCase(), 1800); //
		FUNNEL_STATUS_ORDER.put(READY_FOR_FUNDING.toLowerCase(), 1900);
		FUNNEL_STATUS_ORDER.put(FUNDING_FAILED.toLowerCase(), 2000);
		FUNNEL_STATUS_ORDER.put(FUNDED.toLowerCase(), 2100);
		FUNNEL_STATUS_ORDER.put(TRADED.toLowerCase(), 2200);
		
		// 8st accounts take precedence over old funnel flow
		// SP-12594 no longer in use
		//FUNNEL_STATUS_ORDER.put(EST_PHONE_VALIDATED.toLowerCase(), 3000);
		//FUNNEL_STATUS_ORDER.put(EST_FUNDED.toLowerCase(), 3100);
		//FUNNEL_STATUS_ORDER.put(EST_DOCUMENTS_UPLOADED.toLowerCase(), 3200);
		//FUNNEL_STATUS_ORDER.put(EST_MORE_INFO_REQUIRED.toLowerCase(), 3200);
		//FUNNEL_STATUS_ORDER.put(EST_ID_VERIFIED.toLowerCase(), 3300);
		//FUNNEL_STATUS_ORDER.put(EST_CLOSED_WON.toLowerCase(), 3400);
	}

	/**
	 * Compares two funnel status strings.  Returns:
	 * -1 if lhs < rhs
	 * 0 if lhs == rhs
	 * 1 if lhs > rhs or if lhs/rhs are DOCUMENTS_UPLOADED and MORE_INFO_REQUIRED (since this is a loop)
	 */
	public static Integer compare(String lhs, String rhs) {
		
		System.debug('FunnelStatus: Comparing ' + lhs + ' to ' + rhs);
		
		if (lhs != null) {
			lhs = lhs.toLowerCase();
		}
		if (rhs != null) {
			rhs = rhs.toLowerCase();
		}
		
		// Both strings are the same, return 0
		if (lhs == rhs) {
			System.debug('lhs == rhs, returning 0');
			return 0;
		}
		//lhs is null and rhs is not, return -1
		else if (lhs == null) {
			System.debug('lhs is null, returning -1');
			return -1;
		}
		//lhs is not null and rhs is, return 1
		else if (rhs == null) {
			System.debug('rhs is null, returning 1');
			return 1;
		}
		
		//Since we can have a loop from DOCUMENTS_UPLOADED -> MORE_INFO_REQUIRED -> DOCUMENTS_UPLOADED -> MORE_INFO_REQUIRED -> ...
		//if both the lhs and rhs are either of these 2 then we return 1 since the rhs is greater or 'later' in the funnel stage
		//than lhs.  (The case where they are identical would have been captured above by lhs == rhs)
		if ((lhs == DOCUMENTS_UPLOADED || lhs == MORE_INFO_REQUIRED) && (rhs == DOCUMENTS_UPLOADED || rhs == MORE_INFO_REQUIRED)) {
			System.debug('In docs uploaded/more info required cycle, returning 1');
			return 1;
		}
		
		System.debug('lhs ' + lhs);
		System.debug('rhs ' + rhs);
		
		Integer l = FUNNEL_STATUS_ORDER.get(lhs);
		Integer r = FUNNEL_STATUS_ORDER.get(rhs);

		if (l == null) {
			System.debug('lhs comparator is null, returning -1');
			return -1;
		}
		if (r == null) {
			System.debug('rhs comparator is null, returning 1');
			return 1;
		}
				
		if (l < r) {
			System.debug('lhs is less than rhs, returning -1');
			return -1;
		}
		else if (l > r) {
			System.debug('lhs is greater than rhs, returning 1');
			return 1;
		}
		System.debug('lhs is the same as rhs, returning 0');
		return 0;
	}
	
	public static boolean isDemoStatus(String status){
		boolean result = false;
		
		if(status == null){
			return result;
		}else{
			status = status.toLowerCase();
		}
		
		if(status == DEMO_REGISTERED.toLowerCase() || status == DEMO_TRADED.toLowerCase()){
			result = true;
		}
		
		return result;
	}
}