/**
 * @description       :
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 03-03-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiStatus {
    public final static transient String FIELD_NAME_ACCOUNT_LOCKED
        = 'account_locked';
    public final static transient String FIELD_NAME_TRADING_LOCKED
        = 'trading_locked';
    public final static transient String FIELD_NAME_DEPOSITS_LOCKED
        = 'deposits_locked';
    public final static transient String FIELD_NAME_WITHDRAWALS_LOCKED
        = 'withdrawals_locked';
    public final static transient String FIELD_NAME_TPA_AUTHORIZATION_LOCKED
        = 'tpa_authorization_locked';
    public final static transient String FIELD_NAME_EMAIL_VALIDATED
        = 'email_validated';
    public final static transient String FIELD_NAME_DOCUMENTS_APPROVED
        = 'documents_approved';
    public final static transient String FIELD_NAME_REGISTRATION_COMPLETED
        = 'registration_completed';
    public final static transient String FIELD_NAME_ACCOUNT_CLOSED
        = 'closed';
    public final static transient String FIELD_NAME_INTEREST
        = 'interest_exempt';
    public final static transient String FIELD_NAME_USER_INFO_LAST_UPDATED
        = 'user_info_last_updated';

    public Integer account_locked {get; set;}

    public Integer agreements_outdated {get; set;}

    public Integer closed {get; set;}

    public Integer deposits_locked {get; set;}

    public Integer documents_approved {get; set;}

    public Integer email_validated {get; set;}

    public Integer interest_exempt {get; set;}

    public Integer password_expired {get; set;}

    public Integer registration_completed {get; set;}

    public Integer snail_mail_validated {get; set;}

    public Integer too_many_failed_logins {get; set;}

    public Integer tpa_authorization_locked {get; set;}

    public Integer trading_locked {get; set;}

    public Long user_info_last_updated {get; set;}

    public Integer user_info_outdated {get; set;}

    public Integer withdrawals_locked {get; set;}

    public static StatusWrapper lockAccount() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_ACCOUNT_LOCKED);
        result.status.account_locked = 1;

        return result;
    }

    public static StatusWrapper setApprovedToFund() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_DOCUMENTS_APPROVED);
        
        result.status.documents_approved = 1;
        result.status.deposits_locked = 0;

        return result;
    }

    public static StatusWrapper unlockAccount() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_ACCOUNT_LOCKED);
        result.status.account_locked = 0;

        return result;
    }

    public static StatusWrapper lockTrading() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_TRADING_LOCKED);
        result.status.trading_locked = 1;

        return result; 
    }

    public static StatusWrapper unlockTrading() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_TRADING_LOCKED);
        result.status.trading_locked = 0;

        return result; 
    }

    public static StatusWrapper lockDeposits() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_DEPOSITS_LOCKED);
        result.status.deposits_locked = 1;

        return result; 
    }

    public static StatusWrapper unlockDeposits() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_DEPOSITS_LOCKED);
        result.status.deposits_locked = 0;

        return result; 
    }

    public static StatusWrapper lockWithdrawals() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_WITHDRAWALS_LOCKED);
        result.status.withdrawals_locked = 1;

        return result; 
    }

    public static StatusWrapper unlockWithdrawals() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_WITHDRAWALS_LOCKED);
        result.status.withdrawals_locked = 0;

        return result; 
    }

    public static StatusWrapper lockTpaAuthorization() {
        StatusWrapper result = getWrapper(
            1, FIELD_NAME_TPA_AUTHORIZATION_LOCKED);
        result.status.tpa_authorization_locked = 1;

        return result; 
    }

    public static StatusWrapper unlockTpaAuthorization() {
        StatusWrapper result = getWrapper(
            0, FIELD_NAME_TPA_AUTHORIZATION_LOCKED);
        result.status.tpa_authorization_locked = 0;

        return result; 
    }

    public static StatusWrapper setEmailValidated() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_EMAIL_VALIDATED);
        result.status.email_validated = 1;

        return result; 
    }

    public static StatusWrapper unsetEmailValidated() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_EMAIL_VALIDATED);
        result.status.email_validated = 0;

        return result; 
    }

    public static StatusWrapper setDocumentsApproved() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_DOCUMENTS_APPROVED);
        result.status.documents_approved = 1;

        return result; 
    }

    public static StatusWrapper unsetDocumentsApproved() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_DOCUMENTS_APPROVED);
        result.status.documents_approved = 0;

        return result; 
    }

    public static StatusWrapper setRegistrationCompleted() {
        StatusWrapper result = getWrapper(
            1, FIELD_NAME_REGISTRATION_COMPLETED);
        result.status.registration_completed = 1;

        return result; 
    }

    public static StatusWrapper unsetRegistrationCompleted() {
        StatusWrapper result = getWrapper(
            0, FIELD_NAME_REGISTRATION_COMPLETED);
        result.status.registration_completed = 0;

        return result; 
    }

    public static StatusWrapper closeAccount() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_ACCOUNT_CLOSED);
        result.status.closed = 1;

        return result;
    }

    public static StatusWrapper reopenAccount() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_ACCOUNT_CLOSED);
        result.status.closed = 0;

        return result;
    }

    public static StatusWrapper enableAllInterest() {
        StatusWrapper result = getWrapper(0, FIELD_NAME_INTEREST);
        result.status.interest_exempt = 0;

        return result; 
    }

    public static StatusWrapper disableAllInterest() {
        StatusWrapper result = getWrapper(1, FIELD_NAME_INTEREST);
        result.status.interest_exempt = 1;

        return result; 
    }

    public static StatusWrapper updatePIU() {
        StatusWrapper result = getWrapper(
            0, FIELD_NAME_USER_INFO_LAST_UPDATED);
        result.status.user_info_last_updated = Datetime.now().getTime() / 1000;

        return result; 
    }

    private static Integer getOldValue(Integer value) {
        return value == 1 ? 0 : 1;
    }

    private static AuditTrailManager.AuditTrailChange getChange(
        Integer value, String fieldName
    ) {
        AuditTrailManager.AuditTrailChange result
            = new AuditTrailManager.AuditTrailChange(
                fieldName,
                CsUtils.toBoolean(getOldValue(value)),
                CsUtils.toBoolean(value));

        return result;
    }

    private static StatusWrapper getWrapper(
        Integer value, String fieldName
    ) {
        StatusWrapper result = new StatusWrapper();
        result.status = new UserApiStatus();
        result.change = getChange(value, fieldName);

        return result;
    }

    public class StatusWrapper {
        public UserApiStatus status {get; set;}

        public AuditTrailManager.AuditTrailChange change {get; set;}
    }
}