/* Name: Lead_Before
 * Description : Batch Class to update CRA Score
 * Author: Sahil Handa
 * Last Modified by : Eugene Jung
 * Last Modified Date : 08/15/2023
* */
public class BatchToUpdateCRA implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, database.stateful{
    String query;
    Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    String email;
    Id toUserId;
    Id fromUserId;
    List<String> successfulfxAccountIds = new List<String>();
    List<Database.SaveResult> failedfxAccountSaveResults = new List<Database.SaveResult>();
    integer totalSizeRecords=0;
    
    public BatchToUpdateCRA(String q) {
   		query = q;
  	}

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		List<Id> ids = recIds;
        query = 'SELECT Id, Lead__c, Contact__c, Account__c, RecordTypeId, Employment_Status__c, Income_Source_Details__c, Citizenship_Nationality__c, ' +
        '        Industry_of_Employment__c, Division_Name__c, Source_of_Wealth_governmentBenefits__c, Source_of_Wealth_studentLoan__c, ' +
        '        Singapore_Src_of_Funds_Govt_Benefits__c, Singapore_Src_of_Funds_Student_Loan__c,Customer_Risk_Assessment_Text__c ' +
        'FROM fxAccount__c' +
        'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.querylocator start(Database.BatchableContext BC){
        //query = 'SELECT Id, Lead__c, Contact__c, Account__c, RecordTypeId, Employment_Status__c, Income_Source_Details__c,Citizenship_Nationality__c, Industry_of_Employment__c,Division_Name__c, Source_of_Wealth_governmentBenefits__c,Source_of_Wealth_studentLoan__c, Singapore_Src_of_Funds_Govt_Benefits__c, Singapore_Src_of_Funds_Student_Loan__c,Customer_Risk_Assessment_Text__c FROM fxAccount__c WHERE COUNTRY__C  =\'ALAND ISLANDS\'  OR COUNTRY__C =   \'ALBANIA\'  OR COUNTRY__C =   \'BARBADOS\'  OR COUNTRY__C =   \'BENIN\'  OR COUNTRY__C = \'BERMUDA\'  OR COUNTRY__C =   \'BONAIRE, SAINT EUSTATIUS AND SABA\'  OR COUNTRY__C =   \'CAPE VERDE\'  OR COUNTRY__C =   \'CHILE\'  OR COUNTRY__C =   \'COLOMBIA\'  OR COUNTRY__C =   \'FAROE ISLANDS\'  OR COUNTRY__C =   \'GAZA STRIP\'  OR COUNTRY__C =   \'GREENLAND\'  OR COUNTRY__C =   \'HONDURAS\'  OR COUNTRY__C =   \'HONG KONG\'  OR COUNTRY__C =   \'ICELAND\'  OR COUNTRY__C =   \'JAMAICA\'  OR COUNTRY__C =   \'K OR COUNTRY__C = EA, SOUTH\'  OR COUNTRY__C =   \'MAURITIUS\'  OR COUNTRY__C =   \'MONGOLIA\'  OR COUNTRY__C =   \'NEPAL\'  OR COUNTRY__C =   \'NICARAGUA\'  OR COUNTRY__C =   \'PAPUA NEW GUINEA\'  OR COUNTRY__C =   \'SAINT BERTHELEMY\'  OR COUNTRY__C =   \'SERBIA\'  OR COUNTRY__C =   \'SRI LANKA\'  OR COUNTRY__C =   \'SVALBARD\'  OR COUNTRY__C =   \'SWAZILAND\'  OR COUNTRY__C =   \'TAIWAN\'  OR COUNTRY__C =   \'TOKELAU\'  OR COUNTRY__C =   \'TURKEY\'  OR COUNTRY__C =   \'UGANDA\'  OR COUNTRY__C =   \'URUGUAY\'  OR COUNTRY__C =   \'VANUATU\'';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<fxAccount__c> scope){
        totalSizeRecords+=scope.size();
        
        List<String> fxAccountId = new List<String>();
        
        for(sObject s : scope){
            fxAccountRiskAssessment.calculateCustomerRiskAssessment(scope);
            fxAccountId.add(s.Id);
        }
        try{
            Database.SaveResult[] lsr = Database.update(scope,false);
            // Iterate through the Save Results 
            for(Database.SaveResult sr:lsr)
            {
                if(sr.isSuccess())
                {
                    successfulfxAccountIds.add(sr.getId());
                } else {
                    failedfxAccountSaveResults.add(sr);
                }
            }
        }catch(Exception e){
            
        }
        
    }
    public void finish(Database.BatchableContext BC){
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        email = UserInfo.getUserEmail();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('batch@acme.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        String textBody ='<br/> Total number of records processed : '+totalSizeRecords;
        textBody +='<br/> Total number of success processed : '+successfulfxAccountIds.size();
        textBody +='<br/>';
        textBody += 'Failed records:';
        textBody +='<br/>';
        
        textBody += '<table>';
        textBody += '<tr>';
        textBody += '<th>Id</th>';
        textBody += '<th>Errors</th>';
        textBody += '</tr>';
        
        for (Database.SaveResult sr : failedfxAccountSaveResults) {
            textBody += '<tr>';
            textBody += '<td>' + sr.getId() + '</td>';
            
            textBody += '<td>';
            textBody += '<ul>';
            for (Database.Error err : sr.getErrors()) {
                textBody += '<li>' + err.getStatusCode() + ', ' + err.getMessage() + '; Fields involved: ' + err.getFields() + '</li>';
            }
            textBody += '</ul>';
            textBody += '</td>';
            textBody += '</tr>';
        }
        
        textBody += '</table>';

        mail.setHtmlBody(textBody);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}