/**
 * @File Name          : MessagingCommentRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/30/2023, 12:45:50 AM
**/
public inherited sharing class MessagingCommentRepo {

	public static Messaging_Comment__c getByMessagingSessionAndName(
		ID messagingSessionId,
		String name
	) {
		List<Messaging_Comment__c> recList = [
			SELECT Id
			FROM Messaging_Comment__c
			WHERE Messaging_Session__c = :messagingSessionId
			AND Name = :name
			LIMIT 1
		];
		Messaging_Comment__c result = null;
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static List<Messaging_Comment__c> getByMessagingSession(
		ID messagingSessionId
	) {
		return [
			SELECT 
				Name, 
				Body__c
			FROM Messaging_Comment__c
			WHERE Messaging_Session__c = :messagingSessionId
			ORDER BY CreatedDate ASC
		];
	}
	
}