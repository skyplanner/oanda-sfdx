/*
    BatchNotifyNewDocUploadSchedulable - Notify Account owners on new document upload.
    This batch job will run one hour once.
*/
public with sharing class BatchNotifyNewDocUploadSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Schedulable
{
    public static final Integer BATCH_SIZE = 200;  

    //schedule every 1 hour
    public static final String CRON_SCHEDULE = '0 0 * * * ?';  

    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    string[] queryFields =  new string[]
    {
        'Id', 
        'Case__c',
        'Case__r.Customer_Name__c',
        'Account__c',
        'Account__r.OwnerId',
        'Account__r.Owner.Email',
        'lead__c',
        'lead__r.OwnerId',
        'lead__r.Owner.Email'
    };
    public final static string[] funnelStagesToCheck =  new string[]
    {
        'Documents Uploaded', 
        'More Info Required'
    };

    Map<Id, Case> caseMap;
    Map<Id, Set<Id>> ownerIdCaseListMap;
    Datetime lastOneHour;
	
    public static final Id registraionUserId = UserUtil.getUserIdByName('Registration User');
    public static final Id systemUserId = UserUtil.getUserIdByName('System User');
    public static final Id retentionUserId = UserUtil.getUserIdByName('Retention House User');
    
    @TestVisible
    public static Messaging.SingleEmailMessage[] emailMessages;
    
    public BatchNotifyNewDocUploadSchedulable() 
    {
        caseMap = new Map<Id, Case>{};
        ownerIdCaseListMap = new Map<Id, set<Id>>();

        lastOneHour = Datetime.now().addMinutes(-60);

        query = 'Select Owner.Name,' + String.join(queryFields,',') + ' From Document__c ';
        query += 'where ';
        query += 'RecordType.Name = \'Scan\'';
        query += ' and OwnerId = :registraionUserId';
        query += ' and Case__c != null';
        query += ' and Case__r.RecordType.Name = \'Onboarding\'';
        query += ' and Case__r.CreatedDate != TODAY';    
        query += ' and fxAccount__c != null';
      	query += ' and fxAccount__r.RecordType.Name = \'Retail Live\'';
        query += ' and fxAccount__r.Funnel_Stage__c IN :funnelStagesToCheck';
        query += ' and fxAccount__r.OwnerId != :systemUserId';
        query += ' and fxAccount__r.OwnerId != :retentionUserId';
        query += ' and CreatedDate >= :lastOneHour';
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'Select Owner.Name,' + String.join(queryFields,',') + ' From Document__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {       
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<Document__c> scope)
    {
        for(Document__c scopeDoc : scope)
        {
            Document__c doc = (Document__c)scopeDoc;
			            
            Id ownerId = doc.Account__c != null ?  
                            doc.Account__r.OwnerId :
                                (doc.lead__c != null ?  doc.lead__r.OwnerId : null);

            if(ownerId != null)
            {
                Set<Id> caseIds = ownerIdCaseListMap.get(ownerId);
                if(caseIds == null)
                {
                    caseIds = new Set<Id>{};
                }
                caseIds.add(doc.Case__c);
                ownerIdCaseListMap.put(ownerId, caseIds);
            }
            caseMap.put(doc.Case__c, doc.Case__r);
        }

        emailMessages = new Messaging.SingleEmailMessage[]{};
        for(Id ownerId : ownerIdCaseListMap.keySet())
        {
            Messaging.SingleEmailMessage emessage =  generateEmail(ownerId);
            emailMessages.add(emessage); 
        }
        
        if(emailMessages.size() > 0)
        {
            Messaging.sendEmail(emailMessages);
        }
    }

    private Messaging.SingleEmailMessage generateEmail(ID ownerId)
    {
        string caseSummary = '';
        set<Id> caseIds = ownerIdCaseListMap.get(ownerId);
        for(Id caseId: caseIds)
        {
            Case caseDetails = caseMap.get(caseId);
            caseSummary += '<br/><br/>' + caseDetails.Customer_Name__c + ' : ';
            caseSummary += '<html><a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/ChatClosureCasePage?id=' + caseId + '">' +'Click Here'+ '</a></html>';
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(ownerId);
        mail.setSenderDisplayName('Salesforce');
        mail.setSaveAsActivity(false);
        mail.setSubject('New Document upload from Client');

        string textBody = caseIds.size() > 1 ? 
                        '<br/> The following clients have uploaded new document. ' :
                        '<br/> The following client has uploaded new document. '; 
        textBody += 'Please click on the link to access the case.'; 
        textBody += caseSummary;

        mail.setHtmlBody(textBody);
        
        return mail;
    }
    public void finish(Database.BatchableContext bc)
    {  
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

     //schedule
     public void execute(SchedulableContext context) 
     {
         Database.executeBatch(new BatchNotifyNewDocUploadSchedulable(), BATCH_SIZE);
     }

     //schedule the batch job
     public static void schedule() 
     {  
        System.schedule('BatchNotifyNewDocUpload', CRON_SCHEDULE, new BatchNotifyNewDocUploadSchedulable());
     }
     
     public static void execute() 
     {
         Database.executeBatch(new BatchNotifyNewDocUploadSchedulable(), BATCH_SIZE);
     }
}