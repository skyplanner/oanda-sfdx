/**
 * @File Name          : SurveyEmailHandler_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/4/2019, 4:28:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/27/2019   dmorales     Initial Version
**/
@isTest
private class SurveyEmailHandler_Test {
    @isTest
    static void testConstructor() {
         SurveyEmailHandler c = new SurveyEmailHandler();
         Case caseTest = new Case();
         insert caseTest;
         c.caseID = caseTest.Id;
         System.assert(c != null);
         System.assert(c.caseObj != null);
    }
    @isTest
    static void getImgDocUrl_test() {
		ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = 'SharedDocuments' LIMIT 1][0].Id;
		Document d1 = new Document(
			Name = 'doc1.json',
			ContentType = 'application/json',
			DeveloperName = 'doc1',
			FolderId = folderId
		);
		insert d1;
        SurveyEmailHandler c = new SurveyEmailHandler();
		Test.startTest();
		String result = c.getImgDocUrl('doc1');
		Test.stopTest();
		System.assert(String.isNotBlank(result));
	}

	@isTest
	static void getImgDocUrl_test2() {
		Test.startTest();
        SurveyEmailHandler c = new SurveyEmailHandler();
		String result = c.getImgDocUrl('doc1');
		Test.stopTest();
		System.assert(String.isBlank(result));
	}
}