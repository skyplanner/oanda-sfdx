/**
 * @description       : Class used to schedule an CD update for conditionally approved customers
 * @author            : Jakub Jaworski
**/
public with sharing class ConditionallyApprovedCDCalloutBatch implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.AllowsCallouts, Database.Stateful, Schedulable {

    public static final Integer BATCH_SIZE = 1;
    public static String CALLOUT_ERROR_TYPE = 'Callout';
    public static String MISSING_CLIENT_ID_ERROR_TYPE = 'Missing CD Client Id';

    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    Map<Id,String> idToErrorMap;
    fxAccount__c currentAccount;
    Integer totalSizeOfRecords = 0;

    public ConditionallyApprovedCDCalloutBatch() {
        query = 'SELECT Id, CD_Client_ID__c FROM fxAccount__c WHERE Conditionally_Approved__c = TRUE AND Division_Name__c = \'OANDA Global Markets\' AND US_Shares_Trading_Enabled__c = TRUE AND Is_Closed__c = FALSE';
        idToErrorMap = new Map<Id,String>();
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, CD_Client_ID__c FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    public Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<fxAccount__c> scope){

            totalSizeOfRecords += scope.size();
            currentAccount = scope[0];
            
            if (currentAccount.CD_Client_ID__c == null) {
                addError(MISSING_CLIENT_ID_ERROR_TYPE, null, null);
            } else {
                try {
                        Logger.info(
                            'Updating Trading Account Info',
                            Constants.LOGGER_CURRENT_DESK_CATEGORY);

                        updateTradingAccountInfo();
                } catch(Exception e) {
                    Logger.exception(
                        Constants.LOGGER_CURRENT_DESK_CATEGORY,
                        e);
                }
            }
    }

    public void execute(SchedulableContext context) {
    	Database.executeBatch(new ConditionallyApprovedCDCalloutBatch(), BATCH_SIZE);
  	}

    public void finish(Database.BatchableContext BC){
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (!idToErrorMap.isEmpty()) {

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String email = 'salesforce@oanda.com';
                mail.setToAddresses(new String[] {email});
                mail.setSubject('CD Batch Errors');
                String textBody ='<br/> Total number of records processed: ' + totalSizeOfRecords;
                textBody += '<br/> Number of errors: ' + idToErrorMap.size();
                textBody += '<br/>';
                textBody += 'Failed records:';
                textBody += '<br/>';
                
                textBody += '<table>';
                textBody += '<tr>';
                textBody += '<th>Id</th>';
                textBody += '<th>Errors</th>';
                textBody += '</tr>';
                
                for (Id fxAccId : idToErrorMap.keySet()) {
                    textBody += '<tr>';
                    textBody += '<td>' + fxAccId + '</td>';
                    
                    textBody += '<td>';
                    textBody += idToErrorMap.get(fxAccId);
                    textBody += '</td>';
                    textBody += '</tr>';
                }
                
                textBody += '</table>';
        
                mail.setHtmlBody(textBody);

                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            }
    }

    private void updateTradingAccountInfo() {
		Http http = new Http();
        HttpRequest httpRequest = getUpdateTradingAccountInfo();
        HttpResponse response = http.send(httpRequest);
        
        if(response.getStatusCode() != 200)
        {
            http = new Http();
            response = http.send(httpRequest);
        }
        
        if(response.getStatusCode() != 200)
        {
            addError(CALLOUT_ERROR_TYPE, httpRequest.getBody(), response.getBody());
        }
	}

    private HttpRequest getUpdateTradingAccountInfo() {

		HttpRequest httpRequest;
		String clientId = String.valueOf(Integer.valueOf(currentAccount.CD_Client_ID__c));
		String requestBody = getUpdateTradingAccountInfoRequestBody();

        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/updatetradingaccountinfo'  + '?accountId=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setHeader('id', clientId);
		httpRequest.setTimeout(120000);

		httpRequest.setBody(requestBody);

        return httpRequest;
	}

    private string getUpdateTradingAccountInfoRequestBody() {

        Current_Desk_Parameter__mdt zipCodeParameter = Current_Desk_Parameter__mdt.getInstance('Zip_Code');

		return JSON.serializePretty(new Map<String, Object> {
			'zip' => (String) zipCodeParameter?.Value__c
		});
	}

    private void addError(String errorType, String requestBody, String responseBody) {

        String error;

        if (errorType == CALLOUT_ERROR_TYPE) {
            error = String.format(
                'CurrentDesk API request failed for {0}\n' +
                '----------------------------------\n' +
                'Response: \n{1}\n' +
                'Request Details: \n{2}',
                new List<String> {
                        currentAccount.Id,
                        responseBody,
                        requestBody
                    });
        } else if (errorType == MISSING_CLIENT_ID_ERROR_TYPE) {
            error = MISSING_CLIENT_ID_ERROR_TYPE;
        }

        Logger.error(
            'CurrentDesk Error',
            Constants.LOGGER_CURRENT_DESK_CATEGORY,
            error);

		idToErrorMap.put(currentAccount.Id, error);
	}
}