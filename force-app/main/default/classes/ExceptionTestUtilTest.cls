/**
 * @File Name          : ExceptionTestUtilTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/19/2024, 12:48:34 AM
**/
@IsTest
private without sharing class ExceptionTestUtilTest {

	@IsTest
	static void execute1() {
		Boolean error = false;
		Test.startTest();
		try {
			ExceptionTestUtil.prepareSpException();
			ExceptionTestUtil.execute();
			//...
		} catch (SPException e) {
			error = true;
		} 
		Test.stopTest();
		System.assertEquals(true, error, 'Invalid result');
	}

	@IsTest
	static void execute2() {
		Boolean error = false;
		Test.startTest();
		try {
			ExceptionTestUtil.prepareLogException();
			ExceptionTestUtil.execute();
			//...
		} catch (LogException e) {
			error = true;
		} 
		Test.stopTest();
		System.assertEquals(true, error, 'Invalid result');
	}

	@IsTest
	static void execute3() {
		Boolean error = false;
		Test.startTest();
		try {
			ExceptionTestUtil.prepareDummyException();
			ExceptionTestUtil.execute();
			//...
		} catch (ExceptionTestUtil.DummyTestException e) {
			error = true;
		} 
		Test.stopTest();
		System.assertEquals(true, error, 'Invalid result');
	}
	
}