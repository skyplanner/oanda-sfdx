/**
 * @File Name          : AIUpdateRecordEventHandler.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/29/2022, 4:40:13 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    9/29/2021, 5:57:21 PM   acantero     Initial Version
**/
public without sharing class AIUpdateRecordEventHandler {

    //********************************************************

    public static final String OBJ_API_NAME = 'AIUpdateRecordEvent';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //********************************************************

    final List<AIUpdateRecordEvent> newList;

    public AIUpdateRecordEventHandler(
        List<AIUpdateRecordEvent> newList
    ) {
        this.newList = newList;
    }

    //********************************************************

    public void handleAfterInsert() {
        Set<ID> caseIdSet = new Set<ID>();
        for(AIUpdateRecordEvent event : newList) {
            caseIdSet.add(event.RecordId);
        }
        CaseRoutingManager.getInstance().updateLanguageAndRoute(
            caseIdSet
        );
    }
    
}