@isTest
private class fxAccountViewConTest {

    static testMethod void accountFxAccountViewTest() {
    	loadBIWebServiceSettings();
    	
    	Account theAcct = new Account(LastName='test name', PersonEmail='test@oanda.com', RecordTypeId=RecordTypeUtil.getPersonAccountRecordTypeId());
    	insert theAcct;
    	
    	fxAccount__c liveAccount = new fxAccount__c();
    	liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    	liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
    	liveAccount.fxTrade_User_Id__c = 513619;
    	liveAccount.Account__c = theAcct.Id;
    	insert liveAccount;
    	
    	fxAccount__c demoAccount = new fxAccount__c();
    	demoAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    	demoAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
    	demoAccount.fxTrade_User_Id__c = 1879979;
    	demoAccount.Account__c = theAcct.Id;
    	insert demoAccount;
    	
    	Test.StartTest();
    	setupCalloutToLiveSubAccount();
    	
        PageReference pageRef = Page.fxAccountsOnAccount;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(theAcct);
        
        fxAccountViewCon fxAccViewCon = new fxAccountViewCon(stdAccount);
        SubAccountViewCon subAcctViewCon = new SubAccountViewCon();
        
  
        List<fxAccount__c> fxAccountRecords = fxAccViewCon.records;
        System.assertEquals(2, fxAccountRecords.size());
        
        subAcctViewCon.selectedFxAccountId = liveAccount.Id;
        subAcctViewCon.showSubAccount();
        
        //test sort fxAccounts
        subAcctViewCon.fxAccounts = fxAccountRecords;
        subAcctViewCon.sortFxAccounts();
        System.assertNotEquals( SubAccountViewCon.SORT_ORDER_INIT, subAcctViewCon.sortOrder);
        
        subAcctViewCon.sortFieldName = 'Funnel_Stage__c';
        subAcctViewCon.sortFxAccounts();
        
        
        subAcctViewCon.getSubAccountBlockTitle();
        List<BIWebService.SubAccount> subAccountRecords = subAcctViewCon.subAccounts;
        
        System.assertEquals(true, subAcctViewCon.getShouldShowSubAccounts());
        System.assertEquals(2, subAccountRecords.size());
        Test.StopTest();
        
    }
    
    static testMethod void leadFxAccountViewTest() {
    	loadBIWebServiceSettings();
    	
    	Lead lead = new Lead(LastName='Test convert', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED, Email='joe@example.org');
    	insert lead;
    	
    	
    	fxAccount__c demoAccount = new fxAccount__c();
    	demoAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    	demoAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
    	demoAccount.fxTrade_User_Id__c = 1879979;
    	demoAccount.Lead__c = lead.Id;
    	insert demoAccount;
    	
    	Test.StartTest();
    	
    	setupCalloutToDemoSubAccount();
    	
        PageReference pageRef = Page.fxAccountsOnAccount;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(lead);
        
        fxAccountViewCon fxAccViewCon = new fxAccountViewCon(stdAccount);
        SubAccountViewCon subAcctViewCon = new SubAccountViewCon();
        
  
        List<fxAccount__c> fxAccountRecords = fxAccViewCon.records;
        System.assertEquals(1, fxAccountRecords.size());
        
        subAcctViewCon.selectedFxAccountId = demoAccount.Id;
        subAcctViewCon.showSubAccount();
        
        //test sort fxAccounts
        subAcctViewCon.fxAccounts = fxAccountRecords;
        subAcctViewCon.sortFxAccounts();
        System.assertNotEquals( SubAccountViewCon.SORT_ORDER_INIT, subAcctViewCon.sortOrder);
        
        subAcctViewCon.sortFieldName = 'Funnel_Stage__c';
        subAcctViewCon.sortFxAccounts();
        
        subAcctViewCon.getSubAccountBlockTitle();
        List<BIWebService.SubAccount> subAccountRecords = subAcctViewCon.subAccounts;
        
        System.assertEquals(true, subAcctViewCon.getShouldShowSubAccounts());
        System.assertEquals(1, subAccountRecords.size());
        
        Test.stopTest();
        
    }
    
    static void loadBIWebServiceSettings(){
    	BIWebServiceSetting__c setting = new BIWebServiceSetting__c(name = 'BI Web Services');
    	setting.User_Name__c = 'app-salesforce';
    	setting.Password__c = 'abc';
    	setting.Live_Account_End_Point__c = 'https://biapi.oanda.com:3443/biapi-salesforce/get-game-subaccount-data-by-fxtrade-user-id';
    	setting.Demo_Account_End_Point__c = 'https://biapi.oanda.com:3443/biapi-salesforce/get-live-subaccount-data-by-fxtrade-user-id';
    	
    	insert setting;
    }
    
    static void setupCalloutToLiveSubAccount(){
    	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();

        mock.setStaticResource('BIWebServiceLiveSubAccounts');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
    }
    
     static void setupCalloutToDemoSubAccount(){
     	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();

         mock.setStaticResource('BIWebServiceDemoSubAccounts');
         mock.setStatusCode(200);
         mock.setHeader('Content-Type', 'application/json');
        
         // Set the mock callout mode
         Test.setMock(HttpCalloutMock.class, mock);
     }
}