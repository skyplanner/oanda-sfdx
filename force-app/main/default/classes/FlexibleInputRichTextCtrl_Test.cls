/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 07-06-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   07-06-2020   dmorales   Initial Version
**/
@isTest
private class FlexibleInputRichTextCtrl_Test {
    @isTest
    static void testConstructor() {
        FlexibleInputRichTextCtrl ctrl = new FlexibleInputRichTextCtrl();
        List<String>  linkList = FlexibleInputRichTextCtrl.loadStaticLinks();
        System.assert( linkList != null);
    }
}