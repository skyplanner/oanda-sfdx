/**
 * @File Name          : OmniNonHvcCaseCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/2/2024, 2:38:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2021, 6:57:41 PM   acantero     Initial Version
**/
@IsTest
private without sharing class OmniNonHvcCaseCtrl_Test {

    @isTest
    static void onNewNonHvcCase() {
        NonHvcCaseAttemptInfo result = null;
        Non_HVC_Cases_Status_Pair__mdt statusPair = [
            SELECT 
                Status_allowing__c, 
                Status_not_allow__c,
                Max_Number_Cases__c
            FROM Non_HVC_Cases_Status_Pair__mdt
            WHERE Max_Number_Cases__c <> null
            LIMIT 1
        ];
        String newStatusId = 
            ServicePresenceStatusHelper.getByDevName(
                statusPair.Status_not_allow__c,
                true
            ).Id;
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0006');
        System.runAs(testUser) {
            String userId = UserInfo.getUserId();
            System.debug('userId: ' + userId);
            //...
            insert new Agent_Extra_Info__c(
                Agent_ID__c = userId,
                Non_HVC_Cases__c = statusPair.Max_Number_Cases__c
            );
            //...
            result = OmniExtraCtrl.onNewNonHvcCase(
                statusPair.Status_allowing__c
            );
        }
        Test.stopTest();
        System.assertEquals(false, result.canBeAssigned);
        System.assertEquals(newStatusId, result.newStatusId);
    }

    @isTest
    static void onNonHvcCaseReleased() {
        String result = null;
        Non_HVC_Cases_Status_Pair__mdt statusPair = [
            SELECT 
                Status_allowing__c, 
                Status_not_allow__c 
            FROM Non_HVC_Cases_Status_Pair__mdt
            WHERE Bidirectional__c = true
            AND Max_Number_Cases__c <> null
            LIMIT 1
        ];
        String newStatusId = 
            ServicePresenceStatusHelper.getByDevName(
                statusPair.Status_allowing__c,
                true
            ).Id;
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0006');
        System.runAs(testUser) {
            String userId = UserInfo.getUserId();
            System.debug('userId: ' + userId);
            //...
            OmnichanelSettings settings = 
                OmnichanelSettings.getRequiredDefault();
            insert new Agent_Extra_Info__c(
                Agent_ID__c = userId,
                Non_HVC_Cases__c = 1 
            );
            //...
            result = OmniExtraCtrl.onNonHvcCaseReleased(
                statusPair.Status_not_allow__c
            );
        }
        Test.stopTest();
        System.assertEquals(newStatusId, result);
    }

    @IsTest
    static void declineNonHvcCases() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createTestLeads();
        SPCaseTriggerHandler.enabled = false; //i do not want routing logic here
        OmnichanelRoutingTestDataFactory.createTestCasesForLeads();
        //...
        ID caseId = [select Id from Case limit 1].Id;
        Non_HVC_Case__c oldNonHvcCase = new Non_HVC_Case__c(
            Case__c = caseId,
            Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED //trick
        );
        insert oldNonHvcCase;
        List<String> nonHvcCaseIdList = new List<String>{oldNonHvcCase.Id};
        Test.startTest();
        OmniExtraCtrl.declineNonHvcCases(nonHvcCaseIdList);
        Test.stopTest();
        List<Non_HVC_Case__c> oldNonHvCaseList = [
            select Id 
            from Non_HVC_Case__c 
            where Id = :oldNonHvcCase.Id
            limit 1
        ];
        System.assert(oldNonHvCaseList.isEmpty());
        //...
        List<Non_HVC_Case__c> newNonHvCaseList = [
            select Id 
            from Non_HVC_Case__c 
            where Case__c = :caseId
            limit 1
        ];
        System.assertEquals(false, newNonHvCaseList.isEmpty());
    }
    
    @IsTest
    static void onNewNonHvcCaseEx() {
        
        Boolean error = false;
        Test.startTest();
        try {
            ExceptionTestUtil.prepareDummyException();
            OmniExtraCtrl.onNewNonHvcCase(
                'Status_allowing__c'
            );
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error,'Exception was expected');
    }

    @IsTest
    static void onNonHvcCaseReleasedEx() {
        
        Boolean error = false;
        Test.startTest();
        try {
            ExceptionTestUtil.prepareDummyException();
            OmniExtraCtrl.onNonHvcCaseReleased(
                'Status_allowing__c'
            );
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error,'Exception was expected');
    }
    @IsTest
    static void declineNonHvcCasesEx() {
        
        Boolean error = false;
        Test.startTest();
        try {
            ExceptionTestUtil.prepareDummyException();
            OmniExtraCtrl.declineNonHvcCases(
               new List<String>{'Id'}
            );
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error,'Exception was expected');
    }
}