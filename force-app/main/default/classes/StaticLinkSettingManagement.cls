/**
 * @File Name          : StaticLinkSettingManagement.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/24/2020, 3:50:46 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/24/2020   dmorales     Initial Version
 **/
global without sharing class StaticLinkSettingManagement {

global static Map<String, List<Chat_Offline_Links_Setting__mdt>> getStaticLinkSettings(Boolean clearKey) {
    
    List<Chat_Offline_Links_Setting__mdt> linkList =[ SELECT Id, DeveloperName, Link_Label__c, 
                                                             Division_Acronyms__c,Is_Active__c, URL_Name__c 
	                                                  FROM Chat_Offline_Links_Setting__mdt
                                                      WHERE Is_Active__c = TRUE ];
                                                      
	Map<String, List<Chat_Offline_Links_Setting__mdt> > mapSetting = new Map<String, List<Chat_Offline_Links_Setting__mdt> >();
	for(Chat_Offline_Links_Setting__mdt chatOffSett : linkList) {
        String key = clearKey ? clearString(true,true,chatOffSett.Link_Label__c)
                              : chatOffSett.Link_Label__c;
		if(mapSetting.containsKey(key)) {
			List<Chat_Offline_Links_Setting__mdt> listSett =  mapSetting.get(key);
			listSett.add(chatOffSett);
			mapSetting.put(key, listSett);
		}
		else
			mapSetting.put(key, new List<Chat_Offline_Links_Setting__mdt> {chatOffSett});
	}

	return mapSetting;
}

    global static String clearString(Boolean toLower, Boolean noSpecialCharacters, String toChange){
           if(toLower)
              toChange = toChange.toLowercase();
           if(noSpecialCharacters)
              toChange = toChange.replaceAll('[^\\w]', ''); 
           return toChange;   
    }

}