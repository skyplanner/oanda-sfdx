/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-16-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CASearchDetailsManager {
    Map<String, Comply_Advantage_Search__c> searchesMap;
    List<CASearchDetails> searches;
    public List<Comply_Advantage_Search_Update__mdt> fieldsInfo;
    Map<String, Schema.SObjectField> fieldTypeMap;

    public CASearchDetailsManager(List<Comply_Advantage_Search__c> searchesToUpdate) {
        searchesMap = new Map<String, Comply_Advantage_Search__c>();
        searches = new List<CASearchDetails>();

        for (Comply_Advantage_Search__c s : searchesToUpdate) {
            CASearchDetails d = getSearchDetails(s);
            if (d != null) {
                searchesMap.put(s.Search_Id__c, s);
                searches.add(d);
            }
        }

        fieldsInfo = [
            SELECT 
                Id,
                Active__c,
                Field_Api_Name__c,
                Force_Update__c,
                Method_Name__c
            FROM Comply_Advantage_Search_Update__mdt
            WHERE Active__c = TRUE
        ];

        Boolean isRunningTest = Test.isRunningTest();

        if (isRunningTest) {
            fieldsInfo.addAll([
                SELECT 
                    Id,
                    Active__c,
                    Field_Api_Name__c,
                    Force_Update__c,
                    Method_Name__c
                FROM Comply_Advantage_Search_Update__mdt
                WHERE Active__c = FALSE
            ]);
        }

        fieldTypeMap = Schema.getGlobalDescribe()
            .get('Comply_Advantage_Search__c')
            .getDescribe()
            .Fields
            .getMap();
    }

    public void startUpdating() {
        Callable extension = (Callable) Type.forName('ClassExtension')
            .newInstance();

        Map<Id, Comply_Advantage_Search__c> toUpdate = new Map<Id, Comply_Advantage_Search__c>();

        for (CASearchDetails d : searches) {
            for (Comply_Advantage_Search_Update__mdt fi : fieldsInfo) {
                if (fi.Active__c) {
                    Comply_Advantage_Search__c search = searchesMap.get(d.id);
                    if (String.isNotBlank(d.errorMessage)) {
                        search.CA_not_updated__c = d.errorMessage;
                        toUpdate.put(search.Id, search);
                    } else {
                        Object result = extension.call(
                            fi.Method_Name__c,
                            new Map<String, Object>{
                                'details' => d,
                                'search' => search
                            }
                        );
                        System.debug('detail id: ' + d.searcher_id);
                        if (updateSearch(search, fi)) {
                            search.put(fi.Field_Api_Name__c, result);
                            searchesMap.put(search.Search_Id__c, search);
                            toUpdate.put(search.Id, search);
                        }
                    }
                }
            }
        }

        if (!toUpdate.isEmpty()) {
            update toUpdate.values();
        }
    }

    public CASearchDetails getSearchDetails(Comply_Advantage_Search__c search) {
        CASearchDetails details;

        if (String.isBlank(search.Custom_Division_Name__c) &&
            String.isBlank(search.Division_Name__c)) {
            details = new CASearchDetails();
            details.id = search.Search_Id__c;
            details.errorMessage = 'no division name';

            return details;
        }

        String apiKey = ComplyAdvantageUtil.getApiKey(search);

        if (String.isNotBlank(apiKey)) {
            HttpResponse response = ComplyAdvantageUtil.getSearchDetailsCallout(
                search.Search_Id__c,
                apiKey
            );
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            if (response.getStatusCode() == 200) {
                Map<String, Object> resWrapper = (Map<String, Object>) results.get('content');
                Map<String, Object> responseData = (Map<String, Object>) resWrapper.get('data');

                responseData.remove('tags'); // removed by type inconsistency: (empty: [], fill: {})
                responseData.remove('labels'); // removed by type inconsistency: (empty: [], fill: {})

                details = (CASearchDetails) JSON.deserializeStrict(
                    JSON.serialize(responseData)
                        .replaceAll('"limit"', '"limitt"')
                        .replaceAll('"date"', '"datee"'),
                    CASearchDetails.class
                );
            } else {
                details = new CASearchDetails();
                details.id = search.Search_Id__c;
                details.errorCode = (Integer) results.get('code');
                details.errorMessage = (String) results.get('message');
            }
        }

        return details;
    }

    public Boolean updateSearch(Comply_Advantage_Search__c search, Comply_Advantage_Search_Update__mdt fi) {
        if (search == null) {
            return false;
        }
        if (fi.Force_Update__c) {
            return true;
        }

        Schema.DisplayType fieldType = fieldTypeMap.get(fi.Field_Api_Name__c).getDescribe().getType();

        if (fieldType == Schema.DisplayType.STRING) {
            String value = (String) search.get(fi.Field_Api_Name__c);
            System.debug(fi.Field_Api_Name__c);
            System.debug(value);
            return String.isBlank(value);
        }
        if (fieldType == Schema.DisplayType.BOOLEAN) {
            Boolean value = (Boolean) search.get(fi.Field_Api_Name__c);
            System.debug(fi.Field_Api_Name__c);
            System.debug(value);
            return !value;
        }
        if (fieldType == Schema.DisplayType.INTEGER) {
            Integer value = (Integer) search.get(fi.Field_Api_Name__c);
            System.debug(fi.Field_Api_Name__c);
            System.debug(value);
            return value == null || value == 0;
        }
        if (fieldType == Schema.DisplayType.DOUBLE) {
            Double value = (Double) search.get(fi.Field_Api_Name__c);
            System.debug(fi.Field_Api_Name__c);
            System.debug(value);
            return value == null || value == 0.0;
        }
        if (fieldType == Schema.DisplayType.PERCENT) {
            Double value = (Double) search.get(fi.Field_Api_Name__c);
            System.debug(fi.Field_Api_Name__c);
            System.debug(value);
            return value == null || value == 0.0;
        }

        return false;
    }
}