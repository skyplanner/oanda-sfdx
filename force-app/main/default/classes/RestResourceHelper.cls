/* Name: RestResourceHelper
 * Description : RestResource helper, to handle reading and manipulating Rest Requests
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 20
 */

public with sharing class RestResourceHelper {
    
    private RestRequest request;
    private RestResponse response;
    private List<String> definedParams;

    public RestResourceHelper(RestRequest req, RestResponse res) {
        this(req, res, new List<String>());
    }

    public RestResourceHelper(RestRequest req, RestResponse res, List<String> params) {
        this.request = req;
        this.response = res;
        this.definedParams = params ?? new List<String>();
        this.logRequest(Log.Type.INFO, 'Incoming request: ' + this.request.httpMethod + ' ' + this.request.requestURI); 
    }

    public Map<String,String> pathParams {
        get{
            if(pathParams == null && !definedParams.isEmpty()) {
                pathParams = parsePathParams(this.request.requestURI);
            }
            return pathParams ?? new Map<String,String>();
        }
        private set;
    }

    public String getBodyAsString() {
        return this.request.requestBody != null ? this.request.requestBody.toString().trim() : '';
    }

    public Map<String, Object> getBodyAsMap() {
        return (Map<String,Object>) JSON.deserializeUntyped(this.request.requestBody.toString());
    }

    public String getPathParam(String name) {
        return this.pathParams.containsKey(name) ? this.pathParams.get(name) : null;
    }
    
    public Map<String, String> parsePathParams (String url) {
        List<String> tokens = url.split('/');
        List<String> resourcePathTokens = this.request.resourcePath.removeStart('/services/apexrest').split('/');
        
        Map<String, String> paramMap = new Map<String, String> ();

        for( Integer i = 0 ; i < tokens.size(); i++ ) {

            Integer safeNavigationIndex = i > resourcePathTokens.size() - 1 ? resourcePathTokens.size() - 1 : i;

            String resourcePathCurrentToken = resourcePathTokens.get(safeNavigationIndex);
            String currentToken = tokens.get(i);
            Integer paramIndexToGet = paramMap.size();

            if(resourcePathCurrentToken == '*' && this.definedParams.size() >= paramIndexToGet) {
                paramMap.put(this.definedParams.get(paramMap.size()), currentToken);
            }
        }

        return paramMap;
    }
    
    public void setResponse(Integer code, Object payload){
        this.response.statusCode = code;
        this.response.responseBody = Blob.valueOf(JSON.serialize(payload));
        this.response.addHeader('Content-Type', 'application/json');
    }
    public void setResponse(Integer code){
        this.setResponse(code, null);
    }
    
    private void logRequest(Log.Type type, String message){
        LogEvt__e log = new LogEvt__e(
            Type__c = type.name(),
            Category__c = this.request.resourcePath,
            Message__c = message,
            Details__c = this.request.requestBody?.toString() ?? '<empty body>',
            Id__c = (String)UserInfo.getUserId());
        EventBus.publish(log);
    }

}