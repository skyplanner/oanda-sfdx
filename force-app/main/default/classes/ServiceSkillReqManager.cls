/**
 * @File Name          : ServiceSkillReqManager.cls
 * @Description        : 
 * Returns the list of SkillRequirements corresponding to 
 * ServiceRoutingInfo records
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 4:38:30 PM
**/
public inherited sharing class ServiceSkillReqManager 
	extends BaseServiceSkillReqManager {

	public Boolean getNonLanguageSkillsAreAditional(
		Boolean dropAdditionalSkills
	) {
		OmnichanelSettings settings = OmnichanelSettings.getRequiredDefault();
		Boolean result = (
			(dropAdditionalSkills == true) &&
			(settings.dropAdditionalSkillsAreEnabled == true) &&
			(
				settings.primarySkillsModel == 
				OmnichanelSettings.LANGUAGE_SKILLS_MODEL
			)
		);
		return result;
	}

	@TestVisible
	protected override List<SSkillRequirementInfo> getSkillsInfo(
		BaseServiceRoutingInfo routingInfo
	) {
		ServiceRoutingInfo sRoutingInfo = (ServiceRoutingInfo) routingInfo;
		Boolean nonLanguageSkillsAreAditional = 
			getNonLanguageSkillsAreAditional(
				sRoutingInfo.dropAdditionalSkills
			);
		RegisterRoutingSkillCmd registerSkillCmd = 
			new RegisterRoutingSkillCmd(sRoutingInfo);
		registerSkillCmd.globalSkillNameSet = globalSkillNameSet;

		//...Account Type
		registerSkillCmd.registerSkill(
			SkillsHelper.getAccountTypeSkill(sRoutingInfo), // skillName
			nonLanguageSkillsAreAditional // isAdditionalSkill
		);
		//...Language
		registerSkillCmd.registerSkill(
			SkillsHelper.getLanguageSkill(sRoutingInfo), // skillName
			false // isAdditionalSkill
		);
		//...Region
		registerSkillCmd.registerSkill(
			SkillsHelper.getRegionSkill(sRoutingInfo), // skillName
			nonLanguageSkillsAreAditional // isAdditionalSkill
		);
		//...Customer Type
		registerSkillCmd.registerSkill(
			SkillsHelper.getCustomerTypeSkill(sRoutingInfo), // skillName
			nonLanguageSkillsAreAditional // isAdditionalSkill
		);
		//...Inquiry Nature
		registerSkillCmd.registerSkill(
			SkillsHelper.getInquiryNatureSkill(sRoutingInfo), // skillName
			nonLanguageSkillsAreAditional // isAdditionalSkill
		);
		//...Crypto
		registerSkillCmd.registerSkill(
			SkillsHelper.getCryptoSkill(sRoutingInfo), // skillName
			nonLanguageSkillsAreAditional // isAdditionalSkill
		);
		
		return registerSkillCmd.skillLInfoList;
	}

	@TestVisible
	protected override void setSkillsPriorities(
		List<SSkillRequirementInfo> skillsInfo
	) {
		Map<String, Decimal> skillPriorityByDevNames = 
			ServiceSkillManager.getSkillPriorityByDevNames(globalSkillNameSet);

		for (SSkillRequirementInfo skillRequirementInfo : skillsInfo) {

			if (
				skillPriorityByDevNames.containsKey(skillRequirementInfo.skillName) &&
				(skillRequirementInfo.skillReq.IsAdditionalSkill == true)
			) {
				Integer priority = skillPriorityByDevNames.get(
					skillRequirementInfo.skillName
				).intValue();
				skillRequirementInfo.skillReq.SkillPriority = priority;
			}
		}
	}
	
}