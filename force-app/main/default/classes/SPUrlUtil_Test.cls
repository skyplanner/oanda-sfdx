/**
 * @File Name          : SPUrlUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/25/2021, 2:00:18 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/25/2021, 1:54:58 AM   acantero     Initial Version
**/
@isTest
private without sharing class SPUrlUtil_Test {

    @isTest
    static void test() {
        Map<String,String> currentPageParams = new Map<String,String>
        {
            'key1' => 'value1',
            'key2' => 'value2'
        };
        Map<String,String> nextPageParams = new Map<String,String>();
        Test.startTest();
        SPUrlUtil.addCurrentParam(
            currentPageParams,
            'key1',
            nextPageParams
        );
        SPUrlUtil.addCurrentParam(
            currentPageParams,
            'key2',
            nextPageParams,
            'super key2'
        );
        Test.stopTest();
        System.assertEquals('value1', nextPageParams.get('key1'));
        System.assertEquals(null, nextPageParams.get('key2'));
        System.assertEquals('value2', nextPageParams.get('super key2'));
    }
    
}