/**
 * @Description  : EmailMessageTrigger service class to manage Cases.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-31
**/
public with sharing class EmailCaseService {
    private static final List<String> STATUS_TO_REOPEN = new List<String>{
        Constants.CASE_STATUS_CLOSED,
        Constants.CASE_STATUS_WAITING_ON_INTERNAL,
        Constants.CASE_STATUS_WAITING_ON_DATE
    };

    private static final List<String> CASE_OTMS_RECORD_TYPES = new List<String> {
        RecordTypeUtil.NAME_OTMS_SUPPORT_CASE,
        RecordTypeUtil.NAME_OTMS_ONBOARDING_CASE
    };

    private static final String NEW_DOCUMENT_UPLOADED = Constants.NEW_DOCUMENT_UPLOADED + '%';

    /**
     * @description Method changes the status of a case to "Re-opened" if current status is either "Waiting" or "Closed".
     * @author Jakub Fik | 2024-05-29
     * @param parentCaseIds
     **/
    public void reopenCases(Set<Id> parentCaseIds) {
        if (parentCaseIds == null || parentCaseIds.isEmpty()) {
            return;
        }

        List<Case> parentCases = [
            SELECT Id
            FROM Case
            WHERE
                Id IN :parentCaseIds
                    AND Status IN :STATUS_TO_REOPEN
                    AND Case_Owner_Name__c != :Constants.QUEUE_NAME_SPAM
                    AND Auto_Closed__c = false
        ];

        if (parentCases.isEmpty()) {
            return;
        }

        for (Case loopCase : parentCases) {
            loopCase.Status = Constants.CASE_STATUS_RE_OPENED;
            loopCase.Waiting_for_Date__c = null;
        }

        update parentCases;
    }

    /**
    * @description Method menages creation of new Cases from new document notification.
    * @author Jakub Fik | 2024-07-18 
    * @param addressMessageMap 
    **/
    public void manageOtmsNotificationCases(Map<String,List<EmailMessage>> addressMessageMap) {
        if(addressMessageMap == null || addressMessageMap.isEmpty()) {
            return;
        }
        List<Lead> leadList = [
            SELECT Id, Email, ( 
                SELECT Id
                FROM Cases__r 
                WHERE RecordType.Name IN :CASE_OTMS_RECORD_TYPES
                    AND Subject LIKE :NEW_DOCUMENT_UPLOADED  
                ORDER BY CreatedDate) 
            FROM Lead 
            WHERE RecordType.Name = :RecordTypeUtil.NAME_LEAD_RECORDTYPE_OTMS
                AND Email in :addressMessageMap.keySet()];
        if(leadList.isEmpty()) {
            return;
        }
        
        Map<String,Id> emailToReplace = new Map<String,Id>();
        for (Lead loopLead : leadList) {
            if(loopLead.Cases__r.size() > 1) {
                emailToReplace.put(loopLead.Email, loopLead.Cases__r[0].Id);
            }
        }
        if(emailToReplace.isEmpty()) {
            return;
        }
        List<Case> casesToDelete = new List<Case>();
        for(String emailAddress : addressMessageMap.keySet()) {
            if(emailToReplace.containsKey(emailAddress)) {
                Id newParentId = emailToReplace.get(emailAddress);
                for(EmailMessage loopMessage : addressMessageMap.get(emailAddress)) {
                    if(loopMessage.ParentId != newParentId) {
                        casesToDelete.add(new Case(Id = loopMessage.ParentId));
                        loopMessage.ParentId = newParentId;
                    }
                }
            }
        }
        if( !casesToDelete.isEmpty()) {
            delete casesToDelete;
        }
    }
}