/**
 * @File Name          : ServiceResourceSkillTriggerUtility_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:28:09 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 4:53:46 PM   acantero     Initial Version
**/
@isTest(isParallel = false)
private without sharing class ServiceResourceSkillTriggerUtility_Test {

    @isTest
    static void test() {
        /* String skillId = ServiceResourceTestDataFactory.getValidSkillId();
        Date yesterday = System.today().addDays(-1);
        Date tomorrow = System.today().addDays(1);
        Boolean error = false;
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0002');
        System.runAs(testUser) {
            String userId = UserInfo.getUserId();
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(userId);
            ServiceResourceSkill serviceResourceSkillObj = 
                new ServiceResourceSkill(
                    ServiceResourceId = serviceResourceId,
                    SkillId = skillId,
                    EffectiveStartDate = yesterday
                );
            try {
                insert serviceResourceSkillObj;
                serviceResourceSkillObj = [
                    select Id 
                    from ServiceResourceSkill 
                    where Id = :serviceResourceSkillObj.Id
                ];
                serviceResourceSkillObj.EffectiveEndDate = tomorrow;
                update serviceResourceSkillObj;
                //...
            } catch (Exception ex) {
                error = true;
            }
        }
        Test.stopTest();
        System.assertEquals(false, error); */
    }
    
}