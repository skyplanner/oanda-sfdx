/**
 * @File Name          : NotifyASAViolationAction.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/15/2024, 12:22:17 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 9:38:16 AM   aniubo     Initial Version
 **/
public without sharing class NotifyASAViolationAction {
	@InvocableMethod(
		label='Notify ASA Violation'
		description='Notify ASA Violation for the given record Ids.'
	)
	public static void notifyASAViolation(
		List<NotifyASAViolationData> violationsData
	) {
		ServiceLogManager Logger = ServiceLogManager.getInstance();

		List<Id> recordIds = new List<Id>();
		Logger.log('Notify ASA Violation Action ', 'SLA');
		if (violationsData != null && !violationsData.isEmpty()) {
			for (NotifyASAViolationData violationData : violationsData) {
				recordIds.add(violationData.recordId);
			}
			Logger.log(
				'Notify ASA Violation Action: Records Ids '+ String.join(recordIds, ','),
				 'SLA'
			 );
			 NotifyASAViolationData violationData = violationsData[0];
			SLAConst.SLANotificationObjectType notificationObjectType = violationData
					.isLiveChat
				? SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
				: SLAConst.SLANotificationObjectType.MESSAGING_NOT;

			Logger.log(
				'Notify ASA Violation Action: SLANotificationObjectType '+ notificationObjectType.name(),
				'SLA'
			);
			SLAViolationDataReader dataReader = violationData.isLiveChat
				? (SLAViolationDataReader) new ChatSLAViolationNotifierDataReader()
				: (SLAViolationDataReader) new MessagingSLAViolationNotifierDataReader();

			SLAConst.SLAViolationType violationType = String.isNotBlank(violationData.violationType)
				? SLAConst.SLAViolationType.valueOf(violationData.violationType)
				: SLAConst.SLAViolationType.ASA;

			Logger.log(
				'Notify ASA Violation Action: SLAViolationType '+ violationType.name(),
				'SLA'
			);
			SLAViolationNotifiableFactory.SLAViolationNotifiableFactory(
					notificationObjectType,
					violationType,
					dataReader,
					null
				)
				?.notifyViolation(
					new ChatSLAViolationNotificationData(recordIds)
				);
		}
	}
	public class NotifyASAViolationData {
		@InvocableVariable
		public Id recordId;
		@InvocableVariable
		public Boolean isLiveChat;
		@InvocableVariable
		public String violationType;
	}
}