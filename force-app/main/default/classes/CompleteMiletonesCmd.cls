/**
 * @File Name          : CompleteMiletonesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/1/2021, 11:41:08 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/19/2021, 11:33:06 AM   acantero     Initial Version
**/
public inherited sharing class CompleteMiletonesCmd {

    final List<Case> caseList = new List<Case>();

    public void checkRecord(
        Case rec, 
        Case oldRec
    ) {
        if (
            (rec.SlaStartDate != null) &&
            (rec.isClosed == true) &&
            (oldRec.isClosed != true)
        ) {
            caseList.add(rec);
        }
    }

    public void execute() {
        if (caseList.isEmpty()) {
            return;
        }
        //else...
        MilestoneUtils.completeMiletones(caseList);
    }

}