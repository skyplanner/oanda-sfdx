/* Name: PiuCkaUpdateRestResource
 * Description : Apex API class to handle Knowledge Assesment update
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 31
 */

@RestResource(urlMapping='/api/v1/user/*/assessment')
global class KnowledgeAssessmentRestResource {

    public static final List<String> POST_PARAMS = new List<String>{'user_id'};

    @HttpPost
    global static void doPost() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, POST_PARAMS);

        try {
            Integer userId = Integer.valueOf(context.getPathParam('user_id'));
            
            FxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

            if(fxa == null) {
                String respBody = 'Could not find user with given Id';
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 404;
                return;
            }

            PlatformEventDispatch.insertGenericAfter('KNOWLEDGE_ASSESSMENT_UPDATE', context.getBodyAsString(), fxa.Id);
                
            res.responseBody = Blob.valueOf('Record will be processed.');
            res.statusCode = 200;
            return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

    
}