/**
 * @File Name          : CaseSRoutingInfoManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/21/2024, 1:35:19 AM
**/
@IsTest
private without sharing class CaseSRoutingInfoManagerTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createLead1(initManager);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createLeadCase1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSegmentation1(initManager);
		initManager.storeData();
	}

	// no PendingServiceRouting registered
	@IsTest
	static void getRoutingInfoList1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
	
		Test.startTest();
		CaseSRoutingInfoManager instance = new CaseSRoutingInfoManager();
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		Assert.isNull(result, 'Invalid result');
	}

	// PendingServiceRouting registered (case linked to an Account)
	@IsTest
	static void getRoutingInfoList2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID account1Id = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1, 
			true
		);
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
	
		Test.startTest();
		CaseSRoutingInfoManager instance = new CaseSRoutingInfoManager();
		// pendingRoutingObj and workItemId are passed as separate parameters
		instance.registerPendingServiceRouting(
			new PendingServiceRouting(), // pendingRoutingObj
			case1Id // workItemId
		);
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) result[0];
		Assert.areEqual(
			case1Id, 
			routingInfo.workItem.Id, 
			'Invalid result'
		);
		Assert.areEqual(
			account1Id, 
			routingInfo.relatedAccount.Id, 
			'Invalid result'
		);
	}

	// PendingServiceRouting registered (case linked to a Lead)
	@IsTest
	static void getRoutingInfoList3() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID lead1Id = initManager.getObjectId(
			ServiceTestDataKeys.LEAD_1, 
			true
		);
		ID leadCase1Id = 
			initManager.getObjectId(ServiceTestDataKeys.LEAD_CASE_1, true);
	
		Test.startTest();
		CaseSRoutingInfoManager instance = new CaseSRoutingInfoManager();
		// The workItemId is obtained directly from the pendingRoutingObj
		instance.registerPendingServiceRouting(
			new PendingServiceRouting(
				WorkItemId = leadCase1Id
			)
		);
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) result[0];
		Assert.areEqual(
			leadCase1Id, 
			routingInfo.workItem.Id, 
			'Invalid result'
		);
		Assert.areEqual(
			lead1Id, 
			routingInfo.relatedLead.Id, 
			'Invalid result'
		);
	}

	// test 1 : caseObj.Origin == CASE_ORIGIN_CHATBOT_EMAIL
	// test 2 : caseObj.Origin == CASE_ORIGIN_EMAIL_FRONTDESK
	@IsTest
	static void setLanguagesFields() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			null, // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			null // workItem
		);
		Case caseObj1 = new Case(
			Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL,
			Language_Corrected__c = OmnichanelConst.ENGLISH_LANG
		);
		Case caseObj2 = new Case(
			Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK,
			Language = OmnichanelConst.SPANISH_LANG_CODE
		);
	
		Test.startTest();
		// test 1
		String result1 = CaseSRoutingInfoHelper.setLanguagesFields(
			routingInfo, // routingInfo
			caseObj1 // caseObj
		);
		// test 2
		String result2 = CaseSRoutingInfoHelper.setLanguagesFields(
			routingInfo, // routingInfo
			caseObj2 // caseObj
		);
		Test.stopTest();
		
		Assert.areEqual(
			OmnichanelConst.ENGLISH_LANG_CODE, 
			result1, 
			'Invalid result'
		);
		Assert.areEqual(
			OmnichanelConst.SPANISH_LANG_CODE, 
			result2, 
			'Invalid result'
		);
	}

	/**
	 * test 1 
	 * Routed__c = true, tierHasChanged = false, Ready_For_Routing__c = true
	 * 
	 * test 2
	 * Routed__c = true, tierHasChanged = true, Ready_For_Routing__c = true
	 * 
	 * test 3
	 * Routed__c = false, tierHasChanged = false, Ready_For_Routing__c = true
	 * 
	 * test 4
	 * Routed__c = true, tierHasChanged = false, Ready_For_Routing__c = false
	 */
	@IsTest
	static void getRecordsToUpdate() {
		CaseSRoutingInfoManager instance = 
			new CaseSRoutingInfoManager();

		Test.startTest();
		// test 1 => result is empty
		ServiceRoutingInfo routingInfo1 = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new Case(
					Routed__c = true,
					Ready_For_Routing__c = true
				) // workItem
			);
		routingInfo1.initTier(OmnichannelCustomerConst.TIER_4);
		List<SObject> result1 = instance.getRecordsToUpdate(routingInfo1);
		// test 2 => result is not empty
		ServiceRoutingInfo routingInfo2 = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new Case(
					Routed__c = true,
					Ready_For_Routing__c = true
				) // workItem
			);
		routingInfo2.initTier(OmnichannelCustomerConst.TIER_4);
		routingInfo2.updateTier(OmnichannelCustomerConst.TIER_3);
		List<SObject> result2 = instance.getRecordsToUpdate(routingInfo2);
		// test 3 => result is not empty
		ServiceRoutingInfo routingInfo3 = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new Case(
					Routed__c = false,
					Ready_For_Routing__c = true
				) // workItem
			);
		routingInfo3.initTier(OmnichannelCustomerConst.TIER_4);
		List<SObject> result3 = instance.getRecordsToUpdate(routingInfo3);
		// test 4 => result is not empty
		ServiceRoutingInfo routingInfo4 = 
		(ServiceRoutingInfo) instance.getRoutingInfo(
			new PendingServiceRouting(), // routingObj
			new Case(
				Routed__c = true,
				Ready_For_Routing__c = false
			) // workItem
		);
		routingInfo4.initTier(OmnichannelCustomerConst.TIER_4);
		List<SObject> result4 = instance.getRecordsToUpdate(routingInfo4);
		Test.stopTest();

		Assert.isTrue(result1.isEmpty(), 'Invalid result');
		Assert.isFalse(result2.isEmpty(), 'Invalid result');
		Assert.isFalse(result3.isEmpty(), 'Invalid result');
		Assert.isFalse(result4.isEmpty(), 'Invalid result');
	}

	@IsTest
	static void getRoutingLog() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		CaseSRoutingInfoManager instance = new CaseSRoutingInfoManager();
		SObject result = instance.getRoutingLog(routingInfo);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	/**
	* Test 1 : updateMsgSession = true
	* Test 2 : updateMsgSession = false
	*/
	@IsTest
	static void checkMsgSessionUpdate() {
		ID msgSessionId = null;
		String newTier = OmnichannelCustomerConst.TIER_4;
		CaseSRoutingInfoManager instance = new CaseSRoutingInfoManager();

		Test.startTest();

		// Test 1 -> return true
		Boolean updateMsgSession1 = true;
		List<SObject> recsToUpdate1 = new List<SObject>();
		Boolean result1 = instance.checkMsgSessionUpdate(
			updateMsgSession1, // updateMsgSession
			recsToUpdate1, // recsToUpdate
			msgSessionId, // msgSessionId
			newTier // newTier
		);

		// Test 2 -> return false
		Boolean updateMsgSession2 = false;
		List<SObject> recsToUpdate2 = new List<SObject>();
		Boolean result2 = instance.checkMsgSessionUpdate(
			updateMsgSession2, // updateMsgSession
			recsToUpdate2, // recsToUpdate
			msgSessionId, // msgSessionId
			newTier // newTier
		);

		Test.stopTest();
		
		// Test 1
		Assert.isTrue(result1, 'Invalid result');
		Assert.areEqual(1, recsToUpdate1.size(), 'Invalid result');
		// Test 2
		Assert.isFalse(result2, 'Invalid result');
		Assert.areEqual(0, recsToUpdate2.size(), 'Invalid result');
	}
	
}