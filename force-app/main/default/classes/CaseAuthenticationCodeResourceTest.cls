/**
 * @author Fernando Gomez
 * @since 5/7/2021
 */
@isTest
private class CaseAuthenticationCodeResourceTest {
	@isTest
	static void sendAuthenticationCodeEmail() {
		TestDataFactory testHandler = new TestDataFactory();
		Account acc;
		fxAccount__c fxa;
		Case sampleCase;
		Contact theConttact;
		
		testHandler = new TestDataFactory();
		acc = testHandler.createTestAccount();
		acc.RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId();
		update acc;

		fxa = testHandler.createFXTradeAccount(acc);

		theConttact = [
			SELECT Id
			FROM Contact
			WHERE AccountId = :acc.Id
		];

		sampleCase = new Case(
			recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
			Status = 'Open',
			fxAccount__c = fxa.Id, 
			AccountId = fxa.Account__c,
			ContactId = theConttact.Id,
			Subject = 'Subject of the case'
		);
		insert sampleCase;

		// now we try to send the email
		Test.startTest();
		CaseAuthenticationCodeResource.sendAuthenticationCodeEmail(sampleCase.Id);
		Test.stopTest();
	}
}