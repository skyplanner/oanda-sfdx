@isTest
private class UpdateAccumulatedBatcheableScheduleTest {

    static testMethod void testBatch() {
    	Account a = new Account(LastName='test');
    	insert a;
    	
        fxAccount__c f = new fxAccount__c(Name='test', Trade_Volume_USD_MTD__c=0, Account__c=a.Id);
        insert f;
        
        Opportunity o = new Opportunity(Name='test', fxAccount__c=f.Id, AccountId=a.Id, StageName='test', CloseDate=Date.today(), Metric_Start_DateTime__c=DateTime.now().addDays(-1));
        insert o;
        
        f.Trade_Volume_USD_MTD__c=1;
        update f;
        
        o = [SELECT Current_Trade_Volume_USD_MTD__c, Trade_Volume_USD_Accumulated__c, Change_Trade_Volume_USD__c, Is_Accumulating__c, Starting_Trade_Volume_USD_MTD__c FROM Opportunity WHERE fxAccount__c=:f.Id][0];
        System.assertEquals(0, o.Starting_Trade_Volume_USD_MTD__c);
        System.assertEquals(1, o.Current_Trade_Volume_USD_MTD__c);
        System.assertEquals(1, o.Change_Trade_Volume_USD__c);
        System.assertEquals(null, o.Trade_Volume_USD_Accumulated__c);
        System.assertEquals(true, o.Is_Accumulating__c);
        
        Test.startTest();
        UpdateAccumulatedBatcheableScheduleable.executeBatch();
        Test.stopTest();

        o = [SELECT Current_Trade_Volume_USD_MTD__c, Trade_Volume_USD_Accumulated__c, Change_Trade_Volume_USD__c, Starting_Trade_Volume_USD_MTD__c FROM Opportunity WHERE fxAccount__c=:f.Id][0];
        System.assertEquals(0, o.Current_Trade_Volume_USD_MTD__c);
        System.assertEquals(0, o.Starting_Trade_Volume_USD_MTD__c);
        System.assertEquals(1, o.Change_Trade_Volume_USD__c);
        System.assertEquals(1, o.Trade_Volume_USD_Accumulated__c);
        
    }
}