/* Name: PiuCkaUpdateRestResource
 * Description : New Apex Class to handle Personal Information Update from user-api
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 21
 */

 @RestResource(urlMapping='/api/v1/user/*/kids')
 global class ComplianceRecordKidRestResource {
 
     @TestVisible
     private static final String ENDPOINT_NAME = 'user_kids';
     private static final String LOGGER_CATEGORY_URL = '/api/v1/user/*/kids';
     public static final List<String> GET_PARAMS = new List<String>{'user_id'};
 
     @HttpGet
     global static void doGet() {
 
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;
 
         RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, GET_PARAMS);
 
         try {
             Integer userId = Integer.valueOf(context.getPathParam('user_id'));
             
             FxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

             if(fxa == null) {
                context.setResponse(404, 'Could not find user with given Id');
                return;
             }

             List<Map<String,Object>> response = new List<Map<String,Object>>();

             for(Compliance_Record__c cr : [
                SELECT 
                    Document_Name__c,
                    MT5_Account_Number__c,
                    MT5_Order_Number__c,
                    Document_Version__c,
                    Date_Sent__c
                FROM Compliance_Record__c
                WHERE fxAccount__r.FxTrade_Global_Id__c = :fxAccountUtil.constructGlobalTradeId(userId, true)
                AND RecordType.DeveloperName = 'KID'
             ]) {
                
                Map<String,Object> responseRow 
                    = new Map<String,Object>(cr.getPopulatedFieldsAsMap());

                responseRow.remove('Id');
                responseRow.remove('RecordTypeId');
                responseRow.remove('fxAccount__c');

                response.add(responseRow);
             }

             context.setResponse(200, response);
             return;
 
         } catch(Exception ex) {
             String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
             res.statusCode = 500;
             res.responseBody = Blob.valueOf(respBody);
             Logger.error(
                 req.resourcePath,
                 req.requestURI,
                 res.statusCode + ' ' + respBody);
             return;
         }
     }

    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, GET_PARAMS);
 
        try {
            Integer userId = Integer.valueOf(context.getPathParam('user_id'));
             
            FxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

            if(fxa == null) {
                context.setResponse(404, 'Could not find user with given Id');
                return;
            }

            Map<String, Object> payload = context.getBodyAsMap();
            
            Compliance_Record__c complianceRecord = 
                new Compliance_Record__c(
                    fxAccount__c = fxa.Id,
                    RecordTypeId = RecordTypeUtil.getComplianceRecordKidRecordTypeId()
                );

            ApiMappingService mapper = new ApiMappingService(ENDPOINT_NAME);
            mapper.loggerCategory = ENDPOINT_NAME;
            mapper.loggerCategoryUrl = LOGGER_CATEGORY_URL;

            mapper.setInputFieldstoSobject(complianceRecord, payload);

            try{
                upsert complianceRecord;
                context.setResponse(200, complianceRecord.getPopulatedFieldsAsMap());
            } catch (DmlException ex) {
                context.setResponse(400, ex.getMessage());
                Logger.exception(LOGGER_CATEGORY_URL, ex);
                return;
            }
        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody
            );
            context.setResponse(500, respBody);
        }
    }
 }