/**
 * @File Name          : DeleteAgentWorkClosing_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 4:38:23 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 4:22:27 PM   acantero     Initial Version
**/
@isTest
private without sharing class DeleteAgentWorkClosing_Test {

    @testSetup
    static void setup() {
        SLAViolationsTestDataFactory.createTestChats();
        List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
        String chatId = chatIdList[0];
        String userId = UserInfo.getUserId();
        AgentCapacityHelper.beginChatClosing(
            userId, 
            chatId,
            null //workId
        );
    }

    @isTest
    static void execute() {
        List<Agent_Work_Closing__c> agentWorkClosingList = [
            select Id from Agent_Work_Closing__c
        ];
        Test.startTest();
        DeleteAgentWorkClosing.execute(null);
        DeleteAgentWorkClosing.execute(agentWorkClosingList);
        Test.stopTest();
        Integer count = [select count() from Agent_Work_Closing__c];
        System.assertEquals(0, count);
    }
    
}