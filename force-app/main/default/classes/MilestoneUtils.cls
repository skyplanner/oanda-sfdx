/**
 * @File Name          : MilestoneUtils.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 3:50:56 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/12/2020   acantero     Initial Version
**/
public class MilestoneUtils {

    /**
     * Code provided my Salesforce to complete the milestones when closing the cases
     * author:	Michel Carrillo (SkyPlanner)
     * date:	03/16/2020
     * return:	void
     * param:	caseList 
     */
    public static void completeMiletones(List<Case> caseList){
        if (UserInfo.getUserType() == 'Standard') {
            DateTime completionDate = System.now();
            List<Id> updateCases = new List<Id>();
            for (Case c : caseList) {
                if (
                    (c.SlaStartDate != null) &&
                    (c.SlaExitDate == null) &&
                    ((c.isClosed == true) || (c.Status == 'Closed')) && 
                    (c.SlaStartDate <= completionDate) 
                )
                    updateCases.add(c.Id);
            }
            if (updateCases.isEmpty() == false) {
                completeMilestone(updateCases, completionDate);
            }
        }
    }

    public static void completeMilestone(List<Id> caseIds, DateTime complDate) {
        List<CaseMilestone> cmsToUpdate = [
            SELECT   
                Id, 
                CompletionDate
            FROM    
                CaseMilestone
            WHERE   
                caseId IN: caseIds 
            AND     
                completionDate = null 
            LIMIT 
                1
        ];
        if (cmsToUpdate.isEmpty() == false || Test.isRunningTest()) { 
            for (CaseMilestone cm : cmsToUpdate){
                cm.completionDate = complDate;
            }
            update cmsToUpdate;
        }
    }
    public static Map<String,Milestone_Time_Settings__mdt> getSettingsFilterByViolationType(
        List<String> violationTypes
    ) {
        Map<String,Milestone_Time_Settings__mdt> result = 
            new Map<String,Milestone_Time_Settings__mdt>();
        List<Milestone_Time_Settings__mdt> settings = [
            SELECT
                Channel__c,
                Milestone_Type_Name__c,
                Notification_Body__c,
                Notification_Subject__c,
                Time_Trigger__c,
                Triggered_by_owner_change__c,
                Violation_Type__c
            FROM
                Milestone_Time_Settings__mdt
            WHERE
                Violation_Type__c IN :violationTypes AND 
                Milestone_Type_Name__c LIKE '%Tier%' 
        ];
        for(Milestone_Time_Settings__mdt setting : settings) {
            result.put(setting.Milestone_Type_Name__c, setting);
        }
        return result;
    }

    public static Map<String,Milestone_Time_Settings__mdt> getMilestonesSettings(
        Set<String> milestoneTypeNameSet
    ) {
        Map<String,Milestone_Time_Settings__mdt> result = 
            new Map<String,Milestone_Time_Settings__mdt>();
        List<Milestone_Time_Settings__mdt> settings = [
            SELECT
                Channel__c,
                Milestone_Type_Name__c,
                Notification_Body__c,
                Notification_Subject__c,
                Time_Trigger__c,
                Triggered_by_owner_change__c,
                Violation_Type__c
            FROM
                Milestone_Time_Settings__mdt
            WHERE
                Milestone_Type_Name__c IN :milestoneTypeNameSet
        ];
        for(Milestone_Time_Settings__mdt setting : settings) {
            result.put(setting.Milestone_Type_Name__c, setting);
        }
        return result;
    }

    public static Milestone_Time_Settings__mdt getMilestoneSettings(
        String milestoneTypeName,
        Boolean required
    ) {
        Set<String> milestoneTypeNameSet = 
            new Set<String> {milestoneTypeName};
        Map<String,Milestone_Time_Settings__mdt> settings = 
            getMilestonesSettings(milestoneTypeNameSet);
        Milestone_Time_Settings__mdt result = 
            settings.get(milestoneTypeName);
        checkRequired(result, milestoneTypeName, required);
        return result;
    }

    @testVisible
    static void checkRequired(
        Milestone_Time_Settings__mdt milestoneSettings,
        String milestoneTypeName,
        Boolean required
    ) {
        if (
            (milestoneSettings == null) &&
            (required == true)
        ) {
            throw new MilestoneUtilsException(
                'Milestone_Time_Settings__mdt not found, name: ' + milestoneTypeName
            );
        }
    }

    //******************************************************************** */

    public class MilestoneUtilsException extends Exception {
    }

}