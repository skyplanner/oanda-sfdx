/**
 * @File Name          : ChatLanguagesManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/25/2024, 12:38:39 AM
**/
public without sharing class ChatLanguagesManager {

    public static final Integer NO_CAPACITY = 0;

    static ChatLanguagesManager instance;

    @testVisible
    Set<String> langSkillCodeAvailableSet;
    @testVisible
    Set<String> langSkillEnabledSet;

    // *** english ***

    public Boolean englishLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.ENGLISH_SKILL_CODE));
        }
    }

    public Boolean englishLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.ENGLISH_SKILL));
        }
    }

    // *** chinese ***

	public Boolean chineseLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.CHINESE_SKILL_CODE));
        }
    }

    public Boolean chineseLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.CHINESE_SKILL));
        }
    }

    // *** german ***
    
	public Boolean germanLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.GERMAN_SKILL_CODE));
        }
    }

    public Boolean germanLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.GERMAN_SKILL));
        }
    }

    // *** spanish ***

	public Boolean spanishLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.SPANISH_SKILL_CODE));
        }
    }

    public Boolean spanishLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.SPANISH_SKILL));
        }
    }

    // *** russian ***

	public Boolean russianLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.RUSSIAN_SKILL_CODE));
        }
    }

    public Boolean russianLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.RUSSIAN_SKILL));
        }
    }

    // *** polish ***

    public Boolean polishLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.POLISH_SKILL_CODE));
        }
    }

    public Boolean polishLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.POLISH_SKILL));
        }
    }

    // *** french ***

    public Boolean frenchLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.FRENCH_SKILL_CODE));
        }
    }

    public Boolean frenchLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.FRENCH_SKILL));
        }
    }

    // *** italian ***

    public Boolean italianLangAvailable {
        get {
            return (langSkillCodeAvailableSet.contains(OmnichanelConst.ITALIAN_SKILL_CODE));
        }
    }

    public Boolean italianLangEnabled {
        get {
            return (langSkillEnabledSet.contains(OmnichanelConst.ITALIAN_SKILL));
        }
    }

    // **************

    public static ChatLanguagesManager getInstance() {
        if (instance == null) {
            instance = new ChatLanguagesManager();
        }
        return instance;
    }

    public static Boolean languageIsAvailable(String languageCode) {
        OmnichanelSettings omniSettings = 
            OmnichanelSettings.getRequiredDefault();
        return languageIsAvailable(
            languageCode, // languageCode
            omniSettings.chatChannelDevName // chatChannelDevName
        );
    }

    public static Boolean languageIsAvailable(
        String languageCode,
        String chatChannelDevName
    ) {
        String logInfo = 
            'languageIsAvailable -> ' + 
		    'languageCode: ' + languageCode + 
		    ', chatChannelDevName: ' + chatChannelDevName;
        String langSkill = 
            ServiceLanguagesManager.getSkillByLangCode(languageCode);

        if (String.isBlank(langSkill)) {
            ServiceLogManager.getInstance().log(
				logInfo + ', result: false', // msg
				ChatLanguagesManager.class.getName() // category
			);
            return false;
        }
        //...
        Set<String> langSkillSet = new Set<String> { langSkill };
        ChatAgentBySkills chatAgentManager = 
            new ChatAgentBySkills(chatChannelDevName);
        Set<String> langSkillCodeSet = 
            chatAgentManager.existsCompatibleAgents(langSkillSet);
        Boolean result = !langSkillCodeSet.isEmpty();
        ServiceLogManager.getInstance().log(
			logInfo + ', result: ' + result, // msg
			ChatLanguagesManager.class.getName() // category
		);
        return result;
    }

    ChatLanguagesManager() {
        checkLanguageAvailability();
    }

    Set<String> getLangSkillEnabledSet() {
        if (langSkillEnabledSet == null) {
            langSkillEnabledSet = 
                ServiceLanguagesManager.getLangSkillsEnabledForChat();
        }
        return langSkillEnabledSet;
    }

    void checkLanguageAvailability() {
        getLangSkillEnabledSet();
        if (langSkillEnabledSet.isEmpty()) {
            langSkillCodeAvailableSet = new Set<String>();
            return;
        }
        //else...
        ChatAgentBySkills chatAgentManager = new ChatAgentBySkills();
        langSkillCodeAvailableSet = 
            chatAgentManager.existsCompatibleAgents(langSkillEnabledSet);
    }

    
}