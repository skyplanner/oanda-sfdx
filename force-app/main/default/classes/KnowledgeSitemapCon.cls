public class KnowledgeSitemapCon {
    final static List<String> languages = new String[] {'en_US', 'zh_CN'};
    
    // A property to return the proper Sites URL, used to build our full URL later
    public string siteUrl{
        get{
            String surl = site.getBaseCustomUrl();
            if (surl != '' && surl != null) {
                return site.getBaseCustomUrl();
            } else {
                return site.getBaseUrl();
            }
        }
        set;
    }

    // A method to retrieve the most recent 1000 FAQ__kav articles
    public FAQ__kav[] getFAQList(){ 
        FAQ__kav[] f = new FAQ__kav[]{};
        for(String l : languages) { 
            f.addAll([SELECT Id, KnowledgeArticleId, LastPublishedDate, Title, UrlName, Language FROM FAQ__kav where Language=:l AND PublishStatus = 'Online' AND IsVisibleInPkb = true ORDER BY LastPublishedDate DESC LIMIT 1000]);
        }
        return f;   
    }
}