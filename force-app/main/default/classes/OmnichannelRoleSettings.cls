/**
 * @File Name          : OmnichannelRoleSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/25/2021, 2:49:13 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/1/2021, 11:52:27 PM   acantero     Initial Version
**/
public without sharing class OmnichannelRoleSettings {

    @testVisible
    private static OmnichannelRoleSettings instance;

    @testVisible
    private Map<String,Set<String>> roleSettings;

    @testVisible
    OmnichannelRoleSettings() {
        roleSettings = new Map<String,Set<String>>();
    }

    public static OmnichannelRoleSettings getInstance() {
        if (instance == null) {
            instance = new OmnichannelRoleSettings();
            instance.load();
        }
        return instance;
    }

    public Set<String> getSupervisorRoles(String agentRole) {
        return roleSettings.get(agentRole);
    }

    void load() {
        roleSettings.clear();
        List<Omnichannel_Role__mdt> roleList = [
            SELECT 
                Agent_Role_Dev_Name__c,
                Supervisor_Role_Dev_Names__c
            FROM
                Omnichannel_Role__mdt
        ];
        for(Omnichannel_Role__mdt roleObj : roleList) {
            List<String> supervisorRoleList = 
                roleObj.Supervisor_Role_Dev_Names__c.split(',');
            Set<String> supervisorRoleSet = new Set<String>(supervisorRoleList);
            roleSettings.put(
                roleObj.Agent_Role_Dev_Name__c, 
                supervisorRoleSet
            );
        }
    }


}