public with sharing class SubmitCaseForApprovalController {
    @AuraEnabled (Cacheable=false)
    public static String getCaseDetails(Id caseId){
        Case cs;
        try{
            cs = [SELECT Id, fxAccount__c, fxAccount__r.Has_ComplyAdvantage_Case__c FROM Case WHERE Id =:caseId];
        }
        catch(Exception e){
            return e.getMessage();
        }
        if(cs.fxAccount__r.Has_ComplyAdvantage_Case__c) return 'ok';
        return 'A comply advantage search has not been run for this application.';
    }
    @AuraEnabled (Cacheable=false)
    public static String submitApproval(Id caseId, String comments){
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments(comments);
        req1.setObjectId(caseId);
        Approval.ProcessResult result;
        try{
            result = Approval.process(req1);
        }catch (Exception e){
            return 'There was an error: '+ e.getMessage();
        }
        if(result.isSuccess()){
            return 'ok';
        }else{
            return 'There was an error: '+ String.join(result.errors,'; ');
        }
    }
}