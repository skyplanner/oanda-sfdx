/**
 * @File Name          : LiveChatWizardTest.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/13/2022, 5:33:17 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/19/2019, 12:30:34 PM   dmorales     Initial Version
**/
@isTest
private class LiveChatWizardTest {

   public static final String TEST_LASTNAME = 'Tester';
   public static final String TEST_EMAIL = 'test1@test.com';

   @testSetup
    static void setup() {
      Account testAccount = new Account(
         LastName = TEST_LASTNAME, 
         PersonEmail = TEST_EMAIL
      );
      insert testAccount;    
    }
	
	@isTest
	 private static void testPicklistController()
    {
    	//Testing getDependentPickList method 
        Test.startTest();
        String result =
            LiveChatWizardCtrl.getDependentPicklistValues
             				(Chat_Extension__c.Inquiry_Nature__c);
        Test.stopTest();
        System.assert(result != null);       
    }
 
    @isTest
    private static void testGetClientInfoByEmail()
    {
       Test.startTest();   
       ChatClientInfo existing = LiveChatWizardCtrl.getClientInfoByEmail(TEST_EMAIL);
       Test.stopTest();
       System.assert(existing != null);
    }

    @isTest
    private static void testGetPicklistValuesLabels()
    {
       String picklistJson = LiveChatWizardCtrl.getPicklistValuesAndLabels
       (Chat_Extension__c.Inquiry_Nature__c);
       System.assert( picklistJson != null ); 
    }

    @isTest
    private static void getBackUrl() {
       LiveChatWizardCtrl instance = new LiveChatWizardCtrl();
       String result = instance.getBackUrl();
       System.assertNotEquals(null, result);
    }

    @isTest
    private static void getNextUrl() {
       LiveChatWizardCtrl instance = new LiveChatWizardCtrl();
       String result = instance.getNextUrl();
       System.assertNotEquals(null, result);
    }

    @isTest
    private static void liveChatWizardCtrl1() {
       Test.startTest();
       LiveChatWizardCtrl instance = new LiveChatWizardCtrl();
       Test.stopTest();
       System.assertNotEquals(null, instance.jsonTypeAccountDepend, 'Invalid value'); 
       System.assertNotEquals(null, instance.cryptoDivisions, 'Invalid value');
    }

    @isTest
    static void liveChatWizardCtrl2() {
       Test.setCurrentPage(Page.LiveChatWizard);
       Map<String,String> pageParams = ApexPages.currentPage().getParameters();
       pageParams.put(ChatConst.CUSTOMER_EMAIL, TEST_EMAIL);
       pageParams.put(ChatConst.CUSTOMER_LAST_NAME, TEST_LASTNAME);
       Test.startTest();
       LiveChatWizardCtrl instance = new LiveChatWizardCtrl();
       Test.stopTest();
       System.assertEquals(true, instance.isAnAccount);
    }
	
}