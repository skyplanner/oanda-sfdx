global class LeadReferralHandler {
   public final static String MESSAGE_SUCCESS = 'Success';
   public final static String MESSAGE_MISSING_INFO = 'Miss Info';
   
   @InvocableMethod
   public static List<String> referLeadToSales(List<Lead> leads) {
      List<String> results = new List<String>{MESSAGE_SUCCESS};
      
      Lead ld = (Lead)leads[0];
   	  
   	  System.debug('Input Lead: ' + ld);
   	  
   	  String divName = String.valueOf(ld.Division_Name__c);
   	  String lanPreference = String.valueOf(ld.Language_Preference__c);
   	  
   	  ld.Division_Name__c = divName;
   	  ld.Language_Preference__c = lanPreference;
   	  
   	  System.debug('Lead Division: ' + ld.Division_Name__c);
   	  
   	  if(isOwnerAssigned(ld) == false && (ld.Division_Name__c == null || ld.Division_Name__c == 'Default')){
      	 results[0] = MESSAGE_MISSING_INFO;
      	 System.debug('No Division');
      	 
      	 return results;
      }
      
      //set referral
      if(ld.CX_Referrer__c == null){
      	ld.CX_Referrer__c = UserInfo.getUserId();
      }
      
      //reassign the lead
      if(isOwnerAssigned(ld) == false){
      	 // user chooses to auto assign
      	 ld.setOptions(getDMLOptionFromLeadAssignmentRule());
      }
     
     
     update ld;
      
     return results;
      
   }
   
   private static boolean isOwnerAssigned(Lead ld){
   	   boolean result = true;
   	   
   	   if(ld.ownerId == UserUtil.getSystemUserId() || ld.ownerId == UserInfo.getUserId() || UserUtil.isActive(ld.ownerId) == false){
   	   	   result = false;
   	   }
   	   
   	   return result;
   }
   
   private static Database.DMLOptions getDMLOptionFromLeadAssignmentRule(){
    	//Fetching the assignment rules on case
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Lead' and  name = 'CX Referral' limit 1];


        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        
        return dmlOpts;
    	
    }
    
    
}