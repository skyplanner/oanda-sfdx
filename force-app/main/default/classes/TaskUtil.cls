public without sharing class TaskUtil {
	public static final String STATUS_NOT_STARTED = 'Not Started';
	public static final String STATUS_COMPLETED = 'Completed';
	public static final String TYPE_CALL = 'Call';

	public static final String PRIORITY_NORMAL = 'Normal';

	public static final Set<String> OPEN_STATUS_VALUES = new Set<String> {STATUS_NOT_STARTED};
	public static final Set<String> CALL_TYPE_VALUES = new Set<String> {TYPE_CALL};
	public static final List<Opportunity> relatedOpps = new List<Opportunity>();

	public static void createPracticeAccountTradedTasks(List<Lead> leads) {
		
		List<Task> tasks = new List<Task>();
		
		for (Lead lead : leads) {
			
			//Skip this lead if it doesn't have an owner
			if (lead.OwnerId == null) {
				continue;
			}
			
			Task task = new Task();
			
			task.WhatId = lead.Id;
			task.OwnerId = lead.OwnerId;
			
			task.Status = 'Not Started';
			task.Subject = 'Call';
			task.Type = 'Call';
			task.Description = 'Contact potential customer who just traded on their fxTrade Practice account.';
			
			tasks.add(task);
		}
		
		insert tasks;
	}
	
	public static void checkForMeaningfulInteractions(List<Task> tasks, Map<Id,Task> oldTasks) {
		Set<Id> oppIds = new Set<Id>();
		for(Task task:tasks){
			if((oldTasks==null || task.Meaningful_Interactions__c!=oldTasks.get(task.Id).Meaningful_Interactions__c ||
				task.First_Meaningful_Interaction__c!=oldTasks.get(task.Id).First_Meaningful_Interaction__c ||
				task.CallDurationInSeconds!=oldTasks.get(task.Id).CallDurationInSeconds)
				&& (task.CallDurationInSeconds!=null && task.CallDurationInSeconds>=120) || task.Meaningful_Interactions__c==true
				|| task.First_Meaningful_Interaction__c==true){
					task.Meaningful_Interactions__c=true;
					oppIds.add(task.WhatId);
			}
		}
		List<Task> oppTasks = [SELECT Id, CreatedDate, WhatId, Meaningful_Interactions__c, First_Meaningful_Interaction__c, MI_Included_Client__c FROM Task 
			WHERE WhatId IN :oppIds AND First_Meaningful_Interaction__c=TRUE];
		Map<Id, Opportunity> opps = new Map<Id,Opportunity>([SELECT Id, CreatedDate, Closed_Won_Date__c, fxTrade_Created_Date__c,
				Meaningful_Interaction__c FROM Opportunity WHERE Id IN:oppIds]);
		for(Task task:tasks){
			Opportunity opp = opps.get(task.WhatId);
			if(task.Meaningful_Interactions__c==true){
				Boolean isFirst = true;
				for(Task oppTask: oppTasks){
					if(oppTask.Id!=task.Id && oppTask.WhatId==task.WhatId) isFirst=false;
				}
				for(Task triggerTask:tasks){
					if(triggerTask.Id!=task.Id && triggerTask.WhatId==task.WhatId)isFirst=false;
				}
				if(isFirst){
					task.First_Meaningful_Interaction__c=true;
					task.First_MI_DateTime__c=System.now();
					if(opp!=null && !opp.Meaningful_Interaction__c){
						opp.Meaningful_Interaction__c=true;
						relatedOpps.add(opp);
					}
					if(opp!=null && opp.Closed_Won_Date__c!=null && opp.Closed_Won_Date__c>task.CreatedDate
					&& opp.Closed_Won_Date__c<opp.fxTrade_Created_Date__c+45){
						task.MI_Included_Client__c=true;
						task.MI_Included_Client_DateTime__c=System.now();
					}
				}
			}
		}
		
	}
	public static void completeUnansweredTask(List<Task> tasks, Map<Id,Task> oldTasks) {
		for(Task task:tasks){
			if(oldTasks!=null && task.No_answer__c==true && oldTasks.get(task.Id).No_answer__c==false){
				task.Update_Lead_Status__c='No Answer - Call attempt 1';
				task.Status='Completed';
			}
		}
	}


	public static void updateRelatedOpportunity() {
		if(!relatedOpps.isEmpty()) update relatedOpps;
	}

	public static void checkForWeekendActivityDate(List<Task> tasks) {

		Id bhId = BusinessHoursUtil.getDefaultId();
		for(Task t : tasks) {
			System.debug('t.ActivityDate: ' + t.ActivityDate);
			if (t.ActivityDate != null) {
				System.debug('BusinessHours.isWithin(bhId, t.ActivityDate): ' + BusinessHours.isWithin(bhId, t.ActivityDate));
			}
			
			if(t.CreatedById!=UserUtil.getSystemUserId() && t.ActivityDate!=null && !BusinessHours.isWithin(bhId, t.ActivityDate)) {
				t.ActivityDate = BusinessHours.nextStartDate(bhId, t.ActivityDate).date();
				// TODO: we should check to see how many days into the week.  eg. if the WFR was to wait 48 hours, but 
				// was set on Friday, and the task is due Sunday, 2 business days would be Tuesday and not Monday
			}
		}
	}
	
	public static void updateCaseModificationTime(List<Task> tasks) {
		
		Map<Id, Task> tasksByWhatId = new Map<Id, Task>();
		for (Task t : tasks) {
			if (t.WhatId != null) {
				tasksByWhatId.put(t.WhatId, t);
			}
		}
		
		// Select all cases that are linked to by the tasks
		List<Case> cases = [SELECT Id, CaseModification__c FROM Case WHERE Id IN :tasksByWhatId.keySet()];

		for (Case c : cases) {
			c.CaseModification__c = Datetime.now();
		}
		
		update cases;
	}
	
	public static void updateParentOpp(List<Task> tasks) {
		//(new WithoutSharing()).updateParentOpp2(tasks);
	}
	
	public without sharing class WithoutSharing {
		public void updateParentOpp2(List<Task> tasks) {
			Set<Id> oppIds = new Set<Id>();
			for(Task t : tasks) {
				if(OpportunityUtil.isOpportunityId(t.WhatId) && (TaskUtil.OPEN_STATUS_VALUES.contains(t.Status)==false || t.CreatedById!=UserUtil.getSystemUserId())) {
					oppIds.add(t.WhatId);
				}
			}
			if(oppIds.size()>0) {
				List<Opportunity> oppsToUpdate = new List<Opportunity>();
				for(Id oppId : oppIds) {
					oppsToUpdate.add(new Opportunity(Id=oppId));
				} 
				update oppsToUpdate;
			}
		}
	}
	
	public static boolean isClosedOrWillBeClosed(Task t) {
		return t.IsClosed || t.Status == 'Completed';
	}
	
    public static Map<Id, List<Task>> getAllTasksByLeadOrOppOrConId(Set<Id> leadIds, Set<Id> oppIds, Set<Id> conIds) {
        List<Task> allTasks = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE WhoId IN :leadIds OR WhatId IN :oppIds OR WhoId IN :conIds];

        Map<Id, List<Task>> tasksByObjectId = new Map<Id, List<Task>>();        
        for (Task t : allTasks) {
            Id objId = t.WhoId != null ? t.WhoId : t.WhatId;
            List<Task> tasksForObject = tasksByObjectId.get(objId);
            
            if (tasksForObject == null) {
                tasksForObject = new List<Task>();
                tasksByObjectId.put(objId, tasksForObject);
            }
            
            tasksForObject.add(t);
        }
        
        return tasksByObjectId;
    }
	//Deepak Malkani : Modified the signature of the method to accept DateTime instead of Date. JIRA Story # SP-2451
    public static Map<Id, List<Task>> getAllTasksAfterDateByLeadOrOppOrConId(DateTime afterDate, Set<Id> leadIds, Set<Id> oppIds, Set<Id> conIds) {

        Map<ID, Task> tasksById = new Map<ID, Task>();
        List<Task> tasksFromLeads, tasksFromOpps, tasksFromContacts;
        
        if(leadIds != null && leadIds.size() > 0){
        	
        	try{
        		tasksFromLeads = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhoId IN :leadIds];
        	}catch(System.Exception e){
        		//soql times out, retry
        		System.debug(e.getMessage());
        		tasksFromLeads = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhoId IN :leadIds];
        	}
        	
        	addToTaskMap(tasksById, tasksFromLeads);
        	
        }
        
        if(oppIds != null && oppIds.size() > 0){
        	
        	try{
        		tasksFromOpps = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhatId IN :oppIds];
        	}catch(System.Exception e){
        		//soql times out, retry
        		System.debug(e.getMessage());
        		tasksFromOpps = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhatId IN :oppIds];
        	}
        	
        	addToTaskMap(tasksById, tasksFromOpps);
        	
        }
        
        if(conIds != null && conIds.size() > 0){
        	
        	try{
        		tasksFromContacts = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhoId IN :conIds];
        	}catch(System.Exception e){
        		//soql times out, retry
        		System.debug(e.getMessage());
        		tasksFromContacts = [SELECT Id, WhoId, WhatId, IsClosed, LastModifiedDate, OwnerId, Owner.Name, ActivityDate FROM Task WHERE LastModifiedDate > :afterDate AND WhoId IN :conIds];
        	}
        	
        	addToTaskMap(tasksById, tasksFromContacts);
        }
        
        List<Task> allTasks =  tasksById.values();
     
        Map<Id, List<Task>> tasksByObjectId = new Map<Id, List<Task>>();        
        for (Task t : allTasks) {
            Id objId = t.WhoId != null ? t.WhoId : t.WhatId;
            List<Task> tasksForObject = tasksByObjectId.get(objId);
            
            if (tasksForObject == null) {
                tasksForObject = new List<Task>();
                tasksByObjectId.put(objId, tasksForObject);
            }
            
            tasksForObject.add(t);
        }
        
        return tasksByObjectId;
    }
    
    private static void addToTaskMap(Map<ID, Task> tasksById, List<Task> tasks){
    	if(tasks == null || tasks.size() == 0){
    		return;
    	}
    	
    	for(Task t : tasks){
    		tasksById.put(t.Id, t);
    	}
    }
	
    public static List<Task> coalesceTasks(Id leadId, Id conId, Id oppId, Map<Id, List<Task>> tasksByObjectId) {
        
        Set<Task> tasks = new Set<Task>();
        
        if (leadId != null) {
            List<Task> leadTasks = tasksByObjectId.get(leadId);
            
            if (leadTasks != null) {
                tasks.addAll(leadTasks);
            }
        }
        
        if (conId != null) {
            List<Task> conTasks = tasksByObjectId.get(conId);
            
            if (conTasks != null) {
                tasks.addAll(conTasks);
            }
        }
        
        if (oppId != null) {
            List<Task> oppTasks = tasksByObjectId.get(oppId);
            
            if (oppTasks != null) {
                tasks.addAll(oppTasks);
            }
        }
        
        return new List<Task>(tasks);
    }
    
    public static Boolean hasClosedTaskAfterDate(Datetime startTime, List<Task> tasks, Datetime endTime) {
		
		if (endTime == null) {
			endTime = Datetime.now();
		}
		
		// give them one more day
		endTime = endTime.addDays(1);
		
		System.debug('XXX '+ startTime + ':' + endTime + ' ||| ' + tasks);
		
		if(tasks!=null) {
			for (Task t : tasks) {
				if (t.IsClosed && t.LastModifiedDate >= startTime && (t.LastModifiedDate <= endTime)) {
					return true;
				}
			}
		}
		
		return false;
	}

    public static List<Task> getClosedTasksAfterDate(Datetime startTime, List<Task> tasks, Datetime endTime) {
		List<Task> result = new List<Task>();
		
		if (endTime == null) {
			endTime = Datetime.now();
		}
		
		System.debug('XXX '+ startTime + ':' + endTime + ' ||| ' + tasks);
		
		if(tasks!=null) {
			for (Task t : tasks) {
				if (t.IsClosed && t.LastModifiedDate >= startTime && (t.LastModifiedDate <= endTime) && UserUtil.isIntegrationUserId(t.OwnerId) == false) {
					result.add(t);
				}
			}
		}
		
		return result;
	}

    public static List<Task> getClosedTasksAfterDateByOwner(Id ownerId, Datetime startTime, List<Task> tasks, Datetime endTime) {
		List<Task> result = new List<Task>();
		
		if (endTime == null) {
			endTime = Datetime.now();
		}
		
		System.debug('XXX '+ startTime + ':' + endTime + ' ||| ' + tasks);
		
		if(tasks!=null) {
			for (Task t : tasks) {
				if (t.IsClosed && t.LastModifiedDate >= startTime && (t.LastModifiedDate <= endTime) && t.OwnerId==ownerId) {
					result.add(t);
				}
			}
		}
		
		return result;
	}

	public static Task getTask(Id ownerId, Id whoId, String subject, Id recordTypeId) {
		Task t = new Task(WhoId=whoId, Subject=subject, ActivityDate=Date.today());
		if(ownerId!=null) {
			t.OwnerId=ownerId;
		}
		if(recordTypeId!=null) {
			t.RecordTypeId=recordTypeId;
		}
		return t;
	}

	public static Set<String> getTaskOwners(List<Task> tasks) {
		Set<String> taskOwners = new Set<String>();
		for (Task t : tasks) {
			taskOwners.add(t.Owner.Name);
		}
		
		return taskOwners;
	}

	//find the First Emil response datetime and update it on the case
	public static void updateFirstEmailResponse(set<Id> caseIds) 
	{
		Id OTMSBusinessHrsId = BusinessHoursUtil.getTMSBusinessHoursId();
		Id OTMSRecTypeId = RecordTypeUtil.getOTMSSupportCaseRecordTypeId();
		for(Case c : [SELECT Id, CreatedDate, RecordTypeId,
							 (SELECT CreatedDate 
							  FROM Tasks 
							  WHERE Type = 'Email' AND 
									Owner.Name != 'System User' 
							  ORDER BY CreatedDate Limit 1)
					  FROM Case
					  Where Id IN :caseIds AND
					  		First_Email_Response__c = NULL])
		{
 			if(c.Tasks != null && c.Tasks.size() > 0 )
			{
                Case ca = Tasks.getCase(c.Id);
                ca.First_Email_Response__c = c.Tasks[0].CreatedDate;
                if(ca.RecordTypeId==OTMSRecTypeId){
					Decimal numberOfHours =
							(Decimal) BusinessHours.diff(OTMSBusinessHrsId, c.CreatedDate,  c.Tasks[0].CreatedDate);
					ca.TMS_First_Response_Time__c = numberOfHours.divide(3600000,2);
				}
			    Tasks.updateCase(ca);
			}
		}
	}
	//find the First Emil response datetime and update it on the case
	public static void updateFirstEmailResponse(List<Task> tasks) 
	{
		set<Id> caseIds = new set<Id>{};
		for (Task task : tasks) 
		{
			if(task.Type == 'Email' &&
			   task.WhatId != null && task.WhatId.getsobjecttype() == Case.sobjecttype)
			{
				caseIds.add(task.WhatId);
			}
		}
		updateFirstEmailResponse(caseIds);
	}
	
	//update last sales activity on Account, Lead
	public static void updateLastSalesActivity(Task[] newTasks)
    {  
        string userRoleName = UserUtil.getCurrentUserRole();
        for(Task t: newTasks)
        {
            if(t.Status == 'Completed' && userRoleName.indexOf('Forex Specialist') != -1)
            {
                if(t.WhoId != null && t.WhoId.getsobjecttype() == Lead.getSObjectType())
                {
                    Lead ld= Tasks.getLead(t.WhoId);
                    ld.Last_Sales_Activity__c = date.today();
                    ld.Last_Sales_Activity_By__c = UserInfo.getUserId();
					if(t.Type == 'Call'){
						ld.Last_Sales_Contacted_Call__c = System.now();
					}
					Tasks.updateLead(ld);
                }
                if(t.WhatId != null && t.WhatId.getsobjecttype() == Account.getSObjectType())
                {
                    Account acc= Tasks.getAccount(t.WhatId);
                    acc.Last_Sales_Activity__c = date.today();
                    acc.Last_Sales_Activity_By__c = UserInfo.getUserId();
					if(t.Type == 'Call'){
						acc.Last_Sales_Contacted_Call__c = System.now();
					}
					Tasks.updateAccount(acc);
				}
				if(t.WhatId != null && t.WhatId.getsobjecttype() == Opportunity.getSObjectType())
				{
					Account acc= Tasks.getAccount(t.AccountId);
                    acc.Last_Sales_Activity__c = date.today();
                    acc.Last_Sales_Activity_By__c = UserInfo.getUserId();
					if(t.Type == 'Call'){
						acc.Last_Sales_Contacted_Call__c = System.now();
					}
					Tasks.updateAccount(acc);
				}
            }
        }
	}

	/*
	 *	Modifier : Deepak Malkani.
	 *	Modification Date : July 04 2016
	 *	Purpose/Reason for Modification : Refactored the code + modified the logic to ensure Opportunity Status changes on Task Updates as well.
	 *									  JIRA Story # SP-2941 : Updating the Status AND JIRA Story # SP-2980
	*/
	public static void updateLeadStatus(Map<Id, Task> newtasks, Map<Id, Task> oldTasks) {
		
		//Updates lead or opportunity statuses, typically triggered by a task insertions or Update
		If(newtasks.size()>1){
			return;
		}

		for(Task tskObj : newtasks.values())
		{
			Id taskId = tskObj.Id;
			Task newTask = newtasks.get(taskId);
			Task oldTask = (oldTasks != null && oldTasks.containsKey(taskId)) ? oldTasks.get(taskId) : null;

			//Task Lead Status has changed. Added condition to the If block for JIRA Story SP-3011 
			if(trigger.isUpdate && 
               newTask.Update_Lead_Status__c != null && 
               newTask.Update_Lead_Status__c != oldTask.Update_Lead_Status__c)
            {
				if(String.ValueOf(newTask.WhoId) != null && newTask.WhoId.getsobjecttype() == Lead.getSObjectType())
                {
					Lead ld= Tasks.getLead(newTask.WhoId);
                    ld.Status = newTask.Update_Lead_Status__c;
					Tasks.updateLead(ld);
				}
				if(String.ValueOf(newTask.WhatId) != null && newTask.WhatId.getsobjecttype() == Opportunity.getSObjectType())
                {
                    Opportunity opp = Tasks.getOpportunity(newTask.WhatId);
                    opp.Status__c = newTask.Update_Lead_Status__c;
                    Tasks.updateOpportunity(opp);
				}
			}
			else if(trigger.isInsert && newTask.Update_Lead_Status__c != null)
            {
				if(String.ValueOf(newTask.WhoId) != null && newTask.WhoId.getsobjecttype() == Lead.getSObjectType())
                {
                    Lead ld = Tasks.getLead(newTask.WhoId);
                    ld.Status = newTask.Update_Lead_Status__c;
                    Tasks.updateLead(ld);
				}
				if(String.ValueOf(newTask.WhatId) != null && newTask.WhatId.getsobjecttype() == Opportunity.getSObjectType())
				{
                    Opportunity opp = Tasks.getOpportunity(newTask.WhatId);
                    opp.Status__c = newTask.Update_Lead_Status__c;
                    Tasks.updateOpportunity(opp);
				}
			}
		}
	}

	public static void insertPersonaTag(List<Task> tasks, Map<Id, Task> oldMap, boolean isInsert) {
		Map<Id, Id> accountIdByWhatId = new Map<Id, Id>();
		Set<Id> whatIds = new Set<Id>();
		for(Task t : tasks) {
			if(t.Persona__c!=null && t.Persona__c!='') {
				whatIds.add(t.WhatId);
			}
		}
		
		if(whatIds.size()==0) {
			return;
		}
		
		for(Opportunity o : [SELECT Id, AccountId FROM Opportunity WHERE Id IN :whatIds]) {
			accountIdByWhatId.put(o.Id, o.AccountId);
		}
		
		List<Persona_Tag__c> personaToInsert = new List<Persona_Tag__c>();
		for(Task t : tasks) {
			// if there is any change to the Persona or Comment, insert the persona
			if(isInsert || t.Persona__c!=oldMap.get(t.Id).Persona__c || t.Persona_Comments__c!=oldMap.get(t.Id).Persona_Comments__c) {
				Id accountId = t.WhatId;
				if(isAccountId(t.WhatId)==false) {
					accountId = accountIdByWhatId.get(t.WhatId);
				}
				
				Id leadId;
				if(isLeadId(t.WhoId)) {
					leadId = t.WhoId;
				}
				Persona_Tag__c p = new Persona_Tag__c(Persona__c=t.Persona__c, Comments__c=t.Persona_Comments__c, Account__c=accountId, Lead__c=leadId);
				personaToInsert.add(p);
			}
		}
		if(personaToInsert.size()>0) {
			insert personaToInsert;
		}
	}

	/**
	 * Create 'Owner Changed' tasks
	 */
	public static List<Task> createTaskOwnerChanged(
		List<Account> accs,
		Boolean save) 
    {    
		List<Task> tasks =
			new List<Task>();

		if(accs.isEmpty()) {
			return tasks;
		}

		for(Account acc :accs) {
			tasks.add(
				new Task(
					WhatId = acc.Id,
					OwnerId = acc.OwnerId,
					Subject = 'New Account Owner',
					Status = STATUS_NOT_STARTED,
					Priority = PRIORITY_NORMAL));
		}
		
		if(save)
			insert tasks;

		return tasks;
    }
    
	static boolean isAccountId(Id id) 
	{
		if(string.isNotBlank(id))
		{
			return id.getSObjectType() == Account.getSObjectType();
		}
		return false;
	}
	
	static boolean isLeadId(Id id) 
	{
		if(string.isNotBlank(id))
		{
			return id.getSObjectType() == Lead.getSObjectType();
		}
		return false;
	}


	/**
	* @description Process Builder Sync Call Result action moved into the trigger class.
	* Method assigs Call result text filed into a picklist field.
	* @author Jakub Fik | 2024-03-20 
	* @param taskList 
	**/
	public static void syncCallResultService(List<Task> taskList) {
		if (taskList == null && taskList.isEmpty()) {
		  return;
		}
	
		List<String> pickListValues = new List<String>();
		List<Schema.PicklistEntry> callResultPicklistEntry = Task.Call_Result__c.getDescribe()
		  .getPicklistValues();
		for (Schema.PicklistEntry valueLoop : callResultPicklistEntry) {
		  pickListValues.add(valueLoop.getValue());
		}
	
		for (Task loopTask : taskList) {
		  if (
			String.isNotBlank(loopTask.CallDisposition) &&
			pickListValues.contains(loopTask.CallDisposition) &&
			!loopTask.CallDisposition.equals(loopTask.Call_Result__c)
		  ) {
			loopTask.Call_Result__c = loopTask.CallDisposition;
		  }
		}
	  }
	
}