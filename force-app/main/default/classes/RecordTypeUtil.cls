public with sharing class RecordTypeUtil {
	public static Map<String, Map<String, RecordType>> recordTypeByNameBySObjectName;
	
	public final static String NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD = 'Standard Lead';
	public final static String NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE = 'Retail Lead House Account';
	public final static String NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE = 'Retail Practice Lead';
	public final static String NAME_LEAD_RECORDTYPE_EXCHANGE_RATE = 'Exchange Rate Lead';
	public final static String NAME_LEAD_RECORDTYPE_AFFILIATE = 'IB/Affiliate';
    public final static String NAME_LEAD_RECORDTYPE_OTMS = 'OTMS Leads';
	
	public static final Schema.DescribeSObjectResult fxAccountSobjDescribeResult= fxAccount__c.SobjectType.getDescribe();
	public static final Map<Id, Schema.RecordTypeInfo> fxAccountRecordTypeMapById = fxAccountSobjDescribeResult.getRecordTypeInfosById();
	public static final Map<String ,Schema.RecordTypeInfo> fxAccountRecordTypeMapByName = fxAccountSobjDescribeResult.getRecordTypeInfosByName(); 
	public static final String NAME_FXACCOUNT_RECORDTYPE_LIVE = 'Retail Live';
	public static final String NAME_FXACCOUNT_RECORDTYPE_PRACTICE = 'Retail Practice';
	public static final String NAME_FXACCOUNT_RECORDTYPE_CRYPTO = 'Crypto';
	
	public static final string FXACCOUNT_CRYPTO_RECORD_TYPE_ID = getFxaccountCryptoRecordTypeId();
	
	public static final String NAME_OPPORTUNITY_RETAIL_OAP = 'Retail OAP';
	public static final String NAME_OPPORTUNITY_RETAIL = 'Retail';
	public static final String NAME_OPPORTUNITY_RETENTION = 'Retention';
	public static final Set<String> NAMES_OPPORTUNITY_RETAIL = new Set<String> { NAME_OPPORTUNITY_RETAIL, NAME_OPPORTUNITY_RETAIL_OAP };
	
	public static final String NAME_PERSON_ACCOUNT = 'Person Account';
	public static final String NAME_BUSINESS_ACCOUNT = 'Business Account';
	public static final String NAME_CUSTOMER_ACCOUNT = 'Customer Account';
	public static final String NAME_PROSPECT_ACCOUNT = 'Prospect Account';
	public static final String NAME_ATTRITED_ACCOUNT = 'Attrited Account';
	public static final String NAME_ENTITY_PERSON_ACCOUNT = 'Entity Person Account';
	
	public static final String NAME_EXCHANGE_RATES_ACCOUNT = 'Exchange Rate Customer Account';
	
	public static final String NAME_EXCHANGE_RATES_LEAD = 'Exchange Rate Lead';
	
	public static final String NAME_REGISTRATION_CASE = 'Registration';
	public static final String NAME_SUPPORT_CASE = 'Support';
	public static final String NAME_ONBOARDING_CASE = 'Onboarding';
	public static final String NAME_SUPPORT_CCM_CASE = 'Support - CCM';
	public static final String NAME_SUPPORT_COMPLIANCE_CASE = 'Support - Compliance';
	public static final String NAME_SUPPORT_OB_CASE = 'Support - OB';
    public static final String NAME_COMPLIANCE_CHECK_CASE = 'Compliance Check';
    public static final String NAME_OTMS_SUPPORT_CASE = 'OTMS Support';
    public static final String NAME_OTMS_ONBOARDING_CASE = 'OTMS Onboarding';
		
	public static final Set<String> NAMES_LEAD_RECORDTYPE_FX = new Set<String> {NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD, NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE};
	public static final Set<String> NAMES_LEAD_RECORDTYPE_FOR_MERGE = NAMES_LEAD_RECORDTYPE_FX;
	
	public static final Set<String> NAMES_ACCOUNT_RECORDTYPE_FX = new Set<String> {NAME_PERSON_ACCOUNT, NAME_ENTITY_PERSON_ACCOUNT};
	public static final Set<String> NAMES_ACCOUNT_RECORDTYPE_FOR_MERGE = NAMES_ACCOUNT_RECORDTYPE_FX;
	
	public static final String NAME_DOCUMENT_SUPPORT = 'Support';
	public static final String NAME_DOCUMENT_CHECK = 'Check';
	public static final String NAME_DOCUMENT_SCAN = 'Scan';
	
	public static final String NAME_ENTITY_CONTACT_GENERAL = 'General';

	public static final String NAME_BANKING_PARTNER = 'Banking Partners';
	public static final String NAME_VENDORS = 'Vendors';
	public static final String NAME_AFFILIATES = 'Affiliates';

	public static final String COMPLIANCE_RECORD_KIDS = 'KID';
	public static final String COMPLIANCE_RECORD_LEGAL_AGREEMENTS = 'LEGAL';

	public static final Schema.DescribeSObjectResult caseDescSobjResult= Case.SobjectType.getDescribe();
	public static final Map<Id, Schema.RecordTypeInfo> caseRecordTypeMapById = caseDescSobjResult.getRecordTypeInfosById();
	public static final Map<String ,Schema.RecordTypeInfo> caseRecordTypeMapByName =caseDescSobjResult.getRecordTypeInfosByName(); //Schema.SObjectType.Case.getRecordTypeInfosByName();

	public static final Schema.DescribeSObjectResult partnerSobjDescribeResult= Affiliate__c.SobjectType.getDescribe();
	public static final Map<Id, Schema.RecordTypeInfo> partnerRecordTypeMapById = partnerSobjDescribeResult.getRecordTypeInfosById();
	public static final Map<String ,Schema.RecordTypeInfo> partnerRecordTypeMapByName =partnerSobjDescribeResult.getRecordTypeInfosByName(); 

	public static final string onboardingCaseRecordTypeId = getOnBoardingCaseTypeId();
	public static final string complianceCheckCaseRecordTypeId = getComplianceCheckCaseRecordTypeId();
	public static final string BANKING_PARTNER_RECORD_TYPE_ID = getBankingPartnerRecordTypeId();
	public static final string VENDORS_RECORD_TYPE_ID = getVendorsRecordTypeId();
	public static final string AFFILIATES_RECORD_TYPE_ID = getAffiliatesRecordTypeId();
	public static final string OTMS_SUPPORT_CASE_RECORD_TYPE_ID = getOTMSSupportCaseRecordTypeId();
	public static final string OTMS_LEAD_RECORD_TYPE_ID = getOTMSLeadRecordTypeId();
	
	public static Id getRecordTypeId(String name, String sObjectName) {
		RecordType rt = getRecordType(name, sObjectName);
		return rt==null ? null : rt.Id;
	} 
	
	public static RecordType getRecordType(String name, String sObjectName) {
		getAllRecordTypes();
		Map<String, RecordType> recordTypeByName = recordTypeByNameBySObjectName.get(sObjectName);
		if(recordTypeByName!=null)
		{
			 return recordTypeByName.get(name);
		}
		return null;
	}
	
	
	public static void getAllRecordTypes() {
		if(recordTypeByNameBySObjectName==null) {
			recordTypeByNameBySObjectName = new Map<String, Map<String, RecordType>>();
			for(RecordType rt : [select Id, Name, SobjectType from RecordType]) {
				Map<String, RecordType> recordTypeByName = recordTypeByNameBySObjectName.get(rt.SobjectType);
				if(recordTypeByName==null)
				{
					recordTypeByName = new Map<String, RecordType>();
					recordTypeByNameBySObjectName.put(rt.SobjectType, recordTypeByName);
					
				}
				recordTypeByName.put(rt.Name, rt);
			}
		}
	}
	
	public static Id getLeadRetailId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, LeadUtil.NAME_SOBJECT_LEAD);
	}
	
	public static Id getLeadRetailPracticeId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, LeadUtil.NAME_SOBJECT_LEAD);
	}
	
	public static Id getLeadStandardId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD, LeadUtil.NAME_SOBJECT_LEAD);
	}
	
	public static Boolean isRetailPracticeLead(Lead l) {
		return l.RecordTypeId == getLeadRetailPracticeId();
	}
	
	public static Boolean isRetailLead(Lead l) {
		return l.RecordTypeId == getLeadRetailId();
	}
	
	public static Id getFxAccountLiveId() {
		return getRecordTypeId(NAME_FXACCOUNT_RECORDTYPE_LIVE, fxAccountUtil.NAME_SOBJECT_FXACCOUNT);
	}
	
	public static Id getFxAccountPracticeId() {
		return getRecordTypeId(NAME_FXACCOUNT_RECORDTYPE_PRACTICE, fxAccountUtil.NAME_SOBJECT_FXACCOUNT);
	}

	private static Id getFxaccountCryptoRecordTypeId()
	{
		return fxAccountRecordTypeMapByName.get(NAME_FXACCOUNT_RECORDTYPE_CRYPTO).getRecordTypeId(); 
	} 
	
	public static Id getFxAccountRecordTypeIdFromLead(Lead l) {
		if(isRetailPracticeLead(l)) {
			return getFxAccountPracticeId();
		} else if(isRetailLead(l)) {
			return getFxAccountLiveId();
		} else {
			return null;
		}
	}
	
	public static Id getCustomerAccountRecordTypeId() {
		return getRecordTypeId(NAME_CUSTOMER_ACCOUNT, 'Account');
	}
	
	public static Id getPersonAccountRecordTypeId() {
		return getRecordTypeId(NAME_PERSON_ACCOUNT, 'Account');
	}

	public static Id getBusinessAccountRecordTypeId() {
		return getRecordTypeId(NAME_BUSINESS_ACCOUNT, 'Account');
	}	
	
	public static Id getProspectAccountRecordTypeId() {
		return getRecordTypeId(NAME_PROSPECT_ACCOUNT, 'Account');
	}
	
	public static Id getAttritedAccountRecordTypeId() {
		return getRecordTypeId(NAME_ATTRITED_ACCOUNT, 'Account');
	}
	
	public static Id getExchangeRatesRecordTypeId() {
		return getRecordTypeId(NAME_EXCHANGE_RATES_ACCOUNT, 'Account');
	}

	public static Id getEntityPersonAccountRecordTypeId() {
		return getRecordTypeId(NAME_ENTITY_PERSON_ACCOUNT, 'Account');
	}
	
	public static Id getExchangeRatesLeadRecordTypeId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_EXCHANGE_RATE, 'Lead');
	}

	public static Id getAffiliateLeadRecordTypeId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_AFFILIATE, 'Lead');
	}
    public static Id getOTMSLeadRecordTypeId() {
		return getRecordTypeId(NAME_LEAD_RECORDTYPE_OTMS, 'Lead');
	}
	
	public static Set<Id> getLeadMergeRecordTypeIds() {
		Set<Id> result = new Set<Id>();
		for(String n : NAMES_LEAD_RECORDTYPE_FOR_MERGE) {
			result.add(getRecordTypeId(n, 'Lead'));
		}
		return result;
	}
	
	public static boolean isLeadMergeRecordTypeId(Id recordTypeId) {
		if(getLeadMergeRecordTypeIds().contains(recordTypeId)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isRetentionOpportunity(Opportunity o) {
		System.debug('o: ' + o);
		System.debug('o.RecordTypeId: ' + o.RecordTypeId);
		System.debug('getRecordTypeId(NAME_OPPORTUNITY_RETENTION, OpportunityUtil.NAME_OPPORTUNITY): ' + getRecordTypeId(NAME_OPPORTUNITY_RETENTION, OpportunityUtil.NAME_OPPORTUNITY));
		return o.RecordTypeId==getRetentionOpportunityTypeId();
	}
	
	public static Id getRetentionOpportunityTypeId() {
		return getRecordTypeId(NAME_OPPORTUNITY_RETENTION, OpportunityUtil.NAME_OPPORTUNITY);
	}
	
	public static Id getSalesOpportunityTypeId(){
		return getRecordTypeId(NAME_OPPORTUNITY_RETAIL, OpportunityUtil.NAME_OPPORTUNITY);
	}
	
	public static Id getSupportCaseTypeId(){
		return getRecordTypeId(NAME_SUPPORT_CASE, CaseUtil.NAME_CASE);
	}

	public static Id getSupportOBCaseTypeId(){
		return getRecordTypeId(NAME_SUPPORT_OB_CASE, CaseUtil.NAME_CASE);
	}
	
	public static Id getSupportComplianceCaseTypeId(){
		return getRecordTypeId(NAME_SUPPORT_COMPLIANCE_CASE, CaseUtil.NAME_CASE);
	}
	
	public static Id getOnBoardingCaseTypeId()
	{
		return caseRecordTypeMapByName.get(NAME_ONBOARDING_CASE).getRecordTypeId();
		//return getRecordTypeId(NAME_ONBOARDING_CASE, CaseUtil.NAME_CASE);
	}
    
    public static Id getComplianceCheckCaseRecordTypeId(){
		return getRecordTypeId(NAME_COMPLIANCE_CHECK_CASE, CaseUtil.NAME_CASE);
	}

	public static Id getOTMSSupportCaseRecordTypeId(){
		return getRecordTypeId(NAME_OTMS_SUPPORT_CASE, CaseUtil.NAME_CASE);
	}

	public static Id getOTMSOnboardingCaseRecordTypeId(){
		return getRecordTypeId(NAME_OTMS_ONBOARDING_CASE, CaseUtil.NAME_CASE);
	}

	public static Id getSupportDocumentTypeId(){
		return getRecordTypeId(NAME_DOCUMENT_SUPPORT, DocumentUtil.NAME_DOCUMENT);
	}

	public static Id getScanDocumentTypeId(){
		return getRecordTypeId(NAME_DOCUMENT_SCAN, DocumentUtil.NAME_DOCUMENT);
	}

	public static Id getBankingPartnerRecordTypeId()
	{
		return partnerRecordTypeMapByName.get(NAME_BANKING_PARTNER).getRecordTypeId();
	}

	public static Id getVendorsRecordTypeId()
	{
		return partnerRecordTypeMapByName.get(NAME_VENDORS).getRecordTypeId();
	}

	public static Id getAffiliatesRecordTypeId()
	{
		return partnerRecordTypeMapByName.get(NAME_AFFILIATES).getRecordTypeId();
	}

	public static Id getComplianceRecordKidRecordTypeId()
	{
		return getRecordTypeId(COMPLIANCE_RECORD_KIDS, 'Compliance_Record__c');
	}
	
	public static Id getEntityContactGeneralRecordTypeId() {
		return getRecordTypeId(NAME_ENTITY_CONTACT_GENERAL, 'Entity_Contact__c');
	}

	public static Boolean isCaseOTMSRecordTypes(Id recordTypeId) {
		return recordTypeId == getOTMSSupportCaseRecordTypeId() || recordTypeId == getOTMSOnboardingCaseRecordTypeId();
	}
}