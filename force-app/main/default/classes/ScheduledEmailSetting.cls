/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class ScheduledEmailSetting implements IScheduledEmailSetting {
	@TestVisible
	private static string DEFAULT_WEEK_DAY = 'Sunday';
	@TestVisible
	private static string DEFAULT_TIME = '4';
	@TestVisible
	private static string DEFAULT_AM_PM = 'PM';
	@TestVisible
	private static Integer DEFAULT_SCOPE = 10;

	private Schedule_Mail_Setting__mdt scheduleMtd;

	public ScheduledEmailSetting(Schedule_Mail_Setting__mdt mdt) {
		scheduleMtd = mdt;
	}

	public Integer getScope() {
		return this.scheduleMtd != null &&
			this.scheduleMtd.Scope__c != null
			? scheduleMtd.Scope__c.intValue()
			: DEFAULT_SCOPE;
	}
	public Date getDate() {
		String strDayOfWeek = this.scheduleMtd != null &&
			String.isNotBlank(scheduleMtd.Day_of_the_Week__c)
			? scheduleMtd.Day_of_the_Week__c
			: DEFAULT_WEEK_DAY;
		Integer day = convertWeekDayToInteger(strDayOfWeek);
		return nextDate(day);
	}
	public String getTime() {
		return this.scheduleMtd != null &&
			String.isNotBlank(scheduleMtd.Time__c)
			? scheduleMtd.Time__c
			: DEFAULT_TIME;
	}
	public String getAMOrPM() {
		return this.scheduleMtd != null &&
			String.isNotBlank(scheduleMtd.AM_or_PM__c)
			? scheduleMtd.AM_or_PM__c
			: DEFAULT_AM_PM;
	}
	public Boolean getAllowDelete() {
		return this.scheduleMtd != null &&
			scheduleMtd.Delete_a_Draft_Email__c != null
			? scheduleMtd.Delete_a_Draft_Email__c
			: false;
	}
	public void setAllowDelete(Boolean value) {
		if (this.scheduleMtd != null) {
			this.scheduleMtd.Delete_a_Draft_Email__c = value;
		}
	}
	@TestVisible
	private Integer convertWeekDayToInteger(String dayOfWeek) {
		Integer day = 0;
		switch on dayOfWeek {
			when 'Sunday' {
				day = 0;
			}
			when 'Monday' {
				day = 1;
			}
			when 'Tuesday' {
				day = 2;
			}
			when 'Wednesday' {
				day = 3;
			}
			when 'Thursday' {
				day = 4;
			}
			when 'Friday' {
				day = 5;
			}
			when else {
				day = 6;
			}
		}
		return day;
	}
	@TestVisible
	private Date nextDate(Integer nextDayOfWeek) {
		Date today = Date.today();
		Integer day = today.day();
		Integer currentDayOfWeek = this.dayOfWeek(today);
		Integer next =
			day + Math.mod((nextDayOfWeek + (7 - currentDayOfWeek)), 7);
		return today.addDays(next - day);
	}
	@TestVisible
	private Integer dayOfWeek(Date aDate) {
		return Math.mod(Date.newInstance(1900, 1, 7).daysBetween(aDate), 7);
	}
}