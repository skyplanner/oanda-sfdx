/**
 * @File Name          : OnHvcCaseReroutedCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/16/2022, 3:30:49 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/16/2022, 2:11:43 PM   acantero     Initial Version
**/
@IsTest
private without sharing class OnHvcCaseReroutedCmd_Test {

	@testSetup
    static void setup() {
        Case newHvcCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE,
            Routed__c = true
        );
        List<Case> caseList = new List<Case>{newHvcCase1};
        OmnichanelRoutingTestDataFactory.setupAllForHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    // *** test1 ***
    // valid hvc case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE
    // => the case is added to reroutedCaseIdSet
    // *** test2 ***
    // valid hvc case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_CLOSED
    // => the case is not added to reroutedCaseIdSet
    @IsTest
    static void checkRecord() {
        Case rec = getCase();
        Case oldRec = getCase();
        Test.startTest();
        // test 1
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE;
        OnHvcCaseReroutedCmd instance = new OnHvcCaseReroutedCmd();
        instance.checkRecord(rec, oldRec);
        Integer reroutedCaseIdSetSize1 = instance.reroutedCaseIdSet.size();
        // test 2
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED;
        instance = new OnHvcCaseReroutedCmd();
        instance.checkRecord(rec, oldRec);
        Integer reroutedCaseIdSetSize2 = instance.reroutedCaseIdSet.size();
        Test.stopTest();
        // test 1 results
        System.assertEquals(1, reroutedCaseIdSetSize1, 'Invalid result');
        // test 2 results
        System.assertEquals(0, reroutedCaseIdSetSize2, 'Invalid result');
    }

    // *** test1 ***
    // reroutedCaseIdSet is empty => do nothing, return false
    // *** test2 ***
    // reroutedCaseIdSet has a record 
    // => fire reroute event and return true
    @IsTest
    static void execute() {
        Case rec = getCase();
        Case oldRec = getCase();
        Test.startTest();
        // test 1
        OnHvcCaseReroutedCmd instance = new OnHvcCaseReroutedCmd();
        Boolean result1 = instance.execute();
        // test 2
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE;
        instance.checkRecord(rec, oldRec);
        Boolean result2 = instance.execute();
        Test.stopTest();
        System.assertEquals(false, result1, 'Invalid result');
        System.assertEquals(true, result2, 'Invalid result');
    }

    static Case getCase() {
        return [
            select 
                Id, 
                Agent_Capacity_Status__c, 
                Routed__c,
                Is_HVC__c
            from 
                Case
            limit 1
        ];
    }
    
}