/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public inherited sharing class ScheduledEmailRepo {
	public static Scheduled_Email__c getByCaseAndOwner(Id caseId, Id ownerId) {
		List<Scheduled_Email__c> scheduledEmailList = [
			SELECT
				Id,
				AM_or_PM__c,
				Case__c,
				Email_Message__c,
				Time__c,
				Scheduled_Date__c
			FROM Scheduled_Email__c
			WHERE Case__c = :caseId AND OwnerId = :ownerId
		];
		return scheduledEmailList.isEmpty() ? null : scheduledEmailList.get(0);
	}
  
	public static List<Scheduled_Email__c> getByEmailMessagesAndOwner(
		List<Id> ids,
		Id ownerId
	) {
		return [
			SELECT Id
			FROM Scheduled_Email__c
			WHERE Email_Message__c IN :ids AND OwnerId = :ownerId
		];
	}

	public static List<Scheduled_Email__c> getByEmailMessages(List<Id> ids) {
		return [
			SELECT Id
			FROM Scheduled_Email__c
			WHERE Email_Message__c IN :ids
		];
	}

	public static List<Scheduled_Email__c> getByDateTime(
		Datetime scheduledDateTime,
		Integer count
	) {
		return [
			SELECT Id, Email_Message__c
			FROM Scheduled_Email__c
			WHERE Scheduled_Date_Time__c <= :scheduledDateTime
			LIMIT :count
		];
	}

	public static List<Scheduled_Email__c> getAll(List<String> ids) {
		return [ SELECT Id, Email_Message__c, Case__c, Case__r.OwnerId FROM Scheduled_Email__c WHERE Id IN :ids];
	}
}