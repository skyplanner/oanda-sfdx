/**
 * @File Name          : CaseReroutedTrigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/16/2022, 4:36:43 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/16/2022, 3:43:51 PM   acantero     Initial Version
**/
@IsTest
private without sharing class CaseReroutedTrigger_Test {

	@testSetup
    static void setup() {
        Case newHvcCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE,
            Routed__c = true,
            Internally_Routed__c = false
        );
        List<Case> caseList = new List<Case>{newHvcCase1};
        OmnichanelRoutingTestDataFactory.setupAllForHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    // disable trigger handler, then publish event for a valid case 
    // => nothing happens (the trigger handler does not execute)
    @IsTest
    static void isEnabled() {
        Case caseObj = getCase();
        String originalAgentCapacityStatus = caseObj.Agent_Capacity_Status__c;
        Test.startTest();
        List<Case_Rerouted__e> eventList = new List<Case_Rerouted__e>();
        eventList.add(
            new Case_Rerouted__e(
                Case_ID__c = caseObj.Id
            )
        );
        CaseReroutedTriggerHandler.enabled = false;
        Eventbus.publish(eventList);
        Test.stopTest();
        caseObj = getCase();
        System.assertEquals(
            originalAgentCapacityStatus, 
            caseObj.Agent_Capacity_Status__c, 
            'Invalid value'
        );
        System.assertEquals(
            false, 
            caseObj.Internally_Routed__c, 
            'Invalid value'
        );
    }

    // publish event for a valid case (trigger handler is enabled)
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    // => Internally_Routed__c = true
    @IsTest
    static void handleAfterInsert() {
        Case caseObj = getCase();
        Test.startTest();
        List<Case_Rerouted__e> eventList = new List<Case_Rerouted__e>();
        eventList.add(
            new Case_Rerouted__e(
                Case_ID__c = caseObj.Id
            )
        );
        CaseReroutedTriggerHandler.enabled = true;
        Eventbus.publish(eventList);
        Test.stopTest();
        caseObj = getCase();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
            caseObj.Agent_Capacity_Status__c, 
            'Invalid value'
        );
        System.assertEquals(
            true, 
            caseObj.Internally_Routed__c, 
            'Invalid value'
        );
    }

    // an exception (other than LogException) is thrown 
    @isTest
    static void exception1() {
        Boolean exceptionOk = false;
        Case caseObj = getCase();
        List<Case_Rerouted__e> eventList = new List<Case_Rerouted__e>();
        eventList.add(
            new Case_Rerouted__e(
                Case_ID__c = caseObj.Id
            )
        );
        Test.startTest();
        ExceptionTestUtil.prepareDummyException();
        try {
            new CaseReroutedTriggerHandler(eventList).handleAfterInsert();
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    // an exception (of type LogException) is thrown 
    @isTest
    static void exception2() {
        Boolean exceptionOk = false;
        Case caseObj = getCase();
        List<Case_Rerouted__e> eventList = new List<Case_Rerouted__e>();
        eventList.add(
            new Case_Rerouted__e(
                Case_ID__c = caseObj.Id
            )
        );
        Test.startTest();
        ExceptionTestUtil.exceptionInstance = 
            LogException.newInstance('abc', 'def');
        try {
            new CaseReroutedTriggerHandler(eventList).handleAfterInsert();
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    static Case getCase() {
        return [
            select 
                Id, 
                Agent_Capacity_Status__c, 
                Routed__c,
                Is_HVC__c,
                Internally_Routed__c
            from 
                Case
            limit 1
        ];
    }
    
}