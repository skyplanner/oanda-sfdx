/**
 * @File Name          : ServicePresenceStatusHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 12:29:40 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/22/2021, 12:28:26 AM   acantero     Initial Version
**/
@IsTest
private without sharing class ServicePresenceStatusHelper_Test {

	// @testSetup
    // static void setup() {
    // }

    //blank devName => throw exception
    @IsTest
    static void getByDevName1() {
        Boolean error = false;
        Test.startTest();
        try {
            ServicePresenceStatus result = 
                 ServicePresenceStatusHelper.getByDevName(null, true);
            //...
        } catch ( ServicePresenceStatusHelper.ServPresStatusHelperException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }

    //invalid devName => throw exception
    @IsTest
    static void getByDevName2() {
        Boolean error = false;
        Test.startTest();
        try {
            ServicePresenceStatus result = 
                 ServicePresenceStatusHelper.getByDevName('fakeDevName', true);
            //...
        } catch ( ServicePresenceStatusHelper.ServPresStatusHelperException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
    
}