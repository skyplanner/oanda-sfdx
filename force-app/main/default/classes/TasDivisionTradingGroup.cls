/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-15-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasDivisionTradingGroup implements Comparable {
    public TasDivisionTradingGroupId id { get; set; }

    public String name { get; set; }
    
    public String description { get; set; }

    public List<Integer> homeCurrencies {get; set;}

    public Integer currencyConversionGroupID { get; set; }

    public transient String label {
        get {
            return id?.tradingGroupID + ' - ' + description;
        } 
    }

    public Integer compareTo(Object compareTo) {
        TasDivisionTradingGroup compareToGroup =
            (TasDivisionTradingGroup) compareTo;

        if (id?.tradingGroupID > compareToGroup.id?.tradingGroupID)
            return 1;
        else if (id?.tradingGroupID < compareToGroup.id?.tradingGroupID)
            return -1;
        return 0;
    }
}