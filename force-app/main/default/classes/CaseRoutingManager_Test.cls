/**
 * @File Name          : CaseRoutingManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 4:17:39 AM
**/
@isTest(isParallel = true)
private class CaseRoutingManager_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
    }
    
    //case list param = null
    @isTest
    static void routeUsingSkills() {
        Test.startTest();
        List<Case> caseList1 = null;
        Boolean result1 = 
            CaseRoutingManager.getInstance().routeUsingSkills(
                caseList1 // caseList
            );
        
        Boolean result2 = 
            CaseRoutingManager.getInstance().routeUsingSkills(
                new CaseRoutingManager.CaseRoutingRequest(
                    null, // caseList
                    null, // caseIdHvcMap
                    null, // routeOnlyHvc
                    null // markAsRoutedAndUpdate
                )
            );

        Test.stopTest();
        System.assertEquals(false, result1, 'Invalid value');
        System.assertEquals(false, result2, 'Invalid value');
    }

    //case list param ok
    @isTest
    static void caseRoutingManager_test() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        String emailApiEntitlementId = EntitlementSettings.getEmailApiEntitlementId();
        String emailChatbotEntitlementId = EntitlementSettings.getEmailChatbotEntitlementId();
        String chatEntitlementId = EntitlementSettings.getChatEntitlementId();
        //...
        OmnichanelRoutingTestDataFactory.createTestAccounts();
        OmnichanelRoutingTestDataFactory.createTestLeads();
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        Map<String, ID> leadMap = OmnichanelRoutingTestDataFactory.getLeadMap();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        Set<ID> originalCasesQueueIdSet = CaseRoutingManager.getOriginalCaseQueueIdSet();
        String originalCasesQueueId = new List<ID>(originalCasesQueueIdSet)[0];
        List<Case> caseList = new List<Case>();
        //HVC
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        Case hvcCase1 = new Case(
            AccountId = hvcId,
            //Subject = 'HVC Case 1',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            OwnerId = originalCasesQueueId,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        System.debug('hvcCase1.Is_HVC__c: ' + hvcCase1.Is_HVC__c);
        caseList.add(hvcCase1);
        //...
        Case hvcCase2 = new Case(
            AccountId = hvcId,
            //Subject = 'HVC Case 2',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_LOGIN,
            OwnerId = UserInfo.getUserId(),
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_API
        );
        System.debug('hvcCase2.Is_HVC__c: ' + hvcCase2.Is_HVC__c);
        caseList.add(hvcCase2);
        //...
        Case hvcCase3 = new Case(
            AccountId = hvcId,
            Subject = 'abcd',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY,
            OwnerId = originalCasesQueueId,
            Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL
        );
        System.debug('hvcCase3.Is_HVC__c: ' + hvcCase3.Is_HVC__c);
        caseList.add(hvcCase3);
        //...
        //simulating chat case
        Case hvcCase4 = new Case(
            Chat_Is_HVC__c = OmnichanelConst.CHAT_IS_HVC_YES,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY,
            Origin = OmnichanelConst.CASE_ORIGIN_CHAT
        );
        caseList.add(hvcCase4);
        //...
        //simulating neuraflash chat case
        Case hvcCase5 = new Case(
            Chat_Email__c = OmnichanelRoutingTestDataFactory.HVC_EMAIL,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
            Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE
        );
        caseList.add(hvcCase5);
        //...
        //Non HVC Case
        ID activeCustomerAccountId = accountMap.get(OmnichanelRoutingTestDataFactory.ACTIVE_CUSTOMER_EMAIL);
        Case nonHvcCase = new Case(
            AccountId = activeCustomerAccountId,
            Subject = 'Active Customer Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY
        );
        System.debug('nonHvcCase.Is_HVC__c: ' + nonHvcCase.Is_HVC__c);
        caseList.add(nonHvcCase);
        //Lead Case
        ID newCustomerLeadId = leadMap.get(OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL);
        Case newCustomerLeadCase = new Case(
            Lead__c = newCustomerLeadId,
            Subject = 'New Customer Lead Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER
        );
        caseList.add(newCustomerLeadCase);
        Test.startTest();
        insert caseList;
        Test.stopTest(); 
        Integer psrCount = [select count() from PendingServiceRouting];
        Map<Id,Case> caseMap = new Map<Id,Case>([
            select 
                OwnerId, 
                EntitlementId, 
                Lead__c, 
                AccountId, 
                Is_HVC__c, 
                Chat_Is_HVC__c,
                Chat_Email__c
            from 
                Case
        ]);
        hvcCase1 = caseMap.get(hvcCase1.Id);
        hvcCase2 = caseMap.get(hvcCase2.Id);
        hvcCase3 = caseMap.get(hvcCase3.Id);
        hvcCase4 = caseMap.get(hvcCase4.Id);
        hvcCase5 = caseMap.get(hvcCase5.Id);
        nonHvcCase = caseMap.get(nonHvcCase.Id);
        newCustomerLeadCase = caseMap.get(newCustomerLeadCase.Id);
        ID skillsQueueId = instance.getSkillCasesQueueId();
        System.debug('originalCasesQueueId: ' + originalCasesQueueId);
        System.debug('skillsQueueId: ' + skillsQueueId);
        System.debug('userId: ' + UserInfo.getUserId());
        //...
        System.debug('hvcCase5');
        System.debug(hvcCase5);
        //...
        System.assertEquals(skillsQueueId, hvcCase1.OwnerId);
        System.assertEquals(emailFrontdeskEntitlementId, hvcCase1.EntitlementId);
        //...
        System.assertNotEquals(skillsQueueId, hvcCase2.OwnerId);
        System.assertEquals(emailApiEntitlementId, hvcCase2.EntitlementId);
        //...
        System.assertEquals(skillsQueueId, hvcCase3.OwnerId);
        System.assertEquals(emailChatbotEntitlementId, hvcCase3.EntitlementId);
        //...
        System.assertEquals(chatEntitlementId, hvcCase4.EntitlementId);
        //...
        System.assertEquals(chatEntitlementId, hvcCase5.EntitlementId);
        //...
        System.assertNotEquals(skillsQueueId, nonHvcCase.OwnerId);
        System.assertEquals(null, nonHvcCase.EntitlementId);
        //...
        System.assertNotEquals(skillsQueueId, newCustomerLeadCase.OwnerId);
        System.assertEquals(null, newCustomerLeadCase.EntitlementId);
        //hvcCase4 and hvcCase5 are chat cases, so the chats are routed by omnichanel, not the cases
        System.assertEquals(3, psrCount); 
    }

    //case list param ok
    @isTest
    static void routeNonHvcCasesFromIds() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        //...
        ID newCustomerLeadId = OmnichanelRoutingTestDataFactory.createNewCustomerLead();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        Case newCustomerLeadCase = new Case(
            Lead__c = newCustomerLeadId,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        //disable trigger to avoid automatic routing
        SPCaseTriggerHandler.enabled = false;
        //...
        insert newCustomerLeadCase;
        //enable trigger again
        SPCaseTriggerHandler.enabled = true;
        //...
        Set<ID> caseIdSet = new Set<ID>{newCustomerLeadCase.Id};
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        instance.routeNonHvcCasesFromIds(caseIdSet);
        Test.stopTest(); 
        newCustomerLeadCase = [select Id, Routed__c from Case where Id = :newCustomerLeadCase.Id];
        System.assertEquals(true, newCustomerLeadCase.Routed__c);
    }

    //
    /**
     * Case 1 - Language = 'es' (spanish), then... 
     *  Language_Corrected__c = Spanish
     *  Predicted_Language__c = Spanish
     * 
     * Case 1 - Language = null, then...
     *  Language_Corrected__c = English (default language)
     *  Predicted_Language__c = null
     */
    @isTest
    static void updateLanguageAndRoute() {
        String defaultLanguage = 
            ServiceLanguagesManager.getLanguage(ChatConst.DEFAULT_LANGUAGE);
        String spanishLanguage = 
            ServiceLanguagesManager.getLanguage(OmnichanelConst.SPANISH_LANG_CODE);
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        //...
        ID newCustomerLeadId = OmnichanelRoutingTestDataFactory.createNewCustomerLead();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        Case newCustomerLeadCase1 = new Case(
            Lead__c = newCustomerLeadId,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK,
            Language = OmnichanelConst.SPANISH_LANG_CODE,
            Language_Corrected__c = null,
            Predicted_Language__c = null
        );
        Case newCustomerLeadCase2 = new Case(
            Lead__c = newCustomerLeadId,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK,
            Language = null,
            Language_Corrected__c = null,
            Predicted_Language__c = null
        );
        List<Case> caseList = new List<Case>{newCustomerLeadCase1, newCustomerLeadCase2};
        //disable trigger to avoid automatic routing
        SPCaseTriggerHandler.enabled = false;
        //...
        insert caseList;
        //enable trigger again
        SPCaseTriggerHandler.enabled = true;
        //...
        Set<ID> caseIdSet = new Set<ID>{newCustomerLeadCase1.Id, newCustomerLeadCase2.Id};
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        instance.updateLanguageAndRoute(caseIdSet);
        Test.stopTest(); 
        newCustomerLeadCase1 = [
            select Id, Routed__c, Language_Corrected__c, Predicted_Language__c
            from Case 
            where Id = :newCustomerLeadCase1.Id];
        newCustomerLeadCase2 = [
            select Id, Routed__c, Language_Corrected__c, Predicted_Language__c
            from Case 
            where Id = :newCustomerLeadCase2.Id];
        //...
        System.assertEquals(
            true, 
            newCustomerLeadCase1.Routed__c, 
            'Invalid value'
        );
        System.assertEquals(
            true, 
            newCustomerLeadCase2.Routed__c, 
            'Invalid value'
        );
        System.assertEquals(
            spanishLanguage, 
            newCustomerLeadCase1.Language_Corrected__c,
            'Invalid value'
        );
        System.assertEquals(
            defaultLanguage, 
            newCustomerLeadCase2.Language_Corrected__c,
            'Invalid value'
        );
        System.assertEquals(
            spanishLanguage, 
            newCustomerLeadCase1.Predicted_Language__c,
            'Invalid value'
        );
        System.assertEquals(
            null, 
            newCustomerLeadCase2.Predicted_Language__c,
            'Invalid value'
        );
    }

    @isTest
    static void getEmailCasesRoutingConfig_test() {
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        QueueRoutingConfig result1 = instance.getEmailCasesRoutingConfig();
        QueueRoutingConfig result2 = instance.getEmailCasesRoutingConfig();
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assert(result1 == result2);
    }

    @isTest
    static void getNonEmailCasesRoutingConfig_test() {
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        QueueRoutingConfig result1 = instance.getNonEmailCasesRoutingConfig();
        QueueRoutingConfig result2 = instance.getNonEmailCasesRoutingConfig();
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assert(result1 == result2);
    }

    @isTest
    static void getOriginalCaseQueueIdSet_test() {
        Test.startTest();
        Set<ID> result = CaseRoutingManager.getOriginalCaseQueueIdSet();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    @isTest
    static void getSkillCasesQueueId_test() {
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        ID result1 = instance.getSkillCasesQueueId();
        ID result2 = instance.getSkillCasesQueueId();
        Test.stopTest();
        System.assert(result1 == result2);
    }

    @isTest
    static void getCaseServiceChanelId_test() {
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        ID result1 = instance.getCaseServiceChanelId();
        ID result2 = instance.getCaseServiceChanelId();
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assert(result1 == result2);
    }

    @isTest
    static void getNonHvcCaseServiceChanelId_test() {
        Test.startTest();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        ID result1 = instance.getNonHvcCaseServiceChanelId();
        ID result2 = instance.getNonHvcCaseServiceChanelId();
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assert(result1 == result2);
    }

    

}