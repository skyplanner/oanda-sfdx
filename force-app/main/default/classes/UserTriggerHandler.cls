/**
 * Class to handle the UserTrigger events and logic
 * @author:	Michel Carrillo (SkyPlanner)
 * @since:	02/04/2021
 * @version 1.0
 */
public with sharing class UserTriggerHandler {
    public UserTriggerHandler() {}

    /**
     * Handle the before insert event
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	02/04/2021
     * @params: pNew, trigger.New context variable
     * @return:	void
     */
    public void handleBeforeInsert(List<User> pNew){
        fillUpUserCreatedDate(pNew);
    }

    /**
     * Fill the User_Created_Date__c with the current Datetime
     * @author:	Michel Carrillo (SkyPlanner)
     * @since:	02/04/2021
     * @params: pUserList, The list to be filled up
     * @return:	void
     */
    private void fillUpUserCreatedDate(List<User> pUserList){
        for(User u : pUserList)
            u.User_Start_Date__c = System.now();
    }
}