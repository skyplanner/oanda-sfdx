/**
 * @File Name          : SK_MIFID_NID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/9/2020, 4:18:17 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   acantero     Initial Version
**/
@istest
private class SK_MIFID_NID_Validator_Test {

    @istest
    static void validate_test() {
        Test.startTest();
        SK_MIFID_NID_Validator validator = new SK_MIFID_NID_Validator();
        String result1 = validator.validate(null);
        String result2 = validator.validate('100');
        String result3 = validator.validate('1100');
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(System.Label.MifidValPasspFormatError, result2);
        System.assertEquals(null, result3);
    }

}