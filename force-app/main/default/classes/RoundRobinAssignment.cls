public with sharing class RoundRobinAssignment {

	static Map<ID, Round_Robin_Queue__c> queueByQueueId = new Map<ID, Round_Robin_Queue__c>();

    public static List<Id> getSortedUserIds(Id queueId) {
	   	Map<Id, Set<Id>> queueUserIdsById = UserUtil.getQueueUsers();
    	List<Id> userIds = new List<Id>(queueUserIdsById.get(queueId));
    	userIds.sort();
    	return userIds;
    }
    
   
    public static Id getNextOwnerId(Id queueId) {
     	
     	List<Id> userIds = getSortedUserIds(queueId);
     	
     	/*
    	String lastId = getIdOfLastUserLeadAssignedToForQueue(queueId);
    	
    	// No last id, can't do round robin
    	if (lastId != null) {
    	
	    	// Assume userIds are already sorted
	    	for (Integer i = 0; i < userIds.size(); i++) {
	    		if (userIds.get(i) + '' == lastId) {
	    			
	    			// Pick the next id in the list, wrapping where needed
	    			i = Math.mod((i + 1), userIds.size());
	    			
	    			// Remember which id we picked for next time
			    	setIdOfLastUserLeadAssignedToForQueue(queueId, userIds.get(i));

			    	System.debug('Round robin setting lead owner to ' + userIds.get(i));
			    	
	    			return userIds.get(i); 
	    		}
	    	}
    	}*/
    	
    	String nextId = getRoundRobinOwnerId(queueId, userIds);
    	
    	if(nextId != null){
    		return nextId;
    	}
    	
    	
    	System.debug('Did not find user for queue ' + queueId + ' in list, aborting round robin');
    	return getRandomOwnerId(queueId, userIds);
    }
    
    //Assume the user Ids are sorted
    public static Id getRandomOwnerId(Id queueId, List<Id> userIds ) {
    	
     	//List<Id> userIds = getSortedUserIds(queueId);

    	if (userIds.size() == 0) {
    		return null;
    	}
    	
    	Integer randomSelection = Math.round(Math.floor(Math.random() * userIds.size()));
    	
    	// Remember which id we picked for next time
    	setIdOfLastUserLeadAssignedToForQueue(queueId, userIds.get(randomSelection));
    	
    	System.debug('Randomly returning owner: ' + userIds.get(randomSelection));
    	
    	return userIds.get(randomSelection);
    }
    
    //Assume the user IDs are sorted
    public static Id getRoundRobinOwnerId(ID queueId, List<Id> userIds) {
        
        String lastId = getIdOfLastUserLeadAssignedToForQueue(queueId);
    	
    	// No last id, can't do round robin
    	if (lastId != null) {
    	
	    	// Assume userIds are already sorted
	    	for (Integer i = 0; i < userIds.size(); i++) {
	    		if (userIds.get(i) + '' == lastId) {
	    			
	    			// Pick the next id in the list, wrapping where needed
	    			i = Math.mod((i + 1), userIds.size());
	    			
	    			// Remember which id we picked for next time
			    	setIdOfLastUserLeadAssignedToForQueue(queueId, userIds.get(i));

			    	System.debug('Round robin setting lead owner to ' + userIds.get(i));
			    	
	    			return userIds.get(i); 
	    		}
	    	}
    	}
    	
    	return null;
       
    }
    
    public static void commitAssignments(){
    	
    	if(queueByQueueId.size() > 0){
    		
    		upsert queueByQueueId.values();
    	}
    }
    
    private static ID getIdOfLastUserLeadAssignedToForQueue(Id queueId){
        
        if(queueId == null){
            return null;
        }
    	
    	
    	Round_Robin_Queue__c qStatus = getRoundRobinSetting(queueId);
    	ID lastId = null;
    	
    	if(qStatus != null){
	    	
	    	lastId = qStatus.Last_Assigned_User_Id__c;
    	}
    	
    	return lastId;
    }
    
    //read the round robin setting from cache first.
    //If not exists, read the database
    private static Round_Robin_Queue__c getRoundRobinSetting(Id queueId){
    	
    	//read cache first
    	Round_Robin_Queue__c qStatus = RoundRobinAssignment.queueByQueueId.get(queueId);
    	
    	if(qStatus == null){
    		//read from custom setting
    		qStatus = CustomSettings.getLastUserLeadAssignedToForQueue(queueId);
    		
    		//put it into cache
    		if(qStatus != null){
    			queueByQueueId.put(queueId, qStatus);
    		}
    	}
    	
    	return qStatus; 
    }
    
    private static void setIdOfLastUserLeadAssignedToForQueue(Id queueId, Id userId){
        
        if(queueId == null){
            return;
        }
    	
    	//read the queue entry from the cache
    	//Round_Robin_Queue__c qStatus = RoundRobinAssignment.queueByQueueId.get(queueId);
    	Round_Robin_Queue__c qStatus = getRoundRobinSetting(queueId);
    	
    	if(qStatus == null){
    		//the entry is not in round robin setting, create a new entry
    		qStatus =  new Round_Robin_Queue__c();
    		qStatus.name = queueId;
    		qStatus.Last_Assigned_User_Id__c = userId;
    		
    		RoundRobinAssignment.queueByQueueId.put(queueId, qStatus);
    	}else{
    		
    		qStatus.Last_Assigned_User_Id__c = userId;
    	}
    }
    
    // return the next user, after the current user.  Note, this one does not update the last user custom setting,
    // as technically we are not round robinning, we are assigning to the next person in the queue
    public static Id getNextOwnerIdAfterCurrentOwnerId(Id queueId, Id lastId) {
    	List<Id> userIds;
    	
    	if(lastId!=null) {
	    	userIds = getSortedUserIds(queueId);
	     	
	    	// Assume userIds are already sorted
	    	for (Integer i = 0; i < userIds.size(); i++) {
	    		if (userIds.get(i) + '' == lastId) {
	    			
	    			// Pick the next id in the list, wrapping where needed
	    			i = Math.mod((i + 1), userIds.size());
	    			
			    	System.debug('Round robin setting lead owner to ' + userIds.get(i));
			    	
	    			return userIds.get(i); 
	    		}
	    	}
	    	// no user, or couldn't find user in that queue
	    	return getNextOwnerId(queueId);
    	}
    	
    	System.debug('Did not find user ' + lastId + ' in list, aborting round robin');
    	return getRandomOwnerId(queueId, userIds);
    }
}