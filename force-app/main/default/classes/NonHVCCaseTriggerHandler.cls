/**
 * @File Name          : NonHVCCaseTriggerHandler.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 1:05:56 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/25/2021, 2:32:27 PM   acantero     Initial Version
**/
public without sharing class NonHVCCaseTriggerHandler {

    public static final String OBJ_API_NAME = 'Non_HVC_Case__c';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //********************************************************

    final List<Non_HVC_Case__c> newList;
    final Map<ID,Non_HVC_Case__c> oldMap;

    public NonHVCCaseTriggerHandler(
        List<Non_HVC_Case__c> newList,
        Map<ID,Non_HVC_Case__c> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    //********************************************************

    public void handleAfterUpdate() {
        OnNonHVCCaseStatusChangesCmd onStatusChangesCmd = 
            new OnNonHVCCaseStatusChangesCmd();
        for(Non_HVC_Case__c newRec : newList) {
            Non_HVC_Case__c oldRec = oldMap.get(newRec.Id);
            onStatusChangesCmd.checkRecord(newRec, oldRec);
        }
        onStatusChangesCmd.execute();
    }

    public void handleAfterDelete() {
        OnNonHVCCaseDeletedCmd onDeletedCmd = 
            new OnNonHVCCaseDeletedCmd();
        List<Non_HVC_Case__c> oldList = oldMap.values();
        for(Non_HVC_Case__c oldRec : oldList) {
            onDeletedCmd.checkRecord(oldRec);
        }
        onDeletedCmd.execute();
    }

}