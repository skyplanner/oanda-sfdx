/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoCloseAllAccountController {
    @InvocableMethod 
    public static List<String> closeAllAccount(List<Id> accounts) {
        if (accounts == null || accounts.isEmpty()) {
            return new List<String>();
        }

        Id cryptoRecordTypeId = RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID;

        List<Id> fxaccounts = new List<Id>();
        Map<Id, Note> successNoteMap = new Map<Id, Note>();
        Map<Id, Note> failNoteMap = new Map<Id, Note>();

        for (fxAccount__c fxa : [
            SELECT Id
            FROM fxAccount__c
            WHERE Account__c =: accounts[0] 
                AND RecordTypeId =: cryptoRecordTypeId
        ]) {
            fxaccounts.add(fxa.Id);
        }

        System.debug('fxAccounts to Close (' + fxaccounts.size() + '): ' + fxaccounts);

        List<String> result = CryptoAccountCloseController.closeAccount(fxAccounts, true);

        System.debug('result from Close fxAccounts (' + result.size() + '): ' + result);

        for (String r : result) {
            if (r.contains('could not be')) {
                return result;
            }
        }

        return new List<String> { 'All the Crypto Accounts have been closed successfully' };
    }
}