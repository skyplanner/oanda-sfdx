/**
 * @File Name          : GetGeneralSettingAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/5/2024, 2:25:58 AM
**/
public without sharing class GetGeneralSettingAction {

	@InvocableMethod(label='Get General Setting' callout=false)
	public static List<String> getGeneralSetting(List<String> settingNames) { 
		try {
			ExceptionTestUtil.execute();
			List<String> result = 
				new List<String> { null };

			if (
				(settingNames == null) ||
				settingNames.isEmpty()
			) {
				return result;
			}
			// else...        
			result[0] = SPSettingsManager.getSetting(settingNames[0]);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetGeneralSettingAction.class.getName(),
				ex
			);
		}
	} 
	
}