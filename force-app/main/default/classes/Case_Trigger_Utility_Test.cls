/**
 * @File Name          : Case_Trigger_Utility_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/14/2020, 9:21:15 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/11/2019   dmorales     Initial Version
**/
@isTest
private class Case_Trigger_Utility_Test {
    
    private static Id createCase(Boolean createCode, Boolean useManualRisk){
        Account a = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        if (useManualRisk) {
            a.Manual_Adjusted_Customer_Risk_Assessment__c  = 'High';
        }
        insert a;
       
        String fxAccountRecTypeId = 
            Schema.SObjectType.fxAccount__c
                .getRecordTypeInfosByDeveloperName()
                .get('Retail_Live')
                .getRecordTypeId();
        fxAccount__c fx = new fxAccount__c(
            RecordTypeId = fxAccountRecTypeId,
            Type__c = 'Individual'
        ); 
        fx.Account__c = a.Id;
        fx.Division_Name__c = Case_Trigger_Utility.OANDA_EUROPE;
        fx.Employment_Status__c = 'Retired';
        fx.Industry_of_Employment__c = 'Agricultural';
        fx.Citizenship_Nationality__c = 'France';
        fx.CRA_Val__c = 40;
        if (!useManualRisk) {
            fx.Customer_Risk_Assessment_Text__c  = 'High';
        }
        
        insert fx;
      
        a.fxAccount__c = fx.Id;
        update a;
        
        if(createCode){
             List<Verification_Code_Setting__c> listS = new List<Verification_Code_Setting__c> ();
             listS.add(new Verification_Code_Setting__c(Name = '012365', Order__c = 1 ));
             Database.insert(listS);
        }
       
        String onboardingRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(Case_Trigger_Utility.CASE_ONBOARDING_RT)
            .getRecordTypeId();
        Case c = new Case(
            Subject = 'Test', 
            AccountId = a.Id, 
            RecordTypeId = onboardingRecTypeId
        );
        insert c;
         
        return c.Id;        
    } 
    
    private static Id createFailCase(FailInfo info) {
        String risk = (info.failRisk)
            ? 'Low'
            : 'High';
        String region = (info.failRegion)
            ? 'OANDA Canada'
            : 'OANDA Europe';
        Account a = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        if (info.useManualRisk) {
            a.Manual_Adjusted_Customer_Risk_Assessment__c = risk;
        }
        insert a;
        
        String fxAccountRecTypeDevName = (info.failAccountLive) 
            ? 'Retail_Practice'
            : 'Retail_Live';
        String fxAccountRecTypeId = 
            Schema.SObjectType.fxAccount__c
                .getRecordTypeInfosByDeveloperName()
                .get(fxAccountRecTypeDevName)
                .getRecordTypeId();
        String fxAccountType = (info.failAccountIndividual)
            ? 'Corporate'
            : 'Individual';
        fxAccount__c fx = new fxAccount__c(
            RecordTypeId = fxAccountRecTypeId
        ); 
        fx.Account__c = a.Id;
        fx.Type__c = fxAccountType;
        fx.Employment_Status__c = 'Retired';
        fx.Industry_of_Employment__c = 'Agricultural';
        fx.Citizenship_Nationality__c = 'France';
        fx.CRA_Val__c = 40;
        if (!info.useManualRisk) {
            fx.Customer_Risk_Assessment_Text__c = risk;    
        }
        fx.Customer_Risk_Assessment_Text__c = (info.failRisk)
            ? 'Low'
            : 'High';
        insert fx;
      
        a.fxAccount__c = fx.Id;
        update a;
        
        List<Verification_Code_Setting__c> listS = new List<Verification_Code_Setting__c> ();
        listS.add(new Verification_Code_Setting__c(Name = '012365', Order__c = 1 ));
        Database.insert(listS);      
       
        String recordTypeDevName = (info.failRecordType)
            ? 'Support'
            : 'Registration';
        String caseRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(recordTypeDevName)
            .getRecordTypeId();
        
        Case c = new Case(
            Subject = 'Test',
            RecordTypeId = caseRecTypeId
        );
        if(!info.failAccount)
          c.AccountId = a.Id;
        
        insert c;         
        return c.Id;        
    }

    @isTest
    static void testHandleBeforeInsertUpdateAfterDelete(){
          Id cId = createCase(true, false);
          
          List<Case> c = [SELECT Verification_Code__c, Verification_Code_Order__c, Subject 
                          FROM Case WHERE Id =: cId  ];
                    
          System.assertEquals('012365', c[0].Verification_Code__c);
          System.assertEquals(1, c[0].Verification_Code_Order__c);

          /**Code must be deleted from Verification_Code_Setting__c */
          Integer count = [SELECT Count() FROM Verification_Code_Setting__c WHERE Name = '012365'];
          System.assert(count == 0);
         
          /** Update case, VerificationCode must be the same */
          c[0].Subject = 'Test Change';
          update c;

           List<Case> c1 = [SELECT Verification_Code__c, Verification_Code_Order__c
                          FROM Case WHERE Id =: cId  ];
          System.assert(c1[0].Verification_Code__c == '012365');
          System.assert(c1[0].Verification_Code_Order__c == 1); 
          
          /**Delete case*/
          delete c;

          List<Case> c2 = [SELECT Verification_Code__c, Verification_Code_Order__c 
                          FROM Case WHERE Id =: cId  ];

          System.assert(c2.size() == 0); 
     }

     @isTest
    static void test2HandleBeforeInsert(){
          Id cId = createCase(true, true);
          
          List<Case> c = [SELECT Verification_Code__c, Verification_Code_Order__c, Subject 
                          FROM Case WHERE Id =: cId  ];
                    
          System.assert(c[0].Verification_Code__c == '012365');
          System.assert(c[0].Verification_Code_Order__c == 1);

          /**Code must be deleted from Verification_Code_Setting__c */
          Integer count = [SELECT Count() FROM Verification_Code_Setting__c WHERE Name = '012365'];
          System.assert(count == 0);
     }
     
    @isTest
    static void testHandleBeforeInserNoCodeAvailable(){
         try{
            Id cId = createCase(false, false);
         }
         catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('No Verification Code available') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
         }     
         
    }  
    
    @isTest
    static void testFailRecordType(){
       FailInfo info = new FailInfo();
       info.failRecordType = true;
       info.useManualRisk = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    }
    
    @isTest
    static void testFailAccount(){
       FailInfo info = new FailInfo();
       info.failAccount = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    } 
    
    @isTest
    static void testFailRegion(){
       FailInfo info = new FailInfo();
       info.failRegion = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    } 
    
    @isTest
    static void testFailRisk1(){
       FailInfo info = new FailInfo();
       info.failRisk = true;
       info.useManualRisk = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    }

    @isTest
    static void testFailRisk2(){
       FailInfo info = new FailInfo();
       info.failRisk = true;
       //useManualRisk = false
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    }

    @isTest
    static void testFailAccountLive(){
       FailInfo info = new FailInfo();
       info.failAccountLive = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
                    
       System.assert(String.isBlank(c[0].Verification_Code__c));
    }

    @isTest
    static void testFailAccountIndividual(){
       FailInfo info = new FailInfo();
       info.failAccountIndividual = true;
       Id cId = createFailCase(info); 
       List<Case> c = [SELECT Verification_Code__c
                       FROM Case WHERE Id =: cId  ];
       System.assert(String.isBlank(c[0].Verification_Code__c));
    }

    @isTest
    static void fillFirstOwner(){

        User tUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            UserName = 'testusername01@testemail.com');
        insert tUser;

        Account a = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Case c;
        Test.startTest();
            System.runAs(tUser){
                c = new Case(
                    Subject = 'Test', 
                    AccountId = a.Id,
                    OwnerId = tUser.Id
                );
                insert c;
            }
        test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assertEquals(
            tUser.Id,
            c.First_Owner__c,
            'The owner is wasn\'t filled properly');
    }

    @isTest
    static void fillFirstOwner2(){

        User tUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            UserName = 'testusername01@testemail.com');
        insert tUser;

        Account a = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Group q = new Group(
            Name = 'TestQueue',
            Type = 'Queue'
        );
        insert q;

        System.runAs(new User(Id=UserInfo.getUserId())){
            QueuesObject testQueue = new QueueSObject(
                QueueID = q.id,
                SObjectType = 'Case');
            insert testQueue;
        }

        Case c;
        Test.startTest();
            System.runAs(tUser){
                c = new Case(
                    Subject = 'Test', 
                    AccountId = a.Id,
                    OwnerId = q.Id
                );
                insert c;
            }
        test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assert(
            c.First_Owner__c == null,
            'The owner must be empty');

        c.OwnerId = tUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assertEquals(
            tUser.Id,
            c.First_Owner__c,
            'The owner is wasn\'t filled properly');
    }

    @isTest
    static void fillFirstOwner3(){

        User tUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            UserName = 'testusername01@testemail.com');
        insert tUser;

        String userName = 'system.user@oanda.com' + 
            UserInfo.getUserName().substringAfterLast('.com');

        User systemUser = [SELECT Id, Username FROM User WHERE Username =: userName];

        Account a = new Account(
            FirstName = 'First1', 
            LastName='Last1', 
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Group q = new Group(
            Name = 'TestQueue',
            Type = 'Queue'
        );
        insert q;

        System.runAs(new User(Id=UserInfo.getUserId())){
            QueuesObject testQueue = new QueueSObject(
                QueueID = q.id,
                SObjectType = 'Case');
            insert testQueue;
        }

        Case c;
        Test.startTest();
            System.runAs(tUser){
                c = new Case(
                    Subject = 'Test', 
                    AccountId = a.Id,
                    OwnerId = q.Id
                );
                insert c;
            }
        test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assert(
            c.First_Owner__c == null,
            'The owner must be empty');

        c.OwnerId = systemUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assert(
            c.First_Owner__c == null,
            'The owner must be empty');

        c.OwnerId = tUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id =: c.Id];
        System.assertEquals(
            tUser.Id,
            c.First_Owner__c,
            'The owner is wasn\'t filled properly');
    }

	/**
	 * public static void performConditionalUpdates(
	 *		List<Case> newCases,
	 *		List<Case> oldCases)
	 */
	@isTest
	static void performConditionalUpdates() {
		User tUser1, tUser2;
		Account a;
		Case c;
		
		tUser1 = new User(
			Alias = 'tuser1', 
			Email = 'tuser1@testemail.com',
			LastName = 'Testing', 
			EmailEncodingKey = 'UTF-8', 
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US', 
			TimeZoneSidKey = 'America/Los_Angeles',
			ProfileId = UserInfo.getProfileId(),
			UserName = 'testusername01@testemail.com'
		);
		insert tUser1;

		tUser2 = new User(
			Alias = 'tuser2', 
			Email = 'tuser2@testemail.com',
			LastName = 'Testing', 
			EmailEncodingKey = 'UTF-8', 
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US', 
			TimeZoneSidKey = 'America/Los_Angeles',
			ProfileId = UserInfo.getProfileId(),
			UserName = 'testusername02@testemail.com'
		);
		insert tUser2;

		a = new Account(
			FirstName = 'First1', 
			LastName='Last1', 
			PersonEmail = 'test1@oanda.com',
			Account_Status__c = 'Active'
		);
		insert a;

		c = new Case(
			Subject = 'Test', 
			AccountId = a.Id,
			OwnerId = tUser1.Id,
			Complaint__c = false
		);
		insert c;

		c = [
			SELECT Id,
				QI_Case_Owner__c
			FROM Case
			WHERE Id =: c.Id
		];

		System.assertEquals(
			tUser1.Id,
			c.QI_Case_Owner__c,
			'The Non-complaint case\'s QI Case owner wasn\'t filled properly');

		c.OwnerId = tUser2.Id;
		c.Complaint__c = true;
		c.QI_Case_Owner__c = null;
		update c;

		c = [
			SELECT Id,
				OwnerId,
				First_Owner__c,
				QI_Case_Owner__c
			FROM Case
			WHERE Id =: c.Id
		];

		System.assertEquals(
			tUser2.Id,
			c.OwnerId,
			'The Owner wasn\'t filled properly');

		System.assertEquals(
			c.First_Owner__c,
			c.QI_Case_Owner__c,
			'The Complaint case\'s QI Case owner wasn\'t filled properly');
	}

    class FailInfo {
        
        Boolean failRecordType = false; 
        Boolean failAccount = false; 
        Boolean failRegion = false; 
        Boolean failRisk = false;
        Boolean useManualRisk = false;
        Boolean failAccountLive = false;
        Boolean failAccountIndividual = false;

    }
}