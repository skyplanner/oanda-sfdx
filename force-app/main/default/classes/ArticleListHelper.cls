/**
 * Article list helper class
 * @author       Ariel Cantero
 * @since        06/11/2019
 */
public without sharing class ArticleListHelper {

	// Columns used to filter articles by the search criteria
	private static final List<String> SEARCH_COLUMNS = new List<String> {
		'Title'
	};
	// Articles columns displayed
	private static final List<String> VIEW_COLUMNS = new List<String> {
		'Id', 'KnowledgeArticleId', 'Title', 'CreatedDate', 'LastModifiedDate', 
		'LastPublishedDate', 'PublishStatus', 'PreviewDocument__c'
	};
	
	private static final String XAND = ' AND ';
	private static final String XOR = ' OR ';

	private String searchText;
	private String status;
	private String category;
	private String audience;
	private String division;
	private Boolean hasPreviewDoc;
	private List<String> excludeArtIds;
	private Boolean restrictToVisibleInPkb;

	private String likeClause;
	private Integer pageSize;
	private Integer offset;
	private String sortBy;
	private Boolean isDescending;

	
	public ArticleListHelper(ArticleListFilterCriteria criterias) {
		this.searchText = criterias.searchText;
		this.status = criterias.status;
		this.category = criterias.category;
		this.audience = criterias.audience;
		this.division = criterias.division;
		this.hasPreviewDoc = criterias.hasPreviewDoc;
		this.restrictToVisibleInPkb = criterias.restrictToVisibleInPkb;
		if (String.isNotBlank(criterias.excludeArtIds)) {
			excludeArtIds = criterias.excludeArtIds.split(',');
		}
	}

	public ArticleListHelper(
				ArticleListFilterCriteria criterias, Integer pageSize,
				Integer offset, String sortBy, Boolean isDescending) {
		this(criterias);
		this.pageSize = pageSize;
		this.offset = offset;
		this.sortBy = sortBy;
		this.isDescending = isDescending;
	}
	

	public Integer count() {
		Integer result = Database.countQuery(getCountQuery());
		return result;
	}

	public List<FAQ__kav> search() {
		List<FAQ__kav> articles = Database.query(getRecordsQuery());
		return articles;
	}

	private String getCountQuery() {
		String result = getBaseQuery().replace('[FIELDS]', ' COUNT() ');
		System.debug('getCountQuery -> ' + result);
		return result;
	}

	@TestVisible
	private String getBaseQuery() {
		likeClause = String.isBlank(searchText) ? null : '%' + searchText + '%';
		String hasPreviewDocFilter = (hasPreviewDoc == true) 
			? ' AND PreviewDocument__c <> null ' 
			: '';
		String restrictToVisibleInPkbFilter = (restrictToVisibleInPkb == true) 
			? ' AND IsVisibleInPkb = true ' 
			: '';
		String excludeArtIdsFilter = ((excludeArtIds != null) && (excludeArtIds.size() > 0))
			? ' AND Id NOT IN :excludeArtIds '
			: '';
		String q =  
			' SELECT [FIELDS] ' +				
			' FROM FAQ__kav ' +
			' WHERE PublishStatus = :status ' + 
			hasPreviewDocFilter +
			restrictToVisibleInPkbFilter +
			excludeArtIdsFilter + 
			((likeClause == null) ? '' : getLikeCriteria()) + 
			getDataCategoriesCriteria();
		return q;
	}

	@TestVisible
	private String getDataCategoriesCriteria() {
		System.debug('getDataCategoriesCriteria -> category: ' + category);
		Map<String,String> dataCatFilterMap = new Map<String,String>();
		if (String.isNotBlank(category)) {
			dataCatFilterMap.put('Categories', category);
		}
		if (String.isNotBlank(audience)) {
			dataCatFilterMap.put('Audience', audience);
		}
		if (String.isNotBlank(division)) {
			dataCatFilterMap.put('Division', division);
		}
		System.debug('getDataCategoriesCriteria -> dataCatFilterMap.size: ' + dataCatFilterMap.size());
		return (dataCatFilterMap.isEmpty()) ? '' : getDataCategoriesCriteria(dataCatFilterMap);
	}

	@TestVisible
	private String getDataCategoriesCriteria(Map<String,String> dataCatFilterMap) {
		Boolean firstCrit = true;
		String result = ' WITH DATA CATEGORY ';
		Set<String> keys = dataCatFilterMap.keySet();
		for(String key : keys) {
			if (!firstCrit) {
				result += ' AND ';
			} else {
				firstCrit = false;
			}
			String value = dataCatFilterMap.get(key);
			if (value.contains(',')) {
				String[] values = value.split(',');
				String mutipleValue = '(';
				for(Integer i = 0; i < values.size(); i++) {
					if (i > 0) {
						mutipleValue += ',';
					}
					mutipleValue += (values[i] + '__c');
				}
				mutipleValue += ')';
				result += (' ' + key + '__c' + ' ' + 'AT' + ' ' + mutipleValue + ' ');
			} else {
				result += (' ' + key + '__c' + ' ' + 'BELOW' + ' ' + value + '__c ');
			}
			
		}
		System.debug('getDataCategoriesCriteria -> ' + result);
		return result;
	}

	@TestVisible
	private String getLikeCriteria() {
		List<String> ors = new List<String>();
		for(String field :SEARCH_COLUMNS)
			ors.add(field + ' LIKE :likeClause ');
		return ors.isEmpty() ? '' :
		' AND (' + String.join(ors, XOR) + ') ';
	}

	@TestVisible
	private String getRecordsQuery() {
		String q = getBaseQuery().replace('[FIELDS]', getColumns()) + getLastQueryPart();
		System.debug('getRecordsQuery -> ' + q);
		return q;
	}

	@TestVisible
	private String getLastQueryPart() {
		String result = (' ORDER BY ' + String.escapeSingleQuotes(sortBy) +
			(isDescending == true ? ' DESC ' : ' ASC ') +
			' LIMIT ' + pageSize + ' OFFSET ' + offset);
		return result;
	}

	/**
	 * Build "Select" query part
	 */
	private String getColumns() {
		return String.join(VIEW_COLUMNS, ',');
	}

}