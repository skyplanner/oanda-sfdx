/**
 * Wrapper for Comply advantage JSON: Comments
 * @author Fernando Gomez
 */
public class ComplyAdvantageCommentsResult
		extends ComplyAdvantageResponseBase {
	public List<ComplyAdvantageSearchComment> content { get; set; }

	/**
	 * @param jsonString representation
	 * @return an instance of this class
	 */
	public static ComplyAdvantageCommentsResult parse(String jsonString) {
		return (ComplyAdvantageCommentsResult)JSON.deserialize(
			jsonString, ComplyAdvantageCommentsResult.class);
	}
}