/**
 * @File Name          : SPDataInitManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/13/2023, 4:57:18 PM
**/
@IsTest
private without sharing class SPDataInitManagerTest {

	public static final String DUMMY_OBJ_1 = 'DUMMY_OBJ_1';
	public static final String OPTION_A = 'OPTION_A';

	// test
	// - getObjectKey
	// - getUniqueIndex
	// - setFieldValue
	// - setFieldValues
	// - insertObj
	// - storeData
	// - loadData
	// - getObjectId
	@IsTest
	static void test1() {
		String dummyName1 = SPDataInitManager.DUMMY_PREFIX + 1;
		String dummyName2 = SPDataInitManager.DUMMY_PREFIX + 2;
		String dummyKey1 = 'Dummy Key 1';
		String dummyKey2 = 'Dummy Key 2';
		String dummyValue1 = 'Dummy Value 1';
		String dummyValue2 = 'Dummy Value 2';

		Test.startTest();
		SPDataInitManager initManager = new SPDataInitManager();
		SPDataInitProxy initProxy = initManager.getProxy(
			DUMMY_OBJ_1, 
			Schema.SObjectType.SP_Internal_Data__c
		);
		String objectKey = initProxy.getObjectKey();
		SP_Internal_Data__c dummyRec = new SP_Internal_Data__c(
			Name = dummyName1,
			Key__c = dummyKey1,
			Value__c = dummyValue1
		);

		Boolean optionA1 = (Boolean) initProxy.getObjectOption(OPTION_A, false);
		initProxy.setObjectOption(OPTION_A, true);
		Boolean optionA2 = (Boolean) initProxy.getObjectOption(OPTION_A, false);

		initProxy.setFieldValue(SP_Internal_Data__c.Name, dummyName2);
		initProxy.setFieldValues(new Map<String,Object> {
			'Key__c' => dummyKey2,
			'Value__c' => dummyValue2
		});
		initProxy.insertObj(dummyRec);
		Integer beforeIndex = initProxy.getUniqueIndex();
		initManager.storeData();
		initManager = SPDataInitManager.reload();
		Integer afterIndex = initManager.getUniqueIndex(
			Schema.SObjectType.SP_Internal_Data__c
		);
		ID dummyRecId = initManager.getObjectId(DUMMY_OBJ_1, true);
		Test.stopTest();

		dummyRec = SP_Internal_Data__c.getValues(dummyName2);
		System.assertEquals(DUMMY_OBJ_1, objectKey, 'Invalid value');
		System.assertEquals(dummyRec.Id, dummyRecId, 'Invalid value');
		System.assertEquals(1, beforeIndex, 'Invalid value');
		System.assertEquals(2, afterIndex, 'Invalid value');
		System.assertEquals(dummyKey2, dummyRec.Key__c, 'Invalid value');
		System.assertEquals(dummyValue2, dummyRec.Value__c, 'Invalid value');
		System.assertEquals(false, optionA1, 'Invalid value');
		System.assertEquals(true, optionA2, 'Invalid value');
	}    

	@IsTest
	static void getUniqueName() {
		Test.startTest();
		SPDataInitManager initManager = new SPDataInitManager();
		SPDataInitProxy initProxy = initManager.getProxy(
			DUMMY_OBJ_1, 
			Schema.SObjectType.SP_Internal_Data__c
		);
		Set<String> names = new Set<String>();
		for (Integer i = 1; i <= 10; i++) {
			String newName = initProxy.getUniqueName();
			Boolean isNew = names.add(newName);
			System.assert(isNew, 'All names should be different');
		}
		Test.stopTest();
	}

	// required = true => throw an exception
	@IsTest
	static void getObjectId() {
		Boolean error = false;
		Test.startTest();
		// test 1
		SPDataInitManager instance = new SPDataInitManager();

		try {
			ID result2 = instance.getObjectId(DUMMY_OBJ_1, true);

		} catch(SPDataInitException ex) {
			error = true;
		}
		Test.stopTest();
		System.assertEquals(true, error, 'An exception should have been thrown');
	}    

	// test
	// - setRecordTypeId
	// - getRecordTypeId
	@IsTest
	static void setRecordTypeId() {
		String masterRecTypeDevName = 'Master';
		Test.startTest();
		SPDataInitManager initManager = new SPDataInitManager();
		SPDataInitProxy initProxy = initManager.getProxy(
			DUMMY_OBJ_1, 
			Schema.SObjectType.Account
		);
		ID masterRecTypeId = initProxy.getRecordTypeId(masterRecTypeDevName);
		initProxy.setRecordTypeId(masterRecTypeDevName);
		Object recTypeIdValue = initManager.getFieldValue(
			DUMMY_OBJ_1, // objKey
			SPDataInitManager.RECORD_TYPE_ID, // fieldApiName
			false // required
		);
		Test.stopTest();
		System.assertNotEquals(
			null, 
			recTypeIdValue, 
			'RecordTypeId filed should have a value'
		);
		System.assertEquals(masterRecTypeId, recTypeIdValue, 'Must be equal');
	}

	// test 
	// - getFieldValue
	// field value does not exists => return null)
	//
	// - checkRequiredFieldValue 
	// field value does not exists => throw an exception)
	@IsTest
	static void checkRequiredFieldValue() {
		Boolean error = false;
		Test.startTest();
		// test 1
		SPDataInitManager initManager = new SPDataInitManager();
		SPDataInitProxy initProxy = initManager.getProxy(
			DUMMY_OBJ_1, 
			Schema.SObjectType.SP_Internal_Data__c
		);
		Object fieldValue = 
			initProxy.getFieldValue(SP_Internal_Data__c.Name, false);

		try {
			initProxy.checkRequiredFieldValue(SP_Internal_Data__c.Name);

		} catch(SPDataInitException ex) {
			error = true;
		}
		Test.stopTest();
		System.assertEquals(null, fieldValue, 'Should be null');
		System.assertEquals(true, error, 'An exception should have been thrown');
	}    
	
}