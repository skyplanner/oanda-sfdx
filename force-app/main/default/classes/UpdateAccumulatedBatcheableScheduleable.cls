global with sharing class UpdateAccumulatedBatcheableScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	Datetime startTime;
	Integer numberOfBatches = 0;
	Integer numberOfRecords = 0;
	
	UpdateAccumulatedBatcheableScheduleable() {
		startTime = Datetime.now();
		query = 'SELECT fxAccount__c, Trade_Volume_USD_Accumulated__c, Current_Trade_Volume_USD_MTD__c, OANDA_P_L_USD_Accumulated__c, Current_OANDA_P_L_USD_MTD__c, P_L_USD_Accumulated__c, Current_P_L_USD_MTD__c, Change_Trade_Volume_USD__c, Change_OANDA_P_L_USD__c, Change_P_L_USD__c FROM Opportunity WHERE Is_Accumulating__c=true';
	}	
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT fxAccount__c, Trade_Volume_USD_Accumulated__c, Current_Trade_Volume_USD_MTD__c, OANDA_P_L_USD_Accumulated__c, Current_OANDA_P_L_USD_MTD__c, P_L_USD_Accumulated__c, Current_P_L_USD_MTD__c, Change_Trade_Volume_USD__c, Change_OANDA_P_L_USD__c, Change_P_L_USD__c FROM Opportunity WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	// Batchable interface
	global Iterable<SObject> start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<SObject> sobjects) {
		numberOfBatches++;
		numberOfRecords += sobjects.size();
		
		List<Opportunity> opps = sobjects;
		for(Opportunity o : opps) {
			if(o.Trade_Volume_USD_Accumulated__c==null) {
				o.Trade_Volume_USD_Accumulated__c=0;
			}
			if(o.OANDA_P_L_USD_Accumulated__c==null) {
				o.OANDA_P_L_USD_Accumulated__c=0;
			}
			if(o.P_L_USD_Accumulated__c==null) {
				o.P_L_USD_Accumulated__c=0;
			}
			
			o.Starting_Trade_Volume_USD_MTD__c = 0;
			o.Starting_OANDA_P_L_USD_MTD__c = 0;
			o.Starting_P_L_USD_MTD__c = 0;
			
			o.Trade_Volume_USD_Accumulated__c = (o.Change_Trade_Volume_USD__c==null ? 0 : o.Change_Trade_Volume_USD__c);
			o.OANDA_P_L_USD_Accumulated__c = (o.Change_OANDA_P_L_USD__c==null ? 0 : o.Change_OANDA_P_L_USD__c);
			o.P_L_USD_Accumulated__c = (o.Change_P_L_USD__c==null ? 0 : o.Change_P_L_USD__c);
		}
		
		update opps;
	} 

	global void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		String subject = 'UpdateAccumulatedBatcheableScheduleable completed';
		String body = 'Job started: ' + startTime;
		body += '\nJob ended: ' + Datetime.now();
		body += '\nNumber of batches: ' + numberOfBatches;
		body += '\nNumber of records: ' + numberOfRecords;
		
		EmailUtil.sendEmail('ahwang@oanda.com', subject, body);
		ResetFxAccountMtdBatchable.executeBatch();
	}
	
	// Scheduleable interface
	global void execute(SchedulableContext sc) {
		UpdateAccumulatedBatcheableScheduleable.executeBatch();
   }   
	
	// static methods
	
	// UpdateAccumulatedBatcheableScheduleable.executeBatch();
	public static Id executeBatch() {
		return Database.executeBatch(new UpdateAccumulatedBatcheableScheduleable());
	}
	
	// UpdateAccumulatedBatcheableScheduleable.scheduleEndOfMonth();
	public static Id scheduleEndOfMonth() {
		UpdateAccumulatedBatcheableScheduleable job = new UpdateAccumulatedBatcheableScheduleable();
		
		// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		//            s m hh d m w
		// Last day of every month, at 11pm
		// TODO: check time that other batch jobs finish
		String sch = '0 0 22 L * ?';
		return system.schedule('UpdateAccumulatedBatcheableScheduleable', sch, job);
	}
	
	// UpdateAccumulatedBatcheableScheduleable.scheduleEndOfDay();
	public static Id scheduleEndOfDay() {
		UpdateAccumulatedBatcheableScheduleable job = new UpdateAccumulatedBatcheableScheduleable();
		
		// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		//            s m hh d m w
		// Last day of every month, at 11pm
		// TODO: check time that other batch jobs finish
		String sch = '0 0 22 * * ?';
		return system.schedule('UpdateAccumulatedBatcheableScheduleable Daily', sch, job);
	}
}