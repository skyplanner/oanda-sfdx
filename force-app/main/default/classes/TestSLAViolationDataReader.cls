/**
 * @File Name          : TestSLAViolationDataReader.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 10:12:01 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/4/2024, 10:11:12 AM   aniubo     Initial Version
**/
@isTest
public class TestSLAViolationDataReader implements SLAViolationDataReader {
	Boolean readLead;
	public TestSLAViolationDataReader(Boolean readLead) {
		this.readLead = readLead;
	}

	public List<SObject> getData(
		SLAViolationNotificationData notificationData
	) {
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Id leadId = initManager.getObjectId(ServiceTestDataKeys.LEAD_1, true);

		MessagingSession session = new MessagingSession();
		if (!readLead) {
			MessagingEndUser endUser = new MessagingEndUser(
				AccountId = accountId
			);
			endUser.Account = getAccount(accountId);
			session.MessagingEndUser = endUser;
		}

		session.Case = getCase(caseId);
		if (readLead) {
			session.Lead = getLead(leadId);
		}
		session.Tier__c = OmnichannelCustomerConst.TIER_1;
		session.Language_Code__c = 'en';
		session.Inquiry_Nature__c = 'Inquiry';
		session.OwnerId = UserInfo.getUserId();
		session.Live_or_Practice__c = 'Live';
		session.AgentType = 'Agent';
		List<MessagingSession> sessions = new List<MessagingSession>{ session };
		return sessions;
	}

	public static Case getCase(Id caseId) {
		List<Case> cases = [
			SELECT
				Id,
				Subject,
				Division__c,
				Chat_Division__c,
				Chat_Language_Preference__c,
				Language_Preference__c
			FROM Case
			WHERE Id = :caseId
		];
		return cases.isEmpty() ? null : cases.get(0);
	}

	public static Lead getLead(Id leadId) {
		List<Lead> leads = [
			SELECT Id, Division_Name__c, Primary_Division_Name__c, Name
			FROM Lead
			WHERE Id = :leadId
		];
		return leads.isEmpty() ? null : leads.get(0);
	}

	public static Account getAccount(Id accountId) {
		List<Account> accounts = [
			SELECT Id, Name, Division_Name__c, Primary_Division_Name__c
			FROM Account
			WHERE Id = :accountId
		];
		return accounts.isEmpty() ? null : accounts.get(0);
	}
}