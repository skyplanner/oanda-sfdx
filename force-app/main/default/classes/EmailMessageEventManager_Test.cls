/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-22-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class EmailMessageEventManager_Test {
	@testSetup
	private static void testSetup() {
		Account acc = ScheduledEmailTestUtil.createAccount('Test Name');
		Contact ctc = ScheduledEmailTestUtil.createContact(
			acc.Id,
			'Test LastName',
			'me@test.com'
		);
		ScheduledEmailTestUtil.createDraftEmail(
			ScheduledEmailTestUtil.createCase(ctc).Id
		);
	}
	@isTest
	private static void publish() {
		// Test data setup
		EmailMessage message = EmailMessageRepo.getDraftByParentId(getCaseId());
		// Actual test
		Test.startTest();
		List<Database.SaveResult> result = EmailMessageEventManager.publishEvents(
			new List<EmailMessage>{ message }
		);
		Test.stopTest();
		System.assertEquals(
			true,
			result.get(0).success,
			'publish successfully'
		);

		// Asserts
	}
	private static Id getCaseId() {
		return [SELECT Id FROM Case LIMIT 1].Id;
	}
}