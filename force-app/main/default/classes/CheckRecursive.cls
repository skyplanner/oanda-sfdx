/* Name: checkRecursive
* Description : Apex Class to stop Recursiveness of the Trigger.
* Author: Sri
* Date : 2020-05-28
*/
public class CheckRecursive
{
     // add any record ID that is being updated and doesnt want the triggers to be run recusively
     public static Set<Id> setOfIDs = new Set<Id>();
     // creating the Trigger Context fields after filtering the records in the recursive check
     public List<SObject> triggerNew;
     public List<SObject> triggerOld;

     public CheckRecursive(Map<Id,SObject> trgNewMap, Map<Id,SObject> trgOldMap)
     {    
          If(trgNewMap != null )
          {
              triggerNew = new List<SObject>();
              for(SObject sobj: trgNewMap.values())
              {
                  If(!CheckRecursive.setOfIDs.contains(sobj.Id))
                  {
                      triggerNew.add(sobj);
                  }
              }
          }
          If(trgOldMap != null )
          {
              triggerOld = new List<SObject>();
              for(SObject sobj: trgOldMap.values())
              {
                 If(!CheckRecursive.setOfIDs.contains(sobj.Id))
                 {
                     triggerOld.add(sobj);
                 }
              }
          }
     }
}