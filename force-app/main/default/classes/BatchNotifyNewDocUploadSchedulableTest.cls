@isTest(seeAllData=false)
public class BatchNotifyNewDocUploadSchedulableTest 
{
    final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);
    static final Id systemUserId = UserUtil.getUserIdByName('System User');

    @TestSetup
    static void initData()
    {
        User testuser1 = UserUtil.getRandomTestUser();     
        User testuser2 = UserUtil.getRandomTestUser();
        User testuser3 = UserUtil.getRandomTestUser();
        User testuser4 = UserUtil.getRandomTestUser();
        User testuser5 = UserUtil.getRandomTestUser();

        Account[] acs = new Account[]{         
            new Account(FirstName = 'First1',
                        LastName='Last1', 
                        PersonEmail = 'test1@oanda.com',
                        Account_Status__c = 'Active', 
                        PersonMailingCountry = 'Canada',
                        ownerId = testuser1.Id),
            new Account(FirstName = 'First2',
                        LastName='Last2', 
                        PersonEmail = 'test2@oanda.com',
                        Account_Status__c = 'Active', 
                        PersonMailingCountry = 'Canada',
                        ownerId = testuser2.Id),
        	new Account(FirstName = 'First3',
                        LastName='Last3',
                        PersonEmail = 'test3@oanda.com', 
                        Account_Status__c = 'Active',
                        PersonMailingCountry = 'Canada',
                        ownerId = testuser3.Id),
            new Account(FirstName = 'First4',
                        LastName='Last4',
                        PersonEmail = 'test4@oanda.com', 
                        Account_Status__c = 'Active',
                        PersonMailingCountry = 'Canada',
                        ownerId = testuser4.Id),
            new Account(FirstName = 'First5',
                        LastName='Last5',
                        PersonEmail = 'test5@oanda.com', 
                        Account_Status__c = 'Active',
                        PersonMailingCountry = 'Canada',
                        ownerId = systemUserId)
        };
        insert acs;
            
        fxAccount__c[] tradeAccounts = new fxAccount__c[]
        {
           new fxAccount__c(Account_Email__c = 'test1@oanda.com', //pass
                            Funnel_Stage__c = FunnelStatus.DOCUMENTS_UPLOADED,
                            RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Account__c = acs[0].Id,
                            ownerId = testuser1.Id),
           new fxAccount__c(Account_Email__c = 'test2@oanda.com', //pass
                            Funnel_Stage__c = FunnelStatus.MORE_INFO_REQUIRED,
                            RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Account__c = acs[1].Id,
                            ownerId = testuser2.Id),
       	   new fxAccount__c(Account_Email__c = 'test3@oanda.com',  //fail -> funnel stage traded
        					Funnel_Stage__c = FunnelStatus.TRADED,
        					RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Account__c = acs[2].Id,
                            ownerId = testuser3.Id),
            new fxAccount__c(Account_Email__c = 'test4@oanda.com', // fail -> //non Onboarding case
        					Funnel_Stage__c = FunnelStatus.MORE_INFO_REQUIRED,
        					RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Account__c = acs[3].Id,
                            ownerId = testuser4.Id),
            new fxAccount__c(Account_Email__c = 'test5@oanda.com', // fail -> account owner - system owner / rhu 
        					Funnel_Stage__c = FunnelStatus.MORE_INFO_REQUIRED,
        					RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Account__c = acs[4].Id,
                            ownerId = systemUserId)
        };
        insert tradeAccounts;
        
        Case[] cases = new Case[]{
           new Case(recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(), 
					Status = 'Open',
					fxAccount__c = tradeAccounts[0].Id, 
					AccountId = acs[0].Id,
					Subject = 'Subject of the case'),
           new Case(recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
					Status = 'Open',
					fxAccount__c = tradeAccounts[1].Id, 
					AccountId = acs[1].Id,
					Subject = 'Subject of the case'),
            new Case(recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(), //fail -> funnel stage traded
					Status = 'Open',
					fxAccount__c = tradeAccounts[2].Id, 
					AccountId = acs[2].Id,
					Subject = 'Subject of the case'),
             new Case(recordTypeId = RecordTypeUtil.getSupportCaseTypeId(), // fail -> //non Onboarding case
					  Status = 'Open',
					  fxAccount__c = tradeAccounts[3].Id, 
					  AccountId = acs[3].Id,
					  Subject = 'Subject of the case'),
              new Case(recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),// fail -> non system user
					Status = 'Open',
					fxAccount__c = tradeAccounts[4].Id, 
					AccountId = acs[4].Id,
					Subject = 'Subject of the case'),
              new Case(recordTypeId = RecordTypeUtil.getSupportCaseTypeId(), // fail -> // document record type - scan
					  Status = 'Open',
					  fxAccount__c = tradeAccounts[3].Id, 
					  AccountId = acs[3].Id,
					  Subject = 'Subject of the case')
         };
		insert cases;
        
        Test.setCreatedDate(cases[0].Id, DateTime.now() -2);
        Test.setCreatedDate(cases[1].Id, DateTime.now() -2);
        Test.setCreatedDate(cases[2].Id, DateTime.now() -2);
        Test.setCreatedDate(cases[3].Id, DateTime.now() -2);
 		Test.setCreatedDate(cases[4].Id, DateTime.now() -2);
        Test.setCreatedDate(cases[5].Id, DateTime.now() -2);
        
        Document__c[] docs = new Document__c[]{
            new Document__C(name = 'Test0', 
                            Case__c = cases[0].Id,
                            recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), 
                            fxAccount__c = tradeAccounts[0].Id,
                            ownerId = REGISTRATION_USER.Id),
             new Document__C(name = 'Test1', 
                            Case__c =  cases[1].Id,
                            recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), 
                            fxAccount__c = tradeAccounts[1].Id,
                            ownerId = REGISTRATION_USER.Id),
              new Document__C(name = 'Test2', 
                            Case__c =  cases[2].Id, 
                            recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), 
                            fxAccount__c = tradeAccounts[2].Id, //fail - fxaccount funnel - traded
                            ownerId = REGISTRATION_USER.Id),
               new Document__C(name = 'Test3', 
                               Case__c =  cases[3].Id, //fail - support case
                               recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), 
                               fxAccount__c = tradeAccounts[3].Id,
                               ownerId = REGISTRATION_USER.Id),
                new Document__C(name = 'Test4', 
                               Case__c =  cases[4].Id,
                               recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), 
                               fxAccount__c = tradeAccounts[3].Id,  //fail - account owner - system user/rhu
                               ownerId = REGISTRATION_USER.Id),
               new Document__C(name = 'Test5', 
                               Case__c =  cases[5].Id,
                               recordTypeId = RecordTypeUtil.getSupportDocumentTypeId(), //fail - support document rec
                               fxAccount__c = tradeAccounts[4].Id, 
                               ownerId = REGISTRATION_USER.Id)
        }; 
       insert docs;
    }

    @IsTest
    static void test1()
    {
        Test.startTest();
        BatchNotifyNewDocUploadSchedulable.execute();
        Test.stopTest();
        
        system.assertEquals(2, BatchNotifyNewDocUploadSchedulable.emailMessages.size());
    }
}