/**
 * @File Name          : VerificationCodeScheduler_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/12/2019, 2:50:46 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/11/2019   dmorales     Initial Version
**/
@isTest
private class VerificationCodeScheduler_Test {
    @isTest
    static void testExecute() {
       	Integer beforeCount = [select count() from Verification_Code_Setting__c];
		VerificationCodeScheduler scheduler = new VerificationCodeScheduler();
		Test.startTest();
		scheduler.execute(null);
		Test.stopTest();
		Integer afterCount = [select count() from Verification_Code_Setting__c];
        /* Insert big object is not allowed in test class, so VerificationCodeJob 
        does not insert anything to Verification_Code_Setting__c */
		System.assertEquals(beforeCount, 0);	
        System.assertEquals(afterCount, 0);	
    }
}