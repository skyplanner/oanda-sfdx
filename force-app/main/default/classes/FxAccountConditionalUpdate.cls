/**
 * Handles each one of the workflow updates for the fxAccount.
 * Most of these updates will happen on a before-trigger. There is
 * method for each conditional update and a mapping to call the update
 * dynamically based on the workflow name.
 * @author Fernando Gomez
 */
public class FxAccountConditionalUpdate extends ConditionalUpdate {
	private fxAccount__c newfxAccount;
	private fxAccount__c oldfxAccount;

	private static String SYSTEM_USER_ID;
	private static final String EXPIRATION_DAYS_SETTING =
		'CD_Annual_PIU_Process_Expiration_Days';

	/**
	 * @param newfxAccount
	 * @param oldfxAccount
	 */
	public FxAccountConditionalUpdate(
			fxAccount__c newfxAccount,
			fxAccount__c oldfxAccount) {
		super(newfxAccount, oldfxAccount);
		this.newfxAccount = newfxAccount;
		this.oldfxAccount = oldfxAccount;

		if (SYSTEM_USER_ID == null)
			SYSTEM_USER_ID = UserUtil.getSystemUserId();
	}

	/**
	 * Performs the action in the workflow with the specified name
	 * @param workflowName
	 */
	public override void execute(String workflowName) {
		switch on workflowName {
			when 'WF_Deposit_USD_MTD_Update_DateTime' {
				execute_Deposit_USD_MTD_Update_DateTime();
			}
			when 'WF_Withdrawal_USD_MTD_Update_DateTime' {
				execute_Withdrawal_USD_MTD_Update_DateTime();
			}
			when 'WF_fxAccount_Retail_Live' {
				execute_fxAccount_Retail_Live();
			}
			when 'WF_fxAccount_Set_CD_Defaults' {
				execute_fxAccount_Set_CD_Defaults();
			}
			when 'WF_fxAccount_Set_CD_Preference_ID' {
				execute_fxAccount_Set_CD_Preference_ID();
			}
			when 'WF_fxAccount_US_Shares_CD_Preference' {
				execute_fxAccount_US_Shares_Trading_Set_CD_Preference_ID();
			}
			when else {
				// do nothing as update is not supported
			}
		}
	}

	/**
	 * Performs the action in the workflow:
	 * Deposit_USD_MTD_Update_DateTime
	 */
	public void execute_Deposit_USD_MTD_Update_DateTime() {
		/*
		OR(
			ISCHANGED( Deposit_USD_MTD__c ),
			AND( 
				ISNEW(),
				Deposit_USD_MTD__c > 0 
			)
		)
		*/
		if ((isNew() && getDecimal(newfxAccount, 'Deposit_USD_MTD__c') > 0) ||
				(!isNew() && isChangedDecimal('Deposit_USD_MTD__c'))) {
			// fxAccount: Deposit USD MTD Update DateTime = Now()
			newfxAccount.Deposit_USD_MTD_Update_DateTime__c = System.now();
		}
	}
	
	/**
	 * Performs the action in the workflow:
	 * fxAccount_Retail_Live
	 */
	public void execute_Withdrawal_USD_MTD_Update_DateTime() {
		/*
		OR(
			ISCHANGED( Withdrawal_USD_MTD__c ),
			AND(
				ISNEW(),
				Withdrawal_USD_MTD__c > 0
			)
		)
		*/
		if ((isNew() && getDecimal(newfxAccount, 'Withdrawal_USD_MTD__c') > 0) ||
				(!isNew() && isChangedDecimal('Withdrawal_USD_MTD__c'))) {
			// fxAccount: Deposit USD MTD Update DateTime = Now()
			newfxAccount.Withdrawal_USD_MTD_Update_DateTime__c = System.now();
		}
	}

	/**
	 * Performs the action in the workflow:
	 * fxAccount_Retail_Live
	 */
	public void execute_fxAccount_Retail_Live() {
		/*
		AND(
			ISBLANK(Account__c),
			Owner:User.LastName = "System User",
			RecordType.Name = "Retail Live",
			Lead__c != null
		)
		*/
		if (
				// we a way to set Trigger_Lead_Assignment_Rules__c manually
				// without being overwritten, so if this field is intentionally
				// set to false, we do not change its value.. so, is good to
				// go if the field is not changed
				(isNew() || 
					(!isChangedBoolean('Trigger_Lead_Assignment_Rules__c') &&
					UserInfo.getUserId() != UserUtil.getSystemUserId())) &&
				// original wf comparisons
				String.isBlank(newfxAccount.Account__c) &&
				newfxAccount.OwnerId == SYSTEM_USER_ID &&
				newfxAccount.RecordTypeId 
					== RecordTypeUtil.getFxAccountLiveId() &&
				String.isNotBlank(newfxAccount.Lead__c))
			// fxAccount: Trigger Lead Assignment Rules = true
			newfxAccount.Trigger_Lead_Assignment_Rules__c = true;
	}

	/**
	 * Performs the action in the workflow:
	 * fxAccount_Set_CD_Defaults
	 */
	public void execute_fxAccount_Set_CD_Defaults() {
		/*
		IF(
			ISBLANK(CD_Account_Preference_ID__c),
			CD_Account_Preference_ID__c =
				Current_Desk_Parameter__mdt.Account_Preference_Id,
			CD_Account_Preference_ID__c
		)
		*/
		if (newfxAccount.CD_Account_Preference_ID__c == null &&
			newfxAccount.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS &&
			newfxAccount.CD_Client_ID__c == null &&
			newfxAccount.RecordTypeId == RecordTypeUtil.getFxAccountLiveId())
			// fxAccount: CD_Account_Preference_ID__c = default
			if(newfxAccount.Country__c=='Taiwan'||newfxAccount.Country__c=='Hong Kong'){
				newfxAccount.CD_Account_Preference_ID__c = Integer.valueOf(
					CurrentDeskSync.getConstantParameters().get('Account_Preference_Id_Leverage_200_1'));
			}else{
				newfxAccount.CD_Account_Preference_ID__c = Integer.valueOf(
					CurrentDeskSync.getConstantParameters().get('Account_Preference_Id'));
			}
	}

	/**
	 * Performs the action in the workflow:
	 * fxAccount_Set_CD_Preference_ID
	 */
	public void execute_fxAccount_Set_CD_Preference_ID() {
		Integer mappedPreference,
			currentPreferenceId,
			daysBack = SPGeneralSettings.getInstance()
				.getValueAsInteger(EXPIRATION_DAYS_SETTING);
		/*
		AND (
			NOT(ISNULL(Info_Last_Updated__c)),
			ISCHANGED(Info_Last_Updated__c),
			NOT(ISNULL(CD_Account_Preference_ID__c)),
			Info_Last_Updated__c > DAYS:365
		*/
		if (newfxAccount.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS &&
			newfxAccount.CD_Client_ID__c != null &&
			newfxAccount.Is_Closed__c == false &&
			newfxAccount.RecordTypeId == RecordTypeUtil.getFxAccountLiveId() &&
			// no action if date is null
			newfxAccount.Info_Last_Updated__c != null &&
				// no action if it hasn't changed
				isChangedDatetime('Info_Last_Updated__c') &&
				// and only if it's less than a year
				newfxAccount.Info_Last_Updated__c >= System.today().addDays(-daysBack)) {
			// we get the value as INT
			currentPreferenceId = getInteger(
				newfxAccount, 'CD_Account_Preference_ID__c');

			// we get its mappings
			mappedPreference =
				CurrentDeskSync.getPreferenceMappingsInversed().get(currentPreferenceId);

			// if there is a mapping, we then update
			if (mappedPreference != null)
				newfxAccount.CD_Account_Preference_ID__c = mappedPreference;
		}
	}
	
	/**
	 * Performs the action in the workflow:
	 * WF_fxAccount_US_Shares_CD_Preference
	 */
	public void execute_fxAccount_US_Shares_Trading_Set_CD_Preference_ID() {		
		if (newfxAccount.CD_Account_Preference_ID__c != null &&
			newfxAccount.CD_Client_ID__c != null &&
			newfxAccount.RecordTypeId == RecordTypeUtil.getFxAccountLiveId() &&
			isChanged('US_Shares_Trading_Enabled__c')
		) {			
			Integer currentPreferenceId = getInteger(
				newfxAccount, 'CD_Account_Preference_ID__c');

			Integer mappedPreference 
				= (newfxAccount.US_Shares_Trading_Enabled__c
					? CurrentDeskSync.getUSSharesPreferenceMappings()
					: CurrentDeskSync.getUSSharesPreferenceMappingsInversed())
					.get(currentPreferenceId);

			// if there is a mapping, we then update
			if (mappedPreference != null)
				newfxAccount.CD_Account_Preference_ID__c = mappedPreference;
		}
	}

	/**
	 * @return the sobject record casted into an account
	 */
	private fxAccount__c cast(SObject record) {
		return (fxAccount__c)record;
	}
}