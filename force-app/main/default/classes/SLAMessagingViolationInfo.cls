/**
 * @File Name          : SLAMessagingViolationInfo.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/27/2024, 4:51:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/27/2024, 4:51:05 PM   aniubo     Initial Version
**/
public with sharing class SLAMessagingViolationInfo extends SLAChatViolationInfo {
	public String sourceType { get; set; }
	public SLAMessagingViolationInfo(
		String chatId,
		String chatNumber,
		String violationType
	) {
		super(chatId, chatNumber, violationType);
	}
}