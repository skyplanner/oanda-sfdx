/**
 * @File Name          : SubtypeByAgentInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/2/2021, 12:42:14 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/2/2021, 9:48:33 AM   acantero     Initial Version
**/
public without sharing class SubtypeByAgentInfo {

    @auraEnabled
    public Boolean live {get; set;}
    @auraEnabled
    public Boolean practice {get; set;}
    @auraEnabled
    public String inquiryNatureMapped {get; set;}
    @auraEnabled
    public String serviceType {get; set;}

    public SubtypeByAgentInfo(
        String inquiryNatureMapped,
        String serviceType
    ) {
        this.inquiryNatureMapped = inquiryNatureMapped;
        this.serviceType = serviceType;
    }

}