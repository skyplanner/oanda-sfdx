/**
 * @File Name          : OnLiveChatCaseChangeCmd.cls
 * @Description        : 
 * Detects changes to the "CaseId" field of LiveChatTranscript records and 
 * updates the Tier field in the related Case
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2024, 1:30:53 PM
**/
public inherited sharing class OnLiveChatCaseChangeCmd {

	final List<LiveChatTranscript> recList;
	final Map<Id, LiveChatTranscript> oldMap;

	@TestVisible
	List<Case> recordsToUpdate;

	public OnLiveChatCaseChangeCmd(
		List<LiveChatTranscript> recList,
		Map<Id, LiveChatTranscript> oldMap
	) {
		this.recList = recList;
		this.oldMap = oldMap;
	}

	public Boolean execute() {
		if (
			(recList == null) ||
			recList.isEmpty() ||
			(oldMap == null)
		) {
			return false;
		}
		// else...
		recordsToUpdate = new List<Case>();
		
		for (LiveChatTranscript rec : recList) {
			LiveChatTranscript oldRec = oldMap.get(rec.Id);
			
			if (checkRecord(rec, oldRec) == true) {
				recordsToUpdate.add(
					new Case(
						Id = rec.CaseId,
						Tier__c = rec.Tier__c,
						Ready_For_Routing__c = true
					)
				);
				// do something
			}
		}
		
		update recordsToUpdate;
		return true;
	}
	
	@TestVisible
	Boolean checkRecord(
		LiveChatTranscript newRec,
		LiveChatTranscript oldRec
	) {
		Boolean result = (
			(newRec.CaseId != null) &&
			(newRec.CaseId != oldRec.CaseId)
		);
		return result;
	}
	
}