/**
 * @File Name          : SendMsgAuthenticationCodeActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:13:48 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 11:37:13 AM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class SendMsgAuthenticationCodeActionTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		List<Boolean> actionResults;
		// Actual test
		Test.startTest();
		actionResults = SendMsgAuthenticationCodeAction.sendMsgAuthenticationCode(
			null
		);
		Assert.areEqual(
			false,
			actionResults.get(0),
			'Action Result should be false'
		);
		actionResults = null;
		actionResults = SendMsgAuthenticationCodeAction.sendMsgAuthenticationCode(
			new List<SendMsgAuthenticationCodeAction.ActionInfo>()
		);

		Assert.areEqual(
			false,
			actionResults.get(0),
			'Action Result should be false'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testIsLinkedToAccountOrLeadThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		List<Boolean> actionResults;
		// Actual test
		Test.startTest();
		try {
			actionResults = SendMsgAuthenticationCodeAction.sendMsgAuthenticationCode(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testTestCase() {
		SPDataInitManager initManager = SPDataInitManager.reload();

		SendMsgAuthenticationCodeAction.ActionInfo actionInfo = new SendMsgAuthenticationCodeAction.ActionInfo();
		actionInfo.fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		actionInfo.messagingSessionId = null;
		// Actual test
		Test.startTest();

		List<Boolean> actionResults = SendMsgAuthenticationCodeAction.sendMsgAuthenticationCode(
			new List<SendMsgAuthenticationCodeAction.ActionInfo>{ actionInfo }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			actionResults.get(0),
			'Action Result should be false'
		);
	}
}