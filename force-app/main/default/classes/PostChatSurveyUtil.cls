public without sharing class PostChatSurveyUtil {
	static public void setLiveChatTranscript(List<Post_Chat_Survey__c> surveys, Map<Id, Post_Chat_Survey__c> oldMap, boolean isInsert) {
		Map<Id, Post_Chat_Survey__c> surveysToUpdateById = new Map<Id, Post_Chat_Survey__c>();
		List<String> chatKeys = new List<String>();
		for(Post_Chat_Survey__c s : surveys) {
			if((isInsert && s.Chat_Key__c!=null) || (!isInsert && s.Chat_Key__c!=oldMap.get(s.Id).Chat_Key__c)) {
				surveysToUpdateById.put(s.Id, s);
				chatKeys.add(s.Chat_Key__c);
			}
		}
		
		if(surveysToUpdateById.size()>0) {
			Map<String, LiveChatTranscript> liveChatTranscriptByChatKey = new Map<String, LiveChatTranscript>();
			for(LiveChatTranscript t : [SELECT Id, ChatKey, OwnerId FROM LiveChatTranscript WHERE ChatKey IN :chatKeys]) {
				liveChatTranscriptByChatKey.put(t.ChatKey, t);
			}
			
			for(Post_Chat_Survey__c s : surveysToUpdateById.values()) {
				LiveChatTranscript t = liveChatTranscriptByChatKey.get(s.Chat_Key__c);
				if(t!=null) {
					s.Live_Chat_Transcript__c = t.Id;
					s.OwnerId = t.OwnerId;
				}
			}
			// update surveysToUpdateById.values();
		}
	}
}