/**
 * @File Name          : CreateMessagingCommentAction.cls
 * @Description        : Saves a "Messaging Comment" record associated
 *                       with the "Messaging Session" record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/14/2023, 7:36:55 PM
**/
public without sharing class CreateMessagingCommentAction { 

	@InvocableMethod(label='Create Messaging Comment' callout=false)
	public static void createMessagingComment(List<CommentInfo> comments) { 
		try {
			ExceptionTestUtil.execute();
			   
			if (
				(comments == null) ||
				comments.isEmpty()
			) {
				return;
			}
			// else...        
			CommentInfo info = comments[0];
			MessagingCommentProcess process = new MessagingCommentProcess(
				info.messagingSessionId, // messagingSessionId
				info.key // key
			);
			process.upsertComment(
				info.subject, // subject
				info.comment // body
			);

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CreateMessagingCommentAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class CommentInfo {

		@InvocableVariable(label = 'messagingSessionId' required = false)
		public ID messagingSessionId;

		@InvocableVariable(label = 'key' required = true)
		public String key;

		@InvocableVariable(label = 'comment' required = true)
		public String comment;

		@InvocableVariable(label = 'subject' required = false)
		public String subject;

	}

}