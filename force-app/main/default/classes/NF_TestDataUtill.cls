/**
* Created by Neuraflash LLC on 03/07/18.
* Implementation : Chatbot
* Summary : Create a case record or lead & case record based on Account Object search result
* Details : 1. Searches for a account by email or souid provided.
*           2. Creates a new case if found an account from search filters.
*           3. Creates both case & lead record  if there is no account associated with account .
* 
*/
@isTest
public class NF_TestDataUtill {
    @future
    public static void createQueue(){
        Group g1 = new Group(Name='test funds cases', type='Queue');
        insert g1;
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
        insert q1;
    }
    
    public static nfchat__Chat_Log__c createChatLog(){
        nfchat__Chat_Log__c chatlog = new nfchat__Chat_Log__c();
        chatlog.nfchat__Last_Name__c = 'Test LastName';
        chatlog.nfchat__First_Name__c = 'Test FirstName';
        chatlog.nfchat__Email__c = 'test@testcc.com';
        chatlog.nfchat__AI_Config_Name__c = 'AIConfig Test';
        chatlog.nfchat__Session_Id__c = '1234474y54734y';
        chatlog.nfchat__Chat_Key__c = 'askh123456789as';
        return chatlog;
    }
    
    public static nfchat__Chat_Log_Detail__c createChatLogDetail() {
        nfchat__Chat_Log_Detail__c chtLogDetail = new nfchat__Chat_Log_Detail__c();
        chtLogDetail.nfchat__Intent_Name__c = 'Client Portal';
        chtLogDetail.nfchat__Request__c = 'Test Request';
        chtLogDetail.nfchat__Response__c = '';
        return chtLogDetail;
    }
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        return acc;
    }
    public static Account createPersonAccount(){
        Account acc = new Account();
        Id personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        acc.recordTypeId = personAccountRecordTypeId;
        acc.LastName = 'Test Account';
        return acc;
    }
    public static fxAccount__c createfxAccount(){
        fxAccount__c newfxAcc = new fxAccount__c();
        newfxAcc.Name = 'Test FXAccount';
        return newfxAcc;
    }
    public static Contact createContact(){
        Contact con = new Contact();
        Account acc = NF_TestDataUtill.createAccount();
        insert acc;
        con.AccountId = acc.id;
        con.Email = 'test@test.com';
        con.FirstName = 'test';
        con.LastName = 'contact';
        return con;
    }
    public static Lead createLead(){
        Lead newLead = new Lead();
        newLead.LastName = 'TesLead';
        newLead.email = 'Test@data.com';
        return newLead;
    }
    
    public static LiveChatTranscript getTranscript(){
        
        LiveChatTranscript liveChat = new LiveChatTranscript();
        liveChat.OwnerID = userinfo.getUserId();
        livechat.ChatKey = 'askh123456789as';
        return liveChat;
    }
    
    public static LiveChatVisitor getVisitor(){
        LiveChatVisitor visitor = new LiveChatVisitor();
        return visitor;
    }
    
    public static Case getCase(){
        Case newCase = new Case();
        return newCase;
    }
    public static void getLog(Exception e){
        nfchat__Debug_Log__c  debug_log = new nfchat__Debug_Log__c ();
        debug_log.Name = e.getTypeName().subString(0,10)+' ' + e.getMessage().substring(0,10);
        debug_log.nfchat__Error_Message__c = String.valueOf(e.getLineNumber());
        debug_log.nfchat__Exception_Data__c = e.getMessage();
        debug_log.nfchat__Stack_Trace__c = e.getStackTraceString();
        debug_log.nfchat__Type__c = e.getTypeName();
        insert debug_log;
    }
}