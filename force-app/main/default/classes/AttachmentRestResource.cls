@RestResource(urlMapping='/Attachment/*')
global with sharing class AttachmentRestResource {
    
    
    @HttpPost
    global static String doPost(){
    	
    	

        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        
        String filename = req.params.get('filename');
        if(filename == null || filename == ''){
            return 'File name is required';
        }
        
        System.debug('File Name: ' + filename);
        
        String clientEmail = req.params.get('clientemail');
        if(clientEmail == null || clientEmail == ''){
            return 'Client Email is required';
        }
        
        System.debug('Client Email: ' + clientEmail);
        
        String myNumberFlag = req.params.get('isMyNumber');
        boolean isMyNumber = false;
        
        if(myNumberFlag != null && myNumberFlag.toUpperCase() == 'TRUE'){
            isMyNumber = true;
        }
        
        //only do the API control for OJ for now
    	if(isMyNumber && !CustomSettings.isUserAllowedForAPI(UserInfo.getUserId(), CustomSettings.API_NAME_MY_NUMBER)){
    		return 'User is not allowed to call this API';
    	}
        
        //create a new document
        Document__c doc = new Document__c (name = filename, Is_My_Number__c = isMyNumber);
        linkDocToAccountLead(clientEmail, doc);
        
        
        insert doc;
        
        System.debug('New Document ID: ' + doc.Id);
        
        Blob docData = req.requestBody;

        Attachment a = new Attachment (ParentId = doc.id,
                                       Body = docData,
                                       Name = filename);

        insert a;

        return 'Salesforce ID: ' + doc.id;

   }
   
   private static void linkDocToAccountLead(String clientEmail, Document__c doc){
    
        //check if there is an account with this email
        List<Account> matchedAccounts = [select id from Account where PersonEmail = :clientEmail];
        
        if(matchedAccounts.size() > 0){
            //assume there is no duplicate account
            doc.Account__c = matchedAccounts[0].Id;
            
            System.debug('Document ' + doc.Id + ' Attached to Account ' + matchedAccounts[0].Id);
            //if account is matched, we don't go further
            return;
        }
        
        //check if there is a lead with this email when there is no account
        List<Lead> matchedLeads = [select id from Lead where email = :clientEmail];
        
        if(matchedLeads.size() > 0){
            //assume no duplicate lead
            doc.Lead__c = matchedLeads[0].Id;
            
            System.debug('Document ' + doc.Id + 'Attached to Lead ' + matchedLeads[0].Id);
        }
        
   }
   
    
 
}