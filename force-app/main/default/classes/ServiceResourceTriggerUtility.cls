/**
 * @File Name          : ServiceResourceTriggerUtility.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2021, 1:25:03 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/16/2021, 11:20:38 AM   acantero     Initial Version
**/
public with sharing class ServiceResourceTriggerUtility {

    public static final String OBJ_API_NAME = 'ServiceResource';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //******************************************************* */

    final List<ServiceResource> newList;
    final Map<ID,ServiceResource> oldMap;

    public ServiceResourceTriggerUtility(
        List<ServiceResource> newList,
        Map<ID,ServiceResource> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    public void handleAfterInsert() {
        checkIsActiveChanges();
    }

    public void handleAfterUpdate() {
        checkIsActiveChanges();
    }

    void checkIsActiveChanges() {
        List<ServiceResource> activatedServResList = new List<ServiceResource>();
        Set<String> deactivatedUserIdSet = new Set<String>();
        for(ServiceResource serviceRes : newList) {
            ServiceResource oldRecord = oldMap?.get(serviceRes.Id);
            Boolean oldActive = oldRecord?.IsActive;
            if (
                (serviceRes.IsActive != oldActive) &&
                (serviceRes.ResourceType == OmnichanelConst.SERVICE_RES_AGENT)
            ) {
                if (serviceRes.IsActive == true) {
                    activatedServResList.add(serviceRes);
                    //...
                } else if (
                    (serviceRes.IsActive == false) &&
                    (oldActive == true) //oldActive == null implies that it is a new record, not a deactivation
                ) {
                    deactivatedUserIdSet.add(serviceRes.RelatedRecordId);
                }
            }
        }
        if (!activatedServResList.isEmpty()) {
            LinkRelatedSkillCommand command = 
                new LinkRelatedSkillCommand(activatedServResList);
            command.execute();
        }
        if (!deactivatedUserIdSet.isEmpty()) {
            AgentInfoManager.deleteAgentsInfo(deactivatedUserIdSet);
        }
    }

    public class LinkRelatedSkillCommand {

        final List<ServiceResource> activatedServResList;
        public Map<String,String> skillIdMap {get; private set;}
        public Map<String,Set<String>> servResSkillsMap {get; private set;}

        public LinkRelatedSkillCommand(List<ServiceResource> activatedServResList) {
            this.activatedServResList = activatedServResList;
        }

        public void execute() {
            if (
                (activatedServResList == null) ||
                activatedServResList.isEmpty()
            ) {
                return;
            }
            //else...
            Set<String> servResNameSet = new Set<String>();
            for(ServiceResource serviceRes : activatedServResList) {
                servResNameSet.add(serviceRes.Name);
            }
            skillIdMap = getSkillIdMap(servResNameSet);
            System.debug('skillIdMap: ' + skillIdMap);
            if (skillIdMap.isEmpty()) {
                return;
            }
            //else...
            servResSkillsMap = getServResSkillsMap(skillIdMap.values());
            System.debug('servResSkillsMap: ' + servResSkillsMap);
            List<ServiceResourceSkill> serviceResourceSkillList = 
                getNewServiceResourceSkills();
            System.debug('serviceResourceSkillList: ' + serviceResourceSkillList);
            if (!serviceResourceSkillList.isEmpty()) {
                insert serviceResourceSkillList;
            }
        }

        List<ServiceResourceSkill> getNewServiceResourceSkills() {
            List<ServiceResourceSkill> result = 
                new List<ServiceResourceSkill>();
            for(ServiceResource serviceRes : activatedServResList) {
                String skillId = skillIdMap.get(serviceRes.Name);
                if (skillId != null) {
                    Boolean hasSkill = false;
                    Set<String> skillSet = servResSkillsMap.get(serviceRes.Id);
                    if (skillSet != null) {
                        hasSkill = skillSet.contains(skillId);
                    }
                    if (hasSkill == false) {
                        result.add(
                            new ServiceResourceSkill(
                                EffectiveStartDate = Datetime.now(),
                                EffectiveEndDate = null,
                                ServiceResourceId = serviceRes.Id,
                                SkillId = skillId,
                                SkillLevel = OmnichanelConst.MAX_SKILL_LEVEL
                            )
                        );
                    }
                }
            }
            return result;
        }

        Map<String,String> getSkillIdMap(Set<String> servResNameSet) {
            Map<String,String> result = new Map<String,String>();
            if (
                (servResNameSet == null) ||
                servResNameSet.isEmpty()
            ) {
                return result;
            }
            //else...
            List<Skill> skillList = [
                SELECT 
                    Id, 
                    DeveloperName,
                    MasterLabel 
                FROM 
                    Skill 
                WHERE 
                    MasterLabel IN :servResNameSet
            ];
            if (!skillList.isEmpty()) {
                Set<String> skillDevNameSet = new Set<String>();
                for(Skill skillObj : skillList) {
                    skillDevNameSet.add(skillObj.DeveloperName);
                }
                Map<String,String> skillCodeMapFromDevNames = 
                    ServiceSkillManager.getSkillCodeMapFromDevNames(
                        skillDevNameSet
                    );
                for(Skill skillObj : skillList) {
                    if (skillCodeMapFromDevNames.containsKey(skillObj.DeveloperName)) {
                        result.put(skillObj.MasterLabel, skillObj.Id);
                    }
                }
            }
            return result;
        }

        Map<String,Set<String>> getServResSkillsMap(List<String> skillIdList) {
            Map<String,Set<String>> result = new Map<String,Set<String>>();
            List<ServiceResourceSkill> serviceResourceSkillList = [
                SELECT 
                    Id, 
                    ServiceResourceId, 
                    SkillId
                FROM
                    ServiceResourceSkill
                WHERE
                    SkillId IN :skillIdList 
            ];
            for(ServiceResourceSkill servResSkill : serviceResourceSkillList) {
                Set<String> skillSet = result.get(servResSkill.ServiceResourceId);
                if (skillSet == null) {
                    skillSet = new Set<String>();
                    result.put(servResSkill.ServiceResourceId, skillSet);
                }
                skillSet.add(servResSkill.SkillId);
            }
            return result;
        }
    }


}