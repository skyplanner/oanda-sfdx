/* Name: PaxosUtil
 * Description : Utility class for Paxos
 * Author: Eugene
 * Created Date : 2022-01-25
 * Last Modified By : Eugene
 * Last Modified Date : 2022-01-30
 */

public with sharing class PaxosUtil {

    private static List<External_Notification_Event__e> externalNotificationEvents;
    
    public static Map<String, String> PaxosStatusFunnelStageMap = new Map<String,String>
    {
        'Pending' => 'Pending',
        'Error' => 'Error',
        'Denied' => 'Rejected by Paxos',
        'Approved' => 'Ready For Funding'
    };

    /**
     * Update the Crypto fxAccount Funnel Stage based on the Paxos status
     */
    public static void updateCryptoFxaFunnelStage(List<Paxos__c> newList, Map<Id, Paxos__c> oldMap)
    {
        Map<Id, Paxos__c> fxAccountIdPaxosMap = new Map<Id, Paxos__c>();
        
        for(Paxos__c px : newList)
        {
            Paxos__c pxOld = oldMap?.get(px.Id);

            if(px.fxAccount__c != null &&
                ((pxOld == null && 
                    String.isNotBlank(px.Paxos_Status__c)) ||
                (pxOld != null && 
                    (px.Paxos_Status__c != pxOld.Paxos_Status__c ||
                    px.Admin_Disabled__c != pxOld.Admin_Disabled__c))))
            {
                fxAccountIdPaxosMap.put(px.fxAccount__c, px);
            }
        }

        if(fxAccountIdPaxosMap.isEmpty())
        {
            return;
        }

        Map<Id, fxAccount__c> fxaMap = new Map<Id, fxAccount__c>(
            [SELECT Id, 
                    Funnel_Stage__c
                FROM fxAccount__c 
                WHERE Id IN :fxAccountIdPaxosMap.keySet()]);

        List<fxAccount__c> fxasToUpdate = new List<fxAccount__c>();

        for(Paxos__c px : fxAccountIdPaxosMap.values())
        {
            fxAccount__c fxa = fxaMap.get(px.fxAccount__c);

            // Update fxaccount Funnel Stage when
            // 1. Paxos status is Disabled // check the Admin_Disabled__c flag
            if(px.Paxos_Status__c == Constants.PAXOS_STATUS_DISABLED)
            {
                fxa.Funnel_Stage__c = px.Admin_Disabled__c ? Constants.CRYPTO_FUNNEL_DISABLED_BY_PAXOS : Constants.CRYPTO_FUNNEL_CLOSED;
            }
            // 2. Paxos status is Pending, Error, Denied or Approved
            else if(PaxosStatusFunnelStageMap.keySet().contains(px.Paxos_Status__c))
            {
                fxa.Funnel_Stage__c = PaxosStatusFunnelStageMap.get(px.Paxos_Status__c);
            }
            
            fxasToUpdate.add(fxa);
        }

        if(!fxasToUpdate.isEmpty())
        {
            update fxasToUpdate;
        }
    }

    /**
     * If the Paxos Status changed, send an External Notification Event with the related FxAccount details.
     */
    public static void sendPaxosStatusChangedEvent(List<Paxos__c> newList, Map<Id, Paxos__c> oldMap){
        Set<Id> fxAccountIds = new Set<Id>();
        for(Paxos__c px : newList){
            if(px.Paxos_Status__c != oldMap.get(px.Id).Paxos_Status__c){
                fxAccountIds.add(px.fxAccount__c);
            }
        }
        if(!fxAccountIds.isEmpty()){
            createExternalNotificationEvents(fxAccountIds);
        }
    }

    private static void createExternalNotificationEvents(Set<Id> fxAccountIds){
        List<fxAccount__c> fxAccounts = [SELECT Id,Account_Email__c, Amount_Funded__c, Calculated_Risk_Tolerance__c, Citizenship_Nationality__c, Citizenship_Risk__c, Citizenship_Score__c, 
        Country__c, Currency__c, Division_from_Region__c, Email__c, First_Trade_Date__c, Funded_Date__c,  Funnel_Stage__c, GCLID__c,  GADCLID__c, Government_ID__c,Is_Trade_Account__c, Name,
        Net_Worth_Value__c, Passport_Number__c, Paxos_Identity__c, fxTrade_One_Id__c, (SELECT Id, Paxos_Status__c FROM Paxos_Identity__r)     FROM fxAccount__c WHERE Id IN :fxAccountIds LIMIT 200];
        externalNotificationEvents = new List<External_Notification_Event__e>();
        for(fxAccount__c fxa : fxAccounts){
            externalNotificationEvents.add(ExternalNotificationEventService.createEvent(ExternalNotificationEventService.ORIGIN_FXACCOUNT_PAXOSSTATUS_CHANGE, fxa));
        }
        publishEvents();
    }
    
    private static void publishEvents() {
        if(externalNotificationEvents != null && !externalNotificationEvents.isEmpty()) {
            ExternalNotificationEventService.publishEvents(externalNotificationEvents);
            externalNotificationEvents = null;
        }
    }
}