/**
 * @File Name          : MockSupervisorProvider.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 1:01:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 12:53:44 PM   acantero     Initial Version
**/
@isTest
public without sharing class MockSupervisorProvider implements System.StubProvider {

    public Object handleMethodCall(
        Object stubbedObject, 
        String stubbedMethodName,
        Type returnType, 
        List<Type> listOfParamTypes, 
        List<String> listOfParamNames,
        List<Object> listOfArgs
    ) {
        if (stubbedMethodName == 'addCriteria') {
            return null;
        }
        if (stubbedMethodName == 'getSupervisors') {
            if ((listOfParamNames == null) || listOfParamNames.isEmpty()) {
                return null;
            }
            //else...
            String userId = UserInfo.getUserId();
            return new Set<String> {userId};
        }
        if (stubbedMethodName == 'isEmpty') {
            return false;
        }
        //else...
        return null;
    }

}