/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchChatFormCheck implements
Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful, Database.RaisesPlatformEvents, BatchReflection {
    public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static Integer BATCH_SIZE = 100;
    public static String CRON_SCHEDULE = '0 0 */3 * * ?';

    public Boolean activeChatForm;

    public BatchChatFormCheck() {
        this(null);
    }

    public BatchChatFormCheck(String filter) {
        query = 
        'SELECT Id, Name ' +
        'FROM Site ' +
        'WHERE Name = \'LiveAgent\'';

        System.debug(query);
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Name FROM Site WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public static void executeBatch() {
        Database.executeBatch(new BatchHelpPortalCheck(), BATCH_SIZE);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchHelpPortalCheck(), BATCH_SIZE);
    }

    public void execute(Database.BatchableContext bc, List<Site> scope) {
        activeChatForm = true;

        if (!scope.isEmpty()) {
            Site helpPortal = scope[0];
            SiteDetail hpDetails = [
                SELECT SecureURL
                FROM SiteDetail
                WHERE DurableId = :helpPortal.Id
            ];

            // getting chat form status
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(hpDetails.SecureURL + 'apex/LiveChatWizard');
            request.setMethod('GET');
            HttpResponse response = http.send(request);
            activeChatForm = response.getStatusCode() == 200;
        }
    }
    public void finish(Database.BatchableContext bc) {
        System.debug('FINISH METHOD');
        System.debug('activeChatForm: ' + activeChatForm);
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);

        if (!activeChatForm && !Test.isRunningTest()) {
            Flow.Interview.Omni_Channel_Chat_Form_Down_Alert check =
                new Flow.Interview.Omni_Channel_Chat_Form_Down_Alert(
                    new Map<String, Object>()
                );
            check.start();
        }
    }

    public static String schedule(String schedule) {
        if (String.isNotBlank(schedule)) {
            CRON_SCHEDULE = schedule;
        }
        return System.schedule((
            Test.isRunningTest() ? 'Testing BatchChatFormCheck' : 'Chat Form Check'),
            CRON_SCHEDULE,
            new BatchChatFormCheck()
        );
    }
}