global without sharing class DocumentUtil {
  
  public static final String NAME_DOCUMENT = 'Document__c';

  global class deleteException extends Exception{}
  
  webService static String deleteDocument(Id id) {
      Document__c c = new Document__c(id = id);
      
      try {
        
        UserRecordAccess ura = [select RecordId, HasEditAccess, HasDeleteAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :id];
        if (canCurrentUserDelete(ura)){
          System.debug('Document can be deleted');
          
          delete c;
        }else{
          throw new deleteException('No Permission to delete');
        }
        
        return '';
      } catch (Exception e) {
        return e.getMessage();
      }
  }
  
  private static boolean canCurrentUserDelete(UserRecordAccess ura){
    boolean result = false;
    
    if(isOJAdmin(UserInfo.getUserId())){
      result = ura.HasEditAccess;
    }else{
      //check if the current user is the owner
      Integer counter = [select count() from Document__c where id = :ura.RecordId and ownerId = :UserInfo.getUserId()];
      
      if(counter == 1){
        //the current user is the owner
        result = true;
      }else{
        result = ura.HasDeleteAccess;
      }
      
      
    }
    
    return result;
  }
  
  private static boolean isOJAdmin(Id userId){
    boolean result = false;
    
    Group ojAdminGroup = [select id from Group where name = 'OJ Admin' limit 1];
    
    Integer uCount = [select count() from GroupMember where GroupId = : ojAdminGroup.Id and UserOrGroupId = : userId];
    
    if(uCount > 0){
      result = true;
    }
    
    return result;
  }
  
  /**
   * Create onboarding cases for the new documents only
   * if the current user is integration user.
   */
  public static void processDocsForCase(List<Document__c> docs){
      // Process only if the current user is integration user.
      if(!UserUtil.isIntegrationUser())
        return;
      
      //find or create onboarding cases
      List<ID> fxaIDList = new List<ID>();
      
      for(Document__c doc : docs){
        
        if(doc.fxAccount__c != null && doc.Document_Type__c != 'Comply Advantage' && !doc.Legacy_CRM_Migrated__c){
          fxaIDList.add(doc.fxAccount__c);
        }
      }
      
      System.debug('fxAccount size to be processed: ' + fxaIDList.size());

      //always re-open case when a new doc arrives and the case is closed
      Map<ID, Case> caseByFxAccountId = CaseUtil.getOnBoardingCaseByFxAccount(fxaIDList, 'Document Upload', 'Open', true);
      
      if(caseByFxAccountId.size() == 0){
         return;
      }

      //attach document to the case
      for(Document__c doc : docs){
         Case theCase = caseByFxAccountId.get(doc.fxAccount__c);
         
        if(theCase != null){
          doc.Case__c = theCase.Id;

          //link Document to Account or lead
          if(doc.Account__c == null){
             
            doc.Account__c = theCase.AccountId;
             
          }

          if(doc.Lead__c == null){
             
            doc.Lead__c = theCase.Lead__c;
             
          }
         }
      }
  }

  /* Moved to DocumentLinkUtil class...
  //Attach Lead or Account to the new documents only if the current user is integration user.
  public static void linkDocstoLeadOrAccount(List<Document__c> docs){
      // Process only if the current user is integration user.
      if(!UserUtil.isIntegrationUser()) {
        return;
      }

      //Find all fxAccounts related to the documents
      set<ID> fxaIDs = new set<ID>();

      for(Document__c doc : docs){
        if(doc.fxAccount__c != null && doc.Account__c == null && doc.Lead__c == null){
          fxaIDs.add(doc.fxAccount__c);
        }
      }

      if(fxaIDs.size() == 0){
        return;
      }
      
      //Get the accounts or leads related to the fxAccounts
      Map<Id, fxAccount__c> fxaByfxaId = new Map<Id, fxAccount__c>([SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE Id IN :fxaIDs]);
      if(fxaByfxaId == null || fxaByfxaId.size() ==0){

        return;
      }
      
      //Link documents to Account or Lead
      for(Document__c doc : docs){

        if(doc.fxAccount__c == null){
          continue;
        }
        
        fxAccount__c fxa = fxaByfxaId.get(doc.fxAccount__c);

        if(doc.Account__c == null){
          doc.Account__c = fxa.Account__c;
        }

        if(doc.Lead__c == null){
          doc.Lead__c = fxa.Lead__c;
        }
      } 
  }
  */

  /**
   * Know if document's fxAccount should be set
   * stage to Documents Uploaded
   */
  public static Boolean shouldFxAccSetAsDocUploaded(
      Document__c doc)
  {
    return
      doc.Case__c != null && 
      UserUtil.isRegistrationUser() &&
      isScanDocumentType(doc) &&
      isOnBoardingType(doc);
  }

  /**
   * Know if the doc is 'Scan' type
   */
  public static Boolean isScanDocumentType(
    Document__c doc)
  {
		return doc.RecordTypeId ==
      RecordTypeUtil.getScanDocumentTypeId();
	}

  /**
  * Know if the doc is 'Scan' type
  */
  public static Boolean isOnBoardingType(
    Document__c doc)
  {
    return doc.Case_RT_Id__c ==
      RecordTypeUtil.getOnBoardingCaseTypeId();
  }

}