/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ScheduleMailSender_Test {
	public static final Integer COUNT = 5;
	@testSetup
	private static void testSetup() {
		List<Account> accountList = ScheduledEmailTestUtil.createAccounts(
			COUNT
		);
		List<Contact> contactList = ScheduledEmailTestUtil.createContacts(
			accountList
		);
		List<Case> caseList = ScheduledEmailTestUtil.createCases(contactList);
		ScheduledEmailTestUtil.createDraftEmails(contactList, caseList);
	}
	@isTest
	private static void getScheduleDateTime() {
		Datetime dt = DateTime.newInstance(2012, 12, 28, 10, 0, 0);
		ScheduleMailSender emailSender = new ScheduleMailSender();

		ScheduleMailSender.ScheduleDateTime schedule = emailSender.getScheduleDateTime(
			dt
		);
		checkScheduleDateTime(schedule, dt.date(), '10', 'AM');

		System.assert(schedule != null, 'Not Null');
		dt = DateTime.newInstance(2012, 12, 28, 0, 0, 0);
		schedule = emailSender.getScheduleDateTime(dt);
		checkScheduleDateTime(schedule, dt.date(), '12', 'AM');
		dt = DateTime.newInstance(2012, 12, 28, 15, 0, 0);
		schedule = emailSender.getScheduleDateTime(dt);
		checkScheduleDateTime(schedule, dt.date(), '3', 'PM');
	}
	@isTest
	private static void getScheduledEmails() {
		Datetime dt = DateTime.newInstance(2002, 7, 22, 13, 0, 0);
		ScheduleMailSender emailSender = new ScheduleMailSender();
		createScheduledEmail(emailSender, dt);
		List<Scheduled_Email__c> scheduledEmails = emailSender.getScheduledEmails(
			dt,
			5
		);
		System.assertEquals(5, scheduledEmails.size(), 'Size can be 5');
		dt = DateTime.newInstance(2002, 7, 22, 14, 0, 0);
		scheduledEmails = emailSender.getScheduledEmails(dt, 5);
		System.assertEquals(
			5,
			scheduledEmails.size(),
			'the scheduled time is 1 PM'
		);
		dt = DateTime.newInstance(2002, 7, 22, 12, 0, 0);
		scheduledEmails = emailSender.getScheduledEmails(dt, 5);
		System.assertEquals(
			0,
			scheduledEmails.size(),
			'the scheduled time is 1 PM'
		);
	}
	@isTest
	private static void processScheduledEmails() {
		Datetime dt = DateTime.newInstance(2002, 7, 22, 13, 0, 0);
		ScheduleMailSender emailSender = new ScheduleMailSender();
		createScheduledEmail(emailSender, dt);
		Test.startTest();
		Integer processed = emailSender.processScheduledEmails(dt, COUNT);
		Test.stopTest();
		System.assertEquals(COUNT, processed, 'Count records to be processed');
		List<Scheduled_Email__c> scheduledEmails = emailSender.getScheduledEmails(
			dt,
			COUNT
		);
		System.assertEquals(0, scheduledEmails.size(), 'No Scheduled Emails');
	}
	@isTest
	private static void processScheduledEmails2() {
		Datetime dt = DateTime.newInstance(2002, 7, 22, 13, 0, 0);
		ScheduleMailSender emailSender = new ScheduleMailSender();
		createScheduledEmail(emailSender, dt);
		Test.startTest();
		Integer processed = emailSender.processScheduledEmails(
			DateTime.newInstance(2002, 7, 22, 14, 0, 0),
			COUNT
		);
		Test.stopTest();
		System.assertEquals(COUNT, processed, 'Count records to be processed');
	}
	@isTest
	private static void processNoScheduledEmails() {
		Datetime dt = DateTime.newInstance(2002, 7, 22, 14, 0, 0);
		ScheduleMailSender emailSender = new ScheduleMailSender();
		createScheduledEmail(emailSender, dt);
		Test.startTest();
		Datetime processingTime = DateTime.newInstance(2002, 7, 22, 13, 0, 0);
		Integer processed = emailSender.processScheduledEmails(
			processingTime,
			COUNT
		);
		Test.stopTest();
		System.assertEquals(0, processed, 'No Scheduled Emails');
	}

	private static void checkScheduleDateTime(
		ScheduleMailSender.ScheduleDateTime schedule,
		Date expectedDate,
		String expectedTime,
		String expectedAMOrPm
	) {
		System.assert(schedule != null, ' Could not be null');
		System.assertEquals(
			expectedDate,
			schedule.scheduledDate,
			'Date can be: Datetime.date()'
		);
		System.assertEquals(
			expectedTime,
			schedule.scheduledTime,
			'Time equal to the hour in the datetime'
		);
		System.assertEquals(
			expectedAMOrPm,
			schedule.amOrPm,
			'AM because is less than 12'
		);
	}
	private static void createScheduledEmail(
		ScheduleMailSender emailSender,
		Datetime scheduleDateTime
	) {
		ScheduleMailSender.ScheduleDateTime schedule = emailSender.getScheduleDateTime(
			scheduleDateTime
		);
		ScheduledEmailTestUtil.createScheduledMessagesForDraftEmails(
			schedule.scheduledDate,
			schedule.scheduledTime,
			schedule.amOrPm,
			scheduleDateTime
		);
	}
}