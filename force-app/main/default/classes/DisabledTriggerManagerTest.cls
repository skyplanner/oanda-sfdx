/**
 * @File Name          : DisabledTriggerManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/13/2020, 6:54:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/13/2020, 6:54:29 PM   acantero     Initial Version
**/
@isTest
private class DisabledTriggerManagerTest {

    @isTest
    static void test() {
        String objectApiName1 = 'SuperFakeObjectName1';
        String objectApiName2 = 'SuperFakeObjectName2';
        Test.startTest();
        DisabledTriggerManager manager = DisabledTriggerManager.getInstance();
        Boolean result1 = manager.triggerIsEnabledForObject(objectApiName1);
        manager.disabledTriggerSet = new Set<String>();
        Boolean result2 = manager.triggerIsEnabledForObject(objectApiName1);
        manager.disabledTriggerSet = null;
        manager.addDisableTriggerItem(objectApiName2);
        Boolean result3 = manager.triggerIsEnabledForObject(objectApiName1);
        Boolean result4 = manager.triggerIsEnabledForObject(objectApiName2);
        Test.stopTest();
        System.assertEquals(true, result1);
        System.assertEquals(true, result2);
        System.assertEquals(true, result3);
        System.assertEquals(false, result4);
    }
}