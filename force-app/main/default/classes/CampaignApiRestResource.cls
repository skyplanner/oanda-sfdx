/* Name: CampaignApiRestResource
 * Description : New Apex Class to get all campaigns related to user(live only)
 * Author: Mikolaj Juras
 * Date : 2024 January 5
 */

@RestResource(urlMapping='/api/v1/campaigns/*')
global with sharing class CampaignApiRestResource {

    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        String fxAccountTradeUserIdString = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        if (String.isBlank(fxAccountTradeUserIdString)) {
            res.statusCode = 400;
                String respBody = 'Invalid request - lack of userId in URL. ';
                res.responseBody = Blob.valueOf(respBody) ;
                return;
        } 
        Integer fxAccountTradeUserId = Integer.valueOf(fxAccountTradeUserIdString);
        Id liveRTId = Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get('Retail Live').getRecordTypeId();

        try {     
            List<Campaign> campaigns = new List<Campaign>();
            for (CampaignMember cm : [SELECT contact.account.fxAccount__r.Name, Campaign.Name,Campaign.Id, Contact.name 
                                      FROM CampaignMember 
                                      WHERE contact.account.fxAccount__r.fxTrade_User_Id__c  = :fxAccountTradeUserId 
                                          AND contact.account.fxAccount__r.RecordTypeId = :liveRTId]) {
                Campaign c = new Campaign(
                    Id = cm.Campaign.Id,
                    Name = cm.Campaign.Name
                );
                campaigns.add(c);
            }
            res.statusCode = 200;
            res.responseBody = Blob.valueOf(JSON.serialize(campaigns));
            return;
        } catch (Exception ex) {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Error: ' + ex.getMessage());
            return;
        }
    }
}