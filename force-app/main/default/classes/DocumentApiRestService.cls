/* Name: DocumentApiRestService
 * Description : New Apex Class to handle Document GET/POST actions from user-api
 * Author: Mikolaj Juras
 * Date : 2023 December 28
 */
@RestResource(urlMapping='/api/v1/user/*/document')
global without sharing class DocumentApiRestService {

    private final static String LOGGER_CATEGORY = 'document-get/post';
    private final static String LOGGER_CATEGORY_URL = '@RestResource(/api/v1/user/*/document)';
    private final static String MY_NUMBER_DOC_TYPE = 'My Number';

    private static final String DOC_RT_ID_SCAN = RecordTypeUtil.getScanDocumentTypeId();

    private static final Id FX_ACC_LIVE_RT = Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get('Retail Live').getRecordTypeId();
    private static final String COMPLY_ADVANTAGE_DOC_TYPE = 'Comply Advantage';

    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<UserDocument> userDocs = new List<UserDocument>();
        Integer fxTradeUserId;
        String reqBody = '';
        try {
            fxTradeUserId = getUserIdFromUrl(req.requestURI);
            if(req.requestBody != null){
            reqBody = req.requestBody.toString().trim();
            }
            for(Document__c doc : [SELECT Id, RecordType.Name,Document_Type__c, Name__c, name, Attempt_Number__c, 
                                        Activation_Decision__c,  Journey_Id__c , Document_Source__c, Expiry_Date__c
                                    FROM Document__c 
                                    WHERE fxAccount__r.fxTrade_User_Id__c  = :fxTradeUserId 
                                        AND fxAccount__r.RecordTypeId = : FX_ACC_LIVE_RT
                                        AND Document_Type__c != :COMPLY_ADVANTAGE_DOC_TYPE]) {
                UserDocument currentDoc = createUserDocument(doc);
                userDocs.add(currentDoc);
            }

            if(userDocs.size() == 0) {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf('No Documents found for user Id: ' + fxTradeUserId);
                Logger.error(
                    LOGGER_CATEGORY_URL,
                    LOGGER_CATEGORY,
                    reqBody);
            } else {
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(JSON.serialize(userDocs));
            }

        } catch (Exception ex) {
            res.statusCode = 500;
            String respBody = 'GET/POST Document fro user : ' + fxTradeUserId + 'Something went wrong: ' + ex.getMessage() + ex.getStackTraceString() + ex.getCause();
            res.responseBody = Blob.valueOf(respBody) ;
        }
        

    }
	/*
	 *POST logic
	 * get live Account(with related Doc for journey Id) with userId
	 * if there is no document create Document
	 * in async way create related attachemnt
	 */
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
       
        String reqBody = '';
        Integer fxTradeUserId;
        UserDocument ud;
        Document__c sfDoc;
        try {
            if(req.requestBody != null){
            reqBody = req.requestBody.toString().trim();
            }

            fxTradeUserId = getUserIdFromUrl(req.requestURI);

            ud = (UserDocument) JSON.deserialize(reqBody, UserDocument.class);
            if(ud.journey_id == null) {
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(' User id: ' + fxTradeUserId + ' Journey Id is required: ' + ud?.journey_id);
                return;
            }

            List<Document_Codes__c> docCodeLst = [SELECT DocId__c, Doc_Type__c FROM Document_Codes__c WHERE DocId__c =: ud.doc_type LIMIT 1]; 
            if(docCodeLst.isEmpty()) {
                Logger.error(
                    LOGGER_CATEGORY_URL,
                    LOGGER_CATEGORY,
                    'No code mapping present for : ' + ud.doc_type + ' User id: ' + fxTradeUserId);
            }

            List<fxAccount__c> fxAccs = getFxAccWithDocs(fxTradeUserId, ud.journey_id);
            if(fxAccs.isEmpty()) {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf('No fxAcc not found');
                return;
            }
            //create or update the document
            
            sfDoc = new Document__c();
            sfDoc.Id = fxAccs[0].Documents__r.size() > 0 ? fxAccs[0].Documents__r[0].Id : null;
            sfDoc.fxAccount__c = fxAccs[0].Id;
            sfDoc.Document_Type__c = !docCodeLst.isEmpty() ? docCodeLst[0].Doc_Type__c : null;
            sfDoc.Attempt_Number__c = ud.attempt_number;
            sfDoc.Journey_Id__c = ud.journey_id;
            sfDoc.Activation_Decision__c = setActivationDecisionStringInSalsforce(ud.activation_decision);
            sfDoc.Name = ud.file_name;
            sfDoc.Is_My_Number__c = sfDoc.Document_Type__c != MY_NUMBER_DOC_TYPE ? false : true;
            upsert sfDoc;

            insertAttachemnt(sfDoc.Id, ud.img_data, ud.file_name, ud.mime_type, ud.journey_id, fxTradeUserId);

            res.statusCode = 200;
            res.responseBody = Blob.valueOf('Document inserted correctly' + sfDoc + ' User Id: ' + fxTradeUserId + ' Document Id: ' + sfDoc.Id);
            return;
        } catch (Exception ex) { 
            String respBody = 'Something went wrong document will not be updated. Err msg: ' 
                                + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString() 
                                + ' User id: ' + fxTradeUserId
                                + ' Journey Id: ' + ud?.journey_id;
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                LOGGER_CATEGORY_URL,
                LOGGER_CATEGORY,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

    private static List<fxAccount__c> getFxAccWithDocs(Integer userId, String journeyId) {
        return [SELECT Id, 
                    (SELECT id, Attempt_Number__c, Journey_Id__c, Activation_Decision__c,
                        Document_Source__c, Document_Type__c, PhotoID_Document__c
                    FROM Documents__r WHERE Journey_Id__c = :journeyId) 
                FROM fxAccount__c 
                WHERE fxTrade_User_Id__c = : userId AND recordTypeId = :FX_ACC_LIVE_RT];
    }

    private static Integer getUserIdFromUrl(String url) {
        string urlBeforeAccounts = url.mid(0, url.indexOf('/document'));
        string[] urlFragments = urlBeforeAccounts.split('/');
        string userId = urlFragments[urlFragments.size()-1];
        return Integer.valueOf(userId);
    }

    private static String setActivationDecisionStringInSalsforce(String adText) {
        // do funny thing with paylaod to parse inner JSON...
        ActivationDecision ad = (ActivationDecision)JSON.deserialize(adText.replaceAll('\'', '"'), ActivationDecision.class);
        String adString = '';
        adString += 'Results: ';
        adString += ad?.pass != null && !ad.pass ? 'Fail' : 'Passed'; 
        adString += '\n' + 'Failure Reasons: ';
        adString += ad.failure_reasons;
        adString += '\n' + 'High-Level Result: ';
        adString += ad?.gbg_checks?.is_validated_pass != null && !ad.gbg_checks.is_validated_pass ? 'Fail' : 'Passed' ; 
        adString += '\n' +'Digital Tampering Check: ' ;
        adString += ad?.gbg_checks?.digital_tampering_pass != null && !ad.gbg_checks.digital_tampering_pass ? 'Fail' : 'Passed' ; 
        adString += '\n' +'Face Substitution Check: ' ;
        adString += ad?.face_sub_check != null && !ad.face_sub_check ? 'Fail' : 'Passed' ; 

        return adString;
    }

    @future
    public static void insertAttachemnt(Id parentId, String fileBody, String fileName, String mimeType, String journeyId, Integer userId){
        try {
            Attachment newAttachment = new Attachment();
            newAttachment.ParentId = parentId; // Link the attachment to a parent record
            newAttachment.Body = EncodingUtil.base64Decode(fileBody); // Set the content of the attachment
            newAttachment.Name = fileName; // Set the name of the attachment
            newAttachment.ContentType = mimeType;
            insert newAttachment;
        } catch (Exception ex) { 
            
            String errorMsg = 'Something went wrong ATTACHEMNT CREATION. Err msg: ' 
                                + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString() 
                                + ' Parent id: ' + parentId
                                + ' User Id/fxAcc trade user Id: ' + userId
                                + ' Journey Id: ' + journeyId
                                + ' File name ' + fileName;
            Logger.error(
                LOGGER_CATEGORY_URL,
                LOGGER_CATEGORY,
                errorMsg);
            return;
        }
        
    }


    private static UserDocument createUserDocument( Document__c d) {
        List<Document_Codes__c> docCodeLst = [SELECT DocId__c, Doc_Type__c FROM Document_Codes__c WHERE Doc_Type__c =: d.Document_Type__c LIMIT 1];
        UserDocument ud = new UserDocument();
        ud.doc_type = !docCodeLst.isEmpty() ? Integer.valueOf(docCodeLst[0].DocId__c) :  null;
        ud.activation_decision = d.Activation_Decision__c;
        ud.attempt_number = Integer.valueOf(d.Attempt_Number__c);
        ud.doc_source = d.Document_Source__c;
        //confirm date format!!!
        ud.expiry_date = String.valueOf(d.Expiry_Date__c);
        ud.file_name = d.Name;
        ud.journey_id  = d.Journey_Id__c;
        ud.mime_type = null;
        return ud;
    }

    global class UserDocument{
        public Integer doc_type {get; set;}
        public String file_name {get; set;}
        public String img_data {get; set;}
        public String mime_type {get; set;}
        public Integer attempt_number {get; set;}
        public String journey_id {get; set;}
        public String activation_decision {get; set;}
        public String doc_source {get; set;}
        public String expiry_date {get; set;}
    }

    public class GbgChecks
    {
        public Boolean high_level_result_pass { get; set; }
        public Boolean is_validated_pass { get; set; }
        public Boolean digital_tampering_pass { get; set; }
    }

    public class ActivationDecision
    {
        public Boolean pass { get; set; }
        public List<String> failure_reasons { get; set; }
        public UserCheck user_check { get; set; }
        public GbgChecks gbg_checks { get; set; }
        public Boolean face_sub_check { get; set; }
    }

    public class UserCheck
    {
        public string match_result { get; set; }
    }
}