/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-08-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchUpdatePaxosInAccountTest {
    @isTest
    static void updateAccounts() {
        Account acc = TestDataFactory.getPersonAccount(true);
		fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);

        acc.Paxos_Status__c = null;
        update acc;

        Test.StartTest();

        BatchUpdatePaxosInAccount b = new BatchUpdatePaxosInAccount(null);
        Id batchProcessId = Database.executeBatch(b);

        Test.stopTest();

        AsyncApexJob aaj = [
            SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
            FROM AsyncApexJob
            WHERE Id =: batchProcessId ];

        acc = [SELECT Id, Paxos_Status__c FROM Account WHERE Id = :acc.Id];

        System.assertEquals(true, aaj != null);
        System.assertEquals(1, aaj.TotalJobItems);
        System.assertEquals('Approved', acc.Paxos_Status__c);
    }
}