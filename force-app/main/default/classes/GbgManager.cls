/**
 * SP-9185
 * GBG integration manager class
 *  - Create Action platform events to MuleSoft consume
 *  - Update document gbg status to 'In-Progress'
 */
public with sharing class GbgManager {
    
    @testVisible
    static final String GBG_INPROGRESS_STATUS =
        'In-Progress';

    // Attachment list
    List<Attachment> atts;

    // Doc who need to be scanned with GBG
    Map<Id, Document__c> docsMap;

    // docIds who action platform event
    // were published successfully
    Set<Id> publishedDocs;

    // Manager class for action platform events
    ActionUtil actUtil;

    GbgSettings stts;

    /**
     * Constructor
     */
    public GbgManager(List<Attachment> atts) {
        stts = GbgSettings.getInstance();
        
        if(isGbgOn()){
            this.atts = atts;
            actUtil = new ActionUtil();
            System.debug('isGbgOn...');
        }
    }

    /**
     * Publish a action gbg platform event 
     * which mulesoft is subscribed
     */
    public void scan() {
        // Check if something to scan
        if(atts == null || atts.isEmpty())
            return;

        // Load right docs from attachments
        loadDocuments();

        // Publish action platform events
        // to trigger the scan process
        publish();

        // Update docs
        updateDocs();
    }

    /**
     * Only process if is gbg enabled 
     * and registration user
     */
    private Boolean isGbgOn() {
        return
            stts.isGbgEnabled() &&
            UserUtil.isRegistrationUser();
    }

    /**
     * Load right documents from attachments
     */
    private void loadDocuments() {
        // Get documents ids from attachments
        Set<Id> docIds =
            SObjectUtil.getFieldIds(
                'ParentId',
                atts);

        System.debug(
            'loadDocuments-docIds: ' +
            docIds);
        
        // Get divisions who have the
        // GBG integration enabled
        List<String> divisions =
            stts.getEnabledDivisions();

        System.debug(
            'loadDocuments-divisions: ' + 
            divisions);

        // Retrieve right docs from db
        docsMap = divisions.isEmpty() ?
            new Map<Id, Document__c>() :
            new Map<Id, Document__c>(
                [SELECT Id,
                        Case__c,
                        fxTrade_User_Id__c 
                    FROM Document__c
                    WHERE Id IN :docIds
                    AND fxTrade_User_Id__c != NULL
                    AND GBG_Status__c = NULL
                    AND (Case__r.fxAccount__r.Division_Name__c IN :divisions OR 
                        Case__r.Division__c IN :divisions)
                    AND RecordTypeId = 
                        :RecordTypeUtil.getScanDocumentTypeId()
                    AND Document_Type__c = :
                        Constants.DOC_TYPE_DRIVER_LICENSE
                    FOR UPDATE]);

        System.debug(
            'loadDocuments-docsMap: ' +
            docsMap);
    }

    /**
     * Publish gbg platform event
     */
    private void publish() {
        if(docsMap.isEmpty()) {
            return;
        }

        publishedDocs =
            actUtil.publishGbgActions(
                docsMap.values());

        System.debug(
            'publishedDocs: ' +
            publishedDocs);
    }

    /**
     * Update docs
     * - Set gbgStatus as 'In-Progress'
     */
    private void updateDocs() {
        if(publishedDocs == null || publishedDocs.isEmpty())
            return;

        Document__c doc;
        List<Document__c> docsToUpdt =
            new List<Document__c>();

        for(Id docId :publishedDocs) {
            doc = docsMap.get(docId);
            doc.GBG_Status__c =
                GBG_INPROGRESS_STATUS;
            
            docsToUpdt.add(doc);
        }

        if(docsToUpdt.isEmpty())
            return;

        System.debug(
            'docsToUpdt: ' +
            docsToUpdt);

        // Nothing to do on document trigger 
        // with this gbg status update
        TriggersUtil.disableObjTriggers(
            TriggersUtil.Obj.DOCUMENT);
        
        update docsToUpdt;
        
        TriggersUtil.enableObjTriggers(
            TriggersUtil.Obj.DOCUMENT);
    }
   
}