/**
 * Created by akajda on 17/05/2024.
 */

public with sharing class NoAnsweredTaskController {
    @AuraEnabled(Cacheable=false)
    public static String markTaskAsNotAnswered(String recordId){
        try{
            update new Task(Id=recordId, No_answer__c=true);
            return 'ok';
        }catch(Exception e){
            return e.getMessage();
        }
    }
}