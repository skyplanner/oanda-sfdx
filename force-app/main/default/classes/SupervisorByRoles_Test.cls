/**
 * @File Name          : SupervisorByRoles_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:26:05 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/25/2021, 2:51:26 PM   acantero     Initial Version
**/
@isTest(isParallel = false)
private without sharing class SupervisorByRoles_Test {

    @isTest
    static void test() {
        String userId;
        Boolean isEmpty;
        Set<String> supervisors;
        Map<String,Set<String>> roleSettings = new Map<String,Set<String>>();
        roleSettings.put(
            ServiceResourceTestDataFactory.CEO_ROLE_DEV_NAME, 
            new Set<String> {ServiceResourceTestDataFactory.CEO_ROLE_DEV_NAME}
        );
        Test.startTest();
        OmnichannelRoleSettings omniRoleSettings = new OmnichannelRoleSettings();
        omniRoleSettings.roleSettings = roleSettings;
        OmnichannelRoleSettings.instance = omniRoleSettings;
        //...
        User testUser = ServiceResourceTestDataFactory.createCEOUser('c0003');
        System.runAs(testUser) {
            userId = UserInfo.getUserId();
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(userId);
            //...
            SupervisorByRoles instance = new SupervisorByRoles();
            instance.addCriteria(userId);
            instance.getSupervisors();
            isEmpty = instance.isEmpty();
            supervisors = instance.getSupervisors(userId);
        }
        Test.stopTest();
        System.assertEquals(false, isEmpty);
        System.assert(supervisors.contains(userId));
    }
    
}