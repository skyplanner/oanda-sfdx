/**
 * @File Name          : OnNonHvcCaseReleasedCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/22/2024, 1:44:32 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/14/2022, 3:30:56 PM   acantero     Initial Version
**/
@IsTest
private without sharing class OnNonHvcCaseReleasedCmd_Test {

	@testSetup
    static void setup() {
        Case newCustomerLeadCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE,
            Routed__c = true
        );
        List<Case> caseList = new List<Case>{newCustomerLeadCase1};
        OmnichanelRoutingTestDataFactory.setupAllForNonHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    // *** test1 ***
    // valid non hvc case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_RELEASED
    // => the case is added to releasedCaseIdSet
    // *** test2 ***
    // valid non hvc case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE
    // => the case is added to releasedCaseIdSet and reroutedCaseIdSet
    // *** test3 ***
    // valid non hvc case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_CLOSED
    // => the case is not added to any list
    @IsTest
    static void checkRecord() {
        Case rec = getCase();
        Case oldRec = getCase();
        Test.startTest();
        // test 1
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED;
        OnNonHvcCaseReleasedCmd instance = new OnNonHvcCaseReleasedCmd();
        instance.checkRecord(rec, oldRec);
        Integer releasedCaseIdSetSize1 = instance.releasedCaseIdSet.size();
        Integer reroutedCaseIdSetSize1 = instance.reroutedCaseIdSet.size();
        // test 2
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE;
        instance = new OnNonHvcCaseReleasedCmd();
        instance.checkRecord(rec, oldRec);
        Integer releasedCaseIdSetSize2 = instance.releasedCaseIdSet.size();
        Integer reroutedCaseIdSetSize2 = instance.reroutedCaseIdSet.size();
        // test 3
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED;
        instance = new OnNonHvcCaseReleasedCmd();
        instance.checkRecord(rec, oldRec);
        Integer releasedCaseIdSetSize3 = instance.releasedCaseIdSet.size();
        Integer reroutedCaseIdSetSize3 = instance.reroutedCaseIdSet.size();
        Test.stopTest();
        // test 1 results
        System.assertEquals(1, releasedCaseIdSetSize1, 'Invalid result');
        System.assertEquals(0, reroutedCaseIdSetSize1, 'Invalid result');
        // test 2 results
        System.assertEquals(1, releasedCaseIdSetSize2, 'Invalid result');
        System.assertEquals(1, reroutedCaseIdSetSize2, 'Invalid result');
        // test 3 results
        System.assertEquals(0, releasedCaseIdSetSize3, 'Invalid result');
        System.assertEquals(0, reroutedCaseIdSetSize3, 'Invalid result');
    }

    // *** test1 ***
    // releasedCaseIdSet us empty => do nothing, return false
    // *** test2 ***
    // releasedCaseIdSet and reroutedCaseIdSet has a record 
    // => delete Non_HVC_Case__c record and fire reroute event and return true
    @IsTest
    static void execute() {
        Case rec = getCase();
        Case oldRec = getCase();
        Non_HVC_Case__c oldNonHvcObj = new Non_HVC_Case__c(
            Case__c = rec.Id,
            Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED
        );
        NonHVCCaseTriggerHandler.enabled = false;
        insert oldNonHvcObj;
        NonHVCCaseTriggerHandler.enabled = true;
        Test.startTest();
        // test 1
        OnNonHvcCaseReleasedCmd instance = new OnNonHvcCaseReleasedCmd();
        Boolean result1 = instance.execute();
        // test 2
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE;
        instance.checkRecord(rec, oldRec);
        Boolean result2 = instance.execute();
        Test.stopTest();
        List<Non_HVC_Case__c> oldNonHvcCaseList = [
            select Id from Non_HVC_Case__c where Id = :oldNonHvcObj.Id
        ];
        System.assertEquals(false, result1, 'Invalid result');
        System.assertEquals(true, result2, 'Invalid result');
        System.assert(
            oldNonHvcCaseList.isEmpty(), 
            'When releasing the parked record it must be deleted'
        );
    }

    @IsTest
    static void prepareToReleaseNonHvcCases() {
        Non_HVC_Case__c nonHvcCase1 = new Non_HVC_Case__c(
            Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_DEFAULT
        );
        Non_HVC_Case__c nonHvcCase2 = new Non_HVC_Case__c(
            Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED
        );
        Non_HVC_Case__c nonHvcCase3 = new Non_HVC_Case__c(
            Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED
        );
        List<Non_HVC_Case__c> nonHvcCaseList = new List<Non_HVC_Case__c> {
            nonHvcCase1, nonHvcCase2, nonHvcCase3
        };

        Test.startTest();
        OnNonHvcCaseReleasedCmd instance = new OnNonHvcCaseReleasedCmd();
        instance.prepareToReleaseNonHvcCases(nonHvcCaseList);
        Test.stopTest();
        
        Assert.areEqual(1, instance.recordsToUpdate.size(), 'Invalid result');
        Assert.areEqual(2, instance.recordsToDelete.size(), 'Invalid result');
        Assert.areEqual(
            OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED, 
            instance.recordsToUpdate[0].Status__c, 
            'The Status__c field must be changed from "Default" to "Released"'
        );
    }

    static Case getCase() {
        return [
            select 
                Id, 
                Agent_Capacity_Status__c, 
                Routed__c,
                Is_HVC__c
            from 
                Case
            limit 1
        ];
    }
    
}