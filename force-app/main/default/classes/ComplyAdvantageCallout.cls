/**
 * Generic callout with information needed for
 * all requests to the CA portal.
 * @author Fernando Gomez
 * @since 10/15/2021
 */
public abstract class ComplyAdvantageCallout extends Callout {
	protected String apiKey;
	protected String divisionName;
	protected String country;

	/**
	 * Main constructor
	 */
	public ComplyAdvantageCallout() {
		baseUrl = SPGeneralSettings.getInstance().getValue('CA_Endpoint');
	}

	/**
	 * Sets up the curent instance of CA
	 * @param divisionName
	 * @param country
	 */
	protected void setupInstance(String divisionName, String country) {
		apiKey = getAPiKey(divisionName, country);
	}

	/**
	 * Sets up the curent instance of CA
	 * @param apiKey
	 * @param country
	 */
	protected void setupInstance(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the url of the details endpoint
	 */
	protected Map<String, String> getBaseParams() {
		return new Map<String, String> {
			'api_key' => apiKey
		};
	}

	/**
	 * @return a new exception listing issues with the callout
	 */
	protected ComplyAdvantageException getCalloutException() {
		return new ComplyAdvantageException(
			'Response: ' + resp + '\n' +
			'Request: ' + req);
	}

	/**
	 * throws a failing exception
	 * @param ex
	 */
	protected ComplyAdvantageException fail(Exception ex) {
		Log l = Logger.exception(
			ex.getMessage() + '. ' + ex.getStackTraceString(),
			Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
			req,
			resp);
		return new ComplyAdvantageException(
			ex.getMessage() + '. Log Id: ' + l.id);
	}

	/**
	 * @param divisionName
	 * @param country
	 * @return the apik key for the division
	 */
	private String getAPiKey(String divisionName, String country) {
		List<Comply_Advantage_Division_Setting__c> divisionSettings = [
			SELECT Comply_Advantage_Instance_Setting__r.Name,
				Comply_Advantage_Instance_Setting__r.API_Key__c
			FROM Comply_Advantage_Division_Setting__c
			WHERE Name = :divisionName
		];

		if (divisionSettings.size() > 0) {
			Comply_Advantage_Division_Setting__c temp = divisionSettings[0];
			return temp.Comply_Advantage_Instance_Setting__r.API_Key__c;
		} else
			return null;
	}
}