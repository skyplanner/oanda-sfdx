/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 08-03-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCASSystemUpdateSync implements
    Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful, Database.RaisesPlatformEvents, BatchReflection {
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static Integer BATCH_SIZE = 100;
    public static String CRON_SCHEDULE = '0 0 0 * * ?';
    public static String GLO_MARKETS = 'OANDA Global Markets';

    public List<Comply_Advantage_Search__c> searches;

    public BatchCASSystemUpdateSync() {
        this(null);
    }

    public BatchCASSystemUpdateSync(String filter) {
        query = 'SELECT Id, Name, Is_Monitored__c, Search_Id__c, Is_Secondary_Search__c, ' +
        '   Search_Text__c, Search_Reason__c, Case__c, Affiliate__c, ' +
        '   Custom_Division_Name__c, Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__c, fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Search_Profile__c = NULL AND Is_Monitored__c = TRUE ' +
        (String.isNotBlank(filter) ? filter : '');

        System.debug(query);
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Name, Is_Monitored__c, Search_Id__c, Is_Secondary_Search__c, ' +
        '   Search_Text__c, Search_Reason__c, Case__c, Affiliate__c, ' +
        '   Custom_Division_Name__c, Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__c, fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public static void executeBatch() {
        Database.executeBatch(new BatchCASSystemUpdateSync(), BATCH_SIZE);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchCASSystemUpdateSync(), BATCH_SIZE);
    }

    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        searches = new List<Comply_Advantage_Search__c>();

        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>();
        for (Comply_Advantage_Search__c search : scope) {
            String apiKey = ComplyAdvantageUtil.getApiKey(search);

            if (String.isNotBlank(apiKey)) {
                search.Is_Monitored__c = updateProcess(search, apiKey);
                if (!search.Is_Monitored__c) {
                    searchesToUpdate.add(search);
                }
            }
        }

        if (!searchesToUpdate.isEmpty()) {
            Database.update(searchesToUpdate, false);
        }
    }

    public Boolean updateProcess(Comply_Advantage_Search__c search, String apiKey) {
        HttpResponse response = ComplyAdvantageUtil.getMonitoredInfo(search.Search_Id__c, apiKey);

        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>) results.get('content');
            return Boolean.valueOf(resWrapper?.get('is_monitored'));
        }

        return true;
    }

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

    public static String schedule(String schedule) {
        if (String.isNotBlank(schedule)) {
            CRON_SCHEDULE = schedule;
        }
        return System.schedule((
            Test.isRunningTest() ? 'Testing Batch BatchCASSystemUpdateSync' : 'Update CA Search: monitored'),
            CRON_SCHEDULE,
            new BatchCASSystemUpdateSync()
        );
    }
}