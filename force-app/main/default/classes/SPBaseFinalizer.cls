/**
 * @File Name          : SPBaseFinalizer.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2023, 2:34:05 PM
**/
public inherited sharing virtual class SPBaseFinalizer implements Finalizer {

	public void execute(FinalizerContext ctx) {
		Boolean error = (ctx.getResult() == ParentJobResult.UNHANDLED_EXCEPTION);
		System.debug('JobFinalizer.execute -> error: ' + error);
		Exception ex = (error == true) 
			? ctx.getException()
			: null;
		checkError(error, ex);
	}

	@TestVisible
	void checkError(Boolean error, Exception ex) {
		if (error == true) {
			onError(ex);

		} else {
			onSuccess();
		}
	}

	public virtual void onSuccess() {
		// can be overriden in subclasses
	}

	public virtual void onError(Exception ex) {
		// can be overriden in subclasses
	}

}