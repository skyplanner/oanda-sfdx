/**
 * @File Name          : DisabledTriggerManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/13/2020, 6:46:53 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/21/2020, 11:54:23 AM   acantero     Initial Version
**/
global without sharing class DisabledTriggerManager {

    static DisabledTriggerManager instance;
    @testVisible
    Set<String> disabledTriggerSet;

    @testVisible
    DisabledTriggerManager() {
        getDisabledTriggers();
    }

    global static DisabledTriggerManager getInstance() {
        if (instance == null) {
            instance = new DisabledTriggerManager();
        }
        return instance;
    }

    global Boolean triggerIsEnabledForObject(String objectApiName) {
        if (
            (disabledTriggerSet == null) ||
            (disabledTriggerSet.isEmpty())
        ) {
            return true;
        }
        //else...
        Boolean result = !disabledTriggerSet.contains(objectApiName);
        return result;
    }

    @testVisible
    void getDisabledTriggers() {
        List<Disabled_Trigger__mdt> disabledTriggerList = [
            SELECT 
                Object_API_Name__c 
            FROM 
                Disabled_Trigger__mdt
        ];
        for(Disabled_Trigger__mdt disabledTrigger : disabledTriggerList) {
            addDisableTriggerItem(disabledTrigger.Object_API_Name__c);
        }
    }

    @testVisible
    void addDisableTriggerItem(String objectApiName) {
        if (disabledTriggerSet == null) {
            disabledTriggerSet = new Set<String>();
        }
        disabledTriggerSet.add(objectApiName);
    }

}