/**
 * @File Name          : MessagingConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/18/2024, 12:09:24 PM
**/
public inherited sharing class MessagingConst {

	public static final String MSG_CHANNEL_DEV_NAME_SETTING = 
		'Messaging_Channel_DevName';

	public static final String LEAD_RECORD_TYPE_DEV_NAME_SETTING = 
		'Messaging_Lead_Record_Type_Dev_Name';

	public static final String CASE_RECORD_TYPE_DEV_NAME_SETTING = 
		'Messaging_Case_Record_Type_Dev_Name';

	public static final String CASE_QUEUE_DEV_NAME_SETTING = 
		'Messaging_Case_Queue_Dev_Name';

	public static final String MSG_DEBUG_MODE_SETTING = 
		'Messaging_Debug_Mode';

	public static final String MSG_DEFAULT_LANGUAGE_SETTING = 
		'Messaging_Default_Language';

	public static final String MSG_DEFAULT_REGION_SETTING = 
		'Messaging_Default_Region';

	public static final String MSG_SKILL_DEV_NAME_SETTING = 
		'Messaging_Skill_DevName';

	public static final String MSG_AUTH_EMAIL_TEMPLATE_SETTING = 
		'Messaging_Auth_Email_Template_Dev_Name';

	public static final String STANDARD_LEAD_REC_TYPE_DEV_NAME = 
		'Standard_Lead';

	public static final String ACCOUNT_DELETE_INTENT = 'ACCOUNT_DELETE';

	public static final String EMBEDDED_MESSAGING_CHANNEL = 'EmbeddedMessaging';

	public static final String TEXT_CHANNEL = 'Text';

	public static final String WHATSAPP_CHANNEL = 'WhatsApp';

	public static final String FACEBOOK_CHANNEL = 'Facebook';

}