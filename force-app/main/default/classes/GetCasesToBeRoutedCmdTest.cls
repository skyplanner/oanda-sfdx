/**
 * @File Name          : GetCasesToBeRoutedCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 9:24:26 PM
**/
@IsTest
private without sharing class GetCasesToBeRoutedCmdTest {

	// test 1 : recList is null => return false
	// test 2 : recList is empty => return false
	@IsTest
	static void execute1() {
		List<Case> emptyList = new List<Case>();
		
		Test.startTest();
		// test 1
		GetCasesToBeRoutedCmd instance1 = new GetCasesToBeRoutedCmd(
			null, // recList
			null // oldMap
		);
		List<Case> result1 = instance1.execute();
		// test 2
		GetCasesToBeRoutedCmd instance2 = new GetCasesToBeRoutedCmd(
			emptyList, // recList
			null // oldMap
		);
		List<Case> result2 = instance2.execute();
		Test.stopTest();
		
		Assert.isTrue(result1.isEmpty(), 'Invalid result');
		Assert.isTrue(result2.isEmpty(), 'Invalid result');
	}

	// valid case, Scenario 1
	// Origin = CASE_ORIGIN_EMAIL_FRONTDESK
	// Subject is blank, Description is blank
	@IsTest
	static void execute2() {
		List<Case> caseList = new List<Case>{ 
			new Case (
				Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
			)
		};
		
		Test.startTest();
		GetCasesToBeRoutedCmd instance = new GetCasesToBeRoutedCmd(
			caseList, // recList
			null // oldMap
		);
		List<Case> result = instance.execute();
		Test.stopTest();
		
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}

	// valid case, Scenario 2
	// Origin = CASE_ORIGIN_CHATBOT_EMAIL
	// OwnerId = skillCasesQueueId
	@IsTest
	static void execute3() {
		CaseRoutingManager routingManager = CaseRoutingManager.getInstance();
		ID skillCasesQueueId = routingManager.getSkillCasesQueueId();
		List<Case> caseList = new List<Case>{ 
			new Case (
				Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL,
				OwnerId = skillCasesQueueId
			)
		};
		
		Test.startTest();
		GetCasesToBeRoutedCmd instance = new GetCasesToBeRoutedCmd(
			caseList, // recList
			null // oldMap
		);
		instance.skillCasesQueueId = skillCasesQueueId;
		List<Case> result = instance.execute();
		Test.stopTest();
		
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}
	
}