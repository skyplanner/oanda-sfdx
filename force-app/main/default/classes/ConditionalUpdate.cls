/**
 * Handles each one of the workflow updates for an object.
 * Most of these updates will happen on a before-triggers. There should be
 * a method for each conditional update and a mapping to call the update
 * dynamically based on the workflow name.
 * @author Fernando Gomez
 */
public abstract class ConditionalUpdate {
	private SObject newRecord;
	private	SObject oldRecord;

	/**
	 * @param newRecord
	 * @param oldRecord
	 */
	public ConditionalUpdate(SObject newRecord, SObject oldRecord) {
		this.newRecord = newRecord;
		this.oldRecord = oldRecord;
	}

	/**
	* @param updater
	* @param oldfxAccounts
	*/
	public static void performConditionalUpdates(
			List<ConditionalUpdate> updates,
			List<String> workflows) {
		SPGeneralSettings settings;

		// wheather workflows are active or not is now
		// specified in the General Setting.
		// Record starting with 'WF_'
		settings = SPGeneralSettings.getInstance();

		// we perform each flow
		for (String wfName : workflows)
			// only if its Active
			if (settings.getValueAsBoolean(wfName))
				// we work for every record
				for (ConditionalUpdate updater : updates)
					// 1. execute if needed
					updater.execute(wfName);
	}

	/**
	 * Performs the action in the workflow with the specified name
	 * @param workflowName
	 */
	public abstract void execute(String workflowName);

	/**
	 * @return true if the record is new
	 */
	protected Boolean isNew() {
		return newRecord != null && oldRecord == null;
	}

	/**
	 * @return true if the record is new
	 */
	protected Boolean isBlank(String fieldName) {
		return String.isBlank(getString(newRecord, fieldName));
	}
	
	/**
	 * @param String
	 * @return true if the field has changed
	 */
	protected Boolean isChanged(String fieldName) {
		return SObjectUtil.isRecordFieldsChanged(
			newRecord, oldRecord, fieldName);
	}

	/**
	 * @param String
	 * @return true if the field has changed
	 */
	protected Boolean isChangedBoolean(String fieldName) {
		return getBoolean(newRecord, fieldName) != getBoolean(oldRecord, fieldName);
	}

	/**
	 * @param String
	 * @return true if the field has changed
	 */
	protected Boolean isChangedDecimal(String fieldName) {
		return getDecimal(newRecord, fieldName) != getDecimal(oldRecord, fieldName);
	}

	/**
	 * @param String
	 * @return true if the field has changed
	 */
	protected Boolean isChangedString(String fieldName) {
		return getString(newRecord, fieldName) != getString(oldRecord, fieldName);
	}

	/**
	 * @param String
	 * @return true if the field has changed
	 */
	protected Boolean isChangedDatetime(String fieldName) {
		return getDatetime(newRecord, fieldName) != getDatetime(oldRecord, fieldName);
	}

	/**
	 * @return the value in the specified field as Boolean
	 * @param fieldName
	 */
	protected Boolean getBoolean(SObject record, String fieldName) {
		Object v = record.get(fieldName);
		return v == null ? null : Boolean.valueOf(v);
	}

	/**
	 * @return the value in the specified field as Decimal
	 * @param fieldName
	 */
	protected Decimal getDecimal(SObject record, String fieldName) {
		Object v = record.get(fieldName);
		return v == null ? null : (Decimal)v;
	}

	/**
	 * @return the value in the specified field as Integer
	 * @param fieldName
	 */
	protected Integer getInteger(SObject record, String fieldName) {
		Object v = record.get(fieldName);
		return v == null ? null : Integer.valueOf(v);
	}

	/**
	 * @return the value in the specified field as String
	 * @param fieldName
	 */
	protected String getString(SObject record, String fieldName) {
		Object v = record.get(fieldName);
		return v == null ? null : String.valueOf(v);
	}

	/**
	 * @return the value in the specified field as DateTime
	 * @param fieldName
	 */
	protected Datetime getDatetime(SObject record, String fieldName) {
		Object v = record.get(fieldName);
		return v == null ? null : Datetime.valueOf(v);
	}
}