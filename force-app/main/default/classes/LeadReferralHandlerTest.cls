@isTest
private class LeadReferralHandlerTest {

	@testSetup 
	static void setup() {
		
    }

    static testMethod void LeadReferralTest() {
        	
        	 //test to refer a lead
	        Lead ld = new Lead(LastName='test lead', Email='foo@example.org', RecordTypeId=RecordTypeUtil.getLeadStandardId(), ownerId = UserInfo.getUserId());
	        insert ld;
	        
	        Lead ld0 = [select id, LastName, Email, ownerId, Division_Name__c, CX_Referrer__c, RecordTypeId from Lead where Id = : ld.Id limit 1];
	        Id currUserId = ld0.Id;
	        
	        List<Lead> ldList = new List<Lead>();
	        ldList.add(ld);
	        
	        List<String> results = LeadReferralHandler.referLeadToSales(ldList);
	        System.assertEquals(LeadReferralHandler.MESSAGE_MISSING_INFO, results[0]);
	        
	        ld.Division_Name__c = 'Default';
	        results = LeadReferralHandler.referLeadToSales(ldList);
	        System.assertEquals(LeadReferralHandler.MESSAGE_MISSING_INFO, results[0]);
	        
	        
	        ld.Division_Name__c = 'OANDA Corporate';
	        results = LeadReferralHandler.referLeadToSales(ldList);
	        System.assertEquals(LeadReferralHandler.MESSAGE_SUCCESS, results[0]);
	        
	        ld = [select id, LastName, Email, ownerId, Division_Name__c, CX_Referrer__c from Lead where Id = : ld.Id limit 1];
	        System.assertEquals(UserInfo.getUserId(), ld.CX_Referrer__c);
	        
            User randomUser = UserUtil.getRandomTestUser();
            ld.CX_Referrer__c = randomUser.Id;
            List<Lead> ldList1 = new List<Lead>();
	        ldList1.add(ld);
            
	        results = LeadReferralHandler.referLeadToSales(ldList1);
	        System.assertEquals(LeadReferralHandler.MESSAGE_SUCCESS, results[0]);
	        
	        Lead ld1 = [select id, LastName, Email, ownerId, Division_Name__c, CX_Referrer__c from Lead where Id = : ld.Id limit 1];
	        System.assertEquals(randomUser.Id, ld1.CX_Referrer__c);
        
    }
}