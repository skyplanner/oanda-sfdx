@isTest
private class CustomSettingTest {

    static testMethod void regionByCountryTest() {
        
        Country_Setting__c s1 = new Country_Setting__c();
        s1.name = 'Canada';
        s1.EEA__c = false;
        s1.Group__c = 'Canada';
        s1.ISO_Code__c = 'ca';
        s1.Prohibited__c = false;
        s1.Region__c = 'North America';
        s1.Zone__c = 'Americas';
        
        insert s1;
        
        
        Country_Setting__c s2 = new Country_Setting__c();
        s2.name = 'Macedonia, The Forme';
        s2.EEA__c = false;
        s2.Group__c = 'Europe';
        s2.ISO_Code__c = 'mk';
        s2.Prohibited__c = false;
        s2.Region__c = 'Europe & Central Asia';
        s2.Zone__c = 'EMEA';
        s2.Long_Name__c = 'Macedonia, The Former Yugoslav Republic of';
        
        insert s2;
        
        String region1 = CustomSettings.getRegionByCountry('Canada');
        System.assertEquals('North America', region1);
        
        String region2 = CustomSettings.getRegionByCountry('Macedonia, The Former Yugoslav Republic of');
        System.assertEquals('Europe & Central Asia', region2);
        
    }
    
    static testMethod void languageByCodeTest(){
        Language_Code__c lc = new Language_Code__c();
        
        lc.name = 'EN';
        lc.language__c = 'English';
        
        insert lc;
        
        String lan = CustomSettings.getLanguageByCode('EN');
        System.assertEquals('English', lan);
        
    }

    static testMethod void testValidateCountryName() {
        Country_Setting__c countrySetting = new Country_Setting__c(Name='Canada', Group__c='Canada', ISO_Code__c='ca', Region__c='North America', Zone__c='Americas', Risk_Rating__c='LOW');
        
        Lead lead1 = new Lead(FirstName='test1', LastName='test1', Email='test@test.com', recordTypeId=RecordTypeUtil.getLeadRetailId());
        Account account1 = new Account(FirstName='test1', LastName='test1', recordTypeId=RecordTypeUtil.getPersonAccountRecordTypeId());
        Lead lead2 = new Lead(FirstName='test2', LastName='test2', Email='test@test.com', recordTypeId=RecordTypeUtil.getLeadRetailId());
        Account account2 = new Account(FirstName='test2', LastName='test2', recordTypeId=RecordTypeUtil.getPersonAccountRecordTypeId());
        
        insert countrySetting;
        insert lead1;
        insert account1;
        insert lead2;
        insert account2;
        
        Test.startTest();
        checkRecursive.SetOfIDs = new Set<Id>();

        // Country name in Lead is correct
        lead1 = [SELECT Id, Country FROM Lead WHERE FirstName = 'test1'];
        lead1.Country = 'Canada';
        update lead1;
        checkRecursive.SetOfIDs = new Set<Id>();

        // Country name exists in Account has different capitalization
        account1 = [SELECT Id, PersonMailingCountry FROM Account WHERE FirstName = 'test1'];
        account1.PersonMailingCountry = 'cAnAdA';
        update account1;
        
        // Country name in Lead does not exist in Country_Setting__c
        Boolean expectedExceptionThrown = false;
        try {
            lead2 = [SELECT Id, Country FROM Lead WHERE FirstName = 'test2'];
            lead2.Country = 'Canadaa';
            update lead2;
            checkRecursive.SetOfIDs = new Set<Id>();

        } catch(Exception e) {
            expectedExceptionThrown = e.getMessage().contains('Canadaa is not a valid country name. Did you mean Canada?');
        }
        
        // Current user is System User
        System.runAs(UserUtil.getSystemUserForTest()) {
            account2 = [SELECT Id, PersonMailingCountry FROM Account WHERE FirstName = 'test2'];
            account2.PersonMailingCountry = 'Canadaa';
            update account2;
        }
        Test.stopTest();

        lead1 = [SELECT Id, Country FROM Lead WHERE FirstName = 'test1'];
        account1 = [SELECT Id, PersonMailingCountry FROM Account WHERE FirstName = 'test1'];
        account2 = [SELECT Id, PersonMailingCountry FROM Account WHERE FirstName = 'test2'];
        
        System.assertEquals('Canada', lead1.Country);
        System.assertEquals('Canada', account1.PersonMailingCountry);
        System.assert(expectedExceptionThrown);
        System.assertEquals('Canadaa', account2.PersonMailingCountry);
    }
}