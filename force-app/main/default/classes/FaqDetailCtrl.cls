/**
 * @File Name          : FaqDetailCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/26/2020, 5:34:06 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/23/2020   acantero     Initial Version
**/
public without sharing class FaqDetailCtrl {

    public static final String ANSWERS_SUPPORT_KEY = '/AnswersSupport?';
    public static final String URL_NAME_KEY = 'urlName=';

    public static final String ID_PARAM = 'id';
    public static final String URL_NAME_PARAM = 'urlName';
    public static final String CONSOLE_PARAM = 'capp';
    public static final String SOURCE_URL_PARAM = 'surl';
    public static final String LANGUAGE_PARAM = 'language';
    
    public static final String DEFAULT_LANGUAGE= 'en_US';

    public static final String CONSOLE_PARAM_VALUE = '1';

    public String title { get; private set; }
    public String baseURL { get; private set; }
    public String articleId { get; private set; }
    public String consoleApp { get; private set; }
    public String language { get; private set; }
    public Boolean isPreviewAvailable { get; private set; }
    @testvisible
    public String errorMessage { get; private set; }
    public ArticleWrapper currentArticle { get; private set; }


    public FaqDetailCtrl() {
        isPreviewAvailable = false;
        title = '';
        try {
            PageReference current = ApexPages.currentPage();
            if (current != null) {
                FAQ__kav articleObj = getFaq(current.getParameters());
                if (articleObj != null) {
                    isPreviewAvailable = true;
                    articleId = articleObj.Id;
                    title = articleObj.Title;
                    language = articleObj.Language;
                    //trick
                    articleObj.AllowFeedback__c = false;
                    FAQ faqObj = new FAQ(articleObj, null, null, true);
                    currentArticle = new ArticleWrapper(
                        articleObj.Title,
                        fixArticleLinks(faqObj.answer) //getValidHtml
                    );
                }
            }
        } catch (Exception ex) {
            errorMessage = ex.getMessage() + '<br />' + ex.getStackTraceString();
			System.debug('ERROR: ' + errorMessage);
        }
    }

    @testVisible
    private FAQ__kav getFaq(Map<String, String> params) {
        FAQ__kav result = null;
        baseURL = params.get(SOURCE_URL_PARAM);
            consoleApp = (params.get(CONSOLE_PARAM) == CONSOLE_PARAM_VALUE)
                ? '1'
                : '0';
        articleId = params.get(ID_PARAM);
        if (String.isNotBlank(articleId)) {
            result = getFaqById(articleId);
        } else {
            String articleUrl = params.get(URL_NAME_PARAM);
            language = params.get(LANGUAGE_PARAM);
            if (String.isBlank(language)) {
                language = DEFAULT_LANGUAGE;
            }
            if (String.isNotBlank(articleUrl)) {
                result = getFaqByUrlName(articleUrl, language);
                if (
                    (result == null) &&
                    (language != DEFAULT_LANGUAGE)
                ) {
                    language = DEFAULT_LANGUAGE;
                    result = getFaqByUrlName(articleUrl, language);
                }
            }
        }
        return result;
    }
    
    @testVisible
    private FAQ__kav getFaqById(String artId) {
        FAQ__kav result = null;
		List<FAQ__kav> faqList = [
            SELECT 
                Id,
                KnowledgeArticleId,
				Title,
				Answer__c,
                Language,
                AllowFeedback__c
            FROM 
                FAQ__kav
            WHERE 
                Id = :artId
        ];
        if (!faqList.isEmpty()) {
            result = faqList[0];
        }
        //else...
        return result;
    }

    @testVisible
	private static FAQ__kav getFaqByUrlName(String urlName, String lang) {
        FAQ__kav result = null;
		List<FAQ__kav> faqList = [
			SELECT 
                Id,
                KnowledgeArticleId,
				Title,
				Answer__c,
                Language,
                AllowFeedback__c
            FROM 
                FAQ__kav
            WHERE 
                PublishStatus = 'Online'
            AND 
                UrlName = :urlName
            AND 
                Language = :lang
		];
		if (!faqList.isEmpty()) {
            result = faqList[0];
        }
        //else...
        return result;
	}

    @testVisible
    private String fixArticleLinks(String html) {
        System.debug('fixArticlehtml -> begin');
        Integer index = 0;
        if (String.isBlank(html)) {
            return null;
        }
        //else...
        Boolean changed = false;
        do {
            Integer pos = html.indexOf('<a ', index);
            if (pos == -1) {
                break;
            } 
            //else...
            Integer pos2 = html.indexOf('>', pos);
            if (pos2 == -1) {
                //malformed
                break;
            }
            //else...
            String linkText = html.substring(pos, pos2);
            if (
                linkText.contains(ANSWERS_SUPPORT_KEY) &&
                linkText.contains(URL_NAME_KEY)
            ) {
                Integer pos3 = linkText.indexOf(URL_NAME_KEY);
                Integer pos4 = linkText.indexOf('&amp;', pos3 + 1);
                if (pos4 == -1) {
                    pos4 = linkText.indexOf('"', pos3 + 1);
                }
                if (pos4 != -1) {
                    String articleTitle = linkText.substring(
                        pos3 + URL_NAME_KEY.length(), 
                        pos4
                    );
                    String newLinkText = '<a art href="#' + articleTitle + '"';
                    html = html.replace(linkText, newLinkText);
                }
            }
            index = pos + 1;
        } while(true);
        return html;
    }

    public class ArticleWrapper { 
		public String question { get; set; }
        public String html { get; set; }
        
        public ArticleWrapper(
            String question,
            String html
        ) {
            this.question = question;
            this.html = html;
        }
	}
}