/**
 * Controller for Legal agreement list component
 * 
 * @author Gilbert Gao
 */
public with sharing class LegalAgreementListController {
    
    public class LegalAgreement{
        
        @AuraEnabled
        public String name;
        
        @AuraEnabled
        public String version;
        
        @AuraEnabled
        public String language;
        
        @AuraEnabled
        public String country;
        
        @AuraEnabled
        public String create_datetime;
        
        @AuraEnabled
        public String update_datetime;
    }
    
    @AuraEnabled
    public static List<LegalAgreement> getAgreements(ID fxaId){
        List<LegalAgreement> agreements = new List<LegalAgreement>();
        
        System.debug('fxAccount Id: ' + fxaId);
        
        List<fxAccount__c> fxas = [select Id, Legal_Consent__c from fxAccount__c where Id = :fxaId];
        
        if(fxas.size() > 0 && fxas[0].Legal_Consent__c != null){
            agreements = (List<LegalAgreement>)JSON.deserialize(fxas[0].Legal_Consent__c, List<LegalAgreement>.class);
            
            if(agreements != null){
                
                for(LegalAgreement agreement : agreements){
                    Legal_Agreement_Mapping__c mapping = Legal_Agreement_Mapping__c.getValues(agreement.name);
                    
                    agreement.name = (mapping != null ? mapping.Display_Name__c : agreement.name);
                    agreement.language = (agreement.language != null ? CustomSettings.getLanguageByCode(agreement.language) : null);
                    agreement.country = (agreement.country != null ? CustomSettings.getCountrySettingByCode(agreement.country).Name : null);                    
                }
            }
        }
        
        return agreements;
    }
    
    @AuraEnabled
    public static String getFxAccountName(ID fxaId){
        String result = '';
        
        System.debug('fxAccount Id: ' + fxaId);
        
        List<fxAccount__c> fxas = [select Id, name from fxAccount__c where Id = :fxaId];
        
        if(fxas.size() > 0){
            result = fxas[0].name;
        }
        
        return result;
    }

}