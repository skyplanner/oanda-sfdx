public with sharing class TasUserAccountPatchRequest {
    
    public Integer newPositionsLocked {get; set;}

    public static TasUserAccountPatchRequestWrapper getLockNewPositionsWrapper() {
        TasUserAccountPatchRequestWrapper result 
            = new TasUserAccountPatchRequestWrapper();
        result.request = new TasUserAccountPatchRequest();
        result.request.newPositionsLocked = 1;

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_NEW_POSITIONS_LOCKED, false, true);
        return result;
    }    

    public static TasUserAccountPatchRequestWrapper getUnlockNewPositionsWrapper() {
        TasUserAccountPatchRequestWrapper result 
            = new TasUserAccountPatchRequestWrapper();
        result.request = new TasUserAccountPatchRequest();
        result.request.newPositionsLocked = 0;

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_NEW_POSITIONS_LOCKED, true, false);
        return result;
    }

    public class TasUserAccountPatchRequestWrapper {
        public TasUserAccountPatchRequest request {get; set;}

        public AuditTrailManager.AuditTrailChange change {get; set;}
    }
}