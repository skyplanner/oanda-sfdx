@IsTest
private class ComplyAdvantageSearchEntityTest {

    /* @TestSetup
    static void TestDataSetup() {
        Lead[] leads = new Lead[]
        {
                new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com'),
                new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test2@oanda.com')
        };
        insert leads;

        fxAccount__c[] fxAccounts = new fxAccount__c[]
        {
                new fxAccount__c(Account_Email__c = 'test1@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[0].Id,Division_Name__c = 'OANDA Canada'),
                new fxAccount__c(Account_Email__c = 'test2@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[1].Id,Division_Name__c = 'OANDA Canada')
        };
        insert fxAccounts;

        Case obCase = new Case(Subject = 'Application Review for Robert Gabriel Mugabe',
                Lead__c = leads[0].Id,
                RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                fxAccount__c =fxAccounts[0].Id);
        insert obCase;

        Case complyCase = new Case(Lead__c = leads[0].Id,
                Subject  = 'Comply Advantage Case for Robert Gabriel Mugabe',
                fxAccount__c =fxAccounts[0].Id,
                parentId = obCase.Id,
                status = 'Closed',
                RecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId());
        insert complyCase;

        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = 'Test_Comply_Advantage_Search';
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;

        Comply_Advantage_Search_Entity__c entity =
                new Comply_Advantage_Search_Entity__c(
                        Comply_Advantage_Search__c = comAdvSearchInfo.Id,
                        Unique_ID__c = comAdvSearchInfo.Search_Id__c + ':IWUFH46IEHR',
                        Entity_Id__c = 'IWUFH46IEHR',
                        Match_Status__c = 'potential_match'
                );

        insert entity;
    }
    @IsTest
    static void calculateTotalHitsTest() {
        List<Comply_Advantage_Search__c> actualResult = [SELECT Id, Not_Reviewed_Hits__c, Reviewed_Hits__c FROM Comply_Advantage_Search__c];
        SYstem.assertEquals(1, actualResult[0].Not_Reviewed_Hits__c);
    }

    @IsTest
    static void calculateTotalHitsTestUpdate() {
        Comply_Advantage_Search_Entity__c entity = [SELECT Id FROM Comply_Advantage_Search_Entity__c][0];
        entity.Match_Status__c = 'reviewed';
        update entity;
        List<Comply_Advantage_Search__c> actualResult = [SELECT Id, Not_Reviewed_Hits__c, Reviewed_Hits__c FROM Comply_Advantage_Search__c];
        System.assertEquals(0, actualResult[0].Not_Reviewed_Hits__c);
        System.assertEquals(1, actualResult[0].Reviewed_Hits__c);
    } */
}