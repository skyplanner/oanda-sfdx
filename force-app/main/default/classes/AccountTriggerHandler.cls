/**
 * This class handles the Account triggers events
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   26/02/2021
 */
public class AccountTriggerHandler {

    final List<Account> newList;
    final Map<Id, Account> oldMap;
    final Map<Id, fxAccount__c> fxAccMap;
    final Id personAccRecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId();

    private OutgoingNotificationBus outgoingNotification;

    /**
     * Constructor
     */
    public AccountTriggerHandler(
        List<Account> newList,
        Map<Id, Account> oldMap)
    {
        this.newList = newList;
        this.oldMap = oldMap;
        
        // Get a fxAccount map
        this.fxAccMap = AccountUtil.getFxAccs(newList);

        this.outgoingNotification = new OutgoingNotificationBus();
    }

    /**
     * Handles the before insert event for the Account trigger
     */
    public void onBeforeInsert()
    {
        fxAccount__c fxAcc;

		// SP-8676: we need to make sure emails are
		// not already in use, for all users but the integration user
		if (!UserUtil.isIntegrationUser())
			for (SObject notUnique : 
					new CustomUniquenessValidator(newList, 'PersonEmail')
						.collect()
						.getNotUnique())
				notUnique.addError(
					StringUtil.format(
						Label.Account_Error_Email_Already_Exists,
						notUnique.get('PersonEmail')));

		// Set several vields values before insert
		for(Account acc : newList) 
			if (!acc.hasErrors()) {
				// Get fxAccount
				fxAcc = (fxAccount__c)SObjectUtil.getSobjMapVal(
					acc.fxAccount__c, fxAccMap);
				
				// Set account birth date
				AccountUtil.setBirthDate(acc);
				
				// Set account tax id
				AccountUtil.setTaxId(acc, fxAcc);
				
				// Set account lead conversion owner
				acc.Lead_Conversion_Owner__c = acc.OwnerId;
			
				// Set demo conversion scorre snapshot
				AccountUtil.setDemoConversionScoreSnapshot(acc);
                // SP13106 set OB check based on flags
                setOBcheckBasedOnKYC(acc);

                // Clear phone numbers
                acc.Phone_DR__c = clearPhoneNumber(acc.Phone);
			}

        stateAlbertaValidation();
    }

    /**
     * Handles the before update event for Account trigger
     */
    public void onBeforeUpdate()
    {
		List<Account> emailChanged;
		fxAccount__c fxAcc;

		// SP-8676, we need to make sure emails are
		// not already in use, for all users but the integration user
		if (!UserUtil.isIntegrationUser()) {
			emailChanged = getEmailChanged();

			for (SObject notUnique : 
					new CustomUniquenessValidator(emailChanged, 'PersonEmail')
						.collect()
						.getNotUnique())
				notUnique.addError(
					StringUtil.format(
						Label.Account_Error_Email_Already_Exists,
						notUnique.get('PersonEmail')));
		}

		// Set several vields values before update
		for(Account acc :newList) 
			if (!acc.hasErrors()) {
				// Get fxAccount
				fxAcc = (fxAccount__c)SObjectUtil.getSobjMapVal(
					acc.fxAccount__c, fxAccMap);

				// Set account birthdate
				AccountUtil.setBirthDate(acc);
				
				// Set account tax id
				AccountUtil.setTaxId(acc, fxAcc);

                // Collect cleared phone numbers
                if(acc.Phone!=oldMap.get(acc.Id).Phone) acc.Phone_DR__c = clearPhoneNumber(acc.Phone);
			}

        stateAlbertaValidation();
    }

    /**
     * Handles the after insert event for the Account trigger
     */
    public void onAfterInsert(){
        // Roll down util
        AccountRollDownUtil accRollDownUtil =
            new AccountRollDownUtil();
        
        // Iterate trigger records
        for(Account acc :newList) {
            // Check if roll down is needed
            accRollDownUtil.verify(acc);

            // Add more here...
        }

        // Do processes

        accRollDownUtil.rollDown();
       
        // Add more here...
    }

    /**
     * Handles the after update event for the Account trigger
     */
    public void onAfterUpdate(){
        // Helper structures
        Account oldAcc;
        fxAccount__c fxAcc;
        // Sync util class
        AccountSyncUtil syncUtil =
            new AccountSyncUtil();
        // Roll down util
        AccountRollDownUtil accRollDownUtil =
            new AccountRollDownUtil();
        // Ownership util
        AccountOwnershipUtil ownershipUtil =
            new AccountOwnershipUtil();
        
        // Iterate trigger records
        for(Account acc :newList) {
            // Fill helper structures
            oldAcc = oldMap.get(acc.Id);
            fxAcc = (fxAccount__c)SObjectUtil.getSobjMapVal(
                acc.fxAccount__c, fxAccMap);
           
            // Verify if sync is needed
            syncUtil.verify(acc, oldAcc, fxAcc);
    
            // Check if roll down is needed
            accRollDownUtil.verify(acc, oldAcc);

            // Check if something to do regarding ownership
            ownershipUtil.verify(acc, oldAcc);

            TrustedContactPersonUtil.checkFlagChange(oldAcc, acc);
            // Add more here...
        }

        // Do processes

        // Sync accounts
        syncUtil.sync();
        
        // Roll down
        accRollDownUtil.rollDown();

        // Do process regarding ownership 
        ownershipUtil.process();

        enqueueUserApiIntroducingBrokerUpdate(newList, oldMap);

        TrustedContactPersonUtil.deleteTCPRecords();
    }

	/**
	 * @return accounts that changed email
	 */
	private List<Account> getEmailChanged() {
		List<Account> result = new List<Account>();
		for (Account acc : newList)
			if (String.isNotBlank(acc.PersonEmail) &&
					acc.PersonEmail != oldMap.get(acc.Id).PersonEmail)
				result.add(acc);

		return result;
	}


    public void stateAlbertaValidation() {
        for (Account acc : newList) {
            if (acc.PersonMailingState == 'Alberta' || acc.PersonMailingState == 'AB') {
                if (oldMap == null ||
                    (oldMap.get(acc.Id).PersonMailingState != 'Alberta' &&
                    oldMap.get(acc.Id).PersonMailingState != 'AB')) {
                        acc.State_Alberta__c = true;
                    }
            } else {
                if (oldMap != null &&
                    (oldMap.get(acc.Id).PersonMailingState == 'Alberta' ||
                    oldMap.get(acc.Id).PersonMailingState == 'AB')) {
                        acc.State_Alberta__c = false;
                    }
            }
        }
    }
    //SP-13106 set OB check based on flags
    @TestVisible
    private void setOBcheckBasedOnKYC(Account acc) {
        if(acc.RecordTypeId == personAccRecordTypeId) {
            acc.Age_70_OB_check__c = !acc.Age_70__c ? Constants.ACCOUNT_OB_CHECK_AUTOMATED_REVIEW : Constants.ACCOUNT_OB_CHECK_MANUAL_REVIEW;
            acc.Name_Length_OB_check__c = !acc.Name_Review__c ? Constants.ACCOUNT_OB_CHECK_AUTOMATED_REVIEW : Constants.ACCOUNT_OB_CHECK_MANUAL_REVIEW;
            //commented as not yet clarified if sufficient with compilance
            //acc.Tax_ID_Format_OB_check__c = !acc.Tax_ID_Format_Review__c ? Constants.ACCOUNT_OB_CHECK_AUTOMATED_REVIEW : Constants.ACCOUNT_OB_CHECK_MANUAL_REVIEW;
            acc.Possible_Duplicate_OB_check__c = !acc.Possible_Duplicate__c ? Constants.ACCOUNT_OB_CHECK_AUTOMATED_REVIEW : Constants.ACCOUNT_OB_CHECK_MANUAL_REVIEW;
        }
    }

    public static String clearPhoneNumber(String phoneNumber){
        if(!String.isBlank(phoneNumber)){
            String clearedPhoneNumber = phoneNumber.replaceAll('[^\\d]','');
            if(clearedPhoneNumber.length()>9) clearedPhoneNumber=clearedPhoneNumber.right(10);
            return clearedPhoneNumber;
        }else{
            return phoneNumber;
        }
    }

    public void enqueueUserApiIntroducingBrokerUpdate(List<Account> newAccounts, Map<Id, Account> trgOldMap) {
        Set<Id> accountsWithChangedIntroducingBrokerName = new Set<Id>();

        for(Account acc : newAccounts) {
            if(acc.Introducing_Broker_Name__c != trgOldMap?.get(acc.Id)?.Introducing_Broker_Name__c){
                accountsWithChangedIntroducingBrokerName.add(acc.Id);
            }
        }
        if(accountsWithChangedIntroducingBrokerName.isEmpty()) {
            return;
        }
        for (fxAccount__c fxa: [
                SELECT Id, fxTrade_One_Id__c, fxTrade_User_Id__c, Introducing_Broker__r.Introducing_Broker_Name__c, Division_Name__c, Email__c
                FROM FxAccount__c
                WHERE Introducing_Broker__c IN :accountsWithChangedIntroducingBrokerName
        ]) {
            if (Test.isRunningTest() ||
                    (Util.isSentChangeNotificationAvailable(
                            Constants.FX_ACCOUNT_INTRODUCING_BROKER_CM,
                            fxa.Division_Name__c,
                            fxa.Email__c,
                            null
                    ))) {
                outgoingNotification.addEventRecord(IntroducingBrokerChangedONB.class, fxa, null);
            }

        }
        outgoingNotification.publishPlatformEvents();
    }
}