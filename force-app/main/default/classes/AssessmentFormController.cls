/**
* Controller for Assessment Form component
* 
* @author Gilbert Gao
*/
public with sharing class AssessmentFormController {
    
    public class AssessmentQuestion{
        
        @AuraEnabled
        public String question;
        
        @AuraEnabled
        public String answer;
        
    }
    
    @AuraEnabled
    public static fxAccount__c getFxAccount(ID fxaId){
        fxAccount__c result = null;
        
        System.debug('fxAccount Id: ' + fxaId);
        
        List<fxAccount__c> fxas = [SELECT Id, name,No_of_correct_OEL_Ans__c,OEL_Knowledge_Result__c, Division_Name__c, Knowledge_Result__c, OAU_Suitability_Result__c,
                                   OEL_Trading_Experience_Result__c,OEL_Assessment_Result__c,
                                   Citizenship_Nationality__c, Country__c, Singapore_Resident__c, Singapore_Dependent__c,
                                   Singapore_Financial_Education__c, Singapore_Professional_Qualification__c,
                                   Singapore_Financial_Work_Experience__c, Singapore_Experience_Details__c,
                                   Singapore_Has_Traded_Forex__c, Singapore_Has_Traded_CFDs__c, OAP_CKA_Reason__c,
                                   CKA_Last_User_Update__c, Understands_Risk_of_FX_CFDs__c, Experience_Trading_CFDs_or_FX__c,
                                   Other_Relevant_Experience__c, Year_Graduated__c, Year_Conferred__c, Position_Held__c,
                                   Year_Joined__c, Number_of_Consecutive_Years__c, Name_of_institution__c, Name_of_Institution_Other__c,
                                   Has_Educational_Experience__c, Experience_Qualified_Other__c, Has_Professional_Experience__c,
                                   Experience_Employed_Other__c, Position_Held_Other__c, Has_Working_Experience__c, Experience_Education_Other__c,
                                   Name_of_Broker_Other__c, asset_class_traded__c, Asset_Class_Traded_Other__c, Has_Traded_CFDs__c,
                                   Experience_Details__c, Experience_Employed__c, Experience_Qualified__c, Experience_Education__c, Diploma_or_degree__c
                                   FROM fxAccount__c
                                   WHERE Id = :fxaId];
        
        if(fxas.size() > 0){
            result = fxas[0];
        }
        
        return result;
    }
    
    
    @AuraEnabled
    public static List<AssessmentQuestion> getAssessmentQuestions(ID fxaId){
        List<AssessmentQuestion> questions = new List<AssessmentQuestion>();
        
        System.debug('fxAccount Id: ' + fxaId);
        
        String fieldNames = '';
        List<String> knowledgeFieldNames = getKnowledgeAnswerFieldNames();
        for(String field : knowledgeFieldNames){
            fieldNames += field + ', ';
        }
        
        String query = 'select Division_Name__c,' + fieldNames +' Id from fxAccount__c where Id = :fxaId limit 1'; 
        
        System.debug('Query: ' + query);
        
        List<fxAccount__c> fxas = Database.query(query);
        
        fxAccount__c fxa = fxas[0];
        
        string divisionFilter;
        if(fxa.Division_Name__c == Constants.DIVISIONS_OANDA_CANADA)
        {
            divisionFilter = Constants.DIVISIONS_OANDA_CANADA;
        }
        Map<String, String> questionTextByField = CustomSettings.getAssessmentQuestions(divisionFilter);
        
        for(String field : knowledgeFieldNames){
            String answer = (String)fxa.get(field);
            
            AssessmentQuestion assess = new AssessmentQuestion();
            assess.answer = answer;
            assess.question = questionTextByField.get(field);
            
            if(assess.answer == null || assess.question == null){
                //information is not complete, so skip it
                continue;
            }
            
            questions.add(assess);
        }
        
        return questions;
    }
    
    @AuraEnabled
    public static String getAcceptRejectReason(ID fxaId) {
        fxAccount__c fxa = [SELECT CKA_Reason_Override__c, CKA_Last_User_Update__c
                            FROM fxAccount__c WHERE Id = :fxaId LIMIT 1];
        
        return fxa.CKA_Last_User_Update__c != null ? fxa.CKA_Reason_Override__c : null;
    }
    
    @AuraEnabled
    public static String getKnowledgeResult(ID fxaId) {
        fxAccount__c fxa = [SELECT Knowledge_Result__c
                            FROM fxAccount__c WHERE Id = :fxaId LIMIT 1];
        
        return fxa.Knowledge_Result__c;
    }
    
    private static String[] getKnowledgeAnswerFieldNames(){
        List<String> knowledgeFieldNames = new List<String>();
        
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('fxAccount__c').getDescribe().fields.getMap();
        for(Schema.SObjectField sof : fieldMap.Values()) {
            Schema.DescribeFieldResult dfr = sof.getDescribe(); // describe each field (fd)
            if (dfr.getName().startsWith('Knowledge_Answer')) { 
                
                knowledgeFieldNames.add(dfr.getName());
            }
        }
        
        return knowledgeFieldNames;
    }
    
    @AuraEnabled
    public static fxAccount__c updateFxAccount(fxAccount__c fxa) {
        fxa.CKA_Last_User_Update__c =  datetime.now();
        update fxa;
        
        return fxa;
    }
}