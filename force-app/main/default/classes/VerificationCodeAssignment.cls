/**
 * @Description  : This class handles the assignment of verification codes to high-risk onboarding cases.
 *                 It includes methods to filter eligible cases, retrieve available verification codes, and assign them accordingly.
 *                 Logic extracted from CaseTriggerHandler.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-22
 **/
public inherited sharing class VerificationCodeAssignment {
    /**
     * @description Assigns verification codes to a list of high-risk onboarding cases if available.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     **/
    public static void verify(List<Case> cases) {
        List<Case> casesToVerificationCode = getUnclaimedOnboardingHighRiskCases(cases);

        if (casesToVerificationCode.size() > 0) {
            List<Verification_Code_Setting__c> codesAvailables = VerificationCodeUtility.getVerificationCodesAvailable(
                casesToVerificationCode.size()
            );
            if (codesAvailables.size() > 0) {
                Integer i = 0;
                for (Case c : casesToVerificationCode) {
                    c.Verification_code__c = codesAvailables[i].Name;
                    c.Verification_Code_Order__c = codesAvailables[i].Order__c;
                    i++;
                }
            } else {
                casesToVerificationCode[0].addError('No Verification Code available');
            }
        }
    }

    /**
     * @description Filters and returns a list of high-risk onboarding cases that require updates based on specific account conditions.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     * @return List<Case>
     **/
    private static List<Case> getUnclaimedOnboardingHighRiskCases(List<Case> cases) {
        List<Case> caseToUpdate = new List<Case>();
        List<Case> validCases = new List<Case>();
        String onboardingRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(Constants.CASE_ONBOARDING_RECTYP)
            .getRecordTypeId();
        for (Case c : cases) {
            if (
                (c.RecordTypeId == onboardingRecTypeId) && //the case is Onboarding
                String.isBlank(c.Verification_code__c) && //the case does not have a verification code
                String.isNotBlank(c.AccountId) //the case has an account
            ) {
                validCases.add(c);
            }
        }
        if (validCases.isEmpty()) {
            return caseToUpdate;
        }
        //else..
        Map<Id, Account> accounts = getCaseAccount(validCases);
        for (Case c : validCases) {
            //check case account conditions
            Account a = accounts.get(c.AccountId);
            if (
                (a.fxAccount__r != null) && //has an fxAccount
                (a.fxAccount__r.Is_Practice_Account__c == false) && //fxAccount is live
                (a.fxAccount__r.Type__c == Constants.FX_ACCOUNT_TYPE_INDIVIDUAL) && //fxAccount is individual
                //division is OME or OEL
                ((a.Primary_Division_Name__c == Constants.DIVISIONS_OANDA_EUROPE) ||
                (a.Primary_Division_Name__c == Constants.DIVISIONS_OANDA_EUROPE_MARKETS) ||
                (a.Primary_Division_Name__c == Constants.DIVISIONS_OANDA_EUROPE_AB) ||
                (a.Primary_Division_Name__c == Constants.DIVISIONS_OANDA_MARKETS_EUROPA_AB)) && //risk is High or Medium
                ((a.Customer_Risk_Assessment_OEL_OAU__c == Constants.HIGH_RISK) ||
                (a.Customer_Risk_Assessment_OEL_OAU__c == Constants.MEDIUM_RISK) ||
                (a.Manual_Adjusted_Customer_Risk_Assessment__c == Constants.HIGH_RISK) ||
                (a.Manual_Adjusted_Customer_Risk_Assessment__c == Constants.MEDIUM_RISK))
            ) {
                caseToUpdate.add(c);
            }
        }
        return caseToUpdate;
    }

    /**
     * @description Retrieves a map of Account records indexed by their IDs,
     * filtered by the Account IDs associated with the provided list of Cases.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     * @return Map<Id, Account>
     **/
    public static Map<Id, Account> getCaseAccount(List<Case> cases) {
        Set<Id> caseAccountId = new Set<Id>();
        for (Case c : cases) {
            if (c.AccountId != null) {
                caseAccountId.add(c.AccountId);
            }
        }

        Map<Id, Account> accounts = new Map<Id, Account>(
            [
                SELECT
                    Id,
                    Primary_Division_Name__c,
                    Customer_Risk_Assessment_OEL_OAU__c,
                    Manual_Adjusted_Customer_Risk_Assessment__c,
                    fxAccount__r.Is_Practice_Account__c,
                    fxAccount__r.Type__c
                FROM Account
                WHERE Id IN :caseAccountId
            ]
        );
        return accounts;
    }
}