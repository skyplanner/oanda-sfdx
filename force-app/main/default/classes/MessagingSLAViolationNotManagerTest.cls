/**
 * @File Name          : MessagingSLAViolationNotManagerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 4:15:18 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/4/2024, 10:25:19 AM   aniubo     Initial Version
 **/
@isTest
private class MessagingSLAViolationNotManagerTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();

		initManager.setFieldValues(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			new Map<String, Object>{
				'Division_Name__c' => Constants.DIVISIONS_OANDA_CANADA
			}
		);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		initManager.setFieldValues(
			ServiceTestDataKeys.LEAD_1,
			new Map<String, Object>{ 'fxAccount__c' => fxAccountId }
		);
		ServiceTestDataFactory.createLead1(initManager);
		initManager.storeData();
		Id accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);

		Account account = new Account(
			Id = accountId,
			fxAccount__c = fxAccountId
		);
		update account;
	}

	@isTest
	private static void testMessagingNotification() {
		// Test data setup
		TestSLAViolationDataReader dataReader = new TestSLAViolationDataReader(
			false
		);
		List<SObject> sessions = dataReader.getData(
			new ChatSLAViolationNotificationData(new List<Id>())
		);
		// Actual test
		Map<String, Milestone_Time_Settings__mdt> milestones = MilestoneUtils.getSettingsFilterByViolationType(
			new List<String>{ SLAConst.ASA_EXCEEDED_VT}
		);
		Boolean hasError = false;
		Test.startTest();
		try {
			SLAViolationNotificationManager notificationManager = SLAViolationNotificationManagerFactory.createNotificationManager(
				SLAConst.SLANotificationObjectType.MESSAGING_NOT,
				SLAConst.SLAViolationType.ASA,
				SLAConst.AgentSupervisorModel.DIVISION,
				milestones,
				new FakeSupervisorProviderCreator()
			);
			MessagingSession messagingObj = (MessagingSession) sessions.get(0);
			SLAMessagingViolationInfo result = new SLAMessagingViolationInfo(
				messagingObj.Id,
				messagingObj.Name,
				SLAConst.ASA_EXCEEDED_VT
			);
			result.isNotifiable = true;
			result.isPersistent = true;
			result.division = 'OANDA Canada';
			result.divisionCode = 'OC';
			result.inquiryNature = messagingObj.Inquiry_Nature__c;
			result.Tier = messagingObj.Tier__c;
			result.language = 'English';
			result.liveOrPractice = messagingObj.Live_or_Practice__c;
			result.notificationObjectType = SLAConst.SLANotificationObjectType.MESSAGING_NOT;
			result.accountName = 'Test Account';
			result.ownerId = UserInfo.getUserId();
			result.ownerName = UserInfo.getName();
			result.sourceType = messagingObj.Lead != null ? 'Lead' : 'Account';
			notificationManager.sendNotifications(
				new List<SLAChatViolationInfo>{ result }
			);
		} catch (Exception ex) {
			hasError = true;
		}

		Test.stopTest();
		Assert.areEqual(
			false,
			hasError,
			'No Error occurred while sending notification'
		);
		// Asserts
	}
}