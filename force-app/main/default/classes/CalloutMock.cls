/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 07-07-2022
 * @last modified by  : Dianelys Velazquez
**/
@isTest
global class CalloutMock implements HttpCalloutMock {    
    protected Integer statusCode;
    protected String status;
    protected object body;
    protected Map<String, String> responseHeaders;
    
    public CalloutMock(Integer code, object body, 
        String status, Map<String, String> responseHeaders
    ) {
        this.statusCode = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
	}  
    
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(statusCode);
        res.setStatus(status);
        res.setBody((string)body); 
        
        if (responseHeaders != null) {
			for (String key : responseHeaders.keySet()){
				res.setHeader(key, responseHeaders.get(key));
			}
		}
        
        return res;
    }

    public static StaticResourceCalloutMock getStaticResourceSuccessMock() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStatusCode(200);
        mock.setStatus('Success');
        mock.setHeader('Content-Type', 'application/json');
        return mock;
    }

    public static StaticResourceCalloutMock getStaticResourceSuccessMock(
        String staticResourceName
    ) {
        StaticResourceCalloutMock mock = getStaticResourceSuccessMock();
        mock.setStaticResource(staticResourceName);

        return mock;
    }

    public static MultiStaticResourceCalloutMock getMultiStaticResourceSuccessMock() {
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStatusCode(200);
        mock.setStatus('Success');
        mock.setHeader('Content-Type', 'application/json');
        return mock;
    }

    public static MultiStaticResourceCalloutMock getMultiStaticResourceSuccessMock(
        List<MultiStaticResourceItem> items
    ) {
        MultiStaticResourceCalloutMock multimock 
            = getMultiStaticResourceSuccessMock();

        for (MultiStaticResourceItem item : items) 
            multimock.setStaticResource(item.url, item.staticResourceName);

        return multimock;
    }
    
    public static CalloutMock getOkMock() {
        return new CalloutMock(200, '', 'OK', null);
    }

    public static CalloutMock getErrorMock() {
        return new CalloutMock(
            500, 'Internal Server Error', 'Error', null);
    }

    public static CalloutMock getUnauthorizedMock() {
        return new CalloutMock(
            401, '{"error": "Invalid Client"}', 'Unauthorized', null);
    }

    public static CalloutMock getNotFoundMock() {
        return new CalloutMock(
            404, '{"error": "Not Found"}', 'Not Found', null);
    }

    public class MultiStaticResourceItem {
        public String url {get; set;}

        public String staticResourceName {get; set;}

        public MultiStaticResourceItem(String url, String staticResourceName) {
            this.url = url;
            this.staticResourceName = staticResourceName;
        }
    }
}