/* Name: IhsTaxDeclarationRestResource
* Description : New Apex Class to get IHS tax declaration
* Author: Michal Piatek (mpiatek@oanda.com)
* Date : 2024 July 11
*/
@RestResource(urlMapping='/api/v1/ihs/taxdeclaration/*')
global class IhsTaxDeclarationRestResource {

    public static final List<String> PUT_PARAMS = new List<String>{'userId'};

    public static final String ENDPOINT_NAME = 'ihsTaxDeclaration';
    public static final String LOGGER_CATEGORY_URL = '/api/v1/ihs/taxdeclaration/*';

    @HttpGet
    global static void doGet() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(req, res);

        try {
            Map<String,String> urlParams = req.params;
            String email = urlParams.get('email');

            if(String.isBlank(email)) {
                IhsErrorWrapper errorResp = new IhsErrorWrapper();
                context.setResponse(errorResp.status, errorResp);
                return;
            }

            List<FxAccount__c> fxas = [
                SELECT 
                    Id,
                    Email_Formula__c,
                    TAX_Declarations__r.Treaty_Country_Code__c,
                    TAX_Declarations__c 
                FROM fxAccount__c
                WHERE RecordTypeId = :fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE 
                AND Email_Formula__c = :email 
                AND TAX_Declarations__c != null
            ];

            if(fxas.isEmpty()) {
                IhsErrorWrapper errorResp = new IhsErrorWrapper();
                context.setResponse(errorResp.status, errorResp);
                return;
            }
            
            Map<String,Object> response = new Map<String,Object>();
            response.put('TreatyCountryCode', fxas[0].TAX_Declarations__r.Treaty_Country_Code__c);
            context.setResponse(200, response);

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);

            context.setResponse(500, respBody);
        }
    }

    @HttpPut
    global static void doPut() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(req, res, PUT_PARAMS);

        try {

            Integer userId = Integer.valueOf(context.getPathParam('userId'));

            List<FxAccount__c> fxas = [
                SELECT 
                    Id,
                    TAX_Declarations__c,
                    (SELECT Id From TAX_Declarations__r)
                    FROM fxAccount__c 
                    WHERE fxTrade_Global_Id__c = :fxAccountUtil.constructGlobalTradeId(userId, true) 
            ];

            if(fxas.isEmpty()) {
                IhsErrorWrapper errorResp = new IhsErrorWrapper();
                context.setResponse(errorResp.status, errorResp);
                return;
            }

            Map<String,Map<String,Endpoints_Mappings__mdt>> endpointMappings;
            EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
            endpointMappings = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME);
            Map<String, Endpoints_Mappings__mdt> currentObjMapping = endpointMappings.get('Tax_Declarations__c');

            Map<String, Object> payload = (Map<String, Object>)JSON.deserializeUntyped(context.getBodyAsString());
            TAX_Declarations__c taxDeclaration = new TAX_Declarations__c(Name = 'TAX Declaration', Is_In_Queue__c = false);

            ApiMappingService mapper = new ApiMappingService();
            mapper.loggerCategory = ENDPOINT_NAME;
            mapper.loggerCategoryUrl = LOGGER_CATEGORY_URL;

            for(String inputField : payload.keySet()) {
                if(!currentObjMapping.containsKey(inputField)) {
                    continue;
                }
                mapper.setInputFieldtoSobject(
                    taxDeclaration, 
                    currentObjMapping.get(inputField), 
                    payload
                );
            }

            upsert taxDeclaration;

            FxAccount__c fxaToUpdate = new FxAccount__c();
            fxaToUpdate.put('Id', fxas[0].Id);
            fxaToUpdate.put('TAX_Declarations__c', taxDeclaration.Id);
            update fxaToUpdate;

            context.setResponse(200, new IhsSuccessWrapper(true, fxaToUpdate.Id));

        } catch(Exception ex) {
            String respBody = 
                'Something went wrong record will not be processed. Err msg: ' + 
                ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();

            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);

            context.setResponse(500, respBody);
        }
    }

    class IhsErrorWrapper {
        Integer status {get;set;}
        String message {get;set;}

        public IhsErrorWrapper() {
            this.status = 404;
            this.message = 'No account with this email';
        }
    }

    class IhsSuccessWrapper {
        Boolean isSuccessful {get;set;}
        Id Id {get;set;}

        public IhsSuccessWrapper(Boolean isSuccessful, Id fxaId) {
            this.isSuccessful = isSuccessful;
            this.Id = fxaId;
        }
    }
}