/**
 * Util class for account sync process
 */
public class AccountSyncUtil {
    
    // Ids to sync accounts
    Set<Decimal> fxIdsToSync;

    /**
     * Constructor
     */
    public AccountSyncUtil() {
        fxIdsToSync = new Set<Decimal>();
    }

    /**
     * Verify if sync is required
     */
    public void verify(
        Account acc,
        Account oldAcc,
        fxAccount__c fxAcc)
    {
        if(isReSyncNeeded(acc, oldAcc, fxAcc)) {
            fxIdsToSync.add(
                fxAcc.fxTrade_User_Id__c);
        }
    }

    /**
     * Sync creating Action platform events
     */
    public void sync() {
        if(!fxIdsToSync.isEmpty())
            new ActionUtil().publishSyncAccActions(
                fxIdsToSync);
    }

    /**
     * Know if reSync is needed
     */
    private Boolean isReSyncNeeded(
        Account acc,
        Account oldAcc,
        fxAccount__c fxAcc)
    {
        if(fxAcc == null)
            return false;
        
        return
            acc.fxAccount__c != oldAcc.fxAccount__c &&
            AccountUtil.isReadyToSync(
                acc,
                fxAcc);
    }

}