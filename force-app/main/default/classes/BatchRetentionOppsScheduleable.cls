public with sharing class BatchRetentionOppsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    //static DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
    static DateTime THREE_MONTHS_AGO = DateTime.now().addMonths(-3);
    //static DateTime SIX_MONTHS_AGO = DateTime.now().addMonths(-6);
    static DateTime TWENTY_MONTHS_AGO = DateTime.now().addMonths(-20);
    //static Integer BATCH_SIZE = 200;
    static Integer BATCH_SIZE = 80;
   

    //Deepak Malkani : Added PersonMailingCountry  in the select query - JIRA Story # SP-2816
    // Constructor
    public BatchRetentionOppsScheduleable() {
        //List<String> countries = CustomSettings.getRetentionOppCountries();
//      TODO, replace
//      query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c FROM Account WHERE Last_Traded_fxAccount__c!=null AND Last_Trade_Date__c!=null AND Last_Trade_Date__c < '
        //Deepak Malkani : TODO : we want the countries to be dynamic ..which means if values exists in the custom setting, then apply in the SOQL Query , else do not apply.
        
        //query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, Most_Recently_Traded_fxAccount__c, Most_Recently_Traded_fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
        //       + SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
        //removed the clause + ' AND PersonMailingCountry IN : countries'       

        //Retention Opportunities for OEL (3+ months inactivity)
        query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c, PersonMailingCountry, fxAccount__r.Trade_Volume_USD_30_Day_Max__c FROM Account WHERE Retention_Division__c = \'OEL\' AND Last_Trade_Date__c!=null AND Last_Trade_Date__c <= ' + SoqlUtil.getSoql(THREE_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(TWENTY_MONTHS_AGO) + ' AND fxAccount__r.Trade_Volume_USD_30_Day_Max__c < 50000000';
    }
    
    public BatchRetentionOppsScheduleable(String q){
    	query = q;
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c, PersonMailingCountry, fxAccount__r.Trade_Volume_USD_30_Day_Max__c FROM Account WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    // Batchable.start()
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    // Batchable.execute()  
    public void execute(Database.BatchableContext bc, List<sObject> batch) {
        OpportunityUtil.addRetentionOpps(batch);
    }
    
    // Batchable.finish()
    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
    
    // Scehduleable.execute()
    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchRetentionOppsScheduleable(), BATCH_SIZE);
    }

    public static final String CRON_NAME = 'BatchRetentionOppsScheduleable';
    //public static final String CRON_SCHEDULE_2X05 = '0 5 1,3,5,7,9,11,13,15,17,19,21,23 * * ?';
    public static final String CRON_SCHEDULE_Daily = '0 0 2 * * ?';

    // helper functions
    public static void schedule() {
        System.schedule(CRON_NAME + '_2AM', CRON_SCHEDULE_Daily, new BatchRetentionOppsScheduleable());
    }
    
    // BatchRetentionOppsScheduleable.executeBatch();
    public static Id executeBatch() {
        return Database.executeBatch(new BatchRetentionOppsScheduleable(), BATCH_SIZE);
    }
}