/**
 * @File Name          : SPConditionsUtilTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/26/2023, 1:30:43 PM
**/
@IsTest
private without sharing class SPConditionsUtilTest {

    // fieldValue is blank => exception
    @IsTest
    static void checkRequiredStringField1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStringField(
                Account.SObjectType, // objectToken
                Account.Name, // fieldToken
                null // fieldValue
            );
            //...
        } catch (SPInvalidFieldValueException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error, 
            'If the field is blank an exception should be thrown'
        );
    }

    // required = true, fieldValue is blank  => exception
    @IsTest
    static void checkRequiredStringField2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStringField(
                Account.SObjectType, // objectToken
                Account.Name, // fieldToken
                null, // fieldValue
                true // required
            );
            //...
        } catch (SPInvalidFieldValueException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the field is required and is blank an exception should be thrown'
        );
    }

    // paramValue is blank => exception
    @IsTest
    static void checkRequiredParam1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredParam(
                'someParamName', // paramName
                null // paramValue
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is blank an exception should be thrown'
        );
    }

    // required is true, paramValue is blank => exception
    @IsTest
    static void checkRequiredParam2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredParam(
                'someParamName', // paramName
                null, // paramValue
                true // required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and is empty, an exception should be thrown'
        );
    }

    // paramValue is null => exception
    @IsTest
    static void checkRequiredObjParam1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredObjParam(
                'someParamName', // paramName
                null // paramValue
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is null an exception should be thrown'
        );
    }

    // required = true, paramValue is null => exception
    @IsTest
    static void checkRequiredObjParam2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredObjParam(
                'someParamName', // paramName
                null, // paramValue,
                true // required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and is null an exception should be thrown'
        );
    }

    // objectList is empty => exception
    @IsTest
    static void checkRequiredListParam1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredListParam(
                'someParamName', // paramName
                new List<Account>() // objectList
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is an empty list, an exception should be thrown'
        );
    }

    // objectList is null => exception
    @IsTest
    static void checkRequiredListParam2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredListParam(
                'someParamName', // paramName
                null // objectList
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is null, an exception should be thrown'
        );
    }

    // required is true, objectList is empty => exception
    @IsTest
    static void checkRequiredListParam3() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredListParam(
                'someParamName', // paramName
                new List<Account>(), // objectList
                true // required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and it is an empty list an exception should be thrown'
        );
    }

    // required is true, objectList is null => exception
    @IsTest
    static void checkRequiredListParam4() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredListParam(
                'someParamName', //paramName
                null, //objectList
                true //required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and is null an exception should be thrown'
        );
    }

    // strSet is empty => exception
    @IsTest
    static void checkRequiredStrSetParam1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStrSetParam(
                'someParamName', // paramName
                new Set<String>() // strSet
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is an empty set an exception should be thrown'
        );
    }

    // strSet is null => exception
    @IsTest
    static void checkRequiredStrSetParam2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStrSetParam(
                'someParamName', // paramName
                null // strSet
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is null an exception should be thrown'
        );
    }

    // required = true and strSet is empty => exception
    @IsTest
    static void checkRequiredStrSetParam3() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStrSetParam(
                'someParamName', // paramName
                new Set<String>(), // strSet
                true // required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and it is an empty set an exception should be thrown'
        );
    }

    // required = true and strSet is null => exception
    @IsTest
    static void checkRequiredStrSetParam4() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredStrSetParam(
                'someParamName', // paramName
                null, // strSet
                true // required
            );
            //...
        } catch (SPInvalidParamException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the parameter is required and is null an exception should be thrown'
        );
    }

    // recordList is empty => exception
    @IsTest
    static void checkRequiredResult1() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredResult(
                new List<Account>(), // recordList
                'Account', // entityName
                'Id = fakeValue' // condition
            );
            //...
        } catch (SPNoDataFoundException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the result is an empty list, an exception should be thrown.'
        );
    }

    // recordList is null => exception
    @IsTest
    static void checkRequiredResult2() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredResult(
                null, // recordList
                'Account', // entityName
                'Id = fakeValue' // condition
            );
            //...
        } catch (SPNoDataFoundException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the result is null, an exception should be thrown.'
        );
    }

    // required = true and recordList is empty => exception
    @IsTest
    static void checkRequiredResult3() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredResult(
                new List<Account>(), // recordList
                'Account', // entityName
                'Id = fakeValue', // condition
                true // required
            );
            //...
        } catch (SPNoDataFoundException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the result is required and it is an empty list, an exception should be thrown.'
        );
    }

    // required = true and recordList is null => exception
    @IsTest
    static void checkRequiredResult4() {
        Boolean error = false;
        Test.startTest();
        try {
            SPConditionsUtil.checkRequiredResult(
                null, // recordList
                'Account', // entityName
                'Id = fakeValue', // condition
                true // required
            );
            //...
        } catch (SPNoDataFoundException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(
            true, 
            error,
            'If the result is required and it is null, an exception should be thrown'
        );
    }

    

}