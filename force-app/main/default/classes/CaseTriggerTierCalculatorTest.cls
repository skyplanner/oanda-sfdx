/**
 * Created by akajda on 15/05/2024.
 */

@IsTest
public with sharing class CaseTriggerTierCalculatorTest {

    @IsTest
    static void calculateTier1ForTelephonyTest() {
        TestDataFactory factory = new TestDataFactory();
        List<Account> accs = factory.createTestAccounts(3);
        List<fxAccount__c> fxAccs = factory.createFXTradeAccounts(accs);
        accs[0].fxAccount__c=fxAccs[0].Id;
        accs[1].fxAccount__c=fxAccs[1].Id;
        accs[2].fxAccount__c=fxAccs[2].Id;
        accs[2].Date_Customer_Became_Core__c=System.today();
        update accs;
        fxAccs[2].Division_Name__c=Constants.DIVISIONS_OANDA_GLOBAL_MARKETS;
        update fxAccs[2];

        List<Segmentation__c> segms = new List<Segmentation__c>();
        segms.add(new Segmentation__c(Seg_Date__c=Date.today(), Seg_PL__c='high', fxAccount__c=fxAccs[0].Id));
        segms.add(new Segmentation__c(Seg_Date__c=Date.today(), Seg_PL__c='high_plus', fxAccount__c=fxAccs[1].Id));
        insert segms;

        List<Case> cases = new List<Case>();
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[0].Id));
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[1].Id));
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[2].Id));
        insert cases;

        cases = [SELECT Tier__c FROM Case WHERE Chat_Email__c='test@oanda.com'];

        System.assertEquals(OmnichannelCustomerConst.TIER_1, cases[0].Tier__c);
        System.assertEquals(OmnichannelCustomerConst.TIER_1, cases[1].Tier__c);
        System.assertEquals(OmnichannelCustomerConst.TIER_1, cases[2].Tier__c);
    }

    @IsTest
    static void calculateTier2ForTelephonyTest() {
        TestDataFactory factory = new TestDataFactory();
        List<Account> accs = factory.createTestAccounts(2);
        List<fxAccount__c> fxAccs = factory.createFXTradeAccounts(accs);
        accs[0].fxAccount__c=fxAccs[0].Id;
        accs[1].fxAccount__c=fxAccs[1].Id;
        update accs;
        fxAccs[1].Division_Name__c=Constants.DIVISIONS_OANDA_GLOBAL_MARKETS;
        update fxAccs[1];

        Segmentation__c s = new Segmentation__c(Seg_Date__c=Date.today(), Seg_PL__c='mid', fxAccount__c=fxAccs[0].Id);
        insert s;

        List<Case> cases = new List<Case>();
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[0].Id));
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[1].Id));
        insert cases;

        cases = [SELECT Tier__c FROM Case WHERE Chat_Email__c='test@oanda.com'];

        System.assertEquals(OmnichannelCustomerConst.TIER_2, cases[0].Tier__c);
        System.assertEquals(OmnichannelCustomerConst.TIER_2, cases[1].Tier__c);
    }

    @IsTest
    static void calculateTier3ForTelephonyTest() {
        TestDataFactory factory = new TestDataFactory();
        List<Account> accs = factory.createTestAccounts(1);
        fxAccount__c fxAcc = factory.createFXTradeAccount(accs[0]);
        accs[0].fxAccount__c=fxAcc.Id;
        update accs;

        Segmentation__c s = new Segmentation__c(Seg_Date__c=Date.today(), Seg_PL__c='low', fxAccount__c=fxAcc.Id);
        insert s;
        Case c = new Case(Chat_Email__c='test@oanda.com', Origin='Phone', AccountId=accs[0].Id);
        insert c;

        c= [SELECT Tier__c FROM Case WHERE Id=:c.Id];

        System.assertEquals(OmnichannelCustomerConst.TIER_3, c.Tier__c);
    }

    @IsTest
    static void calculateTier4ForTelephonyTest() {
        Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
        insert l;

        Case c = new Case(Chat_Email__c='test@oanda.com', Origin='Phone');
        insert c;

        c= [SELECT Tier__c FROM Case WHERE Id=:c.Id];

        System.assertEquals(OmnichannelCustomerConst.TIER_4, c.Tier__c);
    }

    @IsTest
    static void calculateTierForTelephonyForEmptyCases() {
        Account a = new Account(LastName='test1', PersonEmail='test1@oanda.com', RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId());
        insert a;
        List<Case> cases = new List<Case>();
        cases.add(new Case(Chat_Email__c='test@oanda.com', Origin='Phone'));
        cases.add(new Case(Chat_Email__c='test1@oanda.com', Origin='Phone', AccountId=a.Id));
        String errorString='';
        try{
            insert cases;
        }catch(Exception e){
            errorString=e.getMessage();
        }
        System.assertEquals('', errorString);

    }

}