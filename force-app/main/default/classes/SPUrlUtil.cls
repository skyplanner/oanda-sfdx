/**
 * @File Name          : SPUrlUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/27/2021, 4:25:08 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/27/2021, 4:01:06 PM   acantero     Initial Version
**/
public without sharing class SPUrlUtil {

	public static void addCurrentParam(
		Map<String,String> currentPageParams,
		String paramName,
		Map<String,String> nextPageParams
	) {
		String paramValue = currentPageParams.get(paramName);
		if (String.isNotBlank(paramValue)) {
			nextPageParams.put(paramName, paramValue);
		}
    }

    public static void addCurrentParam(
		Map<String,String> currentPageParams,
		String paramName,
        Map<String,String> nextPageParams,
        String newParamName
	) {
		String paramValue = currentPageParams.get(paramName);
		if (String.isNotBlank(paramValue)) {
			nextPageParams.put(newParamName, paramValue);
		}
    }

}