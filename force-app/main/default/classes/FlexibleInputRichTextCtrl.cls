/**
 * @File Name          : FlexibleInputRichTextCtrl.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/26/2020, 4:03:32 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/23/2020   dmorales     Initial Version
**/
global without sharing class FlexibleInputRichTextCtrl {
    global FlexibleInputRichTextCtrl() {

    }

	@AuraEnabled
	global static List<String> loadStaticLinks() {
             
       Map<String, List<Chat_Offline_Links_Setting__mdt>> mapSetting = StaticLinkSettingManagement.getStaticLinkSettings(false);
            
       return  new List<String>(mapSetting.keySet());
  
	}
}