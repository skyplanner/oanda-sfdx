public with sharing class BatchRetentionOppsCloseWinSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	static DateTime SEVEN_DAYS_AGO = DateTime.now().addDays(-7);
	static Integer BATCH_SIZE = 200;
	
	// Constructor
	public BatchRetentionOppsCloseWinSchedulable() {
//		TODO, replace
//		query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Division_from_Region__c FROM Account WHERE Last_Traded_fxAccount__c!=null AND Last_Trade_Date__c!=null AND Last_Trade_Date__c < '
		query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Division_from_Region__c FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(SEVEN_DAYS_AGO);
	}
	
	public BatchRetentionOppsCloseWinSchedulable(String q){
		query = q;
	}

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Division_from_Region__c FROM Account WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	// Batchable.start()
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}

	// Batchable.execute()	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		OpportunityUtil.closeWinRetentionOpps(batch);
	}
	
	// Batchable.finish()
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
	
	// Scehduleable.execute()
	public void execute(SchedulableContext context) {
		Database.executeBatch(new BatchRetentionOppsCloseWinSchedulable(), BATCH_SIZE);
	}
	
	// BatchRetentionOppsScheduleable.executeBatch();
	public static Id executeBatch() {
		return Database.executeBatch(new BatchRetentionOppsCloseWinSchedulable(), BATCH_SIZE);
	}
	
	public static final String CRON_NAME = 'BatchRetentionOppsCloseWinSchedulable';
	public static final String CRON_SCHEDULE_Daily = '0 0 4 * * ?';

	// helper functions
	public static void schedule() {
		System.schedule(CRON_NAME + '_4AM', CRON_SCHEDULE_Daily, new BatchRetentionOppsScheduleable());
	}
}