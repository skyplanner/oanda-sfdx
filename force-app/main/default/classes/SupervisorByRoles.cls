/**
 * @File Name          : SupervisorByRoles.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/11/2022, 2:15:25 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/2/2021, 9:24:21 AM   acantero     Initial Version
**/
public without sharing class SupervisorByRoles implements SupervisorProvider {

    Set<String> agentIdSet;

    Map<String,String> userRoleMap;
    Map<String, Set<String>> agentRoleSupervisorIdMap;

    public SupervisorByRoles() {
        this.agentIdSet = new Set<String>();
    }

    public void addCriteria(String criteria) {
        if (String.isNotBlank(criteria)) {
            agentIdSet.add(criteria);
        }
    }

    public Boolean isEmpty() {
        return (
            (agentRoleSupervisorIdMap == null) ||
            agentRoleSupervisorIdMap.isEmpty()
        );
    }

    public Set<String> getSupervisors(String criteria) {
        return getAgentSupervisors(criteria);
    }

    public Set<String> getAgentSupervisors(String agentId) {
        Set<String> result = null;
        if (String.isBlank(agentId)) {
            return result;
        }
        //else...
        String agentRole = (userRoleMap != null)
            ? userRoleMap.get(agentId)
            : null;
        if (agentRole != null) {
            result = agentRoleSupervisorIdMap.get(agentRole);
        }
        return result;
    }
    
    public void getSupervisors() {
        if ((agentIdSet == null) || agentIdSet.isEmpty()) {
            return;
        }
        //else...
        Set<String> agentRoleDevNameSet = new Set<String>();
        userRoleMap = SPSecurityUtil.getUserRoleMap(
            agentIdSet,
            agentRoleDevNameSet
        );
        GetSupervisorsByRoleCmd command = new GetSupervisorsByRoleCmd(
            agentRoleDevNameSet
        );
        agentRoleSupervisorIdMap = command.execute();
    }

    public class GetSupervisorsByRoleCmd {

        final Set<String> agentRoleDevNameSet;
        final Map<String, Set<String>> agentRoleSupervisorIdMap;
        
        Map<String,Set<String>> agentSupervisorRolesMap;
        Set<String> supervisorRoleDevNameSet;
        Map<String, Set<String>> roleSupervisorIdMap;

        public GetSupervisorsByRoleCmd(Set<String> agentRoleDevNameSet) {
            this.agentRoleDevNameSet = agentRoleDevNameSet;
            this.agentRoleSupervisorIdMap = new Map<String, Set<String>>();
        }

        public Map<String, Set<String>> execute() {
            if (
                (agentRoleDevNameSet == null) ||
                agentRoleDevNameSet.isEmpty()
            ) {
                return agentRoleSupervisorIdMap;
            }
            //else...
            agentSupervisorRolesMap = new Map<String,Set<String>>();
            supervisorRoleDevNameSet = new Set<String>();
            getSupervisorRolesByAgent();
            if (!supervisorRoleDevNameSet.isEmpty()) {
                getSupervisorIdsByRole();
                if (!roleSupervisorIdMap.isEmpty()) {
                    linkSupervisorIdsToAgentRole();
                }
            }
            return agentRoleSupervisorIdMap;
        }

        void getSupervisorRolesByAgent() {
            OmnichannelRoleSettings omniRoleSettings = OmnichannelRoleSettings.getInstance();
            for(String agentRole : agentRoleDevNameSet) {
                Set<String> supervisorRoleSet = 
                    omniRoleSettings.getSupervisorRoles(agentRole);
                if (
                    (supervisorRoleSet != null) &&
                    (!supervisorRoleSet.isEmpty())
                ) {
                    agentSupervisorRolesMap.put(agentRole, supervisorRoleSet);
                    supervisorRoleDevNameSet.addAll(supervisorRoleSet);
                }
            }
        }

        void getSupervisorIdsByRole() {
            List<ServiceResource> serviceResList = [
                SELECT  
                    RelatedRecordId,
                    RelatedRecord.UserRole.DeveloperName
                FROM 
                    ServiceResource 
                WHERE 
                    ResourceType = :OmnichanelConst.SERVICE_RES_AGENT 
                AND
                    IsActive = true
                AND
                    RelatedRecord.IsActive = true 
                AND
                    RelatedRecord.UserRole.DeveloperName IN :supervisorRoleDevNameSet
            ];
            roleSupervisorIdMap = new Map<String, Set<String>>();
            for(ServiceResource serviceRes : serviceResList) {
                String supRoleDevName = serviceRes.RelatedRecord.UserRole.DeveloperName;
                Set<String> supervisorIdSet = roleSupervisorIdMap.get(supRoleDevName);
                if (supervisorIdSet == null) {
                    supervisorIdSet = new Set<String>();
                    roleSupervisorIdMap.put(supRoleDevName, supervisorIdSet);
                }
                supervisorIdSet.add(serviceRes.RelatedRecordId);
            }
        }

        void linkSupervisorIdsToAgentRole() {
            for(String agentRole : agentSupervisorRolesMap.keySet()) {
                Set<String> supervisorRoleSet = agentSupervisorRolesMap.get(agentRole);
                for(String supervisorRole : supervisorRoleSet) {
                    Set<String> supervisorIdSet = roleSupervisorIdMap.get(supervisorRole);
                    if (supervisorIdSet != null) {
                        Set<String> agentRoleSupervisorIdSet = agentRoleSupervisorIdMap.get(agentRole);
                        if (agentRoleSupervisorIdSet == null) {
                            agentRoleSupervisorIdSet = new Set<String>();
                            agentRoleSupervisorIdMap.put(agentRole, agentRoleSupervisorIdSet);
                        }
                        agentRoleSupervisorIdSet.addAll(supervisorIdSet);
                    }
                }
            }
        }
    }

    
}