/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 18th, 2019
 *@Updated: By Sahil Handa for SP-8533, Removing Russian 
 */
global abstract class HelpPortalBase {
	protected Integer expirationMonths;
	protected Integer mostVisitedCount;

	global String language { get; protected set; }
	global String languageLabel { get; protected set; }
	global String region { get; protected set; }
	
	/**
	 * Base constructor
	 */
	global HelpPortalBase(String language) {		
		this.language = language;
		this.region = getRegion();
		fetchSetting();
	}

	/**
	 * Base constructor
	 */
	global HelpPortalBase() {		
		language = getLanguage();
		languageLabel = getLanguageLabel(language);
		region = getRegion();
		fetchSetting();
	}

	/**
	 * @return the current language requested
	 */
	private String getLanguage() {
		PageReference currentPage = ApexPages.currentPage();
		String l = currentPage.getParameters().get('language');
		String c = currentPage.getParameters().get('change');

		if (String.isNotEmpty(c)) {
			Cookie lang = new Cookie('prefered_language', c, null, 611040000, false);
			currentPage.setCookies(new Cookie[]{lang});
			return c;
		}
	
		if (!String.isBlank(l))
			return l;
		
		Cookie existLang = currentPage.getCookies().get('prefered_language');
		if (existLang != null)
			return existLang.getValue();
		
		return 'en_US';
	}

	private String getRegion() {
		String r = '';
		PageReference currentPage = ApexPages.currentPage();
		try{
			//because some classes extend form this one run under Lightning Component and doesn't have ApexPage context.
			r = currentPage.getParameters().get('changeRegion');
		}
		catch(Exception e){
			System.debug('Error Not Current page available');
			return r;
		}
	
		
		if (String.isNotEmpty(r)) {
			System.debug('Region change to ' + r);
			Cookie region = new Cookie('prefered_region', r ,null,611040000,false);		
			currentPage.setCookies(new Cookie[]{region});
			return r;
		}
	
			
		Cookie existRegion = currentPage.getCookies().get('prefered_region');
		if (existRegion != null)
			return existRegion.getValue();
		
		return '';
	}


	/**
	 * @param languageLabel
	 * @return the language code based on the language label
	 */
	protected String getLanguageCode(String languageLabel) {
		Schema.DescribeFieldResult fieldResult;
		List<Schema.PicklistEntry> picklistValues;
		String result = 'en_US';

		if (!String.isBlank(languageLabel)) {
			fieldResult = User.LanguageLocaleKey.getDescribe();
			picklistValues = fieldResult.getPicklistValues();

			for (Schema.PicklistEntry picklistEntry : picklistValues)
				if (picklistEntry.getLabel() == languageLabel) {
					result = picklistEntry.getValue();
					break;
				}
		}

		return result;
	}

	/**
	 * @param languageCode
	 * @return the language code based on the language label
	 */
	protected String getLanguageLabel(String languageCode) {
		Schema.DescribeFieldResult fieldResult;
		List<Schema.PicklistEntry> picklistValues;
		String result = 'English';

		if (!String.isBlank(languageCode)) {
			fieldResult = User.LanguageLocaleKey.getDescribe();
			picklistValues = fieldResult.getPicklistValues();

			for (Schema.PicklistEntry picklistEntry : picklistValues)
				if (picklistEntry.getValue() == languageCode) {
					result = picklistEntry.getLabel();
					break;
				}
		}

		return result;
	}

	/**
	 * Sets properties from custom settings
	 */
	protected void fetchSetting() {
		HelpPortalSettings__c settings;
		settings = HelpPortalSettings__c.getOrgDefaults();

		// we need the amount of popular articles
		mostVisitedCount = settings.PopularFaqsCount__c == null ?
			5 : settings.PopularFaqsCount__c.intValue();

		expirationMonths = settings.ArticleExpiration__c == null ?
			18 : settings.ArticleExpiration__c.intValue();
	}

	/**
	 * @return the top ten visited FAQs
	 */
	public List<FAQ> getTopFaqs() {

		return FAQ.doWrap(Database.query(
			' SELECT ' +
				' Id, ' +
				' KnowledgeArticleId, ' +
				' Title, ' +
				' Answer__c, ' +
				' AllowFeedback__c ' +
			' FROM FAQ__kav ' +
			' WHERE Language = \'' + language + '\' ' +
			' AND CreatedDate = LAST_N_DAYS:' + expirationMonths + 
			' ORDER BY ArticleTotalViewCount DESC ' +
			' LIMIT :mostVisitedCount '
		));
	}

	/**
	 * @return the top ten visited FAQs by Division
	 */

	public List<FAQ> getTopFaqsByDivision(String division) {

		String division_filter = '(All__c';
		if(String.isNotEmpty(division)){
           division_filter += ','+ division + '__c';
		}
		division_filter += ')';
        String query = ' SELECT ' +
		' Id, ' +
		' KnowledgeArticleId, ' +
		' Title, ' +
		' Answer__c, ' +
		' AllowFeedback__c ' +
		' FROM FAQ__kav ' +
		' WHERE Language = \'' + language + '\' ' +
		' AND CreatedDate = LAST_N_DAYS:' + expirationMonths +
		' WITH DATA CATEGORY Division__c AT ' + division_filter +
		' ORDER BY ArticleTotalViewCount DESC ' +
		' LIMIT :mostVisitedCount ';  		
		try{
			return FAQ.doWrap(Database.query(query));
		}
		catch(Exception e){
			return new List<FAQ>();
		}		
	}

	/**
	 * @return a list of currently active language
	 */
	global List<Map<String, String>> getLanguages() {
		Set<String> allowedLanguages;
		List<Map<String, String>> result;
		Schema.DescribeFieldResult fieldResult;
		List<Schema.PicklistEntry> picklistValues;
		
		fieldResult = User.LanguageLocaleKey.getDescribe();
		picklistValues = fieldResult.getPicklistValues();
		result = new List<Map<String, String>>();
		allowedLanguages = new Set<String> {
			'en_US',
			'de',
			'zh_CN',
			'zh_TW',
			'fr',
			'it',
			'pt_BR',
			//'ru',
			'es',
			'vi',
			'th',
			'in',
			'ms'
		};

		for (Schema.PicklistEntry picklistEntry : picklistValues)
			if (allowedLanguages.contains(picklistEntry.getValue()))
				result.add(new Map<String, String> {
					'value' => picklistEntry.getValue(),
					'label' => picklistEntry.getLabel() 
				});
		
	   	return result;	  
	}

	/**
	 * @return TRUE if the current browser is on a mobile device.
	 */
	protected Boolean isMobile() {
		String userAgent;
		Pattern p;
		Matcher m;

		userAgent = ApexPages.currentPage().getHeaders().get('User-Agent');
		p = Pattern.compile(
			'.*(android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini).*');
		m = p.matcher(userAgent.toLowerCase());
		return m.matches();
	}
	
	/**
	 * @return GTM (script) from custom label
	 */
	public static String getGtmScript {
		get {
			if (getGtmScript == null) {
				System.Domain domain = System.DomainParser.parse(URL.getOrgDomainUrl());
				getGtmScript = domain.getSandboxName()==null ? System.Label.GTM_Script_Prod : System.Label.GTM_Script_Staging;
			}
			return getGtmScript;
		}
		set;
	}

	/**
	 * @return GTM (noscript) from custom label
	 */
	public static String getGtmNoScript {
		get {
			if (getGtmNoScript == null) {
				System.Domain domain = System.DomainParser.parse(URL.getOrgDomainUrl());
				getGtmNoScript = domain.getSandboxName()==null ? System.Label.GTM_Noscript_Prod : System.Label.GTM_Noscript_Staging;
			}
			return getGtmNoScript;
		}
		set;
	} 
}