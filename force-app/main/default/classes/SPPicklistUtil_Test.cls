/**
 * @File Name          : SPPicklistUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/24/2021, 6:10:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 5:57:09 PM   acantero     Initial Version
**/
@isTest
private without sharing class SPPicklistUtil_Test {

    @isTest
    static void test() {
        Test.startTest();
        List<String> result1 = 
            SPPicklistUtil.getPicklistValues(
                Account.AccountSource
            );
        Boolean result2 = 
            SPPicklistUtil.isValidValue(
                Account.AccountSource,
                'fake value'
            );
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assertEquals(false, result2);
    }
    
}