/**
 * @File Name          : CaseReroutingManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/27/2022, 8:24:43 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/12/2022, 11:52:51 AM   acantero     Initial Version
**/
public without sharing class CaseReroutingManager {

    static CaseReroutingManager instance;

    @TestVisible
    Set<ID> reroutingQueueIdSet;

    List<String> closedStatuses;

    private CaseReroutingManager() {
    }

    public static CaseReroutingManager getInstance() {
        if (instance == null) {
            instance = new CaseReroutingManager();
        }
        return instance;
    }

    public Boolean fireRerouteEventForCases(Set<ID> reroutedCaseIdSet) {
        if (
            (reroutedCaseIdSet == null) ||
            reroutedCaseIdSet.isEmpty()
        ) {
            return false;
        }
        //else...
        List<Case_Rerouted__e> eventList = new List<Case_Rerouted__e>();
        for(ID caseId : reroutedCaseIdSet) {
            eventList.add(
                new Case_Rerouted__e(
                    Case_ID__c = caseId
                )
            );
        }
        Eventbus.publish(eventList);
        return true;
    }

    /**
     * Only cases that still have Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE 
     * are processed
     */
    public Boolean rerouteCasesUsingSkills(Set<ID> caseIdSet) {
        Boolean result = false;
        List<Case> caseList = CaseHelper.getRoutingInfoById(caseIdSet);
        List<Case> validCaseList = new List<Case>();
        for(Case caseObj : caseList) {
            if (
                (caseObj.Routed__c == true) &&
                (caseObj.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE)
            ) {
                caseObj.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING;
                caseObj.Internally_Routed__c = true;
                validCaseList.add(caseObj);
            }
        }
        if (!validCaseList.isEmpty()) {
            update validCaseList;
            result = true;
        }
        return result;
    }

    public Set<ID> getReroutingQueueIdSet() {
        if (reroutingQueueIdSet == null) {
            OmnichanelSettings omniSettings = 
                OmnichanelSettings.getRequiredDefault();
            reroutingQueueIdSet = SPQueueUtil.getQueueIdSetByDevNames(
                omniSettings.caseOwnerReroutingList
            );
        }
        return reroutingQueueIdSet;
    }

    public List<String> getClosedStatuses() {
        if (closedStatuses == null) {
            closedStatuses = CaseHelper.getClosedStatuses();
        }
        return closedStatuses;
    }

    public Boolean newOwnerImpliesRouting(Case rec) {
        return (
            SPSecurityUtil.isCurrentUserAValidStandardUser() &&
            getReroutingQueueIdSet().contains(rec.OwnerId) &&
            (getClosedStatuses().contains(rec.Status) == false)
        );
    }

    public Boolean newStatusImpliesRouting(
        Case rec,
        Case oldRec
    ) {
        return (
            getReroutingQueueIdSet().contains(rec.OwnerId) &&
            (getClosedStatuses().contains(oldRec.Status)) &&
            (getClosedStatuses().contains(rec.Status) == false)
        );
    }

}