/*
@File Name          : HelpPortalMessageSelectionCtrl class to get custom metadata in VF page
@Description        : 
@Author             : Sahil Handa
*/
public class HelpPortalMessageSelectionCtrl {
    
    public Map<String, String> mapMessage {get;set;}
    public List<String> geoRegion {get;set;}
    public Map<String, String> mapMessage2 {get;set;}
    public List<String> geoRegion2 {get;set;}
    
    public HelpPortalMessageSelectionCtrl() {
        //Map to create the VF page HelpPortalContact
        mapMessage = new Map<String,String>();
        geoRegion = new list<String>();
        List< HelpPortalMessageSelection__mdt > messageSelectionList = [SELECT id,label,URL__c,order__C  from HelpPortalMessageSelection__mdt order by order__c];
        for(HelpPortalMessageSelection__mdt message : messageSelectionList){
            System.debug('message ->'+message.Label);
            geoRegion.add(message.Label);
            mapMessage.put(message.Label,message.URL__c);
        }
        //Map to create the VF page HelpPortalContactUs
        mapMessage2 = new Map<String,String>();
        geoRegion2 = new list<String>();
        List< HelpPortalMessageSelection2__mdt	 > messageSelectionList2 = [SELECT id,label,URL__c,order__c  from HelpPortalMessageSelection2__mdt	 order by order__c];
        for(HelpPortalMessageSelection2__mdt	 message : messageSelectionList2){
            System.debug('message ->'+message.Label);
            geoRegion2.add(message.Label);
            mapMessage2.put(message.Label,message.URL__c);
        }
    }
}