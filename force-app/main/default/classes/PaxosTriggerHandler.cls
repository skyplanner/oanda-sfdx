/* Name: PaxosTriggerHandler
 * Description : Trigger handler class for Paxos
 * Author: Eugene
 * Date : 2022-01-25
 */

public with sharing class PaxosTriggerHandler {

    List<Paxos__c> newList;
    Map<Id, Paxos__c> oldMap;

    /**
     * Constructor
     */
    public PaxosTriggerHandler(
        List<Paxos__c> newList)
    {
        this.newList = newList;
    }

    /**
     * Constructor
     */
    public PaxosTriggerHandler(
        List<Paxos__c> newList,
        Map<Id, Paxos__c> oldMap)
    {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    /**
     * Handles the after insert event
     */
    public void onAfterInsert()
    {
        PaxosUtil.updateCryptoFxaFunnelStage(newList, oldMap);
    }

    /**
     * Handles the after update event
     */
    public void onAfterUpdate()
    {
        PaxosUtil.updateCryptoFxaFunnelStage(newList, oldMap);
        PaxosUtil.sendPaxosStatusChangedEvent(newList, oldMap);
    }  

}