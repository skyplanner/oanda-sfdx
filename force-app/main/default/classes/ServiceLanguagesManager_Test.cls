/**
 * @File Name          : ServiceLanguagesManager_Test.cls
 * @Description        :
 * @Author             : acantero
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 12:48:02 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 12:54:37 PM   acantero     Initial Version
 **/
@isTest
private without sharing class ServiceLanguagesManager_Test {
	@isTest
	static void test() {
		Service_Language__mdt serviceLanguageObj = [
			SELECT
				DeveloperName,
				Custom_Label_Name__c,
				Language__c,
				Language_Corrected__c,
				Skill_Dev_Name__c,
				Chat_Button_Id__c
			FROM Service_Language__mdt
			WHERE Language_Corrected__c = NULL
			LIMIT 1
		];
		Service_Language__mdt serviceLanguageObj2 = [
			SELECT DeveloperName, Language_Corrected__c
			FROM Service_Language__mdt
			WHERE Language_Corrected__c != NULL
			LIMIT 1
		];
		String code = serviceLanguageObj.DeveloperName;
		String language = serviceLanguageObj.Language__c;
		String customLabel = serviceLanguageObj.Custom_Label_Name__c;
		String skill = serviceLanguageObj.Skill_Dev_Name__c;
		String chatButtonId = serviceLanguageObj.Chat_Button_Id__c;
		String fakeLanguage = 'fake language';
		String langCorrected = serviceLanguageObj2.Language_Corrected__c;
		String langCorrectedCode = serviceLanguageObj2.DeveloperName;
		Test.startTest();
		String langCustomLabel = ServiceLanguagesManager.getLangCustomLabel(
			code
		);
		String langCustomLabel2 = ServiceLanguagesManager.getLangCustomLabel(
			null
		);
		String langLanguage = ServiceLanguagesManager.getLanguage(code);
		String langLanguage2 = ServiceLanguagesManager.getLanguage(null);
		String langLanguageCode = ServiceLanguagesManager.getLanguageCode(
			language
		);
		String langLanguageCode2 = ServiceLanguagesManager.getLanguageCode(
			fakeLanguage
		);
		String langLanguageCode3 = ServiceLanguagesManager.getLanguageCode(
			null
		);
		String langSkill = ServiceLanguagesManager.getSkillByLangCode(code);
		String langSkill2 = ServiceLanguagesManager.getSkillByLangCode(null);
		String langChatButtonId = ServiceLanguagesManager.getChatButtonIdByLangCode(
			code
		);
		String langChatButtonId2 = ServiceLanguagesManager.getChatButtonIdByLangCode(
			null
		);
		Set<String> langSkillsEnabledForChat = ServiceLanguagesManager.getLangSkillsEnabledForChat();
		String langCodeByLangCorrected1 = ServiceLanguagesManager.getLangCodeByLangCorrected(
			langCorrected
		);
		String langCodeByLangCorrected2 = ServiceLanguagesManager.getLanguageCode(
			fakeLanguage
		);
		String langCodeByLangCorrected3 = ServiceLanguagesManager.getLanguageCode(
			null
		);
		Test.stopTest();
		System.assertEquals(language, langLanguage);
		System.assertEquals(null, langLanguage2);
		System.assertEquals(customLabel, langCustomLabel);
		System.assertEquals(null, langCustomLabel2);
		System.assertEquals(code, langLanguageCode);
		System.assertEquals(fakeLanguage, langLanguageCode2);
		System.assertEquals(null, langLanguageCode3);
		System.assertEquals(skill, langSkill);
		System.assertEquals(null, langSkill2);
		System.assertEquals(chatButtonId, langChatButtonId);
		System.assertEquals(null, langChatButtonId2);
		System.assertNotEquals(null, langSkillsEnabledForChat);
		System.assertEquals(langCorrectedCode, langCodeByLangCorrected1);
		System.assertEquals(fakeLanguage, langCodeByLangCorrected2);
		System.assertEquals(null, langCodeByLangCorrected3);
	}

	@IsTest
	static void getLanguageByFamily() {
		String code = 'es';
		String spanishLanguage = [
			SELECT Language__c
			FROM Service_Language__mdt
			WHERE DeveloperName = :code
			LIMIT 1
		]
		.Language__c;
		Test.startTest();
		String language1 = ServiceLanguagesManager.getLanguageByFamily(null);
		String language2 = ServiceLanguagesManager.getLanguageByFamily(code);
		String language3 = ServiceLanguagesManager.getLanguageByFamily(
			'es_fakefake'
		);
		Test.stopTest();
		System.assertEquals(null, language1, 'Invalid value');
		System.assertEquals(spanishLanguage, language2, 'Invalid value');
		System.assertEquals(spanishLanguage, language3, 'Invalid value');
	}

	@IsTest
	static void getLanguageCorrectedByFamily() {
		Service_Language__mdt serviceLanguageObj = [
			SELECT DeveloperName, Language__c
			FROM Service_Language__mdt
			WHERE Language_Corrected__c = NULL
			LIMIT 1
		];
		String spanishLanguage = [
			SELECT Language__c
			FROM Service_Language__mdt
			WHERE DeveloperName = 'es'
			LIMIT 1
		]
		.Language__c;
		String code = serviceLanguageObj.DeveloperName;
		String language = serviceLanguageObj.Language__c;
		Test.startTest();
		String languageCorrected1 = ServiceLanguagesManager.getLanguageCorrectedByFamily(
			null
		);
		String languageCorrected2 = ServiceLanguagesManager.getLanguageCorrectedByFamily(
			code
		);
		String languageCorrected3 = ServiceLanguagesManager.getLanguageCorrectedByFamily(
			'es_fakefake'
		);
		String languageCorrected4 = ServiceLanguagesManager.getLanguageCorrected(
			'Chinese',
			'Mandarin'
		);

		Test.stopTest();
		System.assertEquals(null, languageCorrected1, 'Invalid value');
		System.assertEquals(language, languageCorrected2, 'Invalid value');
		System.assertEquals(
			spanishLanguage,
			languageCorrected3,
			'Invalid value'
		);
		System.assertEquals('Mandarin', languageCorrected4, 'Invalid value');
	}
	@isTest
	private static void testIsLangAvailableForChatbot() {
		// Test data setup

		// Actual test
		Test.startTest();
		Boolean isEnabled = ServiceLanguagesManager.isLangAvailableForChatbot(
			null
		);
		Assert.areEqual(false, isEnabled, 'Language Code is null');
		isEnabled = ServiceLanguagesManager.isLangAvailableForChatbot('es');
		Test.stopTest();
		Service_Language__mdt languageRec = ServiceLanguagesManager.getLanguageRecord(
			'es'
		);
		Assert.areEqual(
			languageRec?.Available_for_Chatbot__c == true,
			isEnabled,
			'Enable must be' + languageRec?.Available_for_Chatbot__c
		);
	}

	@isTest
	private static void testGetAlternativeName() {
		Test.startTest();
		String alternative = ServiceLanguagesManager.getAlternativeName(null);
		Assert.areEqual(null, alternative, 'Language Code is null');
		alternative = ServiceLanguagesManager.getAlternativeName('es');
		Test.stopTest();
		Service_Language__mdt languageRec = ServiceLanguagesManager.getLanguageRecord(
			'es'
		);
		Assert.areEqual(
			languageRec?.Alternative_Name__c,
			alternative,
			'Enable must be' + languageRec?.Alternative_Name__c
		);
	}
}