/**
 * @File Name          : ViewSurveyController_Test.cls
 * @Description        : 
 * @Author             : fgomez
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/18/2019, 4:26:04 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    ?????????   fgomez       Initial Version
 * 1.0    12/6/2019   dmorales     Increase coverage
**/
@istest
private class ViewSurveyController_Test{
	//------------------------------------------------------------------------------//
	//------------------------------------------------------------------------------//
	private static Survey__c createSurvey(){
		Survey__c s;
		List<Survey_Question__c> qs;
		s = new Survey__c(
			Name = 'sample survey'
		);
		insert s;

        Survey_Question__c qParent = new Survey_Question__c(
				Name = 'qParent',
				Question__c = 'Im a father question',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'No',
				OrderNumber__c = 1,
				Required__c = true,
				Type__c = 'Single Select--Vertical',
				Survey__c = s.Id
			);			
        insert qParent;

		qs = new List<Survey_Question__c> {
			new Survey_Question__c(
				Name = 'q1',
				Question__c = 'Like it?',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'No',
				OrderNumber__c = 1,
				Required__c = true,
				Type__c = 'Single Select--Vertical', 
				Survey__c = s.Id,
				Parent_Question__c = qParent.Id,
				Parent_Question_Answers__c  = 'Maybe'
			),
			new Survey_Question__c(
				Name = 'q2',
				Question__c = 'Don\'t like it?',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'No\nMaybe',
				OrderNumber__c = 2,
				Required__c = true,
				Type__c = 'Multi-Select--Vertical',
				Survey__c = s.Id
			),
			new Survey_Question__c(
				Name = 'q3',
				Question__c = 'Love it?',
				Choices__c = 'Yes\nNope',
				AnswersToTriggerExtraFeedback__c = 'Nope',
				OrderNumber__c = 3,
				Required__c = true,
				Type__c = 'Single Select--Horizontal',
				Survey__c = s.Id
			),
			new Survey_Question__c(
				Name = 'q4',
				Question__c = 'Don\'t love it?',
				OrderNumber__c = 4,
				Required__c = true,
				Type__c = 'Free Text - Single Row Visible',
				Survey__c = s.Id
			)
		};
		insert qs;
        
		return s;
	}

	private static Case createCase(){
		Case c = new Case(
			Subject = 'sample',
			Origin = 'Phone',
			Live_or_Practice__c = 'Live',
			Priority = 'Normal',
			Inquiry_Nature__c = 'Registration',
			Status = 'Open',
			RecordTypeId = 
				Schema.SObjectType.Case.getRecordTypeInfosByName().get(
					'Support').getRecordTypeId()
		);
		insert c;
		return c;
	}

	private static Lead createLead(){
		Lead ldObj = new Lead();
	    ldObj.FirstName = 'TestFirst';
        ldObj.LastName = 'TestLast';
        ldObj.Email = 'test@oanda.com';
		
	   insert ldObj;
	   return ldObj;
	}


	private static Testmethod void testViewSurveyController() {

		ViewSurveyController vsc;
		PageReference pr = Page.TakeSurvey;
	
        Case c = createCase();
		Lead l = createLead();
		Survey__c s = createSurvey();
		pr.getParameters().put('caId', c.Id);	
		pr.getParameters().put('lId', l.Id);
		pr.getHeaders().put('User-Agent', 'iPhone');
		Test.setCurrentPage(pr);
		vsc = new ViewSurveyController(new Apexpages.Standardcontroller(s));
		vsc.submitResults();
		System.assert(!vsc.thankYouRendered);


	    vsc.allQuestions[0].selectedOption = '2';

		vsc.allQuestions[1].selectedOptions = new List<String> { '1', '2' };
		vsc.allQuestions[1].extraFeedback = 'text text';

		vsc.allQuestions[2].selectedOption = '1';
		vsc.allQuestions[2].extraFeedback = 'text text';

		vsc.allQuestions[3].choices = 'text text';
        /* first question */
		vsc.checkSelection();
		vsc.allQuestions[0].selectedOption = '1';
		vsc.checkSelection();	

		vsc.nextQuestion();
		vsc.previousQuestion();

		vsc.submitResults();
		System.assert(vsc.thankYouRendered);

        /**Error if is already submitted */
		vsc.submitResults();
		System.assert(vsc.hasErrors);
		System.assert(vsc.errorMessage == 'surveyTakenError');
	}

	 @istest
	private static void testNoSurvey(){
		ViewSurveyController vsc;
		PageReference pr = Page.TakeSurvey;
		Case c = createCase();
		Survey__c s = new Survey__c();
		pr.getParameters().put('caId', c.Id);
		pr.getParameters().put('change', 'en_US');
		pr.getHeaders().put('User-Agent', 'iPhone');
		Test.setCurrentPage(pr);
		vsc = new ViewSurveyController(new Apexpages.Standardcontroller(s));
		System.assert(vsc.hasErrors);	
		System.assert(vsc.errorMessage == 'noSurveyError');	
	} 
	
	@istest
	private static void testBaseController(){
		ViewSurveyController vsc;
		PageReference pr = Page.TakeSurvey;
		Case c = createCase();
		Survey__c s = createSurvey();
		pr.getParameters().put('caId', c.Id);
		pr.getParameters().put('change', 'en_US');
		pr.getHeaders().put('User-Agent', 'iPhone');
		Test.setCurrentPage(pr);
		vsc = new ViewSurveyController(new Apexpages.Standardcontroller(s));
		List<Map<String, String>> lang = vsc.getLanguages();	
		List<FAQ> faqs = vsc.getTopFaqs();
		List<FAQ> faqsDiv = vsc.getTopFaqsByDivision('');
		System.assert(lang != null);	
		System.assert(vsc.language != null);	
		System.assert(faqs != null);	
		System.assert(faqsDiv != null);	
	}
}