/**
 * @File Name          : DeleteAgentWorkClosing.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/24/2021, 10:58:33 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/24/2021, 10:54:11 PM   acantero     Initial Version
**/
public without sharing class DeleteAgentWorkClosing {

    @InvocableMethod(label='Delete AgentWorkClosing')
    public static void execute(List<Agent_Work_Closing__c> agentWorkClosingList) {
        if (
            (agentWorkClosingList != null) &&
            (!agentWorkClosingList.isEmpty())
        ) {
            delete agentWorkClosingList;
        }
    }
    
}