/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 07-09-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   07-07-2020   dmorales   Initial Version
**/
@isTest
private class ChatOfflineSettings_Test {
    @isTest
    static void testDefaultConstructor() {
        ChatOfflineSettings sett = ChatOfflineSettings.getDefaultInstance();
        System.assert(sett.initializationFails != null);
    }

    @isTest
    static void testConstructor() {
        ChatOfflineSettings sett = new ChatOfflineSettings('Monday','Sunday','00:00','23:59',true);
        System.assert(sett.isOffline == true);
    }

    @isTest
    static void testConstructorReverse() {
       ChatOfflineSettings sett = new ChatOfflineSettings('Sunday','Monday','00:00','23:59',true);
       System.assert(sett.isOffline == true);
    }

    @isTest
    static void testConstructorSameDay() {
       ChatOfflineSettings sett = new ChatOfflineSettings('Monday','Monday','00:00','00:01',true);
       System.assert(sett.isOffline == false);
    }
}