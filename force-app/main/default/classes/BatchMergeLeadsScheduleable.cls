public with sharing class BatchMergeLeadsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	private Map<String, Integer> skippedLeads;
	
	public static final Integer BATCH_SIZE = 20;
	
	public static final String CRON_NAME = 'BatchMergeLeadsScheduleable';
	public static final String CRON_SCHEDULE_1X10 = '0 10 * * * ?';
	
	
	public static void schedule() {
		System.schedule(CRON_NAME + '_1X10', CRON_SCHEDULE_1X10, new BatchMergeLeadsScheduleable());
	}
	
	public BatchMergeLeadsScheduleable() {
//		query = 'SELECT Id, Email FROM Lead WHERE IsConverted = false AND (NOT Email LIKE \'%oanda.com\') AND Duplicate_Email__c = true ORDER BY Email ASC';
		query = 'SELECT Id, Email FROM Lead WHERE IsConverted = false AND Duplicate_Email__c = true ORDER BY Email ASC';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Email FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
   	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		this.skippedLeads = LeadBatchUtil.mergeLeads((Lead[]) batch);
	}
	
	private void verifyPostconditions(Database.BatchableContext bc) {
		
		String msg = '';
		Set<String> emails = new Set<String>();
		
		if (this.skippedLeads != null) {
			
			emails = this.skippedLeads.keySet();
			
			msg += 'Skipped leads:\n\n';
			for (String email : this.skippedLeads.keySet()) {
				msg += email + ': ' + this.skippedLeads.get(email) + '\n';
			}
			msg += '\n\n';
		}
		
		List<Lead> nonMergedLeads = [SELECT Id, Email FROM Lead WHERE Duplicate_Email__c = true AND Email NOT IN :emails];
		
		if (nonMergedLeads.size() > 0) {
			
			Map<String, Set<Id>> leadIdsByEmail = new Map<String, Set<Id>>();
			msg += 'Leads that should have merged but did not:\n\n';
			
			for (Lead lead : nonMergedLeads) {
				Set<Id> ids = leadIdsByEmail.get(lead.Email);
				if (ids == null) {
					ids = new Set<Id>();
					leadIdsByEmail.put(lead.Email, ids);
				}
				ids.add(lead.Id);
			}
			
			for (String email : leadIdsByEmail.keySet()) {
				Set<Id> ids = leadIdsByEmail.get(email);
				msg += email + ': \n';
				for (Id id : ids) {
					msg += '    ' + id + '\n';
				}
				msg += '\n\n';
			}
		}
		
		if (msg != '') {
			String subject = CRON_NAME + ': ' + nonMergedLeads.size() + ' non merged leads';
			EmailUtil.sendEmailForBatchJob(bc.getJobId(), subject, msg);
		}
	}
	
	public void execute(SchedulableContext context) {
		executeBatch();
	}
	
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		/*
		try {
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
			this.verifyPostconditions(bc);
		}
		catch (Exception e) {}
		*/
	}
	
	// BatchMergeLeadsScheduleable.executeBatch()
	public static Id executeBatch() {
		return executeBatch(BATCH_SIZE);
	}
	
	public static Id executeBatch(Integer batchSize) {
		return Database.executeBatch(new BatchMergeLeadsScheduleable(), batchSize);
	}
	
	// Executes lead conversion in same EC
	public static void executeInline() {
		List<Lead> leads = Database.query(new BatchMergeLeadsScheduleable().query);
		LeadBatchUtil.mergeLeads(leads);
	}

}