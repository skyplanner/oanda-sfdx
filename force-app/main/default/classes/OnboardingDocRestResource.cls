/**
 * REST resource to create a document meta data record.
 * 
 * @author Gilbert Gao
 */
@RestResource(urlMapping='/v1/onboarding/document')
global with sharing class OnboardingDocRestResource {
    
    @HTTPPost
	global static String createonBoardingDocument(Integer user_id, Integer doc_type, String file_name) {
		if(!CustomSettings.isAPIEnabled(CustomSettings.API_NAME_ONBOARDING_DOC)){          
            return CDAAdapter.serialize(false, 'API ' + CustomSettings.API_NAME_ONBOARDING_DOC + ' is disabled');
    	}
    	
    	try{
			//find a fxAccount based on the given user id
			List<fxAccount__c> fxAccntLst = [SELECT id, Name FROM fxAccount__c WHERE fxTrade_User_Id__c = : user_id AND RecordTypeId = : RecordTypeUtil.getFxAccountLiveId() LIMIT 1];

			if(!fxAccntLst.isEmpty()){
				
				//Get the corrsponding doc code for the doc type #
				List<Document_Codes__c> doccodeLst = [SELECT DocId__c, Doc_Type__c FROM Document_Codes__c WHERE DocId__c =: doc_type LIMIT 1]; 
				//We create a document record and link the attachment to the document
				Document__c docObj = new Document__c(RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxAccntLst[0].Id, fxTrade_User_Id__c = user_id, Name = file_name, Name__c = file_name, Document_Type__c = doccodeLst[0].Doc_Type__c, Is_My_Number__c = false);

				if(docobj.Document_Type__c == 'My Number'){
					docobj.Is_My_Number__c = true;
				}

				insert docObj;


    			return CDAAdapter.serialize(true, docObj.Id);
			}
			else{
                return CDAAdapter.serialize(false, 'API ' + 'Document could not be created, since fxAccount does not exist for user id '+user_id);
			}
		}
		catch(Exception error){
            return CDAAdapter.serialize(false, 'Document creation failed with error '+error.getMessage() + ' and cause is ' +error.getCause());
		}
	}

}