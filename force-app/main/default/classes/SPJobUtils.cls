/**
 * @File Name          : SPJobUtils.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2023, 2:00:57 AM
**/
public without sharing class SPJobUtils {

	public static final String BATCH_APEX_JOB_TYPE = 'BatchApex';
	public static final String QUEUEABLE_JOB_TYPE = 'Queueable';

	public static Boolean executeBatchIfNoTesting(
		Database.Batchable<SObject> batch,
		Integer scope
	){
		return executeBatchIfCondition(
			batch,
			scope,
			(!Test.isRunningTest()) //condition
		);
	}

	public static Boolean executeBatchIfCondition(
		Database.Batchable<SObject> batch,
		Integer scope,
		Boolean condition
	) {
		Boolean result = false;
		if (condition == true) {
			Database.executeBatch(batch, scope);
			result = true;
		}
		return result;
	}

	public static Boolean isJobRunning(
		String className
	) {
		List<String> runningStatuses = 
				new List<String>{'Preparing', 'Holding', 'Queued', 'Processing'};
		return isJobRunning(
			className,
			runningStatuses
		);
	}

	public static Boolean isJobRunning(
		String className,
		List<String> statusList
	) {
		List<AsyncApexJob> asyncApexJobList = [
			SELECT 
				Id 
			FROM 
				AsyncApexJob 
			WHERE 
				ApexClass.Name = :className
			AND
				Status IN :statusList
			LIMIT 1
		];
		return (!asyncApexJobList.isEmpty());
	}

	public static Boolean abortJob(
		String jobName
	) {
		List<CronTrigger> cronTriggerList = [
			SELECT 
				Id 
			FROM 
				CronTrigger
			WHERE
				CronJobDetail.Name LIKE :jobName
		];
		for(CronTrigger cronTriggerObj : cronTriggerList) {
			System.abortJob(cronTriggerObj.Id);
		}
		return (!cronTriggerList.isEmpty());
	}

	public static AsyncApexJob getLastJobExecutionInfo(
		String className,
		String jobType
	) {
		AsyncApexJob result = null;
		List<AsyncApexJob> recList = [
			SELECT
				CreatedDate,
				JobType,
				Status,
				JobItemsProcessed,
				NumberOfErrors,
				TotalJobItems,
				MethodName,
				ApexClass.Name,
				CompletedDate
			FROM 
				AsyncApexJob
			WHERE
				ApexClass.Name = :className 
			AND
				JobType = :jobType
			ORDER BY
				CreatedDate DESC 
			LIMIT 1
		];
		if(recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

}