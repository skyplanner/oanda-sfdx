/**
 * @File Name          : ServiceChannelHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/12/2021, 12:19:28 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/11/2021, 11:54:27 PM   acantero     Initial Version
**/
public inherited sharing class ServiceChannelHelper {

    public static ServiceChannel getByDevName(
        String devName,
        Boolean required
     ) {
        if (String.isBlank(devName)) {
            throw new ServiceChannelHException(
                'input param "devName" is blank'
            );
        }
        //else...
        List<ServiceChannel> recordList = [
            SELECT
                Id
            FROM
                ServiceChannel
            WHERE
                DeveloperName = :devName
            LIMIT 1
        ];
        if (!recordList.isEmpty()) {
            return recordList[0];
        }
        //else...
        checkRequired(
            required, 
            'ServiceChannel not found, DeveloperName: ' + devName
        );
        return null;
    }

    public static void checkRequired(
        Boolean required, 
        String error
    ){
        if (required == true) {
            throw new ServiceChannelHException(
                error
            );
        }
    }

    //**************************************************************** */

    public class ServiceChannelHException extends Exception {
    }
    
}