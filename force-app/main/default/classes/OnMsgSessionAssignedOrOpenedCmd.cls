/**
 * @File Name          : OnMsgSessionAssignedOrOpenedCmd.cls
 * @Description        : 
 * Updates fields related to the routing process when the routed 
 * Messaging Session is assigned to an agent or opened by an agent
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/15/2024, 1:42:31 AM
**/
public inherited sharing class OnMsgSessionAssignedOrOpenedCmd {

	@TestVisible
	final Set<ID> msgSessionIds;
	
	@TestVisible
	final Set<ID> assignedMsgSessionIds;

	@TestVisible
	final Set<ID> openedMsgSessionIds;

	public List<SObject> recordsToUpdate {get; private set;}

	public OnMsgSessionAssignedOrOpenedCmd() {
		msgSessionIds = new Set<ID>();
		assignedMsgSessionIds = new Set<ID>();
		openedMsgSessionIds = new Set<ID>();
		recordsToUpdate = new List<SObject>();
	}

	/**
	 * Checks if the change corresponds to a MessagingSession record that has
	 * been assigned to an agent or opened by an agent
	 */
	public Boolean checkRecord(
		Schema.SObjectType workItemSObjType, 
		String agentWorkStatus,
		ID workItemId
	) {
		Boolean result = false;

		if (workItemSObjType == Schema.MessagingSession.SObjectType) {
			msgSessionIds.add(workItemId);
			result = true;

			if (
				agentWorkStatus == OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED
			) {
				assignedMsgSessionIds.add(workItemId);

			} else if (
				agentWorkStatus == OmnichanelConst.AGENT_WORK_STATUS_OPENED
			) {
				openedMsgSessionIds.add(workItemId);
			}
		}
		return result;
	}

	public Boolean execute() {
		if (msgSessionIds.isEmpty()) {
			return false;
		}
		//else...
		List<MessagingSession> messagingSessions = 
			MessagingSessionRepo.getCaseInfo(msgSessionIds);
		// Only messaging sessions whose owner is a standard user are valid
		List<MessagingSession> validMessagingSessions = 
			new GetAgentOwnedMsgSessionsCmd(messagingSessions).execute();
		processMessagingSessions(validMessagingSessions);
		return true;
	}

	/**
	 * Updates the ownerId of the cases corresponding to the MessagingSession
	 * records that have been assigned to agents in this run
	 */
	@TestVisible
	void processMessagingSessions(List<MessagingSession> messagingSessions) {
		for (MessagingSession msgSession : messagingSessions) {
			Boolean assigned = assignedMsgSessionIds.contains(msgSession.Id);
			Boolean opened = openedMsgSessionIds.contains(msgSession.Id);
			processMessagingSession(
				msgSession, // msgSession
				assigned, // assigned
				opened // opened
			);
		}
	}

	@TestVisible
	void processMessagingSession(
		MessagingSession msgSession,
		Boolean assigned,
		Boolean opened
	) {
		if (assigned == true) {
			msgSession.Routing_Status__c = 
				OmnichanelConst.ROUTING_STATUS_ASSIGNED;

				if (msgSession.CaseId != null) {
					recordsToUpdate.add(
						new Case(
							Id = msgSession.CaseId,
							OwnerId = msgSession.OwnerId
						)
					);
				}

		} else if (opened = true) {
			msgSession.Routing_Status__c = 
				OmnichanelConst.ROUTING_STATUS_OPENED;
		}
		recordsToUpdate.add(msgSession);
	}
	
}