@RestResource(urlMapping='/api/v1/customer/riskassessment') 
global without sharing class CustomerRiskAssesmentRestResource {

    public static final String LOGGER_CATEGORY = '/api/v1/customer/riskassessment';

    @HttpPost
    global static void doPost() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(req, res);

        try {  
            Map<String, Object> requestObject = (Map<String, Object>) JSON.deserializeUntyped(context.getBodyAsString());

            String divisionName = (String) requestObject.get('division');
            String employmentStatus = (String) requestObject.get('employmentStatus');
            String industryofEmployment = (String) requestObject.get('industryofEmployment');
            String countryOfResidence = (String) requestObject.get('countryOfResidence');
            String citizenshipNationality = (String) requestObject.get('citizenshipNationality');
            String incomeSourceDetails = (String) requestObject.get('incomeSourceDetails');

			Set<String> incomeDetails = new Set<String>(incomeSourceDetails?.split(',') ?? new List<String>()); 
                
            String riskValue = fxAccountRiskAssessment.calculateCustomerRiskAssessment(
                divisionName,
                employmentStatus,
                industryofEmployment,
                countryOfResidence,
                citizenshipNationality,
                incomeDetails
            );
            Map<String,String> riskResponse = new Map<String,String>{'risk' => riskValue};
            context.setResponse(200, riskResponse);       

        } catch(Exception ex) {
            String errorInfo = ex.getMessage() + '\n' + ex.getStackTraceString();
            Logger.error('Error processing request', LOGGER_CATEGORY, errorInfo);
            context.setResponse(500, errorInfo); 
        }
    }
}