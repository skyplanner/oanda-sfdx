/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-19-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class RelatedListMDT {
    private static RelatedListMDT instance;
	private Map<String, Related_List_Setting__mdt> values;

    private RelatedListMDT() {
        values = new Map<String, Related_List_Setting__mdt>();

		for (Related_List_Setting__mdt stt : [
            SELECT
                DeveloperName,
                MasterLabel,
                Object__c,
                Object__r.QualifiedApiName,
                Parent_Id_Field__c,
                Parent_Id_Field__r.Label,
                Parent_Id_Field__r.QualifiedApiName,
                Where_Condition__c,
                (SELECT
                    MasterLabel,
                    Object__c,
                    Object__r.QualifiedApiName,
                    Field__c,
                    Field__r.Label,
                    Field__r.QualifiedApiName,
                    Order__c,
                    Link_to_Record__c,
                    Child_Related__c,
                    Child_Related__r.DeveloperName,
                    Child_Related__r.Object__r.QualifiedApiName
                FROM Related_List_Fields_Setting__r)
            FROM Related_List_Setting__mdt
            WHERE Active__c = TRUE
        ]) {
            values.put(stt.DeveloperName, stt);
        }
    }

    public static RelatedListMDT getInstance() {
		if (instance == null)
			return new RelatedListMDT();

		return instance;
	}

    public Map<String, Related_List_Setting__mdt> getAllMdtMap() {
		return values;
	}    

    public Related_List_Setting__mdt getMdt(String mdtDevName) {
		return values.get(mdtDevName);
	}
}