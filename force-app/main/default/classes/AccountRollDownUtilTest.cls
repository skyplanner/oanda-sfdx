/**
 * AccountRollDownUtil test class
 */
@isTest
public with sharing class AccountRollDownUtilTest {
    
    @isTest
    private static void rollDownFxAccount(){
        Account acc1;
        String accField;
        fxAccount__c fxAcc1;

        RecordType personAccountRecordType =
            [SELECT Id 
                FROM RecordType 
                WHERE DeveloperName = 'PersonAccount' 
                AND SObjectType = 'Account'];

        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.ACCOUNT);
        
        fxAcc1 = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM,
            Government_ID__c = '0123456789');
        insert fxAcc1;
        
        acc1 = new Account(
            fxAccount__c = fxAcc1.Id, 
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            PersonEmail = 'a@b.com',
            FirstName = 'Bartolo',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            LastName = 'Colon',
            MiddleName = 'Iglesias',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155',
            Suffix = 'Suff1',
            Salutation = 'Salut1',
            RecordTypeId = personAccountRecordType.Id);
        insert acc1;

        fxAcc1.Account__c = acc1.Id;
        update fxAcc1;

        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.ACCOUNT);

        Test.startTest();

        // Update one of the roll down fields
        acc1.Salutation = 'Salut2';
        update acc1;

        // Retrieve fxAccount with all the fields
        // that should be rolled down from account
        
        String queryFxAcc =
            'SELECT ' + SoqlUtil.getSoqlFieldList(AccountRollDownUtil.FXACC_FIELDS) + 
            ' FROM fxAccount__c WHERE Id = \'' + fxAcc1.Id + '\'';

        System.debug(
            'AccountRollDownUtilTest-queryFxAcc: ' +
            queryFxAcc);

        fxAcc1 = Database.query(queryFxAcc);

        System.debug(
            'AccountRollDownUtilTest-fxAcc1: ' + 
            fxAcc1);

        for(String fxAccField :AccountRollDownUtil.FXACC_FIELDS) {
            accField =
                AccountRollDownUtil.FXACC_ACC_FIELDS_MAP.get(
                    fxAccField);
            
            // Check that account fields were rolled down to fxAccount
            System.assert(
                fxAcc1.get(fxAccField) != null,
                'fxAccount field "' + fxAccField + 
                '" is empty.');
            System.assertEquals(
                fxAcc1.get(fxAccField),
                acc1.get(accField),
                'fxAccount field "' + fxAccField + 
                '" was not rolled down to fxAccount.');
        }

        Test.stopTest();
    }

    @isTest
    private static void rollDownOpportunity(){
        Account acc1;
        String accField;
        fxAccount__c fxAcc1;
        
        RecordType personAccountRecordType =
            [SELECT Id 
                FROM RecordType 
                WHERE DeveloperName = 'PersonAccount' 
                AND SObjectType = 'Account'];

        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.ACCOUNT);
        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        
        fxAcc1 = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM,
            Government_ID__c = '0123456789');
        insert fxAcc1;

        acc1 = new Account(
            fxAccount__c = fxAcc1.Id, 
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            PersonEmail = 'a@b.com',
            FirstName = 'Bartolo',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            LastName = 'Colon',
            MiddleName = 'Iglesias',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155',
            Suffix = 'Suff1',
            Salutation = 'Salut1',
            RecordTypeId = personAccountRecordType.Id);
        insert acc1;

        Opportunity opp1 = new Opportunity(
            Name = 'test opp',
            StageName = 'test stage',
            CloseDate = Date.today(),
            AccountId = acc1.Id);
        insert opp1;

        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.ACCOUNT);
        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.FXACCOUNT);

        Test.startTest();

        // Update one of the roll down fields
        acc1.Phone = '9999998888';
        update acc1;

        // Retrieve opportunities with all the fields
        // that should be rolled down from account
        
        String queryOpp =
            'SELECT ' + SoqlUtil.getSoqlFieldList(AccountRollDownUtil.OPP_FIELDS) + 
            ' FROM Opportunity WHERE Id = \'' + opp1.Id + '\'';

        System.debug(
            'AccountRollDownUtilTest-queryOpp: ' +
            queryOpp);

        opp1 = Database.query(queryOpp);

        System.debug(
            'AccountRollDownUtilTest-opp1: ' + 
            opp1);

        for(String oppField :AccountRollDownUtil.OPP_FIELDS) {
            accField =
                AccountRollDownUtil.OPP_ACC_FIELDS_MAP.get(
                    oppField);
            
            // Check that account fields were rolled down to opportunities
            System.assert(
                opp1.get(oppField) != null,
                'Opportunity field "' + oppField + 
                '" is empty.');
            System.assertEquals(
                opp1.get(oppField),
                acc1.get(accField),
                'Opportunity field "' + oppField + 
                '" was not rolled down to opportunity.');
        }

        Test.stopTest();
    }

}