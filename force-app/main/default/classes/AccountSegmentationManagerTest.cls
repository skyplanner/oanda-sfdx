/**
 * @File Name          : AccountSegmentationManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/15/2024, 5:11:12 PM
**/
@IsTest
private without sharing class AccountSegmentationManagerTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createSegmentation1(initManager);
		initManager.storeData();
	}

	// test 1 : accountObj <> null
	// test 2 : accountObj = null, leadObj <> null
	// test 3 : accountObj = null, leadObj = null
	@IsTest
	static void getValidFxAccount() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Account accountObj1 = new Account(
			fxAccount__c = fxAccountId
		);
		Lead leadObj1 = new Lead();
		Lead leadObj2 = new Lead(
			fxAccount__c = fxAccountId
		);
	
		Test.startTest();
		// test 1 => return accountObj.fxAccount__c
		ID result1 = AccountSegmentationManager.getValidFxAccount(
			accountObj1, // accountObj
			leadObj1 // leadObj
		);
		// test 2 => return leadObj.fxAccount__c
		ID result2 = AccountSegmentationManager.getValidFxAccount(
			null, // accountObj
			leadObj2 // leadObj
		);
		// test 3 => return null
		ID result3 = AccountSegmentationManager.getValidFxAccount(
			null, // accountObj
			null // leadObj
		);
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isNotNull(result2, 'Invalid result');
		Assert.isNull(result3, 'Invalid result');
	}

	// test 1 : fxAccountId = null
	// test 2 : no fxAccountId previously registered
	// test 3 : valid fxAccountId, segmentationsByFxAccountId = null
	// test 4 : valid fxAccountId, segmentationsByFxAccountId <> null
	@IsTest
	static void getSegmentation() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		ID segmentation1Id = initManager.getObjectId(
			ServiceTestDataKeys.SEGMENTATION_1,
			true
		);

		Test.startTest();
		AccountSegmentationManager instance = new AccountSegmentationManager();
		// test 1 => return null
		Segmentation__c result1 = instance.getSegmentation(null);
		// test 2 => return null
		Segmentation__c result2 = instance.getSegmentation(fxAccount1Id);
		// test 3 => return SEGMENTATION_1 obj
		instance.registerFxAccount(fxAccount1Id);
		Segmentation__c result3 = instance.getSegmentation(fxAccount1Id);
		// test 4 => return SEGMENTATION_1 obj
		Segmentation__c result4 = instance.getSegmentation(fxAccount1Id);
		Test.stopTest();
		
		Assert.isNull(result1, 'Invalid result');
		Assert.isNull(result2, 'Invalid result');
		Assert.areEqual(segmentation1Id, result3.Id, 'Invalid result');
		Assert.areEqual(segmentation1Id, result4.Id, 'Invalid result');
	}
	
}