/**
 * @File Name          : ExternalFileUrlInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2023, 11:53:46 AM
**/
public without sharing class ExternalFileUrlInfo {

	public static String EXTERNAL_FILE_BASE_URL = 'External_File_Base_URL';
	public static String DEFAULT_LANGUAGE = 'en_US';

	public Boolean isRelative {get; set;}
	public String key {get; set;}
	public String languageCode {get; set;}
	public String region {get; set;}
	public String url {get; set;}

	public ExternalFileUrlInfo() {
	}
	
	public ExternalFileUrlInfo(External_File_URL__mdt rec) {
		this.isRelative = rec.Is_Relative__c;
		this.key = rec.Key__c;
		this.languageCode = rec.Language_Code__c;
		this.region = rec.Region__c;
		this.url = rec.URL__c;
		if (isRelative == true) {
			this.url = SPSettingsManager.getSetting(EXTERNAL_FILE_BASE_URL) +
				this.url;
		}
	}

	public static ExternalFileUrlInfo getByKeyRegionAndLanguage(
		String key,
		String region,
		String language
	) {
		ExternalFileUrlInfo result = null;
		
		if (String.isBlank(language)) {
			language = DEFAULT_LANGUAGE;
		}

		if (String.isBlank(region)) {
			region = 'OC';
		}

		External_File_URL__mdt rec = getRecByKeyRegionAndLanguage(
			key, // key
			region, // region
			language // language
		);

		if (rec != null) {
			result = new ExternalFileUrlInfo(rec);
		}
		
		return result;
	}

	@TestVisible
	static External_File_URL__mdt getRecByKeyRegionAndLanguage(
		String key,
		String region,
		String language
	) {
		List<External_File_URL__mdt> recList = [
			SELECT
				Is_Relative__c,
				Key__c,
				Language_Code__c,
				Region__c,
				URL__c
			FROM External_File_URL__mdt
			WHERE Key__c = :key
			AND Region__c = :region
			AND Language_Code__c = :language
			LIMIT 1
		];

		if (recList.isEmpty()) {
			String errorMsg = 'External_File_URL__mdt record not found, ' +
				'key: ' + key +
				', region: ' + region +
				', language: ' + language;

			Logger.error(
				errorMsg, // msg
				ExternalFileUrlInfo.class.getName() // category
			);
			return null;
		} 
		// else...
		return recList[0];
	}

}