/**
 * @File Name          : ChatBaseSLAViolationNotifier.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:44:31 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 9:04:41 AM   aniubo     Initial Version
 **/
public abstract inherited sharing class ChatBaseSLAViolationNotifier extends SLAViolationBaseNotifier {
	protected SLAViolationNotificationManager notificationManagerDiv {
		get;
		set;
	}

	protected List<SLAViolationInfo> violationInfosNoOwner = new List<SLAViolationInfo>();
	protected List<SLAViolationInfo> violationInfosOwner = new List<SLAViolationInfo>();

	public ChatBaseSLAViolationNotifier(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		SLAViolationCanNotifiable violationNotificationChecker
	) {
		super(
			notificationObjectType,
			violationType,
			dataReader,
			violationNotificationChecker
		);
	}
	public override void notifyViolation(
		SLAViolationNotificationData notificationData
	) {
		List<SObject> records = violationDataReader.getData(notificationData);
		if (records != null && !records.isEmpty()) {
			List<SLAViolationInfo> violationInfos = getViolationInfos(records);
			if (
				violationNotificationChecker.canNotify(
					violationType,
					this.notificationObjectType
				)
			) {
				setSLAViolationNotificationManager();
				if (
					violationInfosOwner != null &&
					!violationInfosOwner.isEmpty() &&
					notificationManager != null
				) {
					notificationManager.sendNotifications(violationInfosOwner);
				}
				if (
					violationInfosNoOwner != null &&
					!violationInfosNoOwner.isEmpty() &&
					notificationManagerDiv != null
				) {
					notificationManagerDiv.sendNotifications(
						violationInfosNoOwner
					);
				}
			}
			saveViolationInfo(violationInfos);
		}
	}

	protected override List<SLAViolationInfo> getViolationInfos(
		List<SObject> records
	) {
		List<SLAViolationInfo> violationInfos = new List<SLAViolationInfo>();
		String logDetails = 'infos: ';
		for (SObject record : records) {
			SLAViolationInfo info = createViolationInfo(record);
			logDetails += JSON.serialize(info) + '\\n';
			violationInfos.add(info);
			if (String.isBlank(info.ownerId)) {
				violationInfosNoOwner.add(info);
			} else {
				violationInfosOwner.add(info);
			}
		}
		logManager.log(
			'ChatBaseSLAViolationNotifier.getViolationInfos',
			'SLA',
			logDetails
		);
		return violationInfos;
	}

	protected override void setSLAViolationNotificationManager() {
		if (!violationInfosOwner.isEmpty()) {
			this.notificationManager = SLAViolationNotificationManagerFactory.createNotificationManager(
				getSLANotificationObjectType(),
				this.violationType,
				SLAConst.AgentSupervisorModel.ROLE,
				this.settingsByViolationType,
				new SupervisorProviderFactory()
			);
		}

		if (!violationInfosNoOwner.isEmpty()) {
			this.notificationManagerDiv = SLAViolationNotificationManagerFactory.createNotificationManager(
				getSLANotificationObjectType(),
				this.violationType,
				SLAConst.AgentSupervisorModel.DIVISION,
				this.settingsByViolationType,
				new SupervisorProviderFactory()
			);
		}
	}

	// protected override List<SLAViolationInfo> filterViolationNotifiable(
	// 	List<SLAViolationInfo> infos
	// ) {
	// 	return filter(infos);
	// }
	protected abstract String getChatDivision(SObject obj);

	protected abstract String getChatDivisionCode(SObject obj);

	protected abstract String getChatAccountName(SObject obj);

	protected String getAgentName(Id agentId) {
		List<User> agents = [SELECT Name FROM User WHERE Id = :agentId];
		return agents.isEmpty() ? null : agents.get(0).Name;
	}

	// protected virtual List<SLAViolationInfo> filter(
	// 	List<SLAViolationInfo> infos
	// ) {
	// 	List<SLAViolationInfo> filteredInfos = new List<SLAViolationInfo>();
	// 	for (SLAViolationInfo info : infos) {
	// 		SLAChatViolationInfo chatInfo = (SLAChatViolationInfo) info;
	// 		if (chatInfo.isNotifiable) {
	// 			filteredInfos.add(chatInfo);
	// 		}
	// 	}
	// 	return filteredInfos;
	// }
	protected virtual SLAConst.SLANotificationObjectType getSLANotificationObjectType() {
		return SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT;
	}
}