/**
 * Document__c trigger handler class
 */
public class DocumentTriggerHandler {

    List<Document__c> newList;
    Map<Id, Document__c> oldMap;
    
    Set<Id> caseIds;
    Map<Id, Case> casesMap;

    Set<Id> fxAccIds;
    Map<Id, fxAccount__c> fxAccsMap;

    private static List<Document__c> docsToSend;

    /**
     * Constructor
     */
    public DocumentTriggerHandler(
        List<Document__c> newList)
    {
        this.newList = newList;
    }

    /**
     * Constructor
     */
    public DocumentTriggerHandler(
        List<Document__c> newList,
        Map<Id, Document__c> oldMap)
    {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    /**
     * Handles the before insert event
     */
    public void onBeforeInsert(){
        // Load related records ids
        loadRelatedRecordsIds();
        
        // Load related records
        loadCases();
        loadFxAccs();
        
        // TODO: Try to refactoring with "DocumentLinkUtil.link()"
        DocumentUtil.processDocsForCase(newList);
        //DocumentUtil.linkDocstoLeadOrAccount(newList);

        // TBD: This is to replace 
        // "Link Document to Profile" process builder
        new DocumentLinkUtil(
            newList,
            casesMap,
            fxAccsMap).linkOnInsert();
    }

    /**
     * Handles the before update event
     */
    public void onBeforeUpdate(){
         // Load related records ids
         loadRelatedRecordsIds();
        
         // Load related records
         loadCases();
         loadFxAccs();
         
        // TBD: This is to replace 
        // "Link Document to Profile" process builder
        new DocumentLinkUtil(
            newList,
            casesMap,
            fxAccsMap).linkOnUpdate();
    }

    /**
     * Handles the after insert event
     */
    public void onAfterInsert(){
        // fxAccounts ids to set as 'Approved to Fund'
        Set<Id> fxAccIdsSetFunnelStage =
            new Set<Id>();

        // Iterate trigger records
        for(Document__c doc :newList) {
            // Know if document's fxAccount should be set as 'Approved to Fund'
            if(DocumentUtil.shouldFxAccSetAsDocUploaded(doc)) {
                fxAccIdsSetFunnelStage.add(
                    doc.Case_fxAccount__c);
            }

            // Add more here...
        }

        // Do processes

        // Set fxAccount as 'Approved to Fund'
        fxAccountUtil.setFunnelStageDocUploaded(
            fxAccIdsSetFunnelStage);

        if(CustomSettings.isCustomNotificationForSalesEnabled()) 
        {
            CustomNotificationManager.sendNotificationsForOGMCases(newList, oldMap);
        }

        // Add more here...
    }

    /**
     * Handles the after update event
     */
    public void onAfterUpdate(){

        if(CustomSettings.isCustomNotificationForSalesEnabled()) 
        {
            CustomNotificationManager.sendNotificationsForOGMCases(newList, oldMap);
        }

        // Get documents ready to be sent to IHS
        for(Document__c doc : newList)
        {
            Document__c oldDoc = oldMap.get(doc.Id);
            
            if(doc.IHS_Document_Status__c == Constants.IHS_DOCUMENT_STATUS_READY &&
            (doc.IHS_Document_Status__c != oldDoc.IHS_Document_Status__c))
            {
                if(docsToSend == null) {
                    docsToSend = new List<Document__c>();
                }
                docsToSend.add(doc);
            }
        }

        // Commit changes
        publishEvents();
    }

    /**
     * Load related records ids
     */
    private void loadRelatedRecordsIds()
    {
        caseIds = new Set<Id>();
        fxAccIds = new Set<Id>();
        // Add more related objects ids
        
        // Iterare doc to load related records ids
        for(Document__c doc : newList) {
            if(doc.Case__c <> null) {
                caseIds.add(doc.Case__c);
            }
            if(doc.fxAccount__c <> null) {
                fxAccIds.add(doc.fxAccount__c);
            }
            // Add more related objects here
        }
     }

    /**
      * Load related cases
      */
    private void loadCases() {
        casesMap = new Map<Id, Case>();

        if(caseIds.isEmpty())
            return;
        
        casesMap = new Map<Id, Case>(
            [SELECT Id,
                    AccountId,
                    fxAccount__c,
                    Lead__c,
                    Lead__r.IsConverted,
                    Lead__r.RecordTypeId
                    // Add more fields here
                FROM Case
                WHERE Id IN :caseIds]);
    }

    /**
     * Load related cases
     */
    private void loadFxAccs() {
        fxAccsMap = new Map<Id, fxAccount__c>();

        if(fxAccIds.isEmpty())
            return;
        
        fxAccsMap = new Map<Id, fxAccount__c>(
            [SELECT Id,
                    Lead__c,
                    Account__c
                    // Add more fields here
                FROM fxAccount__c
                WHERE Id IN :fxAccIds]);
    }

    private void publishEvents() {
        if(docsToSend != null && !docsToSend.isEmpty()) {
            new IHSEventManager(docsToSend).scan();
            docsToSend = null;
        }
    }

}