@isTest
private class ApiMappingServiceTest {

    private static String flatJsonObjectStringOnly = '{\"abc\":\"A - mock\",\"def\":\"B - mock\",\"ghi\":\"C - mock\",\"jkl\":\"D - mock\",\"mno\":\"E - mock\"}';
    private static String flatJsonObjectIntegerOnly = '{\"def\":123,\"mno\":456}';
    private static String flatJsonObjectDate = '{\"abc\":\"2024/12/12\",\"def\":\"2024.12.12\"}';


    @isTest
    static void shouldMapFlatObjectWithoutForLoop() {
        
        String testObjectName = 'fxAccount__c';

        Map<String, Endpoints_Mappings__mdt> currentObjMapping = new Map<String,Endpoints_Mappings__mdt> {
            'abc' => construtctCustomMetadata('abc', 'String', 'Knowledge_Answer_1__c'),
            'def' => construtctCustomMetadata('def', 'String', 'Knowledge_Answer_2__c'),
            'ghi' => construtctCustomMetadata('ghi', 'String', 'Knowledge_Answer_3__c'),
            'jkl' => construtctCustomMetadata('jkl', 'String', 'Knowledge_Answer_4__c')
        };

        Map<String, Map<String, Endpoints_Mappings__mdt>> mappingByEndpointName = 
            new Map<String, Map<String, Endpoints_Mappings__mdt>>{testObjectName => currentObjMapping};

        Map<String,Object> payload = (Map<String,Object>) JSON.deserializeUntyped(flatJsonObjectStringOnly);

        ApiMappingService mapper = new ApiMappingService(testObjectName);
        mapper.endpointMappings = mappingByEndpointName;
        
        fxAccount__c fxa = new fxAccount__c();

        LoggerTestDataFactory.enableLogSettings();
        
        Test.startTest();
        mapper.setInputFieldsToSobject(fxa, payload);
        Test.getEventBus().deliver();
        Test.stopTest();
  
        for(String key : currentObjMapping.keySet()){
            System.debug(key);
            String sobjectField = currentObjMapping.get(key).Sobject_Field_Name__c;
            Assert.areEqual((String) payload.get(key), fxa.get(sobjectField), 'Should map field to SObject');
        }

        List<Log__c> mappingErrors = [SELECT Id FROM Log__c];
        System.debug(mappingErrors);
        Assert.areNotEqual(0, mappingErrors.size(), 'There should be unmapped property');
    }

    @isTest
    static void shouldMapFlatObject() {
        
        Map<String, Endpoints_Mappings__mdt> currentObjMapping = new Map<String,Endpoints_Mappings__mdt> {
            'abc' => construtctCustomMetadata('abc', 'String', 'Knowledge_Answer_1__c'),
            'def' => construtctCustomMetadata('def', 'String', 'Knowledge_Answer_2__c'),
            'ghi' => construtctCustomMetadata('ghi', 'String', 'Knowledge_Answer_3__c'),
            'jkl' => construtctCustomMetadata('jkl', 'String', 'Knowledge_Answer_4__c'),
            'mno' => construtctCustomMetadata('mno', 'else', 'Knowledge_Answer_5__c')
        };

        Map<String,Object> payload = (Map<String,Object>) JSON.deserializeUntyped(flatJsonObjectStringOnly);

        ApiMappingService mapper = new ApiMappingService();

        fxAccount__c fxa = new fxAccount__c();

        for(String inputField : payload.keySet()) {
            mapper.setInputFieldtoSobject(
                fxa, 
                currentObjMapping.get(inputField), 
                payload
            );
        }

        for(String key : currentObjMapping.keySet()){
            String sobjectField = currentObjMapping.get(key).Sobject_Field_Name__c;
            Assert.areEqual((String) payload.get(key), fxa.get(sobjectField), 'Should map field to SObject');
        }
    }

    @isTest
    static void shouldMapFlatObjectWithNumbers() {
        
        Map<String, Endpoints_Mappings__mdt> currentObjMapping = new Map<String,Endpoints_Mappings__mdt> {
            'def' => construtctCustomMetadata('def', 'Integer', 'Number_of_Trades_Last_30_Days__c'),
            'mno' => construtctCustomMetadata('mno', 'Integer', 'EID_Pass_Count__c')
        };

        Map<String,Object> payload = (Map<String,Object>) JSON.deserializeUntyped(flatJsonObjectIntegerOnly);

        ApiMappingService mapper = new ApiMappingService();

        fxAccount__c fxa = new fxAccount__c();

        for(String inputField : payload.keySet()) {
            mapper.setInputFieldtoSobject(
                fxa, 
                currentObjMapping.get(inputField), 
                payload
            );
        }

        for(String key : currentObjMapping.keySet()){
            String sobjectField = currentObjMapping.get(key).Sobject_Field_Name__c;
            Assert.areEqual((Integer) payload.get(key), fxa.get(sobjectField), 'Should map field to SObject');
        }
    }


    @isTest
    static void shouldMapAndNormalzieDateField() {
        
        Map<String, Endpoints_Mappings__mdt> currentObjMapping = new Map<String,Endpoints_Mappings__mdt> {
            'abc' => construtctCustomMetadata('abc', 'NORMALIZE_DATE', 'Account_Close_Date__c'),
            'def' => construtctCustomMetadata('def', 'NORMALIZE_DATE', 'Birthdate__c')
        };
        
        Map<String,Object> payload = (Map<String,Object>) JSON.deserializeUntyped(flatJsonObjectDate);

        ApiMappingService mapper = new ApiMappingService();

        fxAccount__c fxa = new fxAccount__c();

        for(String inputField : payload.keySet()) {
            mapper.setInputFieldtoSobject(
                fxa, 
                currentObjMapping.get(inputField), 
                payload
            );
        }

        Assert.areEqual(fxa.Account_Close_Date__c, Date.newInstance(2024, 12, 12));
        Assert.areEqual(fxa.Birthdate__c, Date.newInstance(2024, 12, 12));
    }

    private static Endpoints_Mappings__mdt construtctCustomMetadata(String inputFieldName, String dataType, String sobjectFieldName) {
        Endpoints_Mappings__mdt mdtRecord = new Endpoints_Mappings__mdt();
        mdtRecord.Input_Field_Name__c = inputFieldName;
        mdtRecord.Data_Type__c = dataType;
        mdtRecord.Sobject_Field_Name__c = sobjectFieldName;

        return mdtRecord;
    }
}