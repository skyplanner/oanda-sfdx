public with sharing class OutgoingNotificationEventTriggerHandler extends TriggerHandler{
    private List<Outgoing_Notification__e> newOutgoingNotifications;

    public OutgoingNotificationEventTriggerHandler() {
        this.newOutgoingNotifications = (List<Outgoing_Notification__e>) Trigger.new;
    }

    public override void afterInsert() {
        OutgoingNotificationBus.runJob(this.newOutgoingNotifications);
    }
}