@isTest
private class EmailCaseProcessorTest {

    static testMethod void testEmailWithoutCustomHeader() {

	   // Create a new email, envelope object and Attachment
	   Messaging.InboundEmail email = new Messaging.InboundEmail();
	   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	   Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
	
	   email.subject = 'test';
	   email.plainTextBody = 'test body';
	   email.plainTextBodyIsTruncated = false;
	   email.fromAddress = 'user@acme.com';
	   env.fromAddress = 'user@acme.com';
	   
	   List<Messaging.InboundEmail.Header> headers = new List<Messaging.InboundEmail.Header>();
	   email.headers = headers;
	
	   // set the body of the attachment
	   inAtt.body = blob.valueOf('test');
	   inAtt.fileName = 'my_attachment_name.txt';
	   inAtt.mimeTypeSubType = 'plain/txt';
	
	   email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt };
	
	
	   // call the class and test it with the data in the testMethod
	   EmailCaseProcessor emailServiceObj = new EmailCaseProcessor();
	   emailServiceObj.handleInboundEmail(email, env );  
	   
	   //check if the case is created
	   Case testCase = [select id, Description, Lead__r.email from Case where Subject = 'test'][0]; 
	   
	   System.assertEquals('test body', testCase.description);
	   
	   //test if a new lead is created
	   System.assertEquals('user@acme.com', testCase.Lead__r.email);
	   
	   Document__c doc = [select id, name from Document__c where case__c = : testCase.Id][0];
	   System.assertEquals('my_attachment_name.txt', doc.name);           

	} 
	
	static testMethod void testEmailWithCustomHeader() {
		
	   //create a new lead
	   Lead customer = new Lead(lastname = 'test lead', email = 'test@abc.com');
	   insert customer;

	   // Create a new email, envelope object and Attachment
	   Messaging.InboundEmail email = new Messaging.InboundEmail();
	   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	   Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
	
	   email.subject = 'test';
	   email.plainTextBody = 'test body';
	   email.plainTextBodyIsTruncated = false;
	   env.fromAddress = 'user@acme.com';
	   
	   List<Messaging.InboundEmail.Header> headers = new List<Messaging.InboundEmail.Header>();
	   Messaging.InboundEmail.Header h1 = new Messaging.InboundEmail.Header();
	   h1.name = 'X-OANDA-DELIVER-TO';
	   h1.value = 'test@abc.com';
	   
	   headers.add(h1);
	   email.headers = headers;
	
	   // set the body of the attachment
	   inAtt.body = blob.valueOf('test');
	   inAtt.fileName = 'my_attachment_name.txt';
	   inAtt.mimeTypeSubType = 'plain/txt';
	
	   email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt };
	
	
	   // call the class and test it with the data in the testMethod
	   EmailCaseProcessor emailServiceObj = new EmailCaseProcessor();
	   emailServiceObj.handleInboundEmail(email, env );  
	   
	   //check if the case is created
	   Case testCase = [select id, Description, Lead__c from Case where Subject = 'test'][0]; 
	   
	   System.assertEquals('test body', testCase.description);
	   System.assertEquals(customer.Id, testCase.Lead__c);
	   
	   Document__c doc = [select id, name from Document__c where case__c = : testCase.Id][0];
	   System.assertEquals('my_attachment_name.txt', doc.name);           

	}
	
	static testMethod void testEmailWithMultipleAttachments(){
		//create a new lead
	   Lead customer = new Lead(lastname = 'test lead', email = 'test@abc.com');
	   insert customer;

	   // Create a new email, envelope object and Attachment
	   Messaging.InboundEmail email = new Messaging.InboundEmail();
	   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	   Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
	   Messaging.InboundEmail.BinaryAttachment inAtt1 = new Messaging.InboundEmail.BinaryAttachment();
	
	   email.subject = 'test';
	   email.plainTextBody = 'test body';
	   email.plainTextBodyIsTruncated = false;
	   env.fromAddress = 'user@acme.com';
	   
	   List<Messaging.InboundEmail.Header> headers = new List<Messaging.InboundEmail.Header>();
	   Messaging.InboundEmail.Header h1 = new Messaging.InboundEmail.Header();
	   h1.name = 'X-OANDA-DELIVER-TO';
	   h1.value = 'test@abc.com';
	   
	   headers.add(h1);
	   email.headers = headers;
	
	   // set the body of the attachment
	   inAtt.body = blob.valueOf('test');
	   inAtt.fileName = 'my_attachment_name.txt';
	   inAtt.mimeTypeSubType = 'plain/txt';
	   
	   inAtt1.body = blob.valueOf('test1');
	   inAtt1.fileName = 'my_attachment_name1.txt';
	   inAtt1.mimeTypeSubType = 'plain/txt';
	
	   email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt, inAtt1 };
	
	
	   // call the class and test it with the data in the testMethod
	   EmailCaseProcessor emailServiceObj = new EmailCaseProcessor();
	   emailServiceObj.handleInboundEmail(email, env );  
	   
	   //check if the case is created
	   Case testCase = [select id, Description, Lead__c from Case where Subject = 'test'][0]; 
	   
	   System.assertEquals('test body', testCase.description);
	   System.assertEquals(customer.Id, testCase.Lead__c);
	   
	   List<Document__c> docs = [select id, name from Document__c where case__c = : testCase.Id and name in ('my_attachment_name.txt', 'my_attachment_name1.txt')];
	   System.assertEquals(2, docs.size());        
	}

}