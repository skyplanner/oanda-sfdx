/**
 * @File Name          : HelpPortalCountrySelectionCtrl.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/3/2020, 11:45:27 AM
 * @Modification Log   : 
 * Ver    Date        Author      Modification
 * 1.0    10/8/2019   dmorales    Initial Version
 * 1.1    10/8/2019   fgomez      SP-9089
 * Updated: By Sahil Handa for SP-8705, Adding Russian Error page from Selection

**/
public without sharing class HelpPortalCountrySelectionCtrl extends HelpPortalBase {
    
    //public String statusCookies {get;set;}
    public String countrySelected {get;set;}
    public Map<String, List<Country_Code__mdt>> mapRegionCountry {get;set;}
    public List<String> geoRegion {get;set;}
    public String artP1 {get;set;}
    public String artP2 {get;set;}


    public HelpPortalCountrySelectionCtrl() {
        super();       
        geoRegion = loadGeogRegion();
        mapRegionCountry = loadCountries();
        
        PageReference currentPage = ApexPages.currentPage();
		artP1 = currentPage.getParameters().get('artP1');
		artP2 = currentPage.getParameters().get('artP2');
    }

    public PageReference setCookies(){        
                 
       /* Map<String,Integer> mapRegionCount = new Map<String,Integer>();
       //create a map from division setting list of regions
         mapRegionCount.put('OCAN',0);
         mapRegionCount.put('OC',0);
         mapRegionCount.put('OEL',0);
         mapRegionCount.put('OAU',0);
         mapRegionCount.put('OAP',0);

         Cookie numberOfChange = new Cookie('region_number', JSON.serialize(mapRegionCount) ,null,611040000,false);
         ApexPages.currentPage().setCookies(new Cookie[]{country, region, numberOfChange});*/
         
         Cookie country = new Cookie('prefered_country', countrySelected ,null,611040000,false);
         Country_Code__mdt countryMeta = CountryCodeMetadataUtil.getCountryByCode(countrySelected);
         Cookie region = new Cookie('prefered_region', countryMeta.Region__c ,null,611040000,false);

         ApexPages.currentPage().setCookies(new Cookie[]{country, region});
         String languageCode = getLanguageCode(countryMeta.Default_Language__c);
         String timestamp = String.valueof(DateTime.now().getTime());
         String redirect = '/?change='+languageCode+'&country='+countrySelected+'&when='+timestamp;
         if (String.isNotBlank(artP1) && String.isNotBlank(artP2)) {
            redirect += '#!/article/' + artP1 + '/' + artP2;
         }
         
		PageReference pageRef = new PageReference(redirect);
		return pageRef;
    }

    public List<String> loadGeogRegion(){
        List<Geographical_Region_Setting__mdt> geoRegion = CountryCodeMetadataUtil.getGeographicalRegionOrder();
        System.debug('Region Country ' + geoRegion);
        List<String> geoRegString = new List<String>();
        for(Geographical_Region_Setting__mdt reg: geoRegion){
          geoRegString.add(reg.Geographical_Region__c);
        }
        return geoRegString;
    }

    public Map<String, List<Country_Code__mdt>> loadCountries(){
      
        List<Country_Code__mdt> countries = CountryCodeMetadataUtil.getPortalCountries();     

        Map<String, List<Country_Code__mdt>> mapRC = new Map<String, List<Country_Code__mdt>>();
        Country_Code__mdt lastCountryOAU = new Country_Code__mdt();             
        for(Country_Code__mdt country : countries ){
             country.Country__c = String.isNotEmpty(country.Alternative_Name__c)? country.Alternative_Name__c : country.Country__c;
             if(!mapRC.containsKey(country.Geographical_Region__c)){
                mapRC.put(country.Geographical_Region__c,new List<Country_Code__mdt>{country});
             }
             else{
                List<Country_Code__mdt> countriesForRegion = mapRC.get(country.Geographical_Region__c);
                countriesForRegion.add(country);
                mapRC.put(country.Geographical_Region__c, countriesForRegion);
             }            
        }  

        return mapRC;
    } 

}