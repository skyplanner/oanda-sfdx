/**
 * Test class: FAQ
 * @author Fernando Gomez
 * @version 1.0
 * @since April 1st, 2019
 */
@isTest
private class FAQTest {
	/**
	 * global static List<FAQ> doWrap(List<FAQ__kav> faqs)
	 */
	@isTest
	static void doWrap() {
		FAQ r;
		FAQ__kav f;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question'
		);
		insert f;

		System.assertEquals('This is a question', FAQ.doWrap(f).title);
		System.assertEquals('This is a question',
			FAQ.doWrap(new List<FAQ__kav> { f })[0].title);
		System.assertEquals('snippet', FAQ.doWrap(f, 'snippet', 'snippet').title);
	}

	@isTest
	static void doWrapSection() {
		FAQ r;
		FAQ__kav f;
        String html = '<div id="section0_0"></div>';
		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = html,
			UrlName = 'This-is-a-question'
		);
		insert f;

		System.assertEquals('This is a question', FAQ.doWrap(f).title);
	}
}