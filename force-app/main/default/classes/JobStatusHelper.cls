/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 07-28-2022
 * @last modified by  : Ariel Niubo
**/
@SuppressWarnings('PMD.ApexCRUDViolation')
public without sharing class JobStatusHelper {
	public static final String ACTIVE_STATUS = 'Active';
	public static final String INACTIVE_STATUS = 'Inactive';

	public static Datetime getJobCurrentRequestDate(String jobName) {
		Datetime result = [
			SELECT Request_Date__c
			FROM Job_Status__c
			WHERE Name = :jobName
			LIMIT 1
		]
		?.Request_Date__c;
		if (result != null) {
			return result;
		}
		//else...
		throw new JobStatusException('Job Status not found: ' + jobName);
	}

	public static Job_Status__c getJobStatusForUpdate(String jobName) {
		List<Job_Status__c> jobStatusList = [
			SELECT Status__c, Request_Date__c
			FROM Job_Status__c
			WHERE Name = :jobName
			LIMIT 1
			FOR UPDATE
		];
		if (!jobStatusList.isEmpty()) {
			return jobStatusList[0];
		}
		//else...
		throw new JobStatusException('Job Status not found: ' + jobName);
	}
	public static Job_Status__c getJobStatus(String jobName) {
		List<Job_Status__c> jobStatusList = [
			SELECT Status__c, Request_Date__c
			FROM Job_Status__c
			WHERE Name = :jobName
			LIMIT 1
		];
		return jobStatusList.isEmpty() ? null : jobStatusList.get(0);
	}

	public static void deactivateJob(Job_Status__c jobStatus) {
		jobStatus.Status__c = INACTIVE_STATUS;
		jobStatus.Request_Date__c = System.now();
		update jobStatus;
	}

	//******************************************************************** */

	public class JobStatusException extends Exception {
	}
}