/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-28-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCASSearchProfUpdFromSF implements
    Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful, Database.RaisesPlatformEvents, BatchReflection{
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static Integer BATCH_SIZE = 100;
    public static String CRON_SCHEDULE = '0 0 0 * * ?';
    public static String GLO_MARKETS = 'OANDA Global Markets';

    public List<Comply_Advantage_Search__c> searches;

    public BatchCASSearchProfUpdFromSF() {
        this(null);
    }

    public BatchCASSearchProfUpdFromSF(String filter) {
        query = 'SELECT Id, Name, Search_Id__c, Is_Secondary_Search__c, ' +
        '   Search_Text__c, Search_Reason__c, Case__c, Affiliate__c, ' +
        '   Custom_Division_Name__c, Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__c, fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Search_Profile__c = NULL AND Is_Monitored__c = TRUE ' +
        (String.isNotBlank(filter) ? filter : '');

        System.debug(query);
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Name, Search_Id__c, Is_Secondary_Search__c, ' +
        '   Search_Text__c, Search_Reason__c, Case__c, Affiliate__c, ' +
        '   Custom_Division_Name__c, Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__c, fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public static void executeBatch() {
        Database.executeBatch(new BatchCASSearchProfUpdFromSF(), BATCH_SIZE);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchCASSearchProfUpdFromSF(), BATCH_SIZE);
    }

    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        searches = new List<Comply_Advantage_Search__c>();
        Map<Id, Document__c> docsBySearch = new Map<Id, Document__c>();
        Map<Id, HttpResponse> respsBySearch = new Map<Id, HttpResponse>();

        for (Comply_Advantage_Search__c search : scope) {
            String divisionName = ComplyAdvantageUtil.getDivisionName(search);
            Comply_Advantage_Division_Setting__c divisionSet = ComplyAdvantageUtil.getDivisionSettingByName(
                divisionName,
                search.Search_Parameter_Mailing_Country__c
            );

            if (divisionSet != null) {
                CASearchWrap wrap = updateProcess(search, divisionSet);

                if (wrap.search != null) {
                    searches.add(wrap.search);
                }
                if (wrap.document != null) {
                    docsBySearch.put(search.Id, wrap.document);
                    respsBySearch.put(search.Id, wrap.reportResponse);
                }
            }
        }

        if (!searches.isEmpty()) {
            Database.update(searches, false);
        }

        if (docsBySearch.size() > 0) {
            Database.insert(docsBySearch.values(), false);
        }

        Map<Id, Comply_Advantage_Search__c> searchesMap = new Map<Id, Comply_Advantage_Search__c>(searches);
        List<Attachment> attachments = new List<Attachment>();

        for (Id sId : docsBySearch.keySet()) {
            Document__c doc = docsBySearch.get(sId);
            if (doc.Id != null) {
                Attachment att = ComplyAdvantageUtil.getAttachment(respsBySearch.get(sId), searchesMap.get(sId).Name, doc.Id);
                attachments.add(att);
            }
        }

        if (!attachments.isEmpty()) {
            Database.insert(attachments, false);
        }
    }

    public CASearchWrap updateProcess(Comply_Advantage_Search__c search, Comply_Advantage_Division_Setting__c divisionSet) {
        String searchProfile = divisionSet?.Search_Profile__c;
        String apiKey = divisionSet?.Comply_Advantage_Instance_Setting__r?.API_Key__c;
        HttpResponse response = ComplyAdvantageUtil.updateSearchProfileCallout(search.Search_Id__c, searchProfile, apiKey);

        CASearchWrap wrap = new CASearchWrap();

        if (response.getStatusCode() == 200) {
            search.Search_Profile__c = searchProfile;
            search.Search_Profile_Updated__c = true;
            
            HttpResponse responseDetails = ComplyAdvantageUtil.getSearchDetailsCallout(search.Search_Id__c, apiKey);

            if (responseDetails.getStatusCode() == 200) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(responseDetails.getBody());    
                Map<String, Object> resWrapper = (Map<String, Object>) results.get('content');
                Map<String, Object> responseData = (Map<String, Object>) resWrapper.get('data');   
                Integer totalMatches = Integer.valueOf(responseData.get('total_matches'));
                search.Total_Hits__c = totalMatches;

                if (totalMatches >= 100) {
                    String secondSearchProfile = divisionSet?.Secondary_Search_Profile__c;
                    response = ComplyAdvantageUtil.updateSearchProfileCallout(search.Search_Id__c, secondSearchProfile, apiKey);

                    if (response.getStatusCode() == 200) {
                        responseDetails = ComplyAdvantageUtil.getSearchDetailsCallout(search.Search_Id__c, apiKey);
                        if (responseDetails.getStatusCode() == 200) {
                            results = (Map<String, Object>) JSON.deserializeUntyped(responseDetails.getBody());
                            resWrapper = (Map<String, Object>) results.get('content');
                            responseData = (Map<String, Object>) resWrapper.get('data');
                            Integer totalHits = Integer.valueOf(responseData.get('total_hits'));
                            search.Total_Hits__c = totalHits;
                        }
                        
                        search.Search_Profile__c = secondSearchProfile;
                        search.Search_Profile_Updated__c = true;
                        search.Is_Secondary_Search__c = true;
                    }
                }
            }
            wrap.search = search;

            HttpResponse nameSearchReportResponse = ComplyAdvantageUtil.downloadReportCallout(search.Search_Id__c, apiKey);
            if (nameSearchReportResponse?.getStatusCode() == 200) {
                wrap.document = ComplyAdvantageUtil.getDocument(search);
                wrap.reportResponse = nameSearchReportResponse;
            }
        }

        return wrap;
    }
    public void finish(Database.BatchableContext bc) {
        System.debug('FINISH METHOD');
        System.debug(searches);
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (searches != null) {
            for (Comply_Advantage_Search__c search : searches) {
                search.Entities_Download_Status__c = 'Download';
            }

            if (!searches.isEmpty()) {
                Database.update(searches, false);
            }
        }
    }

    public static String schedule(String schedule) {
        if (String.isNotBlank(schedule)) {
            CRON_SCHEDULE = schedule;
        }
        return System.schedule((
            Test.isRunningTest() ? 'Testing Batch BatchCASSearchProfUpdFromSF' : 'Update CA Search: Search Profile'),
            CRON_SCHEDULE,
            new BatchCASSearchProfUpdFromSF()
        );
    }

    public class CASearchWrap {
        public Comply_Advantage_Search__c search { get; set; }
        public Document__c document { get; set; }
        public HttpResponse reportResponse { get; set; }
    }
}