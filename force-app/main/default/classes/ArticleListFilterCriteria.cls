/**
 * @author Ariel Cantero
 * @since 6/14/2019
 */
public class ArticleListFilterCriteria {

	@AuraEnabled
	public String searchText {get;set;}
	@AuraEnabled
	public String status {get;set;}
	@AuraEnabled
	public String category {get;set;}
	@AuraEnabled
	public String audience {get;set;}
	@AuraEnabled
	public String division {get;set;}
	@AuraEnabled
	public Boolean hasPreviewDoc {get;set;}
	@AuraEnabled
	public String excludeArtIds {get;set;}
	@AuraEnabled
	public Boolean restrictToVisibleInPkb {get;set;}

}