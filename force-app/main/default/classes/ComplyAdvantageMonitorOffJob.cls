/**
 * SP-7630.
 * Syncs all Comply Advantage Searches whose Is_Monitored flag
 * is still turned on adn their respective fxAccount has been closed
 * or locked for some time now. This will happen if the synchronization
 * fails in real time.
 * @author Fernando Gomez
 */
public without sharing class ComplyAdvantageMonitorOffJob
		implements System.Schedulable, Database.AllowsCallouts {
	private List<String> unsyncedSearchIds;

	/**
	 * Main constructor.
	 */
	public ComplyAdvantageMonitorOffJob() {
		// ...
	}

	/**
	 * Implementation from System.Queueable
	 * @param context
	 */
	public void execute(System.SchedulableContext context) {
		// we process them all one by one
		Database.executeBatch(new ComplyAdvantageMonitorOffBatch(), 1);
	}

	/**
	 * Schedules the job to run once a day at midnight.
	 * @param the ID of the newly scheduled Job
	 */
	public static Id scheduleJob() {
		return System.schedule(
			'Comply Advantage Monitoring Sync Job (Daily at 11pm)',
			'0 0 23 * * ? *' ,
			new ComplyAdvantageMonitorOffJob());
	}
}