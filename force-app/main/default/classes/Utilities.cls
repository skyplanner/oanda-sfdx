/**
 * Using without sharing as need to run query on Organization table
 */
public without sharing class Utilities {

	//Get instance from INSTANCE.visual.force.com page so we can build
	public Static String getInstance(){
		String instance = '';
		Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
		String orgType = o.OrganizationType;
		String insName = o.InstanceName;
		if(orgType == 'Developer Edition' || insName.startsWithIgnoreCase('cs')){
			List<String> parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
			instance = parts[parts.size() - 4] + '.';
		}
		return instance;
	}
	//Needed in cases if current org is sandbox
	public static String getSubdomainPrefix(){
		Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
		String orgType = o.OrganizationType;
		String insName = o.InstanceName;
		if(insName.startsWithIgnoreCase('cs')){
			return UserInfo.getUserName().substringAfterLast('.')+ '-';
		}
		return '';
	}

	public static Map<Object,List<String>> getDependentPicklistValues
	(Schema.sObjectField dependToken)
	{
	  Schema.DescribeFieldResult depend = dependToken.getDescribe();
	  Schema.sObjectField controlToken = depend.getController();
	
	  if ( controlToken == null ) return null;
	 
	  Schema.DescribeFieldResult control = controlToken.getDescribe();
	  List<Schema.PicklistEntry> controlEntries =
	   (   control.getType() == Schema.DisplayType.Boolean
		 ?   null
		 :   control.getPicklistValues()
	   );
 
	 String base64map = 
	 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
	 Map<Object,List<String>> dependentPicklistValues = 
												 new Map<Object,List<String>>();
	 for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) 
	  if ( entry.isActive() )
	  {
		//base64chars have reference to controllers of entry 
		List<String> base64chars =
			String.valueOf
			(((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(entry)))
			  .get( 'validFor' ))
			  .split( '' );
		//if there is any entry that dont have any controller value will 
		//scape here
		if(base64chars.size() == 1)
		  continue;

		for ( Integer index = 0; index < (controlEntries != null ? 
			controlEntries.size() : 2); index++ )
		{
			Object controlValue = (controlEntries == null?(Object)(index == 1)
			:(Object) (controlEntries[ index ].isActive() ? 
				controlEntries[ index ].getLabel() : null)
			);
			Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
			//String character = base64chars[ bitIndex ];
			if  ( controlValue == null              
				||(base64map.indexOf(base64chars[ bitIndex ]) & 
					(1 << bitShift)) == 0
				) continue;
			if ( !dependentPicklistValues.containsKey( controlValue ) )
			{
				dependentPicklistValues.put( controlValue, new List<String>() );
			}
			dependentPicklistValues.get( controlValue ).add( entry.getLabel() );
		}
	 }
	  return dependentPicklistValues; 
	}

	/**
	 * Get SHA-256
	 */
	public static String getSHA256(String strInput) {
		return
			String.isBlank(strInput) ? '' : 
				EncodingUtil.convertToHex(
					Crypto.generateDigest(
						'SHA-256',
						Blob.valueOf(strInput)));
	}

	/**
	 * @param setA
	 * @param setB
	 * @return true is there is at least an element on setA
	 * that is present in setB
	 */
	public static Boolean doSetsIntersect(Set<String> setA, Set<String> setB) {
		for (String element : setA)
			if (setB.contains(element))
				return true;

		return false;
	}

	/**
	 * Serialize an object
	 * Note: 'JSON General Exception' is because sometimes the
	 * object you try to serialize is too big and JSON throw an exception.
	 */
	public static String serialize(
		Object obj,
		Boolean pretty) {
        try {
            return pretty ?
				JSON.serializePretty(obj) :
				JSON.serialize(obj);
        } catch(Exception ex) {
            return 'JSON Exception.';
        }
    }

	/**
	 * Serialize an sObject
	 * Note: 'JSON General Exception' is because sometimes the
	 * sObject you try to serialize is too big and JSON throw an exception.
     * 
     * SkyPlanner update 2/27/2024
     * Added safe navigation operator to the method call
	 */
	public static String serialize(
		sObject sObj,
		Boolean pretty) {
			Object obj = sObj?.getPopulatedFieldsAsMap();
            return serialize(obj, pretty);
    }

	/**
	 * @description Get Sobject Type as String (e.g. Account, Case, FxAccount__c, etc) from a Record Id
	 * @param recordId
	 */
	public static String getSObjectTypeFromId(String recordId) {
		Map<String,String> keys = new Map<String,String>();
		Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
		for(String s:describe.keyset()){
			keys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
		}

		return keys.get(recordId.substring(0,3));


	}

	public static Set<Id> findDiff(Set<Id> biggerSet, Set<Id> smallerSet) {
		Set<Id> setDiff = new Set<Id>();
		for (Id obj : biggerSet) {
			if (!smallerSet.contains(obj)) {
				setDiff.add(obj);
			}
		}
		return setDiff;
	}
}