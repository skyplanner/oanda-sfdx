/**
 * @File Name          : CheckMsgAgentAvailabilityActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 12:03:20 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 12:02:54 PM   aniubo     Initial Version
 **/
@isTest
private class CheckMsgAgentAvailabilityActionTest {
	@isTest
	private static void testLanguageCodesIsNullOrEmpty() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<Boolean> result = CheckMsgAgentAvailabilityAction.checkMsgAgentAvailability(
			null
		);
		Assert.areEqual(
			false,
			result.get(0),
			'Language is not available because language code is null'
		);
		result = CheckMsgAgentAvailabilityAction.checkMsgAgentAvailability(
			new List<String>()
		);
		Assert.areEqual(
			false,
			result.get(0),
			'Language is not available because language code is empty'
		);
		Test.stopTest();

		// Asserts
	}
	@isTest
	private static void testCheckMsgAgentAvailabilityThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			List<Boolean> result = CheckMsgAgentAvailabilityAction.checkMsgAgentAvailability(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@IsTest
	static void testCheckMsgAgentAvailabilityAction() {
		List<String> languageCodes = new List<String>{ 'de' };
		Test.startTest();
		List<Boolean> result = CheckMsgAgentAvailabilityAction.checkMsgAgentAvailability(
			languageCodes
		);
		Test.stopTest();

		Assert.areEqual(false, result.get(0), 'Agent is not available');
	}
}