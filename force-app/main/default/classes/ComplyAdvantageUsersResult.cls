/**
 * Wrapper for Comply advantage JSON: Users
 * @author Fernando Gomez
 */
public class ComplyAdvantageUsersResult {
	public Map<String, List<ComplyAdvantageUser>> content;

	/**
	 * @param jsonString representation
	 * @return an instance of this class
	 */
	public static ComplyAdvantageUsersResult parse(String jsonString) {
		return (ComplyAdvantageUsersResult)JSON.deserialize(
			jsonString, ComplyAdvantageUsersResult.class);
	}
}