/**
 * @File Name          : SendMsgAuthenticationCodeAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 6:29:18 AM
**/
public with sharing class SendMsgAuthenticationCodeAction {

	@InvocableMethod(label='Send Messaging Authentication Code' callout=false)
	public static List<Boolean> sendMsgAuthenticationCode(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<Boolean> result = new List<Boolean>{ false };
		
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(infoList[0].messagingSessionId);
			result[0] = process.sendAuthenticationCode(
				infoList[0].fxAccountId
			);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				SendMsgAuthenticationCodeAction.class.getName(),
				ex
			);
		}   
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public ID messagingSessionId;

		@InvocableVariable(label = 'fxAccountId' required = true)
		public ID fxAccountId;
		
	}

}