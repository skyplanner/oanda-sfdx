public with sharing class ComplyAdvantageUpdateSearchReference implements Callable 
{
    public ComplyAdvantageUpdateSearchReference() 
	{
		
	}
   
	public void updateSearchReference(Comply_Advantage_Search__c[] searches)
    {
        Comply_Advantage_Search__c[] searchesToUpdate = new Comply_Advantage_Search__c[]{};
        for(Comply_Advantage_Search__c search : searches)
        {
            try 
            {
                String divisionName = String.isNotBlank(search.Custom_Division_Name__c) ? search.Custom_Division_Name__c : (String.isNotBlank(search.Division_Name__c) ? search.Division_Name__c : '');
                string apiKey = ComplyAdvantageUtil.getAPiKey(divisionName, search.Search_Parameter_Mailing_Country__c);

                if(apiKey != null)
                {
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setEndpoint('https://api.complyadvantage.com/searches/'+ search.Search_Id__c +'/details?api_key='+ apiKey);
                    request.setMethod('GET');
                    request.setTimeout(120000);
                    HttpResponse response = http.send(request);
                    
                    if (response.getStatusCode() == 200) 
                    {
                        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
                        Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                        Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');   
                        string search_ref = (string)responseData.get('ref');

                        search.Search_Reference__c = search_ref;
                        searchesToUpdate.add(search);
                    }
                }
            } 
            catch (Exception ex) 
            {
                System.debug('Exception : ' + ex.getMessage());
            }
        }
        if(searchesToUpdate.size() > 0)
        {
            Database.update(searchesToUpdate,false);
        }
	} 
    public Object call(String action, Map<String, Object> args) 
    {
        switch on action 
        {
            when 'updateSearchReference' 
            {
                Comply_Advantage_Search__c[] caRecords = (Comply_Advantage_Search__c[]) args.get('records');
                updateSearchReference(caRecords);
            }
        }
        return null;
    }
    
}