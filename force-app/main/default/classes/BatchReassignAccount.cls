/**
* Reassign account owner back to the sales owner from retention owner.
  If the original owner is system user, don't assign back.
*/
public with sharing class BatchReassignAccount implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{
	
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchReassignAccount(){
		query = 'SELECT id, accountid, ownerId  from Opportunity where recordtypeId = \'' + RecordTypeUtil.getRetentionOpportunityTypeId() +'\' and last_trade_date__c < LAST_N_DAYS:60 ';
	}
	
	public BatchReassignAccount(String q){
		query = q;
	}

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT id, accountid, ownerId FROM Opportunity WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
 
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		
   		List<Opportunity> retenOpps = (List<Opportunity>) batch;
   		
   		Set<ID> accountIds = new Set<ID>();
   		Map<Id, Opportunity> retenOppByAccountId = new Map<Id, Opportunity>();
   		
   		for(Opportunity opp : retenOpps){
   			accountIds.add(opp.accountId);
   			retenOppByAccountId.put(opp.accountId, opp);
   		}
   		
   		List<Opportunity> saleOpps = [select id, recordTypeId, ownerId, accountId from Opportunity where RecordTypeId != :RecordTypeUtil.getRetentionOpportunityTypeId() and accountId in :accountIds];
   		
   		Map<Id, Opportunity> saleOppByAccountId = new Map<Id, Opportunity>();
   		for(Opportunity opp : saleOpps){
   			saleOppByAccountId.put(opp.accountId, opp);
   		}
   		
   		List<Account> accountList = [select id, ownerId, fxAccount__c, PersonMailingCountry from Account where id in :accountIds];
   		List<Account> accountsToUpdate = new List<Account>();
   		List<fxAccount__c> updatefxList = new List<fxAccount__c>();
   		List<Opportunity> retenOppsToUpdate = new List<Opportunity>();
   		
   		for(Account acct : accountList){
   			
   			Opportunity saleOpp = saleOppByAccountId.get(acct.Id);
			
   			if(saleOpp != null && UserUtil.isActive(saleOpp.ownerId) && (UserUtil.getSystemUserId() != saleOpp.ownerId || acct.PersonMailingCountry == 'Japan')){
   				acct.ownerId = saleOpp.ownerId;
   				accountsToUpdate.add(acct);
				if(acct.fxAccount__c != null){
					List<CXNoteListController.CXNote> notes = CXNoteListController.getCXNotes(acct.fxAccount__c);
					CXNoteListController.CXNote note = new CXNoteListController.CXNote();
					note.timestamp = (Datetime.now().getTime());
					note.comment = System.Label.Batch_Reassign_Account;
					note.author = UserInfo.getName();
					notes.add(note);
					fxAccount__c fxa = new fxAccount__c(
						Id = acct.fxAccount__c,
						Notes__c = JSON.serialize(notes)
					);
					updatefxList.add(fxa);
				}
   				
   				Opportunity retenOpp = retenOppByAccountId.get(acct.Id);
   				if(retenOpp != null){
   					retenOpp.ownerId = saleOpp.ownerId;
   					retenOppsToUpdate.add(retenOpp);
   				}
   				
   			}
   			
   		}
   		
   		//update the account owner
   		if(accountsToUpdate.size() > 0){
   			update accountsToUpdate;
   		}

		//updating the notes
		if(updatefxList.size() > 0){
			update updatefxList;
		}
   		
   		//update retention opportunity
   		if(retenOppsToUpdate.size() > 0){
   			update retenOppsToUpdate;
   		}
   		
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

}