/**
 * @File Name          : OnNonHvcCaseAssignedCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2024, 3:53:07 PM
**/
@IsTest
private without sharing class OnNonHvcCaseAssignedCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		initManager.storeData();
	}

	/**
	 * test 1 : nonHvcCaseIdSet is null
	 * test 2 : nonHvcCaseIdSet is empty
	 * test 3 : nonHvcCaseIdSet is not empty
	 */
	@IsTest
	static void processNonHvcCases() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		ID nonHVCCase1Id = initManager.getObjectId(
			ServiceTestDataKeys.NON_HVC_CASE_1, 
			true
		);
		Set<ID> nonHvcCaseIdSet1 = null;
		Set<ID> nonHvcCaseIdSet2 = new Set<ID>();
		Set<ID> nonHvcCaseIdSet3 = new Set<ID> { nonHVCCase1Id };

		Test.startTest();
		// test 1 => return false
		Boolean result1 = 
			OnNonHvcCaseAssignedCmd.processNonHvcCases(nonHvcCaseIdSet1);
		// test 2 => return false
		Boolean result2 = 
			OnNonHvcCaseAssignedCmd.processNonHvcCases(nonHvcCaseIdSet2);
		// test 3 => return false
		Boolean result3 = 
			OnNonHvcCaseAssignedCmd.processNonHvcCases(nonHvcCaseIdSet3);
		Test.stopTest();
		
		Case caseObj = [
			SELECT Routed_And_Assigned__c 
			FROM Case
			WHERE Id = :case1Id
		];
		Assert.isFalse(
			result1, 
			'If there are no records to process, false is returned'
		);
		Assert.isFalse(
			result2, 
			'If there are no records to process, false is returned'
		);
		Assert.isTrue(
			result3, 
			'If there are records to process, true is returned'
		);
		Assert.isTrue(
			caseObj.Routed_And_Assigned__c, 
			'The Case.Routed_And_Assigned__c field must be set to true.'
		);
	}
	
}