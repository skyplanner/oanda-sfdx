/* Name: PiuCkaUpdateRestResource
 * Description : New Apex Class to handle Personal Information Update from user-api
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 21
 */

 @RestResource(urlMapping='/api/v1/client-identification-code')
 global class FxAccountAuthenticationCodeRestResource {
  
    public static final String LOGGER_CATEGORY = '/api/v1/client-identification-code';

    private static final String JWT_TOKEN_PATTERN = '^Bearer ([A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_.+\\/=]+)$';
    private static final String SSO_TOKEN_PATTERN = '^Bearer ([A-Za-z0-9\\-]+)$';

    private enum TOKEN_TYPE {SSO,JWT}

    @testVisible static Auth0_JWT_Setting__mdt auth0Settings { 
        get {
            if ( auth0Settings == null ){
                auth0Settings = Auth0_JWT_Setting__mdt.getInstance('settings'); 
            }
            return auth0Settings;
        } set; }

    @HttpGet
    global static void doGet() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response);

        try {
            String token = req.headers.get('X-Authorization');

            if(token == null) {
                context.setResponse(401, 'Unable to parse X-Authorization token'); 
                return;
            }

            fxAccount__c fxa = getFxAccountFromTokenInfo(token);

            if (fxa == null) {
                context.setResponse(
                    404,
                    'Live User for given token not found'
                );
                return;
            }
            
            AuthenticationCodeGenerator generator = new AuthenticationCodeGenerator(
                fxa,
                'Authentication_Code__c',
                'Authentication_Code_Expires_On__c'
            );
            
            context.setResponse(200, generator.getCode(true)); 

        } catch (Exception ex) {
            Logger.exception(LOGGER_CATEGORY, ex);
            context.setResponse(500, ex.getMessage() + ' /' + ex.getStackTraceString()); 
        }
    }

    private static fxAccount__c getFxAccountFromTokenInfo (String token){

        if(isJwt(token)) {
            String jwtToken = captureTokenByPattern(token, JWT_TOKEN_PATTERN);
            String fxTradeOneId = returnOneIdFromSubIfJwtSignatureValidated(jwtToken);
            return FxAccountSelector.getFxAccountByOneId(fxTradeOneId);
        }

        if(isSso(token)) {
            throw new IllegalArgumentException('SSO Token not supported');
        }

        throw new IllegalArgumentException('Could not recognize supplied token from X-Authorization header');
    }

    private static Boolean isJwt(String token){
        return !String.isBlank(captureTokenByPattern(token, JWT_TOKEN_PATTERN));
    }

    private static Boolean isSso(String token){
        return !String.isBlank(captureTokenByPattern(token, SSO_TOKEN_PATTERN));
    }

    private static String captureTokenByPattern(String token, String tokenPattern) {
        if (String.isBlank(token)) {
            return null;
        }

        Pattern pattern = Pattern.compile(tokenPattern);
        Matcher matcher = pattern.matcher(token);
        if (matcher.matches()) {
            return matcher.group(1);
        }

        return null;
    }

    private static String returnOneIdFromSubIfJwtSignatureValidated(String jwtString) {
        
        if (jwtString == null) {
			throw new IllegalArgumentException('No token was provided');
		}

        Auth.JWT verifiedJwt = Auth.JWTUtil.validateJWTWithKeysEndpoint(jwtString, auth0Settings.JWKS_Uri__c, false);

        if(verifiedJwt.getIss() != auth0Settings.Required_Issuer__c) {
            throw new IllegalArgumentException('Invalid Issuer');
        }
        	
		return verifiedJwt.getSub().substringAfter('|');
    }
}