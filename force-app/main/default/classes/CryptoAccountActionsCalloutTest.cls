/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
class CryptoAccountActionsCalloutTest {
    public static final string sampleApiReponse = '{'+
        '"adminDisabled": false,'+
        '"userDisabled": false,'+
        '"idVerificationStatus": "DISABLED",'+
        '"sanctionsVerificationStatus": "DISABLED",'+
        '"paxosSummaryStatus": "DISABLED",'+
        '"paxosUserID": "1111111",'+
        '"mt5ClientID": "1111111",'+
        '"displayName": "1111111CPX",'+
        '"isDisabled": false,'+
        '"mt5UserID": "1111111",'+
        '"mt5UserStatus": "ENABLED",'+
        '"paxosAccountID": "88888888-4444-ffff-bbbb-666666666666",'+
        '"paxosAccountStatus": "DISABLED",'+
        '"paxosProfileID": "00000000-7777-dddd-cccc-33333333333",'+
        '"accountAdminDisabled": false,'+
        '"accountUserDisabled": false'+
    '}';

    @testSetup
	static void init() {
		Account acc = TestDataFactory.getPersonAccount(true);
		fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccount(
            acc, true);
	}

    @isTest static void cryptoAccountCloseControllerSuccessTest() {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoAccountCloseController.closeAccount(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoAccountCloseControllerInvalidResponseTest() {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(500, 'Error 1', 'Internal Server Error', null));
        CryptoAccountCloseController.closeAccount(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoLockTradingControllerSuccessTest() {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoLockTradingController.lockTrading(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoLockTradingControllerInvalidResponseTest() {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(500, 'Error 1', 'Internal Server Error', null));
        CryptoLockTradingController.lockTrading(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoAccountEnableTradingControllerSuccessTest() {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoAccountEnableTradingController.enableTrading(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoAccountEnableTradingControllerInvalidResponseTest() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, false);
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c WHERE Id = :fxAcc.Id]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(500, 'Error 1', 'Internal Server Error', null));

        CryptoAccountEnableTradingController.enableTrading(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoCloseAllAccountControllerSuccessTest() {
        List<Id> fxAccountIdList = new List<Id>((new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c])).keySet());
        List<Id> accountIdList = new List<Id>((new Map<Id, Account>(
            [SELECT Id FROM Account])).keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoCloseAllAccountController.closeAllAccount(accountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoCloseAllAccountControllerInvalidResponseTest() {
        List<Id> fxAccountIdList = new List<Id>((new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c])).keySet());
        List<Id> accountIdList = new List<Id>((new Map<Id, Account>(
            [SELECT Id FROM Account])).keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(500, 'Error 1', 'Internal Server Error', null));

        CryptoCloseAllAccountController.closeAllAccount(accountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoAccountWrongParamsTest() {
        CryptoAccountParams params = new CryptoAccountParams();
        params.fxaccountIds = new List<Id>();
        params.action = 0;
        params.addFxAccountDataResult = false;
        params.useTasApi = true;
        params.useUserApi = false;

        CryptoAccountActionsCallout accountActionsCallout = new CryptoAccountActionsCallout(
            params
        );

        Test.startTest();
        
        String result = accountActionsCallout.execute();

        Test.stopTest();
        
        System.assertEquals('Wrong Data for process', result, 'Wrong result for wrong params.');
    }

    @isTest static void cryptoAccountWithOneIdTest() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, false);
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c WHERE Id = :fxAcc.Id]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoAccountEnableTradingController.enableTrading(fxAccountIdList);
        
        Test.stopTest();

        Note[] notes = [SELECT Title 
            FROM Note
            WHERE ParentId =: fxAccountIdList[0]];

        System.assertEquals(false, notes.isEmpty(),
            'A note should be created.');
    }

    @isTest static void cryptoAccountUpdatePaxosTest() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>(
            [SELECT Id FROM fxAccount__c WHERE Id = :fxAcc.Id]);
        List<Id> fxAccountIdList = new List<Id>(fxAccountMap.keySet());
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, 
            new CalloutMock(200, sampleApiReponse, 'success', null));

        CryptoAccountEnableTradingController.enableTrading(fxAccountIdList);
        
        Test.stopTest();

        Paxos__c p = [
            SELECT
                Id,
                Admin_Disabled__c,
                User_Disabled__c
            FROM Paxos__c
            WHERE Id =: fxAcc.Paxos_Identity__c
            LIMIT 1
        ];

        System.assertEquals(false, p.Admin_Disabled__c, 'Paxos Identity Admin Disabled updated correctly');
        System.assertEquals(false, p.User_Disabled__c, 'Paxos Identity User Disabled updated correctly');
    }
}