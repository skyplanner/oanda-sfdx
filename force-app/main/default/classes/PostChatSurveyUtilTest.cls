/**
 * @File Name          : PostChatSurveyUtilTest.cls
 * @Description        : 
 * @Author             : --------
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/12/2019, 3:29:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    ---------   --------     Initial Version
 * 2.0    9/11/2019   acantero     Update
**/
@isTest
private class PostChatSurveyUtilTest {
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

	static testMethod void testSetLiveChatTranscript() {
		System.runAs(SYSTEM_USER) {
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id);
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now(), ChatKey='testkey');
			insert t;
			
			Post_Chat_Survey__c s = new Post_Chat_Survey__c(Chat_Key__c='testkey');
			insert s;
			
			s = [SELECT Live_Chat_Transcript__c FROM Post_Chat_Survey__c WHERE Id=:s.Id];
			
			System.assertEquals(t.Id, s.Live_Chat_Transcript__c);
		}
	}
	
	static testMethod void testPostChatSurveyPage() {
		Lead l = null;
		Case c = null;
		System.runAs(SYSTEM_USER) {
			l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			c = new Case(Subject='test subject', Lead__c=l.Id);
			insert c;
			
		}
		prepareSurvey();
		
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		
		String chatDetails = '{"visitorId":"1","customDetails":[{"label":"LanguagePreference","value":"en_US","entityMaps":[],"displayToAgent":true,"transcriptFields":[]}]}';
		String attachedRecords = '{"CaseId":"'+ c.Id +'","LeadId":"' + l.Id +'"}';

		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		pageParams.put('chatKey', '1');
		pageParams.put('windowLanguage', 'en_US');
		pageParams.put('chatDetails', chatDetails);
		pageParams.put('attachedRecords', attachedRecords);
		Test.startTest();
		PostChatSurveyCon theController = new PostChatSurveyCon();
		Test.stopTest();
		System.assertEquals(false, theController.isError);
		System.assertEquals('en_US', theController.language);
	}

	static testMethod void test2PostChatSurveyPage() {
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		PostChatSurveyCon theController = new PostChatSurveyCon();
		System.assertEquals(true, theController.isError);
	}

	static testMethod void test3PostChatSurveyPage() {
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		pageParams.put('chatKey', '1');
		pageParams.put('attachedRecords', 'attachedRecords');
		PostChatSurveyCon theController = new PostChatSurveyCon();
		System.assertEquals(true, theController.isError);
	}

	//without survey
	static testMethod void test4PostChatSurveyPage() {
		Lead l = null;
		Case c = null;
		System.runAs(SYSTEM_USER) {
			l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			c = new Case(Subject='test subject', Lead__c=l.Id);
			insert c;
		}
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		
		String chatDetails = '{"visitorId":"1","customDetails":[{"label":"LanguagePreference","value":"en_US"}]}';
		String attachedRecords = '{"CaseId":"'+ c.Id +'","LeadId":"' + l.Id +'"}';

		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		pageParams.put('chatKey', '1');
		pageParams.put('windowLanguage', 'en_US');
		pageParams.put('chatDetails', chatDetails);
		pageParams.put('attachedRecords', attachedRecords);
		Test.startTest();
		//without
		PostChatSurveyCon theController = new PostChatSurveyCon();
		Test.stopTest();
		//fails because of no survey found
		System.assertEquals(true, theController.isError); 
	}

	//fails because wrong CaseId
	static testMethod void test5PostChatSurveyPage() {
		Lead l = null;
		Case c = null;
		System.runAs(SYSTEM_USER) {
			l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			c = new Case(Subject='test subject', Lead__c=l.Id);
			insert c;
		}
		prepareSurvey();
		
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		
		String chatDetails = '{"visitorId":"1","customDetails":[{"label":"LanguagePreference","value":"en_US"}]}';
		//pass wrong CaseId
		String attachedRecords = '{"CaseId":"'+ l.Id +'","LeadId":"' + l.Id +'"}';

		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		pageParams.put('chatKey', '1');
		pageParams.put('windowLanguage', 'en_US');
		pageParams.put('chatDetails', chatDetails);
		pageParams.put('attachedRecords', attachedRecords);
		Test.startTest();
		PostChatSurveyCon theController = new PostChatSurveyCon();
		Test.stopTest();
		//fails because wrong CaseId
		System.assertEquals(true, theController.isError);
	}

	static testMethod void testTryRedirecting() {
		Lead l = null;
		Case c = null;
		System.runAs(SYSTEM_USER) {
			l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			c = new Case(Subject='test subject', Lead__c=l.Id);
			insert c;
		}
		prepareSurvey();
		
		PageReference pageRef = Page.PostChatSurvey;
		Test.setCurrentPage(pageRef);
		
		String chatDetails = '{"visitorId":"1","customDetails":[{"label":"LanguagePreference","value":"en_US"}]}';
		String attachedRecords = '{"CaseId":"'+ c.Id +'","LeadId":"' + l.Id +'"}';

		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		pageParams.put('chatKey', '1');
		pageParams.put('windowLanguage', 'en_US');
		pageParams.put('chatDetails', chatDetails);
		pageParams.put('attachedRecords', attachedRecords);
		PostChatSurveyCon theController = new PostChatSurveyCon();
		Test.startTest();
		PageReference result = theController.tryRedirecting();
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	private static void prepareSurvey() {
		SurveySettings__c surveySettings = SurveySettings__c.getOrgDefaults();
		String surveyName = surveySettings.CurrentSurveyName__c;
		if (String.isBlank(surveyName)) {
			surveyName = 'CX Survey';
			surveySettings.CurrentSurveyName__c = surveyName;
			if (String.isBlank(surveySettings.Id)) {
				insert surveySettings;
			} else {
				update surveySettings;
			}
		}
		Survey__c surveyObj = new Survey__c(
			Name = surveyName
		);
		insert surveyObj;
	}
}