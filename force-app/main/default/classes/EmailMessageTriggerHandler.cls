/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class EmailMessageTriggerHandler {
	public static Boolean enabled = true;

	public static Boolean isEnabled() {
		User runningUser = [SELECT Triggers_Disabled__c FROM User WHERE Id = :UserInfo.getUserId()];
		if (runningUser.Triggers_Disabled__c == true) return false; 
		return enabled != null ? enabled : true;
	}
	public void handleBeforeDelete(List<EmailMessage> emailMessageList) {
		SpEmailUtil.preventDeleteDraftEmailMessage(emailMessageList);
		EmailUtil.preventDeleteEmailMessage(
			SpEmailUtil.getNoDraftEmail(emailMessageList)
		);
	}
	public void handleBeforeInsert(List<EmailMessage> emailMessageList) {
		EmailUtil.checkOutgoingEmailSize(emailMessageList);
	}
	public void handleAfterDelete(List<EmailMessage> emailMessageList) {
		SpEmailUtil.deleteScheduledEmail(emailMessageList);
	}
	public void handleAfterInsert(List<EmailMessage> emailMessageList) {
		List<EmailMessage> emailsToUpdateCase = new List<EmailMessage>();
		
		if(CustomSettings.isCustomNotificationForSalesEnabled()) {
            CustomNotificationManager.sendNotificationsForOGMCases(emailMessageList, null);
        }

		for(EmailMessage msg : emailMessageList) {
			verify(msg, emailsToUpdateCase);
		}

		EmailUtil.updateRelatedCases(emailsToUpdateCase);
		
	}
	public void handleAfterUpdate(
		Map<Id, EmailMessage> newMap,
		Map<Id, EmailMessage> oldMap
	) {
		SpEmailUtil.draftEmailSent(newMap, oldMap);
	}
	public void run() {
		if (!isEnabled()) {
			return;
		}
		if ((Trigger.isExecuting && Trigger.isBefore && Trigger.isDelete)) {
			handleBeforeDelete(Trigger.old);
		} else if (Trigger.isExecuting && Trigger.isBefore && Trigger.isInsert) {
			handleBeforeInsert(Trigger.new);
		} else if (Trigger.isExecuting && Trigger.isAfter && Trigger.isDelete) {
			handleAfterDelete(Trigger.old);
		} else if (Trigger.isExecuting && Trigger.isAfter && Trigger.isInsert) {
			handleAfterInsert(Trigger.new);
		} else if (Trigger.isExecuting && Trigger.isAfter && Trigger.isUpdate) {
			handleAfterUpdate(
				(Map<Id, EmailMessage>) Trigger.newMap,
				(Map<Id, EmailMessage>) Trigger.oldMap
			);
		}
	}

	private void verify(EmailMessage msg, List<EmailMessage> emailsToUpdateCase)
	{
		if(msg.Incoming){
			emailsToUpdateCase.add(msg);
		}
	}
}