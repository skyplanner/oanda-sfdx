/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-21-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserPatchRequest {    
    public final static transient String FIELD_NAME_BALANCE_INTEREST_ENABLED
        = 'balance_interest_enabled';

    public String alt_id {get; set;}
    
    public String answer {get; set;}
    
    public Integer api_enable {get; set;}
    
    public String as_user_id {get; set;}
    
    public UserApiAuxUser aux_user {get; set;}

    public Integer balance_interest_enabled {get; set;}
    
    public String bank_accountno {get; set;}

    public String introducing_broker {get; set;}
    /**
     * Ex. "2022-06-28"
    **/
    public String birthdate {get; set;}
    
    public String citizenship {get; set;}

    public List<UserApiAddress> contacts_address {get; set;}

    public UserApiPhone contacts_phone {get; set;}

    public List<UserApiTradingExpirience> trading_experiences {get; set;}

    public String email {get; set;}

    public String employer_bustype {get; set;}

    public String employer_name {get; set;}

    public String entity {get; set;}

    public String entity_type {get; set;}

    public String fax {get; set;}

    public List<String> foreign_residences {get; set;}

    public String homecurr {get; set;}

    public Integer mfa_required {get; set;}

    public String name {get; set;}

    public String password_check {get; set;}

    public String ssn {get; set;}
    
    public Integer withhold_enabled {get; set;}

    public static UserApiUserPatchRequest fromMap(
        Map<String, Object> toUpdate
    ) {
        if (nothingChange(toUpdate))
            return null;

        UserApiUserPatchRequest result = new UserApiUserPatchRequest();

        if (toUpdate.containsKey(Constants.API_FIELD_EMAIL))
            result.email = (String) toUpdate.get(Constants.API_FIELD_EMAIL);

        if (toUpdate.containsKey(Constants.API_FIELD_NAME))
            result.name = (String) toUpdate.get(Constants.API_FIELD_NAME);

        if (toUpdate.containsKey(Constants.API_FIELD_CITIZENSHIP))
            result.citizenship =
                (String) toUpdate.get(Constants.API_FIELD_CITIZENSHIP);

        return result;
    }

    public static UserPatchRequestWrapper getEnableBalanceInterestWrapper() {
        UserPatchRequestWrapper result = new UserPatchRequestWrapper();
        result.user = new UserApiUserPatchRequest();
        result.user.balance_interest_enabled = 1;

        result.change = new AuditTrailManager.AuditTrailChange(
            FIELD_NAME_BALANCE_INTEREST_ENABLED, false, true);
        return result;
    }

    public static UserPatchRequestWrapper getDisableBalanceInterestWrapper() {
        UserPatchRequestWrapper result = new UserPatchRequestWrapper();
        result.user = new UserApiUserPatchRequest();
        result.user.balance_interest_enabled = 0;

        result.change = new AuditTrailManager.AuditTrailChange(
            FIELD_NAME_BALANCE_INTEREST_ENABLED, true, false);
        return result;
    } 
    
    public static UserPatchRequestWrapper getGrantApiAccessWrapper() {
        UserPatchRequestWrapper result = new UserPatchRequestWrapper();
        result.user = new UserApiUserPatchRequest();
        result.user.api_enable = 1;

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_API_ACCESS, false, true);
        return result;
    }

    public static UserPatchRequestWrapper getUpdateIntroducingBrokerWrapper(fxAccount__c fxa) {
        UserPatchRequestWrapper result = new UserPatchRequestWrapper();
        result.user = new UserApiUserPatchRequest();
        result.user.introducing_broker = fxa.Introducing_Broker__r.Introducing_Broker_Name__c ?? '';

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_API_ACCESS, 
            false, 
            true
        );
        
        return result;
    }

    public static UserPatchRequestWrapper getRevokeApiAccessWrapper() {
        UserPatchRequestWrapper result = new UserPatchRequestWrapper();
        result.user = new UserApiUserPatchRequest();
        result.user.api_enable = 0;

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_API_ACCESS, true, false);
        return result;
    }
    
    static Boolean nothingChange(Map<String, Object> toUpdate) {
        return !toUpdate.containsKey(Constants.API_FIELD_EMAIL) && 
            !toUpdate.containsKey(Constants.API_FIELD_NAME) && 
            !toUpdate.containsKey(Constants.API_FIELD_CITIZENSHIP);
    }
    
    public static void setUserFieldToMap(Map<String, Object> wrapper, Boolean isAuxUser, String sfField, String mappedField, Object value) {
        Map<String, Object> auxUserMap = new Map<String, Object>();
        if (isAuxUser && wrapper.get('aux_user') != null) {
            auxUserMap = (Map<String, Object>) wrapper.get('aux_user');
        }

        switch on sfField {
            when 'Birthdate__c' {
                if (value != null) {
                    wrapper.put(mappedField, ((Datetime) value).format('yyyy-MM-dd'));
                } else {
                    wrapper.put(mappedField, '0001-01-01');
                }
            }
            when 'Is_2FA_Required__c' {
                wrapper.put(mappedField, ((Boolean) value == true) ? 1 : 0);
            }
            when 'Citizenship_Nationality__c', 'FXDB_Name__c', 'First_Name__c', 'Middle_Name__c', 'Last_Name__c',
                 'Suffix__c', 'Employer_Name__c', 'Government_ID__c', 'Industry_of_Employment__c', 'Email__c' {
                wrapper.put(mappedField, (String) value ?? '');
            }
            when 'Language_Preference__c' {
                if (String.isNotBlank((String) value)) {
                    wrapper.put(
                        mappedField,
                        OutgoingNotificationBusConstants.fxAccountLanguageMap.get(((String) value).toUpperCase())
                    );
                }
            }
            //Aux users fields
            when 'Employment_Status__c' {
                if (String.isNotBlank((String) value)) {
                    auxUserMap.put(mappedField, OutgoingNotificationBusConstants.fxStatusMap.get(((String) value).toUpperCase()));
                } else {
                    auxUserMap.put(mappedField, 0);
                }
            }
            when 'Has_Opted_Out_Of_Email__c' {
                auxUserMap.put(mappedField, ((Boolean) value == false) ? 'T' : 'F');
            }
            when 'IIROC_Member__c', 'PEFP__c', 'Intermediary__c', 'Relationship_to_OANDA__c' {
                auxUserMap.put(mappedField, ((Boolean) value == true) ? '1' : '0');
            }
            when 'Risk_Tolerance_Amount__c', 'Savings_and_Investments__c', 'Liquid_Net_Worth_Start__c', 'Risk_Tolerance_CAD__c',
                'Liquid_Net_Worth_Value__c', 'Liquid_Net_Worth_End__c', 'Net_Worth_Value__c' {
                auxUserMap.put(mappedField, value ?? 0);
            }
            when 'PEFP_Details__c', 'Employment_Job_Title__c', 'Registration_Source__c', 'Self_Employed_Details__c', 'Annual_Income__c' {
                auxUserMap.put(mappedField, (String) value ?? '');
            }
            when 'Introducing_Broker_Number__c','Net_Worth_Range_Start__c', 'Net_Worth_Range_End__c' {
                auxUserMap.put(mappedField, String.isBlank((String) value) ? 0 : value);
            }
            when else {
                if (isAuxUser) {
                    auxUserMap.put(mappedField, value);
                } else {
                    wrapper.put(mappedField, value);
                }
            }
        }

        if (isAuxUser) {
            wrapper.put('aux_user', auxUserMap);
        }
    }

    public class UserPatchRequestWrapper {
        public UserApiUserPatchRequest user {get; set;}

        public AuditTrailManager.AuditTrailChange change {get; set;}
    }
}