/**
 * Knoledge data categories constants
 * @author       Ariel Cantero
 * @since        06/14/2019
 */
global class DataCategoriesConstants {

	global static final String DAT_CAT_CATEGORIES = 'Categories';
	global static final String DAT_CAT_AUDIENCE = 'Audience';
	global static final String DAT_CAT_DIVISION = 'Division';

	global static final String ARTICLE_SOBJECT_NAME = 'KnowledgeArticleVersion';

}