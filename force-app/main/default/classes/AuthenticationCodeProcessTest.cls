/**
 * @File Name          : AuthenticationCodeProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/21/2023, 9:49:03 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 1:09:18 PM   aniubo     Initial Version
 **/
@isTest
private class AuthenticationCodeProcessTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		initManager.setFieldValue(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			fxAccount__c.Authentication_Code__c,
			'811109'
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			fxAccount__c.Authentication_Code_Expires_On__c,
			Datetime.now().addDays(1)
		);
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);

		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}

	@isTest
	private static void testVerifyAuthenticationCode() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		// Actual test
		Test.startTest();
		AuthenticationCodeProcess authCodeProcess = new AuthenticationCodeProcess(
			fxAccountId,
			caseId
		);
		Boolean isValid = authCodeProcess.verifyAuthenticationCode('811109');

		Assert.areEqual(true, isValid, 'Authentication code is valid');

		isValid = authCodeProcess.verifyAuthenticationCode('811102');

		Assert.areEqual(false, isValid, 'Authentication code is not valid');

		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testVerifyAuthenticationCodeEx() {
		// Test data setup
		Boolean throwException = false;
		// Actual test
		Test.startTest();
		AuthenticationCodeProcess authCodeProcess = new AuthenticationCodeProcess(
			null,
			null
		);
		try {
			Boolean isValid = authCodeProcess.verifyAuthenticationCode(
				'811109'
			);
		} catch (Exception ex) {
			throwException = true;
		}

		Test.stopTest();
		Assert.areEqual(true, throwException, 'Exception is thrown');
	}

	@isTest
	private static void testSendAuthenticationCodeParamsAreNull() {
		// Test data setup
		Boolean throwException = false;
		// Actual test
		Test.startTest();
		AuthenticationCodeProcess authCodeProcess = new AuthenticationCodeProcess(
			null,
			null
		);

		Boolean isValid = authCodeProcess.sendAuthenticationCode('es');

		Test.stopTest();
		Assert.areEqual(
			false,
			isValid,
			'Case Id and FX Account Id are null. Email must no send'
		);
	}

	@isTest
	private static void testSendAuthenticationCode() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Id accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		Id fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		// Actual test
		Test.startTest();
		fxAccount__c fxAccount = FxAccountRepo.getSummaryByAccount(accountId);
		AuthenticationCodeProcess authCodeProcess = new AuthenticationCodeProcess(
			fxAccount.Id,
			caseId
		);
		Boolean isValid = authCodeProcess.sendAuthenticationCode('es');

		Assert.areEqual(true, isValid, 'Authentication code is valid');

		isValid = authCodeProcess.verifyAuthenticationCode('811102');

		Assert.areEqual(false, isValid, 'Authentication code is not valid');

		Test.stopTest();
		fxAccount = getFxAccount(fxAccountId);
		Assert.areEqual(
			true,
			fxAccount.Authentication_Code__c != null,
			'Authentication code is not null'
		);
		Assert.areEqual(
			true,
			fxAccount.Authentication_Code_Expires_On__c != null,
			'Authentication code expires on is not null'
		);
		// Asserts
	}

	private static fxAccount__c getFxAccount(Id fxAccountId) {
		return [
			SELECT Id, Authentication_Code__c, Authentication_Code_Expires_On__c
			FROM fxAccount__c
			WHERE Id = :fxAccountId
		];
	}
}