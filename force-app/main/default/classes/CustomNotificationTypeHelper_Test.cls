/**
 * @File Name          : CustomNotificationTypeHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/21/2021, 5:32:43 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/21/2021, 5:27:56 PM   acantero     Initial Version
**/
@IsTest
private without sharing class CustomNotificationTypeHelper_Test {

	// @testSetup
    // static void setup() {
    // }

    //invalid devName, require = true -> thow exception
    @IsTest
    static void getByDevName1() {
        Boolean error = false;
        Test.startTest();
        try {
            CustomNotificationType result = 
                CustomNotificationTypeHelper.getByDevName(
                    'fakeName', //devName
                    true //required
                );
            //...
        } catch (CustomNotificationTypeHelper.CustomNotificationTypeHelperException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
    
}