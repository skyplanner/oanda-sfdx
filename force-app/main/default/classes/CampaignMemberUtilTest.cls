@isTest
private class CampaignMemberUtilTest {

    static testMethod void testAddTasks() {
        Campaign c = new Campaign(Name='test campaign', Is_Auto_Create_Task__c=true);
        insert c;
        
        Lead l = new Lead(LastName='test', Email='test@oanda.com');
        insert l;
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, LeadId=l.Id);
        insert cm;
        
        System.assertEquals(1, [SELECT Id FROM Task WHERE WhoId=:l.Id].size());
    }

    static testMethod void testGetLastTradeDateFromFxAccount() {
        Campaign c = new Campaign(Name='test campaign', Is_Auto_Create_Task__c=true);
        insert c;
        
        Account a = new Account(LastName='test account');
        insert a;
        
        Contact con = [SELECT Id FROM Contact WHERE AccountId = :a.Id];
        System.debug('con: ' + con);
        
        Opportunity o = new Opportunity(Name='test opp', AccountId=a.Id, StageName='Before you begin', CloseDate=Date.today());
        insert o;
        
        DateTime thirtyDaysAgo = DateTime.now().addDays(-30);
        fxAccount__c fxa = new fxAccount__c(Account__c=a.Id, Opportunity__c=o.Id, Contact__c=con.Id, Last_Trade_Date__c=thirtyDaysAgo, RecordTypeId=RecordTypeUtil.getFxAccountLiveId());
        insert fxa;
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=con.Id);
        insert cm;
        System.debug('cm: ' + cm);
        
        System.assertEquals(thirtyDaysAgo, [SELECT Last_Trade_DateTime_Before_Campaign__c FROM CampaignMember WHERE Id=:cm.Id][0].Last_Trade_DateTime_Before_Campaign__c);
    }
    
    static testMethod void testGetMostRecentReactivationCampaignMembers() {
    	Campaign c1 = new Campaign(Name='test campaign 1', Type=CampaignUtil.CAMPAIGN_TYPE_REACTIVATION);
    	Campaign c2 = new Campaign(Name='test campaign 2', Type=CampaignUtil.CAMPAIGN_TYPE_REACTIVATION);
    	
    	insert new List<Campaign>{c1, c2};
    	
    	Account a = new Account(LastName='test');
    	insert a;
    	
    	Contact c = [SELECT Id FROM Contact WHERE AccountId = :a.Id];
    	
    	CampaignMember cm1 = new CampaignMember(CampaignId=c1.Id, ContactId=c.Id);
    	insert cm1;
    	
    	Opportunity o = new Opportunity(Name='test', AccountId=a.Id, StageName='Not started', CloseDate=Date.today());
    	insert o;
    	
    	fxAccount__c fxa = new fxAccount__c(Contact__c=c.Id, Account__c=a.Id, Opportunity__c=o.Id, Last_Trade_Date__c=DateTime.now().addHours(-1), RecordTypeId=RecordTypeUtil.getFxAccountLiveId());
    	insert fxa;
    	
    	CampaignMember cm2 = new CampaignMember(CampaignId=c2.Id, ContactId=c.Id);
    	insert cm2;
    	
    	Task t1 = new Task(WhoId=c.Id, Status='Completed');
    	insert t1;
    	
		fxa.Last_Trade_Date__c = DateTime.now();
		update fxa;

		boolean hasResponded1 = [SELECT HasResponded FROM CampaignMember WHERE Id = :cm1.Id][0].HasResponded;
		boolean hasResponded2 = [SELECT HasResponded FROM CampaignMember WHERE Id = :cm2.Id][0].HasResponded;
		
		// ideally, we would check to see that the last one is responded, but because we can't guarantee the CreatedDate order, we'll just have to be okay with one of them being marked as responded
		System.assertNotEquals(hasResponded1, hasResponded2);
    }
}