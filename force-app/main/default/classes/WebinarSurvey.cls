public with sharing class WebinarSurvey {
	final static String WEBINAR_NAME_SURVEY_PREFIX = 'Survey - ';
	
	public static void setCampaignAndWhoId(List<Webinar_Survey__c> webinarSurveys) {
		Set<String> campaignNames = new Set<String>();
		Set<String> emails = new Set<String>();
		for(Webinar_Survey__c ws : webinarSurveys) {
			ws.Campaign_Name__c = cleanCampaignName(ws.Campaign_Name__c);
			campaignNames.add(ws.Campaign_Name__c);
			emails.add(ws.Email__c);
		}
		
		if(campaignNames.size()>0) {
			Map<String, Id> idByCampaignName = new Map<String, Id>();
			for(Campaign c : [SELECT Id, Name FROM Campaign WHERE Name IN :campaignNames]) {
				idByCampaignName.put(c.Name, c.Id);
			}
			for(Webinar_Survey__c ws : webinarSurveys) {
				ws.Campaign__c = idByCampaignName.get(ws.Campaign_Name__c);
			}
		}
		if(emails.size()>0) {
			Map<String, Id> whoIdByEmail = getWhoIdByEmail(emails);
			for(Webinar_Survey__c ws : webinarSurveys) {
				setWhoId(ws, whoIdByEmail.get(ws.Email__c));
			}
		}

		// fix the multiselect picklists		
		for(Webinar_Survey__c ws : webinarSurveys) {
			ws.Topics__c = StringUtil.replaceCommaWithSemicolon(ws.Topics_Raw__c);
			ws.Selection_Factors__c = StringUtil.replaceCommaWithSemicolon(ws.Selection_Factors_Raw__c);
			ws.Other_Assistance__c = StringUtil.replaceCommaWithSemicolon(ws.Other_Assistance_Raw__c);
			
			// and set the submitted datetime
			ws.Submitted_DateTime__c = parseDateTime(ws.Submitted_DateTime_Raw__c);
		}
	}
	
	public static String cleanCampaignName(String campaignName) {
		if(campaignName.startsWith(WEBINAR_NAME_SURVEY_PREFIX)) {
			return campaignName.substring(WEBINAR_NAME_SURVEY_PREFIX.length());
		}
		return campaignName;
	}
	
	public static void setWhoId(Webinar_Survey__c ws, Id whoId) {
		if(LeadUtil.isLeadId(whoId)) {
			ws.Lead__c = whoId;
		} else {
			ws.Contact__c = whoId;
		}
	}
	
	public static Map<String, Id> getWhoIdByEmail(Set<String> emails) {
		Map<String, Id> idByEmail = new Map<String, Id>();
		for(Lead l : [SELECT Id, Email FROM Lead WHERE Email IN :emails]) {
			idByEmail.put(l.Email, l.Id);
		}
		for(Contact c : [SELECT Id, Email FROM Contact WHERE Email IN :emails]) {
			idByEmail.put(c.Email, c.Id);
		}
		return idByEmail;
	}

	public static DateTime parseDateTime(String s) {
		integer i = s.indexOf(' EDT');
		if(i>0) {
			s = s.substring(0,i);
		}
		return DateTime.parse(s);
	}
}