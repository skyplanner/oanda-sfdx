/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsDoc {
    public String id { get; set; }
    
    public String last_updated_utc { get; set; }

    public String created_utc { get; set; }

    public List<CASearchDetailsField> fields { get; set; }

    public List<String> types { get; set; }

    public String name { get; set; }

    public String entity_type { get; set; }

    public List<CASearchDetailsAka> aka { get; set; }

    public List<String> sources { get; set; }

    public List<String> keywords { get; set; }

    public List<String> blacklist_ids { get; set; }

    public List<Integer> account_ids { get; set; }

    public List<CASearchDetailsMedia> media { get; set; }

    public List<CASearchDetailsAsset> assets { get; set; }

    public List<CASearchDetailsAssociate> associates { get; set; }

    public Map<String, CASearchDetailsSourceNotes> source_notes { get; set; }
}