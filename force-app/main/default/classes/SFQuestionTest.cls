/**
 * Test: SFQuestion
 * @author Fernando Gomez
 * @version 1.0
 * @since Apr 10th
 */
@isTest
private class SFQuestionTest {
	/**
	 * checkIfFeedbackIsNeeded
	 */
	@isTest 
	static void checkIfFeedbackIsNeeded() {
		Survey__c s;
		List<Survey_Question__c> qs;
		SFQuestion sfParent,sfq1, sfq2, sfq3, sfq4;

		s = new Survey__c(
			Name = 'sample survey'
		);
		insert s;

		Survey_Question__c qParent = new Survey_Question__c(
				Name = 'qParent',
				Question__c = 'Im a father question',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'No',
				OrderNumber__c = 1,
				Required__c = true,
				Type__c = 'Single Select--Vertical',
				Survey__c = s.Id
			);			
        insert qParent;

		qs = new List<Survey_Question__c> {
			new Survey_Question__c(
				Name = 'q1',
				Question__c = 'Like it?',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'No',
				OrderNumber__c = 2,
				Required__c = true,
				Type__c = 'Single Select--Vertical',
				Survey__c = s.Id,
				Parent_Question__c = qParent.Id,
				Parent_Question_Answers__c  = 'Maybe'
			),

			new Survey_Question__c(
				Name = 'q2',
				Question__c = 'Don\'t like it?',
				Choices__c = 'Yes\nNo\nMaybe',
				AnswersToTriggerExtraFeedback__c = 'Maybe',
				OrderNumber__c = 3,
				Required__c = true,
				Type__c = 'Multi-Select--Vertical',
				Survey__c = s.Id
			),
			new Survey_Question__c(
				Name = 'q3',
				Question__c = 'Love it?',
				Choices__c = 'Yes\nNope',
				AnswersToTriggerExtraFeedback__c = 'Nope',
				OrderNumber__c = 4,
				Required__c = true,
				Type__c = 'Single Select--Horizontal',
				Survey__c = s.Id
			),
			new Survey_Question__c(
				Name = 'q4',
				Question__c = 'Don\'t love it?',
				OrderNumber__c = 5,
				Required__c = true,
				Type__c = 'Free Text - Single Row Visible',
				Survey__c = s.Id
			)
		};
		insert qs;

		qs = [
			Select 
				Type__c,
				Id,
				Survey__c, 
				Required__c,
				Question__c, 
				OrderNumber__c,
				Name,
				Choices__c,
				Parent_Question__c,
				Parent_Question_Answers__c,
				AnswersToTriggerExtraFeedback__c, (
					SELECT TranslatedQuestion__c, 
						TranslatedChoices__c,
						TransalatedExtraFeedbackAnswers__c
					FROM SurveyQuestionTranslations__r
					WHERE Language__c = 'en_US'
				)
			From Survey_Question__c
			ORDER BY OrderNumber__c
		];

		sfParent = new SFQuestion(qs[0]);
		sfq1 = new SFQuestion(qs[1]);
		sfq2 = new SFQuestion(qs[2]);
		sfq3 = new SFQuestion(qs[3]);
		sfq4 = new SFQuestion(qs[4]);

	    sfParent.selectedOption = '2';
		sfq1.selectedOption = '1';
		sfq2.selectedOptions = new List<String> { '2' };
		sfq3.selectedOption = '1';
		sfq4.selectedOption = '1';

		sfParent.checkIfFeedbackIsNeeded();
        /* Dependent question */
		sfq1.checkIfFeedbackIsNeeded();
		Boolean dep = sfq1.isDependantAnswer(sfParent);
		String response1 = sfq1.getResponse();
		/**Clear Response */
		sfq1.clearResponse();
		sfq1.selectedOption = '0';
		String clearResponse = sfq1.getResponse();
		
		/*Second question*/
		sfq2.checkIfFeedbackIsNeeded();
		Boolean dep2 = sfq2.isDependantAnswer(sfParent);
		String response2 = sfq2.getResponse();
        /* Third question */
		sfq3.checkIfFeedbackIsNeeded();

       /* Fourth question */
		sfq4.checkIfFeedbackIsNeeded();
        String response4 = sfq4.getResponse();

		System.assert(sfq1.needsExtraFeedback);
		System.assert(sfq2.needsExtraFeedback);
		System.assert(sfq3.needsExtraFeedback);
		System.assert(!sfq4.needsExtraFeedback);
		System.assert(dep);
		System.assert(response1 == 'No');
		System.assert(clearResponse == 'Yes');	
		System.assert(!dep2);
		System.assert(response2 == 'Maybe');
		System.assert(response4 == '');

		
	}
}