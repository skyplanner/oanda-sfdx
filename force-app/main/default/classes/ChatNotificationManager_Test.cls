/**
 * @File Name          : ChatNotificationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 10:56:08 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 10:16:31 AM   acantero     Initial Version
**/
@isTest
private without sharing class ChatNotificationManager_Test {

    @testSetup
    static void setup() {
        SLAViolationsTestDataFactory.createTestChats();
    }

    @isTest
    static void sendNotifications() {
        String chatId = [select Id from LiveChatTranscript limit 1].Id;
        SLAChatViolationInfo info = 
            SLAViolationsTestDataFactory.getSLAChatViolationInfo(
                chatId, 
                SLAConst.ALERT_NO_ANSWER_VT
            );
        List<SLAChatViolationInfo> infoList = 
            new List<SLAChatViolationInfo> { info };
        Boolean error = false;
        Test.startTest();
        try {
            // ChatNotificationManager notifManager = new ChatNotificationManager(
            //     SLAConst.AgentSupervisorModel.ROLE
            // );
            // //fill supervisorProv with mock class instance
            // notifManager.supervisorProv = 
            //     SLAViolationsTestDataFactory.getMockSupervisorByRoles();
            // notifManager.sendNotifications(
            //     infoList,
            //     'fake subject',
            //     'fake body'
            // );
            //...
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
    
}