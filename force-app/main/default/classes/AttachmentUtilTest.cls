@isTest
private class AttachmentUtilTest {

    static User FXS_USER = UserUtil.getRandomTestUser();
    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
    final static User REGISTRATION_USER = UserUtil.getUserByName('Registration User');


    static testMethod void testAttachmentDelete() {
        System.runAs(FXS_USER) {
            Lead l = new Lead(LastName='test lead', Email='test@oanda.com');
            insert l;
            
            Blob body = Blob.valueOf('test body');
            
            Attachment a = new Attachment(ParentId=l.Id, Name='test attachment', Body=body);
            insert a;
            
            
            try {
                delete a;
                System.assertEquals(0,1,'File should not be deleted');
            } catch(Exception e) {
                
            }
            System.assertEquals(1, [SELECT Id FROM Attachment WHERE Id = :a.Id].size());
        }
    }
    
    static testMethod void testReparentFromEmailMessageToDocument() {
        Case c = new Case();
        insert c;
        
        EmailMessage em = new EmailMessage(ParentId=c.Id);
        insert em;
        
        Blob body = Blob.valueOf('test body');
        
        Attachment a = new Attachment(ParentId=em.Id, Name='test attachment', Body=body);
        insert a;
        
        a = [SELECT Id, ParentId FROM Attachment WHERE Id = :a.Id][0];
        System.assertNotEquals(em.Id, a.ParentId);
        
        Document__c d = [SELECT Id, Case__c FROM Document__c WHERE Id = :a.ParentId][0];
        System.assertEquals(c.Id, d.Case__c);
    }
    
    static testMethod void testReparentFromAccountToDocument() {
        Account acct = new Account(Name='test');
        insert acct;
        
        Blob body = Blob.valueOf('test body');
        
        Attachment a = new Attachment(ParentId=acct.Id, Name='test attachment', Body=body);
        insert a;
        
        a = [SELECT Id, ParentId FROM Attachment WHERE Id = :a.Id][0];
        System.assertNotEquals(acct.Id, a.ParentId);
        
        Document__c d = [SELECT Id, Case__c FROM Document__c WHERE Id = :a.ParentId][0];
        
        //we no longer create case when attaching doc to Account
        //Case c = [SELECT Id, AccountId FROM Case WHERE Id = :d.Case__c][0];
        //System.assertEquals(acct.Id, c.AccountId);
    }
    
    static testMethod void testReparentFromLeadToDocument() {
        System.runAs(REGISTRATION_USER) {
	        Lead l = new Lead(LastName='test', Email='test@oanda.com');
	        insert l;
	        
	        Blob body = Blob.valueOf('test body');
	        
	        Attachment a = new Attachment(ParentId=l.Id, Name='test attachment', Body=body);
	        insert a;
            
            a = [SELECT Id, ParentId FROM Attachment WHERE Id = :a.Id][0];
            System.assertNotEquals(l.Id, a.ParentId);
        
            Document__c d = [SELECT Id, Case__c FROM Document__c WHERE Id = :a.ParentId][0];
            
            //we no longer create case when attaching doc to lead
            //Case c = [SELECT Id, Lead__c FROM Case WHERE Id = :d.Case__c][0];
            //System.assertEquals(l.Id, c.Lead__c);
        }
    }
    /*
    static testMethod void testMigrateDocuments() {
        
        System.runAs(SYSTEM_USER) {
            Lead lead = new Lead(LastName='Test convert', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Funnel_Stage__c = FunnelStatus.TRADED, Email='joe@example.org');
            insert lead;
            
            lead = [SELECT Id, Email, IsConverted FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(false, lead.IsConverted);
            
            
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
            tradeAccount.Account_Email__c = lead.Email;
            
            insert tradeAccount;

            Blob body = Blob.valueOf('test body');
            
            Attachment a = new Attachment(ParentId=lead.Id, Name='test attachment', Body=body);
            insert a;
            
            BatchConvertLeadsScheduleable.executeInline();
            
            Lead updatedLead = [SELECT IsConverted, ConvertedAccountId FROM Lead WHERE Id=:lead.Id];
            System.assertEquals(true, updatedLead.IsConverted);

            a = [SELECT Id, ParentId FROM Attachment WHERE Id = :a.Id][0];
    
            Document__c d = [SELECT Id, Case__c, Account__c, Lead__c FROM Document__c WHERE Id = :a.ParentId][0];
            System.assertEquals(null, d.Lead__c);
            System.assertEquals(updatedLead.ConvertedAccountId , d.Account__c);
            
            Case c = [SELECT Id, Lead__c, AccountId FROM Case WHERE Id = :d.Case__c][0];
            System.assertEquals(null, c.Lead__c);
            System.assertEquals(updatedLead.ConvertedAccountId , c.AccountId);
        }
        
    }
*/

    static testMethod void testPostMyNumDocToAccount() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    
	    RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account a = new Account();
        a.FirstName = 'Fred';
        a.LastName = 'Smith';
        a.RecordType = personAccountRecordType;
        a.PersonEmail='test456@abc.com';
        
	    insert a;
	    
	    
	    User sysUser = UserUtil.getSystemUserForTest();
	    
	    API_Access_Control__c control = new API_Access_Control__c(name = 'My Number', User_Allowed__c = sysUser.Id, Enabled__c = true );
	    insert control;
	    
	    System.runas(sysUser){
		    // send MyNumber document 
		    req.requestURI = '/services/apexrest/Attachment';  
		    req.httpMethod = 'POST';
		    
		    req.params.put('filename', 'httpclient-tutorial.pdf');
		    req.params.put('clientemail', 'test456@abc.com');
		    req.params.put('isMyNumber', 'TRUE');
		    req.requestBody = Blob.valueOf('abc');
		    
		
			RestContext.request = req;
	    	RestContext.response = res;
		    
		    String result = AttachmentRestResource.doPost();
		
		    System.assert(result.contains('Salesforce ID:'));
		    
		    //check if the pdf is attached to the account
		    List<Document__c> docs = [select id, Lead__c, Account__c, Is_My_Number__c from Document__c where name = 'httpclient-tutorial.pdf'];
		    System.assertEquals(a.Id, docs[0].Account__c);
		    System.assertEquals(true, docs[0].Is_My_Number__c);
		    
		    //send normal document
		    req.params.put('filename', 'httpclient.pdf');
		    req.params.put('isMyNumber', 'FALSE');
		    
		    result = AttachmentRestResource.doPost();
		
		    System.assert(result.contains('Salesforce ID:'));
		    
		    //check if the pdf is attached to the account
		    docs = [select id, Lead__c, Account__c, Is_My_Number__c from Document__c where name = 'httpclient.pdf'];
		    
		    System.assertEquals(a.Id, docs[0].Account__c);
		    System.assertEquals(false, docs[0].Is_My_Number__c);
		    
	    }
	}

	static testMethod void testPostMyNumDocToLead() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    
	   
        Lead a = new Lead();
        a.FirstName = 'Fred';
        a.LastName = 'Smith';
        a.RecordTypeId = RecordTypeUtil.getLeadRetailId();
        a.Email='test456@abc.com';
        
	    insert a;
	    
	    
	    User sysUser = UserUtil.getSystemUserForTest();
	    
	    API_Access_Control__c control = new API_Access_Control__c(name = 'My Number', User_Allowed__c = sysUser.Id, Enabled__c = true );
	    insert control;
	    
	    System.runas(sysUser){
		    // send MyNumber document 
		    req.requestURI = '/services/apexrest/Attachment';  
		    req.httpMethod = 'POST';
		    
		    req.params.put('filename', 'httpclient-tutorial.pdf');
		    req.params.put('clientemail', 'test456@abc.com');
		    req.params.put('isMyNumber', 'TRUE');
		    req.requestBody = Blob.valueOf('abc');
		    
		
			RestContext.request = req;
	    	RestContext.response = res;
		    
		    String result = AttachmentRestResource.doPost();
		
		    System.assert(result.contains('Salesforce ID:'));
		    
		    //check if the pdf is attached to the account
		    List<Document__c> docs = [select id, Lead__c, Account__c, Is_My_Number__c from Document__c where name = 'httpclient-tutorial.pdf'];
		    System.assertEquals(a.Id, docs[0].Lead__c);
		    System.assertEquals(true, docs[0].Is_My_Number__c);
		    
		    //send normal document
		    req.params.put('filename', 'httpclient.pdf');
		    req.params.put('isMyNumber', 'FALSE');
		    
		    result = AttachmentRestResource.doPost();
		
		    System.assert(result.contains('Salesforce ID:'));
		    
		    //check if the pdf is attached to the account
		    docs = [select id, Lead__c, Account__c, Is_My_Number__c from Document__c where name = 'httpclient.pdf'];
		    
		    System.assertEquals(a.Id, docs[0].Lead__c);
		    System.assertEquals(false, docs[0].Is_My_Number__c);
		    
	    }
	}
}