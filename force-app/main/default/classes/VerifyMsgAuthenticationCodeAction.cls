/**
 * @File Name          : VerifyMsgAuthenticationCodeAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/14/2023, 2:36:35 PM
**/
public without sharing class VerifyMsgAuthenticationCodeAction {
	
	@InvocableMethod(label='Verify Messaging Authentication Code' callout=false)
	public static List<Boolean> verifyMsgAuthenticationCode(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<Boolean> result = new List<Boolean>{ false };
		
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(infoList[0].messagingSessionId);
			result[0] = process.verifyAuthenticationCode(
				infoList[0].fxAccountId, // fxAccountId
				infoList[0].code // code
			);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				VerifyMsgAuthenticationCodeAction.class.getName(),
				ex
			);
		}   
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public ID messagingSessionId;

		@InvocableVariable(label = 'fxAccountId' required = true)
		public ID fxAccountId;

		@InvocableVariable(label = 'code' required = true)
		public String code;
		
	}
	
}