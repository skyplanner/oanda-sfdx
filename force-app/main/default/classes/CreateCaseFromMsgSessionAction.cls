/**
 * @File Name          : CreateCaseFromMsgSessionAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/17/2023, 2:43:05 AM
**/
public without sharing class CreateCaseFromMsgSessionAction {

	@InvocableMethod(label='Create Case From Msg Session' callout=false)
	public static List<CaseInfo> createCaseFromMsgSession(List<ID> messagingSessionIds) { 
		try {
			ExceptionTestUtil.execute();
			List<CaseInfo> result = new List<CaseInfo> { new CaseInfo() };

			if (
				(messagingSessionIds == null) ||
				messagingSessionIds.isEmpty()
			) {
				return result;
			}
			// else...        
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(messagingSessionIds[0]);
			result[0] = new CaseInfo(process.createCaseFromMsgSession());
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CreateCaseFromMsgSessionAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class CaseInfo {

		@InvocableVariable(label = 'caseId' required = false)
		public ID caseId;

		@InvocableVariable(label = 'caseNumber' required = false)
		public String caseNumber;

		public CaseInfo() {
		}

		public CaseInfo(Case caseObj) {
			if (caseObj != null) {
				this.caseId = caseObj.Id;
				this.caseNumber = caseObj.CaseNumber;
			}
		}
		
	}

}