/**
 * @File Name          : RelatedMessagingSessionsReaderMock.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 2:09:07 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/14/2024, 1:43:39 PM   aniubo     Initial Version
**/
@IsTest
public class RelatedMessagingSessionsReaderMock implements IRelatedMessagingSessionsReader {
    private Boolean isAccount = false;
    public RelatedMessagingSessionsReaderMock(Boolean isAccount) {
        this.isAccount = isAccount;
    }
    public Integer getCount(
        Id currentSessionId,
        String filterFieldApiName,
        Set<Id> messagingEndUserIds
    ) {
        return 1;
    }

    public List<MessagingSession> getRelatedMessagingSessions(
        Set<Id> messagingEndUserIds,
        List<String> fields,
        String filterFieldApiName,
        Integer pageSize,
        Integer pageNo
    ) {
        return new List<MessagingSession>();
    }

    public MessagingSessionRelatedEntity getMessagingRelatedEntity(
        Id messagingSessionId
    ) {
        return new MessagingSessionRelatedEntity(
            UserInfo.getUserId(),
            this.isAccount
        );
    }

    public Set<Id> getMessagingEndUsers(Id accountId) {
        return new Set<Id>{ UserInfo.getUserId() };
    }
}