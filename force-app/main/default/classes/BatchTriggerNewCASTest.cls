/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 08-17-2022
 * @last modified by  : Yaneivys Gutierrez
**/

@isTest
private class BatchTriggerNewCASTest {
    @isTest
    static void triggerSearch() {
        Settings__c settings = new Settings__c(
            Name = 'Default',
            Comply_Advantage_Enabled__c = false
        );
        insert settings;
        
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
            Name = 'NA',
            API_Key__c = 'test'
        );
        insert caInstanceSetting;
        
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
            Name = 'OANDA Canada',
            Search_Profile__c = 'CA',
            Fuzziness__c = 60,
            Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
            Region__c = 'OCAN',
            New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
        );
        insert caDivisionSetting;

        List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

        fxAccount__c fxAcc = new fxAccount__c(
            Account_Email__c = 'testingBatch1@oanda.com',
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = 'OANDA Canada',
            Birthdate__c = Date.newInstance(1980, 11, 21),
            Citizenship_Nationality__c = 'Canada',
            Account__c = accs[0].Id,
            Is_Closed__c = false
        );
        insert fxAcc;

        Test.setMock(HttpCalloutMock.class,
            new CalloutMock(200, '', 'success', null));

        Settings__c setts = Settings__c.getValues('Default');
        setts.Comply_Advantage_Enabled__c = true;
        update setts;

        Test.StartTest();

        BatchTriggerNewCAS b = new BatchTriggerNewCAS(' AND Id = \'' + fxAcc.Id + '\'');
        Id batchProcessId = Database.executeBatch(b);

        Test.stopTest();

        AsyncApexJob aaj = [
            SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
            FROM AsyncApexJob
            WHERE Id =: batchProcessId ];


        System.assertEquals(true, aaj != null);
        System.assertEquals(1, aaj.TotalJobItems);
    }
}