/**
 * @version 1.0 Renaming old "Question" to "SFQuestion" class
 * This is to avoid problem with "Question" object which is added by Chatter Answers
 *
 * Class: question
 *  Retrieves the question information and puts it in the question object
 *
 * @version 2.0, Fernando Gomez, SkyPlanner LLC
 */ 
public with sharing class SFQuestion { 
	public String name { get; set; }
	public String id { get; set; }
	public String question { get; set; }
	public String orderNumber { get; set; }
	public String choices { get; set; }
	public String selectedOption { get;set; }

	public List<String> selectedOptions { get; set; }
	public List<String> originalOptions { get; set; }
	public List<SelectOption> singleOptions { get; set; }
	public List<SelectOption> multiOptions { get; set; }
	public Boolean required { get; set; }
	public String questionType { get; set; }
	public String renderFreeText { get; set; }
	public String renderSelectRadio { get; set; }
	public String renderSelectCheckboxes { get; set; }
	public String renderSelectRow { get; set; } 
	public List<SelectOption> rowOptions { get; set; }

	public Boolean hasError { get; set; }
	public Boolean wasChecked { get; set; }
	public String errorMessage { get; set; }
	public String noOfRowsForTextArea { get; set; }

	public Boolean needsExtraFeedback { get; set; }
	public String extraFeedback { get; set; }
	public Set<String> extraFeedbackAnswers { get; set; }

	public Boolean isQuestionDependant { get; private set; }
	public Id parentQuestionId { get; private set; }
	public String parentQuestionAnswersText { get; private set; }
	public Set<String> parentQuestionAnswers { get; private set; }

	/**
	 * Fills up the question object
	 * @param Survey_Question__c
	 */
	public SFQuestion(Survey_Question__c sq) {
		SurveyQuestionTransalation__c translation;
		String extraAnswers;

		name = sq.Name;
		id = sq.Id;
		orderNumber = String.valueOf(sq.OrderNumber__c + 1);
		choices = sq.Choices__c;
		required = sq.Required__c;
		questionType = sq.Type__c;
		parentQuestionId = sq.Parent_Question__c;
		parentQuestionAnswersText = sq.Parent_Question_Answers__c;

		selectedOption = '';
		selectedOptions = new List<String>();
		originalOptions = new List<String>();
		extraFeedback = null;
		hasError = false;
		wasChecked = false;
		errorMessage = null;

		if (!String.isBlank(sq.Choices__c))
			originalOptions = stringToList(sq.Choices__c);
		
		if (!String.isBlank(parentQuestionId)) {
			isQuestionDependant = true;
			parentQuestionAnswers = stringToSet(parentQuestionAnswersText);
		} else
			isQuestionDependant = false;

		// the text of the question can be transalted
		if (sq.SurveyQuestionTranslations__r.isEmpty()) {
			question = sq.Question__c;
			choices = sq.Choices__c;
			extraAnswers = sq.AnswersToTriggerExtraFeedback__c;
		} else {
			translation = sq.SurveyQuestionTranslations__r[0];
			question = translation.TranslatedQuestion__c;
			choices = translation.TranslatedChoices__c;
			extraAnswers = translation.TransalatedExtraFeedbackAnswers__c; 
		}

		if (sq.Type__c == 'Single Select--Vertical'){
			renderSelectRadio ='true';
			singleOptions = stringToSelectOptions(choices);

			renderSelectCheckboxes = 'false';
			renderFreeText = 'false';
			renderSelectRow = 'false';
			selectedOption = '';
			selectedOptions = new List<String>();
		} else if (sq.Type__c == 'Multi-Select--Vertical'){
			renderSelectCheckboxes='true';
			multiOptions = stringToSelectOptions(choices);

			renderSelectRadio = 'false';
			renderFreeText = 'false';
			renderSelectRow = 'false';
			selectedOption = '';
			selectedOptions = new List<String>();
		} else if (sq.Type__c == 'Single Select--Horizontal'){
			renderSelectCheckboxes='false';
			rowOptions = stringToSelectOptions(choices);

			renderSelectRadio = 'false';
			renderFreeText = 'false';
			renderSelectRow = 'true';
			selectedOption = '';
			selectedOptions = new List<String>();

		} else if (sq.Type__c == 'Free Text' || 
				sq.Type__c == 'Free Text - Single Row Visible') {
			renderFreeText = 'true';
			renderSelectRadio ='false';
			renderSelectCheckboxes ='false';
			renderSelectRow = 'false';
			choices = '';

			// If it's text area but for single row then only show single 
			// row even though it's stil text area
			if (sq.Type__c == 'Free Text - Single Row Visible')
				noOfRowsForTextArea = '1';
			else
				noOfRowsForTextArea = '2';
		}

		// we need to serialize the answers
		needsExtraFeedback = false;
		extraFeedbackAnswers = stringToSet(extraAnswers);
	}

	/**
	 * @return if the question need extra feedback based on the answer
	 */
	public void checkIfFeedbackIsNeeded() {
		needsExtraFeedback = false;

		if (extraFeedbackAnswers != null) {
			if (renderSelectRadio == 'true')
				needsExtraFeedback = !String.isBlank(selectedOption) &&
					extraFeedbackAnswers.contains(
						singleOptions.get(Integer.valueOf(selectedOption)).getLabel());
			else if (renderSelectRow == 'true')
				needsExtraFeedback = !String.isBlank(selectedOption) &&
					extraFeedbackAnswers.contains(
						rowOptions.get(Integer.valueOf(selectedOption)).getLabel());
			else if (renderSelectCheckboxes == 'true')
				for (String opt : selectedOptions)
					if (!String.isBlank(opt) && 
							extraFeedbackAnswers.contains(
								multiOptions.get(Integer.valueOf(opt)).getLabel())) {
						
						needsExtraFeedback = true;
						break;
					}
		}
	}

	/**
	 * @param 
	 * @return 
	 */
	public Boolean isDependantAnswer(SFQuestion parentQuestion) {
		if (!isQuestionDependant || parentQuestionAnswers.isEmpty())
			return false;
	
		if (parentQuestion.renderSelectRadio == 'true' ||
				parentQuestion.renderSelectRow == 'true')
			return !String.isBlank(parentQuestion.selectedOption) &&
				parentQuestionAnswers.contains(
					parentQuestion.originalOptions.get(
						Integer.valueOf(parentQuestion.selectedOption)));
		else if (parentQuestion.renderSelectCheckboxes == 'true')
			for (String opt : parentQuestion.selectedOptions)
				if (!String.isBlank(opt) && 
						parentQuestionAnswers.contains(
							parentQuestion.originalOptions.get(Integer.valueOf(opt))))
					return true;
		
		return false;
	}

	/**
	 * Clears any current answer.
	 */
	public void clearResponse() {
		selectedOption = '';
		selectedOptions.clear();
	}

	/**
	 * Parses and sets the proper response from the propoerty to the field
	 * @return The answers in English
	 */
	public String getResponse() {
		String result;

		// radio (row)
		if (renderSelectRow == 'true')
			result = String.isNotBlank(selectedOption) ?
				originalOptions.get(Integer.valueOf(selectedOption)) : null;
		// radio (row or column)
		else if (renderSelectRadio == 'true')
			result = String.isNotBlank(selectedOption) ?
				originalOptions.get(Integer.valueOf(selectedOption)) : null;
		// free text
		else if (renderFreeText == 'true')
			result = choices;
		// check boxes
		else if (renderSelectCheckboxes == 'true')
			for (String opt : selectedOptions)
				if (String.isNotBlank(opt))
					result = String.isBlank(result) ?
						originalOptions.get(Integer.valueOf(opt)) :
						result + '\n' + originalOptions.get(Integer.valueOf(opt));
		
		return result;
	}

	/** 
	 * Splits up the string as given by the user and adds each option
	 * to a list to be displayed as option on the Visualforce page
	 * param: str   String as submitted by the user
	 * returns the List of SelectOption for the visualforce page
	 */
	private List<SelectOption> stringToSelectOptions(String str) {
		List<String> strList;
		Integer i;
		String trimmed;
		List<SelectOption> result = new List<SelectOption>();

		if (!String.isBlank(str)) {
			strList = str.split('\\r?\\n');
			i = 0;
			for (String s : strList) {
				trimmed = s.trim();
				if (!String.isBlank(trimmed))
					result.add(new SelectOption(String.valueOf(i++), trimmed));
			}
		}

		return result;
	}

	/** 
	 * Splits up the string as given ignoring empty lines
	 * param: str   String as submitted by the user
	 * returns the List of SelectOption for the visualforce page
	 */
	private List<String> stringToList(String str) {
		List<String> strList, result = new List<String>();
		Integer i;
		String trimmed;

		if (!String.isBlank(str)) {
			strList = str.split('\\r?\\n');
			i = 0;
			for (String s : strList) {
				trimmed = s.trim();
				if (!String.isBlank(trimmed))
					result.add(trimmed);
			}
		}

		return result;
	}

	/** 
	 * Turns the string into an array, one ite per line.
	 * @param str
	 */
	private Set<String> stringToSet(String str){
		if (String.isBlank(str))
			return null;

		return new Set<String>(str.split('\\r?\\n'));
	}
}