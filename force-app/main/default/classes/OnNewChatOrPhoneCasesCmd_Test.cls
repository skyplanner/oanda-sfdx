/**
 * @File Name          : OnNewChatOrPhoneCasesCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 6/5/2024, 11:53:51 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/13/2022, 5:12:23 PM   acantero     Initial Version
**/
@IsTest
private without sharing class OnNewChatOrPhoneCasesCmd_Test {

    @IsTest
    static void checkRecordChatCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_CHAT,
            OmnichanelConst.YES
        );
        Test.startTest();
        OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
        instance.checkRecord(caseObj);
        Test.stopTest();
        System.assertEquals(true, caseObj.Spot_Crypto__c, 'Invalid value');
    }

    @IsTest
    static void checkRecordChatbotCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE
        );
        Test.startTest();
        OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
        instance.checkRecord(caseObj);
        Test.stopTest();
        System.assertEquals(false, caseObj.Spot_Crypto__c, 'Invalid value');
    }

    @IsTest
    static void checkRecordPhoneCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_PHONE
        );
        Test.startTest();
        OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
        instance.checkRecord(caseObj);
        Test.stopTest();
        System.assertEquals(false, caseObj.Spot_Crypto__c, 'Invalid value');
    }

    @IsTest
    static void executeChatbotCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE
        );
        Boolean throwError = false;
        Id entitlementId = getEntitlementId(EntitlementSettings.CHAT_CASE);
        Test.startTest();
        try {
            OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
            instance.checkRecord(caseObj);
            instance.execute();
        } catch (Exception ex) {
            throwError = true;
        }
        Test.stopTest();
        
        if(entitlementId == null){

            System.assertEquals(true, throwError, 'Error must be thrown. Entitlement not found'); 
        }
        else{
            System.assertEquals(entitlementId, caseObj.EntitlementId, 'Same Entitlement');
        }
    }

    @IsTest
    static void executeChatCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_CHAT,
            OmnichanelConst.YES
        );
        Boolean throwError = false;
        Id entitlementId = getEntitlementId(EntitlementSettings.CHAT_CASE);
        Test.startTest();
        try {
            OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
            instance.checkRecord(caseObj);
            instance.execute();
        } catch (Exception ex) {
            throwError = true;
        }
        Test.stopTest();
        if(entitlementId == null){

            System.assertEquals(true, throwError, 'Error must be thrown. Entitlement not found'); 
        }
        else{
            System.assertEquals(entitlementId, caseObj.EntitlementId, 'Same Entitlement');
        }
    }

    @IsTest
    static void executePhoneCase() {
        Case caseObj = getCase(
            OmnichanelConst.CASE_ORIGIN_PHONE,
            OmnichanelConst.YES
        );
        Boolean throwError = false;
        Id entitlementId = getEntitlementId(EntitlementSettings.PHONE_CASE);
        Test.startTest();
        try {
            OnNewChatOrPhoneCasesCmd instance = new OnNewChatOrPhoneCasesCmd();
            instance.checkRecord(caseObj);
            instance.execute();
        } catch (Exception ex) {
            throwError = true;
        }
        Test.stopTest();
        if(entitlementId == null){

            System.assertEquals(true, throwError, 'Error must be thrown. Entitlement not found'); 
        }
        else{
            System.assertEquals(entitlementId, caseObj.EntitlementId, 'Same Entitlement');
        }
    }


    private static Case getCase(String origin, String chatSpotCrypto) {
        return new Case(
            Origin = origin,
            Chat_Spot_Crypto__c = chatSpotCrypto,
            Chat_Email__c = 'email@test.com'
        );
    }

    private static Case getCase(String origin) {
       return getCase(origin, null);
    }

    private static Id getEntitlementId(String settingName) {
        Id entitlementId ;
        try {
            entitlementId = EntitlementSettings.getEntitlementId(settingName, true);
        } catch (Exception ex) {
            
        }
        return entitlementId;
    }

    
}