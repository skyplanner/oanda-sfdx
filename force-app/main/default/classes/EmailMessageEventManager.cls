/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-22-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class EmailMessageEventManager {
	public static List<Database.SaveResult> publishEvents(
		List<EmailMessage> messages
	) {
		List<Database.SaveResult> saveResultList = new List<Database.SaveResult>();
		List<Email_Message__e> toPublishList = new List<Email_Message__e>();
		for (EmailMessage message : messages) {
			toPublishList.add(createEvent(message));
		}
		if (!messages.isEmpty()) {
			saveResultList = EventBus.publish(toPublishList);
		}
		return saveResultList;
	}
	private static Email_Message__e createEvent(EmailMessage message) {
		Email_Message__e messageEvent = new Email_Message__e(
			Parent_Id__c = message.ParentId
		);
		return messageEvent;
	}
}