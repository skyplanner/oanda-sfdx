/**
 * @description       : 
 * @author            : Eugene Jung
 * @group             : 
 * @last modified on  : 11-10-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class TaxDeclarationTriggerHandler {

    public static Id supportCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Support').getRecordTypeId();
    private static final String IHS_CASE_SUBJECT_SUBTYPE = 'IHS confirmation request';

	List<TAX_Declarations__c> newList;
    Map<Id, TAX_Declarations__c> oldMap;

	/**
     * Constructor
     */
    public TaxDeclarationTriggerHandler(
        List<TAX_Declarations__c> newList)
    {
        this.newList = newList;
    }

    /**
     * Constructor
     */
    public TaxDeclarationTriggerHandler(
        List<TAX_Declarations__c> newList,
        Map<Id, TAX_Declarations__c> oldMap)
    {
        this.newList = newList;
        this.oldMap = oldMap;
    }

	/**
	 * Handles: before Insert
	 */
	public void handleBeforeInsert() {
		// ...
	}

	/**
	 * Handles: after Insert
	 */
	public void handleAfterInsert() {
        validateIHSConfirmationRequest(newList);
	}

	/**
	 * Handles: before Update
	 */
	public void handleBeforeUpdate() {
		// ...
	}

	/**
	 * Handles: after Update
	 */
	public void handleAfterUpdate() {
        validateIHSConfirmationRequest(newList);
	}

	/**
	 * Handles: before Delete
	 */
	public void handleBeforeDelete() {
		// ...
	}

	/**
	 * Handles: after Delete
	 */
	public void handleAfterDelete() {
		// ...
	}

	/**
	 * Handles: after Undelete
	 */
	public void handleAfterUndelete() {
		// ...
	}

    /* public void validateIHSConfirmationRequest(List<TAX_Declarations__c> tds)
    {
        validateIHSConfirmationRequest(tds, null);
    } */

    public void validateIHSConfirmationRequest(List<TAX_Declarations__c> tds)
    {
        Map<Id, TAX_Declarations__c> fxaIdTdMap = new Map<Id, TAX_Declarations__c>();
        Map<fxAccount__c, TAX_Declarations__c> fxaTdMap = new Map<fxAccount__c, TAX_Declarations__c>();

        for(TAX_Declarations__c td : tds) 
        {
            TAX_Declarations__c oldTD = null;
            if(oldMap !=null) {
                oldTD = oldMap.get(td.Id);
            }

            if(td.Is_In_Queue__c)
            {
                if(oldTD == null || td.Is_In_Queue__c !=oldTD.Is_In_Queue__c)
                {
                    fxaIdTdMap.put(td.fxAccount__c, td);
                }
            }
        }
        
        if(fxaIdTdMap.isEmpty()){
            return;
        }

        List<fxAccount__c> fxAccs = new List<fxAccount__c>(
            [SELECT Id, 
                    Lead__c, 
                    Account__c, 
                    Contact__c 
                FROM fxAccount__c 
                WHERE Id IN :fxaIdTdMap.keySet()]);

        for(fxAccount__c fxa : fxAccs)
        {
            fxaTdMap.put(fxa, fxaIdTdMap.get(fxa.Id));
        }

        List<Case> caseToBeInserted = new List<Case>();
        for(fxAccount__c fxa : fxaTDMap.keySet()) 
        {
            String caseDescription = getIHSConfirmRequestCaseDescription(fxaTdMap.get(fxa));
            caseToBeInserted.add(createCase(fxa, caseDescription));
        }
        
        if(!caseToBeInserted.isEmpty())
        {
            DataBase.SaveResult[] saveResults =
            Database.insert(
                caseToBeInserted,
                false);
        }
    }

    private Case createCase(fxAccount__c fxa, String caseDescription)
    {
        return
            new Case(
                Subject = IHS_CASE_SUBJECT_SUBTYPE,
                Description = caseDescription,
                Inquiry_Nature__c = Constants.CASE_INQUIRY_NATURE_ACCOUNT,
                Type__c = Constants.CASE_TYPE_TAX_DECLARATION,
                Subtype__c = IHS_CASE_SUBJECT_SUBTYPE,
                Live_or_Practice__c = Constants.CASE_LIVE,
                Origin = Constants.CASE_ORIGIN_INTERNAL,
                OwnerId = UserUtil.getQueueId('On Boarding Cases'),
                RecordTypeId = supportCaseRecordTypeId,
                fxAccount__c = fxa.Id,
                Lead__c = fxa.Lead__c,
                AccountId = fxa.Account__c,
                ContactId = fxa.Contact__c
            );
    }

    private String getIHSConfirmRequestCaseDescription(TAX_Declarations__c td)
    {
         String caseDescription = System.Label.IHS_Confirmation_Case_Description + '\n' + '\n';
         caseDescription += 'Form Validation Result Code: ' + td.Form_Validation_Result_Code__c + '\n';
         caseDescription += 'Queue Code: ' + td.Queue_Code__c + '\n';
         caseDescription += 'Rejection Reason Code: ' + td.Rejection_Reason_Code__c + '\n';
         caseDescription += 'Rejection Reason Type: ' + td.Rejection_Reason_Type__c + '\n';
         caseDescription += 'Account Review Status: ' + td.Account_Review_Status__c + '\n';
         caseDescription += 'Validation Comments: ' + td.Validation_Comments__c + '\n';

         return caseDescription;
    }
}