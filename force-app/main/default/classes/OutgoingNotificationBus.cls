/**
 * @description       :
 * @author            : OANDA
 * @group             :
 * @last modified on  : 2 July 2024
 * @last modified by  : Ryta Yahavets
**/
public virtual class OutgoingNotificationBus {

    //Maximum CPU in a transaction - 10sec synch (60 asynch) CPU Time doesn’t include time spent on waiting for responses from callouts.
    //Maximum cumulative timeout for all callouts in a transaction - 120 sec synch/asynch

    //Variables are used to collect new events and information (errors) about a record that cannot be processed.
    private static Map<String, Set<String>> addEventsErrors = new Map<String, Set<String>>();
    @TestVisible
    private List<Outgoing_Notification__e> outgoingNotifications = new List<Outgoing_Notification__e>();

    private static Map<String, OutgoingNotificationBus> classInstanceByClassName = new Map<String, OutgoingNotificationBus>();

    /********************************* Constants *****************************************************/

    public final static String OUTGOING_NOTIFICATION_CATEGORY = 'Outgoing Notification';
    public final static String OUTGOING_NOTIFICATION_ERR_MSG = 'Required fields for the requests are missing: ';

    /******************************** Constructors ***********************************************/

    public OutgoingNotificationBus() {
        outgoingNotifications = new List<Outgoing_Notification__e>();
    }

    /******************************** Virtual Methods ***********************************************/

    public virtual String getEventBody(SObject newRecord, SObject oldRecord) {
        return null;
    }

    public virtual String getEventKey(SObject newRecord) {
        return null;
    }

    public virtual void sendRequest(String key, String body) {
    }

    public virtual String getRecordId() {
        return null;
    }

    /******************************** Helpers *****************************************************/

    /**
    * @param classType variable used to create(or get from the saved) an instance of class
    * @param newRecord new trigger record value
    * @param oldRecord old trigger record value
    * @description method for creating new Outgoing_Notification__e records based on overridden methods in inheritor classes
    * The EventName should have the same value as class, based on this value will executed correct request in queueable class.
    */
    public void addEventRecord(Type classType, SObject newRecord, SObject oldRecord) {
        OutgoingNotificationBus outgoingNotificationBus = getInstance(classType);
        String body = outgoingNotificationBus.getEventBody(newRecord, oldRecord);
        if (String.isNotBlank(body)) {
            outgoingNotifications.add(new Outgoing_Notification__e(
                    EventName__c = classType.getName(),
                    Key__c = outgoingNotificationBus.getEventKey(newRecord),
                    Body__c = body
            ));
        }
    }

    /**
    * @param recordId id of record (ex.: fxAccount__c.Id)
    * @param message error message
    * @description provide possible errors with records that for some reason cannot be updated on the third-party resource
    */
    @TestVisible
    protected void addEventsError(String recordId, String message) {
        if (addEventsErrors.containsKey(recordId)) {
            addEventsErrors.get(recordId).add(message);
        } else {
            addEventsErrors.put(recordId, new Set<String>{message});
        }
    }


    public void publishPlatformEvents() {
        List<String> errMsgs = new List<String>();
        if (!outgoingNotifications.isEmpty()){
            List<Database.SaveResult> saveResultList = EventBus.publish(outgoingNotifications);
            for (Database.SaveResult res : saveResultList) {
                if (!res.isSuccess()) {
                    String errMsg = 'Errors returned: ';
                    for (Database.Error error : res.getErrors()) {
                        errMsg += error.getStatusCode() + ' - ' + error.getMessage() + ' ';
                    }
                    errMsgs.add(errMsg);
                }
            }
            if(!errMsgs.isEmpty()) {
                Logger.error(
                        'Events from trigger failed',
                        OUTGOING_NOTIFICATION_CATEGORY,
                        errMsgs.toString()
                );
            }
        }

        if (!addEventsErrors.isEmpty()) {
            Logger.error(
                    OUTGOING_NOTIFICATION_ERR_MSG,
                    OUTGOING_NOTIFICATION_CATEGORY,
                    addEventsErrors.toString()
            );
            addEventsErrors = new Map<String, Set<String>>();
        }
    }

    /******************************** Private Helpers *****************************************************/

    /**
    * @param className extended class name
    * @description get class instance by class name
    */
    public OutgoingNotificationBus getInstance(String className) {
        OutgoingNotificationBus outgoingNotificationBus;
        if (classInstanceByClassName.containsKey(className)) {
            outgoingNotificationBus = (OutgoingNotificationBus) classInstanceByClassName.get(className);
        } else {
            Type classType = Type.forName(className);
            Object classInstance = classType.newInstance();
            outgoingNotificationBus = (OutgoingNotificationBus) classInstance;
            classInstanceByClassName.put(className, outgoingNotificationBus);
        }

        return outgoingNotificationBus;
    }

    /**
    * @param classType extended class type
    * @description get class instance by class type
    */
    private OutgoingNotificationBus getInstance(Type classType) {
        OutgoingNotificationBus outgoingNotificationBus;
        if (classInstanceByClassName.containsKey(classType.getName())) {
            outgoingNotificationBus = (OutgoingNotificationBus) classInstanceByClassName.get(classType.getName());
        } else {
            Object classInstance = classType.newInstance();
            outgoingNotificationBus = (OutgoingNotificationBus) classInstance;
            classInstanceByClassName.put(classType.getName(), outgoingNotificationBus);
        }

        return outgoingNotificationBus;
    }

    /******************************** Static Helpers ***********************************************/

    public static void runJob(List<Outgoing_Notification__e> outgoingNotifications) {
        try {
            OutgoingNotificationQueueable uPEqueable = new OutgoingNotificationQueueable(outgoingNotifications);
            System.enqueueJob(uPEqueable);
        } catch(Exception ex) {
            Logger.error('UPEventBus', 'Sth went wrong', ex.getMessage() );
        }
    }
}