/**
 * @File Name          : ExpressionValidator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/20/2020, 11:37:19 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/20/2020   acantero     Initial Version
**/
public interface ExpressionValidator {
    
    String validate(String val);

}