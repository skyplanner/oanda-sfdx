/**
 * @File Name          : HelpPortalHomeCtrl.cls
 * @Description        : 
 * @Author             : Fernando Gomez
 * @Last Modified By   : dmorales
 * @Last Modified On   : 5/1/2020, 2:12:23 PM
 * @Modification Log   : 
 * Ver		Date			Author		Modification
 * 1.0		3/18/2019		fgomez		Initial Version
 * 1.2		??/??/???		dmorales	
 * Modified By @Sahil Handa for SP-8310
 **/
global class HelpPortalHomeCtrl extends HelpPortalBase {
	// global transient List<SelectOption> languages { get; private set; }
	global transient List<FAQ> mostVisited { get; private set; }
	global transient List<DataCategoryWrapper> topCategories { get; private set; }

	global transient String tradingHoursHtml { get; private set; }
	global transient String holidayNoticesHtml { get; private set; }
	
	global String countryName {get;set;}
	global String divisionDataCategory {get;set;}
	global String countryCode {get;set;}
	global String regionCode {get;set;}
	global String divisionLabel {get;set;}

	global Boolean countrySel {get;set;}
	global Boolean isCountryAllowed {get;set;}
	global Boolean isCountryTMS {get;set;}

	/**
	 * Main constructor
	 */
	global HelpPortalHomeCtrl() {
		super();
		
		HelpPortalRichText tradingHours, holidayNotices;
		
		//mostVisited = getTopFaqs();
	//	languages = getLanguageOptions();
		tradingHours = new HelpPortalRichText(language, 'trading_hours',this.region);
		holidayNotices = new HelpPortalRichText(language, 'holiday_notices',this.region);
		tradingHoursHtml = tradingHours.getHtml();
		holidayNoticesHtml = holidayNotices.getHtml();
		
	}

	/**
	 * Check if the cookie that stores the country selected by the user is present
	 */
	public PageReference checkCountry() {
		PageReference pr;

		countrySel = false;
		Cookie country = ApexPages.currentPage().getCookies().get('prefered_country');
		/*Cookie regionCookie = ApexPages.currentPage().getCookies().get('prefered_region');
		System.debug('RegionCookie ' + regionCookie);*/
		System.debug('Region Base Cookie' + region);
		countryName = '';
		divisionDataCategory = '';
		countryCode = '';
		regionCode = '';
		divisionLabel = '';
			
		if (country != null) {
			String countryCodeCookie = country.getValue();
			Country_Code__mdt countryMetadata =
				CountryCodeMetadataUtil.getCountryByCode(countryCodeCookie);
			if (countryMetadata == null) {	
				String errorMsg =
					'Country_Code__mdt not found for countryCode: ' +
					countryCodeCookie;
				System.debug(logginglevel.ERROR, errorMsg);
				throw new HelpPortalException(errorMsg);
			}		
			//else...
			countrySel = true;
			countryName = String.isNotEmpty(countryMetadata.Alternative_Name__c) ?
				countryMetadata.Alternative_Name__c :
				countryMetadata.Country__c;

			countryCode = countryCodeCookie;
			regionCode = (region != '') ? region : countryMetadata.Region__c;
			System.debug('regionCode ' + regionCode);

			// Fernando Gomez, SP-9089
			// the country might be cannot be phobitedto create new accounts...
			isCountryAllowed = countryMetadata.Is_Prohibited__c == null ||
				!countryMetadata.Is_Prohibited__c;

			isCountryTMS = countryMetadata.Is_TMS__c;

			if (isCountryAllowed) {
				//Mapping between Region (OME, OLE, OAP...)
				// to Division Data Category (OANDA_Europe, OANDA_US, ...)	
				DivisionSettings divisionSett = DivisionSettings.getInstance(regionCode);	

				if(!divisionSett.initializationFails) {
					divisionDataCategory  = divisionSett.divisionDataCategory;
					divisionLabel = divisionSett.divisionLabel;
				}
			
				if (divisionSett.initializationFails && regionCode != countryMetadata.Region__c){
						//if regionCode is from Cookie and the value does not exist in DivisionMetadataType..
						//then try with actual country region code and update cookie region
						System.debug('countrymetadata.Region__c ' + countryMetadata.Region__c);
						divisionSett = new DivisionSettings(countryMetadata.Region__c);	
						System.debug('divisionSett ' + divisionSett);
						if(!divisionSett.initializationFails){
						System.debug('divisionDataCategory ' + divisionSett.divisionDataCategory);
							divisionDataCategory  = divisionSett.divisionDataCategory;
							divisionLabel = divisionSett.divisionLabel;  
							//update cookie
							Cookie cookieR = new Cookie('prefered_region', countryMetadata.Region__c ,null,611040000,false);	
							ApexPages.currentPage().setCookies(new Cookie[]{cookieR});
						}
				}
				
				topCategories = DataCategoryTopArticlesHelper.getFaqCategories(divisionDataCategory, language);  
				mostVisited = getTopFaqsByDivision(divisionDataCategory);
			} 
			// if country is not allowed, we remove the country cookie
			// so users can have a fresh start when refreshing or going back
			else {
				ApexPages.currentPage().setCookies(new List<Cookie> {
					new Cookie('prefered_region', null, null, 0,false),
					new Cookie('prefered_country', null, null, 0, false),
					new Cookie('prefered_language', null, null, 0, false)
				});
			}
		}

		return null; 
	}

	/**
	 * Searches a secified text agains all articles
	 * @param language
	 * @param searchText
	 * @return the list a matching FAQs
	 */
	@RemoteAction
	global static List<FAQ> search(String language,
			String searchText, String division) {
		HelpPortalSearchCtrl c;
		// we use the already define controller for that
		c = new HelpPortalSearchCtrl(language, searchText, '0', '100', division );
		// and we return the results	
		return c.faqs; 
	}

	/**
	 * 
	 * @param articleId
	 * @param overallRating
	 * @param ranking
	 * @param comments
	 */
	@RemoteAction
	global static void submitRating(Id articleId,
			String overallRating, String ranking,
			String comments, String language) {
		HelpPortalArticleCtrl c;	
		// we use the already define controller for that		
		c = new HelpPortalArticleCtrl(articleId,language);		
		// we then save the rating and feedback
		c.submitRating(overallRating, ranking, comments);
	}

}