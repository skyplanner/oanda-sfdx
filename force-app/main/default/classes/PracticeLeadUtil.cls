public without sharing class PracticeLeadUtil {
    
    public static void loadPracticeLeads(List<Practice_Lead__c> pLeads){
    	
    	System.debug('Total Practice Leads to prcess: ' + pLeads.size());
    	
    	//sort by fxtrade user name
    	Map<String, Practice_Lead__c> practiceLeadByUserName = new Map<String, Practice_Lead__c>();
    	//List<Practice_Lead__c> existingLeads = new List<Practice_Lead__c>();
    	
    	for(Practice_Lead__c pl : pLeads){
    		pl.Processed__c = true;
    		
    		//reset error flag
    		pl.Error__c = false;
    		
    		practiceLeadByUserName.put(pl.User_Name__c, pl);
    	}
    	
    	//check if these user name already exists. If so, ignore
    	//This is because we don't do update on practice leads
    	List<fxAccount__c> practiceFxAccts = [select id, name from fxAccount__c where RecordTypeId = :RecordTypeUtil.getFxAccountPracticeId() and name in : practiceLeadByUserName.keySet()];
		
		for(fxAccount__c fxa : practiceFxAccts){
			
			/*
			//update the processed flag, so that we don't process it again
			Practice_Lead__c pLd = practiceLeadByUserName.get(fxa.name);
			
			if(pLd != null){
				existingLeads.add(pLd);
			}
			*/
			
			//now remove it from the set for inserting
			practiceLeadByUserName.remove(fxa.name);
			
			
		}
		
		/*
		if(existingLeads.size() > 0){
			System.debug('Practice Leads to ignore: ' + existingLeads.size());
			
			update existingLeads;
		}
		*/
		
		if(practiceLeadByUserName.size() == 0){
			//update processing status
			update pLeads;
			
			return;
		}
		
		System.debug('Practice Leads to insert: ' + practiceLeadByUserName.size());
		
		//insert practice lead to fxAccount__c and Lead
		List<Practice_Lead__c> pLds = practiceLeadByUserName.values();
		
		Savepoint sp = Database.setSavepoint();
		
		try{
			
			insertLeads(pLds);
	    	insertFxAccounts(pLds);
	    	
	    	//update pLds;
	    	
		}catch(Exception e){
			System.debug('******** exception :' + e);
			System.debug('******** roll back');
			Database.rollback(sp);
			
			for(Practice_Lead__c p : pLds){
				p.Error__c = true;
				//p.Processed__c = true;
			}
			
			//update pLds;
		}
		
		//update processing status
		update pLeads;
    }
    
    private static void insertFxAccounts(List<Practice_Lead__c> pLeads){
    	
    	List<fxAccount__c> fxAccountsToInsert = new List<fxAccount__c>();
    	
    	for(Practice_Lead__c pLd : pLeads){
    		fxAccount__c fxa = new fxAccount__c(RecordTypeId = RecordTypeUtil.getFxAccountPracticeId());
    		
    		fxa.Account_Email__c = pLd.Email__c;
    		fxa.Division_Name__c = pLd.Division_Name__c;
    		fxa.Funnel_Stage__c = pLd.Funnel_Stage__c;
    		fxa.Name = pLd.User_Name__c;
    		fxa.fxTrade_Created_Date__c = pLd.BI_Creation_DateTime__c;
    		
    		//always inserted as house account
    		fxa.ownerId = UserUtil.getSystemUserId();
    		
    		fxAccountsToInsert.add(fxa);
    		
    		//pLd.Processed__c = true;
    		pLd.Inserted__c = true;
    	}
    	
    	if(fxAccountsToInsert.size() > 0){
    		insert fxAccountsToInsert;
    	}
    	
    }
    
    private static void insertLeads(List<Practice_Lead__c> pLeads){
    	List<Lead> leadsToInsert = new List<Lead>();
    	
    	for(Practice_Lead__c pLd : pLeads){
    		Lead ld = new Lead(RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
    		
    		ld.CRM_Legacy_Id__c = pLd.Email__c;
    		ld.Country = pLd.Country__c;
    		ld.Division_Name__c = pLd.Division_Name__c;
    		ld.Email = pLd.Email__c;
    		ld.Language_Preference__c = pLd.Language__c;
    		ld.LastName = pLd.Customer_Name__c;
    		ld.LeadSource = pLd.Channel__c;
    		ld.Phone = pLd.Telephone__c;
    		ld.Territory__c = pLd.zone__c;
    		ld.Traded__c = pLd.Traded__c == 'Y' ? true : false;
    		ld.Username_Practice__c = pLd.User_Name__c;
    		
    		ld.ownerId = UserUtil.getSystemUserId();
    		
    		leadsToInsert.add(ld);
    		
    	}
    	
    	if(leadsToInsert.size() > 0){
    		insert leadsToInsert;
    	}
    }
    
    //Check if the lead identified by sfLeadId field still exists. It may be already merged with a live lead.
    public static void checkDemoLeadExists(List<Practice_Lead__c> pLeads){
    	Set<ID> leadIDs = new Set<ID>();
    	Map<Id, List<Practice_Lead__c>> pLeadsByLeadId = new Map<Id, List<Practice_Lead__c>>();
  
    	System.debug('Practice Leads to check: ' + pLeads.size());
    	
    	for(Practice_Lead__c pLd : pLeads){
    		
    		//this record has been processed, so skip it
    		if(pLd.Processed__c){
    			continue;
    		}
    		
    		//set this record to be processed
    		pLd.Processed__c = true;
    		
    		//no sfLeadId to check, so skip to next one
    		if(pLd.sfLeadId__c == null){
    			continue;
    		}
    		
    		leadIDs.add(pLd.sfLeadId__c);
    		
    		List<Practice_Lead__c> pLdList = pLeadsByLeadId.get(pLd.sfLeadId__c);
    		if(pLdList == null){
    			pLdList = new List<Practice_Lead__c>();
    		}
    		
    		pLdList.add(pLd);
    		pLeadsByLeadId.put(pLd.sfLeadId__c, pLdList);
    	}
    	
    	if(leadIDs.size() == 0){
    		System.debug('No sfLeadId to check');
    		return;
    	}
    	
    	Map<Id, Lead> leadMap = new Map<Id, Lead>([select id, recordTypeId from Lead where Id in : leadIDs and isConverted = false and isDeleted = false]);
    	if(leadMap == null || leadMap.size() == 0){
    		System.debug('No Existing Leads Anymore!');
    		return;
    	}
    	
    	//set the existing lead flag
    	for(ID ldId : leadMap.keySet()){
    		List<Practice_Lead__c> pLdList = pLeadsByLeadId.get(ldId);
    		
    		if(pLdList != null){
    			for(Practice_Lead__c pLd : pLdList){
    				pLd.Lead_Exists__c = true;
    			}
    		}
    	}
    	
    }
    
}