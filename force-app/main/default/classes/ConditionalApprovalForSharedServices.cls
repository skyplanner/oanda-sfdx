/**
 * @description       : Class to asynchronously call Shared Services (SP-12308)
 * @author            : Agnieszka Kajda
 * @group             : 
 * @last modified on  : 08-01-2023
 * @last modified by  : Agnieszka Kajda
**/
public with sharing class ConditionalApprovalForSharedServices {
    @future(callout=true)
    public static void setClosedAttr(List<Id> ids, Boolean isClosed) {
        List<fxAccount__c> fxAccounts = [SELECT Id, RecordTypeId, Email__c FROM fxAccount__c WHERE id IN :ids];
        String payload = '{"closed": '+isClosed+'}';
        String baseUrl = '';

        APISettings apiSettings;
        NamedCredentialsUtil.NamedCredentialData namedCredentialData;
        //The namedCredentialData should replace appSettings after fully implementation
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            namedCredentialData = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
            baseUrl = namedCredentialData.credentialUrl;
        } else {
            apiSettings = APISettingsUtil.getApiSettingsByName(Constants.TAS_API_WRAPPER_SETTING_NAME);
            baseUrl = apiSettings.baseUrl;
        }
        String oneId ='';
        String urlFormat = SPGeneralSettings.getInstance().getValue(Constants.TAS_API_MT5_ACCOUNT_CLOSE_ONLY_ENDPOINT);
        for(fxAccount__c fxAcc: fxAccounts){
            try{            
                CryptoAccountParams params = new CryptoAccountParams();
                params.fxaccountIds = new List<Id>();
                CryptoAccountActionsCallout caac = new CryptoAccountActionsCallout(params);        
                oneId = caac.getOneIdByEmailNoActions(fxAcc.Email__c);

                String env = fxAccountUtil.isLiveAccount(fxAcc) ? 'live' : 'demo';
                baseUrl+= String.format(urlFormat, new List<String> {oneId, env});
                HttpRequest req = new HttpRequest();
                req.setEndpoint(baseUrl);
                req.setMethod('PATCH');
                req.setHeader('Content-Type', 'application/json');
                if (apiSettings != null) {
                    req.setHeader('client_id', apiSettings.clientId);
                    req.setHeader('client_secret', apiSettings.clientSecret);
                }

                req.setTimeout(120000);
                req.setBody(payload);
                
                Http http = new Http();
                HttpResponse response = http.send(req);
            
                if(response.getStatusCode() != 200){
                    System.debug(response.getStatus());
                    Logger.error('ConditionalApprovalForSharedServices error: '+ response.getStatus(), Constants.LOGGER_SHARED_SERVICES_CATEGORY, JSON.serialize(ids));
                }
            }catch(Exception e){
                System.debug(e.getMessage());
                Logger.error('ConditionalApprovalForSharedServices error: '+ e.getMessage(), Constants.LOGGER_GENERAL_CATEGORY, JSON.serialize(ids));
            }
        }


    }
}