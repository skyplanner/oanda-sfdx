/**
 * @File Name          : CategoriesTopArticlesCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/4/2019, 3:33:36 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   acantero     Initial Version
**/
@isTest
private class CategoriesTopArticlesCtrl_Test {

	static final String TEST_DIVISION_1 = 'TestDivision1';
	static final String TEST_CATEGORY_1 = 'TestCategory1';
	static final String TEST_CATEGORY_2 = 'TestCategory2';
	static final String TEST_CATEGORY_3 = 'TestCategory3';

	@testSetup
	static void setup () {
	    ArticlesTestDataFactory.createTopArticles(TEST_DIVISION_1, TEST_CATEGORY_1, TEST_CATEGORY_2, TEST_CATEGORY_3);
	}

	@isTest
	static void getFaqCategories_test() {
		Test.startTest();
		List<DataCategoryWrapper> result = CategoriesTopArticlesCtrl.getFaqCategories(TEST_DIVISION_1);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategoriesInfo_test() {
		Test.startTest();
		CategoriesTopArticlesCtrl.FaqCategoriesInfo result = CategoriesTopArticlesCtrl.getFaqCategoriesInfo();
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void deleteArticle_test() {
		Category_Top_Article__c topArt = [
			select 
				Id 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			and
				Order__c = 1
		];
		String category = TEST_CATEGORY_1;
		String topArtId = topArt.Id;
		Test.startTest();
		CategoriesTopArticlesCtrl.deleteArticle(category, TEST_DIVISION_1, topArtId);
		Test.stopTest();
		List<Category_Top_Article__c> artList = [select Id from Category_Top_Article__c where Id = :topArtId];
		System.assert(artList.isEmpty());
		List<Category_Top_Article__c> othersArtList = [
			select 
				Order__c
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1
			order by
				Order__c
		];
		Integer order = 0;
		for(Category_Top_Article__c otherArt : othersArtList) {
			//System.debug('otherArt.Order__c: ' + otherArt.Order__c);
			System.assertEquals(++order, otherArt.Order__c);
		}
	}

	//no belowArticles
	@isTest
	static void deleteArticle_test2() {
		Category_Top_Article__c topArt = [
			select 
				Id 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			order by
				Order__c desc
			limit 1
		];
		String category = TEST_CATEGORY_1;
		String topArtId = topArt.Id;
		Test.startTest();
		CategoriesTopArticlesCtrl.deleteArticle(category, TEST_DIVISION_1, topArtId);
		Test.stopTest();
		List<Category_Top_Article__c> artList = [select Id from Category_Top_Article__c where Id = :topArtId];
		System.assert(artList.isEmpty());
	}

	@isTest
	static void deleteArticle_test3() {
		String category = null;
		String division = null;
		String topArtId = null;
		Test.startTest();
		CategoriesTopArticlesCtrl.deleteArticle(category, division, topArtId);
		Test.stopTest();
	}

	@isTest
	static void changeArticlesOrder_test() {
		List<Category_Top_Article__c> topArtList = [
			select 
				Id 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			and
				(Order__c = 1 or Order__c = 2)
			order by
				Order__c
		];
		String topArt1Id = topArtList[0].Id;
		String topArt2Id = topArtList[1].Id;
		Test.startTest();
		Boolean result = CategoriesTopArticlesCtrl.changeArticlesOrder(topArt1Id, topArt2Id);
		Test.stopTest();
		System.assertEquals(true, result);
		List<Category_Top_Article__c> topArtList2 = [
			select 
				Id 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			and
				(Order__c = 1 or Order__c = 2)
			order by
				Order__c
		];
		System.assertEquals(topArt2Id, topArtList2[0].Id);
		System.assertEquals(topArt1Id, topArtList2[1].Id);
	}

	@isTest
	static void changeArticlesOrder_test2() {
		String topArt1Id = null;
		String topArt2Id = null;
		Test.startTest();
		Boolean result = CategoriesTopArticlesCtrl.changeArticlesOrder(topArt1Id, topArt2Id);
		Test.stopTest();
		System.assertEquals(false, result);
	}

	@isTest
	static void changeArticlesOrder_test3() {
		Category_Top_Article__c topArt = [
			select 
				Id 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			and
				Order__c = 1
		];
		String topArt1Id = topArt.Id;
		String topArt2Id = topArt.Id;
		Test.startTest();
		Boolean result = CategoriesTopArticlesCtrl.changeArticlesOrder(topArt1Id, topArt2Id);
		Test.stopTest();
		System.assertEquals(false, result);
	}


	@isTest
	static void addArticles_test() {
		FAQ__kav art99 = ArticlesTestDataFactory.createSampleArt('Test Article 99', 'Test-article-99');
		List<Category_Top_Article__c> topArtList = [
			select 
				Order__c 
			from 
				Category_Top_Article__c 
			where 
				DivisionName__c = :TEST_DIVISION_1
			and
				DataCategoryName__c = :TEST_CATEGORY_1 
			order by
				Order__c desc
		];
		Integer lastOrder = topArtList[0].Order__c.intValue();
		String category = TEST_CATEGORY_1;
		CategoriesTopArticlesCtrl.NewArticleWrapper newTopArtW = 
			new CategoriesTopArticlesCtrl.NewArticleWrapper();
		newTopArtW.knoledgeArticleId = art99.KnowledgeArticleId;
		newTopArtW.order = lastOrder + 1;
		List<CategoriesTopArticlesCtrl.NewArticleWrapper> newArticles = 
			new List<CategoriesTopArticlesCtrl.NewArticleWrapper> {
				newTopArtW
		};
		Test.startTest();
		List<String> result = CategoriesTopArticlesCtrl.addArticles(TEST_CATEGORY_1, TEST_DIVISION_1, newArticles);
		Test.stopTest();
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		Category_Top_Article__c insertedArt = [
			select 
				Order__c
			from 
				Category_Top_Article__c 
			where 
				Id = :result[0]
		];
		System.assertEquals(lastOrder + 1, insertedArt.Order__c);
	}

	@isTest
	static void addArticles_test2() {
		Test.startTest();
		List<String> result = CategoriesTopArticlesCtrl.addArticles(TEST_CATEGORY_1, TEST_DIVISION_1, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

}