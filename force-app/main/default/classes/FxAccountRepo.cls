/**
 * @File Name          : FxAccountRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 3:24:03 AM
**/
public inherited sharing class FxAccountRepo {

	public static fxAccount__c getAccountInfoByName(String username) {
		List<fxAccount__c> recList = [
			SELECT 
				Name,
				Account__c,
				Account__r.PersonEmail
			FROM fxAccount__c
			WHERE Name = :username
			AND Account__c <> null
			LIMIT 1
		];
		fxAccount__c result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static fxAccount__c getSummaryByAccount(ID accountId) {
		List<fxAccount__c> recList = [
			SELECT Name
			FROM fxAccount__c
			WHERE Account__c = :accountId
			LIMIT 1
		];
		fxAccount__c result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static fxAccount__c getAuthenticationInfoById(ID recordId) {
		List<fxAccount__c> recList = [
			SELECT
				Account__c,
				Authentication_Code__c,
				Authentication_Code_Expires_On__c
			FROM fxAccount__c
			WHERE Id = :recordId
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'fxAccount__c',
			'Id = ' + recordId
		);
		return recList[0];
	}

}