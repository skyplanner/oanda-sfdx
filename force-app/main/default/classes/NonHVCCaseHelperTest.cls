/**
 * @File Name          : NonHVCCaseHelperTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/6/2024, 4:49:40 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/6/2024, 4:49:40 PM   aniubo     Initial Version
**/
@isTest
private class NonHVCCaseHelperTest {
	@isTest
	private static void testGetById() {
		// Test data setup

		// Actual test
		Test.startTest();
		Set<ID> nonHvcCaseIdSet = new Set<ID>();
		List<Non_HVC_Case__c> nonHvcCaseList = NonHVCCaseHelper.getById(
			nonHvcCaseIdSet
		);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		nonHvcCaseIdSet.add(null);
		nonHvcCaseList = NonHVCCaseHelper.getById(nonHvcCaseIdSet);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		Test.stopTest();

		// Asserts
	}
    @isTest
	private static void testGetByCase() {
		// Test data setup

		// Actual test
		Test.startTest();
		Set<ID> nonHvcCaseIdSet = new Set<ID>();
		List<Non_HVC_Case__c> nonHvcCaseList = NonHVCCaseHelper.getByCase(
			nonHvcCaseIdSet
		);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		nonHvcCaseIdSet.add(null);
		nonHvcCaseList = NonHVCCaseHelper.getByCase(nonHvcCaseIdSet);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		Test.stopTest();

		// Asserts
	}
    @isTest
	private static void testGetCaseInfoById() {
		// Test data setup

		// Actual test
		Test.startTest();
		Set<ID> nonHvcCaseIdSet = new Set<ID>();
		List<Non_HVC_Case__c> nonHvcCaseList = NonHVCCaseHelper.getCaseInfoById(
			nonHvcCaseIdSet
		);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		nonHvcCaseIdSet.add(null);
		nonHvcCaseList = NonHVCCaseHelper.getCaseInfoById(nonHvcCaseIdSet);
		Assert.areEqual(
			true,
			nonHvcCaseList.isEmpty(),
			'Non HVC Case List is not empty'
		);
		Test.stopTest();

		// Asserts
	}
}