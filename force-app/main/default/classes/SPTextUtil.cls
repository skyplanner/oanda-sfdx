/**
 * @File Name          : SPTextUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : SkyPlanner - Dianelys Velazquez
 * @Last Modified On   : 09-26-2024
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   acantero     Initial Version
**/
public class SPTextUtil {

    public static final String END_OF_LINE_LONG_REGEX = '\\r\\n';
    public static final String END_OF_LINE_SHORT_REGEX = '\\n';
    public static final String END_OF_LINE_SHORT = '\n';

    public static String getValueOrEmpty(String strValue) {
        return (strValue != null)
            ? strValue
            : '';
    }

    public static String getStringValueOrEmpty(Object obj) {
        return (obj != null)
            ? String.valueOf(obj)
            : '';
    }
    
    public static List<String> parseMultilineText(String text) {
        if (String.isBlank(text)) {
            return new List<String>();
        }
        //else...
        String validText = text.replaceAll(END_OF_LINE_LONG_REGEX, END_OF_LINE_SHORT);
        List<String> result = validText.split(END_OF_LINE_SHORT_REGEX);
        return result;
    }

    public static String addValuesToText(
        String text, 
        List<String> values, 
        String firstItemPrefix, 
        String firstItemSuffix, 
        String othersItemPrefix,
        String othersItemSuffix
    ) {
        if (
            (values == null) ||
            values.isEmpty()
        ) {
            return text;
        }
        //else...
        if (text == null) {
            text = '';
        }
        for(Integer i = 0; i < values.size(); i++) {
            String prefix = (i > 0)
                ? othersItemPrefix
                : firstItemPrefix;
            String suffix = (i > 0)
                ? othersItemSuffix
                : firstItemSuffix;
            if (prefix != null) {
                text += prefix;
            }
            text += values[i];
            if (suffix != null) {
                text += suffix;
            }
        }
        return text;
    }

    public static String getJsonFromStaticResource(String staticResourceName) {
        StaticResource sr = [
            SELECT Body
            FROM StaticResource
            WHERE Name = :staticResourceName
        ];

        return sr.Body.toString();
    }
}