@IsTest
private class UpdateDemoFxAccountOneIdBatchTest {
    @TestSetup
    private static void testSetup() {
        TestDataFactory testDataFactory = new TestDataFactory();
        List<Lead> newLeads = testDataFactory.createDemoLeads(10);
        testDataFactory.createFXDemoTradeAccounts(newLeads);
    }

    @IsTest
    static void batchPositiveTest() {
        CalloutMock mock = new CalloutMock(200, '{"uuid": "uuid-1234","language": "en", "email": "email"}', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
            Database.executeBatch(new UpdateDemoFxAccountOneIdBatch(), 100);
        Test.stopTest();
        Assert.isTrue([SELECT Id FROM fxAccount__c WHERE fxTrade_One_Id__c != NULL].size() == 10);
    }

    @IsTest
    static void batchNegativeTest() {
        CalloutMock mock = new CalloutMock(400, '', 'Error', null);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
            Database.executeBatch(new UpdateDemoFxAccountOneIdBatch(), 100);
        Test.stopTest();
        Assert.isTrue([SELECT Id FROM fxAccount__c WHERE fxTrade_One_Id__c != NULL].size() == 0);
    }

    @IsTest
    static void batchNegativeTest2() {
        CalloutMock mock = new CalloutMock(404, '{"errorMessage" : "message"}', 'Error', null);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
            Database.executeBatch(new UpdateDemoFxAccountOneIdBatch(), 100);
        Test.stopTest();
        Assert.isTrue([SELECT Id FROM fxAccount__c WHERE fxTrade_One_Id__c != NULL].size() == 0);
    }

    @IsTest
    static void schedulePositiveTest() {
        CalloutMock mock = new CalloutMock(200, '{"uuid": "uuid-1234","language": "en", "email": "email"}', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
            UpdateDemoFxAccountOneIdBatch.schedule();
        Test.stopTest();
    }


}