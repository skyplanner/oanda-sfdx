/**
 * @File Name          : fxAccount_Trigger_Utility.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/21/2020   acantero     Initial Version
**/
public without sharing class fxAccount_Trigger_Utility {
    
    public static void beforeDelete(
        Map<ID,fxAccount__c> oldMap
    ) {
        
        FxAccountMIFIDValidationManager.removeResultValidationsRelated(
            oldMap.keySet()
        );
    }

    public static void mifidValidationProcess(Map<ID,fxAccount__c> newMap,   Map<ID,fxAccount__c> oldMap){
 
        if (!MiFIDExpValSettings.defaultRecIsActive()){
             return;             
        }

        //else
        List<Id> fxAccountPersonalInfoChange = new List<Id>();
        List<Id> fxAccountLastTradeDateChange = new List<Id>();
        Id fxRecordTypeId =   Schema.SObjectType.fxAccount__c
                              .getRecordTypeInfosByDeveloperName()
                              .get(MiFIDExpValConst.FXACCOUNT_REC_TYPE_LIVE)
                              .getRecordTypeId();
        for(Id fxAccountId : newMap.keySet()){
            fxAccount__c newObj = newMap.get(fxAccountId);
            fxAccount__c oldObj = oldMap.get(fxAccountId);                 
            IF(
               newObj.Account__c != null && 
               newObj.RecordTypeId == fxRecordTypeId && 
               newObj.Type__c == MiFIDExpValConst.FXACCOUNT_TYPE_INDIVIDUAL &&
               (
                newObj.Funnel_Stage__c == MiFIDExpValConst.FXACCOUNT_READY_FOR_FUNDING ||
                newObj.Funnel_Stage__c == MiFIDExpValConst.FXACCOUNT_FUNDED ||
                newObj.Funnel_Stage__c == MiFIDExpValConst.FXACCOUNT_TRADED
               ) &&
               FxAccountMIFIDHelper.checkDivision(newObj.Division_Name__c)
             ){
                 IF(newObj.Citizenship_Nationality__c != oldObj.Citizenship_Nationality__c ||
                    newObj.National_Personal_ID__c != oldObj.National_Personal_ID__c ||
                    newObj.Passport_Number__c != oldObj.Passport_Number__c ) {                       
                        fxAccountPersonalInfoChange.add(fxAccountId);
                    } 
                 ELSE IF(newObj.Last_Trade_Date__c != oldObj.Last_Trade_Date__c){                      
                       fxAccountLastTradeDateChange.add(fxAccountId);
                  }   
              }
        }

     
        if (!fxAccountPersonalInfoChange.isEmpty()) {
            FxAccountMIFIDValidationManager.validatefxAccounts(fxAccountPersonalInfoChange, true);
        }

        if (!fxAccountLastTradeDateChange.isEmpty()) {
            FxAccountMIFIDValidationManager.validatefxAccounts(fxAccountLastTradeDateChange, false);
        }
       
    }    
}