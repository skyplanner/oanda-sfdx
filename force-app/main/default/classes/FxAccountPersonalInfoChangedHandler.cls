/**
 * @File Name          : FxAccountPersonalInfoChangedHandler.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/26/2020, 9:20:44 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/22/2020   acantero     Initial Version
**/
public class FxAccountPersonalInfoChangedHandler {
    
    @InvocableMethod(label='fxAccount onPersonalInfoChanged' description='')
    public static void onPersonalInfoChanged(List<Id> fxAccountIdList) {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        if (valSettings.isActive) {
            FxAccountMIFIDValidationManager.validatefxAccounts(fxAccountIdList, true);
        }
    }

}