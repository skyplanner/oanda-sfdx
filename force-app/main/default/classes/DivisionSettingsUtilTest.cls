@isTest
public with sharing class DivisionSettingsUtilTest {

    @isTest 
    static void testGetDivisionSettingByName() {

        Division_Settings__mdt testDivSetting = new Division_Settings__mdt();
        testDivSetting.DeveloperName ='OGM';
        testDivSetting.Division_Name__c = 'Oanda Europe';
        testDivSetting.Enable_Shares_Recalculation__c = true;
        

        DivisionSettingsUtil dsu = DivisionSettingsUtil.getInstance();
        dsu.allDivisionSettings = new List<Division_Settings__mdt>{testDivSetting};
    
        Assert.areEqual(true, dsu.getDivisionSettingByDivisionName('Oanda Europe').Enable_Shares_Recalculation__c, 'The  true should be returned');
        Assert.areEqual(null, dsu.getDivisionSettingByDivisionName('test'), 'No mtd should be returned');
    }
}