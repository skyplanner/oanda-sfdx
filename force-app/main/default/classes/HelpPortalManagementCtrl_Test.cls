/**
 * @File Name          : HelpPortalManagementCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/24/2019, 1:32:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/24/2019   acantero     Initial Version
**/
@isTest
private class HelpPortalManagementCtrl_Test {
	
	@isTest
	static void getSupportedLanguages_test() {
		Test.startTest();
		List<Map<String, String>> result = HelpPortalManagementCtrl.getSupportedLanguages();
		Test.stopTest();
		System.assertNotEquals(null, result);
	}
	
}