/**
 * @File Name          : MessagingSLAViolationNotifier.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:10:38 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/27/2024, 5:12:12 PM   aniubo     Initial Version
 **/
public inherited sharing class MessagingSLAViolationNotifier extends ChatBaseSLAViolationNotifier {
	public MessagingSLAViolationNotifier(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		SLAViolationCanNotifiable violationNotificationChecker
	) {
		super(
			notificationObjectType,
			violationType,
			dataReader,
			violationNotificationChecker
		);
	}

	protected override SLAViolationInfo createViolationInfo(SObject record) {
		MessagingSession messagingObj = (MessagingSession) record;
		SLAMessagingViolationInfo result = new SLAMessagingViolationInfo(
			messagingObj.Id,
			messagingObj.Name,
			getViolationType()
		);
		result.isNotifiable =
			String.isNotBlank(messagingObj.Case?.Chat_Division__c) ||
			(messagingObj.MessagingEndUser?.Account != null &&
			String.isNotBlank(
				messagingObj.MessagingEndUser?.Account.Primary_Division_Name__c
			) ||
			messagingObj.Lead != null &&
			String.isNotBlank(messagingObj.Lead.Primary_Division_Name__c));

		result.isPersistent = true;
		result.division = getChatDivision(messagingObj);
		result.divisionCode = getChatDivisionCode(messagingObj);
		result.inquiryNature = messagingObj.Inquiry_Nature__c;
		result.Tier = messagingObj.Tier__c;
		result.language = String.isNotBlank(
				messagingObj.Case?.Chat_Language_Preference__c
			)
			? messagingObj.Case?.Chat_Language_Preference__c
			: messagingObj.Case?.Language_Preference__c;
		result.liveOrPractice = messagingObj.Live_or_Practice__c;
		result.notificationObjectType = SLAConst.SLANotificationObjectType.MESSAGING_NOT;
		//...
		result.accountName = getChatAccountName(messagingObj);
		result.ownerId = (messagingObj.Routing_Status__c ==
			SLAConst.MESSAGING_ROUTING_STATUS_ASSIGNED ||
			messagingObj.Routing_Status__c ==
			SLAConst.MESSAGING_ROUTING_STATUS_OPENED)
			? messagingObj.OwnerId
			: null;
		if (String.isNotBlank(result.ownerId)) {
			result.ownerName = getAgentName(result.ownerId);
		}
		result.sourceType = messagingObj.Lead != null ? 'Lead' : 'Account';

		return result;
	}

	protected override SLAConst.SLANotificationObjectType getSLANotificationObjectType() {
		return SLAConst.SLANotificationObjectType.MESSAGING_NOT;
	}
	protected override String getChatDivision(SObject obj) {
		MessagingSession messagingObj = (MessagingSession) obj;
		String result = messagingObj.Case?.Chat_Division__c;
		if (String.isBlank(result)) {
			result = getAccountDivisionName(
				messagingObj.MessagingEndUser?.Account
			);
		}
		if (
			String.isBlank(result) &&
			(messagingObj.Lead != null) &&
			String.isNotBlank(messagingObj.Lead.Primary_Division_Name__c)
		) {
			result = messagingObj.Lead.Primary_Division_Name__c;
		}
		return result;
	}

	protected override String getChatDivisionCode(SObject obj) {
		MessagingSession messagingObj = (MessagingSession) obj;
		String result = messagingObj.Case?.Chat_Division__c;
		String divisionName;
		if (String.isBlank(result)) {
			divisionName = getAccountDivisionName(
				messagingObj.MessagingEndUser?.Account
			);
			if (String.isBlank(divisionName)) {
				divisionName = getLeadDivisionName(messagingObj.Lead);
			}
		}
		return result;
	}

	@TestVisible
	private String getLeadDivisionName(Lead sessionLead) {
		String divisionName;
		String result;
		if (
			sessionLead != null &&
			String.isNotBlank(sessionLead.Primary_Division_Name__c)
		) {
			divisionName = sessionLead.Primary_Division_Name__c;
		}
		if (String.isNotBlank(divisionName)) {
			result = ServiceDivisionsManager.getInstance()
				.getDivisionCode(divisionName);
		}
		return result;
	}
	@TestVisible
	private String getAccountDivisionName(Account sessionAccount) {
		String divisionName;
		if (
			(sessionAccount != null) &&
			String.isNotBlank(sessionAccount.Primary_Division_Name__c)
		) {
			divisionName = sessionAccount.Primary_Division_Name__c;
		}
		return divisionName;
	}

	protected override String getChatAccountName(SObject obj) {
		MessagingSession messagingObj = (MessagingSession) obj;
		String result = (messagingObj.MessagingEndUser?.Account != null)
			? messagingObj.MessagingEndUser?.Account.Name
			: messagingObj.Lead != null
					? messagingObj.Lead.Name
					: Label.Unknown_Account_Name;
		return result;
	}
}