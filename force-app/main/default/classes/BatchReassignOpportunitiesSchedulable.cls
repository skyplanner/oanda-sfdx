/* Name: BatchReassignOpportunitiesSchedulable
 * Description : Batch Class on Opportunity for Before Insert and Before Update.
 * Author: OANDA
 * 
 * Update : @Sahil Handa - New job to run on opportunities with Current US/TO RM Distribution rules,Country = US or Canada,NW >= 625k,-Account type = Corporate,Potential HVC score is >=200,Owner is equal to System User,Created date is >= 5/11/2020
 */
public class BatchReassignOpportunitiesSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	public static final Integer BATCH_SIZE = 25;
  
	public static final String CRON_NAME = 'BatchReassignOpportunitiesSchedulable';
	
	public static final String CRON_SCHEDULE = '0 20 23 * * ?';

	private static DateTime ONE_DAY_AGO = DateTime.now().addDays(-1);
	private static DateTime TWO_DAYS_AGO = DateTime.now().addDays(-2);
	private static DateTime SEVEN_DAYS_AGO = DateTime.now().addDays(-7);
	private static DateTime SIX_MONTHS_AGO = DateTime.now().addMonths(-6);
	private static DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);

	
 	public static void schedule() {
    	System.schedule(CRON_NAME, CRON_SCHEDULE, new BatchReassignOpportunitiesSchedulable());
  	}

	public BatchReassignOpportunitiesSchedulable() {
		//Scheduled batches are found here: https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
    }

	public BatchReassignOpportunitiesSchedulable(String q) {
   		query = q;
  	}

	  public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, RecordtypeId, AccountId, OwnerId, Opportunity_Sales_Division__c, fxTrade_Created_Date__c, StageName, Primary_Division_Name__c, Is_Traded__c, fxAccount__r.Trade_Volume_USD_30_Day_Max__c,US_TO_RM_Run_Batch_To_Reassign__c,US_TO_RM_Batch_Not_Processed__c, Account_Mailing_Country__c FROM Opportunity WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	  
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		OpportunityUtil.roundRobinOpportunities((Opportunity[]) batch);
	}
	
  	public void execute(SchedulableContext context) {
    	Database.executeBatch(new BatchReassignOpportunitiesSchedulable(query), BATCH_SIZE);
  	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}


}