/**
 * Salesforce's limits util class
 */
public with sharing class LimitsUtil {
    
    /**
     * Know if we can enqueue a job
     */
    public static Boolean canEnqueueJob() {
        // Force to do job sync on tests because of "TestsUtil.forceJobsSync"
        if(Test.isRunningTest() && TestsUtil.forceJobsSync)
            return false;
        // job limit is not reached
        return !isQueueableJobsLimitReached();
    }
    
    /**
     * Know if queueable job limit is reached
     */
    public static Boolean isQueueableJobsLimitReached() {
        Integer actual = Limits.getQueueableJobs();
        Integer jLimit = Limits.getLimitQueueableJobs();

        System.debug('isQueueableJobsLimitReached-->> ' +
            actual  + ' of ' + jLimit);

        // To avoid "Maximum stack depth has been reached."
        // error on test methods because you can’t chain 
        // queueable jobs in an Apex test. Initially you have 50
        // as limit and after enqueue the first job, you have 
        // 1 as limit, but in tests it is not right because you
        // can not chain jobs on tests.
        if(Test.isRunningTest() && jLimit == 1)
            return true;

        return actual == (jLimit);
    }
}