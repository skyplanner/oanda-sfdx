/**
 * @File Name          : GetShortLanguageCodeActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/10/2024, 9:06:33 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/10/2024, 13:42:51 PM   aniubo     Initial Version
 **/
@isTest
private class GetShortLanguageCodeActionTest {
	@isTest
	private static void testGetLanguageCodeEx() {
		// Test data setup
		Boolean isException = false;
		// Actual test
		Test.startTest();
		try {
			ExceptionTestUtil.prepareDummyException();
			List<String> codes = GetShortLanguageCodeAction.getLanguageCode(
				new List<String>{ 'en_US' }
			);
		} catch (Exception ex) {
			isException = true;
		}

		Test.stopTest();
		// Asserts
		Assert.areEqual(true, isException, 'Exception should be thrown');
	}

	@isTest
	private static void testGetLanguageCodeInputEmptyOrNull() {
		// Actual test
		Test.startTest();

		List<String> codes = GetShortLanguageCodeAction.getLanguageCode(
			new List<String>()
		);

		Assert.areEqual(false, codes.isEmpty(), 'codes should not be empty');
		Assert.areEqual(
			true,
			String.isEmpty(codes.get(0)),
			'Result should be empty'
		);
		codes = GetShortLanguageCodeAction.getLanguageCode(null);

		Assert.areEqual(false, codes.isEmpty(), 'codes should not be empty');
		Assert.areEqual(
			true,
			String.isEmpty(codes.get(0)),
			'Result should be empty'
		);

		Test.stopTest();
		// Asserts
	}

	@isTest
	private static void testGetLanguageCodeBadPrefix() {
		// Actual test
		Test.startTest();

		List<String> codes = GetShortLanguageCodeAction.getLanguageCode(
			new List<String>{ 'BAD_PREFIX_en_US' }
		);
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, codes.isEmpty(), 'Codes should not be empty');
		Assert.areEqual(
			true,
			String.isEmpty(codes.get(0)),
			'Result should be empty'
		);
	}

	@isTest
	private static void testGetLanguageCodeBad() {
		// Actual test
		Test.startTest();

		List<String> codes = GetShortLanguageCodeAction.getLanguageCode(
			new List<String>{ 'BAD_PREFIX_en_US' }
		);
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, codes.isEmpty(), 'Codes should not be empty');
		Assert.areEqual(
			true,
			String.isEmpty(codes.get(0)),
			'Result should be empty'
		);
	}

	@isTest
	private static void testGetLanguageCode() {
		// Actual test
		Test.startTest();

		List<String> codes = GetShortLanguageCodeAction.getLanguageCode(
			new List<String>{
				GetShortLanguageCodeAction.LANGUAGE_IDENTIFIER_PREFIX + 'en_US'
			}
		);
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, codes.isEmpty(), 'Codes should not be empty');
		Assert.areEqual('en_US', codes.get(0), 'Result should be en_US');
	}
}