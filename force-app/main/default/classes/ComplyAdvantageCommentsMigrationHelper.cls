/**
 * @description       : 
 * @author            : Vanglirajan Kalimuthu
 * @group             : 
 * @last modified on  : 10-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class ComplyAdvantageCommentsMigrationHelper implements Callable
{
    public String instanceName;
    public String instanceKey;
    public Map<integer, String> userIdNameMap;
    public Map<String, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId;
    
    public ComplyAdvantageCommentsMigrationHelper(String instanceName) {
        this.instanceName = instanceName;
        this.instanceKey = getInstanceApiKeyByName(instanceName);
        this.userIdNameMap = getUserIdNameMap(instanceKey);
        this.sfBatchEntitiesByUniqueId = new Map<String, Comply_Advantage_Search_Entity__c>{};
    }

    public Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'migrate' {
                migrate((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'migrateWithCheck' {
                migrateWithCheck((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'migrateCommentByComment' {
                migrateCommentByComment((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
        }
        return null;
    }

    private String getInstanceApiKeyByName(String instanceName){
        return [
            SELECT API_Key__c
            FROM Comply_Advantage_Instance_Setting__c
            WHERE Name = :instanceName 
            LIMIT 1
        ].API_Key__c;
    }

    public void migrateCommentByComment(List<Comply_Advantage_Search__c> searches) {
        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>();
        Note[] notes = new Note[]{};
        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        Set<Id> searchIds = searchMap.keySet();

        //retrieve entities FROM Salesforce for the searches
        for (Comply_Advantage_Search_Entity__c entity : [
            SELECT ID, Unique_ID__c, Name, Comply_Advantage_Search__c
            FROM Comply_Advantage_Search_Entity__c
            WHERE Comply_Advantage_Search__c IN :searchIds]) {
            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }

        for (Comply_Advantage_Search__c search : searches) {
            System.debug('search migrated from: ' + search.Search_Reference_Before_OGM_Migration__c);
            if (String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c)) {
                try {
                    Boolean notesSavedSuccessfully = false;
                    Boolean commentsSavedAlready = false; //hasCommentsSaved(search);

                    if (!commentsSavedAlready) {
                        ComplyAdvantageCommentsResult  result = getCAComments(search);
                        
                        if (result != null && result.content != null && !result.content.isEmpty()) {
                            for (ComplyAdvantageSearchComment comment : result.content) {
                                saveNote(search, comment);
                            }
                        }
                        search.Comments_Migration_Status__c = 'Done_profile_update';
                    }
                } 
                catch (Exception ex) {
                    search.Comments_Migration_Status__c = 'Failed';
                    System.debug('Exception : ' + ex.getMessage());
                }
                searchesToUpdate.add(search);
            }
        }

        Database.update(searchesToUpdate, false);

    }
    public void migrateWithCheck(List<Comply_Advantage_Search__c> searches) {
        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>();
        Note[] notes = new Note[]{};
        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        Set<Id> searchIds = searchMap.keySet();

        //retrieve entities FROM Salesforce for the searches
        for (Comply_Advantage_Search_Entity__c entity : [
            SELECT ID, Unique_ID__c, Name, Comply_Advantage_Search__c
            FROM Comply_Advantage_Search_Entity__c
            WHERE Comply_Advantage_Search__c IN :searchIds]) {
            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }
        
        for (Comply_Advantage_Search__c search : searches) {
            System.debug('search migrated from: ' + search.Search_Reference_Before_OGM_Migration__c);
            if (String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c)) {
                try {
                    Boolean notesSavedSuccessfully = false;
                    Boolean commentsSavedAlready = hasCommentsSaved(search);

                    if (!commentsSavedAlready) {
                        String consolidatedNotes = getNotes(search);
                        
                        if (String.isBlank(consolidatedNotes)) {
                            search.Comments_Migration_Status__c = 'Done_profile_update';
                        }
                        else if (consolidatedNotes == 'Failed') {
                            search.Comments_Migration_Status__c ='Failed';
                        }
                        else {
                            notesSavedSuccessfully = saveNote(search, consolidatedNotes);
                            search.Comments_Migration_Status__c = notesSavedSuccessfully ? 'Done_profile_update' : 'Failed';
                        }
                    }
                } 
                catch (Exception ex) {
                    search.Comments_Migration_Status__c = 'Failed';
                    System.debug('Exception : ' + ex.getMessage());
                }
                searchesToUpdate.add(search);
            }
        }

        Database.update(searchesToUpdate, false);
    }

    public void migrate(List<Comply_Advantage_Search__c> searches) {
        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>();
        Note[] notes = new Note[]{};
        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        Set<Id> searchIds = searchMap.keySet();

        //retrieve entities FROM Salesforce for the searches
        for (Comply_Advantage_Search_Entity__c entity : [
            SELECT ID, Unique_ID__c, Name, Comply_Advantage_Search__c
            FROM Comply_Advantage_Search_Entity__c
            WHERE Comply_Advantage_Search__c IN :searchIds]) {
            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }
        
        for (Comply_Advantage_Search__c search : searches) {            
            System.debug('search migrated from: ' + search.Search_Reference_Before_OGM_Migration__c);
            if (String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c)) {
                try {
                    Boolean notesSavedSuccessfully = false;

                    String consolidatedNotes = getNotes(search);
                    
                    if (String.isBlank(consolidatedNotes)) {
                        search.Comments_Migration_Status__c = 'Done_profile_update';
                    }
                    else if (consolidatedNotes == 'Failed') {
                        search.Comments_Migration_Status__c ='Failed';
                    }
                    else {
                        notesSavedSuccessfully = saveNote(search, consolidatedNotes);
                        search.Comments_Migration_Status__c = notesSavedSuccessfully ? 'Done_profile_update' : 'Failed';
                    }
                } 
                catch (Exception ex) {
                    search.Comments_Migration_Status__c = 'Failed';
                    System.debug('Exception : ' + ex.getMessage());
                }
                searchesToUpdate.add(search);
            }
        }
        system.debug('search.Comments_Migration_Status__' + searchesToUpdate[0].Comments_Migration_Status__c);
        Database.update(searchesToUpdate, false);
    }

    public Boolean hasCommentsSaved(Comply_Advantage_Search__c search) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            String apiKey = ComplyAdvantageUtil.getApiKey(search);
            request.setEndpoint('https://api.complyadvantage.com/searches/' + search.Search_Reference__c + '/comments?api_key=' + apiKey);
            request.setMethod('GET');
            request.setTimeout(120000);
            HttpResponse response = http.send(request);
            
            if (response.getStatusCode() == 200) {
                ComplyAdvantageCommentsResult result = ComplyAdvantageCommentsResult.parse(response.getBody());
                return result != null && result.content != null && !result.content.isEmpty() && result.content.size() > 0;
            }
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public ComplyAdvantageCommentsResult getCAComments(Comply_Advantage_Search__c search) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            String searchReference = String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c) ?
                                            search.Search_Reference_Before_OGM_Migration__c : '';

            ComplyAdvantageSearchMigration helper = new ComplyAdvantageSearchMigration(null, instanceName);
            String oldInstanceApiKey = helper.oldInstanceKey;
            request.setEndpoint('https://api.complyadvantage.com/searches/' + searchReference + '/comments?api_key=' + oldInstanceApiKey);
            request.setMethod('GET');
            request.setTimeout(120000);
            HttpResponse response = http.send(request);
            
            if (response.getStatusCode() == 200) {
                ComplyAdvantageCommentsResult result = ComplyAdvantageCommentsResult.parse(response.getBody());

                return result;
            }
            return null;
        }
        catch(Exception ex) {
            return null;
        }
    }

    public String getNotes(Comply_Advantage_Search__c search) {
        String consolidatedNotes = '';

        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            String searchReference = String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c) ?
                                            search.Search_Reference_Before_OGM_Migration__c : '';

            ComplyAdvantageSearchMigration helper = new ComplyAdvantageSearchMigration(null, instanceName);
            String oldInstanceApiKey = helper.oldInstanceKey;
            request.setEndpoint('https://api.complyadvantage.com/searches/' + searchReference + '/comments?api_key=' + oldInstanceApiKey);
            request.setMethod('GET');
            request.setTimeout(120000);
            HttpResponse response = http.send(request);
            
            if (response.getStatusCode() == 200) {
                ComplyAdvantageCommentsResult result = ComplyAdvantageCommentsResult.parse(response.getBody());

                if (result != null && result.content != null && !result.content.isEmpty()) {
                    String[] allNotesCombined = new String[]{};
                    for (ComplyAdvantageSearchComment comment : result.content) {
                        String note = getNoteSaveFormat(search, comment);
                        if(String.isNotBlank(note)) {
                            allNotesCombined.add(note);
                        }
                    }
                    consolidatedNotes = String.join(allNotesCombined, '\n' );
                }
            }
        }
        catch(Exception ex) {
            consolidatedNotes = 'Failed';
        }
        return consolidatedNotes;
    }

    public void saveNote(Comply_Advantage_Search__c search, ComplyAdvantageSearchComment comment) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            String apiKey = ComplyAdvantageUtil.getApiKey(search);
            request.setEndpoint('https://api.complyadvantage.com/searches/' + search.Search_Reference__c + '/comments?api_key=' + apiKey);
            request.setMethod('POST');
            request.setTimeout(120000);
    
            Map<String, String> requestBody = new Map<String, String>{};
            String formattedNote = getNoteSaveFormat(search, comment);
            requestBody.put('comment', formattedNote);
            request.setBody(JSON.serialize(requestBody));
    
            HttpResponse response = http.send(request);
            if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {
              
            }
        }
        catch (Exception ex) {
           
        }
    }

    public Boolean saveNote(Comply_Advantage_Search__c search, String comment) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            String apiKey = ComplyAdvantageUtil.getApiKey(search);
            request.setEndpoint('https://api.complyadvantage.com/searches/' + search.Search_Reference__c + '/comments?api_key=' + apiKey);
            request.setMethod('POST');
            request.setTimeout(120000);
    
            Map<String, String> requestBody = new Map<String, String>{};
            requestBody.put('comment', comment);
            request.setBody(JSON.serialize(requestBody));
    
            HttpResponse response = http.send(request);
            if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {
                return true;
            }
        }
        catch (Exception ex) {
            return false;
        }
       
        return false;
    }

    public String getNoteSaveFormat(Comply_Advantage_Search__c search, ComplyAdvantageSearchComment comment) {  
        String updateSummaryNote = '';

        if (String.isNotBlank(comment.message)) {
            DateTime dt= (DateTime)JSON.deserialize('"' + comment.created_at + '"', DateTime.class);
        
            updateSummaryNote = '\n' + dt.format('MMM dd, yyyy');

            String notesOwner = userIdNameMap.containsKey(comment.user_id) ? userIdNameMap.get(comment.user_id) : 'User (' + comment.user_id + ')';
            String title = notesOwner + ' added new comment to search';
            if (String.isNotBlank(comment.entity_id)) {
                String uniqueId = search.Search_Id__C + ':' + comment.entity_id;
                Comply_Advantage_Search_Entity__c se = sfBatchEntitiesByUniqueId.get(uniqueId);
                title = notesOwner + ' added new comment to entity ' + (se != null ? se.Name + ' (' + comment.entity_id +')' : '');
            }
            updateSummaryNote += '\n' + title;

            updateSummaryNote += '\n' + comment.message;
        }

        return updateSummaryNote;
    }

	private Map<Integer,String> getUserIdNameMap(String apiKey) {
        Map<Integer, String> users = new Map<Integer, String>();
            
        Http http = new Http();

        HttpRequest request = new HttpRequest();
        String serviceURL = 'https://api.complyadvantage.com/users?api_key='+ apiKey;
        request.setEndpoint(serviceURL);
        request.setMethod('GET');
        request.setTimeout(120000);
        
        HttpResponse response = http.send(request); 
        System.debug(response.getBody());

        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String, Object> resWrapper = (Map<String, Object>) results.get('content');
            List<Object> responseData = (List<Object>) resWrapper.get('data');

            for (Object obj : responseData) {
                Map<String, Object> userWrapper = (Map<String, Object>) obj;
                users.put(
                    (Integer) userWrapper.get('id'),
                    (String) userWrapper.get('name')
                );
            }
        }
        
        return users;
	} 
}