/**
 * @File Name          : PendingServiceRouting_Trigger_Utility.cls
 * @Description        :
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/6/2024, 2:50:34 AM
**/
public class PendingServiceRouting_Trigger_Utility {

	//********************************************************

	public static final String OBJ_API_NAME = 'PendingServiceRouting';

	public static Boolean enabled {get; set;}

	public static Boolean isEnabled() {
		return (enabled != false) &&
			DisabledTriggerManager.getInstance()
				.triggerIsEnabledForObject(
					OBJ_API_NAME
				);
	}

	//********************************************************

	final List<PendingServiceRouting> newList;
	final Map<ID,PendingServiceRouting> oldMap;

	public PendingServiceRouting_Trigger_Utility(
		List<PendingServiceRouting> newList,
		Map<ID,PendingServiceRouting> oldMap
	) {
		this.newList = newList;
		this.oldMap = oldMap;
	}

	//********************************************************

	public void assignRoutingPriorityBeforeInsert() {
		Boolean error = false;
		try {
			ExceptionTestUtil.execute();
			ServiceLogManager.getInstance().log(
				'assignRoutingPriorityBeforeInsert', // msg
				PendingServiceRouting_Trigger_Utility.class.getName() // category
			);

			// ServiceRoutingContext routingContext =
			// 	new ServiceRoutingContext(newList);

			ServiceRoutingContext routingContext =
				ServiceRoutingContext.getInstance(newList);
			SRoutingHelper helper = routingContext.getRoutingHelper();
			
			if (helper == null) {
				ServiceLogManager.getInstance().log(
					'assignRoutingPriorityBeforeInsert -> helper is null', // msg
					PendingServiceRouting_Trigger_Utility.class.getName() // category
				);
				return;
			}
			//else...
			helper.calculatePriority();
			//...
		} catch (LogException lex) {
			error = true;
			handleLogException(lex, Test.isRunningTest());
			
		} catch (Exception ex) {
			error = true;
			handleException(ex, Test.isRunningTest());
		}
		setDefaultPriority(
			error, // condition
			newList, // routingObjList
			OmnichanelPriorityConst.DEFAULT_PRIORITY // defaultPriority
		);
	}

	@testVisible
	static void setDefaultPriority(
		Boolean condition,
		List<PendingServiceRouting> routingObjList,
		Integer defaultPriority
	) {
		if (condition == true) {
			for(PendingServiceRouting psrObj : routingObjList) {
				if (psrObj.RoutingPriority == null) {
					psrObj.RoutingPriority = defaultPriority;
				}
			}
		}
	}

	public void assignSkillsAfterInsert() {
		try {
			ExceptionTestUtil.execute();
			ServiceLogManager.getInstance().log(
				'assignSkillsAfterInsert', // msg
				PendingServiceRouting_Trigger_Utility.class.getName() // category
			);

			// ServiceRoutingContext routingContext =
			// 	new ServiceRoutingContext(newList);

			ServiceRoutingContext routingContext =
				ServiceRoutingContext.getInstance(newList);
			SRoutingHelper helper = routingContext.getRoutingHelper();

			if (helper == null) {
				ServiceLogManager.getInstance().log(
					'assignSkillsAfterInsert -> helper is null', // msg
					PendingServiceRouting_Trigger_Utility.class.getName() // category
				);
				return;
			}
			//else...
			List<SkillRequirement> skillReqList = helper.getSkillRequirements();
			insert skillReqList;

			helper.updateWorkItems();
			helper.createRoutingLogs();

		} catch (LogException lex) {
			handleLogException(lex, Test.isRunningTest());
			
		} catch (Exception ex) {
			handleException(ex, Test.isRunningTest());
		}
	}

	public void checkTransfersSkills() {
		try {
			CheckServiceTransferSkillsCmd command =
				CheckServiceTransferSkillsCmd.getCheckServiceTransferSkillsCmd(
					newList,
					oldMap
				);

			if (command == null) {
				return;
			}
			// else...
			command.execute();

		} catch (LogException lex) {
			handleLogException(lex, Test.isRunningTest());

		} catch (Exception ex) {
			handleException(ex, Test.isRunningTest());
		}
	}

	@TestVisible
	Boolean handleException(
		Exception ex,
		Boolean rethrow
	) {
		if (rethrow) {
			throw LogException.newInstance(
				OmnichanelConst.OMNICHANEL_ERROR_CAT,
				ex
			);
		}
		// else...
		Logger.exception(
			OmnichanelConst.OMNICHANEL_ERROR_CAT,
			ex
		);
		return true;
	}

	@TestVisible
	Boolean handleLogException(
		LogException ex,
		Boolean rethrow
	) {
		if (rethrow) {
			throw ex;
		}
		//else...
		return true;
	}

}