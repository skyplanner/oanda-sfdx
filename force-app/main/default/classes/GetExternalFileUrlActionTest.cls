/**
 * @File Name          : GetExternalFileUrlActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/18/2023, 2:16:47 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 2:11:05 PM   aniubo     Initial Version
 **/
@isTest
private class GetExternalFileUrlActionTest {
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		Boolean isError = false;
		List<String> urls;
		// Actual test
		Test.startTest();
		try {
			urls = GetExternalFileUrlAction.getExternalFileUrl(null);
			Assert.areEqual('unknown', urls.get(0), 'Should return unknown');

			urls = null;

			urls = GetExternalFileUrlAction.getExternalFileUrl(
				new List<GetExternalFileUrlAction.ActionInfo>()
			);
			Assert.areEqual('unknown', urls.get(0), 'Should return unknown');
		} catch (Exception ex) {
			isError = true;
		}
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}

	@isTest
	private static void testGetExternalFileUrlEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			GetExternalFileUrlAction.getExternalFileUrl(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testGetExternalFileUrl() {
		// Test data setup
		GetExternalFileUrlAction.ActionInfo info = new GetExternalFileUrlAction.ActionInfo();
		info.fileKey = 'Cryptocurrencies';
		// Actual test
		Test.startTest();
		List<String> urls = GetExternalFileUrlAction.getExternalFileUrl(
			new List<GetExternalFileUrlAction.ActionInfo>{ info }
		);
		Test.stopTest();
		External_File_URL__mdt fileInfo = ExternalFileUrlInfo.getRecByKeyRegionAndLanguage(
			info.fileKey,
			'OC',
			ExternalFileUrlInfo.DEFAULT_LANGUAGE
		);
		// Asserts
		if (fileInfo != null) {
			Assert.areEqual(
				true,
				urls.get(0).contains(fileInfo.URL__c),
				'Should return the correct url'
			);
		} else {
			Assert.areEqual('unknown', urls.get(0), 'Should return unknown');
		}
	}

	@isTest
	private static void testGetExternalFileUrlLangNotSupport() {
		// Test data setup
		GetExternalFileUrlAction.ActionInfo info = new GetExternalFileUrlAction.ActionInfo();
		info.fileKey = 'MyCustomFileKey';
		info.language = 'MyCustomLangX45';
		info.region = 'MyCustomRegionX45';
		// Actual test
		Test.startTest();
		List<String> urls = GetExternalFileUrlAction.getExternalFileUrl(
			new List<GetExternalFileUrlAction.ActionInfo>{ info }
		);
		Test.stopTest();
		External_File_URL__mdt fileInfo = ExternalFileUrlInfo.getRecByKeyRegionAndLanguage(
			info.fileKey,
			info.region,
			info.language
		);
		// Asserts
		if (fileInfo != null) {
			Assert.areEqual(
				true,
				urls.get(0).contains(fileInfo.URL__c),
				'Should return the correct url'
			);
		} else {
			Assert.areEqual('unknown', urls.get(0), 'Should return unknown');
		}
	}
}