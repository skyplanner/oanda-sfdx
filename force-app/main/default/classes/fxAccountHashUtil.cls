/**
 * Util class to handle fxAccount hash fields
 * - Set for a fxAccount record list.
 * - Set for all fxAccount records using a batch.
 *    Criteria parameter is available to filter 
 *    records to update.
 * 
 * Hashed fields
 *  - fxAcc.Hashed_Username__c
 *  - fxAcc.Hashed_Email__c
 * 
 *  Assumptions:
 *  - Account__r.PersonEmail and Contact__r.Email
 * 
 * Scenarios to get new hashed fields
 * 
 *  - When a fxAccount is inserted
 *      fxAcc.Hashed_Username__c = (hashed)fxAcc.Name
 *      fxAcc.Hashed_Email__c = (hashed)fxAcc.Email_Formula__c
 *
 *  - When a fxAccount.Name is updated
 *      fxAcc.Hashed_Username__c = (hashed)fxAcc.Name
 * 
 *  - When a Account.PersonEmail is updated
 *      fxAcc.Hashed_Email__c = (hashed)fxAcc.Email_Formula__c
 * 
 *  - When a Lead.Email is updated
 *      fxAcc.Hashed_Email__c = (hashed)fxAcc.Email_Formula__c
 */
public with sharing class fxAccountHashUtil implements Callable {
    
    private static final Integer CNT_DEFAULT = 200;

    /**
     * Set fxAccount hashed fields
     * for all corresponding fxAccounts
     */
    public static Id setHashedFieldsToAll()
    {
        return
            setHashedFieldsToAll(
                null,
                CNT_DEFAULT);
    }

    /**
     * Set fxAccount hashed fields
     * for all corresponding fxAccounts
     * - cntRcds: How many records to 
     *     process in each batch chunk
     */
    public static Id setHashedFieldsToAll(
        Integer cntRcds)
    {
        return
            setHashedFieldsToAll(
                null,
                cntRcds);
    }

    /**
     * Set fxAccount hashed fields
     * for all corresponding fxAccounts
     * - cntRcds: How many records to 
     *     process in each batch chunk
     * - criteria: query criteria to
     *     retrieve right fxAccount records
     */
    public static Id setHashedFieldsToAll(
        String criteria,
        Integer cntRcds)
    {
        return
            GeneralProcessBatch.process(
                'SELECT Id, Name, Email_Formula__c FROM fxAccount__c ' +
                    (String.isBlank(criteria) ? '' : ' WHERE ' + criteria),
                true,
                'fxAccountHashUtil',
                'setHashedFields',
                cntRcds);
    }

    /**
     * Set fxAccount hashed fields
     */
    public static void setHashedFields(
        List<fxAccount__c> fxAccs)
    {
        setHashedFields(fxAccs, false);
    }

    /**
     * Set fxAccount hashed fields specifying 
     * if you want to save the records too
     */
    public static void setHashedFields(
        List<fxAccount__c> fxAccs,
        Boolean save)
    {
        // Set hashed
        for (fxAccount__c fxAccount :fxAccs) {
            setHashedUserName(fxAccount);
            setHashedEmail(fxAccount);
        }

        // Update records
        if(save) {
            TriggersUtil.disableObjTriggers(
                TriggersUtil.Obj.FXACCOUNT);

            update fxAccs;
            
            TriggersUtil.enableObjTriggers(
                TriggersUtil.Obj.FXACCOUNT);
        }
    }

    /**
     * Set fxAccount hashed fields
     */
    public static void setHashedFields(
        List<fxAccount__c> newFxAccs,
        List<fxAccount__c> oldFxAccs)
    {
        fxAccount__c newFxAcc,
            oldFxAcc;

        for (Integer i = 0; i < newFxAccs.size(); i++) {
			newFxAcc = newFxAccs.get(i);
			oldFxAcc = oldFxAccs.get(i);

            // If name changed then calculate new hash
            if(newFxAcc.Name !=
                oldFxAcc.Name) {
                    setHashedUserName(newFxAcc);
            }
        }
    }

    /**
     * Set fxAccount hashed fields
     */
    public static void setHashedFields(
        List<Account> newAccs,
        List<Account> oldAccs)
    {
        Account newAcc, oldAcc;
        
        // Get account ids who changed 'PersonEmail'
        Set<Id> accIds =
            SObjectUtil.getRecordIdsWhoFieldsChanged(
                newAccs,
                oldAccs,
                'PersonEmail');

        System.debug(
            'Account ids with PersonEmail field updated: ' +
            accIds);

        // Set hashed email
        setHashedEmailFromAccs(
            accIds, true);
    }

    /**
     * Set fxAccount hashed fields
     */
    public static void setHashedFields(
        List<Lead> newLeads,
        List<Lead> oldLeads)
    {
        Lead newLead, oldLead;
        
        // Get lead ids who changed 'Email'
        Set<Id> leadIds =
            SObjectUtil.getRecordIdsWhoFieldsChanged(
                newLeads,
                oldLeads,
                'Email');

        // Set hashed email
        setHashedEmailFromLeads(
            leadIds, true);
    }

    /**
     * Set fxAccount hashed userName
     */
    public static void setHashedUserName(
        fxAccount__c fxAccount)
    {
        fxAccount.Hashed_Username__c =
            Utilities.getSHA256(
                fxAccount.Name);    
    }

    /**
     * Set fxAccount hashed email
     */
    public static void setHashedEmail(
        fxAccount__c fxAccount)
    {
        fxAccount.Hashed_Email__c =
            Utilities.getSHA256(
                fxAccount.Email_Formula__c);
    }

    /**
     * Set fxAccount hashed email
     */
    public static void setHashedEmailFromAccs(
        Set<Id> accIds,
        Boolean stopTrigger)
    {
        if(accIds.isEmpty())
            return;

        // Retrieve fxAccounts from accounts
        List<fxAccount__c> fxAccs =
            [SELECT Id,
                    Email_Formula__c
                FROM fxAccount__c
                WHERE Account__c IN :accIds];

        // Set hash email to fxAccounts
        setHashedEmail(
            fxAccs, stopTrigger);
    }

    /**
     * Set fxAccount hashed email
     */
    public static void setHashedEmailFromLeads(
        Set<Id> leadIds,
        Boolean stopTrigger)
    {
        if(leadIds.isEmpty())
            return;

        // Retrieve fxAccounts from leads
        List<fxAccount__c> fxAccs =
            [SELECT Id,
                    Email_Formula__c
                FROM fxAccount__c
                WHERE Lead__c IN :leadIds];
        // Set hash email to fxAccounts
        setHashedEmail(
            fxAccs, stopTrigger);
    }

    /**
     * Set fxAccount hashed email
     */
    public static void setHashedEmail(
        List<fxAccount__c> fxAccs,
        Boolean stopTrigger)
    {
       if(fxAccs.isEmpty())
            return;
        
        for(fxAccount__c fxAcc :fxAccs) {
            setHashedEmail(fxAcc);
        }

        if(stopTrigger) {
            TriggersUtil.disableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
            update fxAccs;
            TriggersUtil.enableObjTriggers(TriggersUtil.Obj.FXACCOUNT);
        } else {
            update fxAccs;
        }
    }

    /**
     * Callable interface method
     *  - setHashedFields
     */
    public Object call(
        String action, 
        Map<String, Object> args) 
    {
        switch on action {
            when 'setHashedFields' {
                // Set hashed fields and update records
                setHashedFields(
                    (List<fxAccount__c>)args.get('records'),
                    true);       
            }
        }
        return null;
    }

}