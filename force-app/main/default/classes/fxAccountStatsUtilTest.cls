@isTest
private class fxAccountStatsUtilTest {

    static testMethod void leadStatsTest() {
    	populateConfig();
    	
        Lead ld = new Lead(LastName = 'abc', Email = '123@abc.com');
        insert ld;
        
        DateTime currDay = DateTime.now();
    	DateTime theDayBefore = DateTime.now().addDays(-1);
    	
    	fxAccount__c fxaLive1 = createFxAccount(null, ld.Id, true, currDay);
    	fxAccount__c fxaLive2 = createFxAccount(null, ld.Id, true, theDayBefore);
    	
    	fxAccount__c fxaDemo1 = createFxAccount(null, ld.Id, false, currDay);
    	fxAccount__c fxaDemo2 = createFxAccount(null, ld.Id, false, theDayBefore);
    	
    	Test.startTest();
    	BatchSetLivefxAccountStatsScheduleable.executeBatch();
    	Test.stopTest();
    	
    	Lead ld1 = [select id, First_Live_fxAccount_DateTime__c, First_Practice_fxAccount_DateTime__c from Lead where Id = :ld.Id];
    	
    	System.assertEquals(theDayBefore, ld1.First_Live_fxAccount_DateTime__c);
    	System.assertEquals(theDayBefore, ld1.First_Practice_fxAccount_DateTime__c);
        
    }
    
    static testMethod void demoleadStatsTest(){
    	populateConfig();
    	
        Lead ld = new Lead(LastName = 'abc', Email = '123@abc.com', RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId());
        insert ld;
        
        DateTime currDay = DateTime.now();
    	DateTime theDayBefore = DateTime.now().addDays(-1);
    	
    	fxAccount__c fxaDemo1 = createFxAccount(null, ld.Id, false, currDay);
    	fxaDemo1.First_Trade_Date_Practice__c = currDay;
    	fxaDemo1.last7DaysRealizedGL__c = 100.5;
    	fxaDemo1.last7DaysTradeCount__c = 10;
    	fxaDemo1.LastLoggedDate__c = currDay;
    	fxaDemo1.lastUnrealizedGL__c = 50;
    	
    	fxAccount__c fxaDemo2 = createFxAccount(null, ld.Id, false, theDayBefore);
    	fxaDemo2.First_Trade_Date_Practice__c = theDayBefore;
    	fxaDemo2.last7DaysRealizedGL__c = 100.5;
    	fxaDemo2.last7DaysTradeCount__c = 10;
    	fxaDemo2.LastLoggedDate__c = theDayBefore;
    	fxaDemo2.lastUnrealizedGL__c = 50;
    	
    	List<fxAccount__c> fxaList = new List<fxAccount__c>();
    	fxaList.add(fxaDemo1);
    	fxaList.add(fxaDemo2);
    	
    	update fxaList;
    	
    	Test.startTest();
    	BatchSetLivefxAccountStatsScheduleable.executeBatch();
    	Test.stopTest();
    	
    	Lead ld1 = [select id, First_Live_fxAccount_DateTime__c, First_Trade_Date__c,  First_Practice_fxAccount_DateTime__c, last7DaysRealizedGL__c, last7DaysTradeCount__c, LastLoggedDate__c,lastUnrealizedGL__c  from Lead where Id = :ld.Id];
    	
    	System.assertEquals(theDayBefore, ld1.First_Practice_fxAccount_DateTime__c);
    	System.assertEquals(theDayBefore, ld1.First_Trade_Date__c);
    	System.assertEquals(201, ld1.last7DaysRealizedGL__c);
    	System.assertEquals(20, ld1.last7DaysTradeCount__c);
    	System.assertEquals(currDay, ld1.LastLoggedDate__c);
    	System.assertEquals(100, ld1.lastUnrealizedGL__c);
    	
    }
    
    static testMethod void liveLeadStatsTest(){
    	populateConfig();
    	
        Lead ld = new Lead(LastName = 'abc', Email = '123@abc.com', RecordTypeId = RecordTypeUtil.getLeadRetailId());
        insert ld;
        
        DateTime currDay = DateTime.now();
    	DateTime theDayBefore = DateTime.now().addDays(-1);
    	
    	fxAccount__c fxaLive1 = createFxAccount(null, ld.Id, true, currDay);
    	fxAccount__c fxaLive2 = createFxAccount(null, ld.Id, true, theDayBefore);
    	
    	fxAccount__c fxaDemo1 = createFxAccount(null, ld.Id, false, currDay);
    	fxaDemo1.First_Trade_Date_Practice__c = currDay;
    	fxaDemo1.last7DaysRealizedGL__c = 100.5;
    	fxaDemo1.last7DaysTradeCount__c = 10;
    	fxaDemo1.LastLoggedDate__c = currDay;
    	fxaDemo1.lastUnrealizedGL__c = 50;
    	
    	fxAccount__c fxaDemo2 = createFxAccount(null, ld.Id, false, theDayBefore);
    	fxaDemo2.First_Trade_Date_Practice__c = theDayBefore;
    	fxaDemo2.last7DaysRealizedGL__c = 100.5;
    	fxaDemo2.last7DaysTradeCount__c = 10;
    	fxaDemo2.LastLoggedDate__c = theDayBefore;
    	fxaDemo2.lastUnrealizedGL__c = 50;
    	
    	List<fxAccount__c> fxaList = new List<fxAccount__c>();
    	fxaList.add(fxaDemo1);
    	fxaList.add(fxaDemo2);
    	
    	update fxaList;
    	
    	Test.startTest();
    	BatchSetLivefxAccountStatsScheduleable.executeBatch();
    	Test.stopTest();
    	
    	Lead ld1 = [select id, First_Live_fxAccount_DateTime__c, First_Trade_Date__c,  First_Practice_fxAccount_DateTime__c, last7DaysRealizedGL__c, last7DaysTradeCount__c, LastLoggedDate__c,lastUnrealizedGL__c  from Lead where Id = :ld.Id];
    	
    	System.assertEquals(theDayBefore, ld1.First_Practice_fxAccount_DateTime__c);
    	System.assertEquals(theDayBefore, ld1.First_Live_fxAccount_DateTime__c);
    	System.assertEquals(null, ld1.First_Trade_Date__c);
    	System.assertEquals(null, ld1.last7DaysRealizedGL__c);
    	System.assertEquals(null, ld1.last7DaysTradeCount__c);
    	System.assertEquals(null, ld1.LastLoggedDate__c);
    	System.assertEquals(null, ld1.lastUnrealizedGL__c);
    	
    }
    
    static testMethod void accountStatsTest(){
    	populateConfig();
    	
    	Account acc = new Account(LastName = 'abc');
    	insert acc;
    	
    	DateTime currDay = DateTime.now();
    	DateTime theDayBefore = DateTime.now().addDays(-1);
    	
    	fxAccount__c fxaLive1 = createFxAccount(acc.Id, null, true, currDay);
    	fxAccount__c fxaLive2 = createFxAccount(acc.Id, null, true, theDayBefore);
    	
    	fxAccount__c fxaDemo1 = createFxAccount(acc.Id, null, false, currDay);
    	fxAccount__c fxaDemo2 = createFxAccount(acc.Id, null, false, theDayBefore);
    	
    	Test.startTest();
    	BatchSetLivefxAccountStatsScheduleable.executeBatch();
    	Test.stopTest();
    	
    	Account acc1 = [select id, First_Live_fxAccount_DateTime__c, First_Practice_fxAccount_DateTime__c from Account where id = :acc.Id];
    	
    	System.assertEquals(theDayBefore, acc1.First_Live_fxAccount_DateTime__c);
    	System.assertEquals(theDayBefore, acc1.First_Practice_fxAccount_DateTime__c);
    	
    }
    
    
    private static fxAccount__c createFxAccount(ID accountId, ID leadId, boolean isLive, DateTime fxTradeDate){
    	
    	fxAccount__c liveAccount = new fxAccount__c();
    	liveAccount.name = 'abc' + Math.random();
    	
    	if(isLive){
    		liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    	}else{
    		liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    	}
	    
		liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
		liveAccount.Account__c = accountId;
		liveAccount.Lead__c = leadId;
		liveAccount.fxTrade_Created_Date__c = fxTradeDate;
		
		insert liveAccount;
		
    	return liveAccount;
    }
    
   private static DateTime adjustTimeZoneOffSet(DateTime input){
       Timezone tz = Timezone.getTimeZone('America/New_York');
       
       Integer offset = tz.getOffset(input)/1000;
       
       return input.addSeconds(-offset);	
   }
   
   private static void populateConfig(){
   		Practice_fxAccount_Setting__c setting = new Practice_fxAccount_Setting__c(name = 'Default');
    	setting.Allow_First_Trade_Date__c = true;
    	setting.Allow_fxTradeCreateDate__c = true;
    	setting.Allow_last7DaysRealizedGL__c = true;
    	setting.Allow_last7DaysTradeCount__c = true;
    	setting.Allow_LastLoggedDate__c = true;
    	setting.Allow_lastUnrealizedGL__c = true;
    	
    	insert setting;
   }
}