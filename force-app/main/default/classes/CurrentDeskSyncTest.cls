@isTest(SeeAllData=false)
public class CurrentDeskSyncTest 
{
    public static Lead lead;
    public static Account account;
    public static fxAccount__c fxaccount;
	
    public static final string liveCustomerRegReponse = 
    '{'+
        '"ClientId": 5678'+
    '}';
    public static final string demoCustomerRegReponse = 
    '{'+
        '"LeadId": 5678'+
    '}';
    
    @testSetup
    static void init()
    {
        Settings__c settings = new Settings__c(Name='Default', Enable_Current_Desk_Integration__c=true);
        insert settings;
    }
    

    @isTest
    static void test1_liveRegistration_lead() 
    {
        lead = new Lead(FirstName = 'Robert Gabriel1', LastName='Mugabe1', Email = 'test1@oanda.com', RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert lead;

        fxaccount = new fxAccount__c(name = 'Mugabe1', RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = lead.Id , Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;

        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CalloutMock(200, liveCustomerRegReponse, 'success', null));
        CurrentDeskSync.run = true;
        fxaccount.Funnel_Stage__c = Constants.FUNNEL_RFF;
        update fxaccount;
        Test.stopTest();
        
        fxAccount__c fx = [select CD_Client_ID__c from fxAccount__c where Id=:fxaccount.Id];
        system.assertEquals(5678, fx.CD_Client_ID__c);
    }
    
    @isTest
    static void test2_liveRegistration_account() 
    {
        account = new Account(PersonTitle = 'Mr.', FirstName = 'Robert Gabriel1', LastName='Mugabe1', PersonEmail = 'test1@oanda.com');
        insert account;

        fxaccount = new fxAccount__c(name = 'Mugabe1', RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Account__c = account.Id , Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CalloutMock(200, liveCustomerRegReponse, 'success', null));
        CurrentDeskSync.run = true;
        fxaccount.Funnel_Stage__c = Constants.FUNNEL_RFF;
        update fxaccount;
        Test.stopTest();
        
        fxAccount__c fx = [select CD_Client_ID__c from fxAccount__c where Id=:fxaccount.Id];
        system.assertEquals(5678, fx.CD_Client_ID__c);
    }

    @isTest
    static void test3_demoRegistration() 
    {
        lead = new Lead(FirstName = '', LastName='Robert Gabriel2 Mugabe2', Email = 'test2@oanda.com',RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE);
        insert lead;

        fxaccount = new fxAccount__c(name = 'Mugabe2', Lead__c = lead.Id, RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE,Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;

        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, demoCustomerRegReponse, 'success', null));
        Test.startTest();
        
        currentDeskSync.run = false;
        lead.FirstName = 'Robert Gabriel3';
        lead.LastName = 'Mugabe3';
        update lead;
        
        Test.stopTest();
        
        fxAccount__c fx = [select CD_Demo_Lead_Id__c from fxAccount__c where Id=:fxaccount.Id Limit 1];
        system.assertEquals(5678, fx.CD_Demo_Lead_Id__c);
    }

    @isTest
    static void test4_updateLiveCutomerDetails() 
    {
        lead = new Lead(FirstName = 'Robert Gabriel1', LastName='Mugabe1', Email = 'test1@oanda.com', RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert lead;
		
        currentDeskSync.run = false;
        fxaccount = new fxAccount__c(CD_Client_ID__c = 5678, name = 'Mugabe1', RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = lead.Id , Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;
        currentDeskSync.run = true;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutMock(401, '{}', 'success', null));
        fxaccount.CD_Account_Read_Only__c = true;
        fxaccount.CD_Lock_Account__c = true;
        fxaccount.Funding_Limited__c = true;
        update fxaccount;
        
        Test.stopTest();
    }

    @isTest
	static void test4_updateLiveIndividualDetails() {
		account = new Account(
			FirstName = 'Robert Gabriel1',
			LastName='Mugabe1',
			PersonEmail = 'test1@oanda.com'
		);
		insert account;
		
		currentDeskSync.run = false;
		fxaccount = new fxAccount__c(
			CD_Client_ID__c = 5678,
			name = 'Mugabe1',
			RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Account__c = account.Id,
			Division_Name__c = 'OANDA Global Markets'
		);
		insert fxaccount;
		currentDeskSync.run = true;
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CalloutMock(200, '{}', 'success', null));		
		account.FirstName = 'Christopher';
		account.LastName = 'Larrs';
		update account;
		
		Test.stopTest();
	}

    @isTest
	static void test4_archiveUser() {
		currentDeskSync.run = false;

		lead = new Lead(
			FirstName = 'Robert Gabriel1',
			LastName='Mugabe1',
			Email = 'test1@oanda.com',
			RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE
		);
		insert lead;

		fxaccount = new fxAccount__c(
			CD_Client_ID__c = 5678,
			Name = 'Mugabe1',
			Lead__c = lead.Id,
			RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Global Markets'
		);
		insert fxaccount;
		currentDeskSync.run = true;
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CalloutMock(200, '{}', 'success', null));		
		fxaccount.CD_Archive_User__c = true;
		update fxaccount;
		
		Test.stopTest();
	}

    @isTest
    static void test5_updateFlags() 
    {
		lead = new Lead(FirstName = 'Robert Gabriel1', LastName='Mugabe1', Email = 'test1@oanda.com', RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert lead;
		
        currentDeskSync.run = false;
        fxaccount = new fxAccount__c(CD_Client_ID__c = 5678, CD_Reduce_Position_Only__c = true, name = 'Mugabe1', RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = lead.Id , Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;
       
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CalloutMock(200, '{}', 'success', null));
        currentDeskSync.run = true;
        fxaccount.CD_Reduce_Position_Only__c = false;  
 		update fxaccount;        
        Test.stopTest();
    }

    @isTest
    static void test6_mt4Logins() 
    {
		lead = new Lead(
            FirstName = 'Robert Gabriel1',
            LastName='Mugabe1',
            Email = 'test1@oanda.com',
            RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE
        );
        insert lead;
		
        currentDeskSync.run = false;
        fxaccount = new fxAccount__c(CD_Client_ID__c = 5678, name = 'Mugabe1',
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Lead__c = lead.Id , Division_Name__c = 'OANDA Global Markets');
        insert fxaccount;
		
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CDFlagsUpdateMock());
        currentDeskSync.run = true;
        fxaccount.CD_Reduce_Position_Only__c = true;  
 		update fxaccount;  
        Test.stopTest();
        
        fxAccount__c fx = [select MT4_Logins__c 
            from fxAccount__c where Id=:fxaccount.Id Limit 1];
        system.assertEquals('1234,1213', fx.MT4_Logins__c);
    }
    
    private class CDFlagsUpdateMock implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            HTTPResponse res = new HTTPResponse();
            if (req.getEndpoint().indexOf('updateflag') != -1) 
            {
                res.setBody('{}');
                res.setStatusCode(200);
                return res;
            } 
            if (req.getEndpoint().indexOf('platformlogins') != -1) 
            {
                res.setBody('{"Logins" : "1234,1213"}');
                res.setStatusCode(200);
                return res;
            }
            return res;
        }
    }

}