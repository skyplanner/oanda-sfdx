/**
 * @File Name          : IsLinkedToAccountOrLeadAction.cls
 * @Description        : Check if an email address is linked to an 
 *                       Account or Lead
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/4/2023, 1:50:29 AM
**/
public without sharing class IsLinkedToAccountOrLeadAction {

	@InvocableMethod(label='Is Linked To Account Or Lead' callout=false)
	public static List<ActionResult> isLinkedToAccountOrLead(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<ActionResult> result = new List<ActionResult> { new ActionResult() };
			
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(infoList[0].messagingSessionId);
			result[0] = new ActionResult(
				process.checkLinkToAccountOrLead(infoList[0].userEmail)
			);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				IsLinkedToAccountOrLeadAction.class.getName(),
				ex
			);
		}
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'userEmail' required = true)
		public String userEmail;
		
	}

	// *******************************************************************

	public class ActionResult {

		@InvocableVariable(label = 'isLinkedToAccountOrLead' required = false)
		public Boolean isLinkedToAccountOrLead;

		@InvocableVariable(label = 'fxAccountId' required = false)
		public ID fxAccountId;

		public ActionResult() {
			this.isLinkedToAccountOrLead = false;
		}

		public ActionResult(MessagingAccountCheckInfo accountCheckInfo) {
			this();
			if (accountCheckInfo != null) {
				this.isLinkedToAccountOrLead = 
					accountCheckInfo.isLinkedToAccountOrLead;
				this.fxAccountId = accountCheckInfo.fxAccountId;
			}
		}
		
	}
	
}