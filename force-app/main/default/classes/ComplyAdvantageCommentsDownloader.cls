/**
 * Downloads the comments of a comply advantage search.
 * @author Fernando Gomez
 * @since 11/15/2021
 */
public without sharing class ComplyAdvantageCommentsDownloader
	extends ComplyAdvantageDownloaderBase {

	/**
	 * @param searchId
	 */
	public ComplyAdvantageCommentsDownloader(String searchId) {
		super(searchId);
		successCodes.add(201);
		successCodes.add(204);
	}

	/**
	 * Doanloads the content and saves it into salesforce.
	 * Executes out of a queueable conext.
	 */
	public List<ComplyAdvantageSearchComment> getComments() 
	{
		ComplyAdvantageCommentsResult result;
		ComplyAdvantageUsersDownloader usersDownloader;
		Map<Integer, ComplyAdvantageUser> userMap;
		List<ComplyAdvantageSearchComment> comments = new List<ComplyAdvantageSearchComment>{};

		try 
		{
			result = fetchComments();

			// we then put each users in the comment
			if (result != null && result.content != null && !result.content.isEmpty()) 
			{
				comments = result.content;

				// we need all users ammpped by id
				userMap = getUsersMap();

				for (ComplyAdvantageSearchComment comment : result.content)
				{
					if (userMap.containsKey(comment.user_id))
					{
						comment.user = userMap.get(comment.user_id);
					}
				}
			}

			return comments;

		} catch (Exception ex) {
			throw fail(ex);
		}
	}

	/**
	 * Doanloads the content and saves it into salesforce.
	 * Executes out of a queueable conext.
	 */
	public void addComment(String comment, String entityId) {
		Map<String, String> newComment;

		try {
			// we create the comment
			newComment = new Map<String, String>();
			newComment.put('comment', comment);

			if (String.isNotBlank(entityId))
				newComment.put('entity_id', entityId);

			// we then send the comment
			saveComment(newComment);
		} catch (Exception ex) {
			throw fail(ex);
		}
	}
	
	/**
	 * Doanloads the content and saves it into salesforce.
	 * Executes out of a queueable conext.
	 */
	public override void execute() {
		// NOT SUPPORTED YET
	}

	/**
	 * Obtains all comments
	 * @return 
	 */
	private ComplyAdvantageCommentsResult fetchComments() {
		// we obtain all entities in the current page
		get(fetchCommentsEndpoint(), null, getBaseParams());

		// we proceed on a valida response
		if (isResponseCodeSuccess())
			return ComplyAdvantageCommentsResult.parse(resp.getBody());
		else
			throw getCalloutException();
	}

	/**
	 * Saves a comments
	 * @param newComment 
	 */
	private void saveComment(Map<String, String> newComment) {
		// we then send the comment
		post(
			fetchCommentsEndpoint(),
			null,
			getBaseParams(),
			JSON.serialize(newComment)
		);

		// we proceed on a valida response
		if (!isResponseCodeSuccess())
			throw getCalloutException();
	}

	/**
	 * Obtains all users and returns a map
	 * @return 
	 */
	private Map<Integer, ComplyAdvantageUser> getUsersMap() {
		ComplyAdvantageUsersDownloader usersDownloader;
		List<ComplyAdvantageUser> users;
		Map<Integer, ComplyAdvantageUser> result;

		usersDownloader = new ComplyAdvantageUsersDownloader(apiKey);
		users = usersDownloader.getUsers();

		result = new Map<Integer, ComplyAdvantageUser>();
		for (ComplyAdvantageUser u : users)
			result.put(u.id, u);

		return result;
	}

	/**
	 * @return the url of the entity provider servi ce
	 */
	private String fetchCommentsEndpoint() {
		return String.format(
			SPGeneralSettings.getInstance().getValue('CA_Endpoint_Comments'),
			new List<String> { searchReference }
		);
	}
}