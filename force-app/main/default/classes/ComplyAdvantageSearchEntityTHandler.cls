public with sharing class ComplyAdvantageSearchEntityTHandler {
    public void handleTrigger(List<Comply_Advantage_Search_Entity__c> newList, Map<Id, Comply_Advantage_Search_Entity__c> oldMap, System.TriggerOperation triggerEvent) {

        switch on triggerEvent {
            when AFTER_INSERT, AFTER_UPDATE {
                //SP-13828 replaced with roll up summary fields
                //calculateTotalHits(newList, oldMap);
            }
        }
    }

    /* public void calculateTotalHits (List<Comply_Advantage_Search_Entity__c> newList, Map<Id, Comply_Advantage_Search_Entity__c> oldMap) {
        Set<Id> caSearchIds = new Set<Id>();
        for (Comply_Advantage_Search_Entity__c caSearchEntity : newList) {
            if (oldMap == null || caSearchEntity.Match_Status__c != oldMap.get(caSearchEntity.Id).Match_Status__c) {
                caSearchIds.add(caSearchEntity.Comply_Advantage_Search__c);
            }
        }
        List<Comply_Advantage_Search__c> caSearchToUpdate = new List<Comply_Advantage_Search__c>();
        for (Comply_Advantage_Search__c caSearch : [SELECT Id, (SELECT Id, Match_Status__c FROM Comply_Advantage_Search_Entities__r WHERE Is_Removed__c = FALSE) FROM Comply_Advantage_Search__c
                                                WHERE Id = :caSearchIds]) {
            Integer totalCAHits = caSearch.Comply_Advantage_Search_Entities__r.size();
            Integer notReviewedHits = 0;
            for (Comply_Advantage_Search_Entity__c caSearchEntity : caSearch.Comply_Advantage_Search_Entities__r) {
                if (caSearchEntity.Match_Status__c == 'potential_match') {
                    notReviewedHits++;
                }
            }
            caSearch.Not_Reviewed_Hits__c = notReviewedHits;
            caSearch.Reviewed_Hits__c = totalCAHits - notReviewedHits;
            caSearchToUpdate.add(caSearch);
        }
        update caSearchToUpdate;
    } */
}