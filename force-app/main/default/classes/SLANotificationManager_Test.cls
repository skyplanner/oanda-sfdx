/**
 * @File Name          : SLANotificationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/24/2021, 5:41:50 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 5:40:36 PM   acantero     Initial Version
**/
@isTest
private without sharing class SLANotificationManager_Test {

    @isTest
    static void getSlaNotificationId() {
        Test.startTest();
        String result = SLANotificationManager.getSlaNotificationId();
        Test.stopTest();
        System.assert(String.isNotBlank(result));
    }
    
}