/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 18th, 2019
 */
global class HelpPortalSearchCtrl extends HelpPortalBase {
	global String searchText { get; private set; }
	global String category { get; private set; }
	global String encodedText { get; private set; }
	global String encodedCategory { get; private set; }
	global Integer pageNo { get; private set; }
	global Integer pageSize { get; private set; }
	global Boolean isCategory { get; private set; }
	global Boolean isError { get; private set; }
	global String errorMessage { get; private set; }
	global List<FAQ> faqs { get; private set; }
	global String division { get; private set; }

	/**
	 * Main constructor
	 */
	global HelpPortalSearchCtrl() {
		super();
		Map<String, String> params;
		isError = false;

		// the params should be in the current page
		params = ApexPages.currentPage().getParameters();
		searchText = params.get('search');
		category = params.get('category');
		division = params.get('division');

		// if a category was specified we change the search type
		isCategory = !String.isBlank(category);

		// first, we extract the params from the query string
		// we make sure all params are good
		validateAndParse(params.get('pageNo'), params.get('pageSize'));

		// if no error, we fetch our info
		if (!isError)
			doSearch();
	}

	/**
	 * Constructor for internal use
	 * @param language
	 * @param searchText
	 * @param pageNo
	 * @param pageSize
	 */
	global HelpPortalSearchCtrl(String language, String searchText,
			String pageNo, String  pageSize) {
		super(language);
		isError = false;
		isCategory = false;
		this.searchText = searchText;

		// first, we extract the params from the query string
		// we make sure all params are good
		validateAndParse(pageNo, pageSize);

		// if no error, we fetch our info
		if (!isError)
			doSearch();
	}

	/**
	 * Constructor for internal use
	 * @param language
	 * @param searchText
	 * @param pageNo
	 * @param pageSize
	 * @param division
	 */
	global HelpPortalSearchCtrl(String language, String searchText,
			String pageNo, String  pageSize, String divisionSearch) {
		super(language);
		isError = false;
		isCategory = false;
		division = divisionSearch;
		this.searchText = searchText;

		// first, we extract the params from the query string
		// we make sure all params are good
		validateAndParse(pageNo, pageSize);

		// if no error, we fetch our info
		if (!isError)
			doSearch();
	}

	/**
	 * Validates all params
	 * @param pageNo
	 * @param pageSize
	 */
	private void validateAndParse(String pageNo, String pageSize) {
		if (isCategory)
			// we need an encoded version of the category
			// for urls
			encodedCategory = EncodingUtil.urlEncode(category, 'UTF-8');
		else if (String.isBlank(searchText) || searchText.length() < 3) {
			// if we are dealing with search text the 
			// validation is different
			isError = true;
			errorMessage = 'Search criteria is required and ' +
				'must be at least 3 characters';
			return;
		} else
			encodedText = EncodingUtil.urlEncode(searchText, 'UTF-8');
		
		// we make sure page number is an integer
		if (String.isBlank(pageNo))
			this.pageNo = 0;
		else if (pageNo.isNumeric())
			this.pageNo = Integer.valueOf(pageNo);
		else {
			isError = true;
			errorMessage = 'The page number (pageNo) must be a positive integer';
			return;
		}			

		// we make sure page size is an integer
		if (String.isBlank(pageSize))
			this.pageSize = 20;
		else if (pageSize.isNumeric())
			this.pageSize = Integer.valueOf(pageSize);
		else {
			isError = true;
			errorMessage = 'The page size (pageSize) must be a positive integer';
		}
	}

	/**
	 * Proceeds with the search based on the criteria
	 * @return a page of faqs that matched the criteria
	 */
	private void doSearch() {
		String txt;
		Search.SearchResults searchResults;

		// the returned faqs will be 
		// in the first (and only) list
		faqs = new List<FAQ>();
	

        String division_filter = '(All__c';
		 if(!String.isBlank(division))
			 division_filter += ','+ division +'__c';
		 division_filter += ')';
		 
		if (isCategory){
		  
			// we perform a text query on the article object
			faqs = FAQ.doWrap(Database.query(
					' SELECT ' +
						' Id, ' +
						' KnowledgeArticleId, ' +
						' Title, ' +
						' Answer__c, ' +
						' AllowFeedback__c ' +
					' FROM FAQ__kav ' +
					' WHERE PublishStatus = \'Online\' ' +
					' AND Language = :language ' +
					' WITH DATA CATEGORY Categories__c AT ' +
						String.escapeSingleQuotes(category) +
				    ' AND Division__c AT ' + division_filter +
					' LIMIT ' + pageSize +
					' OFFSET ' + (pageSize * pageNo)));
		  }
		else {
    		// we remove double spaces
			txt = searchText; // escapeSosl(searchText);
			// we perform a text query on the article object
			searchResults = Search.find(
				' FIND :txt IN ALL FIELDS ' +
				' RETURNING FAQ__kav ( '+
						' Id, ' +
						' KnowledgeArticleId, ' +
						' Title, ' +
						' Answer__c, ' +
						' AllowFeedback__c ' +
					' WHERE PublishStatus = \'Online\' ' +
					' AND Language = \'' + language + '\' ' +
					' LIMIT ' + pageSize +
					' OFFSET ' + (pageSize * pageNo) +
				' ) ' +
				' WITH DATA CATEGORY Division__c AT ' + division_filter +
				' WITH SNIPPET (target_length=180) ' +
				' UPDATE TRACKING ');

			for (Search.SearchResult r : searchResults.get('FAQ__kav'))
				faqs.add(FAQ.doWrap((FAQ__kav)r.getSObject(),
					r.getSnippet('Title'), r.getSnippet()));
		}
	}

	/**
	 * Prepares the text  to be used in SOSL
	 */
	/*
	private String escapeSosl( String text ) {
		String escapedText;
		Pattern myPattern;
		Matcher myMatcher;

		// handle single quotes, remove reserved logical operators
		escapedText = String.escapeSingleQuotes(text)
			.replaceAll( '(?i)( AND NOT | AND | OR )', ' ');

		// match special characters to escape
		myPattern = Pattern.compile(
			'(\\?|\\&|\\||\\!|\\{|\\}|\\[|\\]|\\(|\\)|\\^|\\~|\\*|\\:|\\"|\\+|\\-)');
		myMatcher = myPattern.matcher(escapedText);

		// escape special characters
		return '*' +
			myMatcher
				.replaceAll('\\\\\\\\$1')
				.replaceAll(' +', '*')
				.trim() + '*';
	}
	*/
}