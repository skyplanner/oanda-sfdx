/**
 * @File Name          : FaqPreviewTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/25/2020, 7:04:07 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/25/2020   acantero     Initial Version
**/
@istest
public class FaqPreviewTestDataFactory {

    public static final String DEFAULT_LANGUAGE= 'en_US';
    public static final String SPANISH_LANGUAGE= 'es';
    public static final String ART_TITLE= 'Test Art 1';
    public static final String ART_URL_NAME= 'Test-Art-1';

    public static ID createTestArticle(String language) {
        return createTestArticle(language, 'bla bla bla');
    }

    public static ID createTestArticle(String language, String body) {
        FAQ__kav newArt = new FAQ__kav(
			Title = ART_TITLE,
            UrlName = ART_URL_NAME,
            Language = language,
            Answer__c = body
		);
        insert newArt;
        newArt = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id = :newArt.Id];
        KbManagement.PublishingService.publishArticle(newArt.KnowledgeArticleId, false);
        return newArt.Id;
    }


}