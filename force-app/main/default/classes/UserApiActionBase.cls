/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-27-2023
 * @last modified by  : Dianelys Velazquez
**/
public abstract class UserApiActionBase implements Callable {
    public static final String ACTION_NAME_IS_VISIBLE = 'isVisible';
    public static final String ACTION_NAME_INVOKE_ACTION = 'invokeAction';

    protected AuditTrailManager auditTrailManager;

    public UserApiActionBase() {
        auditTrailManager = new AuditTrailManager();
    }

    public Object call(String action, Map<String, Object> record) {
        if (action == ACTION_NAME_INVOKE_ACTION)
            return call(record);
            
        if (action == ACTION_NAME_IS_VISIBLE)
            return isVisible(record);

        throw new InvalidParameterValueException('action', action);
    }

    protected abstract String call(Map<String, Object> record);

    protected abstract Boolean isVisible(Map<String, Object> record);

    public static EnvironmentIdentifier getfxTradeUserId(
        Map<String, Object> record
    ) {
        if (!record.containsKey('fxTrade_User_Id__c') || 
            record.get('fxTrade_User_Id__c') == null)
            throw new ApplicationException('Unable to find fx Trade User Id.');
            
        if (!record.containsKey(Constants.API_FIELD_ENV) || 
            record.get(Constants.API_FIELD_ENV) == null)
            throw new ApplicationException(
                'Unable to find fx Trade User environment.');

        return new EnvironmentIdentifier(
            string.valueOf(record.get(Constants.API_FIELD_ENV)),
            string.valueOf(record.get('fxTrade_User_Id__c')));
    }

    public static Id getfxAccountId(Map<String, Object> record) {
        return !record.containsKey('fxAccountId') || 
            record.get('fxAccountId') == null
            ? null : (Id) record.get('fxAccountId');
    }

    public static String getActionName(Map<String, Object> record) {
        return !record.containsKey('actionName') ||
            record.get('actionName') == null
            ? null : (String) record.get('actionName');
    }

    public static String getActionLabel(Map<String, Object> record) {
        return !record.containsKey('actionLabel') ||
            record.get('actionLabel') == null
            ? null : (String) record.get('actionLabel');
    }

    public static UserApiAction getAction(Map<String, Object> record) {
        return !record.containsKey('action') || 
            record.get('action') == null
            ? null : (UserApiAction) record.get('action');
    }

    public static UserApiUserGetResponse getUser(Map<String, Object> record) {
        return !record.containsKey('user') || 
            record.get('user') == null
            ? null : (UserApiUserGetResponse) record.get('user');
    }

    public static TasUserAccountGetResponse getV20DefaultInfo(
        Map<String, Object> record
    ) {
        return !record.containsKey('v20DefaultInfo') || 
            record.get('v20DefaultInfo') == null
            ? null : (TasUserAccountGetResponse) record.get('v20DefaultInfo');
    }

    protected String callUserAction(UserActionCall uac) {
        String fxAccountId = getfxAccountId(uac.record);
        EnvironmentIdentifier identifier = getfxTradeUserId(uac.record);
        UserCallout userCallout = new UserCallout();
        userCallout.updateStatus(identifier, uac.statusWrapper.status);
        auditTrailManager.saveAuditTrail(identifier.id, 
            fxAccountId, uac.action, uac.statusWrapper.change);
        return uac.action + ' action success!!!';
    }

    public class UserActionCall {
        public String action {get; set;}

        public UserApiStatus.StatusWrapper statusWrapper {get; set;}

        public Map<String, Object> record {get; set;}
    }
}