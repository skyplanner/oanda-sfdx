/**
 * @File Name          : RelatedMessagingSessionsService.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/14/2024, 2:09:28 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/8/2024, 2:24:53 PM   aniubo     Initial Version
 **/
public inherited sharing class RelatedMessagingSessionsService implements IRelatedMessagingSessionsService {
	private Id recordId { get; set; }
	private IRelatedMessagingSessionsReader reader { get; set; }
	public RelatedMessagingSessionsService(
		Id recordId,
		IRelatedMessagingSessionsReader reader
	) {
		this.recordId = recordId;
		this.reader = reader;
	}
	public Integer getCount() {
		Integer count = 0;
		SessionInfo info = getSessionInfo();
		if (info != null) {
			count = reader.GetCount(recordId,info.relatedFiledName, info.relatedIds);
		}
		MessagingSessionRelatedEntity relatedEntity = reader.getMessagingRelatedEntity(
			recordId
		);
		return count;
	}
	public List<MessagingSession> getRelatedMessagingSessions(
		List<String> fields,
		Integer pageSize,
		Integer pageNo
	) {
		String fieldName;
		List<MessagingSession> messagingSessions = new List<MessagingSession>{};
		SessionInfo info = getSessionInfo();
		if (info != null) {
			messagingSessions = reader.getRelatedMessagingSessions(
				info.relatedIds,
				fields,
				info.relatedFiledName,
				pageSize,
				pageNo
			);
		}
		return messagingSessions;
	}

	private SessionInfo getSessionInfo() {
		Set<Id> relatedIds = new Set<Id>();
		String fieldName;
		SessionInfo sessionInfo = null;
		MessagingSessionRelatedEntity relatedEntity = reader.getMessagingRelatedEntity(
			recordId
		);
		if (relatedEntity != null) {
			if (relatedEntity.isAccountType) {
				relatedIds = reader.getMessagingEndUsers(
					relatedEntity.recordId
				);
				fieldName = MessagingSession.fields.MessagingEndUserId.getDescribe()
					.getName();
			} else {
				fieldName = MessagingSession.fields.LeadId.getDescribe()
					.getName();
				relatedIds.add(relatedEntity.recordId);
			}
			sessionInfo = new SessionInfo(relatedIds, fieldName);
		}
		return sessionInfo;
	}
	public class SessionInfo {
		public Set<Id> relatedIds { get; set; }
		public String relatedFiledName { get; set; }

		public SessionInfo(Set<Id> relatedIds, String relatedFiledName) {
			this.relatedIds = relatedIds;
			this.relatedFiledName = relatedFiledName;
		}
	}
}