public without sharing class LeadBatchUtil {
	public static final String NAME_LEAD_SOBJECT = 'Lead';
	
	public static final Integer MAX_MERGE_SIZE = 10;
	
	public static Boolean MERGING = false;
	
	public static Map<String, Integer> mergeLeads(Lead[] leadsToMerge) {
		//if(!CustomSettings.isEnableLeadMerging()) {
		//	return;
		//}
		
		// get the emails of all the leads we want to merge
		Set<String> emails = new Set<String>();
		for(Lead l : leadsToMerge) {
			if (l.Email != null) {
				emails.add(l.Email);
			}
		}
		
		//merge between leads
		Map<String, Integer> result = mergeLeads(emails);
		
		//merge remaining leads with contacts
		mergeWithContacts(emails);
		
		
		return result;
	}
	
	/* This needs to be a separate query since CreatedDate is not updateable during the merge */
	private static Map<Id, Lead> getLeadsWithEmailCreatedDate(Set<String> emails) {
		String query = 'SELECT CreatedDate, CreatedById FROM Lead WHERE IsConverted = false AND Email IN ' + SoqlUtil.getSoqlList(emails) + SoqlUtil.getSoqlMergeLeadRecordTypeIds();

		List<Lead> leads = Database.query(query);
		
		Map<Id, Lead> leadsForEmailById = new Map<Id, Lead>();
		for (Lead lead : leads) {
			leadsForEmailById.put(lead.Id, lead);
		}
		
		System.debug('Number of leads selected: ' + leadsForEmailById.keySet().size());
		
		return leadsForEmailById;
	}
	
	private static Map<Id, Lead> getLeadsWithEmail(Set<String> emails, Boolean converted) {
		
		String additionalFields = '';
		if (converted) {
			additionalFields = ' ConvertedAccountId, ConvertedOpportunityId, ConvertedContactId, ';
		}
		
		// query for all leads with the given emails
		String query = 'SELECT ' + additionalFields + getAllLeadFieldsSoql() + ' FROM Lead WHERE IsConverted = ' + converted + ' AND Email IN ' + SoqlUtil.getSoqlList(emails) + SoqlUtil.getSoqlMergeLeadRecordTypeIds() + ' ORDER BY fxTrade_Global_ID__c NULLS LAST';

		List<Lead> leads = Database.query(query);
		
		Map<Id, Lead> leadsForEmailById = new Map<Id, Lead>();
		for (Lead lead : leads) {
			leadsForEmailById.put(lead.Id, lead);
		}
		
		System.debug('Number of leads selected: ' + leadsForEmailById.keySet().size());
		
		return leadsForEmailById;
	}
	
	private static Map<Id, List<Task>> getAssociatedTasks(Set<Id> leadIds) {
		// get tasks associated with the leads
		Map<Id, Task[]> tasksByLeadId = new Map<Id, Task[]>();
		for(Task t : [SELECT Id, OwnerId, Subject, WhoId, Status, CreatedById, CreatedDate FROM Task WHERE WhoId IN :leadIds]) {
			Task[] tasks = tasksByLeadId.get(t.WhoId);
			if(tasks == null) {
				tasks = new List<Task>();
				tasksByLeadId.put(t.WhoId, tasks);
			}
			tasks.add(t);
		}
		
		return tasksByLeadId;
	}

	
	private static Map<String, List<Lead>> groupLeadsByEmail(List<Lead> leads) {
		
		// group leads by email address
		Map<String, List<Lead>> leadsByEmail = new Map<String, List<Lead>>();
		for(Lead lead : leads) {
			// Can't merge leads with null email
			if (lead.Email == null) {
				continue;
			}
			
			List<Lead> leadsWithEmail = leadsByEmail.get(lead.Email);
			if(leadsWithEmail == null) {
				leadsWithEmail = new List<Lead>();
				leadsByEmail.put(lead.Email, leadsWithEmail);
			}
			leadsWithEmail.add(lead);
		}
		
		return leadsByEmail;
	}
	
	private static Map<Id, List<fxAccount__c>> getAssociatedFxAccountsForLeads(Set<Id> leadIds) {
		
		List<fxAccount__c> fxAccounts = [SELECT Id, Lead__c, Account__c, Opportunity__c, Contact__c FROM fxAccount__c WHERE Lead__c IN :leadIds];
		
		Map<Id, List<fxAccount__c>> fxAccountsByLeadId = new Map<Id, List<fxAccount__c>>();
		for (fxAccount__c fxAccount : fxAccounts) {
			List<fxAccount__c> fxAccountsForLead = fxAccountsByLeadId.get(fxAccount.Lead__c);
			if (fxAccountsForLead == null) {
				fxAccountsForLead = new List<fxAccount__c>();
				fxAccountsByLeadId.put(fxAccount.Lead__c, fxAccountsForLead);
			}
			
			fxAccountsForLead.add(fxAccount);
		}
		
		return fxAccountsByLeadId;
	}
	
	public static Map<String, Integer> mergeLeads(Set<String> emails) {
		
		System.debug(emails);
		System.debug(CustomSettings.isEnableLeadMerging());
		
		if(!CustomSettings.isEnableLeadMerging() || emails.size() == 0) {
			return null;
		}
		
		System.debug('Running lead merge logic for leads with emails ' + emails);
		
		Map<Id, Lead> nonConvertedLeadsById = getLeadsWithEmail(emails, false);
		//Map<Id, Lead> convertedLeadsById = getLeadsWithEmail(emails, true);
		
		Map<Id, List<Task>> tasksByLeadId = getAssociatedTasks(nonConvertedLeadsById.keySet());
		
		Map<String, List<Lead>> nonConvertedleadsByEmail = groupLeadsByEmail(nonConvertedLeadsById.values());
		//Map<String, List<Lead>> convertedleadsByEmail = groupLeadsByEmail(convertedLeadsById.values());
		
		Map<Id, Lead> leadsCreatedDate = getLeadsWithEmailCreatedDate(emails);
		
		System.debug('AAAnonConvertedleadsByEmail: ' + nonConvertedleadsByEmail);
		
		// Construct one big map of both converted and nonconverted leads by email
		Map<String, List<Lead>> leadsByEmail = new Map<String, List<Lead>>();
		
		Map<String, Integer> skippedLeads = new Map<String, Integer>();
		
		for (String email : nonConvertedleadsByEmail.keySet()) {
			List<Lead> leads = new List<Lead>();
			List<Lead> nonConvertedLeads = nonConvertedleadsByEmail.get(email);
			
			// Filter out leads with too many duplicates
			if (nonConvertedLeads.size() > MAX_MERGE_SIZE) {
				skippedLeads.put(email, nonConvertedLeads.size() - MAX_MERGE_SIZE);
				for (Integer i =  nonConvertedLeads.size() - 1; i > -1; i--) {
					if (i >= MAX_MERGE_SIZE) {
						nonConvertedLeadsById.remove(nonConvertedLeads.get(i).Id);
						nonConvertedLeads.remove(i);
					}
				}
			}
			
			leads.addAll(nonConvertedLeads);
			
			leadsByEmail.put(email, leads);
		}
		
		
		System.debug('nonConvertedLeadsById: ' + nonConvertedLeadsById);
		System.debug('nonConvertedleadsByEmail: ' + nonConvertedleadsByEmail);
		System.debug('leadsByEmail: ' + leadsByEmail);
		
		// First loop through and handle merging where none of the leads for an email are already
		// converted
		for(String email : leadsByEmail.keySet()) {
			
			List<Lead> leads = leadsByEmail.get(email);
			
			System.debug('Merging leads with email ' + email + '... found ' + leads.size() + ' leads');

			// if there is more than one lead with the same email, merge them
			if(leads.size() < 2) {
				System.debug('Only 1 lead found');
				continue;
			}
			
			
			Lead masterLead = findMasterLead(leads, tasksByLeadId, leadsCreatedDate);
			Lead[] otherLeads = getOtherLeads(masterLead, leads);
				
			System.debug('Merging leads with master Lead: ' + masterLead);
			System.debug('Merging leads with other leads: ' + otherLeads);
				
			mergeMultipleLeads(masterLead, otherLeads, tasksByLeadId);
		}
		
		return skippedLeads;
	}
	
	//some leads may be duplicated with contact. In this case, we need to 
	public static void mergeWithContacts(Set<String> emails){
		if(emails.size() == 0){
			return;
		}
		
		//check if there are existing contacts
		List<Contact> contacts = [select Id, AccountId, Email, OwnerId from Contact where Email in :emails];
		if(contacts.size() == 0){
			System.debug('No contacts to merge');
			
			return;
		}
		
		//get the remaining unmerged leads
		List<Lead> leads = [select id, Email, ConvertedAccountId, ConvertedContactId, ConvertedOpportunityId, isConverted, OwnerId from Lead where isConverted = false and recordTypeId in : RecordTypeUtil.getLeadMergeRecordTypeIds() and Email in :emails];
		if(leads.size() == 0){
			System.debug('No leads left to merge with contacts');
			
			return;
		}
		
		
		//match lead and contact
		Map<ID, Contact> contactByLeadId = new Map<ID, Contact>();
		List<Lead> leadsToDelete = new List<Lead>();
		
		for(Lead ld : leads){
			for(Contact con : contacts){
				
				if(ld.Email == con.Email){
					contactByLeadId.put(ld.Id, con);
					leadsToDelete.add(ld);	
					
					//only merge the first matched contact
					break;
				}
			}
		}
		
		if(contactByLeadId.size() > 0){
			
			//reparent the children records of the leads to contacts
			LeadUtil.migrateChildRecordsForLead(leadsToDelete, contactByLeadId, false);
			
			//deleted the merged leads
			System.debug('Number of leads to delete: ' + leadsToDelete.size());
			delete leadsToDelete;
		}else{
			System.debug('No leads to delete: ');
		}
		
	}
	
	
	// Salesforce has a limitation of merging a maximum of 3 records at a time
	// This method will merge in batches of 3
	public static void mergeMultipleLeads(Lead masterLead, Lead[] otherLeads,  Map<Id, Task[]> tasksByLeadId) {
		
		// Copy over fields from each of the other leads where the masterLead doesn't have a value set
		copyEmptyFields(masterLead, otherLeads);
		
		// Master lead should always have dup email set to false
		masterLead.Duplicate_Email__c = false;
		
		// Determine what the ownership should be on the master lead after merging
		assignMasterOwnerId(masterLead, otherLeads, tasksByLeadId);
		
		// Set the record type of the master lead, if it is not set properly
		setRecordType(masterLead, otherLeads);
		
		//assignCampaigns(masterLead, otherLeads, campaignsByLeadId);
		
		// Now merge the leads (marks the other leads as merged with the master)
		Lead[] currentMergeBatch;
		while(otherLeads.size()>0) {
			if(otherLeads.size()>2) {
				currentMergeBatch = new Lead[] {otherLeads[0], otherLeads[1]};
				otherLeads.remove(0);
				otherLeads.remove(0);
			} else {
				currentMergeBatch = otherLeads.clone();
				otherLeads.clear();
			}
			System.debug('AAA Merging master ' + masterLead + ' with batch ' + currentMergeBatch);
			
			LeadBatchUtil.MERGING = true;
			//try {
				merge masterLead currentMergeBatch;
			//}
			//catch (Dmlexception e) {
			//	
			//	System.debug('Error while merging lead: ' + e);
			//	
			//	// Assignment not allowed by permissions
			//	if (e.getDmlType(0) == System.Statuscode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY) {
			//		System.debug('Error assigning merged lead to owner ' + masterLead.OwnerId + ', deleting other leads: ' + otherLeads);
			//		delete otherLeads;
			//		throw new ApplicationException('An existing <a href=\'/' + masterLead.Id + '\'>lead</a> which is currently unassigned with the same email address already exists but you do not have permission to assign it to yourself.');
			//	}
			//}
			LeadBatchUtil.MERGING = false;

			System.debug('AAA Merged: ' + masterLead);
		}
	}
	
	private static void setRecordType(Lead masterLead, Lead[] otherLeads) {
		if(masterLead.RecordTypeId==RecordTypeUtil.getLeadStandardId()) {
			Id recordTypeId = masterLead.RecordTypeId;
			for(Lead l : otherLeads) {
				if(l.RecordTypeId==RecordTypeUtil.getLeadRetailId()) {
					recordTypeId = l.RecordTypeId;
					masterLead.Funnel_Stage__c = l.Funnel_Stage__c;
				} else if(l.RecordTypeId==RecordTypeUtil.getLeadRetailPracticeId() && recordTypeId==RecordTypeUtil.getLeadStandardId()) {
					recordTypeId = l.RecordTypeId;
					masterLead.Funnel_Stage__c = l.Funnel_Stage__c;
				}
			}
			masterLead.RecordTypeId = recordTypeId;
		}
	}
	
	private static void assignMasterOwnerId(Lead masterLead, Lead[] otherLeads, Map<Id, Task[]> tasksByLeadId) {
		
		// Master lead is already owned by someone else who is active, do nothing
		if (!UserUtil.isIntegrationUserId(masterLead.OwnerId) && UserUtil.isActive(masterLead.OwnerId)) {
			System.debug('AAA Master lead already assigned, not changing assignment');
			return;
		}
		
		// Find the first other lead that is not owned by system user and is active
		for (Lead otherLead : otherLeads) {
			if (!UserUtil.isIntegrationUserId(otherLead.OwnerId) && UserUtil.isActive(otherLead.OwnerId)) {
				
				
				// If master is live and the other is practice, and the other has no activity do not set the other's owner as the master's
				// This is to catch the case when an assigned practice lead with no activity is merging with a new live lead
				// We want it to become a house account if the FXS hasn't worked the lead
				if (LeadUtil.isLiveLead(masterLead) && LeadUtil.isPracticeLead(otherLead)) {
					List<Task> leadTasks = tasksByLeadId.get(otherLead.Id);
					if (leadTasks == null) {
						System.debug('Other lead does not have any tasks, skipping assigment');
						continue;
					}
					
					Boolean hasActivity = false;
					Date threeMonthsBack = Date.today().addMonths(-3);
					for (Task task : leadTasks) {
						// User created task or started
						if (!UserUtil.isIntegrationUserId(task.CreatedById) || !TaskUtil.OPEN_STATUS_VALUES.contains(task.Status))
						{
							// SP-10291 if lead is from Singapore, tasks more than 3 months old are not counted
							if(!(otherLead.Country == 'Singapore' && task.CreatedDate < threeMonthsBack))
							{
								hasActivity = true;
								break;
							}
						}
					}
					
					if (!hasActivity) {
						System.debug('Other lead does not have true activity, skipping assignment');
						continue;
					}
				}
				
				System.debug('Master lead not assigned and other lead is, reassigning to ' + otherLead.OwnerId);
				masterLead.OwnerId = otherLead.OwnerId;
				masterLead.APEX_Allow_Reassignment__c = true;
				return;
			}
		}
		
		System.debug('AAA Assigning master lead to system user');
		masterLead.OwnerId = UserUtil.getSystemUserId();
	}
	
	// get the other leads, that will be merged into the master lead
	public static Lead[] getOtherLeads(Lead masterLead, Lead[] leads) {
		Lead[] result = new Lead[]{};
		for(Lead l : leads) {
			if(l!=masterLead) {
				result.add(l);
			}
		}
		return result;
	}
	
	static Lead[] getMarketoLeads(Lead[] leads, Map<Id, Lead> leadsWithCreatedDateById) {
		Lead[] result = new Lead[]{};
		for(Lead l : leads) {
			Lead leadWithCreatedById = leadsWithCreatedDateById.get(l.Id);
			if(leadWithCreatedById!=null && leadWithCreatedById.CreatedById == UserUtil.getUserIdByName(UserUtil.NAME_MARKETO_USER) && l.recordTypeId == RecordTypeUtil.getLeadStandardId()) {
				//condition for Marketo Lead: 1. created by Marketo user 2. it is a standard lead
				//If the Marketo lead is merged with other leads, then it is no longer standard lead, so it is Marketo lead any more
				result.add(l);
			}
		}
		return result;
	}
	
	public static Lead findMasterLead(Lead[] leads, Map<Id, Task[]> tasksByLeadId, Map<Id, Lead> leadsWithCreatedDateById) {
		
		System.debug('AAA Finding master lead');
		
		// first check to see if there is only one live lead
		Lead[] liveLeads = getLiveLeads(leads);
		if(liveLeads.size()==1) {
			system.debug('Found 1 live lead: ' + liveLeads[0]);
			return liveLeads[0];
		} else if(liveLeads.size()>1) {
			System.debug('Found multiple live leads');
			leads = liveLeads;
		}

		// second check to see if there is only one lead from marketo
		Lead[] marketoLeads = getMarketoLeads(leads, leadsWithCreatedDateById);
		if(marketoLeads.size()==1) {
			system.debug('Found 1 Marketo lead: ' + marketoLeads[0]);
			return marketoLeads[0];
		} else if(marketoLeads.size()>1) {
			System.debug('Found multiple Marketo leads');
			leads = marketoLeads;
		}
		
		// Look for any 8st leads, they take precedence
		Lead[] estLeads = find8stLeads(leads);
		if (estLeads.size() == 1) {
			system.debug('Found 1 8st lead: ' + estLeads[0]);
			return estLeads[0];
		}
		else if (estLeads.size() > 1) {
			System.debug('Found multiple 8st leads');
			leads = estLeads;
		}
		
		// then check to see if there is a single late stage lead
		Lead[] latestStageLeads = findLatestStageLeads(leads);
		if(latestStageLeads.size()==1) {
			System.debug('Found 1 latest stage lead: ' + latestStageLeads[0]);
			return latestStageLeads[0];
		} else if(latestStageLeads.size()>1) {
			System.debug('Found multiple latest stage leads');
			leads = latestStageLeads;
		}
		
		// then check for most tasks on a lead
		Lead[] leadsWithMostTasks = getLeadsWithMostTasks(leads, tasksByLeadId);
		if(leadsWithMostTasks.size()==1) {
			System.debug('Found 1 lead with most tasks: ' + leadsWithMostTasks[0]);
			return leadsWithMostTasks[0];
		} else if(leadsWithMostTasks.size()>1) {
			System.debug('Found multiple leads with most tasks');
			leads = leadsWithMostTasks;
		}
		
		// then check for lead that was created last
		return getNewestLead(leads, leadsWithCreatedDateById);
	}
	
	public static Lead getNewestLead(Lead[] leads, Map<Id, Lead> leadsWithCreatedDateById) {
		Lead result = leads[0];
		for(Lead l : leads) {
			if(leadsWithCreatedDateById.get(l.Id).CreatedDate > leadsWithCreatedDateById.get(result.Id).CreatedDate) {
				result = l;
			}
		}
		system.debug('Found newest lead: '+ result);
		return result;
	}
	
	public static Lead[] getLeadsWithMostTasks(Lead[] leads, Map<Id, Task[]> tasksByLeadId) {
		List<Lead> result = new List<Lead>();
		Integer maxNumberOfTasks = 0;
		for(Lead l : leads) {
			if(result.size()==0) {
				result.add(l);
				maxNumberOfTasks = getNumberOfTasks(tasksByLeadId, l.Id);
			} else {
				Integer currentNumberOfTasks = getNumberOfTasks(tasksByLeadId, l.Id);
				if(maxNumberOfTasks == currentNumberOfTasks) {
					result.add(l);
				} else if(maxNumberOfTasks < currentNumberOfTasks) {
					result.clear();
					result.add(l);
					maxNumberOfTasks = currentNumberOfTasks;
				}
			}
		}
		return result;
	}

	static Integer getNumberOfTasks(Map<Id, Task[]> tasksByLeadId, Id leadId) {
		Task[] tasks = tasksByLeadId.get(leadId);
		if(tasks==null) {
			return 0;
		} else {
			return tasks.size();
		}
	}
	
	public static Lead[] find8stLeads(Lead[] leads) {
		List<Lead> result = new List<Lead>();
		for (Lead l : leads) {
			if (l.LeadSource != null && l.LeadSource.toLowerCase() == '8st') {
				result.add(l);
			}
		}
		
		return result;
	}
	
	public static Lead[] findLatestStageLeads(Lead[] leads) {
		List<Lead> result = new List<Lead>();
		for(Lead l : leads) {
			if(result.size()==0) {
				result.add(l);
			} else {
				Integer compare = FunnelStatus.compare(result[0].Funnel_Stage__c, l.Funnel_Stage__c);
				if(compare==0) {
					result.add(l);
				} else if(compare<0) {
					result.clear();
					result.add(l);
				}
			}
		}
		return result;
	}
	
	public static Lead[] getLiveLeads(Lead[] leads) {
		List<Lead> result = new List<Lead>();
		for(Lead l : leads) {
			if(LeadUtil.isLiveLead(l)) {
				result.add(l);
			}
		}
		return result;
	}
	
	public static void assignLeadsWithActivities(Lead[] leads) {
		Map<Id, Lead> leadById = new Map<Id, Lead>(leads);

		Map<Id, Task> newestTaskByWhoId = new Map<Id, Task>();
		for(Task t : [SELECT Subject, Id, WhoId, CreatedDate, OwnerId FROM Task WHERE OwnerId NOT IN :UserUtil.getIntegrationUserIds() AND OwnerId IN (SELECT Id FROM User WHERE IsActive=true) AND WhoId = :leadById.keySet() AND CreatedDate > :Date.today().addDays(-90)]) {
			Task newestTask = newestTaskByWhoId.get(t.WhoId);
			System.debug('debug t: ' + t);
			if(newestTask==null || t.CreatedDate>newestTask.CreatedDate) {
				newestTaskByWhoId.put(t.WhoId, t);
				System.debug('inserting t: ' + t);
			}
		}
		
		Lead[] leadsToUpdate = new Lead[]{};
		for(Lead l : leads) {
			Task t = newestTaskByWhoId.get(l.Id);
			Id newOwnerId = ( t==null ? UserUtil.getSystemUserId() : t.OwnerId );

			if(newOwnerId!=l.OwnerId) {
				l.OwnerId=newOwnerId;
				leadsToUpdate.add(l);
			}
		}
		
		if(leadsToUpdate.size()>0) {
			update leadsToUpdate;
		}
	}
	
	static Map<String, String> migration = new Map<String, String>();
	static {
		migration.put('ID Verification'.toLowerCase(), FunnelStatus.AWAITING_CLIENT_INFO);
		migration.put('Documents Received'.toLowerCase(), FunnelStatus.DOCUMENTS_UPLOADED);
		migration.put('Ready to Fund'.toLowerCase(), FunnelStatus.READY_FOR_FUNDING);
		migration.put('Funded'.toLowerCase(), FunnelStatus.FUNDED);
		migration.put('Traded'.toLowerCase(), FunnelStatus.TRADED);
		for(String funnelStage : FunnelStatus.FUNNEL_STATUS_LIST) {
			migration.put(funnelStage.toLowerCase(), funnelStage);
		}
		migration.put('', '');
	}
	
	public static String getMigratedFunnelStatus(Lead l) {
		
		
		
		if (LeadUtil.isLiveLead(l)) {
			
			String funnelStage = l.Funnel_Stage__c;
			if (funnelStage == null) {
				return '';
			}
			
			return migration.get(funnelStage.toLowerCase());
		}
		else if (LeadUtil.isPracticeLead(l)) {
			if (l.Traded__c) {
				return FunnelStatus.DEMO_TRADED;
			}
			return FunnelStatus.DEMO_REGISTERED;
		}
		
		return null;
	}
	
	static Map <String, Schema.SObjectType> schemaMap;
	
	static Map <String, Schema.SObjectType> getSchemaMap() {
		if(schemaMap==null) {
			schemaMap = Schema.getGlobalDescribe();
		}
		return schemaMap;
	}
	
	static void copyEmptyFields(lead l1, Lead[] leads) {
		for(Lead l : leads) {
			copyEmptyFields(l1, l);
		}
	}
	
	static void copyEmptyFields(Lead l1, Lead l2) {
		for(String fieldName : getLeadFieldNames()) {
			if (fieldName == 'FirstName' || fieldName == 'LastName') {
				continue;
			}
//			System.debug('Field Name: ' + fieldName + '     LHS: ' + l1.get(fieldName) + '    RHS: ' + l2.get(fieldName));
			if(l2.get(fieldName)!=null && l2.get(fieldName)!='' && l2.get(fieldName)!=false) {
				if(l1.get(fieldName)==null || l1.get(fieldName)=='' || l1.get(fieldName)==false) {
					//System.debug('Putting ' + l2.get(fieldName));
					l1.put(fieldName, l2.get(fieldName));
				}
			}
		}
	}
	
	static String allLeadFieldsSoql;
	static List<String> leadFieldNames;
	
	public static List<String> getLeadFieldNames() {
		if(leadFieldNames==null) {
			getAllLeadFieldsSoql();
		}
		return leadFieldNames;
	}
	
	public static String getAllLeadFieldsSoql() {
		if(allLeadFieldsSoql==null) {
			allLeadFieldsSoql = '';
			leadFieldNames = new List<String>();
			Map <String, Schema.SObjectField> fieldMap = getSchemaMap().get(NAME_LEAD_SOBJECT).getDescribe().fields.getMap();
			for(Schema.SObjectField sof : fieldMap.Values()) {
	            Schema.DescribeFieldResult dfr = sof.getDescribe(); // describe each field (fd)
	            if (dfr.isUpdateable() || dfr.getName() == 'HasOptedOutOfEmail') { // field is updateable
	                allLeadFieldsSoql += dfr.getName() + ',';
	                leadFieldNames.add(dfr.getName());
	            }
	        }
	         
	        if (allLeadFieldsSoql.length()>0) {
	        	allLeadFieldsSoql = allLeadFieldsSoql.substring(0,allLeadFieldsSoql.lastIndexOf(','));
	    	}
	    	
		}
		System.debug('allLeadFieldsSoql: ' + allLeadFieldsSoql);
    	return allLeadFieldsSoql;
	}
}