/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-08-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class EmailUtil {
	
	public static final Integer MAX_EMAIL_BODY_SIZE = 32000;

	public static final String ORG_WIDE_ADDRESS_DO_NOT_REPLAY = 'donotreply@oanda.com';
	
	public static void sendEmailForBatchJob(Id jobId) {
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = getApexJob(jobId);
		sendEmail(a.CreatedBy.Email, 'AyncApexJob: ' + a.JobType + ': ' + a.MethodName + ': ' + a.Status, 'Details: ' + a);
	}
	
	private static AsyncApexJob getApexJob(Id jobId) {
		return [SELECT Id, JobType, MethodName, Status, NumberOfErrors, JobItemsProcessed, CompletedDate, ExtendedStatus, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =	:jobId];
	}
	
	public static void sendEmailForBatchJob(Id jobId, String subject, String plainTextBody) {
		sendEmail(getApexJob(jobId).CreatedBy.Email, subject, plainTextBody);
	}
	
	public static void sendEmail(String toAddress, String subject, String plainTextBody) {
		sendEmail(new String[] {toAddress}, subject, plainTextBody);
	}
	
	public static void sendEmail(String[] toAddresses, String subject, String plainTextBody) {
		sendMail(toAddresses, null, subject, plainTextBody, null, null);
	}
    
	public static void sendEmail(Id targetObjectId, String subject, String plainTextBody) {
		sendMail(null, targetObjectId, subject, plainTextBody, null, null);
	}
    
    public static void sendEmailByTemplate(Id targetObjectId, String templateName, Id whatId) {
        sendEmailByTemplate(targetObjectId,
			templateName, whatId, targetObjectId == null, null);
    }

	/**
	 * SP-9265
	 * @param targetObjectId
	 * @param templateName
	 * @param whatId
	 * @param saveAsActivity
	 */
	@InvocableMethod(
		label='Send Email Using Template'
		description='Send Email Using Template'
	)
	public static void sendEmailByTemplate(List<EmailInformation> infos) {
		for (EmailInformation info : infos)
			sendEmailByTemplate(info.targetObjectId,
				info.templateName, info.whatId, true, null);
	}

	/**
	 * SP-9521
	 * @param targetObjectId
	 * @param templateName
	 * @param whatId
	 * @param saveAsActivity
	 */
	public static void sendEmailByTemplate(Id targetObjectId,
			String templateName, Id whatId, Boolean saveAsActivity,
			String orgWideEmailAddress) {
		EmailTemplate template;
		OrgWideEmailAddress address;
		
		template = getTemplateByName(templateName);
		address = getOrgWideAddress(orgWideEmailAddress);

		// we use template Id
		sendMail(null, targetObjectId, null, null,
			template.Id, whatId, saveAsActivity,
			address != null ? address.Id : null);
	}

	/**
	 * SP-10182
	 * @param toAddresses
	 * @param templateName
	 * @param whatId
	 * @param saveAsActivity
	 */
	public static void sendEmailByTemplate(String[] toAddresses,
			String templateName, Id whatId, Boolean saveAsActivity,
			String orgWideEmailAddress) {
		EmailTemplate template;
		OrgWideEmailAddress address;
		
		template = getTemplateByName(templateName);
		address = getOrgWideAddress(orgWideEmailAddress);

		// we use template Id
		sendMail(toAddresses, null, null, null,
			template.Id, whatId, saveAsActivity,
			address != null ? address.Id : null);
	}
    
    public static void sendEmailByTemplate(Id targetObjectId, Id templateId, Id whatId) {
        sendMail(null, targetObjectId, null, null, templateId, whatId);
    }

	public static void sendEmailByTemplate(String[] toAddresses, String templateName, Id whatId) {
		EmailTemplate template = getTemplateByName(templateName);
        sendMail(toAddresses, null, null, null, template.Id, whatId);
    }

    // When creating new sendEmail methods, please consider the constraints as explained in documentation
    // https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_classes_email_outbound_single.htm
    private static void sendMail(String[] toAddresses, Id targetObjectId,
			String subject, String plainTextBody, Id templateId, Id whatId) {
		Boolean saveAsActivity = true;
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
		// SP-9521: This is kept only for backwards compatibility
        if (targetObjectId != null) {
            // mail.setSaveAsActivity(false);
			saveAsActivity = false;
		}

		// we are ready to send the email
		sendMail(toAddresses, targetObjectId, subject,
			plainTextBody, templateId, whatId, saveAsActivity, null);
    }

	/**
	 * SP-9521
	 * The need to accept the order of save the operation
	 * as an activity.
	 * @param toAddresses
	 * @param targetObjectId
	 * @param subject
	 * @param plainTextBody
	 * @param templateId
	 * @param whatId
	 * @param saveAsActivity
	 * @param orgWideEmailAddressId
	 */
	private static void sendMail(String[] toAddresses, Id targetObjectId,
			String subject, String plainTextBody, Id templateId,
			Id whatId, Boolean saveAsActivity, Id orgWideEmailAddressId) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(toAddresses);
		mail.setTargetObjectId(targetObjectId);
		mail.setSaveAsActivity(saveAsActivity);
		mail.setSubject(subject);
		mail.setPlainTextBody(plainTextBody);
		mail.setWhatId(whatId);
		mail.setTemplateId(templateId);

		if (String.isNotBlank(orgWideEmailAddressId))
			mail.setOrgWideEmailAddressId(orgWideEmailAddressId);

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}

	public static void sendMail(List<Messaging.SingleEmailMessage> mails) {
		if (mails?.size() > 0) {
			Messaging.sendEmail(mails);
		}
	}

	/**
	 * @param toAddresses
	 * @param templateName 
	 * @param whatId
	 * @param whoId
	 */
	public static void sendEmailRenderingTemplate(
			List<String> toAddresses,
			String templateName,
			String whatId,
			String whoId,
			String orgWideEmailAddress) {
		EmailTemplate template;
		OrgWideEmailAddress address;
		Messaging.SingleEmailMessage mail;
		
		template = getTemplateByName(templateName);
		address = getOrgWideAddress(orgWideEmailAddress);

		mail = Messaging.renderStoredEmailTemplate(template.Id, whoId, whatId);
		mail.setToAddresses(toAddresses);
		if(address != null)
			mail.setOrgWideEmailAddressId(address.Id);
		mail.setSaveAsActivity(false);

		Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { mail });
	}
	
	public static void checkOutgoingEmailSize(LIst<EmailMessage> emailMsgs){
		
		for(EmailMessage msg : emailMsgs){
			if(msg.incoming == false){
				if(msg.HtmlBody != null && msg.HtmlBody.length() == MAX_EMAIL_BODY_SIZE ){
					msg.addError('Html Email Body Over Size (32K)');
					System.debug('Html Email Body Over Size (32K) - ' + msg.Id);
				}
				
				if(msg.TextBody != null && msg.TextBody.length() ==  MAX_EMAIL_BODY_SIZE){
					msg.addError('Text Email Body Over Size (32K)');
					System.debug('Text Email Body Over Size (32K) - ' + msg.Id);
				}
			}
		}
	}

	//Prevent users from deleting email messages
    public static void preventDeleteEmailMessage(LIst<EmailMessage> emailMsgs) {
    	if(userinfo.getName() == CustomSettings.getEmailDeletionUserName())
    		return;

    	for(EmailMessage msg : emailMsgs) {
			if(msg.Status != '5') {
				msg.addError('Error: Emails cannot be deleted');
			}
        }
    }

	/**
	 * @param templateName
	 * @return the id of the specified template
	 */
	public static EmailTemplate getTemplateByName(String templateName) {
		return [
			SELECT Id
			FROM EmailTemplate
			WHERE DeveloperName = :templateName
			LIMIT 1
		];
	}

	/**
	 * @param orgWideEmailAddress
	 * @return the org wide address by the email
	 */
	private static OrgWideEmailAddress getOrgWideAddress(
			String orgWideEmailAddress) {
		if (String.isNotBlank(orgWideEmailAddress))
			return [
				SELECT Id
				FROM OrgWideEmailAddress
				WHERE Address = :orgWideEmailAddress
			];
		else
			return null;
	}

	//Update related cases from Email Messages
	public static void updateRelatedCases(List<EmailMessage> emailMessages) {
		List<Id> relatedCaseIds = new List<Id>();
		for(EmailMessage msg : emailMessages) {	
			if(msg.ParentId != null) {
				relatedCaseIds.add(msg.ParentId);
			}
		}

		Map<Id, Case> caseMap = new Map<Id, Case> ([
			SELECT Id, 
				Email_To_Address__c 
			FROM Case 
			WHERE Id IN :relatedCaseIds
		]);

		List<Case> casesToUpdate = new List<Case>();
		for(EmailMessage msg : emailMessages) {
			Case cs = caseMap.get(msg.ParentId);
			If(cs.Email_To_Address__c == null && String.isNotBlank(msg.ToAddress)) {
				cs.Email_To_Address__c = msg.ToAddress.left(255);
				casesToUpdate.add(cs);
			}
		}

		if(casesToUpdate?.size() > 0) {
			update casesToUpdate;
		}
	}

	/**
	 * Used by invocables
	 */
	public class EmailInformation {
		@InvocableVariable(required=true)
		public Id targetObjectId;

		@InvocableVariable(required=true)
		public String templateName;
		
		@InvocableVariable
		public Id whatId;
	}
}