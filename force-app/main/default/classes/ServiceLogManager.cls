/**
 * @File Name          : ServiceLogManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 4:31:22 PM
**/
public without sharing class ServiceLogManager {

	public static final String SERVICE_LOG_ENABLED_CATEGORIES_SETTING = 
		'Service_Log_Enabled_Categories';

	public static final String ALL = 'ALL';
	public static final String NONE = 'NONE';

	static ServiceLogManager instance;

	@TestVisible
	String logProfile;

	@TestVisible
	Boolean enabledForAllCategories;

	@TestVisible
	List<String> enabledCategories; 


	public static ServiceLogManager getInstance() {
		if (instance == null) {
			instance = new ServiceLogManager();
		}
		return instance;
	}

	@TestVisible
	ServiceLogManager() {
		String currentLogProfile = SPSettingsManager.getSettingOrDefault(
			SERVICE_LOG_ENABLED_CATEGORIES_SETTING, // settingName
			NONE // defaultValue
		);
		init(currentLogProfile);
	}

	@TestVisible
	Boolean init(String currentLogProfile) {
		logProfile = currentLogProfile;
		enabledCategories = new List<String>();
		enabledForAllCategories = false;

		if (logProfile == NONE) {
			return false;
		}
		// else...
		if (logProfile == ALL) {
			enabledForAllCategories = true;

		} else {
			enabledCategories.addAll(
				getEnabledCategoriesByProfile(logProfile)
			);
			enabledCategories.addAll(getEnabledCategoriesNoProfile());
		}
		return true;
	}

	@TestVisible
	List<String> getCategories(List<Log_Category__mdt> logCateriesRecords) {
		List<String> result = new List<String>();

		for (Log_Category__mdt logCategoryObj : logCateriesRecords) {
			result.add(logCategoryObj.Category__c);
		}
		return result;
	}

	@TestVisible
	List<String> getEnabledCategoriesNoProfile() {
		List<Log_Category__mdt> logCategories = [
			SELECT Category__c
			FROM Log_Category__mdt
			WHERE Log_Profile__c = NULL
			AND Enabled__c = true
		];
		List<String> result = getCategories(logCategories);
		return result;
	}

	@TestVisible
	List<String> getEnabledCategoriesByProfile(String aLogProfile) {
		List<Log_Category__mdt> logCategories = [
			SELECT Category__c
			FROM Log_Category__mdt
			WHERE Log_Profile__c = :aLogProfile
			AND Enabled__c = true
		];
		List<String> result = getCategories(logCategories);
		return result;
	}

	public Boolean log(
		String msg,
		String category,
		Object details
	) {
		Boolean result = false;

		if (details == null) {
			details = 'null';
		}

		if (
			(enabledForAllCategories == true) ||
			enabledCategories.contains(category)
		) {
			Logger.info(
				msg, // msg
				category, // category
				details // details
			);
			result = true;
		}

		return result;
	}

	public Boolean log(
		String msg,
		String category
	) {
		Boolean result = false;

		if (
			(enabledForAllCategories == true) ||
			enabledCategories.contains(category)
		) {
			Logger.info(
				msg, // msg
				category // category
			);
			result = true;
		}

		return result;
	}

}