public without sharing class ComplyAdvantageCheck implements Queueable,Database.AllowsCallouts  
{
    //during the positive status on CA search results, case updated with below status
    private static string caseClosedStatus = 'Closed';
    private static string caseVerifiedClosedStatus = 'Verified and Closed';
    public static final string onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public static final Integer currentYear = System.Today().year();

    ComplyAdvantageSearch complyAdvantageSearch;
  
    public static set<string> positiveStatusList =new set<string>{'potential_match','true_positive','unknown'};
    
    public static boolean run = true;
    
    public ComplyAdvantageCheck(ComplyAdvantageSearch searchInfo) 
    {
        complyAdvantageSearch = searchInfo;
    }
  
    public void execute(QueueableContext context) 
    { 
        boolean complyAdvantageCaseUpdated = false;
        string caseName = 'Comply Advantage Case for ' +  complyAdvantageSearch.searchText;

        Http http = new Http();
        
        //setup request
        HttpRequest request = new HttpRequest();
        string serviceURL = 'https://api.complyadvantage.com/searches?api_key='+ complyAdvantageSearch.settings.apiKey;
        request.setEndpoint(serviceURL);
        request.setMethod('POST');
        
        string requestBody = getRequestBody();
        request.setBody(requestBody); 
        System.debug('Request Body' + requestBody); 
        
        //make service call
        HttpResponse response = http.send(request); 
        System.debug('Response' + response); 
        
        //process response
        if (response.getStatusCode() == 200) 
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
            Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
            
            //get total_matches, share_url 
            if(responseData.get('id') != null)
            {
                complyAdvantageSearch.caSearchId = string.valueOf((integer) responseData.get('id'));
                string reference = (string)responseData.get('ref');
                integer totalHits = (integer)responseData.get('total_matches');
                string matchStatus = (string)responseData.get('match_status');
                string shareUrl = (string)responseData.get('share_url');
                Datetime createdDateTime = Datetime.valueOf((string)responseData.get('created_at')); 
                
                Comply_Advantage_Search__c newComplyAdvantageSearch = new Comply_Advantage_Search__c();
                newComplyAdvantageSearch.Name = complyAdvantageSearch.searchText;
                newComplyAdvantageSearch.Search_Id__c = complyAdvantageSearch.caSearchId;
                newComplyAdvantageSearch.Case__c = complyAdvantageSearch.caseInfo.Id;
                newComplyAdvantageSearch.Match_Status__c = matchStatus;
                newComplyAdvantageSearch.Report_Link__c = shareUrl;
                newComplyAdvantageSearch.fxAccount__c = complyAdvantageSearch.fxAccountId;
                newComplyAdvantageSearch.Is_Monitored__c = true;
                newComplyAdvantageSearch.Entity_Contact__c  = complyAdvantageSearch.entityContactId;
                newComplyAdvantageSearch.Search_Reference__c = reference;
                newComplyAdvantageSearch.Case_Management_Link__c = 'https://app.complyadvantage.com/#/case-management/search/' + reference;
                newComplyAdvantageSearch.Total_Hits__c = totalHits;

                newComplyAdvantageSearch.Search_Text__c = complyAdvantageSearch.searchText;
                if(complyAdvantageSearch.birthDate != null && 
                   complyAdvantageSearch.birthDate.year() > 1900 &&
                   complyAdvantageSearch.birthDate.year() <= currentYear)
                {
                	newComplyAdvantageSearch.Search_Parameter_Birth_Year__c = complyAdvantageSearch.birthDate != null ? string.valueOf(complyAdvantageSearch.birthDate.year()) : null;
                }
                newComplyAdvantageSearch.Search_Parameter_Citizenship_Nationality__c = complyAdvantageSearch.citizenshipNationality;
                newComplyAdvantageSearch.Search_Parameter_Mailing_Country__c = complyAdvantageSearch.mailingCountry;
                newComplyAdvantageSearch.Search_Reason__c = complyAdvantageSearch.searchReason;
                newComplyAdvantageSearch.Is_Alias_Search__c = complyAdvantageSearch.IsAliasSearch;
                
                system.debug('newComplyAdvantageSearch' + newComplyAdvantageSearch);
                insert newComplyAdvantageSearch;
                complyAdvantageSearch.sfSearchRecordId = newComplyAdvantageSearch.Id;
                
                if(totalHits != 0 && positiveStatusList.contains(matchStatus) && complyAdvantageSearch.caseInfo != null)
                { 
                    complyAdvantageSearch.caseInfo.Priority = 'Critical';
                  
					//parent id is the onboarding case Id. check if the onboarding case is created and linked                                    
                    if(complyAdvantageSearch.caseInfo.parentId != null)
                    {
                        if(complyAdvantageSearch.caseInfo.status == caseClosedStatus)
                        {
                            complyAdvantageSearch.caseInfo.status = 'Open';
                        }
                        if(complyAdvantageSearch.caseInfo.status == caseVerifiedClosedStatus)
                        {
                            complyAdvantageSearch.caseInfo.status = 'Re-opened';
                            
                            CaseComment comment = new CaseComment();
                            comment.CommentBody = 'Case Reopened because of New Comply Advantage Search (' + complyAdvantageSearch.searchReason + ')';
                            comment.ParentId = complyAdvantageSearch.caseInfo.Id;
                            Insert comment;
                        }
                    }                    
                    complyAdvantageCaseUpdated = true;
                }

                if(!complyAdvantageSearch.caseInfo.Comply_Advantage_Search_Completed__c)
                {
                	complyAdvantageSearch.caseInfo.Comply_Advantage_Search_Completed__c = true;
                    complyAdvantageCaseUpdated = true;
                }
                if(complyAdvantageSearch.IsAliasSearch == false &&
                   complyAdvantageSearch.entityContactId == null &&
                   complyAdvantageSearch.caseInfo.Subject != caseName)
                {
                    complyAdvantageSearch.caseInfo.Subject = caseName;
                    complyAdvantageCaseUpdated = true;
                }               
                if(complyAdvantageCaseUpdated)
                {
                	update complyAdvantageSearch.caseInfo; 
                }
            }
            
            //retrieve report PDF and attach to the case
            if (!Test.isRunningTest()) 
            {
                system.enqueueJob(new ComplyAdvantageReport(complyAdvantageSearch)); 
            }
        } else {
            Logger.error(
                'Comply advantage check callout error',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response);
        }
    }
    
    private string getRequestBody()
    {
        string birthYearFilter = '';
        string countryfilter = '';
        
        string requestBody = '{"search_term" : "'+ complyAdvantageSearch.searchText +'", ';
        requestBody += '"client_ref" : "'+ complyAdvantageSearch.searchReference + '", ';
        if(string.isNotBlank(complyAdvantageSearch.settings.searchProfileName))
        {
        	requestBody += '"search_profile" : "'+ complyAdvantageSearch.settings.searchProfileName + '", ';
        }
        if(complyAdvantageSearch.settings.fuzziness != null)
        {
            requestBody += '"fuzziness" : "'+ complyAdvantageSearch.settings.fuzziness + '", ';
        }
        requestBody += '"share_url" : 1';

        if(complyAdvantageSearch.birthDate != null &&  
           complyAdvantageSearch.birthDate.year() > 1900 &&
           complyAdvantageSearch.birthDate.year() <= currentYear )
        {
            birthYearFilter += '"birth_year" : "'+ complyAdvantageSearch.birthDate.year() + '"';
        }
        if(complyAdvantageSearch.citizenshipNationality != null || complyAdvantageSearch.mailingCountry != null)
        {
             Country_Setting__c cnCountrySetting =  CustomSettings.getCountrySettingByName(complyAdvantageSearch.citizenshipNationality);
             Country_Setting__c mcCountrySetting = CustomSettings.getCountrySettingByName(complyAdvantageSearch.mailingCountry);
             
             countryfilter += '"country_codes" : [';
             if(cnCountrySetting != null && cnCountrySetting.ISO_Code__c != null)
             {
                 countryfilter += '"' + cnCountrySetting.ISO_Code__c + '"'; 
             }
             if(cnCountrySetting != null && cnCountrySetting.ISO_Code__c != null &&
                mcCountrySetting != null && mcCountrySetting.ISO_Code__c != null)
             {
                 countryfilter += ',';
             }
             if(mcCountrySetting != null && mcCountrySetting.ISO_Code__c != null)
             {
                 countryfilter += '"' + mcCountrySetting.ISO_Code__c + '"'; 
             }
             countryfilter+= ']';
         }
         if(string.isNotBlank(birthYearFilter) || string.isNotBlank(countryfilter))
         {
             requestBody += ',';
             requestBody += '"filters" : {'; 
             if(string.isNotBlank(birthYearFilter))
             {
                requestBody += birthYearFilter;
             }
             if(string.isNotBlank(birthYearFilter) && string.isNotBlank(countryfilter))
             {
                 requestBody += ',';
             }
             if(string.isNotBlank(countryfilter))
             {
                 requestBody += countryfilter;
             }
             requestBody +='}';
         }
         requestBody +='}';
        
        return requestBody;
    }
    
     public static boolean runOnce() 
     {
         if(run)
         {
             run=false;
             return true;
         }
         else
         {
             return run;
         }
    }
}