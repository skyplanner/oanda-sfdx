/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 06-09-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class APISettings {
   public String label { get; private set; }

   public String name { get; private set; }

   public String clientId { get; private set; }

   public String clientSecret { get; private set; }

   public String grantType { get; private set; }

   public String audience { get; private set; }

   public String authorizationUrl { get; private set; }

   public String redirectUrl { get; private set; }

   public String tokenUrl { get; private set; }

   public String userInfoUrl { get; private set; }

   public String baseUrl { get; private set; }

   public String method { get; private set; }

   public String scope { get; private set; }

   public String responseType { get; private set; }

   public String namedCredential { get; private set; }

   public APISettings(API_Settings__mdt apiSetting) {
      label = apiSetting.Label;
      name = apiSetting.DeveloperName;
      clientId = apiSetting.Client_Id__c;
      clientSecret = apiSetting.Client_Secret__c;
      grantType = apiSetting.Grant_Type__c;
      audience = apiSetting.Audience__c;
      authorizationUrl = apiSetting.Authorization_URL__c;
      redirectUrl = apiSetting.Redirect_URL__c;
      tokenUrl = apiSetting.Token_URL__c;
      userInfoUrl = apiSetting.User_Info_URL__c;
      baseUrl = apiSetting.Base_URL__c;
      method = apiSetting.Method__c;
      scope = apiSetting.Scope__c;
      responseType = apiSetting.Response_Type__c;
      namedCredential = apiSetting.Named_Credential__c;
   }
}