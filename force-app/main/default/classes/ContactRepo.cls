/**
 * @File Name          : ContactRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 3:11:43 AM
**/
public inherited sharing class ContactRepo {

	public static Contact getSummaryByEmail(String email) {
		List<Contact> recList = [
			SELECT 
				Name,
				Email
			FROM Contact
			WHERE Email = :email
			LIMIT 1
		];
		Contact result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}
	
}