/**
 * fxAccount assessment util class
 */
public with sharing class fxAccountAssessmentUtil {
   
    // Right division to process        
    static Set<String> divisions =
        new Set<String>{
            Constants.DIVISIONS_OANDA_EUROPE,
            Constants.DIVISIONS_OANDA_EUROPE_MARKETS,
            Constants.DIVISIONS_OANDA_AUSTRALIA};

    @testVisible
    static final String PASS = 'Pass';
    @testVisible
    static final String FAIL = 'Fail';
    @testVisible
    static final String REJECT = 'Reject';

    /**
     * Constructor
     */
    public fxAccountAssessmentUtil() {}

    /**
     * Know if is a right division to do the calculation
     * TODO: use this function in "AssessmentFormController" to avoid 
     * "{!v.fxAccount.Division_Name__c == 'OANDA Europe' ||
     *    v.fxAccount.Division_Name__c == 'OANDA Europe Markets' ||
     *    v.fxAccount.Division_Name__c == 'OANDA Australia'}"
     */
    public static Boolean isRightDivision(
        fxAccount__c fxa)
    {
        return divisions.contains(
            fxa.Division_Name__c);
    }

    public static Boolean isLiveAccount(fxAccount__c fxa) 
    {
        return 
            // calculate only live fxAccounts
            fxa.RecordTypeId == RecordTypeUtil.getFxAccountLiveId();
    }

    /**
     * Calculate assessment result
     */
    public void setAssessmentResult(List<fxAccount__c> fxAccounts) 
    {
       for(fxAccount__c fxa :fxAccounts)
        {
            // Calculate if is one of the right divisions
            if (isRightDivision(fxa) && isLiveAccount(fxa))
           {
                if(fxAccountUtil.isOAUDiv(fxa) &&
                    String.isNotBlank(fxa.Knowledge_Result__c) &&
                    (String.isBlank(fxa.OEL_Assessment_Result__c) || 
                    fxa.Experience_Trading_CFDs_or_FX__c == true ||
                    fxa.Other_Relevant_Experience__c == true)) {
                    // Set assessment results for Australia
                    setAssessmentResultForAustralia(fxa);
                } else if(fxAccountUtil.isEuropeDiv(fxa) &&
                    String.isNotBlank(fxa.OEL_Knowledge_Result__c) &&
                    // SP-9301, aviod calculating risk when
                    fxa.Trading_Experience_Duration__c != null && 
                    fxa.Trading_Experience_Duration__c != '0' &&
                    fxa.Trading_Experience_Type__c != null &&
                    fxa.Trading_Experience_Type__c != '0' &&
                    (String.isBlank(fxa.OEL_Assessment_Result__c) || 
                    fxa.Traded_Products_50_Times__c == true)) {
                        // Set assessment results for Europe
                        setAssessmentResultForEurope(fxa);
                }   
            }
       }
    }

    /**
     * Calculate assessment result for 'Australia' division
     * 
     * Results
     * - Experience_Trading_CFDs_or_FX__c or Other_Relevant_Experience__c is YES  => pass
     * - Experience_Trading_CFDs_or_FX__c and Other_Relevant_Experience__c are NOT => fail
     */ 
    private void setAssessmentResultForAustralia(
       fxAccount__c fxa) 
    {
        Boolean tradingExpPass = false;
        Integer noCorrectAns =
            fxa.No_of_correct_OEL_Ans__c.intValue();
      
        // Experience Result

        if(fxa.Experience_Trading_CFDs_or_FX__c ||
            fxa.Other_Relevant_Experience__c) {
                fxa.OEL_Trading_Experience_Result__c = PASS;  
                tradingExpPass = true;
        } else {
            fxa.OEL_Trading_Experience_Result__c = FAIL;
        }

        // Overall Result

        if(tradingExpPass) {
            if(noCorrectAns >= 0 && noCorrectAns < 5){
                fxa.OEL_Assessment_Result__c = REJECT;
            } else if(noCorrectAns > 4 && noCorrectAns < 7) {
                fxa.OEL_Assessment_Result__c = FAIL;
            } else if(noCorrectAns > 6 && noCorrectAns < 10) {
                fxa.OEL_Assessment_Result__c = PASS;
            }
        } else {
            if(noCorrectAns >= 0 && noCorrectAns < 5){
                fxa.OEL_Assessment_Result__c = REJECT;
            } else if(noCorrectAns > 4 && noCorrectAns < 7) {
                fxa.OEL_Assessment_Result__c = FAIL;
            } else if(noCorrectAns > 6 && noCorrectAns < 10) {
                fxa.OEL_Assessment_Result__c = FAIL;
            }
        }
    }

    /**
     * Calculate assessment result
     * for 'Europe' divisions
     */ 
    private void setAssessmentResultForEurope(
       fxAccount__c fxa) 
    {
        // Split the fxAccount Duration and Type codes
        List<String> fxaDurations =
            fxa.Trading_Experience_Duration__c.split(',');
        List<String> fxaTypes =
            fxa.Trading_Experience_Type__c.split(',');
        Map<String,Integer> tradingExpMap =
            new Map<String, Integer>();

        for(Integer i = 0; i<fxaTypes.size(); i++) 
        {
            tradingExpMap.put(
                fxaTypes[i],
                Integer.valueOf(fxaDurations[i]));
        }

        if (tradingExpMap.get('201') > 15 ||
            tradingExpMap.get('202') > 15 ||
            tradingExpMap.get('203') > 15) 
        {
            fxa.OEL_Trading_Experience_Result__c= PASS;
        } 
        else if (tradingExpMap.get('204') > 30)
        {
            fxa.OEL_Trading_Experience_Result__c= PASS;
        }
        else
        {
            fxa.OEL_Trading_Experience_Result__c = 
            (fxa.Traded_Products_50_Times__c == true) ? PASS : FAIL;
        }
        
        if( fxa.No_of_correct_OEL_Ans__c <= 4)
        {
           fxa.OEL_Assessment_Result__c = REJECT;
        }
        else if( fxa.No_of_correct_OEL_Ans__c== 5 || fxa.No_of_correct_OEL_Ans__c== 6)
        {
            fxa.OEL_Assessment_Result__c = FAIL;
        }
        else if(fxa.OEL_Trading_Experience_Result__c == PASS && fxa.No_of_correct_OEL_Ans__c>= 7)
        {
            fxa.OEL_Assessment_Result__c = PASS;
        }
        else
        {
            fxa.OEL_Assessment_Result__c = FAIL;
        }
    }

    
}