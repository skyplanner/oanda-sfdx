/**
 * Created by Agnieszka Kajda on 21/09/2023.
 */
@IsTest
private class BatchSendFailedCKAEmailsTest {
    @IsTest
    static void sendEmailCreateCaseTest() {

        Account a = new Account(LastName = 'test account', PersonEmail='test@test.com', Send_Failed_CKA_Email__c=true);
        insert a;

        Test.startTest();
        BatchSendFailedCKAEmails b = new BatchSendFailedCKAEmails();
        Database.executeBatch(b, 10);
        Test.stopTest();

        Case newCase = [SELECT Id, Type__c FROM Case LIMIT 1];
        Account acc = [SELECT Send_Failed_CKA_Email__c FROM Account WHERE Id=:a.Id];

        System.assertNotEquals(newCase, null);
        System.assertEquals('Suitability', newCase.Type__c);
        System.assertEquals(false, acc.Send_Failed_CKA_Email__c);
    }
}