/**
 * @File Name          : FxAccLastTradeDateChangedHandler_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/27/2020, 6:22:50 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   acantero     Initial Version
**/
@isTest
private class FxAccLastTradeDateChangedHandler_Test {

    @isTest
    static void onLastTradeDateChanged_test() {
        Boolean error = false;
        List<ID> fxAccountIdList = MIFIDValidationTestDataFactory.getValidableFxAccountsIdList();
        Test.startTest();
        try {
            FxAccountLastTradeDateChangedHandler.onLastTradeDateChanged(fxAccountIdList);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);

    }
}