/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-20-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ScheduledEmailSettingManager_Test {
	@isTest
	private static void getSetting() {
		ScheduledEmailSettingManager manager = new ScheduledEmailSettingManager();
		IScheduledEmailSetting setting = manager.getSetting();
		System.assert(setting != null, 'Can not be null');
		Schedule_Mail_Setting__mdt settingMtd = ScheduleMailSettingRepo.getFirst();
		System.assertEquals(
			settingMtd.Scope__c,
			setting.getScope(),
			'Ten is the value by default'
		);
		System.assertEquals(
			settingMtd.AM_or_PM__c,
			setting.getAMOrPM(),
			'PM is the value by default'
		);
		System.assertEquals(
			settingMtd.Delete_a_Draft_Email__c,
			setting.getAllowDelete(),
			'false is the value by default'
		);
		System.assertEquals(
			settingMtd.Time__c,
			setting.getTime(),
			'4 is the value by default'
		);
	}
}