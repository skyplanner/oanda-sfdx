@IsTest
public with sharing class DocumentApiRestServiceTest {

    private static string noUserUrl = '/api/v1/user/9999/document';
    private static string validUrl = '/api/v1/user/123123123/document';
    private static Integer testUserId = 123123123;
    private static string postMethod = 'POST';
    private static string getMethod = 'POST';
    
    @TestSetup
    static void makeData(){
        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;


        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 123123123;
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        insert fxAccount;
    }

    @isTest
    static void testGetDocNoUserExists() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = noUserUrl;  
        req.httpMethod = getMethod;
            
        RestContext.request = req;
        RestContext.response = res;
        
        DocumentApiRestService.doGet();
    
        Assert.areEqual(404, res.statusCode, 'there is no user');
    }

    @isTest
    static void testGetDocOK() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = validUrl;  
        req.httpMethod = getMethod;

        Document__c docObj = new Document__c();
        docObj.name = 'Passport';
        docObj.fxAccount__c = [SELECT Id FROM fxAccount__c WHERE fxTrade_User_ID__c = : testUserId].Id;
        docObj.Document_Type__c = 'Passport';
        docObj.Expiry_Date__c = Date.today();
        insert docObj;
            
        RestContext.request = req;
        RestContext.response = res;
        
        DocumentApiRestService.doGet();
    
        Assert.areEqual(200, res.statusCode, 'ok');
    }

    @isTest
    static void testPostDocInvalidrequestMissingDataId() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = validUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(generateFile(true, 2));
    
        RestContext.request = req;
        RestContext.response = res;
        
        DocumentApiRestService.doPost();
    
        Assert.areEqual(400, res.statusCode, 'Bad request Journey Id is required');
    }


    @isTest
    static void testPostDocOK() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = validUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(generateFile(false, 2));
    
        RestContext.request = req;
        RestContext.response = res;
        
        DocumentApiRestService.doPost();
    
        Assert.areEqual(200, res.statusCode, 'ok');
    }

    @isTest
    static void testPostDocOKJourneyExistst() {

        fxAccount__c fxa = [SELECT Id FROM fxAccount__c WHERE fxTrade_User_ID__c = : testUserId];

        Document__c docObj = new Document__c();
        docObj.name = 'Passport';
        docObj.fxAccount__c = fxa.Id;
        docObj.Document_Type__c = 'Passport';
        docObj.Expiry_Date__c = Date.today();
        docObj.Journey_Id__c = 'b7b66e41-ea27-45f7-bf25-123123123';
        insert docObj;

        Assert.areEqual(true, [SELECT id from Attachment where ParentId =: docObj.Id].isEmpty(), 'ok');

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = validUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(generateFile(false, 2));
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        DocumentApiRestService.doPost();
        Test.stopTest();
        Assert.areEqual(200, res.statusCode, 'ok');

        Assert.areEqual(1, [SELECT id, fxAccount__c from Document__c where fxAccount__c =: fxa.Id].size(), 'ok');
        Assert.areEqual(1, [SELECT id, ParentId from Attachment where ParentId =: docObj.Id].size(), 'ok');
    }

    @isTest
    static void testPostDocOKNoDocType() {

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = validUrl;  
        req.httpMethod = postMethod;

        req.requestBody = Blob.valueOf(generateFile(false, 99999));
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        DocumentApiRestService.doPost();
        Test.stopTest();
        Assert.areEqual(200, res.statusCode, 'ok');
        Assert.areEqual(true, res.responseBody.toString().contains('Document inserted correctly'), 'ok');

    }

    @isTest
    static void testInsertAttachemntFail() {

        try { 
            DocumentApiRestService.insertAttachemnt(null, null, null, null, '123', 123123);
        } catch (Exception ex) { 
            Assert.isTrue(ex.getMessage().contains('123123'),'User Id present in err msg');
        }

    }


    @isTest
    static void testInsertAttachemntok() {
        Document__c docObj = new Document__c();
        docObj.name = 'Passport';
        docObj.fxAccount__c = [SELECT Id FROM fxAccount__c WHERE fxTrade_User_ID__c = : testUserId].Id;
        docObj.Document_Type__c = 'Passport';
        docObj.Expiry_Date__c = Date.today();
        insert docObj;
        try { 
            DocumentApiRestService.insertAttachemnt(docObj.id, 'bodyString', 'test.pdf', null, '123', 123123);
        } catch (Exception ex) { 
            Assert.isTrue(true);
        }

    }

    private static String generateFile(Boolean skipJourneyId, Integer docNumber){
        JSONGenerator generator = JSON.createGenerator(true); 
        generator.writeStartObject();
        generator.writeNumberField('doc_type', docNumber);
        generator.writeStringField('file_name', 'testFile1.pdf');
        generator.writeStringField('img_data', 'moNMjMgMCBvYmoNPDwvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aCAxMD4+c3RyZWFtD');
        generator.writeStringField('mime_type', 'application/pdf');
        if(!skipJourneyId) {
            generator.writeStringField('journey_id', 'b7b66e41-ea27-45f7-bf25-123123123');
        }
        generator.writeStringField('activation_decision', '{\'pass\': true, \'failure_reasons\': [\'blurry\'], \'user_check\': {\'match_result\': \'skipped\'}, \'gbg_checks\': {\'high_level_result_pass\': true, \'is_validated_pass\': true, \'digital_tampering_pass\': false}, \'face_sub_check\': false}');
        generator.writeEndObject();
        return generator.getAsString();
    }
}