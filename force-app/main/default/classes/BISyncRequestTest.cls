/*
 *	Author : Deepak Malkani.
 *	Created Date : June 24 2016.
 *	Purpose : Test class with all test scanrios for BI Sync Request Web service class.
*/

@isTest(SeeAllData=false)
private class BISyncRequestTest
{
	
	static TestMethod void UnitTest1_requestBISync_SysAdmin()
	{
		// INITIALISE 
		TestDataFactory testHandler = new TestDataFactory();

		// PREPARE
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);

		// INVOKE TESTING
		Test.startTest();
		//system.assertEquals(false, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
		system.runAs(new User(Id = UserInfo.getUserId())){
			BISyncRequest.requestBISync(tradeAccount.Id);		
		}
		Test.stopTest();
		
		// ASSERTIONS AND VALIDATIONS
		system.assertEquals(true, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
	}

	static TestMethod void UnitTest2_requestBISync_NonSynAdmin(){

		// INITIALISE
		TestDataFactory testHandler = new TestDataFactory();

		// PREPARE
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
		Profile p = [SELECT id, Name FROM Profile WHERE Name = 'Forex Specialist' LIMIT 1];
		User usr = testHandler.createUser(p);

		// INVOKE TESTING
		Test.startTest();
		//system.assertEquals(false, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
		system.runAs(usr){
			BISyncRequest.requestBISync(tradeAccount.Id);	
		}
		Test.stopTest();

		// ASSERTIONS AND VALIDATIONS
		system.assertEquals(true, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
	}

	static TestMethod void UnitTest3_requestBISync_NoFLSAccessUsr(){


		// INITIALISE
		TestDataFactory testHandler = new TestDataFactory();

		// PREPARE
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
		Profile p = [SELECT id, Name FROM Profile WHERE Name = 'CX' LIMIT 1];
		User usr = testHandler.createUser(p);

		// INVOKE TESTING
		Test.startTest();
		//system.assertEquals(false, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
		system.runAs(usr){
			//before invoking the service, we assert that this field is not accessible to the user
			system.assertEquals(false, Schema.SObjectType.fxAccount__c.fields.BI_Sync__c.isAccessible());
			try{
				BISyncRequest.requestBISync(tradeAccount.Id);
			}
			catch(Exception ex){
				system.assertEquals(null, ex.getMessage());
			}
		}
		Test.stopTest();

		// ASSERTIONS AND VALIDATIONS
		system.assertEquals(true, [SELECT BI_Sync__c FROM fxAccount__c WHERE Id =: tradeAccount.Id LIMIT 1].BI_Sync__c);
	}

	static TestMethod void UnitTest4_Negative_BadDataTest(){

		// INITIALISE
		TestDataFactory testHandler = new TestDataFactory();
		String result;
		// PREPARE
		Profile p = [SELECT id, Name FROM Profile WHERE Name = 'CX' LIMIT 1];
		User usr = testHandler.createUser(p);

		// INVOKE TESTING
		Test.startTest();
		system.runAs(usr){
			//before invoking the service, we assert that this field is not accessible to the user
			system.assertEquals(false, Schema.SObjectType.fxAccount__c.fields.BI_Sync__c.isAccessible());
			Schema.DescribeSObjectResult r = fxAccount__c.sObjectType.getDescribe();
			String keyPrefix = r.getKeyPrefix();
			String fakeId = keyPrefix+'000000000ABc';
			result = BISyncRequest.requestBISync(fakeId); //In this case the account Id does not even exist, so update will surely fail.
		}
		Test.stopTest();

		// ASSERTIONS AND VALIDATIONS
		system.assert(result.contains('Update failed'));
		
	}

}