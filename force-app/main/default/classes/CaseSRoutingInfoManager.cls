/**
 * @File Name          : CaseSRoutingInfoManager.cls
 * @Description        : 
 * Creates ServiceRoutingInfo records from PendingServiceRouting records
 * linked to Case object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/21/2024, 1:19:20 AM
**/
public inherited sharing class CaseSRoutingInfoManager 
	extends CommonSRoutingInfoManager 
	implements SWorkItemUpdater, SRoutingLogCreator {

	AccountSegmentationManager segmentationManager;

	public CaseSRoutingInfoManager() {
		super();
		this.segmentationManager = new AccountSegmentationManager();
	}

	protected override List<SObject> getWorkItems(Set<ID> workItemIdSet) {
		return [
			SELECT
				Live_or_Practice__c,
				Inquiry_Nature__c,
				Type__c,
				Subtype__c,
				Complaint__c,
				Origin,
				Language,
				Spot_Crypto__c,
				Priority,
				CCM_CX__c,
				Language_Corrected__c,
				Internally_Routed__c,
				Tier__c,
				Ready_For_Routing__c,
				Routed__c,
				SourceId,
				Account.Id, 
				Account.PersonEmail,
				Account.Primary_Division_Name__c,
				Account.Language_Preference__pc,
				Account.Is_High_Value_Customer__c,
				Account.Funnel_Stage__pc,
				Account.Last_Trade_Date__c,
				Account.fxAccount__c,
				Account.CreatedDate,
				Lead__r.Id,
				Lead__r.Email,
				Lead__r.fxAccount__c,
				Lead__r.Language_Preference__c,
				Lead__r.Division_from_Region__c,
				Lead__r.CreatedDate
			FROM Case
			WHERE Id IN :workItemIdSet
		];
	}

	protected override void processWorkItems(List<SObject> workItems) {
		if (workItems.isEmpty()) {
			return;
		}
		// else...
		for (SObject rec : workItems) {
			Case caseObj = (Case) rec;
			ID fxAccountId = AccountSegmentationManager.getValidFxAccount(
				caseObj.Account, // accountObj 
				caseObj.Lead__r // leadObj
			);
			segmentationManager.registerFxAccount(fxAccountId);
		}
	}

	@TestVisible
	protected override BaseServiceRoutingInfo getRoutingInfo(
		PendingServiceRouting routingObj, 
		SObject workItem
	) {
		Case caseObj = (Case) workItem;
		ID fxAccountId = AccountSegmentationManager.getValidFxAccount(
			caseObj.Account, // accountObj 
			caseObj.Lead__r // leadObj
		);
		Segmentation__c segmentation = 
			segmentationManager.getSegmentation(fxAccountId);

		ServiceRoutingInfoFactory factory = 
			ServiceRoutingInfoFactory.getInstance();
			
		ServiceRoutingInfo result = factory.createNewRecord(
			routingObj, // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			caseObj // workItem
		);
		result.workItemUpdater = this;
		result.routingLogCreator = this;
		result.initTier(caseObj.Tier__c);
		
		// Segmentation must be assigned before setting up the account or lead
		result.segmentation = segmentation;
		
		// checkCustomerInfo = false because
		// checkCustomerType is called directly 
		// at the end of field initialization
		result.setRelatedLead(
			caseObj.Lead__r, // leadObj
			false // checkCustomerInfo
		);
		result.setRelatedAccount(
			caseObj.Account, // accountObj
			false // checkCustomerInfo
		);
		result.accountType = caseObj.Live_or_Practice__c;
		result.inquiryNature = caseObj.Inquiry_Nature__c;
		result.type = caseObj.Type__c;
		result.subtype = caseObj.Subtype__c;
		result.isAComplaint = caseObj.Complaint__c;
		result.crypto = (caseObj.Spot_Crypto__c == true);
		result.priority = caseObj.Priority;
		result.ccmCx = caseObj.CCM_CX__c;
		//DO NOT REMOVE
		// result.isEmpty = (
		//     String.isBlank(caseObj.Subject) &&
		//     String.isBlank(caseObj.Description)
		// );
		if (caseObj.Account != null) {
			result.division = caseObj.Account.Primary_Division_Name__c;

		} else if (caseObj.Lead__r != null) {
			result.division = caseObj.Lead__r.Division_from_Region__c;
		}

		CaseSRoutingInfoHelper.setLanguagesFields(
			result, // routingInfo
			caseObj // caseObj
		);
		//...
		result.checkCustomerType();

		ServiceLogManager.getInstance().log(
			'getRoutingInfo -> result', // msg
			CaseSRoutingInfoManager.class.getName(), // category
			result // details
		);

		return result;
	}

	public List<SObject> getRecordsToUpdate(BaseServiceRoutingInfo routingInfo) {
		ServiceRoutingInfo sRoutingInfo = (ServiceRoutingInfo) routingInfo;
		Case caseObj = (Case) sRoutingInfo.workItem;
		List<SObject> result = new List<SObject>();

		if (
			(caseObj.Ready_For_Routing__c == false) ||
			(caseObj.Routed__c == false) ||
			(sRoutingInfo.tierHasChanged == true)
		) {
			result.add(
				new Case(
					Id = sRoutingInfo.workItem.Id,
					Ready_For_Routing__c = true,
					Routed__c = true,
					Tier__c = sRoutingInfo.tier
				)
			);
		}
		
		Boolean updateMsgSession = (
			(sRoutingInfo.tierHasChanged == true) &&
			(caseObj.SourceId != null) &&
			(
				caseObj.SourceId.getSobjectType() == 
				Schema.MessagingSession.SObjectType
			)
		);
		checkMsgSessionUpdate(
			updateMsgSession, // updateMsgSession
			result, // recsToUpdate
			caseObj.SourceId, // msgSessionId
			sRoutingInfo.tier // newTier
		);
		return result;
	}

	@TestVisible
	Boolean checkMsgSessionUpdate(
		Boolean updateMsgSession,
		List<SObject> recsToUpdate,
		ID msgSessionId,
		String newTier
	) {
		if (updateMsgSession == true) {
			recsToUpdate.add(
				new MessagingSession(
					Id = msgSessionId,
					Tier__c = newTier
				)
			);
		}
		
		return updateMsgSession;
	}

	public SObject getRoutingLog(BaseServiceRoutingInfo routingInfo) {
		SObject result = 
			new RoutingLogManager(routingInfo).getLogForCase();
		return result;
	}

}