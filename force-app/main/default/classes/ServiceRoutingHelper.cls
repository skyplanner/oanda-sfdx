/**
 * @File Name          : ServiceRoutingHelper.cls
 * @Description        : 
 * Methods that allow you to customize the routing performed by Omnichannel
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:37:10 AM
**/
public without sharing class ServiceRoutingHelper 
	extends BaseServiceRoutingHelper {

	OmnichanelSettings settings;

	public ServiceRoutingHelper(List<BaseServiceRoutingInfo> infoList) {
		super(infoList);
		this.settings = OmnichanelSettings.getRequiredDefault();
		completeInformation();
	}

	public override List<SkillRequirement> getSkillRequirements() {
		/**
		 * It is necessary to invoke "completeInformation" again because the leads
		 * created in ChatTranscript sessions are not available in the 
		 * before-insert of the PendingServiceRouting, therefore the search and
		 * mapping of those leads can only be executed successfully in the 
		 * after-insert.
		 */
		completeInformation();
		return super.getSkillRequirements();
	}

	public override Boolean updateWorkItems() {
		List<SObject> toBeUpdated = new List<SObject>();
		
		for (BaseServiceRoutingInfo info : infoList) {
			ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) info;
			List<SObject> recordsToUpdate = 
				routingInfo.workItemUpdater?.getRecordsToUpdate(routingInfo);

			if (
				(recordsToUpdate != null) &&
				(recordsToUpdate.isEmpty() == false)
			) {
				toBeUpdated.addAll(recordsToUpdate);
			}
		}

		update toBeUpdated;
		return true;
	}

	public override Boolean createRoutingLogs() {
		List<SObject> recordsToInsert = new List<SObject>();
		
		for (BaseServiceRoutingInfo info : infoList) {
			ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) info;
			SObject routingLog = 
				routingInfo?.routingLogCreator.getRoutingLog(routingInfo);

			if (routingLog != null) {
				recordsToInsert.add(routingLog);
			}
		}

		insert recordsToInsert;
		return true;
	}

	@testVisible
	void completeInformation() {
		PrepareServiceRoutingInfoCmd prepareInfoCmd = 
			new PrepareServiceRoutingInfoCmd(infoList);
		prepareInfoCmd.execute();
	}

	@TestVisible
	protected override Integer getDropAdditionalSkillsTimeout(
		BaseServiceRoutingInfo info
	) {
		Integer result = null;
		ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) info;
		if (
			(routingInfo.dropAdditionalSkills == true) &&
			(settings.dropAdditionalSkillsAreEnabled == true)
		) {
			result = settings.dropAdditionalSkillsTimeOut;
		}
		return result;
	}

}