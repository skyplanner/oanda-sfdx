/**
 * Util class for test methods
 */
public with sharing class TestsUtil {
    
    // To on/off forcing jobs to run sync
    public static Boolean forceJobsSync = false;

    // Add more test configurations here...

}