/**
 * @File Name          : ServiceChannelHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 12:26:30 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/22/2021, 12:25:27 AM   acantero     Initial Version
**/
@IsTest
private without sharing class ServiceChannelHelper_Test {

	// @testSetup
    // static void setup() {
    // }

    //blank devName => throw exception
    @IsTest
    static void getByDevName1() {
        Boolean error = false;
        Test.startTest();
        try {
            ServiceChannel result = 
                ServiceChannelHelper.getByDevName(null, true);
            //...
        } catch (ServiceChannelHelper.ServiceChannelHException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }

    //invalid devName => throw exception
    @IsTest
    static void getByDevName2() {
        Boolean error = false;
        Test.startTest();
        try {
            ServiceChannel result = 
                ServiceChannelHelper.getByDevName('fakeDevName', true);
            //...
        } catch (ServiceChannelHelper.ServiceChannelHException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
    
}