/**
 * @File Name          : NF_CustomLiveAgentPreChat.cls
 * @Description        : This class is used to get custom details on live chat sneak peak
 * @Author             : Ashish Jethi
 * @CreatedBy          : NeuraFlash, LLC
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    4/25/2019, 3:09:42 PM       Ashish Jethi              Initial Version
**/
global with sharing class NF_CustomLiveAgentPreChat extends nfchat.DefaultLiveAgentPreChat implements nfchat.LiveAgentPrechatProcessor {
    
    
    /**
    * @description : This method is used to get custom details on mouse sneak peak over chat accept button
    * @param nfchat.LiveAgentObject data
    * @return void
    */
    global override void addPreChatDetails(nfchat.LiveAgentObject data) {
        try{
            String chatLogId = super.getChatLogId(data);
            System.debug('>>NF_CustomLiveAgentPreChat: chatLogId='+chatLogId);
            String lastBusinessIntent;
            if(String.isNotEmpty(chatLogId)){
                List<nfchat__Chat_Log_Detail__c> chatLogDetails = [SELECT Id, nfchat__Request__c, nfchat__First_Recognition_Result__c, nfchat__Intent_Name__c
                                                                   FROM nfchat__Chat_Log_Detail__c
                                                                    WHERE nfchat__Chat_Log__c =: chatLogId
                                                                    ORDER BY CreatedDate DESC];

                for(nfchat__Chat_Log_Detail__c chatLogDetail : chatLogDetails){
                    System.debug('>>NF_CustomLiveAgentPreChat: chatLogDetails='+chatLogDetails);
                    if(!String.isEmpty(chatLogDetail.nfchat__Intent_Name__c)){
                        System.debug('>>NF_CustomLiveAgentPreChat: nfchat__Intent_Name__c='+chatLogDetail.nfchat__Intent_Name__c);
                        if(chatLogDetail.nfchat__Intent_Name__c.toLowerCase().indexOf('business.') == 0){
                            String temp = chatLogDetail.nfchat__Intent_Name__c.toLowerCase();
                            String tempName = '';
                            List<String>tempList = temp.split('\\.');
                            for(String s : tempList){
                                if(s != 'business' && s != 'direct' && s != 'end' && s != 'start'){
                                    tempName = tempName + ' ' + s.capitalize();
                                }
                            }
                            tempName = tempName.replace('_',' ');
                            lastBusinessIntent = tempName;
                            System.debug('>>NF_CustomLiveAgentPreChat: lastBusinessIntent='+lastBusinessIntent);
                            break;
                        }
                    }
                }
                if(String.isBlank(lastBusinessIntent)){
                    lastBusinessIntent = 'None';
                }
                //displaying cases on console search result
                List<Case> listCases = [SELECT id, casenumber, Account.Date_Customer_Became_Core__c,
                                        Chat_Type_of_Account__c, Chat_Language_Preference__c,
                                        User_Authenticated__c 
                                        FROM case 
                                        WHERE nfchat__Chat_Log__c  =: chatLogId limit 1];
                System.debug('>>NF_CustomLiveAgentPreChat: listCases='+listCases);

                for (case cs : listCases) {
                    System.debug('>>NF_CustomLiveAgentPreChat: cs='+cs);
                    String authStatus;
                    if(cs.User_Authenticated__c == true){
                        authStatus = 'Authenticated';
                    } else{
                        authStatus = 'Not Authenticated';
                    }
                    String hvcCheck = String.valueOf(cs.Account.Date_Customer_Became_Core__c);
                    System.debug('>>NF_CustomLiveAgentPreChat: hvcCheck='+hvcCheck);
                    List<nfchat.LiveAgentObject.EntityMap> entity = new List<nfchat.LiveAgentObject.EntityMap>();
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Client Language', cs.Chat_Language_Preference__c , entity, new List<String>(), true));
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Last Business Intent', lastBusinessIntent , entity, new List<String>(), true));
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Client Segment', cs.Chat_Type_of_Account__c , entity, new List<String>(), true));
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Is HVC?', String.isNotBlank(hvcCheck) == true ?'Yes':'No' , entity, new List<String>(), true));
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Authentication Status', authStatus , entity, new List<String>(), true));
                    
                    List<nfchat.LiveAgentObject.EntityMap> caseEntity = new List<nfchat.LiveAgentObject.EntityMap>();
                    caseEntity.add(new nfchat.LiveAgentObject.EntityMap('Case', 'CaseNumber', true, true, true));
                    data.addPreChatDetails(new nfchat.LiveAgentObject.CustomDetail('Case.CaseNumber', cs.CaseNumber , caseEntity, new List<String>(), false));
                
                    System.debug('>>NF_CustomLiveAgentPreChat: added CaseEntity='+caseEntity);
                }
            }
            else{
                System.debug('NF_CustomLiveAgentPreChat: no Chat Log to query!');
            }
        }
        catch(Exception e){
            System.debug('Error in NF_CustomLiveAgentPreChat: '+e.getMessage());
        }
    }
}