/**
 * @File Name          : BaseLiveChatCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/13/2022, 11:35:00 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/11/2022, 10:39:34 AM   acantero     Initial Version
**/
public without sharing virtual class BaseLiveChatCtrl extends HelpPortalBase {

    public BaseLiveChatCtrl() {
        super();
    }

    @TestVisible
    protected String updateLanguageFromParams() {
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
		String langPref = pageParams.get(ChatConst.LANGUAGE_PREFERENCE);
		if (String.isBlank(langPref)) {
			langPref = pageParams.get(ChatConst.LANGUAGE_PREFERENCE_OFFLINE);
		}
		if(String.isNotBlank(langPref)) {
			language = langPref;
		}
        return language;
    }

    public Boolean offlineHours {
		get {
			if (offlineHours == null) {
				offlineHours = checkOfflineHours();
			}
            return offlineHours;
		}
		private set;
	}

    public static Boolean checkOfflineHours() {
        Boolean result = false;
        ChatOfflineSettings chatSetting = ChatOfflineSettings.getDefaultInstance();
        if(chatSetting.initializationFails != true) {
            result = chatSetting.isOffline;
        }
        return result;
    }

}