/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-21-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserGetResponse {
    public static final String PREMIUM_ACCESS_KEY = 'can_access_premium';

    public String one_id {get; set;}

    public String country_code {get; set;}

    public String country {get; set;}

    public UserApiUserStatus user_status {get; set;}

    public UserApiSearchItem user {get; set;}

    public UserApiUserAuthorization user_authorization {get; set;}

    public UserApiAuxUser aux_user {get; set;}
    
    public List<UserApiAddress> contacts_address {get; set;}

    public List<UserApiPhone> contacts_phone {get; set;}
    
    public UserApiDivision division {get; set;}

    public List<UserApiForeignResidence> foreign_residence {get; set;}

    public UserApiLanguage language {get; set;}

    public UserApiOapRegulatoryInfo oap_regulatory_info {get; set;}

    public List<UserApiTradingExpirience> trading_experience {get; set;}

    public List<UserApiUserPrivilege> user_privileges {get; set;}

    public transient Boolean premiumEnabled {
        get {           
            if (user_privileges == null)
                return false;
                
            for (Integer i = user_privileges.size() - 1; i >= 0; i--) {
                UserApiUserPrivilege p = user_privileges[i];
                //for (UserApiUserPrivilege p : user_privileges) 
                if (p.name == PREMIUM_ACCESS_KEY)
                    return p.enabled;
            }
            return false;
        }
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = user.toMap();
        UserApiAddress address = 
            contacts_address == null || contacts_address.isEmpty()
            ? null : contacts_address[0];

        result.put(Constants.API_FIELD_DIVISION, 
            division?.division_id + ' - ' + division?.description);
        result.put(Constants.API_FIELD_PREMIUM_ACCESS, premiumEnabled);
        result.put(Constants.API_FIELD_COUNTRY, address?.country);
        result.put(Constants.API_FIELD_REGION, address?.region);
        result.put(Constants.API_FIELD_CITY, address?.city);
        result.put(Constants.API_FIELD_STREET1, address?.address1);
        result.put(Constants.API_FIELD_STREET2, address?.address2);
        result.put(Constants.API_FIELD_POSTAL_CODE, address?.postcode);

        return result;
    }
}