/**
 * Scheduler class for the BatchDocumentsCleaner
 * @author Ariel Cantero, Skyplanner LLC
 * @since 07/10/2019
 */
global without sharing class DocumentsCleanerScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
		BatchDocumentsCleaner docCleanerBatch = new BatchDocumentsCleaner(false);
		if (docCleanerBatch.isValid) {
			Database.executeBatch(docCleanerBatch, BatchDocumentsCleaner.BATCH_EXECUTION_SCOPE);
		}
	}
	
}