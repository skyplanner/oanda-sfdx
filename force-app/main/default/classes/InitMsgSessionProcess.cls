/**
 * @File Name          : InitMsgSessionProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/9/2024, 3:37:40 AM
**/
public inherited sharing class InitMsgSessionProcess {

	final Boolean enhancedChannel;
	final MessagingSession session;
	final ProcessResult result;

	public String channelType {get; set;}
	public String countryCode {get; set;}

	@TestVisible
	MessagingChannelSettings channelSettings;

	@TestVisible
	String region;

	public InitMsgSessionProcess(
		String messagingSessionId,
		Boolean enhancedChannel
	) {
		this(
			MessagingSessionRepo.getInitInfoById(messagingSessionId), // session
			enhancedChannel // enhancedChannel
		);
	}

	public InitMsgSessionProcess(
		MessagingSession session,
		Boolean enhancedChannel
	) {
		this.session = session;
		this.enhancedChannel = enhancedChannel;
		this.channelType = session?.ChannelType;
		this.result = new ProcessResult();
	}
	
	public ProcessResult execute() {
		if (session == null) {
			return result;
		}
		// else...
		if (session.Initialized__c == false) {
			initSession();
		}
		result.region = session.Region__c;
		result.caseNumber = session.Case.CaseNumber;
		result.caseIsClosed = session.Case.IsClosed;
		result.userEmail = session.User_Email__c;
		return result;
	}

	@TestVisible
	void checkChannelSettings() {
		if (channelSettings == null) {
			channelSettings = MessagingChannelSettings.getByChannel(
				channelType, // channelType 
				enhancedChannel // enhancedChannel
			);
		}
	}

	@TestVisible
	void initSession() {
		checkChannelSettings();

		if (channelSettings.extraInfo == true) {
			initRegionFromExtraInfo();

		} else if (channelSettings.isoCountryCode == true) {
			initRegionFromIsoCountryCode();
		}

		checkRegion(session?.MessagingEndUser?.Country_Code__c);
		session.Region__c = region;
		session.Case_Record_Type__c = SPSettingsManager.getSetting(
			MessagingConst.CASE_RECORD_TYPE_DEV_NAME_SETTING
		);
		session.Initialized__c = true;
		SPDataUtils.updateIfNoTesting(session);
	}

	@TestVisible
	void checkRegion(String alternativeCountryCode) {
		if (region == null) {
			setRegionByCountryCode(alternativeCountryCode);
		}

		if (region == null) {
			result.requestRegion = true;
		}
	}

	@TestVisible
	void setRegionByCountryCode(String newCountryCode) {
		prepareCountryCode(newCountryCode);
		if (String.isNotBlank(countryCode)) {
			region = 
				CountryCodeMetadataUtil.getRegionInfoByCountryCode(
					countryCode // countryCode
				)?.Region__c;
		}
	}

	@TestVisible
	void initRegionFromExtraInfo() {
		if (String.isNotBlank(session.Extra_Info__c)) {
			region = 
				DivisionAliasInfo.getByAlias(
					session.Extra_Info__c.toUpperCase()
				)?.division;
		}
	}

	@TestVisible
	void initRegionFromIsoCountryCode() {
		setRegionByCountryCode(session?.MessagingEndUser?.IsoCountryCode);
	}

	// this method exists to allow the use of fake country codes 
	// in the test class
	@TestVisible
	void prepareCountryCode(String newCountryCode) {
		if (String.isNotBlank(newCountryCode)) {
			countryCode = newCountryCode;
		} 
	}

	// *******************************************************************

	public class ProcessResult {

		public String region {get; set;}

		public Boolean requestRegion {get; set;}

		public String caseNumber {get; set;}

		public Boolean caseIsClosed {get; set;}

		public String userEmail {get; set;}

		public ProcessResult() {
			this.requestRegion = false;
			this.caseIsClosed = false;
		}
		
	}

}