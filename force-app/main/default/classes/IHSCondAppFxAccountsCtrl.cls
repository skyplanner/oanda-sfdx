/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-01-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class IHSCondAppFxAccountsCtrl {
    private final List<fxAccount__c> fxAccounts;

    public IHSCondAppFxAccountsCtrl() {
        DateTime yesterday = DateTime.now() - 1;
        String yesterdayText = yesterday.format(
            IHSIntegrationManager.CONDITIONALLY_APPROVED_DATE_FORMAT);
        fxAccounts = [SELECT Name, Conditionally_Approved_Date__c,
                Email__c, fxTrade_User_Id__c, Record_URL__c
            FROM fxAccount__c 
            WHERE Conditionally_Approved_Date_Text__c >= :yesterdayText];
    }

    public List<fxAccount__c> getCondAppFxAccounts() {
		return fxAccounts;
	}
}