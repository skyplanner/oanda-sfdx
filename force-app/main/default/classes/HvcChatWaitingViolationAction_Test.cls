/**
 * @File Name          : HvcChatWaitingViolationAction_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 4:48:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 4:44:40 PM   acantero     Initial Version
**/
@isTest
private without sharing class HvcChatWaitingViolationAction_Test {

    @testSetup
    static void setup() {
        SLAViolationsTestDataFactory.createTestChats();
    }

    @isTest
    static void hvcWaitingViolation() {
        List<String> chatIdList = SLAViolationsTestDataFactory.getChatIdList();
        Test.startTest();
        HvcChatWaitingViolationAction.hvcWaitingViolation(
            chatIdList
        );
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(chatIdList.size(), count);
    }
    
}