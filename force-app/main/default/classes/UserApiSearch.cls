/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 01-09-2023
 * @last modified by  : Dianelys Velazquez
**/
public class UserApiSearch {    
    // External
    @AuraEnabled
    public String userName { get; set; }

    @AuraEnabled
    public String userNameEnv { get; set; }

    @AuraEnabled
    public String v20AccId { get; set; }

    @AuraEnabled
    public String v20AccEnv { get; set; }

    @AuraEnabled
    public String mt4AccId { get; set; }

    // Salesforce
    @AuraEnabled
    public String email { get; set; }

    @AuraEnabled
    public String accName { get; set; }
    
    @AuraEnabled
    public String userId { get; set; }
}