/**
 * @description       : get MT5Accs follow framework and design proposed by Dianelys Velazquez
 * @author            : Mikolaj Juras
 * @group             : 
 * @last modified on  : 30 Jun 2023
 * @last modified by  : Mikolaj Juras
**/
public with sharing class TasMt5AccsDetailsGetResponse {

    public List<TasMT5AccsDetails> mt5Accs {get; set;}
}