/**
 * @File Name          : UpdateFourMsgSessionFieldsActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:14:06 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 11:49:01 AM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class UpdateFourMsgSessionFieldsActionTest {
	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}

	@isTest
	private static void testUpdateInfoClass() {
		// Test data setup

		// Actual test
		Test.startTest();
		UpdateFourMsgSessionFieldsAction.UpdateInfo info = new UpdateFourMsgSessionFieldsAction.UpdateInfo();
		info.messagingSessionId = null;
		info.fieldName1 = 'fieldName1';
		info.fieldValue1 = 'fieldValue1';
		info.fieldName2 = 'fieldName2';
		info.fieldValue2 = 'fieldValue2';
		info.fieldName3 = 'fieldName3';
		info.fieldValue3 = 'fieldValue3';
		info.fieldName4 = 'fieldName4';
		info.fieldValue4 = 'fieldValue4';

		Assert.areEqual(
			info.messagingSessionId,
			info.getRecordId(),
			'Record Id should be the same as the messaging session Id'
		);

		Map<String, Object> fields = info.getFieldValueByFieldNames();
		Assert.areEqual(
			info.fieldValue1,
			fields.get(info.fieldName1),
			'Field 1 value should be the same'
		);
		Assert.areEqual(
			info.fieldValue2,
			fields.get(info.fieldName2),
			'Field 2 value should be the same'
		);
		Assert.areEqual(
			info.fieldValue3,
			fields.get(info.fieldName3),
			'Field 3 value should be the same'
		);
		Assert.areEqual(
			info.fieldValue4,
			fields.get(info.fieldName4),
			'Field 4 value should be the same'
		);

		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testSetFourMsgSessionFields() {
		// Test data setup
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		try {
			UpdateFourMsgSessionFieldsAction.setFourMsgSessionFields(
				new List<UpdateFourMsgSessionFieldsAction.UpdateInfo>()
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			exceptionThrown,
			'Exception should not be thrown'
		);
	}

	@isTest
	private static void testSetFourMsgSessionFields1() {
		// Test data setup
		Boolean exceptionThrown = false;
		SPDataInitManager initManager = SPDataInitManager.reload();
		UpdateFourMsgSessionFieldsAction.UpdateInfo info = new UpdateFourMsgSessionFieldsAction.UpdateInfo();
		info.messagingSessionId = null;
		info.fieldName1 = 'CaseId';
		info.fieldValue1 = initManager.getObjectId(
			ServiceTestDataKeys.CASE_1,
			true
		);
		info.fieldName2 = 'Case_Type__c';
		info.fieldValue2 = 'Minimum deposit questions';
		info.fieldName3 = 'Case_Queue__c';
		info.fieldValue3 = 'System';
		info.fieldName4 = 'Case_Type__c';
		info.fieldValue4 = 'FX Data Services';
		// Actual test
		Test.startTest();
		try {
			UpdateFourMsgSessionFieldsAction.setFourMsgSessionFields(
				new List<UpdateFourMsgSessionFieldsAction.UpdateInfo>{ info }
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			true,
			exceptionThrown != null,
			'Exception should not be thrown'
		);
	}
}