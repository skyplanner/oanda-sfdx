/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 19th, 2019
 */
global class ArticleController extends HelpPortalBase {
	private String articleUrl;

	public ArticleController() {
		super();
		// we need the article url
		articleUrl = ApexPages.currentPage().getParameters().get('urlName');
	}

	/**
	 * Redirects to the new help portal
	 * @return a page reference to redirect to
	 */
	global PageReference redirect() {
		PageReference pr;
		FAQ__kav faq;

		// the page reference if always the home page
		pr = Page.HelpPortalHome;

		// if urls is present, we try to find an article
		// with that url
		if (!String.isBlank(articleUrl))
			try {
				faq = getFaq(articleUrl, language);
				if ((faq == null) && (language != 'en_US')) {
					faq = getFaq(articleUrl, 'en_US');
				}
				if (faq != null) {
					// we set the language
					pr.getParameters().put('language', language);

					// we found the faq
					pr.setAnchor('!/article/' + 
						faq.KnowledgeArticleId + '/' +
						faq.Id + '/');
				}

			} catch (QueryException ex) {
				// article does not exists, we redirect to 404
			}

		return pr;
	}

	@testVisible
	private static FAQ__kav getFaq(String articleUrl, String language) {
		List<FAQ__kav> faqList = [
			SELECT KnowledgeArticleId
			FROM FAQ__kav
			WHERE PublishStatus = 'Online'
			AND Language = :language
			AND UrlName = :articleUrl
		];
		FAQ__kav result = (!faqList.isEmpty())
			? faqList[0]
			: null;
		return result;
	}
}