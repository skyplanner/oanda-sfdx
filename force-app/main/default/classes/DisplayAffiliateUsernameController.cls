/**
 * Created by Agnieszka Kajda on 01/02/2024.
 */

public with sharing class DisplayAffiliateUsernameController {
    @AuraEnabled
    public static String getAffiliateUsername(String recordId) {
        String affiliateUsername = '';
        Map<String, Schema.SObjectType> schemaMap  = Schema.getGlobalDescribe();
        Schema.SObjectType accountObj = schemaMap.get('Account') ;
        Schema.SObjectType fxAccountObj = schemaMap.get('fxAccount__c') ;
        Schema.SObjectType caseObj = schemaMap.get('Case') ;
        String accPrefix = accountObj.getDescribe().getKeyPrefix();
        String fxAccPrefix = fxAccountObj.getDescribe().getKeyPrefix();
        String casePrefix = caseObj.getDescribe().getKeyPrefix();
        if(recordId.startsWith(accPrefix)){
            affiliateUsername=[SELECT Affiliate_Username__c FROM Account WHERE Id=:recordId].Affiliate_Username__c;
        }else if(recordId.startsWith(fxAccPrefix)){
            affiliateUsername=[SELECT Affiliate__r.Username__c FROM fxAccount__c WHERE Id=:recordId].Affiliate__r.Username__c ;
        }else if(recordId.startsWith(casePrefix)){
            affiliateUsername=[SELECT Account.Affiliate_Username__c FROM Case WHERE Id=:recordId].Account.Affiliate_Username__c;
        }else{
            affiliateUsername='Unexpected error occurred.';
        }
        return affiliateUsername;
    }
}