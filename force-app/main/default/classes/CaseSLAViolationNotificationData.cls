/**
 * @File Name          : CaseSLAViolationNotificationData.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/26/2024, 1:01:53 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2024, 1:53:07 PM   aniubo     Initial Version
**/
public  class CaseSLAViolationNotificationData extends SLAViolationNotificationData{
	public List<Case> newList { get; set; }
	public Map<Id, Case> oldMap { get; set; }
	public CaseSLAViolationNotificationData(
		List<Case> newList,
		Map<Id, Case> oldMap
	) {
		this.newList = newList;
		this.oldMap = oldMap;
	}
}