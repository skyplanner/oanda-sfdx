/**
 * @File Name          : CountryCodeMetadataUtil_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 9:22:33 PM
**/
@isTest
private class CountryCodeMetadataUtil_Test {
	@isTest
	static void getCountries() {
		List<Country_Code__mdt> countries = CountryCodeMetadataUtil.getPortalCountries();
		List<Geographical_Region_Setting__mdt> geoRegion = CountryCodeMetadataUtil.getGeographicalRegionOrder();
		Country_Code__mdt country =  CountryCodeMetadataUtil.getCountryByCode('US');
		System.assert(countries.size() > 0);
		System.assert(geoRegion.size() > 0);
		System.assert(country != null);
	}
	@isTest
	static void getCountryFail() {
		Country_Code__mdt country =  CountryCodeMetadataUtil.getCountryByCode('No Name');      
		System.assert(country == null);
	}
	@isTest
	static void getCountryMap() {
		List<String> regions = new List<String>{'OEL','OME','OC','OAP','OCAN'};
		List<String> countries =  CountryCodeMetadataUtil.getCountryMapInRegion(regions);      
		System.assert(countries.size() > 0);
	}

	// test 1 : countryCode is blank
	// test 2 : countryCode.length = 3
	// test 3 : countryCode.length = 2
	// test 3 : countryCode.length = 1
	@IsTest
	static void getRegionInfoByCountryCode1() {
		Test.startTest();
		// test 1 => return null
		Country_Code__mdt result1 = 
			CountryCodeMetadataUtil.getRegionInfoByCountryCode(null);
		// test 2 => return "United States" record (find by Alpha3Code)
		Country_Code__mdt result2 = 
			CountryCodeMetadataUtil.getRegionInfoByCountryCode('USA');
		// test 3 => return "United States" record (find by Alpha2Code)
		Country_Code__mdt result3 = 
			CountryCodeMetadataUtil.getRegionInfoByCountryCode('US');
		// test 4 => return null
		Country_Code__mdt result4 = 
			CountryCodeMetadataUtil.getRegionInfoByCountryCode('U');
		Test.stopTest();
		
		Assert.isNull(result1, 'Invalid result');
		Assert.isNotNull(result2, 'Invalid result');
		Assert.isNotNull(result3, 'Invalid result');
		Assert.isNull(result4, 'Invalid result');
	}

}