/**
 * @File Name          : OmnichannelCustomerConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/24/2024, 3:27:42 PM
**/
public without sharing class OmnichannelCustomerConst {

	public enum CustomerType {HVC, NEW_CUSTOMER, ACTIVE, CUSTOMER}

	// tiers
	public static final String TIER_1 = 'Tier 1';
	public static final String TIER_2 = 'Tier 2';
	public static final String TIER_3 = 'Tier 3';
	public static final String TIER_4 = 'Tier 4';

	//SEG_PL 
	public static final String SEGMENTATION_HIGH_PLUS = 'high_plus';
	public static final String SEGMENTATION_HIGH = 'high';
	public static final String SEGMENTATION_MID = 'mid';
	public static final String SEGMENTATION_LOW = 'low';
	public static final String SEGMENTATION_INACTIVE = 'inactive';
	public static final String SEGMENTATION_BLANK = 'blank';

}