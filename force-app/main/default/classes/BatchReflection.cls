/**
 * @description       : Interface created to dynamically call batch classes by name
 * @author            : Agnieszka Kajda
 * @group             : 
 * @last modified on  : 07-05-2023
 * @last modified by  : Agnieszka Kajda
**/

public interface BatchReflection {
    Id rerunSetup(List<Id> ids, Integer batchSize);
}