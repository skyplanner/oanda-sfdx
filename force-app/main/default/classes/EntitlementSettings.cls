/**
 * @File Name          : EntitlementSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/4/2022, 10:29:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/14/2021, 4:15:49 PM   acantero     Initial Version
**/
public without sharing class EntitlementSettings {

    public static final String EMAIL_FRONTDESK_CASE = 'Email_Frontdesk_Case';
    public static final String EMAIL_API_CASE = 'Email_API_Case';
    public static final String EMAIL_CHATBOT_CASE = 'Email_Chatbot_Case';
    public static final String CHAT_CASE = 'Chat_Case';
    public static final String PHONE_CASE = 'Phone_Case';

    @testVisible
    static String emailFrontdeskEntitlementId;
    @testVisible
    static String emailApiEntitlementId;
    @testVisible
    static String emailChatbotEntitlementId;
    @testVisible
    static String chatEntitlementId;
    @testVisible
    static String phoneEntitlementId;

    public static String getEmailFrontdeskEntitlementId() {
        if (emailFrontdeskEntitlementId == null) {
            emailFrontdeskEntitlementId = getEntitlementId(EMAIL_FRONTDESK_CASE, true);
        }
        return emailFrontdeskEntitlementId;
    }

    public static String getEmailApiEntitlementId() {
        if (emailApiEntitlementId == null) {
            emailApiEntitlementId = getEntitlementId(EMAIL_API_CASE, true);
        }
        return emailApiEntitlementId;
    }

    public static String getEmailChatbotEntitlementId() {
        if (emailChatbotEntitlementId == null) {
            emailChatbotEntitlementId = getEntitlementId(EMAIL_CHATBOT_CASE, true);
        }
        return emailChatbotEntitlementId;
    }

    public static String getChatEntitlementId() {
        if (chatEntitlementId == null) {
            chatEntitlementId = getEntitlementId(CHAT_CASE, true);
        }
        return chatEntitlementId;
    }

    public static String getPhoneEntitlementId() {
        if (phoneEntitlementId == null) {
            phoneEntitlementId = getEntitlementId(PHONE_CASE, true);
        }
        return phoneEntitlementId;
    }

    public static String getEntitlementId(String settingName, Boolean required) {
        Entitlement_Settings__mdt setting = 
            Entitlement_Settings__mdt.getInstance(settingName);
        if (setting != null) {
            return setting.Entitlement_ID__c;
        }
        //else...
        if (required == true) {
            throw new EntitlementSettingsException('Entitlement Setting not found: ' + settingName);
        }
        //else...
        return null;
    }

    //******************************************************************

    public class EntitlementSettingsException extends Exception {
    }
}