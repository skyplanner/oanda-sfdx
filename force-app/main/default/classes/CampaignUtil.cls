public with sharing class CampaignUtil {
	
	static final String STATUS_COMPLETED = 'Completed';
	static final String TYPE_WEBINAR = 'Webinar';
	static final String TYPE_CX = 'CX';
	static final String NAME_CX = 'CX';
	static final String CAMPAIGN_NAME_PARENT_WEBINAR = 'Webinar';
	static final String CAMPAIGN_NAME_PARENT_CX = 'CX Campaign';
	public static final String CAMPAIGN_TYPE_REACTIVATION = 'Reactivation';
	
	static Map<String, Id> idByName = new Map<String, Id>();
	
	public static Id getIdByName(String name)
	{
		Id campaignId = idByName.get(name);
		if(campaignId!=null)
		{
			return campaignId;
		}
		else
		{
			Campaign[] campaigns = [SELECT Id, Name FROM Campaign WHERE Name=:name];
			if(campaigns.size()>0)
			{
				idByName.put(campaigns[0].Name, campaigns[0].Id);
				return campaigns[0].Id;
			}
			return null;
		}
	}
	
	public static Id getWebinarCampaignId()
	{
		return getIdByName(CAMPAIGN_NAME_PARENT_WEBINAR);
	}
	
	public static Id getCxCampaignId()
	{
		return getIdByName(CAMPAIGN_NAME_PARENT_CX);
	}
	
	static boolean isCx(String name)
	{
		return name==NAME_CX;
	}

	public static Map<String, Id> createWebinars(Set<String> names)
	{
		Campaign[] campaignsToInsert = new List<Campaign>();
		for(String name : names) {
			if(getIdByName(name)==null)
			{
				Campaign c = new Campaign
								(
									Name=name, 
									Status=STATUS_COMPLETED, 
									IsActive=true, 
									Type=(isCx(name)?TYPE_CX:TYPE_WEBINAR), 
									ParentId=(isCx(name)?getCxCampaignId():getWebinarCampaignId())
								);
				campaignsToInsert.add(c);
			}
		}
		
		if(campaignsToInsert.size()>0)
		{
			insert campaignsToInsert;
			
			for(Campaign c : campaignsToInsert)
			{
				idByName.put(c.Name, c.Id);
			}
		}
		return idByName;
	}
	
	public static Id createWebinar(String name)
	{
		Campaign c = new Campaign(Name=name, Status=STATUS_COMPLETED, IsActive=true, Type=TYPE_WEBINAR, ParentId=getWebinarCampaignId());
		insert c;
		return c.Id;
	}
	
	public static Boolean isReactivationCampaign(Campaign campaign) {
		return campaign.Type == CAMPAIGN_TYPE_REACTIVATION;
	}
	
	public static List<CampaignMember> getCampaignMembers(Set<Id> contactIds) {
		if (contactIds.size() == 0) {
			return new List<CampaignMember>();
		}
		return [SELECT Id, CreatedDate, CampaignId, ContactId, Status, HasResponded FROM CampaignMember WHERE ContactId IN :contactIds];
	}
	
	public static Map<Id, Campaign> getCampaigns(List<CampaignMember> campaignMembers) {
		if (campaignMembers.size() == 0) {
			return new Map<Id, Campaign>();
		}
		Set<Id> campaignIds = new Set<Id>();
		for (CampaignMember cm : campaignMembers) {
			campaignIds.add(cm.CampaignId);
		}
		
		// Query for the associated campaigns themselves
		return new Map<Id, Campaign>([SELECT Id, Name, OwnerId, Type FROM Campaign WHERE Id IN :campaignIds]);
	}
}