@istest
public class surveyQuestionResponseHelperTest {
    @isTest
    private static void testCreateQuestionResponse(){
        SurveyTaker__c  surveyTaken = new SurveyTaker__c ();
        insert surveyTaken;
        
        Survey__c survey = new Survey__c();
        insert survey;
        Survey_Question__c  sq = new Survey_Question__c ();
        sq.Name =  'Test';
        sq.Question__c = 'Question';
        sq.Choices__c = 'Test1\nTest2';
        sq.Type__c = 'Single Select--Vertical';
        sq.Required__c = True;
        sq.OrderNumber__c = 2;
        sq.Survey__c = survey.Id;
        
        insert sq;
        test.startTest();
        SurveyQuestionResponse__c resp = new SurveyQuestionResponse__c(Survey_Question__c =sq.Id, SurveyTaker__c = surveyTaken.Id, Response__c ='Excellent');
        insert resp;
        resp.Response__c = 'Very Good';
        update resp;
        resp.Response__c = 'Satisfactory';
        update resp;
        resp.Response__c = 'Poor';
        update resp;
        resp.Response__c = 'Very Poor';
        update resp;
        resp = [SELECT id,Response_Number__c from SurveyQuestionResponse__c limit 1];
        System.assertEquals(1, resp.Response_Number__c );
        test.stopTest();
    }
}