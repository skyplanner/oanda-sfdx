/**
 * @File Name          : ScheduledEmailCheckerManager.cls
 * @Description        : 
 * @Author             : Ariel Niubo
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/5/2022, 5:16:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         ???                      Ariel Niubo               Initial Version
**/
@SuppressWarnings('PMD.ApexCRUDViolation')
public without sharing class ScheduledEmailCheckerManager {
	@testVisible
	private static final String JOB_NAME = 'ScheduledEmailCheckerJob';
	@testVisible
	private static final String CRON_EXP_EVERY_ONE_HOUR = '0 0 * * * ?';

	public void start() {
		checkJobStatus();
		checkScheduledEmailChecker();
	}

	public void stop() {
		CronTrigger checkerJob = CronTriggerRepo.byNameAndNotStopped(JOB_NAME);
		if (checkerJob != null) {
			cancelJob(checkerJob.Id);
		}
		deactivateJobStatus();
	}

	// check if the job status exist, if not insert it
	@testVisible
	private void checkJobStatus() {
		String jobName = SendScheduledEmailJob.SEND_EMAIL_JOB;
		Job_Status__c jobStatus = JobStatusHelper.getJobStatus(jobName);
		if (jobStatus == null) {
			jobStatus = new Job_Status__c(
				Name = jobName,
				Request_Date__c = System.now(),
				Status__c = JobStatusHelper.INACTIVE_STATUS
			);
			insert jobStatus;
		}
	}
	
	//check if exist a schedule if not, schedule it
	@testVisible
	private void checkScheduledEmailChecker() {
		CronTrigger checkerJob = CronTriggerRepo.byNameAndNotStopped(JOB_NAME);
		Boolean scheduleNewJob = false;
		if (checkerJob == null) {
			scheduleNewJob = true;
		} else if (checkerJob.CronExpression != CRON_EXP_EVERY_ONE_HOUR) {
			cancelJob(checkerJob.Id);
			scheduleNewJob = true;
		}
		if (scheduleNewJob) {
			scheduleEveryOneHour();
		}
	}

	private void scheduleEveryOneHour() {
		System.schedule(
			JOB_NAME,
			CRON_EXP_EVERY_ONE_HOUR,
			new ScheduledEmailCheckerJob()
		);
	}

	private void deactivateJobStatus() {
		String jobName = SendScheduledEmailJob.SEND_EMAIL_JOB;
		try {
			Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
				jobName
			);
			jobStatus.Status__c = JobStatusHelper.INACTIVE_STATUS;
			update jobStatus;
		} catch (Exception ex) {
			// job status does not exist. In this opportunity, it does not matter because we are stopping the process
		}
		Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
			jobName
		);
	}

	private void cancelJob(Id jobId) {
		System.abortJob(jobId);
	}

	public static String scheduleNextTwoMinutes() {
		Datetime now = Datetime.now().addMinutes(2);
		return System.schedule(
			JOB_NAME + '_TEST',
			String.format(
				'{0} {1} {2} {3} {4} ? {5}',
				new List<Integer>{
					now.second(),
					now.minute(),
					now.hour(),
					now.day(),
					now.month(),
					now.year()
				}
			),
			new ScheduledEmailCheckerJob()
		);
	}

}