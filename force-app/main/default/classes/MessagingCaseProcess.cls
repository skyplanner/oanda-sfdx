/**
 * @File Name          : MessagingCaseProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/12/2024, 11:27:52 PM
**/
public inherited sharing class MessagingCaseProcess {

	final MessagingSession session;

	public MessagingCaseProcess(MessagingSession session) {
		this.session = session;
	}

	public Case createCaseFromMsgSession() {
		if (session == null) {
			return null;
		}
		// else...
		Case caseObj = new Case();
		String recTypeDevName = String.isNotBlank(session.Case_Record_Type__c)
			? session.Case_Record_Type__c
			: SPSettingsManager.getSetting(
				MessagingConst.CASE_RECORD_TYPE_DEV_NAME_SETTING
			);

		if (String.isNotBlank(recTypeDevName)) {
			SPRecTypesManager recTypesManager = 
				new SPRecTypesManager(false); // cacheResults = false
			caseObj.RecordTypeId = recTypesManager.getRecordTypeIdByDevName(
				Schema.SObjectType.Case, // sObjectInfo
				recTypeDevName // recTypeDevName
			);
		}

		caseObj.Subject = 
			(session.Last_User_Intent__c == MessagingConst.ACCOUNT_DELETE_INTENT)
			? Label.Chatbot_Account_Deletion_Request
			: Label.Chatbot_Service_Request;

		setCaseOrigin(caseObj);
		caseObj.Priority = label.Medium;
		caseObj.Session_Id__c = session.Id;

		if (session.EndUserAccountId != null) {
			caseObj.AccountId = session.EndUserAccountId;

		} else if (session.LeadId != null) {
			caseObj.Lead__c = session.LeadId;
		}
		
		if (session.EndUserContactId != null) {
			caseObj.ContactId = session.EndUserContactId;
		}

		if (String.isNotBlank(session.Inquiry_Nature__c)) {
			caseObj.Inquiry_Nature__c = session.Inquiry_Nature__c;
			caseObj.Type__c = session.Case_Type__c;
			caseObj.Subtype__c = session.Case_Subtype__c;

		} else {
			caseObj.Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER;
		}

		//caseRecord.Chat_fxTrade_Account_No_Username__c = log.nfchat_Username__c;
		caseObj.Chat_Division__c = session.Region__c;
		// division name
		caseObj.Division__c = 
			ServiceDivisionsManager.getInstance().getDivisionName(
				session.Region__c // divisionCode
			);
		caseObj.Chat_Email__c = session.User_Email__c;
		caseObj.Chat_Type_of_Account__c = session.Live_or_Practice__c;
		caseObj.Live_or_Practice__c = session.Live_or_Practice__c;
		caseObj.User_Authenticated__c = session.User_Authenticated__c;
		caseObj.Chat_Language_Preference__c = 
			ServiceLanguagesManager.getLanguage(session.Case_Language_Code__c);
		caseObj.Language_Corrected__c = 
			ServiceLanguagesManager.getLangCorrectedByLangCode(
				session.Case_Language_Code__c
			);
		
		caseObj.SourceId = session.Id;
		caseObj.OwnerId = getNewCaseOwnerId();

		ServiceLogManager.getInstance().log(
			'createCaseFromMsgSession -> caseObj (before insert)', // msg
			MessagingCaseProcess.class.getName(), // category
			caseObj // details
		);
		
		insert caseObj;

		caseObj = CaseRepo.getSummaryById(caseObj.Id);
		ServiceLogManager.getInstance().log(
			'createCaseFromMsgSession -> caseObj (after insert)', // msg
			MessagingCaseProcess.class.getName(), // category
			caseObj // details
		);
		return caseObj;
	}

	public Boolean updateCaseOrigin() {
		if (
			(session == null) ||
			(session.CaseId == null)
		) {
			return false;
		}
		// else...
		Case caseObj = CaseRepo.getChatbotInfoById(session.CaseId);
		Boolean caseChanged = updateCaseOrigin(caseObj);
		SPDataUtils.updateIfCondition(
			caseObj, // record
			caseChanged // condition
		);
		return caseChanged;
	}

	@TestVisible
	Boolean updateCaseOrigin(Case caseObj) {
		String originalOrigin = caseObj.Origin;
		setCaseOrigin(caseObj);
		String finalOrigin = caseObj.Origin;

		if (
			(originalOrigin == OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE) &&
			(finalOrigin == OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL)
		) {
			caseObj.Routing_to_Agent_Truncated__c = true;
			// case need to be routed as a CHATBOT_EMAIL
			RouteCaseCmd.invokeWithEvent(caseObj.Id);
		}
		
		Boolean result = (originalOrigin != finalOrigin);
		return result;
	}

	@TestVisible
	void setCaseOrigin(Case caseObj) {
		caseObj.Origin = (session.Escalate_to_Agent__c == true)
			? OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE
			: OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL;
		
		caseObj.Email_Case_Chatbot__c = 
			(session.Escalate_to_Agent__c == false);
	}

	@TestVisible
	ID getNewCaseOwnerId() {
		ID result = null;
		String caseQueue = String.isNotBlank(session.Case_Queue__c)
			? session.Case_Queue__c
			: SPSettingsManager.getSetting(
				MessagingConst.CASE_QUEUE_DEV_NAME_SETTING
			);

		if (String.isNotBlank(caseQueue)) {
			result = SPQueueUtil.getQueueByDevName(
				caseQueue, // queueDevName
				false // required
			)?.Id;
		}

		ServiceLogManager.getInstance().log(
			'getNewCaseOwnerId -> ' +
			'caseQueue: ' + caseQueue + ', result: ' + result, // msg
			MessagingCaseProcess.class.getName() // category
		);
		return result;
	}
	
}