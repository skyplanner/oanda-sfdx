/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsSourceNotes {
    public List<String> aml_types { get; set; }

    public List<String> country_codes { get; set; }

    public String listing_ended_utc { get; set; }

    public String listing_started_utc { get; set; }

    public String name { get; set; }

    public String source_id { get; set; }

    public String url { get; set; }
}