/**
 * fxAccountAssessmentUtil's test class
 */
@isTest
public with sharing class fxAccountAssessmentUtilTest {

    private static final String ANSWER1 = 'Buying Euros while simultaneously selling US Dollars';
    private static final String ANSWER2 = 'A CFD reflects the market movements of an asset, but the actual underlying asset is never owned';
    private static final String ANSWER3 = 'Your positions are closed out';
    private static final String ANSWER4 = 'The market has big movements up and down';
    private static final String ANSWER5 = 'The difference between the Bid (Sell) and Ask (Buy)';
    private static final String ANSWER6 = 'The spread widens and narrows based on changes in the underlying market';
    private static final String ANSWER7 = 'When the market "jumps" from one price to another price, leaving a "gap" on the charts and no opportunity to trade between the two prices';
    private static final String KNOWLEDGE_RESULT = 'Pass';

    @testSetup
	static void setup () {
		insert new Lead(
	    	RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
	    	LastName = 'Doe',
	    	Email = 'jdoe@example.org',
	    	Territory__c = 'North America');
	}

    /**
     * Experience Result = PASS
     * Knowledge Result = 1-4 Correct answers
     * => Knowledge Result == REJECT
     */
    /* @isTest
    static void setAssessmentResultForAustralia1() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Experience_Trading_CFDs_or_FX__c = true,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Result__c = KNOWLEDGE_RESULT);

        validateResults(
            fxAccountAssessmentUtil.PASS,
            fxAccountAssessmentUtil.REJECT);
    } */

    /**
     * Experience Result = PASS
     * Knowledge Result = 5-6 Correct answers
     * => Knowledge Result == FAIL
     */
    /* @isTest
    static void setAssessmentResultForAustralia2() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Experience_Trading_CFDs_or_FX__c = true,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Answer_2__c = ANSWER2,
            Knowledge_Answer_3__c = ANSWER3,
            Knowledge_Answer_4__c = ANSWER4,
            Knowledge_Answer_5__c = ANSWER5,
            Knowledge_Result__c = KNOWLEDGE_RESULT);
        
        validateResults(
            fxAccountAssessmentUtil.PASS,
            fxAccountAssessmentUtil.FAIL);
    } */

    /**
     * Experience Result = PASS
     * Knowledge Result = 7-9 Correct answers
     * => Knowledge Result == PASS
     */
    /* @isTest
    static void setAssessmentResultForAustralia3() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Experience_Trading_CFDs_or_FX__c = true,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Answer_2__c = ANSWER2,
            Knowledge_Answer_3__c = ANSWER3,
            Knowledge_Answer_4__c = ANSWER4,
            Knowledge_Answer_5__c = ANSWER5,
            Knowledge_Answer_6__c = ANSWER6,
            Knowledge_Answer_7__c = ANSWER7,
            Knowledge_Result__c = KNOWLEDGE_RESULT);

        validateResults(
            fxAccountAssessmentUtil.PASS,
            fxAccountAssessmentUtil.PASS);
    } */

    /**
     * Experience Result = FAIL
     * Knowledge Result = 1-4 Correct answers
     * => Knowledge Result == REJECT
     */
    /* @isTest
    static void setAssessmentResultForAustralia4() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Result__c = KNOWLEDGE_RESULT);

        validateResults(
            fxAccountAssessmentUtil.FAIL,
            fxAccountAssessmentUtil.REJECT);
    } */

    /**
     * Experience Result = FAIL
     * Knowledge Result = 5-6 Correct answers
     * => Knowledge Result == FAIL
     */
    /* @isTest
    static void setAssessmentResultForAustralia5() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Answer_2__c = ANSWER2,
            Knowledge_Answer_3__c = ANSWER3,
            Knowledge_Answer_4__c = ANSWER4,
            Knowledge_Answer_5__c = ANSWER5,
            Knowledge_Result__c = KNOWLEDGE_RESULT);

        validateResults(
            fxAccountAssessmentUtil.FAIL,
            fxAccountAssessmentUtil.FAIL);
    } */

    /**
     * Experience Result = FAIL
     * Knowledge Result = 7-9 Correct answers
     * => Knowledge Result == FAIL
     */
    /* @isTest
    static void setAssessmentResultForAustralia6() {
        insert new fxAccount__c(
            Account_Email__c = [SELECT Email FROM Lead LIMIT 1].Email,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
            Division_Name__c = Constants.DIVISIONS_OANDA_AUSTRALIA,
            Knowledge_Answer_1__c = ANSWER1,
            Knowledge_Answer_2__c = ANSWER2,
            Knowledge_Answer_3__c = ANSWER3,
            Knowledge_Answer_4__c = ANSWER4,
            Knowledge_Answer_5__c = ANSWER5,
            Knowledge_Answer_6__c = ANSWER6,
            Knowledge_Answer_7__c = ANSWER7,
            Knowledge_Result__c = KNOWLEDGE_RESULT);

        validateResults(
            fxAccountAssessmentUtil.FAIL,
            fxAccountAssessmentUtil.FAIL);
    } */

    /**
     * Helper method to validate results
     */
    private static void validateResults(
        String tradingExperienceResult,
        String assessmentResult)
    {
        fxAccount__c fxAcc =
            [SELECT OEL_Trading_Experience_Result__c,
                    OEL_Assessment_Result__c
                FROM fxAccount__c
                LIMIT 1];

        System.assertEquals(
            fxAcc.OEL_Trading_Experience_Result__c,
            tradingExperienceResult);
        
        System.assertEquals(
            fxAcc.OEL_Assessment_Result__c,
            assessmentResult);
    }

}