/**
 * @File Name          : SPSecurityUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/12/2022, 5:23:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/2/2021, 10:17:08 AM   acantero     Initial Version
**/
public without sharing class SPSecurityUtil {

    @TestVisible
    static Boolean validStandardUser;

    public static Map<String,String> getUserRoleMap(
        Set<String> userIdSet, //input
        Set<String> roleDevNameSet //output
    ) {
        Map<String,String> result = new Map<String,String>();
        if (
            (userIdSet == null) ||
            userIdSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<User> userList = [
            SELECT 
                Id, 
                UserRole.DeveloperName 
            FROM 
                User 
            WHERE 
                Id IN :userIdSet
        ];
        for(User userObj : userList) {
            String roleDevName = userObj.UserRole.DeveloperName;
            if (String.isNotBlank(roleDevName)) {
                result.put(userObj.Id, roleDevName);
                if (roleDevNameSet != null) {
                    roleDevNameSet.add(roleDevName);
                }
            }
        }
        return result;
    }

    public static Boolean isCurrentUserAValidStandardUser() {
        if (validStandardUser == null) {
            validStandardUser = (
                String.isNotBlank(UserInfo.getSessionId()) && 
                (UserInfo.getUserType() == 'Standard')
            );
        }
        return validStandardUser;
    }

}