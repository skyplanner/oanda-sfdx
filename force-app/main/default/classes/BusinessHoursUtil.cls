/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 08-19-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class BusinessHoursUtil {	
	static Id defaultId;
	static String TMS_BUSINESS_HOURS = 'TMS Business Hours';

	public static Id getDefaultId() {
		if (defaultId == null) {
			List<BusinessHours> bhList = [SELECT Id 
				FROM BusinessHours 
				WHERE IsDefault = true];
			defaultId = bhList.isEmpty() ? null : bhList[0].Id;
		}
		
		return defaultId;
	}
	public static Id getTMSBusinessHoursId() {
		return [SELECT Id FROM BusinessHours WHERE Name=:TMS_BUSINESS_HOURS LIMIT 1].Id;
	}

	public static Boolean isWithinDefaultBusinessHour() {
		return isWithinDefaultBusinessHour(Datetime.now());
	}

	public static Boolean isWithinDefaultBusinessHour(DateTime dt) {
		return BusinessHours.isWithin(getDefaultId(), dt);
	}

	@InvocableMethod
	public static List<Boolean> isWithinBusinessHours() {
		List<Boolean> result = new List<Boolean> {
			isWithinDefaultBusinessHour()
		};
		
		return result;
	}
}