/**
 * @File Name          : MiFIDExpValConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/23/2020, 10:48:55 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   acantero     Initial Version
**/
public class MiFIDExpValConst {

    public enum MiFIDValType {NATIONAL_ID, PASSPORT}

    public enum LetterCase {UPPER_CASE, LOWER_CASE}
    
    public static final String FORMAT_WILDCARD = '*';

    public static final String DEFAULT_MIFID = 'DEFAULT_MIFID';

    public static final String CAN_BE_VALUE = 'Can Be';
    public static final String MUST_BE_VALUE = 'Must Be';
    public static final String CAN_BE_CONDITIONAL_VALUE = 'Can Be (conditional)';

    public static final String SEPARATOR_REMOVE_PREFIX = '--';

    public static final String APEX_VALIDATOR_SUFFIX = '_Validator';

    public static final String FXACCOUNT_REC_TYPE_LIVE = 'Retail_Live';
    public static final String FXACCOUNT_TYPE_INDIVIDUAL = 'Individual';
    public static final String FXACCOUNT_READY_FOR_FUNDING = 'Ready For Funding';
    public static final String FXACCOUNT_FUNDED = 'Funded';
    public static final String FXACCOUNT_TRADED = 'Traded';

    public static final String CASE_OPEN_STATUS = 'Open';
    public static final String CASE_CLOSED_STATUS = 'Closed';
    public static final String CASE_INTERNAL_ORIGIN = 'Internal';
    public static final String CASE_NORMAL_PRIORITY = 'Normal';
    public static final String CASE_ACCOUNT_INQUIRY_NATURE = 'Account';
    public static final String CASE_LIVE = 'Live';
    public static final String CASE_MIFID_REVIEW_TYPE = 'MiFID review';
    
}