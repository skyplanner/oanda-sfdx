/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 07-07-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiAuxUser {
    public String age {get; set;}

    public String bank_reference {get; set;}

    public String bank_status {get; set;}

    public String bank_status_specification {get; set;}

    public String bankruptcy {get; set;}

    public String bankruptcy_status {get; set;}

    public Integer comm_pool_asset_value {get; set;}

    public Integer commodities_exp_years {get; set;}

    public String computer_system {get; set;}

    public Integer deposit_limit_amount {get; set;}

    public Integer deposit_limit_remaining {get; set;}

    public String deposit_limit_update {get; set;}

    public Integer desired_risk_tolerance {get; set;}

    public String employer_bustype_details {get; set;}

    public Integer employment_length {get; set;}

    public String employment_position {get; set;}

    public Integer employment_status {get; set;}

    public Integer equity_opt_exp_years {get; set;}

    public Integer forex_exp_years {get; set;}

    public Integer futures_exp_years {get; set;}

    public String household_income {get; set;}

    public String household_income_currency_type {get; set;}

    public Integer ibroker_id {get; set;}

    public String iiroc_member {get; set;}

    public String iiroc_member_details {get; set;}

    public String income_source {get; set;}

    public String initial_product {get; set;}

    public String int_reg_number {get; set;}

    public String int_registered {get; set;}

    public String int_regulator_name {get; set;}

    public String int_us_resident {get; set;}

    public String intermediary {get; set;}

    public String intermediary_specification {get; set;}

    public String internet_connection {get; set;}

    public Integer last_info_update {get; set;}

    public Integer liquid_net_worth {get; set;}
    
    public Integer liquid_net_worth_range_end {get; set;}
    
    public Integer liquid_net_worth_range_start {get; set;}    

    public String liquid_net_worth_update {get; set;}

    public String marketing_email_opt_in {get; set;}

    public Integer marketing_email_opt_in_lastmod {get; set;}

    public Integer net_worth {get; set;}

    public String net_worth_currency_type {get; set;}

    public Integer net_worth_range_end {get; set;}

    public Integer net_worth_range_start {get; set;}

    public Integer net_worth_update_timestamp {get; set;}

    public Integer number_in_household {get; set;}

    public String oanda_relationship_type {get; set;}

    public String online_trading {get; set;}

    public String other_account {get; set;}

    public String other_account_numbers {get; set;}

    public String other_accounts {get; set;}

    public String other_affiliations {get; set;}

    public String other_guarantor {get; set;}

    public String pefp {get; set;}

    public String pefp_details {get; set;}

    public String products_services_details {get; set;}

    public String public_name {get; set;}

    public String publicly_traded {get; set;}

    public String referred_from {get; set;}

    public String reg_source {get; set;}

    public Integer risk_tolerance_amount {get; set;}

    public String risk_tolerance_currency_type {get; set;}

    public String risk_tolerance_update {get; set;}

    public Integer savings_and_investments {get; set;}

    public String savings_and_investments_update {get; set;}

    public Integer securities_exp_years {get; set;}

    public String security_employee {get; set;}

    public String security_employee_explaination {get; set;}

    public Integer trading_objective {get; set;}

    public String trading_objectives {get; set;}

    public String website_url {get; set;}
}