/**
 * @File Name          : ChatSLAViolationNotifier.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:12:02 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/19/2024, 1:58:24 PM   aniubo     Initial Version
 **/
public inherited sharing class ChatSLAViolationNotifier extends ChatBaseSLAViolationNotifier {
	private Id agentId;
	public ChatSLAViolationNotifier(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		SLAViolationCanNotifiable violationNotificationChecker,
		Id agentId
	) {
		super(
			notificationObjectType,
			violationType,
			dataReader,
			violationNotificationChecker
		);
		this.agentId = agentId;
		this.logManager.log(
			'ChatSLAViolationNotifier:ctrl',
			'SLA',
			'ViolationType =' + violationType.name()
		);
	}

	protected override SLAViolationInfo createViolationInfo(SObject record) {
		LiveChatTranscript chatObj = (LiveChatTranscript) record;
		SLAChatViolationInfo result = new SLAChatViolationInfo(
			chatObj.Id,
			chatObj.Name,
			getViolationType()
		);
		result.isNotifiable =
			String.isNotBlank(chatObj.Chat_Division__c) ||
			((chatObj.Account != null) &&
			String.isNotBlank(chatObj.Account.Primary_Division_Name__c));
		result.isPersistent = true;
		result.division = getChatDivision(chatObj);
		result.divisionCode = getChatDivisionCode(chatObj);
		result.inquiryNature = chatObj.Inquiry_Nature__c;
		result.Tier = chatObj.Tier__c;
		result.language = chatObj.Language__c;
		result.liveOrPractice = chatObj.Chat_Type_of_Account__c;
		result.notificationObjectType = SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT;
		//...
		result.accountName = getChatAccountName(chatObj);
		result.ownerId = agentId;
		if (String.isNotBlank(agentId)) {
			result.ownerName = getAgentName(agentId);
		}

		return result;
	}

	protected override String getChatDivision(SObject obj) {
		LiveChatTranscript chatObj = (LiveChatTranscript) obj;
		String result = chatObj.Division_Name__c;
		if (
			String.isBlank(result) &&
			(chatObj.Account != null) &&
			String.isNotBlank(chatObj.Account.Primary_Division_Name__c)
		) {
			result = chatObj.Account.Primary_Division_Name__c;
		}
		return result;
	}

	protected override String getChatDivisionCode(SObject obj) {
		LiveChatTranscript chatObj = (LiveChatTranscript) obj;
		String result = chatObj.Chat_Division__c;
		if (
			String.isBlank(result) &&
			(chatObj.Account != null) &&
			String.isNotBlank(chatObj.Account.Primary_Division_Name__c)
		) {
			result = ServiceDivisionsManager.getInstance()
				.getDivisionCode(chatObj.Account.Primary_Division_Name__c);
		}
		return result;
	}

	protected override String getChatAccountName(SObject obj) {
		LiveChatTranscript chatObj = (LiveChatTranscript) obj;
		String result = (chatObj.Account != null)
			? chatObj.Account.Name
			: Label.Unknown_Account_Name;
		return result;
	}
}