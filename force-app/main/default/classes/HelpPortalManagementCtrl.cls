/**
 * Controller for component: HelpPortalManagement
 * @author Fernando Gomez, Skyplanner LLC
 * @since 4/30/2019
 */
global without sharing class HelpPortalManagementCtrl extends HelpPortalBase {

	/**
	 * Main constructor
	 */
	global HelpPortalManagementCtrl() {
		super('en_US');
	}

	/**
	 * @return a list of currently active language
	 */
	@AuraEnabled
	global static List<Map<String, String>> getSupportedLanguages() {
		try {
			HelpPortalManagementCtrl ctrl = new HelpPortalManagementCtrl();
			return ctrl.getLanguages();
		} catch(Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}
}