/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-19-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchCAFieldsUpdInAccTest {
    @isTest
	static void updateCAName() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
			Custom_Division_Name__c = 'OANDA Canada',
            Is_Alias_Search__c = false
		);
		insert search;

        fxAcc.Is_CA_Name_Search_Monitored__c = false;
        update fxAcc;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;
		
		Test.StartTest();

        BatchCAFieldsUpdInAcc b = new BatchCAFieldsUpdInAcc(' AND Id = \'' + search.Id + '\'');
        Database.executeBatch(b);

		Test.stopTest();

		fxAcc = [
            SELECT Id, Is_CA_Name_Search_Monitored__c
			FROM fxAccount__c
			WHERE Id = :fxAcc.Id
		];
		System.assertEquals(true, fxAcc.Is_CA_Name_Search_Monitored__c);
	}

    @isTest
	static void updateCAAlias() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
			Custom_Division_Name__c = 'OANDA Canada',
            Is_Alias_Search__c = true
		);
		insert search;

        fxAcc.Is_CA_Alias_Search_Monitored__c = false;
        update fxAcc;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		Test.StartTest();

        BatchCAFieldsUpdInAcc b = new BatchCAFieldsUpdInAcc(' AND Id = \'' + search.Id + '\'');
        Database.executeBatch(b);

		Test.stopTest();

		fxAcc = [
            SELECT Id, Is_CA_Alias_Search_Monitored__c
			FROM fxAccount__c
			WHERE Id = :fxAcc.Id
		];
		System.assertEquals(true, fxAcc.Is_CA_Alias_Search_Monitored__c);
	}
}