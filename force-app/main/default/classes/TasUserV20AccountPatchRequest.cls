/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-03-2023
 * @last modified by  : Jakub Jaworski
**/
public with sharing class TasUserV20AccountPatchRequest {
    public Integer accountLocked {get; set;}

    public Integer commissionGroupID {get; set;}
    
    public List<Integer> pricingGroupIDs {get; set;}

    public Integer currencyConversionGroupOverrideID {get; set;}

    public List<Integer>  financingGroupIDs {get; set;}
    
    //below method was changed to return Map instead of the class instance
    //to avoid sending unwanted null properties (with class instance its either
    //all nulls or no nulls if suppressed)
    public static Map<String,Object> fromMap(
        Map<String, Object> toUpdate
    ) {
        Map<String, Object> payloadMap = new Map<String, Object>();

        if (nothingChange(toUpdate)) {
            return null;
        }
        
        for (String key : toUpdate.keySet()) {
            //zeroes are used to unify payload between Apex and JS
            if (toUpdate.get(key) == '0') {
                toUpdate.put(key, null);
            }
        }
        
        if (toUpdate.containsKey(Constants.API_FIELD_PRICING_GROUP)) {
            List<Integer> pricingGroupIDTempList = new List<Integer>();
            if (toUpdate.get(Constants.API_FIELD_PRICING_GROUP) != null) {
                pricingGroupIDTempList.add(Integer.valueOf(toUpdate.get(Constants.API_FIELD_PRICING_GROUP)));
            }
            payloadMap.put('pricingGroupIDs', pricingGroupIDTempList);
        }

        if (toUpdate.containsKey(Constants.API_FIELD_COMMISSION_GROUP)) {
            payloadMap.put('commissionGroupID', Integer.valueOf(toUpdate.get(Constants.API_FIELD_COMMISSION_GROUP)));
        }

        if (toUpdate.containsKey(Constants.API_FIELD_CURRENCY_CONVERSION_GROUP)) {
            payloadMap.put('currencyConversionGroupOverrideID', Integer.valueOf(toUpdate.get(Constants.API_FIELD_CURRENCY_CONVERSION_GROUP)));
        }

        if (toUpdate.containsKey(Constants.API_FIELD_FINANCING_GROUP)) {
            List<Integer> financingGroupIDTempList = new List<Integer>();
            if (toUpdate.get(Constants.API_FIELD_FINANCING_GROUP) != null) {
                financingGroupIDTempList.add(Integer.valueOf(toUpdate.get(Constants.API_FIELD_FINANCING_GROUP)));
            }
            payloadMap.put('financingGroupIDs', financingGroupIDTempList);
        }

        return payloadMap;
    }

    static Boolean nothingChange(Map<String, Object> toUpdate) {
        return !toUpdate.containsKey(Constants.API_FIELD_PRICING_GROUP) && 
            !toUpdate.containsKey(Constants.API_FIELD_COMMISSION_GROUP) && 
            !toUpdate.containsKey(Constants.API_FIELD_CURRENCY_CONVERSION_GROUP) && 
            !toUpdate.containsKey(Constants.API_FIELD_FINANCING_GROUP);
    }

}