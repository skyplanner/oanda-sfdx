/**
 * Handles each one of the workflow updates for the Comply Advantage object.
 * Most of these updates will happen on a before-trigger. There is
 * a method for each conditional update and a mapping to call the update
 * dynamically based on the workflow name.
 * @author Fernando Gomez
 */
public class ComplyAdvantageConditionalUpdate extends ConditionalUpdate {
	private Comply_Advantage_Search__c newCASearch;
	private Comply_Advantage_Search__c oldCASearch;

	/**
	 * @param newComplyAdvantageSearch
	 * @param oldComplyAdvantageSearch 
	 */
	public ComplyAdvantageConditionalUpdate(
			Comply_Advantage_Search__c newCASearch,
			Comply_Advantage_Search__c oldCASearch) {
		super(newCASearch, oldCASearch);
		this.newCASearch = newCASearch;
		this.oldCASearch = oldCASearch;
	}

	/**
	 * Performs the action in the workflow with the specified name
	 * @param workflowName
	 */
	public override void execute(String workflowName) {
		switch on workflowName {
			when 'WF_Monitoring_Turned_On_Off_Datetime' {
				execute_Monitoring_Turned_On_Off_Datetime();
			}
			when else {
				// do nothing as update is not supported
			}
		}
	}

	/**
	 * Performs the action in the workflow:
	 * Monitoring_Turned_On_Off_Datetime
	 */
	public void execute_Monitoring_Turned_On_Off_Datetime() {
		/*
		OR( ISNEW(), ISCHANGED(Is_Monitored__c))
		*/
		if (isNew() || isChangedBoolean('Is_Monitored__c')) {
			// we set one of the fields depending on the value
			if (newCASearch.Is_Monitored__c == true)
				newCASearch.Monitoring_Turned_On_Datetime__c = System.now();
			else
				newCASearch.Monitoring_Turned_Off_Datetime__c = System.now();
		}
	}
}