/**
 * @File Name          : OnNonHvcCaseAssignedCmd.cls
 * @Description        : 
 * Updates the data of the Case corresponding to the newly assigned 
 * non-HVC Case
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2024, 8:51:12 PM
**/
public inherited sharing class OnNonHvcCaseAssignedCmd {

	final List<Non_HVC_Case__c> recList;
	
	@TestVisible
	List<Case> recordsToUpdate;
	
	public static Boolean processNonHvcCases(Set<ID> nonHvcCaseIdSet) {
		List<Non_HVC_Case__c> nonHvcCases = 
			NonHVCCaseHelper.getById(nonHvcCaseIdSet);
		Boolean result = new OnNonHvcCaseAssignedCmd(nonHvcCases).execute();
		return result;
	}
	
	public OnNonHvcCaseAssignedCmd(List<Non_HVC_Case__c> recList) {
		this.recList = recList;
	}
	
	public Boolean execute() {
		if (
			(recList == null) ||
			recList.isEmpty()
		) {
			return false;
		}
		//else...
		recordsToUpdate = new List<Case>();
		
		for (Non_HVC_Case__c rec : recList) {
			if (rec.Case__c != null) {
				// IMPORTANT
				// It is essential to update the Agent_Capacity_Status__c field
				// to a new value so that the change of owner does not cause a 
				// new routing
				recordsToUpdate.add(
					new Case(
						Id = rec.Case__c,
						OwnerId = rec.OwnerId,
						Routed_And_Assigned__c = true,
						Agent_Capacity_Status__c = 
							OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE
					)
				);
			}
		}
		
		update recordsToUpdate;
		return true;
	}
	
}