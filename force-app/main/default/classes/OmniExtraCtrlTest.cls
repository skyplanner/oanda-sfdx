/**
 * @File Name          : OmniExtraCtrlTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2024, 10:57:44 PM
**/
@IsTest
private without sharing class OmniExtraCtrlTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		initManager.storeData();
	}

	// valid test
	@IsTest
	static void registerAssignedNonHvcCase1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID nonHVCCase1Id = initManager.getObjectId(
			ServiceTestDataKeys.NON_HVC_CASE_1, 
			true
		);

		Test.startTest();
		Boolean result = 
			OmniExtraCtrl.registerAssignedNonHvcCase(nonHVCCase1Id);
		Test.stopTest();
		
		Assert.isTrue(result, 'Invalid result');
	}


	// exception test
	@IsTest
	static void registerAssignedNonHvcCase2() {
		Boolean error = false;

		Test.startTest();
		ExceptionTestUtil.prepareDummyException();
		try {
			Boolean result = OmniExtraCtrl.registerAssignedNonHvcCase(null);
			
		} catch(Exception ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}
	
}