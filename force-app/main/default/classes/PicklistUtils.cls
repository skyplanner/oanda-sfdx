/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
 **/
public inherited sharing class PicklistUtils {
	public static List<SelectOption> getPicklistValues(
		String objName,
		String picklistField
	) {
		if (String.isBlank(objName)) {
			throw createAuraHandledException(
				System.Label.Scheduled_email_ctrl_ObjectName_null
			);
		}
		if (String.isBlank(picklistField)) {
			throw createAuraHandledException(
				System.Label.Scheduled_email_ctrl_FieldName_null
			);
		}
		Schema.SObjectType objType = SObjectUtils.getObjectTypeByName(objName);
		if (objType == null) {
			throw createAuraHandledException(
				System.Label.Scheduled_email_ctrl_invalid_object
			);
		}
		Schema.SObjectField fieldType = SObjectUtils.getObjectFieldByName(
			objType,
			picklistField
		);
		if (fieldType == null) {
			throw createAuraHandledException(
				String.format(
					System.Label.Scheduled_email_ctrl_invalid_field,
					new List<String>{ objName, picklistField }
				)
			);
		}
		List<SelectOption> picklistOptions = new List<SelectOption>();
		for (
			Schema.PicklistEntry entry : fieldType.getDescribe()
				.getPicklistValues()
		) {
			picklistOptions.add(
				new SelectOption(entry.getValue(), entry.getLabel())
			);
		}
		return picklistOptions;
	}
	private static AuraHandledException createAuraHandledException(
		String errorMessage
	) {
		AuraHandledException ex = new AuraHandledException(errorMessage);
		ex.setMessage(errorMessage);
		return ex;
	}
}