/**
* Gilbert
*
* Live chat deployment page controller
* July 27, 2016
*/
public without sharing class ChatDeployCon {
	
	private String customerEmail;
	private String chatButtonName;
	
	public String language { get; set; }
	
	public ChatDeployCon(){
		customerEmail = ApexPages.currentPage().getParameters().get('CustomerEmail');
		
		chatButtonName = ApexPages.currentPage().getParameters().get('ButtonName');
		
		// language = VfSearchController.createSFLanguageCode(
		// 		ApexPages.currentPage().getParameters().get('LanguagePreference'));
		// NOTE: commented as the above was removed and 
		// did not work in the first place.
		language = 'en_US';
	}
	
	public boolean getIsContact() {
	    List<Contact> contacts = [SELECT Id FROM Contact WHERE email=:customerEmail LIMIT 1];
	    return contacts.size()==1;
	}
	
	public String getAPIRoot(){
    	return CustomSettings.getLiveAgentAPIRoot();
    }
    
    public String getJSRoot(){
    	return CustomSettings.getLiveAgentJSRoot();
    }
    
    public String getDeployParam1(){
    	return CustomSettings.getLiveAgentDeployParam1();
    }
    
    public String getDeployParam2(){
    	return CustomSettings.getLiveAgentDeployParam2();
    }
    
    public String getChatButtonId(){
    	String result = '';
    	
    	Live_Chat_Config__c config = Live_Chat_Config__c.getValues(chatButtonName);
    	if(config != null){
    		result = config.Chat_Button_ID__c;
    	}
    	
    	return result;
    	
    }
    
}