/**
 * @File Name          : NonHvcCaseAttemptInfo.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 3:11:22 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/6/2024, 3:11:08 PM   aniubo     Initial Version
 **/
public class NonHvcCaseAttemptInfo {
	@AuraEnabled
	public Boolean canBeAssigned { get; set; }
	@AuraEnabled
	public String newStatusId { get; set; }
}