/**
 * @File Name          : VerificationCodeConfigManager_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/13/2019, 1:32:08 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/13/2019   dmorales     Initial Version
**/
@isTest
private class VerificationCodeConfigManager_Test {
 
    @isTest
	static void getInstance_test() {
		Test.startTest();
		VerificationCodeConfigManager result = VerificationCodeConfigManager.getInstance();
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void getInstance_test2() {
		String settName = 'SomeInvalidTestName';
		Test.startTest();
		VerificationCodeConfigManager result = VerificationCodeConfigManager.getInstance(settName);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void init_test() {
		VerificationCodeConfigManager obj = VerificationCodeConfigManager.getInstance();
		List<Verification_Code_Config__mdt> settings = new List<Verification_Code_Config__mdt>{
			new Verification_Code_Config__mdt()
		};
		Test.startTest();
		obj.init(settings);
		Test.stopTest();
		System.assertEquals(VerificationCodeConfigManager.DEFAULT_COUNT, obj.countToLoad);
	}
}