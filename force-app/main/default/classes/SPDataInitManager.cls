/**
 * @File Name          : SPDataInitManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/11/2023, 10:13:56 PM
**/
public inherited sharing class SPDataInitManager implements SPDataInitializer {

	public static final String RECORD_TYPE_ID = 'RecordTypeId';
	public static final String ID_PREFIX = 'I';
	public static final String INDEX_PREFIX = 'X';
	public static final String DUMMY_PREFIX = 'D';

	final SPRecTypesManager recTypesManager;

	@TestVisible
	final Map<String, Map<String,Object>> defaultValues;

	@TestVisible
	final Map<String, Map<String,Object>> objectOptions;

	@TestVisible
	final Map<String, ID> idByKey;

	@TestVisible
	final Map<String, Integer> indexByObject;

	public SPDataInitManager() {
		this.defaultValues = new Map<String, Map<String,Object>>();
		this.idByKey = new Map<String, ID>();
		this.objectOptions = new Map<String, Map<String,Object>>();
		this.recTypesManager = new SPRecTypesManager(true); // cacheResults = true
		this.indexByObject = new Map<String, Integer>();
	}

	public static SPDataInitManager reload() {
		SPDataInitManager instance = new SPDataInitManager();
		instance.loadData();
		return instance;
	}

	public SPDataInitProxy getProxy(
		String objKey,
		DescribeSObjectResult sObjectInfo
	) {
		return new SPDataInitProxyImp(
			this, // dataInit
			objKey,
			sObjectInfo
		);
	}

	public String getUniqueName(DescribeSObjectResult sObjectInfo) {
		String objName = sObjectInfo.getName();
		return getUniqueName(objName);
	}

	public String getUniqueName(String objName) {
		return objName + ' ' + getUniqueIndex(objName);
	}

	public Integer getUniqueIndex(DescribeSObjectResult sObjectInfo) {
		String objName = sObjectInfo.getName();
		return getUniqueIndex(objName);
	}

	public Integer getUniqueIndex(String objName) {
		Integer result = indexByObject.get(objName);
		
		if (result == null) {
			result = 0;
		}
		indexByObject.put(objName, ++result);
		return result;
	}

	public ID getObjectId(
		String objKey,
		Boolean required
	) {
		ID result = idByKey.get(objKey);

		if (
			(result == null) &&
			(required == true)
		) {
			throw new SPDataInitException(
				'Required Object Key (' + objKey + ') is null'  // msg
			);
		}
		//else...
		return result;
	}

	public ID insertObj(
		SObject newObject,
		String objKey
	) {
		setObjectFieldValues(
			newObject, // obj
			objKey // objKey
		);
		insert newObject;
		idByKey.put(objKey, newObject.Id); 
		return newObject.Id;
	}

	public void setObjectFieldValues(
		SObject newObject,
		String objKey
	) {
		Map<String, Object> objDefaultValues = defaultValues.get(objKey);
		
		if (objDefaultValues != null) {
			for(String field : objDefaultValues.keySet()) {
				newObject.put(field, objDefaultValues.get(field));
			}
		}
	}

	public ID getRecordTypeId(
		DescribeSObjectResult sObjectInfo,
		String recTypeDevName
	) {
		return recTypesManager.getRecordTypeIdByDevName(
			sObjectInfo, 
			recTypeDevName
		);
	}

	public void setRecordTypeId(
		String objKey,
		DescribeSObjectResult sObjectInfo,
		String recTypeDevName
	) {
		String recordTypeId = getRecordTypeId(sObjectInfo, recTypeDevName);
		if (recordTypeId != null) {
			setFieldValue(
				objKey, 
				RECORD_TYPE_ID, // fieldApiName
				recordTypeId // defaultValue
			);
		}
	}

	public void setFieldValue(
		String objKey,
		Schema.SObjectField fieldToken,
		Object newValue
	) {
		String fieldApiName = String.valueOf(fieldToken);
		setFieldValue(objKey, fieldApiName, newValue);
	}

	public void setFieldValue(
		String objKey,
		String fieldApiName,
		Object newValue
	) {
		Map<String, Object> objDefaultValues = getValuesMap(
			objKey, // objKey
			defaultValues // valuesMapByKey
		);
		objDefaultValues.put(fieldApiName, newValue);
	}

	public void setFieldValues(
		String objKey,
		Map<String,Object> newObjValues
	) {
		Map<String, Object> objDefaultValues = getValuesMap(
			objKey, // objKey
			defaultValues // valuesMapByKey
		);
		for(String fieldApiName : newObjValues.keySet()) {
			objDefaultValues.put(fieldApiName, newObjValues.get(fieldApiName));
		}
	}

	public void setObjectOption(
		String objKey,
		String optionName,
		Object newValue
	) {
		Map<String, Object> objOptions = getValuesMap(
			objKey, // objKey
			objectOptions // valuesMapByKey
		);
		objOptions.put(optionName, newValue);
	}

	public Object getObjectOption(
		String objKey,
		String optionName,
		Object defaultValue
	) {
		Object objOption = objectOptions.get(objKey)?.get(optionName);

		if (objOption == null) {
			objOption = defaultValue;
		}
		return objOption;
	}

	@TestVisible
	Map<String, Object> getValuesMap(
		String objKey, 
		Map<String, Map<String,Object>> valuesMapByKey
	) {
		Map<String, Object> valuesMap = valuesMapByKey.get(objKey);
		if (valuesMap == null) {
			valuesMap = new Map<String, Object>();
			valuesMapByKey.put(objKey, valuesMap);
		} 
		return valuesMap;
	}

	public Object getFieldValue(
		String objKey,
		Schema.SObjectField fieldToken,
		Boolean required
	) {
		String fieldApiName = String.valueOf(fieldToken);
		return getFieldValue(objKey, fieldApiName, required);
	}

	public Object getFieldValue(
		String objKey,
		String fieldApiName,
		Boolean required
	) {
		Map<String, Object> objDefaultValues = defaultValues.get(objKey);
		Object result = objDefaultValues?.get(fieldApiName);

		if (
			(result == null) &&
			(required == true)
		) {
			throw new SPDataInitException(
				'Field "' + fieldApiName + '" is required, objKey: ' + objKey // msg
			);
		}
		// else...
		return result;
	}

	public void checkRequiredFieldValue(
		String objKey,
		Schema.SObjectField fieldToken
	) {
		String fieldApiName = String.valueOf(fieldToken);
		checkRequiredFieldValue(objKey, fieldApiName);
	}

	public void checkRequiredFieldValue(
		String objKey,
		String fieldApiName
	) {
		getFieldValue(
			objKey, 
			fieldApiName,
			true // required
		);
	}

	public void storeData() {
		List<SP_Internal_Data__c> objectKeyList = new List<SP_Internal_Data__c>();
		Integer counter = 1;

		for (String key : idByKey.keySet()) {
			objectKeyList.add(
				new SP_Internal_Data__c(
					Name = (ID_PREFIX + counter),
					Key__c = key,
					Value__c = idByKey.get(key)
				)
			);
			counter++;
		}

		counter = 1;
		for (String key : indexByObject.keySet()) {
			objectKeyList.add(
				new SP_Internal_Data__c(
					Name = (INDEX_PREFIX + counter),
					Key__c = key,
					Value__c = String.valueOf(indexByObject.get(key))
				)
			);
			counter++;
		}

		insert objectKeyList;
	}

	void loadData() {
		idByKey.clear();
		indexByObject.clear();
		List<SP_Internal_Data__c> objKeyList = SP_Internal_Data__c.getAll().values();

		for (SP_Internal_Data__c objKey : objKeyList) {
			if (objKey.Name.startsWith(ID_PREFIX)) {
				ID objId = ID.valueOf(objKey.Value__c);
				idByKey.put(objKey.Key__c, objId);

			} else if (objKey.Name.startsWith(INDEX_PREFIX)) {
				Integer objIndex = Integer.valueOf(objKey.Value__c);
				indexByObject.put(objKey.Key__c, objIndex);
			}
		}
	}

}