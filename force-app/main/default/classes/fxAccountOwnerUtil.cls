public without sharing class fxAccountOwnerUtil {
	
	public static void updateFxAccountOwner(List<Lead> leads, Map<Id, Lead> oldMap) {
        Map<Id, Lead> leadById = new Map<Id, Lead>();
        for(Lead l : leads) {
        	if (!LeadUtil.isLiveLead(l) && !LeadUtil.isPracticeLead(l)) {
        		continue;
        	}
        	
            if(oldMap==null || l.OwnerId!=oldMap.get(l.Id).OwnerId) {
                leadById.put(l.Id, l);
            }
        }
        
        if (leadById.size() == 0) {
        	return;
        }
        
        List<fxAccount__c> toUpdate = new List<fxAccount__c>();
        for(fxAccount__c fxa : [SELECT Id, OwnerId, Lead__c FROM fxAccount__c WHERE Lead__c = :leadById.keySet()]) {
            Id leadOwnerId = leadById.get(fxa.Lead__c).OwnerId;
            if(!UserUtil.isUserId(leadOwnerId)) {
                leadOwnerId = UserUtil.getSystemUserId();
            }

            if(fxa.OwnerId!=leadOwnerId) {
                System.debug('Assigning fxAccount ' + fxa.Id + ' to user ' + leadOwnerId);
                fxa.OwnerId = leadOwnerId;
                toUpdate.add(fxa);
            }
        }
        
        if(toUpdate.size()>0) {
            update toUpdate;
        }
    }
	
	static Map<Id, User> activeUsers;
	
	static Map<Id, User> getActiveUsers() {
		if(activeUsers==null) {
			activeUsers = new Map<Id, User>([SELECT Id FROM User WHERE IsActive = false]);
		}
		return activeUsers;
	}
	
	public static void updateFxAccountOwner(List<fxAccount__c> fxAccounts) {
		system.debug('fxAccounts: ' + fxAccounts);
		Set<Id> parentAccountIds = new Set<Id>();
		Set<Id> parentLeadIds = new Set<Id>();
		
		for(fxAccount__c fxa : fxAccounts) {
			if(fxa.Account__c != null) {
				parentAccountIds.add(fxa.Account__c);
			} else if(fxa.Lead__c != null) {
				parentLeadIds.add(fxa.Lead__c);
			}
		}
		
		Map<Id, Account> accountById;
		Map<Id, Lead> leadById;
		if(parentAccountIds.size() > 0) {
			accountById = new Map<Id, Account> ([SELECT Id, OwnerId FROM Account WHERE Id = :parentAccountIds]);
		}
		if(parentLeadIds.size() > 0) {
			leadById = new Map<Id, Lead> ([SELECT Id, OwnerId FROM Lead WHERE Id = :parentLeadIds]);
		}
		
		for(fxAccount__c fxa : fxAccounts) {
			
			Id fxAccountOwnerId; 
				
			if(parentAccountIds.contains(fxa.Account__c) && accountById.get(fxa.Account__c) != null) {
				fxAccountOwnerId = accountById.get(fxa.Account__c).OwnerId;
				System.debug('Using account owner ' + fxAccountOwnerId);
			}
			else if (parentLeadIds.contains(fxa.Lead__c) && leadById.get(fxa.Lead__c) != null) {
				fxAccountOwnerId = leadById.get(fxa.Lead__c).OwnerId;
				system.debug('Using lead owner ' + fxAccountOwnerId);
				if(!UserUtil.isUserId(fxAccountOwnerId)) {
					System.debug('fxAccount owner id ' + fxAccountOwnerId + ' is not a user id, changing to system user');
					fxAccountOwnerId = UserUtil.getSystemUserId();
				}
			}
			else {
				continue;
			}

			if(fxa.OwnerId != fxAccountOwnerId && !getActiveUsers().containsKey(fxAccountOwnerId)) {
				System.debug('Changing owner of fxAccount ' + fxa + ' to ' + fxAccountOwnerId);
				fxa.OwnerId = fxAccountOwnerId;
			}
		}
		system.debug('fxAccounts2: ' + fxAccounts);
	}
	
	public static void updateFxAccountOwner(List<Account> accounts, Map<Id, Account> oldMap) {
		Map<Id, Account> accountById = new Map<Id, Account>();
		for(Account a : accounts) {
			if(a.OwnerId!=oldMap.get(a.Id).OwnerId) {
				accountById.put(a.Id, a);
			}
		}
		List<fxAccount__c> toUpdate = new List<fxAccount__c>();
		for(fxAccount__c fxa : [SELECT Id, OwnerId, Account__c FROM fxAccount__c WHERE Account__c = :accountById.keySet()]) {
			Id accountOwnerId = accountById.get(fxa.Account__c).OwnerId;

			if(fxa.OwnerId!=accountOwnerId) {
				fxa.OwnerId = accountOwnerId;
				toUpdate.add(fxa);
			}
		}
		if(toUpdate.size()>0) {
			update toUpdate;
		}
	}
}