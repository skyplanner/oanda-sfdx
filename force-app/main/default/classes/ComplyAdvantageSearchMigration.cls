/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class ComplyAdvantageSearchMigration  implements Callable, Database.AllowsCallouts {

    public static final Schema.SObjectField SEARCH_ID_FIELD = Comply_Advantage_Search__c.Fields.Search_Id__c;
    public static String SEARCH_REASON;

    public static final String complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    public static final String onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public static final Id complyAdvantageCaseQueueId = UserUtil.getQueueId('Comply Advantage Cases');
       
    public static Map<String, String> countryNameByISOMap =  getCountryCodes();    

    ComplyAdvantageSearchRecord[] searchRecords = new ComplyAdvantageSearchRecord[]{};
 
    Comply_Advantage_Search__c[] searchesToUpdate;
    Comply_Advantage_Search_Entity__c[] entitiesToUpdate;
    
    public String oldInstanceKey;

    public ComplyAdvantageSearchMigration(String sReason, String oldInsName) {
        SEARCH_REASON = sReason;
        oldInstanceKey = getInstanceApiKeyByName(oldInsName);
    }

    public Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'instanceMigrate' {
                instanceMigrate((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'entitiesSync' {
                entitiesSync((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'updateCAEntites' {
                updateCAEntites((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
        }
        return null;
    }

    public void instanceMigrate(List<Comply_Advantage_Search__c> searches) {
        searchesToUpdate = new Comply_Advantage_Search__c[]{};
        entitiesToUpdate = new Comply_Advantage_Search_Entity__c[]{};

        searchRecords = new ComplyAdvantageSearchRecord[]{};

        for (Comply_Advantage_Search__c search: searches) {
            searchRecords.add(new ComplyAdvantageSearchRecord(search, oldInstanceKey));
        }
        for (ComplyAdvantageSearchRecord searchRec : searchRecords) {
            try {           
                migrateSearch(searchRec);
            } 
            catch (Exception ex) {
                Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
            }
        }
        if (searchesToUpdate.size() > 0) {
            Database.SaveResult[] caSaveResult = Database.update(searchesToUpdate, false);
        }
        if (entitiesToUpdate.size() > 0) {
            Database.SaveResult[] entitiesSaveResult = Database.update(entitiesToUpdate, false);
        }
    }
    
    public void migrateSearch(ComplyAdvantageSearchRecord searchRec) {
        ComplyAdvantageSearchResponse caResponse = newSearchCallout(searchRec);
        if (caResponse.statusCode != 200) {
            System.debug('New Search Not Created: ' + searchRec);
            return;
        }
        if (caResponse.newTotalHits >= 100) {
            searchRec.isSecondarySearch = true;
            caResponse = newSearchCallOut(searchRec);
        }

        boolean oldSearchTurnedOff = monitorOffCA(searchRec);
        if (!oldSearchTurnedOff) {
            System.debug('Old Search Not Turned Off: ' + searchRec);
        }

        Comply_Advantage_Search__c searchUpdate = new Comply_Advantage_Search__c(Id=searchRec.recordId);
        searchUpdate.search_reference__c = caResponse.newSearchReference;
        searchUpdate.Search_Reference_Before_OGM_Migration__c = searchRec.searchReference;
        searchUpdate.Search_Id__c = caResponse.newSearchId;
        searchUpdate.Total_Hits__c = caResponse.newTotalHits;
        searchUpdate.Report_Link__c = caResponse.newReportLink;
        searchUpdate.Case_Management_Link__c = 'https://app.complyadvantage.com/#/case-management/search/' + caResponse.newSearchReference;
        searchUpdate.Fuzziness__c = searchRec.divisionSettings.fuzziness * 100;
        searchUpdate.Search_Profile__c = searchRec.searchProfile;
        searchUpdate.is_secondary_search__c = searchRec.isSecondarySearch;
        searchUpdate.Search_Reason__c = SEARCH_REASON;
        
        searchUpdate.Monitor_Change_Date__c = null;
        searchUpdate.Has_Monitor_Changes__c = false;

        if (searchRec.matchStatus != caResponse.newMatchStatus) {
            updateSearchMatchStatus(caResponse.newSearchReference, searchRec.matchStatus, searchRec.divisionSettings.apiKey);
        }
        searchesToUpdate.add(searchUpdate);

        if (searchRec.searchEntities.size() > 0) {
            for (Comply_Advantage_Search_Entity__c entity : searchRec.searchEntities) {
                entity.Monitor_Change_Status__c = '';
                entity.Is_Whitelisted_After_profile_Update__c = false;
                entity.Risk_Level_After_Profile_Update__c = '';
                entity.Match_Status_After_Profile_Update__c = '';
                entity.Unique_ID__c = caResponse.newSearchId + ':' + entity.Entity_Id__c;
                entitiesToUpdate.add(entity);
            }
        }
    }

    public ComplyAdvantageSearchResponse newSearchCallout(ComplyAdvantageSearchRecord searchRec) {
        ComplyAdvantageSearchResponse caResponse = new ComplyAdvantageSearchResponse();
        try {
            String requestBody = getNewSearchRequestBody(searchRec);
            Logger.info('requestBody', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, requestBody);
    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String serviceURL = 'https://api.complyadvantage.com/searches?api_key='+ searchRec.divisionSettings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('POST');
            request.setTimeout(120000);
            request.setBody(requestBody);
            System.debug(requestBody);
            HttpResponse response = http.send(request);
            Logger.info('responseBody', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, response);

            caResponse = new ComplyAdvantageSearchResponse(response);

            if (response.getStatusCode() != 200) {
                System.debug('Wrong new Search Callout: ' + response.getBody());
            }
        }
        catch (Exception ex) {
            Logger.error('Search Profile - New Search', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex.getMessage());
        }
        return caResponse;
    }

    public boolean updateSearchMatchStatus(String searchRef, String matchStatus, String apiKey) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRef +'?api_key='+ apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            String requestBody = '{ "match_status" : "' + matchStatus+'"}';
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);

            if (response.getStatusCode() == 200) {
                return true;
            }
            return false;
        }
        catch (Exception ex) {
            return false;
        }
    }

    public boolean monitorOffCA(ComplyAdvantageSearchRecord searchRec) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'/monitors?api_key='+ searchRec.divisionSettings.oldInstanceApiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            String requestBody = '{ "is_monitored" : false }';
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);

            if (response.getStatusCode() == 200) {
                return true;
            }
            return false;
        }
        catch (Exception ex) {
            return false;
        }
    }

    private String getNewSearchRequestBody(ComplyAdvantageSearchRecord searchRecord) {
        SearchRerquestBody reqBody = new SearchRerquestBody();
       
        reqBody.search_term = searchRecord.searchText;
        reqBody.client_ref = searchRecord.clientReference;
        reqBody.search_profile = searchRecord.searchProfile;
        reqBody.fuzziness =  searchRecord.divisionSettings.fuzziness;

        if (searchRecord.isSecondarySearch) {
           reqBody.exact_match = true;
        }
        reqBody.share_url =  1;

        reqBody.filters = new SearchFilters();
        reqBody.filters.birth_year = String.valueOf(searchRecord.searchParamBirthYear);
        if (String.isNotBlank(searchRecord.searchParamCitizenshipCountry) ||
            String.isNotBlank(searchRecord.searchParamMailingCountry)) {
            Country_Setting__c cnCountrySetting =  CustomSettings.getCountrySettingByName(searchRecord.searchParamCitizenshipCountry);
            Country_Setting__c mcCountrySetting = CustomSettings.getCountrySettingByName(searchRecord.searchParamMailingCountry);
            
            reqBody.filters.country_codes = new String[]{};
            if (cnCountrySetting != null && cnCountrySetting.ISO_Code__c != null) {
                reqBody.filters.country_codes.add(cnCountrySetting.ISO_Code__c);
            }
            if (mcCountrySetting != null && mcCountrySetting.ISO_Code__c != null) {
                reqBody.filters.country_codes.add(mcCountrySetting.ISO_Code__c);
            }
        }
        return JSON.serialize(reqBody);
    }

    public void entitiesSync(List<Comply_Advantage_Search__c> searches) {
        List<Comply_Advantage_Search_Entity__c> entitesToUpsert = new List<Comply_Advantage_Search_Entity__c>{};
        searchesToUpdate = new List<Comply_Advantage_Search__c>{};

        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        Set<Id> searchIds = searchMap.keySet();

        Map<Id, Set<String>> searchEntityUniqueIdsMap = new Map<Id, Set<String>>{};
        Map<String, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId = new Map<String, Comply_Advantage_Search_Entity__c>{};

        //retrieve entities FROM Salesforce for the searches
        for (Comply_Advantage_Search_Entity__c entity : [
            SELECT ID, Unique_ID__c, Comply_Advantage_Search__c
            FROM Comply_Advantage_Search_Entity__c
            WHERE Comply_Advantage_Search__c IN :searchIds]) {    
            Set<String> entityUniqueIds = searchEntityUniqueIdsMap.get(entity.Comply_Advantage_Search__c);
            if (entityUniqueIds == null) {
                entityUniqueIds = new Set<String>{};
            }
            entityUniqueIds.add(entity.Unique_ID__c);
            searchEntityUniqueIdsMap.put(entity.Comply_Advantage_Search__c, entityUniqueIds);

            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }

        for (Comply_Advantage_Search__c search : searches) {
            System.debug('search migrated from: ' + search.Search_Reference_Before_OGM_Migration__c);
            if (String.isNotBlank(search.Search_Reference_Before_OGM_Migration__c)) {
                ComplyAdvantageEntitiesDownloader helper = new ComplyAdvantageEntitiesDownloader(search);
                ComplyAdvantageEntitiesDownloader.EntityDownloadResult result = helper.getEntities();
                if (result.isSuccessful) {
                    Comply_Advantage_Search_Entity__c[] caEntities = result.entities;

                    Set<String> sfSearchEntityIds = searchEntityUniqueIdsMap.get(search.Id);
                    if (sfSearchEntityIds != null && !sfSearchEntityIds.isEmpty()) {
                        caEntities = syncCASFEntities(sfSearchEntityIds, sfBatchEntitiesByUniqueId, caEntities);
                    }
                    entitesToUpsert.addAll(caEntities);
                    
                    search.Entities_Download_Status__c = 'Done_Profile_Update';
                    searchesToUpdate.add(search);
                }
                else {
                    search.Entities_Download_Status__c = 'Failed';
                    searchesToUpdate.add(search);
                }
            }
        }

        if (!entitesToUpsert.isEmpty()) {
            Database.upsert(entitesToUpsert, BatchComplyAdvantageEntitiesDownloader.ENTITY_OBJ_UNIQUE_NUMBER_FIELD, false);
        }
        if (!searchesToUpdate.isEmpty()) {
            Database.update(searchesToUpdate, false);
        }
    }

    //if any entity FROM Salesforce not present in CA result will be marked as 'Removed'
    private Comply_Advantage_Search_Entity__c[] syncCASFEntities(Set<String> sfSearchEntityIds,
                                                                 Map<String, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId,
                                                                 Comply_Advantage_Search_Entity__c[] caEntities) {
        Comply_Advantage_Search_Entity__c[] entities = new Comply_Advantage_Search_Entity__c[]{};
        Set<String> caEntityUniqueIds = new Set<String>{};

        for (Comply_Advantage_Search_Entity__c caEntity : caEntities) {
            String caEntityUniqueId = caEntity.Unique_ID__c;
            if (sfSearchEntityIds.contains(caEntityUniqueId)) {
                //update new match status, whitelisted,risk level for later update
                Comply_Advantage_Search_Entity__c sfEntity = sfBatchEntitiesByUniqueId.get(caEntityUniqueId);
                sfEntity.Is_Whitelisted_After_profile_Update__c = caEntity.Is_Whitelisted__c;
                sfEntity.Match_Status_After_Profile_Update__c = caEntity.Match_Status__c;
                sfEntity.Risk_Level_After_Profile_Update__c = caEntity.Risk_Level__c;
                sfEntity.Is_Removed__c = false;
                entities.add(sfEntity);
            }
            else {
                //new entities
                entities.add(caEntity);
            }
            caEntityUniqueIds.add(caEntityUniqueId);
        }

        //mark removed entites
        for (String sfEntityUniqueId : sfSearchEntityIds) {
            if (!caEntityUniqueIds.contains(sfEntityUniqueId)) {
                Comply_Advantage_Search_Entity__c sfEntity = sfBatchEntitiesByUniqueId.get(sfEntityUniqueId);
                sfEntity.Is_Removed__c = true;
                entities.add(sfEntity);
            }
        }

        return entities;
    } 

    public void updateCAEntites(List<Comply_Advantage_Search__c> searches) {
        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        Set<Id> searchIds = searchMap.keySet();

        Map<String, Comply_Advantage_Search_Entity__c> entityByUniqueId = new Map<String, Comply_Advantage_Search_Entity__c>{};
        Map<Id, Comply_Advantage_Search_Entity__c> entitiesMap = new Map<Id, Comply_Advantage_Search_Entity__c> ([
            SELECT ID,
                Comply_Advantage_Search__c,
                Comply_Advantage_Search__r.Search_Reference_Before_OGM_Migration__c,
                Unique_ID__c, 
                Entity_Id__c,
                Match_Status__c, 
                Match_Status_After_Profile_Update__c,
                Risk_Level__c,
                Risk_Level_After_Profile_Update__c,
                Is_Whitelisted__c, 
                Is_Whitelisted_After_profile_Update__c
            FROM Comply_Advantage_Search_Entity__c
            WHERE Comply_Advantage_Search__c IN :searchIds
                AND Comply_Advantage_Search__r.Search_Reference_Before_OGM_Migration__c != null
                AND Is_Removed__c = false
        ]);

        //process all entities and group entities by Search and update field(match status, risk level, whitelisted)
        Map<Id, Map<String, Set<String>>> matchStatus_searchIdUpdateMap = new Map<Id, Map<String, Set<String>>>{};
        Map<Id, Map<String, Set<String>>> riskLevel_searchIdUpdateMap = new Map<Id, Map<String, Set<String>>>{};
        Map<Id, Map<boolean, Set<String>>> whiteListed_searchIdUpdateMap = new Map<Id, Map<boolean, Set<String>>>{};
        
        for (Comply_Advantage_Search_Entity__c entity : entitiesMap.values()) { 
            entityByUniqueId.put(entity.Unique_ID__c, entity);

            Id complyAdvantageSearchId = entity.Comply_Advantage_Search__c;
            if (entity.Match_Status__c != entity.Match_Status_After_Profile_Update__c) {
                Map<String, Set<String>> updateFieldsMap = matchStatus_searchIdUpdateMap.get(complyAdvantageSearchId);
            
                if (updateFieldsMap == null) {
                    updateFieldsMap = new Map<String, Set<String>>{};
                }

                Set<String> entityIds = updateFieldsMap.get(entity.Match_Status__c);
                if (entityIds == null) {
                    entityIds = new Set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Match_Status__c, entityIds);
                matchStatus_searchIdUpdateMap.put(complyAdvantageSearchId, updateFieldsMap);
            }
            if (entity.Risk_Level__c != entity.Risk_Level_After_Profile_Update__c) {
                Map<String, Set<String>> updateFieldsMap = riskLevel_searchIdUpdateMap.get(complyAdvantageSearchId);
                if (updateFieldsMap == null) {
                    updateFieldsMap = new Map<String, Set<String>>{};
                }

                Set<String> entityIds = updateFieldsMap.get(entity.Risk_Level__c);
                if (entityIds == null) {
                    entityIds = new Set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Risk_Level__c, entityIds);
                riskLevel_searchIdUpdateMap.put(complyAdvantageSearchId,updateFieldsMap);
            }
            if (entity.Is_Whitelisted__c != entity.Is_Whitelisted_After_profile_Update__c) {
                Map<boolean, Set<String>> updateFieldsMap = whiteListed_searchIdUpdateMap.get(complyAdvantageSearchId);
                if (updateFieldsMap == null) {
                    updateFieldsMap = new Map<boolean, Set<String>>{};
                }

                Set<String> entityIds = updateFieldsMap.get(entity.Is_Whitelisted__c);
                if (entityIds == null) {
                    entityIds = new Set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Is_Whitelisted__c, entityIds);
                whiteListed_searchIdUpdateMap.put(complyAdvantageSearchId, updateFieldsMap);
            }
        }

        Map<Id, Comply_Advantage_Search_Entity__c> entitiesToUpdateMap = new  Map<Id, Comply_Advantage_Search_Entity__c>{};
        for (Comply_Advantage_Search__c search :searchMap.values()) {
            Map<String, Set<String>> msUpdateFieldsMap = matchStatus_searchIdUpdateMap.get(search.Id);
            Map<String, Set<String>> rlUpdateFieldsMap = riskLevel_searchIdUpdateMap.get(search.Id);
            Map<boolean, Set<String>> wlUpdateFieldsMap = whiteListed_searchIdUpdateMap.get(search.Id);

            ComplyAdvantageSearchRecord searcRecord = new ComplyAdvantageSearchRecord(search, oldInstanceKey);

            if (msUpdateFieldsMap != null && msUpdateFieldsMap.size() > 0) {
                for (String matchStatus : msUpdateFieldsMap.keySet()) {
                    try {
                        Set<String> entityIds = msUpdateFieldsMap.get(matchStatus);
                        updateEntitiesCallout(searcRecord, entityIds, 'matchStatus', matchStatus);
                        
                        for (String entityId : entityIds) {
                            String uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);
                            
                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdateMap.containsKey(entity.Id) ?
                                                                                entitiesToUpdateMap.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Match_Status_After_Profile_Update__c = matchStatus;
                            entitiesToUpdateMap.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch (Exception ex) {

                    }
                }
            }
            if (rlUpdateFieldsMap != null && rlUpdateFieldsMap.size() > 0) {
                for (String riskLevel : rlUpdateFieldsMap.keySet()) {
                    try {
                        Set<String> entityIds = rlUpdateFieldsMap.get(riskLevel);
                        updateEntitiesCallout(searcRecord, entityIds, 'riskLevel', riskLevel);
                        
                        for (String entityId : entityIds) {
                            String uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);

                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdateMap.containsKey(entity.Id) ?
                                                                                entitiesToUpdateMap.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Risk_Level_After_Profile_Update__c = riskLevel;
                            entitiesToUpdateMap.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch (Exception ex) {

                    }
                }
            }
            if (wlUpdateFieldsMap != null && wlUpdateFieldsMap.size() > 0) {
                for (boolean whiteListed : wlUpdateFieldsMap.keySet()) {
                    try {
                        Set<String> entityIds = wlUpdateFieldsMap.get(whiteListed);
                        updateEntitiesCallout(searcRecord, entityIds, 'whiteListed', whiteListed);
                        
                        for (String entityId : entityIds) {
                            String uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);

                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdateMap.containsKey(entity.Id) ?
                                                                                entitiesToUpdateMap.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Is_Whitelisted_After_profile_Update__c = whiteListed;
                            entitiesToUpdateMap.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch (Exception ex) {

                    }
                }
            }
        }

        if (entitiesToUpdateMap.size() > 0) {
            Database.update(entitiesToUpdateMap.values(), false);
        }
    }

    private boolean updateEntitiesCallout(ComplyAdvantageSearchRecord searchRec,
                                          Set<String> entityIdSet,
                                          String updateType,
                                          object updateValue) {
        try {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'/entities?api_key='+ searchRec.divisionSettings.apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            String[] entityIds = new List<String>(entityIdSet);
            Map<String, Object> obj = new Map<String, Object> {
                'entities' =>  entityIds
            };
            if (updateType == 'matchStatus') {
                String matchStatus = (String) updateValue;
                obj.put('match_status', matchStatus);
            }
            if (updateType == 'riskLevel') {
                String riskLevel = (String) updateValue;
                obj.put('risk_level', riskLevel);
            }
            if (updateType == 'whiteListed') {
                boolean isWhiteListed = (boolean) updateValue;
                obj.put('is_whitelisted', isWhiteListed);
            }

            System.debug('Request' + JSON.serialize(obj));
            String requestBody = JSON.serialize(obj);
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);
            if (response.getStatusCode() == 200) {
                return true;
            }
            return false;
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    private static Map<String, String> getCountryCodes() {
        Map<String, String> countryNamesMap = new Map<String, String>{};
        for (Country_Setting__c countrySetting : Country_Setting__c.getAll().values()) {
            String countryName = countrySetting.Long_Name__c != null ? countrySetting.Long_Name__c : countrySetting.Name;  
            countryNamesMap.put(countrySetting.ISO_Code__c.toUpperCase(), countryName);
        }
        return countryNamesMap;
	} 

    private static String getInstanceApiKeyByName(String instanceName) {
        return [SELECT API_Key__c FROM Comply_Advantage_Instance_Setting__c WHERE Name =:instanceName Limit 1].API_Key__c;
    }

    class SearchRerquestBody {
        String search_term;
        String client_ref;
        Decimal fuzziness;
        String search_profile;
        integer share_url;
        SearchFilters filters;
        Boolean exact_match;
    }

    class SearchFilters {
        String birth_year;
        String entity_type;
        String[] country_codes;
    }

    class DivisionSettings {
        String divisionName;
        String searchProfileName;
        String secondarySearchProfile;
        Decimal fuzziness;

        String apiKey;
        String oldInstanceApiKey;
        
        public DivisionSettings(Comply_Advantage_Division_Setting__c divisionSetting, String oldInstKey) {
            this.divisionName = divisionSetting.Name;
            this.searchProfileName = divisionSetting.Search_Profile__c;
            this.secondarySearchProfile = divisionSetting.Secondary_Search_Profile__c;
            this.oldInstanceApiKey = oldInstKey;

            if (divisionSetting.Fuzziness__c != null && divisionSetting.Fuzziness__c != 0) {
                this.fuzziness = divisionSetting.Fuzziness__c / 100;
            }
            if (divisionSetting.Comply_Advantage_Instance_Setting__r != null) {
                this.apiKey = divisionSetting.Comply_Advantage_Instance_Setting__r.API_Key__c;
            }
        }
    }

    public class ComplyAdvantageSearchResponse {
        String newSearchReference;
        String newSearchId;
        String newMatchStatus;
        integer newTotalHits;
        String newReportLink;

        Integer statusCode;

        public ComplyAdvantageSearchResponse() {
            
        }

        public ComplyAdvantageSearchResponse(HttpResponse response) {
            statusCode = response.getStatusCode();
            if (response != null && response.getStatusCode() == 200)  {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
                Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
                
                //get total_matches, share_url 
                if (responseData.get('id') != null) {
                    this.newSearchId = String.valueOf((integer) responseData.get('id'));
                    this.newSearchReference = (String)responseData.get('ref');
                    this.newTotalHits = (integer)responseData.get('total_matches');
                    this.newMatchStatus = (String)responseData.get('match_status');
                    this.newReportLink = (String)responseData.get('share_url');
                }
            }
        }
    }

    public class ComplyAdvantageSearchRecord {
        Id recordId;

        String clientReference;

        String searchReference;
        String searchId;    

        ComplyAdvantageSearchResponse newSearchResponse;

        String searchText;
        String searchParamCitizenshipCountry;
        String searchParamMailingCountry;
        String searchParamBirthYear;

        Boolean isAliasSearch;
        Boolean isSecondarySearch;

        String matchStatus;
        Decimal totalHits;
        String reportLink;
        String caPortalLink;
        
        String divisionName;

        DivisionSettings divisionSettings;

        Comply_Advantage_Search_Entity__c[] searchEntities;

        public String searchProfile {
            get { return isSecondarySearch ? this.divisionSettings.secondarySearchProfile : this.divisionSettings.searchProfileName;}
            set { searchProfile = value; }
        }

        public ComplyAdvantageSearchRecord(Comply_Advantage_Search__c rec, String oldInstKey) {
            this.recordId = rec.Id;

            this.clientReference = rec.fxAccount__r.Name;
            this.searchReference = rec.Search_Reference__c;
            this.searchId = rec.Search_Id__c;

            this.searchText = rec.Search_Text__c;
            this.searchParamCitizenshipCountry = rec.Search_Parameter_Citizenship_Nationality__c;
            this.searchParamMailingCountry = rec.Search_Parameter_Mailing_Country__c;
            this.searchParamBirthYear = rec.Search_Parameter_Birth_Year__c;
            this.isAliasSearch = rec.Is_Alias_Search__c;

            this.isSecondarySearch = false;

            this.matchStatus = rec.Match_Status__c;
            this.totalHits = rec.Total_Hits__c;
            this.reportLink = rec.Report_Link__c;
            this.caPortalLink = rec.Case_Management_Link__c;
        
            if (String.isNotBlank(rec.Custom_Division_Name__c)) {
                this.divisionName = rec.Custom_Division_Name__c;

                Comply_Advantage_Division_Setting__c ds = ComplyAdvantageUtil.getDivisionSettingByName(
                    divisionName,
                    searchParamMailingCountry
                );
                this.divisionSettings = new DivisionSettings(ds, oldInstKey);
            }

            this.searchEntities = new Comply_Advantage_Search_Entity__c[]{};
            if (rec.Comply_Advantage_Search_Entities__r != null && rec.Comply_Advantage_Search_Entities__r.size() > 0) {
                this.searchEntities = rec.Comply_Advantage_Search_Entities__r;
            }
        }
    }
}