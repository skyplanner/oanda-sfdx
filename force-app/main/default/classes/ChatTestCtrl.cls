/**
 * @File Name          : ChatTestCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2022, 3:08:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/27/2021, 12:42:14 PM   acantero     Initial Version
**/
public without sharing class ChatTestCtrl {

    public String isRedirect {get; set;}
    public String languagePreference {get; set;}
    public String region {get; set;}
    public String customerLastName {get; set;}
    public String customerEmail {get; set;}
    public String accountType {get; set;}

    
    public ChatTestCtrl() {
        this.isRedirect = 'false';
        this.languagePreference = 'en_US';
        this.region = 'OC';
        this.customerLastName = 'Ariel Pestano';
        this.customerEmail = 'pestano@a.com';
        this.accountType = 'Practice';
    }

    public Pagereference reset() {
        this.isRedirect = '';
        this.languagePreference = '';
        this.region = '';
        this.customerLastName = '';
        this.customerEmail = '';
        this.accountType = 'Empty';
        return null;
    }

    public Pagereference invokeChatPage() {
        Pagereference pageRef = Page.LiveChatLanguage;
        preparePage(pageRef);
        return pageRef;
    }

    public Pagereference invokeOfflineHoursPage() {
        Pagereference pageRef = Page.LiveChatOfflineHours;
        preparePage(pageRef);
        return pageRef;
    }

    void preparePage(Pagereference pageRef) {
        Map<String,String> pageParams = pageRef.getParameters();
        prepareParams(pageParams);
    }

    void prepareParams(Map<String,String> pageParams) {
        if (String.isNotBlank(isRedirect)) {
            pageParams.put(ChatConst.IS_REDIRECT, isRedirect);
        }
        if (String.isNotBlank(languagePreference)) {
            pageParams.put(ChatConst.LANGUAGE_PREFERENCE, languagePreference);
        }
        if (String.isNotBlank(region)) {
            pageParams.put('changeRegion', region);
        }
        if (String.isNotBlank(customerLastName)) {
            pageParams.put(ChatConst.CUSTOMER_LAST_NAME, customerLastName);
        }
        if (String.isNotBlank(customerEmail)) {
            pageParams.put(ChatConst.CUSTOMER_EMAIL, customerEmail);
        }
        if (String.isNotBlank(accountType) && (accountType != 'Empty')) {
            pageParams.put(ChatConst.ACCOUNT_TYPE, accountType);
        }
    }
    
}