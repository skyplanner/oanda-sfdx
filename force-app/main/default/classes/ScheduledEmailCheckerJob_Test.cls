/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ScheduledEmailCheckerJob_Test {
	@testSetup
	private static void testSetup() {
		ScheduledEmailTestUtil.createStatusJob(
			SendScheduledEmailJob.SEND_EMAIL_JOB,
			JobStatusHelper.INACTIVE_STATUS
		);
	}
	@isTest
	private static void activateJob() {
		// Test data setup
		String cron_exp = '0 0 * * * ?';
		// Actual test
		Test.startTest();
		String jobId = System.schedule(
			'testScheduledEmailCheckerJob',
			cron_exp,
			new ScheduledEmailCheckerJob()
		);
		CronTrigger ct = [
			SELECT Id, NextFireTime
			FROM CronTrigger
			WHERE id = :jobId
		];
		Datetime now = Datetime.now();
		Datetime nextFire = Datetime.newInstance(
			now.year(),
			now.month(),
			now.day(),
			now.hour() + 1,
			0,
			0
		);
		System.assertEquals(nextFire, ct.NextFireTime, 'Next Hours');
		Test.stopTest();
		Job_Status__c jobStatus = JobStatusHelper.getJobStatus(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		System.assertEquals(
			JobStatusHelper.ACTIVE_STATUS,
			jobStatus.Status__c,
			'The job must be active'
		);

		// Asserts
	}
}