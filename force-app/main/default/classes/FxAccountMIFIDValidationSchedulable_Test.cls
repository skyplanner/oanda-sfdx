/**
 * @File Name          : FxAccountMIFIDValidationSchedulable_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/14/2020, 1:04:20 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/27/2020   acantero     Initial Version
**/
@isTest
private class FxAccountMIFIDValidationSchedulable_Test {

	@isTest
	static void execute_test() {
		Boolean error = false;
        List<fxAccount__c> objList = MIFIDValidationTestDataFactory.createValidableFxAccounts();
		Test.startTest();
		try {
			FxAccountMIFIDValidationSchedulable schedulable = new FxAccountMIFIDValidationSchedulable();
			System.schedule(
            	'My FxAccountMIFIDValidationSchedulable',
            	'0 0 0 15 3 ? 2050', //Dummy CRON expression
				schedulable
        	); 
		} catch (Exception ex) {
			error = true;
		}
		Test.stopTest();
        System.assertEquals(false, error);
	}

}