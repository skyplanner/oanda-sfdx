/**
 * TAS API manager class
 */
public abstract class TasCallout extends Callout {    
    protected Map<String, String> header;
    protected APISettings apiSettings;
    protected NamedCredentialsUtil.NamedCredentialData namedCredentialData;

    /**
     * Constructor
     */
    public TasCallout() {
        header = new  Map<String, String>();
        //The namedCredentialData should replace appSettings after fully implementation
        if (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_Tas_API_Logic__c) {
            namedCredentialData = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
        } else {
            apiSettings = APISettingsUtil.getApiSettingsByName(Constants.TAS_API_WRAPPER_SETTING_NAME);
        }

        if (apiSettings != null) {
            baseUrl = apiSettings.baseUrl;
            header.put('client_id', apiSettings.clientId);
            header.put('client_secret', apiSettings.clientSecret);
            header.put('Content-Type', 'application/json');
        }

        if (namedCredentialData != null) {
            baseUrl = namedCredentialData.credentialUrl;
            header.put('Content-Type', 'application/json');
        }
    }

    protected Exception getError() {
        return exceptionDetails != null
            ? LogException.newInstance(
                Constants.LOGGER_USER_API_CATEGORY,
                exceptionDetails)
            : LogException.newInstance(
                'Error calling TAS API.',
                Constants.LOGGER_USER_API_CATEGORY, req, resp);
    }
}