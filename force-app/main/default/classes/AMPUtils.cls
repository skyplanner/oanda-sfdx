/**
 * Util class for AMP
 * SP-9174
 */
public class AMPUtils {
    
    // Value to consider a big money change
    private static final Decimal MONEY_BIG_CHANGE_VALUE = 100000;

    // Financial fields where analyze some keywords
    private static final Set<String> FXACC_FINANCIAL_FIELDS =
        new Set<String> {
            'Employment_Job_Title__c',
            'Employer_Details__c',
            'Self_Employed_Details__c'
        };

    // Keywords to analyze on financial fields
    private static final Set<String> FXACC_FINANCIAL_FIELDS_VALUES =
        new Set<String> {
            'Trader',
            'Trading',
            'Trades',
            'Forex',
            'Investment',
            'Portfolio', 
            'Advisor',
            'Chase',
            'JP Morgan',
            'Wells Fargo',
            'Capital One',
            'Goldman Sachs',
            'Financial',
            'Gain', 
            'Interactive brokers',
            'Fund manager'};

    @testVisible
    private static final List<String> INDUSTRY_EMP_VALUES =
        new List<String>{
            'Financial',
            'Fund Management',
            'Casino/Betting',
            'Financial/Legal/Accounting'};

    // Income ranges from settings
    private Map<String, Income_Ranges__mdt> incomeRangesMap;

    public AMPUtils() {
        incomeRangesMap = CustomSettings.getIncomeRange();
    }

    /**
     * Update some fields used on
     * BatchOnboardingAMPFollowUp
     */
    public void updateAMP_CaseTags(fxAccount__c newFxAcc, fxAccount__c oldFxAcc) {
        // Know if updated to a PO Box address or 
        // address outside of the US
        if(wasAddressRightUpdated(newFxAcc, oldFxAcc)) {
            newFxAcc.AMP_Street_Changed__c  = true;
        }

        // Know if some financial field was updated
        // and contains one of the financial values defined
        if(wasSomeFinancialFieldRightUpdated(newFxAcc, oldFxAcc)) {
            newFxAcc.AMP_Job_Title_Change__c = true;
        }  
            
        // Know if large money changes
        // - Large increases in income or networth
        // - Large decreases in income or networth
        if(wasWorthIncomeRightUpdated(newFxAcc, oldFxAcc)) {
            newFxAcc.AMP_Net_Worth_Changed__c = true;
        }  

        // Know if industryOfEmployment field 
        // was updated to right value
        if(wasIndustryEmploymentRightUpdated(newFxAcc, oldFxAcc)) {
            newFxAcc.AMP_Industry_Employment_Changed__c = true;
        }
    }

    /**
     * Know if updated to a PO Box address or 
     * address outside of the US
     */
    private Boolean wasAddressRightUpdated(fxAccount__c fxAcc, fxAccount__c oldFxAcc){
        return
            AddressUtils.addressUpdatedToPoBox(fxAcc, oldFxAcc) ||
            AddressUtils.addressUpdatedOutCountry(
                fxAcc,
                oldFxAcc,
                Constants.COUNTRY_UNITED_STATES);
    }

    /**
     * Know if some financial field was updated
     * and contains one of the financial values defined
     */
    private Boolean wasSomeFinancialFieldRightUpdated(
        fxAccount__c fxAcc,
        fxAccount__c oldFxAcc)
    {
        String fxAccVal;

        for(String field :FXACC_FINANCIAL_FIELDS) {
            for(String val :FXACC_FINANCIAL_FIELDS_VALUES) {
                fxAccVal = (String)fxAcc.get(field);
                if(fxAccVal != null &&
                    fxAccVal != oldFxAcc.get(field) &&
                    fxAccVal.containsIgnoreCase(val))  {
                        return true;
                    }         
            }
        }

        return false;
    }

    /**
     * Know if large money changes
     * - Large increases in income or networth
     * - Large decreases in income or networth
     */
    private Boolean wasWorthIncomeRightUpdated(
        fxAccount__c fxAcc,
        fxAccount__c oldFxAcc)
    {
        Decimal newVal;
        Decimal oldVal;

        // Net Worth

        newVal = (Decimal)(fxAcc.Net_Worth_Value__c == null ?
            0 : fxAcc.Net_Worth_Value__c);
        oldVal = (Decimal)(oldFxAcc.Net_Worth_Value__c == null ?
            0 : oldFxAcc.Net_Worth_Value__c);

        // Know if there is a big netWorth change
        if(isBigMoneyChange(newVal, oldVal)) {
            return true;
        }

        // Annual Income using range setting to the numerical value

        newVal = getIncomeNumVal(fxAcc.Annual_Income__c);
        oldVal = getIncomeNumVal(oldFxAcc.Annual_Income__c);

        // Know if there is a big annual income change
        return isBigMoneyChange(newVal, oldVal);
    }

    /**
     * Get numerical value for the income.
     * Getting the range start value
     */
    private Decimal getIncomeNumVal(
        String strIncome)
    {
        if(String.isBlank(strIncome)) {
            return 0;
        }

        if(!incomeRangesMap.containsKey(strIncome)) {
            Logger.error(
                'This income range (' + strIncome + ' ) is not configured by settings.',
                Constants.LOGGER_ONBOARDING_AMP_FOLLOWUP_CATEGORY);
            return 0;
        }

        return
            incomeRangesMap.get(strIncome).Range_Start__c;
    }

    /**
     * Know if industryOfEmployment field 
     * was updated to right value
     */
    private Boolean wasIndustryEmploymentRightUpdated(
        fxAccount__c fxAcc,
        fxAccount__c oldFxAcc)
    {
        return
            fxAcc.Industry_of_Employment__c != oldFxAcc.Industry_of_Employment__c &&
            INDUSTRY_EMP_VALUES.contains(fxAcc.Industry_of_Employment__c);
    }

    /**
     * Know if it is a big money change
     */
    private Boolean isBigMoneyChange(
        Decimal newVal,
        Decimal oldVal)
    {
        return
            newVal != oldVal &&
            Math.abs(newVal - oldVal) >= MONEY_BIG_CHANGE_VALUE;
    }
}