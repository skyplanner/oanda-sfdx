@isTest
private class NF_permissionAssignmentBatchTest {
    testMethod static void testAssignment(){
        test.startTest();
        String query = 'select id, name from user where IsActive = true and (profile.name = \'CX\' or profile.name = \'OB\') LIMIT 100';
        NF_permissionAssignmentBatch obj = new NF_permissionAssignmentBatch(query);
        ID batchID = database.executeBatch(obj);
        test.stopTest();
        system.assert(true,true);
    }
}