/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-16-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
public with sharing class SObjectUtils_Test {
	@isTest
	private static void getObjectNameById() {
		// Test data setup
		String priceBook = 'Pricebook2';

		// Actual test
		Test.startTest();
		String objectName = SObjectUtils.getObjectNameById(
			Test.getStandardPricebookId()
		);
		System.assertEquals(
			true,
			objectName.equalsIgnoreCase(priceBook),
			'Same Name'
		);
		objectName = SObjectUtils.getObjectNameById(null);
		System.assert(objectName == null, 'Name must be null');
		Test.stopTest();

		// Asserts
	}
	@isTest
	private static void getObjectNameByIdNullId() {
		// Test data setup

		// Actual test
		Test.startTest();
		String objectName = SObjectUtils.getObjectNameById(null);
		System.assert(objectName == null, 'Name must be null');
		Test.stopTest();

		// Asserts
	}
	@isTest
	private static void getObjectNameByIdInvalidId() {
		// Test data setup

		// Actual test
		Test.startTest();
		String objectName = SObjectUtils.getObjectNameById('3344');
		System.assert(objectName == null, 'Name must be null');
		Test.stopTest();

		// Asserts
	}
	@IsTest
	static void getObjectTypeByName() {
		Test.startTest();
		Schema.SObjectType sobType = SObjectUtils.getObjectTypeByName(
			'Scheduled_Email__c'
		);
		System.assert(sobType != null, 'SObject Exist');
		sobType = SObjectUtils.getObjectTypeByName('Scheduled_Email');
		System.assert(sobType == null, 'Object not exist');
		Test.stopTest();
	}
	@IsTest
	static void getObjectFieldByName() {
		Test.startTest();
		Schema.SObjectType sobType = SObjectUtils.getObjectTypeByName(
			'Scheduled_Email__c'
		);
		Schema.SObjectField objField = SObjectUtils.getObjectFieldByName(
			sobType,
			'Time__c'
		);
		System.assert(objField != null, 'Field Exist');
		objField = SObjectUtils.getObjectFieldByName(sobType, 'Time');
		System.assert(objField == null, 'Field not exist');
		Test.stopTest();
	}
}