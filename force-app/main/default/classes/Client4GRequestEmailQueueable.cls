public class Client4GRequestEmailQueueable implements Queueable,Database.AllowsCallouts
{
    public Map<Id, string> docsToPrint;
    public set<Id> documentIds;
    public string clientName;
        
    public Client4GRequestEmailQueueable(string clName, Map<Id,string> printDocs, set<Id> docIds)
    {
        clientName = clName;
        docsToPrint = printDocs;
        documentIds = docIds;
    }
	public void execute(QueueableContext context)
    {
        Messaging.SingleEmailMessage[] emailsToSend = new Messaging.SingleEmailMessage[]{};        
        if(docsToPrint != null && docsToPrint.size() > 0)
        {
            Attachment[] emailAttachments = new  Attachment[]{};
            integer marker = 1;
            for(String docId : docsToPrint.keySet())
            {
                Attachment printedDoc = getAttachment(docsToPrint.get(docId), docId);
                emailAttachments.add(printedDoc);

                if(emailAttachments.size() == 10 || marker == docsToPrint.size())
                {
                    Messaging.SingleEmailMessage emailMessage = getEmailMessage(emailAttachments);
                    emailsToSend.add(emailMessage);
                    
                    emailAttachments = new  Attachment[]{};
                }
                marker +=1;
            }
        }
        if(documentIds != null && documentIds.size() > 0)
        {
            Attachment[] emailAttachments = new  Attachment[]{};
            Attachment[] customerDocs = [SELECT Id, Name, Body, ContentType 
                                         FROM Attachment 
                                         WHERE ParentId IN :documentIds];
            
            integer marker = 1;
            for(Attachment doc : customerDocs)
            {
                Attachment a = new Attachment();
                string docType = doc.Name.indexOf('.') == -1 ? '.pdf' : '';
                a.Name = 'Document_' + string.valueOf(marker) + '_' + doc.Name + docType;
                a.Body = doc.Body;
                a.ContentType= doc.ContentType;
                emailAttachments.add(a);

                if(emailAttachments.size() == 10 || marker == customerDocs.size())
                {
                    Messaging.SingleEmailMessage emailMessage = getEmailMessage(emailAttachments);
                    emailsToSend.add(emailMessage);
                    emailAttachments = new Attachment[]{};
                }                
                marker +=1;
            }
        }      
        if(emailsToSend.size() > 0)
        {
            Messaging.sendEmail(emailsToSend);
        }
    }
    
    
    
    public static Attachment getAttachment(string attachmentName, string recordId)
    {
        PageReference pageRef = new PageReference('/apex/PrintView');
        pageRef.getParameters().put('id',recordId);
        Attachment attachment= new Attachment();
        if(Test.isRunningTest())
        {
            attachment.Body = blob.valueOf('Unit.Test');
        }
        else
        {
            attachment.Body = pageRef.getContent();
        }
        attachment.Name = attachmentName + '.pdf';
        attachment.ContentType = 'application/pdf';
        attachment.IsPrivate = false;	
        return attachment;
    }
    
     public Messaging.SingleEmailMessage getEmailMessage(Attachment[] docs)
    {
        string emaildIds;
        if(Settings__c.getValues('Default') != null && string.isNotBlank(Settings__c.getValues('Default').Client_4G_Request_Email_List__c))
        {
            emaildIds = Settings__c.getValues('Default').Client_4G_Request_Email_List__c;
        }
		
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.toAddresses = emaildIds.split(',');
        emailMessage.setSenderDisplayName('Salesforce Support');
        emailMessage.setSubject('4G Request - ' + clientName + ' Records');
        emailMessage.setHtmlBody('Please find attached documents for customer ' + clientName);
        emailMessage.setBccSender(false);
        emailMessage.setUseSignature(false);

       //Set email file attachments
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for (Attachment a : docs)
        {
            // Add to attachment file list
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(a.Name);
            efa.setBody(a.Body);
            fileAttachments.add(efa);
        }
        emailMessage.setFileAttachments(fileAttachments);

        return emailMessage;
    }
    
}