/**
 * @description       : 
 * @author            : Eugene Jung
 * @group             : 
 * @last modified on  : 11-10-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
public class TaxDeclarationTriggerHandlerTest 
{
    //final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);
    //static final Id systemUserId = UserUtil.getUserIdByName('System User');

    @TestSetup
    static void initData()
    {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = new TestDataFactory().createFXTradeAccount(acc);
        Tax_Declarations__c td = new Tax_Declarations__c(
            fxAccount__c = fxAcc.Id
        );
        insert td;
        fxAcc.Tax_Declarations__c = td.Id;
        update fxAcc;
    }

    @IsTest
    static void validateCustomerRiskAssessmentChangeTest()
    {
        Tax_Declarations__c td = [SELECT Id FROM Tax_Declarations__c];

        Test.startTest();
        td.Is_In_Queue__c = true;
		update td;
        Test.stopTest();

        Case[] ihaConfirmCases = [SELECT Id, Subject FROM Case WHERE Subject = 'IHS confirmation request'];
		system.assertEquals(1, ihaConfirmCases.size());
    }
	
}