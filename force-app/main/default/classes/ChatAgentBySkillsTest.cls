/**
 * @File Name          : ChatAgentBySkillsTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 4:06:46 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 4:04:42 PM   aniubo     Initial Version
 **/
@isTest
private class ChatAgentBySkillsTest {
	@isTest
	private static void testExistsCompatibleAgents() {
		// Test data setup
		String langSkill = ServiceLanguagesManager.getSkillByLangCode('es');
		// Actual test
		Test.startTest();

		ChatAgentBySkills chatAgentBySkills = new ChatAgentBySkills();
		Set<String> compatibleAgents = chatAgentBySkills.existsCompatibleAgents(
			new Set<String>{ langSkill }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			0,
			compatibleAgents.size(),
			'There are no agents available.'
		);
	}
	@isTest
	private static void testExistsCompatibleAgentsSkillsSetEmpty() {
		// Test data setup
		// Actual test
		Test.startTest();

		ChatAgentBySkills chatAgentBySkills = new ChatAgentBySkills();
		Set<String> compatibleAgents = chatAgentBySkills.existsCompatibleAgents(
			new Set<String>()
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			0,
			compatibleAgents.size(),
			'There are no agents available.'
		);
	}
}