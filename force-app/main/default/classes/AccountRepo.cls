/**
 * @File Name          : AccountRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/8/2024, 1:06:58 PM
**/
public inherited sharing class AccountRepo {

	public static Account getSummaryByEmail(String email) {
		List<Account> recList = [
			SELECT 
				Name,
				fxAccount__c,
                PersonContactId
			FROM Account
			WHERE PersonEmail = :email
			LIMIT 1
		];
		Account result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static Account getFxAccountInfo(ID accountId) {
		List<Account> recList = [
			SELECT
				fxAccount__r.Name,
				fxAccount__r.Funnel_Stage__c
			FROM Account
			WHERE Id = :accountId
			LIMIT 1
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'Account',
			'Id = ' + accountId
		);
		return recList[0];
	}

	public static Account getAuthenticationInfoById(ID recordId) {
		List<Account> recList = [
			SELECT
				Authentication_Code__c,
				Authentication_Code_Expires_On__c,
				PersonEmail
			FROM Account
			WHERE Id = :recordId
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'Account',
			'Id = ' + recordId
		);
		return recList[0];
	}


}