@isTest
public class GbgManagerTest {

    final static User REGISTRATION_USER =
        UserUtil.getUserByName(
            UserUtil.NAME_REGISTRATION_USER);
    final static Id DOC_RT_SCAN_ID =
        RecordTypeUtil.getScanDocumentTypeId();

    @isTest
    static void gbgScan() {
        GbgSettings stts = GbgSettings.getInstance();
        
        // No continue if gbg is disabled
        if(!stts.isGbgEnabled()) {
            return;
        }
        
        List<String> enabledDivs =
            stts.getEnabledDivisions();

        // No continue if thre is not an
        //  enabled gbg division
        if(enabledDivs.isEmpty()) {
            return;
        }

        // Get enabled division from setting
        String division = enabledDivs[0];

        // Run as 'Registration' user
        System.runAs(REGISTRATION_USER) {
            TestDataFactory testHandler =
                new TestDataFactory();
            fxAccount__c fxa1 =
                testHandler.createFXTradeAccount(
                    testHandler.createTestAccount());
            fxa1.fxTrade_User_Id__c = 1234;
            fxa1.Division_Name__c = division;
            update fxa1;

            Case case1 = new Case(
                Subject = 'sample',
                Origin = 'Phone',
                Live_or_Practice__c = 'Live',
                Priority = 'Normal',
                Inquiry_Nature__c = 'Registration',
                Status = 'Open',
                Division__c = division,
                RecordTypeId = 
                    Schema.SObjectType.Case.getRecordTypeInfosByName().get(
                        'Support').getRecordTypeId());
		    insert case1;

            // Create documents 

            Document__c doc1 = 
                new Document__c(
                    Name = 'doc1',
                    fxAccount__c = fxa1.Id,
                    fxTrade_User_Id__c = fxa1.fxTrade_User_Id__c,
                    Case__c = case1.Id,
                    RecordTypeId =
                        DOC_RT_SCAN_ID,
                    Document_Type__c =
                        Constants.DOC_TYPE_DRIVER_LICENSE);

            Document__c doc2 =
                new Document__c(
                    Name = 'doc2',
                    fxAccount__c = fxa1.Id,
                    fxTrade_User_Id__c = fxa1.fxTrade_User_Id__c,
                    Case__c = case1.Id,
                    RecordTypeId =
                        DOC_RT_SCAN_ID,
                    Document_Type__c =
                        Constants.DOC_TYPE_DRIVER_LICENSE);

            // Document who is not SCAN type
            Document__c doc3 =
                new Document__c(
                    Name = 'doc3',
                    Case__c = case1.Id,
                    fxAccount__c = fxa1.Id,
                    fxTrade_User_Id__c = fxa1.fxTrade_User_Id__c);

            // Documents with scan already In-Progress should successfully upload but without new event
            Document__c doc4 =
                new Document__c(
                    Name = 'doc4',
                    fxAccount__c = fxa1.Id,
                    Case__c = case1.Id,
                    fxTrade_User_Id__c = fxa1.fxTrade_User_Id__c,
                    GBG_Status__c = GbgManager.GBG_INPROGRESS_STATUS);

            List<Document__c> docs =
                new List<Document__c>{
                    doc1, doc2, doc3, doc4};

            insert docs;
            
            // Get documents
            docs = 
                [SELECT Name, GBG_Status__c 
                    FROM Document__c];

            // Verify that the documents 1-3 do not have any gbg status yet
            for (Document__c doc : docs) {
                if (doc.Name != 'doc4') {
                    System.assertEquals(
                    doc.GBG_Status__c,
                    NULL,
                    'Document.Gbg_Status__c has wrong value after inserting.');
                }
            }

            Test.startTest();
                // Insert attachments to docs
                insert new List<Attachment>{
                    new Attachment(
                        ParentId = doc1.Id,
                        Name = 'att1',
                        Body = Blob.valueOf('att1 test content')),
                    new Attachment(
                        ParentId = doc2.Id,
                        Name = 'att2',
                        Body = Blob.valueOf('att2 test content')),
                    new Attachment(
                        ParentId = doc3.Id,
                        Name = 'att3',
                        Body = Blob.valueOf('att3 test content')),
                    new Attachment(
                        ParentId = doc4.Id,
                        Name = 'att4',
                        Body = Blob.valueOf('att4 doc1 test report content'))};
            Test.stopTest();

            // Get documents
            docs = 
                [SELECT Name,
                        GBG_Status__c 
                    FROM Document__c];
            
            // Check docs got the righ gbg status
            for(Document__c doc :docs) {
                if(doc.Name == 'doc1' || doc.Name == 'doc2') {
                    System.assertEquals(
                        doc.GBG_Status__c,
                        GbgManager.GBG_INPROGRESS_STATUS,
                        'Document.Gbg_Status__c was not updated to \'In-Progress\'');
                } else if(doc.Name == 'doc3') {
                    System.assertEquals(
                        doc.GBG_Status__c,
                        NULL,
                        'The NOT scan document Gbg_Status__c was updated to \'In-Progress\'');
                }
            }

            // Verify that action platform events were published for only doc1 and doc2
            System.assertEquals(
                ActionUtil.lastPublished.size(),
                2,
                'Platform events not published.');

            // Verify that the actions platform events have the right doc data (docs 1 and 2)
            Set<Id> docIds = new Set<Id>{
                doc1.Id, doc2.Id};

            for(Action__e action : ActionUtil.lastPublished) {
                System.assert(
                    docIds.contains(action.Parameter_1__c),
                    'Action do not have the right Parameter_1__c value.');
                System.assertEquals(
                    action.fxTrade_User_Id__c, 
                    String.valueOf(doc1.fxTrade_User_ID__c),
                    'Action do not have the right fxTrade_User_Id__c value.');
                System.assertEquals(
                    action.Type__c, 
                    ActionUtil.GBG_TYPE,
                    'Action do not have the right type value.');        
            }
        }
    }

    @isTest
    static void gbgScanDisabled() {
        GbgSettings stts = GbgSettings.getInstance();
        
        List<String> enabledDivs =
            stts.getEnabledDivisions();

        if(!stts.isGbgEnabled() || enabledDivs.isEmpty()) {
            // Run as 'Registration' user
            System.runAs(REGISTRATION_USER) {
                TestDataFactory testHandler =
                    new TestDataFactory();
                fxAccount__c fxa1 =
                    testHandler.createFXTradeAccount(
                        testHandler.createTestAccount());
                fxa1.fxTrade_User_Id__c = 1234;
                fxa1.Division_Name__c =
                    Constants.DIVISIONS_OANDA_EUROPE;
                update fxa1;

                Case case1 = new Case(
                    Subject = 'sample',
                    Origin = 'Phone',
                    Live_or_Practice__c = 'Live',
                    Priority = 'Normal',
                    Inquiry_Nature__c = 'Registration',
                    Status = 'Open',
                    Division__c = Constants.DIVISIONS_OANDA_EUROPE,
                    RecordTypeId = 
                        Schema.SObjectType.Case.getRecordTypeInfosByName().get(
                            'Support').getRecordTypeId());
                insert case1;

                // Create document
                Document__c doc1 = 
                    new Document__c(
                        Name = 'doc1',
                        fxAccount__c = fxa1.Id,
                        fxTrade_User_Id__c = fxa1.fxTrade_User_Id__c,
                        Case__c = case1.Id,
                        RecordTypeId =
                            DOC_RT_SCAN_ID);
                insert doc1;
    
                // Get documents
                List<Document__c> docs = 
                    [SELECT Name, GBG_Status__c 
                        FROM Document__c];

                // There is only one doc
                System.assertEquals(docs.size(), 1);

                Test.startTest();
                    // Insert attachments to docs
                    insert new Attachment(
                            ParentId = doc1.Id,
                            Name = 'att1',
                            Body = Blob.valueOf('att1 test content'));
                Test.stopTest();

                // Get documents
                docs = 
                    [SELECT Name,
                            GBG_Status__c 
                        FROM Document__c];
                
                System.assertEquals(docs.size(), 1);

                doc1 = docs[0];

                // Check docs was not scanned and not get the gbgStatus filled
                System.assertEquals(
                    doc1.GBG_Status__c,
                    NULL,
                    'The document Gbg_Status__c was updated to \'In-Progress\'');

                // Verify that action platform events were not published
                System.assert(
                    ActionUtil.lastPublished == NULL,
                    'Platform events published.');
            }
        }
    }
    
}