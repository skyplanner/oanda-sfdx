/**
 * @File Name          : ChatSLAViolationNotifierDataReader.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 4:42:16 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 4:42:16 PM   aniubo     Initial Version
**/
public with sharing class ChatSLAViolationNotifierDataReader implements SLAViolationDataReader {
	public List<SObject> getData(
		SLAViolationNotificationData notificationData
	) {
		ChatSLAViolationNotificationData chatNotificationData = (ChatSLAViolationNotificationData) notificationData;
		return ChatTranscriptRepo.getChatTranscriptsForSLANotification(
			chatNotificationData.recordIds
		);
	}
}