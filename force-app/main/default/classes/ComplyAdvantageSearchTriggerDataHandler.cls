public with sharing class ComplyAdvantageSearchTriggerDataHandler  extends TriggerDataHandler
{
    public static final Schema.SObjectType FXACCOUNT_SOBJECT_TYPE = Schema.fxAccount__c.getSObjectType();

    public ComplyAdvantageSearchTriggerDataHandler()
    {
        newRecordsByType = new Map<Schema.SObjectType, list<SObject>>
        {

        };
        updateRecordsByType = new  Map<Schema.SObjectType,  Map<Id, SObject>>
        {
            FXACCOUNT_SOBJECT_TYPE => new Map<Id, fxAccount__c>{}
        };
    }

    public void commitTriggerChanges()
    {
        commitRecords(FXACCOUNT_SOBJECT_TYPE, true);

        reset();
    } 

    public override void reset()
    {
        updateRecordsByType = new  Map<Schema.SObjectType,  Map<Id, SObject>>
        {
            FXACCOUNT_SOBJECT_TYPE => new Map<Id, fxAccount__c>{}
        }; 
    }


    //  public override void insertRecord(SObject rec)
    //  public override SObject getRecord(Id recordId)
    //  public override void updateRecord(SObject rec)
    
}