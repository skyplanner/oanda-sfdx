/**
 * Throwable exception for comply advantage work.
 * @author Fernando Gomez
 * @since 10/15/2021
 */
public class ComplyAdvantageException extends Exception { }