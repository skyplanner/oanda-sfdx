/**
 * @File Name          : OrgWideEmailAddressRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 4:50:44 AM
**/
public inherited sharing class OrgWideEmailAddressRepo {

	public static OrgWideEmailAddress getByAddress(String address) {
		List<OrgWideEmailAddress> recList = [
			SELECT Id
			FROM OrgWideEmailAddress
			WHERE Address = :address
		];
		OrgWideEmailAddress result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

}