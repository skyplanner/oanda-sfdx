public with sharing class BatchSendFailedCKAEmails implements Database.Batchable<SObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
    String query;
    Boolean isRerun = false;
    List<Id> ids = new List<Id>();
    static final Integer BATCH_SIZE = 1;
    public static final String CRON_NAME = 'BatchSendFailedCKAEmails';
    public static final String CRON_SCHEDULE_Monthly = '0 0 7 10 * ?';
    public static final String EMAIL_TEMPLATE = 'Failed_CKA_Email';
    public static final String AFC_TEAM_QUEUE = 'AFC Team Cases';
    public static final String FRONTDESK_MAIL = 'frontdesk@oanda.com';

    public BatchSendFailedCKAEmails() {
        query='SELECT Id, PersonContactId FROM Account WHERE Send_Failed_CKA_Email__c=TRUE';
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
        ids = recIds;
        query = 'SELECT Id, PersonContactId FROM Account WHERE Id IN: ids';
        isRerun=true;
        return Database.executeBatch(this, batchSize);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<SObject> batch) {
        List<Account> accs = (List<Account>) batch;
        List<Case> cases = new List<Case>();
        Map<Id,String> personContactsByAccountId = new Map<Id,String>();
        for(Account acc: accs){
            String queueId = UserUtil.getQueueId(AFC_TEAM_QUEUE);
            Case newCase = new Case(
                    AccountId = acc.Id,
                    ContactId = acc.PersonContactId,
                    RecordTypeId = RecordTypeUtil.getSupportComplianceCaseTypeId(),
                    Subject = 'Action Required: Customer Knowledge Assessment',
                    Type__c = 'Suitability',
                    OwnerId = queueId,
                    Status = 'Closed'
            );
            cases.add(newCase);
            personContactsByAccountId.put(acc.Id, acc.PersonContactId);
            acc.Send_Failed_CKA_Email__c=false;
        }
        insert cases;
        for(Case c: cases){
            EmailUtil.sendEmailByTemplate(personContactsByAccountId.get(c.AccountId), EMAIL_TEMPLATE, c.Id, true, FRONTDESK_MAIL);
        }
        TriggersUtil.disableTriggers(
                new List<TriggersUtil.Triggers>{
                        TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE,
                        TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER});
        update accs;
    }

    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchSendFailedCKAEmails(), BATCH_SIZE);
    }

    public static void schedule() {
        System.schedule(CRON_NAME, CRON_SCHEDULE_Monthly, new BatchSendFailedCKAEmails());
    }

    public static Id executeBatch() {
        return Database.executeBatch(new BatchSendFailedCKAEmails(), BATCH_SIZE);
    }
}