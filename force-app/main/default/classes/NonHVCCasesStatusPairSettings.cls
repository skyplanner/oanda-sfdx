/**
 * @File Name          : NonHVCCasesStatusPairSettings.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 3:26:12 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/6/2024, 3:03:10 PM   aniubo     Initial Version
 **/
public with sharing class NonHVCCasesStatusPairSettings {
	public Integer maxNonHvcCases { get; set; }
	public Integer minPercentageNonHvc { get; set; }
	public String statusNotAllow { get; set; }
	public String statusAllow { get; set; }

	public static NonHVCCasesStatusPairSettings createFromByStatusAllowing(
		Non_HVC_Cases_Status_Pair__mdt settings
	) {
		NonHVCCasesStatusPairSettings result = new NonHVCCasesStatusPairSettings();
		result.maxNonHvcCases = getIntegerValue(settings?.Max_Number_Cases__c);
		result.statusNotAllow = settings?.Status_not_allow__c;
		return result;
	}

	public static NonHVCCasesStatusPairSettings createFromByStatusNotAllowing(
		Non_HVC_Cases_Status_Pair__mdt settings
	) {
		NonHVCCasesStatusPairSettings result = new NonHVCCasesStatusPairSettings();
		result.minPercentageNonHvc = getIntegerValue(
			settings?.Min_Occupancy_Percentage__c
		);
		result.statusAllow = settings?.Status_allowing__c;
		result.maxNonHvcCases = getIntegerValue(settings?.Max_Number_Cases__c);
		return result;
	}

	@testVisible
	private static Integer getIntegerValue(Decimal value) {
		if (value == null) {
			return 0;
		}
		if (value.intValue() < 0) {
			return Math.abs(value).intValue();
		}
		return value.intValue();
	}
}