/* Name: KnowledgeAssessmentRestResourceTest
 * Description : Test Class to handle knowledge assessment api
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 June 04
 */

 @isTest
 public class KnowledgeAssessmentRestResourceTest {
     
   public static final string REQUEST_BODY = '{\"experience_result\":\"Pass\",\"knowledge_answer_1\":\"Buying Euros\",\"knowledge_answer_2\":\"A CFD\",\"knowledge_result\":\"Pass\",\"overall_result\":\"Pass\"}';
   public static final string REQUEST_URI_TEMPLATE = '/api/v1/user/{0}/assessment';
   public static final string REST_RESOURCE_PATH = '/api/v1/user/*/assessment';

   public static final Integer VALID_USER_ID = 12345;
   public static final Integer INVALID_USER_ID = 00000;
 
   @TestSetup
   static void initData(){

      User testuser1 = UserUtil.getRandomTestUser();    
      TestDataFactory testHandler = new TestDataFactory();
      Account account = testHandler.createTestAccount();
      Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
      insert contact;

      fxAccount__c fxAccount = new fxAccount__c();
      fxAccount.Account__c = account.Id;
      fxAccount.Contact__c = contact.Id;
      fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
      fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
      fxAccount.Division_Name__c = 'OANDA Europe';
      fxAccount.Citizenship_Nationality__c = 'Canada';
      fxAccount.OwnerId = testuser1.Id; 
      fxAccount.fxTrade_User_ID__c = VALID_USER_ID;
      fxAccount.fxTrade_Global_ID__c = VALID_USER_ID + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
      fxAccount.Net_Worth_Value__c = 10000;
      fxAccount.Liquid_Net_Worth_Value__c = 5000;
      fxAccount.US_Shares_Trading_Enabled__c = false;
      fxAccount.Email__c = account.PersonEmail;
      fxAccount.Name = 'testusername';
      insert fxAccount;
   }
 
   @isTest
   public static void testDoPostWithProperFxAcc() {
      
      RestRequest request = createRequest(VALID_USER_ID);
      RestResponse response = new RestResponse();

      RestContext.request = request;
      RestContext.response = response;

      KnowledgeAssessmentRestResource.doPost();
      
      Assert.areEqual(200, response.statusCode, 'Response Code should be 200');
   }

   @isTest
   public static void testDoPostWithInvalidFxAcc() {
      
      RestRequest request = createRequest(INVALID_USER_ID);
      RestResponse response = new RestResponse();

      RestContext.request = request;
      RestContext.response = response;

      KnowledgeAssessmentRestResource.doPost();
      Assert.areEqual(404, response.statusCode, 'Response Code should be 404');
   }

   @isTest
   public static void testDoPostWithInvalidParamFxAcc() {
      
      RestRequest request = createRequest(null);
      RestResponse response = new RestResponse();

      RestContext.request = request;
      RestContext.response = response;

      KnowledgeAssessmentRestResource.doPost();
      
      Assert.areEqual(500, response.statusCode, 'Response Code should be 500');
   }

   @isTest
   public static void testDoPost() {
      
      RestRequest request = createRequest(VALID_USER_ID);
      RestResponse response = new RestResponse();

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();
      KnowledgeAssessmentRestResource.doPost();
      Test.getEventBus().deliver();
      Test.stopTest();

      fxAccount__c fxa = [
         SELECT 
            Id, 
            OEL_Trading_Experience_Result__c, 
            OEL_Assessment_Result__c, 
            Knowledge_Result__c, 
            Knowledge_Answer_1__c, 
            Knowledge_Answer_2__c 
         FROM fxAccount__c 
         LIMIT 1
      ];
      
      Assert.areEqual('Pass', fxa.OEL_Trading_Experience_Result__c, 'Result should be "Pass"');
      Assert.areEqual('Pass', fxa.OEL_Assessment_Result__c, 'Result should be "Pass"');
      Assert.areEqual('Pass', fxa.Knowledge_Result__c, 'Result should be "Pass"');
      Assert.areEqual('Buying Euros', fxa.Knowledge_Answer_1__c, 'Result should be "Buying Euros"');
      Assert.areEqual('A CFD', fxa.Knowledge_Answer_2__c, 'Result should be "A CFD');
   }

   private static RestRequest createRequest(Integer userId) {
      RestRequest request = new RestRequest();

      request.requestUri = String.format(REQUEST_URI_TEMPLATE, new List<Integer>{userId});
      request.resourcePath = REST_RESOURCE_PATH;
      request.httpMethod = 'POST';
      request.addHeader('Content-Type', 'application/json');
      request.requestBody = Blob.valueOf(REQUEST_BODY);

      return request;
   }
}