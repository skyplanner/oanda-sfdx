public with sharing class AccountUtil {

    public static boolean isBusinessAccount(Account acc) {
        return acc.RecordTypeId == RecordTypeUtil.getExchangeRatesRecordTypeId();
    }
    
    public static boolean isFxtradeCustomer(Account acc) {
        return acc.RecordTypeId == RecordTypeUtil.getPersonAccountRecordTypeId();
    }
    
    public static Map<Id, Id> getContactIdByAccountId(Set<Id> accountIds) {
        Map<Id, Id> contactIdByAccountId = new Map<Id, Id>();
        for(Contact c : [SELECT Id, AccountId FROM Contact WHERE AccountId IN :accountIds]) {
            contactIdByAccountId.put(c.AccountId, c.Id);
        }
        return contactIdByAccountId;
    }

    // Query all fxAccounts under the Accounts to recalculate Customer Risk assessment
    // TODO: Delete it when "Update CRA from Account Change" process builder is deleted
    @InvocableMethod
    public static void updateCRAFromAccount(List<Id> accountIds) {
        fxAccountRiskAssessment.calculateCustomerRiskAssessment(
            new Set<Id>(accountIds), true);
    }

    /**
     * Calculate Customer Risk Assessment
     */
    public static void calculateCustomerRiskAssessment(
        List<Account> accs,
        Map<Id, Account> oldMap)
    {
        Set<Id> accIds = new Set<Id>();

        for(Account acc :accs) {
            if(acc.personMailingCountry != oldMap.get(acc.Id).personMailingCountry)
                accIds.add(acc.Id);
        }

        fxAccountRiskAssessment.calculateCustomerRiskAssessment(
            accIds, true);
    }

    /**
     * Calculate Customer Risk Assessment
     */
    public static void calculateCustomerRiskAssessment(
        List<Account> accs)
    {
        Set<Id> accIds = new Set<Id>();

        for(Account acc :accs) {
            if(String.isNotBlank(acc.personMailingCountry))
                accIds.add(acc.Id);
        }

        fxAccountRiskAssessment.calculateCustomerRiskAssessment(
            accIds, true);
    }

    // Anonymize account emails and phone numbers
    public static void formatAccountInfo(List<Account> accs) {
        List<Account> accountsToUpdate = new List<Account>();

        for(Account a : accs) {

            if(a.PersonEmail != null) {
                a.PersonEmail = a.PersonEmail.substringBefore('@') + '@example.com';
            }
            
            if(a.Phone != null) {
                a.Phone = a.Phone.left(a.Phone.length() - 4) + '5555';
            }

            accountsToUpdate.add(a);
        }

        update accountsToUpdate;
    }
    // IB status check
    public static void checkIBStatus(List<Account> trgNew , Map<Id, Account> trgOldMap)
    {
        for(Account acc : trgNew )
        {
            //set the deactivation timestamp
            if(acc.IB_Status__c=='Inactive' && (trgOldMap.get(acc.Id)).IB_Status__c!= 'Inactive' )
            {
                acc.IB_Deactivation_Timestamp__c = System.now();
            }
            //clear the time stamp
            if(acc.IB_Status__c=='Active' )
            {
                acc.IB_Deactivation_Timestamp__c = null;
            }
        }
    }

    /**
     * Get fxAccounts from an account list
     */
    public static Map<Id, fxAccount__c> getFxAccs(
        List<Account> accs)
    {
        Set<Id> fxAccIds =
            getFxAccIds(accs);

        return fxAccIds.isEmpty() ?
            new Map<Id, fxAccount__c>() :
            new Map<Id, fxAccount__c>(
                [SELECT Id,
                        RecordTypeId,
                        Government_ID__c,
                        fxTrade_User_Id__c
                        // Add more here...
                    FROM fxAccount__c 
                    WHERE Id IN :fxAccIds]);
    }
    
    /**
     * Get fxAccounts from an account list
     */
    public static Set<Id> getFxAccIds(
        List<Account> accs)
    {
        return
            SObjectUtil.getFieldIds(
                'fxAccount__c',
                accs);
    }

    /**
     * Know if account is ready to Sync
     */
    public static Boolean isReadyToSync(
        Account acc,
        fxAccount__c fxAcc)
    {
        return
            String.isNotBlank(acc.fxAccount__c) &&
            acc.PersonEmail != null &&
            !acc.PersonEmail.contains(Constants.OANDA_DOMAIN) &&
            fxAccountUtil.isLiveAccount(fxAcc) &&
            fxAcc.fxTrade_User_Id__c != null;
    }

    /**
     * Know if an account is 'Traded'
     */
    public static Boolean isTraded(Account acc) {
        return
            acc.Funnel_Stage__pc == Constants.FUNNEL_TRADED;
    }

    /**
     * Know if account is from 'Asia' division
     */
    public static Boolean isPrimaryDivisionAsia(Account acc) {
        return
            Constants.DIVISIONS_ASIA_LIST.contains(
                acc.Primary_Division_Name__c);
    }

    /**
     * Sets the birth date into Birthdate_DR__c 
     * field from BirthDate__c
     */
    public static void setBirthDate(Account acc){
        acc.Birthdate_DR__c = 
            acc.BirthDate__c == Constants.DEFAULT_BIRTHDATE ? null : formatdate(acc.BirthDate__c);
    }

    /**
     * Formats the given date into MM/dd/YYYY format
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   26/02/2021
     * @return: String, the formated date as string
     * @param:  pDate, the date to be formatted
     */
    private static String formatDate(Date pDate){
        if(pDate != null){
            return pDate.month() + '/' + pDate.day() + '/' + pDate.year();
        }
        return '';
    }

    /**
     * Set the tax id and determine if it is a dummy data. Set the 
     * result in the field xxxxx 
     */
    public static void setTaxId(
        Account acc,
        fxAccount__c fxAcc)
    {
        if(String.isNotBlank(acc.fxAccount__c) && 
            fxAcc != null) {
                acc.Tax_ID_Format_Review__c =
                    !isValidTaxId(fxAcc?.Government_ID__c);
        }
    }

    static Pattern invalidP;
    static Pattern validP;
    static List<String> invalidTokenList;
    /**
     * Validate if the given text is a valid taxId format
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   12/03/2021
     * @return: Boolean, return true if the has a valid format, false in other case
     * @param:  pTaxId, the tax id to evaluate
     */
    public static Boolean isValidTaxId(String pTaxId){
        String invalidExp = '(.)\\1{4,}';
        if(String.isBlank(pTaxId) || pTaxId.length() < 7){
            return false;
        }
        if(invalidP == null){
            invalidP = Pattern.compile(invalidExp);
        }
        //If it matches 4 or more consecutive repeated characters
        Matcher pm = invalidP.matcher(pTaxId);
        if(pm.find()){
            return false;
        }
        if(invalidTokenList == null){
            invalidTokenList = new List<String>();
            for(Tax_Id_Invalid_Match__mdt item : [SELECT MasterLabel, DeveloperName FROM Tax_Id_Invalid_Match__mdt]){
                invalidTokenList.add(item.MasterLabel);
            }
        }        
        //If it contains any of the predefined invalid tokens
        for(String tk : invalidTokenList){
            if(pTaxId.toUpperCase().contains(tk.toUpperCase())){
                return false;
            }
        }
        if(validP == null){
            validP = Pattern.compile('^[a-zA-Z0-9]*$');
        }
        // If does not contains any non alphanumeric
        pm = validP.matcher(pTaxId);
        if(pm.matches()){
            return true;
        }
        return false;        
    }

    /**
     * Set accounbt "Demo Conversion Score Snapshot"
     */
    public static void setDemoConversionScoreSnapshot(
        Account acc)
    {
        if(acc.Demo_Conversion_Snapshot__c == null &&
            acc.First_Live_fxAccount_DateTime__c != null &&
            acc.First_Practice_fxAccount_DateTime__c != null &&
            acc.First_Practice_fxAccount_DateTime__c < acc.First_Live_fxAccount_DateTime__c) {
                acc.Demo_Conversion_Snapshot__c =
                   acc.Demo_Score__c;
        }
    }

	/**
	 * @return true if the current user is allowed to delete accounts.
	 */
	public static Boolean isDeletionAllowed() {
		// users must have this permission to delete account
		return FeatureManagement.checkPermission('Account_Deletion');
	}

	/**
	 * @return true if the current user is allowed to delete accounts.
	 */
	public static void preventDeletion(List<Account> accounts) {
		for (Account acc : accounts)
			acc.addError('You don\'t have the required permission to delete accounts');
	}
}