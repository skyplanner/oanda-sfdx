/**
 * @File Name          : DocumentManager_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10/24/2019, 12:41:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/24/2019   dmorales     Initial Version
**/
@isTest
private class DocumentManager_Test {
    
    static Document getTestDocument(){
        DocumentManager c = new DocumentManager();
        Document d;
		d = new Document(
			Name = 'Test_Document',
			ContentType = 'application/json',
			DeveloperName = 'TestDoc',
			FolderId = c.folderId,		
			IsPublic = true
		);

		insert d;
        return d;
    }

    static String getTestContentVersion(){
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.VersionData = Blob.valueOf('V.1');
        cv.Title = 'Test Content Version';
        cv.PathOnClient = 'TestContentVersion';
        insert cv;
        return cv.Id;
    }
    
   
    @isTest
    static void testCreatePreviewDocumentId() {
        DocumentManager c = new DocumentManager();
        Document d = getTestDocument();
        Document d1 = c.createPreviewDocumentId(d.Id);
        System.assert(d.DeveloperName == d1.DeveloperName);
    }

    @isTest
    static void testNewCreatePreviewDocumentId() {
        DocumentManager c = new DocumentManager();
        Document d1 = c.createPreviewDocumentId(null);
        System.assert(d1.FolderId == c.folderId);
    }

     @isTest
    static void testGetPreviewDocumentOrNull() {
        DocumentManager c = new DocumentManager();
        Document d = getTestDocument();
        Document d1 = c.getPreviewDocumentOrNull(d.Id);
        System.assert(d.DeveloperName == d1.DeveloperName);

        Document d2 = c.getPreviewDocumentOrNull(null);
        System.assert(d2 == null);
    }

    @isTest
    static void testConvertFileToDocument() {
        DocumentManager c = new DocumentManager();
        
        String idDoc = c.convertFileToDocument('TestFileDoc', 'application/.json', 'Test Body');
        System.assert(idDoc != '');

        String idContentV = getTestContentVersion();
        String idDoc1 = c.convertFileToDocument(idContentV);
        System.assert(idDoc1 != '');

    }
    
  
}