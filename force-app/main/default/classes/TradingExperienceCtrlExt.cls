/*
 *  @Author : Deepak Malkani.
 *  @Created Date : March 21 2017
 *  @Purpose : Used to display Trading Experience Duration and Types in a Custom VF Page on FX Account Level.
*/
/*
 *  @Author : Deepak Malkani.
 *  @Updated Date : May 8 2017
 *  @Purpose : Fixed JIRA Story # SP-4253, TradingExperienceVFPage on fxAccount not displaying correctly. Fixing this BUG now.
*/

public with sharing class TradingExperienceCtrlExt {
    transient fxAccount__c fxa;
    transient Boolean isOAUDiv, isOCANDiv;
    transient List<TradingExperience> tradingExperience = new List<TradingExperience>();
    
    public TradingExperienceCtrlExt(ApexPages.StandardController controller) {
        // Exit if ID is empty
        if(String.isEmpty(controller.getId()))
            return;
        
        // Initialize with SOQL queries
        fxa = [SELECT Id,
                      Trading_Experience_Type__c,
                      Trading_Experience_Duration__c,
                      Division_Name__c,
                      Experience_Trading_CFDs_or_FX__c,
                      Other_Relevant_Experience__c
                FROM fxAccount__c
                WHERE id = : controller.getId()
                LIMIT 1];
        
        isOAUDiv = fxAccountUtil.isOAUDiv(fxa);
        isOCANDiv = fxAccountUtil.isOCANDiv(fxa);

        if(isOAUDiv) {

        } else {
            List<Trading_Experience_Type_Code__c> typeList = [SELECT Type_Code__c, Type__c FROM Trading_Experience_Type_Code__c];
            List<Trading_Experience_Duration_Code__c> durationList = [SELECT Duration_Code__c, Duration__c, Score__c FROM Trading_Experience_Duration_Code__c];
            String regexPattern = '^2\\d\\d$';
            
            // Exit if there is missing fxAccount data
            if(String.isEmpty(fxa.Trading_Experience_Type__c) || String.isEmpty(fxa.Trading_Experience_Duration__c))
                return;
            
            // Split the fxAccount Duration and Type codes
            List<String> fxaDurations = fxa.Trading_Experience_Duration__c.split(',');
            List<String> fxaTypes = fxa.Trading_Experience_Type__c.split(',');
            
            // Transform the lists of sObjects to Maps
            Map<String, String> typeMap = new Map<String, String>();
            for(Trading_Experience_Type_Code__c type : typeList) {
                typeMap.put(type.Type_Code__c, type.Type__c);
            }
            Map<String, String> DurationMap = new Map<String, String>();
            for(Trading_Experience_Duration_Code__c duration : durationList) {
                durationMap.put(duration.Duration_Code__c, duration.Duration__c);
            }
            Map<String, Decimal> scoreMap = new Map<String, Decimal>();
            for(Trading_Experience_Duration_Code__c duration : durationList) {
                scoreMap.put(duration.Duration_Code__c, duration.Score__c);
            }
            
            // Zip the two lists; there is no zip utility in Apex
            // Each result is a list with two elements: Duration and Volume
            Map<String, List<String>> results = new Map<String, List<String>>();
            for(Integer i=0; i<fxaTypes.size(); i++) {
                if(Pattern.Matches(regexPattern, fxaTypes[i])) {
                    // If the Type is 200-level, then add a volume
                    List<String> result = new List<String> { null, fxaDurations[i] };
                    results.put(fxaTypes[i], result);
                } else {
                    // Else, add a Duration
                    List<String> result = new List<String> { fxaDurations[i], null };
                    results.put(fxaTypes[i], result);
                }
            }
            
            // If there is an equivalent 100-level type for a given 200-level type then delete it
            // e.g., Delete 102 if 202 already exists
            for(String fxaType : results.keySet()) {
                if(Pattern.Matches(regexPattern, fxaType)) {
                    if (results.containsKey('1'+fxaType.right(2)))
                        results.remove('1' + fxaType.right(2));
                }
            }
            
            // Add the results to the Trading Experiences
            for(String fxaType : results.keySet()) {
                List<String> result = results.get(fxaType);
                
                String type = typeMap.get(fxaType);
                String duration = durationMap.get(result[0]);
                String volume = result[1];
                Decimal score = scoreMap.get(result[0]);
                
                // Add result if there is a type and one of (duration, volume)
                if (!String.isEmpty(type) && !String.isEmpty(duration+volume))
                    tradingExperience.add(new TradingExperience(type, duration, volume, score));
            }
        }
    }
    
    public List<TradingExperience> getTradingExperience(){
        return tradingExperience;
    }

    public Boolean getIsOAUDiv(){
        return isOAUDiv;
    }

    public Boolean getIsOCANDiv(){
        return isOCANDiv;
    }

    public fxAccount__c getFxa(){
        return fxa;
    }

    public Boolean getHasTradingExperience(){
        return tradingExperience.size() > 0;
    }

    public class TradingExperience {
        public String expDuration { get; set; }
        public String expType { get; set; }
        public String expVolume { get; set; }
        public Decimal expScore { get; set; }        
        
        //constructor for the inner class
        public TradingExperience(String type, String duration, String volume, Decimal score) {
            this.expType = type;
            this.expDuration = duration;
            this.expVolume = volume;
            this.expScore = score;
        }
    }
    
}