/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-20-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class ScheduledEmailController {
	@AuraEnabled(cacheable=true)
	public static ScheduledEmailManager.ScheduledEmailWrapper getScheduledEmail(
		Id caseId
	) {
		return new ScheduledEmailManager(new ScheduledEmailSettingManager())
			.getScheduleEmail(caseId);
	}
 
	@AuraEnabled
	public static ScheduledEmailManager.ScheduledEmailWrapper saveScheduledEmail(
		Id caseId,
		ScheduledEmailManager.ScheduledEmailWrapper wrapper
	) {
		return new ScheduledEmailManager(new ScheduledEmailSettingManager())
			.saveScheduleEmail(caseId, wrapper);
	}
	@AuraEnabled
	public static ScheduledEmailManager.ScheduledEmailWrapper deleteScheduledEmail(
		Id scheduledEmailId
	) {
		return new ScheduledEmailManager(new ScheduledEmailSettingManager())
			.deleteScheduleEmail(scheduledEmailId);
	}
}