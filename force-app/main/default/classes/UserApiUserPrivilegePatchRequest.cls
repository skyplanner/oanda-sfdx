/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-21-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiUserPrivilegePatchRequest {
    public List<UserApiUserPrivilege> user_privileges {get; set;}

    public static UserPrivilegePatchRequestWrapper getGrantPremiumAccessWrapper() {
        UserPrivilegePatchRequestWrapper result 
            = new UserPrivilegePatchRequestWrapper();
        result.privileges = new UserApiUserPrivilegePatchRequest();
        result.privileges.user_privileges = new List<UserApiUserPrivilege> {
            new UserApiUserPrivilege(
                UserApiUserGetResponse.PREMIUM_ACCESS_KEY, 1)
        };

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_PREMIUM_ACCESS, false, true);
        return result;
    }

    public static UserPrivilegePatchRequestWrapper getRevokePremiumAccessWrapper() {
        UserPrivilegePatchRequestWrapper result 
            = new UserPrivilegePatchRequestWrapper();
        result.privileges = new UserApiUserPrivilegePatchRequest();
        result.privileges.user_privileges = new List<UserApiUserPrivilege> {
            new UserApiUserPrivilege(
                UserApiUserGetResponse.PREMIUM_ACCESS_KEY, 0)
        };

        result.change = new AuditTrailManager.AuditTrailChange(
            Constants.API_FIELD_PREMIUM_ACCESS, true, false);
        return result;
    }

    public class UserPrivilegePatchRequestWrapper {
        public UserApiUserPrivilegePatchRequest privileges {get; set;}

        public AuditTrailManager.AuditTrailChange change {get; set;}
    }
}