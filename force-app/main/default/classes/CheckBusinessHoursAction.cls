/**
 * @File Name          : CheckBusinessHoursAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/22/2024, 11:51:58 AM
**/
public without sharing class CheckBusinessHoursAction { 

	@InvocableMethod(label='Check Business Hours' callout=false)
	public static List<Boolean> checkBusinessHours() {   
		try {
			ExceptionTestUtil.execute();
			ChatOfflineSettings chatSetting = 
				ChatOfflineSettings.getDefaultInstance(); 
			Boolean duringBusinessHours = !chatSetting.isOffline;
			List<Boolean> result = new List<Boolean> { duringBusinessHours };
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CheckBusinessHoursAction.class.getName(),
				ex
			);
		}
		/*
		// ONLY FOR TEST
		return new List<Boolean> { 
			SPSettingsManager.checkSetting('Chatbot_Business_Hour_Test') 
		};  
		*/ 
	} 

}