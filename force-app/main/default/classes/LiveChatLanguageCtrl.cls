/**
 * @File Name          : LiveChatLanguageCtrl.cls
 * @Description        : 
 * @Author             : Daymi Morales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 09-14-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/01/2019   Daymi Morales     Initial Version
**/

public without sharing class LiveChatLanguageCtrl extends BaseLiveChatCtrl {
	public static final String SHOW_OFFLINE_WARNING = 'showOfflineWarning';

	transient String langBtnUrl;

	public Boolean englishLangAvailable {get; private set;}
	public Boolean chineseLangAvailable {get; private set;}
	public Boolean germanLangAvailable {get; private set;}
	public Boolean spanishLangAvailable {get; private set;}
	public Boolean russianLangAvailable {get; private set;}
	public Boolean polishLangAvailable {get; private set;}
	public Boolean frenchLangAvailable {get; private set;}
	public Boolean italianLangAvailable {get; private set;}

	public Boolean englishLangEnabled {get; private set;}
	public Boolean chineseLangEnabled {get; private set;}
	public Boolean germanLangEnabled {get; private set;}
	public Boolean spanishLangEnabled {get; private set;}
	public Boolean russianLangEnabled {get; private set;}
	public Boolean polishLangEnabled {get; private set;}
	public Boolean frenchLangEnabled {get; private set;}
	public Boolean italianLangEnabled {get; private set;}

	public Boolean chatAvailable {get; private set;}
	public Boolean showOfflineWarning {get; private set;}
	public String fromLanguage {get; private set;}
	public Boolean isRedirect {private set;get;}
	public Boolean redirectToOfflineHoursPage {get; private set;}

	public Boolean checkActiveEventMessageChat {
		get {
			Event_Message__c activeEventMessage = EventMessageUtil.getActiveEventMessage('Chat Form');
			Boolean result = (activeEventMessage == null);
			return result;
		}
	}

	public String getNextUrl() {
		return getNextUrl(checkActiveEventMessageChat);
	}

	public String getNextUrl(Boolean pCheckActiveEventMessageChat) {
		if(pCheckActiveEventMessageChat == true) {
			return Page.LiveChatWizard.getUrl();
		}
		//else...
		return Page.LiveChatEventMessage.getUrl();
	}

	public String getRefreshUrl() {
		return Page.LiveChatLanguage.getUrl();
	}

	public String getWhen() {
		return String.valueOf(Datetime.now().getTime());
	}

	public LiveChatLanguageCtrl() {
		super();	
		chatAvailable = false;
		redirectToOfflineHoursPage = 
			//!CustomSettings.isCryptoEnabledForOmniChannel() && 
			offlineHours;
		updateLanguageFromParams();
		Map<String,String> pageParams = ApexPages.currentPage().getParameters();
		String isRedirectParam = pageParams.get(ChatConst.IS_REDIRECT);
		isRedirect = (isRedirectParam == 'true');
		String showOfflineWarningParam = pageParams.get(SHOW_OFFLINE_WARNING);
		showOfflineWarning = (showOfflineWarningParam == 'true');

		if (showOfflineWarning == false) {
			fromLanguage = language;
			initLanguages();
		} else {
			fromLanguage = pageParams.get(ChatConst.FROM_LANGUAGE);
		}
	}

	void initLanguages() {
		englishLangAvailable = false;
		chineseLangAvailable = false;
		germanLangAvailable = false;
		spanishLangAvailable = false;
		russianLangAvailable = false;
		polishLangAvailable = false;
		frenchLangAvailable = false;
		italianLangAvailable = false;
		checkLanguageAvailability();
	}

	public void checkLanguageAvailability() {
		ChatLanguagesManager chatLangManager = ChatLanguagesManager.getInstance();
		//..
		englishLangEnabled = chatLangManager.englishLangEnabled;
		chineseLangEnabled = chatLangManager.chineseLangEnabled;
		germanLangEnabled = chatLangManager.germanLangEnabled;
		spanishLangEnabled = chatLangManager.spanishLangEnabled;
		russianLangEnabled = chatLangManager.russianLangEnabled;
		polishLangEnabled = chatLangManager.polishLangEnabled;
		frenchLangEnabled = chatLangManager.frenchLangEnabled;
		italianLangEnabled = chatLangManager.italianLangEnabled;
		//...
		englishLangAvailable = chatLangManager.englishLangAvailable;
		chineseLangAvailable = chatLangManager.chineseLangAvailable;
		germanLangAvailable = chatLangManager.germanLangAvailable;
		spanishLangAvailable = chatLangManager.spanishLangAvailable;
		russianLangAvailable = chatLangManager.russianLangAvailable;
		polishLangAvailable = chatLangManager.polishLangAvailable;
		frenchLangAvailable = chatLangManager.frenchLangAvailable;
		italianLangAvailable = chatLangManager.italianLangAvailable;
		//...
		chatAvailable = (
			englishLangAvailable ||
			chineseLangAvailable ||
			germanLangAvailable ||
			spanishLangAvailable ||
			russianLangAvailable ||
			polishLangAvailable ||
			frenchLangAvailable ||
			italianLangAvailable
		);
	}

	public PageReference checkOfflineHoursRedirect() {
		PageReference result = null;

		if (redirectToOfflineHoursPage) {
			result = Page.LiveChatOfflineHours;
			result.getParameters().put('LanguagePreferenceOffline', language);
			result.setRedirect(true);
		}

		return result;
	}
}