/**
 * Controller for component: fxAccountViewer
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 5/2/2019
 */
global without sharing class FxAccountViewerCtrl {
	private Id accountId;
	private Boolean isLive;

	@TestVisible
	private static final String LIVE_RECORD_TYPE = 'Retail Live';
	@TestVisible
	private static final String PRACTICE_RECORD_TYPE = 'Retail Practice'; 

	/**
	 * @param accountId
	 * @param isLive 	Whether we need to render live or practice accounts
	 */
	global FxAccountViewerCtrl(Id accountId, Boolean isLive) {
		this.accountId = accountId;
		this.isLive = isLive;
	}

	/**
	 * @return the wrapped fx account, grouped by username.
	 */
	global List<UsernameWrapper> getFxAccounts() {
		List<fxAccount__c> accounts;
		UsernameWrapper current;
		List<UsernameWrapper> result;
		Map<String, UsernameWrapper> mapped;

		// live or pactice is a record type
		accounts = isLive ? getLiveAccounts() : getPracticeAccounts();
		result = new List<UsernameWrapper>();
		mapped = new Map<String, UsernameWrapper>();

		// we then organize by username
		for (fxAccount__c acc : accounts)
			if (mapped.containsKey(acc.Name))
				mapped.get(acc.Name).accounts.add(new FxAccountWrapper(acc));
			else {
				current = new UsernameWrapper(acc.Name);
				current.accounts.add(new FxAccountWrapper(acc));
				mapped.put(acc.Name, current);
				result.add(current);
			}

		return result;
	} 

	/**
	 *
	 * @param accountId
	 * @param isLive 	Whether we need to render live or practice accounts
	 * @return all fxAccounts related to the current account. 
	 */
	@AuraEnabled
	global static List<UsernameWrapper> getFxAccounts(Id accountId, Boolean isLive) {
		FxAccountViewerCtrl ctrl;
		try {
			ctrl = new FxAccountViewerCtrl(accountId, isLive);
			return ctrl.getFxAccounts();
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @return live accounts
	 */
	private List<fxAccount__c> getLiveAccounts() {
		return [
			SELECT Id,
				Name,
				Balance_USD__c,
				Has_Open_Trade__c,
				Funnel_Stage__c,
				Type__c,
				Admin_URL__c,
				CRM_URL__c
			FROM fxAccount__c
			WHERE Account__c = :accountId
			AND RecordTypeId = :getRecordTypeId(LIVE_RECORD_TYPE)
			ORDER BY Name
			LIMIT 2000
		];
	}

	/**
	 * @return practice accounts
	 */
	private List<fxAccount__c> getPracticeAccounts() {
		return [
			SELECT Id,
				Name,
				Balance_USD__c,
				Has_Open_Trade__c,
				Funnel_Stage__c,
				Type__c,
				Admin_URL__c,
				CRM_URL__c
			FROM fxAccount__c
			WHERE Account__c = :accountId
			AND RecordTypeId = :getRecordTypeId(PRACTICE_RECORD_TYPE)
			ORDER BY Name
			LIMIT 2000
		];
	}

	/**
	 * @return the id of the selected record type (live or practice)
	 */
	private Id getRecordTypeId(String recordTypeName) {
		return Schema.SObjectType.fxAccount__c.getRecordTypeInfosByName().get(
			recordTypeName).getRecordTypeId();
	}

	/**
	 * Wrapper: Username
	 */
	global class UsernameWrapper {
		@AuraEnabled
		global String username { get; private set; }
		@AuraEnabled
		global List<FxAccountWrapper> accounts { get; private set; }

		/**
		 * Constructor
		 * @param username
		 */
		private UsernameWrapper(String un) {
			this.username = un;
			accounts = new List<FxAccountWrapper>();
		}
	}

	/**
	 * Wrapper: Username
	 */
	global class FxAccountWrapper {
		@AuraEnabled
		global Id recordId { get; private set; }
		@AuraEnabled
		global Decimal balance { get; private set; } // Balance_USD__c
		@AuraEnabled
		global Boolean hasOpenTrade { get; private set; } // Has_Open_Trade__c
		@AuraEnabled
		global String funnelStage { get; private set; } // Funnel_Stage__c
		@AuraEnabled
		global String type { get; private set; } // Type__c
		@AuraEnabled
		global String crmLinkUrl { get; private set; }
		@AuraEnabled
		global String adminLinkUrl { get; private set; }

		/**
		 * Constructor
		 * @param acc
		 */
		private FxAccountWrapper(fxAccount__c acc) {
			recordId = acc.Id;
			balance = acc.Balance_USD__c;
			hasOpenTrade = acc.Has_Open_Trade__c;
			funnelStage = acc.Funnel_Stage__c;
			type = acc.Type__c;
			crmLinkUrl = acc.CRM_URL__c;
			adminLinkUrl = acc.Admin_URL__c;
		}
	} 
}