/**
 * @File Name          : GetAgentOwnedMsgSessionsCmd.cls
 * @Description        : 
 * Returns the messaging sessions whose owner is a standard user
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/9/2024, 7:17:26 PM
**/
public inherited sharing class GetAgentOwnedMsgSessionsCmd {

	final List<MessagingSession> recList;

	public GetAgentOwnedMsgSessionsCmd(List<MessagingSession> recList) {
		this.recList = recList;
	}

	public List<MessagingSession> execute() {
		if (
			(recList == null) ||
			recList.isEmpty()
		) {
			return new List<MessagingSession>();
		}
		//else...
		Set<Id> standardUserIds = getStandardUserIds(getUserIds());
		List<MessagingSession> result = getValidMsgSessions(standardUserIds);
		return result;
	}

	@TestVisible
	List<MessagingSession> getValidMsgSessions(Set<Id> standardUserIds) {
		List<MessagingSession> result = new List<MessagingSession>();

		for (MessagingSession rec : recList) {
			if (
				(rec.OwnerId != null) &&
				(standardUserIds.contains(rec.OwnerId))
			) {
				result.add(rec);
			}
		}

		return result;
	}

	@TestVisible
	Set<Id> getStandardUserIds(Set<ID> userIds) {
		if (userIds.isEmpty()) {
			return new Set<Id>();
		}
		// else...
		Map<ID, User> standardUsersById = new Map<ID, User>(
			UserRepo.getSummaryForStandardUsers(userIds)
		);
		return standardUsersById.keySet();
	}

	@TestVisible
	Set<ID> getUserIds() {
		Set<ID> result = new Set<ID>();

		for (MessagingSession rec : recList) {
			if (
				(rec.OwnerId != null) &&
				(rec.OwnerId.getSobjectType() == Schema.User.SObjectType)
			) {
				result.add(rec.OwnerId);
			}
		}

		return result;
	}
	
}