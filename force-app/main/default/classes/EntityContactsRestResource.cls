/**
 * @description       :
 * @author            : OANDA
 * @group             :
 * @last modified on  : 31 Jul 2024
 * @last modified by  : Ryta Yahavets
 * @Description
 * URL: /services/apexrest/api/v1/user/{user_id}/entity_contacts
**/
@RestResource(urlMapping='/api/v1/user/*/entity_contacts')
global with sharing class EntityContactsRestResource {

    private static final String ENDPOINT_NAME = 'entity_contacts';
    private static final String LOGGER_CATEGORY_URL = '/api/v1/user/*/entity_contacts';
    public static final List<String> GET_PARAMS = new List<String>{'user_id'};
    @TestVisible
    private static final String TRUSTED_CONTACT_PERSON = 'Trusted Contact Person';

    private static final Map<String, String> responseFieldsBySFFields = new Map<String, String>{
            'Id' => 'Id',
            'First_Name__c' => 'first_name',
            'Last_Name__c' => 'last_name',
            'Phone__c' => 'telephone_number',
            'Email__c' => 'email_address',
            'Street__c' => 'street',
            'City__c' => 'city',
            'State_Province__c' => 'province',
            'Country__c' => 'country',
            'Postal_Code__c' => 'postal_code',
            'Third_Party_Determination__c' => 'third_party_determination',
            'Trusted_Contact_Person__c' => 'trusted_contact_person',
            'Relationship__c' => 'relationship_to_me'
    };

    @HttpGet
    global static void doGet() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, GET_PARAMS);

        try {
            Integer userId = Integer.valueOf(context.getPathParam('user_id'));
            fxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

            if (!isFxAccountValid(fxa, context)) {
                return;
            }

            List<Map<String,Object>> response = new List<Map<String,Object>>();

            for(Entity_Contact__c entityContact : getEntityContacts(fxa)) {
                Map<String,Object> responseRow = new Map<String,Object>();

                for (String field : responseFieldsBySFFields.keySet()) {
                    responseRow.put(responseFieldsBySFFields.get(field), entityContact.get(field));
                }
                response.add(responseRow);
            }

            context.setResponse(200, response);
            return;

        } catch (Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                    req.resourcePath,
                    req.requestURI,
                    res.statusCode + ' ' + respBody);
            return;
        }
    }

    @HttpPut
    global static void doPut() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, GET_PARAMS);

        try {
            Integer userId = Integer.valueOf(context.getPathParam('user_id'));
            fxAccount__c fxa = FxAccountSelector.getLiveFxAccountByUserId(userId);

            if (!isFxAccountValid(fxa, context)) {
                return;
            }

            List<Entity_Contact__c> entityContacts = getEntityContacts(fxa);
            Entity_Contact__c entityContact = entityContacts.isEmpty()
                    ? new Entity_Contact__c(
                            RecordTypeId = RecordTypeUtil.getEntityContactGeneralRecordTypeId(),
                            Type__c = TRUSTED_CONTACT_PERSON,
                            Lead__c = String.isNotBlank(fxa.Lead__c) ? fxa.Lead__c : null,
                            Account__c = String.isNotBlank(fxa.Account__c) ? fxa.Account__c : null)
                    : entityContacts.get(0);

            EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
            Map<String, Endpoints_Mappings__mdt> currentObjMapping = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME).get('Entity_Contact__c');

            Map<String, Object> payload = (Map<String, Object>) JSON.deserializeUntyped(context.getBodyAsString());

            ApiMappingService mapper = new ApiMappingService();
            mapper.loggerCategory = ENDPOINT_NAME;
            mapper.loggerCategoryUrl = LOGGER_CATEGORY_URL;

            for(String inputField : payload.keySet()) {
                if(!currentObjMapping.containsKey(inputField) || payload.get(inputField) == null) {
                    continue;
                }
                mapper.setInputFieldtoSobject(
                        entityContact,
                        currentObjMapping.get(inputField),
                        payload
                );
            }

            try{
                upsert entityContact;
                context.setResponse(
                        200,
                        new Map<String, Object>{'isSuccessful' => true, 'id' => entityContact.Id}
                );
            } catch (DmlException ex) {
                context.setResponse(400, ex.getMessage());
                Logger.exception(LOGGER_CATEGORY_URL, ex);
                return;
            }
        } catch (Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() + ex.getStackTraceString();
            Logger.error(
                    req.resourcePath,
                    req.requestURI,
                    res.statusCode + ' ' + respBody
            );
            context.setResponse(500, respBody);
        }
    }

    private static Boolean isFxAccountValid(fxAccount__c fxAccount, RestResourceHelper context) {
        Boolean isValid = true;
        if (fxAccount == null) {
            context.setResponse(404, 'Could not find user with given Id');
            isValid = false;
        } else if (String.isBlank(fxAccount.Lead__c) && String.isBlank(fxAccount.Account__c)) {
            context.setResponse(404, 'Given user does not have related Lead or Account record');
            isValid = false;
        }

        return isValid;
    }

    private static List<Entity_Contact__c> getEntityContacts(fxAccount__c fxAccount){
        String query = 'SELECT ' + String.join(responseFieldsBySFFields.keySet(), ', ') +
            ' FROM Entity_Contact__c where Type__c = \'' + TRUSTED_CONTACT_PERSON + '\'';

        if (String.isNotBlank(fxAccount.Lead__c)) {
            query += ' AND Lead__c =\'' + fxAccount.Lead__c + '\'';
        } else if (String.isNotBlank(fxAccount.Account__c)) {
            query += ' AND Account__c =\'' + fxAccount.Account__c + '\'';
        }

        return Database.query(query);
    }
}