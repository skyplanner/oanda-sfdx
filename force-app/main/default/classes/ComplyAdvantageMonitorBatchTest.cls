/**
 * Tests: 
 * ComplyAdvantageMonitorBatch
 */
@isTest
private class ComplyAdvantageMonitorBatchTest {

	@TestSetup	
	static void makeData() {
		Comply_Advantage_Instance_Setting__c caInstanceSetting;
		Settings__c settings;
		Comply_Advantage_Division_Setting__c caDivisionSetting;

		settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name='NA',
			API_Key__c='test'
		);
		insert caInstanceSetting;
		
		caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c =
				'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;
	}

	/**
	 * Test:
	 * public void execute(Database.BatchableContext BC, List<sObject> scope)
	 */
	@isTest
	static void execute() {
		List<Account> accs;
		fxAccount__c fxAcc1;
		List<Comply_Advantage_Search__c> searches;
		
		accs = (new TestDataFactory()).createTestAccounts(1);

		// comply adv is disabled, so no problem
		fxAcc1 = new fxAccount__c(
			Account_Email__c = 'test1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false
		);
		insert fxAcc1;

		// ca searches
		searches = new List<Comply_Advantage_Search__c> {
			new Comply_Advantage_Search__c(
				Name = 'Sample',
				fxAccount__c = fxAcc1.Id,
				Search_Id__c = '478009745',
				Is_Monitored__c = true
			)
		};
		insert searches;

		// now we launch the batch
		Test.setMock(HttpCalloutMock.class,
			new CalloutMock(200, '', 'success', null));

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		Test.startTest();
		fxAcc1.Is_Closed__c = true;
		update fxAcc1;
		Test.stopTest();

		System.assert([
			SELECT Is_Monitored__c 
			FROM Comply_Advantage_Search__c
			WHERE Id = :searches[0].Id
		][0].Is_Monitored__c == false);
	}
}