public with sharing class fxAccountBeforeTriggerHandler extends fxAccountTriggerHandler
{
     //Afiliate details
    Set<Decimal> cellxpertPartnerIds;
    Map<Decimal, Affiliate__c> cellxpertPartnerIdAffiliateMap;
    Boolean amINotSystemUser = UserInfo.getUserId() != UserUtil.getSystemUserId();

    public fxAccountBeforeTriggerHandler(boolean trgInsert,
                                        boolean trgUpdate,
                                        list<fxAccount__c> triggerNew,
                                        Map<Id, fxAccount__c> triggerOldMap) 
    {
        super(trgInsert,trgUpdate,true,false,triggerNew,triggerOldMap);

        //get related details first before we process the records
        if(isInsert || isUpdate) {
            getRelatedDetails();
        }
    }

    public void onBeforeInsert() {
        //SP-13570 logic from fxAccount_Before.triggers 
        //START
        //iterate records here..have all processing under this.


            for(fxAccount__c newFxAcc : trgNew) {
                //format record before insert. all formatting for the record should be in this method.
                format(newFxAcc, null);
                        //rewrited logic for old trigger
                fxAccountUtil.setDefaultFunnelStage(newFxAcc);
                fxAccountUtil.clearNetWorth(newFxAcc);
                fxAccountUtil.updateIncomeSource(newFxAcc);
                //Added as part of the PushTopic replacement, may be removed once fully implemented
                fxAccountUtil.setEmailDomainName(newFxAcc);
                if (amINotSystemUser) {
                    fxAccountUtil.setFxdbName(newFxAcc, null); 
                }
                fxAccountUtil.updateFirstCKAResult(newFxAcc, null);
                //validation if any
            }

        
            fxAccountUtil.linkNewFxAccountsToLead(trgNew); //keep here, very complex logic - use if SOQL need deeper check
            
            // Update lead assignment based on newly inserted fxAccount and Open Activities
            fxAccountUtil.markHouseAccounts(trgNew); //keep here, very complex logic - use if SOQL need deeper check

            // Set hashed fields
            fxAccountHashUtil.setHashedFields(trgNew); //keep here, very complex logic - use if SOQL need deeper check
            fxAccountOwnerUtil.updateFxAccountOwner(trgNew); //keep here, very complex logic - use if SOQL need deeper check

            //Calculate CRA
            if (CustomSettings.isEnableCRACalculation() && CustomSettings.isUserAllowedForCRACalc(userinfo.getName())) {
                fxAccountRiskAssessment.calculateCustomerRiskAssessment(true,  trgNew, trgNewMap,new Map <Id, fxAccount__c>());
            }


        IHSIntegrationManager mng = new IHSIntegrationManager();
        mng.updateConditionallyApprovedAfterPIU(trgNew, new Map<Id, FxAccount__c>());

        // SP-9128, we must perform all workflows
        fxAccountUtil.performConditionalUpdates(trigger.new, trigger.old);
    }
   
    public void onBeforeUpdate() {

        AMPUtils ampUtil = new AMPUtils();

            //iterate records once..have all processing under this.
            for(fxAccount__c fxAccount : trgNew) {
                fxAccount__c oldFxAccount= trgOldMap.get(fxAccount.Id);
                //format record before update. all formatting for the record should be in this method.
                format(fxAccount, oldFxAccount);            
                //validation if any
                validate(fxAccount, oldFxAccount);
                fxAccountUtil.clearNetWorth(fxAccount);
                fxAccountUtil.updateIncomeSource(fxAccount);
                //Added as part of the PushTopic replacement, may be removed once fully implemented
                fxAccountUtil.setEmailDomainName(fxAccount);
                if (amINotSystemUser) {
                    fxAccountUtil.setFxdbName(fxAccount, oldFxAccount); 
                }
                fxAccountUtil.captureHVCTriggerDate(fxAccount);
                // Process to create AMP cases
                ampUtil.updateAMP_CaseTags(fxAccount, oldFxAccount);
                fxAccountUtil.updateFirstCKAResult(fxAccount, oldFxAccount);
            }

            // Process to create AMP cases
            
            //SP-13570 logic from fxAccount_Before.triggers 
            //START
                fxAccountOwnerUtil.updateFxAccountOwner(trgNew); //keep here, very complex logic - use if SOQL need deeper check
    
                //Calculate CRA
                if (CustomSettings.isEnableCRACalculation() && CustomSettings.isUserAllowedForCRACalc(userinfo.getName())){
                    fxAccountRiskAssessment.calculateCustomerRiskAssessment(false, trgNew, new Map <Id, fxAccount__c>(trgNew),trgOldMap);
                }

            fxAccountUtil.checkForReactivatedClientCampaign(trgNew, trgOldMap);////keep here, very complex logic - use if SOQL need deeper check

            // Update hashed fields
            fxAccountHashUtil.setHashedFields(
                trgNew, trgOldMap.values());
    

            IHSIntegrationManager mng = new IHSIntegrationManager();
            mng.updateConditionallyApprovedAfterPIU(trgNew, trgOldMap);

        // SP-9128, we must perform all workflows
        fxAccountUtil.performConditionalUpdates(trigger.new, trigger.old);
    }

    public void onBeforeDelete() {
        fxAccount_Trigger_Utility.beforeDelete(trgOldMap);
    }

    public void getRelatedDetails() {
       cellxpertPartnerIds = new Set<Decimal>();
   
       for(fxAccount__c fxAccount : trgNew) {
           fxAccount__c oldFxAccount= isUpdate ? trgOldMap.get(fxAccount.Id) : null;

           //affiliate related changes
           checkAffiliateRelatedDetails(fxAccount, oldFxAccount);

           //other logic goes here..
       }

       //get Affiliate details
       getAffiliateRelatedDetails();
    }

    //All data formatting before inserting/updating for fxaccount should go here.
    public void format(fxAccount__c fxa, fxAccount__c oldFxa) {
        //format Affiliate related
        formatAffiliate(fxa, oldFxa);

        //all data formatting code should go here
        fxAccountUtil.formatFxAccountData(fxa, oldFxa);
         
          //other formatting goes here..
      }
      
      //All validation before inserting/updating for fXaccount should go here.
      public void validate(fxAccount__c fxa, fxAccount__c oldFxa)
      {
        // Liquid Net Worth Value cannot be greater than Net Worth Value
        validateNetWorthUpdate(fxa, oldFxa);

        // other validations go here..
      }

     public void checkAffiliateRelatedDetails(fxAccount__c newFxAccount, fxAccount__c oldFxAccount)
     {
        if(isInsert && newFxAccount.Cellxpert_Partner_ID__c != null)
        {
            cellxpertPartnerIds.add(newFxAccount.Cellxpert_Partner_ID__c);
        }
        if(isUpdate && newFxAccount.Cellxpert_Partner_ID__c != oldFxAccount.Cellxpert_Partner_ID__c && newFxAccount.Cellxpert_Partner_ID__c != null)
        {
            cellxpertPartnerIds.add(newFxAccount.Cellxpert_Partner_ID__c);
        }
     }

     public void getAffiliateRelatedDetails()
     {
        cellxpertPartnerIdAffiliateMap = new Map<Decimal, Affiliate__c>{};

        if(cellxpertPartnerIds.size() > 0)
        {
            for(Affiliate__c affilate : [select Id, Cellxpert_Partner_ID__c From Affiliate__c Where Cellxpert_Partner_ID__c IN :cellxpertPartnerIds])
            {
                if(affilate.Cellxpert_Partner_ID__c != null)
                {
                    cellxpertPartnerIdAffiliateMap.put(affilate.Cellxpert_Partner_ID__c, affilate);
                }
            }
        }
    }

    public void formatAffiliate(fxAccount__c newFxAccount, fxAccount__c oldFxAccount)
    {      
        if(isInsert && newFxAccount.Cellxpert_Partner_ID__c != null)
        {
            newFxAccount.Affiliate__c = cellxpertPartnerIdAffiliateMap.containsKey(newFxAccount.Cellxpert_Partner_ID__c) ?
                                            cellxpertPartnerIdAffiliateMap.get(newFxAccount.Cellxpert_Partner_ID__c).Id : null;
        }
        else if(isUpdate && newFxAccount.Cellxpert_Partner_ID__c != oldFxAccount.Cellxpert_Partner_ID__c)
        {
            if(newFxAccount.Cellxpert_Partner_ID__c == null)
            {
               newFxAccount.Affiliate__c = null;
            }
            else if(newFxAccount.Cellxpert_Partner_ID__c != null)
            {
               newFxAccount.Affiliate__c = cellxpertPartnerIdAffiliateMap.containsKey(newFxAccount.Cellxpert_Partner_ID__c) ?
                                                cellxpertPartnerIdAffiliateMap.get(newFxAccount.Cellxpert_Partner_ID__c).Id : null;
            }
        }
    }

    public void validateNetWorthUpdate(fxAccount__c newFxAccount, fxAccount__c oldFxAccount)
    {
        if((SObjectUtil.isRecordFieldsChanged(newFxAccount, oldFxAccount, 'Net_Worth_Value__c') || 
            SObjectUtil.isRecordFieldsChanged(newFxAccount, oldFxAccount, 'Liquid_Net_Worth_Value__c')) &&
            newFxAccount.Net_Worth_Value__c < newFxAccount.Liquid_Net_Worth_Value__c &&
            //automated process user have no profile
            UserInfo.getProfileId() != UserUtil.getRegistrationProfileId() 
            && !UserInfo.getUserName().contains('autoproc'))
        {
            newFxAccount.addError('Error: Liquid Net Worth Value cannot be greater than Net Worth Value');
        }
    }
}