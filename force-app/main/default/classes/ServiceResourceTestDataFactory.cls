/**
 * @File Name          : ServiceResourceTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/27/2021, 4:40:37 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 4:10:39 PM   acantero     Initial Version
**/
@isTest
public without sharing class ServiceResourceTestDataFactory {

    public static final String SERVICE_RES_AGENT = 'A';
    public static final String CEO_ROLE_DEV_NAME = 'CEO';

    public static Service_Skill__mdt getValidSkillInfoFromSettings() {
        Service_Skill__mdt result = [
            select Skill_Dev_Name__c, Skill_Code__c 
            from Service_Skill__mdt
            limit 1
        ];
        return result;
    }

    public static String getValidSkillDevNameFromSettings() {
        String result = getValidSkillInfoFromSettings().Skill_Dev_Name__c;
        return result;
    }

    public static Skill getValidSkill() {
        String skillDevName = getValidSkillDevNameFromSettings();
        Skill result = getSkill(skillDevName);
        return result;
    }

    public static Skill getSkill(String skillDevName) {
        System.debug(skillDevName);
        Skill result = [
            select Id, DeveloperName, MasterLabel
            from Skill 
            where DeveloperName = :skillDevName 
            limit 1
        ];
        return result;
    }

    public static String getValidSkillId() {
        Skill skillObj = getValidSkill();
        return skillObj.Id;
    }

    public static User createCEOUser(String userKey) {
        String roleId = [
            select Id 
            from UserRole 
            where DeveloperName =:CEO_ROLE_DEV_NAME
        ].Id;
        return SPTestUtil.createUserWithRole(userKey, roleId);
    }

    public static String createAgentServiceResource(String userId) {
        ServiceResource serviceResourceObj = 
            getNewAgentServiceResource(userId);
        insert serviceResourceObj;     
        return serviceResourceObj.Id;
    }

    public static String createAgentServiceResIfNotExists(String userId) {
        ID servResId = [
            select Id 
            from ServiceResource 
            where RelatedRecordId = :userId
            and ResourceType = :SERVICE_RES_AGENT
            limit 1
        ]?.Id;
        if (servResId == null) {
            servResId = createAgentServiceResource(userId);
        }
        return servResId;
    }

    public static ServiceResource getNewAgentServiceResource(String userId) {
        ServiceResource serviceResourceObj = new ServiceResource();
        serviceResourceObj.Name = 'Agent ' + userId;
        serviceResourceObj.RelatedRecordId = userId;
        serviceResourceObj.ResourceType = SERVICE_RES_AGENT;
        serviceResourceObj.IsActive = true;
        return serviceResourceObj;
    }
    
}