/**
 * @File Name          : ChatSLAViolationNotificationBuilder.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/28/2024, 3:59:00 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/20/2024, 1:45:06 PM   aniubo     Initial Version
**/
public inherited sharing class ChatSLAViolationNotificationBuilder extends SLAViolationNotificationBaseBuilder {
	public ChatSLAViolationNotificationBuilder(
		SLAConst.SLAViolationType violationType
	) {
		super(violationType);
	}
	public override void buildData() {
		fieldsByPlaceholder = new Map<String, String>{
			Label.MilestoneTimePlaceholder => 'triggerTime',
			Label.ChatNumberPlaceholder => 'chatNumber',
			Label.InquiryNaturePlaceholder => 'inquiryNature',
			Label.DivisionPlaceholder => 'division',
			Label.TierPlaceholder => 'tier',
			Label.AgentNamePlaceholder => 'ownerName',
			Label.AccountNamePlaceholder => 'accountName'
			
		};
		placeholdersByViolationType = new Map<SLAConst.SLAViolationType, List<String>>{
			SLAConst.SLAViolationType.ANSWER => new List<String>{
				Label.ChatNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.AgentNamePlaceholder,
				Label.AccountNamePlaceholder
			},
			SLAConst.SLAViolationType.BOUNCED => new List<String>{
				Label.ChatNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.AgentNamePlaceholder,
				Label.AccountNamePlaceholder
			},
			SLAConst.SLAViolationType.ASA => new List<String>{
				Label.MilestoneTimePlaceholder,
				Label.ChatNumberPlaceholder,
				Label.InquiryNaturePlaceholder,
				Label.DivisionPlaceholder,
				Label.TierPlaceholder,
				Label.AccountNamePlaceholder
			}
		};
	}
}