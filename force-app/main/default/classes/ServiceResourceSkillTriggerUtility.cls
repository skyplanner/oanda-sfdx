/**
 * @File Name          : ServiceResourceSkillTriggerUtility.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/25/2021, 2:22:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/25/2021, 9:44:49 AM   acantero     Initial Version
**/
public without sharing class ServiceResourceSkillTriggerUtility {

    public static final String OBJ_API_NAME = 'ServiceResourceSkill';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //******************************************************* */

    final List<ServiceResourceSkill> newList;
    final Map<ID,ServiceResourceSkill> oldMap;

    public ServiceResourceSkillTriggerUtility(
        List<ServiceResourceSkill> newList,
        Map<ID,ServiceResourceSkill> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    public void handleBeforeInsert() {
        checkSkills();
    }

    public void handleBeforeUpdate() {
        checkSkills();
    }

    void checkSkills() {
        List<ServiceResourceSkill> skillChangedList = 
            new List<ServiceResourceSkill>();
        Set<String> serviceResourceIdSet = new Set<String>();
        for(ServiceResourceSkill serviceResSkill : newList) {
            ServiceResourceSkill oldRecord = oldMap?.get(serviceResSkill.Id);
            String oldSkillId = oldRecord?.SkillId;
            if (
                (serviceResSkill.SkillId != oldSkillId) &&
                String.isNotBlank(serviceResSkill.SkillId)
            ) {
                skillChangedList.add(serviceResSkill);
                serviceResourceIdSet.add(serviceResSkill.ServiceResourceId);
            }
        }
        if (!skillChangedList.isEmpty()) {
            ValidateServiceResSkillsCommand command = 
                new ValidateServiceResSkillsCommand(
                    skillChangedList,
                    serviceResourceIdSet
                );
            command.execute();
        }
    }

    public class ValidateServiceResSkillsCommand  {

        final List<ServiceResourceSkill> agentsServResSkillList;
        final Set<String> skillIdSet;

        public ValidateServiceResSkillsCommand(
            List<ServiceResourceSkill> servResSkillList,
            Set<String> serviceResourceIdSet
        ) {
            this.agentsServResSkillList = new List<ServiceResourceSkill>();
            this.skillIdSet = new Set<String>();
            Set<String> agentsIdSet = 
                ServiceResourceHelper.getAgentsIdSet(serviceResourceIdSet);
            for(ServiceResourceSkill servResSkill : servResSkillList) {
                if (agentsIdSet.contains(servResSkill.ServiceResourceId)) {
                    agentsServResSkillList.add(servResSkill);
                    skillIdSet.add(servResSkill.SkillId);
                }
            }
        }

        public void execute() {
            if (agentsServResSkillList.isEmpty()) {
                return;
            }
            //else...
            Map<String,String> skillCodeMapFromIds = 
                ServiceSkillManager.getSkillCodeMapFromIds(
                    skillIdSet
                );
            for(ServiceResourceSkill serviceResSkill : agentsServResSkillList) {
                String skillCode = 
                    skillCodeMapFromIds.get(serviceResSkill.SkillId);
                if (String.isBlank(skillCode)) {
                    serviceResSkill.addError(Label.InvalidAgentSkill);
                }
            }
        }

    }

}