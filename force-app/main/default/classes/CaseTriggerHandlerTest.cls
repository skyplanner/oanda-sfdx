/**
 * Created by Agnieszka Kajda on 18/10/2023.
 */
@IsTest
private class CaseTriggerHandlerTest {

    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    @IsTest
    static void testLinkToLead() {
        Lead l = new Lead(
            LastName = 'test',
            Email = 'test@oanda.com',
            RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId()
        );
        insert l;

        Case c = new Case(Chat_Email__c = 'test@oanda.com');
        insert c;

        System.assertEquals(l.Id, [SELECT Lead__c FROM Case WHERE Id = :c.Id][0].Lead__c);
    }

    @IsTest
    static void testLinkToAccountContact() {
        Account a = new Account(
            LastName = 'test',
            PersonEmail = 'test@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a;

        Case c = new Case(Chat_Email__c = 'test@oanda.com');
        insert c;

        Contact con = [SELECT Id FROM Contact WHERE AccountId = :a.Id][0];
        c = [SELECT AccountId, ContactId FROM Case WHERE Id = :c.Id][0];
        System.assertEquals(a.Id, c.AccountId);
        System.assertEquals(con.Id, c.ContactId);
    }

    // Test linking case from email to Account
    @IsTest
    static void testLinkToAccountContact2() {
        Account a = new Account(
            LastName = 'test',
            PersonEmail = 'test@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a;

        Case c = new Case(SuppliedEmail = 'test@oanda.com');
        insert c;

        Contact con = [SELECT Id FROM Contact WHERE AccountId = :a.Id][0];
        c = [SELECT AccountId, ContactId FROM Case WHERE Id = :c.Id][0];
        System.assertEquals(a.Id, c.AccountId);
        System.assertEquals(con.Id, c.ContactId);
    }

    // Test linking case when there are duplicate accounts
    @IsTest
    static void testLinkToAccountContact3() {
        Account a1 = new Account(
            LastName = 'test1',
            PersonEmail = 'test1@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a1;

        Account a2 = new Account(
            LastName = 'test2',
            PersonEmail = 'test2@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a2;

        Case c = new Case(SuppliedEmail = 'test1@oanda.com');
        insert c;

        c = [SELECT AccountId, ContactId FROM Case WHERE Id = :c.Id][0];
        System.assertNotEquals(null, c.AccountId);
        System.assertNotEquals(null, c.ContactId);
    }

    public static List<Case> createTestCases(Integer numCases) {
        List<Case> casesList = new List<Case>();

        for (Integer i = 0; i < numCases; i++) {
            casesList.add(new Case(SuppliedEmail = 'test' + i + '@oanda.com', SuppliedName = 'test' + i));
        }
        insert casesList;
        return casesList;
    }

    public static List<Lead> createTestLeads(Integer numLeads) {
        List<Lead> leadsList = new List<Lead>();

        for (Integer i = 0; i < numLeads; i++) {
            leadsList.add(new Lead(Email = 'test' + i + '@oanda.com'));
        }
        insert leadsList;
        return leadsList;
    }

    @IsTest
    static void testcreateLeadIfNeeded() {
        List<Case> testCases;

        Map<String, List<Case>> testCasesByEmailMap = new Map<String, List<Case>>();

        testCases = createTestCases(2);
        testCasesByEmailMap.put('test0@oanda.com', testCases);
        testCasesByEmailMap.put('test1@oanda.com', testCases);

        //testLeads = createTestLeads(2);
        List<Lead> listLeads = [SELECT Id, Email FROM Lead WHERE Email IN :testCasesByEmailMap.keySet()];

        List<Contact> listPersonAccount = [
            SELECT Id, AccountId, Email
            FROM Contact
            WHERE
                Email IN :testCasesByEmailMap.keySet()
                AND (Account.RecordTypeId = :RecordTypeUtil.getPersonAccountRecordTypeId() OR Account.RecordTypeId = :RecordTypeUtil.getEntityPersonAccountRecordTypeId())
        ];

        Test.startTest();

        CaseLeadService.createLeadIfNeeded(testCases);

        System.assertEquals(2, listLeads.size());

        System.assertEquals(0, listPersonAccount.size());

        Test.stopTest();
    }

    @isTest
    static void testCreateLeadIfNeeded2() 
    {
        String email = 'test@oanda.com';

        System.runAs(SYSTEM_USER){
            Case supportCase = new Case(
            Subject = 'Test Support Case',
            SuppliedEmail = email,
            SuppliedName = 'test',
            RecordTypeId = RecordTypeUtil.getSupportCaseTypeId()
            );
            insert supportCase;

            Case otmsSupportCase = new Case(
                Subject = 'Test OTMS Support Case',
                SuppliedEmail = email,
                SuppliedName = 'test',
                RecordTypeId = RecordTypeUtil.OTMS_SUPPORT_CASE_RECORD_TYPE_ID
            );
            insert otmsSupportCase;   
        }

        List<Lead> createdLeads = [SELECT Id, Email, RecordTypeId FROM Lead];
        Assert.areEqual(2, createdLeads.size());
        Assert.areEqual(false, createdLeads[0].RecordTypeId == createdLeads[1].RecordTypeId);
    }

    private static Id createCase(Boolean createCode, Boolean useManualRisk) {
        Account a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        if (useManualRisk) {
            a.Manual_Adjusted_Customer_Risk_Assessment__c = 'High';
        }
        insert a;

        String fxAccountRecTypeId = Schema.SObjectType.fxAccount__c.getRecordTypeInfosByDeveloperName()
            .get('Retail_Live')
            .getRecordTypeId();
        fxAccount__c fx = new fxAccount__c(RecordTypeId = fxAccountRecTypeId, Type__c = 'Individual');
        fx.Account__c = a.Id;
        fx.Division_Name__c = Constants.DIVISIONS_OANDA_EUROPE;
        fx.Employment_Status__c = 'Retired';
        fx.Industry_of_Employment__c = 'Agricultural';
        fx.Citizenship_Nationality__c = 'France';
        fx.CRA_Val__c = 40;
        if (!useManualRisk) {
            fx.Customer_Risk_Assessment_Text__c = 'High';
        }

        insert fx;

        a.fxAccount__c = fx.Id;
        update a;

        if (createCode) {
            List<Verification_Code_Setting__c> listS = new List<Verification_Code_Setting__c>();
            listS.add(new Verification_Code_Setting__c(Name = '012365', Order__c = 1));
            Database.insert(listS);
        }

        String onboardingRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(Constants.CASE_ONBOARDING_RECTYP)
            .getRecordTypeId();
        Case c = new Case(Subject = 'Test', AccountId = a.Id, RecordTypeId = onboardingRecTypeId);
        insert c;

        return c.Id;
    }

    private static Id createFailCase(FailInfo info) {
        String risk = (info.failRisk) ? 'Low' : 'High';
        Account a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        if (info.useManualRisk) {
            a.Manual_Adjusted_Customer_Risk_Assessment__c = risk;
        }
        insert a;

        String fxAccountRecTypeDevName = (info.failAccountLive) ? 'Retail_Practice' : 'Retail_Live';
        String fxAccountRecTypeId = Schema.SObjectType.fxAccount__c
            .getRecordTypeInfosByDeveloperName()
            .get(fxAccountRecTypeDevName)
            .getRecordTypeId();
        String fxAccountType = (info.failAccountIndividual) ? 'Corporate' : 'Individual';
        fxAccount__c fx = new fxAccount__c(RecordTypeId = fxAccountRecTypeId);
        fx.Account__c = a.Id;
        fx.Type__c = fxAccountType;
        fx.Employment_Status__c = 'Retired';
        fx.Industry_of_Employment__c = 'Agricultural';
        fx.Citizenship_Nationality__c = 'France';
        fx.CRA_Val__c = 40;
        if (!info.useManualRisk) {
            fx.Customer_Risk_Assessment_Text__c = risk;
        }
        fx.Customer_Risk_Assessment_Text__c = (info.failRisk) ? 'Low' : 'High';
        insert fx;

        a.fxAccount__c = fx.Id;
        update a;

        List<Verification_Code_Setting__c> listS = new List<Verification_Code_Setting__c>();
        listS.add(new Verification_Code_Setting__c(Name = '012365', Order__c = 1));
        Database.insert(listS);

        String recordTypeDevName = (info.failRecordType) ? 'Support' : 'Registration';
        String caseRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(recordTypeDevName)
            .getRecordTypeId();

        Case c = new Case(Subject = 'Test', RecordTypeId = caseRecTypeId);
        if (!info.failAccount) {
            c.AccountId = a.Id;
        }

        insert c;
        return c.Id;
    }

    @IsTest
    static void testHandleBeforeInsertUpdateAfterDelete() {
        Id cId = createCase(true, false);

        List<Case> c = [
            SELECT Verification_code__c, Verification_Code_Order__c, Subject
            FROM Case
            WHERE Id = :cId
        ];

        System.assertEquals('012365', c[0].Verification_code__c);
        System.assertEquals(1, c[0].Verification_Code_Order__c);

        /**Code must be deleted from Verification_Code_Setting__c */
        Integer count = [SELECT COUNT() FROM Verification_Code_Setting__c WHERE Name = '012365'];
        System.assert(count == 0);

        /** Update case, VerificationCode must be the same */
        c[0].Subject = 'Test Change';
        update c;

        List<Case> c1 = [
            SELECT Verification_code__c, Verification_Code_Order__c
            FROM Case
            WHERE Id = :cId
        ];
        System.assert(c1[0].Verification_code__c == '012365');
        System.assert(c1[0].Verification_Code_Order__c == 1);

        /**Delete case*/
        delete c;

        List<Case> c2 = [
            SELECT Verification_code__c, Verification_Code_Order__c
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(c2.size() == 0);
    }

    @IsTest
    static void test2HandleBeforeInsert() {
        Id cId = createCase(true, true);

        List<Case> c = [
            SELECT Verification_code__c, Verification_Code_Order__c, Subject
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(c[0].Verification_code__c == '012365');
        System.assert(c[0].Verification_Code_Order__c == 1);

        /**Code must be deleted from Verification_Code_Setting__c */
        Integer count = [SELECT COUNT() FROM Verification_Code_Setting__c WHERE Name = '012365'];
        System.assert(count == 0);
    }

    @IsTest
    static void testHandleBeforeInserNoCodeAvailable() {
        try {
            createCase(false, false);
        } catch (Exception e) {
            Boolean expectedExceptionThrown = e.getMessage().contains('No Verification Code available') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
    }

    @IsTest
    static void testFailRecordType() {
        FailInfo info = new FailInfo();
        info.failRecordType = true;
        info.useManualRisk = true;
        Id cId = createFailCase(info);
        List<Case> c = [
            SELECT Verification_code__c
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailAccount() {
        FailInfo info = new FailInfo();
        info.failAccount = true;
        Id cId = createFailCase(info);
        List<Case> c = [
            SELECT Verification_code__c
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailRegion() {
        FailInfo info = new FailInfo();
        info.failRegion = true;
        Id cId = createFailCase(info);
        List<Case> c = [
            SELECT Verification_code__c
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailRisk1() {
        FailInfo info = new FailInfo();
        info.failRisk = true;
        info.useManualRisk = true;
        Id cId = createFailCase(info);
        List<Case> c = [SELECT Verification_code__c FROM Case WHERE Id = :cId];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailRisk2() {
        FailInfo info = new FailInfo();
        info.failRisk = true;
        //useManualRisk = false
        Id cId = createFailCase(info);
        List<Case> c = [SELECT Verification_code__c FROM Case WHERE Id = :cId];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailAccountLive() {
        FailInfo info = new FailInfo();
        info.failAccountLive = true;
        Id cId = createFailCase(info);
        List<Case> c = [
            SELECT Verification_code__c
            FROM Case
            WHERE Id = :cId
        ];

        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void testFailAccountIndividual() {
        FailInfo info = new FailInfo();
        info.failAccountIndividual = true;
        Id cId = createFailCase(info);
        List<Case> c = [
            SELECT Verification_code__c
            FROM Case
            WHERE Id = :cId
        ];
        System.assert(String.isBlank(c[0].Verification_code__c));
    }

    @IsTest
    static void fillFirstOwner() {
        User tUser = new User(
            Alias = 'testuser',
            Email = 'testuser@testemail.com',
            LastName = 'Testing',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            Username = 'testusername01@testemail.com'
        );
        insert tUser;

        Account a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Case c;
        Test.startTest();
        System.runAs(tUser) {
            c = new Case(Subject = 'Test', AccountId = a.Id, OwnerId = tUser.Id);
            insert c;
        }
        Test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assertEquals(tUser.Id, c.First_Owner__c, 'The owner is wasn\'t filled properly');
    }

    @IsTest
    static void fillFirstOwner2() {
        User tUser = new User(
            Alias = 'testuser',
            Email = 'testuser@testemail.com',
            LastName = 'Testing',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            Username = 'testusername01@testemail.com'
        );
        insert tUser;

        Account a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Group q = new Group(Name = 'TestQueue', Type = 'Queue');
        insert q;

        System.runAs(new User(Id = UserInfo.getUserId())) {
            QueueSobject testQueue = new QueueSobject(QueueId = q.Id, SobjectType = 'Case');
            insert testQueue;
        }

        Case c;
        Test.startTest();
        System.runAs(tUser) {
            c = new Case(Subject = 'Test', AccountId = a.Id, OwnerId = q.Id);
            insert c;
        }
        Test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assert(c.First_Owner__c == null, 'The owner must be empty');

        c.OwnerId = tUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assertEquals(tUser.Id, c.First_Owner__c, 'The owner is wasn\'t filled properly');
    }

    @IsTest
    static void fillFirstOwner3() {
        User tUser = new User(
            Alias = 'testuser',
            Email = 'testuser@testemail.com',
            LastName = 'Testing',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            Username = 'testusername01@testemail.com'
        );
        insert tUser;

        String userName = 'system.user@oanda.com' + UserInfo.getUserName().substringAfterLast('.com');

        User systemUser = [SELECT Id, Username FROM User WHERE Username = :userName];

        Account a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        Group q = new Group(Name = 'TestQueue', Type = 'Queue');
        insert q;

        System.runAs(new User(Id = UserInfo.getUserId())) {
            QueueSobject testQueue = new QueueSobject(QueueId = q.Id, SobjectType = 'Case');
            insert testQueue;
        }

        Case c;
        Test.startTest();
        System.runAs(tUser) {
            c = new Case(Subject = 'Test', AccountId = a.Id, OwnerId = q.Id);
            insert c;
        }
        Test.stopTest();

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assert(c.First_Owner__c == null, 'The owner must be empty');

        c.OwnerId = systemUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assert(c.First_Owner__c == null, 'The owner must be empty');

        c.OwnerId = tUser.Id;
        update c;

        c = [SELECT Id, First_Owner__c FROM Case WHERE Id = :c.Id];
        System.assertEquals(tUser.Id, c.First_Owner__c, 'The owner is wasn\'t filled properly');
    }

    /**
     * public static void performConditionalUpdates(
     *		List<Case> newCases,
     *		List<Case> oldCases)
     */
    @IsTest
    static void performConditionalUpdates() {
        User tUser1, tUser2;
        Account a;
        Case c;

        tUser1 = new User(
            Alias = 'tuser1',
            Email = 'tuser1@testemail.com',
            LastName = 'Testing',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            Username = 'testusername01@testemail.com'
        );
        insert tUser1;

        tUser2 = new User(
            Alias = 'tuser2',
            Email = 'tuser2@testemail.com',
            LastName = 'Testing',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = UserInfo.getProfileId(),
            Username = 'testusername02@testemail.com'
        );
        insert tUser2;

        a = new Account(
            FirstName = 'First1',
            LastName = 'Last1',
            PersonEmail = 'test1@oanda.com',
            Account_Status__c = 'Active'
        );
        insert a;

        c = new Case(Subject = 'Test', AccountId = a.Id, OwnerId = tUser1.Id, Complaint__c = false);
        insert c;

        c = [
            SELECT Id, QI_Case_Owner__c
            FROM Case
            WHERE Id = :c.Id
        ];

        System.assertEquals(
            tUser1.Id,
            c.QI_Case_Owner__c,
            'The Non-complaint case\'s QI Case owner wasn\'t filled properly'
        );

        c.OwnerId = tUser2.Id;
        c.Complaint__c = true;
        c.QI_Case_Owner__c = null;
        update c;

        c = [
            SELECT Id, OwnerId, First_Owner__c, QI_Case_Owner__c
            FROM Case
            WHERE Id = :c.Id
        ];

        System.assertEquals(tUser2.Id, c.OwnerId, 'The Owner wasn\'t filled properly');

        System.assertEquals(
            c.First_Owner__c,
            c.QI_Case_Owner__c,
            'The Complaint case\'s QI Case owner wasn\'t filled properly'
        );
    }

    @IsTest
    static void getCasesToTransferTest() {
        Account a = new Account(
            LastName = 'test',
            PersonEmail = 'test@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a;
        List<User> users = new List<User>();
        User u = UserUtil.getTestUser();
        u.Delegated_Case_Owner__c = UserInfo.getUserId();
        users.add(u);
        users.add(UserUtil.getTestUser('seconduser@oanda.com'));
        insert users;
        System.debug('ak uid' + u.Id);
        System.debug('ak del' + u.Delegated_Case_Owner__c);
        System.debug('ak sec' + users[1].Id);
        Case c = new Case(Chat_Email__c = 'test@oanda.com', OwnerId = u.Id);
        insert c;
        Test.startTest();
        c.OwnerId = users[1].Id;
        update c;
        Test.stopTest();
        Case caseAfter = [SELECT Id, OwnerId FROM Case WHERE Id = :c.Id];
        System.assertEquals(UserInfo.getUserId(), caseAfter.OwnerId);
    }

    @IsTest
    static void transferCasesTest() {
        Account a = new Account(
            LastName = 'test',
            PersonEmail = 'test@oanda.com',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );
        insert a;
        User u = UserUtil.getTestUser();
        u.Delegated_Case_Owner__c = UserInfo.getUserId();
        insert u;
        Case c = new Case(Chat_Email__c = 'test@oanda.com', OwnerId = u.Id);
        insert c;
        Map<Id, Id> ownerUsersIdsByCase = new Map<Id, Id>();
        ownerUsersIdsByCase.put(c.Id, u.Id);
        CaseTriggerHandlerHelper.transferCases(new List<Case>{ c }, ownerUsersIdsByCase);
        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

    class FailInfo {
        Boolean failRecordType = false;
        Boolean failAccount = false;
        Boolean failRegion = false;
        Boolean failRisk = false;
        Boolean useManualRisk = false;
        Boolean failAccountLive = false;
        Boolean failAccountIndividual = false;
    }

    @TestSetup
    static void setup() {
        Contact testContact = new Contact(LastName = 'Test Contact');
        insert testContact;
        insert new Case(ContactId = testContact.Id, RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
    }

    /**
     * @description Success test for Info_Required__c field population during insert.
     * @author Jakub Fik | 2024-03-27
     **/
    @IsTest
    static void insertRollUpInfoRequiredToContactTest() {
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        Test.startTest();
        Case testCase = new Case(
            ContactId = testContact.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
            Info_Required__c = 'test'
        );
        insert testCase;
        Test.stopTest();

        Contact updatedContact = [
            SELECT Id, Info_Required__c
            FROM Contact
            WHERE Id = :testContact.Id
        ];
        System.assertEquals(
            testCase.Info_Required__c,
            updatedContact.Info_Required__c,
            'Info Required value supposes to populate.'
        );
    }

    /**
     * @description Success test for Info_Required__c field population during update.
     * @author Jakub Fik | 2024-03-27
     **/
    @IsTest
    static void updateRollUpInfoRequiredToContactTest() {
        String infoRequired = 'test';
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        Case testCase = new Case(ContactId = testContact.Id, RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        TriggerHandler.bypass('CaseTriggerHandler');
        insert testCase;
        TriggerHandler.clearBypass('CaseTriggerHandler');

        Test.startTest();
        testCase.Info_Required__c = infoRequired;
        update testCase;
        Test.stopTest();

        Contact updatedContact = [
            SELECT Id, Info_Required__c
            FROM Contact
            WHERE Id = :testContact.Id
        ];
        System.assertEquals(infoRequired, updatedContact.Info_Required__c, 'Info Required value supposes to populate.');
    }

    @IsTest
    static void populateTypeSummaryTest() {
        List<Case> cases = new List<Case>();
        cases.add(
            new Case(Chat_Email__c = 'test@oanda.com', Subject = 'Case subject', Inquiry_Nature__c = 'Registration')
        );
        cases.add(
            new Case(Chat_Email__c = 'test@oanda.com', Inquiry_Nature__c = 'Registration', Type__c = 'General Service')
        );
        insert cases;
        cases[1].Subject = 'Changed subject';
        update cases[1];
        cases = [SELECT Type_Summary__c FROM Case WHERE Chat_Email__c = 'test@oanda.com'];
        System.assertEquals('Registration, Case subject', cases[0].Type_Summary__c);
        System.assertEquals('Registration, General Service, Changed subject', cases[1].Type_Summary__c);
    }

    @IsTest
    static void changeRTToSupportTest() {
        Case testCase = new Case(
                RecordTypeId = RecordTypeUtil.getOTMSSupportCaseRecordTypeId(),
                Inquiry_Nature__c = 'Registration',
                Type__c = 'Individual',
                Subtype__c = 'Eligibility'
        );
        insert testCase;
        testCase.OwnerId = UserUtil.getQueueId(CaseTriggerHandlerHelper.CX_CASES_QUEUE);
        update testCase;

        testCase = [SELECT Inquiry_Nature__c, Type__c, Subtype__c, RecordTypeId FROM Case WHERE Id=:testCase.Id];
        System.assertEquals(null, testCase.Inquiry_Nature__c);
        System.assertEquals(null, testCase.Type__c);
        System.assertEquals(null, testCase.Subtype__c);
        System.assertEquals(RecordTypeUtil.getSupportCaseTypeId(), testCase.RecordTypeId);

    }

    /**
    * @description Test of assignment of Existing otms lead to case.
    * @author Jakub Fik | 2024-07-18 
    **/
    @IsTest
    static void assigningExistingLeadTest() {
        Lead testLead = new Lead(
            RecordTypeId = RecordTypeUtil.getOTMSLeadRecordTypeId(),
            Email = 'test@email.com',
            LastName = 'testLastName'
        );
        insert testLead;

        Case testCase = new Case (
            Subject = 'New document uploaded - test@email.com',
            RecordTypeId = RecordTypeUtil.getOTMSOnboardingCaseRecordTypeId()
        );

        Test.startTest();
        insert testCase;
        Test.stopTest();

        Case newCaseData = [SELECT ID, Lead__c FROM CASE WHERE ID = :testCase.Id];
        System.assertEquals(testLead.Id, newCaseData.Lead__c, 'Lead id should be automatically populated.');
    }

    /**
    * @description Test of assignment new otms lead.
    * @author Jakub Fik | 2024-07-18 
    **/
    @IsTest
    static void creatingNewOtmsLeadTest() {
        Lead testLead = new Lead(
            RecordTypeId = RecordTypeUtil.getAffiliateLeadRecordTypeId(),
            Email = 'test@email.com',
            LastName = 'testLastName'
        );
        insert testLead;

        Case testCase = new Case (
            Subject = 'New document uploaded - test@email.com',
            RecordTypeId = RecordTypeUtil.getOTMSOnboardingCaseRecordTypeId()
        );

        Test.startTest();
        insert testCase;
        Test.stopTest();

        Case newCaseData = [SELECT ID, Lead__c FROM CASE WHERE ID = :testCase.Id];
        Integer leadCounter = [SELECT COUNT() FROM Lead];
        System.assertEquals(2, leadCounter, 'New lead record should be created.');
        System.assertNotEquals(testLead.Id, newCaseData.Lead__c, 'Lead id should not be automatically populated.');
    }
}