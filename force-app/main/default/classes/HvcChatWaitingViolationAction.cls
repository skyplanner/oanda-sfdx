/**
 * @File Name          : HvcChatWaitingViolationAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 12:34:10 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/8/2021, 12:20:05 AM   acantero     Initial Version
**/
public without sharing class HvcChatWaitingViolationAction {

    @InvocableMethod(label='Report delay in accepting the HVC Chat')
    public static void hvcWaitingViolation(List<String> chatTranscriptIdList) {
        SLAViolationNotifiableFactory.SLAViolationNotifiableFactory(
            SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT,
            SLAConst.SLAViolationType.ASA,
            new ChatSLAViolationNotifierDataReader()
            )?.notifyViolation(
                new ChatSLAViolationNotificationData(chatTranscriptIdList)
            );
    }

}