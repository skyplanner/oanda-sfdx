/**
 * @File Name          : ServiceDivisionsManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 6:09:37 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 11:27:02 AM   acantero     Initial Version
**/
@isTest
private without sharing class ServiceDivisionsManager_Test {

    @isTest
    static void test() {
        Division_Settings__mdt divisionSettingsObj = [
            select DeveloperName, Division_Name__c, Skill_Dev_Name__c
            from Division_Settings__mdt
            where Division_Name__c <> null and Skill_Dev_Name__c <> null
            limit 1
        ];
        String code = divisionSettingsObj.DeveloperName;
        String name = divisionSettingsObj.Division_Name__c;
        String skill = divisionSettingsObj.Skill_Dev_Name__c;
        Test.startTest();
        ServiceDivisionsManager instance = 
            ServiceDivisionsManager.getInstance();
        String divisionName = instance.getDivisionName(code);
        String divisionCode = instance.getDivisionCode(name);
        String skillDevName1 = instance.getDivisionSkillDevName(code);
        String skillDevName2 = instance.getDivisionSkillDevName(name);
        ServiceDivisionsManager.ServiceDivisionInfo info1 = 
            instance.getDivisionByCode(null);
        ServiceDivisionsManager.ServiceDivisionInfo info2 = 
            instance.getDivisionByName(null);
        Test.stopTest();
        System.assertEquals(name, divisionName);
        System.assertEquals(code, divisionCode);
        System.assertEquals(skill, skillDevName1);
        System.assertEquals(skill, skillDevName2);
        System.assertEquals(null, info1);
        System.assertEquals(null, info2);
    }

    // The search is made by a false name so that it 
    // performs the search using the three names
    @isTest
    static void getByDivisionName() {
        Test.startTest();
        ServiceDivisionsManager instance = 
            ServiceDivisionsManager.getInstance();
        ServiceDivisionsManager.ServiceDivisionInfo result = 
            instance.getByDivisionName('super fake name');
        Test.stopTest();
        System.assertEquals(null, result, 'Invalid value');
    }

    
    /**
     * Test 1 : divisionStr = OAU
     * Test 2 : divisionStr = OANDA Australia
     * Test 3 : divisionStr = Super Fake Test Division
     */
    @IsTest
    static void getValidDivisionCode() {
        String fakeDivision = 'Super Fake Test Division';

        Test.startTest();
        ServiceDivisionsManager instance = 
            ServiceDivisionsManager.getInstance();

        // Test 1 -> return OAU
        String divisionStr1 = OmnichanelConst.OANDA_AUSTRALIA_AB;
        String result1 = instance.getValidDivisionCode(divisionStr1);

        // Test 2 -> return OAU
        String divisionStr2 = OmnichanelConst.OANDA_AUSTRALIA;
        String result2 = instance.getValidDivisionCode(divisionStr2);

        // Test 3 -> return 'Super Fake Test Division'
        String divisionStr3 = fakeDivision;
        String result3 = instance.getValidDivisionCode(divisionStr3);

        Test.stopTest();
        
        // Test 1
        Assert.areEqual(
            OmnichanelConst.OANDA_AUSTRALIA_AB,
            result1, 
            'Invalid result'
        );
        // Test 2
        Assert.areEqual(
            OmnichanelConst.OANDA_AUSTRALIA_AB,
            result2, 
            'Invalid result'
        );
        // Test 3
        Assert.areEqual(
            fakeDivision,
            result3, 
            'Invalid result'
        );
    }
    
}