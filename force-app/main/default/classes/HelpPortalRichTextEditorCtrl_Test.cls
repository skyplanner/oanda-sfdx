/**
 * @File Name          : HelpPortalRichTextEditorCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/24/2019, 4:21:17 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/24/2019   acantero     Initial Version
 * Modified By @Sahil Handa for SP-8310
**/
@isTest
private class HelpPortalRichTextEditorCtrl_Test {

    @testSetup
    static void setup () {
        ArticlesTestDataFactory.createTopArticles('d1', 'c1', 'c2', 'c3');
    }

    @isTest
    static void convertFileToDocument_test() {

        Id contentVersionId = createTestContentVersion('file1.txt', '123456789');
        Id fakeId = [select Id from Category_Top_Article__c limit 1][0].Id;
        Boolean invalidIdException = false;
        Test.startTest();
        String result1 = HelpPortalRichTextEditorCtrl.convertFileToDocument(contentVersionId);
        String result2 = null;
        try {
            result2 = HelpPortalRichTextEditorCtrl.convertFileToDocument(fakeId);
        } catch (Exception e) {
            invalidIdException = true;
        }
        Test.stopTest();
        System.assertNotEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(true, invalidIdException);
    }


    @isTest
    static void getHtml_test() {
        Boolean invalidValuesException = false;
        Test.startTest();
        String result1 = HelpPortalRichTextEditorCtrl.getHtml('es', 'd','OC');
        String result2 = null;
        try {
            result2 = HelpPortalRichTextEditorCtrl.getHtml(null, null,null);
        } catch (Exception e) {
            invalidValuesException = true;
        }
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(true, invalidValuesException);
    }

    @isTest
    static void saveHtml_test() {
        Boolean invalidValuesException = false;
        Test.startTest();
        HelpPortalRichTextEditorCtrl.saveHtml('es', 'd', 'abcd','OC');
        try {
            HelpPortalRichTextEditorCtrl.saveHtml(null, null, null,null);
        } catch (Exception e) {
            invalidValuesException = true;
        }
        Test.stopTest();
        System.assertEquals(true, invalidValuesException);
    }

    static String createTestContentVersion(String fileName, String data) {
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.VersionData = Blob.valueOf(data);
        cv.Title = fileName;
        cv.PathOnClient = filename;
        insert cv;
        return cv.Id;
    }

}