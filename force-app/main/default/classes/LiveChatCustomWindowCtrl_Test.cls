/**
 * @File Name          : LiveChatCustomWindowCtrl_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/31/2021, 2:27:31 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/15/2020   dmorales     Initial Version
**/
@isTest
private class LiveChatCustomWindowCtrl_Test {
    @isTest
    static void testConstructor() {
        LiveChatCustomWindowCtrl ctr = new LiveChatCustomWindowCtrl();
        //Long avgWaitTime = ctr.generateWaitTime();
        System.assert(ctr.chatLanguage == 'en_US');
        //System.assert(avgWaitTime != null);
    }

    @isTest
    static void testConstructorCookie(){
        PageReference prChatWindow = Page.LiveChatCustomWindow;
		Cookie cookie_chat_language = new Cookie('chat_language', 'de', null, -1, false);
		prChatWindow.setCookies(new Cookie[]{cookie_chat_language});
		Test.setCurrentPage(prChatWindow);
	    Test.startTest();
        LiveChatCustomWindowCtrl ctr = new LiveChatCustomWindowCtrl();
        Test.stopTest();	
        System.assert(ctr.chatLanguage == 'de');
    }
}