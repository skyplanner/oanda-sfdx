/**
 * @File Name          : SkillsPSRoutingFilterTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/5/2024, 3:28:58 PM
**/
@IsTest
private without sharing class SkillsPSRoutingFilterTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	// test 1 : psr = null
	// test 2 : PendingServiceRouting is not valid (WorkItemId = null)
	@IsTest
	static void isValidForRouting1() {
		PendingServiceRouting psr1 = null;
		PendingServiceRouting psr2 = new PendingServiceRouting();

		Test.startTest();
		SkillsPSRoutingFilter instance = new SkillsPSRoutingFilter();
		// test 1 => return false
		Boolean result1 = instance.isValidForRouting(psr1);
		// test 2 => return false
		// In the current version "prepareForFilter" does nothing yet
		// but we invoke it to give it coverage
		instance.prepareForFilter(new List<PendingServiceRouting> { psr2 });
		Boolean result2 = instance.isValidForRouting(psr2);
		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
	}

	// call method with a valid PSRWrapper record 
	// return true
	@IsTest
	static void isValidForRouting2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		SkillsPSRoutingFilter.PSRWrapper psrWrapperObj = 
			new SkillsPSRoutingFilter.PSRWrapper();
		psrWrapperObj.workItemId = case1Id;
		psrWrapperObj.routingType = OmnichanelConst.SKILLS_BASED_ROUTING_TYPE;

		Test.startTest();
		SkillsPSRoutingFilter instance = new SkillsPSRoutingFilter();
		Boolean result = instance.isValidForRouting(psrWrapperObj);
		Test.stopTest();
		
		Assert.isTrue(result, 'Invalid result');
	}

	// call method with a invalid PSRWrapper record 
	// isPreferredUserRequired = true
	// return false
	@IsTest
	static void isValidForRouting3() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		SkillsPSRoutingFilter.PSRWrapper psrWrapperObj = 
			new SkillsPSRoutingFilter.PSRWrapper();
		psrWrapperObj.workItemId = case1Id;
		psrWrapperObj.routingType = OmnichanelConst.SKILLS_BASED_ROUTING_TYPE;
		psrWrapperObj.isPreferredUserRequired = true;

		Test.startTest();
		SkillsPSRoutingFilter instance = new SkillsPSRoutingFilter();
		Boolean result = instance.isValidForRouting(psrWrapperObj);
		Test.stopTest();
		
		Assert.isFalse(
			result, 
			'When Preferred User Required is true, Skill Requirements can’t be added'
		);
	}

	/**
	 * call "check transfer" version
	 *  
	 * test 1 : isTransfer = true, workItemSObjType = MessagingSession,
	 * checkTransferForMessaging = true
	 * 
	 * test 2 : isTransfer = true, workItemSObjType = MessagingSession,
	 * checkTransferForMessaging = false
	 * 
	 * test 3 : isTransfer = true, workItemSObjType = Case,
	 * checkTransferForMessaging = false
	 * 
	 * test 4 : isTransfer = false, workItemSObjType = MessagingSession,
	 * checkTransferForMessaging = true
	 */
	@IsTest
	static void isValidForRouting4() {
		SkillsPSRoutingFilter.PSRWrapper psrWrapperObj = 
			new SkillsPSRoutingFilter.PSRWrapper();

		Test.startTest();
		SkillsPSRoutingFilter instance = new SkillsPSRoutingFilter();
		// test 1 => return false
		psrWrapperObj.isTransfer = true;
		Boolean result1 = instance.isValidForRouting(
			psrWrapperObj, // psrWrapperObj
			Schema.MessagingSession.SObjectType, // workItemSObjType
			true // checkTransferForMessaging
		);
		// test 2 => return true
		psrWrapperObj.isTransfer = true;
		Boolean result2 = instance.isValidForRouting(
			psrWrapperObj, // psrWrapperObj
			Schema.MessagingSession.SObjectType, // workItemSObjType
			false // checkTransferForMessaging
		);
		// test 3 => return false
		psrWrapperObj.isTransfer = true;
		Boolean result3 = instance.isValidForRouting(
			psrWrapperObj, // psrWrapperObj
			Schema.Case.SObjectType, // workItemSObjType
			false // checkTransferForMessaging
		);
		// test 4 => return true
		psrWrapperObj.isTransfer = false;
		Boolean result4 = instance.isValidForRouting(
			psrWrapperObj, // psrWrapperObj
			Schema.MessagingSession.SObjectType, // workItemSObjType
			true // checkTransferForMessaging
		);

		Test.stopTest();
		
		Assert.isFalse(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isFalse(result3, 'Invalid result');
		Assert.isTrue(result4, 'Invalid result');
	}
	
}