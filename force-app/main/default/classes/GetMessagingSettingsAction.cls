/**
 * @File Name          : GetMessagingSettingsAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 11:43:27 AM
**/
public without sharing class GetMessagingSettingsAction {

	@InvocableMethod(label='Get Messaging Settings' callout=false)
	public static List<MessagingSettingsInfo> getMessagingSettings() { 
		try {
			ExceptionTestUtil.execute();
			MessagingSettingsInfo settingsInfo = new MessagingSettingsInfo();
			List<MessagingSettingsInfo> result = 
				new List<MessagingSettingsInfo> { settingsInfo };
			settingsInfo.debugMode = SPSettingsManager.checkSetting(
				MessagingConst.MSG_DEBUG_MODE_SETTING
			);
			settingsInfo.defaultLanguage = SPSettingsManager.getSetting(
				MessagingConst.MSG_DEFAULT_LANGUAGE_SETTING
			);
			settingsInfo.defaultRegion = SPSettingsManager.getSetting(
				MessagingConst.MSG_DEFAULT_REGION_SETTING
			);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetMessagingSettingsAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class MessagingSettingsInfo {

		@InvocableVariable(label = 'defaultLanguage' required = false)
		public String defaultLanguage;

		@InvocableVariable(label = 'defaultRegion' required = false)
		public String defaultRegion;

		@InvocableVariable(label = 'debugMode' required = false)
		public Boolean debugMode;
		
	}

}