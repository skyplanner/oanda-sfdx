/**
 * @File Name          : InitMsgSessionProcessTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/14/2024, 3:29:50 AM
**/
@IsTest
private without sharing class InitMsgSessionProcessTest {

	// create with messagingSessionId param
	@IsTest
	static void execute1() {
		String messagingSessionId = null;

		Test.startTest();
		InitMsgSessionProcess instance = 
			new InitMsgSessionProcess(
				messagingSessionId, // messagingSessionId
				true // enhancedChannel
			);
		InitMsgSessionProcess.ProcessResult result = instance.execute();
		Test.stopTest();
		
		Assert.isNull(result.region, 'Invalid result');
		Assert.isFalse(result.requestRegion, 'Invalid result');
	}

	// create with session param (Initialized__c = false)
	// channelType = EmbeddedMessaging, enhancedChannel = true
	// Extra_Info__c <> null
	// get region from Extra_Info__c => result.region <> null
	@IsTest
	static void execute2() {
		MessagingSession session = new MessagingSession();
		session.Initialized__c = false;
		session.Extra_Info__c = 'us';

		Test.startTest();
		InitMsgSessionProcess instance = new InitMsgSessionProcess(
			session, // session
			true // enhancedChannel
		);
		instance.channelType = MessagingConst.EMBEDDED_MESSAGING_CHANNEL;
		InitMsgSessionProcess.ProcessResult result = instance.execute();
		Test.stopTest();
		
		Assert.isNotNull(result.region, 'Invalid result');
		Assert.isFalse(result.requestRegion, 'Invalid result');
	}

	// create with session param (Initialized__c = false)
	// channelType = EmbeddedMessaging, enhancedChannel = true
	// Extra_Info__c = null
	// Can't get region from Extra_Info__c => result.requestRegion = true
	@IsTest
	static void execute3() {
		MessagingSession session = new MessagingSession();
		session.Initialized__c = false;
		session.Extra_Info__c = null;

		Test.startTest();
		InitMsgSessionProcess instance = new InitMsgSessionProcess(
			session, // session
			true // enhancedChannel
		);
		instance.channelType = MessagingConst.EMBEDDED_MESSAGING_CHANNEL;
		InitMsgSessionProcess.ProcessResult result = instance.execute();
		Test.stopTest();
		
		Assert.isNull(result.region, 'Invalid result');
		Assert.isTrue(result.requestRegion, 'Invalid result');
	}

	// create with session param (Initialized__c = false)
	// channelType = WhatsApp, enhancedChannel = true
	// try to get region from IsoCountryCode
	// Since MessagingSession records cannot be created
	// the countryCode field is set to ensure that the test works
	// => get region from countryCode => result.region <> null
	@IsTest
	static void execute4() {
		MessagingSession session = new MessagingSession();
		session.Initialized__c = false;

		Test.startTest();
		InitMsgSessionProcess instance = new InitMsgSessionProcess(
			session, // session
			true // enhancedChannel
		);
		instance.channelType = MessagingConst.WHATSAPP_CHANNEL;

		instance.countryCode = 'US';
		InitMsgSessionProcess.ProcessResult result = instance.execute();
		Test.stopTest();
		
		Assert.isNotNull(result.region, 'Invalid result');
		Assert.isFalse(result.requestRegion, 'Invalid result');
	}

	// create with session param (Initialized__c = false)
	// channelType = Facebook, enhancedChannel = true
	// Can't get region from countryCode => result.requestRegion = true
	@IsTest
	static void execute5() {
		MessagingSession session = new MessagingSession();
		session.Initialized__c = false;

		Test.startTest();
		InitMsgSessionProcess instance = new InitMsgSessionProcess(
			session, // session
			true // enhancedChannel
		);
		instance.channelType = MessagingConst.FACEBOOK_CHANNEL;
		InitMsgSessionProcess.ProcessResult result = instance.execute();
		Test.stopTest();
		
		Assert.isNull(result.region, 'Invalid result');
		Assert.isTrue(result.requestRegion, 'Invalid result');
	}

	@IsTest
	static void prepareCountryCode() {
		MessagingSession session = new MessagingSession();

		Test.startTest();
		InitMsgSessionProcess instance = new InitMsgSessionProcess(
			session, // session
			true // enhancedChannel
		);
		instance.prepareCountryCode('US');
		Test.stopTest();
		
		Assert.areEqual('US', instance.countryCode, 'Invalid result');
	}
	
}