public without sharing class LookupController {
  @AuraEnabled(cacheable=true)
  public static List<sobject> findRecords(String searchKey, String objectName, List<String> extraFields, String filter) {
    List<String> fieldsToQuery = new List<String>{'Id', 'Name'};
    if (extraFields != null && extraFields.size() > 0) {
      fieldsToQuery.addAll(extraFields);
    }
    string searchText = '\'%' + String.escapeSingleQuotes(searchKey) + '%\'';
    if(filter != null) {
      searchText = searchText + ' AND ' + filter;
    }
    List<String> params = new List<String>{String.join(fieldsToQuery, ','), objectName, searchText};
    return Database.query(String.format('SELECT {0} FROM {1} WHERE Name LIKE {2}', params));
  }

  @AuraEnabled(cacheable=false)
  public static List<sobject> getRecentlyViewed(String objectName, List<String> extraFields) {
    List<String> fieldsToQuery = new List<String>{'Id', 'Name'};
    if (extraFields != null && extraFields.size() > 0) {
      fieldsToQuery.addAll(extraFields);
    }
    List<String> params = new List<String>{String.join(fieldsToQuery, ','), objectName};
    System.debug('params: ' + params);
    return Database.query(String.format('SELECT {0} FROM {1} WHERE LastViewedDate != NULL ORDER BY LastViewedDate DESC LIMIT 5', params));
  }

  @AuraEnabled(cacheable=true)
  public static Map<String, List<sobject>> searchRecords(String searchKey) {
    searchKey.replace('@', '*@*');
    searchKey = '*' + searchKey + '*';
    List<List<SObject>> searchList = [FIND :searchKey IN ALL FIELDS RETURNING Account(PersonEmail, name), Lead(email, name), User(name, email), Contact(name, email) LIMIT 50];
    Map<String, List<sobject>> retMap = new Map<String, List<sobject>>();
    for(Integer i = 0; i<searchList.size(); i++){
      if(searchList[i].size() > 0){
        retMap.put(searchList[i][0].Id.getSobjectType().getDescribe().getName(), searchList[i]);
      }
    }
    return retMap;
  } 
  
}