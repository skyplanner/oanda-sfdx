@isTest
public class DocumentTriggerHandlerTest {
   
    static testMethod void setFxAccountAsDocUploaded() {
        Account acc = new Account(
            LastName='test account');
        insert acc;

        Opportunity opp = new Opportunity(
            Name='test opp',
            StageName='test stage',
            CloseDate = Date.today(),
            AccountId = acc.Id);
        insert opp;

        Lead lead1 = new Lead(
            LastName='test lead',
            Email='j@example.org');
        insert lead1;

        fxAccount__c fxAcc = new fxAccount__c(
            Account__c = acc.Id,
            Opportunity__c = opp.Id,
            Lead__c = lead1.Id,
            RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE);
        insert fxAcc;

        // Create a case with recordType "Onboarding"
        Case case1 = new Case(
            Subject = 'Case 1',
            fxAccount__c = fxAcc.Id,
            RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId());
        insert case1;

        // fxAccount.FunnelStage should not be "Documents Upload"
        System.assertNotEquals(
            [SELECT Funnel_Stage__c
                FROM fxAccount__c
                LIMIT 1][0].Funnel_Stage__c,
            FunnelStatus.DOCUMENTS_UPLOADED);

        List<Profile> regProfiles =
            [SELECT Id
                FROM Profile 
                WHERE Name = :UserUtil.NAME_REGISTRATION_PROFILE
                LIMIT 1];

        User regUser = new User(
            Alias = 'testuser', 
            Email = 'testuser@testemail.com',
            LastName = 'Testing', 
            EmailEncodingKey = 'UTF-8', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = regProfiles[0].Id,
            UserName = 'testusername01@testemail.com');
        insert regUser;

        // Document must be created by a user with Profile "Registration"
        System.runAs(regUser) {
            // Refresh current user
            UserUtil.refreshCurrentUser();
            
            // Create a document with recordType 'Scan'
            insert new Document__c(
                name = 'Passport',
                RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(),
                Case__c = case1.Id,
                Document_Type__c = 'Passport',
                Expiry_Date__c = Date.today());
        }

        // fxAccount should be updted to funnelStage "Documents Uploaded"
        System.assertEquals(
            [SELECT Funnel_Stage__c
                FROM fxAccount__c
                LIMIT 1][0].Funnel_Stage__c,
            FunnelStatus.DOCUMENTS_UPLOADED);
    }

}