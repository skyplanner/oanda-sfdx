/**
 * @File Name          : BaseSRoutingContext.cls
 * @Description        : 
 * Base class that manages the instances of the classes that allow you to
 * customize omnichannel routing
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 1:29:32 AM
**/
public inherited sharing abstract class BaseSRoutingContext {

	protected final List<PendingServiceRouting> pendingRoutingList;
	protected List<BaseServiceRoutingInfo> routingInfoList;
	protected Map<String, SRoutingInfoManager> routingInfoManagerByType;

	protected SRoutingHelper routingHelper;
	protected SPendingServiceRoutingFilter pendingSRoutingFilter;
	protected SRoutingPriorityCalculator priorityCalculator;
	protected SSkillRequirementsManager skillReqManager;

	public BaseSRoutingContext(
		List<PendingServiceRouting> pendingRoutingList
	) {
		this.pendingRoutingList = (pendingRoutingList != null)
			? pendingRoutingList
			: new List<PendingServiceRouting>();
	}

	public Boolean isSameTransaction(
		List<PendingServiceRouting> newRoutingObjList
	) {
		if (
			(newRoutingObjList == null) ||
			(newRoutingObjList.size() != pendingRoutingList.size())
		) {
			return false;
		}
		// else...
		for (Integer i = 0; i < newRoutingObjList.size(); i++) {
			Boolean psrRecsAreEquals = areEquals(
				pendingRoutingList.get(i), // currentPSR
				newRoutingObjList.get(i) // newPSR
			);

			if (psrRecsAreEquals == false) {
				return false;
			}
		}
		return true;
	}

	@TestVisible
	Boolean areEquals(
		PendingServiceRouting currentPSR, 
		PendingServiceRouting newPSR
	) {
		Boolean result = (
			(
				(currentPSR.Id == null) ||
				(currentPSR.Id == newPSR.Id)
			) &&
			(currentPSR.WorkItemId == newPSR.WorkItemId)
		);
		return result;
	}

	public SPendingServiceRoutingFilter getPendingSRoutingFilter() {
		preparePendingSRoutingFilter();
		return pendingSRoutingFilter;
	}

	public SRoutingPriorityCalculator getPriorityCalculator() {
		if (priorityCalculator == null) {
			priorityCalculator = createPriorityCalculator();
		}
		return priorityCalculator;
	}

	public SSkillRequirementsManager getSkillReqManager() {
		if (skillReqManager == null) {
			skillReqManager = createSkillReqManager();
		}
		return skillReqManager;
	}

	public SRoutingHelper getRoutingHelper() {
		if (
			(routingHelper == null) &&
			(checkRoutingInfoList() == true)
		) {
			routingHelper = createRoutingHelper();
			routingHelper.setPriorityCalculator(getPriorityCalculator());
			routingHelper.setSkillReqManager(getSkillReqManager());
		}
		return routingHelper;
	}

	protected abstract SPendingServiceRoutingFilter createPendingSRoutingFilter();

	protected abstract SRoutingPriorityCalculator createPriorityCalculator();

	protected abstract SSkillRequirementsManager createSkillReqManager();

	protected abstract SRoutingHelper createRoutingHelper();

	protected abstract void initRoutingInfoManagersMap();

	@TestVisible
	void preparePendingSRoutingFilter() {
		if (pendingSRoutingFilter == null) {
			pendingSRoutingFilter = createPendingSRoutingFilter();
			pendingSRoutingFilter.prepareForFilter(pendingRoutingList);
		}
	}

	@TestVisible
	Boolean checkRoutingInfoList() {
		if (routingInfoList == null) {
			prepareRoutingInfoList();
		}
		Boolean result = (routingInfoList.isEmpty() == false);
		return result;
	}

	@TestVisible
	void prepareRoutingInfoList() {
		routingInfoList = new List<BaseServiceRoutingInfo>();
		preparePendingSRoutingFilter();
		prepareRoutingInfoManagersMap();
		
		for (PendingServiceRouting psr : pendingRoutingList) {
			
			if (pendingSRoutingFilter.isValidForRouting(psr) == true) {
				String objectTypeName = '' + psr.WorkItemId.getSobjectType();
				SRoutingInfoManager routingInfoManager = 
					routingInfoManagerByType.get(objectTypeName);
				if (routingInfoManager != null) {
					routingInfoManager.registerPendingServiceRouting(psr);
				}
			}
		}

		List<SRoutingInfoManager> routingInfoManagers = 
			routingInfoManagerByType.values();  
		for (SRoutingInfoManager routingInfoManager : routingInfoManagers) {
			List<BaseServiceRoutingInfo> tempRoutingInfoList = 
				routingInfoManager.getRoutingInfoList();

			if (tempRoutingInfoList != null) {
				routingInfoList.addAll(tempRoutingInfoList);
			}
		}
	}

	@TestVisible
	void prepareRoutingInfoManagersMap() {
		if (routingInfoManagerByType == null) {
			routingInfoManagerByType = new Map<String, SRoutingInfoManager>();
			initRoutingInfoManagersMap();
		}
	}

	@TestVisible
	protected Boolean registerRoutingInfoManager(
		Schema.SObjectType routingSObjectType,
		SRoutingInfoManager routingInfoManager
	) {
		if (
			(routingSObjectType == null) ||
			(routingInfoManager == null)
		) {
			return false;
		}
		// else...
		String typeName = '' + routingSObjectType;
		routingInfoManagerByType.put(typeName, routingInfoManager);
		return true;
	}

}