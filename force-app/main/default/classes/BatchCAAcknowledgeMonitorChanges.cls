/* Name: BatchCAAcknowledgeMonitorChanges
 * Description : SP-13339 Batch to automatically acknowledge monitor changes if the only changes are "Removed"
 * Author: Eugene Jung
 * Date : 07-28-2023
 * Last Modified Date : 11-10-2023
 */
public with sharing class BatchCAAcknowledgeMonitorChanges implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable, Database.AllowsCallouts
{
    public string query;
    public List<String> filterList;
    Boolean acknowledgeAllStatus = false;

    Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public static Integer BATCH_SIZE = 100;
    public static String CRON_SCHEDULE = '0 0 9 * * ?';
    public static final String MONITOR_CHANGE_STATUS_REMOVED = 'Removed';
    set<string> fields = ComplyAdvantageUtil.COMPLY_ADVANTAGE_SEARCH_FEILDS;
    Comply_Advantage_Search__c[] searchesToUpdate = new Comply_Advantage_Search__c[]{};
    Comply_Advantage_Search_Entity__c[] EntitiesToUpdate = new  Comply_Advantage_Search_Entity__c[]{};

    public BatchCAAcknowledgeMonitorChanges() 
    {
        query = 'SELECT ' + SoqlUtil.getSoqlFieldList(fields) + ',' +
        '   (SELECT Id, Monitor_Change_Status__c ' +
        '   FROM Comply_Advantage_Search_Entities__r) ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Is_Monitored__c = true AND Has_Monitor_Changes__c = true AND Monitor_Change_Date__c = LAST_N_DAYS:2';
    }

    public BatchCAAcknowledgeMonitorChanges(String filter, List<String> filterList, Boolean acknowledgeAllStatus){
        query = 'SELECT ' + SoqlUtil.getSoqlFieldList(fields) + ',' +
        '   (SELECT Id, Monitor_Change_Status__c ' +
        '   FROM Comply_Advantage_Search_Entities__r) ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Has_Monitor_Changes__c = true AND ' +
        (String.isNotBlank(filter) ? filter : '');

        this.filterList = filterList;
        this.acknowledgeAllStatus = acknowledgeAllStatus;
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		List<Id> ids = recIds;
        query = 'SELECT ' + SoqlUtil.getSoqlFieldList(fields) + ',' +
        '   (SELECT Id, Monitor_Change_Status__c ' +
        '   FROM Comply_Advantage_Search_Entities__r) ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public static void executeBatch() 
    {
        Database.executeBatch(new BatchCAAcknowledgeMonitorChanges(), BATCH_SIZE);
    }

    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchCAAcknowledgeMonitorChanges(), BATCH_SIZE);
    }
   
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope)
    {
        for(Comply_Advantage_Search__c search : scope) 
        {
            List<Comply_Advantage_Search_Entity__c> entities = search.Comply_Advantage_Search_Entities__r;

            // acknolwedge if the search has only removed entities
            if(!acknowledgeAllStatus) 
            {
                Boolean hasRemovedEntity = false;
                Boolean hasOtherChanges = false;
                // check each entity's monitor change status
                for (Comply_Advantage_Search_Entity__c entity : entities) {
                    if (entity.Monitor_Change_Status__c == MONITOR_CHANGE_STATUS_REMOVED) 
                    {
                        hasRemovedEntity = true;
                    }
                    else if(entity.Monitor_Change_Status__c != MONITOR_CHANGE_STATUS_REMOVED &&
                            entity.Monitor_Change_Status__c != null)
                    {
                        hasOtherChanges = true;
                    }
                }
                
                if(hasRemovedEntity && !hasOtherChanges) 
                {
                    ackMonitorChange(search);
                }
            }   
            else
            {
                ackMonitorChange(search);
            }
        }
        
        if(!searchesToUpdate.isEmpty())
        {
            Database.update(searchesToUpdate, false);
            updateAcknowledgedEntities(searchesToUpdate);
        }
    }

    // acknowledge and update search
    public void ackMonitorChange(Comply_Advantage_Search__c search)
    {
        string apiKey = ComplyAdvantageUtil.getApiKey(search);
        HttpResponse response = ComplyAdvantageUtil.acknowledgeMonitorChange(search.Search_Id__c, apiKey);

        if(response.getStatusCode() == 204)
        {
            search.Monitor_Changes_Acknowledged_By__c = UserInfo.getUserId();
            search.Monitor_Changes_Acknowledged_Date__c =  system.today();
            search.Has_Monitor_Changes__c = false;
            searchesToUpdate.add(search);           
        }
    }
    
    public void updateAcknowledgedEntities(List<Comply_Advantage_Search__c> searches)
    {
        for(Comply_Advantage_Search_Entity__c match : [SELECT Id, 
                                                              Monitor_Change_Status__c,
                                                              Is_Removed__c 
                                                      FROM Comply_Advantage_Search_Entity__c
                                                      WHERE Comply_Advantage_Search__c = :searches AND 
                                                            Monitor_Change_Status__c != null])
        {
            if(acknowledgeAllStatus)
            {
                updateEntity(match);
            }
            else{
                if(match.Monitor_Change_Status__c == 'Removed')
                {
                    updateEntity(match);
                }
            }
        }

        if(EntitiesToUpdate.size() > 0) 
        {
            Database.update(EntitiesToUpdate, false);
        }
    }

    public void updateEntity(Comply_Advantage_Search_Entity__c entity)
    {
        entity.Is_Removed__c = entity.Monitor_Change_Status__c == 'Removed';
        entity.Monitor_Change_Status__c = null;
        EntitiesToUpdate.add(entity);
    }
    
    public void finish(Database.BatchableContext bc) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

    public static void schedule(string schedule) 
    {
        if(string.isNotBlank(schedule))
        {
            CRON_SCHEDULE = schedule;
        }
        System.schedule('BatchCAAcknowledgeMonitorChanges', CRON_SCHEDULE, new BatchCAAcknowledgeMonitorChanges());
    }
}