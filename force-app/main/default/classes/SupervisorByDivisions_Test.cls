/**
 * @File Name          : SupervisorByDivisions_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:25:21 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/25/2021, 12:52:16 PM   acantero     Initial Version
**/
@isTest(isParallel = false)
private without sharing class SupervisorByDivisions_Test {

    @isTest
    static void test() {
        Date yesterday = System.today().addDays(-1);
        Division_Settings__mdt divisionSettingsObj = [
            select DeveloperName, Division_Name__c, Skill_Dev_Name__c
            from Division_Settings__mdt
            where Division_Name__c <> null and Skill_Dev_Name__c <> null
            limit 1
        ];
        String divisionCode = divisionSettingsObj.DeveloperName;
        String divisionName = divisionSettingsObj.Division_Name__c;
        String divisionSkill = divisionSettingsObj.Skill_Dev_Name__c;
        String divisionSkillId = 
            ServiceResourceTestDataFactory.getSkill(divisionSkill).Id;
        //...
        String userId;
        Boolean isEmpty;
        Set<String> supervisors;
        Test.startTest();
        OmnichanelSettings omniSettings = new OmnichanelSettings();
        omniSettings.supervisorRoleDevNameList = new List<String>
        {
            ServiceResourceTestDataFactory.CEO_ROLE_DEV_NAME
        };
        OmnichanelSettings.activeSettings = omniSettings;
        User testUser = ServiceResourceTestDataFactory.createCEOUser('c0002');
        System.runAs(testUser) {
            userId = UserInfo.getUserId();
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(userId);
            ServiceResourceSkill serviceResourceSkillObj = 
                new ServiceResourceSkill(
                    ServiceResourceId = serviceResourceId,
                    SkillId = divisionSkillId,
                    EffectiveStartDate = yesterday
                );
            insert serviceResourceSkillObj;
            //...
            SupervisorByDivisions instance = new SupervisorByDivisions();
            instance.addCriteria(divisionSkill);
            instance.getSupervisors();
            isEmpty = instance.isEmpty();
            supervisors = instance.getSupervisors(divisionSkill);
        }
        Test.stopTest();
        System.assertEquals(false, isEmpty);
        System.assert(supervisors.contains(userId));
    }
    
}