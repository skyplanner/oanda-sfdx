/**
 * @File Name          : ArticleListHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/9/2019, 4:14:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/9/2019, 4:10:56 PM   acantero     Initial Version
**/
@isTest
private class ArticleListHelper_Test {

	

	@testSetup
	static void setup () {
		ArticlesTestDataFactory.createArticlesWithDummyDocs();
	}


	@isTest
	static void count_test() {
		Id art1Id = [select Id from FAQ__kav where PublishStatus = 'Online' and Title = 'Test Article 1'].Id;
		ArticleListFilterCriteria criterias = new ArticleListFilterCriteria();
		criterias.status = 'Online';
		criterias.hasPreviewDoc = true;
		criterias.excludeArtIds = art1Id;
		criterias.restrictToVisibleInPkb = false;
		ArticleListHelper obj = new ArticleListHelper(criterias);
		Test.startTest();
		Integer result = obj.count();
		Test.stopTest();
		System.assertEquals(2, result);
	}


	@isTest
	static void search_test() {
		ArticleListFilterCriteria criterias = new ArticleListFilterCriteria();
		criterias.status = 'Online';
		criterias.hasPreviewDoc = false;
		criterias.restrictToVisibleInPkb = false;
		criterias.searchText = 'Test Article 1';
		Integer pageSize = 20;
		Integer offset = 0;
		String sortBy = 'CreatedDate';
		Boolean isDescending = true;
		ArticleListHelper obj = new ArticleListHelper(criterias, pageSize, offset, sortBy, isDescending);
		Test.startTest();
		List<FAQ__kav> result = obj.search();
		Test.stopTest();
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
	}

	@isTest
	static void getDataCategoriesCriteria_test() {
		ArticleListFilterCriteria criterias = new ArticleListFilterCriteria();
		criterias.status = 'Online';
		criterias.hasPreviewDoc = false;
		criterias.category = 'cat1';
		criterias.audience = 'aud1';
		criterias.division = 'div1';
		ArticleListHelper obj = new ArticleListHelper(criterias);
		Test.startTest();
		String result = obj.getDataCategoriesCriteria();
		Test.stopTest();
		String q = ' WITH DATA CATEGORY  Categories__c BELOW cat1__c  AND  Audience__c BELOW aud1__c  AND  Division__c BELOW div1__c ';
		System.assertEquals(q, result);
	}

}