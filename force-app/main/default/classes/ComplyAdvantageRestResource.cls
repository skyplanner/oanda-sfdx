/**
 * @description       :
 * @author            : Yaneivys Gutierrez
 * @group             :
 * @last modified on  : 11-14-2022
 * @last modified by  : Yaneivys Gutierrez
 * @Description
 * URL: /services/apexrest/api/v1/casearch
**/
@RestResource(urlMapping='/api/v1/casearch/*')
global without sharing class ComplyAdvantageRestResource {

    public static final List<String> POST_PARAMS = new List<String>{'user_id'};

    @HttpPost
    global static void doPost() {
        ComplyAdvantageRestResponse response;

        RestRequest req = RestContext.request;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, POST_PARAMS);
        try {

            Decimal fxAccountUserId = Decimal.valueOf(context.getPathParam('user_id'));
            System.debug('ComplyAdvantageRestResource fxAccount User Id: ' + fxAccountUserId);

            Map<String, Object> result = ComplyAdvantageHelper.complyAdvantageSearchPOST(fxAccountUserId);
            response = new ComplyAdvantageRestResponse(
                    (String) result.get('status'),
                    (Integer) result.get('statusCode'),
                    (String) result.get('message'),
                    (List<Map<String, Object>>) result.get('searchResponse'));
        }
        catch (Exception ex) {
            Logger.error('ca-search-trigger', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex.getMessage());
            response = new ComplyAdvantageRestResponse('Failed', 500, ex.getMessage(), null);
        } finally {
            context.setResponse(200, response);
        }
    }

    public class ComplyAdvantageRestResponse {
        public String status;
        public Integer responseCode;
		public String message;
        public List<Map<String, Object>> searchResponse;

		ComplyAdvantageRestResponse(
            String status,
            Integer responseCode,
            String message,
            List<Map<String, Object>> searchResponse)
        {
            this.status = status;
            this.responseCode = responseCode;
			this.message = message;
			this.searchResponse = searchResponse;
        }
    }
}