/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-20-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiSearchItem {
    final static transient Map<Integer, String> DIVISION_MAP 
        = new Map<Integer, String> {
            1 => 'OC',
            2 => 'OCAN',
            3 => 'OAP',
            4 => 'OEL',
            6 => 'OC-ECP',
            7 => 'OAP-CFD',
            11 => 'OAU',
            12 => 'OEM',
            13 => 'OGM'
        };

    public String TYPE {get; set;}
    
    public String address {get; set;}

    public String alt_id {get; set;}

    public Integer division_id {get; set;}

    public Integer api_enable {get; set;}

    public Integer as_user_id {get; set;}

    public Integer balance_interest_enabled {get; set;}

    public String bank_abano {get; set;}

    public String bank_accountno {get; set;}

    public String bank_address {get; set;}

    public String bank_name {get; set;}

    public String bank_swiftno {get; set;}

    public String beneficiary {get; set;}

    public String billed {get; set;}

    public String birthdate {get; set;}

    public String ccexp {get; set;}

    public String ccname {get; set;}

    public String ccnum {get; set;}

    public String cctype {get; set;}

    public String citizenship {get; set;}

    public Double credit {get; set;}

    public String email {get; set;}

    public String employer_address {get; set;}

    public String employer_bustype {get; set;}

    public String employer_name {get; set;}

    //date: "employer_start": "2022-06-03",
    public String employer_start {get; set;}

    public String employer_tele {get; set;}

    public String entity {get; set;}

    public String entity_type {get; set;}

    public Long entrydate {get; set;}

    public Integer failed_login {get; set;}

    public String fax {get; set;}

    public String homecurr {get; set;}

    public Integer language_id {get; set;}

    public Long last_login {get; set;}

    public Integer logins {get; set;}

    public String marital_status {get; set;}
    
    public Integer markup_group_id {get; set;}

    public Integer mfa_required {get; set;}

    public String name {get; set;}

    public String password_check {get; set;}

    public String profile {get; set;}

    public String ssn {get; set;}

    public String telephone {get; set;}

    public String telephone_work {get; set;}

    public String title {get; set;}

    public Integer user_id {get; set;}

    public String username {get; set;}
    
    public Integer withhold_enabled {get; set;}

    public transient Boolean isLive {
        get { 
            if (isLive == null)
                isLive = false;

            return isLive;
        } set;
    }

    public transient String env {
        get { 
            return isLive ? 'live' : 'demo';
        } set;
    }

    public transient Boolean balanceInterestEnabled {
        get {
            return balance_interest_enabled == 1;
        }
    }

    public transient Boolean apiEnabled {
        get {
            return api_enable == 1;
        }
    }
    
    public transient DateTime createdDate {
        get {
            return entrydate == null 
                ? null : DateTime.newInstance(entrydate * 1000);
        }
    }

    public transient String division {
        get {
            String divisionAbbr = DIVISION_MAP.get(division_id);

            return divisionAbbr == null ? '' : divisionAbbr;
        }
    }

    public Map<String, Object> toMap() {
        return new Map<String, Object> {
            'fxTrade_User_Id__c' => user_id,
            Constants.API_FIELD_ID => user_id,
            Constants.API_FIELD_USER_ID => user_id,
            Constants.API_FIELD_USERNAME => username,
            Constants.API_FIELD_EMAIL => email,
            Constants.API_FIELD_DIVISION => division,
            Constants.API_FIELD_BALANCE => credit,
            Constants.API_FIELD_NAME => name,
            Constants.API_FIELD_CITIZENSHIP => citizenship,
            Constants.API_FIELD_TELEPHONE => telephone,
            Constants.API_FIELD_API_ACCESS => apiEnabled,
            Constants.API_FIELD_CREATED_DATE => createdDate,
            Constants.API_FIELD_ENV => env
        };
    }
}