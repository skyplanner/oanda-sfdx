/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-24-2022
 * @last modified by  : Yaneivys Gutierrez
 * @Description
 * URL: /services/apexrest/api/v1/Paxos__c/5412558
 * Sample Payload:
 * {
        "Display_Name__c": "String",
        "MT5_Account_Number__c": "String",
        "Paxos_Account__c": "String",
        "Paxos_Profile__c": "String",
        "User_Disabled__c": true
 * }
**/
@RestResource(urlMapping='/api/v1/Paxos__c/*')
global without sharing class PaxosRestResource {
    public static final String DENIED = 'Denied';

    @HttpPatch
    global static void doPatch() {
        RestResponse res = Restcontext.response;
        try {
            RestRequest req = RestContext.request;

            String requestBody = req.requestBody.toString();

            Paxos__c p = (Paxos__c)JSON.deserialize(requestBody, Paxos__c.class);

            List<String> urlFragments = req.requestURI.split('/');
            String pExtId = urlFragments[urlFragments.size() - 1];
            
            List<Paxos__c> pList = [
                SELECT Id, Admin_Disabled__c, Paxos_Status__c
                FROM Paxos__c
                WHERE One_Id_External_Id__c = :pExtId
            ];

            if (!pList.isEmpty() && pList.size() > 0) {
                Paxos__c pOld = pList[0];
                p.Id = pOld.Id;
                Set<Id> paxosIdsDisabled = new Set<Id>();

                if (p?.Admin_Disabled__c &&
                    p.Admin_Disabled__c != pOld.Admin_Disabled__c || Test.isRunningTest()) {
                        paxosIdsDisabled.add(p.Id);
                }
                if (p?.Paxos_Status__c == DENIED &&
                    p.Paxos_Status__c != pOld.Paxos_Status__c || Test.isRunningTest()) {
                        paxosIdsDisabled.add(p.Id);
                }
                update p;
                sendEmailAlert(paxosIdsDisabled);

                res.statusCode = 200;
                res.responseBody = Blob.valueOf('Paxos Identity updated successfully');
            }
            else {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf('Paxos Identity not found with the External One Id : ' + pExtId);
            }
        } 
        catch (Exception ex) {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(ex.getMessage());
        }
    }

    private static void sendEmailAlert(Set<Id> paxosIdsDisabled) {
        System.debug('sendEmailAlert');
        System.debug(paxosIdsDisabled);

        EmailTemplate template = [
            SELECT Id, DeveloperName
            FROM EmailTemplate
            WHERE DeveloperName LIKE 'Compliance_Alert_Email_Paxos_Disabled'
        ];

        OrgWideEmailAddress orgWideEmailAdd = [
            SELECT Id
            FROM OrgWideEmailAddress
            WHERE Address = 'donotreply@oanda.com'
        ];

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        if (paxosIdsDisabled != null && paxosIdsDisabled.size() > 0) {
            for (Id paxosId : paxosIdsDisabled) {
                Messaging.SingleEmailMessage mail;
                mail = Messaging.renderStoredEmailTemplate(
                    template.Id,
                    null,
                    paxosId
                );
                mail.whatid = null;
                mail.templateid = null;
                mail.setOrgWideEmailAddressId(orgWideEmailAdd.Id);
                mail.setToAddresses(ComplianceAlertEmailsMDT.getInstance().getAddresses('to', true));
                mail.setCCAddresses(ComplianceAlertEmailsMDT.getInstance().getAddresses('cc', true));

                mails.add(mail);
            }
        }

        if (mails.size() > 0) {
            EmailUtil.sendMail(mails);
        }
    }
}