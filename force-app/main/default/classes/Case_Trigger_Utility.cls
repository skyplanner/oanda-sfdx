/**
 * @File Name          : Case_Trigger_Utility.cls
 * @Description        : This class is created for manage SKYPLANNER implementations
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/14/2020, 8:57:16 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/4/2019   dmorales     Initial Version
 * 1.1    06/04/2021  mcarillo     Added the First Owner functionality
**/
public class Case_Trigger_Utility {

    public static final String CASE_ONBOARDING_RT = 'Registration';

    public static final String FX_ACCOUNT_TYPE_INDIVIDUAL = 'Individual';

    public static final String OANDA_EUROPE = 'OANDA Europe';
    public static final String OANDA_EUROPE_AB = 'OEL';
    public static final String OANDA_MARKETS_EUROPA = 'OANDA Europe Markets';
    public static final String OANDA_MARKETS_EUROPA_AB = 'OEM';

    public static final String HIGH_RISK = 'High';
    public static final String MEDIUM_RISK = 'Medium';

	/**
	 * @param newCases
	 * @param oldCases
	 */
    public static void handleBeforeInsertUpdate(
			List<Case> newCases,
			List<Case> oldCases) {
         verificationCodeAssignment(newCases);
         fillFirstOwnerUser(newCases);

		performConditionalUpdates(newCases, oldCases);
    }

	/**
	 * @param newCases
	 * @param oldCases
	 */
    public static void handleAfterDelete(
			List<Case> newCases,
			List<Case> oldCases) {
        verificationCodeRelease(newCases);
    }
    
    public static void verificationCodeRelease(List<Case> cases){
         /**
         * Find cases with Verification Code
         * Call method for re-insert Verificacion Code into Big Object
         **/
         List<Verification_Code_Setting__c> verifCode = new List<Verification_Code_Setting__c>();    
         
         for(Case c: cases){
            if(! String.isBlank(c.Verification_Code__c))
              verifCode.add( new Verification_Code_Setting__c(Name = c.Verification_Code__c, 
              Order__c = c.Verification_Code_Order__c));
         }
         if(!Test.isRunningTest())
             System.enqueueJob(new VerificationCodeBigObjectJob(verifCode));       
    }

    public static void verificationCodeAssignment(List<Case> cases){

         List<Case> casesToVeficationCode = getUnclaimedOnboardingHighRiskCases(cases);
        
         if(casesToVeficationCode.size() > 0){
                
             List<Verification_Code_Setting__c> codesAvailables = 
                VerificationCodeUtility.getVerificationCodesAvailable(casesToVeficationCode.size());
            if(codesAvailables.size() > 0){
                Integer i = 0;
                for(Case c : casesToVeficationCode ){                 
                  c.Verification_Code__c = codesAvailables[i].Name;
                  c.Verification_Code_Order__c = codesAvailables[i].Order__c;                 
                  i++;
                }
            }
            else
              casesToVeficationCode[0].addError('No Verification Code available');               
         }
    }

	/**
	 * @param newCases
	 * @param oldCases
	 */
	public static void performConditionalUpdates(
			List<Case> newCases,
			List<Case> oldCases) {
		Case newCase, oldCase;
		List<CaseConditionalUpdate> updates;
		List<String> workflows;

		// we will perform these workflows
		workflows = new List<String> {
			'WF_QI_Case_Owner'
		};

		// we'll save updates
		updates = new List<CaseConditionalUpdate>();

		// we work for every record
		for (Integer i = 0; i < newCases.size(); i++) {
			newCase = newCases.get(i);
			oldCase = oldCases != null ? oldCases.get(i) : null;
			updates.add(new CaseConditionalUpdate(newCase, oldCase));
		}

		// we then execute updates
		ConditionalUpdate.performConditionalUpdates(updates, workflows);
	}

    /**
     * This method fills out the First Owner field from the OwnerId
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   06/04/2021
     * @return: void
     * @param:  pCases, the list of cases to process
     */
    private static void fillFirstOwnerUser(List<Case> pCases){
        Set<Id> usersToExclude = getUsersToExclude();
        for(Case cs : pCases){
            if(String.isBlank(cs.First_Owner__c) && 
                    String.isNotBlank(cs.OwnerId) && 
                    cs.OwnerId.to15().startsWith('005') &&
                    !usersToExclude.contains(cs.OwnerId)){
                cs.First_Owner__c = cs.OwnerId;
            }
        }
    }

    /**
     * Gets the users to exclude in the First Owner from the custom metadata
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   26/04/2021
     * @return: Set<String>
     */
    private static Set<Id> getUsersToExclude(){
        Set<Id> result = new Set<Id>();
        String postFix = '';
        if([SELECT IsSandbox FROM Organization].IsSandbox){
            postFix = UserInfo.getUserName().substringAfterLast('.com');
        }
        List<String> userNames = new List<String>();
        for(First_Owner_Exclusion__mdt foe : [SELECT MasterLabel FROM First_Owner_Exclusion__mdt]){
            userNames.add(foe.MasterLabel + postFix);
        }
        for(User usr : [SELECT Id FROM User WHERE Username IN: userNames]){
            result.add(usr.Id);
        }
        return result;
    }

    private static List<Case> getUnclaimedOnboardingHighRiskCases(List<Case> cases) {
          List<Case> caseToUpdate = new List<Case>();
          List<Case> validCases = new List<Case>();
          String onboardingRecTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get(CASE_ONBOARDING_RT)
            .getRecordTypeId();
          for(Case c : cases) {
            if(
                (c.RecordTypeId == onboardingRecTypeId) && //the case is Onboarding
                String.isBlank(c.Verification_Code__c) && //the case does not have a verification code
                String.isNotBlank(c.AccountId)//the case has an account
            ) {
                validCases.add(c);
            }  
          }
          if (validCases.isEmpty()) {
              return caseToUpdate;
          }
          //else..
          Map<Id, Account> accounts = getCaseAccount(validCases);
          for(Case c : validCases) {
              //check case account conditions
              Account a = accounts.get(c.AccountId);
              System.debug('risk: ' + a.Customer_Risk_Assessment_OEL_OAU__c);
              System.debug('manual risk: ' + a.Manual_Adjusted_Customer_Risk_Assessment__c);
              if (
                (a.fxAccount__r != null) &&  //has an fxAccount 
                (a.fxAccount__r.Is_Practice_Account__c == false) && //fxAccount is live
                (a.fxAccount__r.Type__c == FX_ACCOUNT_TYPE_INDIVIDUAL) && //fxAccount is individual
                (//division is OME or OEL 
                    (a.Primary_Division_Name__c == OANDA_EUROPE) ||
                    (a.Primary_Division_Name__c == OANDA_MARKETS_EUROPA) ||
                    (a.Primary_Division_Name__c == OANDA_EUROPE_AB) ||
                    (a.Primary_Division_Name__c == OANDA_MARKETS_EUROPA_AB)
                ) &&
                (//risk is High or Medium
                    (a.Customer_Risk_Assessment_OEL_OAU__c == HIGH_RISK) ||
                    (a.Customer_Risk_Assessment_OEL_OAU__c == MEDIUM_RISK) ||
                    (a.Manual_Adjusted_Customer_Risk_Assessment__c == HIGH_RISK) ||
                    (a.Manual_Adjusted_Customer_Risk_Assessment__c == MEDIUM_RISK)
                )
              ) {
                caseToUpdate.add(c);
              }
          }
          return caseToUpdate;
    }

    public static Map<Id, Account> getCaseAccount(List<Case> cases){
        Set<Id> caseAccountId = new Set<Id>();
        for(Case c : cases ){
          	if (c.AccountId != null) {
			  caseAccountId.add(c.AccountId);
		    } 
        }  

        Map<Id, Account> accounts = new Map<Id, Account>([
            SELECT 
                Id, 
                Primary_Division_Name__c, 
                Customer_Risk_Assessment_OEL_OAU__c,
                Manual_Adjusted_Customer_Risk_Assessment__c,
                fxAccount__r.Is_Practice_Account__c, 
                fxAccount__r.Type__c 
            FROM 
                Account 
            WHERE 
                Id in :caseAccountId
        ]);
        System.debug('accounts ' + accounts);                                             
        return accounts;

    }


}