public with sharing class TasMt5AccountCallout extends TasCallout {

    public TasMt5AccountCallout() {
        super();
    }
    
    /**
    * Get MT5 accounts details by one Id
    */
    public TasMt5AccsDetailsGetResponse getMt5AccsDetails(EnvironmentIdentifier identifier, String fxaccountEmail) {
        String oneId = getOneId(fxaccountEmail);
        if(oneId == null) {
            return null;
        }

        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(Constants.TAS_API_MT5_ENDPOINT),
            new List<String> {oneId, identifier.env + '&mt5_details=true'});

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);
        
        if (notFound())
            return null;

        if (!isResponseCodeSuccess())
            throw getError();

        TasMt5AccsDetailsGetResponse tr = new TasMt5AccsDetailsGetResponse();

        tr.mt5Accs = parseResponse(resp.getBody());
        return tr;
    }

    public List<TasMT5AccsDetails> parseResponse(String body) {
        List<TasMT5AccsDetails> result = new List<TasMT5AccsDetails>();
        try {
            result = (List<TasMT5AccsDetails>) parseJsonToObject(body, List<TasMT5AccsDetails>.class);
        } catch (Exception ex) {
            String replacedJson = '';

            if (body.contains('"currency"')) {
                replacedJson = body.replace('"currency"', '"accountCurrency"');
            } else {
                replacedJson = body;
            }

            JSONParser parser = JSON.createParser(replacedJson);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME && String.valueOf(parser.getText()) == 'mt5') {
                    parser.nextToken();
                    if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                        while (parser.nextToken() != null) {
                            if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                                TasMT5AccsDetails tasMT5AccsDetails = (TasMT5AccsDetails) parser.readValueAs(TasMT5AccsDetails.class);
                                tasMT5AccsDetails.balance = tasMT5AccsDetails.details.Balance;
                                List<String> propertyValues = String.isNotBlank(tasMT5AccsDetails.details.properties.mt5_group)
                                        ? tasMT5AccsDetails.details.properties.mt5_group.split('\\\\')
                                        : new List<String>();

                                if (!propertyValues.isEmpty()) {
                                    Set<String> settings = propertyValues.size() > 1
                                            ? new Set<String>(propertyValues.get(propertyValues.size() - 1).split('\\.'))
                                            : new Set<String>();
                                    String bookType = propertyValues.size() > 2
                                            ? propertyValues.get(propertyValues.size() - 2)
                                            : null;

                                    tasMT5AccsDetails.accountType = settings.contains('r1')
                                            ? 'Retail'
                                            : settings.contains('r0')
                                                ? 'Professional'
                                                : tasMT5AccsDetails.accountType;

                                    tasMT5AccsDetails.sharesEnabled = settings.contains('w1')
                                            ? 'Yes'
                                            : settings.contains('w0')
                                                ? 'No'
                                                : tasMT5AccsDetails.sharesEnabled;

                                    tasMT5AccsDetails.book = bookType.endsWithIgnoreCase('bk')
                                            ? bookType.removeEndIgnoreCase('bk').toUpperCase()
                                            : bookType;

                                    result.add(tasMT5AccsDetails);
                                }
                                parser.skipChildren();

                            }
                            if (parser.getCurrentToken() == JSONToken.END_ARRAY) {
                                break;
                            }
                        }
                    }
                }
            }


        }

        return result;
    }

    /**
    * Get MT5 accounts details by one Id
    */
    public String lockUnlockMt5AccountByAccountId(EnvironmentIdentifier identifier, String action) {
        String lockString = '{ "isDisabled": true }';
        String unlockString = '{ "isDisabled": false }';
        
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(Constants.TAS_API_MT5_LOCK_UNLOCK_ENDPOINT),
            new List<String> {identifier.id, identifier.env});
        
        String actionbody = action == 'lock' ?  lockString : unlockString;
        patch(resource, header, null, actionbody);
        
        if (notFound())
            return null;


            
        if (!isResponseCodeSuccess())
            throw getError();
        
        return resp.getBody();
    } 

    public void updateMt5AccsDetails(
        EnvironmentIdentifier identifier,
        TasV20AccountPatchRequest v20Account
    ) {
    }
   
    public String getOneId(String fxaccountEmail) {
        //set dummy input params
        CryptoAccountParams params = new CryptoAccountParams();
        params.fxaccountIds = new List<Id>();
        CryptoAccountActionsCallout caac = new CryptoAccountActionsCallout(params);        
        return caac.getOneIdByEmailNoActions(fxaccountEmail);
    }

//The new method depends on the Named_Credential_Configuration__mdt and if the checkbox is disabled the logic will not work.
//Please use the new SimpleCallouts class if your logic should not depend on the Named_Credential_Configuration__mdt value
//SimpleCallouts has the same method, which will help you quickly replace it when the new direct connection is fully implemented.
//    public void updateGroupRecalculateTasApi(EnvironmentIdentifier identifier, String body) {
//        String resource = String.format(
//                SPGeneralSettings.getInstance().getValue(
//                        Constants.TAS_API_GROUP_RECALCULATE_ENDPOINT),
//                new List<String> {identifier.id, identifier.env}
//        );
//
//        patch(resource, header, null, body);
//
//        if (!isResponseCodeSuccess())
//            throw getError();
//    }
        
}