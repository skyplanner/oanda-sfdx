/**
 * Created by akajda on 29/09/2023.
 */

public with sharing class BatchBackfillPhoneDR implements Database.Batchable<SObject>  {

    public String query = 'SELECT id, Phone, Phone_DR__c FROM Account WHERE Phone != NULL AND Phone_DR__c = NULL';

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<SObject> batch) {
        List<Account> accs = (List<Account>) batch;
        for(Account acc : accs){
            acc.Phone_DR__c = AccountTriggerHandler.clearPhoneNumber(acc.Phone);
        }
        update accs;
    }

    public void finish(Database.BatchableContext bc) {
    }
}