/**
 * @File Name          : SPCaseTriggerHandler.cls
 * @Description        : 
 * @Author             : Michel Carrillo (SkyPlanner)
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/26/2024, 1:46:27 AM
**/

public without sharing class SPCaseTriggerHandler {

    public static final String OBJ_API_NAME = 'Case';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //******************************************************* */

    final List<Case> newList;
    final Map<ID,Case> oldMap;

    public SPCaseTriggerHandler(
        List<Case> newList,
        Map<ID,Case> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }
    
    public void handleBeforeInsert() {
        OnNewEmailCasesCmd onNewEmailCases = 
            new OnNewEmailCasesCmd();
        OnNewChatOrPhoneCasesCmd onNewChatOrPhoneCases = 
            new OnNewChatOrPhoneCasesCmd();
        for(Case caseObj : newList) {
            onNewEmailCases.checkRecord(caseObj);
            onNewChatOrPhoneCases.checkRecord(caseObj);
        }
        onNewEmailCases.execute();
        onNewChatOrPhoneCases.execute();
    }

    public void handleAfterInsert() {
        CaseRoutingManager routingManager = CaseRoutingManager.getInstance();
        GetCasesToBeRoutedCmd getCasesCmd = new GetCasesToBeRoutedCmd(
            newList, // recList
            null // oldMap
        );
        getCasesCmd.skillCasesQueueId = routingManager.getSkillCasesQueueId();
        List<Case> casesToBeRouted = getCasesCmd.execute();
        routingManager.routeUsingSkills(casesToBeRouted);
    }

    public void handleBeforeUpdate(){
        OnCaseOwnerOrStatusChangesCmd onCaseOwnerOrStatusChanges = 
            new OnCaseOwnerOrStatusChangesCmd();
        PhoneSLAViolationManager phoneSlaManager = 
            new PhoneSLAViolationManager(oldMap);
        //...
        for(Case caseObj : newList) {
            Case oldCase = oldMap.get(caseObj.Id);
            //...
            onCaseOwnerOrStatusChanges.checkRecord(caseObj, oldCase);
            phoneSlaManager.checkRecord(caseObj, oldCase);
        }
        //...
        phoneSlaManager.processHVCPhoneCaseBounces();
    }

    public void handleAfterUpdate(){
        CompleteMiletonesCmd completeMiletones = 
            new CompleteMiletonesCmd();
        OnNonHvcCaseReleasedCmd onNonHvcCaseReleased = 
            new OnNonHvcCaseReleasedCmd();
        OnInternalCasesRoutedCmd onInternalCasesRouted = 
            new OnInternalCasesRoutedCmd();
        //...
        for(Case caseObj : newList) {
            Case oldCase = oldMap.get(caseObj.Id);
            //...
            completeMiletones.checkRecord(caseObj, oldCase);
            onNonHvcCaseReleased.checkRecord(caseObj, oldCase);
            onInternalCasesRouted.checkRecord(caseObj, oldCase);
        }
        //...
        completeMiletones.execute();
        onNonHvcCaseReleased.execute();
        onInternalCasesRouted.execute();
        //...
        new MilestonesSLAViolationManager(
            newList,
            oldMap
        ).processSLAViolations();
    }
    
}