@isTest(seeAllData=false)
public with sharing class BatchNotifyNewRFFCustomersTest 
{
    @TestSetup
    static void initData()
    {
        Account[] acs = new Account[]{         
            new Account(FirstName = 'First1',
                        LastName='Last1', 
                        PersonEmail = 'test1@oanda.com',
                        Account_Status__c = 'Active', 
                        PersonMailingCountry = 'Iran, Islamic Republic of'),
            new Account(FirstName = 'First2',
                        LastName='Last2', 
                        PersonEmail = 'test2@oanda.com',
                        Account_Status__c = 'Active', 
                        PersonMailingCountry = 'Canada')
        };
        insert acs;
            
        fxAccount__c[] tradeAccounts = new fxAccount__c[]
        {
            new fxAccount__c(Account_Email__c = 'test1@oanda.com',
                            Funnel_Stage__c = FunnelStatus.DOCUMENTS_UPLOADED,
                            RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Division_Name__c = 'OANDA Europe Markets',
                            Citizenship_Nationality__c = 'Iran, Islamic Republic of',
                            Account__c = acs[0].Id),
            new fxAccount__c(Account_Email__c = 'test2@oanda.com',
                            Funnel_Stage__c = FunnelStatus.MORE_INFO_REQUIRED,
                            RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                            Division_Name__c = 'OANDA Canada',
                            Citizenship_Nationality__c = 'Canada',
                            Account__c = acs[1].Id)
        };
        insert tradeAccounts;

        fxAccount__c[] updateList = new fxAccount__c[]{};
        tradeAccounts[0].Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        tradeAccounts[1].Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        updateList.add(tradeAccounts[0]);
        updateList.add(tradeAccounts[1]);
        update updateList;
    }

    @IsTest
    static void test1()
    {
        Test.startTest();
        
        BatchNotifyNewRFFCustomersSchedulable.execute();
        Test.stopTest();
        
        system.assertEquals(1, BatchNotifyNewRFFCustomersSchedulable.emailMessages.size());
    }
}