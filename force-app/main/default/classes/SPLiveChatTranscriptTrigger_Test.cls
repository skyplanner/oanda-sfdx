/**
 * @File Name          : SPLiveChatTranscriptTrigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 11:44:17 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/13/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class SPLiveChatTranscriptTrigger_Test {

    // @testSetup
    // static void setup(){
    // }

    //complete info from account
    @isTest
    static void insertUpdate_test1() {
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        Test.startTest();
        insert chatList;
        LiveChatTranscript chat1 = 
            [select Id, Inquiry_Nature__c from LiveChatTranscript where Id = :chatList[0].Id];
        chat1.Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER;
        update chat1;
        Test.stopTest();
        chatList = [
            select 
                Id, 
                Is_HVC__c, 
                Chat_Division__c,
                AccountId
            from 
                LiveChatTranscript
        ];
        chat1 = chatList[0];
        LiveChatTranscript chat2 = chatList[1];
        System.assertEquals(OmnichanelConst.CHAT_IS_HVC_YES, chat1.Is_HVC__c);
        System.assertNotEquals(null, chat1.Chat_Division__c);
        System.assertNotEquals(null, chat1.AccountId);
        System.assertEquals(OmnichanelConst.CHAT_IS_HVC_YES, chat2.Is_HVC__c);
        System.assertNotEquals(null, chat2.Chat_Division__c);
        System.assertNotEquals(null, chat2.AccountId);
    }
    
    //an exception (other than LogException) is thrown 
    @isTest
    static void exception1() {
        Boolean exceptionOk = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        Test.startTest();
        ExceptionTestUtil.prepareDummyException();
        try {
            insert chatList;
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    //an exception (of type LogException) is thrown 
    @isTest
    static void exception2() {
        Boolean exceptionOk = false;
        List<LiveChatTranscript> chatList = 
            OmnichanelRoutingTestDataFactory.getNewChats(
                OmnichanelConst.OANDA_CANADA,
                OmnichanelConst.INQUIRY_NAT_LOGIN
            );
        Test.startTest();
        ExceptionTestUtil.exceptionInstance = 
            LogException.newInstance('abc', 'def');
        try {
            insert chatList;
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }
    
}