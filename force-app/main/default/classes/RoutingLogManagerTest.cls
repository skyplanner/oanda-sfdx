/**
 * @File Name          : RoutingLogManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2024, 1:14:38 AM
**/
@IsTest
private without sharing class RoutingLogManagerTest {

	@IsTest
	static void getLogForCase() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		RoutingLogManager instance = new RoutingLogManager(routingInfo);
		Routing_Log__c result = (Routing_Log__c) instance.getLogForCase();
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isNotNull(result.Priority__c, 'Invalid value');
		Assert.isNotNull(result.Tier__c, 'Invalid value');
		Assert.areEqual(
			RoutingLogManager.CASE_CHANNEL, 
			result.Service_Channel__c, 
			'Invalid value'
		);
	}

	@IsTest
	static void getLogForChatTranscript() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.CHAT, // infoSource
			new LiveChatTranscript() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		RoutingLogManager instance = new RoutingLogManager(routingInfo);
		Routing_Log__c result = 
			(Routing_Log__c) instance.getLogForChatTranscript();
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isNotNull(result.Priority__c, 'Invalid value');
		Assert.isNotNull(result.Tier__c, 'Invalid value');
		Assert.areEqual(
			RoutingLogManager.LIVE_AGENT_CHANNEL, 
			result.Service_Channel__c, 
			'Invalid value'
		);
	}

	@IsTest
	static void getLogForMsgSession() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.MESSAGING, // infoSource
			new MessagingSession() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		RoutingLogManager instance = new RoutingLogManager(routingInfo);
		Routing_Log__c result = 
			(Routing_Log__c) instance.getLogForMsgSession();
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isNotNull(result.Priority__c, 'Invalid value');
		Assert.isNotNull(result.Tier__c, 'Invalid value');
		Assert.areEqual(
			RoutingLogManager.MESSAGING_CHANNEL, 
			result.Service_Channel__c, 
			'Invalid value'
		);
	}
	
}