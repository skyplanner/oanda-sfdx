/**
 * SP-7630.
 * Syncs all Comply Advantage Searches whose Is_Monitored flag
 * is still turned on and their respective fxAccount has been closed
 * for some time now. This will happen if the synchronization
 * fails in real time. The Is_Monitoring is turned off in all CA Searches
 * @author Fernando Gomez
 */
public without sharing class ComplyAdvantageMonitorOffBatch
		implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection,
			Database.AllowsCallouts {
				String query;
				Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	/**
	 * Main constructor
	 */
	public ComplyAdvantageMonitorOffBatch() {
		query='SELECT Id, ' +
		'Search_Id__c, ' +
		'Is_Monitored__c, ' +
		'fxAccount__c, ' +
		'fxAccount__r.Division_Name__c, ' +
		'fxAccount__r.Country__c, ' +
		'Affiliate__c, ' +
		'Affiliate__r.Division_Name__c, ' +
		'Affiliate__r.Country__c ' +
	'FROM Comply_Advantage_Search__c ' +
	'WHERE Is_Monitored__c = true ' +
	'AND ( ' +
		'fxAccount__r.Is_Closed__c = true ' +
	') ' +
	'ORDER BY LastModifiedDate DESC';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query='SELECT Id, ' +
		'Search_Id__c, ' +
		'Is_Monitored__c, ' +
		'fxAccount__c, ' +
		'fxAccount__r.Division_Name__c, ' +
		'fxAccount__r.Country__c, ' +
		'Affiliate__c, ' +
		'Affiliate__r.Division_Name__c, ' +
		'Affiliate__r.Country__c ' +
		'FROM Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

	/**
	* @description Changed to only disable monitoring the closed account, not locked ones
	* @author Yaneivys Gutierrez | 06-22-2022 
	* @param BC 
	* @return Database.QueryLocator 
	**/
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
	 * @param BC
	 * @param csSearches
	 */
	public void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Comply_Advantage_Search__c> caSearches;
		ComplyAdvantageMonitorBatch syncBatch;
		List<String> searchIds;

		// we cast the scope
		caSearches = (List<Comply_Advantage_Search__c>)scope;
		searchIds = new List<String>();

		// we turn off all the searches, gather the search ids
		for (Comply_Advantage_Search__c caSearch : caSearches) {
			caSearch.Is_Monitored__c = false;
			searchIds.add(caSearch.Search_Id__c);
		}

		// and process to sync  to comply advantage portal
		syncBatch = new ComplyAdvantageMonitorBatch(null);
		syncBatch.executeSync(caSearches);

		// we need to disconect the trigger to avoid
		// launching a batch per each scope's execute,
		// we'll turn the monitoring off in the same thread...
		ComplyAdvantageHelper.complyAdvantageSearchAfterTriggerEnabled = false;

		// we update the record
		update caSearches;

		// we reenable the trigger after the trasaction is done
		ComplyAdvantageHelper.complyAdvantageSearchAfterTriggerEnabled = true;
	}

	/**
	 * @param BC
	 * @param csSearches
	 */
	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}