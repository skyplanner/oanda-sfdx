/**
 * @File Name          : CreateLeadFromMsgSessionAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/15/2023, 12:35:02 AM
**/
public without sharing class CreateLeadFromMsgSessionAction { 

	@InvocableMethod(label='Create Lead From Msg Session' callout=false)
	public static List<ID> createLeadFromMsgSession(List<ID> messagingSessionIds) { 
		try {
			ExceptionTestUtil.execute();
			List<ID> result = new List<ID> { null };

			if (
				(messagingSessionIds == null) ||
				messagingSessionIds.isEmpty()
			) {
				return result;
			}
			// else...        
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(messagingSessionIds[0]);
			result[0] = process.createLeadFromMsgSession();
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CreateLeadFromMsgSessionAction.class.getName(),
				ex
			);
		}
	} 
	
}