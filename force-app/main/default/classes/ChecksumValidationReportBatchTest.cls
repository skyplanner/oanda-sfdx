@isTest
public with sharing class ChecksumValidationReportBatchTest {
    
    @TestSetup
    static void setup(){
        MIFIDValidationTestDataFactory.createValidableFxAccounts();
    }

    @IsTest
    static void testBatch(){
        Test.startTest();
        ChecksumValidationReportBatch.runBatchWithDefaultValues();
        Test.stopTest();
    }
}