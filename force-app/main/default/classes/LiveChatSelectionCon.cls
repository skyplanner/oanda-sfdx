/**
 * @File Name          : LiveChatSelectionCon.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/29/2020, 4:41:42 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/3/2020   acantero     Initial Version
 * 1.1    6/25/2020  dmorales     Always redirect to LiveChatLanguage
**/
public without sharing class LiveChatSelectionCon {
	
	public static final String PARAMETER_TOPIC = 'Department';
	public static final String PARAMETER_REGION = 'Division';
	public static final String PARAMETER_LANGUAGE = 'InitLanguage';
	public static final String PARAMETER_ACCOUNT_TYPE = 'AccountType';
	public static final String PARAMETER_LANGUAGE_PREFERENCE = 'LanguagePreference';
	public static final String PARAMETER_EMAIL = 'CustomerEmail';
    
    private String topic;
    private String region;
    private String account_type;
    public String langPref {get; private set;}
	private String email;
	
	public String language { get; set; }
	public Boolean isOffline {get ; private set; }
   
      
    public LiveChatSelectionCon(){
    	topic = ApexPages.currentPage().getParameters().get(PARAMETER_TOPIC);
    	region = ApexPages.currentPage().getParameters().get(PARAMETER_REGION);
    	language = ApexPages.currentPage().getParameters().get(PARAMETER_LANGUAGE);
    	account_type = ApexPages.currentPage().getParameters().get(PARAMETER_ACCOUNT_TYPE);
    	langPref = ApexPages.currentPage().getParameters().get(PARAMETER_LANGUAGE_PREFERENCE);
		email = ApexPages.currentPage().getParameters().get(PARAMETER_EMAIL);
		
		//find if chat agentes are offline
		ChatOfflineSettings chatSetting = ChatOfflineSettings.getDefaultInstance();
        isOffline = chatSetting.initializationFails ? false : chatSetting.isOffline;
	}
	
	public PageReference redirect() {
		PageReference pr;
	   // the page reference if always the home page
		pr = Page.LiveChatLanguage;

	    if(langPref != null && langPref != '' && isOffline){
			pr.getParameters().put('LanguagePreferenceOffline', langPref);
		}
		pr.getParameters().put('isRedirect', 'true');
		
		return pr;
	}
	
    
}