/**
 * @File Name          : ServiceSkillReqManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 5:17:48 PM
**/
@IsTest
private without sharing class ServiceSkillReqManagerTest {

	@IsTest
	static void getSkillRequirements() {
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		sRoutingInfo.language = OmnichanelConst.ENGLISH_LANG_CODE;
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> { sRoutingInfo };
		
		Test.startTest();
		ServiceSkillReqManager instance = new ServiceSkillReqManager();
		List<SkillRequirement> result = 
			instance.getSkillRequirements(infoList);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}

	@IsTest
	static void getNonLanguageSkillsAreAditional() {
		Boolean dropAdditionalSkills = true;
		
		Test.startTest();
		ServiceSkillReqManager instance = new ServiceSkillReqManager();
		Boolean result = 
			instance.getNonLanguageSkillsAreAditional(dropAdditionalSkills);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	@IsTest
	static void getSkillsInfo() {
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		sRoutingInfo.language = OmnichanelConst.ENGLISH_LANG_CODE;
		
		Test.startTest();
		ServiceSkillReqManager instance = new ServiceSkillReqManager();
		List<SSkillRequirementInfo> result = 
			instance.getSkillsInfo(sRoutingInfo);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}

	@IsTest
	static void setSkillsPriorities() {
		SSkillRequirementInfo skillReqInfo = new SSkillRequirementInfo(
			OmnichanelConst.LOGIN_SKILL, // skillName
			true // isAdditionalSkill
		);
		List<SSkillRequirementInfo> skillsInfo = 
			new List<SSkillRequirementInfo> { skillReqInfo };

		Test.startTest();
		ServiceSkillReqManager instance = new ServiceSkillReqManager();
		// trick (set manually globalSkillNameSet)
		instance.globalSkillNameSet = new Set<String>{ OmnichanelConst.LOGIN_SKILL };
		instance.setSkillsPriorities(skillsInfo);
		Test.stopTest();
		
		Assert.isNotNull(
			skillReqInfo.skillReq.SkillPriority, 
			'Additional skills must have SkillPriority <> null'
		);
	}
	
}