@RestResource(urlMapping='/customer/riskassessment/') 
global without sharing class CustomerRiskAssessmentService 
{
    @HttpPost
    global static void CalculateCustomerRiskAssessment() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        ServiceResponse cras = new ServiceResponse();
       
        try
        {  
            String reqBody = '';
            if(req.requestBody != null)
            {
                reqBody = req.requestBody.toString().trim();
            }
            
            Map<String, String> paramValueByname = parseRequestBody(reqBody);
            string divisionName= paramValueByname.get('division');
            string employmentStatus = paramValueByname.get('employmentStatus');
            string industryofEmployment = paramValueByname.get('industryofEmployment');
            string countryOfResidence = paramValueByname.get('countryOfResidence');
            string citizenshipNationality = paramValueByname.get('citizenshipNationality');
            string incomeSourceDetails = paramValueByname.get('incomeSourceDetails');
			set<string> incomeDetails = new set<string>{}; 
                
            if(string.isnotBlank(incomeSourceDetails))
            {
                incomeDetails.addAll(incomeSourceDetails.split(','));
            }
            string risk = fxAccountRiskAssessment.calculateCustomerRiskAssessment(divisionName,
                                                                            employmentStatus,
                                                                            industryofEmployment,
                                                                            countryOfResidence,
                                                                            citizenshipNationality,
                                                                            incomeDetails);
            cras.risk = risk;
            
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(JSON.serializePretty(cras));
            res.statusCode = 200;                                                                 
        }
        catch(Exception ex)
        {
            res.addHeader('Content-Type', 'application/json');
            string errorInfo = ex.getMessage() + '\n' + ex.getStackTraceString();
            res.responseBody = Blob.valueOf('{ "stack trace" : '+errorInfo+'}');
            res.statusCode = 500;
        }
    }
    
    private static Map<String, String> parseRequestBody(String body)
    {
        Map<String, String> fieldValueByName = new Map<String, String>();
        
        JSONParser parser = JSON.createParser(body);

        while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
            {   
                String fieldName = parser.getText();
                parser.nextToken();
                
                String fieldValue = parser.getText();   
                fieldValueByName.put(fieldName, fieldValue);
            }        
        }
        
        
        return fieldValueByName;
    }
    
    class ServiceResponse
    {
        string risk;
    }
}