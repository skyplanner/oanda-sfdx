/**
 * @File Name          : AgentInfoManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:09:44 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/9/2020   acantero     Initial Version
**/
@istest(isParallel = false)
private class AgentInfoManager_Test {

    static final String FAKE_AGENT_ID = '0050B0000081XTXQA2';

    @istest
    static void updateAgentInfo_test1() {
        Test.startTest();
        Boolean result1 = AgentInfoManager.updateAgentInfo(
            null, 
            null, 
            null, 
            null
        );
        Test.stopTest();
        System.assertEquals(false, result1);
    }

    @istest
    static void existsCompatibleAgent_test1() {
        Test.startTest();
        Boolean result1 = AgentInfoManager.existsCompatibleAgent(
            null, 
            null, 
            null
        );
        Test.stopTest();
        System.assertEquals(null, result1);
    }

    @istest
    static void existsCompatibleAgent_test2() {
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = FAKE_AGENT_ID,
            Name = FAKE_AGENT_ID,
            Capacity__c = 3,
            Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
        );
        insert agentInfo;
        Test.startTest();
        Boolean result = AgentInfoManager.existsCompatibleAgent(
            new List<String> {FAKE_AGENT_ID}, 
            'LE CC OME', 
            3
        );
        Test.stopTest();
        System.assertEquals(false, result);
    }

    @istest
    static void deleteCurrentAgentInfo_test1() {
        Test.startTest();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0001');
        System.runAs(supervisor) {
            String agentId = UserInfo.getUserId();
            Agent_Info__c agentInfo = new Agent_Info__c(
                Agent_ID__c = agentId,
                Name = agentId,
                Capacity__c = 3,
                Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
            );
            insert agentInfo;
            //..
            AgentInfoManager.deleteCurrentAgentInfo();
            //...
        }
        Test.stopTest();
        Integer count = [select count() from Agent_Info__c];
        System.assertEquals(0, count);
    }

    @istest
    static void safeDeleteAgentInfo_test1() {
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = FAKE_AGENT_ID,
            Name = FAKE_AGENT_ID,
            Capacity__c = 3,
            Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
        );
        insert agentInfo;
        Test.startTest();
        AgentInfoManager.safeDeleteAgentInfo(
            FAKE_AGENT_ID, 
            true
        );
        Test.stopTest();
        Integer count = [select count() from Agent_Info__c];
        System.assertEquals(1, count);
    }



}