public with sharing class ComplyAdvantageSearchMigrationHelper  implements Callable, Database.AllowsCallouts
{

    public static final Schema.SObjectField SEARCH_ID_FIELD = Comply_Advantage_Search__c.Fields.Search_Id__c;
    public static final string SEARCH_REASON = 'CA Instance Split';

    public static final string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    public static final string onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public static final Id complyAdvantageCaseQueueId = UserUtil.getQueueId('Comply Advantage Cases');
       
    public static Map<string, string> countryNameByISOMap =  getCountryCodes();    

    ComplyAdvantageSearchRecord[] searchRecords = new ComplyAdvantageSearchRecord[]{};
 
    Comply_Advantage_Search__c[] searchesToUpdate;
    Comply_Advantage_Search_Entity__c[] entitiesToUpdate;
    
    public static string naInstanceKey= getInstanceApiKeyByName('NA');
    public static string apacInstanceKey= getInstanceApiKeyByName('APAC');

    public static Map<string,string> divisionoldAPIKeyMap = new Map<string,string>
    {
        Constants.DIVISIONS_OANDA_CANADA => naInstanceKey,
        Constants.DIVISIONS_OANDA_AUSTRALIA => apacInstanceKey
    };


    public ComplyAdvantageSearchMigrationHelper() 
    {
       
    }

    public Object call(String action, Map<String, Object> args) 
    {
        switch on action 
        {
            when 'instanceMigrate' 
            {
                instanceMigrate((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'entitiesSync' 
            {
                entitiesSync((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
            when 'updateCAEntites' 
            {
                updateCAEntites((List<Comply_Advantage_Search__c>)args.get('records'));       
            }
        }
        return null;
    }

    public void instanceMigrate(List<Comply_Advantage_Search__c> searches)
    {
        searchesToUpdate = new Comply_Advantage_Search__c[]{};
        entitiesToUpdate = new Comply_Advantage_Search_Entity__c[]{};

        searchRecords = new ComplyAdvantageSearchRecord[]{};

        for(Comply_Advantage_Search__c search: searches)
        {
            searchRecords.add(new ComplyAdvantageSearchRecord(search));
        }
        for(ComplyAdvantageSearchRecord searchRec : searchRecords)
        {
            try 
            {           
                migrateSearch(searchRec);
            } 
            catch (Exception ex) 
            {
                Logger.exception(Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex);
            }
        }
        if(searchesToUpdate.size() > 0)
        {
            Database.SaveResult[] caSaveResult = Database.update(searchesToUpdate, false);
        }
        if(entitiesToUpdate.size() > 0)
        {
            Database.SaveResult[] entitiesSaveResult = Database.update(entitiesToUpdate, false);
        }
    }

    
    public void migrateSearch(ComplyAdvantageSearchRecord searchRec)
    {
        ComplyAdvantageSearchResponse caResponse = newSearchCallout(searchRec);
        
        if(caResponse.newTotalHits >= 100)
        {
            searchRec.isSecondarySearch = true;
            caResponse = newSearchCallOut(searchRec);
        }
        boolean oldSearchTurnedOff =  monitorOffCA(searchRec);

        Comply_Advantage_Search__c searchUpdate = new Comply_Advantage_Search__c(Id=searchRec.recordId);
        searchUpdate.search_reference__c = caResponse.newSearchReference;
        searchUpdate.Search_Reference_Before_Instance_Split__c = searchRec.searchReference;
        searchUpdate.Search_Id__c = caResponse.newSearchId;
        searchUpdate.Total_Hits__c = caResponse.newTotalHits;
        searchUpdate.Report_Link__c = caResponse.newReportLink;
        searchUpdate.Case_Management_Link__c = 'https://app.complyadvantage.com/#/case-management/search/' + caResponse.newSearchReference;
        searchUpdate.Fuzziness__c = searchRec.divisionSettings.fuzziness * 100;
        searchUpdate.Search_Profile__c = searchRec.searchProfile;
        searchUpdate.is_secondary_search__c = searchRec.isSecondarySearch;
        searchUpdate.Search_Reason__c = SEARCH_REASON;
        
        searchUpdate.Monitor_Change_Date__c = null;
        searchUpdate.Has_Monitor_Changes__c = false;

        if(searchRec.matchStatus != caResponse.newMatchStatus)
        {
            updateSearchMatchStatus(caResponse.newSearchReference, searchRec.matchStatus, searchRec.divisionSettings.apiKey);
        }
        searchesToUpdate.add(searchUpdate);

        if(searchRec.searchEntities.size() > 0)
        {
            for(Comply_Advantage_Search_Entity__c entity : searchRec.searchEntities)
            {
                entity.Monitor_Change_Status__c = '';
                entity.Is_Whitelisted_After_profile_Update__c = false;
                entity.Risk_Level_After_Profile_Update__c = '';
                entity.Match_Status_After_Profile_Update__c = '';
                entity.Unique_ID__c = caResponse.newSearchId + ':' + entity.Entity_Id__c;
                entitiesToUpdate.add(entity);
            }
        }
    }

    public ComplyAdvantageSearchResponse newSearchCallout(ComplyAdvantageSearchRecord searchRec)
    {
        ComplyAdvantageSearchResponse caResponse = new ComplyAdvantageSearchResponse();
        try
        {
            string requestBody = getNewSearchRequestBody(searchRec);
            Logger.info('requestBody', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, requestBody);
    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches?api_key='+ searchRec.divisionSettings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('POST');
            request.setTimeout(120000);
            request.setBody(requestBody); 
            HttpResponse response = http.send(request);
            Logger.info('responseBody', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, response);

            caResponse = new ComplyAdvantageSearchResponse(response);
        }
        catch(Exception ex)
        {
            Logger.error('Search Profile - New Search', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex.getMessage());
        }
        return caResponse;
    }
    public boolean updateSearchMatchStatus(string searchRef, string matchStatus, string apiKey)
    {
        try
        {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRef +'?api_key='+ apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            string requestBody = '{ "match_status" : "' + matchStatus+'"}';
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);

            if(response.getStatusCode() == 200)
            {
                return true;
            }
            return false;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    public boolean monitorOffCA(ComplyAdvantageSearchRecord searchRec)
    {
        try
        {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'/monitors?api_key='+ searchRec.divisionSettings.oldInstanceApiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            string requestBody = '{ "is_monitored" : false }';
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);

            if(response.getStatusCode() == 200)
            {
                return true;
            }
            return false;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    private string getNewSearchRequestBody(ComplyAdvantageSearchRecord searchRecord)
    {
        SearchRerquestBody reqBody = new SearchRerquestBody();
       
        reqBody.search_term = searchRecord.searchText;
        reqBody.client_ref = searchRecord.clientReference;
        reqBody.search_profile = searchRecord.searchProfile;
        reqBody.fuzziness =  searchRecord.divisionSettings.fuzziness;

        if(searchRecord.isSecondarySearch)
        {
           reqBody.exact_match = true;
        }
        reqBody.share_url =  1;

        reqBody.filters = new SearchFilters();
        reqBody.filters.birth_year = String.valueOf(searchRecord.searchParamBirthYear);
        if(string.isNotBlank(searchRecord.searchParamCitizenshipCountry) || string.isNotBlank(searchRecord.searchParamMailingCountry))
        {
            Country_Setting__c cnCountrySetting =  CustomSettings.getCountrySettingByName(searchRecord.searchParamCitizenshipCountry);
            Country_Setting__c mcCountrySetting = CustomSettings.getCountrySettingByName(searchRecord.searchParamMailingCountry);
            
            reqBody.filters.country_codes = new string[]{};
            if(cnCountrySetting != null && cnCountrySetting.ISO_Code__c != null)
            {
                reqBody.filters.country_codes.add(cnCountrySetting.ISO_Code__c);
            }
            if(mcCountrySetting != null && mcCountrySetting.ISO_Code__c != null)
            {
                reqBody.filters.country_codes.add(mcCountrySetting.ISO_Code__c);
            }
        }
        return JSON.serialize(reqBody);
    }


    public void entitiesSync(List<Comply_Advantage_Search__c> searches)
    {
        List<Comply_Advantage_Search_Entity__c> entitesToUpsert = new List<Comply_Advantage_Search_Entity__c>{};
        List<Comply_Advantage_Search__c> searchesToUpdate = new List<Comply_Advantage_Search__c>{};

        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        set<Id> searchIds = searchMap.keySet();

        Map<Id, set<string>> searchEntityUniqueIdsMap = new Map<Id, set<string>>{};
        Map<string, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId = new Map<string, Comply_Advantage_Search_Entity__c>{};

        //retrieve entities from Salesforce for the searches
        for(Comply_Advantage_Search_Entity__c entity : [select ID, Unique_ID__c, Comply_Advantage_Search__c
                                                        From Comply_Advantage_Search_Entity__c
                                                        Where Comply_Advantage_Search__c IN :searchIds])
        {    
            set<string> entityUniqueIds = searchEntityUniqueIdsMap.get(entity.Comply_Advantage_Search__c);
            if(entityUniqueIds == null)
            {
                entityUniqueIds = new set<string>{};
            }
            entityUniqueIds.add(entity.Unique_ID__c);
            searchEntityUniqueIdsMap.put(entity.Comply_Advantage_Search__c, entityUniqueIds);

            sfBatchEntitiesByUniqueId.put(entity.Unique_ID__c, entity);
        }

        for(Comply_Advantage_Search__c search : searches)
        {
            ComplyAdvantageEntitiesDownloader helper = new ComplyAdvantageEntitiesDownloader(search);
            ComplyAdvantageEntitiesDownloader.EntityDownloadResult result = helper.getEntities();
            if(result.isSuccessful)
            {
                Comply_Advantage_Search_Entity__c[] caEntities = result.entities;

                set<string> sfSearchEntityIds = searchEntityUniqueIdsMap.get(search.Id);
                if(sfSearchEntityIds != null && !sfSearchEntityIds.isEmpty())
                {
                    caEntities = syncCASFEntities(sfSearchEntityIds, sfBatchEntitiesByUniqueId, caEntities);
                }
                entitesToUpsert.addAll(caEntities);
                
                search.Entities_Download_Status__c = 'Done_Profile_Update';
                searchesToUpdate.add(search);
            }
            else 
            {
                search.Entities_Download_Status__c = 'Failed';
                searchesToUpdate.add(search);
            }
        }

        if(!entitesToUpsert.isEmpty())
        {
            Database.upsert(entitesToUpsert, BatchComplyAdvantageEntitiesDownloader.ENTITY_OBJ_UNIQUE_NUMBER_FIELD, false);
        }
        if(!searchesToUpdate.isEmpty())
        {
            Database.update(searchesToUpdate, false);
        }
    }

    //if any entity from Salesforce not present in CA result will be marked as 'Removed'
    private Comply_Advantage_Search_Entity__c[] syncCASFEntities(set<string> sfSearchEntityIds,
                                                                 Map<string, Comply_Advantage_Search_Entity__c> sfBatchEntitiesByUniqueId,
                                                                 Comply_Advantage_Search_Entity__c[] caEntities)
    {
        Comply_Advantage_Search_Entity__c[] entities = new Comply_Advantage_Search_Entity__c[]{};
        set<string> caEntityUniqueIds = new set<String>{};

        for(Comply_Advantage_Search_Entity__c caEntity : caEntities)
        {
            string caEntityUniqueId = caEntity.Unique_ID__c;
            if(sfSearchEntityIds.contains(caEntityUniqueId))
            {
                //update new match status, whitelisted,risk level for later update
                Comply_Advantage_Search_Entity__c sfEntity = sfBatchEntitiesByUniqueId.get(caEntityUniqueId);
                sfEntity.Is_Whitelisted_After_profile_Update__c = caEntity.Is_Whitelisted__c;
                sfEntity.Match_Status_After_Profile_Update__c = caEntity.Match_Status__c;
                sfEntity.Risk_Level_After_Profile_Update__c = caEntity.Risk_Level__c;
                sfEntity.Is_Removed__c = false;
                entities.add(sfEntity);
            }
            else
            {
                //new entities
                entities.add(caEntity);
            }
            caEntityUniqueIds.add(caEntityUniqueId);
        }

        //mark removed entites
        for(string sfEntityUniqueId : sfSearchEntityIds)
        {
            if(!caEntityUniqueIds.contains(sfEntityUniqueId))
            {
                Comply_Advantage_Search_Entity__c sfEntity = sfBatchEntitiesByUniqueId.get(sfEntityUniqueId);
                sfEntity.Is_Removed__c = true;
                entities.add(sfEntity);
            }
        }

        return entities;
    } 

    public void updateCAEntites(List<Comply_Advantage_Search__c> searches)
    {

        Map<Id, Comply_Advantage_Search__c> searchMap = new Map<Id,Comply_Advantage_Search__c>(searches);
        set<Id> searchIds = searchMap.keySet();

        Map<String, Comply_Advantage_Search_Entity__c> entityByUniqueId = new Map<String, Comply_Advantage_Search_Entity__c>{};
        Map<Id, Comply_Advantage_Search_Entity__c> entitiesMap = new Map<Id, Comply_Advantage_Search_Entity__c>
        ([
            select ID,
                Comply_Advantage_Search__c ,
                Unique_ID__c, 
                Entity_Id__c,
                Match_Status__c, 
                Match_Status_After_Profile_Update__c,
                Risk_Level__c,
                Risk_Level_After_Profile_Update__c,
                Is_Whitelisted__c, 
                Is_Whitelisted_After_profile_Update__c
            From Comply_Advantage_Search_Entity__c
            Where Comply_Advantage_Search__c IN :searchIds and Is_Removed__c = false
        ]);

        //process all entities and group entities by Search and update field(match status, risk level, whitelisted)
        Map<Id, Map<string, set<String>>> matchStatus_searchIdUpdateMap = new Map<Id, Map<string, set<String>>>{};
        Map<Id, Map<string, set<String>>> riskLevel_searchIdUpdateMap = new Map<Id, Map<string, set<String>>>{};
        Map<Id, Map<boolean, set<String>>> whiteListed_searchIdUpdateMap = new Map<Id, Map<boolean, set<String>>>{};
        
        for(Comply_Advantage_Search_Entity__c entity : entitiesMap.values())
        { 
            entityByUniqueId.put(entity.Unique_ID__c, entity);

            Id complyAdvantageSearchId = entity.Comply_Advantage_Search__c;
            if(entity.Match_Status__c != entity.Match_Status_After_Profile_Update__c)
            {
                Map<string, set<String>> updateFieldsMap = matchStatus_searchIdUpdateMap.get(complyAdvantageSearchId);
            
                if(updateFieldsMap == null)
                {
                    updateFieldsMap = new Map<string, set<String>>{};
                }

                set<String> entityIds = updateFieldsMap.get(entity.Match_Status__c);
                if(entityIds == null)
                {
                    entityIds = new set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Match_Status__c, entityIds);
                matchStatus_searchIdUpdateMap.put(complyAdvantageSearchId, updateFieldsMap);
            }
            if(entity.Risk_Level__c != entity.Risk_Level_After_Profile_Update__c)
            {
                Map<string, set<String>> updateFieldsMap = riskLevel_searchIdUpdateMap.get(complyAdvantageSearchId);
                if(updateFieldsMap == null)
                {
                    updateFieldsMap = new Map<string, set<String>>{};
                }

                set<String> entityIds = updateFieldsMap.get(entity.Risk_Level__c);
                if(entityIds == null)
                {
                    entityIds = new set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Risk_Level__c, entityIds);
                riskLevel_searchIdUpdateMap.put(complyAdvantageSearchId,updateFieldsMap);
            }
            if(entity.Is_Whitelisted__c != entity.Is_Whitelisted_After_profile_Update__c)
            {
                Map<boolean, set<String>> updateFieldsMap = whiteListed_searchIdUpdateMap.get(complyAdvantageSearchId);
                if(updateFieldsMap == null)
                {
                    updateFieldsMap = new Map<boolean, set<String>>{};
                }

                set<String> entityIds = updateFieldsMap.get(entity.Is_Whitelisted__c);
                if(entityIds == null)
                {
                    entityIds = new set<String>{};
                }
                entityIds.add(entity.Entity_Id__c);

                updateFieldsMap.put(entity.Is_Whitelisted__c, entityIds);
                whiteListed_searchIdUpdateMap.put(complyAdvantageSearchId, updateFieldsMap);
            }
        }

        Map<Id, Comply_Advantage_Search_Entity__c> entitiesToUpdate = new  Map<Id, Comply_Advantage_Search_Entity__c>{};
        for(Comply_Advantage_Search__c search :searchMap.values())
        {
            Map<string, set<String>> msUpdateFieldsMap = matchStatus_searchIdUpdateMap.get(search.Id);
            Map<string, set<String>> rlUpdateFieldsMap = riskLevel_searchIdUpdateMap.get(search.Id);
            Map<boolean, set<String>> wlUpdateFieldsMap = whiteListed_searchIdUpdateMap.get(search.Id);

            ComplyAdvantageSearchRecord searcRecord = new ComplyAdvantageSearchRecord(search);

            if(msUpdateFieldsMap != null && msUpdateFieldsMap.size() > 0)
            {
                for(string matchStatus : msUpdateFieldsMap.keySet())
                {
                    try
                    {
                        set<String> entityIds = msUpdateFieldsMap.get(matchStatus);
                        updateEntitiesCallout(searcRecord, entityIds, 'matchStatus', matchStatus);
                        
                        for(string entityId : entityIds)
                        {
                            string uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);
                            
                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdate.containsKey(entity.Id) ?
                                                                                entitiesToUpdate.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Match_Status_After_Profile_Update__c = matchStatus;
                            entitiesToUpdate.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
            if(rlUpdateFieldsMap != null && rlUpdateFieldsMap.size() > 0)
            {
                for(string riskLevel : rlUpdateFieldsMap.keySet())
                {
                    try
                    {
                        set<String> entityIds = rlUpdateFieldsMap.get(riskLevel);
                        updateEntitiesCallout(searcRecord, entityIds, 'riskLevel', riskLevel);
                        
                        for(string entityId : entityIds)
                        {
                            string uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);

                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdate.containsKey(entity.Id) ?
                                                                                entitiesToUpdate.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Risk_Level_After_Profile_Update__c = riskLevel;
                            entitiesToUpdate.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
            if(wlUpdateFieldsMap != null && wlUpdateFieldsMap.size() > 0)
            {
                for(boolean whiteListed : wlUpdateFieldsMap.keySet())
                {
                    try
                    {
                        set<String> entityIds = wlUpdateFieldsMap.get(whiteListed);
                        updateEntitiesCallout(searcRecord, entityIds, 'whiteListed', whiteListed);
                        
                        for(string entityId : entityIds)
                        {
                            string uniqueId = search.Search_Id__c + ':' + entityId; 
                            Comply_Advantage_Search_Entity__c entity = entityByUniqueId.get(uniqueId);

                            Comply_Advantage_Search_Entity__c updateEntiy = entitiesToUpdate.containsKey(entity.Id) ?
                                                                                entitiesToUpdate.get(entity.Id) : 
                                                                                    new Comply_Advantage_Search_Entity__c(Id=entity.Id);

                            updateEntiy.Is_Whitelisted_After_profile_Update__c = whiteListed;
                            entitiesToUpdate.put(updateEntiy.Id, updateEntiy);
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
        }

        if(entitiesToUpdate.size() > 0)
        {
            Database.update(entitiesToUpdate.values(), false);
        }
    }

    private boolean updateEntitiesCallout(ComplyAdvantageSearchRecord searchRec,
                                          set<String> entityIdSet,
                                          string updateType,
                                          object updateValue)
    {
        try
        {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchRec.searchReference +'/entities?api_key='+ searchRec.divisionSettings.apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            String[] entityIds = new List<String>(entityIdSet);
            Map<String, Object> obj = new Map<String, Object> 
            {
                'entities' =>  entityIds
            };
            if(updateType == 'matchStatus')
            {
                string matchStatus = (string) updateValue;
                obj.put('match_status', matchStatus);
            }
            if(updateType == 'riskLevel')
            {
                string riskLevel = (string) updateValue;
                obj.put('risk_level', riskLevel);
            }
            if(updateType == 'whiteListed')
            {
                boolean isWhiteListed = (boolean) updateValue;
                obj.put('is_whitelisted', isWhiteListed);
            }

            system.debug('Request' + JSON.serialize(obj));
            string requestBody = JSON.serialize(obj);
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);
            if(response.getStatusCode() == 200)
            {
                return true;
            }
            return false;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    
    private static Map<string, string> getCountryCodes()
    {
        Map<string, string> countryNamesMap = new Map<string, string>{};
        for(Country_Setting__c countrySetting : Country_Setting__c.getAll().values()) 
        {
            String countryName = countrySetting.Long_Name__c != null ? countrySetting.Long_Name__c : countrySetting.Name;  
            countryNamesMap.put(countrySetting.ISO_Code__c.toUpperCase(), countryName);
        }
        return countryNamesMap;
	} 

    private static string getInstanceApiKeyByName(string instanceName)
    {
        return [select API_Key__c From Comply_Advantage_Instance_Setting__c where Name =:instanceName Limit 1].API_Key__c;
    }

    class SearchRerquestBody
    {
        string search_term;
        string client_ref;
        Decimal fuzziness;
        string search_profile;
        integer share_url;
        SearchFilters filters;
        Boolean exact_match;
    }
    class SearchFilters
    {
        string birth_year;
        string entity_type;
        string[] country_codes;
    }    
    class DivisionSettings
    {
        string divisionName;
        string searchProfileName;
        string secondarySearchProfile;
        decimal fuzziness;

        string apiKey;
        string oldInstanceApiKey;
        
        public DivisionSettings(Comply_Advantage_Division_Setting__c divisionSetting)
        {
            this.divisionName = divisionSetting.Name;
            this.searchProfileName = divisionSetting.Search_Profile__c;
            this.secondarySearchProfile = divisionSetting.Secondary_Search_Profile__c;
            this.oldInstanceApiKey = divisionoldAPIKeyMap.get(divisionSetting.Name);

            if(divisionSetting.Fuzziness__c != null && divisionSetting.Fuzziness__c != 0)
            {
                this.fuzziness = divisionSetting.Fuzziness__c / 100;
            }
            if(divisionSetting.Comply_Advantage_Instance_Setting__r != null)
            {
                this.apiKey = divisionSetting.Comply_Advantage_Instance_Setting__r.API_Key__c;
            }
        }
    }

    public class ComplyAdvantageSearchResponse
    {
        string newSearchReference;
        string newSearchId;
        string newMatchStatus;
        integer newTotalHits;
        string newReportLink;

        public ComplyAdvantageSearchResponse()
        {
            
        }
        public ComplyAdvantageSearchResponse(HttpResponse response)
        {
            if (response != null && response.getStatusCode() == 200) 
            {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());    
                Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
                Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');
                
                //get total_matches, share_url 
                if(responseData.get('id') != null)
                {
                    this.newSearchId = string.valueOf((integer) responseData.get('id'));
                    this.newSearchReference = (string)responseData.get('ref');
                    this.newTotalHits = (integer)responseData.get('total_matches');
                    this.newMatchStatus = (string)responseData.get('match_status');
                    this.newReportLink = (string)responseData.get('share_url');
                }
            }
        }
    }
    public class ComplyAdvantageSearchRecord
    {
        Id recordId;

        string clientReference;

        string searchReference;
        string searchId;    

        ComplyAdvantageSearchResponse newSearchResponse;

        string searchText;
        string searchParamCitizenshipCountry;
        string searchParamMailingCountry;
        string searchParamBirthYear;

        Boolean isAliasSearch;
        Boolean isSecondarySearch;

        string matchStatus;
        Decimal totalHits;
        string reportLink;
        string caPortalLink;
        
        string divisionName;

        DivisionSettings divisionSettings;

        Comply_Advantage_Search_Entity__c[] searchEntities;

        public string searchProfile
        {
            get { return isSecondarySearch ? this.divisionSettings.secondarySearchProfile : this.divisionSettings.searchProfileName;}
            set { searchProfile = value; }
        }

        public ComplyAdvantageSearchRecord(Comply_Advantage_Search__c rec)
        {
            this.recordId = rec.Id;

            this.clientReference = rec.fxAccount__r.Name;
            this.searchReference = rec.Search_Reference__c;
            this.searchId = rec.Search_Id__c;

            this.searchText = rec.Search_Text__c;
            this.searchParamCitizenshipCountry = rec.Search_Parameter_Citizenship_Nationality__c;
            this.searchParamMailingCountry = rec.Search_Parameter_Mailing_Country__c;
            this.searchParamBirthYear = rec.Search_Parameter_Birth_Year__c;
            this.isAliasSearch = rec.Is_Alias_Search__c;

            this.isSecondarySearch = false;

            this.matchStatus = rec.Match_Status__c;
            this.totalHits = rec.Total_Hits__c;
            this.reportLink = rec.Report_Link__c;
            this.caPortalLink = rec.Case_Management_Link__c;
        
            if(string.isNotBlank(rec.Custom_Division_Name__c))
            {
                this.divisionName = rec.Custom_Division_Name__c;

                Comply_Advantage_Division_Setting__c ds = ComplyAdvantageUtil.getDivisionSettingByName(divisionName,searchParamMailingCountry);
                this.divisionSettings = new DivisionSettings(ds);
            }

            this.searchEntities = new Comply_Advantage_Search_Entity__c[]{};
            if(rec.Comply_Advantage_Search_Entities__r !=null && rec.Comply_Advantage_Search_Entities__r.size() > 0)
            {
                this.searchEntities = rec.Comply_Advantage_Search_Entities__r;
            }
        }
    }
}