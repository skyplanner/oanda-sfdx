public without sharing class LiveLeadUtil {
    
    //this method should be called in Live_Lead__c before trigger
    public static void checkLiveLeadConverted(List<Live_Lead__c> liveLeads){
    	
    	set<ID> ldIDs = new Set<ID>();
    	
    	System.debug('Live Lead to process: ' + liveLeads.size());
    	
    	for(Live_Lead__c liveLead : liveLeads){
    		
    		if(liveLead.Processed__c == false && liveLead.LeadId__c != null){
    			ldIDs.add(liveLead.LeadId__c);
    		}
    		
    	}
    	
    	System.debug('number of leads to check: ' + ldIDs.size());
    	
    	List<fxAccount__c> fxAccountsToUpdate = new List<fxAccount__c>();
    	
    	Map<Id, Lead> leadByLeadId = new Map<Id, Lead>([select id, isConverted from Lead where id in :ldIDs]);
    	
    	for(Live_Lead__c liveLead : liveLeads){
    		
    		//If this record is already processed, then don't process it again
    		if(liveLead.Processed__c){
    			continue;
    		}
    		
    		//check if the lead is converted
    		Lead ld = leadByLeadId.get(liveLead.LeadId__c);
    		if(ld == null|| ld.isConverted == true){
    			if(ld != null){
    				System.debug('Lead is converted: ' + ld.Id);
    			}
    			
    			
    			//by default, Lead_Exists__c is ture
    			liveLead.Lead_Exists__c = false;
    			
    			//If lead is converted, we should re-sync the fxAccount, so that the account opportunity can be updated
    			if(liveLead.fxAccountId__c != null){
    				fxAccount__c fxa = new fxAccount__c(id = liveLead.fxAccountId__c, BI_Sync__c = true);
    				fxAccountsToUpdate.add(fxa);
    			}
    			
    		}
    		
    		//indicate this record is processed
    		liveLead.Processed__c = true;
    		
    	}
    	
    	
    	//update fxAccounts
    	System.debug('Number of fxAccounts to update: ' + fxAccountsToUpdate.size());
    	if(fxAccountsToUpdate.size() > 0){
    		Integer errors = 0;
    		
    		Database.SaveResult[] srList = Database.update(fxAccountsToUpdate, false);
    		for(Database.SaveResult sr : srList){
    			if (!sr.isSuccess()) {
    				errors++;
    			}
    		}
    		
    		System.debug('Errors to update fxAccounts: ' + errors);
    	}
    	
    	System.debug('checkLiveLeadConverted ends');
    }
}