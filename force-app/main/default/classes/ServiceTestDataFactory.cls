/**
 * @File Name          : ServiceTestDataFactory.cls
 * @Description        :
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/14/2024, 12:03:32 PM
 **/
@IsTest
public without sharing class ServiceTestDataFactory {
	public static void createLead(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Lead
		);
		String uniqueName = 'Lead' + initProxy.getUniqueIndex();
		Lead leadObj = new Lead(
			FirstName = uniqueName,
			LastName = uniqueName,
			Email = uniqueName.toLowerCase() + '@a.com',
			Language_Preference__c = OmnichanelConst.ENGLISH_LANG,
			Division_Name__c = OmnichanelConst.OANDA_CANADA
		);
		initProxy.insertObj(leadObj);
	}

	public static void createAccount(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Account
		);
		String uniqueName = 'Account' + initProxy.getUniqueIndex();
		Account accountObj = new Account(
			FirstName = uniqueName,
			LastName = uniqueName,
			PersonEmail = uniqueName.toLowerCase() + '@a.com',
			Account_Status__c = 'Active',
			Language_Preference__pc = OmnichanelConst.ENGLISH_LANG
		);
		initProxy.insertObj(accountObj);
	}

	public static void createFxAccount(
		SPDataInitManager initManager,
		String objKey,
		String accountObjKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.fxAccount__c
		);
		ID accountId = initManager.getObjectId(accountObjKey, true);
		Account accountObj = [
			SELECT Name
			FROM Account
			WHERE Id = :accountId
		];
		fxAccount__c fxAccountObj = new fxAccount__c(
			Name = accountObj.Name,
			Account__c = accountObj.Id,
			RecordTypeId = initProxy.getRecordTypeId('Retail_Live'),
			Division_Name__c = OmnichanelConst.OANDA_CANADA
		);
		initProxy.insertObj(fxAccountObj);
	}

	public static void createCase(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Case
		);
		Case caseObj = new Case(
			// AccountId = ...value can be set in initManager
			// Lead__c = ...value can be set in initManager
			Subject = initProxy.getUniqueName()
		);
		initProxy.insertObj(caseObj);
	}

	public static void createGroup(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Group
		);
		Group groupObj = new Group(
			Name = 'queue Name',
			DeveloperName = 'queue_Name',
			type = 'Queue'
		);
		initProxy.insertObj(groupObj);
	}

	public static void createUser(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.User
		);
		String uniqueName = 'spsuperfaketestuser' + initProxy.getUniqueIndex();

		User userObj = new User(
			Alias = 'testUser',
			Email = uniqueName + '@a.com',
			LastName = uniqueName,
			EmailEncodingKey = 'UTF-8', 
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US', 
			TimeZoneSidKey = 'America/Los_Angeles',
			ProfileId = UserInfo.getProfileId(),
			UserName = uniqueName + '@a.com'
		);
		initProxy.insertObj(userObj);
	}

	public static void createEmailTemplate(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.EmailTemplate
		);
		EmailTemplate emailTemplateObj = new EmailTemplate(
			DeveloperName = 'Case_Authentication_Code_Spanish', 
			Name = 'Case: Authentication Code - Spanish',
			Description = 'Language="Spanish"',
			TemplateType = 'text',
			FolderId = UserInfo.getUserId(),
			isActive = true
		);
		initProxy.insertObj(emailTemplateObj);
	}

	public static void createSettings(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Settings__c
		);
		Settings__c settingsObj = new Settings__c(
			name = 'Default',
			Auth_code_OrgWideEmail__c = 'test@emailServer.com'
		);
		initProxy.insertObj(settingsObj);
	}

	public static void createSegmentation(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Segmentation__c
		);
		ID fxAccountId = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Segmentation__c segmentationObj = new Segmentation__c(
			fxAccount__c = fxAccountId,
			Seg_PL__c = OmnichannelCustomerConst.SEGMENTATION_INACTIVE,
			Active__c = true
		);
		initProxy.insertObj(segmentationObj);
	}

	public static void createAgentExtraInfo(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Agent_Extra_Info__c
		);
		ID userId = initManager.getObjectId(
			ServiceTestDataKeys.USER_1,
			true
		);
		Agent_Extra_Info__c agentExtraInfo = new Agent_Extra_Info__c(
			Agent_ID__c = userId,
			Non_HVC_Cases__c = 3
		);
		initProxy.insertObj(agentExtraInfo);
	}

	public static void createAccount1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.ACCOUNT_1,
			Schema.SObjectType.Account
		);
		initProxy.setFieldValue(
			Account.PersonEmail,
			ServiceTestDataKeys.ACCOUNT1_EMAIL
		);
		createAccount(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
		createFxAccount(
			initManager,
			ServiceTestDataKeys.FX_ACCOUNT_1, // objKey
			initProxy.getObjectKey() // accountObjKey
		);
	}

	public static void createLead1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.LEAD_1,
			Schema.SObjectType.Lead
		);
		initProxy.setFieldValue(Lead.Email, ServiceTestDataKeys.LEAD1_EMAIL);
		createLead(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}

	// requires ACCOUNT_1
	public static void createCase1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.CASE_1,
			Schema.SObjectType.Case
		);
		ID accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		initProxy.setFieldValue(Case.AccountId, accountId);
		createCase(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}

	// requires LEAD_1
	public static void createLeadCase1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.LEAD_CASE_1,
			Schema.SObjectType.Case
		);
		ID lead1Id = initManager.getObjectId(
			ServiceTestDataKeys.LEAD_1,
			true
		);
		initProxy.setFieldValue(Case.Lead__c, lead1Id);
		createCase(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}

	public static void createCaseQueue1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.QUEUE_CASE_1,
			Schema.SObjectType.Group
		);

		initProxy.setFieldValue(
			Group.Name,
			MessagingConst.CASE_QUEUE_DEV_NAME_SETTING
		);
		initProxy.setFieldValue(
			Group.DeveloperName,
			MessagingConst.CASE_QUEUE_DEV_NAME_SETTING
		);
		initProxy.setFieldValue(Group.type, 'Queue');
		createGroup(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
		ID queueId = initManager.getObjectId(
			ServiceTestDataKeys.QUEUE_CASE_1,
			true
		);
	}

	public static void createUser1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.USER_1,
			Schema.SObjectType.User
		);
		createUser(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}

	public static void createEmailTemplate1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.SETTINGS_1,
			Schema.SObjectType.EmailTemplate
		);
		createEmailTemplate(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}
	public static void createSettings1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.SETTINGS_1,
			Schema.SObjectType.Settings__c
		);
		createSettings(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}

	public static void createLiveChatVisitor(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.LiveChatVisitor
		);
		LiveChatVisitor chatVisitor = new LiveChatVisitor();
		initProxy.insertObj(chatVisitor);
	}

	public static void createLiveChatVisitor1(SPDataInitManager initManager) {
		createLiveChatVisitor(
			initManager, // initManager
			ServiceTestDataKeys.LIVE_CHAT_VISITOR_1 // objKey
		);
	}

	// default values requires ACCOUNT_1, LIVE_CHAT_VISITOR_1
	public static void createLiveChatTranscript(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.LiveChatTranscript
		);
		ID chatVisitor1Id = initManager.getObjectId(
			ServiceTestDataKeys.LIVE_CHAT_VISITOR_1,
			true
		);
		LiveChatTranscript chat = new LiveChatTranscript(
			ChatKey = ServiceTestHelper.getChatKey('A'),
			Chat_Email__c = ServiceTestDataKeys.ACCOUNT1_EMAIL,
			Chat_Type_of_Account__c = OmnichanelConst.PRACTICE,
			Chat_Language_Preference__c = OmnichanelConst.ENGLISH_LANG_CODE,
			Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_LOGIN,
			Type__c = null,
			Subtype__c = null,
			LiveChatVisitorId = chatVisitor1Id
		);
		initProxy.insertObj(chat);
	}

	// requires ACCOUNT_1, LIVE_CHAT_VISITOR_1
	public static void createLiveChatTranscript1(
		SPDataInitManager initManager
	) {
		createLiveChatTranscript(
			initManager, // initManager
			ServiceTestDataKeys.LIVE_CHAT_TRANSCRIPT_1 // objKey
		);
	}

	// default values requires CASE_1
	public static void createNonHVCCase(
		SPDataInitManager initManager,
		String objKey
	) {
		SPDataInitProxy initProxy = initManager.getProxy(
			objKey,
			Schema.SObjectType.Non_HVC_Case__c
		);
		ID case1Id = initManager.getObjectId(
			ServiceTestDataKeys.CASE_1,
			true
		);
		Non_HVC_Case__c nonHVCCase = new Non_HVC_Case__c(
			Case__c = case1Id
		);
		initProxy.insertObj(nonHVCCase);
	}

	// requires CASE_1
	public static void createNonHVCCase1(
		SPDataInitManager initManager
	) {
		createNonHVCCase(
			initManager, // initManager
			ServiceTestDataKeys.NON_HVC_CASE_1 // objKey
		);
	}

	// requires CASE_1
	public static void createNonHVCCase2(
		SPDataInitManager initManager
	) {
		createNonHVCCase(
			initManager, // initManager
			ServiceTestDataKeys.NON_HVC_CASE_2 // objKey
		);
	}

	// requires CASE_1
	public static void createNonHVCCase3(
		SPDataInitManager initManager
	) {
		createNonHVCCase(
			initManager, // initManager
			ServiceTestDataKeys.NON_HVC_CASE_3 // objKey
		);
	}
	
	public static void createSegmentation1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.SEGMENTATION_1,
			Schema.SObjectType.Segmentation__c
		);
		createSegmentation(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}
	public static void createAgentExtraInfo1(SPDataInitManager initManager) {
		SPDataInitProxy initProxy = initManager.getProxy(
			ServiceTestDataKeys.AGENT_EXTRA_INFO_1,
			Schema.SObjectType.Agent_Extra_Info__c
		);
		createAgentExtraInfo(
			initManager, // initManager
			initProxy.getObjectKey() // objKey
		);
	}
}