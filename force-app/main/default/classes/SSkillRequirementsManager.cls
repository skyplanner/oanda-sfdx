/**
 * @File Name          : SSkillRequirementsManager.cls
 * @Description        : 
 * Returns the list of SkillRequirements corresponding to the 
 * routing information records
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 2:09:59 AM
**/
public interface SSkillRequirementsManager {

	List<SkillRequirement> getSkillRequirements(
		List<BaseServiceRoutingInfo> infoList
	);

}