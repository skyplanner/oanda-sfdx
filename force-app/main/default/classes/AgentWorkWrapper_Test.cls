/**
 * @File Name          : AgentWorkWrapper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/20/2021, 4:37:00 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/9/2020   acantero     Initial Version
**/
@istest
private class AgentWorkWrapper_Test {

    static final String FAKE_WORK_ID = '0Bz3C0000000CvkSAE';

    @istest
    static void AgentWorkWrapper_test1() {
        Test.startTest();
        AgentWorkWrapper w1 = new AgentWorkWrapper(null);
        String skillsCodes1 = w1.getSkillsSummary();
        //...
        AgentWorkWrapper w2 = new AgentWorkWrapper(FAKE_WORK_ID);
        String skillsCodes2 = w2.getSkillsSummary();
        Test.stopTest();
        System.assertEquals(null, skillsCodes1);
        System.assertEquals('', skillsCodes2);
    }
}