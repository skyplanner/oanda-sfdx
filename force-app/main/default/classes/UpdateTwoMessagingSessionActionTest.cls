/**
 * @File Name          : UpdateTwoMessagingSessionActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:14:54 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 12:58:55 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class UpdateTwoMessagingSessionActionTest {

	@testSetup
	private static void testSetup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		System.runAs(new User(Id = UserInfo.getUserId())) {
			ServiceTestDataFactory.createEmailTemplate1(initManager);
		}

		initManager.storeData();
	}
	
	@isTest
	private static void testUpdateInfoClass() {
		// Test data setup

		// Actual test
		Test.startTest();
		UpdateTwoMessagingSessionFieldsAction.UpdateInfo info = new UpdateTwoMessagingSessionFieldsAction.UpdateInfo();
		info.messagingSessionId = null;
		info.fieldName1 = 'fieldName1';
		info.fieldValue1 = 'fieldValue1';
		info.fieldName2 = 'fieldName2';
		info.fieldValue2 = 'fieldValue2';

		Assert.areEqual(
			info.messagingSessionId,
			info.getRecordId(),
			'Record Id should be the same as the messaging session Id'
		);

		Map<String, Object> fields = info.getFieldValueByFieldNames();
		Assert.areEqual(
			info.fieldValue1,
			fields.get(info.fieldName1),
			'Field 1 value should be the same'
		);
		Assert.areEqual(
			info.fieldValue2,
			fields.get(info.fieldName2),
			'Field 2 value should be the same'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testSetTwoMessagingSessionFields() {
		// Test data setup
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		try {
			UpdateTwoMessagingSessionFieldsAction.setTwoMessagingSessionFields(
				new List<UpdateTwoMessagingSessionFieldsAction.UpdateInfo>()
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			exceptionThrown,
			'Exception should not be thrown'
		);
	}

	@isTest
	private static void testSetTwoMessagingSessionFields1() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		Boolean exceptionThrown = false;
		UpdateTwoMessagingSessionFieldsAction.UpdateInfo info = new UpdateTwoMessagingSessionFieldsAction.UpdateInfo();
		info.messagingSessionId = null;
		info.fieldName1 = 'CaseId';
		info.fieldValue1 = info.fieldValue1 = initManager.getObjectId(
			ServiceTestDataKeys.CASE_1,
			true
		);
		info.fieldName2 = 'Case_Type__c';
		info.fieldValue2 = 'Minimum deposit questions';
		// Actual test
		Test.startTest();
		try {
			UpdateTwoMessagingSessionFieldsAction.setTwoMessagingSessionFields(
				new List<UpdateTwoMessagingSessionFieldsAction.UpdateInfo>{
					info
				}
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			true,
			exceptionThrown != null,
			'Exception messagingSessionId does not exist should be thrown'
		);
	}
}