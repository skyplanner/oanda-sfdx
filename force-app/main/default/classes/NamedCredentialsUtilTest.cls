@IsTest
private class NamedCredentialsUtilTest {

    final static User SYSTEM_USER = UserUtil.getSystemUserWithPermissionForTest('Test SAUser' , 'test.sauser@tes.test', NamedCredentialsUtil.NAMED_CREDENTIAL_PERMISSION_SET);

    @IsTest(SeeAllData=true)
    static void getOrgDefinedNamedCredentialPositiveTest() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        System.runAs(SYSTEM_USER) {
            Test.startTest();
                    NamedCredentialsUtil.isTestUseSeeAllData = true;
                    NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
                    Assert.areNotEqual(null, namedCU.credentialUrl);
                    Assert.isTrue(namedCU.credentialName.containsIgnoreCase(Constants.USER_API_NAMED_CREDENTIALS_NAME));
            Test.stopTest();
        }
    }

    @IsTest(SeeAllData=true)
    static void getOrgDefinedNamedCredentialNegativeTest1() {
        Test.startTest();
            try {
                NamedCredentialsUtil.isTestUseSeeAllData = true;
                NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential('Wrong_Named_Credentials', Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
            } catch (NamedCredentialsUtil.MissingCredentialException ex) {
                Assert.areNotEqual(null, ex.getMessage());
            }
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void getOrgDefinedNamedCredentialNegativeTest2() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 404, 'Error', 'Error', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        System.runAs(SYSTEM_USER) {
            Test.startTest();
                try {
                    NamedCredentialsUtil.isTestUseSeeAllData = true;
                    NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
                } catch (NamedCredentialsUtil.MissingCredentialException ex) {
                    Assert.areNotEqual(null, ex.getMessage());
                }
            Test.stopTest();
        }
    }
}