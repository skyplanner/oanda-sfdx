/**
 * @File Name          : PhoneSLAViolationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/6/2024, 1:07:40 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/21/2021, 10:46:35 AM   acantero     Initial Version
**/
public without sharing class PhoneSLAViolationManager {

    public static final Integer MILESTONE_INVALID_PERIOD = -1;

    @testVisible
    static Integer phoneCaseBouncedMilestonePeriod;

    final List<Case> caseList = new List<Case>();
    final Map<Id,Case> oldMap;
    private Map<String,String> milestoneTypeNames;
    private Map<String,Milestone_Time_Settings__mdt> milestonesByTier;

    public PhoneSLAViolationManager(
        Map<Id,Case> oldMap
    ) {
        this.oldMap = oldMap;
        milestoneTypeNames = new Map<String,String>();
        milestonesByTier = new  Map<String,Milestone_Time_Settings__mdt>();
    }

    public void checkRecord(
        Case rec, 
        Case oldRec
    ) {
        if (
            (rec.OwnerId != oldRec.OwnerId) &&
            (rec.Origin == OmnichanelConst.CASE_ORIGIN_PHONE) &&
            (String.isNotBlank(rec.Tier__c)) &&
            (rec.isClosed == false)
        ) {
            caseList.add(rec);
            if(!milestoneTypeNames.containsKey(rec.Tier__c)){
                String milestoneName = getSettingNameByTier(rec.Tier__c);
                milestoneTypeNames.put(rec.Tier__c, milestoneName);
            }
        }
    }
    
    public void processHVCPhoneCaseBounces() {
        if (caseList.isEmpty()) {
            return;
        }
        getMilestoneSettings();
        //else...
        List<Case> hvcPhoneCaseBouncedList = getPhoneCaseBouncedList();
        for(Case caseObj : hvcPhoneCaseBouncedList) {
            caseObj.Milestone_Violated__c = milestoneTypeNames.get(caseObj.Tier__c);
            caseObj.Milestone_Violation_Time__c = System.now();
        }
    }

    public Integer getPhoneCaseBouncedMilestonePeriod(String tier) {
        Milestone_Time_Settings__mdt setting =  milestonesByTier.get(tier);
        Integer timePeriod = MILESTONE_INVALID_PERIOD;
        if(setting != null && setting.Time_Trigger__c != null){
            timePeriod = (setting.Time_Trigger__c * 60).intValue();
        }
        return timePeriod;
    }

    @testVisible
    List<Case> getPhoneCaseBouncedList() {
        List<Case> hvcPhoneCaseList = new List<Case>();
        Set<String> ownerIdSet = new Set<String>();
        Long nowTime = System.now().getTime();
        //else...
        for(Case caseObj : caseList) {
            Integer milestonePeriodSeg = getPhoneCaseBouncedMilestonePeriod(
                caseObj.Tier__c
            );
            Double milestonePeriod = milestonePeriodSeg/ (double)60;
            if (milestonePeriod != MILESTONE_INVALID_PERIOD) {
                Long createdDateTime = caseObj.CreatedDate.getTime();
                Long milliseconds = nowTime - createdDateTime;
                Double differenceInMinutes = milliseconds / (Double)60000;
                Case oldCase = oldMap.get(caseObj.Id);
                if (
                    (differenceInMinutes >= milestonePeriod) &&
                    String.isNotBlank(caseObj.OwnerId) &&
                    String.isNotBlank(oldCase.OwnerId) &&
                    (caseObj.OwnerId != oldCase.OwnerId) &&
                    (SPQueueUtil.ownerIsAQueue(caseObj.OwnerId) == false) &&
                    (SPQueueUtil.ownerIsAQueue(oldCase.OwnerId) == false)
                ) {
                    hvcPhoneCaseList.add(caseObj);
                    ownerIdSet.add(caseObj.OwnerId);
                    ownerIdSet.add(oldCase.OwnerId);
                }
            }
        }
        List<Case> result = getPhoneCaseBouncedList(
            hvcPhoneCaseList,
            ownerIdSet
        );
        return result;
    }

    @testVisible
    List<Case> getPhoneCaseBouncedList(
        List<Case> hvcPhoneCaseList,
        Set<String> ownerIdSet
    ) {
        List<Case> result = new List<Case>();
        if (
            (hvcPhoneCaseList == null) ||
            hvcPhoneCaseList.isEmpty()
        ) {
            return result;
        }
        //else...
        if (!hvcPhoneCaseList.isEmpty()) {
            Set<String> agentsUserIdSet = 
                ServiceResourceHelper.getAgentsUserIdSet(ownerIdSet);
            for(Case caseObj : hvcPhoneCaseList) {
                Case oldCase = oldMap.get(caseObj.Id);
                if (
                    agentsUserIdSet.contains(caseObj.OwnerId) &&
                    agentsUserIdSet.contains(oldCase.OwnerId)
                ) {
                    result.add(caseObj);
                }
            }
        }
        return result;

    }

    private String getSettingNameByTier(String tier){
        String settingNameFormat = 'Phone {0} Case - Bounced';
        return String.format(settingNameFormat, new List<String>{tier});
    }

    private void getMilestoneSettings(){
        Set<String> tierSet = new Set<String>();
        for (String key : milestoneTypeNames.keySet()) {
            String milestoneName = milestoneTypeNames.get(key);
            tierSet.add(milestoneName);
        }
        Map<String,Milestone_Time_Settings__mdt> settingsByName =  MilestoneUtils.getMilestonesSettings(tierSet);
        for (String key : milestoneTypeNames.keySet()) {
            String milestoneName = milestoneTypeNames.get(key);
            milestonesByTier.put(key, settingsByName.get(milestoneName));
        }
    }


}