/**
 * @File Name          : ChatSLAViolationNotificationData.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/23/2024, 1:39:44 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2024, 1:38:18 PM   aniubo     Initial Version
 **/
public with sharing class ChatSLAViolationNotificationData extends SLAViolationNotificationData {
	public List<Id> recordIds { get; set; }
	public ChatSLAViolationNotificationData(List<Id> recordIds) {
		this.recordIds = recordIds;
	}
}