public with sharing class BatchEntityContactsComplyAdvantageSearch  implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful
{
    public Entity_Contact__c entityContact;
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchEntityContactsComplyAdvantageSearch() 
    {

    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'select Id, First_Name__c,Last_Name__c,Citizenship__c,Client_Mailing_Country__c,Birthdate__c, Account__c,Trigger_Comply_Advantage_Search__c From Entity_Contact__c  WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
    public static void executeBatch() 
    {
        Database.executeBatch(new BatchEntityContactsComplyAdvantageSearch(), 1);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
       string query = 'select Id, First_Name__c,Last_Name__c,Citizenship__c,Client_Mailing_Country__c,Birthdate__c, Account__c,Trigger_Comply_Advantage_Search__c'+
                      ' From Entity_Contact__c ' +
                      ' where Trigger_Comply_Advantage_Search__c = true';
        
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<Entity_Contact__c> scope) 
    {
        entityContact = scope[0];
        ComplyAdvantageHelper.validate(null, entityContact);
	}   
    public void finish(Database.BatchableContext bc) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if(entityContact != null)
        {
            entityContact.Trigger_Comply_Advantage_Search__c = false;
            update entityContact;
        }
	}
}