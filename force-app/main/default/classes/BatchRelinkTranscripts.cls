public with sharing class BatchRelinkTranscripts implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private boolean doUpdate = false;
	private static Integer numberFound = 0;
	private static Integer numberUpdated = 0;
	
	public BatchRelinkTranscripts() {
		query = 'SELECT Id, LiveChatVisitorId, CaseId, LeadId, AccountId, ContactId FROM LiveChatTranscript WHERE CaseId=null OR (LeadId=null AND AccountId=null)';
	}
	
	public BatchRelinkTranscripts(boolean doUpdate) {
		query = 'SELECT Id, LiveChatVisitorId, CaseId, LeadId, AccountId, ContactId FROM LiveChatTranscript WHERE CaseId=null OR (LeadId=null AND AccountId=null)';
		this.doUpdate = doUpdate;
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, LiveChatVisitorId, CaseId, LeadId, AccountId, ContactId FROM LiveChatTranscript WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
		numberFound = 0;
		numberUpdated = 0;
    	return Database.getQueryLocator(query);
	}
	
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		System.debug('numberFound: ' + numberFound);
		System.debug('numberUpdated: ' + numberUpdated);
		numberFound += batch.size();
		System.debug('batch.size(): ' + batch.size());
		List<LiveChatTranscript> transcriptsToUpdate = LiveChatTranscriptUtil.linkTranscriptViaVisitor(batch);
		
		numberUpdated += transcriptsToUpdate.size();
		System.debug('numberFound: ' + numberFound);
		System.debug('numberUpdated: ' + numberUpdated);
		System.debug('transcriptsToUpdate.size(): ' + transcriptsToUpdate.size());
		if(doUpdate) {
			update transcriptsToUpdate;
		}
	}

	public void finish(Database.BatchableContext bc) {
		System.debug('numberFound: ' + numberFound);
		System.debug('numberUpdated: ' + numberUpdated);
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

	//usage: BatchRelinkTranscripts.executeBatch();
	public static Id executeBatch() {
		return Database.executeBatch(new BatchRelinkTranscripts(), 20);
	}
	
	// usage: BatchRelinkTranscripts.executeBatch(false);
	public static Id executeBatch(boolean doUpdate) {
		return Database.executeBatch(new BatchRelinkTranscripts(doUpdate), 20);
	}
	
}