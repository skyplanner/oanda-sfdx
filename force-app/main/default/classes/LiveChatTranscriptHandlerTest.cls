/**
 * Test class for the LiveChatTranscriptHandler
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   28/04/2021
 */
@isTest
private class LiveChatTranscriptHandlerTest {

    @isTest static void test01(){

        Case c = new Case();
        insert c;

        Test.startTest();

            LiveChatVisitor v = new LiveChatVisitor();
            insert v;

            LiveChatTranscript lct = new LiveChatTranscript();
            lct.LiveChatVisitorId = v.Id;
            lct.CaseId = c.Id;
            insert lct;

        Test.stopTest();

        c = [SELECT LiveChatTranscript_Count__c FROM Case WHERE Id =: c.Id];

        System.assertEquals(
            1, 
            c.LiveChatTranscript_Count__c, 
            'The transcripts count are not correct'
        );

        LiveChatTranscript lct2 = new LiveChatTranscript();
        lct2.LiveChatVisitorId = v.Id;
        lct2.CaseId = c.Id;
        insert lct2;

        c = [SELECT LiveChatTranscript_Count__c FROM Case WHERE Id =: c.Id];

        System.assertEquals(
            2, 
            c.LiveChatTranscript_Count__c, 
            'The transcripts count are not correct'
        );

        /* Testing delete event */
        delete lct;
        c = [SELECT LiveChatTranscript_Count__c FROM Case WHERE Id =: c.Id];
        System.assertEquals(
            1, 
            c.LiveChatTranscript_Count__c, 
            'The transcripts count are not correct'
        );
    }

    @isTest static void test01Bulk(){

        Integer CASE_COUNT = 50;

        List<Case> caseList = new List<Case>();
        for(Integer i = 0; i < CASE_COUNT; i++){
            caseList.add(
                new Case()
            );
        }
        insert caseList;

        Test.startTest();

            LiveChatVisitor v = new LiveChatVisitor();
            insert v;
            Integer LCT_COUNT = 1;

            List<LiveChatTranscript> lctList = new List<LiveChatTranscript>();
            for(Case c : caseList){
                for(Integer i = 0; i < LCT_COUNT; i++){
                    lctList.add(
                        new LiveChatTranscript(
                            LiveChatVisitorId = v.Id,
                            CaseId = c.Id
                        )
                    );
                }
            }
            insert lctList;

        Test.stopTest();

        caseList = [SELECT LiveChatTranscript_Count__c FROM Case];
        System.assertEquals(CASE_COUNT, caseList.size());

        for(Case c : caseList){
            System.assertEquals(
                LCT_COUNT, 
                c.LiveChatTranscript_Count__c, 
                'The transcripts count are not correct'
            );
        }

        /* Testing delete event */
        delete lctList;
        caseList = [SELECT LiveChatTranscript_Count__c FROM Case];
        System.assertEquals(CASE_COUNT, caseList.size());

        for(Case c : caseList){
            System.assertEquals(
                0, 
                c.LiveChatTranscript_Count__c, 
                'The transcripts count are not correct'
            );
        }
    }

    @isTest static void createActivityIfConvRSUserTestNoUser() {

        LiveChatVisitor v = new LiveChatVisitor();
        insert v;
        LiveChatTranscript lct = new LiveChatTranscript(LiveChatVisitorId = v.Id);

        Test.startTest();
        insert lct;
        Test.stopTest();

        List<Task> tasks = [SELECT Id FROM Task WHERE Subject = :LiveChatTranscriptHandler.CONV_RS_TASK_SUBJECT];

        System.assertEquals(0, tasks.size());
    }
    @isTest static void createActivityIfConvRSUserTestDIfferentUser() {

        Profile integrationProfile = [SELECT Id FROM Profile WHERE Name = :LiveChatTranscriptHandler.CONV_RS_USER_PROFILE LIMIT 1];

        User convRSUser = new User(
            FirstName = 'testUser',
            LastName = 'testLast',
            ProfileId = integrationProfile.Id,
            Alias = LiveChatTranscriptHandler.CONV_RS_ALIAS,
            LanguageLocaleKey = 'en_US',
            Email = 'testmyuser@oanda.com',
            EmailEncodingKey = 'UTF-8',
            isActive = true,
            LocaleSIDKey ='en_US',
            TimeZoneSidKey = 'America/New_York',
            Username = 'convrs1@user.com'
        );

        insert convRSUser;

        LiveChatVisitor v = new LiveChatVisitor();
        insert v;
        LiveChatTranscript lct = new LiveChatTranscript(LiveChatVisitorId = v.Id);

        Test.startTest();
        insert lct;
        Test.stopTest();

        List<Task> tasks = [SELECT Id FROM Task WHERE Subject = :LiveChatTranscriptHandler.CONV_RS_TASK_SUBJECT];

        System.assertEquals(0, tasks.size());
    }

    @isTest static void createActivityIfConvRSUserTestPositive() {

        Profile integrationProfile = [SELECT Id FROM Profile WHERE Name = :LiveChatTranscriptHandler.CONV_RS_USER_PROFILE LIMIT 1];

        User convRSUser = new User(
            FirstName = 'testUser',
            LastName = 'testLast',
            ProfileId = integrationProfile.Id,
            Alias = LiveChatTranscriptHandler.CONV_RS_ALIAS,
            LanguageLocaleKey = 'en_US',
            Email = 'testmyuser@oanda.com',
            EmailEncodingKey = 'UTF-8',
            isActive = true,
            LocaleSIDKey ='en_US',
            TimeZoneSidKey = 'America/New_York',
            Username = 'convrs2@user.com'
        );

        insert convRSUser;
        LiveChatVisitor v = new LiveChatVisitor();
        insert v;

        TestDataFactory tdf = new TestDataFactory();
        Account a = tdf.createTestAccount();

        Opportunity o = tdf.createOpportunity(a);

        fxAccount__c fxA = tdf.createFXTradeAccount(a);
        fxA.Opportunity__c = o.Id;
        update fxA;

        a.fxAccount__c = fxA.Id;
        update a;
        
        List<LiveChatTranscript> transcriptTestRecords = new List<LiveChatTranscript> {
            new LiveChatTranscript(OwnerId = convRSUser.Id, LiveChatVisitorId = v.Id, AccountId = a.Id),
            new LiveChatTranscript(OwnerId = convRSUser.Id, LiveChatVisitorId = v.Id, AccountId = a.Id)
        };

        Test.startTest();
        insert transcriptTestRecords;
        Test.stopTest();

        List<Task> tasks = [SELECT Id, AccountId, WhatId FROM Task WHERE Subject = :LiveChatTranscriptHandler.CONV_RS_TASK_SUBJECT];

        System.assertEquals(2, tasks.size());
        System.assertEquals(a.Id, tasks[0].AccountId);
        System.assertEquals(o.Id, tasks[0].WhatId);
    }
}