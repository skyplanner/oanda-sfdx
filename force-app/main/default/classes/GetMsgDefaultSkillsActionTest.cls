/**
 * @File Name          : GetMsgDefaultSkillsActionTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/19/2024, 9:04:07 PM
**/
@IsTest
private without sharing class GetMsgDefaultSkillsActionTest {

	@IsTest
	static void getMsgDefaultSkills() {
		Test.startTest();
		List<List<ID>> result = GetMsgDefaultSkillsAction.getMsgDefaultSkills();
		Test.stopTest();
		
		Assert.isFalse(
			result[0].isEmpty(), 
			'The method should return at least one skill'
		);
	}
	
}