@isTest
public without sharing class LeadApiRestResourceTest {

    private static string restUrl = '/services/apexrest/api/v1/lead';
    private static string postMethod = 'POST';

    @TestSetup
    static void makeData(){
        Lead l1 = new Lead(LastName='test1', Email='test1lead@example.org');
        Lead l2 = new Lead(LastName='test2', Email='test2lead@example.org', RecordTypeId=RecordTypeUtil.getLeadRetailId());
        insert new Lead[] {l1, l2};
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(l2.Id);
        lc.setDoNotCreateOpportunity(true); 
        lc.setConvertedStatus('Hot');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
    }

   

    @isTest
    static void insertNewLead() {
        RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();

        req.requestBody = Blob.valueOf(JSON.serialize(getInputforEmail('test3lead@example.org', 'lastNameTest')));
	
		RestContext.request = req;
    	RestContext.response = res;
        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);
        LeadApiRestResource.doPost();
        Test.stopTest();
    
        Assert.areEqual(200, res.statusCode, 'lead is inserted');
        Assert.areEqual(3, [SELECT count() from LEad]);
    }

    @isTest
    static void updateExistingLead() {
        RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();

        req.requestBody = Blob.valueOf(JSON.serialize(getInputforEmail('test1lead@example.org', 'lastNameTest')));
	
		RestContext.request = req;
    	RestContext.response = res;
        Test.startTest();
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);
        LeadApiRestResource.doPost();
        Test.stopTest();
    
        Assert.areEqual(200, res.statusCode, 'lead is upserted');
        Assert.areEqual(2, [SELECT count() from Lead]);
        Assert.areEqual('lastNameTest', [SELECT LastName from Lead WHERE Email = 'test1lead@example.org'].LastName);
    }


    @isTest
    static void updateExistingConvertedLead() {
        RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();

        req.requestBody = Blob.valueOf(JSON.serialize(getInputforEmail('test2lead@example.org', 'lastNameTest')));

		RestContext.request = req;
    	RestContext.response = res;
        Test.startTest();
        Assert.areEqual(true, [SELECT IsConverted from Lead WHERE Email = 'test2lead@example.org'].IsConverted);
        CalloutMock mock = new CalloutMock(
			200,
			'{"language": null, "email": "dummy0@dummy.com", "uuid": "aabb713d-2d70-11ef-951a-42010a7801d3"}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);
        LeadApiRestResource.doPost();
        Test.stopTest();
    
        Assert.areEqual(400, res.statusCode, 'lead is not upserted');

    }
    
    @isTest
    static void insertLeadNoEmail() {
        RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();

        req.requestBody = Blob.valueOf(JSON.serialize(getInputforEmail(null, 'lastNameTest')));

		RestContext.request = req;
    	RestContext.response = res;
        Test.startTest();
        LeadApiRestResource.doPost();
        Test.stopTest();
    
        Assert.areEqual(400, res.statusCode, 'lead is not upserted');

    }

    @isTest
    static void insertLeadNolastName() {
        RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();

        req.requestBody = Blob.valueOf(JSON.serialize(getInputforEmail('test1lead@example.org', null)));

		RestContext.request = req;
    	RestContext.response = res;
        Test.startTest();

        LeadApiRestResource.doPost();
        Test.stopTest();
    
        Assert.areEqual(400, res.statusCode, 'lead is not upserted');

    }

    private static LeadApiRestResource.InputClass getInputforEmail(String email, String lName){
        LeadApiRestResource.InputClass input = new LeadApiRestResource.InputClass();
        input.email = email;
        input.last_name = lName;
        return input;
    }
        
}