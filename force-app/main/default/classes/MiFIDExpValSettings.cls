/**
 * @File Name          : MiFIDExpValSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/27/2020, 12:51:14 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/14/2020   acantero     Initial Version
**/
public class MiFIDExpValSettings {

    public static final String DEFAULT_SETTINGS = 'Default_Settings';

    private static MiFIDExpValSettings instance;
    private String key;

    public Boolean initializationFails {get; private set;}

    @TestVisible
    public Boolean isActive {get;private set;}
    @TestVisible
    public Integer maxLength {get;private set;}
    @TestVisible
    public Integer minLength {get; private set;}
    @TestVisible
    public Integer minInvalidNumber {get; private set;}
    @TestVisible
    public Integer minInvalidText {get; private set;}
    @TestVisible
    public Integer minPercentNumber {get; private set;}
    @TestVisible
    public String natPersIDSuffix {get; private set;}
    @TestVisible
    public String passportSuffix {get; private set;}
    @TestVisible
    public List<String> defaultValues {get; private set;}
    @TestVisible
    public Boolean defaultValuesCaseSensitive {get; private set;}
    @TestVisible
    public List<String> accountDivisionNames {get; private set;}
    @TestVisible
    public String caseQueue {get; private set;}
    @TestVisible
    public String caseRecordType {get; private set;}


    private MiFIDExpValSettings(String key) {
        this.key = key;
        load();
    }

    public static MiFIDExpValSettings getInstance(String key) {
        if ((instance == null) || (instance.key != key)) {
            instance = new MiFIDExpValSettings(key);
        }
        return instance;
    }

    public static MiFIDExpValSettings getRequiredDefault() {
        MiFIDExpValSettings result = getInstance(DEFAULT_SETTINGS);
        if (result.initializationFails == true) {
            throw new ExpressionValidatorException('MiFID Expression Validation Settings not found');
        }
        //else...
        return result;
    }

    public static Boolean defaultRecIsActive() {
        if (
            (instance != null) &&
            (instance.key == DEFAULT_SETTINGS)
        ) {
            return instance.isActive;
        }
        //else...
        List<MiFID_Exp_Val_Settings__mdt> settingsList = [
            SELECT
                Is_Active__c
            FROM
                MiFID_Exp_Val_Settings__mdt
            WHERE
                DeveloperName = :DEFAULT_SETTINGS
        ];
        Boolean result = (
            (!settingsList.isEmpty()) &&
            (settingsList[0].Is_Active__c == true)
        );
        return result;
    }

    void load() {
        initializationFails = true;
        List<MiFID_Exp_Val_Settings__mdt> settingsList = [
            SELECT
                Account_Division_Name__c,
                Case_Rec_Type_Dev_Name__c,
                Cases_Queue_Dev_Name__c,
                Default_values__c,
                Default_values_case_sensitive__c,
                Is_Active__c,
                Maximum_length__c,
                Min_invalid_number__c,
                Min_invalid_text__c,
                Min_percent_number__c,
                Minimum_length__c,
                National_Personal_ID_Suffix__c,
                Passport_Suffix__c
            FROM
                MiFID_Exp_Val_Settings__mdt
            WHERE
                DeveloperName = :key
        ];
        if (!settingsList.isEmpty()) {
            initializationFails = false;
            MiFID_Exp_Val_Settings__mdt settings = settingsList[0];
            accountDivisionNames = SPTextUtil.parseMultilineText(settings.Account_Division_Name__c);
            caseRecordType = settings.Case_Rec_Type_Dev_Name__c;
            caseQueue = settings.Cases_Queue_Dev_Name__c;
            defaultValues = SPTextUtil.parseMultilineText(settings.Default_values__c);
            defaultValuesCaseSensitive = settings.Default_values_case_sensitive__c;
            isActive = settings.Is_Active__c;
            maxLength = Integer.valueOf(settings.Maximum_length__c);
            minLength = Integer.valueOf(settings.Minimum_length__c);
            minInvalidNumber = Integer.valueOf(settings.Min_invalid_number__c);
            minInvalidText = Integer.valueOf(settings.Min_invalid_text__c);
            if (settings.Min_percent_number__c != null) {
                minPercentNumber = Integer.valueOf(settings.Min_percent_number__c);
            }
            natPersIDSuffix = settings.National_Personal_ID_Suffix__c;
            passportSuffix = settings.Passport_Suffix__c;
        }
    }
    
}