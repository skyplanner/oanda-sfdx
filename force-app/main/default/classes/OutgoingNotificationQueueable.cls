/**
 * @description       : queueable class to send requests to the server when Outgoing Notification events will sent
 * to update records info on external sources
 * @author            : OANDA
 * @group             :
 * @last modified on  : 3 Jul 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class OutgoingNotificationQueueable implements Queueable, Database.AllowsCallouts {

    private List<Outgoing_Notification__e> recordsToNotify;
    public  Map<String, List<String>> errorLogs = new Map<String, List<String>>();
    private final String LOGGER_CATEGORY = 'OutgoingNotificationQueueable';
    private final String LOGGER_MSG = 'Issue with callouts';

    public OutgoingNotificationQueueable(List<Outgoing_Notification__e> recordsToNotify) {
        this.recordsToNotify = recordsToNotify;
    }

    //the number of requests will be limited by the OutgoingNotificationEventTriggerConfig setting
    public void execute(QueueableContext context) {
        OutgoingNotificationBus outgoingNotification = new OutgoingNotificationBus();
        for (Outgoing_Notification__e onEvent : recordsToNotify) {
            try {
                outgoingNotification = outgoingNotification.getInstance(onEvent.EventName__c);
                outgoingNotification.sendRequest(onEvent.Key__c, onEvent.Body__c);
            } catch (Exception ex) {
                if (errorLogs.containsKey(outgoingNotification.getRecordId())) {
                    errorLogs.get(outgoingNotification.getRecordId()).add(onEvent.EventName__c + ': ' + (ex.getMessage().length() > 50 ? ex.getMessage().substring(0, 50) : ex.getMessage()));
                } else {
                    errorLogs.put(outgoingNotification.getRecordId(), new List<String>{onEvent.EventName__c + ': ' + (ex.getMessage().length() > 50 ? ex.getMessage().substring(0, 50) : ex.getMessage())});
                }
            }
        }

        if (!errorLogs.isEmpty()) {
            Logger.exception(
                    LOGGER_MSG,
                    LOGGER_CATEGORY,
                    errorLogs.toString());
        }
    }
}