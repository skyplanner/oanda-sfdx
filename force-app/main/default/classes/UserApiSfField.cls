/**
 * User API Salesforce field
 */
public class UserApiSfField extends UserApiField {
    public UserApiSfField(
        User_API_Sf_Field__mdt f,
        Map<String, Schema.SObjectField> fieldsMap)
    {
        label = f.label;
        name = f.Field_Api_Name__c;
        showOnList = f.Show_on_List__c;
        showOnDetails = f.Show_on_Details__c;
        readOnly = f.Read_Only__c;
        order = f.Order__c.intValue();
        
        DescribeFieldResult fieldDesc =
            fieldsMap.get(name).getDescribe();

        Schema.DisplayType fType = fieldDesc.getType();
        
        isText = fType == Schema.DisplayType.STRING;
        isTextArea = fType == Schema.DisplayType.TEXTAREA;
        isCheck = fType == Schema.DisplayType.BOOLEAN;
        isPicklist = fType == Schema.DisplayType.PICKLIST;
        isEmail = fType == Schema.DisplayType.EMAIL;
        isDate = fType == Schema.DisplayType.DATE;
        isDateTime = fType == Schema.DisplayType.DATETIME;
        isPhone = fType == Schema.DisplayType.PHONE;
        isNumber = fType == Schema.DisplayType.INTEGER ||
            fType == Schema.DisplayType.LONG ||
            fType == Schema.DisplayType.DOUBLE;
    }
}