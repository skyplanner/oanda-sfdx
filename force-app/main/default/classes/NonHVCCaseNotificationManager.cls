/**
 * @File Name          : NonHVCCaseNotificationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/15/2024, 4:15:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/2/2021, 2:53:04 PM   acantero     Initial Version
**/
public inherited sharing class NonHVCCaseNotificationManager {

    static String nonHvcCaseNotificationTypeId;

    private List<String> placeholders = new List<String>{
        Label.CaseNumberPlaceholder,
        Label.TierPlaceholder
    };
    Map<String, String> fieldsByPlaceholder = new Map<String, String>{
        Label.CaseNumberPlaceholder => 'CaseNumber',
        Label.TierPlaceholder => 'Tier__c'
    };

    public void sendParkedNotifications(List<Case> caseList) {
        if (
            (caseList == null) ||
            caseList.isEmpty()
        ) {
            return;
        }
        //else...
        String currentUserId = UserInfo.getUserId();
        Set<String> userIdSet = 
            new Set<String>{currentUserId};
        for(Case caseObj : caseList) {
            sendParkedNotification(
                caseObj,
                userIdSet
            );
        }
    }

    public void sendParkedNotification(
        Case caseObj,
        Set<String> userIdSet
    ) {
        Messaging.CustomNotification notification = new Messaging.CustomNotification();
        // Set the contents for the notification
        notification.setTitle(
            replacePlaceholders(Label.Non_HVC_Case_Assigned_Subject,caseObj)
        );
        
        notification.setBody(
            replacePlaceholders(Label.Non_HVC_Case_Assigned_Body,caseObj)
        );
        // Set the notification type and target
        notification.setNotificationTypeId(
            getNonHvcCaseNotificationId()
        );
        notification.setTargetId(caseObj.Id);
        // Send the notification
        notification.send(userIdSet);
    }

    public String getNonHvcCaseNotificationId() {
        if (nonHvcCaseNotificationTypeId == null) {
            nonHvcCaseNotificationTypeId = 
                CustomNotificationTypeHelper.getByDevName(
                    OmnichanelConst.NON_HVC_CASE_NOTIF_DEV_NAME,
                    true
                ).Id;
        }
        return nonHvcCaseNotificationTypeId;
    }
    
    private  String replacePlaceholders(String text,Case caseObj) {
		String replacedText = text;
		for (String placeholder : placeholders) {
			if (replacedText.contains(placeholder)) {
				if (fieldsByPlaceholder.containsKey(placeholder)) {
					String field = fieldsByPlaceholder.get(placeholder);
					String value = (String) caseObj.get(field);
					string validValue = SPTextUtil.getValueOrEmpty(value);
					replacedText = replacedText.replace(
						placeholder,
						validValue
					);
				}
			}
		}
		return replacedText;
	}

}