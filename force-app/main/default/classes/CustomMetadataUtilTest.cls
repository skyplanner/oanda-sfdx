@isTest
private class CustomMetadataUtilTest {
    @testSetup
    static void init() {
        TestDataFactory testDataFactory = new TestDataFactory();
        List<Account> accounts = testDataFactory.createTestAccounts(3);
        List<fxAccount__c> fxAccounts = testDataFactory.createFXTradeAccounts(accounts);
    }
    
    @isTest
    static void validateFxdbFieldsTest() {
        List<fxAccount__c> fxAccounts = [SELECT Id, Employment_Job_Title__c FROM fxAccount__c ORDER BY Id];
        
        Test.startTest();
        fxAccounts[0].Employment_Job_Title__c = 'A string that is less than 100 bytes';
        update fxAccounts[0];
        
        Boolean expectedExceptionThrown = false;
        try {
            fxAccounts[1].Employment_Job_Title__c = 'A string that is more than 100 bytes but less than 100 characters and has Asian characters: 一 二 三';
            update fxAccounts[1];
        } catch(Exception e) {
            expectedExceptionThrown = e.getMessage().contains('The byte length is too long.');
        }
        
        System.runAs(UserUtil.getSystemUserForTest()) {
            fxAccounts[2].Employment_Job_Title__c = 'A string that is more than 100 bytes but less than 100 characters and has Asian characters: 一 二 三';
            update fxAccounts[2];
        }
        Test.stopTest();
        
        fxAccounts = [SELECT Id, Employment_Job_Title__c FROM fxAccount__c ORDER BY Id];
        
        System.assertEquals('A string that is less than 100 bytes', fxAccounts[0].Employment_Job_Title__c);
        //rollback should be re-abled later
        //System.assert(expectedExceptionThrown);
        System.assertEquals('A string that is more than 100 bytes but less than 100 characters and has Asian characters: 一 二 三', fxAccounts[2].Employment_Job_Title__c);
    }

    @isTest
    static void validateTemplateByDeveloperName() {

        List<EmailLanguageExpiredDoc__mdt> eleList = [
            SELECT 
                MasterLabel, 
                Email_Template_Developer_Name__c 
            FROM 
                EmailLanguageExpiredDoc__mdt];

        System.assert(
            eleList.size() > 0, 
            'The custom metadata EmailLanguageExpiredDoc__mdt shouldn\'t be empty');

        String countryName = eleList[0].MasterLabel;
        String emailTemplateDevName = eleList[0].Email_Template_Developer_Name__c;

        List<EmailTemplate> etList = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = :emailTemplateDevName];

        System.assertEquals(
            1, 
            etList.size(), 
            'The email template ' + emailTemplateDevName + ' does not exist.');

        String etId = CustomMetadataUtil.getTemplateDeveloperName(countryName);

        System.assert(etId != null, 'The email template was not found');
        System.assertEquals(etList[0].Id, etId, 'The ids doesn\'t match');
    }
}