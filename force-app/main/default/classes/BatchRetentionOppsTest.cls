/*
 *	Author : Deepak Malkani.
 *	Created Date : May 31 2016
 *	Purpose : This is a test class to test the batch process for Opportunity Retention creation
*/

@isTest(SeeAllData = false)
private class BatchRetentionOppsTest
{
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	
	// Deepak Malkani : Test Method to initialise initial set-up data
	@testSetup
	static void initTestData(){
		//Creating Custom Settings
		Retention_Opportunity_Setting__c CS = new Retention_Opportunity_Setting__c();
    	CS.Name = 'Create Retention Opportunity';
    	CS.LifeTime__c = 5000;
    	CS.Create_From_House_Account_Only__c = false;
    	CS.Owners_to_Ignore__c = null;
    	CS.Countries_to_Include__c = 'Canada';
    	insert CS;
    	Retention_Opportunity_Setting__c CSAttrition = new Retention_Opportunity_Setting__c();
    	CSAttrition.Name = 'Create Attrition Opportunity';
    	CSAttrition.Create_From_House_Account_Only__c = false;
    	CSAttrition.Division_To_Exclude__c = 'OJ';
    	CSAttrition.Lookback_In_Months__c = 12;
    	CSAttrition.LifeTime__c = 5000;
    	CSAttrition.Owners_to_Ignore__c = null;
    	CSAttrition.Countries_to_Include__c = 'Canada';
    	insert CSAttrition;
    	Country_Setting__c nigeria = new Country_Setting__c(Name='Nigeria', Group__c='Europe', ISO_Code__c='ng', Region__c='Sub-Saharan Africa', Zone__c='EMEA', Risk_Rating__c='PROHIBITED');
        insert nigeria;
	}

	//Deepak Malkani : Test Method to test the APEX Batch job - to create a retention Opportunity for Accounts which have a live FXTRading Account,
	// with Traded Date > 2 months  
	@isTest
	static void UnitTest_1_createRetOppBatch_2MonthAgo()
	{
		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE

		//Creating a Person Accunt
		Account accnt = testHandler.createTestAccount();
		//Creating a Live TXTrading Account
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
		//creating dummy opportunities under the trading account
    	Opportunity oppty = testHandler.createOpportunity(accnt);

    	// EXECUTE 
		Test.startTest();
		
    	//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
    	Test.stopTest();
    	
    	// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.assertEquals(2, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(1, opptsforAccnt.size());
	}

	//Deepak Malkani : Bulk Test Method to test the APEX Batch job - to create a retention Opportunity for Accounts which have a live FXTRading Account,
	// with Traded Date > 2 months  
	@isTest
	static void UnitTest_2_createRetOppBatch_2MonthAgo_Bulk(){

		//DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE

		//Preparing test data : 200 Accounts and its corresponding FXTradeAccounts and Opportunities.
		List<Account> accntInsList = testHandler.createTestAccounts(20);
		List<fxAccount__c> fxTradList = testHandler.createFXTradeAccounts(accntInsList);
		List<Opportunity> opptyInsList = testHandler.createOpportunities(accntInsList);
		
		//EXECUTE
		Test.startTest();
		String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO) + 'AND PersonMailingCountry = \'Canada\'';
        BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
    	Test.stopTest();
    	
    	// ASSERT AND VALIDATE
    	//There will be 400 opportunities created after the batch run , 200 existing and 200 newly created Retention Opportunties
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId IN : accntInsList];
    	System.assertEquals(40, opptsforAccnt.size());	
	}

	//Deepak Malkani : Creating retention opportunities only for System User, based on Custom Setting
	@isTest
	static void UnitTest_3_createRetOppBatch_for_SysUser_2monthsAgo(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		Retention_Opportunity_Setting__c CS = [SELECT Name, LifeTime__c, Create_From_House_Account_Only__c, Countries_to_Include__c
												FROM Retention_Opportunity_Setting__c
												WHERE Name = 'Create Retention Opportunity'];
		CS.Create_From_House_Account_Only__c = true;
		update CS;

		Account accnt = testHandler.createTestAccountforSysOwner();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
    	Opportunity oppty = testHandler.createOpportunity(accnt);

    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO) + 'AND PersonMailingCountry = \'Canada\'';
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.assertEquals(2, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(1, opptsforAccnt.size());
	}

	//Deepak Malkani : This test method will not create any retention opportunities at all, because at Custom Setting, House Account only is true and the account is owned by a Sales User.
	@isTest
	static void UnitTest_4_noRetOppBatch_2monthsAgo(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		Retention_Opportunity_Setting__c CS = [SELECT Name, LifeTime__c, Create_From_House_Account_Only__c, Countries_to_Include__c
												FROM Retention_Opportunity_Setting__c
												WHERE Name = 'Create Retention Opportunity'];
		CS.Create_From_House_Account_Only__c = true;
		update CS;

		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
    	Opportunity oppty = testHandler.createOpportunity(accnt);

    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c, fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO) + 'AND PersonMailingCountry = \'Canada\'';
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.assertEquals(1, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(0, opptsforAccnt.size());
	}
 
	//Deepak Malkani : Test case where Account is Owned by Lori, who is an owner to be ignored from creation of Retention Opportunities.
	@isTest
	static void UnitTest_5_noRetOppforOwnerLori(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		Retention_Opportunity_Setting__c CS = [SELECT Name, LifeTime__c, Create_From_House_Account_Only__c, Countries_to_Include__c
												FROM Retention_Opportunity_Setting__c
												WHERE Name = 'Create Retention Opportunity'];
		CS.Owners_to_Ignore__c = 'Lorin Mikari';
		update CS;

		User testUsr = UserUtil.getTestUser('usertest@oanda.com');
		testUsr.FirstName = 'Lorin';
		testUsr.LastName = 'Mikari';
		insert testUsr;

		Account accnt = testHandler.createTestAccount();
		accnt.OwnerId = testUsr.Id;
		update accnt;
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
    	Opportunity oppty = testHandler.createOpportunity(accnt);
    	system.debug('---> test data account is '+accnt);
    	// EXECUTE 
		Test.startTest();
		
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c,  fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.debug('---> The opportunities in the list is '+opptsforAccnt);
    	system.assertEquals(1, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(0, opptsforAccnt.size());
	}

	//Deepak Malkani : Test case for Account with MialingCountry = Nigeria, not part of the Countries to Include List
	@isTest
	static void UnitTest_6_noRetOppforCountryNigeria(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		
		Account accnt = testHandler.createTestAccount();
		accnt.PersonMailingCountry = 'Nigeria';
		update accnt;
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
    	Opportunity oppty = testHandler.createOpportunity(accnt);
    	system.debug('---> test data account is '+accnt);

    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c,  fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.debug('---> The opportunities in the list is '+opptsforAccnt);
    	system.assertEquals(1, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(0, opptsforAccnt.size());
	}

	//Deepak Malkani : Test case for Account with Life Time Deposit > 5000 USD. In this case retention Opportunity will not be created.
	@isTest
	static void UnitTest_7_noRetOppforLargerLifeTimeDeposits(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt);
		tradeAccount.Total_Deposits_USD__c = 6000; //Life Deposits > Custom Setting which is 5000, so Retention Opportunities wont be created.
    	update tradeAccount;
    	Opportunity oppty = testHandler.createOpportunity(accnt);
    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c,  fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.debug('---> The opportunities in the list is '+opptsforAccnt);
    	system.assertEquals(1, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(0, opptsforAccnt.size());
	}

	//Deepak Malkani : Test case for Account with Life Time Deposit < 5000 USD. In this case retention Opportunity will be created.
	@isTest
	static void UnitTest_8_RetOppforSmallerLifeTimeDeposits(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt); //trade accounts has Life Time Deposits = 2000 < Custom Setting, so Retention Oppts will be created
    	Opportunity oppty = testHandler.createOpportunity(accnt);

    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c,  fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.assertEquals(2, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(1, opptsforAccnt.size());
	}

	//Deepak Malkani : Test case for Account which already contains a Closed Lost - Contacted Oppty. In this case no new retention/attrition opptys will be created
	@isTest
	static void UnitTest_9_noRetOppforClosedLostNotContacted(){

		// DECLARE
		DateTime TWO_MONTHS_AGO = DateTime.now().addMonths(-2) + 1;
		DateTime THIRTEEN_MONTHS_AGO = DateTime.now().addMonths(-13);
		TestDataFactory testHandler = new TestDataFactory();

		//PREPARE
		
		Account accnt = testHandler.createTestAccount();
		fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt); //trade accounts has Life Time Deposits = 2000 < Custom Setting, so Retention Oppts will be created
    	Opportunity oppty = testHandler.createRetOpportunity(accnt);

    	// EXECUTE 
		Test.startTest();
		//run the batch job with the logged in user
    	String Query = 'SELECT Id, Last_Trade_Date__c, fxAccount__c, OwnerId, Owner.Name, Retention_Division__c, Language_Preference__pc, Has_Open_Trade__c,  fxAccount__r.Account_Locked__c,PersonMailingCountry FROM Account WHERE Last_Trade_Date__c!=null AND Last_Trade_Date__c <= '
                			+ SoqlUtil.getSoql(TWO_MONTHS_AGO) + ' AND Last_Trade_Date__c > ' + SoqlUtil.getSoql(THIRTEEN_MONTHS_AGO);
    	BatchRetentionOppsScheduleable batch = new BatchRetentionOppsScheduleable(query);
    	Database.executeBatch(batch);
		Test.stopTest();

		// ASSERT AND VALIDATE
    	List<Opportunity> opptsforAccnt = [SELECT id, Name FROM Opportunity WHERE AccountId = :accnt.Id];
    	system.assertEquals(1, opptsforAccnt.size());
    	opptsforAccnt.clear();
    	opptsforAccnt = [SELECT id, Name, RecordType.Name FROM Opportunity WHERE AccountId =: accnt.Id AND RecordType.Name = 'Retention'];
    	system.assertEquals(1, opptsforAccnt.size());
	}
	
	/*
	//Gilbert: create 2nd retetnion opportunity after the first retention opportunity is closed/won
	static testMethod void create2ndRetenOppAfterCloseWon(){
    	
    	//create the retention user
    	ID retenProfileId = [ SELECT Id FROM Profile WHERE Name = 'Retention' ].Id;
    	User FXS_USER = UserUtil.getRandomTestUser();
    	FXS_USER.ProfileId = retenProfileId;
        User FXS_USER2 = UserUtil.getRandomTestUser();
        FXS_USER2.ProfileId = retenProfileId;
        
        List<User> userList = new List<User>();
        userList.add(FXS_USER);
        userList.add(FXS_USER2);
        update userList;
        
        System.runAs(SYSTEM_USER) {
        	Retention_Opportunity_Setting__c CS = new Retention_Opportunity_Setting__c();
	    	CS.Name = 'Create Retention Opportunity';
	    	CS.Create_From_House_Account_Only__c = true;
	    	insert CS;
        
	        //add the retention user to retention queue
	        Group g = [select Id from Group where Name='Retention OC' AND Type = 'Queue'];
	       
	        List<GroupMember> members = new List<GroupMember>();
	        members.add(new GroupMember(UserOrGroupId = FXS_USER.Id, GroupId = g.Id));
	        members.add(new GroupMember(UserOrGroupId = FXS_USER2.Id, GroupId = g.Id));
	        insert members;
	        
	    	
	    	TestDataFactory testHandler = new TestDataFactory();
	    	Account accnt = testHandler.createTestAccount();
			fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt); 
	    	Opportunity oppty = testHandler.createRetOpportunity(accnt);
	    	
	    	oppty.OwnerId = FXS_USER.Id;
	    	oppty.StageName = FunnelStatus.RETENTION_CLOSED_WON;
	    	update oppty;
	    	
	    	Test.startTest();
	    	
			BatchRetentionOppsScheduleable.executeBatch();
			
			Test.stopTest();
			
			List<Opportunity> oppList = [select id, name, Metric_End_DateTime__c, stageName, ownerId from Opportunity where recordtypeId = :RecordTypeUtil.getRetentionOpportunityTypeId()];
			
			system.assertEquals(2, oppList.size());
			
			for(Opportunity opp : oppList){
				if(opp.Id == oppty.Id){
					system.assertNotEquals(null, opp.Metric_End_DateTime__c);
				}else{
					system.assertEquals(null, opp.Metric_End_DateTime__c);
					system.assertEquals(FXS_USER.Id, opp.ownerId);
					system.assertEquals(FunnelStatus.RETENTION_NEW, opp.stageName);
				}
			}
		
        }
	}
	
	//Gilbert: create 2nd retetnion opportunity after the first retention opportunity is closed/won
	static testMethod void create2ndRetenOppAfterCloseLoss(){
    	
    	//create the retention user
    	ID retenProfileId = [ SELECT Id FROM Profile WHERE Name = 'Retention' ].Id;
    	User FXS_USER = UserUtil.getRandomTestUser();
    	FXS_USER.ProfileId = retenProfileId;
        User FXS_USER2 = UserUtil.getRandomTestUser();
        FXS_USER2.ProfileId = retenProfileId;
        
        List<User> userList = new List<User>();
        userList.add(FXS_USER);
        userList.add(FXS_USER2);
        update userList;
        
        System.runAs(SYSTEM_USER) {
        	Retention_Opportunity_Setting__c CS = new Retention_Opportunity_Setting__c();
	    	CS.Name = 'Create Retention Opportunity';
	    	CS.Create_From_House_Account_Only__c = true;
	    	insert CS;
        
	        //add the retention user to retention queue
	        Group g = [select Id from Group where Name='Retention OC' AND Type = 'Queue'];
	       
	        List<GroupMember> members = new List<GroupMember>();
	        members.add(new GroupMember(UserOrGroupId = FXS_USER.Id, GroupId = g.Id));
	        members.add(new GroupMember(UserOrGroupId = FXS_USER2.Id, GroupId = g.Id));
	        insert members;
	        
	    	
	    	TestDataFactory testHandler = new TestDataFactory();
	    	Account accnt = testHandler.createTestAccount();
			fxAccount__c tradeAccount = testHandler.createFXTradeAccount(accnt); 
	    	Opportunity oppty = testHandler.createRetOpportunity(accnt);
	    	
	    	oppty.OwnerId = FXS_USER.Id;
	    	oppty.StageName = FunnelStatus.RETENTION_CLOSED_LOST_NO_CONTACT;
	    	update oppty;
	    	
	    	Test.startTest();
	    	
			BatchRetentionOppsScheduleable.executeBatch();
			
			Test.stopTest();
			
			List<Opportunity> oppList = [select id, name, Metric_End_DateTime__c, stageName, ownerId from Opportunity where recordtypeId = :RecordTypeUtil.getRetentionOpportunityTypeId()];
			
			system.assertEquals(2, oppList.size());
			
			for(Opportunity opp : oppList){
				if(opp.Id == oppty.Id){
					system.assertNotEquals(null, opp.Metric_End_DateTime__c);
				}else{
					system.assertEquals(null, opp.Metric_End_DateTime__c);
					system.assertNotEquals(FXS_USER.Id, opp.ownerId);
					system.assertEquals(FunnelStatus.RETENTION_NEW, opp.stageName);
				}
			}
		
        }
	}
	*/
}