/**
 * @File Name          : ExpressionValidationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/24/2020, 2:48:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
@isTest
private class ExpressionValidationManager_Test {

	@isTest
	static void validateExpression_test1() {
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression(null, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

    //test invalid apex class name (derived form developerName)
    @isTest
	static void invokeApexValidation_test1() {
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.validation1 = '\\d{10}';
        fakeExpVal.apexValidation = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
        Boolean throwException = false;
		Test.startTest();
        try {
            ExpressionValidationManager.ExpValError result = 
                expValMan.validateExpression('2234567895', fakeExpVal);
        } catch (Exception ex) {
            throwException = true;
        }
		Test.stopTest();
		System.assert(throwException);
	}

    //test letter validations (extract letters frgament)
    @isTest
	static void validateConsecutiveRepetitive_test1() {
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkOnlyValidFragments = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression('AB1XYCCCCCCCCC6', fakeExpVal);
		Test.stopTest();
		System.assert(result.errorMsg.contains(System.Label.MifidValPasspRepetitiveError));
    }

    //test number validations and remove prefix
    @isTest
	static void validateConsecutiveRepetitive_test2() {
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.validation1 = MiFIDExpValConst.FORMAT_WILDCARD;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        fakeExpVal.checkOnlyValidFragments = true;
        fakeExpVal.prefixSize = 3;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression('777789876543', fakeExpVal);
		Test.stopTest();
		System.assert(result.errorMsg.contains(System.Label.MifidValPasspConsecutiveError));
    }

    //test invalid char
    @isTest
	static void validateConsecutiveRepetitive_test3() {
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.validation1 = MiFIDExpValConst.FORMAT_WILDCARD;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression('11-1111111', fakeExpVal);
		Test.stopTest();
		System.assert(result.errorMsg.contains(System.Label.MifidValInvalidCharError));
    }

    //test invalid lenght
    @isTest
	static void validateConsecutiveRepetitive_test4() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        valSettings.minLength = 3;
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.validation1 = MiFIDExpValConst.FORMAT_WILDCARD;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression('1', fakeExpVal);
		Test.stopTest();
		System.assert(result.errorMsg.contains(System.Label.MifidValInvalidLengthError));
    }

    //test apex validation
    @isTest
	static void validateConsecutiveRepetitive_test5() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        valSettings.minLength = 3;
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'PL_MIFID_NID';
        fakeExpVal.key = 'Poland_NID';
        fakeExpVal.apexValidation = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
        Test.startTest();
        //valid
        ExpressionValidationManager.ExpValError result1 = 
            expValMan.validateExpression('2234567895', fakeExpVal);
        //invalid
        ExpressionValidationManager.ExpValError result2 = 
            expValMan.validateExpression('2234567896', fakeExpVal);
		Test.stopTest();
		System.assertEquals(null, result1);
		System.assertNotEquals(null, result2);
		System.assert(result2.errorMsg.contains(System.Label.MifidValPasspChecksumError));
    }

    //test letter consecutive and repetitive in a valid value (in all value)
    @isTest
	static void validateConsecutiveRepetitive_test6() {
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.checkConsecutiveLetters = true;
        fakeExpVal.checkRepetitiveLetter = true;
        fakeExpVal.checkConsecutiveNumbers = true;
        fakeExpVal.checkRepetitiveNumber = true;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression('AB1XYCC6', fakeExpVal);
		Test.stopTest();
		System.assertEquals(null, result);
    }
    
    //test default value check with CaseSensitive comparison
    @isTest
	static void isAValidDefaultValue_test1() {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        valSettings.defaultValuesCaseSensitive = true;
        valSettings.defaultValues = new List<String> {
            MIFIDValidationTestDataFactory.DEFAULT_VALUE
        };
        ExpressionValidationWrapper fakeExpVal = new ExpressionValidationWrapper();
        fakeExpVal.developerName = 'XX_MIFID';
        fakeExpVal.key = 'XX';
        fakeExpVal.canBeDefaultValue = MiFIDExpValConst.CAN_BE_VALUE;
        ExpressionValidationManager expValMan = new ExpressionValidationManager();
		Test.startTest();
        ExpressionValidationManager.ExpValError result = 
            expValMan.validateExpression(
                MIFIDValidationTestDataFactory.DEFAULT_VALUE.toUpperCase(), 
                fakeExpVal
            );
		Test.stopTest();
		System.assertEquals(null, result);
	}

}