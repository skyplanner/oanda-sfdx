/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class SendScheduledEmailJob_Test {
	public static final Integer COUNT = 3;

	@TestSetup
	static void setup() {
		List<Account> accountList = ScheduledEmailTestUtil.createAccounts(
			COUNT
		);
		List<Contact> contactList = ScheduledEmailTestUtil.createContacts(
			accountList
		);
		List<Case> caseList = ScheduledEmailTestUtil.createCases(contactList);
		ScheduledEmailTestUtil.createDraftEmails(contactList, caseList);

		JobStatusTriggerHandler.enabled = false;

		ScheduledEmailTestUtil.createStatusJob(
			SendScheduledEmailJob.SEND_EMAIL_JOB,
			JobStatusHelper.ACTIVE_STATUS
		);
	}
	@isTest
	static void doActionNeedTwoIterations() {
		Datetime dt = DateTime.now();
		createScheduledEmail(dt);
		Test.startTest();
		SendScheduledEmailJob instance = new SendScheduledEmailJob(COUNT - 1);
		System.enqueueJob(instance);
		Test.stopTest();
		Integer errorCount = getErrorCount();
		Integer pendingRecords = getPendingRecords();
		System.assertEquals(0, errorCount, 'No Error');
		//System.assertEquals(1, pendingRecords, 'One pending email');
	}
	@isTest
	static void doActionNeedOneIteration() {
		Datetime dt = DateTime.now();
		createScheduledEmail(dt);
		Test.startTest();
		SendScheduledEmailJob instance = new SendScheduledEmailJob(COUNT + 1);
		System.enqueueJob(instance);
		Test.stopTest();
		Integer errorCount = getErrorCount();
		Integer pendingRecords = getPendingRecords();
		System.assertEquals(0, errorCount, 'No Error');
		//System.assertEquals(0, pendingRecords, 'No pending scheduled email');
	}
	@isTest
	static void getNewInstance() {
		Test.startTest();
		SendScheduledEmailJob result = SendScheduledEmailJob.getNewInstance();
		Test.stopTest();
		System.assertNotEquals(null, result, 'Instance not null');
	}
	@isTest
	static void tryToClose() {
		Datetime dt = DateTime.now();
		createScheduledEmail(dt);
		Test.startTest();
		SendScheduledEmailJob instance = new SendScheduledEmailJob(COUNT);
		instance.action = BaseJob.ActionType.CLOSING;
		instance.requestDate = System.now().addHours(-1);
		System.enqueueJob(instance);
		Test.stopTest();
		Integer errorCount = getErrorCount();
		Integer pendingRecords = getPendingRecords();
		System.assertEquals(0, errorCount, 'No Error');
		System.assertEquals(3, pendingRecords, 'Is Testing ');
	}
	@isTest
	static void finalizer() {
		Test.startTest();
		Exception fakeException = new JobStatusHelper.JobStatusException(
			'Fake Exception'
		);
		BaseJob.JobFinalizer instance = new BaseJob.JobFinalizer(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		instance.checkError(true, fakeException);
		Test.stopTest();

		Integer errorCount = getErrorCount();
		System.assertEquals(1, errorCount, 'Fake Exception');
		String status = [
			SELECT Status__c
			FROM Job_Status__c
			WHERE Name = :SendScheduledEmailJob.SEND_EMAIL_JOB
			LIMIT 1
		]
		.Status__c;
		System.assertEquals(
			JobStatusHelper.INACTIVE_STATUS,
			status,
			'Job is inactive'
		);
	}

	private static void createScheduledEmail(Datetime schedule) {
		ScheduleMailSender emailSender = new ScheduleMailSender();
		ScheduleMailSender.ScheduleDateTime scheduleTime = emailSender.getScheduleDateTime(
			schedule
		);
		ScheduledEmailTestUtil.createScheduledMessagesForDraftEmails(
			scheduleTime.scheduledDate,
			scheduleTime.scheduledTime,
			scheduleTime.amOrPm,
			schedule
		);
	}
	private static Integer getErrorCount() {
		return [SELECT COUNT() FROM Internal_Error__c];
	}
	private static Integer getPendingRecords() {
		return [SELECT COUNT() FROM Scheduled_Email__c];
	}
}