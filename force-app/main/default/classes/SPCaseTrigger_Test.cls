/**
 * @File Name          : SPCaseTrigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/27/2024, 1:19:19 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/15/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class SPCaseTrigger_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        );
    }

    //an exception (other than LogException) is thrown 
    @isTest
    static void exception1() {
        Boolean exceptionOk = false;
        ID hvcAccountId = OmnichanelRoutingTestDataFactory.getHvcAccountId();
        Test.startTest();
        ExceptionTestUtil.prepareDummyException();
        try {
            insert new Case(
                Origin = 'Email - Frontdesk',
                AccountId = hvcAccountId,
                //Subject = 'ACL Email ' + System.Now(),
                Priority = 'Normal',
                Language = OmnichanelConst.ENGLISH_LANG_CODE
            );
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    //an exception (of type LogException) is thrown 
    @isTest
    static void exception2() {
        Boolean exceptionOk = false;
        ID hvcAccountId = OmnichanelRoutingTestDataFactory.getHvcAccountId();
        Test.startTest();
        ExceptionTestUtil.exceptionInstance = 
            LogException.newInstance('abc', 'def');
        try {
            insert new Case(
                Origin = 'Email - Frontdesk',
                AccountId = hvcAccountId,
                //Subject = 'ACL Email ' + System.Now(),
                Priority = 'Normal',
                Language = OmnichanelConst.ENGLISH_LANG_CODE
            );
            //...
        } catch (Exception ex) {
            System.debug('exception type: ' + ex.getTypeName());
            exceptionOk = true;
        }
        Test.stopTest();
        System.assertEquals(true, exceptionOk);
    }

    @isTest
    static void insert_test() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        ID hvcAccountId = OmnichanelRoutingTestDataFactory.getHvcAccountId();
        Test.startTest();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        insert new Case(
            Origin = 'Email - Frontdesk',
            AccountId = hvcAccountId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Language = OmnichanelConst.ENGLISH_LANG_CODE
        );
        Test.stopTest();
        List<Case> caseList = [select Id, EntitlementId from Case];
        System.assertEquals(1, caseList.size());
        Case caseObj = caseList[0];
        System.assertEquals(emailFrontdeskEntitlementId, caseObj.EntitlementId);
        Integer psrCount = [select count() from PendingServiceRouting];
        System.assertEquals(1, psrCount);
    }

    @isTest
    static void update_test() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        ID hvcAccountId = OmnichanelRoutingTestDataFactory.getHvcAccountId();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        Case case1 = new Case(
            Origin = 'Email - Frontdesk',
            AccountId = hvcAccountId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Chat_Division__c = OmnichanelConst.OANDA_CORPORATION_AB,
            Routed_And_Assigned__c = true
        );
        Case case2 = new Case(
            Origin = 'Email - Frontdesk',
            AccountId = hvcAccountId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Routed_And_Assigned__c = true
        );
        List<Case> newCases = new List<Case> {
            case1, 
            case2
        };
        insert newCases;
        newCases = [select Id, Status, EntitlementId, Milestone_Violated__c from Case];
        System.debug('newCases: ' + newCases);
        String milestoneName = 
            OmnichanelRoutingTestDataFactory.getEmailFrontdeskMilestoneName();
        for(Case caseObj : newCases) {
            caseObj.status = 'Closed';
            caseObj.Milestone_Violated__c = milestoneName;
        }
        Test.startTest();
        System.debug('userType: ' + UserInfo.getUserType());
        update newCases;
        Test.stopTest();
        List<CaseMilestone> caseMilList = [select Id, completionDate, caseId from CaseMilestone];
        System.debug('caseMilList: ' + caseMilList);
        System.assertNotEquals(0, caseMilList.size());
        for(CaseMilestone caseMilObj : caseMilList) {
            System.assertNotEquals(null, caseMilObj.completionDate);
        }
    }

    // test 1 
    // change status of Hvc Case =>
    // related Non_HVC_Case__c should be released
    //
    // test 2
    // set Agent_Capacity_Status__c = Released,
    // (non-hvc case status = default) =>
    // related Non_HVC_Case__c should be released
    // 
    // test 3
    // set Agent_Capacity_Status__c = Released,
    // (non-hvc case status = parked) =>
    // related Non_HVC_Case__c should be deleted
    @isTest
    static void update_test2() {
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        ID hvcAccountId = OmnichanelRoutingTestDataFactory.getHvcAccountId();
        ID leadId = OmnichanelRoutingTestDataFactory.createNewCustomerLead();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        Case case1 = new Case(
            Origin = 'Email - Frontdesk',
            AccountId = hvcAccountId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Chat_Division__c = OmnichanelConst.OANDA_CORPORATION_AB,
            Routed_And_Assigned__c = true
        );
        Case case2 = new Case(
            Origin = 'Email - Frontdesk',
            Lead__c = leadId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Routed_And_Assigned__c = true
        );
        Case case3 = new Case(
            Origin = 'Email - Frontdesk',
            Lead__c = leadId,
            //Subject = 'ACL Email ' + System.Now(),
            Priority = 'Normal',
            Routed_And_Assigned__c = true
        );
        List<Case> newCases = new List<Case> { case1, case2, case3 };

        Test.startTest();
        insert newCases;
        System.runAs(new User(Id = UserInfo.getUserId())) {
		    // code here
	    }
        Non_HVC_Case__c nonHvcCase3 = [
            SELECT Id 
            FROM Non_HVC_Case__c 
            WHERE Case__c = :case3.Id
        ];
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            nonHvcCase3.Status__c = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED;
            nonHvcCase3.OwnerId = UserInfo.getUserId();
		    update nonHvcCase3;
	    }

        List<Non_HVC_Case__c> nonHvcCasesBefore = getNonHvcCases();
        System.debug('nonHvcCasesBefore: ' + nonHvcCasesBefore);
        // test 1 
        case1.Status = OmnichanelConst.CASE_STATUS_IN_PROGRESS;
        // test 2
        case2.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED;
        // test 3
        case3.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED;
        update newCases;
        
        List<Non_HVC_Case__c> nonHvcCasesAfter = getNonHvcCases();
        System.debug('nonHvcCasesAfter: ' + nonHvcCasesAfter);
        Test.stopTest();

        Assert.areEqual(3, nonHvcCasesBefore.size(), 'Invalid result');
        Assert.areEqual(2, nonHvcCasesAfter.size(), 'Invalid result');
        
        for (Non_HVC_Case__c nonHvcCase : nonHvcCasesAfter) {
            Assert.areEqual(
                OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED, 
                nonHvcCase.Status__c, 
                'Invalid value'
            );
            Assert.areNotEqual(
                case3.Id, 
                nonHvcCase.Case__c, 
                'Invalid value'
            );
        }
    }

    static List<Non_HVC_Case__c> getNonHvcCases() { 
        return [
            SELECT
                Case__c,
                Case_Status__c, 
                Status__c
            FROM
                Non_HVC_Case__c
        ];
    }

}