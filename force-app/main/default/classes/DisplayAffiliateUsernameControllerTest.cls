/**
 * Created by Agnieszka Kajda on 01/02/2024.
 */
@IsTest
public with sharing class DisplayAffiliateUsernameControllerTest {
    @IsTest
    static void getAffiliateUsernameTest() {

        TestDataFactory testHandler = new TestDataFactory();
        Account acc = testHandler.createTestAccount();
        fxAccount__c fxa = testHandler.createFXTradeAccount(acc);
		acc.fxAccount__c=fxa.Id;
        update acc;

        Case cs = new Case(
                RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                Status = 'Open',
                fxAccount__c = fxa.Id,
                AccountId = fxa.Account__c,
                Subject = 'Subject of the case');
        insert cs;

        Affiliate__c affiliate = new Affiliate__c(Username__c = 'username',
                Firstname__c = 'John',
                LastName__c = 'Doe',
                Company__c = 'Oanda Corp',
                Division_Name__c = 'OANDA Global Markets',
                Country__c = 'Macao',
                Alias__c = 'doer',
                Email__c = 'aff1@email.com',
                Cellxpert_Partner_ID__c = 1234);
        insert affiliate;

        fxa.Affiliate__c=affiliate.Id;
        fxa.Cellxpert_Partner_ID__c = 1234;
        update fxa;

        System.assertEquals('username', DisplayAffiliateUsernameController.getAffiliateUsername(fxa.Id));
        System.assertEquals('username', DisplayAffiliateUsernameController.getAffiliateUsername(acc.Id));
        System.assertEquals('username', DisplayAffiliateUsernameController.getAffiliateUsername(cs.Id));
        System.assertEquals('Unexpected error occurred.', DisplayAffiliateUsernameController.getAffiliateUsername('12345'));
    }
}