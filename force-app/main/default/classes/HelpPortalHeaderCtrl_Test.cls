/**
 * @File Name          : HelpPortalHeaderCtrl_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 11/20/2019, 6:44:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/20/2019   dmorales     Initial Version
**/
@isTest
private class HelpPortalHeaderCtrl_Test {
     @isTest
     static void getInfoOC() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OC';
         String riskBaseText = c.riskBaseText;     
         
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOC;
         System.assert(riskBaseText == label.replace('\n', '<br>'));
    }
    @isTest
    static void getInfoOEL() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OEL';
         String riskBaseText = c.riskBaseText;       
         Boolean isBottomCookie = c.isBottomCookie;   
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOEL;
         System.assert(riskBaseText == label.replace('\n', '<br>'));
         System.assert(isBottomCookie == true);
    }
    @isTest
    static void getInfoOME() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OME';
         String riskBaseText = c.riskBaseText;
        
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOME;
         System.assert(riskBaseText == label.replace('\n', '<br>'));        
    }
    @isTest
    static void getInfoOCAN() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OCAN';
         String riskBaseText = c.riskBaseText;
        
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOCAN;
         System.assert(riskBaseText == label.replace('\n', '<br>'));
     }
    @isTest
    static void getInfoOAP() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OAP';
         String riskBaseText = c.riskBaseText;
       
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOAP;
         System.assert(riskBaseText == label.replace('\n', '<br>'));
        
    }
    @isTest
    static void getInfoOAU() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'OAU';
         String riskBaseText = c.riskBaseText;
        
         System.assert(c != null);
         String label = System.Label.HelpPFooterRiskOAU;
         System.assert(riskBaseText == label.replace('\n', '<br>'));
      
    }

     @isTest
    static void getFailRiskText() {
         HelpPortalHeaderCtrl c = new HelpPortalHeaderCtrl();
         c.regionCode = 'No Region';
         String riskBaseText = c.riskBaseText;       
         System.assert(c != null);
         System.assert(riskBaseText == '');
    }
}