/**
 * fxAccountHashUtil test class
 */
@isTest
public with sharing class fxAccountHashUtilTest {
    
	/**
	 * - Get userName and email hashed when inserting fxAccount
	 * - Get new userName hash when update fxAccount.Name
	 */
    @isTest
    static void setHashedOnInsertUpdateFxAccount() {
		Account acc = new TestDataFactory().createTestAccount();

		Contact contact = new Contact(
			MailingCountry = 'Canada',
			LastName ='test',
			Email = 'contact@gmail.com',
			Account = acc);
		insert contact;

		contact =
			[SELECT Email 
			FROM Contact
			WHERE Id = :contact.Id];

		// Contact must have email
		System.assert(
			String.isNotBlank(contact.Email),
			'Contact have not Email field filled.');

		fxAccount__c fxAccount = new fxAccount__c();
		fxAccount.Name = '12365';
		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		fxAccount.Account__c = acc.Id;
        fxAccount.Contact__c = contact.Id;
		fxAccount.Division_Name__c = 'OANDA Global Markets';
		insert fxAccount;
		
		fxAccount =
			[SELECT Email_Formula__c,
					Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// fxAccount should have 'Email_Formula__c' filled witih contact email
		System.system.assertEquals(
			fxAccount.Email_Formula__c,
			'contact@gmail.com',
			'Account Email_Formula__c empty.');
		
		String hashName = fxAccount.Hashed_Username__c;
		String hashEmail = fxAccount.Hashed_Email__c;

		System.assert(
			String.isNotBlank(hashName),
			'UserName was not hashed');
		System.assert(
			String.isNotBlank(hashEmail),
			'Account email was not hashed');
		
		// Updating fxAccount Name shuld 
		// get new userName hash
		fxAccount.Name = '556589';
		update fxAccount;
		
		fxAccount =
			[SELECT Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		String hashName2 = fxAccount.Hashed_Username__c;
		String hashEmail2 = fxAccount.Hashed_Email__c;

		System.assert(
			String.isNotBlank(hashName2),
			'UserName hash got empty');
		System.assert(String.isNotBlank(hashEmail2),
			'Email hash got empty');
		System.assert(hashName != hashName2,
			'UserName hash was not updated');
    }

	/**
	 * fxAccount must get new hashed email
	 * when account.PersonEmail is updated
	 */
	@isTest
	static void setHashedOnUpdateAccount() {
		Account acc = new TestDataFactory().createTestAccount();

		Contact contact = new Contact(
			MailingCountry = 'Canada',
			LastName ='test',
			Email = 'contact@gmail.com',
			Account = acc);
		insert contact;

		TriggersUtil.disableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);
		
		fxAccount__c fxAccount = new fxAccount__c();
		fxAccount.Name = '12365';
		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		fxAccount.Account__c = acc.Id;
		fxAccount.Contact__c = contact.Id;
		fxAccount.Division_Name__c = 'OANDA Global Markets';
		insert fxAccount;
		
		TriggersUtil.enableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);

		fxAccount =
			[SELECT Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// Should have empty hashed because
		// I stopped fxAccount triggers before
		System.assert(
			String.isBlank(fxAccount.Hashed_Username__c));
		System.assert(
			String.isBlank(fxAccount.Hashed_Email__c));

		// Update account.PersonEmail

		checkRecursive.SetOfIDs = new Set<Id>();
		acc.PersonEmail = 'newEmail@gmail.com';
		update acc;

		fxAccount =
			[SELECT Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// fxAccount should get new email hashed field
		System.assert(
			String.isNotBlank(fxAccount.Hashed_Email__c),
			'Account email was not hashed');
	}

	/**
	 * fxAccount must get new hashed email field
	 * when lead.Email is updated
	 */
	@isTest
	static void setHashedOnUpdateLead() {
		TriggersUtil.disableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);

		Account acc = new TestDataFactory().createTestAccount();

		Lead lead = new Lead();
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        lead.LastName = 'Last1';
        lead.Email = 'test@abc.com';
        insert lead;

		fxAccount__c fxAccount = new fxAccount__c();
		fxAccount.Name = '12365';
		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
		fxAccount.Account__c = acc.Id;
		fxAccount.Lead__c = lead.Id;
		fxAccount.Division_Name__c = 'OANDA Global Markets';
		insert fxAccount;
		
		TriggersUtil.enableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);

		fxAccount =
			[SELECT Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// Should have empty hashed because
		// I stopped fxAccount triggers before
		System.assert(
			String.isBlank(fxAccount.Hashed_Username__c));
		System.assert(
			String.isBlank(fxAccount.Hashed_Email__c));

		// Update lead.Email

		lead.Email = 'newEmail@gmail.com';
		update lead;

		fxAccount =
			[SELECT Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// fxAccount should get new email hashed field
		System.assert(
			String.isNotBlank(fxAccount.Hashed_Email__c),
			'Account email was not hashed');
	}

	@isTest
    static void testHashedFieldsToAll() {
		TriggersUtil.disableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);

		Account acc = new TestDataFactory().createTestAccount();

		Contact contact = new Contact(
			MailingCountry = 'Canada',
			LastName ='test',
			Email = 'contact@gmail.com',
			Account = acc);
		insert contact;

		fxAccount__c fxAccount =
			new fxAccount__c(
				Name = '12365',
				Account_Email__c = acc.PersonEmail,
				RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
				Account__c = acc.Id,
				Contact__c = contact.Id,
				Division_Name__c = 'OANDA Global Markets');
		insert fxAccount;

		TriggersUtil.enableObjTriggers(
			TriggersUtil.Obj.FXACCOUNT);

		fxAccount =
			[SELECT Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				LIMIT 1];

		System.assert(
			String.isBlank(
				fxAccount.Hashed_Username__c));
		System.assert(
			String.isBlank(
				fxAccount.Hashed_Email__c));

		Test.startTest();
		
		// Set hash fields for all fxAccount records
		fxAccountHashUtil.setHashedFieldsToAll();

		Test.stopTest();

		fxAccount =
			[SELECT Hashed_Username__c,
					Hashed_Email__c
				FROM fxAccount__c
				WHERE Id = :fxAccount.Id];

		// Hashed fields should be filled by batch process

		System.assert(
			String.isNotBlank(fxAccount.Hashed_Username__c),
			'UserName hash got empty after batch');
		System.assert(String.isNotBlank(fxAccount.Hashed_Email__c),
			'Email hash got empty after batch');
	}

}