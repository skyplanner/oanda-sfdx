@isTest
public class UserTriggerHandlerTest {

    @isTest
    private static void fillUserCreatedDate(){
        User tUser;
        Test.startTest();
            tUser = new User(
                Alias = 'tuser', 
                Email = 'tuser@testemail.com',
                LastName = 'Testing', 
                EmailEncodingKey = 'UTF-8', 
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', 
                TimeZoneSidKey = 'America/Los_Angeles',
                ProfileId = UserInfo.getProfileId(),
                UserName = 'testusername01@testemail.com');
            insert tUser;
        Test.stopTest();

        tUser = [SELECT Id, CreatedDate, User_Start_Date__c FROM User Where Id = :tUser.Id];
        System.debug('tUser ===> ' + tUser);
        System.assert(
            tUser.CreatedDate.getTime() - tUser.User_Start_Date__c.getTime() < 1000,
            'There are more than 1 sec of difference. ' + tUser.CreatedDate + ' - ' + tUser.User_Start_Date__c
        );
    }

    @isTest
    private static void fillUserCreatedDateBulk(){
        User tUser;
        List<User> userList = new List<User>();
        Test.startTest();

            for(Integer i = 0; i < 250; i++)
                userList.add(
                    new User(
                        Alias = 'tuser' + i, 
                        Email = 'tuser@sptestemail.com',
                        LastName = 'Testing', 
                        EmailEncodingKey = 'UTF-8', 
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', 
                        TimeZoneSidKey = 'America/Los_Angeles',
                        ProfileId = UserInfo.getProfileId(),
                        UserName = 'testusername' + i + '@testemail.com')
                );
            insert userList;
        Test.stopTest();

        userList = [SELECT Id, CreatedDate, User_Start_Date__c FROM User WHERE Email = 'tuser@sptestemail.com'];
        System.assertEquals(userList.size(), 250, 'There are ' + userList.size());
        for(User u : userList)
            System.assert(
                u.CreatedDate.getTime() - u.User_Start_Date__c.getTime() < 60000,
                'The dates are more than 1 min appart. ' + u.CreatedDate.getTime() + ' - ' + u.User_Start_Date__c.getTime()
            );
    }
}