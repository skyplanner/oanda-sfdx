/**
 * @File Name          : LiveChatOfflineHoursCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2022, 2:19:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/9/2022, 1:49:24 PM   acantero     Initial Version
**/
public without sharing class LiveChatOfflineHoursCtrl extends BaseLiveChatCtrl {

    public Boolean isRedirect {get; private set;}
    public String htmlOffline {get; private set;}

    public String languageRegionInfo {get; private set;}

    public LiveChatOfflineHoursCtrl() {
        super();
        updateLanguageFromParams();
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
		String isRedirectParam = pageParams.get(ChatConst.IS_REDIRECT);
		isRedirect = (isRedirectParam == 'true');
        htmlOffline = prepareHtmlOffline();
    }

    String prepareHtmlOffline() {
        String result = '';
        if(isRedirect){
            ChatOfflineSettings chatSetting = ChatOfflineSettings.getDefaultInstance();
            String defaultRegion = !chatSetting.initializationFails ? chatSetting.redirectDivision : '';
            region = defaultRegion;
        }
        languageRegionInfo = 'language: ' + language + ', region: ' + region;
         //SP-13578 temporarily use the OC English template as default
        //HelpPortalRichText chatOfflineText = new HelpPortalRichText(language, 'chat_offline_message', region);
        HelpPortalRichText chatOfflineText = new HelpPortalRichText('en_US', 'chat_offline_message', 'OC');
        String chatOfflineTextHtml = chatOfflineText.getHtmlLinkFixed(region);
        if (String.isNotBlank(chatOfflineTextHtml)) {
            result = chatOfflineTextHtml;
        }
        return result;
	}

}