/**
 * @File Name          : SetMessagingRegionAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/8/2024, 3:16:21 AM
**/
public without sharing class SetMessagingRegionAction {

	@InvocableMethod(label='Set Messaging Region' callout=false)
	public static List<ActionResult> setMessagingRegion(List<ActionInfo> infoList) {  
		try {
			ExceptionTestUtil.execute();
			List<ActionResult> result = new List<ActionResult> { new ActionResult() };
			
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...      
			result[0] = setMessagingRegion(infoList[0]);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				IsLinkedToAccountOrLeadAction.class.getName(),
				ex
			);
		}
	}

	@TestVisible
	static ActionResult setMessagingRegion(ActionInfo info) {
		MessagingChatbotProcess process = 
			new MessagingChatbotProcess(info.messagingSessionId); 
		MessagingChatbotProcess.MessagingCountryInfo countryInfo = 
			process.setRegionFromCountryCode(info.countryCode);
		return prepareActionResult(countryInfo);
	}

	@TestVisible
	static ActionResult prepareActionResult(
		MessagingChatbotProcess.MessagingCountryInfo countryInfo
	) {
		ActionResult result = new ActionResult();
		if (countryInfo != null) {
			result.countryName = countryInfo.countryName;
			result.region = countryInfo.region;
			result.success = true;
		}
		return result;
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'countryCode' required = true)
		public String countryCode;
		
	}

	// *******************************************************************

	public class ActionResult {

		@InvocableVariable(label = 'success' required = false)
		public Boolean success;

		@InvocableVariable(label = 'region' required = false)
		public String region;

		@InvocableVariable(label = 'countryName' required = false)
		public String countryName;

		public ActionResult() {
			this.success = false;
		}
		
	}

}