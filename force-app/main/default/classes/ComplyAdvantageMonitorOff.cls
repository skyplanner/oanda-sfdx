public class ComplyAdvantageMonitorOff implements Queueable,Database.AllowsCallouts 
{ 
    string searchId;
    string apiKey;
    boolean isMonitored;
    
    public ComplyAdvantageMonitorOff(string psearchId, string papiKey, boolean pisMonitored)
    {
        searchId = psearchId;
        apiKey = papiKey;
        isMonitored = pisMonitored;
    }
    
    public void execute(QueueableContext context) 
    {
        if(searchId != null)
        {  
            Http http = new Http();
               
            //setup request
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchId +'/monitors?api_key='+ apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
			request.setMethod('POST');
            
            string requestBody = '{"is_monitored" :'+ isMonitored +'}';
            request.setBody(requestBody); 
            
            //make service call
            HttpResponse response = http.send(request);
			
            Logger.debug(
                'Comply Advantage Search turned off - callout',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response);
            
            //process response
            if (response.getStatusCode() == 200) 
            {
                Logger.debug(
                    'Comply Advantage Search turned off for Search Id' + searchId,
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY);
                
                /* Comply_Advantage_Search__c[] previousSearches = [select Id,
                                                                        Is_Monitored__c
                                                                From Comply_Advantage_Search__c 
                                                                where Search_Id__c =:searchId];
                if(previousSearches.size() > 0)
                {
                    previousSearches[0].Is_Monitored__c = isMonitored;
                    ComplyAdvantageHelper.updateComplyAdvantageSearch(previousSearches[0], false);                    
                } */
       		}
            else
            {
                Logger.error(
                    'Error : while Comply Advantage Search turned off for Search Id' + searchId,
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response);
            }
        }   
    }
}