/*
    /services/apexrest/api/v1/users/Documents?fxaccount_ids=a0K7j0000030KeO,a0K7j00000326jg,a0K7j0000032GKP
*/
@RestResource(urlMapping='/api/v1/users/Documents')
global without sharing class DocumentsRestResource 
{
    public static final Id liveRecordTypeId =  RecordTypeUtil.getFxAccountLiveId();
    public static final Id supportDocRecordTypeId =  RecordTypeUtil.getSupportDocumentTypeId();

    @HttpGet
    global static void getDocuments()
    {
       Blob responseBody;

       RestRequest req = RestContext.request;
       RestResponse res = RestContext.response;
       res.addHeader('Content-Type', 'application/json');
       
       String fxaccount_ids = req.params.get('fxaccount_ids');
       if(string.isNotBlank(fxaccount_ids))
       {
            string[] fxaccountIds = fxaccount_ids.split(',');

            Map<string, set<string>> fxacNameDocumentMap = getDocumentsByFxaccount(fxaccountIds);

            res.responseBody = Blob.valueOf(JSON.serialize(fxacNameDocumentMap));
            res.statusCode = 200;
       }
    }

    @HttpPost
    global static void createCRMMigrationDocument() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        Map<string, string> usernameDocumentMap = new Map<string, string>{};

        try
        {  
            String reqBody = req.requestBody.toString().trim();
           
            NewDocumentRequest newDocumentRequestBody = (NewDocumentRequest)JSON.deserialize(reqBody, NewDocumentRequest.class);

            Map<String, String[]> userDocumentsMap = (Map<String, String[]>)newDocumentRequestBody.user_documents;
            Map<String, String> usernameIdMap = (Map<String, String>)newDocumentRequestBody.username_id_map;

            Map<Id, String> fxaIdNameMap = new Map<Id, String>{};

            if(!userDocumentsMap.isEmpty())
            {
                set<string> userNames = new set<string>{};
                for(string userName : userDocumentsMap.keySet())
                {
                    String[] documents =  userDocumentsMap.get(userName);
                    if(!documents.isEmpty())
                    {
                        userNames.add(userName);
                    }
                }
                if(!userNames.isEmpty())
                {
                    Document__c[] crmMigrationDocuments = new Document__c[]{};
                    Document__c[] existingMigrationDocuments = new Document__c[]{};

                    for(fxAccount__c fxa : [select Id,
                                                Name,
                                                Account__c,
                                                (select Id From Documents__r where Name = 'CRM Migration Documents')
                                            from fxAccount__c
                                            where Name IN :userNames And RecordTypeId=:liveRecordTypeId])
                    {
                        fxaIdNameMap.put(fxa.Id, fxa.Name);

                        if(fxa.Documents__r != null && fxa.Documents__r.size() > 0)
                        {
                            existingMigrationDocuments.addAll(fxa.Documents__r);
                        }
                        
                        Document__c crmDoc = new Document__c();
                        crmDoc.Name = 'CRM Migration Documents';
                        crmDoc.fxAccount__c = fxa.Id;
                        crmDoc.Account__c = fxa.Account__c;
                        crmDoc.RecordTypeId = supportDocRecordTypeId;
                        crmDoc.Show_on_Profile__c = true;
                        crmDoc.Legacy_CRM_Migrated__c = true;

                        string userFiles = userDocumentsMap.get(fxa.Name) != null ? String.join(userDocumentsMap.get(fxa.Name), ';') : null;
                        crmDoc.CRM_Files__c = userFiles;

                        crmMigrationDocuments.add(crmDoc);
                    }

                    if(!existingMigrationDocuments.isEmpty())
                    {
                        Database.delete(existingMigrationDocuments, false);
                    }
                    if(!crmMigrationDocuments.isEmpty())
                    {
                        Database.insert(crmMigrationDocuments);

                        for(Document__c doc : crmMigrationDocuments)
                        {
                            string fxacName = fxaIdNameMap.get(doc.fxAccount__c);
                            usernameDocumentMap.put(fxacName, doc.Id);
                        }
                    }
                }
            }
            
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(JSON.serializePretty(usernameDocumentMap));
            res.statusCode = 200;                                                                 
        }
        catch(Exception ex)
        {
            res.addHeader('Content-Type', 'application/json');
            string errorInfo = ex.getMessage() + '\n' + ex.getStackTraceString();
            res.responseBody = Blob.valueOf('{ "stack trace" : '+errorInfo+'}');
            res.statusCode = 500;
        }
    }

    @HttpPut
    global static void updateFxAccount() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        try
        {  
            String reqBody = req.requestBody.toString().trim();
           
            UpdateFXaccountRequest updateFXaccountRequestBody = (UpdateFXaccountRequest)JSON.deserialize(reqBody, UpdateFXaccountRequest.class);

            String[] fxacIds = (String[])updateFXaccountRequestBody.user_ids;
            Boolean isProcessed = (Boolean)updateFXaccountRequestBody.is_processed;
           
            if(!fxacIds.isEmpty())
            {
                if(isProcessed)
                {
                    fxAccount__c[] fxaccountsUpdate = new fxAccount__c[]{};
                    for(String fxacId : fxacIds)
                    {
                        fxAccount__c fxa = new fxAccount__c(Id = fxacId);
                        fxa.Document_Migration_Status__c = 'Processed';
                        fxaccountsUpdate.add(fxa);
                    }
                    if(!fxaccountsUpdate.isEmpty())
                    {
                        Database.update(fxaccountsUpdate,false);
                    }
                }
                else
                {
                    Map<Id, Id> fxacDocumentIdMap = new Map<Id, Id>{};
                    Map<Id, Document__c> documentMap = new Map<Id, Document__c>{};
                    
                    for(Document__c doc : [select Id, 
                                                Name,
                                                fxAccount__c,
                                                fxAccount__r.Document_Migration_Status__c,
                                                CRM_Files__c
                                            From Document__c 
                                            Where fxAccount__c IN :fxacIds AND 
                                                Name = 'CRM Migration Documents'])
                    {
                        documentMap.put(doc.Id, doc);
                        fxacDocumentIdMap.put(doc.fxAccount__c, doc.Id);
                    }

                    set<Id> documentIds = documentMap.keySet();
                    Map<Id, string[]> documentAttachmentsMap = new Map<Id, string[]>{};
                
                    for(Attachment attach : [SELECT Id, Name, ParentId
                                            FROM Attachment 
                                            WHERE ParentId IN :documentIds])
                    {
                        string[] attachmentNames = documentAttachmentsMap.get(attach.ParentId);
                        if(attachmentNames == null)
                        {
                            attachmentNames = new string[]{};
                        }
                        attachmentNames.add(attach.Name);
                        documentAttachmentsMap.put(attach.ParentId, attachmentNames);
                    }
                    
                    fxAccount__c[] fxaccountsUpdate = new fxAccount__c[]{};
                    for(String fxacId : fxacIds)
                    {
                        fxAccount__c fxa = new fxAccount__c(Id = fxacId);

                        if(fxacDocumentIdMap.containsKey(fxacId))
                        {
                            Id documentId = fxacDocumentIdMap.get(fxacId);

                            Document__c doc = documentMap.get(documentId);
                            
                            string[] crmdbFiles = String.isNotBlank(doc.CRM_Files__c) ? doc.CRM_Files__c.split(';') : new string[]{};

                            string[] documentsList = documentAttachmentsMap.containsKey(documentId) ?
                                                            documentAttachmentsMap.get(documentId) : new string[]{};

                            Boolean allFilesTransferred = crmdbFiles.size() == documentsList.size();

                            fxa.Document_Migration_Status__c = allFilesTransferred ? 'Done' : 'Done_with_Failures';
                        }
                        else 
                        {
                            //no document found
                            fxa.Document_Migration_Status__c = 'Done_No_Files';
                        }
                        fxaccountsUpdate.add(fxa);
                    }

                    if(!fxaccountsUpdate.isEmpty())
                    {
                        Database.update(fxaccountsUpdate,false);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            res.addHeader('Content-Type', 'application/json');
            string errorInfo = ex.getMessage() + '\n' + ex.getStackTraceString();
            res.responseBody = Blob.valueOf('{ "stack trace" : '+errorInfo+'}');
            res.statusCode = 500;
        }
    }

    private static Map<string, set<string>> getDocumentsByFxaccount(string[] fxaccountIds)
    {
        Map<string, set<string>> fxacNameAttachmentsNameMap = new Map<string, set<string>>{};

        Map<Id, string> docIdFxacNameMap = new Map<Id, string>{};
        set<Id> documentParentIds = new set<Id>{};
        Map<Id, Id> fxAccountRelatedRecordsMap = new Map<Id, Id>{};
        Map<Id, String> fxacIdNameMap = new Map<Id, String>{};
        
        for(fxAccount__c fxa : [select Id,
                                       Name,
                                       Account__c,
                                       (select Id From Cases__r)
                                from fxAccount__c
                                where Id IN :fxaccountIds])
        {
            fxacNameAttachmentsNameMap.put(fxa.Name, new set<string>{});
            fxacIdNameMap.put(fxa.Id, fxa.Name);

            documentParentIds.add(fxa.Id);

            if(fxa.Account__c != null)
            {
                documentParentIds.add(fxa.Account__c);
                fxAccountRelatedRecordsMap.put(fxa.Account__c, fxa.Id);
            }

            if(fxa.Cases__r != null && fxa.Cases__r.size() > 0)
            {
                for(Case ca : fxa.Cases__r)
                {
                    documentParentIds.add(ca.Id);
                    fxAccountRelatedRecordsMap.put(ca.Id, fxa.Id);
                }
            }
        }

        set<Id> documentIds = new set<Id>{};
        for(Document__c doc : [select Id, 
                                      Name,
                                      Account__c,
                                      fxAccount__c,
                                      Case__c
                               From Document__c 
                               Where Account__C IN :documentParentIds OR 
                                     fxAccount__c IN :documentParentIds OR 
                                     Case__c IN :documentParentIds])
        {
            if(doc.Name != 'CRM Migration Documents')
            {
                string fxacName;
                if(string.isNotBlank(doc.fxAccount__c))
                {
                    fxacName = fxacIdNameMap.get(doc.fxAccount__c);               
                }
                if(string.isNotBlank(doc.Account__c) && string.isBlank(fxacName))
                {
                    Id fxacId = fxAccountRelatedRecordsMap.get(doc.Account__c);
                    fxacName = fxacIdNameMap.get(fxacId);
                }
                if(string.isNotBlank(doc.Case__c) && string.isBlank(fxacName))
                {
                    Id fxacId = fxAccountRelatedRecordsMap.get(doc.Case__c);
                    fxacName = fxacIdNameMap.get(fxacId);
                }

                if(string.isNotBlank(fxacName))
                {
                    docIdFxacNameMap.put(doc.Id, fxacName);
                    documentIds.add(doc.Id);
                }
            }
        }
       
        if(!documentIds.isEmpty())
        {
            for(Attachment attachment : [SELECT Id, Name, ParentId 
                                        FROM Attachment 
                                        WHERE ParentId IN :documentIds])
            {
                string fxacName = docIdFxacNameMap.get(attachment.ParentId);
                set<string> attachmentNames = fxacNameAttachmentsNameMap.get(fxacName);
                attachmentNames.add(attachment.Name);
                fxacNameAttachmentsNameMap.put(fxacName, attachmentNames);
            }

            for(ContentDocumentLink attachment : [SELECT ContentDocument.title,ContentDocument.FileExtension, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :documentIds])
            {
                if(attachment.ContentDocument != null)
                {
                    string fxacName = docIdFxacNameMap.get(attachment.LinkedEntityId);
                    set<string> attachmentNames = fxacNameAttachmentsNameMap.get(fxacName);
                    attachmentNames.add(attachment.ContentDocument.title);
                    attachmentNames.add(attachment.ContentDocument.title + '.' + attachment.ContentDocument.FileExtension);
                    fxacNameAttachmentsNameMap.put(fxacName, attachmentNames);
                }
            }
        }
        return fxacNameAttachmentsNameMap;
    }

    class NewDocumentRequest
    {
        Map<String, String[]> user_documents;
        Map<String, String> username_id_map;
    }
    class UpdateFXaccountRequest
    {
        String[] user_ids;
        boolean is_processed;
    }

}