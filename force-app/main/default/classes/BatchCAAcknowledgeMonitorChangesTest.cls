/* Name: BatchCAAcknowledgeMonitorChangesTest
 * Description : 
 * Author: Eugene Jung
 * Date : 07-28-2023
* Last Modified Date : 11-10-2023
 */
@isTest
public with sharing class BatchCAAcknowledgeMonitorChangesTest {

	@TestSetup
	static void makeData(){
		Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

		Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Removed',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '12345',
            Search_Reference__c = 'abcde',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada',
            Has_Monitor_Changes__c = true,
            Monitor_Change_Date__c = System.today()
		);
		insert search;

        Comply_Advantage_Search_Entity__c entity = new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = search.Id,
				Unique_ID__c = search.Search_Id__c + ':IWUFH46IEHR',
				Entity_Id__c = 'IWUFH46IEHR',
                Monitor_Change_Status__c = 'Removed'
		);
        insert entity;

		Comply_Advantage_Search__c search2 = new Comply_Advantage_Search__c(
			Name = 'Updated',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '23456',
            Search_Reference__c = 'bcdef',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada',
            Has_Monitor_Changes__c = true,
            Monitor_Change_Date__c = System.today()
		);
		insert search2;

        Comply_Advantage_Search_Entity__c entity2 = new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = search2.Id,
				Unique_ID__c = search.Search_Id__c + ':KWUFH46IEHR',
				Entity_Id__c = 'KWUFH46IEHR',
                Monitor_Change_Status__c = 'Updated'
		);
        insert entity2;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;
	}

    @isTest
	static void testAcknowledgeMonitorChange() {

		// testing when the the search has only removed entities

		CalloutMock mock = new CalloutMock(
			204,
			'{ "content": { "data": { "search_profile": { "name": "CA Temp" } } } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        BatchCAAcknowledgeMonitorChanges batchAckMonitorChanges = new BatchCAAcknowledgeMonitorChanges();
        Database.executeBatch(batchAckMonitorChanges);

		Test.stopTest();

        Comply_Advantage_Search__c search = [
			SELECT Id, Has_Monitor_Changes__c, Monitor_Changes_Acknowledged_Date__c
			FROM Comply_Advantage_Search__c
			WHERE Name = 'Removed'
		];

        // upon successful response, it should update Has_Monitor_Changes__c to false and populate Monitor_Changes_Acknowledged_Date__c
        System.assertEquals(false, search.Has_Monitor_Changes__c);
        System.assertNotEquals(null, search.Monitor_Changes_Acknowledged_Date__c);
	}

	@isTest
	static void testAcknowledgeMonitorChangeNegative() {

		CalloutMock mock = new CalloutMock(
			204,
			'{ "content": { "data": { "search_profile": { "name": "CA Temp" } } } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        BatchCAAcknowledgeMonitorChanges batchAckMonitorChanges = new BatchCAAcknowledgeMonitorChanges();
        Database.executeBatch(batchAckMonitorChanges);

		Test.stopTest();

        Comply_Advantage_Search__c search = [
			SELECT Id, Has_Monitor_Changes__c, Monitor_Changes_Acknowledged_Date__c
			FROM Comply_Advantage_Search__c
			WHERE Name = 'Updated'
		];

        // should not make any change to the search
        System.assertEquals(true, search.Has_Monitor_Changes__c);
        System.assertEquals(null, search.Monitor_Changes_Acknowledged_Date__c);
	}

	@isTest
	static void testAcknowledgeMonitorChangeAllMatchStatus() {

		CalloutMock mock = new CalloutMock(
			204,
			'{ "content": { "data": { "search_profile": { "name": "CA Temp" } } } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();
		
		Comply_Advantage_Search__c[] searchBefore = [
			SELECT Id, Search_Id__c, Has_Monitor_Changes__c, Monitor_Changes_Acknowledged_Date__c
			FROM Comply_Advantage_Search__c
		];

		List<String> filterList = new List<String>();
		for(Comply_Advantage_Search__c search : searchBefore){
			filterList.add(search.Search_Id__c);
		}

        BatchCAAcknowledgeMonitorChanges batchAckMonitorChanges = new BatchCAAcknowledgeMonitorChanges(' Search_Id__c IN :filterList', filterList, true);
        Database.executeBatch(batchAckMonitorChanges);

		Test.stopTest();

        Comply_Advantage_Search__c[] searchListAfter = [
			SELECT Id, Has_Monitor_Changes__c, Monitor_Changes_Acknowledged_Date__c
			FROM Comply_Advantage_Search__c
		];

        // upon successful response, it should update Has_Monitor_Changes__c to false and populate Monitor_Changes_Acknowledged_Date__c for all searches
		// regardless of the entity change status
        System.assertEquals(false, searchListAfter[0].Has_Monitor_Changes__c);
		System.assertEquals(false, searchListAfter[1].Has_Monitor_Changes__c);
        System.assertNotEquals(null, searchListAfter[0].Monitor_Changes_Acknowledged_Date__c);
		System.assertNotEquals(null, searchListAfter[1].Monitor_Changes_Acknowledged_Date__c);
	}
}