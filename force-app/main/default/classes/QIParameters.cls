public without sharing class QIParameters {
    
    public Map<String, List<Decimal>> valuesByDeductionParameter {get; private set;}
    public QIParameters(String pCaseType, String pTenureTier) {
        valuesByDeductionParameter = new Map<String, List<Decimal>>();
        for(QI_Parameter__c qi : [
                SELECT Id,
                    Name,
                    Deduction_Parameter__c,
                    Tenure_1_year__c,
                    Tenure_0_5_year__c,
                    Tenure_greater_1_year__c
                FROM 
                    QI_Parameter__c
                WHERE
                    Active_Inactive__c = 'Yes'
                AND Deduction_Point__c =: pCaseType]){
            System.debug('qi ===> ' + qi);
            if(valuesByDeductionParameter.containsKey(qi.Deduction_Parameter__c))
                valuesByDeductionParameter
                    .get(qi.Deduction_Parameter__c)
                    .add((Decimal)qi.get(pTenureTier));
            else
                valuesByDeductionParameter
                    .put(
                        qi.Deduction_Parameter__c,
                        new List<Decimal>{(Decimal)qi.get(pTenureTier)});
        }
    }
}