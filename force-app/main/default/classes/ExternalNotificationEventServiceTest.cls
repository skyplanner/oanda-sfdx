@isTest
public with sharing class ExternalNotificationEventServiceTest {
    
    @isTest
    public static void createEventTest(){

        Account acc = new TestDataFactory().createTestAccount();
        Test.startTest();
        External_Notification_Event__e ev = ExternalNotificationEventService.createEvent('TestCase', Utilities.serialize(acc, true), acc.Id, 'account');
        Test.stopTest();

        System.assertEquals('TestCase', ev.Origin__c);
        System.assertEquals(acc.Id, ev.RelatedRecordId__c);
        System.assertEquals('account', ev.SObject__c);
    
    }

    @isTest
    public static void createEventFromRecordTest(){

        Account acc = new TestDataFactory().createTestAccount();
        Test.startTest();
        External_Notification_Event__e ev = ExternalNotificationEventService.createEvent('TestCase', acc);
        Test.stopTest();

        System.assertEquals('TestCase', ev.Origin__c);
        System.assertEquals(acc.Id, ev.RelatedRecordId__c);
        System.assertEquals('account', ev.SObject__c);
    }

    @isTest
    public static void createAndPublishEventTest(){

        Account acc = new TestDataFactory().createTestAccount();
        Test.startTest();
        ExternalNotificationEventService.createAndPublishEvent('TestCase', Utilities.serialize(acc, true), acc.Id, 'account');
        Test.stopTest();
    
    }

    @isTest
    public static void createEventAndPublishFromRecordTest(){

        Account acc = new TestDataFactory().createTestAccount();
        Test.startTest();
        ExternalNotificationEventService.createAndPublishEvent('TestCase', acc);
        Test.stopTest();
    }
}