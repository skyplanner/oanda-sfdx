/**
 * @File Name          : MessagingChatbotProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 7:54:21 PM
**/
public without sharing class MessagingChatbotProcess {

	final String messagingSessionId;

	public MessagingChatbotProcess(String messagingSessionId) {
		this.messagingSessionId = messagingSessionId;
	}

	public MessagingCountryInfo setRegionFromCountryCode(String countryCode) {
		MessagingSession session = 
			MessagingSessionRepo.getSummaryById(messagingSessionId);
		MessagingCountryInfo result = setRegionFromCountryCode(
			countryCode, // countryCode
			session // session
		);
		return result;
	}

	@TestVisible
	MessagingCountryInfo setRegionFromCountryCode(
		String countryCode,
		MessagingSession session
	) {
		MessagingCountryInfo result = null;
		if (
			String.isBlank(countryCode) ||
			(session == null)
		) {
			return result;
		}
		// else...
		Country_Code__mdt country = 
			CountryCodeMetadataUtil.getRegionInfoByCountryCode(countryCode);

		if (country != null) {
			result = new MessagingCountryInfo();
			result.region = country.Region__c;
			result.countryName = 
				String.isNotBlank(country.Alternative_Name__c)
				? country.Alternative_Name__c
				: country.Country__c;
			
			session.Region__c = country.Region__c;
			MessagingEndUser endUser = new MessagingEndUser(
				Id = session.MessagingEndUserId,
				Country_Code__c = countryCode
			);
			SPDataUtils.updateIfNoTesting(session);
			SPDataUtils.updateIfNoTesting(endUser);
		}
		return result;
	}
	
	public Boolean setLanguageAndInquiryNature(
		String endUserId,
		String languageCode,
		String inquiryNature
	) {
		MessagingSession session = 
			MessagingSessionRepo.getSummaryById(messagingSessionId);
		Boolean result = setLanguageAndInquiryNature(
			endUserId, // endUserId
			languageCode, // languageCode
			inquiryNature, // inquiryNature
			session // session
		);
		return result;
	}

	@TestVisible
	Boolean setLanguageAndInquiryNature(
		String endUserId,
		String languageCode,
		String inquiryNature,
		MessagingSession session
	) {
		if (
			String.isBlank(languageCode) ||
			(session == null)
		) {
			return false;
		}
		// else...
		session.Language_Code__c = languageCode;

		if (session.CaseId == null) {
			session.Case_Language_Code__c = languageCode;
		}
		
		MessagingEndUser endUser = new MessagingEndUser(
			Id = endUserId,
			Language_Code__c = languageCode
		);

		if (String.isNotBlank(inquiryNature)) {
			session.Inquiry_Nature__c = inquiryNature;
		}

		SPDataUtils.updateIfNoTesting(session);
		SPDataUtils.updateIfNoTesting(endUser);
		return true;
	}

	public MessagingAccountCheckInfo checkLinkToAccountOrLead(String userEmail) {
		MessagingSession session = 
			MessagingSessionRepo.getSummaryById(messagingSessionId);
		MessagingAccountProcess accountProcess = 
			new MessagingAccountProcess(session);
		MessagingAccountCheckInfo result = 
			accountProcess.checkLinkToAccountOrLead(userEmail);
		return result;
	}

	public MessagingAccountCheckInfo checkLinkToFxAccount(String username) {
		MessagingSession session = 
			MessagingSessionRepo.getSummaryById(messagingSessionId);
		MessagingAccountProcess accountProcess = 
			new MessagingAccountProcess(session);
		MessagingAccountCheckInfo result = 
			accountProcess.checkLinkToFxAccount(username);
		return result;
	}
	
	public ID createLeadFromMsgSession() {
		MessagingSession session = 
			MessagingSessionRepo.getLeadInfoById(messagingSessionId);
		MessagingLeadProcess leadProcess = new MessagingLeadProcess(session);
		Lead leadObj = leadProcess.createLeadFromMsgSession();
		registerLeadCreation(
			session, // session
			leadObj // newLead
		);
		return leadObj?.Id;
	}

	public Case createCaseFromMsgSession() {
		MessagingSession session = 
			MessagingSessionRepo.getCaseInfoById(messagingSessionId);
		MessagingCaseProcess caseProcess = new MessagingCaseProcess(session);
		Case caseObj = caseProcess.createCaseFromMsgSession();
		updateMsgSessionLookup(
			session, // session
			caseObj, // linkedEntity
			'CaseId' // lookupFieldName
		);
		return caseObj;
	}

	public void updateMessagingCaseOrigin() {
		MessagingSession session = 
			MessagingSessionRepo.getCaseInfoById(messagingSessionId);
		MessagingCaseProcess caseProcess = new MessagingCaseProcess(session);
		caseProcess.updateCaseOrigin();
	}

	public Boolean updateMessagingCaseLanguage(String languageCode) {
		MessagingSession session = 
			MessagingSessionRepo.getSummaryById(messagingSessionId);
		Boolean result = updateMessagingCaseLanguage(
			languageCode, // languageCode
			session // session
		);
		return result;
	}

	@TestVisible
	Boolean updateMessagingCaseLanguage(
		String languageCode,
		MessagingSession session
	) {
		if (
			String.isBlank(languageCode) ||
			(session == null) ||
			(session.CaseId != null)
		) {
			return false;
		}
		// else...
		session.Case_Language_Code__c = languageCode;
		SPDataUtils.updateIfNoTesting(session);
		return true;
	}

	public Boolean sendAuthenticationCode(ID fxAccountId) {
		MessagingSession session = 
			MessagingSessionRepo.getAuthenticationInfoById(messagingSessionId);
		Boolean result = sendAuthenticationCode(
			fxAccountId, // fxAccountId
			session // session
		);
		return result;
	}

	@TestVisible
	Boolean sendAuthenticationCode(
		ID fxAccountId,
		MessagingSession session
	) {
		if (
			(fxAccountId == null) ||
			(session == null)
		) {
			return false;
		}
		// else...
		AuthenticationCodeProcess authCodeProcess = 
			new AuthenticationCodeProcess(
				fxAccountId, // fxAccountId
				session.CaseId // caseId
			);
		Boolean result = authCodeProcess.sendAuthenticationCode(
			session.Language_Code__c
		);
		return result;
	}

	public Boolean verifyAuthenticationCode(
		ID fxAccountId,
		String code
	) {
		MessagingSession session = 
			MessagingSessionRepo.getAuthenticationInfoById(messagingSessionId);
		Boolean result = verifyAuthenticationCode(
			fxAccountId, // fxAccountId
			code, // code
			session // session
		);
		return result;
	}

	@TestVisible
	Boolean verifyAuthenticationCode(
		ID fxAccountId,
		String code,
		MessagingSession session
	) {
		if (
			(fxAccountId == null) ||
			String.isBlank(code) ||
			(session == null)
		) {
			return false;
		}
		// else...
		AuthenticationCodeProcess authCodeProcess = 
			new AuthenticationCodeProcess(
				fxAccountId, // fxAccountId
				session.CaseId // caseId
			);
		Boolean result = authCodeProcess.verifyAuthenticationCode(code);
		session.User_Authenticated__c = result;
		SPDataUtils.updateIfNoTesting(session);
		return result;
	}

	@TestVisible
	void registerLeadCreation(
		MessagingSession session,
		Lead newLead
	) {
		if (
			(session != null) &&
			(newLead != null)
		) {
			session.New_Lead__c = true;
			session.LeadId = newLead.Id;

			MessagingEndUser endUser = new MessagingEndUser(
				Id = session.MessagingEndUserId,
				LeadId = newLead.Id
			);

			SPDataUtils.updateIfNoTesting(session);
			SPDataUtils.updateIfNoTesting(endUser);
		}
	}

	@TestVisible
	void updateMsgSessionLookup(
		MessagingSession session,
		SObject linkedEntity,
		String lookupFieldName
	) {
		if (
			(session != null) &&
			(linkedEntity != null)
		) {
			session.put(lookupFieldName, linkedEntity.Id);
			SPDataUtils.updateIfNoTesting(session);
		}
	}

	public static Boolean checkMsgAgentAvailability(String languageCode) {
		String messagingChannelDevName = 
			SPSettingsManager.getSetting(
				MessagingConst.MSG_CHANNEL_DEV_NAME_SETTING
			);
		Boolean result = ChatLanguagesManager.languageIsAvailable(
			languageCode, // languageCode
			messagingChannelDevName // chatChannelDevName
		);
		ServiceLogManager.getInstance().log(
			'checkMsgAgentAvailability -> ' + 
			'languageCode: ' + languageCode +
			', result: ' + result, // msg
			MessagingChatbotProcess.class.getName() // category
		);
		return result;
	}

	public static Boolean isLangAvailableForChatbot(String languageCode) {
		Boolean result = 
			ServiceLanguagesManager.isLangAvailableForChatbot(languageCode);
		return result;
	}

	public static String getLanguageName(String languageCode) {
		String result = 'UNKNOWN';
		String languageCustomLabel = 
			ServiceLanguagesManager.getLangCustomLabel(
				languageCode
			);

		if (String.isNotBlank(languageCustomLabel)) {
			result = System.Label.get(
				'', // namespace
				languageCustomLabel // label
			);
		}
		return result;
	}

	// ***************************************************************

	public class MessagingCountryInfo {

		public String region {get; set;}
		public String countryName {get; set;}

	}

}