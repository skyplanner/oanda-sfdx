/**
 * @File Name          : ServiceRoutingInfoFactoryTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 5:37:46 PM
**/
@IsTest
private without sharing class ServiceRoutingInfoFactoryTest {

	@IsTest
	static void createNewRecord() {
		Test.startTest();
		ServiceRoutingInfoFactory instance = 
			ServiceRoutingInfoFactory.getInstance();
		ServiceRoutingInfo result = instance.createNewRecord(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
		Assert.isNotNull(
			result.customerTypeProvider, 
			'customerTypeProvider must have been initialized'
		);
	}
	
}