/**
 * @File Name          : ServiceLanguagesManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/8/2023, 3:22:29 AM
**/
public without sharing class ServiceLanguagesManager {

	public static final String LANGUAGE_CODE_SEPARATOR = '_';

	public static String getLangCustomLabel(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = languageRec?.Custom_Label_Name__c;
		return result;
	}

	/**
	 * In case the languageCode has the form: <language>_<country>
	 * first the search is done using the original value and 
	 * if no results are obtained then it eliminates the country part 
	 * from the languageCode and makes a new search with that 
	 * reduced languageCode
	 */
	public static String getLanguageByFamily(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		String result = getLanguage(languageCode, null);
		if (
			String.isBlank(result) &&
			languageCode.contains(LANGUAGE_CODE_SEPARATOR)
		) {
			String generalLangCode = 
				languageCode.substringBefore(LANGUAGE_CODE_SEPARATOR);
			result = getLanguage(generalLangCode, null);
		}
		return result;
	}

	public static String getLanguage(String languageCode) {
		return getLanguage(
			languageCode, // languageCode
			languageCode // defaultValue
		);
	}

	public static String getLanguage(
		String languageCode,
		String defaultValue
	) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = languageRec?.Language__c;
		return String.isNotBlank(result)
			? result
			: defaultValue;
	}

	public static String getAlternativeName(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = languageRec?.Alternative_Name__c;
		return result;
	}

	/**
	 * In case the languageCode has the form: <language>_<country>
	 * first the search is done using the original value and 
	 * if no results are obtained then it eliminates the country part 
	 * from the languageCode and makes a new search with that 
	 * reduced languageCode
	 */
	public static String getLanguageCorrectedByFamily(
		String languageCode
	) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		String result = getLangCorrectedByLangCode(languageCode);
		if (
			String.isBlank(result) &&
			languageCode.contains(LANGUAGE_CODE_SEPARATOR)
		) {
			String generalLangCode = 
				languageCode.substringBefore(LANGUAGE_CODE_SEPARATOR);
			result = getLangCorrectedByLangCode(generalLangCode);
		}
		return result;
	}

	public static String getLangCorrectedByLangCode(
		String languageCode
	) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = null;
		
		if (languageRec != null) {
			result = getLanguageCorrected(
				languageRec.Language__c,
				languageRec.Language_Corrected__c
			);
		}
		return result;
	}

	@TestVisible
	static String getLanguageCorrected(
		String language,
		String languageCorrected
	) {
		return String.isNotBlank(languageCorrected)
			? languageCorrected
			: language;
	}

	public static String getLanguageCode(String language) {
		if (String.isBlank(language)) {
			return null;
		}
		//else...
		String result = [
			SELECT
				DeveloperName
			FROM
				Service_Language__mdt
			WHERE
				Language__c = :language
		]?.DeveloperName;
		//...
		if (result == null) {
			result = [
				SELECT
					DeveloperName
				FROM
					Service_Language__mdt
				WHERE
					Alternative_Name__c = :language
			]?.DeveloperName;
		}
		return String.isNotBlank(result)
			? result
			: language;
	}

	public static String getLangCodeByLangCorrected(String languageCorrected) {
		if (String.isBlank(languageCorrected)) {
			return null;
		}
		//else...
		String result = [
			SELECT
				DeveloperName
			FROM
				Service_Language__mdt
			WHERE
				Language__c = :languageCorrected
		]?.DeveloperName;
		//...
		if (result == null) {
			result = [
				SELECT
					DeveloperName
				FROM
					Service_Language__mdt
				WHERE
					Language_Corrected__c = :languageCorrected
			]?.DeveloperName;
		}
		return String.isNotBlank(result)
			? result
			: languageCorrected;
	}

	public static String getSkillByLangCode(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = languageRec?.Skill_Dev_Name__c;
		return result;
	}

	public static String getChatButtonIdByLangCode(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		String result = languageRec?.Chat_Button_Id__c;
		return result;
	}

	public static Set<String> getLangSkillsEnabledForChat() {
		Set<String> result = new Set<String>();
		List<Service_Language__mdt> serviceLangList = [
			SELECT
				Skill_Dev_Name__c
			FROM
				Service_Language__mdt
			WHERE
				Available_for_Chat__c = TRUE
		];
		for(Service_Language__mdt serviceLang : serviceLangList) {
			result.add(serviceLang.Skill_Dev_Name__c);
		}
		return result;
	}

	public static Boolean isLangAvailableForChatbot(String languageCode) {
		if (String.isBlank(languageCode)) {
			return false;
		}
		//else...
		Service_Language__mdt languageRec = getLanguageRecord(languageCode);
		Boolean result = (languageRec?.Available_for_Chatbot__c == true);
		return result;
	}

	public static Service_Language__mdt getLanguageRecord(String languageCode) {
		if (String.isBlank(languageCode)) {
			return null;
		}
		//else...
		List<Service_Language__mdt> recList = [
			SELECT
				Alternative_Name__c,
				Available_for_Chatbot__c,
				Chat_Button_Id__c,
				Custom_Label_Name__c,
				Language__c,
				Language_Corrected__c,
				Skill_Dev_Name__c
			FROM Service_Language__mdt
			WHERE DeveloperName = :languageCode
		];
		Service_Language__mdt result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

}