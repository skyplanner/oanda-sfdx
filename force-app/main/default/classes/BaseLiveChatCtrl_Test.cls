/**
 * @File Name          : BaseLiveChatCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2022, 11:02:25 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/11/2022, 10:52:52 AM   acantero     Initial Version
**/
@IsTest
private without sharing class BaseLiveChatCtrl_Test {

    @IsTest
    static void updateLanguageFromParams1() {
        // This page is used which is linked to a child instance of this class
        PageReference pageRef = Page.LiveChatLanguage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ChatConst.LANGUAGE_PREFERENCE, 'de');
        ApexPages.currentPage().getParameters().put(ChatConst.LANGUAGE_PREFERENCE_OFFLINE, 'es');
        Test.startTest();
        BaseLiveChatCtrl instance = new BaseLiveChatCtrl();
        String result = instance.updateLanguageFromParams();
        Test.stopTest();
        System.assertEquals('de', result, 'Invalid result');
    }

    @IsTest
    static void updateLanguageFromParams2() {
        // This page is used which is linked to a child instance of this class
        PageReference pageRef = Page.LiveChatLanguage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ChatConst.LANGUAGE_PREFERENCE_OFFLINE, 'de');
        Test.startTest();
        BaseLiveChatCtrl instance = new BaseLiveChatCtrl();
        String result = instance.updateLanguageFromParams();
        Test.stopTest();
        System.assertEquals('de', result, 'Invalid result');
    }

    @IsTest
    static void offlineHours() {
        Test.startTest();
        BaseLiveChatCtrl instance = new BaseLiveChatCtrl();
        Boolean result = instance.offlineHours;
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid result');
    }
    
}