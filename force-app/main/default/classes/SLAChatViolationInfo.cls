/**
 * @File Name          : SLAChatViolationInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/27/2024, 4:50:38 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/8/2021, 11:45:07 AM   acantero     Initial Version
**/
public without sharing virtual class SLAChatViolationInfo extends SLAViolationInfo {

    public String chatId {get; set;}
    public String chatNumber {get; set;}
    public String accountName {get; set;}
    public Boolean isNotifiable {get; set;}

    public SLAChatViolationInfo(
        String chatId,
        String chatNumber,
        String violationType
    ) {
        this.chatId = chatId;
        this.chatNumber = chatNumber;
        this.violationType = violationType;
        this.channel = SLAConst.CHAT_CHANNEL;
    }

}