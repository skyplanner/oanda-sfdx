/**
 * @File Name          : SLAViolationNotificationBuilder.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/20/2024, 2:08:53 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/20/2024, 1:55:06 PM   aniubo     Initial Version
 **/
public interface SLAViolationNotificationBuilder {
	string buildBody(String body, SLAViolationInfo violationInfo);
	string buildSubject(String subject, SLAViolationInfo violationInfo);
}