/**
 * @File Name          : SPEmailUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : Ariel Niubo
 * @Last Modified On   : 08-02-2022
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/17/2021, 11:02:03 AM   acantero     Initial Version
**/
public without sharing class SPEmailUtil {

    public static final String EMAIL_REGEX = 
        '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{1,}|[0-9]{1,3})';

    public static Boolean checkEmail(String emailValue) {
        if (String.isBlank(emailValue)) {
            return false;
        }
        //else...
        Boolean result = Pattern.matches(EMAIL_REGEX, emailValue);
        return result;
    }
	//Schedule Emails 
	private static IScheduledEmailSettingManager scheduledEmailSettingManager;

	public static IScheduledEmailSettingManager getScheduledEmailSettingManager() {
		if (scheduledEmailSettingManager == null) {
			scheduledEmailSettingManager = new ScheduledEmailSettingManager();
		}
		return scheduledEmailSettingManager;
	}
	public static List<EmailMessage> getNoDraftEmail(
		List<EmailMessage> emailMsgs
	) {
		return filterEmailMessageList(emailMsgs, false);
	}
	private static List<EmailMessage> filterEmailMessageList(
		List<EmailMessage> emailMsgs,
		Boolean inDraft
	) {
		List<EmailMessage> filteredEmailList = new List<EmailMessage>();
		for (EmailMessage message : emailMsgs) {
			if (inDraft) {
				if (EmailMessageManager.isDraft(message)) {
					filteredEmailList.add(message);
				}
			} else {
				if (!EmailMessageManager.isDraft(message)) {
					filteredEmailList.add(message);
				}
			}
		}
		return filteredEmailList;
	}

	public static void preventDeleteDraftEmailMessage(List<EmailMessage> emailMsgs) {
		List<EmailMessage> emailInDraft = filterEmailMessageList(
			emailMsgs,
			true
		);
		if (!emailInDraft.isEmpty()) {
			EmailMessageManager emailMsgManager = new EmailMessageManager(
				getScheduledEmailSettingManager()
			);
			if (!emailMsgManager.canDeleteDraft(emailInDraft)) {
				addError(
					emailInDraft,
					System.Label.Discard_Draft_Error_Message
				);
			}
		}
	}
	public static void deleteScheduledEmail(List<EmailMessage> emailMsgs) {
 
		Map<Id, EmailMessage> emailDraftMap = new Map<Id, EmailMessage>();
		for (EmailMessage message : emailMsgs) {
			if (EmailMessageManager.isDraft(message)) {
				emailDraftMap.put(message.Id, message);
			}
		}
		if (!emailDraftMap.isEmpty()) {
			ScheduledEmailManager scheduledManager = new ScheduledEmailManager(
				getScheduledEmailSettingManager()
			);
			scheduledManager.deleteByEmailMessage(
				new List<Id>(emailDraftMap.keySet())
			);
			EmailMessageEventManager.publishEvents(emailDraftMap.values());
		}
	}

	public static void draftEmailSent(
		Map<Id, EmailMessage> newMap,
		Map<Id, EmailMessage> oldMap
	) {
		List<EmailMessage> emailMsgs = new List<EmailMessage>();
		for (Id key : newMap.keySet()) {

			if(String.isEmpty(key))
				continue;

			if (
				newMap.get(key).Status ==
				EmailMessageRepo.EMAIL_MESSAGE_STATUS_SENT &&
				oldMap.get(key).Status ==
				EmailMessageRepo.EMAIL_MESSAGE_STATUS_DRAFT
			) {
				emailMsgs.add(oldMap.get(key));
			}
		}
		if (!emailMsgs.isEmpty()) {
			deleteScheduledEmail(emailMsgs);
		}
	}
	private static void addError(
		List<EmailMessage> emailMsgs,
		String errorMessage
	) {
		for (EmailMessage msg : emailMsgs) {
			msg.addError(errorMessage);
		}
	}

}