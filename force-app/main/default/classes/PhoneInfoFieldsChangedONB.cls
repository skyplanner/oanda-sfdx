public with sharing class PhoneInfoFieldsChangedONB extends OutgoingNotificationBus {
    private String recordId = '';
    public final static String MISSING_FX_TRADE_USER_ID = 'PhoneInfoFieldsChangedONB - missing fxTrade_User_Id__c';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        Map<String, Object> wrapper = new Map<String, Object>();

        for (String field : OutgoingNotificationBusConstants.phoneInfoUpdate.keySet()) {
            if (newRecord.get(field) != oldRecord.get(field)) {
                wrapper.put(OutgoingNotificationBusConstants.phoneInfoUpdate.get(field), newRecord.get(field) ?? '');
            }
        }

        if (!wrapper.isEmpty() && ((fxAccount__c) newRecord).fxTrade_User_Id__c == null) {
            addEventsError(newRecord.Id, MISSING_FX_TRADE_USER_ID);
            return null;
        }

        if (!wrapper.isEmpty()) {
            wrapper.put('TYPE', 'Telephone');
            wrapper.put('priority', 0);
            return JSON.serialize(new Map<String, List<Object>>{'contacts_phone' => new List<Object>{wrapper}});
        }
        return null;
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize( new Map<String, String> {
                'env' => 'live',
                'recordId' =>  ((fxAccount__c) newRecord).Id,
                'tradeUserId' => String.valueOf((Integer)((fxAccount__c) newRecord).fxTrade_User_Id__c)
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME).updateContactPhone(
                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
                body);
//        new UserCallout().updateContactPhone(
//                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
//                body);
    }

    public override String getRecordId() {
        return recordId;
    }
}