/**
 * @File Name          : CheckMsgAgentAvailabilityAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/24/2024, 10:49:27 PM
**/
public without sharing class CheckMsgAgentAvailabilityAction { 

	@InvocableMethod(label='Check Messaging Agent Availability' callout=false)
	public static List<Boolean> checkMsgAgentAvailability(
		List<String> languageCodes
	) {
		try {
			ExceptionTestUtil.execute();    
			List<Boolean> result = new List<Boolean>{ false };
			
			if (
				(languageCodes == null) ||
				languageCodes.isEmpty()
			) {
				return result;
			}
			// else...
			String languageCode = languageCodes[0];
			result[0] = MessagingChatbotProcess.checkMsgAgentAvailability(
				languageCode
			);
			ServiceLogManager.getInstance().log(
				'checkMsgAgentAvailability -> ' +
				'languageCode: ' + languageCode +
				', result (details)', // msg
				CheckMsgAgentAvailabilityAction.class.getName(), // category
				result // details
			);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CheckMsgAgentAvailabilityAction.class.getName(),
				ex
			);
		}
	} 

}