/**
 * @File Name          : CaseHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/6/2022, 5:34:32 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/5/2021, 11:09:30 AM   acantero     Initial Version
**/
@IsTest
private without sharing class CaseHelper_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        //...
        ID newCustomerLeadId = OmnichanelRoutingTestDataFactory.createNewCustomerLead();
        //EMAIL_FRONTDESK and EMAIL_API are only routed directly if 
        //the subject and description fields are empty
        Case newCustomerLeadCase = new Case(
            Lead__c = newCustomerLeadId,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        //disable trigger to avoid automatic routing
        SPCaseTriggerHandler.enabled = false;
        //...
        insert newCustomerLeadCase;
        //enable trigger again
        SPCaseTriggerHandler.enabled = true;
    }

    //1 - invalid caseIdSet -> returns an empty list
    //2 - valid caseIdSet -> returns the found case
    @IsTest
    static void getRoutingInfoById() {
        String caseId = [select Id from Case limit 1].Id;
        Set<ID> caseIdSet = new Set<ID>{caseId};
        Test.startTest();
        List<Case> result1 = CaseHelper.getRoutingInfoById(null);
        List<Case> result2 = CaseHelper.getRoutingInfoById(caseIdSet);
        Test.stopTest();
        System.assertEquals(0, result1.size(), 'Invalid value');
        System.assertEquals(1, result2.size(), 'Invalid value');
    }

    //1 - invalid caseIdSet -> returns an empty list
    //2 - valid caseIdSet -> returns the found case
    @IsTest
    static void getNotRoutedOrAssigned() {
        String caseId = [select Id from Case limit 1].Id;
        Set<ID> caseIdSet = new Set<ID>{caseId};
        Test.startTest();
        List<Case> result1 = CaseHelper.getNotRoutedOrAssigned(null);
        List<Case> result2 = CaseHelper.getNotRoutedOrAssigned(caseIdSet);
        Test.stopTest();
        System.assertEquals(0, result1.size(), 'Invalid value');
        System.assertEquals(1, result2.size(), 'Invalid value');
    }

    //1 - invalid caseId -> returns null
    //2 - valid caseId -> returns the found case
    @IsTest
    static void getCloseInfoById() {
        String caseId = [select Id from Case limit 1].Id;
        Test.startTest();
        Case result1 = CaseHelper.getCloseInfoById(null);
        Case result2 = CaseHelper.getCloseInfoById(caseId);
        Test.stopTest();
        System.assertEquals(null, result1, 'Invalid value');
        System.assertEquals(caseId, result2.Id, 'Invalid value');
    }

    //invalid newVal -> return false
    //vali newVal -> return true
    @IsTest
    static void isValidLanguageCorrectedValue() {
        Test.startTest();
        Boolean result1 = CaseHelper.isValidLanguageCorrectedValue('fake_value');
        Boolean result2 = CaseHelper.isValidLanguageCorrectedValue('English');
        Test.stopTest();
        System.assertEquals(false, result1, 'Invalid value');
        System.assertEquals(true, result2, 'Invalid value');
    }

    //1 - invalid languageCorrected -> return defaultLanguage
    //2 - languageCorrected ok -> return languageCorrected
    @IsTest
    static void getValidLanguageCorrected() {
        Test.startTest();
        String result1 = CaseHelper.getValidLanguageCorrected(
            'fake_value',
            'English'
        );
        String result2 = CaseHelper.getValidLanguageCorrected(
            'Spanish',
            'English'
        );
        Test.stopTest();
        System.assertEquals('English', result1,'Invalid value');
        System.assertEquals('Spanish', result2, 'Invalid value');
    }

    @IsTest
    static void getClosedStatuses() {
        Test.startTest();
        List<String> result = CaseHelper.getClosedStatuses();
        Test.stopTest();
        System.assertNotEquals(null, result,'Invalid result');
    }

}