/**
 * @File Name          : ChatLanguagesManagerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 10:29:12 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 9:57:41 AM   aniubo     Initial Version
 **/
@isTest
private class ChatLanguagesManagerTest {
	@isTest
	private static void testLanguageIsAvailable() {
		Test.startTest();
		Boolean languageIsAvailable = ChatLanguagesManager.languageIsAvailable(
			'de'
		);
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Language should not be available because not agent available'
		);
		languageIsAvailable = ChatLanguagesManager.languageIsAvailable('45rg');

		Assert.areEqual(
			false,
			languageIsAvailable,
			'Language should not be available because language code is not valid'
		);
		Test.stopTest();

		// Asserts
	}

	@IsTest
	static void testGetInstance() {
		Test.startTest();
		ChatLanguagesManager languagesManager = ChatLanguagesManager.getInstance();
		Assert.areEqual(
			true,
			languagesManager != null,
			'Instance should not be null'
		);
		Test.stopTest();
		Set<String> langSkillEnabledSet;
		langSkillEnabledSet = ServiceLanguagesManager.getLangSkillsEnabledForChat();

		Assert.areEqual(
			langSkillEnabledSet.size(),
			languagesManager.langSkillEnabledSet.size(),
			'langSkillEnabledSet should be the same'
		);
		Assert.areEqual(
			0,
			languagesManager.langSkillCodeAvailableSet.size(),
			'langSkillCodeAvailableSet should be empty'
		);
	}

	@isTest
	private static void testCheckSkillProperties() {
		// Test data setup
		Test.startTest();
		ChatLanguagesManager languagesManager = ChatLanguagesManager.getInstance();
		languagesManager.langSkillCodeAvailableSet = new Set<String>();
		Boolean languageIsAvailable = languagesManager.englishLangAvailable;

		Assert.areEqual(
			false,
			languageIsAvailable,
			'English Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.ENGLISH_SKILL_CODE
		);
		languageIsAvailable = languagesManager.englishLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'English Language should  be available'
		);

		languageIsAvailable = languagesManager.chineseLangAvailable;

		Assert.areEqual(
			false,
			languageIsAvailable,
			'Chinese Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.CHINESE_SKILL_CODE
		);
		languageIsAvailable = languagesManager.chineseLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Chinese Language should  be available'
		);

		languageIsAvailable = languagesManager.germanLangAvailable;

		Assert.areEqual(
			false,
			languageIsAvailable,
			'German Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.GERMAN_SKILL_CODE
		);
		languageIsAvailable = languagesManager.germanLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'German Language should  be available'
		);

		languageIsAvailable = languagesManager.spanishLangAvailable;

		Assert.areEqual(
			false,
			languageIsAvailable,
			'Spanish Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.SPANISH_SKILL_CODE
		);
		languageIsAvailable = languagesManager.spanishLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Spanish Language should  be available'
		);

		languageIsAvailable = languagesManager.russianLangAvailable;

		Assert.areEqual(
			false,
			languageIsAvailable,
			'Russian Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.RUSSIAN_SKILL_CODE
		);
		languageIsAvailable = languagesManager.russianLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Russian Language should  be available'
		);

		languageIsAvailable = languagesManager.polishLangAvailable;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Polish Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.POLISH_SKILL_CODE
		);
		languageIsAvailable = languagesManager.polishLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Polish Language should  be available'
		);

		languageIsAvailable = languagesManager.italianLangAvailable;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Italian Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.ITALIAN_SKILL_CODE
		);
		languageIsAvailable = languagesManager.italianLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Italian Language should  be available'
		);

		languageIsAvailable = languagesManager.frenchLangAvailable;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'French Language should not be available'
		);
		languagesManager.langSkillCodeAvailableSet.add(
			OmnichanelConst.FRENCH_SKILL_CODE
		);
		languageIsAvailable = languagesManager.frenchLangAvailable;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'French Language should  be available'
		);
		Test.stopTest();

		// Asserts
	}

	@IsTest
	static void testCheckSkillEnableProperties() {
		Test.startTest();
		ChatLanguagesManager languagesManager = ChatLanguagesManager.getInstance();
		languagesManager.langSkillEnabledSet = new Set<String>();
		Boolean languageIsAvailable = languagesManager.englishLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'English Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.ENGLISH_SKILL);
		languageIsAvailable = languagesManager.englishLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'English Language should be enabled'
		);

		languageIsAvailable = languagesManager.chineseLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Chinese Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.CHINESE_SKILL);
		languageIsAvailable = languagesManager.chineseLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Chinese Language should be enabled'
		);

		languageIsAvailable = languagesManager.germanLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'German Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.GERMAN_SKILL);
		languageIsAvailable = languagesManager.germanLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'German Language should be enabled'
		);

		languageIsAvailable = languagesManager.spanishLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Spanish Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.SPANISH_SKILL);
		languageIsAvailable = languagesManager.spanishLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Spanish Language should be enabled'
		);

		languageIsAvailable = languagesManager.russianLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Russian Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.RUSSIAN_SKILL);
		languageIsAvailable = languagesManager.russianLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Russian Language should be enabled'
		);

		languageIsAvailable = languagesManager.polishLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Polish Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.POLISH_SKILL);
		languageIsAvailable = languagesManager.polishLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Polish Language should be enabled'
		);

		languageIsAvailable = languagesManager.frenchLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'French Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.FRENCH_SKILL);
		languageIsAvailable = languagesManager.frenchLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'French Language should be enabled'
		);

		languageIsAvailable = languagesManager.italianLangEnabled;
		Assert.areEqual(
			false,
			languageIsAvailable,
			'Italian Language should not be enabled'
		);
		languagesManager.langSkillEnabledSet.add(OmnichanelConst.ITALIAN_SKILL);
		languageIsAvailable = languagesManager.italianLangEnabled;
		Assert.areEqual(
			true,
			languageIsAvailable,
			'Italian Language should be enabled'
		);

		Test.stopTest();
	}
}