/**
 * @File Name          : SPDataUtils.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/9/2023, 7:27:08 PM
**/
public inherited sharing class SPDataUtils {

	public static Boolean updateIfNoTesting(
		SObject record
	){
		return updateIfCondition(
			record,
			(Test.isRunningTest() == false) //condition
		);
	}

	public static Boolean updateIfCondition(
		SObject record,
		Boolean condition
	) {
		Boolean result = false;

		if (
			(record != null) &&
			(condition == true)
		) {
			update record;
			result = true;
		}
        
		return result;
	}

    public static Boolean upsertIfNoTesting(
		SObject record
	){
		return upsertIfCondition(
			record,
			(Test.isRunningTest() == false) //condition
		);
	}

	public static Boolean upsertIfCondition(
		SObject record,
		Boolean condition
	) {
		Boolean result = false;

		if (
			(record != null) &&
			(condition == true)
		) {
			upsert record;
			result = true;
		}
        
		return result;
	}

    public static Boolean insertIfNoTesting(
        SObject record
    ){
        return insertIfCondition(
            record,
            (Test.isRunningTest() == false) //condition
        );
    }

    public static Boolean insertIfCondition(
        SObject record,
        Boolean condition
    ) {
        Boolean result = false;
        
        if (
            (record != null) &&
            (condition == true) 
        ) {
            insert record;
            result = true;
        }
        
        return result;
    }

    public static Boolean insertIfCondition(
        List<SObject> records,
        Boolean condition
    ) {
        Boolean result = false;
        if (
            (records != null) &&
            (records.isEmpty() == false) &&
            (condition == true)
        ) {
            insert records;
            result = true;
        }
        return result;
    }

	public static Boolean insertIfNoTesting(
		List<SObject> records
    ){
        return insertIfCondition(
            records,
            (Test.isRunningTest() == false) //condition
        );
    }

    public static Boolean deleteIfCondition(
        List<SObject> records,
        Boolean condition
    ) {
        Boolean result = false;
        if (
            (records != null) &&
            (records.isEmpty() == false) &&
            (condition == true)
        ) {
            delete records;
            result = true;
        }
        return result;
    }

	public static Boolean deleteIfNoTesting(
		List<SObject> records
    ){
        return deleteIfCondition(
            records,
            (Test.isRunningTest() == false) //condition
        );
    }

}