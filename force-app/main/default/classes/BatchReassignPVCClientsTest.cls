@isTest
private class BatchReassignPVCClientsTest {

    static testMethod void reassginFxAccountsTest() {
        Account acc = new Account(lastname ='test account', PersonMailingCountry='Singapore');
        insert acc;
        
        User testuser1 = UserUtil.getRandomTestUser();
        Opportunity opp1 = new Opportunity(StageName=FunnelStatus.NOT_STARTED, Name='Test opp1', CloseDate=Date.today(), AccountId=acc.Id, ownerId = testuser1.id,
                                           RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, OpportunityUtil.NAME_OPPORTUNITY));
        insert opp1;
        fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Asia Pacific',
			Birthdate__c = Date.newInstance(1980, 11, 21),
            Last_trade_date__c = Date.today(),
            Balance_USD__c = 40000,
			Citizenship_Nationality__c = 'Malaysia',
			Account__c = acc.Id,
			Is_Closed__c = false
		);
        insert fxAcc;
        String funnelTraded = Constants.FUNNEL_TRADED;
        set<string> divisions_OAP = new set<string>
        {
            'OANDA Asia Pacific',
            'OANDA Asia Pacific CFD'
        };

        Test.startTest();
        BatchReassignPVCClients b = new BatchReassignPVCClients();
        Database.executeBatch(b, 200);
        Test.stopTest();

        Account accAfter = [select id, ownerId from Account where id = :acc.Id];
        String queueId = UserUtil.getQueueId('PHVC Clients - OAP');
		Id newOwner = RoundRobinAssignment.getNextOwnerId(queueId);
        System.assertEquals(newOwner, accAfter.OwnerId);

    }

    static testMethod void notReassginFxAccountsTest() {

        String oapQueueId = UserUtil.getQueueId('OAP Relationship Managers');
        Id oldOwner = RoundRobinAssignment.getNextOwnerId(oapQueueId);
        Account acc = new Account(LastName ='test account', PersonMailingCountry='Singapore', OwnerId=oldOwner);
        insert acc;

        User testuser1 = UserUtil.getRandomTestUser();
        Opportunity opp1 = new Opportunity(StageName=FunnelStatus.NOT_STARTED, Name='Test opp1', CloseDate=Date.today(), AccountId=acc.Id, ownerId = testuser1.id,
                RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, OpportunityUtil.NAME_OPPORTUNITY));
        insert opp1;
        fxAccount__c fxAcc = new fxAccount__c(
                Account_Email__c = 'testingBatch1@oanda.com',
                Funnel_Stage__c = FunnelStatus.TRADED,
                RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                Division_Name__c = 'OANDA Asia Pacific',
                Birthdate__c = Date.newInstance(1980, 11, 21),
                Last_trade_date__c = Date.today(),
                Balance_USD__c = 40000,
                Citizenship_Nationality__c = 'Malaysia',
                Account__c = acc.Id,
                Is_Closed__c = false
        );
        insert fxAcc;
        String funnelTraded = Constants.FUNNEL_TRADED;
        set<string> divisions_OAP = new set<string>
        {
                'OANDA Asia Pacific',
                'OANDA Asia Pacific CFD'
        };

        Test.startTest();
        BatchReassignPVCClients b = new BatchReassignPVCClients();
        Database.executeBatch(b, 200);
        Test.stopTest();

        Account accAfter = [select id, ownerId from Account where id = :acc.Id];
        System.assertEquals(oldOwner, accAfter.OwnerId);

    }
}