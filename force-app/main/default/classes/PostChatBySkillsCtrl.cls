/**
 * @File Name          : PostChatBySkillsCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/23/2021, 10:38:46 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/4/2021, 10:35:46 AM   acantero     Initial Version
**/
public without sharing class PostChatBySkillsCtrl {

    public static final String ERROR_PARAM = 'error';
    public static final String APEX_PREFIX = '/apex/';

    public String redirectUrl {get; private set;}
    public String languagePreference {get; private set;}
    public String languageCustomLabel {get; private set;}

    public PostChatBySkillsCtrl() {
        languagePreference = '';
        languageCustomLabel = '';
        PageReference postChatPageRef = null;
        redirectUrl = '';
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        String error = pageParams.get(ERROR_PARAM);
        if (String.isBlank(error)) {
            System.debug('checkError -> error is blank');
            String postChatPage = OmnichanelSettings.getRequiredDefault().postChatPage;
            if (String.isNotBlank(postChatPage)) {
                postChatPageRef = new PageReference(APEX_PREFIX + postChatPage);
            }
        } else {
            postChatPageRef = Page.LiveChatAgent;
            PostChatHelper helper = new PostChatHelper(pageParams);
            languagePreference = helper.language;
            languageCustomLabel = 
                ServiceLanguagesManager.getLangCustomLabel(
                    languagePreference
                );
        }
        if (postChatPageRef != null) {
            redirectUrl = postChatPageRef.getUrl();
        }
    }

}