/**
 * @File Name          : RemoveNonHvcCasesSchedulableTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/19/2024, 1:03:42 AM
**/
@IsTest
private without sharing class RemoveNonHvcCasesSchedulableTest {

	/**
	 * Three Non_HVC_Case__c records are created
	 * with status = Released
	 */
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		LoggerTestDataFactory.enableLogSettings();
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_1, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_2, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_3, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		ServiceTestDataFactory.createNonHVCCase2(initManager);
		ServiceTestDataFactory.createNonHVCCase3(initManager);
		initManager.storeData();
	}

	/**
	 * 3 records match the filters
	 * recordsLimit = 2
	 * => completed = false
	 * => count = 2
	 * => there is 1 record of Non_HVC_Case__c left
	 */
	@IsTest
	static void execute1() {
		Test.startTest();
		RemoveNonHvcCasesDynamicSchedulable instance = 
			new RemoveNonHvcCasesDynamicSchedulable();
		String jobId = System.schedule(
			'DummySchedulable', // jobName
			'0 0 0 3 9 ? 2042', // cronExp
			instance // schedulable
		);
		Test.stopTest();
		
		Boolean areErrorsOrExceptions = 
			LoggerTestDataFactory.areErrorsOrExceptions();
		
		Assert.isFalse(areErrorsOrExceptions, 'Invalid value');
	}

	/**
	 * test 1 : success = true => return true
	 * test 2 : success = false => return false
	 */
	@IsTest
	static void checkEnqueueActionResult() {

		Test.startTest();
		RemoveNonHvcCasesSchedulable instance = 
			new RemoveNonHvcCasesSchedulable();

		// test 1 => return true
		Boolean result1 = instance.checkEnqueueActionResult(true);

		// test 2 => return false
		Boolean result2 = instance.checkEnqueueActionResult(false);

		Test.stopTest();
		
		Assert.isTrue(result1, 'Invalid result');
		Assert.isFalse(result2, 'Invalid result');
	}
	
}