@isTest
private class PlatformEventDispatchTest {

    private static final String EVENT_BODY = '{\"test\": 123}';
    private static final String EVENT_CONTEXT = 'testContext';
    private static final String EVENT_TYPE_NAME = 'TestValue';
    private static final Map<String,String> EVENT_CONTEXT_MAP = new Map<String,String>();

    @isTest
    static void shouldDispatchAfterEventAfter() {

        PlatformEventDispatch.insertGenericAfter(EVENT_TYPE_NAME, EVENT_BODY, EVENT_CONTEXT);
        PlatformEventDispatch.insertGenericAfter(EVENT_TYPE_NAME, EVENT_BODY, EVENT_CONTEXT_MAP);
        PlatformEventDispatch.insertGenericAfter(EVENT_TYPE_NAME, EVENT_BODY);

        Api_Event_After__e apiEvent = new Api_Event_After__e();
        apiEvent.Request_Body__c = EVENT_BODY;
        apiEvent.Handler_Type_Name__c = EVENT_TYPE_NAME;
        apiEvent.Context_Variables__c = EVENT_CONTEXT;

        PlatformEventDispatch.insertGenericAfter(new List<Api_Event_After__e>{apiEvent});
        Test.getEventBus().deliver();
    }

    @isTest
    static void shouldDispatchAfterEventImmediate() {

        PlatformEventDispatch.insertGenericImmediate(EVENT_TYPE_NAME, EVENT_BODY, EVENT_CONTEXT);
        PlatformEventDispatch.insertGenericImmediate(EVENT_TYPE_NAME, EVENT_BODY, EVENT_CONTEXT_MAP);
        PlatformEventDispatch.insertGenericImmediate(EVENT_TYPE_NAME, EVENT_BODY);

        Api_Event_Immediate__e apiEvent = new Api_Event_Immediate__e();
        apiEvent.Request_Body__c = EVENT_BODY;
        apiEvent.Handler_Type_Name__c = EVENT_TYPE_NAME;
        apiEvent.Context_Variables__c = EVENT_CONTEXT;

        PlatformEventDispatch.insertGenericImmediate(new List<Api_Event_Immediate__e>{apiEvent});
        Test.getEventBus().deliver();
    }
}