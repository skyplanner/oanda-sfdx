/**
 * @File Name          : CaseReroutingManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/27/2022, 11:59:33 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/12/2022, 12:00:53 PM   acantero     Initial Version
**/
@IsTest
private without sharing class CaseReroutingManager_Test {

    @testSetup
    static void setup() {
        Case newCustomerLeadCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE,
            Routed__c = true
        );
        Case newCustomerLeadCase2 = new Case(
            Routed__c = false
        );
        List<Case> caseList = 
            new List<Case>{newCustomerLeadCase1, newCustomerLeadCase2};
        OmnichanelRoutingTestDataFactory.setupAllForNonHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    // The cases that meet the conditions 
    // (Routed__c = true and Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE)
    // are routed internally, the others are not
    @IsTest
    static void fireRerouteEventForCases() {
        Set<ID> caseIdSet = new Map<Id,Case>([select Id from Case]).keySet();
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        Boolean result1 = instance.fireRerouteEventForCases(null);
        Boolean result2 = instance.fireRerouteEventForCases(new Set<ID>());
        Boolean result3 = instance.fireRerouteEventForCases(caseIdSet);
        Test.stopTest();
        List<Case> caseList = [
            select Id, Routed__c, Internally_Routed__c, Agent_Capacity_Status__c 
            from Case
        ];
        for(Case caseObj : caseList) {
            if (caseObj.Routed__c == true) {
                System.assertEquals(true, caseObj.Internally_Routed__c, 'Invalid value');
                System.assertEquals(
                    OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
                    caseObj.Agent_Capacity_Status__c,
                    'Invalid value'
                );
            } else {
                System.assertEquals(false, caseObj.Internally_Routed__c, 'Invalid value');
                System.assertNotEquals(
                    OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
                    caseObj.Agent_Capacity_Status__c,
                    'Invalid value'
                );
            }
        }
    }

    @IsTest
    static void getReroutingQueueIdSet() {
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        Set<ID> result = instance.getReroutingQueueIdSet();
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid value');
    }

    @IsTest
    static void getClosedStatuses() {
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        List<String> result = instance.getClosedStatuses();
        Test.stopTest();
        System.assertNotEquals(null, result, 'Invalid value');
    }

    //all is ok => return true
    @IsTest
    static void newOwnerImpliesRouting1() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{caseObj.OwnerId};
        Boolean result = instance.newOwnerImpliesRouting(caseObj);
        Test.stopTest();
        System.assertEquals(true, result, 'Invalid value');
    }

    //CurrentUser is not a valid Standard user => return false
    @IsTest
    static void newOwnerImpliesRouting2() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Test.startTest();
        SPSecurityUtil.validStandardUser = false;
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{caseObj.OwnerId};
        Boolean result = instance.newOwnerImpliesRouting(caseObj);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid value');
    }

    //ReroutingQueueIdSet does not contain CurrentUser => return false
    @IsTest
    static void newOwnerImpliesRouting3() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>();
        Boolean result = instance.newOwnerImpliesRouting(caseObj);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid value');
    }

    //Case is closed => return false
    @IsTest
    static void newOwnerImpliesRouting4() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        caseObj.Status = 'Closed';
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{caseObj.OwnerId};
        Boolean result = instance.newOwnerImpliesRouting(caseObj);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid value');
    }

    // all is ok => return true
    @IsTest
    static void newStatusImpliesRouting1() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Case oldCaseObj = [select Id, OwnerId, Status from Case where Id = :caseObj.Id];
        oldCaseObj.Status = OmnichanelConst.CASE_STATUS_CLOSED;
        caseObj.Status = OmnichanelConst.CASE_STATUS_OPEN;
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{caseObj.OwnerId};
        Boolean result = instance.newStatusImpliesRouting(caseObj, oldCaseObj);
        Test.stopTest();
        System.assertEquals(true, result, 'Invalid result');
    }

    // ReroutingQueueIdSet does not contain CurrentUser => return false
    @IsTest
    static void newStatusImpliesRouting2() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Case oldCaseObj = [select Id, OwnerId, Status from Case where Id = :caseObj.Id];
        oldCaseObj.Status = OmnichanelConst.CASE_STATUS_CLOSED;
        caseObj.Status = OmnichanelConst.CASE_STATUS_OPEN;
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{};
        Boolean result = instance.newStatusImpliesRouting(caseObj, oldCaseObj);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid result');
    }

    // status change is not from closed status to non closed status => return false
    @IsTest
    static void newStatusImpliesRouting3() {
        Case caseObj = [select Id, OwnerId, Status from Case limit 1];
        Case oldCaseObj = [select Id, OwnerId, Status from Case where Id = :caseObj.Id];
        oldCaseObj.Status = OmnichanelConst.CASE_STATUS_OPEN;
        caseObj.Status = OmnichanelConst.CASE_STATUS_IN_PROGRESS;
        Test.startTest();
        CaseReroutingManager instance = CaseReroutingManager.getInstance();
        instance.reroutingQueueIdSet = new Set<ID>{caseObj.OwnerId};
        Boolean result = instance.newStatusImpliesRouting(caseObj, oldCaseObj);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid result');
    }
    
}