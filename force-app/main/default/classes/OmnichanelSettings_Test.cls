/**
 * @File Name          : OmnichanelSettings_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/19/2021, 9:45:18 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/16/2020   acantero     Initial Version
**/
@isTest(isParallel = true)
private class OmnichanelSettings_Test {

    //settings ok
    @isTest
    static void  test1() {
        Test.startTest();
        OmnichanelSettings settings = OmnichanelSettings.getRequiredDefault();
        Test.stopTest();
        System.assertNotEquals(null, settings);
    }

    @isTest
    static void getValidPositiveNumber_test() {
        Test.startTest();
        Integer result1 = OmnichanelSettings.getValidPositiveNumber(5);
        Integer result2 = OmnichanelSettings.getValidPositiveNumber(-3);
        Integer result3 = OmnichanelSettings.getValidPositiveNumber(null);
        Test.stopTest();
        System.assertEquals(5, result1);
        System.assertEquals(0, result2);
        System.assertEquals(0, result3);
    }

}