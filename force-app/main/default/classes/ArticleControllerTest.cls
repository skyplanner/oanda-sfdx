/**
 * @File Name          : ArticleControllerTest.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/12/2019, 4:28:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/7/2019   dmorales     Initial Version
**/

@isTest
private class ArticleControllerTest {

    
    @isTest
    static void articlePageTest() {
    	Id artId = crateTestArticle();     
        List<FAQ__kav> result = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id =: artId ];
        KbManagement.PublishingService.publishArticle(result[0].KnowledgeArticleId, true);

        PageReference pageRef = Page.AnswersSupport;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('urlName', 'This-is-a-question');
        ApexPages.currentPage().getParameters().put('language', 'en_US');
        
        ArticleController theCon = new ArticleController();
        theCon.redirect();
                    
        System.assertNotEquals(null,theCon);
    }

    @isTest
    static void articlePageTest2() {
    	Id artId = crateTestArticle();     
        List<FAQ__kav> result = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id =: artId ];
        KbManagement.PublishingService.publishArticle(result[0].KnowledgeArticleId, true);

        PageReference pageRef = Page.AnswersSupport;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('urlName', 'This-is-a-question');
        ApexPages.currentPage().getParameters().put('language', 'es');
        
        ArticleController theCon = new ArticleController();
        theCon.redirect();
                    
        System.assertNotEquals(null,theCon);
    }

    static Id crateTestArticle() {
		// we create a faq
		FAQ__kav f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is an answer',
			UrlName = 'This-is-a-question',
            Language = 'en_US'
		);
		insert f;
        return f.Id;
    }
}