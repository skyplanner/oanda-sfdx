/**
 * @File Name          : SPValidationUtils_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/11/2020, 1:24:18 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   acantero     Initial Version
**/
@istest
private class SPValidationUtils_Test {

    @istest
    static void checkFlexibleFormat_test() {
        Test.startTest();
        Boolean result1 = SPValidationUtils.checkFlexibleFormat(null, null, null);
        Boolean result2 = SPValidationUtils.checkFlexibleFormat(null, 2, 4);
        Boolean result3 = SPValidationUtils.checkFlexibleFormat('AB1234', 2, 2);
        Boolean result4 = SPValidationUtils.checkFlexibleFormat('AB1234', 2, 2);
        Boolean result5 = SPValidationUtils.checkFlexibleFormat('AB1234C', 2, 4);
        Boolean result6 = SPValidationUtils.checkFlexibleFormat('AB_1234', 2, 4);
        Boolean result7 = SPValidationUtils.checkFlexibleFormat('AB1234C', 3, 4);
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        System.assertEquals(false, result4);
        System.assertEquals(false, result5);
        System.assertEquals(false, result6);
        System.assertEquals(true, result7);
    }

    @istest
    static void verifyLuhnCheckDigit_test() {
        Test.startTest();
        Boolean result1 = SPValidationUtils.verifyLuhnCheckDigit(null);
        Boolean result2 = SPValidationUtils.verifyLuhnCheckDigit('1');
        Boolean result3 = SPValidationUtils.verifyLuhnCheckDigit('13456677A');
        Boolean result4 = SPValidationUtils.verifyLuhnCheckDigit('111111111');
        Boolean result5 = SPValidationUtils.verifyLuhnCheckDigit('79927398713');
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
        System.assertEquals(false, result4);
        System.assertEquals(true, result5);
    }

    @istest
    static void calculateLuhnCheckDigit_test() {
        Test.startTest();
        Integer result1 = SPValidationUtils.calculateLuhnCheckDigit(null);
        Integer result2 = SPValidationUtils.calculateLuhnCheckDigit('13456677A');
        Integer result3 = SPValidationUtils.calculateLuhnCheckDigit('7992739871');
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(3, result3);
    }

    @istest
    static void verifyModCheckDigit_test() {
        Test.startTest();
        Boolean result1 = SPValidationUtils.verifyModCheckDigit(null, null, null);
        Boolean result2 = SPValidationUtils.verifyModCheckDigit('1', 3, true);
        Boolean result3 = SPValidationUtils.verifyModCheckDigit('13456677A', 3, true);
        Boolean result4 = SPValidationUtils.verifyModCheckDigit('1371', 2, true);
        Boolean result5 = SPValidationUtils.verifyModCheckDigit('88880', 11, true);
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
        System.assertEquals(true, result4);
        System.assertEquals(true, result5);
    }

    @istest
    static void calculateMod_test() {
        Test.startTest();
        Long result1 = SPValidationUtils.calculateMod(null, null, null); //invalid params
        Long result2 = SPValidationUtils.calculateMod('137', 0, true); //invalid mod number
        Long result3 = SPValidationUtils.calculateMod('13456677A', 2, true); //value is not a number 
        Long result4 = SPValidationUtils.calculateMod('137', 2, true);
        Long result5 = SPValidationUtils.calculateMod('8888', 11, true);
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
        System.assertEquals(1, result4);
        System.assertEquals(0, result5);
    }

}