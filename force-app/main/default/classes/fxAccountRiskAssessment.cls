public with sharing class fxAccountRiskAssessment { 

    public static List<String> industriesWithHighScore = new List<String> 
    {
        'Defence/Military/Aerospace',
        'Export/Import',
        'Not for Profit/Charity',
        'Property/Construction',
        'Automotive',
        'Pharma/Biotech',
        'Casino-related'
    };
    
    public static List<String> industriesWithHighScoreOAP = new List<String> 
    {
        'Money-Lenders',
        'Stored Value Facility/Virtual Currencies',
        'Banking Services/Wealth Management',
        'Trust Companies',
        'Remittance Agents',
        'Precious Metals/Pawnbrokers',
        'Not for Profit/Charity',
        'Automotive',
        'Corporate Service Providers (CSPs)'
    };

    public static string GOVT_BENEFITS = 'Government Benefits';
    public static string STUDENT_LOAN = 'Student Loan'; 
    public static string INHERITANCE = 'Inheritance';
    public static Map<ID, Schema.RecordTypeInfo> fxAccountRecordTypeMap = Schema.SObjectType.fxAccount__c.getRecordTypeInfosById(); 

    //Trigger the calculation on new fxAccount or during key fields update only
    public static void calculateCustomerRiskAssessment(boolean isNew,
                                                       fxAccount__c[] newFxAccounts,
                                                       Map<id, fxAccount__c> newFxAccountsMap,
                                                       Map<id, fxAccount__c> oldFxAccountsMap) 
    {
        if(isNew)
        {
            calculateCustomerRiskAssessment(newFxAccounts);
        }
        else
        {
            fxAccount__c[] changedFxAccounts = new  fxAccount__c[]{};
            for(Id fxAccountId : newFxAccountsMap.keySet())
            {
                fxAccount__c oldFxAccount = oldFxAccountsMap.get(fxAccountId);
                fxAccount__c newFxAccount = newFxAccountsMap.get(fxAccountId);

                if(oldFxAccount.get('Division_Name__c') != newFxAccount.get('Division_Name__c') ||
                   oldFxAccount.get('Employment_Status__c') != newFxAccount.get('Employment_Status__c') ||
                   oldFxAccount.get('Industry_of_Employment__c') != newFxAccount.get('Industry_of_Employment__c') ||             
                   oldFxAccount.get('Citizenship_Nationality__c') != newFxAccount.get('Citizenship_Nationality__c') ||
                   oldFxAccount.get('Income_Source_Details__c') != newFxAccount.get('Income_Source_Details__c') ||
                   oldFxAccount.get('Source_of_Wealth_governmentBenefits__c') != newFxAccount.get('Source_of_Wealth_governmentBenefits__c') ||
                   oldFxAccount.get('Source_of_Wealth_studentLoan__c') != newFxAccount.get('Source_of_Wealth_studentLoan__c') ||
                   oldFxAccount.get('Singapore_Src_of_Funds_Govt_Benefits__c') != newFxAccount.get('Singapore_Src_of_Funds_Govt_Benefits__c') ||
                   oldFxAccount.get('Singapore_Src_of_Funds_Student_Loan__c') != newFxAccount.get('Singapore_Src_of_Funds_Student_Loan__c') ||
                   oldFxAccount.get('Singapore_Src_of_Funds_Inheritance__c') != newFxAccount.get('Singapore_Src_of_Funds_Inheritance__c'))

                {
                    changedFxAccounts.add(newFxAccount);
                }
            }
            calculateCustomerRiskAssessment(changedFxAccounts);
        }
    }

    public static string calculateCustomerRiskAssessment(string divisionName,
                                                         string employmentStatus,
                                                         string industryofEmployment,
                                                         string countryOfResidence,
                                                         string citizenshipNationality,
                                                         set<string> incomeSourceDetails) 
    {
        if(string.isBlank(divisionName) || string.isBlank(countryOfResidence) || string.isBlank(citizenshipNationality) ||  string.isBlank(EmploymentStatus)) 
        {
            return 'Incomplete Data';
        }
        
        Integer countryOfResidenceScore = getCountryOfResidenceScore(countryOfResidence);
        Integer citizenshipScore = getCitizenshipScore(citizenshipNationality);
        
        if(countryOfResidenceScore == 0 || citizenshipScore == 0 || (string.isBlank(industryofEmployment) && (employmentStatus == 'Employed' || employmentStatus == 'Self Employed'))) 
        {
            return 'Incomplete Data';
        }

        Integer occupationScore =  getOccupationScore(employmentStatus);
        Integer industryScore = getIndustryScore(divisionName, industryofEmployment, employmentStatus);
        Integer sourceOfFundsScore = getSourceOfFundsScore(incomeSourceDetails,
                                                           incomeSourceDetails.contains(GOVT_BENEFITS),
                                                           incomeSourceDetails.contains(STUDENT_LOAN),
                                                           incomeSourceDetails.contains(GOVT_BENEFITS),
                                                           incomeSourceDetails.contains(STUDENT_LOAN),
                                                           incomeSourceDetails.contains(INHERITANCE),
                                                           divisionName);
        Double productRiskScore = getProductRiskScore();
        Integer deliveryChannelRiskScore = getDeliveryChannelRiskScore();
            
        return calculateRisk(citizenshipScore,
                             countryOfResidenceScore,
                             occupationScore,
                             industryScore,
                             sourceOfFundsScore,
                             productRiskScore,
                             deliveryChannelRiskScore);
    }

    public static List<fxAccount__c> calculateCustomerRiskAssessment(
        Set<Id> accIds,
        Boolean save)
    {
        System.debug(
            'calculateCustomerRiskAssessment-accIds: ' +
            accIds);
        
        List<fxAccount__c> fxaList =
            [SELECT Id,
                    Lead__c,
                    Contact__c,
                    Account__c,
                    RecordTypeId,
                    Employment_Status__c,
                    Income_Source_Details__c,
                    Citizenship_Nationality__c,
                    Industry_of_Employment__c,
                    Division_Name__c,
                    Source_of_Wealth_governmentBenefits__c,
                    Source_of_Wealth_studentLoan__c,
                    Singapore_Src_of_Funds_Govt_Benefits__c,
                    Singapore_Src_of_Funds_Student_Loan__c,
                    Singapore_Src_of_Funds_Inheritance__c,
                    Customer_Risk_Assessment_Text__c
                FROM fxAccount__c
                WHERE RecordTypeId = :fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE
                AND Account__c IN :accIds];

        if(!fxaList.isEmpty()) {
            // Calculate risks
            calculateCustomerRiskAssessment(fxaList);
        
            if(save) {
                // Stop fxAccount is not possible because
                //there are other processes waiting for this updates
                update fxaList;
            }
        }

        return fxaList;
    }

    public static void calculateCustomerRiskAssessment(List<fxAccount__c> fxAccounts) 
    {

        Map<Id, String> countryByFxaId = getCountryByFxaId(fxAccounts);
        for(fxAccount__c fxa : fxAccounts) 
        {
            Integer countryOfResidenceScore = getCountryOfResidenceScore(countryByFxaId.get(fxa.Id));
            Integer citizenshipScore = getCitizenshipScore(fxa);

            if(fxAccountRecordTypeMap.get(fxa.recordTypeId).getName() != RecordTypeUtil.NAME_FXACCOUNT_RECORDTYPE_LIVE) 
            {
                fxa.Customer_Risk_Assessment_Text__c = null;
            }
            else if(fxa.Employment_Status__c == null 
                || (fxa.Industry_of_Employment__c == null && (fxa.Employment_Status__c == 'Employed' || fxa.Employment_Status__c == 'Self Employed')) 
                || citizenshipScore == 0
                || citizenshipScore == null
                || countryOfResidenceScore == 0
                || countryOfResidenceScore == null
                || fxa.Division_Name__c == null) 
            {
                fxa.Customer_Risk_Assessment_Text__c = 'Incomplete Data';
                System.debug('IncompleteData');
            }
            else 
            {
                Integer occupationScore = getOccupationScore(fxa);
                Integer industryScore = getIndustryScore(fxa);
                Integer sourceOfFundsScore = getSourceOfFundsScore(fxa);
                Double productRiskScore = getProductRiskScore();
                Integer deliveryChannelRiskScore = getDeliveryChannelRiskScore();

                fxa.Customer_Risk_Assessment_Text__c = calculateRisk(citizenshipScore,
                                                                     countryOfResidenceScore, 
                                                                     occupationScore,
                                                                     industryScore, 
                                                                     sourceOfFundsScore,
                                                                     productRiskScore,
                                                                     deliveryChannelRiskScore);
                fxa.Country_of_Residence_Score_CRA__c = countryOfResidenceScore;
                fxa.Country_of_Nationality_Score_CRA__c = citizenshipScore;
                fxa.Occupation_Score_CRA__c = occupationScore;
                fxa.Industry_Score_CRA__c = industryScore;
                fxa.SOF_Score_CRA__c = sourceOfFundsScore;
                fxa.Product_Risk_Score_CRA__c = productRiskScore;
                fxa.Delivery_Channel_Risk_Score_CRA__c = deliveryChannelRiskScore;
            }
        }
    }
    
    private static string calculateRisk(integer citizenshipScore,
                                        integer countryOfResidenceScore,
                                        integer occupationScore,
                                        integer industryScore,
                                        integer sourceOfFundsScore,
                                        double productRiskScore,
                                        integer deliveryChannelriskScore)
    {
        string riskAssesmentResult= '';
        Double finalScore = citizenshipScore + countryOfResidenceScore + occupationScore + sourceOfFundsScore + industryScore + productRiskScore + deliveryChannelriskScore;

        if(finalScore >= 71) 
        {
            riskAssesmentResult = 'High';
        }
        else if(finalScore >= 46) 
        {
            riskAssesmentResult = 'Medium';
        }
        else 
        {
            riskAssesmentResult = 'Low';
        }
        System.debug('Risk Assesment Result: ' + riskAssesmentResult);
        System.debug('Final Score: ' + finalScore);
        
        return riskAssesmentResult;
    }
    
    private static Map<Id, String> getCountryByFxaId(List<fxAccount__c> fxAccounts) 
    {

        Map<Id, Id> accIdByfxId = new Map<Id, Id>();
        Map<Id, Id> leadIdByfxId = new Map<Id, Id>();

        for(fxAccount__c fxa : fxAccounts) 
        {
            if(fxa.Contact__c!=null) 
            {
                accIdByfxId.put(fxa.Id, fxa.Account__c);
            }
            else if(fxa.Lead__c!=null) 
            {
                leadIdByfxId.put(fxa.Id, fxa.Lead__c);
            }
            else
            {
                continue;
            }
        }

        Map<Id, String> countryByAccId = new Map<Id, String>();
        Map<Id, String> countryByLeadId = new Map<Id, String>();

        for(Account account : [SELECT Id, fxAccount__c, PersonMailingCountry FROM Account WHERE Id IN :accIdByfxId.values()]) 
		{
            countryByAccId.put(account.Id, account.PersonMailingCountry);
        }

        for(Lead lead : [SELECT Id, fxAccount__c, Country FROM Lead WHERE Id IN :leadIdByfxId.values()]) 
		{
            countryByLeadId.put(lead.Id, lead.Country);
        }

        Map<Id, String> countryByFxaId = new Map<Id, String>();

        for(fxAccount__c fxa : fxAccounts) 
		{
            if(fxa.Contact__c!=null) 
			{
                countryByFxaId.put(fxa.Id, countryByAccId.get(fxa.Account__c));
            }
            else if(fxa.Lead__c!=null) 
			{
                countryByFxaId.put(fxa.Id, countryByLeadId.get(fxa.Lead__c));
            }
            else 
			{
                continue;
            }
        }
        System.debug('Country By FxaId: ' + countryByFxaId);

        return countryByFxaId;

    }
    
    private static Integer getOccupationScore(fxAccount__c fxAc) 
    {   
        return getOccupationScore(fxAc.Employment_Status__c);
    }
    private static Integer getOccupationScore(string employmentStatus) 
    {       
        Integer result = 0;
        if(employmentStatus == 'Unemployed') 
        {
            result=20;
        }
        else if(employmentStatus == 'Retired' || employmentStatus == 'Student')
        {
            result=10;
        }

        System.debug('Occupation Score: ' + result);
        return result;
    }

    private static Integer getIndustryScore(fxAccount__c fxAc)
    {
        return getIndustryScore(fxAc.Division_Name__c, fxAc.Industry_of_Employment__c, fxAc.Employment_Status__c);
    }
    private static Integer getIndustryScore(string divisionName, string industryofEmployment, string employmentStatus)
    {
        Integer result=10;
        if((isOap(divisionName) && industriesWithHighScoreOAP.contains(industryofEmployment)) ||
        (!isOap(divisionName) && industriesWithHighScore.contains(industryofEmployment)))
        {
            result=20;
        }
        if (employmentStatus == 'Retired' || employmentStatus == 'Student' || employmentStatus == 'Unemployed')
        {
            result=0;
        }

        System.debug('Industry Score: ' + result);
        return result;
    }

    private static Integer getSourceOfFundsScore(fxAccount__c fxAccount) 
    {
        return getSourceOfFundsScore(new set<string>{fxAccount.Income_Source_Details__c},
                                     fxAccount.Source_of_Wealth_governmentBenefits__c,
                                     fxAccount.Source_of_Wealth_studentLoan__c,
                                     fxAccount.Singapore_Src_of_Funds_Govt_Benefits__c,
                                     fxAccount.Singapore_Src_of_Funds_Student_Loan__c,
                                     fxAccount.Singapore_Src_of_Funds_Inheritance__c,
                                     fxAccount.Division_Name__c);
    }
    private static Integer getSourceOfFundsScore(set<string> incomeSourceDetails,
                                                 boolean sourceOfWealthGovernmentBenefits,
                                                 boolean sourceOfWealthStudentLoan,
                                                 boolean singaporeSrcOfFundsGovtBenefits,
                                                 boolean singaporeSrcOfFundsStudentLoan,
                                                 boolean singaporeSrcOfFundsInheritance,
                                                 string divisionName) 
    {
        Integer result = 10;

        if(isOap(divisionName))
        {
            if(incomeSourceDetails.contains(INHERITANCE) || singaporeSrcOfFundsInheritance) 
            {
                result=15;
            }
            else if(incomeSourceDetails.contains(GOVT_BENEFITS) || 
            incomeSourceDetails.contains(STUDENT_LOAN) || 
            sourceOfWealthGovernmentBenefits || 
            sourceOfWealthStudentLoan || 
            singaporeSrcOfFundsGovtBenefits || 
            singaporeSrcOfFundsStudentLoan)
            {
                result=20;
            }
        }
        else
        {
            if(incomeSourceDetails.contains(GOVT_BENEFITS) || 
            incomeSourceDetails.contains(STUDENT_LOAN) || 
            sourceOfWealthGovernmentBenefits || 
            sourceOfWealthStudentLoan || 
            singaporeSrcOfFundsGovtBenefits || 
            singaporeSrcOfFundsStudentLoan) 
            {
                result=15;
            }
        }

        System.debug('Source Of Funds Score: ' + result);
        return result;
    }

    private static Decimal getProductRiskScore()
    {
        Decimal result = 2.5;
        return result;
    }

    private static Integer getDeliveryChannelRiskScore()
    {
        Integer result = 10;
        return result;
    }
    
    private static Integer getCitizenshipScore(fxAccount__c fxAccount) 
    {
        return getCitizenshipScore(fxAccount.Citizenship_Nationality__c);
    }
    private static Integer getCitizenshipScore(string citizenship) 
    {
        Integer result = 0;

        if(citizenship != null)
        {
            String citizenshipRiskRating = CustomSettings.getRiskRatingByCountry(citizenship);
            if(citizenshipRiskRating == 'PROHIBITED') 
            {
                result=36;
            }
            else if(citizenshipRiskRating == 'HIGH') 
            {
                result=12;
            }
            else if(citizenshipRiskRating == 'MEDIUM') 
            {
                result=8;
            }
            else if(citizenshipRiskRating == 'LOW') 
            {
                result=4;
            }
        }
        
        System.debug('Citizenship Score: ' + result);
        return result;
    }
    
    private static Integer getCountryOfResidenceScore(String countryOfResidence) 
    {
        Integer result = 0;

        if(countryOfResidence != null) 
        {
            String countryRating = CustomSettings.getRiskRatingByCountry(countryOfResidence);
            if(countryRating == 'PROHIBITED')
            {
                result=36;
            }
            else if(countryRating == 'HIGH') 
            {
                result=20;
            }
            else if(countryRating == 'MEDIUM') 
            {
                result=10;
            }
            else if(countryRating == 'LOW') 
            {
                result=6;
            }
        }
        
        System.debug('Country Of Residence Score: ' + result);
        return result;
    }

    static Boolean isOap(String divisionName)
    {
        return ((divisionName == Constants.DIVISIONS_OANDA_ASIA_PACIFIC) || 
                (divisionName == Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD));
    }
}