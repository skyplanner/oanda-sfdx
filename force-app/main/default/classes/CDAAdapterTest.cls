@isTest
private class CDAAdapterTest {

	@isTest
	static void createSuccess()
	{
		Boolean isSuccess = true;
		String message = 'Supplementary message indicating a success';
		
		String result = CDAAdapter.serialize(isSuccess, message);
		
		system.assertEquals(true, result.contains(CDAAdapter.STATUS_SUCCESS));
	}
	
	@isTest
	static void createFailure()
	{
		Boolean isSuccess = false;
		String message = 'Supplementary message indicating a failure';
		
		String result = CDAAdapter.serialize(isSuccess, message);
		
		system.assertEquals(true, result.contains(CDAAdapter.STATUS_FAILURE));
	}
}