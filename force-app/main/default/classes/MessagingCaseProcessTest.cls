/**
 * @File Name          : MessagingCaseProcessTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/15/2024, 3:26:34 PM
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class MessagingCaseProcessTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		String queueDevName = SPSettingsManager.getSetting(
			MessagingConst.CASE_QUEUE_DEV_NAME_SETTING
		);
		ServiceTestDataFactory.createAccount1(initManager);

		initManager.setFieldValue(
			ServiceTestDataKeys.QUEUE_CASE_1, // objKey
			Group.DeveloperName, // fieldToken
			queueDevName // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.QUEUE_CASE_1, // objKey
			Group.Name, // fieldToken
			queueDevName // newValue
		);
		initManager.setFieldValue(
			ServiceTestDataKeys.CASE_1, // objKey
			Case.Origin, // fieldToken
			OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE // newValue
		);
		ServiceTestDataFactory.createCaseQueue1(initManager);
		ServiceTestDataFactory.createLead1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		//ServiceTestDataFactory.createContact1(initManager);
		initManager.storeData();
	}

	@isTest
	private static void testCreateCaseFromMsgSession() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID accountId = initManager.getObjectId(
			ServiceTestDataKeys.ACCOUNT_1,
			true
		);
		ID leadId = initManager.getObjectId(ServiceTestDataKeys.LEAD_1, true);
		// ID contactId = initManager.getObjectId(
		// 	ServiceTestDataKeys.CONTACT_1,
		// 	true
		// );
		String caseRecordType = SPSettingsManager.getSetting(
			MessagingConst.CASE_RECORD_TYPE_DEV_NAME_SETTING
		);
		String queueDevName = SPSettingsManager.getSetting(
			MessagingConst.CASE_QUEUE_DEV_NAME_SETTING
		);

		MessagingSession session = new MessagingSession(
			Id = null,
			Case_Record_Type__c = caseRecordType,
			Last_User_Intent__c = MessagingConst.ACCOUNT_DELETE_INTENT,
			Escalate_to_Agent__c = true,
			LeadId = leadId,
			Inquiry_Nature__c = 'Trade',
			Case_Type__c = 'Close a Trade for Client',
			Region__c = 'Asia',
			User_Email__c = 'user@email-server.com',
			Live_or_Practice__c = 'Practice',
			User_Authenticated__c = true,
			Language_Code__c = 'de',
			Case_Queue__c = queueDevName
		);
		// Actual test
		Case summaryCase;
		Test.startTest();
		MessagingCaseProcess messagingCaseProcess = new MessagingCaseProcess(
			session
		);
		try {
			 summaryCase = messagingCaseProcess.createCaseFromMsgSession();
		} catch (Exception ex) {
			
		}
		Test.stopTest();

		// Asserts
		Assert.areEqual(true, summaryCase != null, 'Case should be created');
	}

	@isTest
	private static void testCreateCaseFromMsgSessionSessionNull() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingCaseProcess messagingCaseProcess = new MessagingCaseProcess(
			null
		);
		Case summaryCase = messagingCaseProcess.createCaseFromMsgSession();
		Test.stopTest();

		// Asserts
		Assert.areEqual(null, summaryCase, 'Case should not be created');
	}

	@isTest
	private static void testUpdateCaseOriginSessionNull() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingCaseProcess messagingCaseProcess = new MessagingCaseProcess(
			null
		);
		Boolean isUpdated = messagingCaseProcess.updateCaseOrigin();

		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isUpdated, 'Case should not be Updated');
	}

	@isTest
	private static void testUpdateCaseOriginCaseIdNull() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingCaseProcess messagingCaseProcess = new MessagingCaseProcess(
			new MessagingSession()
		);
		Boolean isUpdated = messagingCaseProcess.updateCaseOrigin();

		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isUpdated, 'Case should not be Updated');
	}

	/**
	 * Case.Origin = CHATBOT_CASE
	 * MessagingSession.Escalate_to_Agent__c = true
	 * then case origin is not updated
	 */
	@isTest
	private static void testUpdateCaseOriginCase() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID caseId = initManager.getObjectId(
			ServiceTestDataKeys.CASE_1,
			true
		);
		Boolean isUpdated = null;

		// Actual test
		Test.startTest();
		User testUser = SPTestUtil.createUser('u0007');
		System.runAs(testUser) {
			MessagingCaseProcess messagingCaseProcess = new MessagingCaseProcess(
				new MessagingSession(
					CaseId = caseId,
					Escalate_to_Agent__c = true
				)
			);
			isUpdated = messagingCaseProcess.updateCaseOrigin();
		}
		Test.stopTest();

		// Asserts
		Assert.isFalse(isUpdated, 'Invalid value');
	}

	// test 1 : no changes
	// test 2 : origin changes from CHATBOT_EMAIL to CHATBOT_CASE
	// test 3 : origin changes from CHATBOT_CASE to CHATBOT_EMAIL
	@isTest
	private static void updateCaseOrigin() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID caseId = initManager.getObjectId(
			ServiceTestDataKeys.CASE_1,
			true
		);
		Case caseObj = CaseRepo.getChatbotInfoById(caseId);
		caseObj.Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL;
		Boolean result1 = null;
		Boolean result2 = null;
		Boolean routingToAgentTruncated2 = null;
		Boolean result3 = null;
		Boolean routingToAgentTruncated3 = null;

		Test.startTest();
		User testUser = SPTestUtil.createUser('u0008');
		System.runAs(testUser) {
			// test 1
			MessagingCaseProcess instance1 = new MessagingCaseProcess(
				new MessagingSession(
					CaseId = caseId,
					Escalate_to_Agent__c = false
				)
			);
			result1 = instance1.updateCaseOrigin(caseObj);
			
			// test 2
			caseObj.Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL;
			MessagingCaseProcess instance2 = new MessagingCaseProcess(
				new MessagingSession(
					CaseId = caseId,
					Escalate_to_Agent__c = true
				)
			);
			result2 = instance2.updateCaseOrigin(caseObj);
			routingToAgentTruncated2 = 
				caseObj.Routing_to_Agent_Truncated__c;
			// test 3
			caseObj.Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE;
			caseObj.Routing_to_Agent_Truncated__c = false;
			MessagingCaseProcess instance3 = new MessagingCaseProcess(
				new MessagingSession(
					CaseId = caseId,
					Escalate_to_Agent__c = false
				)
			);
			result3 = instance3.updateCaseOrigin(caseObj);
			routingToAgentTruncated3 = 
				caseObj.Routing_to_Agent_Truncated__c;
		}
		Test.stopTest();

		// Asserts
		Assert.isFalse(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isFalse(routingToAgentTruncated2, 'Invalid result');
		Assert.isTrue(result3, 'Invalid result');
		Assert.isTrue(routingToAgentTruncated3, 'Invalid result');
	}
}