/**
 * @File Name          : CreateLeadFromMsgSessionActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:09:54 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 12:56:50 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class CreateLeadFromMsgSessionActionTest {
	@isTest
	private static void testMessagingSessionIdsIsNullOrEmpty() {
		// Test data setup
		List<Id> Ids;
		// Actual test
		Test.startTest();
		Ids = CreateLeadFromMsgSessionAction.createLeadFromMsgSession(null);
		Assert.areEqual(
			true,
			Ids != null && Ids.size() > 0,
			'Id  list should not be null or empty'
		);
		Ids = null;
		Ids = CreateLeadFromMsgSessionAction.createLeadFromMsgSession(
			new List<ID>()
		);

		Assert.areEqual(
			true,
			Ids != null && Ids.size() > 0,
			'Id list should not be null or empty'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testCreateLeadFromMsgSessionThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		List<Id> ids;
		// Actual test
		Test.startTest();
		try {
			ids = CreateLeadFromMsgSessionAction.createLeadFromMsgSession(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testCreateLeadFromMsgSession() {
		// Test data setup
	
		// Actual test
		Test.startTest();
		List<ID> result = CreateLeadFromMsgSessionAction.createLeadFromMsgSession(
			new List<ID>{ null }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			true,
			result != null && result.size() > 0,
			'Id list should not be null or empty'
		);
	}
}