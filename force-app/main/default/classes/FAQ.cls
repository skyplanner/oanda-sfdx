/**
 * @File Name          : FAQ
 * @Description        : Contains information about a FAQ
 * @Author             : Fernando Gomez
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 7/6/2020, 10:27:48 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/5/2019         Fernando Gomez          Initial Version
 * 1.1    12/9/2019           dmorales       		Add style to table
**/
global class FAQ {
	global Id faqId { get; private set; }
	global Id knowledgeId { get; private set; }
	global String title { get; private set; }
	global String answer { get; private set; }
	global Boolean allowFeedback { get; private set; }

	/**
	 * Overload for short info FAQs (lists and such)
	 * @param original
	 */
	global FAQ(FAQ__kav original) {
		this(original, null, null, false);
	}

	/**
	 * Main constructor
	 * @param original
	 * @param isDeep
	 */
	global FAQ(FAQ__kav original, String titleSnippet, String snippet) {
		this(original, titleSnippet, snippet, false);
	}

	/**
	 * Main constructor
	 * @param original
	 * @param isDeep
	 */
	global FAQ(FAQ__kav original, 
			String titleSnippet,
			String snippet,
			Boolean isDeep) {       			
		String raw;
		faqId = original.Id;
		knowledgeId = original.KnowledgeArticleId;
		title = String.isBlank(titleSnippet) ? original.Title : titleSnippet;		

		// if we are supposed to render the full artivle with all info...
		if (isDeep) {
			allowFeedback = original.AllowFeedback__c == null ?
				false : original.AllowFeedback__c;

			raw = // we add both answer (is specified) separated by a line
				(String.isBlank(original.Answer__c) ? '' : original.Answer__c) + 
				(String.isBlank(original.Answer_2__c) ? '' : original.Answer_2__c);
			System.debug('raw ' + raw);				
			String answer1 = fixHtml(raw);
			System.debug('answer 1' + answer1);
			answer = fixSection(answer1);
			System.debug('answer ' + answer);
		} else {
			allowFeedback = false;
			answer = !String.isBlank(snippet) ? 
				// if we have a snippet, we use
				snippet :
				// if not, we render a string with html removed
				// and no longer than 180 characters as a snippet
				String.isBlank(original.Answer__c) ? '' :
					original.Answer__c.replaceAll('<[^>]*>', '').abbreviate(180);
		}
	}

	/**
	 * Creates the wrappers based on the actual
	 * @param faqs
	 */
	global static List<FAQ> doWrap(List<FAQ__kav> faqs) {
		List<FAQ> result = new List<FAQ>();
		for (FAQ__kav faq : faqs)
			result.add(new FAQ(faq));
		return result;
	}

	/**
	 * Creates the wrappers based on the actual
	 * @param faqs
	 */
	global static FAQ doWrap(FAQ__kav faq) {
		return new FAQ(faq, null, null, true);
	}

	/**
	 * Creates the wrappers based on the actual
	 * @param faqs
	 * @param titleSnippet
	 * @param snippet
	 */
	global static FAQ doWrap(FAQ__kav faq, String titleSnippet, String snippet) {
		return new FAQ(faq, titleSnippet, snippet, false);
	}

	/**
	 * Remove empty paragraphs, which tend to be very stupidly inserted
	 * by SF native HTML editor.
	 */
	private String fixHtml(String content) {
		// <p[^>]*>(\s|&nbsp;|<\/?\s?br\s?\/?>)*<\/?p>
		// '<a href=".*?(#anchor_[a-zA-Z0-9_\\-]*?)" *target="_blank">'
		return 
			content
				// we remove empty paragraphs
				.replaceAll('<p[^>]*>(&nbsp;| )*<\\/p>', '')
				// we need to add the live chat click if our current anchor
				// is supposed to launch the live chat window			
				.replaceAll('<a href="#open_live_chat"',
					'<a open-chat="true" href="javascript:void(0);"')
				.replaceAll('<a href="http://www.open-live-chat.com[/]?"',
					'<a open-chat="true" href="javascript:void(0);"')
				// we add the directive to the help portal allows anchoring
				.replaceAll('<a href="#', '<a anchor="true" href="#')
				// we add a directive to tables so the get scrolled
				.replaceAll('<table ', '<table class="p" wrap-table="true" ')
				// we also allow iframes to autoplay
				.replaceAll('<iframe ', '<iframe allow="autoplay" ');								
	}

	private String fixSection(String content){
	
		String regexTest ='id="section[0-9]+(\\_[0|1])"';	
		Matcher matcher =Pattern.compile(regexTest).matcher(content);
		List<SectionWrapper> sectionIndex = new List<SectionWrapper>();
		while (matcher.find()) {
			String word = matcher.group();	
			word = word.replaceAll('"','');
			String[] stringSplit = word.split('section');
			String numericPart = stringSplit[1];
			String[] numericSplit = numericPart.split('_');
			Integer sectionNum = Integer.valueOf(numericSplit[0]);
			Integer isColl = Integer.valueOf(numericSplit[1]);
			sectionIndex.add(new SectionWrapper(sectionNum, isColl == 0? false: true));
		}
	
	    if(sectionIndex.size()>0){
			String contentAux = '';			
			
			content = content.replaceAll('class="section-span-symbol', 
										'class="section-span-symbol" data-toggle-handle="true" ');
			for(SectionWrapper indexWrapper: sectionIndex){
				Integer index = indexWrapper.sectionNumber;
				String collapseNum = indexWrapper.isCollapsed? '1':'0';
				String ariaExpanded = indexWrapper.isCollapsed? 'false': 'true';
				contentAux = content.replaceAll('target="_section_a'+index+'"', 
				' href="javascript:void(0)"' + //"
				' data-toggle="collapse"'+
				' data-target="#panel'+index+'"' + 
				' aria-expanded="'+ ariaExpanded +'"' +
				' aria-controls="panel'+index+'"').
				replaceAll('id="panel'+index+'">',
				'id="panel'+index+'" data-parent="#section'+index+'_'+ collapseNum +'">');				
				content = contentAux;
			}			
		}
		
		
		return content;
	}

	public class SectionWrapper {
		Integer sectionNumber {set;get;}
		Boolean isCollapsed {set;get;}

		public SectionWrapper(Integer pSection, Boolean pCollapse){
			sectionNumber = pSection;
			isCollapsed = pCollapse;
		}
	}
}