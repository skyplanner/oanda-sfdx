/**
 * @File Name          : VerificationCodeConfigManager.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/13/2019, 11:47:01 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/13/2019   dmorales     Initial Version
**/
global without sharing class VerificationCodeConfigManager {

	public static Integer DEFAULT_COUNT = 200;
    public static final String DEFAULT_SETTING_NAME = 'DefaultSetting';
	
    private static VerificationCodeConfigManager instance = null;
   
    global Integer countToLoad {get; private set;}
	
    private VerificationCodeConfigManager(String settName) {
    	List<Verification_Code_Config__mdt> settings = getSettings(settName);
    	init(settings);
    }

	global static VerificationCodeConfigManager getInstance() {
		if (instance == null) {
			instance = new VerificationCodeConfigManager(DEFAULT_SETTING_NAME);
		}
		return instance;
	}

	global static VerificationCodeConfigManager getInstance(String settName) {
		if (instance == null) {
			String validName = String.isBlank(settName) ? DEFAULT_SETTING_NAME : settName;
			instance = new VerificationCodeConfigManager(validName);
		}
		return instance;
	}

	@TestVisible
	private void init(List<Verification_Code_Config__mdt> settings) {
		if (settings.isEmpty()) {
			countToLoad = DEFAULT_COUNT;			
		} else {
			countToLoad = (settings[0].Count_before_Loading__c != null) 
				? settings[0].Count_before_Loading__c.intValue()
				: DEFAULT_COUNT;			
		}
	}

	@TestVisible
	private static List<Verification_Code_Config__mdt> getSettings(String settName) {
		List<Verification_Code_Config__mdt> settings = [
			SELECT 
				Count_before_Loading__c
			FROM 
				Verification_Code_Config__mdt 
			WHERE 
				QualifiedApiName = :settName
			LIMIT 1
		];
		return settings;
	}

}