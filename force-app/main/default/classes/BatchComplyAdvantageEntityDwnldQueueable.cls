public with sharing class BatchComplyAdvantageEntityDwnldQueueable  extends ComplyAdvantageCallout implements Database.AllowsCallouts
{
    protected Comply_Advantage_Search__c caSearch;
    protected Comply_Advantage_Search__c[] caSearches;
   
    private String ENTITY_PROVIDER_INTERNAL = 'internal';
	private String ENTITY_PROVIDER_COMPLY = 'comply';

    private List<Comply_Advantage_Search_Entity__c> entities = new List<Comply_Advantage_Search_Entity__c>{};

    public BatchComplyAdvantageEntityDwnldQueueable(Comply_Advantage_Search__c caSearch) 
    {
        this.caSearch = caSearch;
        String divisionName = String.isNotBlank(caSearch.Custom_Division_Name__c) ? 
                                caSearch.Custom_Division_Name__c :
                                    (String.isNotBlank(caSearch.Division_Name__c) ? 
                                        caSearch.Division_Name__c : '');

        this.apiKey = ComplyAdvantageUtil.getAPiKey(divisionName, 
                                                    caSearch.Search_Parameter_Mailing_Country__c);
    }
  
    public EntityDownloadResult getEntities() 
	{
		try 
		{
			getComplyAdvantageEntities(1);

            getInternalMatchEntities();

            EntityDownloadResult entityDownloadResult = new EntityDownloadResult();
            entityDownloadResult.ComplyAdvantageSearchRecordId = this.caSearch.Id;
            entityDownloadResult.entities = entities;
            entityDownloadResult.isSuccessful = true;
            return entityDownloadResult;
		}
		catch (Exception ex) 
		{
            EntityDownloadResult entityDownloadResult = new EntityDownloadResult();
            entityDownloadResult.ComplyAdvantageSearchRecordId = this.caSearch.Id;
            entityDownloadResult.entities = entities;
            entityDownloadResult.isSuccessful = false;
            return entityDownloadResult;
		}
	}

    private void getComplyAdvantageEntities(Integer pageNumber) 
	{
		ComplyAdvantageEntitiesResult result;

		// we obtain all entities in the current page
		get(getEntityProviderEndpoint(ENTITY_PROVIDER_COMPLY), null, getEntityProviderParams(pageNumber));

		// we proceed on a valida response
		if (isResponseCodeSuccess()) 
		{
			result = ComplyAdvantageEntitiesResult.parse(resp.getBody());
            List<Comply_Advantage_Search_Entity__c> caEntities = result.getEntities(caSearch.Id, caSearch.Search_Id__c, false);
            entities.addAll(caEntities);

            // if(caEntities.size() >= 100)
            // {
            //     getComplyAdvantageEntities(pageNumber + 1);
            // }
		} 
		else
		{
			throw getCalloutException();
		}
	}

    private void getInternalMatchEntities() 
	{
        List<Comply_Advantage_Search_Entity__c> internalEntities = new List<Comply_Advantage_Search_Entity__c>{};

        if(caSearch.Custom_Division_Name__c == Constants.DIVISIONS_OANDA_ASIA_PACIFIC || 
           caSearch.Custom_Division_Name__c == Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD ||
           caSearch.Custom_Division_Name__c == Constants.DIVISIONS_OANDA_AUSTRALIA ||
           caSearch.Custom_Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
        {
            internalEntities = getInternalMatches();
            entities.addAll(internalEntities);
        }
    }
	private List<Comply_Advantage_Search_Entity__c> getInternalMatches() 
	{
		ComplyAdvantageEntitiesResult result;

		// we obtain all entities in the current page
		get(getEntityProviderEndpoint(ENTITY_PROVIDER_INTERNAL), null, getEntityProviderParams(1));

		// we proceed on a valida response
		if (isResponseCodeSuccess()) 
		{
			result = ComplyAdvantageEntitiesResult.parse(resp.getBody());
			return result.getEntities(caSearch.Id, caSearch.Search_Id__c, true);
		} 
		else
		{
			throw getCalloutException();
		}
	}

    private void save(List<Comply_Advantage_Search_Entity__c> entities, boolean downloadCompleted) 
    {

        if(downloadCompleted)
        {
                //once we downloaded all the entities,update CA Search as Entities Downloaded
            Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
                Search_Id__c= caSearch.Search_Id__c,
                Entities_Download_Status__c = 'Done');
            upsert search Search_Id__c;
        }
	}

	private String getEntityProviderEndpoint(string entityProvider) 
	{
		return String.format(
			SPGeneralSettings.getInstance().getValue('CA_Endpoint_Entity_Provider'),
			new List<String> { caSearch.Search_Reference__c, entityProvider }
		);
	}
	private Map<String, String> getEntityProviderParams(integer pageNo) {
		Map<String, String> result = getBaseParams();
		result.put('page', String.valueOf(pageNo));
		result.put('per_page', String.valueOf(100));
		return result;
	}

    public class EntityDownloadResult
    {
        public Id ComplyAdvantageSearchRecordId;
        public Boolean isSuccessful;
        public List<Comply_Advantage_Search_Entity__c> entities;
    }
}