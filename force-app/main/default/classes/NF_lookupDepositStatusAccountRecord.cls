/**
* Created by Neuraflash LLC on 03/01/19.
* Implementation : Chatbot
* Summary : Lookup Account records by searching Email  & Return Boolean
* Details : 1. Searches for an Account record by email provided.
*           2. Return Boolean if Account found/not found.
*/
global without sharing class NF_lookupDepositStatusAccountRecord implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
            Map<String, String> eventParams = new Map<String, String>();
            String emailValue =(String)paramMap.get('email'); //get email from DF
			String countValue =(String)paramMap.get('count'); //get count from DF
            List<Account> listAccount = [SELECT Id, Funnel_Stage__pc,Primary_Division_Name__c FROM Account WHERE PersonEmail =: emailValue
                                         ORDER BY CreatedDate ASC NULLS FIRST LIMIT 1];
            if(listAccount.isEmpty()){
				integer countInt = Integer.valueOf(countValue);
				countInt += 1;
                eventParams.put('result','false');
				eventParams.put('count',String.valueOf(countInt));
            }
			else{
                eventParams.put('result','true');
				eventParams.put('funnelStage',listAccount[0].Funnel_Stage__pc);
				eventParams.put('oemClient',listAccount[0].Primary_Division_Name__c);
            }
            eventParams.put('email',emailValue);
            return JSON.serialize(eventParams);
    }
}