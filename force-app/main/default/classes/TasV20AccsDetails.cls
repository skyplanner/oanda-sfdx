/**
 * Wrapper class for v20 Account Details
 */
public class TasV20AccsDetails {
    @AuraEnabled
    public String NAV {get; set;}

    @AuraEnabled
    public String accountID {get; set;}

    @AuraEnabled
    public String accountName {get; set;}

    @AuraEnabled
    public Integer accountNumber {get; set;}

    @AuraEnabled
    public String accountPositionValueLimit {get; set;}

    @AuraEnabled
    public Integer accountRiskGroupID {get; set;}

    @AuraEnabled
    public String accountType {get; set;}

    @AuraEnabled
    public String alias {get; set;}

    @AuraEnabled
    public String balance {get; set;}

    @AuraEnabled
    public Integer cellID {get; set;}

    @AuraEnabled
    public Boolean checkMargin {get; set;}

    @AuraEnabled
    public String commission {get; set;}

    @AuraEnabled
    public Integer commissionGroupID {get; set;}

    @AuraEnabled
    public Boolean configurationLocked {get; set;}

    @AuraEnabled
    public Integer createdByUserID {get; set;}

    @AuraEnabled
    public String createdTime {get; set;}

    @AuraEnabled
    public String currencyISO {get; set;}

    @AuraEnabled
    public Integer currencyConversionGroupID {get; set;}

    @AuraEnabled
    public Boolean depositLocked {get; set;}

    @AuraEnabled
    public String dividendAdjustment {get; set;}

    @AuraEnabled
    public Integer divisionID {get; set;}

    @AuraEnabled
    public Integer divisionTradingGroupID {get; set;}

    @AuraEnabled
    public String error_msg {get; set;}

    @AuraEnabled
    public String financing {get; set;}

    @AuraEnabled
    public String guaranteedExecutionAmountForgiven {get; set;}

    @AuraEnabled
    public String guaranteedExecutionFees {get; set;}
    
    @AuraEnabled
    public String guaranteedStopLossOrderMode {get; set;}
    
    @AuraEnabled
    public String health {get; set;}
    
    @AuraEnabled
    public Boolean hedgingEnabled {get; set;}
    
    @AuraEnabled
    public String id {get; set;}

    @AuraEnabled
    public String lastAppliedPriceTimestamp {get; set;}

    @AuraEnabled
    public String lastTransactionID {get; set;}

    @AuraEnabled
    public Boolean locked {get; set;}

    @AuraEnabled
    public String marginAvailable {get; set;}

    @AuraEnabled
    public String marginCallMarginUsed {get; set;}

    @AuraEnabled
    public String marginCallPercent {get; set;}

    @AuraEnabled
    public String marginCloseoutMarginUsed {get; set;}

    @AuraEnabled
    public String marginCloseoutNAV {get; set;}

    @AuraEnabled
    public String marginCloseoutPercent {get; set;}

    @AuraEnabled
    public String marginCloseoutPositionValue {get; set;}

    @AuraEnabled
    public String marginCloseoutUnrealizedPL {get; set;}

    @AuraEnabled
    public String marginRate {get; set;}

    @AuraEnabled
    public String marginUsed {get; set;}

    @AuraEnabled
    public String maxOpenTradeCount {get; set;}

    @AuraEnabled
    public String maxOpenTradeCountOverride {get; set;}

    @AuraEnabled
    public Long maxPendingOrderCount {get; set;}

    @AuraEnabled
    public Long maxPendingOrderCountOverride {get; set;}

    @AuraEnabled
    public String mt4AccountID {get; set;}

    @AuraEnabled
    public Integer mt4ServerID {get; set;}

    @AuraEnabled
    public Boolean negativeBalanceProtected {get; set;}

    @AuraEnabled
    public String negativeBalanceTopUp {get; set;}

    @AuraEnabled
    public Boolean newPositionsLocked {get; set;}

    @AuraEnabled
    public Integer openPositionCount {get; set;}

    @AuraEnabled
    public Integer openTradeCount {get; set;}

    @AuraEnabled
    public Boolean orderCancelLocked {get; set;}

    @AuraEnabled
    public Boolean orderCreationLocked {get; set;}

    @AuraEnabled
    public Boolean orderFillLocked {get; set;}

    @AuraEnabled
    public Integer pendingOrderCount {get; set;}

    @AuraEnabled
    public String pl {get; set;}

    @AuraEnabled
    public Integer positionAggregationModeOverride {get; set;}

    @AuraEnabled
    public String positionValue {get; set;}

    @AuraEnabled
    public List<Integer> pricingGroupIDs {get; set;}

    @AuraEnabled
    public List<AccountProperty> properties {get; set;}

    @AuraEnabled
    public String resettablePL {get; set;}

    @AuraEnabled
    public String resettablePLTime {get; set;}

    @AuraEnabled
    public Integer siteID {get; set;}

    @AuraEnabled
    public String state	{get; set;}

    @AuraEnabled
    public Integer tradingGroupID {get; set;}

    @AuraEnabled
    public String unrealizedPL {get; set;}

    @AuraEnabled
    public String userID {get; set;}

    @AuraEnabled
    public String withdrawalLimit {get; set;}

    @AuraEnabled
    public Boolean withdrawalLocked {get; set;}
    
    @AuraEnabled
    public transient Boolean isMT4Account {
        get {
            return String.isNotBlank(mt4AccountID);
        }
    }
    
    @AuraEnabled
    public transient Boolean unchanged {
        get {
            return true;
        }
    }
    
    @AuraEnabled
    public transient Boolean dtgChanged {
        get {
            return false;
        }
    }
    
    @AuraEnabled
    public transient Boolean serverChanged {
        get {
            return false;
        }
    }

    @AuraEnabled
    public transient Integer originalDivisionTradingGroupID {
        get {
            return divisionTradingGroupID;
        }
    }

    @AuraEnabled
    public transient Integer originalMt4ServerID {
        get {
            return mt4ServerID;
        }
    }

    @AuraEnabled
    public transient List<PicklistOption> dtgOptions {get; set;}

    @AuraEnabled
    public transient List<PicklistOption> mt4ServerOptions {get; set;}

    public static Map<String, String> getPropertyMappings() {
        return new Map<string, string> {
            'currency' => 'currencyISO'
        };
    }    

    public class AccountProperty {
        public Integer accountPropertyID {get; set;}

        public String accountPropertyName {get; set;}
    }

    public class PicklistOption {
        @AuraEnabled
        public String label {get; set;}

        @AuraEnabled
        public Integer value {get; set;}

        @AuraEnabled
        public Boolean selected {get; set;}

        @AuraEnabled
        public Boolean disabled {get; set;}
    }
}