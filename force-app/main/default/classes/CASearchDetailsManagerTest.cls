/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-16-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class CASearchDetailsManagerTest {
    static CalloutMock getMock() {
        return new CalloutMock(
			200,
			'{' +
			'	"content": {' +
			'		"data": {' +
			'			"id": 111111111,' +
			'			"ref": "1111111111-UNWU1V0z",' +
			'			"searcher_id": 478009745,' +
			'			"assignee_id": 478009746,' +
			'			"filters": {' +
			'				"birth_year": 1998,' +
			'				"country_codes": [ "CA" ],' +
			'				"exact_match": false,' +
			'				"fuzziness": 0.5,' +
			'				"remove_deceased": 0,' +
			'				"entity_type": "person",' +
			'				"types": [' +
			'					"pep-class-1"' +
			'				]' +
			'			},' +
			'			"match_status": "false_positive",' +
			'			"risk_level": "low",' +
			'			"search_term": "Qian-Yu Tong",' +
			'			"submitted_term": "Qian-Yu Tong",' +
			'			"client_ref": "Qianyu1998",' +
			'			"total_hits": 1,' +
			'			"total_matches": 1,' +
			'			"total_blacklist_hits": 0,' +
			'			"created_at": "2019-08-02 01:49:14",' +
			'			"updated_at": "2022-09-07 01:13:14",' +
			'			"blacklist_filters": {' +
			'				"blacklist_ids": [' +
			'					"61ea14d3b338db93377345545"' +
			'				]' +
			'			},' +
			'			"tags": [],' +
			'			"labels": [],' +
			'			"search_profile": {' +
			'				"name": "OAP",' +
			'				"slug": "oap"' +
			'			},' +
			'			"limit": 100,' +
			'			"offset": 0,' +
			'			"searcher": {' +
			'				"id": 6601,' +
			'				"email": "oap-compliance-test@oanda.com",' +
			'				"name": "Oanda Compliance Team ",' +
			'				"phone": "",' +
			'				"created_at": "2019-07-01 02:34:03",' +
			'				"user_is_active": false' +
			'			},' +
			'			"assignee": {' +
			'				"id": 7670,' +
			'				"email": "Aira.test_demo@test.com",' +
			'				"name": "Aira Test",' +
			'				"phone": "",' +
			'				"created_at": "2019-10-25 20:43:09",' +
			'				"user_is_active": false' +
			'			},' +
			'			"hits": [' +
			'				{' +
			'					"doc": {' +
			'						"id": "4857893475W2WBST",' +
			'						"last_updated_utc": "2022-09-13 16:23:06",' +
			'						"created_utc": "2022-09-13 16:23:06",' +
			'						"fields": [' +
			'							{' +
			'								"name": "Country",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "China"' +
			'							},' +
			'							{' +
			'								"name": "Original Country Text",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "China"' +
			'							},' +
			'							{' +
			'								"locale": "en",' +
			'								"name": "Political Position",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"tag": "political_position",' +
			'								"value": "Member of the Local Council"' +
			'							},' +
			'							{' +
			'								"name": "Chamber",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "Standing Committee of Qingdao Municipal People\'s Congress"' +
			'							},' +
			'							{' +
			'								"name": "Function",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "委 员"' +
			'							},' +
			'							{' +
			'								"name": "Institution Type",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "City Council"' +
			'							},' +
			'							{' +
			'								"name": "Locationurl",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "http://index.html"' +
			'							},' +
			'							{' +
			'								"name": "Region",' +
			'								"source": "china-standing-committee-of-qingdao-municipal-peoples-congress-members",' +
			'								"value": "Shandong"' +
			'							},' +
			'							{' +
			'								"name": "Countries",' +
			'								"tag": "country_names",' +
			'								"value": "China"' +
			'							}' +
			'						],' +
			'						"account_ids": [' +
			'							456789' +
			'						],' +
			'						"media": [' +
			'							{' +
			'								"date": "2018-09-17T00:00:00Z",' +
			'								"pdf_url": "https://www.thezimbabwemail.com/law-crime/mugabes-son-in-law-under-probe/",' +
			'								"snippet": "- Mugabe\'s son-in-law under probe PRESIDENT Emmerson Mnangagwa\'s ...",' +
			'								"title": "Mugabe\'s son-in-law under probe – The Zimbabwe Mail",' +
			'								"url": "https://www.thezimbabwemail.com/law-crime/mugabes-son-in-law-under-probe/"' +
			'							}' +
			'						],' +
			'						"types": [' +
			'							"pep",' +
			'							"pep-class-4"' +
			'						],' +
			'						"name": "童 煜 (Tong  Yu )",' +
			'						"entity_type": "person",' +
			'						"aka": [' +
			'							{' +
			'								"name": "Tong  Yu "' +
			'							},' +
			'							{' +
			'								"name": "童 煜"' +
			'							}' +
			'						],' +
			'						"sources": [' +
			'							"china-standing-committee-of-qingdao-municipal-peoples-congress-members"' +
			'						],' +
			'						"keywords": [ "some" ],' +
			'						"blacklist_ids": [ "some" ],' +
			'						"assets": [' +
			'							{' +
			'								"public_url": "http://url.com"' +
			'							},' +
			'							{' +
			'								"source": "china"' +
			'							},' +
			'							{' +
			'								"type": "picture"' +
			'							}' +
			'						],' +
			'						"associates": [' +
			'							{' +
			'								"name": "china name",' +
			'								"association": "association"' +
			'							}' +
			'						],' +
			'						"source_notes": {' +
			'							"china-standing-committee-of-qingdao-municipal-peoples-congress-members": {' +
			'								"aml_types": [' +
			'									"pep-class-4"' +
			'								],' +
			'								"country_codes": [' +
			'									"CN"' +
			'								],' +
			'								"name": "China Standing Committee of Qingdao Municipal People\'s Congress Members",' +
			'								"url": "http://rdcwh.qingdao.gov.cn/n8146584/index.html"' +
			'							}' +
			'						}' +
			'					},' +
			'					"match_types": [' +
			'						"name_exact",' +
			'						"name_variations_removal"' +
			'					],' +
			'					"match_types_details": {' +
			'						"Tong Yu": {' +
			'							"match_types": {' +
			'								"qian": [' +
			'									"name_variations_removal"' +
			'								],' +
			'								"tong": [' +
			'									"name_exact"' +
			'								],' +
			'								"yu": [' +
			'									"name_exact"' +
			'								]' +
			'							},' +
			'							"type": "name"' +
			'						}' +
			'					},' +
			'					"match_status": "potential_match",' +
			'					"is_whitelisted": false,' +
			'					"score": 30.107817, ' +
			'					"risk_level": "low"' +
			'				}' +
			'			],' +
			'			"blacklist_hits": [],' +
			'			"share_url": "http://share-url.com"' +
			'		}' +
			'	}' +
			'}',
			'OK',
			null
		);
    }

    static CASearchDetails getTestDetails() {
        CASearchDetails details = new CASearchDetails();
        details.id = '111111111';
		details.ref = '1111111111-UNWU1V0z';
		details.searcher_id = 478009745;
		details.assignee_id = 478009746;
        details.filters = new CASearchDetailsFilters();
		details.filters.birth_year = 1998;
		details.filters.country_codes = new List<String> { 'CA' };
		details.filters.entity_type = 'person';
		details.filters.exact_match = false;
		details.filters.fuzziness = 0.5;
		details.filters.remove_deceased = 0;
		details.filters.types = new List<String>();
		details.match_status = 'false_positive';
		details.risk_level = 'low';
        details.search_term = 'Qian-Yu Tong';
        details.submitted_term = 'Qian-Yu Tong';
        details.client_ref = 'Qianyu1998';
        details.total_hits = 1;
        details.total_matches = 1;
        details.total_blacklist_hits = 0;
        details.created_at = '2019-08-02 01:49:14';
        details.updated_at = '2022-09-07 01:13:14';
        details.blacklist_filters = new CASearchDetailsBlacklistFilters();
        details.blacklist_filters.blacklist_ids = new List<String> { '61ea14d3b338db93377345545' };
        details.search_profile = new CASearchDetailsProfile();
        details.search_profile.name = 'OAP';
        details.search_profile.slug = 'oap';
        details.limitt = 100;
        details.offset = 0;
        details.searcher = new CASearchDetailsSearcher();
        details.searcher.id = 6601;
        details.searcher.email = 'oap-compliance-test@oanda.com';
        details.searcher.name = 'Oanda Compliance Team';
        details.searcher.phone = '';
        details.searcher.created_at = '2019-07-01 02:34:03';
        details.searcher.user_is_active = false;
        details.assignee = new CASearchDetailsAssignee();
        details.assignee.id = 7670;
        details.assignee.email = 'Aira.test_demo@test.com';
        details.assignee.name = 'Aira Test';
        details.assignee.phone = '';
        details.assignee.created_at = '2019-10-25 20:43:09';
        details.assignee.user_is_active = false;

        CASearchDetailsField f = new CASearchDetailsField();
        f.locale = 'en';
        f.name = 'Political Position';
        f.source = 'china-standing-committee-of-qingdao-municipal-peoples-congress-members';
        f.tag = 'political_position';
        f.value = 'Member of the Local Council';

        CASearchDetailsAka k = new CASearchDetailsAka();
        k.name = 'Tong  Yu';
        k.first_name = 'Tong';
        k.last_name = 'Yu';

        CASearchDetailsMedia m = new CASearchDetailsMedia();
        m.datee = '2018-09-17T00:00:00Z';
        m.pdf_url = 'https://www.thezimbabwemail.com/law-crime/mugabes-son-in-law-under-probe/';
        m.snippet = '- Mugabe\'s son-in-law under probe PRESIDENT Emmerson Mnangagwa\'s ...';
        m.title = 'Mugabe\'s son-in-law under probe – The Zimbabwe Mail';
        m.url = 'https://www.thezimbabwemail.com/law-crime/mugabes-son-in-law-under-probe/';

        CASearchDetailsAsset a = new CASearchDetailsAsset();
        a.public_url = 'http://url.com';
        a.source = 'china';
        a.type = 'picture';
		a.url = 'http://url.com';

        CASearchDetailsAssociate aso = new CASearchDetailsAssociate();
        aso.name = 'china name';
        aso.association = 'association';

        CASearchDetailsSourceNotes sn = new CASearchDetailsSourceNotes();
        sn.aml_types = new List<String> { 'pep-class-4' };
        sn.country_codes = new List<String> { 'CN' };
        sn.listing_ended_utc = '2022-09-13 16:23:06';
        sn.listing_started_utc = '2022-09-13 16:23:06';
        sn.name = 'China Standing Committee of Qingdao Municipal People\'s Congress Members';
        sn.source_id = '3456787';
        sn.url = 'http://rdcwh.qingdao.gov.cn/n8146584/index.html';

        CASearchDetailsDoc doc = new CASearchDetailsDoc();
        doc.id = '4857893475W2WBST';
        doc.last_updated_utc = '2022-09-13 16:23:06';
        doc.created_utc = '2022-09-13 16:23:06';
        doc.fields = new List<CASearchDetailsField> { f };
        doc.types = new List<String> { 'pep' };
        doc.name = '童 煜 (Tong  Yu )';
        doc.entity_type = 'personZ';
        doc.aka = new List<CASearchDetailsAka> { k };
        doc.sources = new List<String> { 'china-standing-committee-of-qingdao-municipal-peoples-congress-members' };
        doc.keywords = new List<String> { 'some' };
        doc.blacklist_ids = new List<String> { 'some' };
        doc.account_ids = new List<Integer> { 456789 };
        doc.media = new List<CASearchDetailsMedia> { m };
        doc.assets = new List<CASearchDetailsAsset> { a };
        doc.associates = new List<CASearchDetailsAssociate> { aso };
        doc.source_notes = new Map<String, CASearchDetailsSourceNotes> { 'china' => sn };

        CASearchDetailsMatchTypeDet d = new CASearchDetailsMatchTypeDet();
        d.match_types = new Map<String, List<String>>();
        d.type = 'type';

        CASearchDetailsHits hit = new CASearchDetailsHits();
        hit.doc = doc;
        hit.match_types = new List<String>();
        hit.match_types_details = new Map<String, CASearchDetailsMatchTypeDet> { 'china' => d };
        hit.match_status = 'match';
        hit.is_whitelisted = false;
        hit.score = 0;
        hit.risk_level = 'low';

        details.hits = new List<CASearchDetailsHits> { hit };
        details.blacklist_hits = new List<CASearchDetailsHits>();
        details.share_url = 'http://share-url.com';

        return details;
    }

    static List<Comply_Advantage_Search_Update__mdt> getFieldsInfos() {        
        Comply_Advantage_Search_Update__mdt fiNew1 = new Comply_Advantage_Search_Update__mdt();
        fiNew1.Active__c = true;
        fiNew1.DeveloperName = 'Total Hits';
        fiNew1.Field_Api_Name__c = 'Total_Hits__c';
        fiNew1.Force_Update__c = false;
        fiNew1.Method_Name__c = 'totalHits';
        
        Comply_Advantage_Search_Update__mdt fiNew2 = new Comply_Advantage_Search_Update__mdt();
        fiNew2.Active__c = true;
        fiNew2.DeveloperName = 'Birth Year';
        fiNew2.Field_Api_Name__c = 'Search_Parameter_Birth_Year__c';
        fiNew2.Force_Update__c = false;
        fiNew2.Method_Name__c = 'birthYear';
        
        Comply_Advantage_Search_Update__mdt fiNew3 = new Comply_Advantage_Search_Update__mdt();
        fiNew3.Active__c = true;
        fiNew3.DeveloperName = 'Fuzziness';
        fiNew3.Field_Api_Name__c = 'Fuzziness__c';
        fiNew3.Force_Update__c = false;
        fiNew3.Method_Name__c = 'fuzziness';
        
        Comply_Advantage_Search_Update__mdt fiNew4 = new Comply_Advantage_Search_Update__mdt();
        fiNew4.Active__c = true;
        fiNew4.DeveloperName = 'Search Profile';
        fiNew4.Field_Api_Name__c = 'Search_Profile__c';
        fiNew4.Force_Update__c = false;
        fiNew4.Method_Name__c = 'searchProfile';
        
        Comply_Advantage_Search_Update__mdt fiNew5 = new Comply_Advantage_Search_Update__mdt();
        fiNew5.Active__c = true;
        fiNew5.DeveloperName = 'Search Parameter Mailing Country';
        fiNew5.Field_Api_Name__c = 'Search_Parameter_Mailing_Country__c';
        fiNew5.Force_Update__c = false;
        fiNew5.Method_Name__c = 'country';
        
        Comply_Advantage_Search_Update__mdt fiNew6 = new Comply_Advantage_Search_Update__mdt();
        fiNew6.Active__c = true;
        fiNew6.DeveloperName = 'Search Parameter Citizenship Nationality';
        fiNew6.Field_Api_Name__c = 'Search_Parameter_Citizenship_Nationality__c';
        fiNew6.Force_Update__c = false;
        fiNew6.Method_Name__c = 'nationality';

        return new List<Comply_Advantage_Search_Update__mdt> {
            fiNew1,
            fiNew2,
            fiNew3,
            fiNew4,
            fiNew5,
            fiNew6
        };
    }

    @isTest
	static void updateCASearch() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '111111111',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
            Total_Hits__c = 0,
			Custom_Division_Name__c = 'OANDA Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = getMock();
		
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

		CASearchDetailsManager manager = new CASearchDetailsManager([
			SELECT
                Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c, Division_Name__c,
                Search_Profile__c, Fuzziness__c, Search_Parameter_Birth_Year__c, Total_Hits__c,
                Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c,
                fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c
            FROM Comply_Advantage_Search__c
            WHERE Id = :search.Id
        ]);
        manager.fieldsInfo = new List<Comply_Advantage_Search_Update__mdt>();
        manager.fieldsInfo.addAll(getFieldsInfos());
        manager.startUpdating();

		Test.stopTest();

		search = [
			SELECT 
                Fuzziness__c, Total_Hits__c, Search_Parameter_Birth_Year__c,
                Search_Profile__c, Search_Parameter_Mailing_Country__c,
                Search_Parameter_Citizenship_Nationality__c
			FROM Comply_Advantage_Search__c
			WHERE Id = :search.Id
		];
		System.assertEquals(50, search.Fuzziness__c);
		System.assertEquals('1998', search.Search_Parameter_Birth_Year__c);
		System.assertEquals(1, search.Total_Hits__c);
		System.assertEquals('oap', search.Search_Profile__c);
		System.assertEquals('Canada', search.Search_Parameter_Mailing_Country__c);
		System.assertEquals('Canada', search.Search_Parameter_Citizenship_Nationality__c);
	}

    @isTest
	static void updateCASearchIssue() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '111111111',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
            Total_Hits__c = 0,
			Custom_Division_Name__c = 'OANDA Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			400,
			'{ "code": 400, "message": "search not found", "status": "failure" }',
			'OK',
			null
		);
		
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

		CASearchDetailsManager manager = new CASearchDetailsManager([
			SELECT
                Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c, Division_Name__c,
                Search_Profile__c, Fuzziness__c, Search_Parameter_Birth_Year__c, Total_Hits__c,
                Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c,
                fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c
            FROM Comply_Advantage_Search__c
            WHERE Id = :search.Id
        ]);
        manager.fieldsInfo = new List<Comply_Advantage_Search_Update__mdt>();
        manager.fieldsInfo.addAll(getFieldsInfos());
        manager.startUpdating();

		Test.stopTest();

		search = [
			SELECT CA_not_updated__c
			FROM Comply_Advantage_Search__c
			WHERE Id = :search.Id
		];
		System.assertEquals('search not found', search.CA_not_updated__c);
	}

	@isTest
	static void getSearchDetailsTest() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 0,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '111111111',
			Is_Monitored__c = true,
			Fuzziness__c = 0,
			Custom_Division_Name__c = 'OANDA Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = getMock();
		
		Test.setMock(HttpCalloutMock.class, mock);

        Test.StartTest();

        CASearchDetailsManager manager = new CASearchDetailsManager([
			SELECT
                Id, Search_Id__c, Search_Reference__c, Custom_Division_Name__c,
                Division_Name__c, Fuzziness__c, Search_Parameter_Birth_Year__c,
                Search_Parameter_Mailing_Country__c, Search_Parameter_Citizenship_Nationality__c,
                fxAccount__c, fxAccount__r.Country__c, fxAccount__r.Citizenship_Nationality__c
            FROM Comply_Advantage_Search__c
            WHERE Id = :search.Id
        ]);
        manager.fieldsInfo = new List<Comply_Advantage_Search_Update__mdt>();
        manager.fieldsInfo.addAll(getFieldsInfos());

        CASearchDetails details = manager.getSearchDetails(search);

        CASearchDetails copy = getTestDetails();

        Test.stopTest();

        System.assertEquals(copy.id, details.id);
	}
}