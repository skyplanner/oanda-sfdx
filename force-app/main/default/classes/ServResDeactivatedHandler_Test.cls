/**
 * @File Name          : ServResDeactivatedHandler_Test.cls
 * @Description        : ServiceResourceDeactivatedHandler_Test
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/17/2020, 1:59:16 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/17/2020   acantero     Initial Version
**/
@isTest
private class ServResDeactivatedHandler_Test {

    static final String FAKE_AGENT_ID = '0050B0000081XTXQA2';

    @isTest
    static void onServiceResourceDeactivated_test1() {
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = FAKE_AGENT_ID,
            Name = FAKE_AGENT_ID,
            Capacity__c = 3,
            Skills__c = 'LE AP LS CC CH AL IL CN OAU OCA OEL IO'
        );
        insert agentInfo;
        Test.startTest();
        List<String> userIdList = new List<String>{ FAKE_AGENT_ID };
        ServiceResourceDeactivatedHandler.onServiceResourceDeactivated(userIdList);
        Test.stopTest();
        Integer count = [select count() from Agent_Info__c];
        System.assertEquals(0, count);
    }

    @isTest
    static void onServiceResourceDeactivated_test2() {
        Boolean error = false;
        Test.startTest();
        try {
            ServiceResourceDeactivatedHandler.onServiceResourceDeactivated(null);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }


}