/**
 * Util class to do the linking process to documents
 */
public class DocumentLinkUtil {
    
    // Docs list to process
    List<Document__c> docs;
    
    // Related records to the docs
    Map<Id, Case> casesMap;
    Map<Id, fxAccount__c> fxAccsMap;

    Case docCase;
    fxAccount__c docFxAcc;

    Boolean isIntegrationUser;
    Boolean isInsert;

    /**
     * Constructor
     */
    public DocumentLinkUtil(
        List<Document__c> docs,
        Map<Id, Case> casesMap,
        Map<Id, fxAccount__c> fxAccsMap)
    {
        this.docs = docs;
        this.casesMap = casesMap;
        this.fxAccsMap = fxAccsMap;
        this.isIntegrationUser =
            UserUtil.isIntegrationUser();
    }

    /**
     * Link documents before insert
     */
    public void linkOnInsert() {
        isInsert = true;
        link();
    }

    /**
     * Link documents before update
     */
    public void linkOnUpdate() {
        isInsert = false;
        link();
    }

    /**
     * Link documents to
     *  - Account
     *  - Lead
     *  - fxAccount
     */
    private void link() {
        // Iterate doc to link
        for(Document__c doc :docs) 
        {
            // Get related records
            docCase = getRelatedCase(doc);
            docFxAcc = getRelatedFxAcc(doc);

            /* Scene 1
             * Old method 'DocumentUtil.linkDocstoLeadOrAccount'
             * TODO: Try to merge with the below conditions from 
             * 'Link Document to Profile' process builder
            */
            if(isInsert && isIntegrationUser &&
                doc.fxAccount__c != null &&
                doc.Account__c == null &&
                doc.Lead__c == null &&
                docFxAcc != null)
            {
                doc.Account__c = docFxAcc.Account__c;
                doc.Lead__c = docFxAcc.Lead__c;
            }
        
            /* Process Builder migration: 'Link Document to Profile' */
            
            // Scene 2
            // No case but linked to fxAccount
            if(doc.Case__c == null && 
                doc.fxAccount__c <> null)
            {
                doc.Account__c = docFxAcc.Account__c;
                doc.Show_on_Profile__c = true;
            }
            // Scene 3
            // No case and no fxAccount
            else if(doc.Case__c == null &&
                !doc.Show_on_Profile__c) 
            {
                doc.Show_on_Profile__c = true;
            }
            // Scene 4
            // Case is linked to Std/Demo Lead
            else if(doc.Show_on_Profile__c &&
                doc.Case__c <> null &&
                docCase != null && 
                docCase.Lead__c <> null &&
                !docCase.Lead__r.IsConverted &&
                !RecordTypeUtil.isRetailLead(docCase.Lead__r))
            {
                doc.Lead__c = docCase.Lead__c;
            }
            // Scene 5
            // Case is Orphan
            else if(doc.Lead__c <> null &&
                docCase != null && 
                docCase.AccountId <> null &&
                doc.fxAccount__c <> null &&
                doc.Show_on_Profile__c)
            {
                doc.Show_on_Profile__c = false;
            }
            // Scene 6
            // Case is lnked to Account and fxAccount
            else if(doc.Show_on_Profile__c &&
                doc.Case__c <> null &&
                docCase != null &&
                docCase.fxAccount__c <> null &&
                docCase.AccountId <> null)
            {
                doc.Account__c = docCase.AccountId;
                doc.fxAccount__c = docCase.fxAccount__c;
            }
            // Scene 7
            // Case is linked to Account but not to fxAccount
            else if(doc.Show_on_Profile__c && 
                doc.Case__c <> null && 
                docCase != null && 
                docCase.AccountId <> null)
            {
                doc.Account__c = docCase.AccountId;
            }
            // Scene 8
            // Case is linked to Lead and fxAccount
            else if(doc.Show_on_Profile__c &&
                doc.Case__c <> null &&
                docCase != null && 
                docCase.fxAccount__c <> null &&
                docCase.Lead__c <> null &&
                !docCase.Lead__r.IsConverted)
            {
                doc.Lead__c = docCase.Lead__c;
                doc.fxAccount__c = docCase.fxAccount__c;
            }
            // Scene 9
            // Case is linked to Lead but NOT to fxAccount
            else if(doc.Show_on_Profile__c &&
                doc.Case__c <> null &&
                docCase != null && 
                docCase.Lead__c <> null &&
                !docCase.Lead__r.IsConverted )
            {
                doc.Lead__c = docCase.Lead__c;
            }
        }
    }

    /**
     * Get related case
     */
    private Case getRelatedCase(
        Document__c doc)
    {
        return 
            (Case)SObjectUtil.getSobjMapVal(
                doc.Case__c,
                casesMap);
    }

    /**
     * Get related fxAccount
     */
    private fxAccount__c getRelatedFxAcc(
        Document__c doc)
    {
        return
            (fxAccount__c)
            SObjectUtil.getSobjMapVal(
                doc.fxAccount__c,
                fxAccsMap);
    }

}