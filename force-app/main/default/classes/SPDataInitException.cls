/**
 * @File Name          : SPDataInitException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/3/2023, 7:58:41 PM
**/
public inherited sharing class SPDataInitException extends Exception {
}