/**
 * @File Name          : MIFIDValidationTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-19-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/23/2020   acantero     Initial Version
**/
@isTest
public class MIFIDValidationTestDataFactory {

    //values calculated from the data inserted in the createValidableFxAccounts method
    public static final Integer FXACCOUNT_TOTAL_COUNT = 8;
    public static final Integer FXACCOUNT_VALIDATED_COUNT = 1;
    public static final Integer FXACCOUNT_INVALID_NOVALIDATED_COUNT = 5;
    public static final Integer FXACCOUNT_INVALID_TOTAL_COUNT = 6; //include validated fxAccount

    public static final String MEXICO_COUNTRY = 'Mexico';
    public static final String SOUTH_AFRICA_COUNTRY = 'South Africa';
    public static final String FRANCE_COUNTRY = 'France';
    public static final String TOGO_COUNTRY = 'Togo';
    public static final String PORTUGAL_COUNTRY = 'Portugal';
    public static final String MALTA_COUNTRY = 'Malta';

    public static final String OANDA_CORPORATION_DIV = 'OANDA Corporation';
    public static final String OANDA_EUROPE_DIV = 'OANDA Europe';
    public static final String OANDA_Europe_Markets_DIV = 'OANDA Europe Markets';

    public static final String DEFAULT_VALUE = 'CONCAT';
    
    public static Account createTestAccount(String lastName){
        Account accountObj = new Account(
            LastName = lastName, 
            PersonEmail = lastName + '@a.com', 
            Account_Status__c = 'Active'
        );
        insert accountObj;
        return accountObj;
    }

    public static fxAccount__c createFxAccount(
        String lastName,
        String division,
        String nationality, 
        String personalId, 
        String passport, 
        Boolean live,
        String type,
        String funnelStage,
        Datetime lastTradeDate,
        Boolean mifidValidated
    ) {
        Account accountObj = createTestAccount(lastName);
        String recordTypeDevName = (live)
            ? 'Retail_Live'
            : 'Retail_Practice';
        String recordTypeId = 
            Schema.SObjectType.fxAccount__c
                .getRecordTypeInfosByDeveloperName()
                .get(recordTypeDevName)
                .getRecordTypeId();
        fxAccount__c obj = new fxAccount__c(
            Name = lastName,
            Account__c = accountObj.Id,
            Type__c = type,
            Funnel_Stage__c = funnelStage,
            Division_Name__c = division,
            Citizenship_Nationality__c = nationality,
            National_Personal_ID__c = personalId,
            Passport_Number__c = passport,
            Last_Trade_Date__c = lastTradeDate
          //  MiFID_validated__c = mifidValidated
        );
        return obj;
    }

    public static fxAccount__c createValidableFxAccount(
        String lastName,
        String division,
        String nationality, 
        String personalId,
        String passport
    ) {
        return createValidableFxAccount(
            lastName,
            division,
            nationality, 
            personalId,
            passport,
            true
        );
    }

    public static fxAccount__c createValidableFxAccount(
        String lastName,
        String division,
        String nationality, 
        String personalId,
        String passport,
        Boolean insertNow
    ) {
        fxAccount__c result = createFxAccount(
            lastName,
            division,
            nationality, 
            personalId,
            passport,
            true,
            'Individual',
            'Funded',
            System.now(),
            false
        );
        if (insertNow == true) {
            insert result;
        }
        return result;
    }

    public static fxAccount__c createNonValidableFxAccount(
        String lastName,
        String nationality, 
        String personalId,
        String passport
    ) {
        return createNonValidableFxAccount(
            lastName,
            nationality, 
            personalId,
            passport,
            true
        );
    }

    public static fxAccount__c createNonValidableFxAccount(
        String lastName,
        String nationality, 
        String personalId,
        String passport,
        Boolean insertNow
    ) {
        fxAccount__c result = createFxAccount(
            lastName,
            OANDA_CORPORATION_DIV,
            nationality, 
            personalId,
            passport,
            false,
            'Individual',
            'Funded',
            null,
            false
        );
        if (insertNow == true) {
            insert result;
        }
        return result;
    }

    public static fxAccount__c createValidatedFxAccount(
        String lastName,
        String nationality, 
        String personalId,
        String passport
    ) {
        return createValidatedFxAccount(
            lastName,
            nationality,
            personalId,
            passport,
            true
        );
    }

    public static fxAccount__c createValidatedFxAccount(
        String lastName,
        String nationality, 
        String personalId,
        String passport,
        Boolean insertNow
    ) {
        fxAccount__c result = createFxAccount(
            lastName,
            OANDA_EUROPE_DIV,
            nationality, 
            personalId,
            passport,
            true,
            'Individual',
            'Funded',
            System.now(),
            true
        );
        if (insertNow == true) {
            insert result; 
            MiFID_Validation_Result__c validResult = createValidationResult(result.Id, true, insertNow);          
        }
        return result;
    }

    //create 8 FxAccount
    //-- 7 validable (2 valid, 5 invalid), 
    //-- 1 validated (invalid)
    public static List<fxAccount__c> createValidableFxAccounts() {
        //valid
        fxAccount__c ok1 = createValidableFxAccount('Negrete', OANDA_EUROPE_DIV, MEXICO_COUNTRY, null, '1224568', false);
        //valid
        fxAccount__c ok2 = createValidableFxAccount('Zulu', OANDA_EUROPE_DIV, SOUTH_AFRICA_COUNTRY, DEFAULT_VALUE, 'A85967412', false);
        //invalid (passport)
        fxAccount__c error1 = createValidableFxAccount('Griezmann', OANDA_Europe_Markets_DIV, FRANCE_COUNTRY, null, '74747', false);
        //invalid (passport repetitive)  with country WITHOUT custom metadata info (DEFAULT)
        fxAccount__c error2 = createValidableFxAccount('Togoman', OANDA_EUROPE_DIV, TOGO_COUNTRY, null, 'AAAAAAA', false);
        //invalid (National Personal ID consecutive)
        fxAccount__c error3 = createValidableFxAccount('Figo', OANDA_EUROPE_DIV, PORTUGAL_COUNTRY, '123987611', null, false);
        //invalid with country without Citizenship / Nationality but VALIDATED (MiFID_validated__c = true)
        fxAccount__c error4 = createValidableFxAccount('ET', OANDA_EUROPE_DIV, null, 'X99', null, false);
        //invalid (NID must be default, passport has wrong format)
        fxAccount__c error5 = createValidableFxAccount('Cantinflas', OANDA_EUROPE_DIV, MEXICO_COUNTRY, '1122334', 'B1346899', false);
        //invalid (but already validated) (NID must be default)
        fxAccount__c validated1 = createValidatedFxAccount('Luna', MEXICO_COUNTRY, 'A12', null, false);
		List<fxAccount__c> objList = 
            new List<fxAccount__c> {ok1, ok2, error1, error2, error3, error4, error5, validated1};
        insert objList;
        return objList;
    }

    public static fxAccount__c createValidValidableFxAccount() {
        return MIFIDValidationTestDataFactory.createValidableFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.OANDA_EUROPE_DIV, 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568', 
            true
        );
    }

    public static List<ID> getValidableFxAccountsIdList() {
        List<fxAccount__c> objList = createValidableFxAccounts();
        List<ID> idList = new List<ID>();
        for(fxAccount__c obj : objList) {
            idList.add(obj.Id);
        }
        return idList;
    }

    //return Expression Validation Map corresponding to fxAccounts created in "createValidableFxAccounts" method
    public static Map<String,ExpressionValidationWrapper> getValFxAccountsExpValMap() {
        return getValFxAccountsExpValMap(Datetime.now());
    }

    public static Map<String,ExpressionValidationWrapper> getValFxAccountsExpValMap(
        Datetime lastModifiedDate
    ) {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        String nidSuffix = valSettings.natPersIDSuffix;
        String passSuffix = valSettings.passportSuffix;
        Map<String,ExpressionValidationWrapper> expValMap 
            = new Map<String,ExpressionValidationWrapper>();
        //...Mexico
        //.........Personal ID
        ExpressionValidationWrapper mexicoPersId = new ExpressionValidationWrapper();
        mexicoPersId.developerName = 'MX_MIFID' + nidSuffix;
        mexicoPersId.key = MEXICO_COUNTRY + nidSuffix;
        mexicoPersId.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        mexicoPersId.canBeDefaultValue = MiFIDExpValConst.MUST_BE_VALUE;
        expValMap.put(mexicoPersId.key, mexicoPersId);
        //.........Passport
        ExpressionValidationWrapper mexicoPass = new ExpressionValidationWrapper();
        mexicoPass.developerName = 'MX_MIFID' + passSuffix;
        mexicoPass.key = MEXICO_COUNTRY + passSuffix;
        mexicoPass.validation1 = '\\d{7}' + '\n' + '[A-Z]{1}\\d{6}(\\d{2})?';
        mexicoPass.checkConsecutiveNumbers = true;
        mexicoPass.checkRepetitiveNumber = true;
        mexicoPass.checkConsecutiveLetters = true;
        mexicoPass.checkRepetitiveLetter = true;
        mexicoPass.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        expValMap.put(mexicoPass.key, mexicoPass);
        //...South Africa
        //.........Personal ID
        ExpressionValidationWrapper southAfricaPersId = new ExpressionValidationWrapper();
        southAfricaPersId.developerName = 'ZA_MIFID' + nidSuffix;
        southAfricaPersId.key = SOUTH_AFRICA_COUNTRY + nidSuffix;
        southAfricaPersId.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        southAfricaPersId.canBeDefaultValue = MiFIDExpValConst.MUST_BE_VALUE;
        expValMap.put(southAfricaPersId.key, southAfricaPersId);
        //.........Passport
        ExpressionValidationWrapper southAfricaPass = new ExpressionValidationWrapper();
        southAfricaPass.developerName = 'ZA_MIFID' + passSuffix;
        southAfricaPass.key = SOUTH_AFRICA_COUNTRY + passSuffix;
        southAfricaPass.validation1 = '[A-Z]{1}\\d{8}' + '\n' + '\\d{9}';
        southAfricaPass.checkConsecutiveNumbers = true;
        southAfricaPass.checkRepetitiveNumber = true;
        southAfricaPass.checkConsecutiveLetters = true;
        southAfricaPass.checkRepetitiveLetter = true;
        southAfricaPass.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        expValMap.put(southAfricaPass.key, southAfricaPass);
        //...France
        //.........Personal ID
        ExpressionValidationWrapper francePersId = new ExpressionValidationWrapper();
        francePersId.developerName = 'FR_MIFID' + nidSuffix;
        francePersId.key = FRANCE_COUNTRY + nidSuffix;
        expValMap.put(francePersId.key, francePersId);
        //.........Passport
        ExpressionValidationWrapper francePass = new ExpressionValidationWrapper();
        francePass.developerName = 'FR_MIFID' + passSuffix;
        francePass.key = FRANCE_COUNTRY + passSuffix;
        francePass.canBeBlank = MiFIDExpValConst.MUST_BE_VALUE;
        expValMap.put(francePass.key, francePass);
        //...Portugal
        //.........Personal ID
        ExpressionValidationWrapper portugalPersId = new ExpressionValidationWrapper();
        portugalPersId.developerName = 'PT_MIFID' + nidSuffix;
        portugalPersId.key = PORTUGAL_COUNTRY + nidSuffix;
        portugalPersId.validation1 = '\\d{9}';
        portugalPersId.checkConsecutiveNumbers = true;
        portugalPersId.checkRepetitiveNumber = true;
        portugalPersId.checkConsecutiveLetters = true;
        portugalPersId.checkRepetitiveLetter = true;
        portugalPersId.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        portugalPersId.canBeDefaultValue = MiFIDExpValConst.CAN_BE_VALUE;
        expValMap.put(portugalPersId.key, portugalPersId);
        //.........Passport
        ExpressionValidationWrapper portugalPass = new ExpressionValidationWrapper();
        portugalPass.developerName = 'PT_MIFID' + passSuffix;
        portugalPass.key = PORTUGAL_COUNTRY + passSuffix;
        portugalPass.validation1 = '[A-Z]{1,2}\\d{6}';
        portugalPass.checkConsecutiveNumbers = true;
        portugalPass.checkRepetitiveNumber = true;
        portugalPass.checkConsecutiveLetters = true;
        portugalPass.checkRepetitiveLetter = true;
        portugalPass.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        expValMap.put(portugalPass.key, portugalPass);
        //...Malta
        //.........Personal ID
        ExpressionValidationWrapper maltaPersId = new ExpressionValidationWrapper();
        maltaPersId.developerName = 'MT_MIFID' + nidSuffix;
        maltaPersId.key = MALTA_COUNTRY + nidSuffix;
        maltaPersId.validation1 = '\\d{7}[MGAPLHBZ]';
        maltaPersId.checkConsecutiveNumbers = true;
        maltaPersId.checkRepetitiveNumber = true;
        maltaPersId.checkConsecutiveLetters = true;
        maltaPersId.checkRepetitiveLetter = true;
        maltaPersId.canBeBlank = MiFIDExpValConst.CAN_BE_CONDITIONAL_VALUE;
        expValMap.put(maltaPersId.key, maltaPersId);
        //.........Passport
        ExpressionValidationWrapper maltaPass = new ExpressionValidationWrapper();
        maltaPass.developerName = 'MT_MIFID' + passSuffix;
        maltaPass.key = MALTA_COUNTRY + passSuffix;
        maltaPass.validation1 = '\\d{7}';
        maltaPass.checkConsecutiveNumbers = true;
        maltaPass.checkRepetitiveNumber = true;
        maltaPass.checkConsecutiveLetters = true;
        maltaPass.checkRepetitiveLetter = true;
        maltaPass.canBeBlank = MiFIDExpValConst.CAN_BE_CONDITIONAL_VALUE;
        expValMap.put(maltaPass.key, maltaPass);
        //...Default
        //.........Personal ID
        ExpressionValidationWrapper defaultPersId = new ExpressionValidationWrapper();
        defaultPersId.developerName = MiFIDExpValConst.DEFAULT_MIFID + nidSuffix;
        defaultPersId.key = MiFIDExpValConst.DEFAULT_MIFID + nidSuffix;
        defaultPersId.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        defaultPersId.canBeDefaultValue = MiFIDExpValConst.MUST_BE_VALUE;
        expValMap.put(defaultPersId.key, defaultPersId);
        //.........Passport
        ExpressionValidationWrapper defaultPass = new ExpressionValidationWrapper();
        defaultPass.developerName = MiFIDExpValConst.DEFAULT_MIFID + nidSuffix;
        defaultPass.key = MiFIDExpValConst.DEFAULT_MIFID + nidSuffix;
        defaultPass.checkConsecutiveNumbers = true;
        defaultPass.checkRepetitiveNumber = true;
        defaultPass.checkConsecutiveLetters = true;
        defaultPass.checkRepetitiveLetter = true;
        defaultPass.canBeBlank = MiFIDExpValConst.CAN_BE_VALUE;
        expValMap.put(defaultPass.key, defaultPass);
        //...
        for(ExpressionValidationWrapper expVal : expValMap.values()) {
            expVal.lastModifiedDate = lastModifiedDate;
        }
        return expValMap;
    }

    public static Integer showValidationErrors() {
        List<Case> caseList = 
            [select Description from Case where Type__c = :MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE];
        for(Case caseObj : caseList) {
            System.debug('Error: ' + caseObj.Description);
        }
        return caseList.size();
    }

    public static MiFID_Validation_Result__c createValidationResult(Id fxAccountId, Boolean valid, Boolean doInsert){
        MiFID_Validation_Result__c validResult = new MiFID_Validation_Result__c(fxAccount__c = fxAccountId,
        Valid__c = valid);
        if(doInsert)
          insert validResult;
        return validResult;        
    }

    public static Case createCaseWithFxAccount(fxAccount__c fxAccountObj){
  
        Case caseWithFxAccount = new Case();
        caseWithFxAccount.fxAccount__c = fxAccountObj.Id;
        caseWithFxAccount.MiFID_validated__c = false;

        insert caseWithFxAccount;
        return caseWithFxAccount;
    }

}