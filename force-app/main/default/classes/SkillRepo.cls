/**
 * @File Name          : SkillRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/18/2024, 12:17:13 PM
**/
public inherited sharing class SkillRepo {
	
	public static List<Skill> getByDevNames(Set<String> skillNameSet) {
		return [
			SELECT DeveloperName
			FROM Skill
			WHERE DeveloperName IN :skillNameSet
		];
	}
	
}