/**
 * @File Name          : CaseRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/12/2024, 11:25:56 PM
**/
public inherited sharing class CaseRepo {

	public static Case getSummaryById(ID recordId) {
		List<Case> recList = [
			SELECT 
				CaseNumber,
				OwnerId
			FROM Case
			WHERE Id = :recordId
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'Case',
			'Id = ' + recordId
		);
		return recList[0];
	}

	public static Case getChatbotInfoById(ID recordId) {
		List<Case> recList = [
			SELECT
				Email_Case_Chatbot__c,
				Origin,
				Routing_to_Agent_Truncated__c
			FROM Case
			WHERE Id = :recordId
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'Case',
			'Id = ' + recordId
		);
		return recList[0];
	}

	public static Case getRoutingInfoById(ID recordId) {
		List<Case> recList = [
			SELECT 
				Internally_Routed__c,
				Is_HVC__c,
				Routed__c,
				Routed_And_Assigned__c
			FROM Case
			WHERE Id = :recordId
		];
		SPConditionsUtil.checkRequiredResult(
			recList,
			'Case',
			'Id = ' + recordId
		);
		return recList[0];
	}

}