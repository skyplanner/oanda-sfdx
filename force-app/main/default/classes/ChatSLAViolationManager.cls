/**
 * @File Name          : ChatSLAViolationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/15/2021, 10:20:01 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/7/2021, 3:46:34 PM   acantero     Initial Version
**/
public without sharing class ChatSLAViolationManager extends SLAViolationManager {

    final String violationType;
    final String subject;
    final String body;

    ChatSLAViolationManager(
        String violationType,
        String subject,
        String body
    ) {
        this.violationType = violationType;
        this.subject = subject;
        this.body = body;
    }

    public static void hvcWaitingViolation(
        List<String> chatIdList
    ) {
        ChatSLAViolationManager manager =
            new ChatSLAViolationManager(
                SLAConst.ASA_EXCEEDED_VT,
                Label.HVCChatWaitingNotifSubject, 
                Label.HVCChatWaitingNotifBody
            );
        manager.notAssignedChatsViolation(chatIdList);
    }

    public static void hvcMissedViolation(
        List<String> chatIdList
    ) {
        ChatSLAViolationManager manager =
            new ChatSLAViolationManager(
                SLAConst.ALERT_NO_ANSWER_VT,
                Label.HVCChatMissedNotifSubject, 
                Label.HVCChatMissedNotifBody
            );
        manager.notAssignedChatsViolation(chatIdList);
    }

    public static void declinedViolation(
        String chatId,
        String agentId
    ) {
        ChatSLAViolationManager manager =
            new ChatSLAViolationManager(
                SLAConst.ALERT_NO_ANSWER_VT,
                Label.ChatBouncedNotifSubject, 
                Label.ChatBouncedNotifBody
            );
        manager.assignedChatsViolation(chatId, agentId);
    }

    //************ private methods

    void notAssignedChatsViolation(
        List<String> chatIdList
    ) {
        List<LiveChatTranscript> chatList = getChatList(chatIdList);
        List<SLAChatViolationInfo> chatViolationInfoList = 
            new List<SLAChatViolationInfo>();
        List<SLAChatViolationInfo> notificationInfoList = 
            new List<SLAChatViolationInfo>();
        for(LiveChatTranscript chatObj : chatList) {
            SLAChatViolationInfo chatViolationInfo = getSLAChatViolationInfo(chatObj);
            chatViolationInfoList.add(chatViolationInfo);
            if (
                String.isNotBlank(chatObj.Chat_Division__c) ||
                (
                    (chatObj.Account != null) &&
                    String.isNotBlank(
                        chatObj.Account.Primary_Division_Name__c
                    )
                )
            ) {
                notificationInfoList.add(chatViolationInfo);
            }
        }
        if (!notificationInfoList.isEmpty()) {
            ChatNotificationManager notifManager = new ChatNotificationManager(
                SLAConst.AgentSupervisorModel.DIVISION
            );
            notifManager.sendNotifications(
                notificationInfoList, 
                subject, 
                body
            );
        }
        saveViolationInfo(chatViolationInfoList);
    }

    void assignedChatsViolation(
        String chatId,
        String agentId
    ) {
        List<String> chatIdList = new List<String> {chatId};
        List<LiveChatTranscript> chatList = getChatList(chatIdList);
        if (chatList.isEmpty()) {
            return;
        }
        //else...
        LiveChatTranscript chatObj = chatList[0];
        SLAChatViolationInfo chatViolationInfo = getSLAChatViolationInfo(chatObj);
        chatViolationInfo.ownerId = agentId;
        if (String.isNotBlank(agentId)) {
            chatViolationInfo.ownerName = [
                SELECT 
                    Name 
                FROM 
                    User 
                WHERE 
                    Id = :agentId
            ]?.Name;
        }
        //...
        List<SLAChatViolationInfo> chatViolationInfoList = 
            new List<SLAChatViolationInfo>
            {
                chatViolationInfo
            };
        ChatNotificationManager notifManager = new ChatNotificationManager(
            SLAConst.AgentSupervisorModel.ROLE
        );
        notifManager.sendNotifications(
            chatViolationInfoList, 
            subject, 
            body
        );
        saveViolationInfo(chatViolationInfoList);
    }

    List<LiveChatTranscript> getChatList(List<String> chatIdList) {
        List<LiveChatTranscript> result = [
            SELECT  
                Id, 
                Name,
                Is_HVC__c,
                Language__c,
                Chat_Division__c,
                Inquiry_Nature__c,
                Division_Name__c,
                Chat_Type_of_Account__c,
                Account.Name,
                Account.Primary_Division_Name__c
            FROM    
                LiveChatTranscript 
            WHERE 
                Id IN :chatIdList
        ];
        return result;
    }

    SLAChatViolationInfo getSLAChatViolationInfo(LiveChatTranscript chatObj) {
        SLAChatViolationInfo result = 
            new SLAChatViolationInfo(
                chatObj.Id,
                chatObj.Name,
                violationType
            );
        result.isPersistent = true;
        result.division = getChatDivision(chatObj);
        result.divisionCode = getChatDivisionCode(chatObj);
        result.inquiryNature = chatObj.Inquiry_Nature__c;
        result.isHVC = 
            (chatObj.Is_HVC__c == OmnichanelConst.CHAT_IS_HVC_YES);
        result.language = chatObj.Language__c;
        result.liveOrPractice = chatObj.Chat_Type_of_Account__c;
        //...
        result.accountName = getChatAccountName(chatObj);
        return result;
    }

    static String getChatDivision(LiveChatTranscript chatObj) {
        String result = chatObj.Division_Name__c;
        if (
            String.isBlank(result) &&
            (chatObj.Account != null) &&
            String.isNotBlank(chatObj.Account.Primary_Division_Name__c)
        ) {
            result = chatObj.Account.Primary_Division_Name__c;
        }
        return result;
    }

    static String getChatDivisionCode(LiveChatTranscript chatObj) {
        String result = chatObj.Chat_Division__c;
        if (
            String.isBlank(result) &&
            (chatObj.Account != null) &&
            String.isNotBlank(chatObj.Account.Primary_Division_Name__c)
        ) {
            result = ServiceDivisionsManager.getInstance().getDivisionCode(
                chatObj.Account.Primary_Division_Name__c
            );
        }
        return result;
    }

    static String getChatAccountName(LiveChatTranscript chatObj) {
        String result = (chatObj.Account != null) 
            ? chatObj.Account.Name
            : Label.Unknown_Account_Name;
        return result;
    }

}