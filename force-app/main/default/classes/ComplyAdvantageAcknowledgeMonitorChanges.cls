public with sharing class ComplyAdvantageAcknowledgeMonitorChanges extends ComplyAdvantageDownloaderBase 
{
    string searchSalesforceId;
    public ComplyAdvantageAcknowledgeMonitorChanges(String searchSalesforceId, String searchId) 
    {
		super(searchId);

        this.searchSalesforceId = searchSalesforceId;

        successCodes.add(201);
        successCodes.add(204);
	  }
    public override void execute() 
    {
	    ackMonitorChanges();
	}
    public void ackMonitorChanges() 
    {
       // Update Acknowledgement in CA first
       updateComplyAdvantage();
       
       if (isResponseCodeSuccess()) 
       {
           updateSalesforceRecords();
       }
  	}

    public void updateComplyAdvantage()
    {
       post(getAcknowledgeEndpoint(), null, getBaseParams(), null);
    }

    public void updateSalesforceRecords()
    {
        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(Id=searchSalesforceId);
        search.Monitor_Changes_Acknowledged_By__c = UserInfo.getUserId();
        search.Monitor_Changes_Acknowledged_Date__c =  system.today();
        search.Has_Monitor_Changes__c = false;
        update search;
        
        Comply_Advantage_Search_Entity__c[] updateMatches = new  Comply_Advantage_Search_Entity__c[]{};
        for(Comply_Advantage_Search_Entity__c match : [SELECT Id, 
                                                              Monitor_Change_Status__c,
                                                              Is_Removed__c 
                                                      FROM Comply_Advantage_Search_Entity__c
                                                      WHERE Comply_Advantage_Search__c = :searchSalesforceId AND 
                                                            Monitor_Change_Status__c != null])
        {
            if(match.Monitor_Change_Status__c == 'Added' || 
                match.Monitor_Change_Status__c == 'Updated' || 
                match.Monitor_Change_Status__c == 'Removed')
            {
                match.Is_Removed__c = match.Monitor_Change_Status__c == 'Removed';
                match.Monitor_Change_Status__c = null;
                updateMatches.add(match);
            }
        }
        if(updateMatches.size() > 0) 
        {
            update updateMatches;
        }
    }

   	private string getAcknowledgeEndpoint()
    {
      return String.format
      (
			  SPGeneralSettings.getInstance().getValue('CA_Endpoint_Acknowledge'),
		  	new List<String> { searchReference }
		  );
	  } 
}