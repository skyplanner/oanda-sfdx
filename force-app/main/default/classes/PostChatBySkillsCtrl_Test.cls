/**
 * @File Name          : PostChatBySkillsCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/23/2021, 12:04:07 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 11:56:28 PM   acantero     Initial Version
**/
@isTest
private without sharing class PostChatBySkillsCtrl_Test {

	// @testSetup
    // static void setup() {
    // }

    //ERROR_PARAM is empty
    @isTest
    static void test1() {
        Test.setCurrentPage(Page.PostChatBySkills);
        Test.startTest();
        PostChatBySkillsCtrl instance = new PostChatBySkillsCtrl();
        Test.stopTest();
        System.assert(String.isNotBlank(instance.redirectUrl));
    }

    //ERROR_PARAM has a value
    @isTest
    static void test2() {
        Test.setCurrentPage(Page.PostChatBySkills);
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        pageParams.put(PostChatBySkillsCtrl.ERROR_PARAM, 'fake error');
        Test.startTest();
        PostChatBySkillsCtrl instance = new PostChatBySkillsCtrl();
        Test.stopTest();
        System.assertEquals(Page.LiveChatAgent.getUrl(), instance.redirectUrl);
    }
    
}