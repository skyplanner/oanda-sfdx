/**
 * Description : Test class for AffiliateEmailHandler
 * Author: ejung
 * Date : 2024-04-22
 */
@isTest
private class AffiliateEmailHandlerTest {

    @isTest
    static void testHandleInboundEmail() {
        // Create test data: Inbound email with valid sender and recipient
        Messaging.InboundEmail testEmail = new Messaging.InboundEmail();
        testEmail.subject = 'Test Subject';
        testEmail.plainTextBody = 'Test Body';
        testEmail.fromAddress = 'testuser@oanda.com';
        testEmail.toAddresses = new List<String>{'partner1@example.com'};
        testEmail.ccAddresses = new List<String>{'cc1@example.com'};

        // Create test partner record
        Affiliate__c testPartner = new Affiliate__c(
            Username__c = 'Test Partner',
            Email__c = 'partner1@example.com',
            Firstname__c = 'Test',
            LastName__c = 'Partner',
            RecordTypeId = RecordTypeUtil.AFFILIATES_RECORD_TYPE_ID
        );
        insert testPartner;

        // Create test internal user
        Profile prof = [SELECT Id FROM Profile WHERE Name='Forex Specialist']; 
        User testUser = UserUtil.getRandomTestUser();

        // Test handleInboundEmail method
        AffiliateEmailHandler handler = new AffiliateEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(testEmail, null);
        Test.stopTest();

        // Verify results
        System.assert(result.success, 'Expected the email handling to be successful');
        System.assert(String.isBlank(result.message), 'Expected no error message');

        // Verify task creation
        List<Task> createdTasks = [SELECT Id FROM Task];
        System.assertEquals(1, createdTasks.size(), 'Expected one task to be created');
    }

    @isTest
    static void testHandleInboundEmail_NoPartner() {
        // Create test data: Inbound email with valid sender but no recipient
        Messaging.InboundEmail testEmail = new Messaging.InboundEmail();
        testEmail.subject = 'Test Subject';
        testEmail.plainTextBody = 'Test Body';
        testEmail.fromAddress = 'test@example.com';

        // Test handleInboundEmail method
        AffiliateEmailHandler handler = new AffiliateEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(testEmail, null);
        Test.stopTest();

        // Verify result
        System.assert(!result.success, 'Expected the email handling to fail');
    }
}