public class LegalAgreement{
    @AuraEnabled
    public String id;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String code;
    @AuraEnabled
    public Date version;
    @AuraEnabled
    public String language;
    @AuraEnabled
    public String country;
    @AuraEnabled
    public String consent;
    public Datetime create_datetime;
    @AuraEnabled
    public Datetime update_Datetime;
    @AuraEnabled
    public List<LegalAgreement> children {get; Set;}

    public void addChild(LegalAgreement la) {
        if(la == null) {
            return;
        }

        if(children == null) {
            children = new List<LegalAgreement>{la};
        } else {
            children.add(la);
        }
    }

    public void addChildren(List<LegalAgreement> las) {
        if(las == null) {
            return;
        }

        if(children == null) {
            children = las;
        } else {
            children.addAll(las);
        }
    }
}