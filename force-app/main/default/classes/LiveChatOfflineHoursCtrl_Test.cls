/**
 * @File Name          : LiveChatOfflineHoursCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2022, 11:37:57 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/11/2022, 11:34:23 AM   acantero     Initial Version
**/
@IsTest
private without sharing class LiveChatOfflineHoursCtrl_Test {

    @IsTest
    static void liveChatOfflineHoursCtrl() {
        PageReference pageRef = Page.LiveChatOfflineHours;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ChatConst.IS_REDIRECT, 'true');
        Test.startTest();
        LiveChatOfflineHoursCtrl instance = new LiveChatOfflineHoursCtrl();
        Boolean isRedirect = instance.isRedirect;
        String htmlOffline = instance.htmlOffline;
        Test.stopTest();
        System.assertEquals(true, isRedirect, 'Invalid result');
        System.assertNotEquals(null, htmlOffline, 'Invalid result');
    }
    
}