/**
 * @File Name          : SLAViolationCanNotifyCheckerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 12:52:16 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2024, 12:19:54 PM   aniubo     Initial Version
 **/
@isTest
private class SLAViolationCanNotifyCheckerTest {
	@isTest
	private static void testInitDisableNotificationsByType() {
		// Test data setup

		// Actual test
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.initDisableNotificationsByType();
		Test.stopTest();
		SPGeneralSettings settings = SPGeneralSettings.getInstance();
		List<String> values = settings.getValueAsList(
			SLAViolationCanNotifyChecker.SP_GENERAL_SETTING_SLA_NO_NOTIFICATION_NAME
		);
		// Asserts
		if (values.isEmpty()) {
			System.assertEquals(
				0,
				checker.disableNotificationsByObjectType.size(),
				'Size must be 0'
			);
		} else {
			System.assertNotEquals(
				0,
				checker.disableNotificationsByObjectType.size(),
				'Size must not be 0'
			);
		}
	}

	@isTest
	private static void testForSingleCaseInitDisable() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = new List<String>{ 'CASE_AHT', 'CASE_BOUNCED' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			3,
			checker.disableNotificationsByObjectType.size(),
			'Size must be 3. All notification types for Case must be present.'
		);
		System.assertEquals(
			2,
			checker.disableNotificationsByObjectType.get(
					SLAConst.SLANotificationObjectType.CHAT_CASE_NOT
				)
				.size(),
			'Size must be 2. All violation types for Chat Case must be present.'
		);
		Test.stopTest();
	}

	@isTest
	private static void testForCaseInitDisable() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = new List<String>{ 'CASE_CHAT_AHT' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.containsKey(
				SLAConst.SLANotificationObjectType.CHAT_CASE_NOT
			),
			'Notification type for Chat Case must be present.'
		);
		disables = new List<String>{ 'CASE_EMAIL_AHT' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.containsKey(
				SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT
			),
			'Notification type for Email Case must be present.'
		);
		disables = new List<String>{ 'CASE_PHONE_AHT' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.containsKey(
				SLAConst.SLANotificationObjectType.PHONE_CASE_NOT
			),
			'Notification type for Phone Case must be present.'
		);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.get(
					SLAConst.SLANotificationObjectType.PHONE_CASE_NOT
				)
				.contains(SLAConst.SLAViolationType.AHT),
			'Violation type AHT must be present.'
		);
		Test.stopTest();
	}

	@isTest
	private static void testForChat() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = new List<String>{ 'CHAT_ANSWER' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.containsKey(
				SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
			),
			'Notification type for Live Chat must be present.'
		);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.get(
					SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
				)
				.contains(SLAConst.SLAViolationType.ANSWER),
			'Violation type ANSWER must be present.'
		);
		Test.stopTest();
	}

	@isTest
	private static void testForMessaging() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = new List<String>{ 'MESSAGING_ANSWER' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.containsKey(
				SLAConst.SLANotificationObjectType.MESSAGING_NOT
			),
			'Notification type for Messaging must be present.'
		);
		System.assertEquals(
			true,
			checker.disableNotificationsByObjectType.get(
					SLAConst.SLANotificationObjectType.MESSAGING_NOT
				)
				.contains(SLAConst.SLAViolationType.ANSWER),
			'Violation type ANSWER must be present.'
		);
		Test.stopTest();
	}

	@isTest
	private static void testCanNotify() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		checker.disableNotificationsByObjectType.put(
			SLAConst.SLANotificationObjectType.MESSAGING_NOT,
			new List<SLAConst.SLAViolationType>{ SLAConst.SLAViolationType.ANSWER }
		);
		
		Boolean canNotify = checker.canNotify(
			SLAConst.SLAViolationType.ANSWER,
			SLAConst.SLANotificationObjectType.MESSAGING_NOT
		);
		System.assertEquals(
			false,
			canNotify,
			'Can notify must be false.'
		);
		canNotify = checker.canNotify(
			SLAConst.SLAViolationType.ANSWER,
			SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
		);
		System.assertEquals(
			true,
			canNotify,
			'Can notify must be true.'
		);
		canNotify = checker.canNotify(
			SLAConst.SLAViolationType.BOUNCED,
			SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
		);
		System.assertEquals(
			true,
			canNotify,
			'Can notify must be true.'
		);
		Test.stopTest();
	}
	@isTest
	private static void testBadDisable() {
		Test.startTest();
		SLAViolationCanNotifyChecker checker = new SLAViolationCanNotifyChecker();
		checker.disableNotificationsByObjectType = new Map<SLAConst.SLANotificationObjectType, List<SLAConst.SLAViolationType>>();
		List<String> disables = new List<String>{ 'UNKNOWN_ANSWER' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			0,
			checker.disableNotificationsByObjectType.size(),
			'size must be 0 UNKNOWN is not a valid value '
		);
		disables = new List<String>{ 'CASE_UNKNOWN_ANSWER' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			0,
			checker.disableNotificationsByObjectType.size(),
			'size must be 0 UNKNOWN is not a valid value '
		);
		
		disables = new List<String>{ 'CASE_UNKNOWN' };
		checker.initDisableNotificationsByType(disables);
		System.assertEquals(
			0,
			checker.disableNotificationsByObjectType.size(),
			'size must be 0 UNKNOWN is not a valid value '
		);
		Test.stopTest();
	}
}