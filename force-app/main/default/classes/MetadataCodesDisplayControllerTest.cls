/* Name: MetadataCodesDisplayControllerTest
 * Description : Test Class for MetadataCodesDisplayController. 
 *               The logic is defined to get custom metadata but actually can get any component where DeveloperName is used instead of Name.
 * Author: Mikolaj Juras
 * Date : 2023 June 12
 */
@IsTest
public with sharing class MetadataCodesDisplayControllerTest {
    @IsTest
    static void testGetMetadataRecords() {
        String anyDeveloperNameRT1 = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().values()[0].DeveloperName;
        String anyDeveloperNameRT2 = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().values()[1].DeveloperName;
        Assert.areEqual(0, MetadataCodesDisplayController.getMetadataRecords('RecordType', null, null).size(), 'Empty list should be retrned for null input');
        Assert.areEqual(1, 
                        MetadataCodesDisplayController.getMetadataRecords('RecordType', 'DeveloperName', anyDeveloperNameRT1).size(), 
                        'List size should equal to 1');
        Assert.areEqual(2, 
                        MetadataCodesDisplayController.getMetadataRecords('RecordType', 'DeveloperName', '' + anyDeveloperNameRT1 + ',' + anyDeveloperNameRT2).size(), 
                        'List size should equal to 2');
    }
}