/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-19-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class RelatedListCntrl {
    @AuraEnabled(cacheable=true)
    public static RelatedListWrapper fetchRelatedList(Id recordId, String mdtDevName) {
        RelatedListHelper.RelatedListResponse resp = RelatedListHelper.getRelatedListQuery(recordId, mdtDevName);
        System.debug('query: '+ resp.query);
        List<SObject> objs = RelatedListHelper.getSObjects(resp.query);
        System.debug('objs: '+ objs);
        RelatedListWrapper result = RelatedListHelper.getWrapper(objs, resp.wrapper);
        
        return result;
    }
}