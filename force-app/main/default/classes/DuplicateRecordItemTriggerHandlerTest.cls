/**
 * Test class for the DuplicateRecordItemTriggerHandler
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   25/02/2021
 */
@isTest
public class DuplicateRecordItemTriggerHandlerTest {

    private static final Integer QTY = 100;
    private static final String BULK_LASTNAME = 'bulk';

    @testSetup static void dataCreation(){

        RecordType personAccountRecordType =  [
            SELECT Id 
            FROM RecordType 
            WHERE DeveloperName = 'PersonAccount' 
            AND sObjectType = 'Account'];

        Account a = new Account(LastName='test account');
        insert a;

        fxAccount__c liveAccount = new fxAccount__c();
        liveAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        liveAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        liveAccount.Division_Name__c = DuplicateRecordItemTriggerHandler.DIVISION_NAME_OGM;
        liveAccount.Account__c = a.Id;
        insert liveAccount;

        Account acc1, acc2;
        acc1 = new Account(
            FirstName = 'testname1',
            LastName = 'abc',
            Phone = '3051234567',
            PersonEmail = 'person1@spemail.com',
            RecordTypeId = personAccountRecordType.Id,
            fxAccount__c = liveAccount.Id
        );
        insert acc1;
        acc2 = new Account(
            FirstName = 'testname2',
            LastName = 'abc',
            Phone = '3051234567',
            PersonEmail = 'person2@spemail.com',
            RecordTypeId = personAccountRecordType.Id,
            fxAccount__c = liveAccount.Id
        );
        insert acc2;

        /* Duplicate Rules */
        List<DuplicateRule> drList = [
            SELECT 
                Id, 
                DeveloperName, 
                SobjectType, 
                SobjectSubtype 
            FROM 
                DuplicateRule
            WHERE SobjectType = :DuplicateRecordItemTriggerHandler.ACCOUNT_SOBJECT_TYPE
            AND SobjectSubtype = :DuplicateRecordItemTriggerHandler.PERSON_ACCOUNT_NAME];

        System.assert(
            !drList.isEmpty(), 
            'There is no duplicate rules defined for Person Accounts. ' + 
            'If you decided to remove the Duplicates Rules you might need to delete ' + 
            'DuplicateRecordItemTrigger, DuplicateRecordItemTriggerHandler and this test class.');

        DuplicateRecordSet dupRS = new DuplicateRecordSet();
        dupRS.DuplicateRuleId = drList[0].Id;    
        insert dupRS;
        
        DuplicateRecordItem dupRIacct = new DuplicateRecordItem();
        dupRIacct.DuplicateRecordSetId = dupRS.Id;
        dupRIacct.RecordId = acc1.Id;
        insert dupRIacct;

        dupRIacct = new DuplicateRecordItem();
        dupRIacct.DuplicateRecordSetId = dupRS.Id;
        dupRIacct.RecordId = acc2.Id;
        insert dupRIacct;


        /* BULK DATA TEST */
        List<Account> accList = new List<Account>();        
        for (Integer i = 0; i < QTY; i++) {
            accList.add(new Account(
                FirstName = 'testname',
                LastName = BULK_LASTNAME,
                Phone = '7861234567',
                PersonEmail = 'personbulk' + i + '@spemail.com',
                RecordTypeId = personAccountRecordType.Id,
                fxAccount__c = liveAccount.Id
            ));
        }
        insert accList;

        DuplicateRecordSet bulkDupRS = new DuplicateRecordSet();
        bulkDupRS.DuplicateRuleId = drList[0].Id;
        insert bulkDupRS;

        List<DuplicateRecordItem> driList = new List<DuplicateRecordItem>();
        for (Account acc : accList) {
            driList.add(new DuplicateRecordItem(
                DuplicateRecordSetId = bulkDupRS.Id,
                RecordId = acc.Id
            ));
        }
        insert driList;
    }

    @isTest
    static void testSimpleDuplicates(){

            Test.startTest();

            List<DuplicateRecordItem> diList = [SELECT Id, 
                    RecordId, 
                    Record.Name, 
                    DuplicateRecordSet.DuplicateRule.SobjectType, 
                    DuplicateRecordSet.DuplicateRule.SobjectSubtype 
                FROM DuplicateRecordItem];

            System.assert(!diList.isEmpty(), diList);

            Account acc2 = [SELECT Possible_Duplicate__c FROM Account WHERE FirstName = 'testname2'];
            System.assert(
                acc2.Possible_Duplicate__c, 
                'The Possible Duplicate checkbox was not checked');

        Test.stopTest();
    }

    @isTest
    static void testBulkDuplicates(){

            Test.startTest();

            Map<Id, Account> accMap = new Map<Id, Account>([
                SELECT Id, Possible_Duplicate__c 
                FROM Account 
                WHERE LastName = :BULK_LASTNAME]);
            System.assertEquals(QTY, accMap.values().size());

            List<DuplicateRecordItem> diList = [SELECT Id, 
                    RecordId, 
                    Record.LastName, 
                    DuplicateRecordSet.DuplicateRule.SobjectType, 
                    DuplicateRecordSet.DuplicateRule.SobjectSubtype 
                FROM DuplicateRecordItem
                WHERE RecordId IN :accMap.keySet()];

            System.assert(!diList.isEmpty(), diList);
            System.assertEquals(QTY, diList.size(), 'The qty does not match. ' + diList);

            List<Account> accList = [
                SELECT Possible_Duplicate__c 
                FROM Account 
                WHERE LastName = :BULK_LASTNAME];

            for (Account acc : accMap.values()) 
            {
                System.assert(
                    acc.Possible_Duplicate__c, 
                    'The Possible Duplicate checkbox was not checked');
            }

        Test.stopTest();
    }
}