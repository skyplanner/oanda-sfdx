/**
 * @Description  : Trigger Handler for EmailMessage sObjects.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-21
 **/ 
public with sharing class NewEmailMessageTriggerHandler extends TriggerHandler {
    private final List<EmailMessage> triggerNew;
    private final EmailCaseService caseService;

    /**
     * @description Constructor.
     * @author Jakub Fik | 2024-05-21
     **/
    public NewEmailMessageTriggerHandler() {
        this.triggerNew = (List<EmailMessage>) Trigger.new;
        this.caseService = new EmailCaseService();
    }

    /**
     * @description Override beforeInsert method.
     * @author Jakub Fik | 2024-05-21
     **/
    public override void beforeInsert() {
        this.caseService.manageOtmsNotificationCases(this.getAddressMessageMap());
    }

    /**
     * @description Override afterInsert method.
     * @author Jakub Fik | 2024-05-21
     **/
    public override void afterInsert() {
        Set<Id> parentCaseIds = new Set<Id>();
        for (EmailMessage loopMessage : this.triggerNew) {
            if (loopMessage.Incoming && loopMessage.ParentId != null) {
                parentCaseIds.add(loopMessage.ParentId);
            }
        }
        this.caseService.reopenCases(parentCaseIds);
    }


    /** 
    * @description Method creates and returns Email Address - EmailMessage map.
    * @author Jakub Fik | 2024-07-22 
    * @return Map<String, List<EmailMessage>> 
    **/
    private Map<String,List<EmailMessage>> getAddressMessageMap() {
        Map<String,List<EmailMessage>> addressMessageMap = new Map<String,List<EmailMessage>>();
        for (EmailMessage loopMessage : this.triggerNew) {
            String emailAddress = CaseUtil.getEmailFromSubject(loopMessage.Subject);
            if(String.isNotBlank(emailAddress)) {
                List<EmailMessage> mailList = addressMessageMap.containsKey(emailAddress) ? 
                    addressMessageMap.get(emailAddress) : new List<EmailMessage>();
                mailList.add(loopMessage);
                addressMessageMap.put(emailAddress, mailList);
            }
        }
        return addressMessageMap;
    }
}