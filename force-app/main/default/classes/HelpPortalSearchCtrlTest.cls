/**
 * Test class: HelpPortalSearchCtrl
 * @author Fernando Gomez
 * @version 1.0
 * @since April 1st, 2019
 */
@isTest
private class HelpPortalSearchCtrlTest {
	/**
	 * global HelpPortalSearchCtrl()
	 */
	@isTest
	static void constructor1() {
		HelpPortalSearchCtrl ctrl;
		PageReference pr;
		FAQ__kav f;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is the answer to the question',
			UrlName = 'This-is-a-question',
			Language = 'en_US'
		);
		insert f;

		// we need to publish the article
		KbManagement.PublishingService.publishArticle([
				SELECT KnowledgeArticleId
				FROM FAQ__kav
				WHERE Id = :f.Id
			].KnowledgeArticleId, true);

		pr = Page.HelpPortalSearch;
		pr.getParameters().put('search', 'question answer');
		pr.getParameters().put('pageNo', '0');
		pr.getParameters().put('pageSize', '10');
		pr.getParameters().put('language', 'en_US');
		Test.setCurrentPage(pr);
		Test.setFixedSearchResults(new List<Id> { f.Id });

		Test.startTest();
		ctrl = new HelpPortalSearchCtrl();
		Test.stopTest();

		System.assertEquals(null, ctrl.errorMessage);
		System.assert(!ctrl.isError);
		System.assertEquals(1, ctrl.faqs.size());
	}

	/**
	 * global HelpPortalSearchCtrl()
	 */
	@isTest
	static void constructor2() {
		HelpPortalSearchCtrl ctrl;
		FAQ__kav f;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is the answer to the question',
			UrlName = 'This-is-a-question',
			Language = 'en_US'
		);
		insert f;

		// we need to publish the article
		KbManagement.PublishingService.publishArticle([
				SELECT KnowledgeArticleId
				FROM FAQ__kav
				WHERE Id = :f.Id
			].KnowledgeArticleId, true);

		Test.setFixedSearchResults(new List<Id> { f.Id });
		Test.startTest();
		ctrl = new HelpPortalSearchCtrl('en_US', 'question answer', '0', '10');
		Test.stopTest();

		System.assertEquals(null, ctrl.errorMessage);
		System.assert(!ctrl.isError);
		System.assertEquals(1, ctrl.faqs.size());
	}

	/**
	 * global HelpPortalSearchCtrl()
	 */
	@isTest
	static void constructor3() {
		HelpPortalSearchCtrl ctrl;
		PageReference pr;
		FAQ__kav f;

		// we create a faq
		f = new FAQ__kav(
			Title = 'This is a question',
			Answer__c = 'This is the answer to the question',
			UrlName = 'This-is-a-question',
			Language = 'en_US'
		);
		insert f;

		// and we assign a category
		insert new FAQ__DataCategorySelection(
			ParentId = f.Id,
			DataCategoryGroupName = 'Categories',
			DataCategoryName = 'Getting_started'
		);

		//assign a division
		insert new FAQ__DataCategorySelection(
			ParentId = f.Id,
			DataCategoryGroupName = 'Division',
			DataCategoryName = 'OANDA_Canada'
		);

		// we need to publish the article
		KbManagement.PublishingService.publishArticle([
				SELECT KnowledgeArticleId
				FROM FAQ__kav
				WHERE Id = :f.Id
			].KnowledgeArticleId, true);

		pr = Page.HelpPortalSearch;
		pr.getParameters().put('category', 'Getting_started__c');
		pr.getParameters().put('division', 'OANDA_Canada');
		pr.getParameters().put('pageNo', '0');
		pr.getParameters().put('pageSize', '10');
		pr.getParameters().put('language', 'en_US');
		Test.setCurrentPage(pr);
		Test.setFixedSearchResults(new List<Id> { f.Id });

		Test.startTest();
		ctrl = new HelpPortalSearchCtrl();
		Test.stopTest();

		System.assertEquals(null, ctrl.errorMessage);
		System.assert(!ctrl.isError);
		System.assertEquals(1, ctrl.faqs.size());
	}

	/**
	 * global HelpPortalSearchCtrl()
	 */
	@isTest
	static void constructor4() {
		HelpPortalSearchCtrl ctrl;
		PageReference pr;

		ctrl = new HelpPortalSearchCtrl();
		System.assert(ctrl.isError);

		pr = Page.HelpPortalSearch;
		pr.getParameters().put('search', 'sample');
		pr.getParameters().put('language', 'en_US');
		pr.getParameters().put('pageNo', 'ggg');
		pr.getParameters().put('pageSize', 'gggg');
		Test.setCurrentPage(pr);

		ctrl = new HelpPortalSearchCtrl();
		System.assert(ctrl.isError);
		
		pr.getParameters().put('pageNo', '0');
		ctrl = new HelpPortalSearchCtrl();
		System.assert(ctrl.isError);

		pr.getParameters().put('pageSize', '20');
		ctrl = new HelpPortalSearchCtrl();
		System.assert(!ctrl.isError);
	}
}