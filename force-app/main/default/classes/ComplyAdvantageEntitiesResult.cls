/**
 * Wrapper for Comply advantage JSON: Entities result Result
 * @author Fernando Gomez
 */
public class ComplyAdvantageEntitiesResult extends ComplyAdvantageResponseBase {
	public List<ComplyAdvantageSearchHit> content { get; set; }


	public List<Comply_Advantage_Search_Entity__c> getEntities(
			Id caSearchId,
			String searchId,
			boolean isInternalList) {
		return getEntities(caSearchId, searchId, isInternalList, content);
	}


	// /**
	//  * Returns all entities in the CA search
	//  * @param caSearchId
	//  * @param searchId
	//  */
	// public List<Comply_Advantage_Search_Entity__c> getEntities(
	// 		Id caSearchId,
	// 		String searchId) {
	// 	return getEntities(caSearchId, searchId, content);
	// }

	/**
	 * @param jsonString representation
	 * @return an instance of this class
	 */
	public static ComplyAdvantageEntitiesResult parse(String jsonString) {
		return (ComplyAdvantageEntitiesResult)JSON.deserialize(
			jsonString, ComplyAdvantageEntitiesResult.class);
	}
}