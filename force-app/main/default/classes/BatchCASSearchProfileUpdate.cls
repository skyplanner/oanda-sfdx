/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-15-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCASSearchProfileUpdate implements
    Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts,Database.RaisesPlatformEvents, BatchReflection {
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static Integer BATCH_SIZE = 100;
    public static String CRON_SCHEDULE = '0 0 0 * * ?';
    public static String GLO_MARKETS = 'OANDA Global Markets';

    public BatchCASSearchProfileUpdate() {
        this(null);
    }

    public BatchCASSearchProfileUpdate(String filter) {
        query = 'SELECT Id, Search_Id__c, Custom_Division_Name__c, ' +
        '   Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
        'WHERE Search_Profile__c = NULL AND Is_Monitored__c = TRUE ' +
        (String.isNotBlank(filter) ? filter : '');
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Search_Id__c, Custom_Division_Name__c, ' +
        '   Division_Name__c, Search_Parameter_Mailing_Country__c, ' +
        '   fxAccount__r.Lead__c, fxAccount__r.Lead__r.Country, ' +
        '   fxAccount__r.Account__c, fxAccount__r.Account__r.PersonMailingCountry ' + 
        'FROM Comply_Advantage_Search__c ' +
		'WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public static void executeBatch() {
        Database.executeBatch(new BatchCASSearchProfileUpdate(), BATCH_SIZE);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchCASSearchProfileUpdate(), BATCH_SIZE);
    }

    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        List<Comply_Advantage_Search__c> updateSearches = new List<Comply_Advantage_Search__c>();

        for (Comply_Advantage_Search__c search : scope) {
            String apiKey = ComplyAdvantageUtil.getApiKey(search);

            if (String.isNotBlank(apiKey)) {
                search.Search_Profile__c = ComplyAdvantageUtil.getSearchProfile(search.Search_Id__c, apiKey);

                if (String.isNotBlank(search.Search_Profile__c)) {
                    updateSearches.add(search);
                }
            }
        }

        if (!updateSearches.isEmpty()) {
            Database.update(updateSearches, false);
        }
    }

    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

    public static String schedule(String schedule) {
        if (String.isNotBlank(schedule)) {
            CRON_SCHEDULE = schedule;
        }
        return System.schedule((
            Test.isRunningTest() ? 'Testing Batch BatchCASSearchProfileUpdate' : 'Update CA Search: Search Profile'),
            CRON_SCHEDULE,
            new BatchCASSearchProfileUpdate()
        );
    }
}