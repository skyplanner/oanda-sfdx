/**
 * @File Name          : OnInternalCasesRoutedCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/14/2022, 2:39:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/14/2022, 10:59:31 AM   acantero     Initial Version
**/
@IsTest
private without sharing class OnInternalCasesRoutedCmd_Test {

	@testSetup
    static void setup() {
        Case newCustomerLeadCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE,
            Routed__c = true
        );
        List<Case> caseList = new List<Case>{newCustomerLeadCase1};
        OmnichanelRoutingTestDataFactory.setupAllForNonHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    // *** test1 ***
    // valid case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    // => the case meets the condition and is added to the list
    // *** test2 ***
    // valid case (Routed__c == true), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_CLOSED
    // => the case does not meet the condition and is not added to the list
    // *** test2 ***
    // invalid case (Routed__c == false), set Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    // => the case does not meet the condition and is not added to the list
    @IsTest
    static void checkRecord() {
        Case rec = getCase();
        Case oldRec = getCase();
        Test.startTest();
        OnInternalCasesRoutedCmd instance = new OnInternalCasesRoutedCmd();
        // test1
        rec.Agent_Capacity_Status__c = 
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING;
        instance.checkRecord(rec, oldRec);
        Integer caseListSize1 = instance.caseList.size();
        // test2
        rec.Agent_Capacity_Status__c = 
            OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED;
        instance = new OnInternalCasesRoutedCmd();
        instance.checkRecord(rec, oldRec);
        Integer caseListSize2 = instance.caseList.size();
        // test3
        rec.Routed__c = false;
        rec.Agent_Capacity_Status__c = 
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING;
        instance = new OnInternalCasesRoutedCmd();
        instance.checkRecord(rec, oldRec);
        Integer caseListSize3 = instance.caseList.size();
        Test.stopTest();
        System.assertEquals(1, caseListSize1, 'Invalid result');
        System.assertEquals(0, caseListSize2, 'Invalid result');
        System.assertEquals(0, caseListSize3, 'Invalid result');
    }

    // *** test1 ***
    // caseList is empty => do nothing, return false
    // *** test2 ***
    // caseList has one record => the case is routed
    @IsTest
    static void execute() {
        Test.startTest();
        OnInternalCasesRoutedCmd instance = new OnInternalCasesRoutedCmd();
        // test1
        Boolean result1 = instance.execute();
        // test2
        Case rec = getCase();
        Case oldRec = getCase();
        rec.Agent_Capacity_Status__c = 
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING;
        instance.checkRecord(rec, oldRec);
        Boolean result2 = instance.execute();
        Integer psrCount = [select count() from PendingServiceRouting];
        Test.stopTest();
        System.assertEquals(false, result1, 'Invalid result');
        System.assertEquals(true, result2, 'Invalid result');
        System.assertEquals(1, psrCount, 'Invalid result');
    }

    static Case getCase() {
        return [
            select 
                Id, 
                Agent_Capacity_Status__c, 
                Routed__c,
                Is_HVC__c
            from 
                Case
            limit 1
        ];
    }
    
}