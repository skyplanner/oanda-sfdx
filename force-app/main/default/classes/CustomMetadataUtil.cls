public with sharing class CustomMetadataUtil {
    public static void validateFxdbFields(List<SObject> sObjects, Map<Id, SObject> oldMap) {
        // Grab FXDB Field Custom Metadata
        String sObjectType = sObjects[0].getSObjectType().getDescribe().getName();
        List<FXDB_Field__mdt> fxdbFields = [SELECT Name__c, Max_Byte_Size__c FROM FXDB_Field__mdt WHERE SObject__c = :sObjectType];
        
        for(SObject sObj : sObjects) {
            SObject oldSObject = null;
            
            if(oldMap != null) {
                oldSObject = oldMap.get(sObj.Id);
            }
            
            for(FXDB_Field__Mdt fxdbField : fxdbFields) {
                String fieldValue = (String)sObj.get(fxdbField.Name__c);
                if((oldSObject == null || fieldValue != (String)oldSObject.get(fxdbField.Name__c))
                        && !UserUtil.isIntegrationUserId(UserInfo.getUserId())
                        && UserUtil.getUserIdByName(UserUtil.NAME_REGISTRATION_USER) != UserInfo.getUserId()
                        && String.isNotEmpty(fieldValue)) {
                    Blob bl = Blob.valueOf(fieldValue);
                    
                    // Check the string's byte size if it can fit in FXDB
                    if(bl.size() > fxdbField.Max_Byte_Size__c) {
                        // Throw an exception
                        sObj.adderror('The byte length is too long. Please reduce it from ' + bl.size() + ' bytes to ' + fxdbField.Max_Byte_Size__c + ' bytes. (Are you using Asian characters?)');
                    }
                }
            }
        }
    }

    private static Map<String, String> regionTemplateName;
    /**
     * Retrieve the Email Template Developer Name for the given Country or Region based on 
     * EmailLanguageExpiredDoc__mdt
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   13/04/2021
     * @return: String, Email Template developer name 
     * @param:  pLocationIdentifier, Country or Region name.
     */
    public static String getTemplateDeveloperName(String pLocationIdentifier){
        if(regionTemplateName == null){
            regionTemplateName = getTemplateIdsMap();
        }
        return regionTemplateName.get(pLocationIdentifier);
    }

    private static Map<String, String> getTemplateIdsMap(){
        Map<String, String> devNameEmailTemplatesMap = new Map<String, String>();
        Set<String> etDevNameSet = new Set<String>();
        for(EmailLanguageExpiredDoc__mdt ele : 
            [SELECT MasterLabel, Email_Template_Developer_Name__c 
            FROM    EmailLanguageExpiredDoc__mdt])
        {
            etDevNameSet.add(ele.Email_Template_Developer_Name__c);
            devNameEmailTemplatesMap.put(
                ele.MasterLabel,
                ele.Email_Template_Developer_Name__c);
        }

        Map<String, Id> emailTemplateMap = new Map<String, Id>();
        for(EmailTemplate et : [
            SELECT Id, DeveloperName
            FROM EmailTemplate 
            WHERE DeveloperName IN :etDevNameSet])
        {
            emailTemplateMap.put(et.DeveloperName, et.Id);
        }

        Map<String, Id> res = new Map<String, Id>();
        String etDevName;
        for(String location : devNameEmailTemplatesMap.keySet()){
            etDevName = devNameEmailTemplatesMap.get(location);
            if(emailTemplateMap.containsKey(etDevName)){
                res.put(location, emailTemplateMap.get(etDevName));
            }
        }
        return res;
    }
}