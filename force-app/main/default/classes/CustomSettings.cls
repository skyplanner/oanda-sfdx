public with sharing class CustomSettings {
    
    public static final String API_NAME_MY_NUMBER = 'My Number';
    public static final String API_NAME_ONBOARDING_DOC = 'Onboarding Document';
    public static final String API_NAME_EID_RESULT = 'EIDResult';
    public static final String API_NAME_FX_ACCOUNT_UPDATE = 'FxAccountUpdate';

    private static Boolean leadConversionEnabled = null;
    public static Integer maxCustomSettingLength = 34;

    private static Map<String, Round_Robin_Queue__c> roundRobinQueueMapVar;
    private static Map<String, Round_Robin_Queue__c> roundRobinQueueMap {
        get{
            if(roundRobinQueueMapVar == null){
                roundRobinQueueMapVar = Round_Robin_Queue__c.getAll();
            }
            return roundRobinQueueMapVar;
        } 
        private set;
    }
    
    public static Boolean isEnableLeadConversion() {
        
        if (leadConversionEnabled == null) {
            Settings__c settings = Settings__c.getValues('Default');
            leadConversionEnabled = (settings != null && settings.Enable_Lead_Conversion__c);

            System.debug('settings: ' + settings);
            System.debug('isEnableLeadConversion: ' + leadConversionEnabled);
        }
        return leadConversionEnabled;
    }
    
    public static void setEnableLeadConversion(Boolean enable) {
        
        leadConversionEnabled = enable;
        Settings__c settings = Settings__c.getValues('Default');
        
        if(settings == null) {
            // only need to insert settings if enabling
            if (enable) {
                settings = new Settings__c(Name='Default', Enable_Lead_Conversion__c=enable);
                insert settings;
            }
        } else {
            if (enable != settings.Enable_Lead_Conversion__c) {
                // only use a DML if changing
                settings.Enable_Lead_Conversion__c = enable;
                update settings;
            }
        }
    }
    
    private static Boolean leadMergingEnabled = null;
    
    public static Boolean isEnableLeadMerging() {
        
        if (leadMergingEnabled == null) {
            Settings__c settings = Settings__c.getValues('Default');
            leadMergingEnabled = (settings != null && settings.Enable_Lead_Merging__c);

            System.debug('settings: ' + settings);
            System.debug('isEnableLeadMerging: ' + leadMergingEnabled);
        }
        return leadMergingEnabled;
    }
    
    public static void setEnableLeadMerging(Boolean enable) {
        
        leadMergingEnabled = enable;
        Settings__c settings = Settings__c.getValues('Default');
        
        if(settings == null) {
            // only need to insert settings if enabling
            if (enable) {
                settings = new Settings__c(Name='Default', Enable_Lead_Merging__c=enable);
                insert settings;
            }
        } else {
            if (enable != settings.Enable_Lead_Merging__c) {
                // only use a DML if changing
                settings.Enable_Lead_Merging__c = enable;
                update settings;
            }
        }
    }
    
    public static String getIdOfLastUserLeadAssignedToForQueue(Id queueId) {
        Round_Robin_Queue__c queue = roundRobinQueueMap.get(queueId);
        
        if (queue == null) {
            return null;
        }
        
        return queue.Last_Assigned_User_Id__c;
    }
    
    public static Round_Robin_Queue__c getLastUserLeadAssignedToForQueue(Id queueId){
        return roundRobinQueueMap.get(queueId);
    }
    
    public static void setIdOfLastUserLeadAssignedToForQueue(Id queueId, Id userId) {
        Round_Robin_Queue__c queue = roundRobinQueueMap.get(queueId);
        
        if (queue == null) {
            queue = new Round_Robin_Queue__c(Name=queueId, Last_Assigned_User_Id__c=userId);
            insert queue;
        }
        else {
            queue.Last_Assigned_User_Id__c = userId;
            update queue;
        }
    }

    // Grab a list of country names from Custom Settings by selecting its long name first if it exists
    public static List<String> getCountryNames() {
        List<Country_Setting__c> countrySettings = Country_Setting__c.getAll().values();
        List<String> countryNames = new List<String>();
        
        for(Country_Setting__c countrySetting : countrySettings) {
            String countryName = countrySetting.Long_Name__c != null ? countrySetting.Long_Name__c : countrySetting.Name;
            countryNames.add(countryName);
        }
        
        return countryNames;
    }

    public static String getRegionByCountry(String countryName){
        String region = null;
        String countryNameAllCaps = countryName.toUpperCase();

        List<Country_Setting__c> countryList = Country_Setting__c.getAll().values();

        for(Country_Setting__c cs : countryList){
            if((countryNameAllCaps.length() <= maxCustomSettingLength && cs.Name.toUpperCase() == countryNameAllCaps)
                || (cs.Long_Name__c != null && cs.Long_Name__c.toUpperCase() == countryNameAllCaps)){
                region = cs.Region__c;
                break;
            }
        }
        
        return region;
    }

    public static String getRiskRatingByCountry(String countryName){
        String riskRating = null;
        String countryNameAllCaps = countryName.toUpperCase();

        List<Country_Setting__c> countryList = Country_Setting__c.getAll().values();

        for(Country_Setting__c cs : countryList){
            if((countryNameAllCaps.length() <= maxCustomSettingLength && cs.Name.toUpperCase() == countryNameAllCaps)
                || (cs.Long_Name__c != null && cs.Long_Name__c.toUpperCase() == countryNameAllCaps)){
                riskRating = cs.Risk_Rating__c;
                break;
            }
        }
        
        return riskRating;
    }
    
    public static Country_Setting__c getCountrySettingByCode(String countryCode){
        Country_Setting__c result = null;
        
        List<Country_Setting__c> countryList = Country_Setting__c.getAll().values();
        
        for(Country_Setting__c cs : countryList){
            if(cs.ISO_Code__c == countryCode){
                result = cs;
                
                break; 
            }
        }
        
        return result;
    }

    public static Country_Setting__c getCountrySettingByName(String countryName)
    {
        Country_Setting__c result;
        
        if(String.isNotBlank(countryName))
        {
            String countryNameAllCaps = countryName.toUpperCase();

            List<Country_Setting__c> countryList = Country_Setting__c.getAll().values();

            for(Country_Setting__c cs : countryList){
                if((countryNameAllCaps.length() <= maxCustomSettingLength && cs.Name.toUpperCase() == countryNameAllCaps)
                    || (cs.Long_Name__c != null && cs.Long_Name__c.toUpperCase() == countryNameAllCaps)){
                    result = cs;
                    break;
                }
            }
        }      
        return result;
    }
    
    public static String getLanguageByCode(String code){
        String language = null;
        
        if(code == null){
            return language;
        }
        
        Language_Code__c lanCode = Language_Code__c.getValues(code.toUpperCase());
        
        if(lanCode != null){
            language = lanCode.Language__c;
        }
        
        return language;
    }
    
    //return the latest lookback datetime
    public static DateTime getCreateRetentionOppLookBackDateTime(boolean isAttrition){
        Datetime lookback = null;
        
        Retention_Opportunity_Setting__c retOppSetting = null;
        if(isAttrition){
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Attrition Opportunity');
        }else{
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
        }
        
        if(retOppSetting == null){
            return lookback;
        }
        
        
        if(retOppSetting.Lookback_In_Months__c != null && retOppSetting.Lookback_In_Months__c > 0){
            lookback = DateTime.now().addMonths((Integer)(0 - retOppSetting.Lookback_In_Months__c));
        }
        
        if(retOppSetting.Lookback_Date__c != null && lookback != null && lookback < retOppSetting.Lookback_Date__c){
            lookback = retOppSetting.Lookback_Date__c;
        }else if(retOppSetting.Lookback_Date__c != null && lookback == null){
            lookback = retOppSetting.Lookback_Date__c;
        } 
        
        
        return lookback;
    }
    
    //Deepak Malkani : Added method to get map of ignore owner Ids while creating retention opportunity 
    public static Boolean getRetentionOppIgnoreOwners(Account accnt){
        Retention_Opportunity_Setting__c retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
        List<String> ownerNames = new List<String>();
        Map<String, String> distinctOwnersMap = new Map<String, String>();
        Map<Id, User> usrMap = new Map<Id, User>();
        if(retOppSetting != null && retOppSetting.Owners_to_Ignore__c != null && retOppSetting.Owners_to_Ignore__c != ''){  
            ownerNames = retOppSetting.Owners_to_Ignore__c.split(',');
        }
        for(Integer i=0; i<ownerNames.size(); i++){
            distinctOwnersMap.put(ownernames[i], ownernames[i]);
        }
        ownernames.clear();
        if(!distinctOwnersMap.isEmpty()){
            if(distinctOwnersMap.containsKey(accnt.Owner.Name))
            {
                System.debug('---> the account is owned by one of them whose owner needsto be ignored. Value returned is true');
                return true;
            }
            else
            {
                System.debug('---> the account is NOT owned by one of them whose owner needsto be ignored. Value returned is false');
                return false;
            }
        }
        else
            return false;
    }

    //Deepak Malkani : Added method to return Country values for Create Attrition Opportunity Custom Setting
    // JIRA Story # SP-2816
    public static Boolean getRetentionOppCountries(Account accnt){

        Boolean Flag = false;
        Retention_Opportunity_Setting__c retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
        String[] countries;
        Map<String, String> countryMap = new Map<String, String>();
        if(retOppSetting.Countries_to_Include__c != null && retOppSetting.Countries_to_Include__c != ''){
            countries = retOppSetting.Countries_to_Include__c.split(',');
        }
        if(countries!= null){
            for(Integer i=0; i<countries.size();i++){
                countryMap.put(countries[i], countries[i]);
            }
        }
        if(!countryMap.isEmpty()){
            //Map is populated and country is in the included list --so create retention Oppoty, value of flag is false.
            if (countryMap.containsKey(accnt.PersonMailingCountry))
                Flag = false;
            else
            //Map is populated and country is NOT in the included list --so DO NOT create retention Oppoty, value of flag is true.
                Flag = true;
        }
        // The custom setting itself is blanked out or is empty. In this case process any account irrespective of Country and create retention Opptys for them, so flag is false
        else{
            Flag = false;
        }
        return Flag;
        
    }

    //Deepak Malkani : Added method to return LieTimeDeposit value for Create Attrition Opportunity Custom Setting
    // JIRA Story # SP-2816
    public static Double getCreateRetentionOppLifeTimeDeposit(){

        Retention_Opportunity_Setting__c retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
        Double LifeTimeVal = Double.ValueOf(retOppSetting.LifeTime__c);
        if(retOppSetting.LifeTime__c != null)
            return Double.valueOf(retOppSetting.LifeTime__c);
        else
            return null;
    }

    public static boolean getCreateRetentionOppOnly4HouseAcct(boolean isAttrition){
        boolean result = false;
        
        Retention_Opportunity_Setting__c retOppSetting = null;
        if(isAttrition){
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Attrition Opportunity');
        }else{
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
        }
        
        if(retOppSetting != null){
            result = retOppSetting.Create_From_House_Account_Only__c;
        }
        
        return result;
    }
    
    public static boolean isUserAllowedForAPI(ID userId, String apiName){
        boolean result = false;
        
        API_Access_Control__c control = API_Access_Control__c.getValues(apiName);
        
        if(control != null && control.Enabled__c == true && control.User_Allowed__c == userId){
            result = true;
        }
        
        return result;
    }

    public static boolean isAPIEnabled(String apiName){
        boolean result = false;

        API_Access_Control__c control = API_Access_Control__c.getValues(apiName);

        if(control != null && control.Enabled__c == true){
            result = true;
        }

        return result;
    }
    
    public static List<String> getExcludedDivisions(boolean isAttrition){
         List<String> exDivisionList = new List<String>();
         Retention_Opportunity_Setting__c retOppSetting = null;
         
         if(isAttrition){
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Attrition Opportunity');
         }else{
            retOppSetting = Retention_Opportunity_Setting__c.getValues('Create Retention Opportunity');
         }
         
         if(retOppSetting != null && retOppSetting.Division_To_Exclude__c != null){
            String divisions = retOppSetting.Division_To_Exclude__c;
            
            exDivisionList = divisions.split(':');
         
         }
         
         return exDivisionList;
         
    }
    
    public static String getLiveAgentAPIRoot(){
        String result = '';
        
        Settings__c settings = Settings__c.getValues('Default');
        if(settings != null && settings.Live_Agent_API_Root__c != null){
            result = settings.Live_Agent_API_Root__c;
        }
        
        return result;
    }
    
    public static String getLiveAgentJSRoot(){
        String result = '';
        
        Settings__c settings = Settings__c.getValues('Default');
        if(settings != null && settings.Live_Agent_JS_Root__c != null){
            result = settings.Live_Agent_JS_Root__c;
        }
        
        return result;
    }
    
    public static String getLiveAgentDeployParam1(){
        String result = '';
        
        Settings__c settings = Settings__c.getValues('Default');
        if(settings != null && settings.Live_Agent_Deployment_Param1__c != null){
            result = settings.Live_Agent_Deployment_Param1__c;
        }
        
        return result;
    }
    
    public static String getLiveAgentDeployParam2(){
        String result = '';
        
        Settings__c settings = Settings__c.getValues('Default');
        if(settings != null && settings.Live_Agent_Deployment_Param2__c != null){
            result = settings.Live_Agent_Deployment_Param2__c;
        }
        
        return result;
    }

    public static Integer getTradingExperienceDurationNumber(String name){
        Integer result = null;

        if(name == null){
            return result;
        }

        Trading_Experience_Duration_Code__c code = Trading_Experience_Duration_Code__c.getValues(name);

        if(code != null){
            result = (Integer)code.Duration_Number__c;
        }

        return result;
    }

    public static Map<String, Client_Onboarding_Setting__mdt> getOnboardingSettingByDivision(){
        List<Client_Onboarding_Setting__mdt> settings = [select MasterLabel, Create_Onboarding_Case__c from Client_Onboarding_Setting__mdt];
        Map<String, Client_Onboarding_Setting__mdt> settingByDivision = new Map<String, Client_Onboarding_Setting__mdt>();

        for(Client_Onboarding_Setting__mdt cos : settings){
            settingByDivision.put(cos.MasterLabel, cos);
        }

        return settingByDivision;
    }

    public static Map<String, String> getAssessmentQuestions(string divisionName) 
    {
        List<Knowledge_Assessment_Question_Map__mdt> questions = [select Question_Text__c, Question_Field__c from Knowledge_Assessment_Question_Map__mdt where Division_Name__c =:divisionName];

        Map<String, String> questionTextByField = new Map<String, String>();

        for(Knowledge_Assessment_Question_Map__mdt q : questions)
        {
            questionTextByField.put(q.Question_Field__c, q.Question_Text__c);
        }

        return questionTextByField;
    }

    public static Map<String, String> getAssessmentQuestions()
    {
        List<Knowledge_Assessment_Question_Map__mdt> questions = [select Question_Text__c, Question_Field__c from Knowledge_Assessment_Question_Map__mdt];

        Map<String, String> questionTextByField = new Map<String, String>();

        for(Knowledge_Assessment_Question_Map__mdt q : questions){

            questionTextByField.put(q.Question_Field__c, q.Question_Text__c);

        }

        return questionTextByField;
    }

    public static String getEmailDeletionUserName() {
        String emailDeletionUserName = null;

        Settings__c settings = Settings__c.getValues('Default');
        emailDeletionUserName = settings.Email_Deletion_User__c;

		return emailDeletionUserName;
	}
	
	//get net worth range based on the given net worth value and division
	public static Net_Worth_Range__mdt getNetWorthRange(Integer netWorth, String div){
		
		if(netWorth == null){
			return null;
		}
		
		System.debug('Net Worth Value: ' + netWorth + ', Division: ' + div);
		
		List<Net_Worth_Range__mdt> ranges = [select DeveloperName, Range_Start__c, Range_End__c from Net_Worth_Range__mdt where Division_Name__c = :div and Range_Start__c <= :netWorth order by Range_Start__c desc];
		
		Net_Worth_Range__mdt range = null;
		
		if(ranges.size() == 0){
			System.debug('No division matched');
			
			//no division specific range, so get the general range
			ranges = [select DeveloperName, Range_Start__c, Range_End__c from Net_Worth_Range__mdt where Division_Name__c = null and Range_Start__c <= :netWorth order by Range_Start__c desc];
		}
		
		if(ranges.size() > 0){
			range = ranges[0];
		}
		
		return range;
	}
	
	//by default, the last trade date protection is always on
	public static boolean isLastTradeDateProtected(){
		boolean result = true;
    	
    	Settings__c settings = Settings__c.getValues('Default');
    	if(settings != null && settings.Last_Trade_Date_Protection__c != null){
    		result = settings.Last_Trade_Date_Protection__c;
    	}
    	
    	return result;
	}

	public static Boolean isEnableCRACalculation() {

		Boolean craCalculationEnabled = false;
		
		Settings__c settings = Settings__c.getValues('Default');
		craCalculationEnabled = (settings != null && settings.Enable_CRA_Calculation__c);

		System.debug('settings: ' + settings);
		System.debug('isEnableCRACalculation: ' + craCalculationEnabled);
			
		return craCalculationEnabled;
	}
       
    // Checks whether an object's country name exists in Custom Settings.
    // Certain logic depends on this being true.
    public static void validateCountryName(List<SObject> sObjects, Map<Id, SObject> oldMap) {
        final String COUNTRY_FIELD_LEAD = 'Country';
        final String COUNTRY_FIELD_ACCOUNT = 'PersonMailingCountry';
        
        // Do not validate if user is Registration User or System User
        if(UserInfo.getUserId() == UserUtil.getUserIdByName(UserUtil.NAME_REGISTRATION_USER)
           || UserInfo.getUserId() == UserUtil.getSystemUserId()
           || UserInfo.getUserName().contains('autoproc')) {
               return;
        }
        
        // Determine the country field name
        String countryField = null;
        switch on sObjects[0] {
            when Lead lead {
                countryField = COUNTRY_FIELD_LEAD;
            } when Account sObj {
                countryField = COUNTRY_FIELD_ACCOUNT;
            }
        }

        List<String> countryNames = CustomSettings.getCountryNames();
        
        for(SObject sObj : sObjects) {
            if(countryField == null) {
                sObj.adderror('Invalid sObject type. Please use one of [Lead, Account].');
            }

            SObject oldSObject = null;
            if(oldMap != null) {
                oldSObject = oldMap.get(sObj.Id);
            }
            
            String newCountryName = (String)sObj.get(countryField);
            if(oldSObject == null || newCountryName != oldSObject.get(countryField)) {
                if(!countryNames.contains((String)sObj.get(countryField))) {
                    // Provide a country suggestion based on the best Levenshtein Distance 
                    Integer bestLevenshteinDistance = 2147483647;   // 2^31-1 is the greatest Integer
                    String bestSuggestion = null;
                    
                    for(String countryName : countryNames) {
                        Integer LevenshteinDistance = newCountryName.toLowerCase().getLevenshteinDistance(countryName.toLowerCase());
                        if(LevenshteinDistance < bestLevenshteinDistance && countryName != '--') {
                            bestLevenshteinDistance = levenshteinDistance;
                            bestSuggestion = countryName;
                        }
                    }
                    
                    if(bestLevenshteinDistance == 0) {
                        // If the capitalization is different than in Custom Settings, do not generate an error
                        sObj.put(countryField, bestSuggestion);
                    } else if(bestSuggestion == null) {
                        sObj.adderror(newCountryName + ' is not a valid country name.');
                    } else {
                        sObj.adderror(newCountryName + ' is not a valid country name. Did you mean ' + bestSuggestion + '?');
                    }
                }
            }
        }
    }

    public static boolean isUserAllowedForCRACalc(String name){
        boolean result = True;
        Settings__c settings = Settings__c.getValues('Default');
        system.debug('CRA Exception User: ' + settings.CRA_Calculation_Exception_User__c);

        if(settings.CRA_Calculation_Exception_User__c == null || settings.CRA_Calculation_Exception_User__c == '') {
            return result;

        } else {
            List<String> exceptionUsers = settings.CRA_Calculation_Exception_User__c.split(',');
            for(String user : exceptionUsers) {
                if(user == name) {
                    result = False;
                } 
            }
        }

        return result;
    }

    public static Map<String, Income_Ranges__mdt> getIncomeRange(){
        List<Income_Ranges__mdt> ranges = [select MasterLabel, Range_Start__c, Range_End__c from Income_Ranges__mdt];
        Map<String, Income_Ranges__mdt> rangesByIncome = new Map<String, Income_Ranges__mdt>();

        for(Income_Ranges__mdt ir : ranges) {
            rangesByIncome.put(ir.MasterLabel, ir);
        }

        return rangesByIncome;
    }

    public static Boolean isEnableCloseLoseRetentionOpp() {

        Boolean retentionCloseLoseEnabled = false;
        
        Settings__c settings = Settings__c.getValues('Default');
        retentionCloseLoseEnabled = (settings != null && settings.Enable_Close_Lose_Retention_Opp__c);

        System.debug('settings: ' + settings);
        System.debug('isEnableCloseLoseRetentionOpp: ' + retentionCloseLoseEnabled);
            
        return retentionCloseLoseEnabled;
    }

    public static Boolean isCryptoEnabledForOmniChannel() {        
        Boolean result = Settings__c.getValues('Default')
            ?.Crypto_Enabled_For_Omni_Channel__c;

        return result != null && result;
    }

    public static Boolean isCustomNotificationForSalesEnabled() {        
        Boolean result = Settings__c.getValues('Default')
            ?.Enable_Custom_Notification_for_Sales__c;

        return result != null && result;
    }
}