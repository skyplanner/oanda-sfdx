/* Name: surveyQuestionResponseHelper
 * Description : Apex class helper on SurveyQuestionResponse__c for Before Insert and Before Update.
 * Author: Sahil Handa
 */
public class surveyQuestionResponseHelper {
    //Function to update the Survey response.
    public static void updateResponseInNumber(List<SurveyQuestionResponse__c > surveyResponseList){
        for(SurveyQuestionResponse__c Resp : surveyResponseList){
            if(Resp.Response__c == 'Excellent'){
                Resp.Response_Number__c = 5;
            }else if(Resp.Response__c == 'Very Good'){
                Resp.Response_Number__c = 4;
            }else if(Resp.Response__c == 'Satisfactory'){
                Resp.Response_Number__c = 3;
            }else if(Resp.Response__c == 'Poor'){
                Resp.Response_Number__c = 2;
            }else if(Resp.Response__c == 'Very Poor'){
                Resp.Response_Number__c = 1;
            }
        }
    }

}