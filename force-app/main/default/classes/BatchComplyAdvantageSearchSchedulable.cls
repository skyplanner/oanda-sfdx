public class BatchComplyAdvantageSearchSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable 
{
   public set<string> searchEnabledDivisions = new set<String>{};
   public string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
   public Id fxAccountLiveRecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
   public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
   public static String CRON_SCHEDULE = '0 0 6 * * ?';
   public static Id[] fxAccountIds;
   public static String filterParameters;
   public static string additionalQueryFilters = 'CreatedDate = LAST_N_DAYS:2';

   public static void schedule(string schedule) 
   {
      if(string.isNotBlank(schedule))
      {
          CRON_SCHEDULE = schedule;
      }
	  System.schedule('BatchComplyAdvantageSearchSchedulable', CRON_SCHEDULE, new BatchComplyAdvantageSearchSchedulable());
   }
   public static void executeBatch() 
   {
	   Database.executeBatch(new BatchComplyAdvantageSearchSchedulable(), 1);
   }
   public static void executeBatch(string filter) 
   {
       if(string.isNotBlank(filter))
       {
           filterParameters = filter;
       }
	   Database.executeBatch(new BatchComplyAdvantageSearchSchedulable(), 1);
   }
   public static void executeBatch(Id[] fxAccIds) 
   {
       if(fxAccIds != null && fxAccIds.size() >0)
       {
           fxAccountIds = fxAccIds;
       }
	   Database.executeBatch(new BatchComplyAdvantageSearchSchedulable(), 1);
   }
    
   public BatchComplyAdvantageSearchSchedulable() 
   {   
       // either by ids or custom date
       if(string.isNotBlank(filterParameters))
       {
           additionalQueryFilters = filterParameters;
       }
       if(fxAccountIds != null && fxAccountIds.size() >0)
       {
           additionalQueryFilters = 'Id IN :fxAccountIds';
       }
       for(Comply_Advantage_Division_Setting__c divisionSetting :[SELECT Name FROM Comply_Advantage_Division_Setting__c])
       {
			searchEnabledDivisions.add(divisionSetting.Name);
       }
	   query = 'select Id,'+
                'Division_Name__c,'+
                'Lead__c,'+
                'Lead__r.Name,'+
                'Account__c,'+
                'Account__r.Name,'+
                'Has_ComplyAdvantage_Case__c, ' +
                'Trigger_Comply_Advantage_Search__c,' +
                '(select Id from Comply_Advantage_Searches__r) '+
                'from fxAccount__c '+
                'where ' + 
                'RecordTypeId = :fxAccountLiveRecordTypeId AND '+
                'Division_Name__c IN :searchEnabledDivisions AND ' + additionalQueryFilters;
   }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'select Id,'+
        'Division_Name__c,'+
        'Lead__c,'+
        'Lead__r.Name,'+
        'Account__c,'+
        'Account__r.Name,'+
        'Has_ComplyAdvantage_Case__c, ' +
        'Trigger_Comply_Advantage_Search__c,' +
        '(select Id from Comply_Advantage_Searches__r) '+
        'from fxAccount__c WHERE Id IN :ids';
        isRerun=true;
        return Database.executeBatch(this, batchSize);
    }
   
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<fxAccount__c> scope) 
    {
        fxAccount__c fxAccount = scope[0];
        Comply_Advantage_Search__c[] caSearches = fxAccount.Comply_Advantage_Searches__r;
        
        if(((fxAccount.Lead__c != null && fxAccount.Lead__r.Name != 'default default') || 
            (fxAccount.Account__c != null && fxAccount.Account__r.Name != 'default default')) &&
            (fxAccount.Trigger_Comply_Advantage_Search__c == true || fxAccount.Has_ComplyAdvantage_Case__c == false || caSearches.size() == 0 ))
        {
            ComplyAdvantageHelper.initialComplyAdvantageSearch(fxAccount);
        }
		
	}   
    public void execute(SchedulableContext context) 
    {
		Database.executeBatch(new BatchComplyAdvantageSearchSchedulable(), 1);
	}
    
    public void finish(Database.BatchableContext bc) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if(!Test.isRunningTest())
        {
            try 
            {
                EmailUtil.sendEmailForBatchJob(bc.getJobId());
            }
            catch (Exception e) {}
        }
	}
}