/**
 * @File Name          : SRoutingInfoManager.cls
 * @Description        : 
 * Creates routing information records from PendingServiceRouting records
 * for a given object type.
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/30/2024, 2:25:47 AM
**/
public interface SRoutingInfoManager {

	void registerPendingServiceRouting(
		PendingServiceRouting pendingRoutingObj
	);

	List<BaseServiceRoutingInfo> getRoutingInfoList();
	
}