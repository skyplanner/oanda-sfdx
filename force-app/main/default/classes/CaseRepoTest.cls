/**
 * @File Name          : CaseRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/6/2024, 2:01:41 AM
**/
@IsTest
private without sharing class CaseRepoTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}
	
	// test 1 : valid
	// test 2 : exception
	@IsTest
	static void getSummaryById() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Boolean error2 = false;
		
		Test.startTest();
		// test 1
		Case result1 = CaseRepo.getSummaryById(case1Id);
		// test 2
		try {
			Case result2 = CaseRepo.getSummaryById(null);
			
		} catch(SPNoDataFoundException ex) {
			error2 = true;
		}
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isTrue(error2, 'Invalid result');
	}

	// test 1 : valid
	// test 2 : exception
	@IsTest
	static void getChatbotInfoById() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Boolean error2 = false;
		
		Test.startTest();
		// test 1
		Case result1 = CaseRepo.getChatbotInfoById(case1Id);
		// test 2
		try {
			Case result2 = CaseRepo.getChatbotInfoById(null);
			
		} catch(SPNoDataFoundException ex) {
			error2 = true;
		}
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isTrue(error2, 'Invalid result');
	}

	// test 1 : valid
	// test 2 : exception
	@IsTest
	static void getRoutingInfoById() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Boolean error2 = false;
		
		Test.startTest();
		// test 1
		Case result1 = CaseRepo.getRoutingInfoById(case1Id);
		// test 2
		try {
			Case result2 = CaseRepo.getRoutingInfoById(null);
			
		} catch(SPNoDataFoundException ex) {
			error2 = true;
		}
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isTrue(error2, 'Invalid result');
	}
	
}