/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-21-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class EmailMessageManager {
	private IScheduledEmailSettingManager settingManager;
	public EmailMessageManager(IScheduledEmailSettingManager settingManager) {
		this.settingManager = settingManager;
	}
	public Boolean canDeleteDraft(List<EmailMessage> messages) {
		List<Id> ids = new List<Id>();
		for (EmailMessage message : messages) {
			if (isDraft(message)) {
				ids.add(message.Id);
			}
		}

		List<Scheduled_Email__c> scheduledEmails = ScheduledEmailRepo.getByEmailMessagesAndOwner(
			ids,
			UserInfo.getUserId()
		);
		return scheduledEmails.isEmpty() ||
			settingManager.getSetting().getAllowDelete();
	}
	public static Boolean isDraft(EmailMessage message) {
		return message.Status == EmailMessageRepo.EMAIL_MESSAGE_STATUS_DRAFT;
	}
}