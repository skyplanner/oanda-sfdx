/**
 * @description       : SFDC Platform event dispatch util
 * @author            : Michal Piatek (mpiatek@oanda.com)
 * @group             : 
 * @last modified on  : 28-05-2024
 * @last modified by  : Michal Piatek
**/
public with sharing class PlatformEventDispatch {

    /***************************************
     * insert Api After Commit Event
     ***************************************/

    public static void insertGenericAfter(String handlerName, String eventData, String eventContext) {
        Api_Event_After__e apiEvent = new Api_Event_After__e();
        apiEvent.Request_Body__c = eventData;
        apiEvent.Handler_Type_Name__c = handlerName;
        apiEvent.Context_Variables__c = eventContext;
        publishEvent(apiEvent);
    }

    public static void insertGenericAfter(List<SObject> eventsToRepublish) {
        List<SObject> afterEventsToRepublish = new List<SObject>();
        for(SObject event : eventsToRepublish) {
            Api_Event_After__e apiEvent = new Api_Event_After__e();
            apiEvent.Request_Body__c = (String) event.get('Request_Body__c');
            apiEvent.Handler_Type_Name__c = (String) event.get('Handler_Type_Name__c');
            apiEvent.Context_Variables__c = (String) event.get('Context_Variables__c');
            afterEventsToRepublish.add(apiEvent);
        }
        publishEvents(afterEventsToRepublish);
    }

    public static void insertGenericAfter(String handlerName, String eventData, Map<String,String> eventContextMap) {
        insertGenericAfter(handlerName, eventData, JSON.serialize(eventContextMap ?? new Map<String, String>()));
    }

    public static void insertGenericAfter(String handlerName, String eventData) {
        insertGenericAfter(handlerName, eventData, (String) null);
    }

    /***************************************
     * insert Api Immediate Event
     ***************************************/

    public static void insertGenericImmediate(String handlerName, String eventData, String eventContext) {
        Api_Event_Immediate__e apiEvent = new Api_Event_Immediate__e();
        apiEvent.Request_Body__c = eventData;
        apiEvent.Handler_Type_Name__c = handlerName;
        apiEvent.Context_Variables__c = eventContext;
        publishEvent(apiEvent);
    }

    public static void insertGenericImmediate(List<SObject> eventsToRepublish) {
        List<SObject> afterEventsToRepublish = new List<SObject>();
        for(SObject event : eventsToRepublish) {
            Api_Event_Immediate__e apiEvent = new Api_Event_Immediate__e();
            apiEvent.Request_Body__c = (String) event.get('Request_Body__c');
            apiEvent.Handler_Type_Name__c = (String) event.get('Handler_Type_Name__c');
            apiEvent.Context_Variables__c = (String) event.get('Context_Variables__c');
            afterEventsToRepublish.add(apiEvent);
        }
        publishEvents(afterEventsToRepublish);
    }

    public static void insertGenericImmediate(String handlerName, String eventData, Map<String,String> eventContextMap) {
        insertGenericImmediate(handlerName, eventData, JSON.serialize(eventContextMap ?? new Map<String, String>()));
    }

    public static void insertGenericImmediate(String handlerName, String eventData) {
        insertGenericImmediate(handlerName, eventData, (String) null);
    }

    /***************************************
     * private utils
     ***************************************/

    private static void publishEvent(SObject evt) {
        publishEvents(new List<SObject>{evt});
    }

    private static void publishEvents(List<SObject> events) {
        List<Database.SaveResult> sr = EventBus.publish(events);

        for(Integer i = 0; i < sr.size(); i++){
            if (!sr[i].isSuccess()) {
                SObject evt = events[i];
                LogEvt__e errLog = new LogEvt__e(
                    Type__c = 'Error',
                    Category__c = 'API_EVENT_HANDLER: ' + evt.get('Handler_Type_Name__c'),
                    Message__c = 'Failed to publish event: ' + evt,
                    Details__c = 'ERRORS: ' +sr[i].getErrors() + '\n' + 'PAYLOAD: ' + evt.get('Request_Body__c'),
                    Id__c = (String)UserInfo.getUserId()
                );
                EventBus.publish(errLog);
            }
        }
    }

    /***************************************
     * handler interface
     ***************************************/

    public interface IPlatformEventHandlerClass {
        void handle(List<SObject> events);
    }
}