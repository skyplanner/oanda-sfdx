/**
 * @File Name          : OmnichanelRoutingException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/23/2020, 8:16:33 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/23/2020   acantero     Initial Version
**/
public class OmnichanelRoutingException extends Exception {
}