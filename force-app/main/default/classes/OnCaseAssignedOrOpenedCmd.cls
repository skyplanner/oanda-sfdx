/**
 * @File Name          : OnCaseAssignedOrOpenedCmd.cls
 * @Description        : 
 * Updates fields related to the routing process when the routed Case is 
 * assigned to an agent or opened by an agent
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/29/2024, 2:01:06 AM
**/
public inherited sharing class OnCaseAssignedOrOpenedCmd {

	@TestVisible
	final Set<ID> caseIds;

	public List<SObject> recordsToUpdate {get; private set;}

	public OnCaseAssignedOrOpenedCmd() {
		caseIds = new Set<ID>();
		recordsToUpdate = new List<SObject>();
	}

	/**
	 * Checks if the change corresponds to a Case record
	 */
	public Boolean checkRecord(
		Schema.SObjectType workItemSObjType, 
		ID workItemId
	) {
		Boolean result = false;

		if (workItemSObjType == Schema.Case.SObjectType) {
			caseIds.add(workItemId);
			result = true;
		}
		return result;
	}

	public Boolean execute() {
		if (caseIds.isEmpty()) {
			return false;
		}
		// else...
		List<Case> caseList = 
			CaseHelper.getNotRoutedOrAssigned(caseIds);

		for (Case caseObj : caseList) {
			caseObj.Routed_And_Assigned__c = true;
			caseObj.Agent_Capacity_Status__c = 
				OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE;
			recordsToUpdate.add(caseObj);
		}
		return true;
	}

}