/**
 * @File Name          : SPLiveChatTranscriptTriggerHandler.cls
 * @Description        : 
 * @Author             : Michel Carrillo (SkyPlanner)
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/27/2024, 1:21:19 AM
**/

public without sharing class SPLiveChatTranscriptTriggerHandler {

    /******************************************************* */

    public static final String OBJ_API_NAME = 'LiveChatTranscript';

    public static Boolean enabled {get; set;}

    public static Boolean isEnabled() {
        return (enabled != false) && 
            DisabledTriggerManager.getInstance()
                .triggerIsEnabledForObject(
                    OBJ_API_NAME
                );
    }
    
    //******************************************************* */

    final List<LiveChatTranscript> newList;
    final Map<ID,LiveChatTranscript> oldMap;

    public SPLiveChatTranscriptTriggerHandler(
        List<LiveChatTranscript> newList,
        Map<ID,LiveChatTranscript> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    /**
     * Handler method for the before insert event
     * author:	Michel Carrillo (SkyPlanner)
     * date:	03/26/2020
     * return:	void
     */
    public void handleBeforeInsert() {
        ChatHelper helper = new ChatHelper(newList);
        // helper.fillFromNFChatData();
        helper.completeInfo();
        checkLanguageAndDivision();
    }

    public void handleBeforeUpdate() {
        ChatHelper helper = new ChatHelper(newList);
        // helper.fillFromNFChatData();
        helper.completeInfo();
        checkLanguageAndDivision();
    }

    public void handleAfterInsert() {
        new ChatHelper(newList).checkMissedChats(null);
    }

    public void handleAfterUpdate() {
        new ChatHelper(newList).checkMissedChats(oldMap);
        OnLiveChatCaseChangeCmd caseChangedCmd = new OnLiveChatCaseChangeCmd(
            newList, // recList
            oldMap // oldMap
        );
        caseChangedCmd.execute();
    }

    void checkLanguageAndDivision() {
        FillLangAndDivisionAction langAndDivisionAction = 
            new FillLangAndDivisionAction(
                newList,
                oldMap
            );
        langAndDivisionAction.execute();
    }

    public class FillLangAndDivisionAction {

        final Map<String,String> languagesMap = new Map<String,String>();
        final Map<String,String> divisionsMap = new Map<String,String>();

        final List<LiveChatTranscript> newList;
        final Map<ID,LiveChatTranscript> oldMap;

        public FillLangAndDivisionAction(
            List<LiveChatTranscript> newList,
            Map<ID,LiveChatTranscript> oldMap
        ) {
            this.newList = newList;
            this.oldMap = oldMap;
        }

        void execute() {
            for(LiveChatTranscript chat : newList) {
                String oldLangPreference = null;
                String oldDivision = null;
                String oldDivisionName = null;
                if (oldMap != null) {
                    LiveChatTranscript oldRec = oldMap.get(chat.Id);
                    oldLangPreference = oldRec.Chat_Language_Preference__c;
                    oldDivision = oldRec.Chat_Division__c;
                    oldDivisionName = oldRec.Division_Name__c;
                }
                if (chat.Chat_Language_Preference__c != oldLangPreference) {
                    chat.Language__c = getLanguage(chat.Chat_Language_Preference__c);
                }
                if (
                    (chat.Chat_Division__c != oldDivision) &&
                    (chat.Division_Name__c == oldDivisionName)
                ) {
                    chat.Division_Name__c = getDivisionName(
                        chat.Chat_Division__c
                    );
                }
            }
        }

        String getLanguage(String langPreference) {
            String result = null;
            if (String.isNotBlank(langPreference)) {
                result = languagesMap.get(
                    langPreference
                );
                if (result == null) {
                    result = ServiceLanguagesManager.getLanguage(
                        langPreference
                    );
                    languagesMap.put(
                        langPreference, 
                        result
                    );
                }
            }
            return result;
        }

        String getDivisionName(String division) {
            String result = null;
            if (String.isNotBlank(division)) {
                result = divisionsMap.get(
                    division
                );
                if (result == null) {
                    result = 
                        ServiceDivisionsManager.getInstance().getDivisionName(
                            division
                        );
                    divisionsMap.put(
                        division, 
                        result
                    );
                }
            }
            return result;
        }
    }

}