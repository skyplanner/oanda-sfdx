/**
 * @File Name          : ChatCaseCapacityTimerCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/6/2021, 10:48:59 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/10/2021, 2:35:28 PM   acantero     Initial Version
**/
public without sharing class ChatCaseCapacityTimerCtrl {

    @AuraEnabled
    public static Integer getWrapUpTime() {
        Integer result = 
            OmnichanelSettings.getRequiredDefault().caseChatWrapUpTime;
        return result;
    }

    @AuraEnabled
    public static WorkClosingInfo getCaseWorkClosingInfo(String caseId) {
        Integer wrapUpTime = 
            OmnichanelSettings.getRequiredDefault().caseChatWrapUpTime;
        WorkClosingInfo result = new WorkClosingInfo(wrapUpTime);
        String userId = UserInfo.getUserId();
        Agent_Work_Closing__c workClosing = 
            AgentCapacityHelper.getCaseClosing(
                caseId,
                userId
            );
        if (workClosing != null) {
            result.closingId = workClosing.Id;
            result.closingTime = workClosing.Closing_Time__c.intValue();
        }
        return result;
    }

    @AuraEnabled
    public static String beginChatClosing(
        String chatId,
        String workId
    ) {
        String userId = UserInfo.getUserId();
        String result = 
            AgentCapacityHelper.beginChatClosing(
                userId, 
                chatId, 
                workId
            );
        return result;
    }

    @AuraEnabled
    public static String beginCaseClosing(
        String caseId
    ) {
        String userId = UserInfo.getUserId();
        String result = 
            AgentCapacityHelper.beginCaseClosing(
                userId, 
                caseId
            );
        return result;
    }

    @AuraEnabled
    public static void endWorkClosing(
        String closingObjId
    ) {
        AgentCapacityHelper.endWorkClosing(closingObjId);
    }

    @AuraEnabled
    public static void freeCaseCapacity(
        String caseId,
        String closingObjId
    ) {
        Set<String> caseIdSet = new Set<String>{caseId};
        AgentCapacityHelper.freeCasesCapacity(caseIdSet);
        AgentCapacityHelper.endWorkClosing(closingObjId);
    }

    //******************************************************************* */

    public class WorkClosingInfo {

        @AuraEnabled
        public Integer wrapUpTime {get; set;}
        @AuraEnabled
        public String closingId {get; set;}
        @AuraEnabled
        public Integer closingTime {get; set;}

        public WorkClosingInfo(
            Integer wrapUpTime
        ) {
            this.wrapUpTime = wrapUpTime;
        }
    }

}