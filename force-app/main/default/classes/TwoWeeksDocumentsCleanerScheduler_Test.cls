@isTest
private class TwoWeeksDocumentsCleanerScheduler_Test {

	@testSetup
	static void setup () {
	    ArticlesTestDataFactory.createArticlesWithDummyDocs();
	}

	@isTest
	static void execute_test() {
		Integer beforeCount = [select count() from Document];
		TwoWeeksDocumentsCleanerScheduler scheduler = new TwoWeeksDocumentsCleanerScheduler();
		Test.startTest();
		scheduler.execute(null);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

}