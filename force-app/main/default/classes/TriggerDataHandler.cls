public virtual  class TriggerDataHandler 
{
    public static Map<Schema.SObjectType,  list<SObject>> newRecordsByType;
    public static Map<Schema.SObjectType,  Map<Id, SObject>> updateRecordsByType;

    public virtual void insertRecord(SObject rec)
    {
        Schema.SObjectType sobjType = rec.getSObjectType();
        List<SObject> newRecords = newRecordsByType.get(sobjType);
        newRecords.add(rec);
    }
    public virtual SObject getRecord(Id recordId)
    {
        Schema.SObjectType sobjType = recordId.getSObjectType();

        Map<Id, SObject> sobjMap = updateRecordsByType.get(sobjType);

        SObject rec = sobjMap.get(recordId);

        if(rec == null)
        {
            rec = sobjType.newSObject(recordId);
        }
        
        return rec;
    }
    public virtual void updateRecord(SObject rec)
    {
        Schema.SObjectType sobjType = rec.getSObjectType();

        Map<Id, SObject> sobjMap = updateRecordsByType.get(sobjType);

        sobjMap.put(rec.Id, rec);
    }

    public void commitRecords(Schema.SObjectType sobjType, boolean disableTriggers)
    {  
        List<SObject> newRecords = (List<SObject>)newRecordsByType.get(sobjType);
 
        Map<Id, SObject> updateRecordsMap = (Map<Id, SObject>) updateRecordsByType.get(sobjType);
        
        if((newRecords != null && newRecords.size() > 0) || (updateRecordsMap != null && updateRecordsMap.size() > 0))
        {
            SObject[] recordsToSave = new SObject[]{};

            if((newRecords != null && newRecords.size() > 0))
            {
                recordsToSave.addAll(newRecords);
            }
            if(updateRecordsMap != null && updateRecordsMap.size() > 0)
            {
                recordsToSave.addAll(updateRecordsMap.values());
            }
            
            // upsert
            DatabaseUtil.upsertRecords(recordsToSave, false, disableTriggers);
        }
    }

    public virtual void reset()
    {
       
    }
}