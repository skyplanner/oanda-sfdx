/**
 * @File Name          : FxAccountPersonalInfoChangedHandler_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/27/2020, 5:42:40 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   acantero     Initial Version
**/
@isTest
private class FxAccountPersonalInfoChangedHandler_Test {
    
    @isTest
    static void onPersonalInfoChanged_test() {
        Boolean error = false;
        List<ID> fxAccountIdList = MIFIDValidationTestDataFactory.getValidableFxAccountsIdList();
        Test.startTest();
        try {
            FxAccountPersonalInfoChangedHandler.onPersonalInfoChanged(fxAccountIdList);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

}