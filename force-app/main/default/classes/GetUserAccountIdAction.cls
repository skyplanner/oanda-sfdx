/**
 * @File Name          : GetUserAccountIdAction.cls
 * @Description        : Returns the Account Id linked to an email address
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 11:47:51 AM
**/
public without sharing class GetUserAccountIdAction { 

	@InvocableMethod(label='Get User Account Id' callout=false)
	public static List<ID> getUserAccountId(List<String> userEmailList) {  
		try {
			ExceptionTestUtil.execute();
			List<ID> result = new List<ID> { null };

			if (
				(userEmailList == null) ||
				userEmailList.isEmpty()
			) {
				return result;
			}
			// else...        
			Account accountObj = AccountRepo.getSummaryByEmail(userEmailList[0]);
			
			if (accountObj != null) {
				result[0] = accountObj.Id;
			}

			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetUserAccountIdAction.class.getName(),
				ex
			);
		}
	} 

}