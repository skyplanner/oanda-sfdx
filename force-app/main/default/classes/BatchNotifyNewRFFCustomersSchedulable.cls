public with sharing class BatchNotifyNewRFFCustomersSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Schedulable
{
    static final Integer BATCH_SIZE = 200;  

    //schedule every 1 hour
    static final String CRON_SCHEDULE = '0 0 * * * ?';

    Datetime lastOneHour;

    string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    set<string> countriesToNotify = new set<String>{'Iran, Islamic Republic of', 'Korea, Democratic People\'s Republic'};
   
    Id fxAccountLiveRecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_FXACCOUNT_RECORDTYPE_LIVE, 'fxAccount__c');

    @TestVisible
    public static Messaging.SingleEmailMessage[] emailMessages;


    public BatchNotifyNewRFFCustomersSchedulable() 
    {
        lastOneHour = Datetime.now().addMinutes(-60);

        query = 'select Id, Citizenship_Nationality__c, Ready_for_Funding_DateTime__c, Name, Email_Formula__c, fxTrade_User_Id__c,RFF_SF_Datetime__c '+
                'from fxAccount__c '+
                'where RecordTypeId = :fxAccountLiveRecordTypeId AND '+
                       'Division_Name__c = \'OANDA Europe Markets\' AND ' + 
                       'Citizenship_Nationality__c IN :countriesToNotify AND ' +
                       'RFF_SF_Datetime__c >= :lastOneHour';             
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'select Id, Citizenship_Nationality__c, Ready_for_Funding_DateTime__c, Name, Email_Formula__c, fxTrade_User_Id__c,RFF_SF_Datetime__c '+
        'from fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {       
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope)
    {
        emailMessages = new Messaging.SingleEmailMessage[]{}; 

        for(fxAccount__c fxa : scope)
        {
            emailMessages.add(generateEmail(fxa));
        }        
        if(emailMessages.size() > 0)
        {
            Messaging.sendEmail(emailMessages);
        }
    }

    private Messaging.SingleEmailMessage generateEmail(fxAccount__c fxa)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSenderDisplayName('Salesforce');
        mail.setSaveAsActivity(false);       
        mail.setSubject('OEM - NEW RFF ACCOUNT ALERT- ' + getCountryName(fxa) + ' national');
        mail.setHtmlBody(getEmailBody(fxa));   
        mail.setToAddresses(new string[]{'eu-newaccounts@oanda.com','afc@oanda.com'});  
        return mail;
    }

    private string getEmailBody(fxAccount__c fxa)  
    {
        string emailBody = '<br/> New RFF account with nationality ' + getCountryName(fxa);
        emailBody += '<br/><br/>Ready For Funding Date and Time: ' +  (fxa.Ready_for_Funding_DateTime__c !=null ? String.valueOf(fxa.Ready_for_Funding_DateTime__c) : '');
        emailBody += '<br/>username: ' + fxa.Name;
        emailBody += '<br/>email address: ' + fxa.Email_Formula__c;
        emailBody += '<br/>user id: ' + integer.valueOf(fxa.fxTrade_User_Id__c);

        emailBody += '<br/>Salesforce url: <html><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+fxa.Id+'"> '+ (URL.getSalesforceBaseUrl().toExternalForm()+'/'+fxa.Id) +'</a></html>';
        return emailBody;
    }

    private string getCountryName(fxAccount__c fxa)
    {
        return (fxa.Citizenship_Nationality__c == 'Korea, Democratic People\'s Republic') ? 
                            'North Korea' : 
                                fxa.Citizenship_Nationality__c; 
    }

    public void finish(Database.BatchableContext bc)
    {  
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

     //schedule
     public void execute(SchedulableContext context) 
     {
         Database.executeBatch(new BatchNotifyNewRFFCustomersSchedulable(), BATCH_SIZE);
     }

     //schedule the batch job
     public static void schedule() 
     {  
        System.schedule('BatchNotifyNewRFFCustomers', CRON_SCHEDULE, new BatchNotifyNewRFFCustomersSchedulable());
     }
     
     public static void execute() 
     {
         Database.executeBatch(new BatchNotifyNewRFFCustomersSchedulable(), BATCH_SIZE);
     }
}