/**
* Created by Neuraflash LLC on 13/07/18.
* Implementation : Chatbot
* Summary : 
* Details :
*
*/
@isTest
public class NF_TranscriptUpdateClassTest {
	@testSetup
    static void testSetup(){
        contact testcon = NF_TestDataUtill.createContact();
        insert testcon; 
    }
    static testMethod void testOwnerUpdate(){
        Account acc = [select id from Account order by createddate desc limit 1];
        Contact con = [select id from contact order by createddate desc limit 1];
        nfchat__Chat_Log__c testLog = NF_TestDataUtill.createChatLog();
        insert testLog;
        nfchat__Chat_Log_Detail__c testChatlogDetail = NF_TestDataUtill.createChatLogDetail();
        testChatlogDetail.nfchat__Chat_Log__c = testLog.id;
        testChatlogDetail.nfchat__Is_Event_Request__c = false;
        testChatlogDetail.nfchat__Input_Mode__c = 'Text';
        testChatlogDetail.nfchat__Api_Request__c = 'User Request';
        insert testChatlogDetail;
        Case eachCase = NF_TestDataUtill.getCase();
        eachCase.nfchat__Chat_Log__c = testLog.id;
        eachCase.AccountId = acc.id;
        eachCase.ContactId = con.id;
        insert eachCase;
        
        
        Test.startTest();
        LiveChatVisitor visitor = NF_TestDataUtill.getVisitor();
        insert visitor;
        LiveChatTranscript testChat = NF_TestDataUtill.getTranscript();
        testChat.LiveChatVisitorId = visitor.id;
        insert testChat;
        Test.stopTest();
        
        LiveChatTranscript testChat2 = [select id, Caseid from LiveChatTranscript where ChatKey = 'askh123456789as'];
        system.assert(testChat2.CaseId != NULL);
    }
    static testMethod void testOwnerUpdate_withCase(){
        Account acc = [select id from Account order by createddate desc limit 1];
        Contact con = [select id from contact order by createddate desc limit 1];
        nfchat__Chat_Log__c testLog = NF_TestDataUtill.createChatLog();
        insert testLog;
        nfchat__Chat_Log_Detail__c testChatlogDetail = NF_TestDataUtill.createChatLogDetail();
        testChatlogDetail.nfchat__Chat_Log__c = testLog.id;
        insert testChatlogDetail;
        Case eachCase = NF_TestDataUtill.getCase();
        eachCase.nfchat__Chat_Log__c = testLog.id;
        eachCase.AccountId = acc.id;
        eachCase.ContactId = con.id;
        insert eachCase;
        
        
        Test.startTest();
        LiveChatVisitor visitor = NF_TestDataUtill.getVisitor();
        insert visitor;
        LiveChatTranscript testChat = NF_TestDataUtill.getTranscript();
        testChat.LiveChatVisitorId = visitor.id;
        testChat.CaseId = eachCase.id;
        insert testChat;
        Test.stopTest();
        
        LiveChatTranscript testChat2 = [select id, Caseid from LiveChatTranscript where ChatKey = 'askh123456789as'];
        system.assert(testChat2.CaseId != NULL);
    }
}