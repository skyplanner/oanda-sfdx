/**
 * Created by akajda on 16/01/2024.
 */
@IsTest
public with sharing class BatchMassSendEmailsTest {
    @IsTest
    static void sendEmailCreateCaseTest() {

        Account a = new Account(LastName = 'test account', PersonEmail='test@test.com');
        insert a;

        Test.startTest();
        BatchMassSendEmails b = new BatchMassSendEmails(new List<Id>{a.Id},'Failed_CKA_Email','AFC Team Cases','Test','Suitability');
        Database.executeBatch(b, 10);
        Test.stopTest();

        Case newCase = [SELECT Id, Type__c FROM Case LIMIT 1];

        System.assertNotEquals(newCase, null);
        System.assertEquals('Suitability', newCase.Type__c);
    }
}