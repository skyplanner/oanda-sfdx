/**
 *  All complyAdvantage searches should be monitored off when fxAccount is closed
*/

public with sharing class BatchCAMonitorOffClosedAccSchedulable implements Database.Batchable<sObject>, Schedulable,Database.AllowsCallouts, Database.RaisesPlatformEvents, BatchReflection
{
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static String CRON_SCHEDULE = '0 0 6 * * ?';

    public BatchCAMonitorOffClosedAccSchedulable() 
    {
        set<string> fields = ComplyAdvantageUtil.COMPLY_ADVANTAGE_SEARCH_FEILDS;

        query = 'select ' + SoqlUtil.getSoqlFieldList(fields)+ ' from Comply_Advantage_Search__c';
        query += ' where fxAccount__r.Is_Closed__c = true AND Is_Monitored__c = true';
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        set<string> fields = ComplyAdvantageUtil.COMPLY_ADVANTAGE_SEARCH_FEILDS;
        query = 'select ' + SoqlUtil.getSoqlFieldList(fields)+ ' from Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public static void executeBatch() 
    {
        Database.executeBatch(new BatchCAMonitorOffClosedAccSchedulable(), 100);
    }
    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchCAMonitorOffClosedAccSchedulable(), 100);
    }
    public static void schedule(string schedule) 
    {
        if(string.isNotBlank(schedule))
        {
            CRON_SCHEDULE = schedule;
        }
        System.schedule('BatchCAMonitorOffClosedAccSchedulable', CRON_SCHEDULE, new BatchCAMonitorOffClosedAccSchedulable());
    }
   
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) 
    {
        Comply_Advantage_Search__c[] updateStatusSearches = new Comply_Advantage_Search__c[]{};
        for(Comply_Advantage_Search__c search : scope)
        {
           Boolean isSucessful = monitorOffCA(search);
           if(isSucessful)
           {
                search.Is_Monitored__c = false;
                updateStatusSearches.add(search);
           }
        }
        if(!updateStatusSearches.isEmpty())
        {
            Database.update(updateStatusSearches, false);
        }
    } 
    public boolean monitorOffCA(Comply_Advantage_Search__c search)
    {
        try
        {
            string apiKey = ComplyAdvantageUtil.getApiKey(search);

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ search.search_reference__c +'/monitors?api_key='+ apiKey;
            request.setEndpoint(serviceURL);
            request.setHeader('X-HTTP-Method-Override','PATCH');
            request.setMethod('POST');
            
            string requestBody = '{ "is_monitored" : false }';
            request.setBody(requestBody); 
            
            HttpResponse response = http.send(request);

            if(response.getStatusCode() == 200)
            {
                return true;
            }
            return false;
        }
        catch(Exception ex)
        {
            return false;
        }
    }  
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}