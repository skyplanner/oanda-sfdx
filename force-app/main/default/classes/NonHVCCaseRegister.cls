/**
 * @File Name          : NonHVCCaseRegister.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/9/2022, 5:04:50 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/1/2021, 3:11:14 PM   acantero     Initial Version
**/
public inherited sharing class NonHVCCaseRegister {

    final Map<String,Integer> parkedRecOwnerMap = new Map<String,Integer>();
    final Map<String,Integer> releasedRecOwnerMap = new Map<String,Integer>();
    final Set<String> ownerIdSet = new Set<String>();
    final Set<ID> parkedNonHvcCaseIdSet = new Set<ID>();
    final Set<ID> releasedOwnerIdSet = new Set<ID>();
    //...
    final List<Case> parkedCaseList = new List<Case>();

    public void onCaseParked(Non_HVC_Case__c nonHvcCase) {
        incOwnerCounter(
            parkedRecOwnerMap,
            nonHvcCase.OwnerId
        );
        ownerIdSet.add(nonHvcCase.OwnerId);
        parkedNonHvcCaseIdSet.add(nonHvcCase.Id);
        parkedCaseList.add(
            new Case(
                Id = nonHvcCase.Case__c,
                OwnerId = nonHvcCase.OwnerId,
                Routed_And_Assigned__c = true,
                Agent_Capacity_Status__c = 
                    OmnichanelConst.CASE_CAPACITY_STATUS_PARKED
            )
        );
    }

    public void onCaseReleased(Non_HVC_Case__c nonHvcCase) {
        incOwnerCounter(
            releasedRecOwnerMap,
            nonHvcCase.OwnerId
        );
        ownerIdSet.add(nonHvcCase.OwnerId);
        releasedOwnerIdSet.add(nonHvcCase.OwnerId);
    }

    public void process() {
        udpateCases();
        updateAgentExtraInfo();
        sendParkedNotifications();
        sendReleasedEvents();
    }

    void udpateCases() {
        if (!parkedCaseList.isEmpty()) {
            update parkedCaseList;
        }
    }

    void updateAgentExtraInfo() {
        if (ownerIdSet.isEmpty()) {
            return;
        }
        //else...
        List<Agent_Extra_Info__c> agentExtraInfoList = 
            AgentExtraInfoHelper.getByAgentsForUpdate(ownerIdSet);
        Map<String,Agent_Extra_Info__c> agentExtraInfoMap = 
            new Map<String,Agent_Extra_Info__c>();
        for (Agent_Extra_Info__c agentExtraInfo : agentExtraInfoList) {
            agentExtraInfoMap.put(
                agentExtraInfo.Agent_ID__c,
                agentExtraInfo
            );
        }
        List<Agent_Extra_Info__c> newAgentExtraInfoList = 
            new List<Agent_Extra_Info__c>();
        for (String ownerId : ownerIdSet) {
            Agent_Extra_Info__c agentExtraInfo = 
                agentExtraInfoMap.get(ownerId);
            if (agentExtraInfo == null) {
                agentExtraInfo = new Agent_Extra_Info__c(
                    Agent_ID__c = ownerId,
                    Non_HVC_Cases__c = 0
                );
                newAgentExtraInfoList.add(agentExtraInfo);
            }
            updateNonHVCCasesCount(agentExtraInfo);
        }
        if (!agentExtraInfoList.isEmpty()) {
            update agentExtraInfoList;
        }
        if (!newAgentExtraInfoList.isEmpty()) {
            insert newAgentExtraInfoList;
        }
    }

    @testVisible
    void updateNonHVCCasesCount(Agent_Extra_Info__c agentExtraInfo){
        Integer parkedCount = 
                parkedRecOwnerMap.get(agentExtraInfo.Agent_ID__c);
        if (parkedCount != null) {
            agentExtraInfo.Non_HVC_Cases__c += parkedCount;
        }
        Integer releasedCount = 
            releasedRecOwnerMap.get(agentExtraInfo.Agent_ID__c);
        if (releasedCount != null) {
            agentExtraInfo.Non_HVC_Cases__c -= releasedCount;
        }
        if (agentExtraInfo.Non_HVC_Cases__c < 0) {
            agentExtraInfo.Non_HVC_Cases__c = 0;
        }
    }

    @testVisible
    void incOwnerCounter(
        Map<String,Integer> ownerInfoMap, 
        String ownerId
    ) {
        Integer counter = ownerInfoMap.get(ownerId);
        if (counter == null) {
            counter = 0;
        }
        counter++;
        ownerInfoMap.put(ownerId, counter);
    }

    @testVisible
    void sendParkedNotifications() {
        if (parkedNonHvcCaseIdSet.isEmpty()) {
            return;
        }
        //else...
        List<Non_HVC_Case__c> nonHvcCaseList = 
            NonHVCCaseHelper.getCaseInfoById(parkedNonHvcCaseIdSet);
        List<Case> caseList = new List<Case>();
        for(Non_HVC_Case__c nonHvcCase : nonHvcCaseList) {
            caseList.add(nonHvcCase.Case__r);
        }
        new NonHVCCaseNotificationManager()
            .sendParkedNotifications(caseList);
    }

    void sendReleasedEvents() {
        if (releasedOwnerIdSet.isEmpty()) {
            return;
        }
        //else...
        List<Non_HVC_Case_Released__e> eventList = 
            new List<Non_HVC_Case_Released__e>();
        for(ID agentId : releasedOwnerIdSet) {
            eventList.add(
                new Non_HVC_Case_Released__e(
                    Agent_ID__c = agentId
                )
            );
        }
        Eventbus.publish(eventList);
    }

}