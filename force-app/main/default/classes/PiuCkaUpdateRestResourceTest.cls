/* Name: PiuCkaUpdateRestResource
 * Description : Test Class to handle Personal Information Update from user-api
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 May 22
 */

@isTest
public class PiuCkaUpdateRestResourceTest {
    
    public static final string REQUEST_BODY = '{\"questions\":[{\"question\":\"Which of the following statements about Contracts for Difference (CFDs) is correct?\",\"answer\":\"A CFD reflects the market movements of an asset, but the actual underlying asset is never owned\",\"choice\":1},{\"question\":\"When margin closeout occurs, which of the following statements is true?\",\"answer\":\"Your position closes\",\"choice\":3},{\"question\":\"What is \u201Cthe spread\u201D?\",\"answer\":\"The difference between the Bid (Sell) and Ask (Buy)\",\"choice\":1},{\"question\":\"What is \u201Cgapping\u201D?\",\"answer\":\"When the market \\\"jumps\\\" from one price to another price, leaving a \\\"gap\\\" on the charts and no opportunity to trade between the two prices\",\"choice\":1},{\"question\":\"Which of the following processes are important to minimising risk when trading forex?\",\"answer\":\"All of the above\",\"choice\":4}],\"knowledge_result\":\"Pass\",\"experience_result\":\"Pass\",\"overall_result\":\"Pass\",\"timestamp\":1664458173612,\"author\":\"fxdb\",\"agent\":\"r10n-api\"}';
   public static final string REQUEST_URI_TEMPLATE = '/api/v1/user/{0}/piu_cka_update';
   public static final string REST_RESOURCE_PATH = '/api/v1/user/*/piu_cka_update';
   private static final string postMethod = 'POST';

   public static final Integer VALID_USER_ID = 12345;
   public static final Integer INVALID_USER_ID = 00000;

   @TestSetup
   static void initData(){

        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = VALID_USER_ID;
        fxAccount.fxTrade_Global_ID__c = VALID_USER_ID + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        insert fxAccount;
   }

    @isTest
    public static void testDoPostWithProperFxAcc() {
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/user/' + VALID_USER_ID + '/piu_cka_update';
        request.resourcePath = '/api/v1/user/*/piu_cka_update';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(REQUEST_BODY);

        RestContext.request = request;
        RestContext.response = response;

        PiuCkaUpdateRestResource.doPost();
        
        Assert.areEqual(200, response.statusCode, 'Response Code should be 200');
    }

    @isTest
    public static void testDoPostWithInvalidFxAcc() {
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/user/' + INVALID_USER_ID + '/piu_cka_update';
        request.resourcePath = '/api/v1/user/*/piu_cka_update';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(REQUEST_BODY);

        RestContext.request = request;
        RestContext.response = response;

        PiuCkaUpdateRestResource.doPost();
        Assert.areEqual(404, response.statusCode, 'Response Code should be 404');
    }

    @isTest
    public static void testDoPostWithInvalidParamFxAcc() {
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/user/' + null + '/piu_cka_update';
        request.resourcePath = '/api/v1/user/*/piu_cka_update';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(REQUEST_BODY);

        RestContext.request = request;
        RestContext.response = response;

        PiuCkaUpdateRestResource.doPost();
        
        Assert.areEqual(500, response.statusCode, 'Response Code should be 500');
    }

    @isTest
    public static void testDoPost() {
        
        fxAccount__c fxa = [SELECT Id, fxTrade_User_ID__c FROM fxAccount__c LIMIT 1];

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = String.format(REQUEST_URI_TEMPLATE, new List<Integer>{VALID_USER_ID});
        req.resourcePath = REST_RESOURCE_PATH; 
        req.httpMethod = postMethod;
    
        req.requestBody = Blob.valueOf(REQUEST_BODY);
    
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        PiuCkaUpdateRestResource.doPost();
        Test.getEventBus().deliver();
        Test.stopTest();

        List<Personal_Information_Update__c> createdPiu = [
            SELECT 
                Id, 
                Knowledge_Assessment_Result__c, 
                Overall_Result__c, 
                Trading_Experience_Result__c,
                (SELECT 
                    Id, 
                    No__c, 
                    Question__c, 
                    Answer__c 
                FROM Questionnaire__r)
            FROM Personal_Information_Update__c
        ];
        
        Assert.areEqual(200, res.statusCode, 'Record should be processed');
        Assert.areEqual(1, createdPiu.size(), 'There should be PIU created');
        Assert.areEqual(5, createdPiu[0].Questionnaire__r.size(), 'There should be created 5 questionnaires');
        Assert.areEqual('Pass', createdPiu[0].Knowledge_Assessment_Result__c, 'Result should be "Pass"');
        Assert.areEqual('Pass', createdPiu[0].Overall_Result__c, 'Result should be "Pass"');
        Assert.areEqual('Pass', createdPiu[0].Trading_Experience_Result__c, 'Result should be "Pass"');
    }
}