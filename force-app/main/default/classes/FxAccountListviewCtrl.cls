/**
 * Implements the controllers methods for the fxaccountListview component
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   18/03/2021
 */
public with sharing class FxAccountListviewCtrl {

    /**
     * Retrieve all the fxAccounts related to the Child accounts related
     * to the given account Id
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   18/03/2021
     * @return: List<fxAccount>, the accounts retrieved
     * @param:  accountId, te he Id of hte account to the fxAccounts from
     */
    @AuraEnabled(cacheable=true)
    public static List<AccountWrapper> getFxAccounts(Id accountId){

        List<AccountWrapper> res = new List<AccountWrapper>();
        Set<Id> accIdSetTmp = new Set<Id>();
        AccountWrapper accWrapperTmp;
        if(String.isBlank(accountId)){
            throw new AuraHandledException('The accountId cannot be empty');
        }
        for(fxAccount__c fxa : [
            SELECT Id,
                Name,
                Account_Locked__c,
                Ready_for_Funding_Date__c,
                Is_Closed__c,
                Email__c,
                Division_Name__c,
                Ready_for_Funding_DateTime__c,
                Account__c,
                Account__r.Name,
                Account__r.PersonEmail,
                Account__r.Account_Closed__pc,
                Account__r.Primary_Division_Name__c,
                Account__r.fxAccount__r.Name,
                Funnel_Stage__c
            FROM 
                fxAccount__c
            WHERE
                Account__r.Parent_Account__c =: accountId
            Order By Account__c])
        {
            if(!accIdSetTmp.contains(fxa.Account__c)){
                accIdSetTmp.add(fxa.Account__c);
                accWrapperTmp = new AccountWrapper();
                accWrapperTmp.key = fxa.Account__c;
                accWrapperTmp.accId = fxa.Account__c;
                accWrapperTmp.accountName = fxa.Account__r.Name;
                accWrapperTmp.primaryFxAccount = fxa.Account__r.fxAccount__r.Name;
                accWrapperTmp.accountClosed = fxa.Account__r.Account_Closed__pc;
                accWrapperTmp.primaryDivisionName = fxa.Account__r.Primary_Division_Name__c;
                accWrapperTmp.children = new List<FxAccountWrapper>();
                res.add(accWrapperTmp);
            }
            FxAccountWrapper fxaWrapper = new FxAccountWrapper();
            fxaWrapper.key = fxa.Id;
            fxaWrapper.fxAccountName = fxa.Name;
            fxaWrapper.emailAddress = fxa.Email__c;
            fxaWrapper.accountLocked = fxa.Account_Locked__c;
            fxaWrapper.readyForFundingDate = fxa.Ready_for_Funding_Date__c;
            fxaWrapper.funnelStage = fxa.Funnel_Stage__c;
            fxaWrapper.divisionName = fxa.Division_Name__c;            
            accWrapperTmp.children.add(fxaWrapper);
        }
        return res;
    }

    public class AccountWrapper{
        @AuraEnabled
        public Id key {get; Set;}
        @AuraEnabled
        public Id accId {get; Set;}
        @AuraEnabled
        public String accountName {get; Set;}
        @AuraEnabled
        public String primaryFxAccount {get; Set;}
        @AuraEnabled
        public Boolean accountClosed {get; Set;}
        @AuraEnabled
        public String primaryDivisionName {get; Set;}
        @AuraEnabled
        public List<FxAccountWrapper> children {get; Set;}
    }

    public class FxAccountWrapper{
        @AuraEnabled
        public Id key {get; Set;}
        @AuraEnabled
        public String fxAccountName {get; Set;}
        @AuraEnabled
        public String emailAddress {get; Set;}
        @AuraEnabled        
        public Boolean accountLocked {get; Set;}
        @AuraEnabled
        public Datetime readyForFundingDate {get; Set;}
        @AuraEnabled
        public String funnelStage {get; Set;}
        @AuraEnabled
        public String divisionName {get; Set;}
    }
}