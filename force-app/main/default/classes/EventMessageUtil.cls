public without sharing class EventMessageUtil {

    public static void deactivateExistingMessages (List<Event_Message__c> newEventMessages) {
        Set<String> eventMessageTypes = new Set<String>();

        // get all distinct types of active messages
        for(Event_Message__c em : newEventMessages) {
            if (em.Active__c) {
                eventMessageTypes.add(em.Type__c);
            }
        }

        if(eventMessageTypes.isEmpty())
            return;

        // retrieve and deactivate all existing active messages
        List<Event_Message__c> eventMessagesToDeactivate = [Select Id, Type__c, Active__c FROM Event_Message__c WHERE Type__c In :eventMessageTypes AND Active__c = True AND Id not In :newEventMessages];
        for(Event_Message__c em : eventMessagesToDeactivate) {
            em.Active__c = False;
        }

        update eventMessagesToDeactivate;
    }

    public static Event_Message__c getActiveEventMessage(String eventMsgType) {
        List<Event_Message__c> eventMessages = [
            SELECT 
                Id, 
                Message_English__c, 
                Message_French__c, 
                Message_German__c, 
                Message_Italian__c, 
                Message_Portuguese__c, 
                Message_Simplified_Chinese__c, 
                Message_Spanish__c, 
                Message_Traditional_Chinese__c
            FROM 
                Event_Message__c 
            WHERE 
                Active__c = TRUE 
            AND 
                Type__c = :eventMsgType
        ];

        // return null if there is no active event message
		if(eventMessages.size() < 1) {
			return null;
		}
		Event_Message__c eventMessage = eventMessages[0];

        return eventMessage;
    }

    public static String getActiveEventMessageLang(String lang, String eventMsgType) {
		
		Event_Message__c eventMessage = getActiveEventMessage(eventMsgType);

        if(eventMessage == null) {
            return null;
        }

		String eventMessageLang;
		if(lang == 'zh_CN') {
			eventMessageLang = eventMessage.Message_Simplified_Chinese__c;
		}
		else if(lang == 'zh_TW') {
			eventMessageLang = eventMessage.Message_Traditional_Chinese__c;
		}
		else if(lang == 'de') {
			eventMessageLang = eventMessage.Message_German__c;
		}
		else if(lang == 'es') {
			eventMessageLang = eventMessage.Message_Spanish__c;
		}
		else if(lang == 'fr') {
			eventMessageLang = eventMessage.Message_French__c;
		}
		else if(lang == 'it') {
			eventMessageLang = eventMessage.Message_Italian__c;
		}
		else if(lang == 'pt') {
			eventMessageLang = eventMessage.Message_Portuguese__c;
		}
        else {
            eventMessageLang = eventMessage.Message_English__c;
        }

        // return Message_English__c as default if message is null
        if(eventMessageLang == null) {
            eventMessageLang = eventMessage.Message_English__c;
        }

		return eventMessageLang;
	}
}