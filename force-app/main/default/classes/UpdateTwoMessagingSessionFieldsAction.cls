/**
 * @File Name          : UpdateTwoMessagingSessionFieldsAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/16/2023, 3:27:01 AM
**/
public without sharing class UpdateTwoMessagingSessionFieldsAction { 

	@InvocableMethod(label='Set Two Messaging Session Fields' callout=false)
	public static void setTwoMessagingSessionFields(List<UpdateInfo> updateInfoList) {    
		new MessagingSessionUpdateManager().processUpdate(updateInfoList);
	}

	// *******************************************************************

	public class UpdateInfo implements FieldValueSource {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'fieldName1' required = true)
		public String fieldName1;

		@InvocableVariable(label = 'fieldValue1' required = false)
		public String fieldValue1;

		@InvocableVariable(label = 'fieldName2' required = true)
		public String fieldName2;

		@InvocableVariable(label = 'fieldValue2' required = false)
		public String fieldValue2;

		public String getRecordId() {
			return messagingSessionId;
		}

		public Map<String, Object> getFieldValueByFieldNames() {
			Map<String, Object> result = new Map<String, Object>();
			result.put(fieldName1, fieldValue1);
			result.put(fieldName2, fieldValue2);
			return result;
		}

	}

}