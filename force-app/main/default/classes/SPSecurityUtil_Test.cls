/**
 * @File Name          : SPSecurityUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2022, 7:14:01 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 6:53:35 PM   acantero     Initial Version
**/
@isTest(isParallel = false)
private without sharing class SPSecurityUtil_Test {

    @isTest
    static void test() {
        Set<String> roleDevNameSet = new Set<String>();
        Map<String,String> result;
        Boolean result2 = null;
        Test.startTest();
        User testCEOUser = ServiceResourceTestDataFactory.createCEOUser('c0001');
        System.runAs(testCEOUser) {
            String userId = UserInfo.getUserId();
            Set<String> userIdSet = new Set<String> {userId};
            result = SPSecurityUtil.getUserRoleMap(
                userIdSet,
                roleDevNameSet
            );
            result2 = SPSecurityUtil.isCurrentUserAValidStandardUser();
        }
        Test.stopTest();
        System.assertEquals(1, result.size(), 'Invalid result');
        System.assertEquals(1, roleDevNameSet.size(), 'Invalid result');
        System.assertNotEquals(null, result2, 'Invalid result');
    }
    
}