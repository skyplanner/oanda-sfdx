/**
*/
@isTest
private class LeadBatchUtilTest {
    
    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
    final static User MARKETO_USER = UserUtil.getUserByName(UserUtil.NAME_MARKETO_USER);
    
    static User FXS_USER = UserUtil.getRandomTestUser();
    static User FXS_USER2 = UserUtil.getRandomTestUser();
    
    static testMethod void testGetLiveLeads() {
        CustomSettings.setEnableLeadMerging(false);
        Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
        Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555');
        insert new Lead[]{l1, l2};
            
            CustomSettings.setEnableLeadMerging(true);
        Test.startTest();
        BatchMergeLeads.executeBatch();
        Test.stopTest();
        
        Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id];
        System.assertEquals(leads.size(), 1);
        System.assertEquals(leads[0].Id, l1.Id);
        System.assertEquals(leads[0].LastName, 'test lead 1');
    }
    
    static testMethod void testGetLeadsWithMostTasks() {
        CustomSettings.setEnableLeadConversion(false);
        CustomSettings.setEnableLeadMerging(false);
        Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
        Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
        insert new Lead[]{l1, l2};
            
            User u = UserUtil.getTestUser();
        insert u;
        
        Task t1 = new Task(Subject='test task 2', WhoId=l1.Id, OwnerId=u.Id);
        insert t1;
        
        CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
        Test.startTest();
        BatchMergeLeads.executeBatch();
        Test.stopTest();
        
        Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id];
        System.assertEquals(leads.size(), 1);
        System.assertEquals(leads[0].Id, l1.Id);
        System.assertEquals(leads[0].LastName, 'test lead 1');
    }
    
    static testMethod void testGetLeadsWithMostTasksMultipleTasks() {
        CustomSettings.setEnableLeadConversion(false);
        CustomSettings.setEnableLeadMerging(false);
        Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
        Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
        insert new Lead[]{l1, l2};
            
            User u = UserUtil.getTestUser();
        insert u;
        
        Task t1 = new Task(Subject='test task 1', WhoId=l1.Id, OwnerId=u.Id);
        Task t2 = new Task(Subject='test task 2', WhoId=l2.Id, OwnerId=u.Id);
        insert new Task[]{t1, t2};
            
            CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
        Test.startTest();
        BatchMergeLeads.executeBatch();
        Test.stopTest();
        
        Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id];
        System.assertEquals(leads.size(), 1);
    }
    
    static testMethod void testFindLatestStageLeads2() {
        CustomSettings.setEnableLeadConversion(false);
        CustomSettings.setEnableLeadMerging(false);
        Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com', Funnel_Stage__c=FunnelStatus.DOCUMENTS_UPLOADED);
        Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com', Funnel_Stage__c=FunnelStatus.FUNDED);
        insert new Lead[]{l1, l2};
            
            CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        Test.startTest();
        BatchMergeLeads.executeBatch();
        Test.stopTest();
        
        Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName, Funnel_Stage__c FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id];
        System.assertEquals(leads.size(), 1);
        System.assertEquals(leads[0].Id, l2.Id);
        System.assertEquals(FunnelStatus.FUNDED, leads[0].Funnel_Stage__c);
    }
    
    static testMethod void testAssignLeadsWithActivities() {
        CustomSettings.setEnableLeadConversion(false);
        Lead l1 = new Lead(LastName='test lead 1', Email='joe1@example.org', RecordTypeId=RecordTypeUtil.getLeadRetailId());
        Lead l2 = new Lead(LastName='test lead 2', Email='joe2@example.org', RecordTypeId=RecordTypeUtil.getLeadRetailId());
        insert new Lead[]{l1, l2};
            
            User u = UserUtil.getTestUser();
        insert u;
        
        Task t1 = new Task(Subject='test task 2', WhoId=l1.Id, OwnerId=u.Id);
        insert t1;
        
        CustomSettings.setEnableLeadConversion(true);
        Test.startTest();
        BatchAssignLeads.executeBatch();
        Test.stopTest();
        
        Lead reassignedLead1 = [SELECT Id, Name, OwnerId FROM Lead WHERE Id=:l1.Id];
        Lead reassignedLead2 = [SELECT Id, Name, OwnerId FROM Lead WHERE Id=:l2.Id];
        System.assertEquals(UserUtil.getSystemUserId(), reassignedLead2.OwnerId);
        System.assertEquals(u.Id, reassignedLead1.OwnerId);
    }
    
    static testMethod void testMergeMultipleLeads() {
        
        System.runAs(UserUtil.getSystemUserForTest()) {
            CustomSettings.setEnableLeadConversion(false);
            CustomSettings.setEnableLeadMerging(false);
            Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com');
            Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555');
            Lead l3 = new Lead(LastName='test lead 3', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555');
            Lead l4 = new Lead(LastName='test lead 4', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555');
            insert new Lead[]{l1, l2, l3, l4};
                
                User u = UserUtil.getTestUser();
            insert u;
            
            Task t1 = new Task(Subject='test task 2', WhoId=l1.Id, OwnerId=u.Id);
            insert t1;
            
            CustomSettings.setEnableLeadMerging(true);
            CustomSettings.setEnableLeadConversion(true);
            Test.startTest();
            BatchMergeLeads.executeBatch();
            Test.stopTest();
            
            Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id OR Id=:l3.Id OR Id=:l4.Id];
            System.assertEquals(leads.size(), 1);
            System.assertEquals(leads[0].Id, l1.Id);
            System.assertEquals(leads[0].LastName, 'test lead 1');
        }
    }
    
    static testMethod void testMergeMultipleLeadsInInsert() {
        
        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        Lead l1 = new Lead(LastName='test lead on insert 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmergeoninsert@oanda.com');
        Lead l2 = new Lead(LastName='test lead on insert 2', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmergeoninsert@oanda.com', Phone='416-555-5555');
        insert new Lead[] { l1, l2 };
                   //checkRecursive.SetOfIDs = new Set<Id>();

        Test.startTest();
        
        checkRecursive.SetOfIDs = new Set<Id>();
        mergeConvert();
        Test.stopTest();
        
        Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Email = 'testleadmergeoninsert@oanda.com'];
        
        System.assertEquals(1, leads.size());
        System.assertEquals(RecordTypeUtil.getLeadRetailId(), leads[0].RecordTypeId);
        System.assertEquals('test lead on insert 1', leads[0].LastName);
        System.assertEquals('416-555-5555', leads[0].Phone);
    }
    
    static testMethod void testCopyEmptyFields() {
        
        System.runAs(UserUtil.getSystemUserForTest()) {
            CustomSettings.setEnableLeadConversion(false);
            CustomSettings.setEnableLeadMerging(false);
            Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='testleadmerge@oanda.com', Phone='416-555-6666');
            Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555', Description='test description');
            Lead l3 = new Lead(LastName='test lead 3', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555', Company='test company');
            Lead l4 = new Lead(LastName='test lead 4', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='testleadmerge@oanda.com', Phone='416-555-5555', IsUnreadByOwner=true);
            insert new Lead[]{l1, l2, l3, l4};
                
                User u = UserUtil.getTestUser();
            insert u;
            
            Task t1 = new Task(Subject='test task 2', WhoId=l1.Id, OwnerId=u.Id);
            insert t1;
            
            CustomSettings.setEnableLeadMerging(true);
            CustomSettings.setEnableLeadConversion(true);
            Test.startTest();
            BatchMergeLeads.executeBatch();
            Test.stopTest();
            
            Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName, Description, Company, IsUnreadByOwner FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id OR Id=:l3.Id OR Id=:l4.Id];
            System.assertEquals(leads.size(), 1);
            System.assertEquals(leads[0].Id, l1.Id);
            System.assertEquals(leads[0].LastName, 'test lead 1');
            System.assertEquals(leads[0].Description, 'test description');
            System.assertEquals(leads[0].Company, 'test company');
            System.assertEquals(leads[0].IsUnreadByOwner, true);
        }
    }
    
    static testMethod void testFindLatestStageLeads() {
        System.runAs(UserUtil.getSystemUserForTest()) {
            CustomSettings.setEnableLeadMerging(false);
            Lead l1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Funnel_Stage__c=FunnelStatus.FUNDED, Email='testleadmerge@oanda.com');
            Lead l2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Funnel_Stage__c=FunnelStatus.MORE_INFO_REQUIRED, Email='testleadmerge@oanda.com', Phone='416-555-5555');
            insert new Lead[]{l1, l2};
                
                CustomSettings.setEnableLeadMerging(true);
            Test.startTest();
            BatchMergeLeads.executeBatch();
            Test.stopTest();
            
            Lead[] leads = [SELECT Id, Name, RecordTypeId, Email, Phone, LastName FROM Lead WHERE Id=:l1.Id OR Id=:l2.Id];
            System.assertEquals(leads.size(), 1);
            System.assertEquals(leads[0].Id, l1.Id);
            System.assertEquals(leads[0].LastName, 'test lead 1');
        }
    }
    
    static testMethod void testConvertLeads() {
        
        CustomSettings.setEnableLeadConversion(false);
        
        System.runAs(UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER)) {
            Lead trade1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='trade1@oanda.com');
            Lead trade2 = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='trade2@oanda.com', Phone='416-555-5555');
            Lead trade3 = new Lead(LastName='test lead 3', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email='trade3@oanda.com');
            Lead practice1 = new Lead(LastName='test lead 4', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId(), Email='practice1@oanda.com');
            
            insert new Lead[]{trade1, trade2, trade3, practice1};
                
                fxAccount__c fxTrade1 = new fxAccount__c(Lead__c=trade1.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Funnel_Stage__c=FunnelStatus.TRADED);
            fxAccount__c fxTrade2 = new fxAccount__c(Lead__c=trade2.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Funnel_Stage__c=FunnelStatus.TRADED);
            fxAccount__c fxTrade3 = new fxAccount__c(Lead__c=trade3.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Funnel_Stage__c=FunnelStatus.FUNDED);
            fxAccount__c fxPractice1 = new fxAccount__c(Lead__c=practice1.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE,Funnel_Stage__c=FunnelStatus.DEMO_TRADED);
            
            insert new fxAccount__c[]{fxTrade1, fxTrade2, fxTrade3, fxPractice1};
                
                CustomSettings.setEnableLeadConversion(true);
            
            Test.startTest();
            BatchConvertLeads.executeBatch();
            Test.stopTest();
            
            Lead[] leads = [SELECT Id, IsConverted FROM Lead WHERE Id = :trade1.Id OR Id = :trade2.Id OR Id = :trade3.Id OR Id = :practice1.Id];
            System.assertEquals(leads.size(), 4);
            System.assert(leads[0].IsConverted);
            System.assert(leads[1].IsConverted);
            System.assert(!leads[2].IsConverted);
            System.assert(!leads[3].IsConverted);
        }
    }
    
    static testMethod void testLeadToFxAccount() {
        TestsUtil.forceJobsSync = true;

        Test.startTest();
        
        CustomSettings.setEnableLeadConversion(false);
        CustomSettings.setEnableLeadMerging(false);
        
        // Create test leads with old funnel stage and traded values
        Lead[] leads = new Lead[] {
            new Lead(FirstName='Joe', LastName='Schmoe', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD, Email='l1@oanda.com', OwnerId=FXS_USER.Id),
                new Lead(FirstName='John', LastName='Doe', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Email='l2@oanda.com', Traded__c = true, Username_Practice__c = 'prac1', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Jason', LastName='Bourne', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Email='l3@oanda.com', Traded__c = false, Username_Practice__c = 'prac2', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Jack', LastName='Black', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l4@oanda.com', Funnel_Stage__c = 'ID Verification', Username__c = 'trade1', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Josh', LastName='Tosh', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l5@oanda.com', Funnel_Stage__c = 'Documents Received', Username__c = 'trade2', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Jimmy', LastName='Jones', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l6@oanda.com', Funnel_Stage__c = 'Ready to Fund', Username__c = 'trade3', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Jeremy', LastName='Jiggery', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l7@oanda.com', Funnel_Stage__c = 'Funded', Username__c = 'trade4', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Julio', LastName='Foolio', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l8@oanda.com', Funnel_Stage__c = 'Traded', Username__c = 'trade5', OwnerId=FXS_USER.Id),
                new Lead(FirstName='Jasper', LastName='Casper', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='l9@oanda.com', Funnel_Stage__c = 'Traded', OwnerId=FXS_USER.Id)
                };
                    
                    insert leads;
        Set<Id> leadIds = new Set<Id>();
        for (Lead lead : leads) {
            leadIds.add(lead.Id);
        }
        
        // Test that the last lead doesn't get changed
        fxAccount__c existingFxAccount = new fxAccount__c();
        existingFxAccount.Account_Email__c = leads[8].Email;
        existingFxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        existingFxAccount.Name = 'trade6';
        insert existingFxAccount;
        
        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        Datetime lastModifiedLead = [SELECT Id, LastModifiedDate FROM Lead WHERE Id = :leads[8].Id].LastModifiedDate;
        Datetime lastModifiedFxAccount = [SELECT Id, LastModifiedDate FROM fxAccount__c WHERE Id = :existingFxAccount.Id].LastModifiedDate;
        
        // Run the batch
        BatchLeadToFxAccount bc = new BatchLeadToFxAccount();
        bc.query += ' LIMIT 200';
        Database.executeBatch(bc);
        Test.stopTest();
        
        
        Lead[] migratedLeads = [SELECT Id, OwnerId, FirstName, LastName, RecordTypeId, Email, Funnel_Stage__c, Username__c, Username_Practice__c, LastModifiedDate FROM Lead WHERE Id IN :leadIds];
        
        // Verify that the existing lead didn't get modified
        System.assertEquals(lastModifiedLead, migratedLeads[8].LastModifiedDate);
        
        // Check that basic info didn't change
        for (Integer i = 0; i < leads.size(); i++) {
            Lead orig = leads[i];
            Lead mig = migratedLeads[i];
            
            System.assertEquals(orig.OwnerId, mig.OwnerId);
            System.assertEquals(orig.FirstName, mig.FirstName);
            System.assertEquals(orig.LastName, mig.LastName);
            System.assertEquals(orig.Email, mig.Email);
            System.assertEquals(orig.RecordTypeId, mig.RecordTypeId);
            //			System.assert(mig.Username__c == null);
            //			System.assert(mig.Username_Practice__c == null);
        }
        
        
        // Check that the leads have the proper funnel stage
        System.assertEquals(FunnelStatus.DEMO_TRADED, migratedLeads[1].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.DEMO_REGISTERED, migratedLeads[2].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.AWAITING_CLIENT_INFO, migratedLeads[3].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.DOCUMENTS_UPLOADED, migratedLeads[4].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.READY_FOR_FUNDING, migratedLeads[5].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.FUNDED, migratedLeads[6].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.TRADED, migratedLeads[7].Funnel_Stage__c);
        
        // Get the newly create fxAccounts
        List<fxAccount__c> migratedFxAccounts = [SELECT Id, Name, RecordTypeId, Funnel_Stage__c, Lead__c, LastModifiedDate FROM fxAccount__c WHERE Lead__c IN :leadIds];
        
        System.assertEquals(8, migratedFxAccounts.size());
        
        // Map them so we can link them to the lead
        Map<Id, fxAccount__c> fxAccountByLeadId = new Map<Id, fxAccount__c>();
        for (fxAccount__c fxAccount : migratedFxAccounts) {
            if (fxAccountByLeadId.containsKey(fxAccount.Lead__c)) {
                System.assert(false);
            }
            fxAccountByLeadId.put(fxAccount.Lead__c, fxAccount);
        }
        
        // Make the migrated list of accounts line up with the leads		
        migratedFxAccounts.clear();
        for (Lead lead : leads) {
            migratedFxAccounts.add(fxAccountByLeadId.get(lead.Id));
        }
        
        // Standard lead did not produce fxaccount
        System.assert(migratedFxAccounts[0] == null);
        
        // Check that usernames properly moved over
        System.assertEquals('prac1',  migratedFxAccounts[1].Name);
        System.assertEquals('prac2',  migratedFxAccounts[2].Name);
        System.assertEquals('trade1',  migratedFxAccounts[3].Name);
        System.assertEquals('trade2',  migratedFxAccounts[4].Name);
        System.assertEquals('trade3',  migratedFxAccounts[5].Name);
        System.assertEquals('trade4',  migratedFxAccounts[6].Name);
        System.assertEquals('trade5',  migratedFxAccounts[7].Name);
        
        // Verify that the existing lead didn't get modified
        System.assertEquals(lastModifiedFxAccount, migratedFxAccounts[8].LastModifiedDate);
        
        // Check that funnel state is correct
        System.assertEquals(FunnelStatus.DEMO_TRADED, migratedFxAccounts[1].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.DEMO_REGISTERED, migratedFxAccounts[2].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.AWAITING_CLIENT_INFO, migratedFxAccounts[3].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.DOCUMENTS_UPLOADED, migratedFxAccounts[4].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.READY_FOR_FUNDING, migratedFxAccounts[5].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.FUNDED, migratedFxAccounts[6].Funnel_Stage__c);
        System.assertEquals(FunnelStatus.TRADED, migratedFxAccounts[7].Funnel_Stage__c);
    }
    
    static testMethod void realworldTest() {
        TestsUtil.forceJobsSync = true;

        CustomSettings.setEnableLeadConversion(true);
        
        Lead tradeConvert1Lead = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, LastName='Andrew', Email='andrew@example.org');
        insert tradeConvert1Lead;
        
        fxAccount__c tradeConvert1Account = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Name = 'andrew', Account_Email__c = 'andrew@example.org');
        insert tradeConvert1Account;
        
        Lead duplicateTradeConvert1Lead = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, LastName='Andrew', Email='andrew@example.org');
        insert duplicateTradeConvert1Lead;
        
        fxAccount__c anotherTradeConvert1Account = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Name = 'andrew2', Account_Email__c = 'andrew@example.org');
        insert anotherTradeConvert1Account;
        
        tradeConvert1Account.Trigger_Lead_Assignment_Rules__c = false;
        anotherTradeConvert1Account.Trigger_Lead_Assignment_Rules__c = false;
        update new fxAccount__c[] {tradeConvert1Account, anotherTradeConvert1Account};
            
            checkRecursive.SetOfIDs = new Set<Id>();
        
        // Convert it
        tradeConvert1Lead.OwnerId = FXS_USER.Id;
        update tradeConvert1Lead;
        
        Test.startTest();
        checkRecursive.SetOfIDs = new Set<Id>();
        
        mergeConvert();
        
        Test.stopTest();
        
        tradeConvert1Lead = [SELECT Id, IsConverted, ConvertedOpportunityId, ConvertedAccountId, ConvertedContactId FROM Lead WHERE Id = :tradeConvert1Lead.Id];
        System.assert(tradeConvert1Lead.IsConverted);
        
        anotherTradeConvert1Account = [SELECT Id, Lead__c, Opportunity__c, Account__c, Contact__c FROM fxAccount__c WHERE Id = :anotherTradeConvert1Account.Id];
        
        System.assertEquals(null, anotherTradeConvert1Account.Lead__c);
        System.assertEquals(tradeConvert1Lead.ConvertedOpportunityId, anotherTradeConvert1Account.Opportunity__c);
        System.assertEquals(tradeConvert1Lead.ConvertedAccountId, anotherTradeConvert1Account.Account__c);
        System.assertEquals(tradeConvert1Lead.ConvertedContactId, anotherTradeConvert1Account.Contact__c);
        
    }
    
    
    static testMethod void realworldTest1() {
        
        CustomSettings.setEnableLeadConversion(true);
        
        
        // Create some more non converted leads
        Lead trade1Lead = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, LastName='Albert', Email='albert@example.org');
        Lead trade2Lead = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, LastName='Stephanie', Email='stephanie@example.org');
        Lead practice1Lead = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, LastName='Mudita', Email='mudita@example.org');
        
        
        insert new Lead[] { trade1Lead, trade2Lead, practice1Lead};
            
            
            // Create some fxAccounts
            fxAccount__c trade1Account = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Name = 'albert', Account_Email__c = 'albert@example.org');
        fxAccount__c trade2Account = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Name = 'stephanie', Account_Email__c = 'stephanie@example.org');
        fxAccount__c practice1Account = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Name = 'mudita', Account_Email__c = 'mudita@example.org');
        
        fxAccount__c bogusAccount = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Name = 'phantomuser');
        
        Database.insert(new fxAccount__c[] { trade1Account, trade2Account, practice1Account, bogusAccount }, false);
        
        trade1Account.Trigger_Lead_Assignment_Rules__c = false;
        trade2Account.Trigger_Lead_Assignment_Rules__c = false;
        update new fxAccount__c[] {trade1Account, trade2Account};
            
            Test.startTest();
        
        mergeConvert();
        
        Test.stopTest();
        
        System.assertEquals(null, bogusAccount.Id);
        
        trade1Account = [SELECT Id, Lead__c, Opportunity__c, Account__c, Contact__c FROM fxAccount__c WHERE Id = :trade1Account.Id];
        trade2Account = [SELECT Id, Lead__c, Opportunity__c, Account__c, Contact__c FROM fxAccount__c WHERE Id = :trade2Account.Id];
        practice1Account = [SELECT Id, Lead__c, Opportunity__c, Account__c, Contact__c FROM fxAccount__c WHERE Id = :practice1Account.Id];
        
        
        
        System.assertEquals(null, trade1Account.Lead__c);
        System.assertNotEquals(null, trade1Account.Opportunity__c);
        System.assertNotEquals(null, trade1Account.Account__c);
        System.assertNotEquals(null, trade1Account.Contact__c);
        
        System.assertEquals(null, trade2Account.Lead__c);
        System.assertNotEquals(null, trade2Account.Opportunity__c);
        System.assertNotEquals(null, trade2Account.Account__c);
        System.assertNotEquals(null, trade2Account.Contact__c);
        
        System.assertEquals(practice1Lead.Id, practice1Account.Lead__c);
        System.assertEquals(null, practice1Account.Opportunity__c);
        System.assertEquals(null, practice1Account.Account__c);
        System.assertEquals(null, practice1Account.Contact__c);
        
        
    }
    
    static testMethod void testMarketoUserLeadMerge() {
        
        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        Lead trade1;
        Lead trade2;
        fxAccount__c fxa;
        
        System.runAs(MARKETO_USER) {
            
            // Insert live lead
            trade1 = new Lead();
            trade1.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            trade1.FirstName = 'Joe';
            trade1.LastName = 'Dimaggio';
            trade1.Email = 'jdimaggio@example.org';
            LeadUtil.useDefaultAssignment(trade1);
            
            insert trade1;
        }
        
        trade1 = [SELECT Id, OwnerId FROM Lead WHERE Id = :trade1.Id];
        System.assertNotEquals(MARKETO_USER.Id, trade1.OwnerId);
        
        System.runAs(SYSTEM_USER) {
            
            // Insert live lead
            trade2 = new Lead();
            trade2.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            trade2.FirstName = 'Joe';
            trade2.LastName = 'Dimaggio';
            trade2.Email = 'jdimaggio@example.org';
            
            insert trade2;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            fxa = new fxAccount__c(Account_Email__c='jdimaggio@example.org', Name='test fxaccount');
            insert fxa;
        }
        checkRecursive.SetOfIDs = new Set<Id>();
        
        Test.startTest();
        
        fxa.Trigger_Lead_Assignment_Rules__c=true;
        update fxa;
        checkRecursive.SetOfIDs = new Set<Id>();
        
        mergeConvert();
        checkRecursive.SetOfIDs = new Set<Id>();
        
        BatchTriggerLeadAssignment.executeInline();
        
        Test.stopTest();
        
        
        trade1 = [SELECT Id, OwnerId, Owner.Name, CreatedById, RecordTypeId, ConvertedOpportunityId FROM Lead WHERE Id = :trade1.Id OR Id = :trade2.Id];
        System.assertEquals(MARKETO_USER.Id, trade1.CreatedById);
        System.assertNotEquals(MARKETO_USER.Id, trade1.OwnerId);
        
        //The leas has no country, so it will go to System User
        //System.assertNotEquals(SYSTEM_USER.Id, trade1.OwnerId);
        System.assertEquals(LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, trade1.RecordTypeId);
        
        
        fxa = [SELECT Lead__c, Opportunity__c FROM fxAccount__c WHERE Id = :fxa.Id];
        System.assertEquals(trade1.ConvertedOpportunityId, fxa.Opportunity__c);
    }
    
    //one live lead is merged with contact
    private static testMethod void testMergeLiveLeadWithContact(){
        TestsUtil.forceJobsSync = true;
        
        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        TestDataFactory factory = new TestDataFactory();
        Account acct = factory.createTestAccount();
        factory.createFXTradeAccount(acct);
        
        
        System.runAs(UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER)) {
            Lead trade1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email=acct.PersonEmail);
            
            insert trade1;
            checkRecursive.SetOfIDs = new Set<Id>();

            fxAccount__c fxTrade1 = new fxAccount__c(Lead__c=trade1.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Funnel_Stage__c=FunnelStatus.AWAITING_CLIENT_INFO, Trigger_Lead_Assignment_Rules__c = false);
            
            insert fxTrade1;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            fxTrade1.Trigger_Lead_Assignment_Rules__c = false;
            update fxTrade1;
            
            //create children objects
            Case c = new Case(Subject = 'test case', Lead__c = trade1.Id);
            insert c;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            Test.startTest();
            BatchMergeLeadsScheduleable.executeInline();
            Test.stopTest();
            
            //lead should be deleted now
            Integer counter = [SELECT count() FROM Lead WHERE Id = :trade1.Id];
            System.assertEquals(0, counter);
            
            //case and fxAccount are re-parented to account
            Contact con = [select id from Contact where AccountId = :acct.Id limit 1];
            c = [select id, Lead__c, AccountId, ContactId from Case where Id = :c.Id];
            
            System.assertEquals(null, c.Lead__c);
            System.assertEquals(acct.Id, c.AccountId);
            System.assertEquals(con.id, c.ContactId);
            
            fxTrade1 = [select Id, Lead__c, Account__c, Contact__c from fxAccount__c where Id = :fxTrade1.Id limit 1];
            System.assertEquals(null, fxTrade1.Lead__c);
            System.assertEquals(acct.Id, fxTrade1.Account__c);
            System.assertEquals(con.id, fxTrade1.Contact__c);
            
            
        }
    }
    
    //multiple leads merge with contact
    private static testMethod void testMergeLeadsWithContact2(){
        TestsUtil.forceJobsSync = true;

        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        TestDataFactory factory = new TestDataFactory();
        Account acct = factory.createTestAccount();
        factory.createFXTradeAccount(acct);
        
        
        System.runAs(UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER)) {
            Lead trade1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email=acct.PersonEmail);
            Lead standard = new Lead(LastName='test lead 2', RecordTypeId=RecordTypeUtil.getLeadStandardId(), Email=acct.PersonEmail);
            
            insert new Lead[]{trade1, standard};
                
                fxAccount__c fxTrade1 = new fxAccount__c(Lead__c=trade1.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Funnel_Stage__c=FunnelStatus.AWAITING_CLIENT_INFO, Trigger_Lead_Assignment_Rules__c = false);
                    checkRecursive.SetOfIDs = new Set<Id>();

            insert fxTrade1;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            fxTrade1.Trigger_Lead_Assignment_Rules__c = false;
            update fxTrade1;
            
            //create children objects
            Case c = new Case(Subject = 'test case', Lead__c = standard.Id);
            insert c;
            
            Test.startTest();
            checkRecursive.SetOfIDs = new Set<Id>();
            
            BatchMergeLeadsScheduleable.executeInline();
            Test.stopTest();
            
            //lead should be deleted now
            Integer counter = [SELECT count() FROM Lead WHERE Id = :trade1.Id or Id = :standard.Id];
            System.assertEquals(0, counter);
            
            //case and fxAccount are re-parented to account
            Contact con = [select id from Contact where AccountId = :acct.Id limit 1];
            c = [select id, Lead__c, AccountId, ContactId from Case where Id = :c.Id];
            
            System.assertEquals(null, c.Lead__c);
            System.assertEquals(acct.Id, c.AccountId);
            System.assertEquals(con.id, c.ContactId);
            
            fxTrade1 = [select Id, Lead__c, Account__c, Contact__c from fxAccount__c where Id = :fxTrade1.Id limit 1];
            System.assertEquals(null, fxTrade1.Lead__c);
            System.assertEquals(acct.Id, fxTrade1.Account__c);
            System.assertEquals(con.id, fxTrade1.Contact__c);
            
            
        }
    }
    
    
    //one live lead is merged with multiple contacts
    private static testMethod void testMergeLiveLeadWithContact3(){
        TestsUtil.forceJobsSync = true;

        CustomSettings.setEnableLeadMerging(true);
        CustomSettings.setEnableLeadConversion(true);
        
        TestDataFactory factory = new TestDataFactory();
        Account acct = factory.createTestAccount();
        factory.createFXTradeAccount(acct);
        
        Account acct1 = factory.createTestAccount();
        factory.createFXTradeAccount(acct1);
        
        System.runAs(UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER)) {
            Lead trade1 = new Lead(LastName='test lead 1', RecordTypeId=RecordTypeUtil.getLeadRetailId(), Email=acct.PersonEmail);
            
            insert trade1;
                    checkRecursive.SetOfIDs = new Set<Id>();

            fxAccount__c fxTrade1 = new fxAccount__c(Lead__c=trade1.Id, RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Funnel_Stage__c=FunnelStatus.AWAITING_CLIENT_INFO, Trigger_Lead_Assignment_Rules__c = false);
            
            insert fxTrade1;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            fxTrade1.Trigger_Lead_Assignment_Rules__c = false;
            update fxTrade1;
            
            //create children objects
            Case c = new Case(Subject = 'test case', Lead__c = trade1.Id);
            insert c;
            checkRecursive.SetOfIDs = new Set<Id>();
            
            Test.startTest();
            BatchMergeLeadsScheduleable.executeInline();
            Test.stopTest();
            
            //lead should be deleted now
            Integer counter = [SELECT count() FROM Lead WHERE Id = :trade1.Id];
            System.assertEquals(0, counter);
            
            //case and fxAccount are re-parented to account
            c = [select id, Lead__c, AccountId, ContactId from Case where Id = : c.Id];
            
            System.assertEquals(null, c.Lead__c);
            System.assertNotEquals(null, c.AccountId);
            System.assertNotEquals(null, c.ContactId);
            
            fxTrade1 = [select Id, Lead__c, Account__c, Contact__c from fxAccount__c where Id = :fxTrade1.Id limit 1];
            System.assertEquals(null, fxTrade1.Lead__c);
            System.assertNotEquals(null, fxTrade1.Account__c);
            System.assertNotEquals(null, fxTrade1.Contact__c);
            
            
        }
    }
    
    private static void mergeConvert() {
        checkRecursive.SetOfIDs = new Set<Id>();
        
        BatchMergeLeadsScheduleable.executeInline();
        checkRecursive.SetOfIDs = new Set<Id>();
        
        BatchConvertLeadsScheduleable.executeInline();
    }
}