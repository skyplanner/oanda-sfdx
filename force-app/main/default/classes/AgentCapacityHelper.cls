/**
 * @File Name          : AgentCapacityHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/23/2021, 12:46:24 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification  
 *==============================================================================
 * 1.0    3/16/2021, 1:09:34 PM   acantero     Initial Version
**/
public with sharing class AgentCapacityHelper {

    public static String beginChatClosing(
        String userId,
        String chatId,
        String workId
    ) {
        Agent_Work_Closing__c closingObj = 
            new Agent_Work_Closing__c(
                Agent__c = userId,
                Chat__c = chatId,
                Agent_Work__c = workId
            );
        insert closingObj;
        return closingObj.Id;
    }

    public static String beginCaseClosing(
        String userId,
        String caseId
    ) {
        Agent_Work_Closing__c closingObj = 
            new Agent_Work_Closing__c(
                Agent__c = userId,
                Case__c = caseId
            );
        insert closingObj;
        return closingObj.Id;
    }

    public static Agent_Work_Closing__c getCaseClosing(
        String caseId,
        String agentId
    ) {
        Agent_Work_Closing__c result = null;
        List<Agent_Work_Closing__c> agentWorkClosingList = [
            SELECT
                Id,
                Case__c,
                Closing_Time__c
            FROM
                Agent_Work_Closing__c
            WHERE
                Case__c = :caseId
            AND 
                Agent__c = :agentId 
            AND
                Released__c = FALSE
        ];
        if (!agentWorkClosingList.isEmpty()) {
            result = agentWorkClosingList[0];
        }
        return result;
    }

    public static void endWorkClosing(
        String closingObjId
    ) {
        if (String.isNotBlank(closingObjId)) {
            List<Agent_Work_Closing__c> agentWorkClosingList = [
                SELECT
                    Id
                FROM
                    Agent_Work_Closing__c
                WHERE
                    Id = :closingObjId
            ];
            if (!agentWorkClosingList.isEmpty()) {
                delete agentWorkClosingList;
            }
        }
    }

    public static void finishPendingCaseClosing(String userId) {
        Set<String> chatIdSet = new Set<String>();
        List<Agent_Work_Closing__c> agentWorkClosingList = [
            SELECT
                Id,
                Case__c,
                Released__c
            FROM
                Agent_Work_Closing__c
            WHERE
                Agent__c = :userId
            AND
                Case__c <> NULL
        ];
        if (agentWorkClosingList.isEmpty()) {
            return;
        }
        //else...
        Set<String> caseIdSet = new Set<String>();
        for(Agent_Work_Closing__c workClosing : agentWorkClosingList) {
            if (workClosing.Released__c != true) {
                caseIdSet.add(workClosing.Case__c);
            }
        }
        freeCasesCapacity(caseIdSet);
        delete agentWorkClosingList;
    }

    public static List<ChatWorkClosingInfo> getPendingChatWorks(String userId) {
        GetPendingChatWorksCommand command = 
            new GetPendingChatWorksCommand(userId);
        return command.execute();
    }

    public static void freeCasesCapacity(Set<String> caseIdSet) {
        if (
            (caseIdSet == null) || 
            caseIdSet.isEmpty()
        ) {
            return;
        }
        //else...
        List<Case> caseList = new List<Case>();
        for(String caseId : caseIdSet) {
            caseList.add(
                new Case(
                    Id = caseId,
                    Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED
                )
            );
        }
        update caseList;
    }

    public class GetPendingChatWorksCommand {

        String userId;
        List<Agent_Work_Closing__c> agentWorkClosingList;
        List<AgentWork> agentWorkList;

        public GetPendingChatWorksCommand(
            String userId
        ) {
            this(userId, null, null);
        }

        public GetPendingChatWorksCommand(
            String userId,
            List<Agent_Work_Closing__c> agentWorkClosingList,
            List<AgentWork> agentWorkList
        ) {
            this.userId = userId;
            this.agentWorkClosingList = agentWorkClosingList;
            this.agentWorkList = agentWorkList;
        }

        public List<ChatWorkClosingInfo> execute() {
            List<ChatWorkClosingInfo> result = 
                new List<ChatWorkClosingInfo>();
            if (agentWorkClosingList == null) {
                agentWorkClosingList = getAgentWorkClosingList();
            }
            if (agentWorkClosingList.isEmpty()) {
                return result;
            }
            //else...
            if (agentWorkList == null) {
                agentWorkList = getAgentWorkList();
            }
            Set<String> validWorkList = new Set<String>();
            for(AgentWork agentWorkObj : agentWorkList) {
                validWorkList.add(agentWorkObj.Id);
            }
            List<Agent_Work_Closing__c> invalidList = new List<Agent_Work_Closing__c>();
            for(Agent_Work_Closing__c workClosing : agentWorkClosingList) {
                if (
                    String.isBlank(workClosing.Agent_Work__c) ||
                    (!validWorkList.contains(workClosing.Agent_Work__c))
                ) {
                    invalidList.add(workClosing);
                    //...
                } else {
                    result.add(
                        new ChatWorkClosingInfo(
                            workClosing.Agent_Work__c,
                            workClosing.Id
                        )
                    );
                }
            }
            if (!invalidList.isEmpty()) {
                delete invalidList;
            }
            return result;
        }

        List<Agent_Work_Closing__c> getAgentWorkClosingList() {
            List<Agent_Work_Closing__c> result = [
                SELECT
                    Id,
                    Agent_Work__c	
                FROM
                    Agent_Work_Closing__c
                WHERE
                    Agent__c = :userId
                AND
                    Chat__c <> NULL
            ];
            return result;
        }

        List<AgentWork> getAgentWorkList() {
            Set<String> agentWorkIdSet = new Set<String>();
            for(Agent_Work_Closing__c workClosing : agentWorkClosingList) {
                if (String.isNotBlank(workClosing.Agent_Work__c)) {
                    agentWorkIdSet.add(workClosing.Agent_Work__c);
                }
            }
            List<AgentWork> result = [
                SELECT 
                    Id 
                FROM 
                    AgentWork 
                WHERE 
                    Id IN :agentWorkIdSet 
                AND 
                    Status = :OmnichanelConst.AGENT_WORK_STATUS_OPENED
            ];
            return result;
        }
        
    }

    public class ChatWorkClosingInfo {

        @AuraEnabled
        public String workId {get; set;}
        @AuraEnabled
        public String closingId {get; set;}

        public ChatWorkClosingInfo(
            String workId,
            String closingId
        ) {
            this.workId = workId;
            this.closingId = closingId;
        }

    }


}