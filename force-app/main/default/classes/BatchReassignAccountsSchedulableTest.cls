/* Name: BatchReassignAccountsSchedulableTest 
 * Description : Apex Test Class for the BatchReassignAccountsSchedulableTest 
 * Author: Sri 
 * Date : 2020-05-28
 */
@isTest(seeAllData=false)
public class BatchReassignAccountsSchedulableTest 
{

    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    @testSetup static void init()
    {

        //Create test data
        List<Account> accntInsList = new List<Account>();
        for(Integer i=0; i<6;i++)
        {
            Account accnt = new Account();
            accnt.FirstName = 'TestFirst'+i;
            accnt.LastName = 'TestLast' +i;
            accnt.PersonEmail = 'test'+i+'@oanda.com';
            accnt.Account_Status__c = 'Active';
            if(i<2)
            {
                accnt.PersonMailingCountry = 'Canada'; 
                accnt.Division_Name__c = 'OANDA Canada';  
            }
            else if(i < 4)
            {
                accnt.PersonMailingCountry = 'United Kingdom';   
                accnt.Division_Name__c = 'OANDA Europe';
            }
            else if(i < 6)
            {
                accnt.PersonMailingCountry = 'Thailand';   
                accnt.Division_Name__c = 'OANDA Global Markets';
            }
            accntInsList.add(accnt);
        }

        if(!accntInsList.isEmpty())
        {
            try
            {
            insert accntInsList;
            }
            catch(Exception e)
            {
            }
        }
        List<Opportunity> opptyInsList = new List<Opportunity>();
        for(Integer k = 0; k<6; k++)
        {
            Opportunity oppty = new Opportunity(Name='test name'+k, AccountId=accntInsList[k].Id, ownerId= BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id, CloseDate=Date.today(), StageName=FunnelStatus.TRADED, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, 'Opportunity'));
            opptyInsList.add(oppty);
        }
        
        if(!opptyInsList.isEmpty())
        {
            try
            {
            insert opptyInsList;
            }
            catch(Exception e)
            {
            }
        }
        List<fxAccount__c> fxaList = new List<fxAccount__c>();
        for(Integer k =0; k<4 ; k++)
        {
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'j1@example.org'+k;
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            tradeAccount.First_Trade_Date__c = Datetime.now();
            tradeAccount.Last_Trade_Date__c = DateTime.now();
            tradeAccount.Balance_USD__c = 12000;
            tradeAccount.Account__c = accntInsList[k].Id;
            tradeAccount.Opportunity__c = opptyInsList[k].Id;
            tradeAccount.Division_Name__c='OANDA Europe';
            fxaList.add(tradeAccount);
        }
        if(! fxaList.isEmpty())
        {
            try
            {
            insert fxaList;
            }
            catch(Exception e)
            {
            }
        }
        for(Integer k =0; k<4 ; k++)
        {
            accntInsList[k].fxAccount__c = fxaList[k].Id;
        }
        try
        {
        update accntInsList;
        }
        catch(Exception e)
        {
        }
        
    }
    
    @isTest static void testReassignAccountsSchedulable1() 
    {
        // executing the AMR batch job under test context
        Test.startTest();
        CheckRecursive.setOfIDs = new Set<Id>();
        System.debug('update start');
        String Query = 'SELECT Id, OwnerId,AMR_Transfer_Criteria__c, Date_Customer_Became_Core__c, Current_Balance_USD__c, PersonMailingCountry FROM Account WHERE AMR_Transfer_Criteria__c = true';
        BatchReassignAccountsSchedulable batch = new BatchReassignAccountsSchedulable(query);
        Database.executeBatch(batch);
        System.debug('update stop');
        Test.stopTest();
        // re-querying the data for checking the owner. we need to make sure the owner of the opportunity is not updated even though we change the account owner
        List<Opportunity> oppsReassigned = [SELECT id, Name, OwnerId ,Account.OwnerId FROM Opportunity where Name='test name0' ];
        system.assertEquals(BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id, oppsReassigned[0].OwnerId);
		system.assertNotEquals(oppsReassigned[0].account.ownerId, BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id);
    }

    @isTest static void testReassignAccountsSchedulable2() 
    {
        // executing the OEL batch job under test context
        Test.startTest();
        CheckRecursive.setOfIDs = new Set<Id>();
        System.debug('update start');
        String Query = 'SELECT Id, OwnerId,OEL_Transfer_Criteria__c, Date_Customer_Became_Core__c, Current_Balance_USD__c, PersonMailingCountry FROM Account WHERE OEL_Transfer_Criteria__c = true';
        BatchReassignAccountsSchedulable batch = new BatchReassignAccountsSchedulable(query);
        Database.executeBatch(batch);
        System.debug('update stop');
        Test.stopTest();
        // re-querying the data for checking the owner. we need to make sure the owner of the opportunity is not updated even though we change the account owner
        List<Opportunity> oppsReassigned = [SELECT id, Name, OwnerId,Account.OwnerId FROM Opportunity where Name='test name2'];
        system.assertEquals(BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id, oppsReassigned[0].OwnerId);
		system.assertNotEquals( oppsReassigned[0].account.ownerId, BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id);
    }
    
    @isTest static void testReassignAccountsSchedulable3() 
    {
        // executing the OGM (HVC) batch job under test context
        Test.startTest();
        CheckRecursive.setOfIDs = new Set<Id>();
        System.debug('update start');
        String Query = 'SELECT Id, OwnerId,OGM_Transfer_Criteria_HVC__c, Date_Customer_Became_Core__c, Current_Balance_USD__c, PersonMailingCountry FROM Account WHERE OGM_Transfer_Criteria_HVC__c = true';
        BatchReassignAccountsSchedulable batch = new BatchReassignAccountsSchedulable(query);
        Database.executeBatch(batch);
        System.debug('update stop');
        Test.stopTest();
        // re-querying the data for checking the owner. we need to make sure the owner of the opportunity is not updated even though we change the account owner
        List<Opportunity> oppsReassigned = [SELECT id, Name, OwnerId,Account.OwnerId FROM Opportunity where Name='test name4'];
        system.assertEquals(BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id, oppsReassigned[0].OwnerId);
		system.assertNotEquals( oppsReassigned[0].account.ownerId, BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id);
    }

    @isTest static void testAssignAccountSP9064() 
    {

        Account acc = new Account(
            Name = 'taccount'
        );
        insert acc;

        fxAccount__c fxAcc = new fxAccount__c();
        fxAcc.Account__c = acc.Id;
        fxAcc.Balance_USD__c = 11000;
        fxAcc.Trade_Volume_USD_Last7d__c = 3000000;
        fxAcc.Is_Closed__c = false;
        fxAcc.Account_Locked__c = false;
        fxAcc.Last_Trade_Date__c = System.today();
        fxAcc.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        insert fxAcc;

        Account accnt = new Account(
            FirstName = 'TestFirst',
            LastName = 'TestLast',
            PersonEmail = 'zzassignment@oanda.com',
            Account_Status__c = 'Active',
            fxAccount__c = fxAcc.Id,
            PersonMailingCountry = 'Indonesia',
            Date_Customer_Became_Core__c = System.today()
        );
        insert accnt;

        accnt = [SELECT 
                Id,
                ROA_RM_Transfer_Criteria__c,
                Owner.Username, 
                PersonMailingCountry,
                fxAccount__r.Last_Trade_Date__c,
                Current_Balance_USD__c,
                fxAccount__r.Trade_Volume_USD_Last7d__c,
                fxAccount__r.Is_Closed__c,
                fxAccount__r.Account_Locked__c,
                fxAccount__r.Funnel_Stage__c,
                Is_Excluded_User__c
            FROM Account WHERE Id =: accnt.Id];

        System.assert(accnt.ROA_RM_Transfer_Criteria__c, accnt);

        Test.startTest();

            System.debug('update start');
            String Query = 
                'SELECT Id, OwnerId, Date_Customer_Became_Core__c, ROA_RM_Transfer_Criteria__c, PersonMailingCountry FROM Account WHERE ROA_RM_Transfer_Criteria__c = true';
            BatchReassignAccountsSchedulable batch = new BatchReassignAccountsSchedulable(query);
            Database.executeBatch(batch);
            System.debug('update stop');

        Test.stopTest();
        
        List<Account> accReassigned = 
            [SELECT 
                Id, 
                OwnerId, 
                Owner.Name,
                Date_Customer_Became_Core__c,
                Current_Balance_USD__c, 
                PersonMailingCountry 
            FROM 
                Account
            WHERE Id =: accnt.Id];

        System.assert(accReassigned.size() == 1, accReassigned.size());

        Id transferQueueId = UserUtil.getQueueId('ROA RMs for Account Transfer');
        Set<Id> currentQueueUsers = new Set<Id>();
        currentQueueUsers.addAll(RoundRobinAssignment.getSortedUserIds(transferQueueId));

        System.assert(currentQueueUsers.contains(accReassigned[0].OwnerId), 'Current value is ' + accReassigned[0].Owner.Name);

        // for(Account acc : accReassigned){
        //     System.assertEquals(UserUtil.getUserIdByName('Helen Zha'), acc.OwnerId);
        // }

        // system.assertEquals(BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id, accReassigned[0].OwnerId);
		// system.assertNotEquals( accReassigned[0].account.ownerId, BatchReassignAccountsSchedulableTest.SYSTEM_USER.Id);
    }
}