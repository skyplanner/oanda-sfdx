/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 07-07-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   07-07-2020   dmorales   Initial Version
**/
@isTest
private class StaticLinkSettingManagement_Test {
    @isTest
    static void testConstructor() {
        Map<String, List<Chat_Offline_Links_Setting__mdt>> result = StaticLinkSettingManagement.getStaticLinkSettings(true);
        System.assert(result != null);
    }
}