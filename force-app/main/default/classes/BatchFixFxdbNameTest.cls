@isTest
public class BatchFixFxdbNameTest {

    @isTest
    static void testBatchFixFxdbName() {
        // Create test data
        List<fxAccount__c> testAccounts = new List<fxAccount__c>();
        Lead lead = new Lead(
            LastName='test lead',
            Email='test@test.com');
        insert lead;
        for (Integer i = 0; i < 5; i++) {
            testAccounts.add(new fxAccount__c(
                Name = 'Test Account ' + i,
                FXDB_Name__c = 'Test FXDB Name ' + i,
                Lead__c = lead.Id,
                First_Name__c = 'fn' + i,
                Middle_Name__c = '',    
                Last_Name__c = 'ln' + i,
                Suffix__c = 'Mr'
                // Add other required fields here
            ));
        }
        insert testAccounts;

        // Modify a few accounts to have two consecutive spaces in FXDB_Name__c
        for (Integer i = 0; i < 2; i++) {
            testAccounts[i].FXDB_Name__c = 'FXDB  Name';
        }
        update testAccounts;

        // Run the batch job
        Test.startTest();
        BatchFixFxdbName.executeBatch('SELECT id, FXDB_Name__c, First_Name__c, Middle_Name__c, Last_Name__c, Suffix__c from fxAccount__c' , 200);
        Test.stopTest();

        // Verify the results
        List<fxAccount__c> updatedAccounts = [SELECT Id, FXDB_Name__c FROM fxAccount__c WHERE Id IN :testAccounts];
        for (fxAccount__c updatedAccount : updatedAccounts) {
            System.assertNotEquals('FXDB  Name' ,updatedAccount.FXDB_Name__c, 'FXDB_Name__c should be null');
        }
    }
}