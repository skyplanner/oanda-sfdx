@isTest
private class BatchReassignAccountTest {

    static testMethod void reassginAccountTest() {
        Account a = new Account(lastname ='test account');
        insert a;
        
        User testuser1 = UserUtil.getRandomTestUser();
        Opportunity opp1 = new Opportunity(StageName=FunnelStatus.NOT_STARTED, Name='null Test opp1', CloseDate=Date.today(), AccountId=a.Id, ownerId = testuser1.id,
                                           RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, OpportunityUtil.NAME_OPPORTUNITY));
        insert opp1;
        
        User testuser2 = UserUtil.getRandomTestUser();
        Opportunity opp2 = new Opportunity(StageName=FunnelStatus.NOT_STARTED, Name='null Test opp2', CloseDate=Date.today(), AccountId=a.Id, ownerId = testuser2.id,
                                           RecordTypeId = RecordTypeUtil.getRetentionOpportunityTypeId());
        insert opp2;
        
        String query = 'SELECT id, accountid, ownerId  from Opportunity where recordtypeId = \'' + RecordTypeUtil.getRetentionOpportunityTypeId() +'\'';
        Test.startTest();
        BatchReassignAccount b = new BatchReassignAccount(query);
        Database.executeBatch(b, 200);
        Test.stopTest();
        
        Opportunity opp1a = [select id, ownerId from Opportunity where id = :opp1.Id];
        Opportunity opp2a = [select id, ownerId from Opportunity where id = :opp2.Id];
        Account aa = [select id, ownerId from Account where id = :a.Id];
        
        System.assertEquals(opp1a.OwnerId, opp2a.OwnerId);
        System.assertEquals(opp1a.OwnerId, aa.OwnerId);
    }
}