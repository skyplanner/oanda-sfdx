/**
 * @File Name          : FieldValueSource.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/16/2023, 3:23:15 AM
**/
public interface FieldValueSource {

	String getRecordId();
	Map<String, Object> getFieldValueByFieldNames();

}