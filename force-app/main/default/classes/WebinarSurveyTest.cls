@isTest
private class WebinarSurveyTest {
	static testMethod void testCleanCampaignName() {
		String campaignName = WebinarSurvey.cleanCampaignName('Survey - Understanding Forex - The ABCs - OC - 20140514');
		System.assertEquals('Understanding Forex - The ABCs - OC - 20140514', campaignName);
    }
    
    static testMethod void basicTest() {
    	Webinar_Survey__c ws = new Webinar_Survey__c();
		ws.Email__c = 'devnull@oanda.com';
		ws.Campaign_Name__c = 'test campaign';
		ws.Topics_Raw__c = 'test topic1, Test topic2';
		//ws.Submitted_DateTime_Raw__c = '5/21/14 8:21 PM EDT';
		ws.Submitted_DateTime_Raw__c = '21/5/14 8:21 PM EDT';
		insert ws;
    }
    
    static testMethod void testParseDateTime() {
    	//DateTime dt = WebinarSurvey.parseDateTime('5/21/14 8:21 PM EDT');
    	DateTime dt = WebinarSurvey.parseDateTime('21/5/14 8:21 PM EDT');
    	//DateTime dt = WebinarSurvey.parseDateTime('10/14/2011 11:46 AM');
		System.assertEquals(DateTime.newInstance(2014, 5, 21, 20, 21, 0), dt);
    }
}