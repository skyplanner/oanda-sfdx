public with sharing class ContactInfoFieldsChangedONB extends OutgoingNotificationBus {

    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        Map<String, Object> wrapper = new Map<String, Object>();
        Boolean isUpdated = false;

        for (String field : OutgoingNotificationBusConstants.contactInfoUpdate.keySet()) {
            if (newRecord.get(field) != oldRecord.get(field)) {
                isUpdated = true;
            }
            wrapper.put(OutgoingNotificationBusConstants.contactInfoUpdate.get(field), newRecord.get(field) ?? '');
        }

        if (isUpdated && ((fxAccount__c) newRecord).fxTrade_User_Id__c == null) {
            addEventsError(newRecord.Id, 'ContactInfoFieldsChangedONB - missing fxTrade_User_Id__c');
            return null;
        }

        if (isUpdated) {
            wrapper.put('TYPE', 'Primary');
            return JSON.serialize(new Map<String, List<Object>>{'contacts_address' => new List<Object>{wrapper}});
        }
        return null;
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize( new Map<String, String> {
                'env' => 'live',
                'recordId' =>  ((fxAccount__c) newRecord).Id,
                'tradeUserId' => String.valueOf((Integer)((fxAccount__c) newRecord).fxTrade_User_Id__c)
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME).updateAddress(
                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
                body);
//        new UserCallout().updateAddress(
//                new EnvironmentIdentifier((String)keys.get('env'), (String)keys.get('tradeUserId')),
//                body);
    }

    public override String getRecordId() {
        return recordId;
    }
}