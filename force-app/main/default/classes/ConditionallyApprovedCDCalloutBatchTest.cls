/**
 * @description       : Test class for ConditionallyApprovedCDCalloutBatch class
 * @author            : Jakub Jaworski
**/
@isTest
private class ConditionallyApprovedCDCalloutBatchTest {

    public static String CRON_EXP = '0 30 * * * ?';

    @isTest
    static void conditionallyApprovedCDCalloutBatchTestSchedule() {
        TestDataFactory tdf = new TestDataFactory();

        List<fxAccount__c> fxAccountList = tdf.createFXTradeAccounts(tdf.createLeads(1));

        fxAccountList[0].Conditionally_Approved__c = true;
        fxAccountList[0].Division_Name__c = 'OANDA Global Markets';
        fxAccountList[0].US_Shares_Trading_Enabled__c = true;
        fxAccountList[0].CD_Client_ID__c = 12345;

        update fxAccountList;

        Test.startTest();
        String jobId = System.schedule('test',CRON_EXP, new ConditionallyApprovedCDCalloutBatch());
        Test.stopTest();
        
        List<AsyncApexJob> jobsScheduled = [SELECT Id, ApexClassID, ApexClass.Name, Status, JobType FROM AsyncApexJob WHERE JobType IN ('ScheduledApex','BatchApex')];
        System.assertEquals(2, jobsScheduled.size(), 'expecting both scheduled and batch jobs');
    }

    @isTest
    static void conditionallyApprovedCDCalloutBatchTestPositive() {
        TestDataFactory tdf = new TestDataFactory();

        List<fxAccount__c> fxAccountList = tdf.createFXTradeAccounts(tdf.createLeads(1));

        fxAccountList[0].Conditionally_Approved__c = true;
        fxAccountList[0].Division_Name__c = 'OANDA Global Markets';
        fxAccountList[0].US_Shares_Trading_Enabled__c = true;
        fxAccountList[0].CD_Client_ID__c = 12345;

        update fxAccountList;

        CalloutMock mock = new CalloutMock(
                200,
                '{}',
                'OK',
                null
            );

        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        String jobId = Database.executeBatch(new ConditionallyApprovedCDCalloutBatch(), 1);
        Test.stopTest();
        //No email sent means no errors occured
        // System.assertEquals(0, Limits.getEmailInvocations()); - not giving reliable results due to async issues
    }

    @isTest
    static void conditionallyApprovedCDCalloutBatchTestErrorResponse() {
        TestDataFactory tdf = new TestDataFactory();

        List<fxAccount__c> fxAccountList = tdf.createFXTradeAccounts(tdf.createLeads(1));

        fxAccountList[0].Conditionally_Approved__c = true;
        fxAccountList[0].Division_Name__c = 'OANDA Global Markets';
        fxAccountList[0].US_Shares_Trading_Enabled__c = true;
        fxAccountList[0].CD_Client_ID__c = 12345;

        update fxAccountList;

        CalloutMock mock = new CalloutMock(
            400,
            '{}',
            'Bad Request',
            null
        );

		Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        String jobId = Database.executeBatch(new ConditionallyApprovedCDCalloutBatch(), 1);
        Test.stopTest();
        // System.assertEquals(1, Limits.getEmailInvocations()); - this is not working most likely due to multiple layers of async operations
    }

    @isTest
    static void conditionallyApprovedCDCalloutBatchTestMissingCDClientId() {
        TestDataFactory tdf = new TestDataFactory();

        List<fxAccount__c> fxAccountList = tdf.createFXTradeAccounts(tdf.createLeads(1));

        fxAccountList[0].Conditionally_Approved__c = true;
        fxAccountList[0].Division_Name__c = 'OANDA Global Markets';
        fxAccountList[0].US_Shares_Trading_Enabled__c = true;

        update fxAccountList;

        CalloutMock mock = new CalloutMock(
			200,
			'{}',
			'OK',
			null
		);

		Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        ConditionallyApprovedCDCalloutBatch b = new ConditionallyApprovedCDCalloutBatch();
        String jobId = Database.executeBatch(b, 1);
        
        Test.stopTest();
        
        // System.assertEquals(1, Limits.getEmailInvocations()); - this is not working most likely due to multiple layers of async operations
    }
}