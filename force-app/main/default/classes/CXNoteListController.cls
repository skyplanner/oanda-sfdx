/**
* Controller for CX Note List component
* 
* @author Gilbert Gao
* @last modified on  : 02-22-2021
* @last modified by  : Silvia @Skyplanner
* @last modified : 06-10-2021 Sahil Handa for fixes of the errored script ran by techops.
*/
public with sharing class CXNoteListController {
	

	public class CXNote{

		public Double timestamp;

		@AuraEnabled
        public String author;

        @AuraEnabled
        public String comment;
 
        @AuraEnabled
        public String subject;  
  
        //convert epoch time to datetime
        @AuraEnabled
        public Datetime getcreatedDateTime(){
        	Datetime result = null;

        	if(timestamp != null){
                DateTime todayDate = DateTime.now();
                DateTime dateTimeStamp= DateTime.newInstance(Math.floor(timestamp).longValue());
                DateTime dateTimeStamp2= DateTime.newInstance(Math.floor(timestamp*1000).longValue());
                if(todayDate.year() > dateTimeStamp.year()+40){
                    result = dateTimeStamp2;
                }else
                {
                    result = dateTimeStamp;
                }
        	}

        	return result;
        }   
	}

    //get the notes for the given fxAccount
	@AuraEnabled
	public static List<CXNote> getCXNotes(ID fxAccountId){
        /**
		 * Changes: Adjusted method to get Notes from: fxAccount and Affiliate
		 * Silvia @Skyplanner 16/02/2021
		 */
        List<fxAccount__c> fxas;
        List<Affiliate__c> affiliates;
        String noteValue;
        
        String objectTypeName = fxAccountId.getSObjectType().getDescribe().getName();
        
        List<CXNote> notes = new List<CXNote>();

        System.debug('Current ID for object '+objectTypeName+': '+ fxAccountId);
        if(fxAccountId == null){
        	return notes;
        }

        String query = 'select id, Notes__c';
        query +=' From '+ objectTypeName + ' Where';
        query +=' Id = :fxAccountId';
        List<sObject> sobjList = Database.Query(query);

        if(sobjList.size() == 0){
            return notes;
        }

        if(objectTypeName == 'fxAccount__c'){
            fxas = (List<fxAccount__c>)sobjList;
            noteValue = fxas[0].Notes__c;
        }
        else{
            if(objectTypeName == 'Affiliate__c'){
                affiliates = (List<Affiliate__c>)sobjList;
                noteValue = affiliates[0].Notes__c;
            }
            else{
                return notes;
            }
        }
       /*  List<fxAccount__c> fxas = [select id, Notes__c from fxAccount__c where Id = :fxAccountId]; 
        if(fxas.size() == 0 || fxas[0].Notes__c == null){
        	return notes;
        } */

        notes=parseNotes(noteValue);

		return notes;

    }

    public static List<CXNote> parseNotes (String noteValue){

        List<CXNote> notes = new List<CXNote>();
        if(String.isBlank(noteValue)){
            return notes;
        }
        
        //parse the json encoded CX Notes
		JSONParser parser = JSON.createParser(noteValue);

		while (parser.nextToken() != null) {

	        // Start at the array of CX Notes
	        if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
	            while (parser.nextToken() != null) {

	                // Advance to the start object marker to
	                //  find next CX Note object.
	                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
	                    
	                    CXNote entry = (CXNote)parser.readValueAs(CXNote.class);

	                    system.debug('CX Note timestamp: ' + entry.timestamp);
	                    notes.add(entry);

	                    System.debug('Comment: ' + entry.comment);

	                }
	            }
	        }

        }

        return notes;

    }
    
    //get the notes for the given fxAccount
    public static void addNewCXNote(fxAccount__c fxa, String noteEntry)
    {    
        List<CXNote> notes = new List<CXNote>(); 
        if(string.isNotBlank(noteEntry) &&  fxa != null)
        {
            if(fxa.Notes__c != null)
            {
                JSONParser parser = JSON.createParser(fxa.Notes__c);
                while (parser.nextToken() != null) 
                {
                    if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
                    {
                        while (parser.nextToken() != null) 
                        {
                            if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                            {
                                CXNote entry = (CXNote)parser.readValueAs(CXNote.class);
                                notes.add(entry);
                            }
                        }
                    }
                }
            }

            CXNote note = new CXNote();
            note.timestamp = (Datetime.now().getTime());
            note.comment = noteEntry;
            note.author = UserInfo.getName();
            notes.add(note);

            fxa.Notes__c = JSON.serialize(notes);
        }
	}


	//get all the live fxAccounts for the given account
	@AuraEnabled
	public static List<SObject> getLiveFxAccounts(ID accountId){

        String objectTypeName = accountId.getSObjectType().getDescribe().getName();
        
        System.debug(objectTypeName +' ID: ' + accountId);
        
        if(objectTypeName == 'fxAccount__c' || 
            objectTypeName == 'Account' || 
            objectTypeName == 'Lead' ||
            objectTypeName == 'Opportunity'){
           return  [select Id, 
                        Name, 
                        fxTrade_User_Id__c 
                    from fxAccount__c 
                    where (Id = :accountId or Account__c = :accountId or Lead__c = :accountId or Opportunity__c = :accountId) and
                            recordTypeId = :recordTypeUtil.getFxAccountLiveId()];

             
        }
        else{
            return [select Id, 
                          Name,
                          Firstname__c,
                          Middle_Name__c,
                          LastName__c,
                          Corporate_Name__c,
                          Affiliate_Type__c 
                    from Affiliate__c
                    where Id = :accountId];
        }
       
	}

	//Add a note entry, and return the current notes
	@AuraEnabled
	public static List<CXNote> addNoteEntry(ID fxAccountId, String noteEntry){
        List<SObject> updateList = new List<SObject>();
       
        String objectTypeName = fxAccountId.getSObjectType().getDescribe().getName();
       
        List<CXNote> notes = getCXNotes(fxAccountId);

        System.debug('Number of notes loaded: ' + notes.size());

        CXNote note = new CXNote();
        note.timestamp = (Datetime.now().getTime());
        note.comment = noteEntry;
        note.author = UserInfo.getName();

        notes.add(note);

        if(objectTypeName == 'fxAccount__c'){
            fxAccount__c fxa = new fxAccount__c(Id = fxAccountId, Notes__c = JSON.serialize(notes));
            updateList.add(fxa);
        }
        else{
            if(objectTypeName == 'Affiliate__c'){
                Affiliate__c affiliate =  new Affiliate__c(Id = fxAccountId, Notes__c = JSON.serialize(notes));
                updateList.add(affiliate);
            }
        } 

        if(!updateList.isEmpty()){
            Database.update(updateList);
  
        }

        return notes;
	}

    //Add a RFF note entry
    @InvocableMethod
    public static void addRFFNoteEntry(List<Id> fxAccountIds){

        String accountRFFNote = 'Account Ready for Funding';

        if(fxAccountIds.size() != 1) {
            return;
        }
        else{
            addNoteEntry(fxAccountIds[0], accountRFFNote);
        }                    
    }

}