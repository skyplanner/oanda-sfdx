/**
 * @File Name          : SPEmailUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/24/2021, 5:53:43 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 5:51:41 PM   acantero     Initial Version
**/
@isTest
private without sharing class SPEmailUtil_Test {

    @isTest
    static void checkEmail() {
        Test.startTest();
        Boolean result1 = SPEmailUtil.checkEmail('a@b.com');
        Boolean result2 = SPEmailUtil.checkEmail('a@b.');
        Boolean result3 = SPEmailUtil.checkEmail(null);
        Test.stopTest();
        System.assertEquals(true, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
    }
    
}