/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
public class ScheduledEmailTestUtil {
	public static Account createAccount(String name) {
		Account acc = new Account(Name = name);
		insert acc;
		return acc;
	}
	public static Contact createContact(
		Id accountId,
		String lastName,
		String email
	) {
		Contact ctc = new Contact(
			LastName = lastName,
			FirstName = lastName,
			Email = email,
			AccountId = accountId
		);
		insert ctc;
		return ctc;
	}
	public static Case createCase(Contact ctc) {
		Case newCase = new Case(
			Status = 'New',
			ContactId = ctc.Id,
			Priority = 'Medium',
			Origin = 'Email',
			SuppliedEmail = 'test@oanda.com'
		);
		insert newCase;
		return newCase;
	}
	public static List<Account> createAccounts(Integer count) {
		List<Account> accountList = new List<Account>();
		for (Integer i = 0; i < count; i++) {
			Account newAccount = new Account(Name = 'Test Account ' + i);
			accountList.add(newAccount);
		}
		insert accountList;
		return accountList;
	}
	public static List<Contact> createContacts(List<Account> accounts) {
		List<Contact> contactList = new List<Contact>();
		for (Integer i = 0; i < accounts.size(); i++) {
			Contact newContact = new Contact(
				FirstName = 'Test FistName  ' + i,
				LastName = 'Test LastName  ' + i,
				Email = 'a' + i + '@b' + i + '.com',
				AccountId = accounts.get(i).Id
			);
			contactList.add(newContact);
		}
		insert contactList;
		return contactList;
	}
	public static List<Case> createCases(List<Contact> contacts) {
		List<Case> caseList = new List<Case>();
		for (Contact ctc : contacts) {
			Case newCase = new Case(
				Status = 'New',
				ContactId = ctc.Id,
				Priority = 'Medium',
				Origin = 'Email',
				SuppliedEmail = 'support@test.com'
			);
			caseList.add(newCase);
		}
		insert caseList;
		return caseList;
	}
	public static List<EmailMessage> createDraftEmails(
		List<Contact> contacts,
		List<Case> cases
	) {
		Map<Id, SObject> caseMapByContact = convertToMap(cases, 'ContactId');
		List<EmailMessage> emailMessageList = new List<EmailMessage>();
		for (Contact ctc : contacts) {
			Case currentCase = (Case) caseMapByContact.get(ctc.Id);
			EmailMessage em = new EmailMessage(
				FromAddress = 'support@test.com',
				FromName = ctc.Name,
				ToAddress = ctc.Email,
				Status = EmailMessageRepo.EMAIL_MESSAGE_STATUS_DRAFT,
				ParentId = currentCase.Id
			);
			em.Subject = 'Test from code for ' + ctc.Name;
			em.TextBody = 'TextBody ' + ctc.Name;
			em.HtmlBody = 'htmlBody ';
			emailMessageList.add(em);
		}
		insert emailMessageList;
		return emailMessageList;
	}
	public static List<Scheduled_Email__c> createScheduledMessages(
		List<EmailMessage> messages,
		Date scheduleDate,
		String scheduleTime,
		String amOrPm,
		Datetime scheduledDateTime
	) {
		List<Scheduled_Email__c> scheduledEmailList = new List<Scheduled_Email__c>();
		for (EmailMessage message : messages) {
			Scheduled_Email__c sm = new Scheduled_Email__c(
				Scheduled_Date__c = scheduleDate,
				Time__c = scheduleTime,
				AM_or_PM__c = amOrPm,
				Email_Message__c = message.Id,
				Case__c = message.ParentId,
				Scheduled_Date_Time__c = scheduledDateTime
			);
			scheduledEmailList.add(sm);
		}
		insert scheduledEmailList;
		return scheduledEmailList;
	}
	public static void createScheduledMessagesForDraftEmails(
		Date scheduleDate,
		String scheduleTime,
		String amOrPm,
		Datetime scheduledDateTime
	) {
		List<EmailMessage> messages = [
			SELECT Id, ParentId
			FROM EmailMessage
			WHERE Status = :EmailMessageRepo.EMAIL_MESSAGE_STATUS_DRAFT
		];
		createScheduledMessages(
			messages,
			scheduleDate,
			scheduleTime,
			amOrPm,
			scheduledDateTime
		);
	}
	public static void createStatusJob(String name, String status) {
		Job_Status__c jobStatus = new Job_Status__c(
			Name = name,
			Request_Date__c = System.now(),
			Status__c = status
		);
		insert jobStatus;
	}
	private static Map<Id, SObject> convertToMap(
		List<SObject> objList,
		String key
	) {
		Map<Id, SObject> returnMap = new Map<Id, SObject>();
		for (SObject obj : objList) {
			returnMap.put(Id.valueOf((String) obj.get(key)), obj);
		}
		return returnMap;
	}

	public static void createDraftEmail(Id caseId) {
		EmailMessage em = new EmailMessage();
		em.FromAddress = 'testemail@123.com';
		em.FromName = 'From Address';
		em.ToAddress = 'helloworld@test.com';
		em.Subject = 'Test from code';
		em.TextBody = 'TextBody';
		em.HtmlBody = 'htmlBody';
		em.ParentId = caseId;
		em.Status = '5';
		insert em;
	}
}