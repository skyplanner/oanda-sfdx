/**
 * @File Name          : MessagingChannelSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/9/2024, 1:01:16 PM
**/
public without sharing class MessagingChannelSettings {

	//properties here
	public String channelType {get; set;}
	public Boolean enhancedChannel {get; set;}
	public Boolean extraInfo {get; set;}
	public Boolean isoCountryCode {get; set;}

	public MessagingChannelSettings() {
	}

	public MessagingChannelSettings(Messaging_Channel_Settings__mdt rec) {
		channelType = rec.Channel_Type__c;
		enhancedChannel = rec.Enhanced_Channel__c;
		extraInfo = rec.Extra_Info__c;
		isoCountryCode = rec.Iso_Country_Code__c;
	}

	public static MessagingChannelSettings getByChannel(
		String channelType,
		Boolean enhancedChannel
	) {
		Messaging_Channel_Settings__mdt rec = getRecByChannel(
			channelType, // channelType
			enhancedChannel // enhancedChannel
		);
		MessagingChannelSettings result = new MessagingChannelSettings(rec);
		ServiceLogManager.getInstance().log(
			'getByChannel -> result', // msg
			MessagingChannelSettings.class.getName(), // category
			result // details
		);
		return result;
	}

	@TestVisible
	static Messaging_Channel_Settings__mdt getRecByChannel(
		String channelType,
		Boolean enhancedChannel
	) {
		List<Messaging_Channel_Settings__mdt> recList = [
			SELECT
				Channel_Type__c,
				Enhanced_Channel__c,
				Extra_Info__c,
				Iso_Country_Code__c
			FROM Messaging_Channel_Settings__mdt
			WHERE Channel_Type__c = :channelType
			AND Enhanced_Channel__c = :enhancedChannel
			LIMIT 1
		];
		SPConditionsUtil.checkRequiredResult(
			recList, // recordList
			'Messaging_Channel_Settings__mdt', // entityName
			'ByChannel -> ' +
			'channelType: ' + channelType + ', ' +
			'enhancedChannel: ' + enhancedChannel // condition
		);
		return recList[0];
	}

}