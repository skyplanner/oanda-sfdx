/**
 * @File Name          : GetSameBooleanValueActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/19/2023, 9:26:55 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 9:26:55 AM   aniubo     Initial Version
 **/
@isTest
private class GetSameBooleanValueActionTest {
	@isTest
	private static void testValueListIsNullOrEmpty() {
		// Test data setup
		List<Boolean> sameValues;
		// Actual test
		Test.startTest();

		sameValues = GetSameBooleanValueAction.getSameValue(null);
		Assert.areEqual(true, sameValues.isEmpty(), 'Should return empty list');

		sameValues = null;

		sameValues = GetSameBooleanValueAction.getSameValue(new List<String>());
		Assert.areEqual(true, sameValues.isEmpty(), 'Should return empty list');
		Test.stopTest();
		// Asserts
	}

	@isTest
	private static void testGetSameValueEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			GetSameBooleanValueAction.getSameValue(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testGetSameValue() {
		// Test data setup
		List<String> valueList = new List<String>{
			'True',
			'1',
			'YES',
			'NOP',
			'False'
		};
		// Actual test
		Test.startTest();
		List<Boolean> result = GetSameBooleanValueAction.getSameValue(
			valueList
		);
		Test.stopTest();

		Assert.areEqual(
			true,
			result.get(0),
			'True  value should be return true'
		);
		Assert.areEqual(true, result.get(1), '1 value should be return true');
		Assert.areEqual(true, result.get(2), 'YES value should be return true');
		Assert.areEqual(
			false,
			result.get(3),
			'NOP value should be return false'
		);
		Assert.areEqual(
			false,
			result.get(4),
			'False value should be return false'
		);
		// Asserts
	}
}