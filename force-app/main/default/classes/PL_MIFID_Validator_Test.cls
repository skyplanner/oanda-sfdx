/**
 * @File Name          : PL_MIFID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/24/2020, 5:19:29 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
@isTest
private class PL_MIFID_Validator_Test {

	@isTest
	static void validate_test() {
        PL_MIFID_Validator val = new PL_MIFID_Validator();
		Test.startTest();
		String result1 = val.validate('2234567895'); //10 chars valid
		String result2 = val.validate('2234567896'); //10 chars error
		String result3 = val.validate('02070803628'); //11 chars valid
		Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(System.Label.MifidValPasspChecksumError, result2);
        System.assertEquals(null, result3);
	}

}