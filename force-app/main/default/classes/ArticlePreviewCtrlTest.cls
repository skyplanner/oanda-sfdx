/**
 * Controller for page: ArticlePreview
 * @author Fernando Gomez
 * @since 5/13/2019
 * @LastModified 17/04/2020 dmorales
 */
@isTest
private class ArticlePreviewCtrlTest {
	
	static Document getWrongTestDocument(){
        DocumentManager c = new DocumentManager();
        Document d;
		d = new Document(
			Name = 'Test_Document',
			ContentType = 'application/json',
			DeveloperName = 'TestDoc',
			FolderId = c.folderId,		
			IsPublic = true
		);

		insert d;
        return d;
    }
     
	static Document getFAQTestDocument(){
        DocumentManager c = new DocumentManager();
        Document d;
		String json = getJson();
		d = new Document(
			Name = 'Test_Document',
			ContentType = 'application/json',
			DeveloperName = 'TestDoc',
			Body = Blob.valueOf(json),
			FolderId = c.folderId,		
			IsPublic = true
		);

		insert d;
        return d;
    }

	static String getJson(){
	    String json = '[' +
			 '{ "items": ' +
			 '  [  ' +
					 ' { ' +
					  '"deleted":false, ' +
					  '"name":"Header", ' +
					  '"tag":"h2", ' +
					  '"value":"Test Article", ' +
					  '"error":false ' +
					 ' }, ' +
					 ' { ' +
						'"deleted":false, ' +
						'"name":"Sub-Header", ' +
						'"tag":"h4", ' +
						'"value":"Sub header", ' +
						'"error":false ' + 
					 ' }, ' +
					 ' { ' +
						'"deleted":false, ' +
						'"name":"Section Start", ' +
						'"tag":"section_start", ' +
						'"value":"Section Start", ' +
						'"error":false, ' + 
						'"collapsed": "false"' +
				     ' }, ' +
					 ' { ' +					
						'"name":"Paragraph", ' +
					   	'"tag":"p", ' +
						'"value":"This is a paragraph", ' +
						'"deleted":false, ' +
						'"error":false ' +
					 ' }, ' +
					 ' { ' +
						'"deleted":false, ' +
						'"name":"Section End", ' +
						'"tag":"section_end", ' +
						'"value":"Section End", ' +
						'"error":false ' + 						
				     ' }, ' +
					 ' { ' +
						 '"name":"List", ' +
						 '"tag":"ul", ' +
						 '"items": ' +
						 '[ ' +
							 ' {"value":"List", "deleted":false, "error":false } ' +
						' ], ' +
						 '"deleted":false ' +
					  ' }, ' +
					  ' { ' +
						  '"name":"List", ' +
						  '"tag":"ol", ' +
						  '"items":[ {"value":"List", "deleted":false,  "error":false }], ' +
						  '"deleted":false ' +
					  ' }, ' +
					  ' { ' +
						  '"name":"Table", ' +
						  '"tag":"table", ' +
						  '"columns":[ {"value":"Column Name",  "deleted":false,  "error":false }], ' +
						  '"rows":[ {"cells":[ {"value":"Column",  "deleted":false,  "error":false }],  "deleted":false }] ' +
					  ' }, ' +
					  ' { ' +
						  '"name":"Anchor", ' +
						  '"tag":"a", ' +
						  '"id":"anchor_1571956545161_", ' +
						  '"extra":"Help", ' +
						  '"deleted":false ' +
					  ' }, ' +
					  '{ ' +
						 '"name":"Video",'+
						 '"tag":"video",'+
						 '"type":"youtube.com",'+
						 '"videoId":"ceGLEhahLKQ",'+
						 '"caption":"Caption X",'+
						 '"autoplay":false,'+
						 '"isValid":true,'+
						 '"deleted":false,'+
						 '"selected":false'+
					 '  },' +
					  ' { ' +
						  '"name":"Image", ' +
						  '"tag":"img", ' +
						  '"fileUrl":"/servlet/servlet.ImageServer?id=0150t000000GVuQAAW&oid=00D0t0000000wRVEAY", ' +
						  '"fileName":"1.jpg", ' +
						  '"fileSize":12917, ' +
						  '"fileType":"image/jpeg", ' +
						  '"caption":"", ' +
						  '"error":null ' +
					  ' } ' +					
				' ], ' +
				 '"languageCode":"en_US", ' +
				 '"languageLabel":"English", ' +
				 '"question":"Test article 1", ' +
				 '"error":false ' +
				' } ' +
			']';
        return json;        
	} 

	@isTest static void testContruction() {
		Document d = getWrongTestDocument();
		ArticlePreviewCtrl ctr = new ArticlePreviewCtrl(d.Id);
		System.assert( ctr !=  null);
	}

	
	@isTest static void testContruction1() {	
		ArticlePreviewCtrl ctr = new ArticlePreviewCtrl();
		System.assert( ctr !=  null);
	}
	
	@isTest static void testConstruction2() {
		Document d = getFAQTestDocument();
		ArticlePreviewCtrl ctr = new ArticlePreviewCtrl();
		System.assert( ctr !=  null);        	 
		PageReference pageT = Page.ArticlePreview;
		  pageT.getParameters().put('previewId', d.Id);		 
		Test.setCurrentPage(pageT);	   
		ctr.extractPreview();
		System.assert( ctr.isPreviewAvailable ==  true);
	}

	@isTest static void testGetPreview() {
		Document d = getFAQTestDocument();
		ArticlePreviewCtrl ctr = new ArticlePreviewCtrl('[{"language":"es_US"}]');
		System.assert( ctr !=  null);        	 
		Id prevId = ctr.getPreview(d.Id);  
		System.assert( prevId != null);
	}

	@isTest static void testGetPreview2() {
		Document d = getFAQTestDocument();	        	 
		Id prevId = ArticlePreviewCtrl.getPreview('[{"languageCode":"es_US"}]', d.Id);  
		System.assert( prevId != null);
	} 

	@isTest static void testCreateUpdateArticle() {
		Document d = getFAQTestDocument();	
		String json = getJson();	 
		FAQ__kav faq = ArticlePreviewCtrl.createNewArticle(json, d.Id);  
		ArticlePreviewCtrl.TranslationsWrapper faqT = ArticlePreviewCtrl.getTranslationsFromArticle(faq.Id);  
		System.assert( faq != null);
		System.assert( faqT != null);

		FAQ__kav faqU = ArticlePreviewCtrl.updateExistentArticle(faq.Id, json, d.Id);
		System.assert( faqU != null);
	}

 	@isTest static void testConvertFileToDoc() {				 
		String doc = ArticlePreviewCtrl.convertFileToDocument('TestFileDoc', 'application/.json', 'Test Body');  
		System.assert( doc != '');
	} 

	@isTest static void testGetSupportLang() {				 
		List<Map<String, String>> langS = ArticlePreviewCtrl.getSupportedLanguages();  
		System.assert( langS != null);
	} 
	
}