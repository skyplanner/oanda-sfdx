/**
 * Created by Agnieszka Kajda on 31/01/2024.
 */

public without sharing class QueueManagementController {
    public class QueueOption {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        public QueueOption(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    public class Queues {
        @AuraEnabled
        public List<QueueOption> availableQueues;
        @AuraEnabled
        public List<String> selectedQueues;
        public Queues(){
            availableQueues = new List<QueueOption>();
            selectedQueues = new List<String>();
        }
    }

    @AuraEnabled(Cacheable = true)
    public static Queues getQueues(String recordId){
        Queues queues = new Queues();
        List<QueueOption> allQueues = new List<QueueOption>();
        List<String> selectedQueues = new List<String>();
        for(Group gr : [SELECT Id, Name, (SELECT Id, UserOrGroupId FROM GroupMembers) FROM Group WHERE Type = 'Queue'
        ORDER BY Name ASC]) {
            allQueues.add(new QueueOption(gr.Name, gr.Id));
            for(GroupMember gm: gr.GroupMembers){
                if(gm.UserOrGroupId == recordId) selectedQueues.add(gr.Id);
            }
        }
        queues.availableQueues=allQueues;
        queues.selectedQueues=selectedQueues;
        return queues;
    }

    public static void createAddedAndDeletedMemberList(String recordId, List<String> initialQueues, List<String> selectedQueues, List<GroupMember> newMemberList, List<GroupMember> deleteMemberList) {
        List<Id> deleteMember = new List<Id>();
        for(String queueId: selectedQueues ) {
            if(!initialQueues.contains(queueId)) {
                newMemberList.add(new GroupMember(GroupId = queueId, UserOrGroupId = recordId));
            }
        }
        for(String queueId: initialQueues ) {
            if(!selectedQueues.contains(queueId)) {
                deleteMember.add(queueId);
            }
        }
        if(!deleteMember.isEmpty()) {
            deleteMemberList.addAll([SELECT Id FROM GroupMember WHERE GroupId =: deleteMember AND UserOrGroupId =:recordId]);
        }
    }

    @AuraEnabled
    public static Map<String,String> saveQueuesChanges (String recordId, List<String> selectedQueues, List<String> initialQueues) {
        Map<String,String> returnMessage = new Map<String,String>();
        List<GroupMember> newMemberList = new List<GroupMember>();
        List<GroupMember> deleteMemberList = new List<GroupMember>();
        createAddedAndDeletedMemberList(recordId, initialQueues,selectedQueues,newMemberList,deleteMemberList);
        try {
            insert newMemberList;
            delete deleteMemberList;
            returnMessage.put('status', 'Success');
            returnMessage.put('message', 'Queues updated successfully.');
        }catch (Exception e) {
            returnMessage.put('status', 'Error');
            returnMessage.put('message', 'Failed to update the Queues. Please contact Administrator');
        }

        return returnMessage;
    }

    @AuraEnabled
    public static Boolean isUserOptedOut(String recordId){
        return [SELECT Disable_Lead_Assignment__c FROM User WHERE Id=:recordId].Disable_Lead_Assignment__c;
    }

    @AuraEnabled
    public static Map<String,String>  saveUserOptingOutChange(String recordId, Boolean isUserOptedOut){
        Map<String,String> returnMessage = new Map<String,String>();
        User user = new User(Id=recordId, Disable_Lead_Assignment__c=isUserOptedOut);
        try {
            update user;
            returnMessage.put('status', 'Success');
            returnMessage.put('message', 'Change saved successfully.');
        }catch (Exception e) {
            returnMessage.put('status', 'Error');
            returnMessage.put('message', 'Failed to update the User: '+ e.getMessage() +' Please contact Administrator');
        }
        return returnMessage;
    }




}