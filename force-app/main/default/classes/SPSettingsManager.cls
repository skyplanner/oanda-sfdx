/**
 * @File Name          : SPSettingsManager.cls
 * @Description        : Allows you to load settings stored in 
 * "General Settings". It is similar to the SPGeneralSettings 
 * class but only loads the elements it needs individually, 
 * it does not batch load all the records or cache them.
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/17/2023, 8:14:16 PM
**/
public inherited sharing class SPSettingsManager {

	public static final String DEFAULT_SEPARATOR = ',';

	public static Boolean checkSetting(String settingName) {
		String settingValue = getSetting(settingName);
		return (settingValue == 'true');
	}

	public static Integer getSettingAsInt(String settingName) {
		String settingValue = getSettingOrDefault(settingName, '0');
		return Integer.valueOf(settingValue);
	}

	public static String getSettingOrDefault(
		String settingName,
		String defaultValue
	) {
		String result = getSetting(settingName);
		if (String.isBlank(result)) {
			result = defaultValue;
		}
		return result;
	}

	public static String getOrgSuffix() {
		return UserInfo.getUserName().substringAfterLast('.').toLowerCase();
	}

	public static String getOrgSettingKey(String settingName) {
		return settingName + '_' + getOrgSuffix();
	}

	public static String getSetting(String settingName) {
		String orgSettingKey = getOrgSettingKey(settingName);
		SP_General_Setting__mdt setting = 
			SP_General_Setting__mdt.getInstance(orgSettingKey);
		if (setting == null) {
			setting = SP_General_Setting__mdt.getInstance(settingName);
		}
		return setting?.Value__c;
	}

	public static List<String> getSettingAsList(String settingName) {
		return getSettingAsList(
			settingName, // settingName
			DEFAULT_SEPARATOR // separator
		);
	}

	public static List<String> getSettingAsList(
		String settingName,
		String separator
	) {
		String settingValue = getSetting(settingName);
		// if there is a value...
		if (String.isNotBlank(settingValue)) {
			return settingValue.split(separator);
		}
		//else...
		return new List<String>();
	}

	public static Set<String> getSettingAsSet(String settingName) {
		return getSettingAsSet(
			settingName, // settingName
			DEFAULT_SEPARATOR // separator
		);
	}

	public static Set<String> getSettingAsSet(
		String settingName,
		String separator
	) {
		return new Set<String>(
			getSettingAsList(
				settingName, 
				separator
			)
		);
	}

}