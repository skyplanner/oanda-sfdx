/**
 * @File Name          : SCustomerTypeProvider.cls
 * @Description        : 
 * Returns the CustomerType based on the information of a specific customer
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/25/2024, 11:18:49 AM
**/
public interface SCustomerTypeProvider {

	OmnichannelCustomerConst.CustomerType getCustomerType(
		SCustomerTypeInfo customerInfo
	);

}