/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-09-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasDefaultAccountConfig {
    public Integer commissionGroupID {get; set;}

    public Boolean configurationLocked {get; set;}

    public Boolean depositLocked {get; set;}

    public TasDivisionTradingGroupId divisionTradingGroupID {get; set;}

    public Boolean hedgingEnabled {get; set;}

    public Boolean locked {get; set;}

    public Boolean newPositionsLocked {get; set;}

    public Boolean orderCancelLocked {get; set;}

    public Boolean orderCreationLocked {get; set;}

    public Boolean orderFillLocked {get; set;}

    public List<Integer> pricingGroupOverrideIDs {get; set;}

    public Boolean withdrawalLocked {get; set;}

    public Integer currencyConversionGroupOverrideID {get; set;}

    public List<Integer> financingGroupIDs {get; set;}
}