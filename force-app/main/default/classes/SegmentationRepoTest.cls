/**
 * @File Name          : SegmentationRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/15/2024, 5:06:01 PM
**/
@IsTest
private without sharing class SegmentationRepoTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createSegmentation1(initManager);
		initManager.storeData();
	}

	@IsTest
	static void getActiveByFxAccount() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID fxAccount1Id = initManager.getObjectId(
			ServiceTestDataKeys.FX_ACCOUNT_1,
			true
		);
		Set<ID> fxAccountIds = new Set<ID> { fxAccount1Id };

		Test.startTest();
		List<Segmentation__c> result = 
			SegmentationRepo.getActiveByFxAccount(fxAccountIds);
		Test.stopTest();
		
		Assert.isFalse(result.isEmpty(), 'Invalid result');
	}
	
}