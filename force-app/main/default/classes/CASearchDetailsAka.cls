/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsAka {
    public String name { get; set; }

    public String first_name { get; set; }

    public String last_name { get; set; }
}