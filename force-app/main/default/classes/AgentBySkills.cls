/**
 * @File Name          : AgentBySkills.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/25/2024, 1:01:36 AM
**/
public without sharing class AgentBySkills {

    @testVisible
    List<String> servicePresenceStatusIdList;

    public List<String> activeAgents {get; set;}

    public AgentBySkills(
        List<String> servicePresenceStatusIdList
    ) {
        ServiceLogManager.getInstance().log(
			'AgentBySkills -> servicePresenceStatusIdList', // msg
			AgentBySkills.class.getName(), // category
			servicePresenceStatusIdList // details
		);
        this.servicePresenceStatusIdList = servicePresenceStatusIdList;
    }

    public Boolean existsCompatibleAgent(
        String skillsCode, 
        Decimal capacityWeight,
        String currentAgentId
    ) {
        List<String> realActiveAgents = (activeAgents != null)
            ? activeAgents
            : SPChatUtil.getOtherActiveAgents(servicePresenceStatusIdList, currentAgentId);
        if (realActiveAgents.isEmpty()) {
            return false;
        }
        //else...
        Boolean result = AgentInfoManager.existsCompatibleAgent(realActiveAgents, skillsCode, capacityWeight);
        return result;
    }

    public Set<String> existsCompatibleAgents(
        Set<String> skillsCodes, 
        Decimal capacityWeight
    ) {
        ServiceLogManager.getInstance().log(
			'existsCompatibleAgents -> ' +
			'capacityWeight: ' + capacityWeight +
			', skillsCodes: (details)', // msg
			AgentBySkills.class.getName(), // category
			skillsCodes // details
		);
        Set<String> result = new Set<String>();

        if (
            (skillsCodes == null) ||
            skillsCodes.isEmpty()
        ) {
            ServiceLogManager.getInstance().log(
				'existsCompatibleAgents -> result', // msg
				AgentBySkills.class.getName(), // category
				result // details
			);
            return result;
        }
        //else...
        List<String> realActiveAgents = (activeAgents != null)
            ? activeAgents
            : SPChatUtil.getActiveAgents(servicePresenceStatusIdList);
        ServiceLogManager.getInstance().log(
			'existsCompatibleAgents -> realActiveAgents', // msg
			AgentBySkills.class.getName(), // category
			realActiveAgents // details
		);

        if (realActiveAgents.isEmpty()) {
            ServiceLogManager.getInstance().log(
				'existsCompatibleAgents -> result', // msg
				AgentBySkills.class.getName(), // category
				result // details
			);
            return result;
        }
        //else...
        for (String skillsCode : skillsCodes) {
            Boolean existsAgent = AgentInfoManager.existsCompatibleAgent(
                realActiveAgents, // userIdList
                skillsCode, // skills
                capacityWeight // capacityWeight
            );

            if (existsAgent == true) {
                result.add(skillsCode);
            }
        }

        ServiceLogManager.getInstance().log(
			'existsCompatibleAgents -> result', // msg
			AgentBySkills.class.getName(), // category
			result // details
		);
        return result;
    }

}