/**
 * @File Name          : OnCaseOwnerOrStatusChangesCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/4/2024, 12:00:46 PM
**/
@IsTest
private without sharing class OnCaseOwnerOrStatusChangesCmd_Test {

    @testSetup
    static void setup() {
        Case newCustomerLeadCase1 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_IN_USE,
            Routed__c = true,
            Status = OmnichanelConst.CASE_STATUS_OPEN
        );
        List<Case> caseList = new List<Case>{newCustomerLeadCase1};
        OmnichanelRoutingTestDataFactory.setupAllForNonHvcEmailFrontdeskCases(
            caseList, 
            true // setupLanguagesFields
        );
    }

    /**
     * test 1 : no internal field has changed => return false
     * test 2 : Routed__c has changed => return true
     * test 3 : Routed_And_Assigned__c has changed => return true
     * test 4 : Agent_Capacity_Status__c has changed => return true
     * test 5 : Ready_For_Routing__c has changed => return true
     */
    @IsTest
    static void isAnInternalChange() {
        Case oldRec = new Case();

        Test.startTest();
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        // test 1 => return false
        Case rec1 = new Case();
        Boolean result1 = instance.isAnInternalChange(rec1, oldRec);
        // test 2 => return true
        Case rec2 = new Case(Routed__c = true);
        Boolean result2 = instance.isAnInternalChange(rec2, oldRec);
        // test 3 => return true
        Case rec3 = new Case(Routed_And_Assigned__c = true);
        Boolean result3 = instance.isAnInternalChange(rec3, oldRec);
        // test 4 => return true
        Case rec4 = new Case(
            Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED
        );
        Boolean result4 = instance.isAnInternalChange(rec4, oldRec);
        // test 5 => return true
        Case rec5 = new Case(
            Ready_For_Routing__c = true
        );
        Boolean result5 = instance.isAnInternalChange(rec5, oldRec);
        Test.stopTest();

        Assert.isFalse(result1, 'Invalid result');
        Assert.isTrue(
            result2, 
            'Changes to the Routed__c field should be considered internal changes'
        );
        Assert.isTrue(
            result3, 
            'Changes to the Routed_And_Assigned__c field should be considered internal changes'
        );
        Assert.isTrue(
            result4, 
            'Changes to the Agent_Capacity_Status__c field should be considered internal changes'
        );
        Assert.isTrue(
            result5, 
            'Changes to the Ready_For_Routing__c field should be considered internal changes'
        );
    }

    // change status 
    // case 1 : change Agent_Capacity_Status__c at the same time => nothing happens
    // case 2 : is a valid Routed Case and old status = Open => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_RELEASED
    // case 3 : is a valid Routed Case and old status <> Open => Agent_Capacity_Status__c does not change
    // case 4 : is not a valid Routed Case => Agent_Capacity_Status__c does not change
    @IsTest
    static void checkRecord1() {
        Case rec = getCase();
        Case oldRec = getCase();
        String originalAgentCapacityStatus = oldRec.Agent_Capacity_Status__c;
        String originalStatus = oldRec.Status;
        Test.startTest();
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        // change status
        rec.Status = OmnichanelConst.CASE_STATUS_IN_PROGRESS;
        // case 1
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED;
        instance.checkRecord(rec, oldRec);
        String result1 = rec.Agent_Capacity_Status__c;
        rec.Agent_Capacity_Status__c = originalAgentCapacityStatus; // restore original value
        // case 2
        instance.checkRecord(rec, oldRec);
        String result2 = rec.Agent_Capacity_Status__c;
        rec.Agent_Capacity_Status__c = originalAgentCapacityStatus; // restore original value
        // case 3
        oldRec.Status = 'In Progress';
        instance.checkRecord(rec, oldRec);
        String result3 = rec.Agent_Capacity_Status__c;
        oldRec.Status = originalStatus; // restore original value
        // case 4
        // you have to change the field in the two objects because 
        // otherwise it is interpreted as an internal change
        rec.Routed__c = false;
        oldRec.Routed__c = false;
        //...
        instance.checkRecord(rec, oldRec);
        String result4 = rec.Agent_Capacity_Status__c;
        //...
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_CLOSED, 
            result1,
            'Invalid value'
        );
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, 
            result2,
            'Invalid value'
        );
        System.assertEquals(
            originalAgentCapacityStatus, 
            result3,
            'Invalid value'
        );
        System.assertEquals(
            originalAgentCapacityStatus, 
            result4,
            'Invalid value'
        );
    }

    // change owner for a valid Routed Case, the new owner implies routing 
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE
    @IsTest
    static void checkRecord2() {
        Case rec = getCase();
        Case oldRec = getCase();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0009');
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager reroutingManager = CaseReroutingManager.getInstance();
        System.runAs(supervisor) {
            String agentId = UserInfo.getUserId();
            reroutingManager.reroutingQueueIdSet = new Set<ID>{agentId};
            OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
            // change owner
            rec.OwnerId = agentId;
            instance.checkRecord(rec, oldRec);
        }
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
    }

    // change owner for a valid Routed Case, the new owner does not implies routing 
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_RELEASED
    @IsTest
    static void checkRecord3() {
        Case rec = getCase();
        Case oldRec = getCase();
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0010');
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        System.runAs(supervisor) {
            String agentId = UserInfo.getUserId();
            OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
            // change owner
            rec.OwnerId = agentId;
            instance.checkRecord(rec, oldRec);
        }
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
    }

    // change owner, case is not a valid Routed Case, the new owner implies routing 
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    @IsTest
    static void checkRecord4() {
        Case rec = getCase();
        Case oldRec = getCase();
        // you have to change the field in the two objects because 
        // otherwise it is interpreted as an internal change
        rec.Routed__c = oldRec.Routed__c = false; // to ensure that it is not a "valid routed case"
        //...
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0009');
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager reroutingManager = CaseReroutingManager.getInstance();
        System.runAs(supervisor) {
            String agentId = UserInfo.getUserId();
            reroutingManager.reroutingQueueIdSet = new Set<ID>{agentId};
            OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
            // change owner
            rec.OwnerId = agentId;
            instance.checkRecord(rec, oldRec);
        }
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
    }

    // all conditions are met => return true
    // Routed__c = false => return false
    // Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_RELEASED => return false
    @IsTest
    static void isAValidRoutedCase() {
        Case rec = getCase();
        Test.startTest();
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        Boolean result1 = instance.isAValidRoutedCase(rec);
        //...
        rec.Routed__c = false;
        Boolean result2 = instance.isAValidRoutedCase(rec);
        //...
        rec.Routed__c = true;
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED;
        Boolean result3 = instance.isAValidRoutedCase(rec);
        Test.stopTest();
        System.assertEquals(true, result1, 'Invalid result');
        System.assertEquals(false, result2, 'Invalid result');
        System.assertEquals(false, result3, 'Invalid result');
    }

    // ownerChange and new case owner implies routing 
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_REROUTE
    @IsTest
    static void markRoutedCaseAsReleased1() {
        Case rec = getCase();
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager reroutingManager = CaseReroutingManager.getInstance();
        reroutingManager.reroutingQueueIdSet = new Set<ID>{rec.OwnerId};
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        instance.markRoutedCaseAsReleased(rec, true);
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
    }

    // ownerChange = false
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_RELEASED
    @IsTest
    static void markRoutedCaseAsReleased2() {
        Case rec = getCase();
        Test.startTest();
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        instance.markRoutedCaseAsReleased(rec, false);
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
    }

    // new case owner implies routing 
    // => return true
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    // => Internally_Routed__c = true
    // => Routed__c = true
    @IsTest
    static void checkNewOwnerImpliesRouting1() {
        Case rec = getCase();
        rec.Routed__c = false;
        Test.startTest();
        SPSecurityUtil.validStandardUser = true;
        CaseReroutingManager reroutingManager = CaseReroutingManager.getInstance();
        reroutingManager.reroutingQueueIdSet = new Set<ID>{rec.OwnerId};
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        Boolean result = instance.checkNewOwnerImpliesRouting(rec);
        Test.stopTest();
        System.assertEquals(true, result, 'Invalid value');
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
        System.assertEquals(true, rec.Internally_Routed__c, 'Invalid value');
        System.assertEquals(true, rec.Routed__c, 'Invalid value');
    }

    // the new case owner does not imply routing
    // => return true
    // => Agent_Capacity_Status__c does not change
    // => Internally_Routed__c does not change
    // => Routed__c does not change
    @IsTest
    static void checkNewOwnerImpliesRouting2() {
        Case rec = getCase();
        String oldAgentCapacityStatus = rec.Agent_Capacity_Status__c;
        rec.Routed__c = false;
        Test.startTest();
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        Boolean result = instance.checkNewOwnerImpliesRouting(rec);
        Test.stopTest();
        System.assertEquals(false, result, 'Invalid value');
        System.assertEquals(
            oldAgentCapacityStatus, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
        System.assertEquals(false, rec.Internally_Routed__c, 'Invalid value');
        System.assertEquals(false, rec.Routed__c, 'Invalid value');
    }

    // new case status implies routing 
    // => return true
    // => Agent_Capacity_Status__c = CASE_CAPACITY_STATUS_ROUTING
    // => Internally_Routed__c = true
    // => Routed__c = true
    @IsTest
    static void checkNewStatusImpliesRouting() {
        Case oldRec = getCase();
        Case rec = getCase();
        oldRec.Status = OmnichanelConst.CASE_STATUS_CLOSED;
        rec.Status = OmnichanelConst.CASE_STATUS_OPEN;
        Test.startTest();
        CaseReroutingManager reroutingManager = CaseReroutingManager.getInstance();
        reroutingManager.reroutingQueueIdSet = new Set<ID>{rec.OwnerId};
        OnCaseOwnerOrStatusChangesCmd instance = new OnCaseOwnerOrStatusChangesCmd();
        Boolean result = instance.checkNewStatusImpliesRouting(rec, oldRec);
        Test.stopTest();
        System.assertEquals(true, result, 'Invalid value');
        System.assertEquals(
            OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING, 
            rec.Agent_Capacity_Status__c,
            'Invalid value'
        );
        System.assertEquals(true, rec.Internally_Routed__c, 'Invalid value');
        System.assertEquals(true, rec.Routed__c, 'Invalid value');
    }

    static Case getCase() {
        return [
            select 
                Id, 
                Agent_Capacity_Status__c, 
                OwnerId, 
                Status,
                Routed__c,
                Routed_And_Assigned__c,
                Internally_Routed__c,
                Ready_For_Routing__c
            from 
                Case
            limit 1
        ];
    }
    
}