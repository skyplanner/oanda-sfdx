@isTest
private class TestClass {
    
    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
    static User FXS_USER = UserUtil.getRandomTestUser();
    static User FXS_USER2 = UserUtil.getRandomTestUser();
    
    static {
        CustomSettings.setEnableLeadConversion(true);
        CustomSettings.setEnableLeadMerging(true);
    }
    
    static testMethod void testBatchAssignPractice() {
        System.runAs(SYSTEM_USER) {
            
            Lead lead1 = new Lead(LastName='Schmoe', RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Email='joe@example.org', Territory__c = 'North America');
            insert lead1;
            
            fxAccount__c fxAccount = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Account_Email__c = 'joe@example.org');
            insert fxAccount;
            
            Test.startTest();
            BatchAssignPracticeLeadsScheduleable.executeBatch();
            Test.stopTest();
                        
            lead1 = [SELECT Id, OwnerId FROM Lead WHERE Id =: lead1.Id];
            
            // Lead assigned to queue
            //System.assert(SYSTEM_USER.Id != lead1.OwnerId);
        }
    }
    
    static testMethod void testAutomaticLeadAssignmentOfStandardLeads() {
        
        System.runAs(SYSTEM_USER) {
            Lead webinar = new Lead(LastName = 'Schmoe', Email = 'joe@example.org', Territory__c = 'North America', RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD);
            LeadUtil.useDefaultAssignment(webinar);
            
            insert webinar;
            
            webinar = [SELECT Id, OwnerId FROM Lead WHERE Id =: webinar.Id];
            
            //System.assert(webinar.OwnerId != SYSTEM_USER.Id);
            System.assert(!UserUtil.isQueueUser(webinar.OwnerId));
        }
    }
    
    static testMethod void testConvertLeadOwnedByInactive() {
        
        System.runAs(SYSTEM_USER) {
            User fxs = UserUtil.getTestUser();
            insert fxs;
            
            Lead lead = new Lead(LastName = 'Schmoe', Email = 'jschmoe@example.org', RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Territory__c = 'North America');
            insert lead;
            
            fxAccount__c fxAccount = new fxAccount__c(RecordTypeId=fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Account_Email__c = 'jschmoe@example.org');
            insert fxAccount;
            
            
            //Assign to a (currently) active user
            lead.OwnerId = fxs.Id;
            update lead;
            
            // Make owner inactive
            fxs.IsActive = false;
            update fxs;

            Test.startTest();
                        
            // Try to convert the lead
            BatchConvertLeadsScheduleable.executeInline();
            
            Test.stopTest();
                        
            lead = [SELECT Id, OwnerId, IsConverted FROM Lead WHERE Id =: lead.Id];
            
            // Lead was reassigned and converted
            System.assert(lead.IsConverted);
            System.assert(fxs.Id != lead.OwnerId);
            //System.assert(SYSTEM_USER.Id != lead.OwnerId);
        }
    }
    
    
    static testMethod void testMergeCampaignsWithLeads() {
        
        System.runAs(SYSTEM_USER) {
            Lead existing = new Lead();
            existing.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existing.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existing.FirstName = 'Jake';
            existing.LastName = 'Rake';
            existing.Email = 'jrake@example.org';
            
            insert existing;
            
            fxAccount__c liveAccount = new fxAccount__c();
            liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            liveAccount.Account_Email__c = existing.Email;
            liveAccount.recordTypeId = RecordTypeUtil.getFxAccountLiveId();
            
            insert liveAccount;
            
            //liveAccount.Trigger_Lead_Assignment_Rules__c = false;
            //update liveAccount;
            
            Lead webinar = new Lead();
            webinar.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            webinar.FirstName = 'Jacob';
            webinar.LastName = 'Rake';
            webinar.Email = 'jrake@example.org';
            
            insert webinar;
            
            
            Campaign campaign = new Campaign();
            campaign.Name = 'Test';
            insert campaign;
            
            CampaignMember cm = new CampaignMember();
            cm.CampaignId = campaign.Id;
            cm.LeadId = webinar.Id;
            cm.Status = 'Responded';
            insert cm;
            
            Test.startTest();
            checkRecursive.SetOfIDs = new Set<Id>();

            mergeConvert();
            
            Test.stopTest();
            
            
            existing = [SELECT Id, IsConverted FROM Lead WHERE Email = 'jrake@example.org'];
            
            System.assert(!existing.IsConverted);
            
            cm = [SELECT Id, LeadId, ContactId, CampaignId, Status, HasResponded FROM CampaignMember WHERE CampaignId = :campaign.Id];
            
            System.assertEquals(existing.Id, cm.LeadId);
            System.assert(cm.ContactId == null);
            System.assertEquals('Responded', cm.Status);
            System.assertEquals(campaign.Id, cm.CampaignId);
            System.assert(cm.HasResponded);
        }
    }
    
    
    
    static testMethod void testMergeCampaignsWithConvertedLead() {
        
        Lead existing;
        CampaignMember cm;
        Campaign campaign;
        
        
        //System.runAs(SYSTEM_USER)
        System.runAs(new User(Id = UserInfo.getUserId())) {
            existing = new Lead();
            existing.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existing.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existing.FirstName = 'Jake';
            existing.LastName = 'Rake';
            existing.Email = 'jrake@example.org';
            
            insert existing;
            
            fxAccount__c liveAccount = new fxAccount__c();
            liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            liveAccount.Account_Email__c = existing.Email;
            liveAccount.lead__c = existing.Id;
            
            insert liveAccount;
            
            Test.startTest();
            
            List<fxAccount__c> liveList = [select id, lead__c, lead__r.name  from fxAccount__c where id = :liveAccount.id];
            System.debug('Existing lead: ' + liveList[0].lead__r.name);
            
            // Trigger lead conversion to Opportunity
            existing = [SELECT Id, OwnerId FROM Lead WHERE Id = :existing.Id];
            existing.OwnerId = FXS_USER.Id;
            update existing;
            
            mergeConvert();
        }
        //Test.stopTest();
       
        
        //Test.startTest();
        System.runAs(SYSTEM_USER){
            Lead webinar = new Lead();
            webinar.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            webinar.FirstName = 'Jacob';
            webinar.LastName = 'Rake';
            webinar.Email = 'jrake@example.org';
            
            insert webinar;
            
            
            campaign = new Campaign();
            campaign.Name = 'Test';
            insert campaign;
            
            cm = new CampaignMember();
            cm.CampaignId = campaign.Id;
            cm.LeadId = webinar.Id;
            cm.Status = 'Responded';
            insert cm;
            
        }

        //System.runAs(new User(Id = UserInfo.getUserId())){
        
        //Test.stopTest();
            
        //    Test.startTest();
            
        //    mergeConvert();
            
            
            
        //    existing = [SELECT Id, IsConverted, ConvertedContactId FROM Lead WHERE Email = 'jrake@example.org'];
            
        //    System.assert(existing.IsConverted);
            
            // Original cm is deleted
        /*    try {
                cm = [SELECT Id, LeadId, ContactId FROM CampaignMember WHERE Id = :cm.Id];
                System.assert(false);
            }
            catch (Exception e) {

            }
        //}*/    
        Test.stopTest();
            
        //creating some delay for Is_Accumulating flag
        Integer start = System.Now().millisecond();
        while(System.Now().millisecond()< start+10){ 
            }
            
        cm = [SELECT Id, LeadId, ContactId, CampaignId, Status, HasResponded FROM CampaignMember WHERE CampaignId = :campaign.Id];
            
        System.assert(cm.LeadId != null);
       // System.assertEquals(existing.ConvertedContactId, cm.ContactId);
        System.assertEquals('Responded', cm.Status);
        System.assertEquals(campaign.Id, cm.CampaignId);
        System.assert(cm.HasResponded);
        
    }
    
    static testMethod void testMergeTasksWithLeads() {
        
        System.runAs(SYSTEM_USER) {
            Lead existing = new Lead();
            existing.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existing.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existing.FirstName = 'Jake';
            existing.LastName = 'Rake';
            existing.Email = 'jrake@example.org';
            
            insert existing;
            
            fxAccount__c liveAccount = new fxAccount__c();
            liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            liveAccount.Account_Email__c = existing.Email;
            liveAccount.recordTypeId = RecordTypeUtil.getFxAccountLiveId();
            
            insert liveAccount;
            
            
            
            Lead webinar = new Lead();
            webinar.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            webinar.FirstName = 'Jacob';
            webinar.LastName = 'Rake';
            webinar.Email = 'jrake@example.org';
            
            insert webinar;
            
            Task task = new Task();
            task.Subject = 'Call';
            task.WhoId = webinar.Id;
            
            insert task;
            
            checkRecursive.SetOfIDs = new Set<Id>();

            
            mergeConvert();
            
            
            
            
            existing = [SELECT Id, IsConverted FROM Lead WHERE Email = 'jrake@example.org'];
            
            System.assert(!existing.IsConverted);
            
            task = [SELECT Id, WhoId FROM Task WHERE Id = :task.Id];
            
            System.assertEquals(existing.Id, task.WhoId);
        }
    }
    
    
    
    static testMethod void testMergeTasksWithConvertedLead() {
        
        System.runAs(SYSTEM_USER) {
            Lead existing = new Lead();
            existing.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existing.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existing.FirstName = 'Jake';
            existing.LastName = 'Rake';
            existing.Email = 'jrake@example.org';
            
            insert existing;
            
            fxAccount__c liveAccount = new fxAccount__c();
            liveAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            liveAccount.Account_Email__c = existing.Email;
            
            insert liveAccount;
            
            // Trigger lead conversion to Opportunity
            existing = [SELECT Id, OwnerId FROM Lead WHERE Id = :existing.Id];
            existing.OwnerId = FXS_USER.Id;
            update existing;
            
            mergeConvert();
        
            Test.startTest();
            
            Lead webinar = new Lead();
            webinar.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            webinar.FirstName = 'Jacob';
            webinar.LastName = 'Rake';
            webinar.Email = 'jrake@example.org';
            
            insert webinar;
            
            webinar.OwnerId = FXS_USER.Id;
            update webinar;
            
            // Task created by same user
            Task sameUserTask;
            System.runAs(FXS_USER) {
                sameUserTask = new Task();
                sameUserTask.Subject = 'Call';
                sameUserTask.WhoId = webinar.Id;
            
                insert sameUserTask;
            }
            
            
            webinar.OwnerId = FXS_USER2.Id;
            update webinar;
            
            // Task created by different user
            Task differentUserTask;
            System.runAs(FXS_USER2) {
                differentUserTask = new Task();
                differentUserTask.Subject = 'Call';
                differentUserTask.WhoId = webinar.Id;
            
                insert differentUserTask;
            }
            
            //Task created by system user
            Task systemTask = new Task();
            systemTask.Subject = 'Call';
            systemTask.WhoId = webinar.Id;
            
            insert systemTask;
            
            checkRecursive.SetOfIDs = new Set<Id>();

            mergeConvert();
            
            
            existing = [SELECT Id, IsConverted, ConvertedOpportunityId, ConvertedContactId FROM Lead WHERE Email = 'jrake@example.org'];
            
            System.assert(existing.IsConverted);
            
            Test.stopTest();
            
            //all tasks should be reparented
            for(Task t : [SELECT Id, WhoId, WhatId FROM Task WHERE Id = :systemTask.Id or Id = :sameUserTask.Id or Id = :differentUserTask.Id]){
            	System.assertEquals(existing.ConvertedContactId, t.WhoId);
            }
            
        }
    }
    
    /* static testMethod void test8ST() {
        System.runAs(SYSTEM_USER) {
            Lead lead = new Lead();
            lead.LastName = 'Schmoe';
            lead.Email = 'js@example.org';
            
            insert lead;
            
            
            fxAccount__c fxAccount = new fxAccount__c();
            fxAccount.Account_Email__c = 'js@example.org';
            fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            fxAccount.Registration_Source__c = '8st';
            fxAccount.Funnel_Stage__c = FunnelStatus.EST_PHONE_VALIDATED;
            
            insert fxAccount;
            
            Test.startTest();
            
            //mergeConvert();
            BatchConvertLeadsScheduleable.executeInline();
            
            
            //fxAccount.Funnel_Stage__c = FunnelStatus.EST_FUNDED;
            //update fxAccount;
            
            //mergeConvert();

            fxAccount.Funnel_Stage__c = FunnelStatus.EST_DOCUMENTS_UPLOADED;
            update fxAccount;
            
            //mergeConvert();
            BatchConvertLeadsScheduleable.executeInline();
            
            fxAccount.Funnel_Stage__c = FunnelStatus.EST_MORE_INFO_REQUIRED;
            update fxAccount;
            
            //mergeConvert();
            BatchConvertLeadsScheduleable.executeInline();
            
            
            
            fxAccount.Funnel_Stage__c = FunnelStatus.EST_ID_VERIFIED;
            fxAccount.Total_Deposits_USD__c = 1;
            fxAccount.Traded__c = true;
            fxAccount.Trigger_Lead_Assignment_Rules__c = false;
            update fxAccount;
            
            //mergeConvert();
            BatchConvertLeadsScheduleable.executeInline();
            
            Test.stopTest();
            
            
            lead = [SELECT Id, Funnel_Stage__c, IsConverted, ConvertedOpportunityId FROM Lead WHERE Id = :lead.Id];
            
            System.assert(lead.IsConverted);
            
            Opportunity opp = [SELECT Id, IsClosed, StageName FROM Opportunity WHERE Id = :lead.ConvertedOpportunityId];
            
            System.assert(opp.IsClosed);
        }
    } */
    
    static testMethod void test8STAssigned() {
        System.runAs(SYSTEM_USER) {
            Lead lead = new Lead();
            lead.LastName = 'Schmoe';
            lead.Email = 'js@example.org';
            lead.Territory__c = 'North America';
            
            insert lead;
            
            
            fxAccount__c fxAccount = new fxAccount__c();
            fxAccount.Account_Email__c = 'js@example.org';
            fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            fxAccount.Registration_Source__c = '8st';
            fxAccount.Funnel_Stage__c = FunnelStatus.EST_PHONE_VALIDATED;
            
            insert fxAccount;
            
            
        //    mergeConvert();
            
            
            LeadUtil.useDefaultAssignment(lead);
            update lead;
            
            Test.startTest();
            
            mergeConvert();
            
        /*    lead = [SELECT Id, IsConverted, ConvertedOpportunityId, OwnerId FROM Lead WHERE Id = :lead.Id];
            
            System.assert(lead.IsConverted);
            System.assert(lead.OwnerId != SYSTEM_USER.Id);
            
            
            Opportunity opp = [SELECT Id, IsClosed FROM Opportunity WHERE Id = :lead.ConvertedOpportunityId];
            
            System.assert(!opp.IsClosed);
            
            fxAccount = [SELECT Id, Funnel_Stage__c, Lead__c FROM fxAccount__c WHERE Id =: fxAccount.Id];
            fxAccount.Funnel_Stage__c = FunnelStatus.EST_ID_VERIFIED;
            fxAccount.Total_Deposits_USD__c = 1;
            fxAccount.Traded__c = true;
            update fxAccount;
            
            opp = [SELECT Id, IsClosed FROM Opportunity WHERE Id = :lead.ConvertedOpportunityId];
            System.assert(opp.IsClosed);*/
            
            Test.stopTest();
        }
    }
    
    
    static testMethod void testMergeStandardLeadAndReassignLiveHouseAccount() {
        
        Lead existingLead;
        fxAccount__c tradeAccount;
        
        System.runAs(SYSTEM_USER) {
            existingLead = new Lead();
            existingLead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existingLead.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existingLead.FirstName = 'Jake';
            existingLead.LastName = 'Rake';
            existingLead.Email = 'jrake3@example.org';
            
            insert existingLead;
            
            tradeAccount = new fxAccount__c();
            tradeAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
            tradeAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            tradeAccount.Account_Email__c = existingLead.Email;
            
            insert tradeAccount;
            
            tradeAccount.Trigger_Lead_Assignment_Rules__c = false;
            update tradeAccount;
        }
        
        Lead newLead;
        System.runAs(FXS_USER) {
            newLead = new Lead();
            newLead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            newLead.FirstName = 'Jacob';
            newLead.LastName = 'Rake';
            newLead.Email = 'jrake3@example.org';
            
            insert newLead;
        }
        checkRecursive.SetOfIDs = new Set<Id>();
        
        Test.startTest();
        
        mergeConvert();
        
        Test.stopTest();
        
        // New lead merged and deleted
        try {
            newLead = [SELECT Id, OwnerId FROM Lead WHERE Id = :newLead.Id];
            System.assert(false);
        }
        catch (Queryexception e) {}

        existingLead = [SELECT Id, IsConverted, OwnerId FROM Lead WHERE Id = :existingLead.Id];
        System.assertEquals(true, existingLead.IsConverted);
        System.assertEquals(FXS_USER.Id, existingLead.OwnerId);
        
        tradeAccount = [SELECT Id, OwnerId, Lead__c FROM fxAccount__c WHERE Id = :tradeAccount.Id];
        System.assertEquals(FXS_USER.Id, tradeAccount.OwnerId);
    }
    
    
    static testMethod void testMerge8STLeadWithExistingHouseAccount() {
        TestsUtil.forceJobsSync = true;

        Lead existingLead, estLead;
        fxAccount__c tradeAccount, estAccount;
        
        System.runAs(SYSTEM_USER) {
            existingLead = new Lead();
            existingLead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            existingLead.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            existingLead.FirstName = 'Jake';
            existingLead.LastName = 'Rake';
            existingLead.Email = 'jrake3@example.org';
            
            insert existingLead;
            
            tradeAccount = new fxAccount__c();
            tradeAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
            tradeAccount.Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN;
            tradeAccount.Account_Email__c = existingLead.Email;
            
            insert tradeAccount;
            
            
            
            estLead = new Lead();
            estLead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            estLead.Funnel_Stage__c = FunnelStatus.EST_PHONE_VALIDATED;
            estLead.LastName = 'Rake';
            estLead.Email = 'jrake3@example.org';
            estLead.LeadSource = '8st';
            
            insert estLead;
            
            estAccount = new fxAccount__c();
            estAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
            estAccount.Funnel_Stage__c = FunnelStatus.EST_PHONE_VALIDATED;
            estAccount.Account_Email__c = existingLead.Email;
            estAccount.Registration_Source__c = '8st';
            
            insert estAccount;
            
            checkRecursive.SetOfIDs = new Set<Id>();

            mergeConvert();
        }
        
        // New lead merged and deleted
        try {
            existingLead = [SELECT Id, OwnerId FROM Lead WHERE Id = :existingLead.Id];
            System.assert(false);
        }
        catch (Queryexception e) {}
        
        estLead = [SELECT Id, LeadSource, Funnel_Stage__c FROM Lead WHERE Id = :estLead.Id];
        System.assertEquals('8st', estLead.LeadSource);
        System.assertEquals(FunnelStatus.EST_PHONE_VALIDATED, estLead.Funnel_Stage__c);
    }
    
    static testMethod void testRollupAccountTypeLead() {
        TestsUtil.forceJobsSync = true;

        Lead lead = new Lead();
        lead.LastName = 'Schmoe';
        lead.Email = 'jschmoe@example.org';
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
        insert lead;
        
        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
        fxAccount.Lead__c = lead.Id;
        
        insert fxAccount;
        
        lead = [SELECT Id, Has_Trade_Account__c, Has_Practice_Account__c FROM Lead WHERE Id =: lead.Id];
        
        System.assert(lead.Has_Practice_Account__c);
        System.assert(!lead.Has_Trade_Account__c);
        
        fxAccount__c fxAccount2 = new fxAccount__c();
        fxAccount2.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxAccount2.Lead__c = lead.Id;
        
        insert fxAccount2;
        
        lead = [SELECT Id, Has_Trade_Account__c, Has_Practice_Account__c FROM Lead WHERE Id =: lead.Id];
        
        System.assert(lead.Has_Practice_Account__c);
        System.assert(lead.Has_Trade_Account__c);
    }
    
    static testMethod void testRollupAccountTypeOpp() {
        TestsUtil.forceJobsSync = true;
        
        Lead lead = new Lead();
        lead.LastName = 'Schmoe';
        lead.Email = 'jschmoe@example.org';
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        insert lead;
        
        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxAccount.Funnel_Stage__c = FunnelStatus.AWAITING_CLIENT_INFO;
        fxAccount.Lead__c = lead.Id;
        
        insert fxAccount;
        
        fxAccount.Trigger_Lead_Assignment_Rules__c = false;
        update fxAccount;
        
        lead.OwnerId = FXS_USER.Id;
        update lead;
        
        Test.startTest();
        
        mergeConvert();
        
        Test.stopTest();
        
        lead = [SELECT Id, ConvertedOpportunityId FROM Lead WHERE Id =: lead.Id];
        
        Opportunity opp = [SELECT Id, Has_Trade_Account__c, Has_Practice_Account__c FROM Opportunity WHERE Id = :lead.ConvertedOpportunityId];
        
        System.assert(opp.Has_Trade_Account__c);
        System.assert(!opp.Has_Practice_Account__c);
        
        fxAccount__c practice = new fxAccount__c();
        practice.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
        practice.Account_Email__c = 'jschmoe@example.org';
        
        insert practice;
        
        opp = [SELECT Id, Has_Trade_Account__c, Has_Practice_Account__c FROM Opportunity WHERE Id = :lead.ConvertedOpportunityId];
        
        System.assert(opp.Has_Trade_Account__c);
        System.assert(opp.Has_Practice_Account__c);
    }
    
        
    private static void mergeConvert() {
        BatchMergeLeadsScheduleable.executeInline();
        BatchConvertLeadsScheduleable.executeInline();
    }
}