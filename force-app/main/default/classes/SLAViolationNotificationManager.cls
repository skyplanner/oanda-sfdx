/**
 * @File Name          : SLAViolationNotificationManager.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/19/2024, 2:19:19 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/16/2024, 5:19:03 PM   aniubo     Initial Version
**/
public interface SLAViolationNotificationManager {
	void sendNotifications(List<SLAViolationInfo> slaViolationInfos);
	SLAConst.SLAViolationType getSLAViolationType();
}