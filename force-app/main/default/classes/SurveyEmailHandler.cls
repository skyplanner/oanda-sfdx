/**
 * @File Name          : SurveyEmailHandler.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/4/2019, 4:03:49 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/26/2019   dmorales     Initial Version
**/
public with sharing class SurveyEmailHandler {
    public static final String HEADER = 'oanda_header_skp3_jpg';   
    public String caseID {get;set;}
    public Case caseObj {
                          set; 
                          get {
                                return getCase();
                              }
                        }
    public String headerURL{get;set;}

    public SurveyEmailHandler() {
      headerURL = getImgDocUrl(HEADER);    
    }

    public Case getCase(){
        List<Case> caseObj = [SELECT CaseNumber, 
                CreatedDate, 
                Customer_Name__c, 
                SurveyUrl__c, 
                Owner.Name,
                Owner.FirstName 
                FROM
                Case WHERE Id =: caseID];
        
         return caseObj.size()>0 ? caseObj[0]: null;        
    }

     public String getImgDocUrl(String docName) {
        String result = '';
        Document[] docList = [SELECT Id, LastModifiedDate FROM Document WHERE DeveloperName = :docName];
        if(!docList.isEmpty()) {
            return getImageUrl(docList[0].Id, docList[0].LastModifiedDate);
        }
        return result;     
    }

    public static String getImageUrl(String imageId, Datetime lastModifiedDate) {
        String result = System.URL.getSalesforceBaseUrl().toExternalForm() + 
            '/servlet/servlet.ImageServer?id='+ imageId + 
            '&oid=' + UserInfo.getOrganizationId() +
            '&lastMod=' + lastModifiedDate.getTime();
        return result;
    }
}