/**
 * Annual PIU processes. Takes care of the business logic
 * to run yearly realted to PIU (personal information update)
 * @author Fernando Gomez
 * @version 1.0
 * @see https://oandacorp.atlassian.net/browse/SP-10406
 */
public without sharing class CurrentDeskPIUAnnualProcessBatch
	implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection,
		Database.AllowsCallouts,
		Database.Stateful,
		Schedulable {
	private static final String DIVISION_SETTING =
		'CD_Annual_PIU_Process_Divisions';
	
	private static final String EXPIRATION_DAYS_SETTING =
		'CD_Annual_PIU_Process_Expiration_Days';

	private Id liveRecordTypeId;
	private Date oneYearBack;
	private List<String> divisions;
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	Map<Integer, Integer> mappings = CurrentDeskSync.getPreferenceMappings();
	private Set<Integer> mappingsKeySet = mappings.keySet();

	/**
	 * Main constructor
	 */
	public CurrentDeskPIUAnnualProcessBatch() {
		SPGeneralSettings settings = SPGeneralSettings.getInstance();
		Integer daysBack = 
			SPGeneralSettings.getInstance()
				.getValueAsInteger(EXPIRATION_DAYS_SETTING);

		liveRecordTypeId = RecordTypeUtil.getFxAccountLiveId();
		oneYearBack = System.today().addDays(-daysBack);

		divisions = settings.getValueAsList(DIVISION_SETTING);

		query = 'SELECT Id, CD_Account_Preference_ID__c' + ' FROM fxAccount__c WHERE ';
		// for all live fxAccounts...
		query += 'RecordTypeId = :liveRecordTypeId AND ';
		// we cannot do anything if integration with CD has not happened
		query += 'CD_Client_ID__c != NULL AND ';
		// in available divisions (just OGM so far) ...
		query += 'Division_Name__c = :divisions AND ';
		// with CD_Account_Preference_ID = 72, 73, 77, 78
		query += 'CD_Account_Preference_ID__c IN :mappingsKeySet AND ';
		// and funding limited = false
		query += 'Funding_Limited__c = false AND ';
		// and Info_Last_Updated_OGM__c is > 365 days 
		query += 'Info_Last_Updated_OGM__c < :oneYearBack AND ';
		// and Is_Closed__c = false
		query += 'Is_Closed__c = false';

		system.debug('query: ' + query);
		system.debug('divisions: ' + divisions);
		system.debug('PreferenceIds: ' + mappingsKeySet);
		system.debug('oneYearBack: ' + oneYearBack);
	}

	public CurrentDeskPIUAnnualProcessBatch(String q)
	{
		query = q;
		system.debug('query: ' + query);
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, CD_Account_Preference_ID__c FROM fxAccount__c  WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	/**
	 * @param be
	 */
	public Database.QueryLocator start(Database.BatchableContext be) {
		return Database.getQueryLocator(query);
	}

	/**
	 * @param be
	 * @param fxAccounts
	 */
	public void execute(
			Database.BatchableContext be,
			List<sObject> batch) {
		Integer currentPreferenceId;
		List<fxAccount__c> fxAccounts = (List<fxAccount__c>) batch;

		// the task here is to switch account preference based on a mapping.
		for (fxAccount__c fxAccount : fxAccounts) {
			currentPreferenceId =
				Integer.valueOf(fxAccount.CD_Account_Preference_ID__c);
			fxAccount.CD_Account_Preference_ID__c = mappings.get(currentPreferenceId);
		}

		// an update is in order, this will automatically send the value
		// to current desk portal using the existing integration
		update fxAccounts;
	}

	public void execute(SchedulableContext SC) {
		Database.executebatch(new CurrentDeskPIUAnnualProcessBatch(query), 1);
	}

	/**
	 * @param be
	 */
	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}