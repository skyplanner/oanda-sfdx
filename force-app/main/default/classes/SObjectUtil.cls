/**
 * SObject util class
 */
public with sharing class SObjectUtil {
   
    /**
	 * Get record ids if some fields changed
	 */
	public static Set<Id> getRecordIdsWhoFieldsChanged(
		List<SObject> objs,
		List<SObject> oldObjs,
		String field)
	{
		return getRecordIdsWhoFieldsChanged(
			objs,
			oldObjs,
			new List<String>{field});
	}

	/**
	 * Get record ids if some fields changed
	 */
	public static Set<Id> getRecordIdsWhoFieldsChanged(
		List<SObject> objs,
		List<SObject> oldObjs,
		List<String> fields)
	{
		SObject obj, oldObj;
		Set<Id> r = new Set<Id>();

		for (Integer i = 0; i < objs.size(); i++) {
			obj = objs.get(i);
			oldObj = oldObjs.get(i);
			if(isRecordFieldsChanged(obj, oldObj, fields)) {
				r.add(obj.Id);
			}
		}

		return r;
	}

	/**
	 * Know if some fields changed
	 */
	public static Boolean isRecordFieldsChanged(
		SObject obj,
		SObject oldObj,
		List<String> fields)
	{
		for(String f :fields) {
			if(isRecordFieldsChanged(
				obj, oldObj, f))
					return true;
		}

		return false;
	}

	/**
	 * Know if some fields changed
	 */
	public static Boolean isRecordFieldsChanged(
		SObject obj,
		SObject oldObj,
		String field)
	{
		if (oldObj == null && obj.get(field) != null)
            return true;

        return oldObj != null && 
			obj.get(field) != oldObj.get(field);
	}

    /**
     * Get ids from list of records and
     * specifying the field api name
     */
    public static Set<Id> getFieldIds(
        String field,
        List<SObject> rcds)
    {
        Id id;
        Set<Id> r = new Set<Id>();
        
        for(SObject rc : rcds) {
            id = (Id)rc.get(field);

            if(id != null)
                r.add(id);
        }
            
        return r;
    }

	public static SObject getSobjMapVal(
        Id k,
        Map<Id, SObject> m)
    {
        return
            m.containsKey(k) ? 
                m.get(k) : null;
    }

    /**
	 * Get ids from obj list
	 */
	public static Set<Id> getIds(List<SObject> objs) 
	{
		return (objs != null && !objs.isEmpty() ?  
			new Map<Id, SObject>(objs).keySet() :
			new set<Id>{});
	}

    /**
	 * Merge specific fields from source 
	 * list to map target of objects
	 */
	public static void mergeObjs(
		List<String> fields,
		Map<Id, SObject> targetMap, 
		List<SObject> sourceList)
	{
		for(SObject obj :sourceList) {
			Id oId = (Id)obj.get('Id');
			if(targetMap.containsKey(oId)) {
				mergeObjs(
					fields,
					targetMap.get(oId),
					obj);
			} else {
				targetMap.put(oId, obj);
			}
		}
	}

	/**
	 * Merge specific fields on two sObjects 
	 */
	public static void mergeObjs(
		List<String> fields,
		SObject target, 
		SObject source) 
	{
		for(String field :fields)
			target.put(field, source.get(field));
	}

	/**
     * Get object api name
     */
    public static String getObjName(Id xId) {
        return xId.getSObjectType().getDescribe().getName();
    }

}