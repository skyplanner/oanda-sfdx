global with sharing class BatchTriggerLeadAssignment implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public static final Integer BATCH_SIZE = 2;
	
	public static final String CRON_NAME = 'BatchTriggerLeadAssignment';
	public static final String CRON_SCHEDULE_1X25 = '0 25 * * * ?';

	private Integer delayInMin = 0;

    //This is a comma separated string without space
	//format exmaple: MON,TUE,WED,THU
	public String runOnDaysOfWeek;
	
	public static void schedule() {
		System.schedule(CRON_NAME + '_1X25', CRON_SCHEDULE_1X25, new BatchTriggerLeadAssignment());
	}
	
	public BatchTriggerLeadAssignment() {
		query = 'SELECT Id, Lead__c, Opportunity__c, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE RecordTypeId = \'' + RecordTypeUtil.getFxAccountLiveId() + '\' and Trigger_Lead_Assignment_Rules__c = true';
	}

	public BatchTriggerLeadAssignment(String q) {
		query = q;
	}

	public BatchTriggerLeadAssignment(Integer minToDelay) {
		delayInMin = minToDelay;

		DateTime cutoffTime = DateTime.now().addMinutes(minToDelay * -1);
		query = 'SELECT Id, Lead__c, Opportunity__c, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE RecordTypeId = \'' + RecordTypeUtil.getFxAccountLiveId() + '\' and CreatedDate < ' + SoqlUtil.getSoqlGMT(cutoffTime) + ' and Trigger_Lead_Assignment_Rules__c = true';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Lead__c, Opportunity__c, Trigger_Lead_Assignment_Rules__c FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<fxAccount__c> fxAccounts = (List<fxAccount__c>)batch;

		checkLeadAssignment(fxAccounts);
	}
	
	public void execute(SchedulableContext context) {

        if(shouldRun()){
			if(delayInMin > 0){
	           Database.executeBatch(new BatchTriggerLeadAssignment(delayInMin), BATCH_SIZE);
			}else{
			   executeBatch();
			}
	    }
		
	}

	public void finish(Database.BatchableContext bc) {
		try {
			BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
		}
		catch (Exception e) {}
	}
	
	public static Id executeBatch() {
		return Database.executeBatch(new BatchTriggerLeadAssignment(), BATCH_SIZE);
	}
	
	public static void executeInline() {
		List<fxAccount__c> fxAccounts = Database.query(new BatchTriggerLeadAssignment().query);
		checkLeadAssignment(fxAccounts);
	}
	
	private static void checkLeadAssignment(List<fxAccount__c> fxAccounts) {
		
		if (fxAccounts.size() == 0) {
			return;
		}
		
		System.debug('Checking if lead assignment should be triggered for ' + fxAccounts.size() + ' fxAccount(s)');
		
		Set<Id> leadIds = new Set<Id>();
		
		// Check each account to see if the lead should be triggered
		for(fxAccount__c fxAccount : fxAccounts) {
			if (fxAccount.Opportunity__c == null && fxAccount.Trigger_Lead_Assignment_Rules__c) {
				System.debug('The lead associated with fxAccount ' + fxAccount.Id + ' may need to be reassigned');
				leadIds.add(fxAccount.Lead__c);
			}
		}
		
		if (leadIds.size() == 0) {
			System.debug('No lead assignment should be triggered');
			return;
		}
		
		// First need to determine if merge has run yet, we need to query for all leads with the
		// emails of the leads under consideration
		List<Lead> potentialLeadsToTrigger = [SELECT Id, Email FROM Lead WHERE Id IN :leadIds];
		
		Set<String> emails = new Set<String>();
		for (Lead lead : potentialLeadsToTrigger) {
			emails.add(lead.Email);
		}
		
		//we only need to look at the non-converted leads. We don't care if there are duplicated converted leads
		potentialLeadsToTrigger = [SELECT Id, Email FROM Lead WHERE RecordTypeId = :RecordTypeUtil.getLeadMergeRecordTypeIds() and isConverted = false AND Email IN :emails];
		
		Map<String, List<Id>> potentialLeadsIdsByEmail = new Map<String, List<Id>>();
		
		// Map the lead ids by email
		for (Lead lead : potentialLeadsToTrigger) {
			List<Id> potentialLeadIds = potentialLeadsIdsByEmail.get(lead.Email);
			if (potentialLeadIds == null) {
				potentialLeadIds = new List<Id>();
				potentialLeadsIdsByEmail.put(lead.Email, potentialLeadIds);
			}
			
			potentialLeadIds.add(lead.Id);
		}
		
		// Determine which lead ids are duplicates
		Set<Id> duplicateLeads = new Set<Id>();
		for (String email : potentialLeadsIdsByEmail.keySet()) {
			List<Id> potentialLeadIds = potentialLeadsIdsByEmail.get(email);
			
			if (potentialLeadIds.size() > 1) {
				System.debug('XXX Duplicate email: ' + email + ' has ' + potentialLeadIds.size() + ' leads');
				duplicateLeads.addAll(potentialLeadIds);
			}
		}
		
		List<fxAccount__c> fxAccountsToUpdate = new List<fxAccount__c>();
		// Filter those leads out of the update process
		for (fxAccount__c fxAccount : fxAccounts) {
			// Lead is not ready to be assigned until merge is done
			if (duplicateLeads.contains(fxAccount.Lead__c)) {
				System.debug('XXX Removing lead ' + fxAccount.Lead__c + ' from consideration');
				leadIds.remove(fxAccount.Lead__c);
			}
			// The leads has merged and is ready to be reassigned, set the flag to false on the fxAccount
			else {
				System.debug('XXX Lead ' + fxAccount.Lead__c + ' will be reassigned');
				fxAccount.Trigger_Lead_Assignment_Rules__c = false;
				fxAccountsToUpdate.add(fxAccount);
			}
		}
		
		// Set the flag to false on the selected fxAccounts
		update fxAccountsToUpdate;
		
		// Finally select the actual leads to trigger
		List<Lead> leadsToTrigger = [SELECT Id, Country FROM Lead WHERE Id IN :leadIds AND IsConverted = false AND (OwnerId=:UserUtil.getSystemUserId() OR OwnerId=:UserUtil.getUserIdByName(UserUtil.NAME_MARKETO_USER))];
		System.debug('XXX Leads to trigger: ' + leadsToTrigger);
		
		Map<Id, Id> ownerIdByLeadId = LeadUtil.getOriginalOwnerIds(leadsToTrigger);
		
		// Update each lead to use lead assignment
		for(Lead lead : leadsToTrigger) {
			
			Id originalOwner = ownerIdByLeadId.get(lead.Id);

			// Lead had no original owner, assign it using default rules
			// SP-10291 lead is from Singapore, assign it using default rules
			if (originalOwner == null || lead.Country == 'Singapore') {
				System.debug('Using default assignment rules for lead ' + lead.Id);
				Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.assignmentRuleHeader.useDefaultRule = true;
				lead.setOptions(dmo);
			}
			else {
				lead.OwnerId = originalOwner;
			}
		}

		if(leadsToTrigger.size()>0) {		
			update leadsToTrigger;
		}
	}

	public boolean shouldRun(){
		boolean result = false;

        Datetime dt = System.now();

        String curDayOfWeek = dt.format('EEE').toUpperCase();

        System.debug('Today is ' + curDayOfWeek + ', Days of Week allowed: ' + runOnDaysOfWeek);
        if(runOnDaysOfWeek != null){
        	List<String> days = runOnDaysOfWeek.toUpperCase().split(',');

        	for(String day : days){
        		if(day == curDayOfWeek){
        			result = true;
                    break;
        		}
        	}
        }else{
        	//if curDayOfWeek is not defined, then always run
        	result = true;
        }


        return result;

	}
}