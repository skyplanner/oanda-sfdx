public with sharing class PersonalInfoUpdateEventTriggerHandler {
    
    Map<String,Map<String,Endpoints_Mappings__mdt>> endpointMappings;
    List<List<Questionnaire__c>> individualPiuEventsQuestionnaires = new List<List<Questionnaire__c>>();

    private final static String ENDPOINT_NAME = 'piu_cka_update';
    private final static String LOGGER_CATEGORY = 'piuCkaUpdate';
    private final static String LOGGER_CATEGORY_URL = '@RestResource(/api/v1/user/*/piu_cka_update)';

    public void handlePiuInsert(List<Piu_Cka_Update__e> piusEventsToProcess) {
        try {
            EndpointsMappingsUtil emu = new EndpointsMappingsUtil();
            endpointMappings = emu.getEnpointsMappingByEndpoint(ENDPOINT_NAME);
            Map<String, Endpoints_Mappings__mdt> currentObjMapping = endpointMappings.get('Personal_Information_Update__c');

            List<Personal_Information_Update__c> piusToCreate = new List<Personal_Information_Update__c>();

            for (Piu_Cka_Update__e event : piusEventsToProcess) {
                String fxAccountId = event.fxTrade_User_Id__c;
                String jsonBody = event.User_Payload__c;

                Map<String, Object> payload = (Map<String, Object>)JSON.deserializeUntyped(jsonBody);
                Personal_Information_Update__c piu = new Personal_Information_Update__c(fxAccount__c = fxAccountId);
                
                for(String inputField : payload.keySet()) {
                    if(!currentObjMapping.containsKey(inputField)){
                        continue;
                    }

                    setInputFieldtoSobject(
                        piu, 
                        inputField, 
                        payload.get(inputField),
                        currentObjMapping
                    );
                }
                piusToCreate.add(piu);
            }
            List<Database.SaveResult> sr = Database.insert(piusToCreate, false);
            List<Questionnaire__c> questionnairesToInsert = new List<Questionnaire__c>();

            for(Integer i = 0; i < sr.size(); i++) {
                if(!sr[i].isSuccess()){
                    Logger.error(ENDPOINT_NAME, 'Error Inserting PIU, ' + sr[i].getErrors());
                    continue;
                }
                List<Questionnaire__c> questionnairesForSinglePiu = individualPiuEventsQuestionnaires.get(i);
                for(Questionnaire__c q : questionnairesForSinglePiu) {
                    q.Personal_Information_Update__c = sr[i].getId();
                }
                questionnairesToInsert.addAll(questionnairesForSinglePiu);
            }

            insert questionnairesToInsert;

        } catch (Exception ex) { 
            Logger.error(ENDPOINT_NAME, 'Error processing: '+ ENDPOINT_NAME, ' input ' + piusEventsToProcess + ' ' + ex.getMessage() + ' ' + ex.getStackTraceString());
            return;
        }
    }

    private void setInputFieldtoSobject(SObject currentSobject, String inputField, Object fieldValue,  Map<String, Endpoints_Mappings__mdt> mapping){
        String fieldName;
        String dataType;

        try {
            fieldName = mapping.get(inputField)?.Sobject_Field_Name__c;
            dataType = mapping.get(inputField)?.Data_Type__c;
            switch on dataType {
                when 'Integer' {
                    currentSobject.put(fieldName, Integer.valueOf(fieldValue));
                }
                when 'QUESTIONS' {
                    List<Questionnaire__c> questionnaires = new List<Questionnaire__c>();
                    for(Object question : (List<Object>) fieldValue) {
                        
                        Map<String, Object> questionMap = (Map<String, Object>) question;

                        Questionnaire__c questionnaire = new Questionnaire__c();
    
                        for(String payloadFieldName : questionMap.keySet()) {

                            Object value = questionMap.get(payloadFieldName);

                            setInputFieldtoSobject(questionnaire, payloadFieldName, value, mapping);
                        }
                        questionnaires.add(questionnaire);

                    }
                    individualPiuEventsQuestionnaires.add(questionnaires);
                }
                when 'String' {
                    currentSobject.put(fieldName, String.valueOf(fieldValue));
                }
                when else {
                    currentSobject.put(fieldName, fieldValue);
                }
            }
        } catch (Exception ex) {
            Logger.error(
                LOGGER_CATEGORY_URL,
                LOGGER_CATEGORY,
                ' The following field name ' + fieldName 
                + ' field value:  ' + fieldValue + ' for object: ' + currentSobject 
                + ' was not mapped. Err msg: ' + ex.getMessage() 
                + ' Update performed by ' + UserInfo.getName());
        }
    }
}