/**
 * @File Name          : ChatClientInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/17/2022, 10:44:19 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/11/2021, 4:41:14 PM   acantero     Initial Version
**/
public without sharing class ChatClientInfo {

    public Boolean isHVC {get; set;}
    public String division {get; set;}
    public String divisionName {get; set;}
    public Boolean isAnAccount {get; set;}
    public Boolean isCryptoValid {get; set;}
    public String fxAccount {
        get; 
        set {
            fxAccount = String.isNotBlank(value)
                ? value
                : '';
        }
    }
    public String ldRecordTypeId {
        get; 
        set {
            ldRecordTypeId = String.isNotBlank(value)
                ? value
                : '';
        }
    }

    public static ChatClientInfo getAccountInfo(
        Boolean isHVC, 
        String division,
        String fxAccount
    ) {
        ChatClientInfo info = new ChatClientInfo();
        info.isAnAccount = true;
        info.isHVC = isHVC;
        info.isCryptoValid = false;
        info.division = division;
        info.fxAccount = fxAccount;
        return info;
    }

    public static ChatClientInfo getLeadInfo(
        String fxAccount,
        String ldRecordTypeId
    ) {
        ChatClientInfo info = new ChatClientInfo();
        info.isAnAccount = false;
        info.isHVC = false;
        info.isCryptoValid = false;
        info.fxAccount = fxAccount;
        info.ldRecordTypeId = ldRecordTypeId;
        return info;
    }

}