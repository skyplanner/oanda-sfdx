/**
 * @File Name          : OmnichanelSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 5:29:21 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   acantero     Initial Version
**/
public without sharing class OmnichanelSettings {

    public static final String DEFAULT_SETTINGS = 'Default_Settings';
    public static final Integer MAX_PUSH_ATTEMPTS_DEFAULT = 0;
    public static final Integer MAX_NON_HVC_CASES_DEFAULT = 1;
    public static final Integer MIN_PERCENTAGE_NON_HVC_DEFAULT = 100;

    public static final String LANGUAGE_SKILLS_MODEL = 'Language Skills';

    @testVisible
    private static OmnichanelSettings activeSettings;
    
    //deprecated
    public Boolean initializationFails {get; private set;}

    @testVisible
    public Integer limitAvgWaitTime {get; private set;}
    @testVisible
    public List<String> casesQueueDevNameList {get; private set;}
    @testVisible
    public List<String> supervisorRoleDevNameList {get; private set;}
    @testVisible
    public String chatChannelDevName {get; private set;}
    @testVisible
    public Integer pushTimeout {get; private set;}
    @testVisible
    public Integer maxPushAttempts {get; private set;}
    @testVisible
    public String defaultChatButtonId {get; private set;}
    @testVisible
    public Integer dropAdditionalSkillsTimeOut {get; private set;}
    @testVisible
    public String primarySkillsModel {get; private set;}
    @testVisible
    public String postChatPage {get; private set;}
    @testVisible
    public Integer caseChatWrapUpTime {get; private set;}
    @testVisible
    public List<String> caseOwnerReroutingList {get; private set;}
    @testVisible
    public List<String> noHvcParkPresentStatuses{get; set;}

    @testVisible
    OmnichanelSettings() {
        initializationFails = false;
    }

    public static OmnichanelSettings getRequiredDefault() {
        if (activeSettings == null) {
            activeSettings = loadActiveRecord();
        }
        return activeSettings;
    }

    public static Boolean existsDefaultChatButtonId() {
        String defaultChatButtonId = getDefaultChatButtonId();
        Boolean result = String.isNotBlank(defaultChatButtonId);
        return result;
    }

    public static String getDefaultChatButtonId() {
        if (activeSettings != null) {
            return activeSettings.defaultChatButtonId;
        }
        //else...
        List<Omnichanel_Settings__mdt> settingsList = [
            SELECT 
                Default_Chat_Button_Id__c
            FROM
                Omnichanel_Settings__mdt
            WHERE
                Active__c = true
        ];
        checkRequiredRecord(settingsList);
        String result = settingsList[0].Default_Chat_Button_Id__c;
        return result;
    }

    public Boolean dropAdditionalSkillsAreEnabled {
        get {
            return
                (
                    (dropAdditionalSkillsTimeOut != null) &&
                    (dropAdditionalSkillsTimeOut > 0) &&
                    String.isNotBlank(primarySkillsModel)
                );
        }
    }

    public static void checkRequiredRecord(List<Omnichanel_Settings__mdt> settingsList) {
        if (settingsList.isEmpty()) {
            throw new OmnichanelRoutingException('Omnichanel Settings not found');
        }
    }

    static OmnichanelSettings loadActiveRecord() {
        OmnichanelSettings result = null;
        List<Omnichanel_Settings__mdt> settingsList = [
            SELECT 
                Case_Chat_Wrap_Up_Time__c,
                Case_Owner_involving_rerouting__c,
                Cases_Queue_Developer_Name__c,
                Chat_Serv_Channel_Dev_Name__c,
                Default_Chat_Button_Id__c,
                Drop_Additional_Skills_Time_Out__c,
                Limit_avg_wait_time__c,
                Max_push_attempts__c,
                Post_Chat_Page__c,
                Primary_Skills_Model__c,
                Push_Time_Out__c,
                Supervisor_Role_Developer_Name__c,
                No_Parking_Available_Statuses__c
            FROM
                Omnichanel_Settings__mdt
            WHERE
                Active__c = true
        ];
        checkRequiredRecord(settingsList);
        //...
        result = new OmnichanelSettings();
        Omnichanel_Settings__mdt settings = settingsList[0];
        result.limitAvgWaitTime = Integer.valueOf(settings.Limit_avg_wait_time__c);
        result.chatChannelDevName = settings.Chat_Serv_Channel_Dev_Name__c;
        result.pushTimeout = getValidPositiveNumber(settings.Push_Time_Out__c);
        result.maxPushAttempts = MAX_PUSH_ATTEMPTS_DEFAULT;
        if (settings.Max_push_attempts__c != null) {
            result.maxPushAttempts = getValidPositiveNumber(settings.Max_push_attempts__c);
        }
        if (String.isNotBlank(settings.Cases_Queue_Developer_Name__c)) {
            result.casesQueueDevNameList = 
                settings.Cases_Queue_Developer_Name__c.split(',');
        }
        if (String.isNotBlank(settings.Supervisor_Role_Developer_Name__c)) {
            result.supervisorRoleDevNameList = 
                settings.Supervisor_Role_Developer_Name__c.split(',');
        } 
        result.defaultChatButtonId = settings.Default_Chat_Button_Id__c; 
        result.dropAdditionalSkillsTimeOut = getValidPositiveNumber(
            settings.Drop_Additional_Skills_Time_Out__c
        ); 
        result.primarySkillsModel = settings.Primary_Skills_Model__c;
        result.postChatPage = settings.Post_Chat_Page__c;
        result.caseChatWrapUpTime = getValidPositiveNumber(
            settings.Case_Chat_Wrap_Up_Time__c
        ); 
        if (String.isNotBlank(settings.Case_Owner_involving_rerouting__c)) {
            result.caseOwnerReroutingList = 
                settings.Case_Owner_involving_rerouting__c.split(',');
        }
        if (String.isNotBlank(settings.No_Parking_Available_Statuses__c)) {
            result.noHvcParkPresentStatuses = 
                settings.No_Parking_Available_Statuses__c.split(',');
        }
        //...
        return result;
    }

    @testVisible
    static Integer getValidPositiveNumber(Object val) {
        Integer result = (val != null)
            ? Integer.valueOf(val)
            : 0;
        if (result < 0) {
            result = 0;
        }
        return result;
    }
    
}