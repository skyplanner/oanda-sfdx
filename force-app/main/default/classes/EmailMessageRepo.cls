/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public inherited sharing class EmailMessageRepo {
	public static String EMAIL_MESSAGE_STATUS_DRAFT = '5';
	public static String EMAIL_MESSAGE_STATUS_SENT = '3';
	public static EmailMessage getDraftByParentId(Id parentId) {
		List<EmailMessage> messages = [
			SELECT Id, ParentId
			FROM EmailMessage
			WHERE ParentId = :parentId AND Status = :EMAIL_MESSAGE_STATUS_DRAFT AND CreatedById = :UserInfo.getUserId()
		];
		return messages.isEmpty() ? null : messages.get(0);
	}
}