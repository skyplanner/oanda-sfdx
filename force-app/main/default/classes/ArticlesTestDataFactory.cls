/**
 * @File Name          : ArticlesTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/23/2019, 1:26:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   acantero     Initial Version
**/
@isTest
public class ArticlesTestDataFactory {

	public static String FOLDER_NAME = 'HelpPortal';

	public static FAQ__kav createSampleArt(String artTitle, String artUrlName) {
		FAQ__kav newArt = new FAQ__kav(
			Title = artTitle,
			UrlName = artUrlName
		);
		insert newArt;
		FAQ__kav result = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id = :newArt.Id];
		return result;
	}
	
	//Create 4 articles (Online), only 3 of them has Docs
	public static void createArticlesWithDummyDocs() {
		createArtsWithDummyDocs(true);
	}

	public static List<FAQ__kav> createArtsWithDummyDocs(Boolean publish) {
		ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_NAME LIMIT 1][0].Id;
		List<Document> docList = new List<Document>();
		docList.add(
	    	new Document(
				Name = 'doc1.json',
				ContentType = 'application/json',
				DeveloperName = 'doc1',
				FolderId = folderId
			)
		);
		docList.add(
	    	new Document(
				Name = 'doc2.json',
				ContentType = 'application/json',
				DeveloperName = 'doc2',
				FolderId = folderId
			)
		);
		docList.add(
	    	new Document(
				Name = 'doc3.json',
				ContentType = 'application/json',
				DeveloperName = 'doc3',
				FolderId = folderId
			)
		);
		insert docList;
		//...
	    List<FAQ__kav> articles = new List<FAQ__kav>();
	    articles.add(
	    	new FAQ__kav(
				Title = 'Test Article 1',
				UrlName = 'Test-article-1',
				PreviewDocument__c = docList[0].Id
			)
		);
		articles.add(
	    	new FAQ__kav(
				Title = 'Test Article 2',
				UrlName = 'Test-article-2',
				PreviewDocument__c = docList[1].Id
			)
		);
		articles.add(
	    	new FAQ__kav(
				Title = 'Test Article 3',
				UrlName = 'Test-article-3',
				PreviewDocument__c = docList[2].Id
			)
		);
		articles.add(
	    	new FAQ__kav(
				Title = 'Test Article 4',
				UrlName = 'Test-article-4'
			)
		);
		insert articles;
		//...
		Set<Id> ids = new Set<Id>();
		for (FAQ__kav art : articles) {
			ids.add(art.Id);
		}
		List<FAQ__kav> result = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE Id IN : ids];
		if (publish) {
			for (FAQ__kav art : result) {
				KbManagement.PublishingService.publishArticle(art.KnowledgeArticleId, false);
			}
		}
		return result;
	}

	public static void createTopArticles(String division, String dataCatName1, String dataCatName2, String dataCatName3) {
		List<FAQ__kav> createdArtList = createArtsWithDummyDocs(true);
		List<Category_Top_Article__c> topArtList = new List<Category_Top_Article__c>();
		Integer cat1Order = 0;
		Integer cat2Order = 0;
		Integer cat3Order = 0;
		Integer count = 0; 
		for(FAQ__kav art : createdArtList) {
			count++;
			topArtList.add(
				new Category_Top_Article__c(
					DivisionName__c = division,
					DataCategoryName__c = dataCatName1,
					KnoledgeArticleId__c = art.KnowledgeArticleId,
					Order__c = ++cat1Order
				)
			);
			if (count >= 2) {
				topArtList.add(
					new Category_Top_Article__c(
						DivisionName__c = division,
						DataCategoryName__c = dataCatName2,
						KnoledgeArticleId__c = art.KnowledgeArticleId,
						Order__c = ++cat2Order
					)
				);
			}
			if (count >= 3) {
				topArtList.add(
					new Category_Top_Article__c(
						DivisionName__c = division,
						DataCategoryName__c = dataCatName3,
						KnoledgeArticleId__c = art.KnowledgeArticleId,
						Order__c = ++cat3Order
					)
				);
			}
		}
		insert topArtList;
	}

	public static List<Document> createOrphanDocs(Integer count) {
		ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_NAME LIMIT 1][0].Id;
		List<Document> docList = new List<Document>();
		for(Integer i = 1; i <= count; i++) {
			docList.add(
		    	new Document(
					Name = 'orphanDoc' + i + '.json',
					ContentType = 'application/json',
					DeveloperName = 'orphanDoc' + i,
					FolderId = folderId
				)
			);
		}
		insert docList;
		return docList;
	}

	public static Document createOrphanDocWithBody(String nameSufix, String body) {
		ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_NAME LIMIT 1][0].Id;
		Document doc = new Document(
			Name = 'orphanDoc' + nameSufix + '.json',
			ContentType = 'application/json',
			DeveloperName = 'orphanDoc' + nameSufix,
			FolderId = folderId
		);
		if (String.isNotBlank(body)) {
			doc.Body = Blob.valueOf(body);
		}
		insert doc;
		return doc;
	}


}