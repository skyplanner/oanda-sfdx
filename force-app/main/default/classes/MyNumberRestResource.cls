@RestResource(urlMapping='/api/v1/my_number/*')
global with sharing class MyNumberRestResource {
	
	public static final List<String> PARAMS = new List<String>{'user_id','oj_id'};

	@HttpGet
    global static void doGet() {
		
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, PARAMS);

        try {
			Boolean isOj = context.getPathParam('oj_id') != null;

			String userId = 
				isOj 
				? context.getPathParam('oj_id') 
				: context.getPathParam('user_id');

			List<My_Number__c> myNum = 
				isOj
				? [SELECT Id,OJ_Client_Id__c, My_Number__c FROM My_Number__c WHERE OJ_Client_Id__c = :userId LIMIT 1]
				: [SELECT Id,fxTrade_User_Id__c, My_Number__c FROM My_Number__c WHERE fxTrade_User_Id__c = :userId LIMIT 1];
			
			if(myNum.isEmpty()) {
				String respBody = 'Could not find user with given Id';
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 404;
                return;
			}

            res.addHeader('Content-Type', 'application/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serialize(myNum[0].getPopulatedFieldsAsMap()));
			return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

	@HttpPost
    global static void doPost() {

		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;

		RestResourceHelper context = new RestResourceHelper(req, res);

        try {

			Map<String,Object> payload = (Map<String,Object>) JSON.deserializeUntyped(context.getBodyAsString());

			My_Number__c myNum = new My_Number__c();

			for(String key : payload.keySet()) {
				myNum.put(key, String.valueOf(payload.get(key)));
			}

			insert myNum;

            res.addHeader('Content-Type', 'application/json');
			res.responseBody = Blob.valueOf(JSON.serialize(myNum.getPopulatedFieldsAsMap()));
			res.statusCode = 201;
			return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }

	@HttpDelete
    global static void doDelete() {

		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;

		RestResourceHelper context = new RestResourceHelper(req, res, PARAMS);

        try {

			String userId = context.getPathParam('user_id');
			
			List<My_Number__c> myNum = [SELECT Id, My_Number__c FROM My_Number__c WHERE fxTrade_User_Id__c = :userId LIMIT 1];
			
			if(myNum.isEmpty()) {
				String respBody = 'Could not find user with given Id';
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 404;
                return;
			}

			delete myNum;
			res.statusCode = 200;
			return;

        } catch(Exception ex) {
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(respBody);
            Logger.error(
                req.resourcePath,
                req.requestURI,
                res.statusCode + ' ' + respBody);
            return;
        }
    }
}