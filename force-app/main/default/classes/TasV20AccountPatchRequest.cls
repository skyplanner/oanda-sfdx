/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-09-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasV20AccountPatchRequest {
    public Decimal accountPositionValueLimit { get; set; }

    public String alias { get; set; }

    public Integer commissionGroupID { get; set; }
    
    public Boolean configurationLocked { get; set; }
    
    public Boolean depositLocked { get; set; }
    
    public Boolean hedgingEnabled { get; set; }
    
    public Boolean locked { get; set; }
    
    public Decimal marginRate { get; set; }

    public Integer mt4ServerID { get; set; }
    
    public Boolean newPositionsLocked { get; set; }
    
    public Boolean orderCancelLocked { get; set; }
    
    public Boolean orderCreationLocked { get; set; }
    
    public Boolean orderFillLocked { get; set; }
    
    public Integer positionAggregationModeOverride { get; set; }
    
    public List<Integer> pricingGroupIDs { get; set; }

    public Integer tradingGroupID { get; set; }

    public Boolean withdrawalLocked { get; set; }
}