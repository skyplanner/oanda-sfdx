/**
 * @description       : Wrapper class for MT5 Account Details follow framework and design proposed by Dianelys Velazquez
 * @author            : Mikolaj Juras
 * @group             : 
 * @last modified on  : 30 Jun 2023
 * @last modified by  : Mikolaj Juras
**/
public without sharing class TasMT5AccsDetails {
    @AuraEnabled
    public String mt5UserID {get; set;}

    @AuraEnabled
    public Boolean isDisabled {get; set;}
    
    @AuraEnabled
    public String balance {get; set;}

    @AuraEnabled
    public String accountCurrency {get; set;}

    @AuraEnabled
    public String accountType {get; set;}

    @AuraEnabled
    public String sharesEnabled {get; set;}

    @AuraEnabled
    public String book {get; set;}

    public Details details {get; set;}

    public class Details {
        public String Balance {get; set;}
        public Properties properties {get; set;}
    }

    public class Properties {
        public String mt5_group {get; set;}
    }

}