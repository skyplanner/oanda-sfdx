/**
* Created by Shantanu(Neuraflash LLC) on 22/01/19.
* Implementation : Chatbot
* Summary : Returns Chat Button Id and Case Queue(record owner)
* Details : Uses Case Creation Matrix to determine the return params 
*/
global without sharing class NF_GetCaseCreationMatrix implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String, Object> paramMap, String aiServiceResponse, String aiConfig) {
		Map<String, String> eventParams = new Map<String, String>();

		try{
			Map<String, Object> mapSession = (Map<String, Object>) JSON.deserializeUntyped(aiServiceResponse);
			String chatSessionId = (String)mapSession.get('sessionId'); 
			String customerEmail = (String)paramMap.get('email');
            String customerAccType = (String)paramMap.get('accountType');

			//Create Matrix Params to be validated for routing
			ChatSessionMatrixParams params = new ChatSessionMatrixParams();
			//Get chat log record with all details required for determining routing config
			List<nfchat__Chat_Log__c> chatLogs = [SELECT Id, nfchat__Session_Id__c, nfchat_Account_Type__c, Language_Preference__c,
													    (SELECT Id, nfchat__Intent_Name__c FROM nfchat__Chat_Log_Details__r ORDER BY Name)
												  FROM nfchat__Chat_Log__c WHERE nfchat__Session_Id__c =: chatSessionId
												  ORDER BY CreatedDate DESC LIMIT 1];

			System.debug('>>NF_GetCaseCreationMatrix: chatLogs='+chatLogs);

			if(!chatLogs.isEmpty()){
				params.accountOrLead = checkExistingCustomer(customerEmail);
                params.demoInterest = checkDemoInterest(customerAccType);
				params.hvc = checkHVC(customerEmail);
				params.intent = getIntentName(chatLogs[0].nfchat__Chat_Log_Details__r);
				params.language = chatLogs[0].Language_Preference__c;
				System.debug('>>NF_GetCaseCreationMatrix: params='+params);

				OutputParams outParams = getRoutingConfig(params);
				System.debug('>>NF_GetCaseCreationMatrix: outParams='+outParams);

				eventParams.put('chatButtonId', outParams.chatButtonId);
				eventParams.put('caseQueue', outParams.caseQueueName);
				eventParams.put('email',customerEmail);
			}
		}
		catch(Exception ex){
			System.debug('>> Exception in NF_GetCaseCreationMatrix > processRequest : ' + ex.getMessage());
			return null;
		}
		return JSON.serialize(eventParams);
    }
 /**
   * @description	-	Determines routing config to be used
   * @return		-	OutputParams
   */
	global OutputParams getRoutingConfig(ChatSessionMatrixParams params){
		OutputParams outParams = new OutputParams();
		//Check for all params in the routing config
		Boolean hvcValid;
		Boolean accountOrLeadValid;
		Boolean demoInterestValid;
		Boolean intentValid;

		//Get defined roiuting configurations
		Map<String, Case_Creation_Matrix__mdt> configMap = getCaseMatrixRecords();
		//Set fallback/default values to return
		outParams.caseQueueName = configMap.get('Fallback').Case_Queue_Name__c;
		//outParams.chatButtonId = configMap.get('Fallback').Chat_Button_Id__c;

		Chat_button_by_skill__mdt chatButtonMetadata = getChatButtonMetadataBySkill(params.language);
		if(chatButtonMetadata != null && chatButtonMetadata.Button_Id__c != null){
			outParams.chatButtonId = chatButtonMetadata.Button_Id__c;
			System.debug('NF_GetCaseCreationMatrix.getRoutingConfig set outParams.chatButtonId='+outParams.chatButtonId);
		}

		//Iterate through the configs to get the matched config
		for(Case_Creation_Matrix__mdt config : configMap.values()){
			hvcValid = false;
			accountOrLeadValid = false;
			demoInterestValid = false;
			intentValid = false;

			//Skip Fallback configuration
			if(config.DeveloperName != 'Fallback'){
				hvcValid = validateParam(config.HVC__c, params.hvc);
				if(!hvcValid)
					continue;
				accountOrLeadValid = validateParam(config.Account_or_Lead__c, params.accountOrLead);
				if(!accountOrLeadValid)
					continue;
				demoInterestValid = validateParam(config.Demo_Interest__c, params.demoInterest);
				if(!demoInterestValid)
					continue;
				intentValid = validateIntentParam(config.Intent__c, params.intent);

				System.debug('NF_GetCaseCreationMatrix.params: ' + params);
				System.debug('NF_GetCaseCreationMatrix.getRoutingConfig validation:\n' +
							 'Config: ' + config.DeveloperName + '\n' +
							 'hvcValid: ' + hvcValid + '\n' + 
							 'accountOrleadValid: ' + accountOrLeadValid + '\n' + 
							 'demoInterestValid: ' + demoInterestValid + '\n' +
							 'intentValid: ' + intentValid);

				//Check if all validations passed
				if(hvcValid && accountOrLeadValid && demoInterestValid && intentValid){
					outParams.caseQueueName = config.Case_Queue_Name__c;
					//outParams.chatButtonId = config.Chat_Button_Id__c;
					return outParams;
				}
			}
		}
		return outParams;
	}

 /**
   * @description	-	Validates the param
   * @return		-	Boolean
   */
	global Boolean validateParam(String configValue, String paramValue){
		if(!checkIfParamIsRequired(configValue)){
			return true;
		}
		else{
			return configValue.toLowerCase() == paramValue.toLowerCase();
		}
	}

 /**
   * @description	-	Determines if param is required for the config
   * @return		-	Boolean
   */
	global Boolean checkIfParamIsRequired(String configValue){
		if(configValue == null || configValue == 'N/A'){
			return false;
		}
		else{
			return true;
		}
	}

 /**
   * @description	-	Validates the param
   * @return		-	Boolean
   */
	global Boolean validateIntentParam(String configValue, String paramValue){
		if(!checkIfParamIsRequired(configValue)){
			return true;
		}
		else{
			//return configValue.toLowerCase().contains(paramValue.toLowerCase());//business.account.create.yes.live
			Boolean isIntentValid = false;
			List<String> intentList = configValue.toLowerCase().split(',');
			for(String intentName : intentList){
				if(paramValue.toLowerCase().contains(intentName)){
					isIntentValid = true;
					break;
				}
			}
			return isIntentValid;
		}
	}

 /**
   * @description	-	Determines if the Customer is Existing Account or Lead or none
   * @return		-	String - Account/Lead/Not Found
   */
	global String checkExistingCustomer(String customerEmail){
		if(String.isNotBlank(customerEmail)){
			//check for Account 
			List<Account> customerAccount = [SELECT Id, Date_Customer_Became_Core__c
											 FROM Account WHERE PersonEmail =: customerEmail
											 ORDER BY CreatedDate DESC LIMIT 1];
			if(!customerAccount.isEmpty()){
				return 'Account';
			}
			//check for Lead
			else{
				//check for Account 
				List<Lead> listLead = [SELECT Id 
										FROM Lead
										WHERE Email =: customerEmail
										ORDER BY CreatedDate DESC LIMIT 1];
				if(listLead.isEmpty()){
					return 'Not Found';
				}
				else{
					return 'Lead';
				}
			}
		}
		return 'Not Found';
	}

 /**
   * @description	-	Determines if the customer is HVC
   * @return		-	String(Yes/No)
   */
	global String checkHVC(String customerEmail){
		if(String.isNotBlank(customerEmail)){
			List<Account> customerAccount = [SELECT Id, Is_High_Value_Customer__c
											 FROM Account
											 WHERE PersonEmail =: customerEmail
											 ORDER BY CreatedDate DESC LIMIT 1];
			if(!customerAccount.isEmpty()){
				return customerAccount[0].Is_High_Value_Customer__c ? 'Yes' : 'No';
			}
		}

		return 'N/A';
	}

 /**
   * @description	-	Determines demo interest
   * @return		-	String(Yes/No)
   */
	global String checkDemoInterest(String accountType){
		if(String.isNotBlank(accountType)){
			return (accountType.toLowerCase() == 'demo' || accountType.toLowerCase() == 'practice') ? 'Yes' : 'No';
		}
		else{
			return 'No';
		}
	}

 /**
   * @description	-	Determines the latest business intent name 
   * @return		-	String intent name
   */
	global String getIntentName(List<nfchat__Chat_Log_Detail__c> chatLogDetails){
		String intentName = '';
		for(Integer i = chatLogDetails.size() - 1; i >= 0; i--){
			if(String.isNotEmpty(chatLogDetails[i].nfchat__Intent_Name__c) && chatLogDetails[i].nfchat__Intent_Name__c.indexOf('business.') == 0){
				intentName = chatLogDetails[i].nfchat__Intent_Name__c;
				break;
			}
		}
		return intentName;
	}

 /**
   * @description	-	Fetches case matrix configuration items from Case_Creation_Matrix__mdt
   * @return		-	List<Case_Creation_Matrix__mdt>
   */
	global Map<String, Case_Creation_Matrix__mdt> getCaseMatrixRecords(){
		List<Case_Creation_Matrix__mdt> caseMatrixConfigs = [SELECT Label, DeveloperName, Account_or_Lead__c, Case_Queue_Name__c, Demo_Interest__c, HVC__c, Intent__c
																FROM Case_Creation_Matrix__mdt
																ORDER BY Label ASC];

		// TODO: Implement a priority system for cases?
		Map<String, Case_Creation_Matrix__mdt> configMap = new Map<String, Case_Creation_Matrix__mdt>();
		for(Case_Creation_Matrix__mdt config : caseMatrixConfigs){
			configMap.put(config.DeveloperName, config);
		}
		return configMap;
	}

	global static Chat_button_by_skill__mdt getChatButtonMetadataBySkill(String language){
		String defaultLanguage = 'English';

		if(String.isEmpty(language)){
			language = defaultLanguage;
		}

		Chat_button_by_skill__mdt[] chatButtons = [SELECT Id, Skill__c, Button_Id__c
												   FROM Chat_button_by_skill__mdt
												   WHERE Skill__c = :language
												   LIMIT 1];

		//try and find the default language if something goes wrong
		if(chatButtons.size() == 0){
			chatButtons = [SELECT Id, Skill__c, Button_Id__c
						   FROM Chat_button_by_skill__mdt
						   WHERE Skill__c = :defaultLanguage
						   LIMIT 1];
		}

		System.debug('>>NF_GetCaseCreationMatrix.getChatButtonBySkill: chatButtons='+chatButtons);
		return chatButtons.size() > 0 != null ? chatButtons[0] : null;
	}

 /**
   * @description	- 	Wrapper class to store case matrix params for this record
   */
	global class ChatSessionMatrixParams{
		global String accountOrLead;
		global String demoInterest;
		global String hvc;
		global String intent;
		global String language;
	}

 /**
   * @description	- 	Wrapper class to store output params
   */
	global class OutputParams{
		global String chatButtonId;
		global String caseQueueName;
	}
}