/**
 * @File Name          : SCustomerTypeInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:05:28 AM
**/
public without sharing class SCustomerTypeInfo {

	public String accountId {get; set;}
	public String fxAccountId {get; set;}
	public Boolean isALead {get; set;}
	public String division {get; set;}
	public Boolean isHVC {get; set;}
	public String funnelStage {get; set;}
	public Datetime lastTradeDate {get; set;}
	public Datetime createdDate {get; set;}
	public String segPL {get; set;}

	public SCustomerTypeInfo() {
	}

	public SCustomerTypeInfo(Account accountObj) {
		this.isALead = false;
		this.accountId = accountObj.Id;
		this.fxAccountId = accountObj.fxAccount__c;
		this.isHVC = accountObj.Is_High_Value_Customer__c;
		this.funnelStage = accountObj.Funnel_Stage__pc;
		this.lastTradeDate = accountObj.Last_Trade_Date__c;
		this.createdDate = accountObj.CreatedDate;
	}

	public SCustomerTypeInfo(Lead leadObj) {
		this.isALead = true;
		this.createdDate = leadObj.CreatedDate;
		this.isHVC = false;
	}

	public static SCustomerTypeInfo getInfoForNewLead() {
		SCustomerTypeInfo result = new SCustomerTypeInfo();
		result.isALead = true;
		result.createdDate = Datetime.now();
		result.isHVC = false;
		return result;
	}
	
}