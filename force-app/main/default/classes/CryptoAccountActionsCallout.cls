/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 11-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
// CryptoAccountActionsCallout class can be used to 
// 1. Close the account
// 2. Lock the account
// 3. Enable the Account
// 4. Close All Accounts

public with sharing class CryptoAccountActionsCallout extends CryptoCallout {
    CryptoAccountParams params;
    List<String> result = new List<String>();

    Map<Id, fxAccount__c> fxaccountMap;

    Map<Id, String> oneIdsByfxAcc = new Map<Id, String>();
    Map<Id, Note> sToInsert = new Map<Id, Note>();
    Map<Id, Note> fToInsert = new Map<Id, Note>();

    Map<Id, fxAccount__c> fxaccountToUpdate = new Map<Id, fxAccount__c>();
    List<Paxos__c> paxosToUpdate = new List<Paxos__c>();

    public CryptoAccountActionsCallout(CryptoAccountParams params) {
        super();

        this.params = params;
        
        this.fxaccountMap = new Map<Id, fxAccount__c>([
            SELECT
                Id,
                Name,
                Email__c,
                fxTrade_One_Id__c,
                Paxos_Identity__c,
                Paxos_Identity__r.Admin_Disabled__c,
                Paxos_Identity__r.User_Disabled__c,
                Paxos_Identity__r.Id_Verification_Status__c,
                Paxos_Identity__r.Sanctions_Verification_Status__c,
                Paxos_Identity__r.Paxos_Status__c,
                Paxos_Identity__r.Paxos_Account_Number__c,
                Paxos_Identity__r.Display_Name__c,
                Paxos_Identity__r.MT5_Account_Number__c,
                Paxos_Identity__r.MT5_Account_Status__c,
                Paxos_Identity__r.Paxos_Account__c,
                Paxos_Identity__r.Paxos_Account_Status__c,
                Paxos_Identity__r.Paxos_Profile__c,
                Paxos_Identity__r.Paxos_Account_Admin_Disabled__c,
                Paxos_Identity__r.Paxos_Account_User_Disabled__c,
                Is_Closed__c,
                MT5_Client_Id__c
            FROM fxAccount__c
            WHERE Id IN :params.fxaccountIds
        ]);
    }

    public String execute() {
        Map<String, String> logRowToMessage = new Map<String,String>();
        if (params.fxaccountIds == null || params.fxaccountIds.size() <= 0 ||
            (String.isBlank(params.calloutResourcePathTas) && String.isBlank(params.calloutResourcePathUser))) {
            result.add('Wrong Data for process');
            logRowToMessage.put('Wrong Data for process', JSON.serializePretty(params));
            return String.join(result, '\n');
        }

        Map<Id, String> fxaccountEmails = new Map<Id, String>();

        for (fxAccount__c fxa : fxaccountMap.values()) {
            if (params.action == 1 && fxa.Is_Closed__c) { // fxAccount Closed and action Enable
                logRowToMessage.put(fxa.Name + ':', 'You cannot enable a Closed Account');
                result.add(fxa.Name + ': ' + 'You cannot enable a Closed Account');
            } else if (String.isNotBlank(fxa.fxTrade_One_Id__c)) {
                this.oneIdsByfxAcc.put(fxa.Id, fxa.fxTrade_One_Id__c);
            } else {
                fxaccountEmails.put(fxa.Id, fxa.Email__c);
            }
        }

        if (fxaccountEmails.size() > 0) {
            this.oneIdsByfxAcc.putAll(getOneIds(fxaccountEmails));
        }

        CryptoAccountTasActionsCallout tasCallout = new CryptoAccountTasActionsCallout();

        for (Id fxaId : this.oneIdsByfxAcc.keySet()) {
            try {
                String oneId = this.oneIdsByfxAcc.get(fxaId);

                Boolean sTas = false;
                Boolean sUser = false;
                
                Map<String, Object> bodyTas;
                Map<String, Object> bodyUser;

                if (params.useTasApi) {
                    Crypto_Account_Setting__mdt mdt = CryptoAccountSettingsMdt.getInstance().getMdt(params.calloutResourcePathTas);
                    String resource = String.format(
                        mdt.Value__c,
                        new List<String> { 
                            oneId,
                            mdt.Env__c,
                            String.valueOf(mdt.Check_Balance__c)
                        }
                    );
                    logRowToMessage.put('Tas Request:' , 'PATCH:' + resource + ' / ' + JSON.serializePretty(params.requestBody));  
                    sTas = tasCallout.patchMethod(resource, header, null, params.requestBody);

                    bodyTas = (Map<String, Object>) JSON.deserializeUntyped(tasCallout.resp.getBody());
                    logRowToMessage.put('Tas Response:' , JSON.serializePretty(bodyTas));  
                }
                if (params.useUserApi) {
                    Crypto_Account_Setting__mdt mdt = CryptoAccountSettingsMdt.getInstance().getMdt(params.calloutResourcePathUser);
                    String resource = String.format(
                        mdt.Value__c,
                        new List<String> { 
                            oneId,
                            mdt.Env__c,
                            String.valueOf(mdt.Check_Balance__c)
                        }
                    );
                    logRowToMessage.put('User Request:' , 'PATCH:' + resource + ' / ' + JSON.serializePretty(params.requestBody));  
                    patch(resource, header, null, params.requestBody);
                    sUser = isResponseCodeSuccess();

                    bodyUser = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
                    logRowToMessage.put('User Response:' , JSON.serializePretty(bodyUser));  
                }

                if ((params.useTasApi && sTas && params.useUserApi && sUser) ||
                    (params.useTasApi && sTas && !params.useUserApi) ||
                    (params.useUserApi && sUser && !params.useTasApi)) {
                    if (params.successNoteMap?.get(fxaId) != null) {
                        sToInsert.put(fxaId, params.successNoteMap.get(fxaId));
                        result.add(
                            (params.addFxAccountDataResult 
                                ? (fxaccountMap.get(fxaId)?.Name + ': ')
                                : ''
                            ) +
                            params.successNoteMap.get(fxaId).Body
                        );
                    }
                    updateFxAccount(fxaId, bodyTas, bodyUser);
                    updatePaxos(fxaId, bodyTas, bodyUser);
                } else {
                    String errorMsg = getErrorMessage(tasCallout, sTas, sUser, bodyTas, bodyUser);
                    processError(errorMsg, fxaId);
                }
            }
            catch (Exception ex) {
                processError(ex.getMessage(), fxaId);
                result.add(ex.getMessage());
            }
        }

        if (sToInsert.size() > 0) {
            insert sToInsert.values();
        }

        if (fToInsert.size() > 0) {
            insert fToInsert.values();
        }

        if (fxaccountToUpdate.size() > 0) {
            update fxaccountToUpdate.values();
        }

        if (paxosToUpdate.size() > 0) {
            update paxosToUpdate;
        }
        processLog(logRowToMessage);
        return String.join(result, '\n');
    }

    private Map<Id, String> getOneIds(Map<Id, String> fxaccountEmails) {
        Map<Id, String> oneIdsMap = new Map<Id, String>();

        for (Id fxaId : fxaccountEmails.keySet()) {
            String email = fxaccountEmails.get(fxaId);
            try {
                String resource = String.format(
                    CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),
                    new List<String> { email }
                );

                // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
                // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
                // If the problem will resolved on the server side, the fix can be removed.
                Map<String, String> headersForGetRequest = header.clone();
                if (headersForGetRequest.containsKey('Content-Type')) {
                    headersForGetRequest.remove('Content-Type');
                }

                get(resource, headersForGetRequest, null);
    
                if (isResponseCodeSuccess()) {
                    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
                    String uuid = (String) results.get('uuid');
                    fxAccount__c fxa = this.fxaccountMap.get(fxaId);
                    fxa.fxTrade_One_Id__c = uuid;
                    this.fxaccountToUpdate.put(fxaId, fxa);
                    oneIdsMap.put(fxaId, uuid);
                } else {
                    String errorMsg = exceptionDetails != null 
                    ? exceptionDetails.getMessage() 
                    : resp.getBody();
                    processError(errorMsg, fxaId);
                }
            }
            catch (Exception ex) {
                processError(ex.getMessage(), fxaId);
                result.add(ex.getMessage());
            }
        }

        return oneIdsMap;
    }

    private void updateFxAccount(Id fxaId, Map<String, Object> bodyTas, Map<String, Object> bodyUser) {
        fxAccount__c fxa;
        if (this.fxaccountToUpdate.get(fxaId) != null) {
            fxa = this.fxaccountToUpdate.get(fxaId);
        } else {
            fxa = this.fxaccountMap.get(fxaId);
        }

        if (fxa != null) {
            Boolean toUpdate = false;

            if (params.action == 0) { // Lock
                fxa.Account_Locked__c = true;
                toUpdate = true;
            }
            else if (params.action == 1) { // Enable
                fxa.Account_Locked__c = false;
                fxa.Is_Closed__c = false;
                toUpdate = true;
            }
            else if (params.action == 2) { // Close
                fxa.Is_Closed__c = true;
                toUpdate = true;
            }

            String mt5ClientID = (String)bodyUser?.get('mt5ClientID');
            if (String.isNotBlank(mt5ClientID) && mt5ClientID != fxa.MT5_Client_Id__c) {
                fxa.MT5_Client_Id__c = (String)bodyUser?.get('mt5ClientID');
                toUpdate = true;
            }

            if (toUpdate) {
                this.fxaccountToUpdate.put(fxaId, fxa);
            }
        }
    }

    private void updatePaxos(Id fxaId, Map<String, Object> bodyTas, Map<String, Object> bodyUser) {
        Paxos__c p = this.fxaccountMap.get(fxaId).Paxos_Identity__r;
        if (p != null) {
            Boolean toUpdate = false;

            // Boolean adminDisabled = (Boolean)bodyUser?.get('adminDisabled');
            // if (adminDisabled != null && adminDisabled != p.Admin_Disabled__c) {
            //     p.Admin_Disabled__c = adminDisabled;
            //     toUpdate = true;
            // }

            Boolean userDisabled = (Boolean)bodyUser?.get('userDisabled');
            if (userDisabled != null && userDisabled != p.User_Disabled__c) {
                p.User_Disabled__c = userDisabled;
                toUpdate = true;
            }

            String idVerificationStatus = (String)bodyUser?.get('idVerificationStatus');
            if (String.isNotBlank(idVerificationStatus) && idVerificationStatus != p.Id_Verification_Status__c) {
                p.Id_Verification_Status__c = (String)bodyUser?.get('idVerificationStatus');
                toUpdate = true;
            }

            String sanctionsVerificationStatus = (String)bodyUser?.get('sanctionsVerificationStatus');
            if (String.isNotBlank(sanctionsVerificationStatus) && sanctionsVerificationStatus != p.Sanctions_Verification_Status__c) {
                p.Sanctions_Verification_Status__c = (String)bodyUser?.get('sanctionsVerificationStatus');
                toUpdate = true;
            }

            String paxosSummaryStatus = (String)bodyUser?.get('paxosSummaryStatus');
            if (String.isNotBlank(paxosSummaryStatus) && paxosSummaryStatus != p.Paxos_Status__c) {
                p.Paxos_Status__c = (String)bodyUser?.get('paxosSummaryStatus');
                toUpdate = true;
            }

            String paxosUserID = (String)bodyUser?.get('paxosUserID');
            if (String.isNotBlank(paxosUserID) && paxosUserID != p.Paxos_Account_Number__c) {
                p.Paxos_Account_Number__c = (String)bodyUser?.get('paxosUserID');
                toUpdate = true;
            }

            String displayName = (String)bodyTas?.get('displayName');
            if (String.isNotBlank(displayName) && displayName != p.Display_Name__c) {
                p.Display_Name__c = displayName;
                toUpdate = true;
            }

            String mt5UserID = (String)bodyTas?.get('mt5UserID');
            if (String.isNotBlank(mt5UserID) && mt5UserID != p.MT5_Account_Number__c) {
                p.MT5_Account_Number__c = mt5UserID;
                toUpdate = true;
            }

            String mt5UserStatus = (String)bodyTas?.get('mt5UserStatus');
            if (String.isNotBlank(mt5UserStatus) && mt5UserStatus != p.MT5_Account_Status__c) {
                p.MT5_Account_Status__c = mt5UserStatus;
                toUpdate = true;
            }

            String paxosAccountID = (String)bodyTas?.get('paxosAccountID');
            if (String.isNotBlank(paxosAccountID) && paxosAccountID != p.Paxos_Account__c) {
                p.Paxos_Account__c = paxosAccountID;
                toUpdate = true;
            }

            String paxosAccountStatus = (String)bodyTas?.get('paxosAccountStatus');
            if (String.isNotBlank(paxosAccountStatus) && paxosAccountStatus != p.Paxos_Account_Status__c) {
                p.Paxos_Account_Status__c = paxosAccountStatus;
                toUpdate = true;
            }

            String paxosProfileID = (String)bodyTas?.get('paxosProfileID');
            if (String.isNotBlank(paxosProfileID) && paxosProfileID != p.Paxos_Profile__c) {
                p.Paxos_Profile__c = paxosProfileID;
                toUpdate = true;
            }
            
            // Boolean accountAdminDisabled = (Boolean)bodyTas?.get('accountAdminDisabled');
            // if (accountAdminDisabled != null && accountAdminDisabled != p.Paxos_Account_Admin_Disabled__c) {
            //     p.Paxos_Account_Admin_Disabled__c = accountAdminDisabled;
            //     toUpdate = true;
            // }

            Boolean accountUserDisabled = (Boolean)bodyTas?.get('accountUserDisabled');
            if (accountUserDisabled != null && accountUserDisabled != p.Paxos_Account_User_Disabled__c) {
                p.Paxos_Account_User_Disabled__c = accountUserDisabled;
                toUpdate = true;
            }

            if (toUpdate) {
                paxosToUpdate.add(p);
            }
        }
    }

    private String getErrorMessage(
        CryptoAccountTasActionsCallout tasCallout,
        Boolean sTas,
        Boolean sUser,
        Map<String, Object> bodyTas,
        Map<String, Object> bodyUser) {
        String errorMsg = '';
        if (params.useTasApi && !sTas) {
            if ((Integer) bodyTas?.get('statusCode') == 403 ||
                tasCallout.resp.getStatusCode() == 403) {
                errorMsg = System.Label.Remaining_Balance_Error;
            } else {
                String msg = (String) bodyTas?.get('errorMessage');
                msg = String.isNotBlank(msg) ? msg : (String) bodyTas?.get('message');
                errorMsg = tasCallout.exceptionDetails != null 
                    ? tasCallout.exceptionDetails.getMessage() 
                    : (String.isNotBlank(msg) ? msg : tasCallout.resp.getBody());
            }
        }
        if (params.useUserApi && !sUser) {
            if ((Integer) bodyUser?.get('statusCode') == 403 ||
                resp.getStatusCode() == 403) {
                errorMsg = System.Label.Remaining_Balance_Error;
            } else {
                String msg = (String) bodyUser?.get('errorMessage');
                msg = String.isNotBlank(msg) ? msg : (String) bodyUser?.get('message');
                errorMsg += '\n' + (exceptionDetails != null 
                    ? exceptionDetails.getMessage() 
                    : (String.isNotBlank(msg) ? msg : resp.getBody()));
            }
        }
        return errorMsg;
    }

    private void processLog( Map<String, String> logRows) {
        String errorMsg = '';
        for(String key : logRows.keySet()) {
            errorMsg += key + logRows.get(key) + '\n';
        }

        Logger.info('Crypto Action executed', Constants.LOGGER_CRYPTO_CATEGORY, errorMsg);
    }
    
    private void processError(String errorMsg, Id fxaId) {
        if (params.failNoteMap?.get(fxaId) != null) {
            params.failNoteMap.get(fxaId).Body += (String.isNotBlank(errorMsg) ? '\n' + errorMsg : '');
            fToInsert.put(fxaId, params.failNoteMap.get(fxaId));
            result.add(
                (params.addFxAccountDataResult 
                    ? (fxaccountMap.get(fxaId)?.Name + ': ')
                    : ''
                ) +
                params.failNoteMap.get(fxaId).Body
            );
        }

        Logger.error(params.cryptoAccountAction, 
            Constants.LOGGER_CRYPTO_CATEGORY, 
            errorMsg + '\n' + getRequestInfo());
    }

    private String getRequestInfo() {
        if (req == null)
            return '';

        return 'Endpoint: ' + req.getEndpoint() + '\n' +
            'Body: ' + req.getBody() + '\n';
    }

    public class CryptoAccountStatusRequest {
        public Map<String, Object> body { get; set; }

        public Boolean isDisabled { 
            get {
                return (Boolean)body?.get('isDisabled');
            }
            set {
                body.put('isDisabled', value);
            }
        }

        public CryptoAccountStatusRequest() {
            body = new Map<String, Object>();
            
            isDisabled = false;
        }
    }


    public String getOneIdByEmailNoActions(String fxaccountEmail) {
        String uuid;
        String resource = String.format(
                CryptoAccountSettingsMdt.getInstance().getValue(Constants.USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT),  
                new List<String> { fxaccountEmail }
            );

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);
        if (isResponseCodeSuccess()) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
            uuid = (String) results?.get('uuid');
        } 
        return uuid;
    }

}