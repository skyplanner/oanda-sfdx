/**
* Created by Neuraflash LLC on 03/01/19.
* Implementation : Chatbot
* Summary : Lookup Account records by searching Name/DOB & Return Boolean
* Details : 1. Searches for an Account record by name/DOB provided.
*           2. Return Boolean if single Account found/not found.
*
*/

global without sharing class NF_lookupAccountByNameDOB implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(aiServiceResponse);
        String sessionId = (String)m.get('sessionId');

        Map<String, String> eventParams = new Map<String, String>();
        String res = 'false';
        String email = null;

        List<nfchat__Chat_Log__c> listChatLog = [SELECT Id,nfchat__First_Name__c,Date_Of_Birth__c FROM nfchat__Chat_Log__c WHERE nfchat__Session_Id__c=: sessionId LIMIT 1];
        if(!listChatLog.isEmpty()) {
            if(String.isNotBlank(listChatLog[0].nfchat__First_Name__c) &&
                    String.isNotBlank(listChatLog[0].Date_Of_Birth__c)) {
                List<Account> listAccount = [SELECT Id,BirthDate__c,PersonEmail FROM Account
                WHERE Name =: listChatLog[0].nfchat__First_Name__c
                ORDER BY CreatedDate ASC NULLS FIRST];

                if(listAccount.size() == 1) {
                    Date dateString = NF_lookupAccountByNameDOB.setStringToDateFormat(listChatLog[0].Date_Of_Birth__c);
                    if(listAccount[0].BirthDate__c == dateString){
                        res = 'true';
                        email = listAccount[0].PersonEmail;
                    }
                }
            }
        }

        eventParams.put('result', res);
        eventParams.put('email', email);
        return JSON.serialize(eventParams);
    }
    private static Date setStringToDateFormat(String myDate) {
        String[] strDate = myDate.split('/');
        Integer myIntDate = integer.valueOf(strDate[1]);
        Integer myIntMonth = integer.valueOf(strDate[0]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        return d;
    }
}