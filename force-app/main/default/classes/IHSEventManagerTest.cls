@isTest
public with sharing class IHSEventManagerTest 
{
    @TestSetup
    static void makeData(){
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxa = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxa.fxTrade_User_Id__c = 1234;
        fxa.Division_Name__c = Constants.DIVISIONS_OANDA_EUROPE;
        update fxa;

        // Create case
        Case cs = new Case(
            Subject = 'sample',
            Status = 'Open');
        insert cs;
        
        // Create document
        Document__c doc = 
            new Document__c(
                Name = 'doc',
                fxAccount__c = fxa.Id,
                Case__c = cs.Id);
        insert doc;
    }

    @isTest
    static void IHSScanPositive() 
    {
        Test.startTest();

        Document__c doc = [SELECT Name, IHS_Document_Status__c FROM Document__c];
            // Update IHS Document Status to 'Ready'
            doc.IHS_Document_Status__c = Constants.IHS_DOCUMENT_STATUS_READY;
            update doc;
        Test.stopTest();

        Document__c docAfter = [SELECT Name, IHS_Document_Status__c FROM Document__c];
        
        // Check doc got the right IHS Document Status
        Assert.areEqual(docAfter.IHS_Document_Status__c, Constants.IHS_DOCUMENT_STATUS_SENDING_TO_IHS_INITIATED);

        // Verify that action platform events were published
        Assert.areEqual(ActionUtil.lastPublished.size(), 1); 
    }

    @isTest
    static void IHSScanNegative() 
    {
        Test.startTest();

        Document__c doc = [SELECT Name, IHS_Document_Status__c FROM Document__c];
            // Update IHS Document Status to 'Sent to IHS successfully'
            doc.IHS_Document_Status__c = Constants.IHS_DOCUMENT_STATUS_SENT_TO_IHS_SUCCESSFULLY;
            update doc;
        Test.stopTest();

        Document__c docAfter = [SELECT Name, IHS_Document_Status__c FROM Document__c];
        
        // Check IHS Document Status did NOT get update to 'Sending to IHS Initiated'
        Assert.areNotEqual(docAfter.IHS_Document_Status__c, Constants.IHS_DOCUMENT_STATUS_SENDING_TO_IHS_INITIATED);

        // Verify that action platform event was NOT published
        Assert.areEqual(ActionUtil.lastPublished, null); 
    }    
}