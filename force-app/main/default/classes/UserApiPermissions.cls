/**
 * User API app permissions manager class
 */
public class UserApiPermissions {
    public static final String PERM_SAVE_UPDATES_LIVE = 'UAPI_Save_Updates';
    public static final String PERM_SAVE_UPDATES_DEMO = 'UAPI_Save_Updates_Demo';
    public static final String PERM_SAVE_V20_ACCOUNT_LIVE = 'UAPI_Save_v20Account';
    public static final String PERM_SAVE_V20_ACCOUNT_DEMO = 'UAPI_Save_v20Account_Demo';
    public static final String PERM_RESEND_MT4_LIVE = 'UAPI_Resend_MT4_Notification';
    public static final String PERM_RESEND_MT4_DEMO = 'UAPI_Resend_MT4_Notification_Demo';
    public static final String PERM_SAVE_KEY = 'save';
    public static final String PERM_SAVE_V20_ACCOUNT_KEY = 'save-v20-account';
    public static final String PERM_RESEND_MT4_NOTIFICATION_KEY = 'resend-mt4';
    public static final String PERM_LOCK_MT5_LIVE = 'UAPI_Lock_mt5_Live';
    public static final String PERM_LOCK_MT5_DEMO = 'UAPI_Lock_mt5_Demo';
    public static final String PERM_UNLOCK_MT5_LIVE = 'UAPI_Unlock_mt5_Live';
    public static final String PERM_UNLOCK_MT5_DEMO = 'UAPI_Unlock_mt5_Demo';


    /**
     * Get application permissions
     */
    public static PermissionsWrapper getPermissions() {
        PermissionsWrapper result = new PermissionsWrapper();
        result.live = new Map<String, Boolean> {
            PERM_SAVE_KEY => checkPermission(PERM_SAVE_UPDATES_LIVE),
            PERM_SAVE_V20_ACCOUNT_KEY => 
                checkPermission(PERM_SAVE_V20_ACCOUNT_LIVE),
            PERM_RESEND_MT4_NOTIFICATION_KEY => 
                checkPermission(PERM_RESEND_MT4_LIVE),
                PERM_LOCK_MT5_LIVE => 
                checkPermission(PERM_LOCK_MT5_LIVE),
                PERM_UNLOCK_MT5_LIVE => 
                checkPermission(PERM_UNLOCK_MT5_LIVE)
        };

        result.demo = new Map<String, Boolean> {
            PERM_SAVE_KEY => checkPermission(PERM_SAVE_UPDATES_DEMO),
            PERM_SAVE_V20_ACCOUNT_KEY => 
                checkPermission(PERM_SAVE_V20_ACCOUNT_DEMO),
            PERM_RESEND_MT4_NOTIFICATION_KEY => 
                checkPermission(PERM_RESEND_MT4_DEMO),
            PERM_LOCK_MT5_DEMO => 
                checkPermission(PERM_LOCK_MT5_DEMO),
            PERM_UNLOCK_MT5_DEMO => 
                checkPermission(PERM_UNLOCK_MT5_DEMO)
        };

        return result;
    }

    /**
     * Check custom permission
     */
    private static Boolean checkPermission(String cp) {
        return FeatureManagement.checkPermission(cp);
    }

    /**
     * Get available actions
     * - Iterate setting actions to know what respective
     *   custom permissions with that name are available
     *   for the current user
     */
    public static List<UserApiAction> getAvailableActions(String fxTradeName,
        UserApiUserGetResponse userInfo, TasUserAccountGetResponse v20Info
    ) {
        List<UserApiAction> actions = new List<UserApiAction>();
        
        // Iterate actions from settings
        // to get availables for the current user
        for (UserApiAction action : UserApiAppSettings.getActions()) {
            if (action.isAvailableForCurrentUser()) {
                Boolean isEnvVisible = action.env == 'all' ||
                    action.env == userInfo?.user?.env;
                Boolean isLink = action.isLinkAction(fxTradeName);
                Boolean isVisible = false;

                if (!isLink) {
                    Callable cls = (Callable) Type.forName(action.managerClass)
                        .newInstance();
                    isVisible = (Boolean) cls.call(
                        UserApiActionBase.ACTION_NAME_IS_VISIBLE,
                        new Map<String, Object> {
                            'action'=> action,
                            'user'=> userInfo,
                            'v20DefaultInfo'=> v20Info
                        });
                }

                if (isEnvVisible && (isLink || isVisible))
                    actions.add(action);
            }
        }

        return actions;
    }

    public class PermissionsWrapper {
        @AuraEnabled
        public Map<String, Boolean> live {get; set;}

        @AuraEnabled
        public Map<String, Boolean> demo {get; set;}
    }
}