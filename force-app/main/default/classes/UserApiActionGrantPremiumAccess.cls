/**
 * User API 'Grant Premium Access' action manager class
 */
public class UserApiActionGrantPremiumAccess extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        Id fxAccountId = getfxAccountId(record);
        EnvironmentIdentifier identifier = getfxTradeUserId(record);
        UserApiUserPrivilegePatchRequest.UserPrivilegePatchRequestWrapper w
            = UserApiUserPrivilegePatchRequest.getGrantPremiumAccessWrapper();
        UserCallout userCallout = new UserCallout();
        userCallout.updateUserPrivileges(identifier, w.privileges);
        String action = getActionLabel(record);
        auditTrailManager.saveAuditTrail(
            identifier.id, fxAccountId, action, w.change);

        return action + ' action success!!!';
    }
    
    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return !user.premiumEnabled;
    }
}