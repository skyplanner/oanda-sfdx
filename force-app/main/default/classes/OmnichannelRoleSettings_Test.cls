/**
 * @File Name          : OmnichannelRoleSettings_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/22/2021, 11:31:30 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 11:21:56 PM   acantero     Initial Version
**/
@isTest
private without sharing class OmnichannelRoleSettings_Test {

    @isTest
    static void test() {
        String agentRole = [
            select Agent_Role_Dev_Name__c
            from Omnichannel_Role__mdt
            limit 1
        ].Agent_Role_Dev_Name__c;
        Test.startTest();
        OmnichannelRoleSettings instance = 
            OmnichannelRoleSettings.getInstance();
        Set<String> result = instance.getSupervisorRoles(agentRole);
        Test.stopTest();
        System.assertEquals(false, result.isEmpty());
    }
    
}