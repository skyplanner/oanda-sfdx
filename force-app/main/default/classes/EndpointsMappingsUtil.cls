/**
 * @File Name          : EndpointsMappingsUtil.cls
 * @Description        : 
 * @Author             : Mikolaj Juras
 * @CreateDate         : 8th April 2024
**/
public with sharing class EndpointsMappingsUtil {

    public Map<String,Map<String,Endpoints_Mappings__mdt>> getEnpointsMappingByEndpoint(String endpointUrl) {

        Map <String,Map <String,Endpoints_Mappings__mdt>> currentEndpointMapping = new  Map<String,Map<String,Endpoints_Mappings__mdt>>();
        for(Endpoints_Mappings__mdt em : [SELECT Data_Type__c, Input_Field_Name__c, Sobject_Field_Name__c, Sobject_Name__c 
                                            FROM Endpoints_Mappings__mdt 
                                            WHERE Endpoint_Url_Name__c =: endpointUrl AND Is_Active__c = true]) {
            if(!currentEndpointMapping.containsKey(em.Sobject_Name__c)) {
                currentEndpointMapping.put(em.Sobject_Name__c, new Map<String, Endpoints_Mappings__mdt>());                
            }
            // we have to keep data clear
            currentEndpointMapping.get(em.Sobject_Name__c).put(em.Input_Field_Name__c, em);
        }
        return currentEndpointMapping;
    }
}