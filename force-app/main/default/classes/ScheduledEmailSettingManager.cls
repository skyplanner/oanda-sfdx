/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-19-2022
 * @last modified by  : Ariel Niubo
 **/
public without sharing class ScheduledEmailSettingManager implements IScheduledEmailSettingManager {
	private IScheduledEmailSetting setting { get; private set; }
	public ScheduledEmailSettingManager() {
	}
	public IScheduledEmailSetting getSetting() {
		if (setting == null) {
			setting = new ScheduledEmailSetting(
				ScheduleMailSettingRepo.getFirst()
			);
		}
		return setting;
	}
}