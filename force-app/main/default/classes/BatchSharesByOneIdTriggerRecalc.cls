/**
 * @description       : Batch job to be called from fxAccount trigger, desired size 1 to trigger shares recalc in TAS 
 * @author            : Mikolaj Juras
 * @group             : 
 * @last modified on  : 14 July 2023
 * @last modified by  : Mikolaj Juras
**/
public with sharing class BatchSharesByOneIdTriggerRecalc implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.AllowsCallouts, Database.Stateful {
    
    Set<Id> fxAccountIds;
    String query = 'SELECT Id, RecordTypeId, Email__c, fxTrade_One_Id__c from fxAccount__c where Id In :fxAccountIds';
    @TestVisible
    private final static String LOGGER_CATEGORY = 'trigger-shares-recalculation';
    Boolean isRerun = false;
    List <RecalcBodyRequest> requestToBeSend = new List<RecalcBodyRequest>();
    
    private BatchSharesByOneIdTriggerRecalc(Set<Id> fxAccountIds) {
        this.fxAccountIds = fxAccountIds;
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
        isRerun=true;
        return executeBatch(new Set<Id>(recIds));
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope) {
        if (scope.size() != 1) {
            Logger.error('BatchSharesByOneIdTriggerRecalc batch size is not set to 1', LOGGER_CATEGORY, JSON.serialize(scope));
        }
    
        fxAccount__c fxAccToProcess = scope[0];
        String fxAccOneId = fxAccToProcess.fxTrade_One_Id__c != null 
                            ? fxAccToProcess.fxTrade_One_Id__c
                            : getOneIdByEmail(fxAccToProcess.Email__c);

        String fxAccEnv = fxAccountUtil.getEnvVariable(fxAccToProcess);

        if (String.isBlank(fxAccEnv)) {
            Logger.error('BatchSharesByOneIdTriggerRecalc RT do not match', LOGGER_CATEGORY, 'fxAccId: ' + fxAccToProcess.Id + 'RT Id: ' + fxAccToProcess.RecordTypeId);
        }

        if(fxAccOneId != null && String.isNotBlank(fxAccEnv)) {
            BatchSharesByOneIdTriggerRecalc.RecalcBodyRequest recReq = new BatchSharesByOneIdTriggerRecalc.RecalcBodyRequest();
            recReq.env = fxAccEnv;
            recReq.oneID = fxAccOneId;
            requestToBeSend.add(recReq);          
        }
    }
           
    public void finish(Database.BatchableContext bc) {
        if(!requestToBeSend.isEmpty()) {
            RecalcBodyRequestWrapper recalcListWrapper = new RecalcBodyRequestWrapper();
            recalcListWrapper.items = requestToBeSend;
            TriggerSharesRecalc newRecalc = new TriggerSharesRecalc();
            newRecalc.requestToBeSendInner = JSON.serialize(recalcListWrapper);
            System.enqueueJob(newRecalc);
        }
  
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
    
    public static Id executeBatch(Set<Id> fxAccsIdsToRecalcShares) {
        return Database.executeBatch(new BatchSharesByOneIdTriggerRecalc(fxAccsIdsToRecalcShares),1);
    }

    private String getOneIdByEmail(String email) {
        //set dummy input params
        CryptoAccountParams params = new CryptoAccountParams();
        params.fxaccountIds = new List<Id>();
        CryptoAccountActionsCallout caac = new CryptoAccountActionsCallout(params);        
        return caac.getOneIdByEmailNoActions(email);
    }

    @future(callout=true)
    public static void callSharesRecalcEndpoint(String oneIdsToRecalc) {
        APISettings apiSettings = APISettingsUtil.getApiSettingsByName(Constants.GROUP_RECALCULATION_ENDPOINT);
        String url = apiSettings.baseUrl;
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', apiSettings.clientId);
            req.setHeader('client_secret', apiSettings.clientSecret);
            req.setTimeout(120000);
            req.setBody(oneIdsToRecalc);
        
            Http http = new Http();
            HttpResponse response = http.send(req);
            if(response.getStatusCode() ==  200) {
                Logger.info('Shares Recalculation trigger correctly', LOGGER_CATEGORY, oneIdsToRecalc + response.getBody());
            }

            if(response.getStatusCode() != 200){
                Logger.error('BatchSharesByOneIdTriggerRecalc response is not OK', LOGGER_CATEGORY, oneIdsToRecalc + response.getBody());
            }
    }
    public class RecalcBodyRequestWrapper {
        List<RecalcBodyRequest> items;
    }

    public class RecalcBodyRequest {
        String env;
        String oneID;
    }

    public class TriggerSharesRecalc implements Queueable {
        String requestToBeSendInner;
        public void execute(QueueableContext context) {
            BatchSharesByOneIdTriggerRecalc.callSharesRecalcEndpoint(requestToBeSendInner);
        }
    }
}