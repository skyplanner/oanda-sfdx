/**
 * @File Name          : SRoutingPriorityProcessTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/21/2024, 11:32:05 AM
**/
@IsTest
private without sharing class SRoutingPriorityProcessTest {

	// test 1 : createdFromChat = true
	// test 2 : isHVC = true
	// test 3 : isNewCustomer = true, 
	// test 4 : isActiveCustomer = true
	// test 5 : isAComplaint = true
	// test 6 : priority = CASE_PRIORITY_CRITICAL
	// test 7 : priority = CASE_PRIORITY_HIGH
	// test 8 : ccmCx = true
	// test 9 : deposit = true
	@IsTest
	static void getRoutingPriority1() {
		Test.startTest();
		SRoutingPriorityProcess instance = new SRoutingPriorityProcess();
		// test 1
		ServiceRoutingInfo routingInfo = getNewChatRoutingInfo();
		Integer result1 = instance.getRoutingPriority(routingInfo);
		// test 2
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.relatedCustomerType = 
			OmnichannelCustomerConst.CustomerType.HVC;
		routingInfo.initTier(OmnichannelCustomerConst.TIER_3);
		Integer result2 = instance.getRoutingPriority(routingInfo);
		// test 3
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.relatedCustomerType = 
			OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER;
		routingInfo.initTier(OmnichannelCustomerConst.TIER_2);
		Integer result3 = instance.getRoutingPriority(routingInfo);
		// test 4
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.relatedCustomerType = 
			OmnichannelCustomerConst.CustomerType.ACTIVE;
		Integer result4 = instance.getRoutingPriority(routingInfo);
		// test 5
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.isAComplaint = true;
		routingInfo.initTier(OmnichannelCustomerConst.TIER_1);
		Integer result5 = instance.getRoutingPriority(routingInfo);
		// test 6
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.priority = OmnichanelConst.CASE_PRIORITY_CRITICAL;
		Integer result6 = instance.getRoutingPriority(routingInfo);
		// test 7
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.priority = OmnichanelConst.CASE_PRIORITY_HIGH;
		Integer result7 = instance.getRoutingPriority(routingInfo);
		// test 8
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.ccmCx = true;
		Integer result8 = instance.getRoutingPriority(routingInfo);
		// test 9
		routingInfo = getNewCaseRoutingInfo();
		routingInfo.deposit = true;
		Integer result9 = instance.getRoutingPriority(routingInfo);
		Test.stopTest();
		
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result1, 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result2, 
			'Invalid result'
		);
		Assert.areEqual(
			true, 
			result2 >= 	OmnichanelPriorityConst.PRIORITY_TIER_3_WEIGHT , 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result3, 
			'Invalid result'
		);
		Assert.areEqual(
			true, 
			result3  <= OmnichanelPriorityConst.PRIORITY_TIER_2_WEIGHT , 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result4, 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result5, 
			'Invalid result'
		);
		Assert.areEqual(
			true, 
			result5 <= 
			(OmnichanelPriorityConst.DEFAULT_PRIORITY - OmnichanelPriorityConst.PRIORITY_TIER_1_WEIGHT) , 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result6, 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result7, 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result8, 
			'Invalid result'
		);
		Assert.areNotEqual(
			OmnichanelPriorityConst.DEFAULT_PRIORITY, 
			result9, 
			'Invalid result'
		);
	}

	// test 1 : newValue > FINAL_PRIORITY_MIN_VALUE
	// test 2 : newValue < FINAL_PRIORITY_MIN_VALUE
	@IsTest
	static void getValidRoutingPriority() {
		Integer newValue1 = 
			OmnichanelPriorityConst.FINAL_PRIORITY_MIN_VALUE + 1;
		Integer newValue2 = 
			OmnichanelPriorityConst.FINAL_PRIORITY_MIN_VALUE - 1;
	
		Test.startTest();
		// test 1
		Integer result1 = 
			SRoutingPriorityProcess.getValidRoutingPriority(newValue1);
		// test 2
		Integer result2 = 
			SRoutingPriorityProcess.getValidRoutingPriority(newValue2);
		Test.stopTest();
		
		Assert.areEqual(newValue1, result1, 'Invalid result');
		Assert.areEqual(
			OmnichanelPriorityConst.FINAL_PRIORITY_MIN_VALUE, 
			result2, 
			'Invalid result'
		);
	}

	static ServiceRoutingInfo getNewCaseRoutingInfo() {
		return getNewRoutingInfo(ServiceRoutingInfo.Source.CASES);
	}

	static ServiceRoutingInfo getNewChatRoutingInfo() {
		return getNewRoutingInfo(ServiceRoutingInfo.Source.CHAT);
	}

	static ServiceRoutingInfo getNewRoutingInfo(
		ServiceRoutingInfo.Source infoSource
	) {
		return new ServiceRoutingInfo(
			null, // psrObj
			infoSource, // infoSource
			null // workItem
		);
	}
	
}