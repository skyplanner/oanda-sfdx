/**
 * @File Name          : PhoneSLAViolationManager_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/6/2024, 1:09:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/27/2021, 2:05:52 PM   acantero     Initial Version
**/
@isTest
private without sharing class PhoneSLAViolationManager_Test {

    @testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        );
        
    }

    //valid phone cased bounced
    @isTest
    static void processHVCPhoneCaseBounces1() {
        //create test case
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        //...
        List<Case> caseList = OmnichanelRoutingTestDataFactory.getNewTestCases(false, true);
        //there is only one HVC case
        Case caseObj = caseList[0];
        caseObj.Origin = OmnichanelConst.CASE_ORIGIN_PHONE;
        insert caseObj;
        //...
        //enable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = true;
        String userId = UserInfo.getUserId();
        ServiceResourceTestDataFactory.createAgentServiceResIfNotExists(userId);
        caseObj = [select Id, OwnerId from Case where Id = :caseObj.Id];
        System.debug('Case Id: ' + caseObj.Id + ', OwnerId: ' + caseObj.OwnerId);
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0004');
        System.runAs(testUser) {
            String secondUserId = UserInfo.getUserId();
            System.debug('secondUserId: ' + secondUserId);
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(secondUserId);
            caseObj.OwnerId = secondUserId;
            update caseObj;
        }
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(0, count);
    }

    //valid phone cased bounced but milestone period exceeded
    @isTest
    static void processHVCPhoneCaseBounces2() {
        Integer testMilestonePeriod = getMilestoneTime('Phone_Tier_1_Chat_ASA');
        //create test case
        //disable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = false;
        //...
        List<Case> caseList = OmnichanelRoutingTestDataFactory.getNewTestCases(false, true);
        //there is only one HVC case
        Case caseObj = caseList[0];
        caseObj.Origin = OmnichanelConst.CASE_ORIGIN_PHONE;
        insert caseObj;
        Test.setCreatedDate(caseObj.Id, System.now().addMinutes(-(testMilestonePeriod * 2)));
        //...
        //enable SPCaseTrigger Trigger
        SPCaseTriggerHandler.enabled = true;
        String userId = UserInfo.getUserId();
        ServiceResourceTestDataFactory.createAgentServiceResIfNotExists(userId);
        caseObj = [select Id, OwnerId from Case where Id = :caseObj.Id];
        System.debug('Case Id: ' + caseObj.Id + ', OwnerId: ' + caseObj.OwnerId);
        Test.startTest();
        User testUser = SPTestUtil.createUser('u0004');
        System.runAs(testUser) {
            String secondUserId = UserInfo.getUserId();
            System.debug('secondUserId: ' + secondUserId);
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(secondUserId);
            caseObj.OwnerId = secondUserId;
            PhoneSLAViolationManager.phoneCaseBouncedMilestonePeriod = testMilestonePeriod;
            update caseObj;
        }
        Test.stopTest();
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(1, count);
    }

    //invalid constructor params
    @isTest
    static void processHVCPhoneCaseBounces3() {
        Boolean error = false;
        Test.startTest();
        try {
            PhoneSLAViolationManager instance = new PhoneSLAViolationManager(
                null //oldMap
            );
            instance.processHVCPhoneCaseBounces();
            //...
        } catch(Exception e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    //invalid milestone period 
    @isTest
    static void processHVCPhoneCaseBounces4() {
        Boolean error = false;
        Test.startTest();
        try {
            PhoneSLAViolationManager instance = new PhoneSLAViolationManager(
                new Map<Id,Case>()
            );
            PhoneSLAViolationManager.phoneCaseBouncedMilestonePeriod = 
                PhoneSLAViolationManager.MILESTONE_INVALID_PERIOD;
            instance.processHVCPhoneCaseBounces();
            //...
        } catch(Exception e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @isTest
    static void getHVCPhoneCaseBouncedList1() {
        Test.startTest();
        PhoneSLAViolationManager instance = new PhoneSLAViolationManager(
            new Map<Id,Case>()
        );
        List<Case> result = instance.getPhoneCaseBouncedList();
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    private static Integer getMilestoneTime(String name){
        List<Milestone_Time_Settings__mdt> milestoneTypeList = [
            SELECT Id, QualifiedApiName, Time_Trigger__c 
            FROM  Milestone_Time_Settings__mdt 
            WHERE QualifiedApiName = :name
        ];
        return milestoneTypeList.isEmpty() ? 0 : 
        (milestoneTypeList[0].Time_Trigger__c *60).intValue();
    }
    
}