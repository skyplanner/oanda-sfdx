/**
 * @File Name          : CategoriesTopArticlesCtrl.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/1/2019, 7:53:28 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
public without sharing class CategoriesTopArticlesCtrl {
	
	@AuraEnabled
	public static List<DataCategoryWrapper> getFaqCategories(String division) {
		return DataCategoryTopArticlesHelper.getFaqCategories(division, null);
	}

	@AuraEnabled
	public static FaqCategoriesInfo getFaqCategoriesInfo() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();

		FaqCategoriesInfo info = new FaqCategoriesInfo(
			settingsMgr.categoryTopArticlesCount,
			settingsMgr.restrictTopArticles,
			settingsMgr.commonDivision
		);
		info.divisions = DataCategoriesHelper.getDataCategoriesByGroup(
			DataCategoriesConstants.ARTICLE_SOBJECT_NAME, 
			DataCategoriesConstants.DAT_CAT_DIVISION,
			false
		);
		if ((info.divisions != null) && (!info.divisions.isEmpty())) {
			info.selDivisionIndex = 0;
			String division = info.divisions[0].value;
			info.categories = DataCategoryTopArticlesHelper.getFaqCategories(division, null);
		}
		return info;
	}

	@AuraEnabled
	public static void deleteArticle(String category, String division, String topArtId) {
		if (String.isBlank(category) || String.isBlank(division) || String.isBlank(topArtId)) {
			return;
		}
		//else...
		Decimal order = 0; 
		List<Category_Top_Article__c> topArticles = [
			SELECT 
				Id, Order__c
			FROM 
				Category_Top_Article__c
			WHERE 
				Id = :topArtId
			LIMIT 1
		];
		if (topArticles.size() > 0) {
			Category_Top_Article__c topArt = topArticles[0];
			order = topArt.Order__c;
			delete topArt;
		}
		List<Category_Top_Article__c> belowArticles = [
			SELECT 
				Id, Order__c
			FROM 
				Category_Top_Article__c
			WHERE 
				DataCategoryName__c = :category 
			AND
				DivisionName__c = :division
			AND
				Order__c > :order
		];
		if (belowArticles.isEmpty()) {
			return;
		}
		//else...
		for(Category_Top_Article__c topArt : belowArticles) {
			topArt.Order__c--;
		}
		update belowArticles;
	}

	@AuraEnabled
	public static Boolean changeArticlesOrder(String topArt1Id, String topArt2Id) {
		if (String.isBlank(topArt1Id) || String.isBlank(topArt2Id)) {
			return false;
		}
		//else...
		List<Category_Top_Article__c> topArticles = [
			SELECT
				Order__c
			FROM
				Category_Top_Article__c
			WHERE
				Id = :topArt1Id OR
				Id = :topArt2Id
		];
		if (topArticles.size() != 2) {
			return false;
		}
		//else...
		Decimal temp = topArticles[0].Order__c;
		topArticles[0].Order__c = topArticles[1].Order__c;
		topArticles[1].Order__c = temp;
		update topArticles;
		return true;
	}

	@AuraEnabled
	public static List<String> addArticles(String category, String division, List<NewArticleWrapper> newArticles) {
		if (String.isBlank(category) || String.isBlank(division) || (newArticles == null) || newArticles.isEmpty()) {
			return null;
		}
		//else...
		List<Category_Top_Article__c> newTopArticles = new List<Category_Top_Article__c>();
		for(NewArticleWrapper newArt : newArticles) {
			Category_Top_Article__c newCatArticle = new Category_Top_Article__c(
				DataCategoryName__c = category,
				DivisionName__c = division,
				KnoledgeArticleId__c = newArt.knoledgeArticleId,
				Order__c = newArt.order
			);
			newTopArticles.add(newCatArticle);
		}
		insert newTopArticles;
		List<String> result = new List<String>();
		for(Category_Top_Article__c insertedArt : newTopArticles) {
			result.add(insertedArt.Id);
		}
		return result;
	}

	public class NewArticleWrapper {
		@AuraEnabled
		public String knoledgeArticleId {get; set;}
		@AuraEnabled
		public Integer order {get; set;}
	}

	public class FaqCategoriesInfo {
		@AuraEnabled
		public Integer maxArticles {get; set;}
		@AuraEnabled
		public Boolean restrictTopArticles {get; set;}
		@AuraEnabled
		public List<DataCategoryWrapper> categories {get; set;}
		@AuraEnabled
		public String commonDivision {get; set;}
		@AuraEnabled
		public List<DataCategoriesHelper.DataCategoryWrapper> divisions {get; set;}
		@AuraEnabled
		public Integer selDivisionIndex {get; set;}
		

		public FaqCategoriesInfo(Integer maxArticles, 
								Boolean restrictTopArticles,
								String commonDivision) {
			this.maxArticles = maxArticles;
			this.restrictTopArticles = restrictTopArticles;
			this.commonDivision = commonDivision;
		}
	}

}