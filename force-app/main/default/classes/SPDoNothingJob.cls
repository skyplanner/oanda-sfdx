/**
 * @File Name          : SPDoNothingJob.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2023, 3:55:22 PM
**/
public without sharing class SPDoNothingJob implements Queueable {

	public void execute(QueueableContext context) {
		System.attachFinalizer(new JobFinalizer());
		System.debug('SPDoNothingJob.execute');
	}

	// *************************************************************

	public class JobFinalizer extends SPBaseFinalizer {

		public override void onSuccess() {
			System.debug('JobFinalizer.onSuccess');
		}
	}
	
}