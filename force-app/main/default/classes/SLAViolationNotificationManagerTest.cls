/**
 * @File Name          : SLAViolationNotificationManagerTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/5/2024, 11:38:36 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/29/2024, 4:49:38 PM   aniubo     Initial Version
 **/
@isTest
private class SLAViolationNotificationManagerTest {
	@testSetup
	private static void testSetup() {
		OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createHvcAccount(
            OmnichanelConst.OANDA_CORPORATION
        );
		SPDataInitManager initManager = new SPDataInitManager();

		ServiceTestDataFactory.createAccount1(initManager);
		initManager.setFieldValues(
			ServiceTestDataKeys.CASE_1,
			new Map<String, Object>{
				'Inquiry_Nature__c' => 'Funding & Withdrawal',
				'Milestone_Violated__c' => 'Chat Case Tier 1 - Resolution Time',
				'Tier__c' => OmnichannelCustomerConst.TIER_1,
				'Chat_Language_Preference__c' => 'English',
				'Live_or_Practice__c' => 'Live',
				'Origin' => SLAConst.CASE_ORIGIN_EMAIL_API
			}
		);
		OmnichanelRoutingTestDataFactory.prepareEntitlementSettings();
		ServiceTestDataFactory.createCase1(initManager);
		ServiceTestDataFactory.createSettings1(initManager);
		initManager.storeData();
	}
	@isTest
	private static void testCaseSLAViolationNotifier() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();

		Map<String, Milestone_Time_Settings__mdt> milestones = MilestoneUtils.getSettingsFilterByViolationType(
			new List<String>{ SLAConst.AHT_EXCEEDED_VT }
		);
		Id caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);

		Case caseRecord = getCase(caseId);
		Boolean hasError = false;
		// Actual test
		Test.startTest();
		try {
			SLAViolationNotificationManager notificationManager = SLAViolationNotificationManagerFactory.createNotificationManager(
				SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT,
				SLAConst.SLAViolationType.AHT,
				SLAConst.AgentSupervisorModel.DIVISION,
				milestones,
				new FakeSupervisorProviderCreator()
			);
			Milestone_Time_Settings__mdt setting = milestones.get(
				caseRecord.Milestone_Violated__c
			);
			SLAMilestoneViolationInfo result = new SLAMilestoneViolationInfo(
				caseRecord.Id,
				caseRecord.CaseNumber,
				setting
			);
			result.isPersistent = true;
			result.division = Constants.DIVISIONS_OANDA_CANADA;
			result.inquiryNature = caseRecord.Inquiry_Nature__c;
			result.Tier = caseRecord.Tier__c;
			result.language = String.isNotBlank(
					caseRecord.Chat_Language_Preference__c
				)
				? caseRecord.Chat_Language_Preference__c
				: caseRecord.Language_Preference__c;
			result.liveOrPractice = caseRecord.Live_or_Practice__c;
			result.caseOrigin = caseRecord.Origin;
			result.notificationObjectType = SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT;
			notificationManager.sendNotifications(
				new List<SLAMilestoneViolationInfo>{ result }
			);
		} catch (Exception ex) {
			hasError = true;
		}

		Test.stopTest();
		Assert.areEqual(
			false,
			hasError,
			'No Error occurred while sending notification'
		);

		// Asserts
	}
	private static Case getCase(Id caseId) {
		List<Case> cases = [
			SELECT
				Id,
				CaseNumber,
				Division__c,
				Chat_Division__c,
				Chat_Language_Preference__c,
				Language_Preference__c,
				Inquiry_Nature__c,
				Milestone_Violated__c,
				Tier__c,
				Live_or_Practice__c,
				Origin
			FROM Case
			WHERE Id = :caseId
		];
		return cases.isEmpty() ? null : cases.get(0);
	}
}