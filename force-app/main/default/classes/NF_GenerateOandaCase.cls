/**
 * Created by Neuraflash LLC on 03/07/18.
 * Implementation : Chatbot
 * Summary : Create a case record or lead & case record based on Account Object search result
 * Details : 1. Searches for a account by email or souid provided.
 *           2. Creates a new case if found an account from search filters.
 *           3. Creates both case & lead record  if there is no account associated with account .
 *
 */
global without sharing class NF_GenerateOandaCase implements nfchat.PreInvokeClassAIMethodService {
    public Case caseCheck;
    public Boolean isDeleteAccountIntentTriggered = false;
    public Map<String, DialogFlow_Parameter__mdt> customMetaDataMap;
    public DialogFlow_Parameter__mdt parametersEntity;
    public NF_GenerateOandaCase() {
        caseCheck = new Case();
    }

    public List<String> ReturnStringResponse(nfchat.AIParameters data) {
        DialogFlow_Parameter__mdt parametersEntity = new DialogFlow_Parameter__mdt();
        String source = label.Chatbot;

        Map<String, Object> paramsMap = null;
        if (String.isNotEmpty(data.params)) {
            paramsMap = (Map<String, Object>) JSON.deserializeUntyped(
                data.params
            );
            System.debug('>>NF_GenerateOandaCase: paramsMap=' + paramsMap);
        }

        nfchat__Chat_Log__c log = [
            SELECT
                Id,
                Name,
                nfchat__First_Name__c,
                nfchat_Case_Description__c,
                nfchat__Session_Id__c,
                nfchat_Account_Type__c,
                nfchat_Username__c,
                nfchat__Email__c,
                Language_Preference__c,
                Language_Corrected__c,
                Case_Queue_Name__c,
                Case_Type__c,
                Authenticated__c,
                Case_Sub_Type__c,
                Total_Savings__c,
                Risk_Limit__c,
                Income_Source__c,
                Outcome__c,
                nfchat__Live_Agent_Availability_Status__c,
                Inquiry_Nature__c,
                Spot_Crypto__c,
                nfchat__Live_Chat_Transcript__c
            FROM nfchat__Chat_Log__c
            WHERE nfchat__Session_Id__c = :data.sessionId
            LIMIT 200
        ];
        System.debug('>>NF_GenerateOandaCase: nfchat__Chat_Log__c log=' + log);

        customMetaDataMap = new Map<String, DialogFlow_Parameter__mdt>();
        for (DialogFlow_Parameter__mdt record : [
            SELECT
                MasterLabel,
                Intent_Name__c,
                chatQueue__c,
                inquiryNature__c,
                inquiryType__c,
                recordType__c,
                subType__c
            FROM DialogFlow_Parameter__mdt
        ]) {
            customMetaDataMap.put(record.Intent_Name__c.toLowerCase(), record);
        }
        //Iterate through all chat log details to get the last business intent hit, last intent reached is first
        nfchat__Chat_Log_Detail__c[] chatLogDetails = [
            SELECT
                Id,
                nfchat__Request__c,
                nfchat__First_Recognition_Result__c,
                nfchat__Intent_Name__c,
                nfchat__Dialog_Turn__c
            FROM nfchat__Chat_Log_Detail__c
            WHERE nfchat__Chat_Log__c = :log.id
            ORDER BY CreatedDate DESC
        ];

        for (nfchat__Chat_Log_Detail__c chatLogDetail : chatLogDetails) {
            if (!String.isEmpty(chatLogDetail.nfchat__Intent_Name__c)) {
                if (
                    chatLogDetail.nfchat__Intent_Name__c ==
                    'business.account.delete'
                ) {
                    isDeleteAccountIntentTriggered = true;
                }
                DialogFlow_Parameter__mdt parametersEntityTemp = new DialogFlow_Parameter__mdt();
                //Get the metadata configuration for this intent if it's a business intent
                if (
                    chatLogDetail.nfchat__Intent_Name__c.toLowerCase()
                        .indexOf('business.') == 0
                ) {
                    parametersEntityTemp = customMetaDataMap.get(
                        chatLogDetail.nfchat__Intent_Name__c.toLowerCase()
                    );
                    if (parametersEntityTemp != null) {
                        parametersEntity = parametersEntityTemp;
                        break;
                    }
                }
            }
        }
        //Check if no matching business intent configuration found - fallback to bot.welcome
        if (parametersEntity == null) {
            parametersEntity = customMetaDataMap.get(label.Welcome_Intent_Name);
        }
        System.debug(
            '>>NF_GenerateOandaCase: parametersEntity=' + parametersEntity
        );

        List<Account> AccountList = [
            SELECT id
            FROM Account
            WHERE
                PersonEmail = :log.nfchat__Email__c
                AND recordType.name = :label.Person_Account
            LIMIT 1
        ];
        List<Lead> LeadList = [
            SELECT Id
            FROM Lead
            WHERE Email = :log.nfchat__Email__c
            ORDER BY CreatedDate ASC NULLS FIRST
            LIMIT 1
        ];
        List<Contact> contactList = new List<Contact>();
        if (AccountList.size() > 0) {
            contactList = [
                SELECT id, email
                FROM contact
                WHERE accountId = :AccountList[0].id
            ];
        }
        List<Case> caseFilteredBySessionId = [
            SELECT id, CaseNumber
            FROM case
            WHERE Session_Id__c = :data.sessionId
        ];

        List<Group> caseQueueList = new List<Group>();
        //Get the Queue to which the Case needs to be assigned to from the Chat Log record. The Case Matrix logic populates this on the Chat Log.
        if (String.isNotBlank(log.Case_Queue_Name__c)) {
            caseQueueList = [
                SELECT Id, DeveloperName
                FROM Group
                WHERE DeveloperName = :log.Case_Queue_Name__c
            ];
        }

        //If Queue not found get the queue from custom metadata or fallback to CX Cases
        if (caseQueueList.isEmpty()) {
            if (
                parametersEntity != null &&
                String.isNotBlank(parametersEntity.chatQueue__c)
            ) {
                caseQueueList = [
                    SELECT Id, Name
                    FROM Group
                    WHERE DeveloperName = :parametersEntity.chatQueue__c
                ];
            } else {
                caseQueueList = [
                    SELECT Id, Name
                    FROM Group
                    WHERE DeveloperName = :label.CX_Cases
                ];
            }
        }
        List<User> users = [
            SELECT id
            FROM User
            WHERE name = :label.System_user
            LIMIT 200
        ];

        Case newCase = null;

        if (caseFilteredBySessionId.isEmpty()) {
            if (!AccountList.isEmpty()) {
                //method call for case
                List<DialogFlow_Parameter__mdt> ParametersList = new List<DialogFlow_Parameter__mdt>();
                ParametersList.add(parametersEntity);
                newCase = createCaseRecord(
                    log,
                    AccountList,
                    contactList,
                    caseQueueList,
                    '',
                    ParametersList,
                    paramsMap
                );
            } else {
                if (!LeadList.isEmpty()) {
                    //update lead and create case record
                    List<DialogFlow_Parameter__mdt> ParametersList = new List<DialogFlow_Parameter__mdt>();
                    ParametersList.add(parametersEntity);
                    newCase = createCaseRecord(
                        log,
                        AccountList,
                        contactList,
                        caseQueueList,
                        LeadList[0].id,
                        ParametersList,
                        paramsMap
                    );
                } else {
                    //method call for lead
                    Lead leadRec = createLeadRecord(users, log);
                    List<DialogFlow_Parameter__mdt> ParametersList = new List<DialogFlow_Parameter__mdt>();
                    ParametersList.add(parametersEntity);
                    if (leadRec.id != null) {
                        //method call for case
                        newCase = createCaseRecord(
                            log,
                            AccountList,
                            contactList,
                            caseQueueList,
                            leadRec.id,
                            ParametersList,
                            paramsMap
                        );
                    } else {
                        newCase = createCaseRecord(
                            log,
                            AccountList,
                            contactList,
                            caseQueueList,
                            '',
                            ParametersList,
                            paramsMap
                        );
                    }
                }
            }
        }
        List<String> tempResultFormatter = new List<String>();

        if (paramsMap != null) {
            String promptMessage = (String) paramsMap.get(label.prompt);
            if (caseCheck.id != null) {
                promptMessage = promptMessage.replace(
                    label.CASENUMBER,
                    caseCheck.CaseNumber
                );
                tempResultFormatter.add(promptMessage);
            } else {
                promptMessage = promptMessage.replace(
                    label.CASENUMBER,
                    label.Error
                );
                tempResultFormatter.add(promptMessage);
            }
        }

        updateChatLog(log, newCase);
        addChatLogDetails(log, chatLogDetails);

        return tempResultFormatter;
    }

    /*
     * Custom fields used by SkyPlanner for routing
     */
    public Boolean updateChatLog(nfchat__Chat_Log__c chatLog, Case newCase) {
        System.debug('!NF_GenerateOandaCase: updateChatLog');

        Boolean updateLog = false;

        if (chatLog == null) {
            return false;
        }

        System.debug(
            '!NF_GenerateOandaCase: updateChatLog: chatLog=' + chatLog
        );

        try {
            if (newCase != null) {
                System.debug(
                    '!NF_GenerateOandaCase: updateChatLog: newCase.Inquiry_Nature__c=' +
                    newCase.Inquiry_Nature__c
                );
                System.debug(
                    '!NF_GenerateOandaCase: updateChatLog: chatLog.Inquiry_Nature__c=' +
                    chatLog.Inquiry_Nature__c
                );

                if (String.isNotEmpty(newCase.Inquiry_Nature__c)) {
                    chatLog.Inquiry_Nature__c = newCase.Inquiry_Nature__c;
                }

                if (String.isNotEmpty(newCase.Chat_Type_of_Account__c)) {
                    chatLog.Chat_Type_of_Account__c = newCase.Chat_Type_of_Account__c;
                }

                if (String.isNotEmpty(newCase.Type__c)) {
                    chatLog.Type__c = newCase.Type__c;
                }

                if (newCase.Account != null) {
                    chatLog.Is_HVC__c = newCase.Account.Is_High_Value_Customer__c
                        ? 'Yes'
                        : 'No';
                }

                update chatLog;

                updateLog = true;
            }
        } catch (Exception e) {
            System.debug(
                'NF_GenerateOandaCase: unable to update chatLog exception=' + e
            );
        }

        return updateLog;
    }

    public Case createCaseRecord(
        nfchat__Chat_Log__c log,
        List<Account> accounts,
        List<Contact> contacts,
        List<Group> groups,
        String leadId,
        List<DialogFlow_Parameter__mdt> ParameterListData,
        Map<String, Object> paramsMap
    ) {
        String caseRecordTypeId = '';
        Case caseRecord = new Case();

        try {
            if (isDeleteAccountIntentTriggered) {
                caseRecord.Subject = label.Chatbot_Account_Deletion_Request;
            } else {
                caseRecord.Subject = label.Chatbot_Service_Request;
            }

            if(log != null && 'Available'.equalsIgnoreCase(log.nfchat__Live_Agent_Availability_Status__c)){
                caseRecord.Origin = 'Chatbot - Case';
            }
            else{
                caseRecord.Origin = 'Chatbot - Email';
            }

            caseRecord.Priority = label.Medium;
            caseRecord.Session_Id__c = log.nfchat__Session_Id__c;

            if (!accounts.isEmpty()) {
                caseRecord.AccountId = accounts[0].id;
            }
            if (!contacts.isEmpty()) {
                caseRecord.ContactId = contacts[0].id;
            }
            
            if (ParameterListData[0].recordType__c != null) {
                System.debug('using recordType');
                caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                    .get(ParameterListData[0].recordType__c)
                    .getRecordTypeId();
                caseRecord.Inquiry_Nature__c = ParameterListData[0]
                    .inquiryNature__c;
                caseRecord.Type__c = ParameterListData[0].inquiryType__c;
                caseRecord.Subtype__c = ParameterListData[0].subType__c;
            } else {
                caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                    .get(label.Support)
                    .getRecordTypeId();
                System.debug('else try support recordType');
                System.debug('log.Inquiry_Nature__c='+log.Inquiry_Nature__c);
                //OAN-241: if the chat log has an inquiry nature field, use it on the Case
                if (log != null && String.isNotBlank(log.Inquiry_Nature__c)) {
                    caseRecord.Inquiry_Nature__c = log.Inquiry_Nature__c;
                } else {
                    caseRecord.Inquiry_Nature__c = label.Other;
                }
                System.debug('set caseRecord.Inquiry_Nature__c='+caseRecord.Inquiry_Nature__c);

                //OAN-274: defaulting to General Service is no longer needed
                caseRecord.Subtype__c = '';
            }

            System.debug(
                '!NF_GenerateOandaCase.createCaseRecord: set caseRecord.Inquiry_Nature__c=' +
                caseRecord.Inquiry_Nature__c
            );

            caseRecord.Chat_fxTrade_Account_No_Username__c = log.nfchat_Username__c;
            caseRecord.Chat_Email__c = log.nfchat__Email__c;
            caseRecord.Chat_Type_of_Account__c = log.nfchat_Account_Type__c;

            //OAN-200: 'Demo' selection should result in 'Practice'
            if ('Live'.equalsIgnoreCase(log.nfchat_Account_Type__c)) {
                caseRecord.Live_or_Practice__c = 'Live';
            } else if (
                'Demo'.equalsIgnoreCase(log.nfchat_Account_Type__c) ||
                'Practice'.equalsIgnoreCase(log.nfchat_Account_Type__c)
            ) {
                caseRecord.Live_or_Practice__c = 'Practice';
            }

            caseRecord.RecordTypeId = caseRecordTypeId;
            caseRecord.nfchat__Chat_Log__c = log.id;

            // *** Phase 2 - new field updates ***
            //User_Authenticated__c
            if (String.isNotBlank(log.Authenticated__c))
                caseRecord.User_Authenticated__c = log.Authenticated__c.equalsIgnoreCase(
                        'true'
                    )
                    ? true
                    : false;
            //Type__c
            if (String.isNotBlank(log.Case_Type__c))
                caseRecord.Type__c = log.Case_Type__c;
            //Chat_Language_Preference__c
            if (String.isNotBlank(log.Language_Preference__c))
                caseRecord.Chat_Language_Preference__c = log.Language_Preference__c;
            //Language_Corrected__c
            if (String.isNotBlank(log.Language_Corrected__c))
                caseRecord.Language_Corrected__c = log.Language_Corrected__c;
            //Subtype__c
            if (String.isNotBlank(log.Case_Sub_Type__c))
                caseRecord.Subtype__c = log.Case_Sub_Type__c;

            if (!String.isEmpty(log.nfchat_Case_Description__c)) {
                caseRecord.Description = log.nfchat_Case_Description__c;
            } else {
                caseRecord.Description = label.Please_see_chat_transcript;
            }

            if (!groups.isEmpty()) {
                caseRecord.OwnerId = groups[0].Id;
            }

            if (String.isNotBlank(leadId)) {
                caseRecord.Lead__c = leadId;
            }

            //OAN-226: Adding "Spot Crypto" flag to case if the user entered a crypto flow
            if (log.Spot_Crypto__c != null) //Sanity check, should be false by default
                caseRecord.Spot_Crypto__c = Boolean.valueOf(log.Spot_Crypto__c);

            //OAN-172
            System.debug('>>NF_GenerateOandaCase: paramsMap=' + paramsMap);
            if (paramsMap != null) {
                Boolean isEscalation =
                    paramsMap.get('escalation') != null &&
                    Boolean.valueOf(paramsMap.get('escalation'));
                System.debug(
                    '>>NF_GenerateOandaCase: isEscalation=' + isEscalation
                );

                caseRecord.Email_Case_Chatbot__c = isEscalation ? false : true;
                System.debug(
                    '>>NF_GenerateOandaCase: Email_Case_Chatbot__c=' +
                    caseRecord.Email_Case_Chatbot__c
                );
            }
        } catch (Exception e) {
            System.debug(
                '>>NF_GenerateOandaCase: error populating Case' + e.getMessage()
            );
        }

        if (caseRecord.id == null) {
            try {
                insert caseRecord;
                assignSubjectOnCase(caseRecord.id, log.id);
                List<nfchat__Chat_Log_Detail__c> chatLogDetailList = [
                    SELECT id, nfchat__Class_Invocations__c
                    FROM nfchat__Chat_Log_Detail__c
                    WHERE nfchat__Chat_Log__c = :log.id
                    ORDER BY CreatedDate ASC
                    LIMIT 200
                ];
                if (!chatLogDetailList.isEmpty()) {
                    chatLogDetailList[chatLogDetailList.size() - 1]
                        .nfchat__Class_Invocations__c = label.GenerateCase;
                    update chatLogDetailList;
                }
                caseCheck = [
                    SELECT Lead__c, CaseNumber, Email_Case_Chatbot__c
                    FROM Case
                    WHERE Id = :caseRecord.id
                ];
                System.debug('>>NF_GenerateOandaCase: caseCheck=' + caseCheck);
            } catch (DmlException e) {
                NF_GenerateOandaCase.createDebugLog(e);
            }
        }

        return caseRecord;
    }

    public Lead createLeadRecord(
        List<User> userList,
        nfchat__Chat_Log__c chatLogRecord
    ) {
        Id leadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName()
            .get(label.Standard_Lead)
            .getRecordTypeId();
        Lead leadRecord = new Lead();
        if (leadRecordTypeId != null) {
            leadRecord.RecordTypeId = leadRecordTypeId;
        }
        leadRecord.Email = chatLogRecord.nfchat__Email__c;
        leadRecord.lastName = chatLogRecord.nfchat__Email__c;
        leadRecord.nfchat__Chat_Log__c = chatLogRecord.id;
        if (!userList.isEmpty()) {
            leadRecord.OwnerId = userList[0].id;
        }
        try {
            insert leadRecord;
            List<nfchat__Chat_Log_Detail__c> chatLogDetailList = [
                SELECT id, Lead_Invocation_Class__c
                FROM nfchat__Chat_Log_Detail__c
                WHERE nfchat__Chat_Log__c = :chatLogRecord.id
                ORDER BY CreatedDate ASC
                LIMIT 200
            ];
            if (!chatLogDetailList.isEmpty()) {
                chatLogDetailList[chatLogDetailList.size() - 1]
                    .Lead_Invocation_Class__c = label.GenerateLead;
                update chatLogDetailList;
            }
        } catch (DmlException e) {
            System.debug('Exception-->' + e.getMessage());
            NF_GenerateOandaCase.createDebugLog(e);
            Lead exceptionLead = new Lead();
            return exceptionLead;
        }
        return leadRecord;
    }
    @future
    public static void assignSubjectOnCase(String caseID, String LogId) {
        System.debug('NF_GenerateOandaCase: assignSubjectOnCase');
        Case caseObj = [
            SELECT Id, subject, User_Authenticated__c
            FROM Case
            WHERE id = :caseID AND nfchat__Chat_Log__c = :LogId
        ];

        nfchat__Chat_Log__c logObj = [
            SELECT
                id,
                Authenticated__c,
                EmailCheck__c,
                Annual_Income__c,
                Net_Worth__c,
                Liquid_Net_Worth__c,
                Total_Savings__c,
                Current_Employer__c,
                Job_Title__c,
                nfchat__Company__c,
                Products__c,
                Industry__c,
                Income_Source__c,
                Risk_Limit__c
            FROM nfchat__Chat_Log__c
            WHERE id = :LogId
        ];

        if (
            caseObj.id != null &&
            logObj.id != null &&
            logObj.EmailCheck__c == 'true'
        ) {
            caseObj.subject = label.Chatbot_Service;
            caseObj.Email_Followup_Requested__c = true;
        }
        if (String.isNotEmpty(logObj.Authenticated__c)) {
            caseObj.User_Authenticated__c = logObj.Authenticated__c.equalsIgnoreCase(
                    'true'
                )
                ? true
                : false;
        }
        update caseObj;
        String caseComent = NF_GenerateOandaCase.createCaseComment(
            logObj,
            caseObj
        );
    } /* Added & updated for email followup check*/
    /**
     * @description -   Creates a new Case Comment and adds it to the Case record
     * @return      -   String
     */
    public static String createCaseComment(
        nfchat__Chat_Log__c chatLog,
        Case relatedCases
    ) {
        CaseComment comment = new CaseComment();
        comment.ParentId = relatedCases.Id;
        comment.CommentBody = NF_GenerateOandaCase.getCommentBody(chatLog);
        insert comment;

        return 'Case Comment Added';
    }

    /**
     * @description -   Creates the Case Comment Body
     * @return      -   String
     */
    public static String getCommentBody(nfchat__Chat_Log__c chatLog) {
        String commentBody = '\n \n See Chatbot Chat Log transcript for more details\n \n';

        commentBody += getCommentValue('Current Employer: ',    chatLog.Current_Employer__c);
        commentBody += getCommentValue('Company: ',             chatLog.nfchat__Company__c);
        commentBody += getCommentValue('Industry: ',            chatLog.Industry__c);
        commentBody += getCommentValue('Products / Services: ', chatLog.Products__c);
        commentBody += getCommentValue('Job Title: ',           chatLog.Job_Title__c);
        commentBody += getCommentValue('Annual Income: ',       chatLog.Annual_Income__c);
        commentBody += getCommentValue('Net Worth: ',           chatLog.Net_Worth__c) ;
        commentBody += getCommentValue('Source: ',              chatLog.Income_Source__c);
        commentBody += getCommentValue('Liquid Net Worth: ',    chatLog.Liquid_Net_Worth__c);
        commentBody += getCommentValue('Risk Limit: ',          chatLog.Risk_Limit__c);
        commentBody += getCommentValue('Total Savings and Liquid Investments: ',       chatLog.Total_Savings__c);

        return commentBody;
    }

    public static String getCommentValue(
        String commentLabel,
        String commentValue
    ) {
        if (String.isEmpty(commentValue)) {
            return '';
        }

        return commentLabel + String.valueOf(commentValue) + '\n';
    }

    public static void createDebugLog(Exception e) {
        nfchat__Debug_Log__c debug_log = new nfchat__Debug_Log__c();
        debug_log.Name =
            e.getTypeName().subString(0, 10) +
            ' ' +
            e.getMessage().substring(0, 10);
        debug_log.nfchat__Error_Message__c = String.valueOf(e.getLineNumber());
        debug_log.nfchat__Exception_Data__c = e.getMessage();
        debug_log.nfchat__Stack_Trace__c = e.getStackTraceString();
        debug_log.nfchat__Type__c = e.getTypeName();
        insert debug_log;
    }

    private static void addChatLogDetails(
        nfchat__Chat_Log__c log,
        nfchat__Chat_Log_Detail__c[] chatLogDetails
    ) {
        System.debug('>>NF_GenerateOandaCase.addChatLogDetails');
        if (log == null || chatLogDetails == null) {
            return;
        }

        System.debug(
            '>>NF_GenerateOandaCase.addChatLogDetails: chatLogDetails=' +
            chatLogDetails
        );

        try {
            nfchat__Chat_Log_Detail__c lastBusinessIntentDetail = getLastBusinessIntent(
                chatLogDetails
            );
            if (lastBusinessIntentDetail != null) {
                Decimal lastTurn = lastBusinessIntentDetail.nfchat__Dialog_Turn__c;
                String lastIntentName = lastBusinessIntentDetail.nfchat__Intent_Name__c;

                nfchat__Chat_Log_Detail__c newDetail = createChatLogDetail(
                    log,
                    lastBusinessIntentDetail
                );
                System.debug(
                    '>>NF_GenerateOandaCase.addChatLogDetails: newDetail=' +
                    newDetail
                );

                if (newDetail != null) {
                    insert newDetail;
                    System.debug(
                        '>>NF_GenerateOandaCase.addChatLogDetails: inserted newDetail!'
                    );
                }
            }
        } catch (Exception e) {
            System.debug('>>NF_GenerateOandaCase: error=' + e.getMessage());
        }
    }

    @TestVisible
    private static nfchat__Chat_Log_Detail__c createChatLogDetail(
        nfchat__Chat_Log__c log,
        nfchat__Chat_Log_Detail__c lastBusinessIntent
    ) {
        nfchat__Chat_Log_Detail__c detail1 = null;

        Decimal lastTurn = lastBusinessIntent.nfchat__Dialog_Turn__c;
        System.debug(
            '>>NF_GenerateOandaCase.createChatLogDetail: lastTurn=' + lastTurn
        );
        String lastIntentName = lastBusinessIntent.nfchat__Intent_Name__c;
        System.debug(
            '>>NF_GenerateOandaCase.createChatLogDetail: lastIntentName=' +
            lastIntentName
        );
        System.debug(
            '>>NF_GenerateOandaCase.createChatLogDetail: log.Total_Savings__c=' +
            log.Total_Savings__c
        );

        String request = '';
        if (lastIntentName.contains('business.deposit.status')) {
            if (String.isNotBlank(log.Total_Savings__c)) {
                request = log.Total_Savings__c;
            } else if (String.isNotEmpty(log.Risk_Limit__c)) {
                request = log.Risk_Limit__c;
            } else if (String.isNotEmpty(log.Income_Source__c)) {
                request = log.Income_Source__c;
            }
        }
        System.debug(
            '>>NF_AddChatLogDetails.createChatLogDetail: request=' + request
        );

        if (String.isNotEmpty(request)) {
            detail1 = new nfchat__Chat_Log_Detail__c();
            detail1.nfchat__Chat_Log__c = log.Id;
            detail1.nfchat__Dialog_Turn__c = lastTurn;
            detail1.nfchat__Intent_Name__c = lastIntentName;
            detail1.nfchat__Input_Mode__c = 'Text';
            detail1.nfchat__Request__c = request;
        }

        System.debug(
            '>>NF_AddChatLogDetails.createChatLogDetail: detail1=' + detail1
        );

        return detail1;
    }

    @TestVisible
    private static nfchat__Chat_Log_Detail__c getLastBusinessIntent(
        nfchat__Chat_Log_Detail__c[] chatLogDetails
    ) {
        nfchat__Chat_Log_Detail__c lastBusinessIntent = null;

        System.debug(
            '>>NF_AddChatLogDetails: chatLogDetails=' + chatLogDetails
        );
        if (chatLogDetails.size() > 0) {
            for (Integer i = 0; i < chatLogDetails.size(); i++) {
                if (
                    String.isNotEmpty(chatLogDetails[i].nfchat__Intent_Name__c)
                ) {
                    if (
                        lastBusinessIntent == null &&
                        chatLogDetails[i]
                            .nfchat__Intent_Name__c.indexOf('business.') == 0
                    ) {
                        lastBusinessIntent = chatLogDetails[i];
                        break;
                    }
                }
            }

            System.debug(
                '>>NF_AddChatLogDetails: lastBusinessIntent=' +
                lastBusinessIntent
            );
        }

        return lastBusinessIntent;
    }
}