public without sharing class BIWebService {
	public static final Integer TIME_OUT_MILLISECS = 60000;
	   
	private String userName;
	private String passwd;                          
	private String liveAcctEndPointURL = 'https://biapi.oanda.com:3443/biapi-salesforce/get-live-subaccount-data-by-fxtrade-user-id';
	private String demoAcctEndPointURL = 'https://biapi.oanda.com:3443/biapi-salesforce/get-game-subaccount-data-by-fxtrade-user-id';
	
	private String authorizationHeader;
	private Httprequest request;
	private Http http;
	
	public class SubAccount{
    	
    	public String fxtrade_user_id {get; set;}
        public Integer number_trades_yearly {get; set;}
        public Integer leverage {get; set;}
        public Decimal volume_trades_monthly {get; set;}
        public Decimal pl_yearly {get; set;}
        public boolean is_in_margin_call {get; set;}
        public Integer number_trades_weekly {get; set;}
        public String username {get; set;}
        public String type {get; set;}
        public Integer number_trades_monthly {get; set;}
        public Decimal pl_total {get; set;}
        public Decimal pl_weekly {get; set;}
        public String account_currency {get; set;}
        public Decimal volume_trades_yearly {get; set;}
        public Decimal volume_trades_weekly {get; set;}
        public Decimal balance {get; set;}
        public Decimal pl_monthly {get; set;}
        public String account_number {get; set;}
        public Decimal interest {get; set;}
        public Decimal volume_trades_total {get; set;}
        public String account_name {get; set;}
        public Integer number_trades_total {get; set;}
        public Decimal margin_used {get; set;}
        
        public SubAccount(){
        	is_in_margin_call = false;
        }
        
        public String getVolumeDetail(){
        	String result = ' Weekly: ';
        	
        	if(volume_trades_weekly != null && volume_trades_weekly != 0){
        		result += volume_trades_weekly;
        	}
            result += '<br>';
       
        	      result += 'Monthly: ';
        	      
        	if(volume_trades_monthly != null && volume_trades_monthly != 0){
        		result +=  volume_trades_monthly;
        	} 
        	result += '<br>';
        	
        	      result += ' Yearly: ';
        	if(volume_trades_yearly != null && volume_trades_yearly != 0){
        		result += volume_trades_yearly;
        	}
        	result += '<br>';
        	
        	      result += '  Total: ';
        	if(volume_trades_total != null && volume_trades_total  != 0){
        		result += volume_trades_total;
        	}
        	      
        	return result;
        }
        
        public String getPLDetail(){
        	String result = ' Weekly: ';
        	
        	if(pl_weekly != null && pl_weekly != 0){
        		result += pl_weekly;
        	}
        	result += '<br>';
        	
        	result += 'Monthly: ';
        	
        	if(pl_monthly != null && pl_monthly != 0){
        		result += pl_monthly;
        	}
        	result += '<br>';
        	
        	result += ' Yearly: ';
        	if(pl_yearly != null && pl_yearly != 0){
        		result += pl_yearly;
        	}
        	
        	result += '<br>';
        	
        	      result += '  Total: ';
        	if(pl_total != null && pl_total != 0){
        		result += pl_total;
        	}
        	
        	return result;
        }
        
        public String getTradeDetail(){
        	String result = ' Weekly: ';
        	
        	if(number_trades_weekly != null && number_trades_weekly != 0){
        		result += number_trades_weekly;
        	}
        	
        	result += '<br>';
        	
        	result += 'Monthly: ';
        	if(number_trades_monthly != null && number_trades_monthly != 0){
        		result += number_trades_monthly;
        	}
        	result += '<br>';
        	
        	result += ' Yearly: ';
        	if(number_trades_yearly != null && number_trades_yearly != 0){
        		result += number_trades_yearly;
        	}
        	result += '<br>';
        	
        	      result += '  Total: ';
        	if(number_trades_total != null && number_trades_total != 0){
        		result += number_trades_total;
        	}
        	
        	return result;
        }
    	
    }
	
	public BIWebService(){
		//load configuration
		BIWebServiceSetting__c setting = BIWebServiceSetting__c.getValues('BI Web Services');
		userName = setting.User_Name__c;
		passwd = setting.Password__c;
		liveAcctEndPointURL = setting.Live_Account_End_Point__c;
		demoAcctEndPointURL = setting.Demo_Account_End_Point__c;
		
		Blob headerValue = Blob.valueOf(userName + ':' + passwd);
        authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        
        request = new HttpRequest();
        http = new Http();
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', authorizationHeader);
        
        //request.setClientCertificateName('BI_Web_Services');
	}
	
	
	public List<SubAccount> queryLiveFxTrade(String userId){
		System.debug('Query Live FxTrade for user Id: ' + userId);
		
		request.setEndpoint(liveAcctEndPointURL);
		
		String body = createQueryParam(userId);
		System.debug('live fxTrade request body: ' + body);
		
		request.setBody(body);
		request.setTimeout(TIME_OUT_MILLISECS);
		
		System.debug('live fxTrade request endpoint: ' + request.getEndpoint());
		
		List<SubAccount> subAccounts;
		
		try{
			HttpResponse response = http.send(request); 
			
			String responseBody = response.getBody();
			System.debug('responseBody: '+ responseBody);
			
			subAccounts = extractSubAccounts(responseBody);
			
		}catch(System.CalloutException e){
			System.debug('ERROR: ' + e);
		}
		
		return subAccounts;
	}
	
	public List<SubAccount> queryDemoFxTrade(String userId){
		System.debug('Query Demo FxTrade for user Id: ' + userId);
		
		request.setEndpoint(demoAcctEndPointURL);
		
		String body = createQueryParam(userId);
		System.debug('Demo fxTrade request body: ' + body);
		
		request.setBody(body);
		request.setTimeout(TIME_OUT_MILLISECS);
		
		System.debug('Demo fxTrade request endpoint: ' + request.getEndpoint());
		
		List<SubAccount> subAccounts;
		
		try{
			HttpResponse response = http.send(request); 
			
			String responseBody = response.getBody();
			System.debug('responseBody: '+ responseBody);
			
			subAccounts = extractSubAccounts(responseBody);
			
		}catch(System.CalloutException e){
			System.debug('ERROR: ' + e);
		}
		
		return subAccounts;
	}
	
	private String createQueryParam(String userId){
		String param = '{"fxtrade_user_id" : ';
		
		param = param + userId + '}';
		
		return param;
	}
	
	/**
	   Live fxTrade Schema model: 
	      [
			  {
			    "fxtrade_user_id": 0,
			    "number_trades_yearly": 0,
			    "leverage": 0,
			    "volume_trades_monthly": 0,
			    "pl_yearly": 0,
			    "is_in_margin_call": "string",
			    "number_trades_weekly": 0,
			    "username": "string",
			    "type": "string",
			    "number_trades_monthly": 0,
			    "pl_total": 0,
			    "pl_weekly": 0,
			    "currency": "string",
			    "volume_trades_yearly": 0,
			    "volume_trades_weekly": 0,
			    "balance": 0,
			    "pl_monthly": 0,
			    "account_number": "string",
			    "interest": 0,
			    "volume_trades_total": 0,
			    "account_name": "string",
			    "number_trades_total": 0,
			    "margin_used": 0
			  }
         ]
         
         
       Demo fxTrade Schema model:
       
         [
			  {
			    "fxtrade_user_id": 0,
			    "number_trades_yearly": 0,
			    "leverage": 0,
			    "volume_trades_monthly": 0,
			    "pl_yearly": 0,
			    "is_in_margin_call": "string",
			    "number_trades_weekly": 0,
			    "username": "string",
			    "type": "string",
			    "number_trades_monthly": 0,
			    "pl_total": 0,
			    "pl_weekly": 0,
			    "currency": "string",
			    "volume_trades_yearly": 0,
			    "volume_trades_weekly": 0,
			    "balance": 0,
			    "pl_monthly": 0,
			    "account_number": "string",
			    "interest": 0,
			    "volume_trades_total": 0,
			    "account_name": "string",
			    "number_trades_total": 0,
			    "margin_used": 0
			  }
         ]
	*/
	
	private List<SubAccount> extractSubAccounts(String responseBody){
		List<SubAccount> results = new List<SubAccount>();
		
		JSONParser parser = JSON.createParser(responseBody);

        while (parser.nextToken() != null) {

            if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                
                SubAccount sub = extractSingleSubAccount(parser);
                if(sub != null){
                	results.add(sub);
                }

            }
        }
        
        System.debug('Number of Sub Accounts: ' + results.size());
        return results;
	}
	
	private SubAccount extractSingleSubAccount(JSONParser parser){
		 SubAccount result = new SubAccount();
		 
		 while (parser.nextToken() != null) {

            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String fieldName = parser.getText();
                
                //advance to the value
                parser.nextToken();
                if(parser.getCurrentToken() == JSONToken.VALUE_NULL){
                	continue;
                }
                
                if(fieldName == 'fxtrade_user_id'){
                	result.fxtrade_user_id = parser.getText();
                }else if(fieldName == 'number_trades_yearly'){
                	result.number_trades_yearly = parser.getIntegerValue();
                }else if(fieldName == 'leverage'){
                	result.leverage = parser.getIntegerValue();
                }else if(fieldName == 'volume_trades_monthly'){
                	result.volume_trades_monthly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'pl_yearly'){
                	result.pl_yearly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'is_in_margin_call' && parser.getText() == 'Y'){
                	result.is_in_margin_call = true;
                }else if(fieldName == 'number_trades_weekly'){
                	result.number_trades_weekly = parser.getIntegerValue();
                }else if(fieldName == 'username'){
                	result.username  = parser.getText();
                }else if(fieldName == 'type'){
                	result.type  = parser.getText();
                }else if(fieldName == 'number_trades_monthly'){
                	result.number_trades_monthly = parser.getIntegerValue();
                }else if(fieldName == 'pl_total'){
                	result.pl_total = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'pl_weekly'){
                	result.pl_weekly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'currency'){
                	result.account_currency  = parser.getText();
                }else if(fieldName == 'volume_trades_yearly'){
                	result.volume_trades_yearly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'volume_trades_weekly'){
                	result.volume_trades_weekly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'balance'){
                	result.balance = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'pl_monthly'){
                	result.pl_monthly = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'account_number'){
                	result.account_number  = parser.getText();
                }else if(fieldName == 'interest'){
                	result.interest = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'volume_trades_total'){
                	result.volume_trades_total = formatCurrency(parser.getDecimalValue());
                }else if(fieldName == 'account_name'){
                	result.account_name  = parser.getText();
                }else if(fieldName == 'number_trades_total'){
                	result.number_trades_total = parser.getIntegerValue();
                }else if(fieldName == 'margin_used'){
                	result.margin_used = formatCurrency(parser.getDecimalValue());
                }

            }else if(parser.getCurrentToken() == JSONToken.END_OBJECT){
            	//end of this Sub Account object
            	break;
            }
        }
        
		return result;
	}
	
	
	private Decimal formatCurrency(Decimal input){
		Decimal result = input;
		
		if(result != null){
			result = result.setScale(2);
		}
		
		return result;
	}
    
}