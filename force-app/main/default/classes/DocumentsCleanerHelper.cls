/**
 * It is responsible for eliminating obsolete documents that 
 * are not associated with any article
 * @author Ariel Cantero, Skyplanner LLC
 * @since 07/10/2019
 */
global without sharing class DocumentsCleanerHelper {

	private static final String ONLINE_STATUS = 'Online';
	private static final String DRAFT_STATUS = 'Draft';
	private static final String ARCHIVED_STATUS = 'Archived';

	private static final String TAG_IMAGE_PROPERTY = '"tag":"img"';
	private static final String ITEMS_KEY = 'items';
	private static final String TAG_KEY = 'tag';
	private static final String FILE_URL_KEY = 'fileUrl';
	private static final String IMG_VALUE = 'img';
	private static final String ID_PARAM_MARK = '?id=';
	private static final String OID_PARAM_MARK = '&oid=';

	global static void findAndRemoveUselessDocs(Set<ID> docIdList) {
		List<Document> docList = [
			SELECT
				Id
			FROM
				Document
			WHERE
				Id IN :docIdList
		];
		findAndRemoveUselessDocs(docList);
	}

	global static void findAndRemoveUselessDocs(List<Document> docList) {
		if ((docList == null) || (docList.isEmpty())) {
			return;
		}
		//else...
		Map<ID,Document> docMap = new Map<ID,Document>(docList);
		excludeArticlesDocs(docMap, ONLINE_STATUS);
		excludeArticlesDocs(docMap, DRAFT_STATUS);
		excludeArticlesDocs(docMap, ARCHIVED_STATUS);
		if (docMap.isEmpty()) {
			return;
		}
		//else...
		//clone values and keys because originals are read only
		List<Document> docsToDelete = new List<Document>(docMap.values());
		
		//
		//Deleting images is disabled until an option is found not to delete shared images
		//

		// Set<ID> docsIdToDelete = new Set<ID>(docMap.keySet());
		// List<Document> docsWithBody = [
		// 	SELECT
		// 		Id, Body
		// 	FROM
		// 		Document
		// 	WHERE
		// 		Id in :docsIdToDelete
		// ];
		// for(Document docWithBody : docsWithBody) {
		// 	if (docWithBody.Body != null) {
		// 		String bodyStr = docWithBody.Body.toString();
		// 		if (String.isNotBlank(bodyStr)) {
		// 			Boolean hasImages = (bodyStr.indexOf(TAG_IMAGE_PROPERTY) != -1);
		// 			if (hasImages) {
		// 				Set<String> imgDocIdList = getImgDocsIds(bodyStr);
		// 				for(String imgDocId : imgDocIdList) {
		// 					if (docsIdToDelete.add(imgDocId)) {
		// 						docsToDelete.add(new Document(Id = imgDocId));
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		Database.delete(docsToDelete, false);
	}

	@TestVisible
	private static void excludeArticlesDocs(Map<ID,Document> docMap, String articleStatus) {
		if ((docMap == null) || docMap.isEmpty()) {
			return;
		}
		//else...
		Set<ID> docIdSet = docMap.keySet();
		String query = 
			'SELECT PreviewDocument__c ' +
			'FROM FAQ__kav ' +
			'WHERE PublishStatus = :articleStatus AND ' +
			'PreviewDocument__c IN :docIdSet';
		List<FAQ__kav> artList = Database.query(query);
		for(FAQ__kav art : artList) {
			docMap.remove(art.PreviewDocument__c);
		}
	}

	// private static Set<String> getImgDocsIds(String docBody) {
	// 	Set<String> result = new Set<String>();
	// 	String validJson = '{"translations":' + docBody + '}';
	// 	Map<String, Object> topObject = 
	// 		(Map<String, Object>)JSON.deserializeUntyped(validJson);
	// 	List<Object> translations = (List<Object>)topObject.get('translations');
	// 	for(Object traslationObj : translations) {
	// 		Map<String, Object> translation = (Map<String, Object>)traslationObj;
	// 		Object itemsObj = translation.get(ITEMS_KEY);
	// 		if (itemsObj != null) {
	// 			List<Object> items = (List<Object>)itemsObj;
	// 			for(Object itemObj : items) {
	// 				Map<String, Object> item = (Map<String, Object>)itemObj;
	// 				Object tagObj = item.get(TAG_KEY);
	// 				if (tagObj != null) {
	// 					String tag = (String)tagObj;
	// 					if (tag == IMG_VALUE) {
	// 						Object fileUrlObj = item.get(FILE_URL_KEY);
	// 						if (fileUrlObj != null) {
	// 							String fileUrl = (String)fileUrlObj;
	// 							System.debug('fileUrl: ' + fileUrl);
	// 							Integer pos = fileUrl.indexOf(ID_PARAM_MARK);
	// 							if (pos != -1) {
	// 								Integer beginPos = pos + ID_PARAM_MARK.length();
	// 								if (beginPos < fileUrl.length()) {
	// 									Integer endPos = fileUrl.indexOf(OID_PARAM_MARK, pos);
	// 									if (endPos != -1) {
	// 										String imageId = fileUrl.substring(beginPos, endPos);
	// 										System.debug('imageId: ' + imageId);
	// 										result.add(imageId);
	// 									}
	// 								} 
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	return result;
	// }

	global static String getUselessDocsCheckingQuery(String docsFolderId, Boolean onlyTwoWeeksRange) {
		if (String.isBlank(docsFolderId)) {
			return null;
		}
		//else...
		String query = 'SELECT Id ' + 
						'FROM Document ' +
						'WHERE FolderId = \'' + docsFolderId + '\' ' +
						'and ContentType = \'application/json\' ' +
						'and DeveloperName like \'' + DocumentManager.PREVIEW_PREFIX + '%\' ' +
						'and CreatedDate < YESTERDAY ';
		if (onlyTwoWeeksRange) {
			query += 'and CreatedDate >= LAST_WEEK ';
		}
		return query;
	}

}