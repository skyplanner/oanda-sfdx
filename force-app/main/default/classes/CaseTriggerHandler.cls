/**
 * @Description  : Trigger handler for Case sobject.
 * @Updated      : 2024-05-06
 **/
public inherited sharing class CaseTriggerHandler extends TriggerHandler {
    private final List<Case> triggerNew;
    private final Map<Id, Case> triggerNewMap;
    private final Map<Id, Case> triggerOldMap;

    private static final Id OTMS_SUPPORT_RECORD_TYPE_ID = RecordTypeUtil.getOTMSSupportCaseRecordTypeId();
    private static final Id OTMS_ONBOARDING_RECORD_TYPE_ID = RecordTypeUtil.getOTMSOnboardingCaseRecordTypeId();

    /**
     * @description Constructor.
     * @author Jakub Fik | 2024-05-06
     **/
    public CaseTriggerHandler() {
        this.triggerNew = (List<Case>) Trigger.new;
        this.triggerNewMap = (Map<Id, Case>) Trigger.newMap;
        this.triggerOldMap = (Map<Id, Case>) Trigger.oldMap;
    }

    /**
     * @description Override beforeInsert method.
     * @author Jakub Fik | 2024-05-06
     **/
    public override void beforeInsert() {
        VerificationCodeAssignment.verify(this.triggerNew);
        CaseTriggerHandlerHelper.performConditionalUpdates(this.triggerNew, null);
        CaseTriggerHandlerHelper.linkToAccountContactOrLead(this.triggerNew);
        CaseTriggerTierCalculator.calculateTierForTelephony(this.triggerNew);

        Set<Id> usersToExclude = CaseTriggerHandlerHelper.getUsersToExclude();

        for (Case c : this.triggerNew) {
            if(isOTMSNewDocumentUploaded(c)) {
                c.SuppliedEmail = CaseUtil.getEmailFromSubject(c.Subject);
                c.SuppliedName = c.SuppliedEmail;
            }
            if (isOwnerSpam(c) || isOTMSSubjectToClose(c)) {
                c.Close_Case__c = true;
            }
            if (c.Close_Case__c) {
                c.Status = Constants.CASE_STATUS_CLOSED;
                c.Auto_Closed__c = true;
            }
            CaseTriggerHandlerHelper.copyReasonFailed(c);
            CaseTriggerHandlerHelper.fillFirstOwnerUser(usersToExclude, c);
            this.populateTypeSummary(c);
            this.clearContact(c);
        }
        Formula.recalculateFormulas(this.triggerNew);
        CaseLeadService.createLeadIfNeeded(this.triggerNew);
    }

    /**
     * @description Override beforeUpdate method.
     * @author Jakub Fik | 2024-05-06
     **/
    public override void beforeUpdate() {
        Map<Id, Id> ownerUsersIdsByCase = new Map<Id, Id>();

        VerificationCodeAssignment.verify(this.triggerNew);
        CaseTriggerHandlerHelper.performConditionalUpdates(this.triggerNew, this.triggerOldMap.values());
        CaseTriggerHandlerHelper.linkToAccountContactOrLead(this.triggerNew);

        Set<Id> usersToExclude = CaseTriggerHandlerHelper.getUsersToExclude();

        for (Case c : this.triggerNew) {
            Case oldCase = this.triggerOldMap.get(c.Id);

            this.populateTypeSummary(c);

            CaseTriggerHandlerHelper.copyReasonFailed(c);
            CaseTriggerHandlerHelper.fillFirstOwnerUser(usersToExclude, c);
            CaseTriggerHandlerHelper.getCasesToTransfer(c, ownerUsersIdsByCase, this.triggerOldMap.get(c.Id));
            if (isOwnerChanged(c, oldCase.OwnerId) && isOwnerSpam(c)) {
                c.Close_Case__c = true;
            }
            if (isCloseCaseChanged(c, oldCase.Close_Case__c)) {
                c.Status = Constants.CASE_STATUS_CLOSED;
            }
            if (!UserUtil.getIntegrationProfileIds().contains(UserInfo.getProfileId())) {
                CaseTriggerHandlerHelper.validateCaseApproval(c, oldCase.Application_Status__c);
            }
            if(isOTMSSupportAndOwnerChangedToCX(c,oldCase.OwnerId, oldCase.RecordTypeId)){
                CaseTriggerHandlerHelper.changeRTToSupport(c);
            }
            if (isStatusChanged(c.Status, oldCase.Status) && c.Status==Constants.CASE_STATUS_CLOSED
                && c.RecordTypeId==OTMS_SUPPORT_RECORD_TYPE_ID) {
                CaseTriggerHandlerHelper.setTMSResolutionTime(c);
            }
        }

        CaseTriggerHandlerHelper.transferCases(this.triggerNew, ownerUsersIdsByCase);
    }

    /**
     * @description Override afterInsert method.
     * @author Jakub Fik | 2024-05-06
     **/
    public override void afterInsert() {
        List<Contact> contactsToUpdate = new List<Contact>();

        ComplyAdvantageHelper.linkCAOnboardingCases(this.triggerNew);
        CaseTriggerHandlerHelper.updateFxAcCACaseFlag(this.triggerNew);
        CaseTriggerHandlerHelper.updateLastUpdateDateField(this.triggerNewMap.keySet());
        if (CustomSettings.isCustomNotificationForSalesEnabled()) {
            CustomNotificationManager.sendNotificationsForOGMCases(this.triggerNew, this.triggerOldMap);
        }
        for (Case loopCase : this.triggerNew) {
            if (isOnboarding(loopCase)) {
                contactsToUpdate.add(
                    new Contact(Id = loopCase.ContactId, Info_Required__c = loopCase.Info_Required__c)
                );
            }
        }
        if (!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }
    }

    /**
     * @description Override afterUpdate method.
     * @author Jakub Fik | 2024-05-06
     **/
    public override void afterUpdate() {
        Map<Id, Datetime> fxAccId_ApprvFund = new Map<Id, Datetime>();
        Set<Id> fxAccId_MoreInfoRequired = new Set<Id>();
        List<Id> caseIdsToProcess = new List<Id>();
        Set<Id> statusChangedCaseIds = new Set<Id>();
        List<Contact> contactsToUpdate = new List<Contact>();

        for (Case c : this.triggerNew) {
            Case oldCase = this.triggerOldMap.get(c.Id);

            // Know if case's fxAccount should be set as 'Approved to Fund'
            if (shouldFxAccSetToApprovedToFund(c, oldCase)) {
                fxAccId_ApprvFund.put(c.fxAccount__c, c.Date_of_CX_Lead_Approval__c);
            }
            if (shouldFxAccSetFunnelStageToMIR(c, oldCase)) {
                fxAccId_MoreInfoRequired.add(c.fxAccount__c);
            }
            if (isMiFIDValidatedChange(c, oldCase)) {
                caseIdsToProcess.add(c.Id);
            }
            if (isStatusChanged(c.Status, oldCase.Status)) {
                statusChangedCaseIds.add(c.Id);
            }
            if (isInfoRequiredChanged(c, oldCase) && isOnboarding(c)) {
                contactsToUpdate.add(new Contact(Id = c.ContactId, Info_Required__c = c.Info_Required__c));
            }
        }

        // Update fxAccounts as 'Approved to Fund'
        fxAccountUtil.setFxAccsToApprovedToFund(fxAccId_ApprvFund);

        // Update fxAccount Funnel Stage to 'More Info Required'
        fxAccountUtil.setFunnelStage(fxAccId_MoreInfoRequired, Constants.FUNNEL_MORE_INFO_REQUIRED);

        CaseTriggerHandlerHelper.updateLastUpdateDateField(statusChangedCaseIds);
        if (!caseIdsToProcess.isEmpty()) {
            CaseMiFIDValidatedChange.fxAccountSetValidationResult(caseIdsToProcess);
        }

        if (!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }

        if (CustomSettings.isCustomNotificationForSalesEnabled() && CustomNotificationManager.firstRun) {
            CustomNotificationManager.sendNotificationsForOGMCases(this.triggerNew, this.triggerOldMap);
            CustomNotificationManager.firstRun = false;
        }

        //...
    }

    /**
     * @description Override afterDelete method.
     * @author Jakub Fik | 2024-05-06
     **/
    public override void afterDelete() {
        CaseTriggerHandlerHelper.verificationCodeRelease(this.triggerOldMap.values());
    }

    /**
     * @description
     * @updated Jakub Fik | 2024-05-10
     * @param newCase
     * @param oldCase
     * @return Boolean
     **/
    private static Boolean isMiFIDValidatedChange(Case newCase, Case oldCase) {
        return newCase.fxAccount__c != null &&
            newCase.Type__c == MiFIDExpValConst.CASE_MIFID_REVIEW_TYPE &&
            oldCase != null &&
            newCase.MiFID_Validated__c != oldCase.MiFID_Validated__c;
    }

    /**
     * @description Method checks if status field changed.
     * @author Jakub Fik | 2024-05-10
     * @param newStatus
     * @param oldStatus
     * @return Boolean
     **/
    private static Boolean isStatusChanged(String newStatus, String oldStatus) {
        return newStatus != oldStatus;
    }

    /**
     * @description Know if case's fxAccount should be set as 'Approved to Fund'
     * @updated 2024-05-10
     * @param newCase
     * @param oldCase
     * @return Boolean
     **/
    public static Boolean shouldFxAccSetToApprovedToFund(Case newCase, Case oldCase) {
        return CaseUtil.isOnBoardingType(newCase) &&
            oldCase.Date_of_CX_Lead_Approval__c == null &&
            newCase.Date_of_CX_Lead_Approval__c != null &&
            SObjectUtil.isRecordFieldsChanged(newCase, oldCase, 'Date_of_CX_Lead_Approval__c');
    }

    /**
     * @description Check if fxAccount funnel stage should be set to More Info Required
     * @updated | 2024-05-10
     * @param newCase
     * @param oldCase
     * @return Boolean
     **/
    public static Boolean shouldFxAccSetFunnelStageToMIR(Case newCase, Case oldCase) {
        return CaseUtil.isOnBoardingType(newCase) && //is an Onboarding case
            newCase.fxAccount__c != null && //has an fxAccount
            newCase.Application_Status__c == OmnichanelConst.CASE_STATUS_IN_PROGRESS && //Application Status is 'In Progress'
            (//Application Status or In Progress Reason changed
            newCase.Application_Status__c != oldCase.Application_Status__c ||
            newCase.Reason_Failed__c != oldCase.Reason_Failed__c) &&
            (//current user is OB or AML
            UserInfo.getProfileId() == UserUtil.getOBProfileId() ||
            UserInfo.getProfileId() == UserUtil.getOBAllSecProfileId() ||
            UserInfo.getProfileId() == UserUtil.getAMLProfileId());
    }

    /**
     * @description Method checks if case is onboarding record type and is lined into contact.
     * @author Jakub Fik | 2024-05-10
     * @param newCase
     * @return Boolean
     **/
    private static Boolean isOnboarding(Case newCase) {
        return String.isNotBlank(newCase.ContactId) &&
            newCase.RecordTypeId == RecordTypeUtil.getOnBoardingCaseTypeId() &&
            String.isNotBlank(newCase.Info_Required__c);
    }

    /**
     * @description Method checks if info required filed changed.
     * @author Jakub Fik | 2024-05-10
     * @param newCase
     * @param oldCase
     * @return Boolean
     **/
    private static Boolean isInfoRequiredChanged(Case newCase, Case oldCase) {
        return newCase.Info_Required__c != oldCase.Info_Required__c;
    }

    private void populateTypeSummary(Case newCase) {
        if (
            this.triggerOldMap == null ||
            newCase.Inquiry_Nature__c != this.triggerOldMap.get(newCase.Id).Inquiry_Nature__c ||
            newCase.Type__c != this.triggerOldMap.get(newCase.Id).Type__c ||
            newCase.Subtype__c != this.triggerOldMap.get(newCase.Id).Subtype__c ||
            newCase.Subject != this.triggerOldMap.get(newCase.Id).Subject
        ) {
            List<String> summary = new List<String>();
            if (String.isNotBlank(newCase.Inquiry_Nature__c))
                summary.add(newCase.Inquiry_Nature__c);
            if (String.isNotBlank(newCase.Type__c))
                summary.add(newCase.Type__c);
            if (String.isNotBlank(newCase.Subtype__c))
                summary.add(newCase.Subtype__c);
            if (String.isNotBlank(newCase.Subject))
                summary.add(newCase.Subject);
            newCase.Type_Summary__c = String.join(summary, ', ');
        }
    }

    /**
     * @description Method returns true when OTMS Support Record Type had Owner changed to CX Cases queue
     * @author Agnieszka Kajda | 2024-06-27
     * @param newCase
     * @param oldStatus
     * @param oldRecType
     * @return Boolean
     **/
    private Boolean isOTMSSupportAndOwnerChangedToCX(Case newCase,  Id oldOwnerId, Id oldRecType) {
        return isOwnerChanged(newCase, oldOwnerId) 
            && RecordTypeUtil.isCaseOTMSRecordTypes(oldRecType)
            && newCase.OwnerId == UserUtil.getQueueId(CaseTriggerHandlerHelper.CX_CASES_QUEUE);
    }

    /**
     * @description Method returns true if owner is Spam queue.
     * @author Jakub Fik | 2024-06-14
     * @param newCase
     * @param oldOwnerId
     * @return Boolean
     **/
    private Boolean isOwnerSpam(Case newCase) {
        return 
            UserUtil.isQueueUser(newCase.OwnerId) &&
            Constants.QUEUE_NAME_SPAM.equals(newCase.Case_Owner_Name__c);
    }

    /**
    * @description Method returns true if owner changed
    * @author Jakub Fik | 2024-06-19 
    * @param newCase 
    * @param oldOwnerId 
    * @return Boolean 
    **/
    private Boolean isOwnerChanged(Case newCase, Id oldOwnerId) {
        return newCase.OwnerId != oldOwnerId;
    }

    /**
     * @description Method returns true if Close Case field was changed into true.
     * @author Jakub Fik | 2024-06-14
     * @param newCase
     * @param oldClosedCase
     * @return Boolean
     **/
    private Boolean isCloseCaseChanged(Case newCase, Boolean oldClosedCase) {
        return newCase.Close_Case__c && newCase.Close_Case__c != oldClosedCase;
    }

    private void clearContact(Case c) {
        if (c.ContactId != null && RecordTypeUtil.isCaseOTMSRecordTypes(c.RecordTypeId)) {
            c.ContactId = null;
        }
    }

	/**
	* @description Method checks if record type is OTMS and if case with subject should be closed.
	* @author Jakub Fik | 2024-07-01 
	* @param newCase 
	* @return Boolean 
	**/
	private static Boolean isOTMSSubjectToClose(Case newCase){
        return RecordTypeUtil.isCaseOTMSRecordTypes(newCase.RecordTypeId) 
            && CaseUtil.isSubjectToClose(newCase.Subject);
	} 

	/**
	* @description Method checks if record type is OTMS and subject is New Document uploaded alert.
	* @author Jakub Fik | 2024-07-18 
	* @param c 
	* @return Boolean 
	**/
	private static Boolean isOTMSNewDocumentUploaded(Case c){
        return c.Subject != null && c.RecordTypeId == OTMS_ONBOARDING_RECORD_TYPE_ID && c.Subject.contains(Constants.NEW_DOCUMENT_UPLOADED);
	} 
}