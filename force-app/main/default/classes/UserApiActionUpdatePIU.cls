/**
 * @description       :
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 03-03-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiActionUpdatePIU extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.updatePIU();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.piuOutdated;
    }
}