/**
 * @File Name          : OnInternalCasesRoutedCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/20/2024, 4:00:15 AM
**/
public inherited sharing class OnInternalCasesRoutedCmd {

	@TestVisible
	final List<Case> caseList = new List<Case>();

	public void checkRecord(
		Case rec, 
		Case oldRec
	) {
		if (
			(rec.Routed__c == true) &&
			(rec.Agent_Capacity_Status__c != oldRec.Agent_Capacity_Status__c) &&
			(rec.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING)
		) {
			caseList.add(rec);
		} 
	}

	public Boolean execute() {
		if (caseList.isEmpty()) {
			return false;
		}
		//else...
		CaseRoutingManager.getInstance().routeUsingSkills(caseList);
		return true;
	}
	
}