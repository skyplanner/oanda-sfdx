/**
 * Base functionality of member in charge of downloading
 * information from the ComplyAdvantage portal.
 * @author Fernando Gomez
 * @since 10/19/2021
 */
public abstract class ComplyAdvantageDownloaderBase
	extends ComplyAdvantageCallout
	implements Queueable, Database.AllowsCallouts {

	protected String searchId;
	protected String searchReference;
	protected String caSearchId;

	public ComplyAdvantageDownloaderBase() {}
	/**
	 * @param searchId
	 */
	public ComplyAdvantageDownloaderBase(String searchId) {
		super();
		Comply_Advantage_Search__c caSearch;

		// main field here
		this.searchId = searchId;
		
		// we need some extra information such as api key:
		// based on the search Id, we need the actual record
		caSearch = getCaSearch();

		// we can't doi anything else if this search does not exists
		if (caSearch == null)
			throw new ComplyAdvantageException(
				String.format(
					Label.CA_Search_Not_Found_Exception,
					new List<String> { searchId }
				));

		// the instance is based on division and country in this case
		setupInstance(
			caSearch.Division_Name__c,
			caSearch.Search_Parameter_Mailing_Country__c);
		
		caSearchId = caSearch.Id;
		searchReference = caSearch.Search_Reference__c;
	}

	/**
	 * @return the record (Comply_Advantage_Search__c) in SF
	 * representing the ca search in the portal
	 */
	protected Comply_Advantage_Search__c getCaSearch() {
		try {
			return [
				SELECT Id,
					Search_Reference__c,
					Division_Name__c,
					Search_Parameter_Mailing_Country__c,
					Match_Status__c
				FROM Comply_Advantage_Search__c
				WHERE Search_Id__c = :searchId
			];
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Doanloads the content and saves it into salesforce.
	 * Executes out of a queueable conext.
	 */
	public abstract void execute();

	/**
	 * Doanloads the content and saves it into salesforce.
	 * Executes within invokable context.
	 */
	public virtual void execute(QueueableContext context) {
		execute();
	}
}