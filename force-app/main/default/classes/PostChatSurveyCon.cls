/**
 * Extract the context variable and creates a redirection to the survey.
 * @version 2.0, Fernando Gomez, SkyPlanner LLC
 */
public without sharing class PostChatSurveyCon  extends SurveyRedirectorBase {
	public String chatKey { get; private set; }
	public String attachedRecordsJson { get; private set; }
	public String chatDetailsJson { get; private set; }

	public Boolean isError { get; private set; }
	public String reason { get; private set; }
	
	/**
	 * Basic constructor
	 */
	public PostChatSurveyCon() {
		Map<String, String> params;
		String surveyName;
		AttachedRecords attached;
		Case oCase;
		Survey__c survey;
		ChatDetails details;
		// Get url parameters
		params = Apexpages.currentPage().getParameters();

		// we extract the props
		chatKey = params.get('chatKey');
		language = params.get('windowLanguage');
		chatDetailsJson = params.get('chatDetails');
		attachedRecordsJson = params.get('attachedRecords');

		if (String.isBlank(chatKey) || String.isBlank(attachedRecordsJson)) {
			isError = true;
			reason = 'Needed parameters are blank';
		} else {
			// we convert the JSON
			try {
				attached = (AttachedRecords)JSON.deserialize(
					attachedRecordsJson, AttachedRecords.class);
				details = (ChatDetails)JSON.deserialize(
					chatDetailsJson, ChatDetails.class);
			} catch (Exception ex) {
				isError = true;
				reason = 
					'Could deserialized the attached records or chat details.<br/>' +
					ex.getMessage();
				return;
			}

			// since we now have the details, we try to find a language there
			findLanguage(details);

			// we need the survey
			 if (String.isBlank(surveyName = getSurveyName()) ||
					(survey = getSurvey(surveyName)) == null) {
				isError = true;
				reason = 'The survey was not found.';
			} else {
				isError = false;
				caseId = attached.CaseId;
				leadId = attached.LeadId;
				contactId = null;
				surveyId = survey.Id;

				if (String.isNotBlank(caseId)) { 
					if ((oCase = getCase(attached.CaseId)) == null) {
						isError = true;
						reason = 'The related case was not found';
					} else {
						contactId = oCase.ContactId;
						leadId = oCase.Lead__c;
					}
				}				
			}
		}
	}

	/**
	 * @return a page reference to the survey
	 */
	public PageReference tryRedirecting() {
		PageReference pr = null;
		
		if (!isError) {
			pr = goToSurvey();
			pr.getParameters().put('chatKey', chatKey);
		}

		return pr;
	}

	/**
	 * Finds the language in the chat details
	 */
	private void findLanguage(ChatDetails details) {
		if (details.customDetails != null)
			for (CustomDetail cd : details.customDetails)
				if (cd.label == 'LanguagePreference') {
					language = cd.value;
					return;
				}
	}

	/**
	 * Attach record is a JSON and we need this class
	 * to process it.
	 */
	private class AttachedRecords {
		Id CaseId { get; set; }
		Id LeadId { get; set; }
	}

	private class CustomDetail {
		String value { get; set; }
		List<String> entityMaps { get; set; }
		Boolean displayToAgent { get; set; }
		String label { get; set; }
		List<String> transcriptFields { get; set; }
	}

	private class ChatDetails {
		String visitorId { get; set; }
		List<CustomDetail> customDetails { get; set; }
	}
}