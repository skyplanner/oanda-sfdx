/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-19-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchCAFieldsUpdInAcc implements
    Database.Batchable<sObject>, Database.AllowsCallouts, Database.RaisesPlatformEvents, BatchReflection{
    public String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchCAFieldsUpdInAcc(String filter) {
        query = 
        'SELECT Id, Name, fxAccount__c, ' +
        '    Is_Monitored__c, fxAccount__r.Is_CA_Name_Search_Monitored__c, ' +
        '    Is_Alias_Search__c, fxAccount__r.Is_CA_Alias_Search_Monitored__c ' +
        '    FROM Comply_Advantage_Search__c WHERE Is_Monitored__c = TRUE AND fxAccount__c != null AND (' +
        '    (Is_Alias_Search__c = TRUE AND fxAccount__r.Is_CA_Alias_Search_Monitored__c = FALSE) OR ' +
        '    (Is_Alias_Search__c = FALSE AND fxAccount__r.Is_CA_Name_Search_Monitored__c = FALSE) ' +
        '    ) ' +
        (String.isNotBlank(filter) ? filter : '');
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 
        'SELECT Id, Name, fxAccount__c, ' +
        '    Is_Monitored__c, fxAccount__r.Is_CA_Name_Search_Monitored__c, ' +
        '    Is_Alias_Search__c, fxAccount__r.Is_CA_Alias_Search_Monitored__c ' +
        '    FROM Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Comply_Advantage_Search__c> scope) {
        System.debug('scope: ' + scope);

        Map<Id, fxAccount__c> toUpdate = new Map<Id, fxAccount__c>();

        for (Comply_Advantage_Search__c s : scope) {
            fxAccount__c fx = toUpdate.get(s.fxAccount__c);
            if (fx == null) {
                fx = s.fxAccount__r;
            }
            if (s.Is_Alias_Search__c) {
                fx.Is_CA_Alias_Search_Monitored__c = true;
            }
            else {
                fx.Is_CA_Name_Search_Monitored__c = true;
            }
            toUpdate.put(fx.Id, fx);
        }

        if (!toUpdate.isEmpty()) {
            update toUpdate.values();
        }
    }

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}