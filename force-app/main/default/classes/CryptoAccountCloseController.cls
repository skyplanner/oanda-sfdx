/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-31-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoAccountCloseController {
    @InvocableMethod 
    public static List<String> closeAccount(List<Id> fxAccounts) {
        return closeAccount(fxAccounts, false);
    }

    public static List<String> closeAccount(List<Id> fxAccounts, Boolean addFxAccountDataResult) {
        if (fxAccounts == null || fxAccounts.isEmpty()) {
            return new List<String>();
        }

        Map<Id, Note> successNoteMap = new Map<Id, Note>();
        Map<Id, Note> failNoteMap = new Map<Id, Note>();

        for (Id fxaId : fxAccounts) {
            successNoteMap.put(fxaId, new Note(
                ParentId = fxaId,
                Body = String.format(Label.Close_Note_Body_Success,
                    new List<String> {UserUtil.currentUser.Name}),
                Title = System.Label.Close_Note_Title_Success
            ));
            failNoteMap.put(fxaId, new Note(
                ParentId = fxaId,
                Body = String.format(Label.Close_Note_Body_Fail,
                    new List<String> {UserUtil.currentUser.Name}),
                Title = System.Label.Close_Note_Title_Fail
            ));
        }

        CryptoAccountActionsCallout.CryptoAccountStatusRequest request =
            new CryptoAccountActionsCallout.CryptoAccountStatusRequest();
        request.isDisabled = true;

        Crypto_Account_Setting__mdt mdt = CryptoAccountSettingsMdt.getInstance().getMdt('Close_Action_Info');

        CryptoAccountParams params = new CryptoAccountParams();
        params.fxaccountIds = fxAccounts;
        params.action = 2;
        params.cryptoAccountAction = Constants.CRYPTO_ACCOUNT_CLOSE_ACTION;
        //Leave Enable_Disable_Check_Balance_Endpoint after fully implementation of User API
        params.calloutResourcePathUser = (Util.namedCredentialConfiguration != null && Util.namedCredentialConfiguration.Use_New_User_API_Logic__c) ? 'Enable_Disable_Check_Balance_Endpoint' : 'Close_Endpoint';
        params.calloutResourcePathTas = 'Close_Endpoint_TAS';
        params.requestBody = JSON.serializePretty(request.body);
        params.successNoteMap = successNoteMap;
        params.failNoteMap = failNoteMap;
        params.addFxAccountDataResult = addFxAccountDataResult;
        params.useTasApi = mdt.TAS_api__c;
        params.useUserApi = mdt.USER_api__c;

        CryptoAccountActionsCallout accCloseTradingCallout = new CryptoAccountActionsCallout(
            params
        );

        return new List<String> { accCloseTradingCallout.execute() };
    }
}