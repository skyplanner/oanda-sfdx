/**
 * Represents an atuhentication code. Contains information
 * about the object the code is related to, and expiration date.
 * @author Fernando Gomez
 * @since 5/7/2021
 */
public class AuthenticationCode {
	@AuraEnabled
	public String code { get; set; }

	@AuraEnabled
	public DateTime expiresOn { get; set; }

	public transient Id recordId { get; set; }
	public transient String fieldApiName { get; set; }
	public transient String expiresOnFieldApiName { get; set; }

	/**
	 * @param code
	 * @param expiresOn
	 */
	public AuthenticationCode(String code, DateTime expiresOn) {
		this(code, expiresOn, null, null, null);
	}

	/**
	 * @param code
	 * @param expiresOn
	 */
	public AuthenticationCode(String code, DateTime expiresOn,
			Id recordId, String fieldApiName, String expiresOnFieldApiName) {
		this.code = code;
		this.expiresOn = expiresOn;
		this.recordId = recordId;
		this.fieldApiName = fieldApiName;
		this.expiresOnFieldApiName = expiresOnFieldApiName;
	}

	/**
	 * @return true is the ode is valid based on
	 * value and expriation date
	 */
	public Boolean isCodeValid() {
		return String.isNotBlank(code) &&
			expiresOn != null &&
			expiresOn > System.now();
	}
}