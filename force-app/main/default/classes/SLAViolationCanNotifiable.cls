/**
 * @File Name          : SLAViolationCanNotifiable.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 11:34:44 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2024, 11:27:42 AM   aniubo     Initial Version
 **/
public interface SLAViolationCanNotifiable {
	Boolean canNotify(
		SLAConst.SLAViolationType violationType,
		SLAConst.SLANotificationObjectType notificationObjectType
	);
}