/**
 * @File Name          : SLAConst.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/22/2024, 12:46:24 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/6/2021, 4:31:23 PM   acantero     Initial Version
**/
public without sharing class SLAConst {

    public static final String CHAT_CHANNEL = 'Chat';
    public static final String EMAIL_CHANNEL = 'Email';
    public static final String PHONE_CHANNEL = 'Phone';

    //violation type
    public static final String ALERT_NO_ANSWER_VT = 'Alert no answer';
    public static final String AHT_EXCEEDED_VT = 'AHT Exceeded';
    public static final String ASA_EXCEEDED_VT = 'ASA Exceeded';

    public static final String SLA_NOTIFICATION_DEV_NAME = 'SLA_Notification';

    public static final String PHONE_CASE_BOUNCED_MILESTONE = 'Phone Case - Bounced';

    public enum AgentSupervisorModel { DIVISION, ROLE }

    public enum SLAViolationType { ANSWER, ASA, BOUNCED, AHT }

    public enum ChatType{ CHAT,MESSAGING}

    public enum SLANotificationObjectType{
        CHAT_CASE_NOT,
        EMAIL_CASE_NOT,
        PHONE_CASE_NOT,
        LIVE_CHAT_NOT,
        MESSAGING_NOT
    }
    public static final String CASE_ORIGIN_PHONE = 'Phone';
    public static final String CASE_ORIGIN_CHAT = 'Chat';
    public static final String CASE_ORIGIN_EMAIL_API = 'Email - API';
    public static final String CASE_ORIGIN_CHATBOT_CASE = 'Chatbot - Case';
    public static final String CASE_ORIGIN_FRONT_DESK = 'Email - Frontdesk';
    public static final String CASE_ORIGIN_CHATBOT_EMAIL_CASE = 'Chatbot - Email';
    public static final String MESSAGING_AGENT_TYE = 'Agent'; 
    public static final String MESSAGING_ROUTING_STATUS_ASSIGNED = 'Assigned'; 
    public static final String MESSAGING_ROUTING_STATUS_OPENED = 'Opened'; 

}