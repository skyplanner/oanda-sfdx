/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-15-2022
 * @last modified by  : Yaneivys Gutierrez
 * @Description
 * URL: /services/apexrest/api/v1/fxaccounts/live/5412558
 * Sample Payload:
 * {
        "W8_W9_Status__c" : "String",
        "US_Shares_Trading_Enabled__c": true,
        "Conditionally_Approved__c": true
 * }
**/
@RestResource(urlMapping='/api/v1/fxaccounts/live/*')
global without sharing class LiveFxaccountRestResource {
    @HttpPatch
    global static void doPatch() {
        RestResponse res = Restcontext.response;
        try {
            RestRequest req = RestContext.request;

            String requestBody = req.requestBody.toString();

            fxAccount__c fxAcc = (fxAccount__c)JSON.deserialize(requestBody, fxAccount__c.class);

            List<String> urlFragments = req.requestURI.split('/');
            String fxUserId = urlFragments[urlFragments.size() - 1];
            Id liveRtId = RecordTypeUtil.getFxAccountLiveId();
            String liveRtId15 = liveRtId.to15();
            String fxGlobalId = String.format('{0}+{1}', new List<String> { fxUserId, liveRtId15 });
            
            List<fxAccount__c> fxList = [
                SELECT Id
                FROM fxAccount__c
                WHERE fxTrade_Global_ID__c = :fxGlobalId
            ];

            if (!fxList.isEmpty() && fxList.size() > 0) {
                fxAcc.Id = fxList[0].Id;
                update fxAcc;

                res.statusCode = 200;
                res.responseBody = Blob.valueOf(System.Label.Live_Account_Update_Success);
            }
            else {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf(String.format(System.Label.Live_Account_Not_Found, new List<String> { fxUserId }));
            }
        } 
        catch (Exception ex) {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(ex.getMessage());
        }
    }

    @HttpGet
    global static void doGet() {
        RestResponse res = Restcontext.response;
        try {
            RestRequest req = RestContext.request;

            res.addHeader('Content-Type', 'application/json');

            String fields = req.params.get('fields');
            if (String.isBlank(fields)) {
                res.statusCode = 404;
                res.responseBody = Blob.valueOf(System.Label.Live_Account_Fields_Missing);
            } else {
                List<String> urlFragments = req.requestURI.split('/');
                String fxUserId = urlFragments[urlFragments.size() - 1];
                Id liveRtId = RecordTypeUtil.getFxAccountLiveId();
                String liveRtId15 = liveRtId.to15();
                String fxGlobalId = String.format('{0}+{1}', new List<String> { fxUserId, liveRtId15 });
                
                String q =
                    ' SELECT ' + fields +
                    ' FROM fxAccount__c' +
                    ' WHERE fxTrade_Global_ID__c = :fxGlobalId';


                List<fxAccount__c> fxList = Database.query(q);
                if (!fxList.isEmpty() && fxList.size() > 0) {
                    fxAccount__c fx = fxList[0];

                    Map<String, Object> fxMap = fx.getPopulatedFieldsAsMap();
                    for (String field : fields.split(',')){
                        if (!fxMap.containsKey(field)){
                            fx.put(field, null);
                        }
                    }
                    
                    res.statusCode = 200;
                    res.responseBody = Blob.valueOf(JSON.serialize(fx));
                }
                else {
                    res.statusCode = 404;
                    res.responseBody = Blob.valueOf(String.format(System.Label.Live_Account_Not_Found, new List<String> { fxUserId }));
                }
            }
        } 
        catch (Exception ex) {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(ex.getMessage());
        }
    }
}