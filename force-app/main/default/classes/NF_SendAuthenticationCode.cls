/**
 * @description       : Sends an authentication code email
 * @author            : NeuraFlash
 * @created on        : 11/11/21
 * @group             : 
 * @last modified on  : 07-01-2022
 * @last modified by  : Yaneivys Gutierrez
**/
global without sharing class NF_SendAuthenticationCode implements nfchat.DataAccessClass {
    public Case[] caseCheck = new List<Case>{};
    public String[] emails = new List<String>{};
    public fxAccount__c accountToEmail;
    public String languageSelected = '';
    public Id contactId;
    
    private static final String DEFAULT_TEMPLATE_NAME = 'Case_Authentication_Code'; // Taken from CaseAuthenticationCodeResource
    private static final String EMAIL_ADDRESS = Settings__c.getValues('Default')?.Auth_code_OrgWideEmail__c;

    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
        Map<String, String> eventParams = new Map<String, String>();

        try {
            Map<String, Object> mapSession = (Map<String, Object>) JSON.deserializeUntyped(aiServiceResponse);

            //String emailValue =(String)paramMap.get('email'); //get email from DF
            String chatSessionId = (String)mapSession.get('sessionId');  // Get Session ID for case lookup

            nfchat__Chat_Log__c currentChatLog = [
                SELECT nfchat__Session_Id__c, nfchat__Email__c, Language_Preference__c
                FROM nfchat__Chat_Log__c
                WHERE nfchat__Session_Id__c =: chatSessionId
            ];

            String chatSessionEmail = currentChatLog?.nfchat__Email__c;  // Get email for account lookup
            languageSelected = currentChatLog?.Language_Preference__c;

            System.debug('>> NF_SendAuthenticationCode: Checking Case');
            System.debug('>> NF_SendAuthenticationCode: Session ID: ' + chatSessionId);
            System.debug('>> NF_SendAuthenticationCode: Email: ' + chatSessionEmail);
            System.debug('>> NF_SendAuthenticationCode: Language: ' + languageSelected);

            caseCheck = [  
            SELECT id, CaseNumber, Status, RecordTypeId, fxAccount__c, AccountId, ContactId, Subject, Client_Authentication_Code__c, Client_Authentication_Code_Expires_On__c
            FROM case
            WHERE Session_Id__c = :chatSessionId ];
            //WHERE Id = '5004C00000B5ADMQA3' ];

            System.debug('>>NF_SendAuthenticationCode: Cases found: ' + caseCheck);

            // if (caseCheck.size() > 0) {
            //     System.debug('>> NF_SendAuthenticationCode: Case #' + caseCheck[0].CaseNumber);
            //     System.debug('>> NF_SendAuthenticationCode: Status: ' + caseCheck[0].Status);
            //     System.debug('>> NF_SendAuthenticationCode: RecordTypeID: ' + caseCheck[0].RecordTypeID);
            //     System.debug('>> NF_SendAuthenticationCode: fxAccount: ' + caseCheck[0].fxAccount__c);
            //     System.debug('>> NF_SendAuthenticationCode: AccountID: ' + caseCheck[0].AccountID);
            //     System.debug('>> NF_SendAuthenticationCode: ContactID: ' + caseCheck[0].ContactID);
            //     System.debug('>> NF_SendAuthenticationCode: Subject: ' + caseCheck[0].Subject);

            //     // Try calling the code generation class
            //     CaseAuthenticationCodeResource codeGen = new CaseAuthenticationCodeResource(caseCheck[0].Id);
                
            //     // Code and Code Expiration Date are stored in the relevant case
            //     AuthenticationCode code = codeGen.generateCode(true); // Generate code, replace if invalid
            //     System.debug('>> NF_SendAuthenticationCode: Code generated: ' + code.code);

            //     System.debug('>> Code generated: ' + code);    
            //     codeGen.sendEmail();   // Send email
            
            // }

            /**
             * Code Generation Logic using ACCOUNT
             * 1. Look up fxAccount, by email provided
             * 2. If one exists, generate the Auth Code Email
             */
            List<fxAccount__c> fxAccounts = [
                SELECT Id, Name, Email__c, Account__r.PersonEmail, Authentication_Code__c, Authentication_Code_Expires_On__c  
                FROM fxAccount__c
                WHERE Account__r.PersonEmail = :chatSessionEmail
            ];
            System.debug('>>NF_SendAuthenticationCode: Found fxAccount=' + fxAccounts);

            Account accountToUpdate = [
                SELECT Authentication_Code__c, Authentication_Code_Expires_On__c
                FROM Account
                WHERE ID =: fxAccounts[0].Account__r.Id
            ];

            if (fxAccounts.size() > 0) {
                // Initialize code generator w/ Account + fields
                AuthenticationCodeGenerator codeGen = new AuthenticationCodeGenerator(
                    accountToUpdate, //fxAccounts[0].Account__r, 
                    'Authentication_Code__c', 
                    'Authentication_Code_Expires_On__c'
                );

                accountToEmail = fxAccounts[0];

                List<Contact> listOfContacts = [
                    SELECT Id, Name, Email
                    FROM Contact
                    //WHERE (Name =: fxAccounts[0].Name AND Email =: fxAccounts[0].Account__r.PersonEmail )
                    WHERE Email =: fxAccounts[0].Account__r.PersonEmail
                    LIMIT 1
                ];
                
                codeGen.generateNewCodeAndSaveToRecord();

                // also update fxAccount
                try {
                    System.debug('>>NF_SendAuthenticationCode: ATTEMPTING TO UPDATE FXACCOUNT');
                    fxAccounts[0].Authentication_Code__c = accountToUpdate.Authentication_Code__c;
                    fxAccounts[0].Authentication_Code_Expires_On__c = accountToUpdate.Authentication_Code_Expires_On__c;

                    update fxAccounts;
                    System.debug('>>NF_SendAuthenticationCode: FXACCOUNT UPDATED');
                }
                catch (Exception e) {
                    System.debug('>>NF_SendAuthenticationCode: FAILED TO UPDATE FXACCOUNT');
                    System.debug('>>NF_SendAuthenticationCode: ' + e.getMessage());
                    System.debug('>>NF_SendAuthenticationCode: ' + e.getStackTraceString());
                }

                if (listOfContacts.size() > 0 ) {
                    System.debug('>>NF_SendAuthenticationCode: Found contact=' + listOfContacts);
                    contactId = listOfContacts[0].Id;
                    emails.add(listOfContacts[0].Email);

                    caseCheck = [  
                    SELECT id, CaseNumber, Status, RecordTypeId, fxAccount__c, AccountId, ContactId, Subject, Client_Authentication_Code__c, Client_Authentication_Code_Expires_On__c
                    FROM case
                    WHERE Session_Id__c = :chatSessionId ];

                    System.debug('>>NF_SendAuthenticationCode: Updated case=' + caseCheck[0].Client_Authentication_Code__c);
                    System.debug('>>NF_SendAuthenticationCode: Updated case=' + caseCheck[0].Client_Authentication_Code_Expires_On__c);

                    sendEmail();
                }           
            }
        }
        catch(Exception ex){
            System.debug('>> Exception in NF_SendAuthenticationCode > processRequest : ' + ex.getMessage());
            System.debug('>>        at Line Number: ' + ex.getLineNumber());
            System.debug('>>        stack trace:    ' + ex.getStackTraceString());
            return null;
        }
        return JSON.serialize(eventParams);
    }

    // /**
    //  * Adapted from CaseAuthenticationCodeResource
    //  */
    public void sendEmail() {
		EmailTemplate template;

        System.debug('>> Sending email');
        System.debug('>> CONTACT:  ' + ContactId);
        System.debug('>> LANGUAGE: ' + languageSelected);
        System.debug('>> DEFAULT TEMPLATE: ' + DEFAULT_TEMPLATE_NAME);

		template = EmailTemplateUtil.getTemplateByLanguage(
			DEFAULT_TEMPLATE_NAME,
			languageSelected);

        if (template == null) {
            template = [SELECT Id FROM EmailTemplate WHERE DeveloperName =: DEFAULT_TEMPLATE_NAME ];
        }
		
		/*EmailUtil.sendEmailByTemplate(emails,
			// we use the default template
			template != null ? template.DeveloperName : DEFAULT_TEMPLATE_NAME,
			ContactId, //aCase.Id,
			true,
			'oanda@theskyplanner.com');*/

        Id senderId;
        if (String.isNotBlank(EMAIL_ADDRESS)) {
            senderId = [
                SELECT Id
                FROM OrgWideEmailAddress
                WHERE Address = :EMAIL_ADDRESS
            ]?.Id;
        }

        sendMail(
            emails,                         // toAddresses[]
            contactId,                      // targetObjectId
            null,                           // Subject
            null,                           // PlainTextBody
            template.Id,                    // templateId
            caseCheck[0]?.Id, //accountToEmail.Account__r.Id,   // whatId
            true,                           // saveAsActivity
            senderId                        // orgWideEmailAddressId
        );
	}

    /**
	 * COPIED FROM EMAILUTIL
     * SP-9521
	 * The need to accept the order of save the operation
	 * as an activity.
	 * @param toAddresses
	 * @param targetObjectId
	 * @param subject
	 * @param plainTextBody
	 * @param templateId
	 * @param whatId
	 * @param saveAsActivity
	 * @param orgWideEmailAddressId
	 */
	private static void sendMail(
            String[] toAddresses, 
            Id targetObjectId,
            String subject, 
            String plainTextBody, 
            Id templateId,
            Id whatId, 
            Boolean saveAsActivity, Id orgWideEmailAddressId) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setTargetObjectId(targetObjectId);
        mail.setSaveAsActivity(saveAsActivity);
        mail.setSubject(subject);
        mail.setPlainTextBody(plainTextBody);
        mail.setWhatId(whatId);
        mail.setTemplateId(templateId);

        if (String.isNotBlank(orgWideEmailAddressId))
            mail.setOrgWideEmailAddressId(orgWideEmailAddressId);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}