/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-19-2023
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class RelatedListCntrlTest {
    @testSetup
	static void setup () {
		Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fx = (new TestDataFactory()).createFXTradeAccount(acc);
        Trading_Account__c mt5 = new Trading_Account__c(
            RecordTypeId = Trading_Account__c.SobjectType.getDescribe().getRecordTypeInfosByName().get('MT5 Live').getRecordTypeId()
        );
        insert mt5;
	}

	@isTest
	static void testFetchRelatedList() {
        fxAccount__c fx = [SELECT Id FROM fxAccount__c LIMIT 1];

		Test.startTest();

		RelatedListWrapper result = RelatedListCntrl.fetchRelatedList(fx.Id, 'fxAccounts');

		Test.stopTest();

		System.assertEquals(true, result.apiNames.size() > 0);
    }
}