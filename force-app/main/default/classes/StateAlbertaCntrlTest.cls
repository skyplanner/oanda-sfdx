/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-07-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class StateAlbertaCntrlTest {
	@isTest
	static void getAccountRecord() {
		Account acc;

		// we need to insert a new account
		acc = new Account(
			FirstName = 'John',
			LastName = 'Doe',
			PersonEmail = 'johndoe@gmail.com'
		);
		insert acc;

		System.assertEquals(1, StateAlbertaCntrl.getAccountRecord(acc.Id).size());
	}

    @isTest
	static void userResponseTrue() {
		Account acc;

		// we need to insert a new account
		acc = new Account(
			FirstName = 'John',
			LastName = 'Doe',
			PersonEmail = 'johndoe@gmail.com',
            PersonMailingState = 'AB'
		);
		insert acc;

        acc = StateAlbertaCntrl.getAccountRecord(acc.Id)[0];
		System.assertEquals(true, acc.State_Alberta__c);

        Test.startTest();

        StateAlbertaCntrl.userResponse(acc.Id, true);

        Test.stopTest();

        acc = StateAlbertaCntrl.getAccountRecord(acc.Id)[0];
		System.assertEquals(false, acc.State_Alberta__c);
	}

    @isTest
	static void userResponseFalse() {
		Account acc;

		// we need to insert a new account
		acc = new Account(
			FirstName = 'John',
			LastName = 'Doe',
			PersonEmail = 'johndoe@gmail.com',
            PersonMailingState = 'AB'
		);
		insert acc;

        acc = StateAlbertaCntrl.getAccountRecord(acc.Id)[0];
		System.assertEquals(true, acc.State_Alberta__c);

        Test.startTest();

        StateAlbertaCntrl.userResponse(acc.Id, false);

        Test.stopTest();

        acc = [SELECT Id, State_Alberta__c, PersonMailingState FROM Account WHERE Id = :acc.Id][0];
		System.assertEquals(false, acc.State_Alberta__c);
		System.assertEquals(null, acc.PersonMailingState);
	}
}