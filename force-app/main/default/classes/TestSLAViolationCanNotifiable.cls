/**
 * @File Name          : TestSLAViolationCanNotifiable.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:15:18 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/7/2024, 1:15:18 PM   aniubo     Initial Version
**/
@IsTest
public class TestSLAViolationCanNotifiable implements SLAViolationCanNotifiable {
	public Boolean canNotify(
		SLAConst.SLAViolationType violationType,
		SLAConst.SLANotificationObjectType notificationObjectType
	) {
		return true;
	}
}