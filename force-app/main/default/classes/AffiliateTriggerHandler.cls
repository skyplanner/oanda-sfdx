/**
 * All triggers handlers must expose this functionality.
 * @author Fernando @Skyplanner
 * @since 12/10/2020
 */
public without sharing class AffiliateTriggerHandler {
	private Boolean isComplyAdantageSearchOn;
	private List<Affiliate__c> newAffiliates, oldAffiliates;
	
	/**
	 * Main constructor
	 * @param newAffiliates
	 * @param oldAffiliates
	 */
	public AffiliateTriggerHandler(
			List<Affiliate__c> newAffiliates,
			List<Affiliate__c> oldAffiliates) {
		this.newAffiliates = newAffiliates;
		this.oldAffiliates = oldAffiliates;
		isComplyAdantageSearchOn =
			Settings__c.getValues('Default') != null &&
			Settings__c.getValues('Default').Comply_Advantage_Enabled__c;
	}

	/**
	 * Handles: before Insert
	 */
	public void handleBeforeInsert() 
	{
		for (Affiliate__c partner : newAffiliates)
		{
			if(partner.RecordTypeId == RecordTypeUtil.BANKING_PARTNER_RECORD_TYPE_ID ||
				partner.RecordTypeId == RecordTypeUtil.VENDORS_RECORD_TYPE_ID)
			{
				partner.Affiliate_Type__c = 'Corporate';
			}
		}
	}

	/**
	 * Handles: after Insert
	 */
	public void handleAfterInsert() {
		if (isComplyAdantageSearchOn)
			for (Affiliate__c affiliate : newAffiliates)
				ComplyAdvantageHelper.validate(null, affiliate);
	}

	/**
	 * Handles: before Update
	 */
	public void handleBeforeUpdate() {
		// ...
	}

	/**
	 * Handles: after Update
	 */
	public void handleAfterUpdate() {
		if (isComplyAdantageSearchOn)
			for (Integer i = 0; i < newAffiliates.size(); i++)
				ComplyAdvantageHelper.validate(oldAffiliates[i], newAffiliates[i]);
	}

	/**
	 * Handles: before Delete
	 */
	public void handleBeforeDelete() {
		// ...
	}

	/**
	 * Handles: after Delete
	 */
	public void handleAfterDelete() {
		// ...
	}

	/**
	 * Handles: after Undelete
	 */
	public void handleAfterUndelete() {
		// ...
	}
}