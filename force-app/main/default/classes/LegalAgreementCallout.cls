/**
 * Legal agreement call util class
 */
public class LegalAgreementCallout extends Callout {

    LegalAgreement_Settings__c stts;

    private static final String PARAMETER_USER_ID = '{user_id}';
    private static final String PARAMETER_CLIENT_ID = 'client_id';
    private static final String PARAMETER_CLIENT_SECRET = 'client_secret';
    private static final String BASE_URL = 'callout:Legal_Agreements';

    /**
     * Constructor
     */
    public LegalAgreementCallout() {
        baseUrl = BASE_URL;
        timeout = 120000;
        successCodes = new Set<Integer>{200};
        stts = LegalAgreement_Settings__c.getOrgDefaults();
    }

    /**
     * Callout to get legal agreements
     */
    public String getLegalAgreements(String fxTradeUserId) {
        // Validate settings values
        validateStts();

        // Build get agreements resource
        String getAgsResource =
            stts.Get_Agreements_Resource__c.replace(
                '{user_id}',
                fxTradeUserId);

        // Make the request
        get(
            getAgsResource,
            new Map<String, String>{
                'Content-Type' => 'application/json',
                PARAMETER_CLIENT_ID => stts.Client_Id__c,
                PARAMETER_CLIENT_SECRET => stts.Client_Secret__c}, 
            null);

        // Process the response
        if(isResponseCodeSuccess()) {
            Logger.debug(
                'Callout',
                Constants.LOGGER_LEGAL_AGREEMENT_CATEGORY,
                req,
                resp);
        } else {
            throw LogException.newInstance(
                'Error calling Legal-Agreement API.',
                Constants.LOGGER_LEGAL_AGREEMENT_CATEGORY,
                req,
                resp);
        }

        return resp.getBody();
    }

    /**
     * Validate custom settings
     */
    private void validateStts() {
        if(String.isBlank(stts.Get_Agreements_Resource__c) ||
            String.isBlank(stts.Client_Id__c) ||
            String.isBlank(stts.Client_Secret__c)) {
                throw LogException.newInstance(
                    'Legal Agreements settings error.',
                    Constants.LOGGER_LEGAL_AGREEMENT_CATEGORY,
                    'You need to specify API resources and credentials on the \'Legal Agreement\' settings.');
        }
    }

}