/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-16-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class SegmentationManager {
    public void manageSegmentationActivated(
        List<Segmentation__c> newList, Map<Id, Segmentation__c> oldMap
    ) {
        List<Segmentation__c> segmentationList = new List<Segmentation__c>();

        for (Segmentation__c s: newList) {
            if (s.Active__c && (oldMap == null || !oldMap.get(s.Id).Active__c))
                segmentationList.add(s);
        }

        if (segmentationList.isEmpty())
            return;

        Set<Id> fxAccountIds = updatefxAccounts(newList);
        inactivateOldSegmentations(fxAccountIds);
    }

    public void manageSegmentationRemoved(List<Segmentation__c> deletedSegmentations) {
        Set<Id> fxAccountIds = new Set<Id>();
        List<Segmentation__c> segmentationList = new List<Segmentation__c>();

        for (Segmentation__c s : deletedSegmentations) {
            if (s.Active__c)
                fxAccountIds.add(s.fxAccount__c);
        }

        for (fxAccount__c fx : [SELECT (SELECT Id 
                FROM Segmentations__r
                ORDER BY CreatedDate DESC)
            FROM fxAccount__c 
            WHERE Id IN: fxAccountIds]
        ) {
            if (fx.Segmentations__r?.size() > 0) {
                fx.Segmentations__r[0].Active__c = true;
                segmentationList.add(fx.Segmentations__r[0]);                
            }
        }

        if (!segmentationList.isEmpty()) {
            update segmentationList;
        }
    }

    private Set<Id> updatefxAccounts(List<Segmentation__c> newSegmentations) {
        Map<Id, fxAccount__c> fxAccountMap = new Map<Id, fxAccount__c>();

        for (Segmentation__c s : newSegmentations) {
            fxAccountMap.put(s.fxAccount__c, 
                new fxAccount__c (
                    Id = s.fxAccount__c,
                    Segmentation__c = s.Id
                ));
        }

        if (!fxAccountMap.isEmpty()) {
            update fxAccountMap.values();
        }

        return fxAccountMap.keySet();
    }

    private void inactivateOldSegmentations(Set<Id> fxAccountIds) {
        List<Segmentation__c> segmentationList = new List<Segmentation__c>();

        for (fxAccount__c fx : [SELECT (SELECT Id 
                FROM Segmentations__r 
                WHERE Active__c = true 
                ORDER BY CreatedDate DESC)
            FROM fxAccount__c 
            WHERE Id IN: fxAccountIds]
        ) {
            if (fx.Segmentations__r?.size() > 1) {
                for (Integer i = 1; i < fx.Segmentations__r.size(); i++) {
                    fx.Segmentations__r[i].Active__c = false;
                    segmentationList.add(fx.Segmentations__r[i]);
                }
            }
        }

        if (!segmentationList.isEmpty()) {
            update segmentationList;
        }
    }
}