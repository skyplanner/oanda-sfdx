public without sharing class BatchJobUtil {
    
    public static void startJob(List<Batch_Job__c> jobs){
    	
    	//only insert one at a time
    	if(jobs[0].Job_Name__c == 'BatchLoadPracticeLeadSchedulable'){
    		BatchLoadPracticeLeadSchedulable.executeBatch();
    	}
    }
    
}