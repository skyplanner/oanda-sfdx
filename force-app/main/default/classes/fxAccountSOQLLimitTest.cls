@isTest
public class fxAccountSOQLLimitTest {
	
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	
	static {
		CustomSettings.setEnableLeadConversion(true);
		CustomSettings.setEnableLeadMerging(true);
	}
	
	// Test what happens when multiple fxAccount inserts cause leads to convert
    static testMethod void testBulkTradedInsertSOQLLimits() {
    	final Integer size = 5;
    	
    	System.runAs(SYSTEM_USER) {
    		List<Lead> houseAccounts = new List<Lead>();
    		List<fxAccount__c> practiceAccounts = new List<fxAccount__c>();
	    	List<fxAccount__c> tradeAccounts = new List<fxAccount__c>();
	    	
	    	for (Integer i = 0; i < size; i++) {
	    		Lead lead = new Lead();
	    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		lead.LastName = 'Foobar';
	    		lead.Email = 'rand' + Math.round(Math.random() * 1000000) + '@example.org';
	    		
	    		houseAccounts.add(lead);
	    		
	    		
	    		fxAccount__c practiceAccount = new fxAccount__c();
	    		practiceAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
	    		practiceAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
	    		practiceAccount.Division_Name__c = 'OANDA Europe';
	    		
	    		practiceAccounts.add(practiceAccount);
	    		
	    		
	    		fxAccount__c tradeAccount = new fxAccount__c();
	    		tradeAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
	    		tradeAccount.Division_Name__c = 'OANDA Corporate';
    			tradeAccount.Lead_Score__c = 100;
    			tradeAccount.Email_Validated__c = true;
	    		
	    		tradeAccounts.add(tradeAccount);
	    	}
	    	
	    	insert houseAccounts;
	    	
	    	for (Integer i = 0; i < houseAccounts.size(); i++) {
	    		tradeAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
	    		practiceAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
	    	}
	    	
	    	insert practiceAccounts;
	    	
	    	
	    	test.startTest();
	    	
	    	insert tradeAccounts;
	    	
	    	test.stopTest();
    	}
    }
    
    // Test what happens when multiple fxAccount udpates cause leads to convert
    static testMethod void testBulkTradedUpdateSOQLLimits() {
    	final Integer size = 6;
    	
    	System.runAs(SYSTEM_USER) {
	    	List<Lead> houseAccounts = new List<Lead>();
    		List<fxAccount__c> practiceAccounts = new List<fxAccount__c>();
	    	List<fxAccount__c> tradeAccounts = new List<fxAccount__c>();
	    	
	    	for (Integer i = 0; i < size; i++) {
	    		Lead lead = new Lead();
	    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		lead.LastName = 'Foobar';
	    		lead.Email = 'rand' + Math.round(Math.random() * 1000000) + '@example.org';
	    		
	    		houseAccounts.add(lead);
	    		
	    		fxAccount__c practiceAccount = new fxAccount__c();
	    		practiceAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
	    		practiceAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
	    		practiceAccount.Division_Name__c = 'OANDA Europe';
	    		
	    		
	    		practiceAccounts.add(practiceAccount);
	    		
	    		
	    		fxAccount__c tradeAccount = new fxAccount__c();
	    		tradeAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
	    		tradeAccount.Funnel_Stage__c = FunnelStatus.FUNDED;
	    		tradeAccount.Division_Name__c = 'OANDA Corporate';
	    		
	    		tradeAccounts.add(tradeAccount);
	    	}
	    	
	    	insert houseAccounts;
	    	
	    	for (Integer i = 0; i < houseAccounts.size(); i++) {
	    		practiceAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
	    		tradeAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
	    	}
	    	
	    	insert practiceAccounts;
	    	
	    	insert tradeAccounts;
	    	
	    	for (Integer i = 0; i < houseAccounts.size(); i++) {
	    		tradeAccounts.get(i).Lead_Score__c = 100;
	    		tradeAccounts.get(i).Email_Validated__c = true;
	    		tradeAccounts.get(i).Funnel_Stage__c = FunnelStatus.TRADED;
	    	}
	    	
	    	test.startTest();
	    	
	    	update tradeAccounts;
	    	
	    	test.stopTest();
    	}
    }
    
	// Test what happens when multiple fxAccount inserts cause leads to convert
    static testMethod void testBulkInsertPracticeSOQLLimits() {
    	final Integer size = 10;
    	
    	List<Lead> houseAccounts = new List<Lead>();
    	List<fxAccount__c> tradeAccounts = new List<fxAccount__c>();
    	List<fxAccount__c> practiceAccounts = new List<fxAccount__c>();
    	
    	
    	for (Integer i = 0; i < size; i++) {
    		Lead lead = new Lead();
    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		lead.LastName = 'Foobar';
    		lead.Email = 'rand' + Math.round(Math.random() * 1000000) + '@example.org';
    		
    		houseAccounts.add(lead);
    		
    		fxAccount__c tradeAccount = new fxAccount__c();
    		tradeAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		tradeAccount.Funnel_Stage__c = FunnelStatus.FUNDED;
    		tradeAccount.Division_Name__c = 'OANDA Corporate';
    		tradeAccount.Lead_Score__c = 100;
    		tradeAccount.Email_Validated__c = false;
    		
    		tradeAccounts.add(tradeAccount);
    		
    		fxAccount__c practiceAccount = new fxAccount__c();
    		practiceAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    		practiceAccount.Funnel_Stage__c = FunnelStatus.DEMO_TRADED;
    		practiceAccount.Division_Name__c = 'OANDA Europe';
    		practiceAccount.Lead_Score__c = 50;
    		practiceAccount.Email_Validated__c = true;
    		
    		practiceAccounts.add(practiceAccount);
    	}
    	
    	insert houseAccounts;
    	
    	for (Integer i = 0; i < houseAccounts.size(); i++) {
    		tradeAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
    		practiceAccounts.get(i).Lead__c = houseAccounts.get(i).Id;
    	}
    	
    	insert tradeAccounts;
    	
    	test.startTest();
    	
    	insert practiceAccounts;
    	
    	test.stopTest();
    }
}