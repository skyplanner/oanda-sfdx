public with sharing class TrustedContactPersonUtil 
{
    public static set<Id> parentIds = new set<Id>{};

    /**
     *   @param:  oldRecord, Lead/Account
     *   @param:  newRecord, Lead/Account
    */
    public static void checkFlagChange(SObject oldRecord, SObject newRecord)
    {
        Boolean hasTCP_old = (boolean) oldRecord.get('Has_Trusted_Contact_Person__c');
        Boolean hasTCP_new = (boolean) newRecord.get('Has_Trusted_Contact_Person__c');

        if(hasTCP_old != hasTCP_new && !hasTCP_new)
        {
            parentIds.add((Id)newRecord.get('Id'));
        }
    }

    public static void deleteTCPRecords()
    {
        if(!parentIds.isEmpty())
        {
            Entity_Contact__c[] entityContacts = [select Id 
                                                From Entity_Contact__c
                                                where Type__c= 'Trusted Contact Person' AND 
                                                        (Account__c IN :parentIds OR
                                                            Lead__c IN :parentIds)];
            Database.Delete(entityContacts, false);
        }
    }
}