/**
 * @File Name          : SPBaseFinalizerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2023, 4:03:47 PM
**/
@IsTest
private without sharing class SPBaseFinalizerTest {

	@IsTest
	static void execute() {
		Test.startTest();
		System.enqueueJob(new SPDoNothingJob());
		Test.stopTest();
		System.assert(true, 'Error');
	}

	@IsTest
	static void checkError() {
		SPBaseFinalizer instance = new SPBaseFinalizer();
		Test.startTest();
		instance.checkError(false, null);
		instance.checkError(true, new SPException());
		Test.stopTest();
		System.assert(true, 'Error');
	}    
	
}