@isTest
private class ArticleListCtrl_Test {

	@testSetup
	static void setup () {
		ArticlesTestDataFactory.createArticlesWithDummyDocs();
	}

	@isTest
	static void countArticles_test() {
		ArticleListFilterCriteria criterias = new ArticleListFilterCriteria();
		criterias.status = 'Online';
		criterias.hasPreviewDoc = true;
		Test.startTest();
		Integer result = ArticleListCtrl.countArticles(criterias);
		Test.stopTest();
		System.assertEquals(3, result);
	}

	@isTest
	static void fetchArticles_test() {
		ArticleListFilterCriteria criterias = new ArticleListFilterCriteria();
		criterias.status = 'Online';
		criterias.hasPreviewDoc = true;
		criterias.searchText = 'Test Article 1';
		Integer pageSize = 20;
		Integer offset = 0;
		String sortBy = 'CreatedDate';
		Boolean isDescending = true;
		Test.startTest();
		List<FAQ__kav> result = ArticleListCtrl.fetchArticles(criterias, pageSize, offset, sortBy, isDescending);
		Test.stopTest();
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
	}

	@isTest
	static void getCategories_test() {
		List<DataCategoriesHelper.DataCategoryWrapper> result = ArticleListCtrl.getCategories();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getAudiences_test() {
		List<DataCategoriesHelper.DataCategoryWrapper> result = ArticleListCtrl.getAudiences();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getDivisions_test() {
		List<DataCategoriesHelper.DataCategoryWrapper> result = ArticleListCtrl.getDivisions();
		System.assertNotEquals(null, result);
	}

}