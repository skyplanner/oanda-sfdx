/**
 * @File Name          : ChatHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:11:56 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   acantero     Initial Version
**/
@isTest(isParallel = false)
private class ChatHelper_Test {

    // @testSetup
    // static void setup(){
    // }
    
    //empty list
    // @isTest
    // static void fillFromNFChatData_test() {
    //     Boolean error = false;
    //     Test.startTest();
    //     try {
    //         ChatHelper helper = new ChatHelper(null);
    //         helper.fillFromNFChatData();
    //     } catch (Exception ex) {
    //         error = true;
    //     }
    //     Test.stopTest();
    //     System.assertEquals(false, error);
    // }

    // //only pre form chat in list
    // @isTest
    // static void fillFromNFChatData_test2() {
    //     Boolean error = false;
    //     List<LiveChatTranscript> chatTranscriptList = 
    //         new List<LiveChatTranscript> {
    //             new LiveChatTranscript(
    //                 From_Pre_Chat_Form__c = true
    //             ) 
    //         };
    //     Test.startTest();
    //     try {
    //         ChatHelper helper = new ChatHelper(chatTranscriptList);
    //         helper.fillFromNFChatData();
    //     } catch (Exception ex) {
    //         error = true;
    //     }
    //     Test.stopTest();
    //     System.assertEquals(false, error);
    // }

    // @isTest
    // static void copyChatInfo_test() {
    //     List<nfchat__Chat_Log__c> nfChatList = new List<nfchat__Chat_Log__c> {
    //         new nfchat__Chat_Log__c(
    //             nfchat__Chat_Key__c = 'abcde',
    //             nfchat__Email__c = 'a@b.com',
    //             Language_Preference__c = OmnichanelConst.ENGLISH_LANG,
    //             Chat_Type_of_Account__c = OmnichanelConst.LIVE,
    //             Inquiry_Nature__c = null,
    //             Type__c = null
    //         )
    //     };
    //     Map<String,LiveChatTranscript> chatMap = new Map<String,LiveChatTranscript>{
    //         'abcde' => new LiveChatTranscript()
    //     };
    //     Test.startTest();
    //     new ChatHelper(null).copyChatInfo(nfChatList, chatMap);
    //     Test.stopTest();
    //     System.assertEquals('a@b.com', chatMap.values()[0].Chat_Email__c);
    // }

    //empty list
    @isTest
    static void checkMissedChats_test1() {
        Boolean error = false;
        Test.startTest();
        try {
            ChatHelper helper = new ChatHelper(null);
            helper.checkMissedChats(null);
        } catch (Exception ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @isTest
    static void checkMissedChats_test2() {
        Test.startTest();
        List<LiveChatTranscript> chatList = null;
        User supervisor = OmnichanelRoutingTestDataFactory.createChatSupervisor('s0004');
        System.runAs(supervisor) {
            OmnichanelRoutingTestDataFactory.createAgentServiceResWithSkill(
                UserInfo.getUserId(), 
                UserInfo.getUserName(),
                OmnichanelConst.OCAN_SKILL
            );
            chatList = 
                OmnichanelRoutingTestDataFactory.getNewChats(
                    OmnichanelConst.OANDA_CANADA,
                    OmnichanelConst.INQUIRY_NAT_LOGIN
                );
            insert chatList;
            LiveChatTranscript chat1 = 
                [select Id, Chat_Division__c from LiveChatTranscript where Id = :chatList[0].Id];
            chat1.Chat_Division__c = null; //force code to look for Account.Primary_Division_Name__c
            update chat1;
            chatList = [
                select 
                    Id, 
                    Is_HVC__c
                from 
                    LiveChatTranscript
            ];
            for(LiveChatTranscript chatObj : chatList) {
                chatObj.Status = OmnichanelConst.CHAT_STATUS_MISSED;
            }
            ChatHelper helper = new ChatHelper(chatList);
            helper.checkMissedChats(null);
        }
        Test.stopTest();
        List<ServiceResourceSkill> resSkillList = [
            select 
                ServiceResource.RelatedRecord.UserRole.DeveloperName,
                Skill.DeveloperName
            from 
                ServiceResourceSkill
        ];
        System.debug('resSkillList.size: ' + resSkillList.size());
        for(ServiceResourceSkill resSkill : resSkillList) {
            System.debug('resSkill: ' + resSkill);
            System.debug('user role: ' + resSkill.ServiceResource.RelatedRecord.UserRole.DeveloperName);
            System.debug('skill: ' + resSkill.Skill.DeveloperName);
        }
        Integer count = [select count() from SLA_Violation__c];
        System.assertEquals(chatList.size(), count);
    }

    

}