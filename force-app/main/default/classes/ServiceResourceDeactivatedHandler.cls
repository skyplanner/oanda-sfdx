/**
 * @File Name          : ServiceResourceDeactivatedHandler.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/16/2020, 6:23:43 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/16/2020   acantero     Initial Version
**/
public without sharing class ServiceResourceDeactivatedHandler {

    @InvocableMethod(label='ServiceResource onDeactivated' description='')
    public static void onServiceResourceDeactivated(List<String> userIdList) {
        if (
            (userIdList == null) ||
            userIdList.isEmpty()
        ) {
            return;
        }
        //else...
        Set<String> agentIdList = new Set<String>(userIdList);
        AgentInfoManager.deleteAgentsInfo(agentIdList);
    }

}