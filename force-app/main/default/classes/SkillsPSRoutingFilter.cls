/**
 * @File Name          : SkillsPSRoutingFilter.cls
 * @Description        : 
 * Returns as valid the PendingServiceRouting records that are being routed
 * using "Skills"
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/5/2024, 3:30:41 PM
**/
public inherited sharing class SkillsPSRoutingFilter 
	implements SPendingServiceRoutingFilter {

	public static final String CHECK_PSR_TRANSFER_FOR_MESSAGING_ROUTING_SETTING = 
		'Omnichannel_Check_PSR_Transfer_For_Msg';

	@TestVisible
	Boolean checkPsrTransferForMsg;

	List<PendingServiceRouting> pendingRoutingList;

	public SkillsPSRoutingFilter() {
		checkPsrTransferForMsg = SPSettingsManager.checkSetting(
			CHECK_PSR_TRANSFER_FOR_MESSAGING_ROUTING_SETTING
		);
	}

	/**
	 * Method created in case it is necessary to run a mass validation before
	 * individual validation
	 */
	public void prepareForFilter(
		List<PendingServiceRouting> pendingRoutingList
	) {
		this.pendingRoutingList = pendingRoutingList;
	}

	public Boolean isValidForRouting(PendingServiceRouting psr) {
		ServiceLogManager.getInstance().log(
			'isValidForRouting -> psr', // msg
			SkillsPSRoutingFilter.class.getName(), // category
			psr // details
		);
		if (psr == null) {
			return false;
		}
		// else...
		Boolean result = isValidForRouting(new PSRWrapper(psr));
		ServiceLogManager.getInstance().log(
			'isValidForRouting -> result: ' + result, // msg
			SkillsPSRoutingFilter.class.getName() // category
		);
		return result;
	}

	@TestVisible
	Boolean isValidForRouting(PSRWrapper psrWrapperObj) {
		Boolean result = (
			(psrWrapperObj.isPreferredUserRequired == false) && 
			(psrWrapperObj.isReadyForRouting == false) && 
			(psrWrapperObj.workItemId != null) &&
			(
				psrWrapperObj.routingType == 
				OmnichanelConst.SKILLS_BASED_ROUTING_TYPE
			)
		);

		if (result == true) {
			Schema.SObjectType workItemSObjType = 
				psrWrapperObj.workItemId.getSobjectType();
			result = isValidForRouting(
				psrWrapperObj, // psrWrapperObj
				workItemSObjType, // workItemSObjType
				checkPsrTransferForMsg // checkTransferForMessaging
			);
		}
		
		return result;
	}

	@TestVisible
	Boolean isValidForRouting(
		PSRWrapper psrWrapperObj,
		Schema.SObjectType workItemSObjType,
		Boolean checkTransferForMessaging
	) {
		Boolean result = true;

		if (
			(psrWrapperObj.isTransfer == true) &&
			(
				(workItemSObjType != Schema.MessagingSession.SObjectType) ||
				(checkTransferForMessaging == true)
			)
		) {
			result = false;
		}

		return result;
	}

	// ************************************************************************

	public class PSRWrapper {

		public Boolean isReadyForRouting {get; set;}
		public Boolean isTransfer {get; set;}
		public ID workItemId {get; set;}
		public String routingType {get; set;}
		public Boolean isPreferredUserRequired {get; set;}

		public PSRWrapper() {
			this.isReadyForRouting = false;
			this.isTransfer = false;
			this.isPreferredUserRequired = false;
		}

		public PSRWrapper(PendingServiceRouting psr) {
			this.isTransfer = psr.IsTransfer;
			this.isReadyForRouting = psr.IsReadyForRouting;
			this.workItemId = psr.WorkItemId;
			this.routingType = psr.RoutingType;
			this.isPreferredUserRequired = psr.IsPreferredUserRequired;
		}

	}
	
}