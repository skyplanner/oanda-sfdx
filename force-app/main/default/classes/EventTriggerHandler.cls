/**
 * @description       : SFDC API Platform event trigger handler
 * @author            : Michal Piatek (mpiatek@oanda.com)
 * @group             : 
 * @last modified on  : 28-05-2024
 * @last modified by  : Michal Piatek
**/
public virtual class EventTriggerHandler extends TriggerHandler {

    Integer SINGLE_TRANSACTION_EVENT_LIMIT = 2000;
    Integer MAX_REPLAY_COUNT = 10;

    public enum EventExecutionAction {
		RUN,
        REPUBLISH,
		TERMINATE
	}

    //SObject is parent class for Platform Event -> Api_Event_After__e or Api_Event_Immediate__e
    private List<SObject> newApiEvents;

    private Map<String, List<SObject>> eventsByHandlerTypeName = new Map<String, List<SObject>>();

    public EventTriggerHandler() {
      this.newApiEvents = (List<SObject>) Trigger.new ?? new List<SObject>();
      this.groupEventsByHandlerTypeName();
    }
    
    public override void afterInsert() {
        Map<String, String> implementationByHandlerTypeName = getHandlerImplementationByHandlerTypeName();

        /* Treat each event type like separate event 
            -> count replays and set max event batch size independently for each event type */
        for (String handlerType : eventsByHandlerTypeName.keySet()) {
            List<SObject> eventsForHandlerType = eventsByHandlerTypeName.get(handlerType);

            Map<EventExecutionAction, List<SObject>> eventsGrouppedByAction = groupEventsByDesiredAction(eventsForHandlerType);

            //Run
            List<SObject> eventsToRun = eventsGrouppedByAction.get(EventExecutionAction.RUN);
            callHandler(implementationByHandlerTypeName.get(handlerType), eventsToRun);

            //Republish
            List<SObject> eventsToRepublish = eventsGrouppedByAction.get(EventExecutionAction.REPUBLISH);
            republishEvents(eventsToRepublish);

            //Terminate
            List<SObject> eventsToTerminate = eventsGrouppedByAction.get(EventExecutionAction.TERMINATE);
            terminateEvents(eventsToTerminate);
        }
    }

    private void callHandler(String handlerName, List<SObject> events) {
        try {
            PlatformEventDispatch.IPlatformEventHandlerClass handler = 
            (PlatformEventDispatch.IPlatformEventHandlerClass) Type.forName(handlerName).newInstance();

            handler.handle(events);
        } catch (Exception ex) {
            Logger.error('Could not run handler for defined type: ' + handlerName, ex.getMessage() + '\n' + ex.getStackTraceString());
        }

    }

    @TestVisible
    private void terminateEvents(List<SObject> eventsToTerminate) {
        for(SObject event : eventsToTerminate) {
            LogEvt__e errLog = new LogEvt__e(
                Type__c = 'Error',
                Category__c = 'API_EVENT_HANDLER: ' + event.get('Handler_Type_Name__c'),
                Message__c = 'infitie loop on event: ' + event,
                Details__c = 'replay event replay count:  : ' +event.get('Replay_Counter__c'),
                Id__c = (String)UserInfo.getUserId()
            );
            EventBus.publish(errLog);
        }
    }

    @TestVisible
    private void republishEvents(List<SObject> events) {
        List<SObject> eventsToRepublish = new List<SObject>();
        for(SObject evt : events) {
            Integer counter = Integer.valueOf(evt.get('Replay_Counter__c'));

            SObject newEvt = evt.clone();
            newEvt.put('Replay_Counter__c', ++counter);
            eventsToRepublish.add(newEvt);
        }
        PlatformEventDispatch.insertGenericAfter(eventsToRepublish);
    }


    @TestVisible
    private Map<EventExecutionAction, List<SObject>> groupEventsByDesiredAction(List<SObject> events) {
        
        List<SObject> eventsToTerminate = new List<SObject>();
        List<SObject> eventsToRepublish = new List<SObject>();
        List<SObject> eventsToRun = new List<SObject>();

        for(SObject evt : events) {
            Integer replayCount = Integer.valueOf(evt.get('Replay_Counter__c'));

            if(replayCount >= MAX_REPLAY_COUNT) {
                eventsToTerminate.add(evt);
                continue;
            }
            
            if(eventsToRun.size() < SINGLE_TRANSACTION_EVENT_LIMIT){
                eventsToRun.add(evt);
                continue;
            }

            eventsToRepublish.add(evt);
        }

        return new Map<EventExecutionAction, List<SObject>>{
            EventExecutionAction.RUN => eventsToRun,
            EventExecutionAction.TERMINATE => eventsToTerminate,
            EventExecutionAction.REPUBLISH => eventsToRepublish
        };
    }

    private void groupEventsByHandlerTypeName() {
        for(SObject apiEvent : newApiEvents) {
            String handlerName = (String) apiEvent.get('Handler_Type_Name__c');
            List<SObject> currentEventsFromMap = this.eventsByHandlerTypeName.get(handlerName) ?? new List<SObject>();

            currentEventsFromMap.add(apiEvent);
            
            this.eventsByHandlerTypeName.put(handlerName, currentEventsFromMap);
        }
    }

    private Map<String, String> getHandlerImplementationByHandlerTypeName() {
        Map<String, String> implementationByHandlerType = new Map<String, String>();
        for(Api_Event_Handler_Config__mdt handlerConfig : Api_Event_Handler_Config__mdt.getAll().values()) {
            implementationByHandlerType.put(handlerConfig.Handler_Type_Name__c, handlerConfig.Implementation_Class__c);
        }
        return implementationByHandlerType;
    }
}