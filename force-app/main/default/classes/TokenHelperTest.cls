@IsTest
public class TokenHelperTest {
    @IsTest(SeeAllData=true)
    static void updateNamedCredentialTokenPositiveTest() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            TokenHelper.TokenHelperResult th = TokenHelper.updateNamedCredentialToken(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS);
            Assert.isTrue(th.isSuccess);
            Assert.areEqual(10, th.minToTokenExpire);
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void updateNamedCredentialTokenTokenNegativeTest1() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 404, 'Error', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            TokenHelper.TokenHelperResult th = TokenHelper.updateNamedCredentialToken('WRONG_CREDENTIALS', Constants.PRINCIPAL_NAME_CREDENTIALS);
        Assert.isFalse(th.isSuccess);
        Assert.areEqual('Error', th.status);
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void updateNamedCredentialTokenTokenNegativeTest2() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            TokenHelper.TokenHelperResult th = TokenHelper.updateNamedCredentialToken(Constants.TAS_API_NAMED_CREDENTIALS_NAME, '123');
        Assert.isNotNull(th.status);
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenPositiveTest() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS);
            TokenHelper.TokenHelperResult th = TokenHelper.manageNamedCredentialToken(Constants.TAS_API_NAMED_CREDENTIALS_NAME, (ConnectApi.Credential)credentialResultMap.get('credential'),  false);
            Assert.isTrue(th.isSuccess);
            Assert.areEqual(10, th.minToTokenExpire);
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenNegativeTest1() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 404, 'Error', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS);
            TokenHelper.TokenHelperResult th = TokenHelper.manageNamedCredentialToken(Constants.TAS_API_NAMED_CREDENTIALS_NAME, (ConnectApi.Credential)credentialResultMap.get('credential'), true);
            Assert.isFalse(th.isSuccess);
            Assert.areEqual('Error', th.status);
        Test.stopTest();

    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenNegativeTest2() {
        MockService mockData = new MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 404, '', 'Error', null);
        Test.setMock(HttpCalloutMock.class, mockData);
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS);
            TokenHelper.TokenHelperResult th = TokenHelper.manageNamedCredentialToken(Constants.TAS_API_NAMED_CREDENTIALS_NAME, (ConnectApi.Credential)credentialResultMap.get('credential'), true);
            Assert.isFalse(th.isSuccess);
            Assert.areEqual('Error', th.status);
        Test.stopTest();
    }

    @IsTest(SeeAllData=true)
    static void getMinutesBetweenNowAndTokenExpirationDatePositiveTest() {
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(Constants.TAS_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS);
            Assert.isTrue(TokenHelper.getMinutesBetweenNowAndTokenExpirationDate((ConnectApi.Credential)credentialResultMap.get('credential')) >= 0);
        Test.stopTest();
    }

    public class MockService implements HttpCalloutMock {
        private Integer statusCode;
        private String status;
        private String body;
        private Map<String, String> responseHeaders;
        private Map<String, MockService> responseByEndpoint;

        public MockService() {
            responseByEndpoint = new Map<String, TokenHelperTest.MockService>();
        }

        public MockService(Integer code, String body, String status, Map<String, String> responseHeaders) {
            this.statusCode = code;
            this.status = status;
            this.body = body;
            this.responseHeaders = responseHeaders;
        }

        public void addEndPoint(String endPoint, Integer code, String body, String status, Map<String, String> responseHeaders) {
            responseByEndpoint.put(endPoint, new MockService(code, body, status, responseHeaders));
        }

        public HttpResponse respond(HttpRequest request) {
            MockService instance = responseByEndpoint.get(request.getEndpoint());
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(instance.statusCode);
            res.setStatus(instance.status);
            res.setBody(instance.body);

            if (responseHeaders != null) {
                for (String key : responseHeaders.keySet()){
                    res.setHeader(key, responseHeaders.get(key));
                }
            }

            return res;
        }
    }
}