/**
 * Validates that each record contains a
 * unique value on the specified field.
 * @author: Fernando Gomez, SkyPlanner
 * @since 9/29/2021
 */
public without sharing class CustomUniquenessValidator {
	private List<SObject> records;
	private String fieldApiName;

	private Map<String, Integer> valueCount;
	private List<SObject> unique;
	private List<SObject> notUnique;

	/**
	 * @param records
	 * @param fieldApiName
	 */
	public CustomUniquenessValidator(List<SObject> records, String fieldApiName) {
		this.records = records;
		this.fieldApiName = fieldApiName;
		valueCount = new Map<String, Integer>();

		unique = new List<SObject>();
		notUnique = new List<SObject>();
	}

	/**
	 * Collect all records with duplicates
	 * @return the instance for chained calls
	 */
	public CustomUniquenessValidator collect() {
		String value, objectApiName, query;
		Set<String> values;
		Set<Id> excludeIds;

		// do nothing if no record
		if (!records.isEmpty()) {
			// we don't to fetch present records from the database
			excludeIds = new Set<Id>();
			values = new Set<String>();
			objectApiName = records[0].getSObjectType().getDescribe().getName();

			// we need to extract the values and start
			// a count.. then we'll add to that count 
			for (SObject record : records) {
				value = String.valueOf(record.get(fieldApiName));
				excludeIds.add(record.Id);

				// we do nothing for empty values
				if (String.isNotBlank(value)) {
					// we add the values to filter
					values.add(value);
					// we prepare the map
					addToMap(value);
				}
			}

			query = String.format(
				'SELECT {0} FROM {1} WHERE Id NOT IN :excludeIds ' +
				'AND {2} != NULL AND {3} IN :values',
				new List<String> {
					fieldApiName,
					objectApiName,
					fieldApiName,
					fieldApiName
				});

			// now we need to bring
			for (SObject record : Database.query(query)) {
				value = String.valueOf(record.get(fieldApiName));
				addToMap(value);
			}
		}

		// we now sort record into unique and not unique
		for (SObject record : records) {
			value = String.valueOf(record.get(fieldApiName));

			if (String.isNotBlank(value) &&
					valueCount.get(value) > 1)
				// this is not unique then
				notUnique.add(record);
			else
				unique.add(record);
		}

		return this;
	}

	/**
	 * 
	 * @return the records with a non-unique value
	 */
	public List<SObject> getUnique() {
		return unique;
	}

	/**
	 * 
	 * @return the records with a unique value
	 */
	public List<SObject> getNotUnique() {
		return notUnique;
	}

	/**
	 * @param value
	 */
	private void addToMap(String value) {
		valueCount.put(value,
			valueCount.containsKey(value) ? valueCount.get(value) + 1 : 1);
	}
}