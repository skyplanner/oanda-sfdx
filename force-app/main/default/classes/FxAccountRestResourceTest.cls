@isTest
private class FxAccountRestResourceTest {

	private static string restUrl = '/services/apexrest/User';

	static testMethod void testDoPostNoUsername() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    // req.params.put();
	
		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Username is required', result);
	}

	static testMethod void testDoPost() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    
	    //change these information to the request body
	    //req.params.put('username', 'testusername');
	    //req.params.put('email', 'test@oanda.com');
	    //req.params.put('lastname', 'testlastname');
	    
	    JSONGenerator generator = JSON.createGenerator(true); 
	    generator.writeStartObject();
	    generator.writeStringField('username', 'testusername');
	    generator.writeStringField('email', 'test@oanda.com');
	    generator.writeStringField('lastname', 'testlastname');
	    generator.writeStringField('recordType', 'trade');
	    generator.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator.getAsString());
	    
	
		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    fxAccount__c f = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'][0];
	    System.assertEquals(null, f.Account__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    Lead l = [SELECT Id, Email, LastName FROM Lead WHERE Id=:f.Lead__c][0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    
	}

	static testMethod void testJson() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    
	    //req.params.put('username', 'testusername');
	    //req.params.put('email', 'test@oanda.com');
	    //req.params.put('lastname', 'testlastname');
	    Map<String, String> jsonParam = new Map<String, String> {'Result' => 'Pass', 'Field' => 'Value'}; 
	    String jsonString = JSON.serialize(jsonParam);
	    //req.params.put('eid', jsonString);
	    
	    JSONGenerator generator = JSON.createGenerator(true); 
	    generator.writeStartObject();
	    generator.writeStringField('username', 'testusername');
	    generator.writeStringField('email', 'test@oanda.com');
	    generator.writeStringField('lastname', 'testlastname');
	    generator.writeStringField('recordType', 'trade');
	    generator.writeStringField('eid', jsonString);
	    generator.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator.getAsString());

		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    fxAccount__c f = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'][0];
	    System.assertEquals(null, f.Account__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    Lead l = [SELECT Id, Email, LastName, Phone, EID_Result__c FROM Lead WHERE Id=:f.Lead__c][0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('Field: Value<br>Result: <b>Pass</b><br>', l.EID_Result__c);
	}
	
	static testMethod void testDoPostUpdate() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    //req.params.put('username', 'testusername');
	    //req.params.put('email', 'test@oanda.com');
	    //req.params.put('lastname', 'testlastname');
	    //req.params.put('phone', '555-555-5555');
	    //req.params.put('birthdate', '2000-01-31');
	    //req.params.put('emailvalidated', 'true');
	    
	    JSONGenerator generator = JSON.createGenerator(true); 
	    generator.writeStartObject();
	    
	    generator.writeStringField('username', 'testusername');
	    generator.writeStringField('email', 'test@oanda.com');
	    generator.writeStringField('lastname', 'testlastname');
	    generator.writeStringField('phone', '555-555-5555');
	    generator.writeStringField('birthdate', '2000-01-31');
	    generator.writeStringField('emailvalidated', 'true');
	    generator.writeStringField('recordType', 'trade');
	    
	    generator.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator.getAsString());
	
		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    fxAccount__c f = [SELECT Id, Lead__c, Account__c, Name, Email_Validated__c FROM fxAccount__c WHERE Name='testusername'][0];
	    System.assertEquals(null, f.Account__c);
	    System.assertEquals(true, f.Email_Validated__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    Lead l = [SELECT Id, Email, LastName, Phone, Birthdate__c FROM Lead WHERE Id=:f.Lead__c][0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('555-555-5555', l.Phone);
	    System.assert(Date.newInstance(2000, 1, 31).isSameDay(l.Birthdate__c));

		req = new RestRequest();     
		RestContext.request = req;
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    //req.params.put('username', 'testusername');
	    //req.params.put('phone', '555-555-5556');
	    
	    JSONGenerator generator1 = JSON.createGenerator(true); 
	    generator1.writeStartObject();
	    generator1.writeStringField('username', 'testusername');
	    generator1.writeStringField('phone', '555-555-5556');
	    generator1.writeStringField('recordType', 'trade');
	    generator1.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator1.getAsString());

		System.debug('second post');
		
	    result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    List<fxAccount__c> fxas = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'];
	    System.assertEquals(1, fxas.size());
	    f=fxas[0];
	    System.assertEquals(null, f.Account__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    List<Lead> leads = [SELECT Id, Email, LastName, Phone FROM Lead WHERE Id=:f.Lead__c];
	    l=leads[0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('555-555-5556', l.Phone);
	}
	
	static testMethod void testCreateDemoLead(){
		RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	  
	    
	    JSONGenerator generator = JSON.createGenerator(true); 
	    generator.writeStartObject();
	    
	    generator.writeStringField('username', 'testusername');
	    generator.writeStringField('email', 'test@oanda.com');
	    generator.writeStringField('lastname', 'testlastname');
	    generator.writeStringField('phone', '555-555-5555');
	    generator.writeStringField('birthdate', '2000-01-31');
	    generator.writeStringField('emailvalidated', 'true');
	    generator.writeStringField('recordType', 'demo');
	    
	    generator.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator.getAsString());
	
		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	    System.assertEquals('Done.', result);
	    
	    fxAccount__c f = [SELECT Id, Lead__c, Account__c, Name, Email_Validated__c FROM fxAccount__c WHERE Name='testusername'][0];
	    System.assertEquals(null, f.Account__c);
	    System.assertEquals(true, f.Email_Validated__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    Lead l = [SELECT Id, Email, LastName, Phone, Birthdate__c FROM Lead WHERE Id=:f.Lead__c][0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('555-555-5555', l.Phone);
	    System.assert(Date.newInstance(2000, 1, 31).isSameDay(l.Birthdate__c));
	    
	    req = new RestRequest();     
		RestContext.request = req;
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	   
	    
	    JSONGenerator generator1 = JSON.createGenerator(true); 
	    generator1.writeStartObject();
	    generator1.writeStringField('username', 'testusername');
	    generator1.writeStringField('phone', '555-555-5556');
	    generator1.writeStringField('recordType', 'demo');
	    generator1.writeEndObject();
	    
	    req.requestBody = Blob.valueOf(generator1.getAsString());

		System.debug('second post');
		
	    result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    List<fxAccount__c> fxas = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'];
	    System.assertEquals(1, fxas.size());
	    f=fxas[0];
	    System.assertEquals(null, f.Account__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    List<Lead> leads = [SELECT Id, Email, LastName, Phone FROM Lead WHERE Id=:f.Lead__c];
	    l=leads[0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('555-555-5556', l.Phone);
	    
	    
	}
/*
	static testMethod void testDoPostUpdateAccount() {

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	
	    // pass the req and resp objects to the method     
	    req.requestURI = restUrl; 
	    req.httpMethod = 'POST';
	    req.params.put('username', 'testusername');
	    req.params.put('email', 'test@oanda.com');
	    req.params.put('lastname', 'testlastname');
	    req.params.put('phone', '555-555-5555');
	
		RestContext.request = req;
    	RestContext.response = res;
	    
	    String result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    fxAccount__c f = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'][0];
	    System.assertEquals(null, f.Account__c);
	    System.assertNotEquals(null, f.Lead__c);
	    
	    Lead l = [SELECT Id, Email, LastName, Phone FROM Lead WHERE Id=:f.Lead__c][0];
	    System.assertEquals('test@oanda.com', l.Email);
	    System.assertEquals('testlastname', l.LastName);
	    System.assertEquals('555-555-5555', l.Phone);

		req = new RestRequest();     
		RestContext.request = req;
	    req.requestURI = restUrl;
	    req.httpMethod = 'POST';
	    req.params.put('username', 'testusername');
	    req.params.put('phone', '555-555-5556');

    	BatchTriggerLeadAssignment.executeInline();
    	
    	Lead trade2 = [SELECT Id, OwnerId, Title, IsConverted FROM Lead WHERE Id = :f.Lead__c];
    	System.assertNotEquals(UserUtil.getSystemUserId(), trade2.OwnerId);
    	
    	BatchMergeLeadsScheduleable.executeInline();
    	BatchConvertLeadsScheduleable.executeInline();

    	trade2 = [SELECT Id, OwnerId, Title, IsConverted FROM Lead WHERE Id = :f.Lead__c];
    	System.assertEquals(true, trade2.IsConverted);
    	System.assertNotEquals(UserUtil.getSystemUserId(), trade2.OwnerId);

		System.debug('second post');
		
	    result = FxAccountRestResource.doPost();
	
	    System.assertEquals('Done.', result);
	    List<fxAccount__c> fxas = [SELECT Id, Lead__c, Account__c, Name FROM fxAccount__c WHERE Name='testusername'];
	    System.assertEquals(1, fxas.size());
	    f=fxas[0];
	    System.assertNotEquals(null, f.Account__c);
	    System.assertEquals(null, f.Lead__c);
	    
	    List<Account> accounts = [SELECT Id, PersonEmail, LastName, Phone FROM Account WHERE Id=:f.Account__c];
	    Account a=accounts[0];
	    System.assertEquals('test@oanda.com', a.PersonEmail);
	    System.assertEquals('testlastname', a.LastName);
	    System.assertEquals('555-555-5556', a.Phone);
	}*/
}