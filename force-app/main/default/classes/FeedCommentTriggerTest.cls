/**
 * Created by Agnieszka Kajda on 27/10/2023.
 */
@IsTest
private class FeedCommentTriggerTest {

    @IsTest
    static void insertFeedComment() {

        Case c = new Case(Subject='Test Case', Status = 'New');
        insert c;

        FeedItem f = new FeedItem();
        f.ParentId = c.Id;
        f.Body = 'test';
        insert f;

        User testU = new User(
                LastName='test user',
                Username='testuser1@spoanda.com',
                Email='testuser@spoanda.com',
                Alias='testuser',
                ProfileId=[SELECT Id FROM Profile WHERE Id = :UserInfo.getProfileId()].Id,
                TimeZoneSidKey='GMT',
                LocaleSidKey='en_US',
                EmailEncodingKey='ISO-8859-1',
                LanguageLocaleKey='en_US',
                User_Start_Date__c = System.now(),
                IsActive=true);
        insert testU;

        String result='';
        System.runAs(testU){
            FeedComment fc = new FeedComment();
            fc.CommentBody = 'Test';
            fc.FeedItemId = f.Id;
            try{
                insert fc;
            }catch(Exception e){
                result=e.getMessage();
            }
        }
        System.assertEquals(result.contains('You cannot create, edit nor delete comments.'), true, 'By default user cannot insert feed comments.');
        System.assertEquals(UserUtil.canUserModifyFeedComments(), false, 'By default user cannot insert feed comments.');
    }

}