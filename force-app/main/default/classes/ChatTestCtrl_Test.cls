/**
 * @File Name          : ChatTestCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2022, 11:42:34 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/22/2021, 4:10:25 PM   acantero     Initial Version
**/
@isTest
private without sharing class ChatTestCtrl_Test {

    @isTest
    static void test() {
        Test.startTest();
        ChatTestCtrl instance = new ChatTestCtrl();
        String isRedirect = instance.isRedirect;
        String languagePreference = instance.languagePreference;
        String customerLastName = instance.customerLastName;
        String customerEmail = instance.customerEmail;
        String accountType = instance.accountType;
        Pagereference result1 = instance.invokeChatPage();
        Pagereference result2 = instance.reset();
        Pagereference result3 = instance.invokeOfflineHoursPage();
        Test.stopTest();
        System.assert(String.isNotBlank(isRedirect));
        System.assert(String.isNotBlank(languagePreference));
        System.assert(String.isNotBlank(customerLastName));
        System.assert(String.isNotBlank(customerEmail));
        System.assert(String.isNotBlank(accountType));
        System.assertNotEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertNotEquals(null, result3);
    }
    
}