/**
 * @File Name          : CustomNotificationTypeHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/2/2021, 2:39:43 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/2/2021, 2:31:50 PM   acantero     Initial Version
**/
public inherited sharing class CustomNotificationTypeHelper {

    public static CustomNotificationType getByDevName(
        String devName, 
        Boolean required
    ) {
        String criteria = 'devName: ' + devName;
        CustomNotificationType result = null;
        if (String.isNotBlank(devName)) {
            List<CustomNotificationType> recList = [
                SELECT 
                    Id 
                FROM 
                    CustomNotificationType 
                WHERE 
                    DeveloperName = :devName
                LIMIT 1
            ];
            if (!recList.isEmpty()) {
                result = recList[0];
            }
        }
        return checkRequired(
            result,
            required,
            criteria
        );
    }

    @TestVisible
    static CustomNotificationType checkRequired(
        CustomNotificationType rec,
        Boolean required,
        String criteria
    ) {
        if (
            (rec == null) &&
            (required == true)
        ) {
            throw new CustomNotificationTypeHelperException(
                'CustomNotificationType not found, ' + criteria
            );
        }
        //else...
        return rec;
    }

    //**************************************************************

    public class CustomNotificationTypeHelperException extends Exception {
    }
    
}