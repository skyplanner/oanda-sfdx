/**
 * @File Name          : SPNoDataFoundException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/23/2022, 4:04:32 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2022, 3:35:21 PM   acantero     Initial Version
**/
public inherited sharing class SPNoDataFoundException extends Exception {

    public static SPNoDataFoundException getNew(
        String entityName,
        String condition
    ) {
        return new SPNoDataFoundException(
            entityName + ' not found, ' + condition
        );
    }

}