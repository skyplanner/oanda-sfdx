/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-03-2023
 * @last modified by  : Jakub Jaworski
**/
public with sharing class TasV20AccountCallout 
    extends TasCallout {

    public TasV20AccountCallout() {
        super();
    }

    /**
     * Get v20 account details by V20 Account Id
     */
    public TasV20AccsDetails getV20AccsDetails(
        EnvironmentIdentifier v20AccountId
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_V20_ACCOUNT_ENDPOINT),
            new List<String> {v20AccountId.Id, v20AccountId.env});

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);

        if (notFound())
            return null;
        
        if (!isResponseCodeSuccess())
            throw getError();

        return (TasV20AccsDetails) JSON.deserialize(
            resp.getBody(), TasV20AccsDetails.class);
    }

    /**
     * Update v20 account details by V20 Account Id
     */
    public void updateV20AccsDetails(
        EnvironmentIdentifier v20AccountId,
        TasV20AccountPatchRequest v20Account
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_V20_ACCOUNT_ENDPOINT),
            new List<String> {v20AccountId.id, v20AccountId.env});

        patch(resource, header, null, Json.serialize(v20Account, true));
        
        if (!isResponseCodeSuccess())
            throw getError();
    }

    /**
     * Update User v20 account details by user Id
     */
    public void updateDefaults(
        EnvironmentIdentifier fxTradeUserId,
        Map<String, Object> userV20Account
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_USER_V20_ACCOUNT_PATCH_ENDPOINT),
            new List<String> {fxTradeUserId.id, fxTradeUserId.env});

        patch(resource, header, null, Json.serialize(userV20Account, false));

        if (!isResponseCodeSuccess())
            throw getError();
    }

    /**
     * Update User account details by user Id
     */
    public void updateAccountInfo(
        EnvironmentIdentifier fxTradeUserId,
        TasUserAccountPatchRequest userAccount
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_USER_ACCOUNT_PATCH_ENDPOINT),
            new List<String> {fxTradeUserId.id, fxTradeUserId.env});

        patch(resource, header, null, Json.serialize(userAccount, true));

        if (!isResponseCodeSuccess())
            throw getError();
    }

    /**
     * Get v20 account details by User Id
     */
    public TasUserAccountGetResponse getV20AccountsByUser(
        EnvironmentIdentifier fxTradeUserId
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_USER_ACCOUNT_ENDPOINT),
            new List<String> {fxTradeUserId.Id, fxTradeUserId.env});

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);
        
        if (notFound())
            return null;
        
        if (!isResponseCodeSuccess())
            throw getError();
        
        return (TasUserAccountGetResponse) parseJsonToObject(
            TasUserAccountGetResponse.class, 
            TasV20AccsDetails.getPropertyMappings());
    }

    public TasGroupsInfoGetResponse getGroupInfo(
        String env, Integer divisionId
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_GROUPS_ENDPOINT),
            new List<String> {env, String.valueOf(divisionId)});

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);
        
        if (notFound())
            return null;
        
        if (!isResponseCodeSuccess())
            throw getError();

        return (TasGroupsInfoGetResponse) JSON.deserialize(
            resp.getBody(), TasGroupsInfoGetResponse.class);
    }

    public TasMT4ServersConfigGetResponse getMT4Servers(
        String env, List<Integer> pricingGroups
    ) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_MT4_SERVERS_ENDPOINT),
            new List<String> {env, String.join(pricingGroups, ',')});

        // Added an error fix when trying to perform a GET with “Content-Type”: “application/json”,
        // for Mule it works fine with/without this parameter in the header, for the User/Tas API this is critical.
        // If the problem will resolved on the server side, the fix can be removed.
        Map<String, String> headersForGetRequest = header.clone();
        if (headersForGetRequest.containsKey('Content-Type')) {
            headersForGetRequest.remove('Content-Type');
        }

        get(resource, headersForGetRequest, null);
        
        if (!isResponseCodeSuccess())
            throw getError();

        return (TasMT4ServersConfigGetResponse) JSON.deserialize(
            resp.getBody(), TasMT4ServersConfigGetResponse.class);
    }
    
    public void resendMT4Notification(EnvironmentIdentifier v20AccountId) {
        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.TAS_API_RESEND_MT4_NOTIFICATION_ENDPOINT),
            new List<String> {v20AccountId.id, v20AccountId.env});

        post(resource, header, null, '{}');
        
        if (!isResponseCodeSuccess())
            throw getError();
    }
}