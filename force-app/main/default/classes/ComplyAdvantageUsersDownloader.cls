/**
 * Downloads the comments of a comply advantage users.
 * @author Fernando Gomez
 * @since 11/15/2021
 */
public class ComplyAdvantageUsersDownloader
	extends ComplyAdvantageCallout {

	/**
	 * @param divisionName
	 * @param country
	 */
	public ComplyAdvantageUsersDownloader(
			String divisionName,
			String country) {
		// the instance is based on division and country in this case
		setupInstance(divisionName, country);
	}

	/**
	 * @param apiKey
	 */
	public ComplyAdvantageUsersDownloader(String apiKey) {
		// the instance is based on division and country in this case
		setupInstance(apiKey);
	}

	/**
	 * Doanloads and return the content.
	 * @return a list of users
	 */
	public List<ComplyAdvantageUser> getUsers() {
		ComplyAdvantageUsersResult result;

		try {
			// we try to obtain the details
			result = fetchUsers();

			// we return the list of comments
			return result.content != null ?
				result.content.get('data') : null;

		} catch (Exception ex) {
			throw fail(ex);
		}
	}

	/**
	 * 
	 * @return 
	 */
	private ComplyAdvantageUsersResult fetchUsers() {
		// we obtain all entities in the current page
		get(fetchUsersEndpoint(), null, getBaseParams());

		// we proceed on a valida response
		if (isResponseCodeSuccess())
			return ComplyAdvantageUsersResult.parse(resp.getBody());
		else
			throw getCalloutException();
	}

	/**
	 * @return the url of the entity provider servi ce
	 */
	private String fetchUsersEndpoint() {
		return SPGeneralSettings.getInstance().getValue('CA_Endpoint_Users');
	}
}