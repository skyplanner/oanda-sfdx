/**
 * @File Name          : SPInvalidParamException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/23/2022, 3:51:59 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2022, 3:34:33 PM   acantero     Initial Version
**/
public inherited sharing class SPInvalidParamException extends Exception {

    public static SPInvalidParamException getNew(
        String paramName
    ) {
        return new SPInvalidParamException(
            'Param "' + paramName + '" is blank'
        );
    }

    public static SPInvalidParamException getNewForList(
        String paramName
    ) {
        return new SPInvalidParamException(
            'Param "' + paramName + '" is null or empty'
        );
    }

}