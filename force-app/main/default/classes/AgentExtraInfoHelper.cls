/**
 * @File Name          : AgentExtraInfoHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/20/2021, 10:46:15 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/23/2021, 1:54:38 PM   acantero     Initial Version
**/
public inherited sharing class AgentExtraInfoHelper {

    public static Agent_Extra_Info__c getByAgent(
        String agentId
    ) {
        Agent_Extra_Info__c result = null;
        List<Agent_Extra_Info__c> agentExtraInfoList = [
            SELECT
                Id, 
                Non_HVC_Cases__c 
            FROM 
                Agent_Extra_Info__c
            WHERE 
                Agent_ID__c = :agentId
            LIMIT 1
        ];
        if (!agentExtraInfoList.isEmpty()) {
            result = agentExtraInfoList[0];
        }
        return result;
    }

    public static List<Agent_Extra_Info__c> getByAgentsForUpdate(
        Set<String> agentIdSet
    ) {
        if (
            (agentIdSet == null) ||
            agentIdSet.isEmpty()
        ) {
            return new List<Agent_Extra_Info__c>();
        }
        //else...
        List<Agent_Extra_Info__c> result = [
            SELECT
                Id, 
                Agent_ID__c,
                Non_HVC_Cases__c 
            FROM 
                Agent_Extra_Info__c
            WHERE 
                Agent_ID__c IN :agentIdSet
            FOR UPDATE
        ];
        return result;
    }

    public static Boolean checkByAgent(
        String agentId
    ) {
        List<Agent_Extra_Info__c> agentExtraInfoList = [
            SELECT
                Id, 
                Non_HVC_Cases__c 
            FROM 
                Agent_Extra_Info__c
            WHERE 
                Agent_ID__c = :agentId
            LIMIT 1
        ];
        Boolean result = !agentExtraInfoList.isEmpty();
        return result;
    }

}