/**
 * @File Name          : GetExternalFileUrlAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 11:38:35 AM
**/
public without sharing class GetExternalFileUrlAction { 

	@InvocableMethod(label='Get External File URL' callout=false)
	public static List<String> getExternalFileUrl(List<ActionInfo> infoList) {
		try {
			ExceptionTestUtil.execute();   
			List<String> result = new List<String>{ 'unknown' };
			// List<String> result = new List<String>{ null };

			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...     
			ActionInfo info = infoList[0];
			ExternalFileUrlInfo fileInfo = ExternalFileUrlInfo.getByKeyRegionAndLanguage(
				info.fileKey, // key
				info.region, // region
				info.language // language
			);

			if (fileInfo != null) {
				result = new List<String>{ fileInfo.url };
			}
			
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetExternalFileUrlAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'region' required = true)
		public String region;

		@InvocableVariable(label = 'language' required = false)
		public String language;

		@InvocableVariable(label = 'fileKey' required = true)
		public String fileKey;

	}

}