/**
 * @File Name          : ServiceQuestionsMapper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/12/2022, 8:36:31 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/30/2021, 5:21:12 PM   acantero     Initial Version
**/
public with sharing class ServiceQuestionsMapper {

    public final Boolean live;

    public String inquiryNatureId {get; private set;}
    public String specificQuestionId {get; private set;}

    @testVisible
    public String inquiryNatureMappedId {get; private set;}
    @testVisible
    public String typeId {get; private set;}

    @testVisible
    public Boolean inquiryNatureMappedHasSkill {get; private set;}
    @testVisible
    public String defaultSkill {get; private set;}

    public String inquiryNatureMapped {get; private set;}
    public String serviceType {get; private set;}
    public String serviceSubtype {get; private set;}

    public String skill {get; private set;}
    //public Boolean typeEnteredByAgent {get; private set;}
    //public Boolean subtypeEnteredByAgent {get; private set;}

    public static ServiceQuestionsMapper createFromUnmappedValues(
        Boolean live,
        String inquiryNature,
        String specificQuestion
    ) {
        ServiceQuestionsMapper instance 
            = new ServiceQuestionsMapper(live);
        instance.initFromUnmappedValues(
            inquiryNature,
            specificQuestion
        );
        return instance;
    }

    public static ServiceQuestionsMapper createFromMappedValues(
        Boolean live,
        String inquiryNatureMapped,
        String serviceType
    ) {
        ServiceQuestionsMapper instance = 
            new ServiceQuestionsMapper(live);
        instance.initFromMappedValues(
            inquiryNatureMapped,
            serviceType
        );
        return instance;
    }
    
    public ServiceQuestionsMapper(Boolean live) {
        this.live = live;
    }

    @testVisible
    void initFromUnmappedValues(
        String inquiryNature,
        String specificQuestion
    ) {
        inquiryNatureId = getInquiryNatureId(inquiryNature);
        specificQuestionId = getSpecificQuestionId(specificQuestion);
        if (String.isNotBlank(inquiryNatureId)) {
            getMappedValues();
        }
    }

    @testVisible
    void initFromMappedValues(
        String inquiryNatureMapped,
        String serviceType
    ) {
        Service_Inquiry_Nature_Mapped__mdt inquiryNatureMappedObj = 
            getInquiryNatureMappedObj(inquiryNatureMapped);
        if (inquiryNatureMappedObj == null) {
            return;
        }
        //else...
        inquiryNatureMappedId = inquiryNatureMappedObj.Id;
        inquiryNatureMappedHasSkill = inquiryNatureMappedObj.Has_a_Skill__c;
        defaultSkill = 
            inquiryNatureMappedObj.Default_Skill__r.Skill_Dev_Name__c;
        processMappedValues(serviceType);
    }

    @testVisible
    void processMappedValues(String serviceType) {
        if (inquiryNatureMappedHasSkill == true) {
            if (String.isNotBlank(defaultSkill)) {
                skill = defaultSkill;
            } else {
                getRoutingValues();
            }
            return;
        }
        //else... 
        typeId = getTypeId(serviceType);
        if (String.isNotBlank(typeId)) {
            getRoutingValues();
        }
        if (String.isBlank(skill)) {
            skill = defaultSkill;
        }
    }


    public static List<SubtypeByAgentInfo> getSubtypeByAgentList() {
        List<SubtypeByAgentInfo> result = new List<SubtypeByAgentInfo>();
        List<Service_Questions_Mapping__mdt> mappingList = [
            SELECT 
                Live__c, 
                Practice__c, 
                Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c, 
                Type__r.Type__c
            FROM
                Service_Questions_Mapping__mdt
            WHERE
                Subtype_entered_by_Agent__c = true
        ];
        for(Service_Questions_Mapping__mdt mapping : mappingList) {
            if (
                (mapping.Live__c == true) ||
                (mapping.Practice__c == true)
            ) {
                SubtypeByAgentInfo info = new SubtypeByAgentInfo(
                    mapping.Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c,
                    mapping.Type__r.Type__c
                );
                info.live = mapping.Live__c;
                info.practice = mapping.Practice__c;
                result.add(info);
            }
        }
        return result;
    }

    void getMappedValues() {
        String query = getMappedValuesQuery();
        Service_Questions_Mapping__mdt mapping = getMapping(query);
        if (mapping != null) {
            inquiryNatureMapped = mapping.Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c;
            serviceType = mapping.Type__r.Type__c;
            serviceSubtype = mapping.Subtype__r.Subtype__c;
        }
    }

    @testVisible
    void getRoutingValues() {
        String query = getRoutingValuesQuery();
        Service_Questions_Mapping__mdt mapping = getMapping(query);
        if (mapping != null) {
            skill = mapping.Skill__r.Skill_Dev_Name__c;
        }
    }

    public Service_Questions_Mapping__mdt getMapping(String query) {
        Service_Questions_Mapping__mdt result = null;
        List<Service_Questions_Mapping__mdt> mappingList = 
            Database.query(
                query
            );
        if (!mappingList.isEmpty()) {
            result = mappingList[0];
        }
        return result;
    }

    String getMappedValuesQuery() {
        //field tokens are used to prevent the fields used in the dynamic query from  
        //being deleted or renamed without the code finding out
        SObjectField liveToken = Service_Questions_Mapping__mdt.Live__c;
        SObjectField practiceToken = Service_Questions_Mapping__mdt.Practice__c;
        SObjectField inquiryNatureToken = Service_Questions_Mapping__mdt.Inquiry_Nature__c;
        SObjectField specificQuestionToken = Service_Questions_Mapping__mdt.Specific_Question__c;
        String result = 
            'SELECT ' +
            'Inquiry_Nature_Mapped__r.Inquiry_Nature_Mapped__c, ' +
            'Type__r.Type__c, ' + 
            'Subtype__r.Subtype__c, ' +
            'Id ' +
            'FROM Service_Questions_Mapping__mdt ' + 
            'WHERE ';
        result += (live == true)
            ? 'Live__c = true '
            : 'Practice__c = true ';
        if (String.isNotBlank(inquiryNatureId)) {
            result += 'AND Inquiry_Nature__c = :inquiryNatureId ';
        }
        if (String.isNotBlank(specificQuestionId)) {
            result += 'AND Specific_Question__c = :specificQuestionId ';
        }
        result += 'LIMIT 1 ';
        return result;
    }

    String getRoutingValuesQuery() {
        //field tokens are used to prevent the fields used in the dynamic query from  
        //being deleted or renamed without the code finding out
        SObjectField liveToken = Service_Questions_Mapping__mdt.Live__c;
        SObjectField practiceToken = Service_Questions_Mapping__mdt.Practice__c;
        SObjectField inquiryNatureMappedToken = Service_Questions_Mapping__mdt.Inquiry_Nature_Mapped__c;
        SObjectField typeToken = Service_Questions_Mapping__mdt.Type__c;
        String result = 
            'SELECT ' +
            'Skill__r.Skill_Dev_Name__c, ' +
            //'Type_entered_by_Agent__c, ' + 
            //'Subtype_entered_by_Agent__c, ' +
            'Id ' +
            'FROM Service_Questions_Mapping__mdt ' + 
            'WHERE ';
        result += (live == true)
            ? 'Live__c = true '
            : 'Practice__c = true ';
        if (String.isNotBlank(inquiryNatureMappedId)) {
            result += 'AND Inquiry_Nature_Mapped__c = :inquiryNatureMappedId ';
        }
        if (String.isNotBlank(typeId)) {
            result += 'AND Type__c = :typeId ';
        }
        result += 'LIMIT 1 ';
        return result;
    }

    public String getInquiryNatureId(String inquiryNature) {
        if (String.isBlank(inquiryNature)) {
            return null;
        }
        //else...
        String result = [
            SELECT
                Id
            FROM 
                Service_Inquiry_Nature__mdt
            WHERE 
                Inquiry_Nature__c = :inquiryNature
        ]?.Id;
        if (result == null) {
            throw new ServiceQuestionsMapperException(
                'Service_Inquiry_Nature__mdt record not found, inquiryNature: ' + 
                inquiryNature
            );
        }
        //else...
        return result;
    }

    public String getSpecificQuestionId(String specificQuestion) {
        if (String.isBlank(specificQuestion)) {
            return null;
        }
        //else...
        String result = [
            SELECT
                Id
            FROM 
                Service_Specific_Question__mdt
            WHERE 
                Question__c = :specificQuestion
        ]?.Id;
        if (result == null) {
            throw new ServiceQuestionsMapperException(
                'Service_Specific_Question__mdt record not found, specificQuestion: ' + 
                specificQuestion
            );
        }
        //else...
        return result;
    }

    public Service_Inquiry_Nature_Mapped__mdt getInquiryNatureMappedObj(
        String inquiryNatureMapped
    ) {
        Service_Inquiry_Nature_Mapped__mdt result  = null;
        if (String.isBlank(inquiryNatureMapped)) {
            return result;
        }
        //else...
        List<Service_Inquiry_Nature_Mapped__mdt> inquiryNatureMappedList = [
            SELECT
                Id,
                Has_a_Skill__c,
                Default_Skill__r.Skill_Dev_Name__c
            FROM 
                Service_Inquiry_Nature_Mapped__mdt
            WHERE 
                Inquiry_Nature_Mapped__c = :inquiryNatureMapped
            LIMIT 1
        ];
        if (inquiryNatureMappedList.isEmpty()) {
            System.debug(
                System.LoggingLevel.ERROR, 
                'Service_Inquiry_Nature_Mapped__mdt record not found, inquiryNatureMapped: ' + 
                inquiryNatureMapped
            );
        } else {
            result = inquiryNatureMappedList[0];
        }
        return result;
    }

    public String getTypeId(String serviceType) {
        if (String.isBlank(serviceType)) {
            return null;
        }
        //else...
        String result = [
            SELECT
                Id
            FROM 
                Service_Type__mdt
            WHERE 
                Type__c = :serviceType
        ]?.Id;
        if (result == null) {
            System.debug(
                System.LoggingLevel.ERROR, 
                'Service_Type__mdt record not found, serviceType: ' + 
                serviceType
            );
        }
        return result;
    }

    //********************************************************************* */

    public class ServiceQuestionsMapperException extends Exception {
    }

}