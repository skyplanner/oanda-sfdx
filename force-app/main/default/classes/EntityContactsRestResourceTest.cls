@IsTest
private class EntityContactsRestResourceTest {

    public static final Map<String, Object> POST_REQUEST_BODY = new Map<String, Object>{
            'first_name' => 'first_name',
            'last_name' => null,
            'telephone_number' => '1234',
            'email_address' => 'test@test.test',
            'street' => 'street',
            'city' => 'city',
            'province' => 'province',
            'country' => 'country',
            'postal_code' => '001',
            'third_party_determination' => false,
            'trusted_contact_person' => true,
            'relationship_to_me' => 'son'
    };

    @TestSetup
    static void initData(){
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Lead lead = testHandler.createTestLeadEid();

        List<fxAccount__c> fxAccounts = new List<fxAccount__c>{
            new fxAccount__c(
                Account__c = account.Id,
                Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING,
                RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
                Division_Name__c = 'OANDA Europe',
                Citizenship_Nationality__c = 'Canada',
                fxTrade_User_ID__c = 12345678,
                fxTrade_Global_ID__c = 12345678 + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15(),
                Email__c = account.PersonEmail,
                Name = 'testWithAccount'),
            new fxAccount__c(
                Lead__c = lead.Id,
                Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING,
                RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
                Division_Name__c = 'OANDA Europe',
                Citizenship_Nationality__c = 'Canada',
                fxTrade_User_ID__c = 87654321,
                fxTrade_Global_ID__c = 87654321 + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15(),
                Email__c = account.PersonEmail,
                Name = 'testWithlead')
        };
        insert fxAccounts;

        Entity_Contact__c entityContact = new Entity_Contact__c(
                First_Name__c = 'Test',
                Last_Name__c = 'Contact',
                RecordTypeId = RecordTypeUtil.getEntityContactGeneralRecordTypeId(),
                Type__c = EntityContactsRestResource.TRUSTED_CONTACT_PERSON,
                Account__c = account.Id
        );
        insert entityContact;
    }

    @IsTest
    static void testDoGetSuccess200() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/12345678/entity_contacts';
        req.httpMethod = 'GET';

        Test.startTest();
            EntityContactsRestResource.doGet();
        Test.stopTest();

        List<Object> response = (List<Object>) JSON.deserializeUntyped(res.responseBody.toString());

        Assert.areEqual(200, res.statusCode);
        Assert.areEqual(1,  response.size());
    }

    @IsTest
    static void testDoGetError500() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/qwer-1234/entity_contacts';
        req.httpMethod = 'GET';

        Test.startTest();
        EntityContactsRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(500, res.statusCode);
    }

    @IsTest
    static void testDoGetError404() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/1234444/entity_contacts';
        req.httpMethod = 'GET';

        Test.startTest();
        EntityContactsRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(404, res.statusCode);
    }

    @IsTest
    static void testDoGetError404Second() {
        fxAccount__c fxAccount = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE Account__c != NULL LIMIT 1];
        fxAccount.Account__c = null;
        update fxAccount;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/12345678/entity_contacts';
        req.httpMethod = 'GET';

        Test.startTest();
        EntityContactsRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(404, res.statusCode);
    }

    @IsTest
    static void testDoPutSuccess200() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/12345678/entity_contacts';
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(JSON.serialize(POST_REQUEST_BODY));

        Test.startTest();
        EntityContactsRestResource.doPut();
        Test.stopTest();

        Map<String, Object> response = ( Map<String, Object>) JSON.deserializeUntyped(res.responseBody.toString());
        Entity_Contact__c entityContact = [SELECT Id, Last_Name__c FROM Entity_Contact__c LIMIT 1];

        Assert.areEqual(200, res.statusCode);
        Assert.areEqual(true, (Boolean) response.get('isSuccessful'));
        Assert.areEqual(entityContact.Id, (Id) response.get('id'));
        Assert.areEqual(entityContact.Last_Name__c, 'Contact', 'Field was not update because in request last_name = null');
    }

    @IsTest
    static void testDoPutSuccess200Second() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/87654321/entity_contacts';
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(JSON.serialize(POST_REQUEST_BODY));

        Test.startTest();
        EntityContactsRestResource.doPut();
        Test.stopTest();
        System.debug('res.responseBody.toString() ' + res.responseBody.toString());
        Map<String, Object> response = ( Map<String, Object>) JSON.deserializeUntyped(res.responseBody.toString());
        Entity_Contact__c entityContact = [SELECT Id, Last_Name__c FROM Entity_Contact__c WHERE Lead__c IN (SELECT Id FROM Lead) LIMIT 1];

        Assert.areEqual(200, res.statusCode);
        Assert.areEqual(true, (Boolean) response.get('isSuccessful'));
        Assert.areEqual(entityContact.Id, (Id) response.get('id'));
        Assert.areEqual(2, [SELECT Id, Last_Name__c FROM Entity_Contact__c].size());
    }


    @IsTest
    static void testDoPutError500() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.response = res;
        RestContext.request = req;

        req.resourcePath = '/services/apexrest/api/v1/user/*/entity_contacts';
        req.requestURI = '/api/v1/user/qwer-1234/entity_contacts';
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(JSON.serialize(POST_REQUEST_BODY));

        Test.startTest();
        EntityContactsRestResource.doGet();
        Test.stopTest();

        Assert.areEqual(500, res.statusCode);
    }
}