/**
 * @File Name          : UpdateMsgCaseOriginAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/3/2024, 2:30:05 AM
**/
public without sharing class UpdateMsgCaseOriginAction {

	@InvocableMethod(label='Update Messaging Case Origin' callout=false)
	public static void updateMsgCaseOrigin(List<ID> messagingSessionIds) {  
		try {
			ExceptionTestUtil.execute();
		
			if (
				(messagingSessionIds == null) ||
				messagingSessionIds.isEmpty()
			) {
				return;
			}
			// else...      
			MessagingChatbotProcess process = 
				new MessagingChatbotProcess(messagingSessionIds[0]);
			process.updateMessagingCaseOrigin();

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				UpdateMsgCaseOriginAction.class.getName(),
				ex
			);
		}   
	}

}