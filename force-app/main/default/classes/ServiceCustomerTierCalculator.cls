/**
 * @File Name          : ServiceCustomerTierCalculator.cls
 * @Description        : 
 * @Author             : aniubo
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:06:46 AM
**/
public inherited sharing class ServiceCustomerTierCalculator 
	implements SCustomerTierCalculator {
		
	public String getTier(SCustomerTypeInfo customerInfo) {
		ServiceLogManager.getInstance().log(
			'getTier -> customerInfo', // msg
			ServiceCustomerTierCalculator.class.getName(), // category
			customerInfo // details
		);
		String tier = OmnichannelCustomerConst.TIER_4;

		if (
			(customerInfo.isALead == false) &&
			String.isNotBlank(customerInfo.fxAccountId)
		) {
			ServiceDivisionsManager divisionsMgr = ServiceDivisionsManager.getInstance();
			String division = String.isNotBlank(customerInfo.division) ?  
					divisionsMgr.getValidDivisionCode(customerInfo.division) : 
					null;
			if (
				String.isNotBlank(division) &&
				division == OmnichanelConst.OANDA_GLOBAL_MARKETS
			) {
				tier = customerInfo.isHVC != null && customerInfo.isHVC
					? OmnichannelCustomerConst.TIER_1
					: OmnichannelCustomerConst.TIER_2;
					
			} else {
				
				if (
					String.isNotBlank(customerInfo.segPL) &&
					(
						customerInfo.segPL != 
						OmnichannelCustomerConst.SEGMENTATION_BLANK
					 ) &&
					(   
						customerInfo.segPL !=
						OmnichannelCustomerConst.SEGMENTATION_INACTIVE
					)
				) {
					if (
						(
							customerInfo.segPL == 
							OmnichannelCustomerConst.SEGMENTATION_HIGH
						) ||
						(
							customerInfo.segPL ==
							OmnichannelCustomerConst.SEGMENTATION_HIGH_PLUS
						)
					) {
						tier = OmnichannelCustomerConst.TIER_1;
					
					} else if (
						customerInfo.segPL ==
						OmnichannelCustomerConst.SEGMENTATION_MID
					) {
						tier = OmnichannelCustomerConst.TIER_2;
						
					} else if (
						customerInfo.segPL ==
						OmnichannelCustomerConst.SEGMENTATION_LOW
					) {
						tier = OmnichannelCustomerConst.TIER_3;
					}
				}
			}
		}
		
		ServiceLogManager.getInstance().log(
			'getTier -> tier: ' + tier, // msg
			ServiceCustomerTierCalculator.class.getName() // category
		);
		return tier;
	}
}