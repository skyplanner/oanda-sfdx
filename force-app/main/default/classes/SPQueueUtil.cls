/**
 * @File Name          : SPQueueUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2022, 10:22:37 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/4/2021, 12:26:31 AM   acantero     Initial Version
**/
public without sharing class SPQueueUtil {

    public static final String QUEUE_TYPE = 'Queue';

    /**
     * Get gueue by dev name
     */
    public static Group getQueueByDevName(
        String queueDevName, 
        Boolean required)
    {
        Group result = null;
        List<Group> queueList =
            [SELECT Id 
                FROM Group 
                WHERE Type = :QUEUE_TYPE 
                AND DeveloperName = :queueDevName];

        if (!queueList.isEmpty()) {
            result = queueList[0];
        }

        if (result == null && required == true) {
            throw LogException.newInstance(
                'Queue not found: ' + queueDevName,
                Constants.LOGGER_GENERAL_CATEGORY,
                '');
        }

        return result;
    }

    public static List<Group> getByDevNames(
        List<String> queueDevNameList
    ) {
        if (
            (queueDevNameList == null) ||
            queueDevNameList.isEmpty()
        ){
            return new List<Group>();
        }
        //else...
        List<Group> result = [
            SELECT 
                Id 
            FROM
                Group 
            WHERE
                Type = :QUEUE_TYPE 
            AND 
                DeveloperName IN :queueDevNameList
        ];
        return result;
    }

    public static Set<ID> getQueueIdSetByDevNames(
        List<String> queueDevNameList
    ) {
        Set<ID> result = new Set<ID>();
        List<Group> queueList = getByDevNames(queueDevNameList);
        for(Group queueObj : queueList) {
            result.add(queueObj.Id);
        }
        return result;
    }

    public static ID getQueueIdByDevName(
        String queueDevName, 
        Boolean required
    ) {
        ID result = null;
        List<Group> queueList = [
            SELECT 
                Id 
            FROM
                Group 
            WHERE
                Type = :QUEUE_TYPE 
            AND 
                DeveloperName = :queueDevName
        ];
        if (!queueList.isEmpty()) {
            result = queueList[0].Id;
        }
        if (
            (result == null) &&
            (required == true)
        ) {
            throw LogException.newInstance(
                'Queue not found: ' + queueDevName,
                Constants.LOGGER_GENERAL_CATEGORY,
                '');

        }
        return result;
    }


    public static Boolean ownerIsAQueue(ID ownerId) {
        Schema.SObjectType ownerSObjType = ownerId.getSobjectType();
        Boolean result = (Schema.Group.SObjectType == ownerSObjType);
        return result;
    }

}