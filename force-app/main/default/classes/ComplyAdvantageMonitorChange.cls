/**
 * @description       : 
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : Silvia @Skyplanner
**/
public without sharing class ComplyAdvantageMonitorChange implements Queueable,Database.AllowsCallouts 
{ 
    Comply_Advantage_Search__c searchRecord;
    ComplyAdvantageMonitorNotification.MonitorData monitorInfo;
    ComplyAdvantageSearch searchModel;

    public ComplyAdvantageMonitorChange(Comply_Advantage_Search__c s, ComplyAdvantageMonitorNotification.MonitorData cd)
    {
       this.searchRecord = s;
       this.monitorInfo = cd;

       searchModel = new ComplyAdvantageSearch();
       searchModel.searchText = searchRecord.search_text__c;
       searchModel.caSearchId = searchRecord.Search_Id__c;
       searchModel.searchReference = searchRecord.Search_Reference__c;
       searchModel.fxAccountId = searchRecord.fxAccount__c;
       searchModel.fxAccount = searchRecord.fxAccount__r;
       searchModel.affiliateId = searchRecord.Affiliate__c;
       searchModel.affiliate = searchRecord.Affiliate__r;
       searchModel.divisonName = searchRecord.Division_Name__c;
       searchModel.mailingCountry = searchRecord.Search_Parameter_Mailing_Country__c;
      
       searchModel.caseId = searchRecord.Case__c; 
       if(searchRecord.Case__c != null)
       {
            searchModel.caseInfo = searchRecord.Case__r;
            searchModel.ContactId = searchRecord.Case__r.ContactId;
            searchModel.AccountId = searchRecord.Case__r.AccountId;
       }
       searchModel.reportPrefix = 'Comply Advantage Report (Monitored Status Change): ';
       searchModel.currentSearch = searchRecord; 
    }
    
    public void execute(QueueableContext context) 
    {  
        HttpResponse getSearchDetailsCalloutResponse =  getSearchDetailsCallout();
        HttpResponse getSearchReportCalloutResponse = getSearchReportCallout();

        updateEntities();

        handleCalloutResponses(getSearchReportCalloutResponse, getSearchDetailsCalloutResponse);
    }

    private void updateEntities()
    {
        ComplyAdvantageMonitorUpdateEntites caUpdateEntities = new ComplyAdvantageMonitorUpdateEntites(searchRecord, monitorInfo);
        if(LimitsUtil.canEnqueueJob()) 
		{        
		    System.enqueueJob(caUpdateEntities);
        }
        else 
        {
            caUpdateEntities.execute();
        }
	}

    private HttpResponse getSearchDetailsCallout()
    {
        Http http = new Http();     
        HttpRequest request = new HttpRequest();
    
        string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchModel.searchReference +'?api_key='+ searchModel.settings.apiKey + '&share_url=1';
        request.setEndpoint(serviceURL);
        request.setMethod('GET');
        request.setTimeout(120000);

        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 200)
        {
            Logger.error(
                'Comply advantage monitor change error',
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                request,
                response);

            http = new Http();   
            response = http.send(request);
        }
        return response;
    }

    public HttpResponse getSearchReportCallout()
    {
        Http http = new Http();
        HttpResponse response;

        try 
        {
            HttpRequest request = new HttpRequest();
            string serviceURL = 'https://api.complyadvantage.com/searches/'+ searchModel.searchReference +'/certificate?api_key='+ searchModel.settings.apiKey;
            request.setEndpoint(serviceURL);
            request.setMethod('GET');
            request.setTimeout(120000);
        
            response = http.send(request);
            if (response.getStatusCode() != 200)
            {
                Logger.error(
                    'Comply advantage monitor change error',
                    Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                    request,
                    response);

                http = new Http();   
                response = http.send(request);
            }            
        }
        catch(Exception e) 
        {
            Logger.exception(
                Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                e);
        }
        return response;
    }

    private void handleCalloutResponses(HttpResponse searchReportResponse, HttpResponse searchDetailsResponse)
    {
        if (searchReportResponse != null && searchReportResponse.getStatusCode() == 200)
        { 
            string documentPrefix = (searchModel.reportPrefix != null) ? searchModel.reportPrefix : 'Comply Advantage Report: ';
            string documentName = documentPrefix + searchModel.searchText;
            if(documentName.length() > 80 )
            {
                documentName = documentName.substring(0, 79);
            }

            Document__c doc = new Document__c();
            doc.name = documentName;
            doc.RecordTypeId = RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_DOCUMENT_CHECK, 'Document__c');
            doc.Document_Type__c = 'Comply Advantage';
            doc.Notes__c = searchModel.searchReason;
            doc.fxAccount__c = searchModel.fxAccountId;
            doc.Case__c = searchModel.caseId;
            doc.Account__c = searchModel.AccountId;
            doc.Affiliate__c = searchModel.affiliateId;
            doc.Comply_Advantage_Search__c = searchRecord.Id; //searchModel.sfSearchRecordId;
            Database.SaveResult docSaveResult =  Database.insert(doc);             
            
            Attachment attach = new Attachment();
            attach.Body = searchReportResponse.getBodyAsBlob(); 
            attach.Name = documentName;
            attach.ParentId = docSaveResult.getId();
            attach.ContentType = 'application/pdf';
            attach.IsPrivate = false;			
            insert attach;
        }

        if (searchDetailsResponse != null && searchDetailsResponse.getStatusCode() == 200)
        {  
            Case complyAdvantageCase = searchRecord.Case__r;
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(searchDetailsResponse.getBody());    
            Map<String, Object> resWrapper = (Map<String, Object>)results.get('content');
            Map<String, Object> responseData = (Map<String, Object>)resWrapper.get('data');   
            string matchStatus = (string)responseData.get('match_status');
            string shareUrl = (string)responseData.get('share_url');
            integer totalHits = (integer)responseData.get('total_matches');
            
            //update the comply Advantage Search with latest Results
            searchRecord.Total_Hits__c = totalHits;
            searchRecord.Report_Link__c = shareUrl;
            if(monitorInfo.is_suspended)
            {
                searchRecord.Is_Monitored__c = false;
                searchRecord.Is_Suspended__c = true;
            }
            if(matchStatus != searchRecord.Match_Status__c )
            {
                searchRecord.Match_Status__c = matchStatus;
                searchRecord.Match_Status_Override_Reason__c = 'dummy';
            }
            ComplyAdvantageHelper.updateComplyAdvantageSearch(searchRecord, false);  

            if(matchStatus != searchRecord.Match_Status__c )
            {
                searchRecord.Match_Status_Override_Reason__c = '';
                ComplyAdvantageHelper.updateComplyAdvantageSearch(searchRecord, false); 
            }
			
            //add comment about monitor status update
            CaseComment comment = new CaseComment();
            comment.CommentBody = getChangeSummary(matchStatus);
            comment.ParentId = complyAdvantageCase.Id;
            Insert comment;
            
            //Reopen the Comply Advantage case
            if(shouldReopenCase(matchStatus))
            {
                complyAdvantageCase.Priority = 'Critical';
                complyAdvantageCase.OwnerId = UserUtil.getQueueId('Comply Advantage Cases');
                if(complyAdvantageCase.status != 'Open' && complyAdvantageCase.status != 'Re-opened')
                {
                    complyAdvantageCase.status = 'Re-opened';
                }
                try
                {        
                    Update complyAdvantageCase;
                }
                catch(Exception e){
                    Logger.exception(
                        Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,
                        e);
                }
            }
        }    
    }

    private boolean shouldReopenCase(string matchStatus)
    {
        return  (matchStatus != searchRecord.Match_Status__c ||
                 (monitorInfo.added != null && monitorInfo.added.size() > 0) || 
                 (monitorInfo.updated != null && monitorInfo.updated.size() > 0) || 
                 // SP-13339 do not re-open the case if the only update is removal
                 // (monitorInfo.removed != null && monitorInfo.removed.size() > 0) || 
                 monitorInfo.is_suspended);
    }

    private string getChangeSummary(string matchStatus)
    {
        string changeSummary = 'Comply Advantage monitored status update for Search (' + searchModel.searchText + ')';

        if(monitorInfo.added != null && monitorInfo.added.size() > 0)
        changeSummary += '\n' +  monitorInfo.added.size() + ' New Matches';

        if(monitorInfo.updated != null && monitorInfo.updated.size() > 0)
        changeSummary +='\n' +  monitorInfo.updated.size() + ' Matches Updated';

        if(monitorInfo.removed != null && monitorInfo.removed.size() > 0)
        changeSummary += '\n' +  monitorInfo.removed.size() + ' Matches Removed';

        changeSummary += monitorInfo.is_suspended ? ('\n' +  'Search Suspended') : '';
        changeSummary += matchStatus != searchRecord.Match_Status__c ? ('\n Match status changed from ' + searchRecord.Match_Status__c + ' to ' + matchStatus) : '';
        return changeSummary;
    } 
}