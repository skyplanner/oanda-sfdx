/* Name: CaseCommentTriggerHandler
 * Description : Trigger Hanlder class for CaseComment
 * Author: ejung
 * Date : 2023-03-31
 */
public with sharing class CaseCommentTriggerHandler extends TriggerHandler {
    
    List<CaseComment> caseCommentList;

    /**
     * Constructor
     */
    public CaseCommentTriggerHandler() {
        this.caseCommentList = (List<CaseComment>) Trigger.new;
    }
    
    /**
     * Handles the before insert event
     */
    public override void afterInsert()
    {
        if(CustomSettings.isCustomNotificationForSalesEnabled()) 
        {
            CustomNotificationManager.sendNotificationsForOGMCases(caseCommentList, null);
        }
    }
}