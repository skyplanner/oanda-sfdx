global class NewComplyAdvantageSearch 
{
    webservice static void ComplyAdvantageSearchByFxAccountId(Id fxAccountId) 
    {
        if(string.isNotBlank(fxAccountId))
        {
            fxAccount__c fxa = new fxAccount__c(Id=fxAccountId);
            ComplyAdvantageHelper.initialComplyAdvantageSearch(fxa);  
        }
    }

    @InvocableMethod 
    public static void ComplyAdvantageSearchByFxAccountId(List<fxAccount__c> fxAccounts) 
    {
        if(fxAccounts != null && fxAccounts.size() > 0)
        {
            ComplyAdvantageHelper.initialComplyAdvantageSearch(fxAccounts[0]);  
        }
    }
}