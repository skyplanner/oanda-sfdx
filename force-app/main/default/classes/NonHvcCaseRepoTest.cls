/**
 * @File Name          : NonHvcCaseRepoTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/17/2024, 9:40:09 AM
**/
@IsTest
private without sharing class NonHvcCaseRepoTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.setFieldValue(
			ServiceTestDataKeys.NON_HVC_CASE_1, // objKey
			Non_HVC_Case__c.Status__c, // fieldToken
			OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED // newValue
		);
		ServiceTestDataFactory.createNonHVCCase1(initManager);
		initManager.storeData();
	}

	// valid params => result is not empty
	@IsTest
	static void getModifiedBeforeDate1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		String status = OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED;  
		Datetime lastModifiedDate = Datetime.now();
		Integer queryLimit = 100;

		Test.startTest();
		List<Non_HVC_Case__c> result = NonHvcCaseRepo.getModifiedBeforeDate(
			status, // status
			lastModifiedDate, // lastModifiedDate
			queryLimit // queryLimit
		);
		Test.stopTest();
		
		Assert.isFalse(
			result.isEmpty(), 
			'One record match filters and should be processed'
		);
	}

	// invalid params (status) => result is empty
	@IsTest
	static void getModifiedBeforeDate2() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		String status = OmnichanelConst.NON_HVC_CASE_STATUS_PARKED;  
		Datetime lastModifiedDate = Datetime.now();
		Integer queryLimit = 100;

		Test.startTest();
		List<Non_HVC_Case__c> result = NonHvcCaseRepo.getModifiedBeforeDate(
			status, // status
			lastModifiedDate, // lastModifiedDate
			queryLimit // queryLimit
		);
		Test.stopTest();
		
		Assert.isTrue(
			result.isEmpty(), 
			'No records match the filters and ' + 
			'therefore none should be processed.'
		);
	}

	// invalid params (lastModifiedDate) => result is empty
	@IsTest
	static void getModifiedBeforeDate3() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		String status = OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED;  
		Datetime lastModifiedDate = Datetime.now().addDays(-1);
		Integer queryLimit = 100;

		Test.startTest();
		List<Non_HVC_Case__c> result = NonHvcCaseRepo.getModifiedBeforeDate(
			status, // status
			lastModifiedDate, // lastModifiedDate
			queryLimit // queryLimit
		);
		Test.stopTest();
		
		Assert.isTrue(
			result.isEmpty(), 
			'No records match the filters and ' + 
			'therefore none should be processed.'
		);
	}
	
}