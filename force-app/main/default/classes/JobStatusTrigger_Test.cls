/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-28-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private without sharing class JobStatusTrigger_Test {
	@testSetup
	static void setup() {
		JobStatusTriggerHandler.enabled = false;
		ScheduledEmailTestUtil.createStatusJob(
			SendScheduledEmailJob.SEND_EMAIL_JOB,
			JobStatusHelper.INACTIVE_STATUS
		);
	}

	@isTest
	static void afterUpdate() {
		JobStatusTriggerHandler.enabled = true;
		Datetime requestDate = System.now();
		Test.startTest();
		Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
			SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		jobStatus.Status__c = JobStatusHelper.ACTIVE_STATUS;
		jobStatus.Request_Date__c = requestDate;
		update jobStatus;
		Test.stopTest();
		Integer errorCount = [SELECT COUNT() FROM Internal_Error__c];
		System.assertEquals(0, errorCount, 'No errors');
	}
}