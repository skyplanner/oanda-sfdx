/**
 * @File Name          : SPChatUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/24/2024, 8:45:20 PM
**/
public without sharing class SPChatUtil {

    public static List<String> getServPresenceStatusIdList(
        String channelDevName
    ) {
        List<String> result = new List<String>();
        if (String.isBlank(channelDevName)) {
            return result;
        }
        //else...
        List<ServiceChannelStatus> servChannelStatusList = [
            SELECT
                ServicePresenceStatusId
            FROM
                ServiceChannelStatus 
            WHERE 
                ServiceChannel.DeveloperName = :channelDevName
            AND
                IsDeleted = FALSE
        ];
        for(ServiceChannelStatus servChannelStatusObj : servChannelStatusList) {
            result.add(servChannelStatusObj.ServicePresenceStatusId);
        }
        return result;
    }

    public static Set<String> getAgentActiveSkills(String userId) {
        Set<String> result = new Set<String>();
        if (String.isBlank(userId)) {
            return result;
        }
        //else...
        Datetime now = Datetime.now();
        List<ServiceResource> serviceResList = [
            SELECT
                Id, 
                (
                    SELECT 
                        Skill.DeveloperName 
                    FROM 
                        ServiceResourceSkills 
                    WHERE 
                        IsDeleted = FALSE 
                    AND 
                        EffectiveStartDate <= :now 
                    AND 
                        (
                            (EffectiveEndDate = NULL) OR (EffectiveEndDate > :now)
                        )
                    ORDER BY
                        SkillId
                ) 
            FROM
                ServiceResource 
            WHERE 
                RelatedRecordId = :userId
            AND
                 ResourceType = :OmnichanelConst.SERVICE_RES_AGENT
        ];
        if (!serviceResList.isEmpty()) {
            ServiceResource serRes = serviceResList[0];
            if (serRes.ServiceResourceSkills != null) {
                for(ServiceResourceSkill serResSkill : serRes.ServiceResourceSkills) {
                    result.add(serResSkill.Skill.DeveloperName);
                }
            }
        }
        return result;
    }

    public static Set<String> getAgentWorkSkills(String workId) {
        Set<String> result = new Set<String>();
        if (String.isBlank(workId)) {
            return result;
        }
        //else...
        List<AgentWorkSkill> agentWorkSkills = [
            SELECT
                Skill.DeveloperName
            FROM
                AgentWorkSkill 
            WHERE 
                AgentWorkId = :workId
            AND
                WasDropped = FALSE
            ORDER BY
                SkillId
        ];
        for(AgentWorkSkill skillObj : agentWorkSkills) {
            result.add(skillObj.Skill.DeveloperName);
        }
        return result;
    }

    public static List<String> getActiveAgents(
        List<String> servicePresenceStatusIdList
    ) {
        ServiceLogManager.getInstance().log(
			'getActiveAgents -> ' +
			'servicePresenceStatusIdList', // msg
			SPChatUtil.class.getName(), // category
			servicePresenceStatusIdList // details
		);
        List<String> result = new List<String>();
        if (
            (servicePresenceStatusIdList == null) ||
            servicePresenceStatusIdList.isEmpty()
        ) {
            ServiceLogManager.getInstance().log(
                'getActiveAgents -> result', // msg
                SPChatUtil.class.getName(), // category
                result // details
            );
            return result;
        }
        //else...
        List<UserServicePresence> userServPresenceList = [
            SELECT
                UserId
            FROM
                UserServicePresence
            WHERE
                IsCurrentState = TRUE 
            AND 
                IsAway = FALSE
            AND
                ServicePresenceStatusId IN :servicePresenceStatusIdList
        ];
        for(UserServicePresence userServPresence : userServPresenceList) {
            result.add(userServPresence.UserId);
        }
        ServiceLogManager.getInstance().log(
            'getActiveAgents -> result', // msg
            SPChatUtil.class.getName(), // category
            result // details
        );
        return result;
    }

    public static List<String> getOtherActiveAgents(
        List<String> servicePresenceStatusIdList,
        String agentId
    ) {
        List<String> result = new List<String>();
        if (String.isEmpty(agentId)) {
            agentId = null;
        }
        if (
            (servicePresenceStatusIdList == null) ||
            servicePresenceStatusIdList.isEmpty() 
        ) {
            return result;
        }
        //else...
        List<UserServicePresence> userServPresenceList = [
            SELECT
                UserId
            FROM
                UserServicePresence
            WHERE
                IsCurrentState = TRUE 
            AND 
                IsAway = FALSE
            AND
                ServicePresenceStatusId IN :servicePresenceStatusIdList
            AND
                UserId <> :agentId
        ];
        for(UserServicePresence userServPresence : userServPresenceList) {
            result.add(userServPresence.UserId);
        }
        return result;
    }

    public static Boolean agentIsActive(String agentId) {
        if (String.isBlank(agentId)) {
            return false;
        }
        //else...
        Set<String> agentIdSet = new Set<String> { agentId };
        Set<String> activeAgents = checkActiveAgents(agentIdSet);
        Boolean result = activeAgents.contains(agentId);
        return result;
    }

    public static Set<String> checkActiveAgents(Set<String> agentIdSet) {
        Set<String> result = new Set<String>();
        if (
            (agentIdSet == null) ||
            agentIdSet.isEmpty()
        ) {
            return result;
        }
        //else...
        List<UserServicePresence> userServPresenceList = [
            SELECT
                UserId
            FROM
                UserServicePresence
            WHERE
                IsCurrentState = TRUE 
            AND
                UserId IN :agentIdSet
        ];
        for(UserServicePresence userServPres : userServPresenceList) {
            result.add(userServPres.UserId);
        }
        return result;
    }

    public static Integer getChatCapacityWeight() {
        Integer result = null;
        List<QueueRoutingConfig> routConfigList = [
            SELECT
                CapacityWeight 
            FROM 
                QueueRoutingConfig 
            WHERE 
                DeveloperName = :OmnichanelConst.CHAT_ROUTING_CONFIG
        ];
        if (!routConfigList.isEmpty()) {
            result = Integer.valueOf(routConfigList[0].CapacityWeight);
        }
        return result;
    }

}