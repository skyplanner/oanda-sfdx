/**
 * @File Name          : GetGeneralSettingActionTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 5:39:46 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/29/2024, 5:35:39 PM   aniubo     Initial Version
**/
@isTest
private class GetGeneralSettingActionTest {
	@isTest
	private static void testGetGeneralSettingEmpty() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<String> settings = GetGeneralSettingAction.getGeneralSetting(
			new List<String>()
		);

		Test.stopTest();
		System.assertEquals(
			true,
			settings != null,
			'Settings should not be null'
		);

		// Asserts
	}
	@isTest
	private static void testGetGeneralSetting() {

		Test.startTest();
		List<String> settings = GetGeneralSettingAction.getGeneralSetting(
			new List<String>{'Settings'}
		);

		Test.stopTest();
		System.assertEquals(
			true,
			settings != null,
			'Settings should not be null'
		);

		// Asserts
	}

	@isTest
	private static void testGetGeneralSettingEx() {

		ExceptionTestUtil.prepareDummyException();
		Boolean hasError = false;
		Test.startTest();
		try {
			List<String> settings = GetGeneralSettingAction.getGeneralSetting(
			new List<String>{'User_API_Get_Audit_Log_Endpoint'}
		);
		} catch (Exception ex) {
			hasError = true;
		}
		

		Test.stopTest();
		System.assertEquals(
			true,
			hasError,
			'Exception should be thrown'
		);

		// Asserts
	}
}