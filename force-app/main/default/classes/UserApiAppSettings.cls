/**
 * Manager class for User_API app settings
 */
public class UserApiAppSettings {   
    public List<UserApiAction> actions;
    public List<UserApiExtField> extFields;
    public List<UserApiSfField> sfFields;
    public List<String> sfQueryFields;
    public Map<String, Schema.SObjectField> fieldsMap;
    
    private static UserApiAppSettings instance;

    /**
     * Constructor
     */
    private UserApiAppSettings() {
        fieldsMap = CSUtils.getObjectMetadata(
            fxAccountUtil.NAME_SOBJECT_FXACCOUNT).fields.getMap();

        loadExtFields();
        loadSfFields();
    }

    /**
     * Load actions from settings
     */
    public static List<UserApiAction> getActions() {
        List<UserApiAction> actions = new List<UserApiAction>();

        for (User_API_Action__mdt astt : [SELECT Action__c, Label, Icon__c,
                DeveloperName, Environment__c, Order__c, Link__c,
                Custom_Permission__c, Manager_Class_Name__c
            FROM User_API_Action__mdt
            ORDER BY Order__c ASC]
        ) {
            actions.add(new UserApiAction(astt));
        }

        return actions;
    }

    /**
     * Load external fields from settings
     */
    private void loadExtFields() {
        extFields = new List<UserApiExtField>();
        
        for (User_API_Ext_Field__mdt f : 
            [SELECT Label,
                    Type__c,
                    Order__c,
                    Read_Only__c,
                    DeveloperName,
                    Show_on_List__c,
                    Show_on_Details__c
                FROM User_API_Ext_Field__mdt
                ORDER BY Order__c ASC])
        {
			extFields.add(new UserApiExtField(f));
        }
    }

    /**
     * Load sf fields from settings
     */
    private void loadSfFields() {
        sfFields = new List<UserApiSfField>();
        sfQueryFields = new List<String>();
        
        for (User_API_Sf_Field__mdt f : 
            [SELECT Label,
                    Order__c,
                    Read_Only__c,
                    Show_on_List__c,
                    Show_on_Details__c,
                    Field_Api_Name__c
                FROM User_API_Sf_Field__mdt
                ORDER BY Order__c ASC])
        {
			sfFields.add(
                new UserApiSfField(f, fieldsMap));
            sfQueryFields.add(f.Field_Api_Name__c);
        }
    }

    /**
     * Get settings class instance
     */
    public static UserApiAppSettings getInstance() {
        if (instance == null) {
            instance = new UserApiAppSettings();
        }

        return instance;
    }
}