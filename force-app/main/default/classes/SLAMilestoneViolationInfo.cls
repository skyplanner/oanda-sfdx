/**
 * @File Name          : SLAMilestoneViolationInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 3:48:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/6/2021, 4:20:12 PM   acantero     Initial Version
**/
public without sharing class SLAMilestoneViolationInfo extends SLAViolationInfo {

    public Milestone_Time_Settings__mdt milestoneSettings {get; set;}
    public String caseNumber {get; set;}

    public SLAMilestoneViolationInfo(
        String caseId,
        String caseNumber,
        Milestone_Time_Settings__mdt milestoneSettings
    ) {
        this.caseId = caseId;
        this.caseNumber = caseNumber;
        this.milestoneSettings = milestoneSettings;
        this.violationType = milestoneSettings.Violation_Type__c;
        this.channel = milestoneSettings.Channel__c;
    }
}