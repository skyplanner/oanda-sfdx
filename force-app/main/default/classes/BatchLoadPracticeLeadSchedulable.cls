public with sharing class BatchLoadPracticeLeadSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private Integer batchSize = 0;
	private boolean canBeRetried = false;
	
	public static final Integer BATCH_SIZE = 200;
	public static final DateTime TWO_HOURS_AGO = DateTime.now().addHours(-2);
	private static final String defaultQuery = 'select BI_Creation_DateTime__c, Channel__c, Country__c, Customer_Name__c, Division_Name__c, Email__c, Funnel_Stage__c, Processed__c, Language__c, Telephone__c, Traded__c, User_Name__c, zone__c from Practice_Lead__c where CreatedDate > ' +  SoqlUtil.getSoql(TWO_HOURS_AGO) + ' and Processed__c = false';
	private static final String retryQuery =   'select BI_Creation_DateTime__c, Channel__c, Country__c, Customer_Name__c, Division_Name__c, Email__c, Funnel_Stage__c, Processed__c, Language__c, Telephone__c, Traded__c, User_Name__c, zone__c from Practice_Lead__c where CreatedDate > ' +  SoqlUtil.getSoql(TWO_HOURS_AGO) + ' and Error__c = true';
	
	public BatchLoadPracticeLeadSchedulable(){
		
		setRetryInfo(BATCH_SIZE);
		query = defaultQuery;
	}
	
	public BatchLoadPracticeLeadSchedulable(Integer bSize){
		batchSize = bSize;
		query = defaultQuery;
	}
	
	public BatchLoadPracticeLeadSchedulable(String q){
		
		query = q;
	}
	
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'select BI_Creation_DateTime__c, Channel__c, Country__c, Customer_Name__c, Division_Name__c, Email__c, Funnel_Stage__c, Processed__c, Language__c, Telephone__c, Traded__c, User_Name__c, zone__c from Practice_Lead__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	//batch execute
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<Practice_Lead__c> pLeads = (List<Practice_Lead__c>)batch;
		
		PracticeLeadUtil.loadPracticeLeads(pLeads);
	}
	
	//schedulable execute
	public void execute(SchedulableContext context) {
		executeBatch();
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		if(batchSize > 1 && canBeRetried &&!isRerun){
			Integer errorCount = [select count() from Practice_Lead__c where createddate > : BatchLoadPracticeLeadSchedulable.TWO_HOURS_AGO and Error__c = true];
    	
	    	if(errorCount > 0){
	    		Database.executeBatch(new BatchLoadPracticeLeadSchedulable(retryQuery), 1);
	    	}
		}
		
		//start clean up after loading TODO
		
	}
	
	private void setRetryInfo(Integer bSize){
		batchSize = bSize;
		canBeRetried = true;
		
	}
	
	public static Id executeBatch() {
		return executeBatch(BATCH_SIZE);
	}
	
	public static Id executeBatch(Integer batchSize) {
		return Database.executeBatch(new BatchLoadPracticeLeadSchedulable(), batchSize);
	}
    
}