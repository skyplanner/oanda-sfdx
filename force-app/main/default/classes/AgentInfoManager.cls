/**
 * @File Name          : AgentInfoManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/24/2024, 9:20:37 PM
**/
public without sharing class AgentInfoManager {

    public static final Integer DEFAULT_CAPACITY_WEIGHT = 1;

    public static Boolean updateCurrentAgentInfo(
        Decimal capacity,
        String skills
    ) {
        String agentId = UserInfo.getUserId();
        String agentName = UserInfo.getUserName();
        return updateAgentInfo(
            agentId,
            agentName,
            capacity,
            skills
        );
    }

    public static Boolean updateAgentInfo(
        String agentId,
        String agentName,
        Decimal capacity,
        String skills
    ) {
        if (
            String.isBlank(agentId)
        ) {
            return false;
        }
        //else...
        Agent_Info__c agentInfo = new Agent_Info__c(
            Agent_ID__c = agentId,
            Capacity__c = capacity
        );
        if (String.isNotBlank(agentName)) {
            agentInfo.Name = agentName;
        }
        if (skills != null) {
            agentInfo.Skills__c = skills;
        }
        upsert agentInfo Agent_Info__c.Fields.Agent_ID__c;
        return true;
    }

    public static void deleteCurrentAgentInfo() {
        String agentId = UserInfo.getUserId();
        Boolean isActive = SPChatUtil.agentIsActive(agentId);
        safeDeleteAgentInfo(agentId, isActive);
    }

    @testVisible
    static void safeDeleteAgentInfo(String agentId, Boolean isActive) {
        if (
            (String.isBlank(agentId)) ||
            (isActive)
        ) {
            return;
        }
        //else...
        Set<String> agentIdSet = new Set<String>{ agentId };
        deleteAgentsInfo(agentIdSet);
    }

    public static void deleteAgentsInfo(Set<String> agentIdSet) {
        List<Agent_Info__c> agentInfoList = [
            SELECT
                Id
            FROM
                Agent_Info__c
            WHERE
                Agent_ID__c IN :agentIdSet
        ];
        if (!agentInfoList.isEmpty()) {
            delete agentInfoList;
        }
    }

    public static Boolean existsCompatibleAgent(
        List<String> userIdList,
        String skills, 
        Decimal capacityWeight
    ) {
        ServiceLogManager.getInstance().log(
			'existsCompatibleAgents -> ' +
			'capacityWeight: ' + capacityWeight +
			', skills: ' + skills +
			', userIdList: (details)', // msg
			AgentInfoManager.class.getName(), // category
			userIdList // details
		);

        if (capacityWeight == null) {
            capacityWeight = DEFAULT_CAPACITY_WEIGHT;
        }

        if (
            String.isBlank(skills) ||
            (userIdList == null) ||
            userIdList.isEmpty()
        ) {
            ServiceLogManager.getInstance().log(
				'existsCompatibleAgents -> result: null', // msg
				AgentInfoManager.class.getName() // category
			);
            return null;
        }
        // else...
        List<List<SObject>> results = [
            FIND 
                :skills
            RETURNING 
                Agent_Info__c(
                    Agent_ID__c, 
                    Capacity__c
                    WHERE
                        Agent_ID__c IN :userIdList 
                    AND 
                        Capacity__c >= :capacityWeight
                    LIMIT 1
                )
        ];
        List<SObject> agentInfoList = results[0];
        Boolean result = !agentInfoList.isEmpty();

        if (result == true) {
            Agent_Info__c agentInfoObj = (Agent_Info__c) agentInfoList[0];
            ServiceLogManager.getInstance().log(
                'existsCompatibleAgents -> agentInfoObj', // msg
                AgentInfoManager.class.getName(), // category
                agentInfoObj // details
            );
        }

        ServiceLogManager.getInstance().log(
            'existsCompatibleAgents -> result: ' + result, // msg
            AgentInfoManager.class.getName() // category
        );
        return result;
    }

}