/**
 * @File Name          : FxAccountMIFIDValidationSchedulable.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/23/2020, 11:03:27 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/22/2020   acantero     Initial Version
**/
global class FxAccountMIFIDValidationSchedulable implements Schedulable {
    
    global void execute(SchedulableContext sc) {
		FxAccountMIFIDValidationBatch validationBatch = new FxAccountMIFIDValidationBatch();
		Database.executeBatch(validationBatch, FxAccountMIFIDValidationBatch.BATCH_EXECUTION_SCOPE);
	}

}