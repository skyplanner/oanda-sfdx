/**
 * @File Name          : BaseServiceRoutingHelper.cls
 * @Description        : 
 * Basic implementation of the SRoutingHelper interface
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:30:37 AM
**/
public inherited sharing virtual class BaseServiceRoutingHelper 
	implements SRoutingHelper {

	protected SRoutingPriorityCalculator priorityCalculator;
	protected SSkillRequirementsManager skillReqManager;
	protected final List<BaseServiceRoutingInfo> infoList;

	public BaseServiceRoutingHelper(List<BaseServiceRoutingInfo> infoList) {
		this.infoList = infoList;
	}

	public void setPriorityCalculator(
		SRoutingPriorityCalculator priorityCalculator
	) {
		this.priorityCalculator = priorityCalculator;
	}

	public SRoutingPriorityCalculator getPriorityCalculator() {
		return priorityCalculator;
	}

	public void setSkillReqManager(SSkillRequirementsManager skillReqManager) {
		this.skillReqManager = skillReqManager;
	}

	public SSkillRequirementsManager getSkillReqManager() {
		return skillReqManager;
	}

	public virtual List<SkillRequirement> getSkillRequirements() {
		return skillReqManager.getSkillRequirements(infoList);
	}

	@TestVisible
	protected virtual Integer getDropAdditionalSkillsTimeout(
		BaseServiceRoutingInfo info
	) {
		return null;
	}

	public virtual void calculatePriority() {
		for (BaseServiceRoutingInfo info : infoList) {
			info.routingObj.RoutingPriority = 
				priorityCalculator.getRoutingPriority(info);
				
			// drop aditional skills
			Integer dropAdditionalSkillsTimeOut = 
				getDropAdditionalSkillsTimeout(info);
			
			if (dropAdditionalSkillsTimeOut != null) {
				info.routingObj.DropAdditionalSkillsTimeout = 
					dropAdditionalSkillsTimeOut;
			}
		}
	}

	/**
	 * To be overriden in descendents
	 */
	public virtual Boolean updateWorkItems() {
		return false;
	}

	/**
	 * To be overriden in descendents
	 */
	public virtual Boolean createRoutingLogs() {
		return false;
	}

}