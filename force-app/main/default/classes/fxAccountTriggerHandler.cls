public abstract class fxAccountTriggerHandler 
{
    public static Id registraionProfileID = UserUtil.getRegistrationProfileId();
    public static Id currentUserProfileId = userinfo.getProfileId();
    public static Id systemUserId = UserUtil.getSystemUserId();
    public static Id oapEscalationQueueId = UserUtil.getQueueId('OAP Escalations');
    public static Id supportCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Support').getRecordTypeId();

    public static set<string> relatedAccountFields = new set<string>
    {
        'Id',
        'fxAccount__c',
        'Date_Customer_Became_Core__c',
        'Date_Customer_Became_VIP__c'
    };
    public static set<string> relatedOpportunityFields = new set<string>
    {
        'Id',
        'fxAccount__c',
        'OwnerId'
    };
    public static set<string> relatedCaseFields = new set<string>
    {
        'id',
		'recordTypeId',
		'Status',
		'fxAccount__c',
		'AccountId',
		'Lead__c',
		'ContactId',
		'fxTrade_User_Id__c'
    };

    public static set<string> relatedEIDFields = new set<string>
    {
        'id',
        'fxAccount__c',
		'Provider__c',
		'Date__c'
    };
    
    protected boolean isInsert;
    protected boolean isUpdate;
    protected boolean isBefore;
    protected boolean isAfter;
    protected list<fxAccount__c> trgNew;
    protected Map<Id, fxAccount__c> trgNewMap;
    protected Map<Id, fxAccount__c> trgOldMap;
    protected fxAccountTriggerDataHandler fxaTrgDataHandler;
    protected set<Id> fxAccountIds;

    //related accounts
    protected Map<Id, Account> fxAccIdAccountMap;
    protected set<Id> relatedAccountIds;
    
    //related Opportunities
    protected Map<Id, Opportunity> fxAccIdOppMap;
    protected set<Id> relatedOpportunityIds;        
    
    //related Cases
    protected Map<Id, Map<Id, Case>> fxAccIdCasesMap; 
    protected Map<Id, Case> fxAccIdOBCaseMap;     

    //related EIDs
    protected Map<Id, EID_Result__c> fxAccIdEIDMap;
    protected Set<Id> relatedEIDIds;
    protected Set<Id> fxAccIdsToFireCAS;
    protected Set<Id> fxAccIdsToFireCASUsed;

    protected OutgoingNotificationBus outgoingNotification;

    public fxAccountTriggerHandler(boolean trgInsert,
                                   boolean trgUpdate,
                                   boolean trgIsBefore,
                                   boolean trgIsAfter,
                                   list<fxAccount__c> triggerNew,
                                   Map<Id, fxAccount__c> triggerOldMap)
     {
        fxaTrgDataHandler = new fxAccountTriggerDataHandler();
        fxAccountIds = new set<Id>{};
        fxAccIdsToFireCAS = new Set<Id>();
        fxAccIdsToFireCASUsed = new Set<Id>();
        outgoingNotification = new OutgoingNotificationBus();

        this.isInsert = trgInsert;
        this.isUpdate = trgUpdate;
        this.isBefore = trgIsBefore;
        this.isAfter = trgIsAfter;
        this.trgNew = triggerNew;
        this.trgOldMap = triggerOldMap;

        if(isAfter || isUpdate)
        {
            fxAccountIds = SObjectUtil.getIds(trgNew);
        }
     }

     public void populateRelatedAccounts()
     {
         fxAccIdAccountMap = new Map<Id, Account>{};
         relatedAccountIds = new set<Id>{};
         
         if(!fxAccountIds.isEmpty())
         {
             string accountQuery = 'Select ' +  soqlUtil.getSoqlFieldList(relatedAccountFields) + ' From Account Where fxAccount__c IN :fxAccountIds';
             for(Account ac : Database.query(accountQuery))
             {
                fxAccIdAccountMap.put(ac.fxAccount__c , ac);
                relatedAccountIds.add(ac.Id);
             }
         }
     }

     public void populateRelatedOpportunities()
     {
        fxAccIdOppMap = new Map<Id, Opportunity>{};
         relatedOpportunityIds = new set<Id>{};
         
         if(!fxAccountIds.isEmpty())
         {
             string opQuery = 'Select ' +  soqlUtil.getSoqlFieldList(relatedOpportunityFields) + ' From Opportunity Where fxAccount__c IN :fxAccountIds';
             for(Opportunity op : Database.query(opQuery))
             {
                fxAccIdOppMap.put(op.fxAccount__c , op);
                relatedOpportunityIds.add(op.Id);
             }
         }
     }

     public void populateRelatedCases()
     {
        fxAccIdCasesMap = new Map<Id, Map<Id, Case>>{}; 
        fxAccIdOBCaseMap = new Map<Id, Case>{};  
         
        if(!fxAccountIds.isEmpty())
        {
            string caseQuery = 'Select ' +  soqlUtil.getSoqlFieldList(relatedCaseFields) + ' From Case Where fxAccount__c IN :fxAccountIds';
            for(Case ca : Database.query(caseQuery))
            {
               Map<Id, Case> relatedCasesMap = fxAccIdCasesMap.get(ca.fxAccount__c);
               if(relatedCasesMap == null)
               {
                    relatedCasesMap = new Map<Id, Case>{};
               }
               relatedCasesMap.put(ca.Id, ca);
               fxAccIdCasesMap.put(ca.fxAccount__c, relatedCasesMap);
              
               if(ca.RecordTypeId == RecordTypeUtil.onboardingCaseRecordTypeId)
               {
                    fxAccIdOBCaseMap.put(ca.fxAccount__c, ca);
               }
            }
        }
     }

     public void populateRelatedEIDs()
     {
        fxAccIdEIDMap = new Map<Id, EID_Result__c>{};
        relatedEIDIds = new set<Id>{};
         
        if(!fxAccountIds.isEmpty())
        {
            string eidQuery = 'Select ' +  soqlUtil.getSoqlFieldList(relatedEIDFields) + ' From EID_Result__c Where fxAccount__c IN :fxAccountIds';
            for(EID_Result__c eid : Database.query(eidQuery))
            {
                fxAccIdEIDMap.put(eid.fxAccount__c, eid);
                relatedEIDIds.add(eid.Id);
            }
        }
     }

    public void publishOutgoingNotificationEvents() {
        outgoingNotification.publishPlatformEvents();
    }
}