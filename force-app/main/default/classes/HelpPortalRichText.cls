/**
 * @File Name          : HelpPortalRichText.cls
 * @Description        : 
 * @Author             : Fernando Gomez, Skyplanner LLC
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/4/2021, 1:55:30 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         4/30/2019                Fernando Gomez     Initial Version
**/
global without sharing class HelpPortalRichText {
	private static String FOLDER_NAME = 'HelpPortal';

	private String languageCode;
	private String documentPrefix;
	private String documentName;
	private String divisionCode;

	private Id folderId;
	private Id documentId;

	/**
	 * @param languageCode
	 * @param documentName
	 */
    global HelpPortalRichText(String languageCode, String documentPrefix,String divisionCode) {
		this.languageCode = languageCode;
		this.documentPrefix = documentPrefix;
		this.divisionCode = divisionCode;

		if (String.isBlank(documentPrefix))
			throw new HelpPortalRichTextException(
				'Document prefix is need to save the html.');

		folderId = getFolderId();
		checkRequiredFolder(folderId);

		documentName = getDocumentName();
		documentId = getDocumentId();
	}

	@testVisible
	void checkRequiredFolder(Id aFolderId) {
		if (aFolderId == null) {
			throw new HelpPortalRichTextException(
				'Setup missing, Folder not found.'
			);
		}
	}

	/**
	 * @return
	 */
	global String getHtml() {
		Blob r;
		if (documentId == null)
			return null;
		else {	
			r = [
				SELECT Body
				FROM Document 
				WHERE Id = :documentId
			][0].Body;
			return r == null ? '' : r.toString();
		}
	}

	/**
	 * 
	 * @param html
	 */
	global void saveHtml(String html) {
		Document d;
		String v = String.isBlank(html) ? '' : html;
		
        if (documentId == null){
            
       
			d = new Document(
				Name = documentName  + '.html',
				DeveloperName = documentName ,
				FolderId = folderId,
				Body = Blob.valueOf(v)
			);
                            System.debug('d'+d);

        }
		else {
			d = getDocument();
			d.Name = documentName + '.html';
			d.DeveloperName = documentName;
			d.Body = Blob.valueOf(v);
            System.debug('d'+d);
		}
		System.debug('d'+d.id);
		upsert d;
		documentId = d.Id;
	}

	/**
	 * @return the id of the folder in which 
	 * we need to store the documents
	 */
	private Id getFolderId() {
		try {
			return [
				SELECT Id
				FROM Folder 
				WHERE DeveloperName = :FOLDER_NAME
				LIMIT 1
			][0].Id;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @return the document in which 
	 * we need to store the html
	 */
	private Document getDocument() {
		try {
			return [
				SELECT Id
				FROM Document 
				WHERE FolderId = :folderId
				AND DeveloperName = :documentName
			];
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @return the id of the document in which 
	 * we need to store the html
	 */
	private Id getDocumentId() {
		Document d = getDocument();
		return d == null ? null : d.Id;
	}

	/**
	 * @return the constructed name of the document
	 */
	private String getDocumentName() {
        System.debug(divisionCode + '_'+ languageCode + '_' + documentPrefix);
		// return divisionCode + '_'+ languageCode + '_' + documentPrefix;
		return getDocumentName(
			languageCode,
			documentPrefix,
			divisionCode
		);
	}

	/**
	 * @return the constructed name of the document
	 */
	public static String getDocumentName(
		String pLanguageCode, 
		String pDocumentPrefix,
		String pDivisionCode
	) {
		return pDivisionCode + '_'+ pLanguageCode + '_' + pDocumentPrefix;
	}

	/**
	 * @return the html with static links fixed
	 */
	global String getHtmlLinkFixed(String regionCode) {
		Blob r;
		if (documentId == null)
			return null;
		else {	
			r = [
				SELECT Body
				FROM Document 
				WHERE Id = :documentId
			][0].Body;

			if(r == null)
			  return '';
			  
			//else  
			String html = r.toString();
			String regexTest ='http://staticlink_(\\w+)';	
			Matcher matcher =Pattern.compile(regexTest).matcher(html);
			
			Map<String, List<Chat_Offline_Links_Setting__mdt>> mapSetting = StaticLinkSettingManagement.getStaticLinkSettings(true);
			ChatOfflineSettings chatOffSett = ChatOfflineSettings.getDefaultInstance();
			String defaultRegion = !chatOffSett.initializationFails ? chatOffSett.defaultDivision : '';
			
			while (matcher.find()) {
				String word = matcher.group();			
				String[] wordSplit = word.split('_');
				String staticLinkLabel = wordSplit[1];
				String urlToReplace = '';
				//String urlToReplace = 'javascript:void(0)';
				if(mapSetting.containsKey(staticLinkLabel)){
					List<Chat_Offline_Links_Setting__mdt> linkSettList = mapSetting.get(staticLinkLabel);
					for(Chat_Offline_Links_Setting__mdt linkSett : linkSettList){
						if((linkSett.Division_Acronyms__c == regionCode) || 
						   (linkSett.Division_Acronyms__c == defaultRegion)){
							   urlToReplace = linkSett.URL_Name__c;
						 	 
						   if(linkSett.Division_Acronyms__c == regionCode)
							   break;
						}						 
					}
				}
                if(urlToReplace == ''){
					html = html.replaceAll('<a href="http://staticlink_'+staticLinkLabel+'[/]?" target="_blank">', 
										   '<a href="javascript:void(0)">');
				}
				else
				   html = html.replaceAll('http://staticlink_'+staticLinkLabel+'[/]?', urlToReplace);
			}
					
			return html;
		}
	}

	/**
	 * Throwable exception
	 */
	global class HelpPortalRichTextException extends Exception {  } 
}