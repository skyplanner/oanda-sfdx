/**
 * @File Name          : ServiceRoutingHelperTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/29/2024, 10:30:52 PM
**/
@IsTest
private without sharing class ServiceRoutingHelperTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	@IsTest
	static void test() {
		PendingServiceRouting psr = new PendingServiceRouting();
		ServiceRoutingInfo sRoutingInfo = new ServiceRoutingInfo(
			psr, // psrObj
			ServiceRoutingInfo.Source.CASES, // infoSource
			new Case() // workItem
		);
		sRoutingInfo.dropAdditionalSkills = true;
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> {sRoutingInfo };
	
		Test.startTest();
		ServiceRoutingHelper instance = new ServiceRoutingHelper(infoList);

		instance.setPriorityCalculator(new SRoutingPriorityProcess());
		SRoutingPriorityCalculator priorityCalculator = 
			instance.getPriorityCalculator();

		instance.setSkillReqManager(new ServiceSkillReqManager());
		SSkillRequirementsManager skillReqManager = 
			instance.getSkillReqManager();	

		List<SkillRequirement> skillRequirements = 
			instance.getSkillRequirements();

		instance.calculatePriority();
		Test.stopTest();
		
		Assert.isNotNull(priorityCalculator, 'Invalid result');
		Assert.isNotNull(skillReqManager, 'Invalid result');
		Assert.isNotNull(skillRequirements, 'Invalid result');
		Assert.isNotNull(psr.RoutingPriority, 'Invalid result');
	}

	@IsTest
	static void updateWorkItems() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		CaseSRoutingInfoManager routingInfoManager = 
			new CaseSRoutingInfoManager();
		ServiceRoutingInfo routingInfo = 
			(ServiceRoutingInfo) routingInfoManager.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new Case(
					Id = case1Id
				) // workItem
			);
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> { routingInfo };

		Test.startTest();
		ServiceRoutingHelper instance = new ServiceRoutingHelper(infoList);
		Boolean result = instance.updateWorkItems();
		Test.stopTest();
		
		Assert.isTrue(result, 'Invalid result');
	}

	@IsTest
	static void createRoutingLogs() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		CaseSRoutingInfoManager routingInfoManager = 
			new CaseSRoutingInfoManager();
		ServiceRoutingInfo routingInfo = 
			(ServiceRoutingInfo) routingInfoManager.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new Case(
					Id = case1Id
				) // workItem
			);
		List<ServiceRoutingInfo> infoList = 
			new List<ServiceRoutingInfo> { routingInfo };

		Test.startTest();
		ServiceRoutingHelper instance = new ServiceRoutingHelper(infoList);
		Boolean result = instance.createRoutingLogs();
		Test.stopTest();
		
		Integer count = [SELECT count() FROM Routing_Log__c];
		Assert.isTrue(result, 'Invalid result');
		Assert.areEqual(1, count, 'A Routing Log__c record must be created');
	}
	
}