/**
 * Provides access to the values in the custom metadata settings: SP_General_Setting
 * @author Fernando Gomez, SkyPlanner
 * @version 1.0
 */
public without sharing class SPGeneralSettings {
	private static SPGeneralSettings instance;
	private Map<String, String> values;

	/**
	 * Private constructor
	 */
	private SPGeneralSettings() {
		values = new Map<String, String>();

		for (SP_General_Setting__mdt stt : [
					SELECT QualifiedApiName, Value__c
					FROM SP_General_Setting__mdt
				])
			values.put(stt.QualifiedApiName, stt.Value__c);
	}

	/**
	 * @return the value of the setting with the specified api name
	 * @param settingApiName
	 */
	public String getValue(String settingApiName) {
		return values.get(settingApiName);
	}
	
	/**
	 * @return the value of the setting with the specified api name
	 * casted into a Boolean.
	 * @throws TypeException a type exception if the value cannot be casted
	 * @param settingApiName
	 */
	public Boolean getValueAsBoolean(String settingApiName) {
		String v = values.get(settingApiName);
		return v == null ? null : Boolean.valueOf(v);
	}
	

	public List<String> getValueAsList(String settingApiName) {
		String v = values.get(settingApiName);
		return String.isBlank(v) ? new List<String>() : v.split(',');
	}

	/**
	 * @return the value of the setting with the specified api name
	 * casted into a List<String>.
	 * @param settingApiName
	 * @param separator
	 */
	public List<String> getValueAsList(String settingApiName, String separator) {
		String v = values.get(settingApiName);
		return v == null ? new List<String>() : v.split(separator);
	}

	/**
	 * @return the value of the setting with the specified api name
	 * @throws TypeException a type exception if the value cannot be casted
	 * @param settingApiName
	 */
	public Integer getValueAsInteger(String settingApiName) {
		String v = values.get(settingApiName);
		return String.isBlank(v) ? null : Integer.valueOf(v);
	}


	/**
	 * @return the values stored in all settings
	 */
	public Map<String, String> getAll() {
		return values;
	}

	/**
	 * @return the only instance of this class allowed
	 */
	public static SPGeneralSettings getInstance() {
		if (instance == null)
			return new SPGeneralSettings();

		return instance;
	}

	/**
	 * @return the value of the setting with the specified api name
	 */
	@AuraEnabled
	public static Map<String, String> getGeneralSettingsValues() {
		try {
			return getInstance().getAll();
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}