/**
 * @File Name          : CreateMessagingCaseCommentsActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 12:44:08 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 1:14:14 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class CreateMessagingCaseCommentsActionTest {
	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			CreateMessagingCaseCommentsAction.createMessagingCaseComments(null);
			CreateMessagingCaseCommentsAction.createMessagingCaseComments(
				new List<CreateMessagingCaseCommentsAction.ActionInfo>()
			);
		} catch (Exception ex) {
			isError = true;
		}
		Test.stopTest();
		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}
	@isTest
	private static void testCreateMessagingCaseCommentsThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			CreateMessagingCaseCommentsAction.createMessagingCaseComments(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}
	@isTest
	private static void testTestCase() {
		// Test data setup
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID caseId = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		Id messageSessionID = null;
		Boolean isError = false;
		// Actual test
		Test.startTest();
		CreateMessagingCaseCommentsAction.ActionInfo info = new CreateMessagingCaseCommentsAction.ActionInfo();
		info.caseId = caseId;
		info.messagingSessionId = messageSessionID;
		try {
			CreateMessagingCaseCommentsAction.createMessagingCaseComments(
				new List<CreateMessagingCaseCommentsAction.ActionInfo>{ info }
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(false, isError, 'Exception should not have been threw');
	}
}