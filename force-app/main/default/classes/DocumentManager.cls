/**
 * Manage interaction with images for rich texts and such
 * @author Fernando Gomez, Skyplanner LLC
 * @since 4/30/2019
 */
global without sharing class DocumentManager {
	global static final String FOLDER_NAME = 'HelpPortal';
	global static final String PREVIEW_PREFIX = 'preview';

    @TestVisible
	private Id folderId;

	/**
	 * Main constructor
	 */
	global DocumentManager() {
		folderId = getFolderId();
	}

	/**
	 * Returns the document currently being used for preview, 
	 * or a new if it has not been created
	 */
	global Document createPreviewDocumentId(Id previewDocumentId) {
		String previewName = getPreviewName();
		try {
			return [
				SELECT Id,
					Body,
					ContentType,
					DeveloperName
				FROM Document 
				WHERE Id = :previewDocumentId
			];
		} catch (QueryException ex) {
			return new Document(
				Name = previewName + '.json',
				ContentType = 'application/json',
				DeveloperName = previewName,
				FolderId = folderId
			);
		}
	}

	/**
	 * Returns the document currently being used for preview.
	 */
	global Document getPreviewDocument(Id previewDocumentId) {
		return [
			SELECT Id,
				Body,
				ContentType,
				DeveloperName
			FROM Document 
			WHERE Id = :previewDocumentId
		];
	}

	/**
	 * Returns the document currently being used for preview, 
	 * or null if it does not exists.
	 */
	global Document getPreviewDocumentOrNull(Id previewDocumentId) {
		try {
			return getPreviewDocument(previewDocumentId);
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Saves the image as a document and returns the url so it can
	 * be used in the html
	 * @param fileName
	 * @param contentType
	 * @param base64Data
	 */
	global String convertFileToDocument(String fileName,
			String contentType, String base64Data) {
		Document d;
		String devName;

		// we ned a new dev name
		devName = getDeveloperName(fileName);

		// we create the document
		d = new Document(
			Name = fileName,
			ContentType = contentType,
			DeveloperName = devName,
			FolderId = folderId,
			Body = EncodingUtil.base64Decode(base64Data),
			IsPublic = true
		);

		insert d;
		return '/servlet/servlet.ImageServer' +
			'?id=' + d.Id +
			'&oid=' + UserInfo.getOrganizationId();
	}

	/**
	 * Saves the image as a document and returns the url so it can
	 * be used in the html
	 * @param html
	 */
	global String convertFileToDocument(Id contentVersionId) {
		ContentVersion version;
		Document d;
		String devName;

		// we need the content version
		version = getContentVersion(contentVersionId);
		devName = getDeveloperName(version.Title);

		// we create the document
		d = new Document(
			Name = version.Title,
			ContentType = version.FileType,
			DeveloperName = devName,
			FolderId = folderId,
			Body = version.VersionData,
			IsPublic = true
		);

		insert d;
		return '/servlet/servlet.ImageServer' +
			'?id=' + d.Id +
			'&oid=' + UserInfo.getOrganizationId();
	}

	/**
	 * @return the id of the folder in which 
	 * we need to store the documents
	 */
	global static Id getFolderId() {
		try {
			return [
				SELECT Id
				FROM Folder 
				WHERE DeveloperName = :FOLDER_NAME
				LIMIT 1
			][0].Id;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @param contentVersionId
	 * @return
	 */
	private ContentVersion getContentVersion(Id contentVersionId) {
		return [
			SELECT 
				Title,
				FileExtension,
				FileType,
				VersionData
			FROM ContentVersion
			WHERE Id = :contentVersionId
		];
	}

	/**
	 * @param
	 * @return
	 */
	private String getDeveloperName(String title) {
		return ('a' + System.now().getTime()).left(40);
	}

	/**
	 * @param
	 * @return
	 */
	private String getPreviewName() {
		return (PREVIEW_PREFIX + System.now().getTime()).left(40);
				
	}
}