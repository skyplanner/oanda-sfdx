/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 11-08-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchUpdatePaxosInAccount implements
    Database.Batchable<sObject>, Database.AllowsCallouts,Database.RaisesPlatformEvents, BatchReflection{
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchUpdatePaxosInAccount(String filter) {
        query = 'SELECT Id, Paxos_Status__c, fxAccount__c, fxAccount__r.Account__c, fxAccount__r.Account__r.Paxos_Status__c ' +
        'FROM Paxos__c ' +
        'WHERE Paxos_Status__c != NULL AND fxAccount__r.Account__r.Paxos_Status__c = NULL ' +
        (String.isNotBlank(filter) ? filter : '');
    }
    
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Paxos_Status__c, fxAccount__c, fxAccount__r.Account__c, fxAccount__r.Account__r.Paxos_Status__c ' +
        'FROM Paxos__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('start method: ' + Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Paxos__c> scope) {
        System.debug('scope: ' + scope);
        List<Account> accsToUpdate = new List<Account>();

        for (Paxos__c p : scope) {
            if (p.fxAccount__r.Account__r.Paxos_Status__c != p.Paxos_Status__c) {
                Account a = new Account(
                    Id = p.fxAccount__r.Account__c,
                    Paxos_Status__c = p.Paxos_Status__c
                );
                accsToUpdate.add(a);
            }
        }

        if (accsToUpdate.size() > 0) {
            update accsToUpdate;
        }
    }

    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        if (!Test.isRunningTest()) {
            try {
                EmailUtil.sendEmailForBatchJob(bc.getJobId());
            }
            catch (Exception e) {}
        }
	}
}