@isTest
private without sharing class RestResourceHelperTest {

    private static RestRequest req = new RestRequest();
    private static RestResponse res = new RestResponse();

    @isTest
    private static void shouldNotParsePathParamsWithoutConfig(){
        req.requestURI = 'test/123/test/456';
        req.resourcePath = 'test/*/abcd/*';

        RestResourceHelper context = new RestResourceHelper(req, res, null);

        Assert.areEqual(context.pathParams.keySet().size(),  0, 'Path params should be empty, because none was defined');
        Assert.isNull(context.getPathParam('invalid'), 'Path param should be null, as its not provided in mapping');
    }

    @isTest
    private static void shouldParsePathParameters(){
        req.requestURI = 'test/123/test/456';
        req.resourcePath = 'test/*/abcd/*';

        List<String> params = new List<String>{'param1', 'param2'};

        RestResourceHelper context = new RestResourceHelper(req, res, params);

        Assert.areEqual(context.pathParams.keySet().size(),  2, 'Path params should be parsed');
        Assert.areEqual(context.getPathParam(params[0]), '123', 'Should read first path param');
        Assert.areEqual(context.getPathParam(params[1]), '456', 'Should read second path param');
    }

    @isTest
    private static void shouldReturnEmptyStringWhenNoRequestBody(){

        RestResourceHelper context = new RestResourceHelper(req, res, null);

        Assert.areEqual(context.getBodyAsString(),  '', 'Body should be empty string');
    }

    @isTest
    private static void shouldReturnReqBodyAsString(){

        req.requestBody = Blob.valueOf('Testbody');

        RestResourceHelper context = new RestResourceHelper(req, res, null);

        Assert.areNotEqual(context.getBodyAsString(),  '', 'Body should not be empty string');
    }

    @isTest
    private static void shouldReturnReqBodyAsMap(){

        req.requestBody = Blob.valueOf('{ "test": "test"}');

        RestResourceHelper context = new RestResourceHelper(req, res);

        Map<String, Object> response = context.getBodyAsMap();
        Assert.areEqual((String) response.get('test'),  'test', 'Error reading response property');
    }

}