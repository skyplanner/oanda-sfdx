/**
 * @File Name          : SkillsHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/21/2024, 11:30:09 AM
**/
@isTest(isParallel = true)
private class SkillsHelper_Test {
    
    @isTest
    static void getLanguageSkill_test() {
        ServiceRoutingInfo info1 = new ServiceRoutingInfo(null, null, null);
        ServiceRoutingInfo info2 = new ServiceRoutingInfo(null, null, null);
        info2.language = 'Custom';
        ServiceRoutingInfo info3 = new ServiceRoutingInfo(null, null, null);
        info3.language = 'Custom';
        info3.alternativeLanguage = OmnichanelConst.SPANISH_LANG_CODE;
        Test.startTest();
        String result1 = SkillsHelper.getLanguageSkill(info1);
        String result2 = SkillsHelper.getLanguageSkill(info2);
        String result3 = SkillsHelper.getLanguageSkill(info3);
        Test.stopTest();
        System.assertEquals(OmnichanelConst.ENGLISH_SKILL, result1);
        System.assertEquals(OmnichanelConst.ENGLISH_SKILL, result2);
        System.assertEquals(OmnichanelConst.SPANISH_SKILL, result3);
    }

    @isTest
    static void getCustomerTypeSkill_test() {
        ServiceRoutingInfo info1 = new ServiceRoutingInfo(null, null, null);
        Test.startTest();
        String result1 = SkillsHelper.getCustomerTypeSkill(info1);
        Test.stopTest();
        System.assertEquals(null, result1);
    }

    @isTest
    static void getInquiryNatureSkill_test() {
        ServiceRoutingInfo info1 = 
            new ServiceRoutingInfo(null, ServiceRoutingInfo.Source.CHAT, null);
        ServiceRoutingInfo info2 = new ServiceRoutingInfo(null, null, null);
        info2.inquiryNature = OmnichanelConst.INQUIRY_NAT_FUNDING_WITHDRAWAL;
        info2.type = 'test type';
        ServiceRoutingInfo info3 = new ServiceRoutingInfo(null, null, null);
        info3.inquiryNature = 'test inquiryNature';
        Test.startTest();
        String result1 = SkillsHelper.getCustomerTypeSkill(info1);
        String result2 = SkillsHelper.getCustomerTypeSkill(info2);
        String result3 = SkillsHelper.getCustomerTypeSkill(info3);
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(null, result3);
    }

    @isTest
    static void getCryptoSkill() {
        ServiceRoutingInfo info1 = 
            new ServiceRoutingInfo(null, ServiceRoutingInfo.Source.CHAT, null);
        info1.crypto = true;
        //...
        ServiceRoutingInfo info2 = 
            new ServiceRoutingInfo(null, ServiceRoutingInfo.Source.CHAT, null);
        Test.startTest();
        String result1 = SkillsHelper.getCryptoSkill(info1);
        String result2 = SkillsHelper.getCryptoSkill(info2);
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.CRYPTO_SKILL, 
            result1, 
            'If crypto = true the associated skill should be returned'
        );
        System.assertEquals(
            null, 
            result2, 
            'If crypto = false null should be returned'
        );
    }
    @isTest
    static void getSeniorAgentSkill() {
        ServiceRoutingInfo info1 = 
            new ServiceRoutingInfo(null, ServiceRoutingInfo.Source.CHAT, null);
        info1.initTier(OmnichannelCustomerConst.TIER_1);
        //...
        ServiceRoutingInfo info2 = 
            new ServiceRoutingInfo(null, ServiceRoutingInfo.Source.CHAT, null);
            info2.initTier(OmnichannelCustomerConst.TIER_2);
        Test.startTest();
        String result1 = SkillsHelper.getSeniorAgentSkill(info1);
        String result2 = SkillsHelper.getSeniorAgentSkill(info2);
        Test.stopTest();
        System.assertEquals(
            OmnichanelConst.SENIOR_AGENT_SKILL, 
            result1, 
            'Iftier = OmnichannelCustomerConst.TIER_1 the associated skill should be returned'
        );
        System.assertEquals(
            null, 
            result2, 
            'tier != OmnichannelCustomerConst.TIER_1 null should be returned'
        );
    }

}