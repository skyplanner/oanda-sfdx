/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-13-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsAssignee {
    public Integer id { get; set; }
    
    public String email { get; set; }
    
    public String name { get; set; }

    public String phone { get; set; }

    public String created_at { get; set; }

    public Boolean user_is_active { get; set; }
}