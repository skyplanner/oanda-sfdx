/**
 * @File Name          : SLAViolationNotificationBaseBuilder.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/20/2024, 5:14:07 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/20/2024, 12:38:46 PM   aniubo     Initial Version
 **/
public inherited sharing abstract class SLAViolationNotificationBaseBuilder implements SLAViolationNotificationBuilder {
	protected SLAConst.SLAViolationType violationType { get; set; }
	protected Map<SLAConst.SLAViolationType, List<String>> placeholdersByViolationType {
		get;
		set;
	}
	protected Map<String, String> fieldsByPlaceholder { get; set; }
	protected SLAViolationInfo violationInfo { get; set; }
	protected Map<String, Object> violationInfoByKey { get; set; }

	public SLAViolationNotificationBaseBuilder(
		SLAConst.SLAViolationType violationType
	) {
		this.violationType = violationType;
		buildData();
	}

	public string buildBody(String body,SLAViolationInfo violationInfo) {
		buildViolationMap(violationInfo);
		return replacePlaceholders(body);
	}
	public string buildSubject(String subject,SLAViolationInfo violationInfo) {
		buildViolationMap(violationInfo);
		return replacePlaceholders(subject);
	}
	public abstract void buildData();

	protected virtual void buildViolationMap(SLAViolationInfo violationInfo) {
		String infoSer = JSON.serialize(violationInfo);
		violationInfoByKey = (Map<String, Object>) JSON.deserializeUntyped(
			infoSer
		);
	}

	protected virtual String replacePlaceholders(String text) {
		List<String> placeholders = placeholdersByViolationType.get(
			violationType
		);
		String replacedText = text;
		for (String placeholder : placeholders) {
			if (replacedText.contains(placeholder)) {
				if (fieldsByPlaceholder.containsKey(placeholder)) {
					String field = fieldsByPlaceholder.get(placeholder);
					String value = (String) violationInfoByKey.get(field);
					string validValue = SPTextUtil.getValueOrEmpty(value);
					replacedText = replacedText.replace(
						placeholder,
						validValue
					);
				}
			}
		}
		return replacedText;
	}
}