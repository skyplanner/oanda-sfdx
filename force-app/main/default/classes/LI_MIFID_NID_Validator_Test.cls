/**
 * @File Name          : LI_MIFID_NID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/9/2020, 3:39:20 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   acantero     Initial Version
**/
@istest
private class LI_MIFID_NID_Validator_Test {
    
    @istest
    static void validate_test() {
        Test.startTest();
        LI_MIFID_NID_Validator validator = new LI_MIFID_NID_Validator();
        String result1 = validator.validate(null);
        String result2 = validator.validate('A123');
        String result3 = validator.validate('A12345678B');
        Test.stopTest();
        System.assertEquals(System.Label.MifidValPasspFormatError, result1);
        System.assertEquals(System.Label.MifidValPasspFormatError, result2);
        System.assertEquals(null, result3);
    }

}