/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public with sharing class ScheduledEmailCheckerJob implements Schedulable {
	public void Execute(SchedulableContext context) {
		activateJob();
	}
	private void activateJob() {

		// job is not used anymore , we need consistency of the test methods though
		Job_Status__c jobStatus = JobStatusHelper.getJobStatusForUpdate(
		 	SendScheduledEmailJob.SEND_EMAIL_JOB
		);
		jobStatus.Status__c = JobStatusHelper.ACTIVE_STATUS;
		jobStatus.Request_Date__c = System.now();
		update jobStatus;
		// ---


		Schedule_Mail_Setting__mdt settings = ScheduleMailSettingRepo.getFirst();
		Database.executeBatch(new SendScheduledEmailBatch(), settings != null? Integer.valueOf(settings.Scope__c): 10);
	}
}