/**
 * @File Name          : InitMsgSessionAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/9/2024, 1:03:44 PM
**/
public without sharing class InitMsgSessionAction {

	@InvocableMethod(label='Init Messaging Session' callout=false)
	public static List<ActionResult> initMsgSession(List<ActionInfo> infoList) { 
		try {
			ExceptionTestUtil.execute();
			List<ActionResult> result = 
				new List<ActionResult> { new ActionResult() };

			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return result;
			}
			// else...        
			result[0] = initMsgSession(infoList[0]);
			return result;
			
		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				InitMsgSessionAction.class.getName(),
				ex
			);
		}
	} 

	@TestVisible
	static ActionResult initMsgSession(ActionInfo info) { 
		ActionResult result = new ActionResult();
		Boolean enhancedChannel = (info.enhancedChannel == true);
		InitMsgSessionProcess process = 
			new InitMsgSessionProcess(
				info.messagingSessionId,
				enhancedChannel
			);
		InitMsgSessionProcess.ProcessResult initResult = process.execute();
		result.region = initResult.region;
		result.requestRegion = initResult.requestRegion;
		result.caseNumber = initResult.caseNumber;
		result.caseIsClosed = initResult.caseIsClosed;
		result.userEmail = initResult.userEmail;
		return result;
	}

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public ID messagingSessionId;

		@InvocableVariable(label = 'enhancedChannel' required = false)
		public Boolean enhancedChannel;
		
	} 

	// *******************************************************************

	public class ActionResult {

		@InvocableVariable(label = 'region' required = false)
		public String region;

		@InvocableVariable(label = 'requestRegion' required = false)
		public Boolean requestRegion;

		@InvocableVariable(label = 'caseNumber' required = false)
		public String caseNumber;

		@InvocableVariable(label = 'caseIsClosed' required = false)
		public Boolean caseIsClosed;

		@InvocableVariable(label = 'userEmail' required = false)
		public String userEmail;

		public ActionResult() {
			this.requestRegion = false;
			this.caseIsClosed = false;
		}
		
	}

}