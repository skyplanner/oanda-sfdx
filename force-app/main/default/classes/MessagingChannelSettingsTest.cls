/**
 * @File Name          : MessagingChannelSettingsTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/13/2024, 11:38:25 PM
**/
@IsTest
private without sharing class MessagingChannelSettingsTest {

	@IsTest
	static void getByChannel() {
		// There should be at least one record of this custom metadata type
		Messaging_Channel_Settings__mdt settingsRec = [
			SELECT
				Channel_Type__c,
				Enhanced_Channel__c
			FROM Messaging_Channel_Settings__mdt
			LIMIT 1
		];
		String channelType = settingsRec.Channel_Type__c;
		Boolean enhancedChannel = settingsRec.Enhanced_Channel__c;

		Test.startTest();
		MessagingChannelSettings result = MessagingChannelSettings.getByChannel(
			channelType, // channelType
			enhancedChannel // enhancedChannel
		);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}
	
}