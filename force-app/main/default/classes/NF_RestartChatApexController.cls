/**
 * @File Name          : NF_RestartChatApexController.cls
 * @Description        : This class is Apex side controller for restart Chat compoent
 * @Author             : Ashish Jethi
 * @CreatedBy          : NeuraFlash, LLC
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 12:04:31 PM       Ashish Jethi               Initial Version
**/
public without sharing class NF_RestartChatApexController {
     /**
     * @description This method will return the community url
     * @return String
     */
     @AuraEnabled(cacheable=true)
     public static String getCommunityURL() {
         String commURL = URL.getSalesforceBaseUrl().toExternalForm();

         if(site.getPathPrefix() != null){
             commURL += site.getPathPrefix().removeEnd('/s');
         }

         return commURL;
     }
}