/**
 * @File Name          : LI_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/9/2020, 3:27:02 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   acantero     Initial Version
**/
public class LI_MIFID_NID_Validator extends FlexibleFormatValidator {

    public static final Integer LETTERS_COUNT = 2;
    public static final Integer DIGITS_COUNT = 8;

    public LI_MIFID_NID_Validator() {
        super(LETTERS_COUNT, DIGITS_COUNT);
    }

}