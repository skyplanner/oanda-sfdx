/**
 * @File Name          : MilestonesSLAViolationManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 4:51:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/8/2021, 9:48:50 AM   acantero     Initial Version
**/
public without sharing class MilestonesSLAViolationManager  {

    final List<Case> caseList;
    final Map<Id,Case> oldMap;

    public MilestonesSLAViolationManager(
        List<Case> caseList, 
        Map<Id,Case> oldMap
    ) {
        this.caseList = caseList;
        this.oldMap = oldMap;
    }

    public void processSLAViolations() {
        SLAViolationNotifiable  notifier = SLAViolationNotifiableFactory.SLAViolationNotifiableFactory(
            SLAConst.SLANotificationObjectType.CHAT_CASE_NOT, 
            SLAConst.SLAViolationType.AHT,
            new CaseSLAViolationNotifierDataReader()
        );
        notifier.notifyViolation(
            new CaseSLAViolationNotificationData(caseList, oldMap)
        );
    }

}