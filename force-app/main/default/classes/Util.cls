public with sharing class Util {
    /**
     * @return the current ESAPI SFDCAccessController object being used to maintain the access control rules for this application.
     */
    public static SFDCAccessController AccessController {
        get {
            if (AccessController == null) {
                AccessController = new SFDCAccessController();
            }
            return AccessController;
        } private set;
    }

    public static void log(String message){
        System.debug(message);
    }

    public static Boolean isListNullOrEmpty(List<Object> providedList) {
        return providedList == null || providedList.isEmpty();
    }

    public static Named_Credential_Configuration__mdt namedCredentialConfiguration {
        get {
            if (namedCredentialConfiguration == null) {
                String instanceName = System.DomainParser.parse(URL.getOrgDomainUrl()).getSandboxName();

                String credentialIdentity;
                //identify if prod, stage or onboarding/developer sandbox
                if (instanceName == null) {
                    credentialIdentity = Constants.CREDENTIAL_CONFIGURATION_NAME_PROD;
                } else if (instanceName == Constants.INSTANCE_NAME_STAGING) {
                    credentialIdentity = Constants.CREDENTIAL_CONFIGURATION_NAME_STG;
                } else {
                    credentialIdentity = Constants.CREDENTIAL_CONFIGURATION_NAME_DEV;
                }

                if (Named_Credential_Configuration__mdt.getInstance(credentialIdentity) != null) {
                    namedCredentialConfiguration = Named_Credential_Configuration__mdt.getInstance(credentialIdentity);
                }
            }
            return namedCredentialConfiguration;
        }
        private set;
    }

    /**
    * @param credentialName DeveloperName of named credentials
    * @return the Org suffix (_Dev) based on Named_Credential_Configuration__mdt
    */
    public static String getValidCredentialSuffix() {
        return namedCredentialConfiguration != null && namedCredentialConfiguration.Baypass_Instance_Credential__c == true
                ? namedCredentialConfiguration.Alternative_Credential__c
                : namedCredentialConfiguration.Default_Credential_Suffix__c;
    }

    public without sharing class WithoutSharing {
        public Boolean isUserHasPermission(String permissionSetName) {
            Set<Id> groupIds = new Set<Id>();

            for (PermissionSetGroupComponent permissionSetGroup : [SELECT PermissionSetGroupId FROM PermissionSetGroupComponent WHERE PermissionSet.Name = :permissionSetName]) {
                groupIds.add(permissionSetGroup.PermissionSetGroupId);
            }

            return [SELECT COUNT()
                FROM PermissionSetAssignment
                    WHERE AssigneeId = :UserInfo.getUserId()
                    AND (PermissionSet.Name = :permissionSetName OR PermissionSetGroupId IN :groupIds)
                    LIMIT 1
            ] == 1;
        }
    }

    /**
    * @param qualifiedApiNameOrDeveloperName developer name of Notification_about_object_changes__mdt record
    * @param division devision value from the record being updated
    * @param email email value from the record being updated
    * @param lastModifyUser lastModifyUser Id value from the record being updated
    * @return true or false based on the field values of the provided Notification_about_object_changes__mdt record
    * @description some of the Notification_about_object_changes__mdt fields can be removed after fully implementation
    */
    public static Boolean isSentChangeNotificationAvailable(String qualifiedApiNameOrDeveloperName, String division, String email, String lastModifyUser) {
        Boolean result = false;
        String emailDomain = String.isNotBlank(email) ? email.substringAfterLast('@') : '';

        ChangeNotification changeNotification;
        if (changeNotificationMap.containsKey(qualifiedApiNameOrDeveloperName)) {
            changeNotification = changeNotificationMap.get(qualifiedApiNameOrDeveloperName);
        } else {
            Notification_about_object_changes__mdt notificationAboutObjectChanges = Notification_about_object_changes__mdt.getInstance(qualifiedApiNameOrDeveloperName);
            if (notificationAboutObjectChanges == null) {
                return result;
            }
            changeNotification = new ChangeNotification();
            changeNotification.skipCheckForUsers =
                    String.isBlank(notificationAboutObjectChanges.Not_relevant_for_Users__c)
                            ? new Set<String>()
                            : new Set<String>(notificationAboutObjectChanges.Not_relevant_for_Users__c.split(','));
            changeNotification.actualForOandaEmails = notificationAboutObjectChanges.Enable_For_Oanda_Email_Domain_Only__c ?? false;
            changeNotification.useNewLogic = notificationAboutObjectChanges.Use_new_logic_instead_PushTopic__c ?? false;
            changeNotification.divisionNames =
                    String.isBlank(notificationAboutObjectChanges.Division_Names__c)
                            ? new Set<String>()
                            : new Set<String>(notificationAboutObjectChanges.Division_Names__c.split(','));
            changeNotificationMap.put(qualifiedApiNameOrDeveloperName, changeNotification);
        }

        Boolean isAvailableForUser = lastModifyUser == null || !changeNotification.skipCheckForUsers.contains(lastModifyUser);

        //Uncomment after fully implementation and remove check for actualForOandaEmails, useNewLogic and divisionNames
//      result = !changeNotification.skipCheckForUsers.contains(lastModifyUser);

        if (isAvailableForUser && changeNotification.useNewLogic) {
            return true;
        }

        if (isAvailableForUser && String.isNotBlank(division) && changeNotification.divisionNames.contains(division)) {
            return true;
        }

        if (isAvailableForUser && changeNotification.actualForOandaEmails && String.isNotBlank(emailDomain) && emailDomain == Constants.OANDA_DOMAIN) {
            return true;
        }

        return result;
    }

    private static Map<String, ChangeNotification> changeNotificationMap = new Map<String, ChangeNotification>();

    public class ChangeNotification {
        public Set<String> skipCheckForUsers = new Set<String>();
        public Boolean actualForOandaEmails = false;
        public Boolean useNewLogic = false;
        public Set<String> divisionNames = new Set<String>();
    }
}