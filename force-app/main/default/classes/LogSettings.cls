/**
 * Class to manage Log settings
 */
public class LogSettings {
    
    private static final Integer DEFAULT_DAYS_TO_STORE = 15;

    // Settings instance
    private static LogSettings instance = null;

    private LogSettings__c stts;
    private DateTime oldLimit;

    /**
     * Constructor
     */
    private LogSettings() {
        load();
    }

    /**
     * Get settings intance
     */
    public static LogSettings getInstance() {
        if(instance == null) {
            instance = new LogSettings();
        }
        return instance;
    }

    /**
     * Know if logs are active
     */
    public Boolean isActive() {
        return stts.Active__c;
    }

    /**
     * Know if logs debug are active
     */
    public Boolean isDebugActive() {
        return stts.Debug_Active__c;
    }

    /**
     * Set active flag
     */
    public void setActive(Boolean v) {
        stts.Active__c = v;
        update stts;
    }

    /**
     * Set debug flag
     */
    public void setDebug(Boolean v) {
        stts.Debug_Active__c = v;
        update stts;
    }

    /**
     * Get the date to compare and
     * know if a log should be deleted
     */
    public DateTime getOldDate() {
        return oldLimit;
    }

    /**
     * Load setting records
     */
    private void load() {
        // Load settings
        stts = LogSettings__c.getOrgDefaults();
        // Date to know if a log should be deleted
        oldLimit = DateTime.now().addDays(
            -(stts.Days_Store__c == null ? 
                DEFAULT_DAYS_TO_STORE : 
                stts.Days_Store__c.intValue()));
    }

}