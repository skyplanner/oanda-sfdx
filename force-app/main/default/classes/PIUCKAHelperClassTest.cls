@isTest (seeAllData = false)
public with sharing class PIUCKAHelperClassTest 
{
    static testMethod void getQandATest() 
    {
        List<Personal_Information_Update__c> items = PIUCKAHelperClass.getQandA(null);
        System.assertEquals(items.size(),0);
    }

	@isTest
	static void updateResultsOnfxAccount() {
        TestDataFactory factory = new TestDataFactory();
        Account accnt = factory.createTestAccount();
        fxAccount__c fxa = factory.createFXTradeAccount(accnt);
        fxa.Division_Name__c = 'OANDA Europe';
        update fxa;

        Test.startTest();

        fxa = [SELECT Id, Division_Name__c, PIU_Knowledge_Result__c FROM fxAccount__c WHERE Id = :fxa.Id];
        System.assertEquals(fxa.PIU_Knowledge_Result__c, null);

        Personal_Information_Update__c piu = new Personal_Information_Update__c(Knowledge_Assessment_Result__c = 'Pass', Trading_Experience_Result__c = 'Fail', Overall_Result__c = 'Fail', fxAccount__c = fxa.Id);
        insert piu;

        fxa = [SELECT Id, Division_Name__c, PIU_Knowledge_Result__c FROM fxAccount__c WHERE Id = :fxa.Id];
        System.assertEquals(fxa.PIU_Knowledge_Result__c, 'Pass');

		Test.stopTest();
	}
}