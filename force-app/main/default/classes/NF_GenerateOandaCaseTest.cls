/**
* Created by Neuraflash LLC on 03/07/18.
* Implementation : Chatbot
* Summary : Create a case record or lead & case record based on Account Object search result
* Details : 1. Searches for a account by email or souid provided.
*           2. Creates a new case if found an account from search filters.
*           3. Creates both case & lead record  if there is no account associated with account .
* 
*/
@isTest
public class NF_GenerateOandaCaseTest {
	@testSetup
	static void createTestData() {
	NF_TestDataUtill.createQueue(); 
        nfchat__Chat_Log__c chatLog = new nfchat__Chat_Log__c();
        chatLog.nfchat__First_Name__c = 'Joy';
        chatLog.nfchat_Case_Description__c = 'Description';
        chatLog.nfchat_Account_Type__c = 'Live';
        chatLog.nfchat__Last_Name__c = 'Smith';
        chatLog.nfchat__Phone__c = '1234567890';
        chatLog.nfchat_Username__c = 'joy@test.in';
        chatLog.nfchat__Email__c = 'joySmith@test.in';
        chatLog.nfchat__Company__c = 'Smith Tech';
        chatLog.nfchat__AI_Config_Name__c = 'AIConfig Test';
        chatLog.nfchat__Session_Id__c = '1234474y54734y';
        chatlog.Annual_Income__c = '123';
        chatlog.Authenticated__c = 'true';
        chatlog.Current_Employer__c = 'NF';
        chatlog.Income_Source__c = 'Inherited';
        chatlog.Industry__c = 'Test';
        chatlog.Job_Title__c = 'Dev';
        chatlog.Language_Preference__c = 'English';
        chatlog.Net_Worth__c = '1233456';
        chatlog.Job_Title__c = 'Dev';
        chatlog.EmailCheck__c = 'true';
        chatlog.Case_Type__c = 'Live';
        chatlog.Case_Sub_Type__c = 'Individual';
        insert chatLog;
        nfchat__Chat_Log_Detail__c chatLogDetail = new nfchat__Chat_Log_Detail__c();
        chatLogDetail.nfchat__Intent_Name__c = 'business.account.registration';
        chatLogDetail.nfchat__Chat_Log__c = chatLog.id;
        insert chatLogDetail;
	}
   
	static testMethod void CreateCaseRecord() {
		
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(); // record type choice
	    Account acc = new Account();
		acc.RecordTypeId = accountRecordTypeId;
		acc.lastName = 'Jones';
		acc.PersonEmail = 'joySmith@test.in';
		insert acc;
		test.startTest();
		nfchat__Chat_Log__c chatLog = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, subject, Chat_fxTrade_Account_No_Username__c, Chat_Email__c, Chat_Type_of_Account__c, Subtype__c, Type__c, Inquiry_Nature__c  from Case]);
		system.assertEquals(1,caseList.size());
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLog.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Email__c,chatLog.nfchat__Email__c);
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLog.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Type_of_Account__c,chatLog.nfchat_Account_Type__c);
	}

	static testMethod void CreateCaseRecordWithEmailFollowup() {
		
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(); // record type choice
	    Account acc = new Account();
		acc.RecordTypeId = accountRecordTypeId;
		acc.lastName = 'Jones';
		acc.PersonEmail = 'joySmith@test.in';
		insert acc;
		test.startTest();
		nfchat__Chat_Log__c chatLog = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		chatLog.EmailCheck__c = 'true';
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Case>caseList = new List<Case>([select id, subject, Chat_fxTrade_Account_No_Username__c, Chat_Email__c, Chat_Type_of_Account__c, Subtype__c, Type__c, Inquiry_Nature__c  from Case]);
		system.assertEquals(1,caseList.size());
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLog.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Email__c,chatLog.nfchat__Email__c);
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLog.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Type_of_Account__c,chatLog.nfchat_Account_Type__c);
	}
	
	static testMethod void createLeadRecord() {
		test.startTest();
		nfchat__Chat_Log__c chatLog = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        nfchat.AIParameters data = new nfchat.AIParameters();
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        
        NF_GenerateOandaCase leadGenerationInstance = new NF_GenerateOandaCase();
		leadGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		List<Lead>leadList = new List<Lead>([select id from Lead]);
		system.assertEquals(1,leadList.size());
	}
	static testMethod void createCase() {
		Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(); // record type choice
	    Account acc = new Account();
		acc.RecordTypeId = accountRecordTypeId;
		acc.lastName = 'Jones';
		acc.PersonEmail = 'joySmith@test.in';
        insert acc;
        test.startTest();
		nfchat__Chat_Log__c chatLog = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		nfchat.AIParameters data = new nfchat.AIParameters();
        string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		nfchat__Chat_Log__c chatLogUpdated = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		List<Case>caseList = new List<Case>([select id, subject, Chat_fxTrade_Account_No_Username__c, Chat_Email__c, Chat_Type_of_Account__c, Subtype__c, Type__c, Inquiry_Nature__c  from Case]);
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLogUpdated.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Email__c,chatLogUpdated.nfchat__Email__c);
		system.assertEquals(caseList[0].Chat_fxTrade_Account_No_Username__c,chatLogUpdated.nfchat_Username__c);
		system.assertEquals(caseList[0].Chat_Type_of_Account__c,chatLogUpdated.nfchat_Account_Type__c);
	}
    static testMethod void createException() {
		Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(); // record type choice
	    Account acc = new Account();
		acc.RecordTypeId = accountRecordTypeId;
		acc.lastName = 'Jones';
		acc.PersonEmail = 'joySmith@test.in';
        insert acc;
        test.startTest();
		nfchat__Chat_Log__c chatLog = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
        chatLog.nfchat__Email__c = '';
        update chatlog;
		nfchat.AIParameters data = new nfchat.AIParameters();
        string payload = '{"ObjectName": "Case","field": "CaseNumber","prompt": "your case is created successfully with casenumber CASENUMBER ","compareValue": "","AutomatedSearch": false}';
        data.sessionId = '1234474y54734y';
        data.params = payload;
        data.payload = payload;
        NF_GenerateOandaCase caseGenerationInstance = new NF_GenerateOandaCase();
		caseGenerationInstance.ReturnStringResponse(data);
		test.stopTest();
		nfchat__Chat_Log__c chatLogUpdated = [select id, Name, nfchat__First_Name__c, nfchat_Case_Description__c, nfchat_Account_Type__c , nfchat__Last_Name__c, nfchat__Phone__c, nfchat_Username__c, nfchat__Email__c, nfchat__Company__c from nfchat__Chat_Log__c limit 1];
		List<Case>caseList = new List<Case>([select id, subject, Chat_fxTrade_Account_No_Username__c, Chat_Email__c, Chat_Type_of_Account__c, Subtype__c, Type__c, Inquiry_Nature__c  from Case]);
        system.assert(caseList.size() > 0);
	}
    
    
}