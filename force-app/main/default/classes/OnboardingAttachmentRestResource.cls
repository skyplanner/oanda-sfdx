@RestResource(urlMapping='/v1/onboarding/attachments')
global with sharing class OnboardingAttachmentRestResource {

	/*
	*	sample json request body
	    {
			"user_id" : 123,
		    "doc_type" : 1,
		    "file_name" : "abc1.txt",
		    "img_data" : "aGVsbG8gd29ybGQ=", //base64 encoded string
		    "mime_type" : "text/plain"
        }   
	*
	*
	*/
	@HTTPPost
	global static String createonBoardingAttachments(Integer user_id, Integer doc_type, String file_name, String img_data,  String mime_type) {
		
		//Gilbert: add api access control
        if(!CustomSettings.isAPIEnabled(CustomSettings.API_NAME_ONBOARDING_DOC)){          
            return CDAAdapter.serialize(false, 'API ' + CustomSettings.API_NAME_ONBOARDING_DOC + ' is disabled');
    	}

		try{
			//lets create a sample attachent and 
			List<fxAccount__c> fxAccntLst = [SELECT id, Name FROM fxAccount__c WHERE fxTrade_User_Id__c = : user_id AND RecordTypeId = : RecordTypeUtil.getFxAccountLiveId() LIMIT 1];

			if(!fxAccntLst.isEmpty()){
				
				//Get the corrsponding doc code for the doc type #
				List<Document_Codes__c> doccodeLst = [SELECT DocId__c, Doc_Type__c FROM Document_Codes__c WHERE DocId__c =: doc_type LIMIT 1]; 
				//We create a document record and link the attachment to the document
				Document__c docObj = new Document__c(RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxAccntLst[0].Id, fxTrade_User_Id__c = user_id, Name = file_name, Name__c = file_name, Document_Type__c = doccodeLst[0].Doc_Type__c, Is_My_Number__c = false);

				if(docobj.Document_Type__c == 'My Number'){
					docobj.Is_My_Number__c = true;
				}

				insert docObj;

				Attachment attachObj = new Attachment (ParentId = docObj.Id, Name = file_name, ContentType = mime_type, Body = EncodingUtil.base64Decode(img_data));
				insert attachObj;

    			return CDAAdapter.serialize(true, 'API ' + 'Attachment has been created sucessfully and its id is '+attachObj.Id);
			}
			else{
                return CDAAdapter.serialize(false, 'API ' + 'Attachment could not be created, since fxAccount does not exist for user id '+user_id);
			}
		}
		catch(Exception error){
            return CDAAdapter.serialize(false, 'Document creation failed with error '+error.getMessage() + ' and cause is ' +error.getCause());
		}
	}
}