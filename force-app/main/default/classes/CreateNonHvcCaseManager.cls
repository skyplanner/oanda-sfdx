/**
 * @File Name          : CreateNonHvcCaseManager.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 4/5/2024, 2:53:08 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/6/2024, 3:30:03 PM   aniubo     Initial Version
 **/
public inherited sharing class CreateNonHvcCaseManager {
	public  static final Integer MAX_NON_HVC_CASES = 3;
	public  static final Integer MIN_PER_NON_HVC = 80;
	public NonHvcCaseAttemptInfo newNonHvcCase(String currentStatus) {
		NonHvcCaseAttemptInfo info = new NonHvcCaseAttemptInfo();

		Integer nonHvcCasesCount = new AgentExtraInfoManager()
			.getcNonHvcCasesByAgent();
		Integer newNonHvcCasesCount = nonHvcCasesCount + 1;
		NonHVCCasesStatusPairManager manager = new NonHVCCasesStatusPairManager();
		NonHVCCasesStatusPairSettings settings = manager
			.getByStatusAllowing(currentStatus);
		Integer maxNonHvcCases = settings != null
			? settings.maxNonHvcCases
			: MAX_NON_HVC_CASES;
		if (settings != null) {
			info.canBeAssigned = (newNonHvcCasesCount <= maxNonHvcCases);
			if (newNonHvcCasesCount >= maxNonHvcCases) {
				if (String.isNotBlank(settings.statusNotAllow)) {
					info.newStatusId = ServicePresenceStatusHelper.getByDevName(
							settings.statusNotAllow,
							true
						)
						.Id;
				}
			}
		}
		return info;
	}

	public String nonHvcCaseReleased(String currentStatus) {
		String info;
		Integer nonHvcCasesCount = new AgentExtraInfoManager()
			.getcNonHvcCasesByAgent();

		NonHVCCasesStatusPairManager manager = new NonHVCCasesStatusPairManager();
		NonHVCCasesStatusPairSettings settings = manager
			.getByStatusNotAllowing(currentStatus);
		Integer maxNonHvcCases = settings != null
			? settings.maxNonHvcCases
			: MAX_NON_HVC_CASES;
		Integer minPercentageNonHvc = settings != null
			? settings.minPercentageNonHvc
			: MIN_PER_NON_HVC;
		if ( maxNonHvcCases >0 ) {
			Integer occupancyPercentage =
				(nonHvcCasesCount * 100) / maxNonHvcCases;
			if (occupancyPercentage <= minPercentageNonHvc) {
				if (String.isNotBlank(settings.statusAllow)) {
					info = ServicePresenceStatusHelper.getByDevName(
							settings.statusAllow,
							true
						)
						.Id;
				}
			}
		}

		return info;
		//...
	}

	public void declineNonHvcCases(List<String> nonHvcCaseIds) {
		if ((nonHvcCaseIds == null) || nonHvcCaseIds.isEmpty()) {
			return;
		}
		//else...
		Set<ID> nonHvcCaseIdSet = new Set<ID>();
		for (String nonHvcCaseId : nonHvcCaseIds) {
			nonHvcCaseIdSet.add(nonHvcCaseId);
		}
		List<Non_HVC_Case__c> nonHvcCaseList = NonHVCCaseHelper.getById(
			nonHvcCaseIdSet
		);
		if (nonHvcCaseList.isEmpty()) {
			return;
		}
		//else...
		Set<ID> caseIdSet = new Set<ID>();
		for (Non_HVC_Case__c nonHvcCase : nonHvcCaseList) {
			caseIdSet.add(nonHvcCase.Case__c);
		}
		delete nonHvcCaseList;
		//re route cases
		CaseRoutingManager.getInstance().routeNonHvcCasesFromIds(caseIdSet);
	}
}