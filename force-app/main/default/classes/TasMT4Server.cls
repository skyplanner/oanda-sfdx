/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-15-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasMT4Server implements Comparable {
    public String server_id {get; set;}

    public String server_name {get; set;}
    
    public Boolean valid {get; set;}

    public transient String label {
        get {
            return server_id + ' - ' + server_name;
        } 
    }

    public Integer compareTo(Object compareTo) {
        TasMT4Server compareToGroup = (TasMT4Server) compareTo;

        if (server_id > compareToGroup.server_id)
            return 1;
        else if (server_id < compareToGroup.server_id)
            return -1;
        return 0;
    }
}