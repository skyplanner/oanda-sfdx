/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-20-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class BatchfxAccUpdateByCASMonOffSched implements
    Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.RaisesPlatformEvents, BatchReflection {
    public string query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static String CRON_SCHEDULE = '0 0 */4 * * ?';

    public BatchfxAccUpdateByCASMonOffSched() {
        query = 'SELECT Id, Is_CA_Name_Search_Monitored__c, Is_CA_Alias_Search_Monitored__c, ' + 
        '(SELECT Id, Is_Monitored__c FROM Comply_Advantage_Searches__r WHERE Is_Monitored__c = TRUE) ' +
        'FROM fxAccount__c ' +
        'WHERE Is_Closed__c = TRUE AND (Is_CA_Name_Search_Monitored__c = TRUE OR Is_CA_Alias_Search_Monitored__c = TRUE)';
    }

    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
        query = 'SELECT Id, Is_CA_Name_Search_Monitored__c, Is_CA_Alias_Search_Monitored__c, ' + 
        '(SELECT Id, Is_Monitored__c FROM Comply_Advantage_Searches__r WHERE Is_Monitored__c = TRUE) ' +
        'FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public static void executeBatch() {
        Database.executeBatch(new BatchfxAccUpdateByCASMonOffSched(), 100);
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchfxAccUpdateByCASMonOffSched(), 100);
    }

    public void execute(Database.BatchableContext bc, List<fxAccount__c> scope) {
        List<fxAccount__c> updatefxAccount = new List<fxAccount__c>();

        for(fxAccount__c fx : scope) {
            if (fx.Comply_Advantage_Searches__r == null || fx.Comply_Advantage_Searches__r.size() == 0) {
                fx.Is_CA_Name_Search_Monitored__c = false;
                fx.Is_CA_Alias_Search_Monitored__c = false;
                updatefxAccount.add(fx);
            }
        }

        if (!updatefxAccount.isEmpty()) {
            Database.update(updatefxAccount, false);
        }
    }

    public void finish(Database.BatchableContext bc) {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

    public static String schedule(String schedule) {
        if (String.isNotBlank(schedule)) {
            CRON_SCHEDULE = schedule;
        }
        return System.schedule((
            Test.isRunningTest() ? 'Testing Batch BatchfxAccUpdateByCASMonOffSched' : 'Update monitoring flags closed fxAccount'),
            CRON_SCHEDULE,
            new BatchfxAccUpdateByCASMonOffSched()
        );
    }
}