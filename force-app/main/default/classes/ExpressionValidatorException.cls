/**
 * @File Name          : ExpressionValidatorException.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/20/2020, 12:20:21 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/20/2020   acantero     Initial Version
**/
public class ExpressionValidatorException extends Exception {
    
}