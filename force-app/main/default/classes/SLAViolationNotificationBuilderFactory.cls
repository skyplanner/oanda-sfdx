/**
 * @File Name          : SLAViolationNotificationBuilderFactory.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/27/2024, 4:17:38 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/20/2024, 2:01:06 PM   aniubo     Initial Version
 **/
public inherited sharing class SLAViolationNotificationBuilderFactory {
	public static SLAViolationNotificationBuilder createNotificationBuilder(
		SLAConst.SLANotificationObjectType type,
		SLAConst.SLAViolationType violationType
	) {
		SLAViolationNotificationBuilder builder;
		switch on type {
			when CHAT_CASE_NOT, EMAIL_CASE_NOT, PHONE_CASE_NOT {
				builder = new CaseSLAViolationNotificationBuilder(
					violationType
				);
			}
			when LIVE_CHAT_NOT {
				builder = new ChatSLAViolationNotificationBuilder(
					violationType
				);
			}
			when MESSAGING_NOT {
				builder = new MessagingSLAViolationNotificationBuilder(
					violationType
				);
			}
		}
		return builder;
	}
}