/**
 * @File Name          : SPRecTypesManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/26/2023, 2:41:23 AM
**/
public inherited sharing class SPRecTypesManager {

	final Map<String, Map<String, ID>> recTypesIdsByObject;
	final Boolean cacheResults;

	public SPRecTypesManager(Boolean cacheResults) {
		this.cacheResults = cacheResults;
		if (cacheResults == true) {
			this.recTypesIdsByObject = new Map<String, Map<String, ID>>();
		}
	}

	/**
	* @return the list of IDs. 
	* Each ID goes in the order corresponding to its DevName in the input list.
	*/
	public List<ID> getRecordTypeIdsByDevNames(
		DescribeSObjectResult sObjectInfo,
		List<String> recTypeDevNames
	) {
		List<ID> result = new List<ID>();
		if (
			(sObjectInfo == null) ||
			(recTypeDevNames == null) ||
			recTypeDevNames.isEmpty()
		) {
			return result;
		}
		//else...
		if (cacheResults == true) {
			Map<String, ID> recTypeIdsByDevName = 
				getRecTypeIdsByDevName(sObjectInfo);
			for (String recTypeDevName : recTypeDevNames) {
				result.add(recTypeIdsByDevName.get(recTypeDevName));
			}
			return result;
		}
		// else...
		Map<String, Schema.RecordTypeInfo> recTypeInfosByDevName = 
			sObjectInfo.getRecordTypeInfosByDeveloperName();
		for (String recTypeDevName : recTypeDevNames) {
			result.add(
				recTypeInfosByDevName.get(recTypeDevName)?.getRecordTypeId()
			);
		}
		return result;
	}

	public ID getRecordTypeIdByDevName(
		DescribeSObjectResult sObjectInfo,
		String recTypeDevName
	) {
		if (
			(sObjectInfo == null) ||
			String.isBlank(recTypeDevName)
		) {
			return null;
		}
		//else...
		if (cacheResults == true) {
			return getRecTypeIdsByDevName(sObjectInfo).get(recTypeDevName);
		}
		// else...
		return sObjectInfo
			.getRecordTypeInfosByDeveloperName()
			.get(recTypeDevName)?.getRecordTypeId();
	}

	Map<String, ID> getRecTypeIdsByDevName(DescribeSObjectResult sObjectInfo) {
		String objName = sObjectInfo.getName();
		// Map<String, Map<String, ID>> recTypesIdsByObject;
		Map<String, ID> recTypesIdsByDevName = 
			recTypesIdsByObject.get(objName);

		if (recTypesIdsByDevName != null) {
			return recTypesIdsByDevName;
		}
		// else...
		recTypesIdsByDevName = new Map<String, ID>();
		Map<String, Schema.RecordTypeInfo> recTypesByDevName = 
			sObjectInfo.getRecordTypeInfosByDeveloperName();
			
		for (String devName : recTypesByDevName.keySet()) {
			recTypesIdsByDevName.put(
				devName, 
				recTypesByDevName.get(devName).getRecordTypeId()
			);
		}
		recTypesIdsByObject.put(objName, recTypesIdsByDevName);
		return recTypesIdsByDevName;
	}

}