/**
 * @File Name          : UpdateMsgSessionBooleanFieldActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:14:32 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 12:50:58 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class UpdateMsgSessionBooleanFieldActionTest {
	@isTest
	private static void testSetMsgSessionBooleanField() {
		// Test data setup
		UpdateMsgSessionBooleanFieldAction.UpdateInfo updateInfo = new UpdateMsgSessionBooleanFieldAction.UpdateInfo();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'New_Lead__c';
		updateInfo.fieldValue = false;
		Boolean exceptionThrown = false;

		Test.startTest();
		try {
			UpdateMsgSessionBooleanFieldAction.setMsgSessionBooleanField(
				new List<UpdateMsgSessionBooleanFieldAction.UpdateInfo>{
					updateInfo
				}
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Map<String, Object> result = updateInfo.getFieldValueByFieldNames();

		Assert.areEqual(
			updateInfo.fieldValue,
			result.get(updateInfo.fieldName),
			'Field value should be equal' + updateInfo.fieldValue
		);

		Test.stopTest();
	}
}