@isTest
 public class FxAccountAuthCodeRestResourceTest {
     
    public static final string JWT_TOKEN = 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vb2FuZGEuY29tL3Rva2VuSUQiOiJlMjk1MzFmYTA3MWM2MTU4YjA2MTEyOGI0Yzc2N2I4YSIsImh0dHA6Ly9vYW5kYS5jb20vcHJpdmlsZWdlcyI6eyJsaXZlIjp7ImFjY291bnRQcml2aWxlZ2VzIjp7ImNyeXB0byI6eyJhY2NvdW50cyI6W119LCJtdDVPR00iOnsiYWNjb3VudHMiOltdfSwibXQ1Ijp7ImFjY291bnRzIjpbeyJtdDVVc2VySUQiOiI1NTU2MDY3In0seyJtdDVVc2VySUQiOiI1NTgyODg5In0seyJtdDVVc2VySUQiOiI1MjA1ODY4In0seyJtdDVVc2VySUQiOiI1NzExNDQ5In0seyJtdDVVc2VySUQiOiIzOTk5OTMyIn1dfSwidjIwIjp7InNpdGVJRCI6MSwiYWNjb3VudHMiOlt7ImRJRCI6NCwidUlEIjoyNzYzMzQsImFJRHMiOltbMSwxNF0sWzIsMTRdLFszLDE0XV19XX19LCJjYW5BY2Nlc3NQcmVtaXVtIjowLCJjYW5BY2Nlc3NVc2VyRXh0ZXJuYWxJbmZvIjowLCJjYW5BY2Nlc3NVc2VySW5mbyI6MSwiZW1haWxWYWxpZGF0ZWQiOjEsInJlZ2lzdHJhdGlvbkNvbXBsZXRlZCI6MSwiYWdyZWVtZW50c091dGRhdGVkIjowLCJjYW5PbmJvYXJkQ3J5cHRvIjowLCJjYW5PbmJvYXJkTXQ1IjoxLCJ1c2VySWQiOjI3NjMzNCwiZGl2aXNpb25JZCI6NH0sImRlbW8iOnsiYWNjb3VudFByaXZpbGVnZXMiOnsiY3J5cHRvIjp7ImFjY291bnRzIjpbXX0sIm10NU9HTSI6eyJhY2NvdW50cyI6W119LCJtdDUiOnsiYWNjb3VudHMiOlt7Im10NVVzZXJJRCI6IjUzNTA5NTUifSx7Im10NVVzZXJJRCI6IjU1NTIxNDIifV19LCJ2MjAiOnsic2l0ZUlEIjoxMDEsImFjY291bnRzIjpbeyJkSUQiOjQsInVJRCI6NzY4MjczLCJhSURzIjpbWzEsMTRdLFsyLDE0XV19XX19LCJjYW5BY2Nlc3NQcmVtaXVtIjowLCJjYW5BY2Nlc3NVc2VyRXh0ZXJuYWxJbmZvIjowLCJjYW5BY2Nlc3NVc2VySW5mbyI6MSwiZW1haWxWYWxpZGF0ZWQiOjEsInJlZ2lzdHJhdGlvbkNvbXBsZXRlZCI6MSwiYWdyZWVtZW50c091dGRhdGVkIjowLCJjYW5PbmJvYXJkQ3J5cHRvIjowLCJjYW5PbmJvYXJkTXQ1IjoxLCJ1c2VySWQiOjc2ODI3MywiZGl2aXNpb25JZCI6NH19LCJodHRwOi8vb2FuZGEuY29tL3ZlcnNpb24iOiIxLjAiLCJpc3MiOiJodHRwczovL2F1dGguc3RhZ2luZy5vYW5kYS5jb20vIiwic3ViIjoiYXV0aDB8NjBmNzlmZDEtMmNhYi0xMWVmLTk1MWEtNDIwMTBhNzgwMWQzIiwiYXVkIjpbIm9hbmRhIiwiaHR0cHM6Ly9zdGctb2FuZGEudXMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTcxOTgzNDQxMywiZXhwIjoxNzE5ODM4MDEzLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwiYXpwIjoiZWJYYzVCc2ppc1lpQmhWTVZwYjdGU1BnVVFyN0xkdEgifQ.JO5HVLHjy-JNjMkpY24W9-GeaWWQm6UmM2zS8wW2LS8nI8w4YBfpv1i8TLc-mxy25iAAATpQLC2RbIoekuNBUNXQFow03udRxgB6ABYw77oGtoi16YVEFgQ4w3IZA1gFjy7-BeHZMzwylmf2wNO3LAdjJI6yP5lSehDToQwqefNo7OMSCAxyTIgp9IF4kowJ94Rkkron3WIHhGfjDB4R1o5MrSm1BCXD6-MGKBrbhiVtTAgjqCzzP8aYq0i5nE201mDFQfL1WYtbY_cPRZkjLqxAzjy32lq3ofOrNcIOxfnWZP0FOkChNbRz5NhKDbbaWDusqynnij6Dx3SB_pmBdA';
    public static final String SSO_TOKEN = 'Bearer 12345678900987654321-abc34135acde13f13530';
    public static final String JWKS = '{' +
    '"keys": [' +
        '{' +
            '"p": "3I0eayOCJbF_Ks36VK7w-D5JLjVshpZyFBVFK4t78qIygDPMsQnXqE7CzcOh1_TsEj351CNI8IEc5ZSTcaMmBs3GxlglCzFXEL4Sse93tHB3QMwp2XXmrjPNOuAXwSQCj5dxmPDzaqWQQD1iX4a3vnoef2nKskKNVGhVXqoTXoU",' +
            '"kty": "RSA",' +
            '"q": "z_b7b3DW_qPmdQpGdTC4Lp98YypOmK8lUVZ41C1yK9fV0o8LZsp5xuDQnLyYx-arpsgSrmBOBUB910vKDihQP9lZ_kd42Bdgt9DNmtkdYA-Kk3yAXsY5lqKpBmzhh7fkhntgYowGNyw1Dh3tFe32KeZfucaW3ECG91QBnxSIIEk",' +
            '"d": "UeXID8Bq4ChqdPseNz8x0suNL4DDIwb2T9IkHdeHk7xTXdIgkudvwYzU0Cx_O0LTmypLbKFfrAmqyL0DiG6N0qlI9Iz8ZntqT1L4pp18PJcMysLuAT46oTR2hw5K76VG2s4M3FMF-AY4ZP7OwPlej2NtVH3_9MKov3x_LrnzY-uDtZPcXqBdzI1ptHPRPUquyz7Jk1MeJhr45s6AsOmFUSisLUbm5t14rQE4g-HSSVuBIpsVgLIJNqxJ5QPaKm7IVhc0Qpow51nZYx9WRzqQUL028AtiKrOce2IiS3xAibEBukyJgfw1h4iyvCuoPlm6iOr4XbeSQre-NezgI_YJoQ",' +
            '"e": "AQAB",' +
            '"use": "sig",' +
            '"kid": "4456",' +
            '"qi": "Iw7TpYSje5gWgia_blNmM3fxhiZW4-tBtNW0Gp4JqEBybaRDL7UaJFEMEBfypKTsBXdzru4ElhqTieaPA9yhorJ_lMG3z7O5yRrBZfp3jv3N46_zBqcPDzjyk-0-sBf6N6vH1vHpOobu22_ybkXD0dDhn6E9kmj2iY2PBu84BAY",' +
            '"dp": "j6oVAHE229rNHVLjF7qFGQ9pmAIQ2XKyiMZ6Mi0_5gMlpkEXutLQecAkQOiISiYuiIt6a2c4ogFYk2u-x60gnhn4u0a7ipjvP-awQtvcO9tusvTK5e5Y_2ya6RkEIDmWuXZK9vcteHC0kEPsCpG0qwzdRN3ivU-AxFTaFznuOAk",' +
            '"alg": "RS256",' +
            '"dq": "cOqpmtip45kaiHILz5Wka6ws3asbAcwXdxJ8iLgkRanrb2g7yJQnitSGcJP2YJPen7ACdKvVsQRTT5LXItjAuepJ8sbb1njbCvNUr78zM_U8SNhQ2yTGIAQxwktcKVSvwFU9AEUHGu3l4MRe9ok-H4H2d-hcATsIDUKPntCHGQ",' +
            '"n": "syrj0jvkjndEdBi9u2zarm1U_qzMERMt3u62NXDoZj0PHk6N6q_57InGZO88rhux8dBAmG0CkTNtAJrh6AE3NNAHxmtblBW2aWmDGCAWX3tz5vElUe2xvBbMHZvhQKHOa1K6F3_V-1XyTaNqPs-i9lFXzh9MOXubjnT6RsAAzV1W1IuhFsb0z5eRJSrztIoBAA2Ud-mN6dR4tNPUHim1mBH0_6Ensk9UA9SDHPDVtHx-AA9u7yhT_pTa0G8hWfe7JSeBRiyLJkkf3ApY_c-5eZ6cvWIa9VuySh1TtpFRupReWsscLfOjM5xtIwHEE0A7lQ1-FK8vP5PoCkSIhf6T7Q"' +
        '}' +
    ']' +
'}';

    @TestSetup
    static void initData(){
 
        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_One_Id__c = '60f79fd1-2cab-11ef-951a-42010a7801d3';
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        fxAccount.Authentication_Code__c = '123456';
        fxAccount.Authentication_Code_Expires_On__c = Datetime.now().addMinutes(15);
        insert fxAccount;
    }

    @isTest
    static void testDoPostWithValidJwt() {
 
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/client-identification-code';
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Authorization', JWT_TOKEN);

        RestContext.request = request;
        RestContext.response = response;

        Auth.HttpCalloutMockUtil.setHttpMock(new CalloutMock(200, JWKS, 'OK', null));
        
        FxAccountAuthenticationCodeRestResource.auth0Settings  = (Auth0_JWT_Setting__mdt) JSON.deserialize('{"JWKS_Uri__c":"https://auth.staging.oanda.com/", "Required_Issuer__c" : "https://auth.staging.oanda.com/"}', Auth0_JWT_Setting__mdt.class);
        Test.startTest();

        FxAccountAuthenticationCodeRestResource.doGet();

        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.responseBody.toString());

        Assert.areEqual('123456', String.valueOf(responseMap.get('code')), 'Code does not match');
    }
 
    @isTest
    static void testDoPostWithSsoToken() {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/client-identification-code';
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');
        request.addHeader('X-Authorization', SSO_TOKEN);

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        FxAccountAuthenticationCodeRestResource.doGet();

        Test.stopTest();
        
        Assert.areEqual(500, response.statusCode, 'Should return code 500 when no SSO token provided');
    }

    @isTest
    static void testDoPostWithoutToken() {
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri = '/api/v1/client-identification-code';
        request.httpMethod = 'GET';
        request.addHeader('Content-Type', 'application/json');

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        FxAccountAuthenticationCodeRestResource.doGet();

        Test.stopTest();
        
        Assert.areEqual(401, response.statusCode, 'Should return code 401 when no Auth0 token provided');
    }


}