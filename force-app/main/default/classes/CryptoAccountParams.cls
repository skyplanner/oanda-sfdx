/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 07-25-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class CryptoAccountParams {
    public List<Id> fxaccountIds { get; set; }
    public Integer action { get; set; } // 0: Lock, 1: Enable, 2: Close
    public String cryptoAccountAction { get; set; }
    public String calloutResourcePathTas { get; set; }
    public String calloutResourcePathUser { get; set; }
    public String requestBody { get; set; }
    public Map<Id, Note> successNoteMap { get; set; }
    public Map<Id, Note> failNoteMap { get; set; }
    public Boolean addFxAccountDataResult { get; set; }
    public Boolean useTasApi { get; set; }
    public Boolean useUserApi { get; set; }
}