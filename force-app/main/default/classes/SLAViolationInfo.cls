/**
 * @File Name          : SLAViolationInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 3:39:01 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/6/2021, 4:16:05 PM   acantero     Initial Version
**/
public without sharing virtual class SLAViolationInfo {

    public String ownerId {get; set;}
    public String ownerName {get; set;}
    public String caseId {get; set;}
    public String channel {get; set;}
    public String division {get; set;}
    public String divisionCode {get; set;}
    public String inquiryNature {get; set;}
    public String language {get; set;}
    public String liveOrPractice {get; set;}
    public String violationType {get; set;}
    public Boolean isPersistent {get; set;}
    public String tier  {get; set;}
    public String triggerTime  {get; set;}
    public String caseOrigin  {get; set;}
    public SLAConst.SLANotificationObjectType notificationObjectType {get; set;}

}