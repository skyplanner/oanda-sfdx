/**
 * @File Name          : IsLinkedToAccountOrLeadActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 12:44:15 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 9:59:42 AM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class IsLinkedToAccountOrLeadActionTest {
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		List<IsLinkedToAccountOrLeadAction.ActionResult> actionResults;
		// Actual test
		Test.startTest();
		actionResults = IsLinkedToAccountOrLeadAction.isLinkedToAccountOrLead(
			null
		);
		Assert.areEqual(
			true,
			actionResults != null && actionResults.size() > 0,
			'Action Result list should not be null or empty'
		);
		actionResults = null;
		actionResults = IsLinkedToAccountOrLeadAction.isLinkedToAccountOrLead(
			new List<IsLinkedToAccountOrLeadAction.ActionInfo>()
		);

		Assert.areEqual(
			true,
			actionResults != null && actionResults.size() > 0,
			'Action Result list should not be null or empty'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testIsLinkedToAccountOrLeadThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		List<IsLinkedToAccountOrLeadAction.ActionResult> actionResults;
		// Actual test
		Test.startTest();
		try {
			actionResults = IsLinkedToAccountOrLeadAction.isLinkedToAccountOrLead(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testTActionResultConstructor() {
		// Test data setup
		MessagingAccountCheckInfo checkInfo = new MessagingAccountCheckInfo(
			true
		);
		checkInfo.fxAccountId = '001000000000000';

		// Actual test
		Test.startTest();
		IsLinkedToAccountOrLeadAction.ActionResult actionResult = new IsLinkedToAccountOrLeadAction.ActionResult(
			checkInfo
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			checkInfo.fxAccountId,
			actionResult.fxAccountId,
			'Fx Account Id should be equal to 001000000000000'
		);

		Assert.areEqual(
			checkInfo.isLinkedToAccountOrLead,
			actionResult.isLinkedToAccountOrLead,
			'Is Linked To Account Or Lead should be equal to true'
		);
	}

	@isTest
	private static void testIsLinkedToAccountOrLead() {
		// Test data setup
		IsLinkedToAccountOrLeadAction.ActionInfo actionInfo = new IsLinkedToAccountOrLeadAction.ActionInfo();
		actionInfo.userEmail = 'fake@server.com';
		actionInfo.messagingSessionId = null;
		// Actual test
		Test.startTest();

		List<IsLinkedToAccountOrLeadAction.ActionResult> actionResults = IsLinkedToAccountOrLeadAction.isLinkedToAccountOrLead(
			new List<IsLinkedToAccountOrLeadAction.ActionInfo>{ actionInfo }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			actionResults.get(0).isLinkedToAccountOrLead,
			'Is Linked To Account Or Lead should be equal to false'
		);
		Assert.areEqual(
			true,
			actionResults.get(0).fxAccountId == null,
			'Fx Account Id should be equal to null'
		);
	}
}