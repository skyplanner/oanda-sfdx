/**
 * General batch class who call a 'Callable'
 * method to process each batch's chunk.
 * 
 * How use:
 *  GeneralProcessBatch.process(
 *      <query>,
 *      <if you want to retrieve chunk records from db locked for update>
 *      <class how has the callable method to process the records>,
 *      <callable method to process the records>,
 *      <callable method to process when batch finish>,
 *      <cant the records to process in each batch's chunk>);
 *
 * Sample:
 *  GeneralProcessBatch.process(
 *      'SELECT Id, Name, Account_Email__c FROM fxAccount__c',
 *      true,
 *      'fxAccountHashUtil',
 *      'setHashedFields',
 *      cntRcds);
 * 
 */
public with sharing class GeneralProcessBatch
    implements
        Database.Batchable<SObject>,
        Database.AllowsCallouts{
    
    private static final Integer CNT_DEFAULT = 200;
    private static final String Q_WHERE = 'where';

    private String query;
    private Boolean forUpdate;
    private String className;
    private String processMethod;
    private String finishMethod;
    private List<SObject> rcds;

    /**
     * Process records calling
     * some callable method
     */
    public static Id process(
        String query,
        Boolean forUpdate,
        String className,
        String processMethod)
    {
        return
            process(
                query,
                forUpdate,
                className,
                processMethod,
                CNT_DEFAULT);
    }

    /**
     * Process records calling
     * some callable method
     */
    public static Id process(
        String query,
        Boolean forUpdate,
        String className,
        String processMethod,
        Integer cntRecords)
    {
        return
            process(
                query,
                forUpdate,
                className,
                processMethod,
                null,
                cntRecords);
    }

    /**
     * Process records calling
     * some callable method
     */
    public static Id process(
        String query,
        Boolean forUpdate,
        String className,
        String processMethod,
        String finishMethod,
        Integer cntRecords)
    {
        if(String.isBlank(query) ||
            String.isBlank(className) ||
            String.isBlank(processMethod)) 
                return null;
        
        return
            Database.executebatch(
                new GeneralProcessBatch(
                    query,
                    forUpdate,
                    className,
                    processMethod,
                    finishMethod),
                    cntRecords == null ?
                        CNT_DEFAULT :
                        cntRecords);
    }

    /**
     * Constructor
     */
    private GeneralProcessBatch(
        String query,
        Boolean forUpdate,
        String className,
        String processMethod,
        String finishMethodName)
    {
        this.query = query.toLowerCase();
        this.forUpdate = forUpdate;
        this.className = className;
        this.processMethod = processMethod;
        this.finishMethod = finishMethod;
    }

    /**
     * Batch START method
     */
    public Database.QueryLocator start(
        Database.BatchableContext BC)
    {
		return Database.getQueryLocator(query);
	}

    /**
     * Batch EXECUTE method
     */
    public void execute(
        Database.BatchableContext BC,
        List<SObject> rcds)
    {
        // If it is for update, retrieve
        // records locked from db
        this.rcds = forUpdate ?
            retrieveRecords(rcds) : rcds;
  
        // Do the process
        processing();
    }

    /**
     * Batch FINISH method
     */
    public void finish(Database.BatchableContext bc) {
        if(String.isEmpty(finishMethod))
            return;

        call(
            className,
            finishMethod,
            null);
    }

    /**
     * Process records
     */
    private void processing() {
        if(rcds.isEmpty())
            return;
        
        call(
            className,
            processMethod,
            new Map<String, List<SObject>>{
                'records' => rcds
            });
    }

    /**
     * Retrieve chunk records using
     * FOR UPDATE keyword
     */
    private List<SObject> retrieveRecords(List<SObject> chunkRcds) {
        // Get ids from chunk records
        Set<Id> chunkRcdIds =
            new Map<Id, SObject>(chunkRcds).keySet();
        
        // Build chunk query

        String q, xSelect, xWhere; 
        
        if(query.contains(Q_WHERE)) {
            String[] arr = this.query.split(Q_WHERE);
            xSelect = arr[0];
            xWhere = arr[1];

            q = xSelect + ' ' +
                Q_WHERE + ' (' + xWhere + ') AND ' +
                ' Id IN ' + SoqlUtil.getSoqlList(chunkRcdIds);

        } else {
            q = this.query + ' ' + Q_WHERE +
            ' Id IN ' + SoqlUtil.getSoqlList(chunkRcdIds);
        }

        q += ' FOR UPDATE';

        System.debug(
            'GeneralProcessBatch-chunk-query: ' + q);

        // Retrieve chunk records from database
        return Database.query(q);
    }

    /**
     * Call methods in the 
     * proccessing class
     */
    private void call(
        String className,
        String method,
        Map<String, Object> args)
    {
        // Create new class instance
        Callable cls = 
            (Callable)Type.forName(className).newInstance();
        
        // Do the process on chunk records
        cls.call(
            method, 
            args); 
    }

}