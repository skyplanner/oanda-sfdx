/**
 * @File Name          : CaseSRoutingInfoHelper.cls
 * @Description        : 
 * Fills the language fields of a "ServiceRoutingInfo" record from the Case
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/31/2024, 1:14:15 AM
**/
public inherited sharing class CaseSRoutingInfoHelper {

	public static String setLanguagesFields(
		ServiceRoutingInfo routingInfo,
		Case caseObj
	) {
		if (
			(
				caseObj.Internally_Routed__c == true || 
				caseObj.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_EMAIL
			) &&
			String.isNotBlank(caseObj.Language_Corrected__c)
		) {
			routingInfo.language = 
				ServiceLanguagesManager.getLangCodeByLangCorrected(
					caseObj.Language_Corrected__c
				);
			return routingInfo.language;
		} 
		//else...
		if (caseObj.Account != null) {
			routingInfo.language = caseObj.Account.Language_Preference__pc;     
			//...   
		} else if (caseObj.Lead__r != null) {
			routingInfo.language = caseObj.Lead__r.Language_Preference__c;
		}
		//EXCEPTION FOR Email - Frontdesk AND Email - API 
		if (
			(
				(caseObj.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK) ||
				(caseObj.Origin == OmnichanelConst.CASE_ORIGIN_EMAIL_API)
			) &&
			String.isNotBlank(caseObj.Language) 
		) {
			routingInfo.alternativeLanguage = routingInfo.language;
			routingInfo.language = caseObj.Language;
		}
		return routingInfo.language;
	}

}