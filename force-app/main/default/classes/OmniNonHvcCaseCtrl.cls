/**
 * @File Name          : OmniNonHvcCaseCtrl.cls
 * @Description        :
 * @Author             : acantero
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/6/2024, 3:46:49 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/23/2021, 1:32:14 PM   acantero     Initial Version
 **/
public without sharing class OmniNonHvcCaseCtrl {
	@AuraEnabled
	public static NonHvcCaseAttemptInfo onNewNonHvcCase(String currentStatus) {
		try {
			ExceptionTestUtil.execute();
			CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();

			return manager.newNonHvcCase(currentStatus);
			//...
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static string onNonHvcCaseReleased(String currentStatus) {
		try {
			ExceptionTestUtil.execute();
			CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();
			return manager.nonHvcCaseReleased(currentStatus);
			//...
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static void declineNonHvcCases(List<String> nonHvcCaseIdList) {
		try {
			ExceptionTestUtil.execute();
			CreateNonHvcCaseManager manager = new CreateNonHvcCaseManager();
			manager.declineNonHvcCases(nonHvcCaseIdList);
			//...
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}