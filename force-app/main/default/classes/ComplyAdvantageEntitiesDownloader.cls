public inherited sharing class ComplyAdvantageEntitiesDownloader  extends ComplyAdvantageCallout implements Database.AllowsCallouts
{
	protected set<string> internalHitAvailableDivisions = new set<string>{
		Constants.DIVISIONS_OANDA_ASIA_PACIFIC, 
        Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD,
        Constants.DIVISIONS_OANDA_AUSTRALIA,
        Constants.DIVISIONS_OANDA_GLOBAL_MARKETS
	};
    protected Comply_Advantage_Search__c caSearch;
    protected Comply_Advantage_Search__c[] caSearches;
   
    private String ENTITY_PROVIDER_INTERNAL = 'internal';
	private String ENTITY_PROVIDER_COMPLY = 'comply';

    public ComplyAdvantageEntitiesDownloader(Comply_Advantage_Search__c caSearch) 
    {
        this.caSearch = caSearch;
        String divisionName = String.isNotBlank(caSearch.Custom_Division_Name__c) ? 
                                caSearch.Custom_Division_Name__c :
                                    (String.isNotBlank(caSearch.Division_Name__c) ? 
                                        caSearch.Division_Name__c : '');

        this.apiKey = ComplyAdvantageUtil.getAPiKey(divisionName, 
                                                    caSearch.Search_Parameter_Mailing_Country__c);
    }
  
    public EntityDownloadResult getEntities() 
	{
		List<Comply_Advantage_Search_Entity__c> entities = new List<Comply_Advantage_Search_Entity__c>{};
		try 
		{
			entities.addAll(getComplyAdvantageEntities());
            entities.addAll(getInternalMatchEntities());

            EntityDownloadResult entityDownloadResult = new EntityDownloadResult();
            entityDownloadResult.ComplyAdvantageSearchRecordId = this.caSearch.Id;
            entityDownloadResult.entities = entities;
            entityDownloadResult.isSuccessful = true;
            return entityDownloadResult;
		}
		catch (Exception ex) 
		{
            EntityDownloadResult entityDownloadResult = new EntityDownloadResult();
            entityDownloadResult.ComplyAdvantageSearchRecordId = this.caSearch.Id;
            entityDownloadResult.entities = entities;
            entityDownloadResult.isSuccessful = false;
            return entityDownloadResult;
		}
	}
    private List<Comply_Advantage_Search_Entity__c> getComplyAdvantageEntities() 
	{
		ComplyAdvantageEntitiesResult result = getEntitiesCallout(ENTITY_PROVIDER_COMPLY);

		List<Comply_Advantage_Search_Entity__c> caEntities = result.getEntities(caSearch.Id, caSearch.Search_Id__c, false);

		return caEntities;

		// if(caEntities.size() >= 100)
		// {
		//     getComplyAdvantageEntities(pageNumber + 1);
		// }
	}

    private List<Comply_Advantage_Search_Entity__c> getInternalMatchEntities() 
	{
		List<Comply_Advantage_Search_Entity__c> internalEntities = new List<Comply_Advantage_Search_Entity__c>{};

        if(internalHitAvailableDivisions.contains(caSearch.Custom_Division_Name__c))
        {
			ComplyAdvantageEntitiesResult result = getEntitiesCallout(ENTITY_PROVIDER_INTERNAL);

            internalEntities = result.getEntities(caSearch.Id, caSearch.Search_Id__c, true);
            
			return internalEntities;
        }
		return internalEntities;
    }

	private String getEntityProviderEndpoint(string entityProvider) 
	{
		return String.format
		(
			SPGeneralSettings.getInstance().getValue('CA_Endpoint_Entity_Provider'),
			new List<String> { caSearch.Search_Reference__c, entityProvider }
		);
	}
	private Map<String, String> getEntityProviderParams(integer pageNo) 
	{
		Map<String, String> result = getBaseParams();
		result.put('page', String.valueOf(pageNo));
		result.put('per_page', String.valueOf(100));
		return result;
	}

	//upload
	public void upload(List<String> entityIds, ComplyAdvantageKeyInformation template) 
	{
		try 
		{
			// we send the new values into CA
			uploadEntities(entityIds, template);

			// we also need to update salesforce 
			updateEntities(entityIds, template);
		} 
		catch (Exception ex) 
		{
			throw fail(ex);
		}
	}
	/**
	 * Fetch the current page
	 * @param entityIds
	 * @param template
	 */
	private void uploadEntities(List<String> entityIds, ComplyAdvantageKeyInformation template) 
	{
		Map<String, Object> obj;
		
		obj = new Map<String, Object> 
		{
			'entities' => entityIds
		};

		if (String.isNotBlank(template.match_status))
			obj.put('match_status', template.match_status);
		
		if (String.isNotBlank(template.risk_level))
			obj.put('risk_level', template.risk_level);
		
		if (template.is_whitelisted != null)
			obj.put('is_whitelisted', template.is_whitelisted);

		Logger.debug(JSON.serialize(obj));
				
		patch( getEntityUpdateEndpoint(), null, getBaseParams(), JSON.serialize(obj));

		// we proceed on a valida response
		if (!isResponseCodeSuccess())
			throw getCalloutException();
	}

	/**
	 * @param entityIds
	 * @param template
	 */
	private void updateEntities(List<String> entityIds,	ComplyAdvantageKeyInformation template) 
	{
		Id searchId = this.caSearch.Id;

		Comply_Advantage_Search_Entity__c[]  entities =  [SELECT Id
														  FROM Comply_Advantage_Search_Entity__c
														  WHERE Comply_Advantage_Search__c = :searchId AND 
																Entity_Id__c IN :entityIds];

		for (Comply_Advantage_Search_Entity__c entity :entities) 
		{
			if (String.isNotBlank(template.match_status))
				entity.Match_Status__c = template.match_status;
			
			if (String.isNotBlank(template.risk_level))
				entity.Risk_Level__c = template.risk_level;

			if (template.is_whitelisted != null)
				entity.Is_Whitelisted__c = template.is_whitelisted;
		}
		update entities;
	}
	private String getEntityUpdateEndpoint() 
	{
		return String.format(
			SPGeneralSettings.getInstance().getValue('CA_Endpoint_Entity_Update'),
			new List<String> {caSearch.Search_Reference__c }
		);
	}

	public ComplyAdvantageSearchHit getEntity(String entityId) 
	{
		ComplyAdvantageSearchHit entity;

		try 
		{
			ComplyAdvantageEntitiesResult result = getEntitiesCallout(ENTITY_PROVIDER_COMPLY);
			entity = filterEntitiesById(entityId, result.content);

			if(entity == null)
			{
				result = getEntitiesCallout(ENTITY_PROVIDER_INTERNAL);
				entity = filterEntitiesById(entityId, result.content);
			}
			
			return entity;
		} 
		catch (Exception ex) 
		{
			throw fail(ex);
		}
	}

	private ComplyAdvantageSearchHit filterEntitiesById(String entityId, ComplyAdvantageSearchHit[] hits)
	{
		if(hits != null && !hits.isEmpty())
		{
			for (ComplyAdvantageSearchHit hit : hits)
			{
				if (hit.id == entityId)
				{
					return hit;
				}
			}
		}
		return null;
	}

	private ComplyAdvantageEntitiesResult getEntitiesCallout(string entityProvider)
	{
		// if CA demo is enabled, check if mocked API response from static resource should be used
		if(ComplyAdvantageHelper.isDemoCAEnabled())
		{	
			String demoResponseBody = ComplyAdvantageHelper.getDemoRespBody(caSearch.fxAccount__r.Email_Formula__c, false);
			if(String.isNotBlank(demoResponseBody)) 
			{
				return ComplyAdvantageEntitiesResult.parse(demoResponseBody);
			}
		}

		// Make the actual callout if not using demo response
		get(getEntityProviderEndpoint(entityProvider), null, getEntityProviderParams(1));

		if (isResponseCodeSuccess()) 
		{
			return ComplyAdvantageEntitiesResult.parse(resp.getBody());
		} 
		else
		{
			throw getCalloutException();
		}
	}
	
    public class EntityDownloadResult
    {
        public Id ComplyAdvantageSearchRecordId;
        public Boolean isSuccessful;
        public List<Comply_Advantage_Search_Entity__c> entities;
    }
}