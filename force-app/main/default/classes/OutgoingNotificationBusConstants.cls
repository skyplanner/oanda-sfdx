public with sharing class OutgoingNotificationBusConstants {
    public static final Map<String, String> userInfoFields = new Map<String, String> {
            'Birthdate__c' => 'birthdate',
            'Government_ID__c' => 'ssn',
            'Employer_Name__c' => 'employer_name',
            'Is_2FA_Required__c' => 'mfa_required',
            'Industry_of_Employment__c' => 'employer_bustype',
//            'Title__c' => 'title',
            'Citizenship_Nationality__c' => 'citizenship',
            'First_Name__c' => 'given_name',
            'Middle_Name__c' => 'middle_name',
            'Last_Name__c' => 'family_name',
            'Suffix__c' => 'name_suffix',
            'Language_Preference__c' => 'language_id',
            'FXDB_Name__c' => 'name',
//            'Business_Name__c' => 'business_name',
            'Email__c' => 'email'
    };

    public static final Map<String, String> auxUserInfoFields = new Map<String, String> {
            'Registration_Source__c' => 'reg_source',
            'Employment_Status__c' => 'employment_status',
            'Employment_Job_Title__c' => 'employment_position',
            'Annual_Income__c' => 'household_income',
            'Net_Worth_Value__c' => 'net_worth',
            'Risk_Tolerance_Amount__c' => 'desired_risk_tolerance',
            'IIROC_Member__c' => 'iiroc_member',
            'Income_Source_Details__c' => 'income_source',
            'Risk_Tolerance_CAD__c' => 'risk_tolerance_amount',
            'Has_Opted_Out_Of_Email__c' => 'marketing_email_opt_in',
            'Introducing_Broker_Number__c' => 'ibroker_id',
            'Net_Worth_Range_Start__c' => 'net_worth_range_start',
            'Net_Worth_Range_End__c' => 'net_worth_range_end',
            'Self_Employed_Details__c' => 'products_services_details',
            'PEFP__c' => 'pefp',
            'PEFP_Details__c' => 'pefp_details',
            'Intermediary__c' => 'intermediary',
            'Relationship_to_OANDA__c' => 'oanda_relationship_type',
            'Liquid_Net_Worth_Value__c' => 'liquid_net_worth',
            'Liquid_Net_Worth_Start__c' => 'liquid_net_worth_range_start',
            'Liquid_Net_Worth_End__c' => 'liquid_net_worth_range_end',
            'Savings_and_Investments__c' => 'savings_and_investments'
    };

    public static final Map<String, String> contactInfoUpdate = new Map<String, String> {
            'Street__c'       => 'address1',
            'City__c'         => 'city',
            'State__c'        => 'region',
            'Country_Name__c' => 'country',
            'Postal_Code__c'  => 'postcode'
    };

    public static final Map<String, String> phoneInfoUpdate = new Map<String, String> {
            'Phone__c' => 'phone'
    };

    public static final Map<String, Integer> fxAccountLanguageMap = new Map<String, Integer> {
            'ENGLISH' => 1,
            'GERMAN' => 2,
            'CHINESE SIMPLIFIED' => 3,
            'CHINESE TRADITIONAL' => 4,
            'SPANISH' => 5,
            'FRENCH' => 6,
            'ITALIAN' => 7,
            'JAPANESE' => 8,
            'KOREAN' => 9,
            'PORTUGUESE' => 10,
            'RUSSIAN' => 11,
            'VIETNAMESE' => 12,
            'THAI' => 13,
            'BAHASA MALAY' => 14,
            'BAHASA INDONESIA' => 15
    };
    public static final Map<String, String> fxStatusMap = new Map<String, String> {
            'EMPLOYED' => '0',
            'SELF EMPLOYED' => '1',
            'RETIRED' => '2',
            'UNEMPLOYED' => '3',
            'STUDENT' => '5'
    };
}