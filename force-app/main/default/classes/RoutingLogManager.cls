/**
 * @File Name          : RoutingLogManager.cls
 * @Description        : Create routing log records
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2024, 12:40:56 AM
**/
public inherited sharing class RoutingLogManager {

	public static final String CASE_CHANNEL = 'Case';
	public static final String LIVE_AGENT_CHANNEL = 'Live Agent';
	public static final String MESSAGING_CHANNEL = 'Messaging';

	final ServiceRoutingInfo sRoutingInfo;

	public RoutingLogManager(BaseServiceRoutingInfo routingInfo) {
		SPConditionsUtil.checkRequiredObjParam(
			'routingInfo', // paramName
			routingInfo // paramValue
		);
		this.sRoutingInfo = (ServiceRoutingInfo) routingInfo;
	}

	public SObject getLogForCase() {
		Routing_Log__c result = getNewRoutingLog(CASE_CHANNEL);
		result.Case__c = sRoutingInfo.workItem.Id;
		return result;
	}

	public SObject getLogForChatTranscript() {
		Routing_Log__c result = getNewRoutingLog(LIVE_AGENT_CHANNEL);
		result.Chat_Transcript__c = sRoutingInfo.workItem.Id;
		return result;
	}

	public SObject getLogForMsgSession() {
		Routing_Log__c result = getNewRoutingLog(MESSAGING_CHANNEL);
		result.Messaging_Session__c = sRoutingInfo.workItem.Id;
		return result;
	}

	Routing_Log__c getNewRoutingLog(String serviceChannel) {
		Routing_Log__c result = new Routing_Log__c(
			Priority__c = sRoutingInfo.routingObj.RoutingPriority,
			Tier__c = sRoutingInfo.tier,
			Service_Channel__c = serviceChannel
		);
		return result;
	}

}