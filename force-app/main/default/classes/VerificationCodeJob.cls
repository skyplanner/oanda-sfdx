/**
 * @File Name          : VerificationCodeJob.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/12/2019, 2:42:51 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   dmorales     Initial Version
**/
public virtual class VerificationCodeJob  implements Queueable {
    
    private Integer count;
   
    public VerificationCodeJob(Integer count) {
           this.count = count;         
    }

    public void execute(QueueableContext context) {
        
            List<Verification_Code__b > codes = [SELECT Order__c, 
                                            Code__c FROM Verification_Code__b  
                                            LIMIT :count];
            List<Decimal> uniqueIndex = new List<Decimal>();                                

            List<Verification_Code_Setting__c> listS = new List<Verification_Code_Setting__c> ();
            for(Verification_Code__b boCodes : codes){
                listS.add(new Verification_Code_Setting__c(Name = boCodes.Code__c, Order__c = boCodes.Order__c));
                uniqueIndex.add(boCodes.Order__c);
            }
            
            Database.insert(listS);            
            VerificationCodeUtility.deleteVerificationCodeFromBigObject(uniqueIndex);     
       
    }
}