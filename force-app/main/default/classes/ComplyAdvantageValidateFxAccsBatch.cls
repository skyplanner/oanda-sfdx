public class ComplyAdvantageValidateFxAccsBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {

    private Set<Id> fxAccIdsToFireCAS;
    private Map<Id, fxAccount__c> oldFxMap;

    // Constructor to pass the Set of fxAccIds
    private ComplyAdvantageValidateFxAccsBatch(Set<Id> fxAccIdsToFireCAS, Map<Id,fxAccount__c> oldFxAccs) {
        this.fxAccIdsToFireCAS = fxAccIdsToFireCAS;
        this.oldFxMap = oldFxAccs;
    }

    public Database.QueryLocator start(Database.BatchableContext context) {
        // Query the records based on the provided fxAccIds
        String query = 'SELECT Id, Name, Citizenship_Nationality__c, Birthdate__c, Is_Closed__c, Account_Locked__c FROM FxAccount__c WHERE Id IN :fxAccIdsToFireCAS';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<FxAccount__c> scope) {
        if(scope.size() == 1 ) {
            try {
                fxAccount__c oldFxAcc = oldFxMap.containsKey(scope[0].Id) ? oldFxMap.get(scope[0].Id) : null;
                ComplyAdvantageHelper.validate(oldFxAcc, scope[0]);
            } catch (Exception ex) {
                //log issues immediately 
                Logger.error(ex.getMessage(), Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ' Err cause: ' + ex.getCause() + ' fxAccountsIds to check for correct CAS: '  + scope );
            }
        }
    }

    public void finish(Database.BatchableContext context) {}

    public static void executeBatch(Set<Id> fxAccIds, Map<Id,fxAccount__c> oldFxAccs) {
        Database.executeBatch(new ComplyAdvantageValidateFxAccsBatch(fxAccIds, oldFxAccs), 1);
    }
}