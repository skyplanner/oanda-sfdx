/**
 * @File Name          : MsgSessionSRoutingInfoManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/15/2024, 12:31:09 AM
**/
@IsTest
private without sharing class MsgSessionSRoutingInfoManagerTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	// A PendingServiceRouting is registered with WorkItemId = null given 
	// the difficulty in creating MessagingSession test records
	@IsTest
	static void getRoutingInfoList1() {
		Test.startTest();
		MsgSessionSRoutingInfoManager instance = 
			new MsgSessionSRoutingInfoManager();
		instance.registerPendingServiceRouting(
			new PendingServiceRouting(
				WorkItemId = null
			)
		);
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		Assert.isTrue(result.isEmpty(), 'Invalid result');
	}

	@IsTest
	static void getRoutingInfo1() {
		Test.startTest();
		MsgSessionSRoutingInfoManager instance = 
			new MsgSessionSRoutingInfoManager();
		BaseServiceRoutingInfo result = instance.getRoutingInfo(
			new PendingServiceRouting(), // routingObj
			new MessagingSession() // workItem
		);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	// test 1 : workItems is empty
	// test 2 : workItems is not empty
	@IsTest
	static void processWorkItems() {
		Test.startTest();
		MsgSessionSRoutingInfoManager instance = 
			new MsgSessionSRoutingInfoManager();
		// test 1 
		instance.processWorkItems(new List<MessagingSession>());
		// test 2
		instance.processWorkItems(new List<MessagingSession> {
			new MessagingSession()
		});
		Test.stopTest();

		Assert.isNotNull(instance, 'Invalid result');
	}

	/**
	 * test 1 
	 * msgSession.CaseId <> null
	 * 
	 * test 2
	 * msgSession.CaseId = null
	 */
	@IsTest
	static void getRecordsToUpdate() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);
		MsgSessionSRoutingInfoManager instance = 
			new MsgSessionSRoutingInfoManager();

		Test.startTest();
		// test 1 => result.size = 2 (MessagingSession and Case records)
		ServiceRoutingInfo routingInfo1 = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new MessagingSession(
					CaseId = case1Id
				) // workItem
			);
		routingInfo1.initTier(OmnichannelCustomerConst.TIER_4);
		List<SObject> result1 = instance.getRecordsToUpdate(routingInfo1);
		// test 2 => result.size = 1 (MessagingSession record)
		ServiceRoutingInfo routingInfo2 = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new MessagingSession() // workItem
			);
		routingInfo2.updateTier(OmnichannelCustomerConst.TIER_3);
		List<SObject> result2 = instance.getRecordsToUpdate(routingInfo2);
		Test.stopTest();

		Assert.areEqual(2, result1.size(), 'Invalid result');
		Assert.areEqual(1, result2.size(), 'Invalid result');
	}

	@IsTest
	static void getRoutingLog() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.MESSAGING, // infoSource
			new MessagingSession() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		MsgSessionSRoutingInfoManager instance = 
			new MsgSessionSRoutingInfoManager();
		SObject result = instance.getRoutingLog(routingInfo);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}

	/**
	 * Test 1
	 * sRoutingInfo.division = null, chatRegion = OC
	 * 
	 * Test 2
	 * sRoutingInfo.division = OCAN, chatRegion = OC
	 */
	@IsTest
	static void setDivision() {
		Test.startTest();

		// Test 1 -> division = OC
		MsgSessionSRoutingInfoManager instance1 = 
			new MsgSessionSRoutingInfoManager();
		ServiceRoutingInfo sRoutingInfo1 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.MESSAGING, // infoSource
			new MessagingSession() // workItem
		);
		sRoutingInfo1.division = null;
		String chatRegion1 = OmnichanelConst.OANDA_CORPORATION_AB;
		instance1.setDivision(
			sRoutingInfo1, // sRoutingInfo
			chatRegion1 // chatRegion
		);

		// Test 2 -> division = OCAN
		MsgSessionSRoutingInfoManager instance2 = 
			new MsgSessionSRoutingInfoManager();
		ServiceRoutingInfo sRoutingInfo2 = new ServiceRoutingInfo(
			new PendingServiceRouting(), // psrObj
			ServiceRoutingInfo.Source.MESSAGING, // infoSource
			new MessagingSession() // workItem
		);
		sRoutingInfo2.division = OmnichanelConst.OANDA_CANADA_AB;
		String chatRegion2 = OmnichanelConst.OANDA_CORPORATION_AB;
		instance2.setDivision(
			sRoutingInfo2, // sRoutingInfo
			chatRegion2 // chatRegion
		);

		Test.stopTest();
		
		// Test 1
		Assert.areEqual(
			OmnichanelConst.OANDA_CORPORATION_AB, 
			sRoutingInfo1.division, 
			'Invalid value'
		);
		// Test 2
		Assert.areEqual(
			OmnichanelConst.OANDA_CANADA_AB, 
			sRoutingInfo2.division, 
			'Invalid value'
		);
	}
	
}