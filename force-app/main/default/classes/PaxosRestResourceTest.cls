/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 10-24-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@IsTest
private class PaxosRestResourceTest {
    @isTest
	static void testUpdatePaxosFields500() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);

        Paxos__c p = new Paxos__c(
            Id = fxAcc.Paxos_Identity__c
        );

        p.One_Id_External_Id__c = 'qwertyuiop';
        update p;

        p.Admin_Disabled__c = true;
        p.Paxos_Status__c = PaxosRestResource.DENIED;
        p.Display_Name__c = 'Testing name';

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/Paxos__c/qwertyuiop';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{' + JSON.serialize(p) + '}');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        PaxosRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(500, RestContext.response.statusCode);
	}

    @isTest
    static void testUpdatePaxosFields404() {
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/Paxos__c/5412558';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Display_Name__c": "Testing name"}');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        PaxosRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(404, RestContext.response.statusCode);
    }

    @isTest
    static void testUpdatePaxosFields200() {
        Paxos__c p = new Paxos__c();
        p.One_Id_External_Id__c = 'qwertyuiop';
        insert p;

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/Paxos__c/qwertyuiop';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Display_Name__c": "Testing name"}');
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        PaxosRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(200, RestContext.response.statusCode);
    }

    @isTest
	static void testUpdatePaxosFields200WithAlert() {
        Account acc = TestDataFactory.getPersonAccount(true);
        fxAccount__c fxAcc = TestDataFactory.createFXTradeCryptoAccountWithOneId(
            acc, true, true);

        Paxos__c p = new Paxos__c(
            Id = fxAcc.Paxos_Identity__c
        );

        p.One_Id_External_Id__c = 'qwertyuiop';
        update p;

        p.Admin_Disabled__c = true;
        p.Paxos_Status__c = PaxosRestResource.DENIED;
        p.Display_Name__c = 'Testing name';

        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/api/v1/Paxos__c/qwertyuiop';
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(JSON.serialize(p));
        
		RestResponse res = new RestResponse();

        RestContext.request = request;
		RestContext.response = res;
        
		Test.startTest();
        
        PaxosRestResource.doPatch();
        
        Test.stopTest();

		System.assertEquals(200, RestContext.response.statusCode);
	}
}