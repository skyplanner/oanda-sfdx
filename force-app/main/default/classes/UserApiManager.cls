/**
 * User API manager class
 */
public class UserApiManager {    
    UserApiAppSettings stts;
    UserCallout userCallout;
    AuditTrailManager auditTrailMngr;

    public static String BIZ_OPS_TECH_SUPPORT_L2 = 'L2 Technical Support';

    public UserApiManager() {
        stts = UserApiAppSettings.getInstance();
        userCallout = new UserCallout();
        auditTrailMngr = new AuditTrailManager();
    }

    /**
     * Load all required data
     */
    public Data loadData() {

        Id profileId = UserInfo.getProfileId();
        String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

        Data r = new Data();
        r.extFields = stts.extfields;
        r.sfFields = stts.sfFields;
        r.isBizOpsL2Profile = (profileName == BIZ_OPS_TECH_SUPPORT_L2);
        return r;
    }    

    public List<UserApiAuditLog> getAuditLogs(
        EnvironmentIdentifier id
    ) {
        UserApiAuditLogsGetResponse result = 
            userCallout.getAuditLogs(id);

        return result == null 
            ? new List<UserApiAuditLog>() : result.logs;
    }

    public UserApiUserGetResponse getUser(EnvironmentIdentifier id) {
        return userCallout.getUser(id);
    }

    public String getStatus(EnvironmentIdentifier id) {
        return getStatus(userCallout.getUser(id)?.user_status);
    }

    public String getStatus(UserApiUserStatus userStatus) {
        if (userStatus == null)
            return '';

        if (userStatus.accountClosed)
            return 'Closed';

        if (userStatus.accountLocked)
            return 'Locked';

        if (userStatus.tooManyFailedLogins)
            return 'Too Many Failed Logins';

        if (userStatus.passwordExpired)
            return 'Password Expired';

        if (!userStatus.registrationCompleted)
            return 'Registration Incomplete';

        if (!userStatus.emailValidated)
            return 'Email Not Validated';

        return 'Active';
    }

    public StatusesWrapper getStatusList(EnvironmentIdentifier id) {
        return getStatusList(userCallout.getUser(id));
    }

    public StatusesWrapper getStatusList(UserApiUserGetResponse user) {
        StatusesWrapper result = new StatusesWrapper();

        if (user?.user_authorization != null) {
            result.authorizationStatuses.add(new UserStatusWrapper(
                'Can Authorize Tpa', user.user_authorization.canAuthorizeTpa));

            result.authorizationStatuses.add(new UserStatusWrapper(
                'Can Deposit', user.user_authorization.canDeposit));

            result.authorizationStatuses.add(new UserStatusWrapper(
                'Can Trade', user.user_authorization.canTrade));
        }

        if (user?.user_status != null) {
            result.userStatuses.add(new UserStatusWrapper(
                'Account Locked', user.user_status.accountLocked));

            result.userStatuses.add(new UserStatusWrapper(
                'Agreements Outdated', user.user_status.agreementsOutdated));

            result.userStatuses.add(new UserStatusWrapper(
                'Closed', user.user_status.accountClosed));

            result.userStatuses.add(new UserStatusWrapper(
                'Deposits Locked', user.user_status.depositsLocked));

            result.userStatuses.add(new UserStatusWrapper(
                'Documents Approved', user.user_status.documentsApproved));

            result.userStatuses.add(new UserStatusWrapper(
                'Email Validated', user.user_status.emailValidated));

            result.userStatuses.add(new UserStatusWrapper(
                'Interest Exempt', !user.user_status.allInterestEnabled));

            result.userStatuses.add(new UserStatusWrapper(
                'Leverage Locked', user.user_status.leverageLocked));

            result.userStatuses.add(new UserStatusWrapper(
                'Password Expired', user.user_status.passwordExpired));

            result.userStatuses.add(new UserStatusWrapper(
                'PIU Outdated', user.user_status.piuOutdated));

            result.userStatuses.add(new UserStatusWrapper(
                'Registration Completed',
                user.user_status.registrationCompleted));

            result.userStatuses.add(new UserStatusWrapper(
                'Snail Mail Validated', user.user_status.snailMailValidated));

            result.userStatuses.add(new UserStatusWrapper(
                'Too Many Failed Logins',
                user.user_status.tooManyFailedLogins));

            result.userStatuses.add(new UserStatusWrapper(
                'Tpa Authorization Locked',
                user.user_status.tpaAuthorizationLocked));

            result.userStatuses.add(new UserStatusWrapper(
                'Trading Locked', user.user_status.tradingLocked));

            result.userStatuses.add(new UserStatusWrapper(
                'Withdrawals Locked', user.user_status.withdrawalsLocked));
        }

        return result;
    }

    public void save(
        Map<String, Object> record, Map<String, Object> oldRecord
    ) {
        EnvironmentIdentifier id = UserApiActionBase.getfxTradeUserId(record);
        UserApiUserPatchRequest user = UserApiUserPatchRequest.fromMap(record);

        if (user == null)
            return;

        userCallout.updateUser(id, user);
        auditTrailMngr.saveAuditTrail(id.id, record, oldRecord);
    }

    public class Data {
        @AuraEnabled
        public List<UserApiExtField> extFields;

        @AuraEnabled
        public List<UserApiSfField> sfFields;

        @AuraEnabled
        public Boolean isBizOpsL2Profile;
    }
    
    public class StatusesWrapper {
        @AuraEnabled
        public List<UserStatusWrapper> authorizationStatuses { get; set; }

        @AuraEnabled
        public List<UserStatusWrapper> userStatuses { get; set; }

        public StatusesWrapper() {
            userStatuses = new List<UserStatusWrapper>();
            authorizationStatuses = new List<UserStatusWrapper>();
        }
    }

    public class UserStatusWrapper {
        @AuraEnabled
        public String name;

        @AuraEnabled
        public Boolean status;

        public UserStatusWrapper(String name, Boolean status) {
            this.name = name;
            this.status = status;
        }
    }
}