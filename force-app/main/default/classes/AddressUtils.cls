/**
 * Address utils class
 */
public class AddressUtils {
    
    /**
     * Know if address was updated to a PO Box address 
     */
	public static Boolean addressUpdatedToPoBox(
        SObject obj,
        SObject oldObj)
    {
		String newVal = (String)obj.get('Street__c');
		String oldVal = (String)oldObj.get('Street__c');
        
		return 
			newVal != oldVal && newVal != null && 
            (    
                newVal.containsIgnoreCase('PO BOX') ||
                newVal.containsIgnoreCase('P.O. BOX') ||
                newVal.containsIgnoreCase('P.O BOX') ||
                newVal.containsIgnoreCase('P.O BOX.') ||
                newVal.containsIgnoreCase('P.O. BOX.')
			);
    }

	/**
     * Know if address was updated outside of the US
     */
    public static Boolean addressUpdatedOutCountry(
		SObject obj,
        SObject oldObj,
        String country)
    {
		String newVal = (String)obj.get('Country_Name__c');
		String oldVal = (String)oldObj.get('Country_Name__c');

        if(String.isBlank(newVal) ||
            String.isBlank(oldVal)) {
                return false;
        }

        return 
			oldVal != newVal && 
            oldVal.containsIgnoreCase(country);
    }

}