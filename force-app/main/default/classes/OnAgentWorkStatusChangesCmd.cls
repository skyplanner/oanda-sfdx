/**
 * @File Name          : OnAgentWorkStatusChangesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/1/2024, 1:40:52 PM
**/
public inherited sharing class OnAgentWorkStatusChangesCmd {

	//this field exists because the status field is not creteable or updateable in test
	public static Boolean assignedIsDefaultStatusInTest = false;

	@TestVisible
	final List<SObject> recordsToUpdate;

	OnCaseAssignedOrOpenedCmd caseCmd;
	OnMsgSessionAssignedOrOpenedCmd msgSessionCmd;

	public OnAgentWorkStatusChangesCmd() {
		recordsToUpdate = new List<SObject>();
		caseCmd = new OnCaseAssignedOrOpenedCmd();
		msgSessionCmd = new OnMsgSessionAssignedOrOpenedCmd();
	}

	public Boolean checkRecord(
		AgentWork rec, 
		AgentWork oldRec
	) {
		Boolean statusChange = (oldRec != null)
			? (rec.Status != oldRec.Status)
			: true;
		Boolean result = checkRecord(
			rec, // rec
			oldRec, // oldRec
			statusChange, // statusChange
			rec.Status // newStatus
		);
		return result;
	}

	@TestVisible
	Boolean checkRecord(
		AgentWork rec, 
		AgentWork oldRec,
		Boolean statusChange,
		String newStatus
	) {
		Boolean recordProcessed = false;
		
		if (
			String.isNotBlank(rec.WorkItemId) &&
			(statusChange == true) &&
			(
				(newStatus == OmnichanelConst.AGENT_WORK_STATUS_ASSIGNED) ||
				(newStatus == OmnichanelConst.AGENT_WORK_STATUS_OPENED) ||
				(
					Test.isRunningTest() &&
					(assignedIsDefaultStatusInTest == true)
				)
			)
		) {
			Schema.SObjectType workItemSObjType = 
				rec.WorkItemId.getSobjectType();

			recordProcessed = msgSessionCmd.checkRecord(
				workItemSObjType, // workItemSObjType
				newStatus, // agentWorkStatus
				rec.WorkItemId // workItemId
			);
			
			if (recordProcessed == false) {
				recordProcessed = caseCmd.checkRecord(
					workItemSObjType, // workItemSObjType
					rec.WorkItemId // workItemId
				);
			}
		}
		return recordProcessed;
	}

	public void execute() {
		msgSessionCmd.execute();
		recordsToUpdate.addAll(msgSessionCmd.recordsToUpdate);

		caseCmd.execute();
		recordsToUpdate.addAll(caseCmd.recordsToUpdate);
		
		update recordsToUpdate;
	}

}