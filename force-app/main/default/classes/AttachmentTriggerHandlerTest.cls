/**
 * @Description  : Test class for AttachmentTriggerHandler.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-06
**/
@isTest
public with sharing class AttachmentTriggerHandlerTest {
    @TestSetup
    static void setup() {
        insert new Contact(LastName = 'testName');
    }

    /**
    * @description Tests insert of Attachment sobject.
    * @author Jakub Fik | 2024-05-06 
    **/
    @IsTest
    static void insertAttachmentTest() {
        Boolean isError = false;
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        Blob body = Blob.valueOf('test body');

        Attachment testAttachment = new Attachment(ParentId = testContact.Id, Body = body, Name = 'testName');
        Test.startTest();
        try {
            insert testAttachment;
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();
        System.assertEquals(false, isError, 'Attachment should be inserted without any error.');
    }

    /**
    * @description Tests update of Attachment sobject.
    * @author Jakub Fik | 2024-05-06 
    **/
    @IsTest
    static void updateAttachmentTest() {
        Boolean isError = false;
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        Blob body = Blob.valueOf('test body');
        Attachment testAttachment = new Attachment(ParentId = testContact.Id, Body = body, Name = 'testName');
        insert testAttachment;

        Test.startTest();
        try {
            testAttachment.Name = 'New Name';
            update testAttachment;
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();
        System.assertEquals(false, isError, 'Attachment should be updated without any error.');
    }

    /**
    * @description Tests delete of Attachment sobject.
    * @author Jakub Fik | 2024-05-06 
    **/
    @IsTest
    static void deleteAttachmentTest() {
        Boolean isError = false;
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        Blob body = Blob.valueOf('test body');
        Attachment testAttachment = new Attachment(ParentId = testContact.Id, Body = body, Name = 'testName');
        insert testAttachment;

        Test.startTest();
        try {
            delete testAttachment;
        } catch (Exception ex) {
            isError = true;
        }
        Test.stopTest();
        System.assertEquals(false, isError, 'Attachment should be deleted without any error.');
    }
}