/**
 * @Description  : Class handles the creation and linking of Leads based on Cases.
 * Logic extracted from CaseTriggerHandler.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-22
 **/

 public inherited sharing class CaseLeadService {
    /**
     * @description Creates or links leads for cases based on email, avoiding duplication with existing contacts or leads.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     **/
    public static void createLeadIfNeeded(List<Case> cases) {
        Map<String, List<Case>> casesByEmailMap = new Map<String, List<Case>>();

        for (Case c : cases) {
            if (c.Lead__c == null && c.Chat_Web_Email__c != null && c.Chat_Web_Email__c != '') {
                List<Case> casesWithEmailsList = casesByEmailMap.get(c.Chat_Web_Email__c);
                if (casesWithEmailsList == null) {
                    casesWithEmailsList = new List<Case>();
                    casesByEmailMap.put(c.Chat_Web_Email__c, casesWithEmailsList);
                }
                casesWithEmailsList.add(c);
            }
        }

        if (casesByEmailMap.size() == 0) {
            return;
        }

        // Remove emails with non-OTMS cases which already exist as Accounts from the map since we don't need to do anything with them
        List<Contact> existingPersonAccount = [
            SELECT Id, AccountId, Email
            FROM Contact
            WHERE
                Email IN :casesByEmailMap.keySet()
                AND (Account.RecordTypeId = :RecordTypeUtil.getPersonAccountRecordTypeId() OR Account.RecordTypeId = :RecordTypeUtil.getEntityPersonAccountRecordTypeId())
        ];

        for (Contact contact : existingPersonAccount) 
        {
            if (casesByEmailMap.containsKey(contact.Email) && ! RecordTypeUtil.isCaseOTMSRecordTypes(casesByEmailMap.get(contact.Email)[0].RecordTypeId)) {
                casesByEmailMap.remove(contact.Email);
            }
        }

        // List of Existing Leads to the mapAttachedCasesToLead keySet
        List<Lead> existingLeads = [
            SELECT Id, Email, RecordTypeId
            FROM Lead
            WHERE Email IN :casesByEmailMap.keySet() AND IsConverted = FALSE
        ];

        // Keep track of which emails have already existing acc/con and remove them from the map after
        Set<String> emailsToRemove = new Set<String>();

        // Link existing Leads
        for (Lead l : existingLeads) 
        {
            List<Case> casesWithEmailList = casesByEmailMap.get(l.Email);

            for (Case c : casesWithEmailList) {
                if (shouldLinkCaseToLead(l, c)) 
                {
                    c.Lead__c = l.Id;
                    emailsToRemove.add(l.Email);
                }
            }
        }

        for (String email : emailsToRemove) {
            casesByEmailMap.remove(email);
        }

        List<Lead> leads = new List<Lead>();
        Map<String, Lead> leadsByEmailMap = new Map<String, Lead>();

        // Create new Leads
        for (String email : casesByEmailMap.keySet()) {
            List<Case> casesWithEmailList = casesByEmailMap.get(email);
            String name = getSuppliedName(casesWithEmailList);

            Lead ld = new Lead();

            ld.RecordTypeId = RecordTypeUtil.isCaseOTMSRecordTypes(casesWithEmailList[0].RecordTypeId) ? RecordTypeUtil.OTMS_LEAD_RECORD_TYPE_ID : LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_STANDARD;
            ld.LastName = name;
            ld.Email = email;
            ld.Status = LeadUtil.LEAD_STATUS_NOT_CONTACTED;
            ld.LeadSource = LeadUtil.LEAD_SOURCE_CX;
            //ld.Status = 'Not Contacted (default)';
            //ld.LeadSource = 'Agent';

            leads.add(ld);
            // Add Leads to the Map inorder to link the leads to the Case
            leadsByEmailMap.put(email, ld);
        }

        if (leads.size() > 0) {
            insert leads;
        }

        // Attach Case to the newly created Lead as you have the Lead ID now
        for (String email : casesByEmailMap.keySet()) {
            Lead ld = leadsByEmailMap.get(email);

            List<Case> casesWithEmailList = casesByEmailMap.get(email);

            for (Case c : casesWithEmailList) {
                c.Lead__c = ld.Id;
            }
        }
    }

    /**
     * @description Retrieves the first non-empty name from a list of cases, or returns 'Unknown' if none found.
     * @author Jakub Fik | 2024-05-22
     * @param cases
     * @return String
     **/
    private static String getSuppliedName(List<Case> cases) {
        String name = 'Unknown';
        for (Case c : cases) {
            if (c.Chat_Web_Name__c != null && c.Chat_Web_Name__c != '') {
                name = c.Chat_Web_Name__c;
                break;
            }
        }
        return name;
    }

    private static Boolean shouldLinkCaseToLead(Lead l, Case c) {
        return (RecordTypeUtil.isCaseOTMSRecordTypes(c.RecordTypeId) && l.RecordTypeId == RecordTypeUtil.OTMS_LEAD_RECORD_TYPE_ID) ||
               (! RecordTypeUtil.isCaseOTMSRecordTypes(c.RecordTypeId) && l.RecordTypeId != RecordTypeUtil.OTMS_LEAD_RECORD_TYPE_ID);
    }
}