@isTest(SeeAllData = false)
public with sharing class Client4GRequestHandlerTest 
{
    public static Account a;

    @testSetup static void init()
    {
        Settings__c settings = new Settings__c(Name='Default', Client_4G_Request_Email_List__c='salesforce@oanda.com');
        insert settings;

        User u = UserUtil.getTestUser();
        insert u;

        a = new Account(LastName='test account', OwnerId=u.Id);
        insert a;

        Opportunity o = new Opportunity(Name='test opp', StageName='test stage', CloseDate=Date.today(), AccountId=a.Id);
        insert o;
        
        fxAccount__c fxa1 = new fxAccount__c(Name='test fxAccount', Account__c=a.Id, Opportunity__c=o.Id);
        insert fxa1;

        Case c = new Case( Subject = 'Test',AccountId = a.Id, RecordTypeId = '012U0000000ADfD');
        insert c;

        Document__c[] docs = new  Document__c[]
        {
            new Document__c(Name = 'd1', Case__c = c.Id),
            new Document__c(Name = 'd1', fxAccount__c = fxa1.Id)
        };
        Insert docs;

        Blob body = Blob.valueOf('test body');  
        Attachment[] attachments = new Attachment[]
        {
            new Attachment(ParentId=docs[0].Id, Name='test attachment', Body=body),
            new Attachment(ParentId=docs[1].Id, Name='test attachment', Body=body)
        };
        insert attachments;
    }
   
    static testMethod void testEmailAttachments() 
    {
        Account ac = [select Id From Account Limit 1];
        
        Test.startTest();
        Client4GRequestHandler.emailClientRecords(ac.Id);
        Test.stopTest();
        
        system.assertEquals(2, Client4GRequestHandler.documentsToPrint.size()); //  1 - account, 1 -case
        system.assertEquals(2, Client4GRequestHandler.documentIds.size()); // 2- Documents 
    }
   
}