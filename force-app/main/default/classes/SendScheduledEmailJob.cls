/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
public without sharing class SendScheduledEmailJob extends BaseJob {
	public static final String SEND_EMAIL_JOB = 'SEND_SCHEDULED_EMAIL';

	public final Integer scope;
	public final Datetime processingDateTime;

	public static SendScheduledEmailJob getNewInstance() {
		IScheduledEmailSettingManager settingManager = new ScheduledEmailSettingManager();
		return new SendScheduledEmailJob(
			settingManager.getSetting().getScope()
		);
	}

	public SendScheduledEmailJob(Integer scope) {
		super(SEND_EMAIL_JOB);
		this.scope = scope;
		this.processingDateTime = Datetime.now();
	}
	public SendScheduledEmailJob(Integer scope, Datetime processingDateTime) {
		super(SEND_EMAIL_JOB);
		this.scope = scope;
		this.processingDateTime = processingDateTime;
	}

	public override BaseJob getNextJobInstance() {
		return new SendScheduledEmailJob(this.scope, this.processingDateTime);
	}

	public override Boolean doAction(QueueableContext context) {

		return true;

		// ScheduleMailSender emailSender = new ScheduleMailSender();
		// Integer count = emailSender.processScheduledEmails(
		// 	this.processingDateTime,
		// 	scope
		// );
		// Boolean jobIsDone = (count < scope);
		// return jobIsDone;
	}
}