public with sharing class Attachment_Util {
    class AttachmentDeletionException extends Exception {}
    
    public static void preventDeleteAttachment(Attachment[] attachments) {
        for(Attachment a : attachments) {
            if(!UserUtil.isSystemAdministratorProfile(UserInfo.getUserId()) && !UserUtil.isIntegrationUserId(UserInfo.getUserId())) {
                a.addError('Error: Attachments cannot be deleted');
            }
        }
    }
    
    public static void reparentFromEmailMessageToDocument(List<Attachment> attachments) {
        List<Document__c> docsToInsert = new List<Document__c>();
        Set<Id> emailMessageIds = new Set<Id>();
        
        for(Attachment a : attachments) {
            if(isEmailMessageId(a.ParentId)) {
                emailMessageIds.add(a.ParentId);
            }
        }
        
        if(emailMessageIds.size()==0) {
            return;
        }
        
        Map<Id, Id> caseIdByEmailMessageId = new Map<Id, Id>();
        for(EmailMessage em : [SELECT Id, ParentId FROM EmailMessage WHERE Id IN :emailMessageIds]) {
            if(isCaseId(em.ParentId)) {
                caseIdByEmailMessageId.put(em.Id, em.ParentId);
            }
        }
        
        for(Attachment a : attachments) {
            Id caseId = caseIdByEmailMessageId.get(a.ParentId);
            if(caseId!=null) {
                docsToInsert.add(new Document__c());
            }
        }

        if(docsToInsert.size()>0) {
            insert docsToInsert;
    
            Integer docIndex = 0;
            for(Attachment a : attachments) {
                Document__c d = docsToInsert[docIndex++];
                
                Id caseId = caseIdByEmailMessageId.get(a.ParentId);
                // Attachment -> EmailMessage -> Document -> Case
                
                // Attachment -> EmailMessage -> Case
                // Attachment -> Document -> Case
                if(caseId!=null) {
                    d.Case__c = caseId;
                    d.Name = a.Name.left(80);
                    a.ParentId = d.Id;
                }
            }
            System.assertEquals(docsToInsert.size(), docIndex);
            
            update docsToInsert;
        }
    }
    
    // TODO refector reparentFromAccountToDocument() and reparentFromEmailMessageToDocument()
    
    public static void reparentFromAccountToDocument(List<Attachment> attachments) {
        List<Document__c> docsToInsert = new List<Document__c>();
        //List<Case> casesToInsert = new List<Case>();
        Set<Id> accountIds = new Set<Id>();
        
        for(Attachment a : attachments) {
            if(isAccountId(a.ParentId)) {
                accountIds.add(a.ParentId);
                docsToInsert.add(new Document__c(Account__c=a.ParentId));
                //casesToInsert.add(new Case(Subject='Client Documents for Approval', Type='Onboarding', Inquiry_Nature__c='General', Funding_OB_Trading__c='On Boarding', AccountId=a.ParentId));
            } else if(isLeadId(a.ParentId)) {
                accountIds.add(a.ParentId);
                docsToInsert.add(new Document__c(Lead__c=a.ParentId));
                //casesToInsert.add(new Case(Subject='Client Documents for Approval', Type='Onboarding', Inquiry_Nature__c='General', Funding_OB_Trading__c='On Boarding', Lead__c=a.ParentId));
            }
        }
        
        if(accountIds.size()==0) {
            return;
        }
/*
        // TODO: group attachments into one case        
        Map<Id, Id> caseIdByAccountId = new Map<Id, Id>();
        for(Case c : [SELECT Id, Account FROM Case WHERE Account IN :accountIds]) {
            if(isCaseId(em.ParentId)) {
                caseIdByEmailMessageId.put(em.Id, em.ParentId);
            }
        }
*/        

        insert docsToInsert;
        //insert casesToInsert;

        Integer docIndex = 0;
        for(Attachment a : attachments) {
            if(isAccountId(a.ParentId) || isLeadId(a.ParentId)) {
                Document__c d = docsToInsert[docIndex++];
                //Case c = casesToInsert[docIndex++];
                
                // Attachment -> EmailMessage -> Document -> Case
                
                // Attachment -> EmailMessage -> Case
                // Attachment -> Document -> Case
                //d.Case__c = c.Id;
                d.Name = a.Name.left(80);
                a.ParentId = d.Id;
            }
        }
        System.assertEquals(docsToInsert.size(), docIndex);
        
        update docsToInsert;
    }
    
    static boolean isEmailMessageId(Id i ) {
        if(((String) i).startsWith('02s')) {
            return true;
        } else {
            return false;
        }
    }
    
    static boolean isCaseId(Id i ) {
        if(((String) i).startsWith('500')) {
            return true;
        } else {
            return false;
        }
    }
    
    static boolean isAccountId(Id i ) {
        if(((String) i).startsWith('001')) {
            return true;
        } else {
            return false;
        }
    }
    
    static boolean isLeadId(Id i ) {
        if(((String) i).startsWith('00Q')) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
    
    // this code is for not storing attachments in Salesforce
    // April 30, 2015: decided to store attachments in Salesforce

    public static void preventAttachmentsOnEmailMessage(Attachment[] attachments) {
        List<Id> attachmentIdsToDelete = new List<Id>();
        for(Attachment a : attachments) {
            if(isEmailMessageId(a.ParentId) && UserUtil.isIntegrationUserId(a.CreatedById)) {
                attachmentIdsToDelete.add(a.Id);
            }
        }
        
        if(attachmentIdsToDelete.size()>0) {
            deleteAttachmentFuture(attachmentIdsToDelete);
        }
    }
    
    @future
    public static void deleteAttachmentFuture(List<Id> ids) {
        if(ids.size()==0) {
            return;
        }
        List<Attachment> attachmentsToDelete = new List<Attachment>();
        for(Id i : ids) {
            attachmentsToDelete.add(new Attachment(Id=i));
        }
        delete attachmentsToDelete;
    }
    */
}