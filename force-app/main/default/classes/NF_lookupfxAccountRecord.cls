/**
* Created by Neuraflash LLC on 04/01/19.
* Implementation : Chatbot
* Summary : Lookup fxAccount records by searching username & Return Boolean
* Details : 1. Searches for a fxAccount record by username provided.
*           2. Return Boolean if Lead found/not found.
* 
*/

global without sharing class NF_lookupfxAccountRecord implements nfchat.DataAccessClass{
    
    global String processRequest(Map<String,Object> paramMap, String aiServiceResponse, String aiConfig) {
            Map<String, String> eventParams = new Map<String, String>();
            String inputUsername = (String)paramMap.get('username'); //get email from DF
            System.debug('>>NF_lookupfxAccountRecord: inputUsername=' + inputUsername);

            List<fxAccount__c> fxAccountsByUsername = [SELECT Id, Name
                                              FROM fxAccount__c
                                              WHERE name = :inputUsername
                                              ORDER BY CreatedDate
                                              ASC NULLS FIRST LIMIT 1];
            System.debug('>>NF_lookupfxAccountRecord: fxAccountsByUsername=' + fxAccountsByUsername);


            //there are two EmailAddress field on account(Confirm)
            if(fxAccountsByUsername.isEmpty()){
                System.debug('>>NF_lookupfxAccountRecord: No fxAccounts returned');
                eventParams.put('Result','false');
            }
			else{
                System.debug('>>NF_lookupfxAccountRecord: found fxAccounts');
                eventParams.put('Result','true');

                System.debug('>>NF_lookupfxAccountRecord: retrieve accounts by Name=' + fxAccountsByUsername[0].Name);
                List<Account> accounts = [SELECT PersonEmail
                                          FROM Account
                                          WHERE fxAccount__r.Name = :fxAccountsByUsername[0].Name];

                System.debug('>>NF_lookupfxAccountRecord: accounts=' + accounts);

                if (!accounts.isEmpty()) {
                    inputUsername = accounts[0].PersonEmail;
                }
            }

            System.debug('>>NF_lookupfxAccountRecord: returning username=' + inputUsername);

            eventParams.put('email', inputUsername);
            return JSON.serialize(eventParams);
    }
}