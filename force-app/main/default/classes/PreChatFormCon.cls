global without sharing class PreChatFormCon {
	@RemoteAction
	static global boolean getcontact(string e) {
	    List<Contact> contacts = [SELECT Id FROM Contact WHERE email=:e LIMIT 1];
	    return contacts.size()==1;
	}
	
	public String getAPIRoot(){
    	return CustomSettings.getLiveAgentAPIRoot();
    }
    
    public String getJSRoot(){
    	return CustomSettings.getLiveAgentJSRoot();
    }

}