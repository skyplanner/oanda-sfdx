/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-03-2023
 * @last modified by  : Jakub Jaworski
**/
public with sharing class TasApiManager {
    public final static transient String FIELD_NAME_MT4_SERVER = 'mt4ServerID';
    public final static transient String FIELD_NAME_DIVISION_TRADING_GROUP 
        = 'divisionTradingGroupId';
    public final static transient String ACTION_NAME_MT4_NOTIFICATION 
        = 'MT4 Notification Resent for v20 Account Id: {0}';
    TasV20AccountCallout tasCallout;
    AuditTrailManager auditTrailMngr;
    TasMt5AccountCallout tasMt5AccsCallout;

    public TasApiManager() {
        tasCallout = new TasV20AccountCallout();
        auditTrailMngr = new AuditTrailManager();
        tasMt5AccsCallout = new TasMt5AccountCallout();
    }

    public void updateV20Account(Updatev20AccountWrapper requestWrapper) {
        TasV20AccountPatchRequest request = new TasV20AccountPatchRequest();
        request.mt4ServerID = (requestWrapper.mt4ServerId !=requestWrapper.prevMt4ServerId) ? requestWrapper.mt4ServerId : null;
        request.tradingGroupID = (requestWrapper.divisionTradingGroupId != requestWrapper.prevDivisionTradingGroupId)
            ? requestWrapper.divisionTradingGroupId : null;
        EnvironmentIdentifier id = new EnvironmentIdentifier(
            requestWrapper.env, requestWrapper.v20AccountId);
        tasCallout.updateV20AccsDetails(id, request);
        saveAuditTrail(requestWrapper);
    }

    public TasUserAccountGetResponse getV20UserInfo(
        EnvironmentIdentifier fxTradeUserId
    ) {
        return tasCallout.getV20AccountsByUser(fxTradeUserId);
	}

    public Map<String, List<Map<String, String>>> getGroups(
        String env, Integer divisionId
    ) {
        Map<String, List<Map<String, String>>> result 
            = new Map<String, List<Map<String, String>>>();
        TasGroupsInfoGetResponse r = tasCallout.getGroupInfo(env, divisionId);

        //zeroes are used to unify payload between Apex and JS
        Map<String,String> noneValueMap = new Map<String,String> {
            'value' => '0',
            'label' => '-- - None'
        };
        Map<String,String> defaultValueMap = new Map<String,String> {
            'value' => '0',
            'label' => '-- - Division Default'
        };

        List<Map<String,String>> pricingGroups = new List<Map<String,String>>();
        //adding None value at the beggining of the list
        pricingGroups.add(noneValueMap);
        if(r?.getPricingGroups() != null && !r?.getPricingGroups().isEmpty()) {
            List<Map<String,String>> pricingGroupsToFilter = r.getPricingGroups();
            for (Map<String,String> g : pricingGroupsToFilter) {
                //filtering out unwanted groups
                if (!Constants.PRICING_GROUP_NUMBERS_TO_IGNORE.contains(g.get('value'))) {
                    pricingGroups.add(g);
                }
            }
            
        }
        result.put(Constants.API_FIELD_PRICING_GROUP, pricingGroups);

        List<Map<String,String>> commissionGroups = new List<Map<String,String>>();
        //adding None value at the beggining of the list
        commissionGroups.add(noneValueMap);
        if(r?.getCommissionGroups() != null && !r?.getCommissionGroups().isEmpty()) {
            commissionGroups.addAll(r.getCommissionGroups());
        }
        result.put(Constants.API_FIELD_COMMISSION_GROUP, commissionGroups);


        List<Map<String,String>> currencyConversionGroups = new List<Map<String,String>>();
        //adding Default Division value at the beggining of the list
        currencyConversionGroups.add(defaultValueMap);
        if(r?.getCurrencyConversionGroups() != null && !r?.getCurrencyConversionGroups().isEmpty()) {
            currencyConversionGroups.addAll(r.getCurrencyConversionGroups());
        }
        result.put(Constants.API_FIELD_CURRENCY_CONVERSION_GROUP, currencyConversionGroups);

        List<Map<String,String>> financingGroups = new List<Map<String,String>>();
        //adding None value at the beggining of the list
        financingGroups.add(noneValueMap);
        if(r?.getFinancingGroups() != null && !r?.getFinancingGroups().isEmpty()) {
            financingGroups.addAll(r.getFinancingGroups());
        }
        result.put(Constants.API_FIELD_FINANCING_GROUP, financingGroups);

        return result;
	}

    public List<TasDivisionTradingGroup> getDivisionTradingGroups(
        String env, Integer divisionId
    ) {
        TasGroupsInfoGetResponse r = tasCallout.getGroupInfo(env, divisionId);

        return r?.groups?.division_trading_groups;
	}

    public List<TasMT4Server> getMT4Servers(
        String env, List<Integer> pricingGroups
    ) {        
        TasMT4ServersConfigGetResponse r =
            tasCallout.getMT4Servers(env, pricingGroups);

        return r?.mt4_servers;
	}
    
    public void resendMT4Notification(Updatev20AccountWrapper w) {
        tasCallout.resendMT4Notification(
            new EnvironmentIdentifier(w.env, w.v20AccountId));
        saveMT4NotificationTrail(w);
    }

    public void saveGroupsInfo(
        Map<String, Object> record, Map<String, Object> oldRecord
    ) {
        EnvironmentIdentifier id = UserApiActionBase.getfxTradeUserId(oldRecord);
        Map<String, Object> userV20Account =
            TasUserV20AccountPatchRequest.fromMap(record);

        if (userV20Account == null)
            return;
                    
        tasCallout.updateDefaults(id, userV20Account);
        auditTrailMngr.saveAuditTrail(id.id, record, oldRecord);
    }

	void saveAuditTrail(Updatev20AccountWrapper w) {
        saveDivisionTradingGroupTrail(w);
        saveMT4ServerTrail(w);
	}

	void saveDivisionTradingGroupTrail(Updatev20AccountWrapper w) {
        if (w.divisionTradingGroupId != w.prevDivisionTradingGroupId) {
            AuditTrailManager.AuditTrailChange change
                = new AuditTrailManager.AuditTrailChange(
                    FIELD_NAME_DIVISION_TRADING_GROUP,
                    w.prevDivisionTradingGroupId,
                    w.divisionTradingGroupId);

            auditTrailMngr.saveAuditTrail(w.fxTradeUserId, w.fxAccountId,
                w.v20AccountId, 'Update v20Account: ' + w.v20AccountId, change);
        }
	}

	void saveMT4ServerTrail(Updatev20AccountWrapper w) {
        if (w.mt4ServerId != w.prevMt4ServerId) {
            AuditTrailManager.AuditTrailChange change
                = new AuditTrailManager.AuditTrailChange(
                    FIELD_NAME_MT4_SERVER, w.prevMt4ServerId, w.mt4ServerId);
            
            auditTrailMngr.saveAuditTrail(w.fxTradeUserId, w.fxAccountId,
                w.v20AccountId, 'Update v20Account: ' + w.v20AccountId, change);
        }
	}

    void saveMT4NotificationTrail(Updatev20AccountWrapper w) {
        AuditTrailManager.AuditTrailChange change
            = new AuditTrailManager.AuditTrailChange();

        auditTrailMngr.saveAuditTrail(w.fxTradeUserId, w.fxAccountId,
            w.v20AccountId, String.format(ACTION_NAME_MT4_NOTIFICATION,
            new List<String> {w.v20AccountId}), change);
	}

    public List<TasMT5AccsDetails> getMt5AccountsByOneId(EnvironmentIdentifier ei, String email) {
     
        TasMt5AccsDetailsGetResponse result = tasMt5AccsCallout.getMt5AccsDetails(ei, email);
        return result == null ? new List<TasMT5AccsDetails>() : result.mt5Accs;
    }

    public void lockUnlockMt5Acc(EnvironmentIdentifier ei, String action, String fxTradeUserId){
        tasMt5AccsCallout.lockUnlockMt5AccountByAccountId(ei, action);
        String mt5Acc = ' mt5 Account: ';
        String newValue = action == 'lock' ? 'true' : 'false';
        String orginalValue = action == 'lock' ? 'false' : 'true';
        AuditTrailManager.AuditTrailChange change
                = new AuditTrailManager.AuditTrailChange(
                    Constants.MT5_IS_DISABLED,
                    orginalValue,
                    newValue);

            auditTrailMngr.saveAuditTrail(fxTradeUserId, null, action + mt5Acc + ei.id, change);

	}

    
    public class Updatev20AccountWrapper {
        @AuraEnabled
        public Id fxAccountId { get; set; }
        
        @AuraEnabled
        public String v20AccountId { get; set; }

        @AuraEnabled
        public String env { get; set; }

        @AuraEnabled
        public String fxTradeUserId { get; set; }

        @AuraEnabled
        public Integer divisionTradingGroupId { get; set; }

        @AuraEnabled
        public Integer prevDivisionTradingGroupId { get; set; }

        @AuraEnabled
        public Integer mt4ServerId { get; set; }

        @AuraEnabled
        public Integer prevMt4ServerId { get; set; }
    }
}