/**
 * @File Name          : CheckServiceTransferSkillsCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/6/2024, 3:04:02 AM
**/
public inherited sharing class CheckServiceTransferSkillsCmd {

    final List<PendingServiceRouting> transferList;

    public CheckServiceTransferSkillsCmd(List<PendingServiceRouting> transferList) {
        this.transferList = transferList;
    }

    //Returns false if it finds any transfer without assigned skills
    public Boolean execute() {
        Boolean result = true;
        //String log = '';
        Integer transferListSize = (transferList != null)
            ? transferList.size()
            : 0;
        //log = 'acl.getTransfersSkillRequirements -> transferList.size: ' + transferListSize;
        if (transferListSize == 0) {
            //insert new Acl_log__c(Text5__c = log);
            return result;
        }
        //else...
        Map<ID,PendingServiceRouting> transferMap = 
            new Map<ID,PendingServiceRouting>(transferList);
        Set<ID> routingIdSet = transferMap.keySet();
        //log += '\n routingIdSet: ' + routingIdSet;
        List<SkillRequirement> skillRequirementList = [
            SELECT 
                RelatedRecordId 
            FROM 
                SkillRequirement 
            WHERE 
                RelatedRecordId in :routingIdSet
        ];
        Set<ID> routingWithSkillsIdSet = new Set<ID>();
        for(SkillRequirement skillReq : skillRequirementList) {
            routingWithSkillsIdSet.add(skillReq.RelatedRecordId);
        }
        //log += '\n routingWithSkillsIdSet: ' + routingWithSkillsIdSet;
        for(ID routingId : routingIdSet) {
            if (!routingWithSkillsIdSet.contains(routingId)) {
                PendingServiceRouting transferPsr = 
                    transferMap.get(routingId);
                transferPsr.addError(
                    'To transfer a job you must have at least one skill assigned'
                );
                result = false;
            }
        }
        //log += '\n routingWithoutSkillsIdSet: ' + routingWithoutSkillsIdSet;
        //insert new Acl_log__c(Text5__c = log);
        return result;
    }

    public static CheckServiceTransferSkillsCmd getCheckServiceTransferSkillsCmd(
        List<PendingServiceRouting> objList,
        Map<Id,PendingServiceRouting> oldMap
    ) {
        String messagingChannelDevName = 
			SPSettingsManager.getSettingOrDefault(
				MessagingConst.MSG_CHANNEL_DEV_NAME_SETTING, // settingName
                'sfdc_livemessage' // defaultValue
			);
        ID messagingChannelId = 
            ServiceChannelHelper.getByDevName(
                messagingChannelDevName, // devName
                true // required
            ).Id;
        List<PendingServiceRouting> transferList = new List<PendingServiceRouting>();

        for (PendingServiceRouting pendingRoutObj : objList) {
            PendingServiceRouting oldRecord = oldMap.get(pendingRoutObj.Id);

            if (
                (pendingRoutObj.ServiceChannelId != messagingChannelId) && 
                (pendingRoutObj.IsTransfer == true) && 
                (pendingRoutObj.IsReadyForRouting == true) && 
                (oldRecord.IsReadyForRouting != true) &&
                (pendingRoutObj.WorkItemId != null) &&
                (pendingRoutObj.RoutingType == OmnichanelConst.SKILLS_BASED_ROUTING_TYPE)
            ) {
                transferList.add(pendingRoutObj);
            }
        }
        if (transferList.isEmpty()) {
            return null;
        }
        //else...
        return new CheckServiceTransferSkillsCmd(transferList);
    }

}