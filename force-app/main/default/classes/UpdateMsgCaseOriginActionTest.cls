/**
 * @File Name          : UpdateMsgCaseOriginActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:14:24 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 12:45:55 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class UpdateMsgCaseOriginActionTest {
	@isTest
	private static void testMessagingSessionIdsIsNullOrEmpty() {
		// Test data setup
		Boolean exceptionThrown = false;
		// Actual test
		Test.startTest();
		try {
			UpdateMsgCaseOriginAction.updateMsgCaseOrigin(null);
			UpdateMsgCaseOriginAction.updateMsgCaseOrigin(new List<ID>());
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(false, exceptionThrown, 'Exception must not be thrown');
	}

	@isTest
	private static void testUpdateMsgCaseOriginEx() {
		// Test data setup
		Boolean exceptionThrown = false;
		ExceptionTestUtil.prepareDummyException();
		// Actual test
		Test.startTest();
		try {
			UpdateMsgCaseOriginAction.updateMsgCaseOrigin(null);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, exceptionThrown, 'Exception must be thrown');
	}
	@IsTest
	static void testUpdateMsgCaseOrigin() {
		List<ID> messagingSessionIds = new List<ID>{
			null
		};
		Boolean exceptionThrown = false;
		Test.startTest();
		try {
			UpdateMsgCaseOriginAction.updateMsgCaseOrigin(messagingSessionIds);
		} catch (Exception ex) {
			exceptionThrown = true;
		}
		Test.stopTest();
		Assert.areEqual(false, exceptionThrown, 'Exception must not be thrown');
	}
}