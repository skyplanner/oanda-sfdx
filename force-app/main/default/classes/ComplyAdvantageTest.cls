@isTest(seeAllData=false)
public class ComplyAdvantageTest 
{  
    public static final string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    public static final string onboardingCaseRecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId();
    public static final Id complyAdvantageCaseQueueId = UserUtil.getQueueId('Comply Advantage Cases');
    
    public static final string sampleApiReponse = '{'+
        '"code": 200,'+
        '"status": "success",'+
        '"content": {'+
            '"data": {'+
                '"id": 5678,'+
                '"ref": "1554108581-EG5TXMgr",'+
                '"searcher_id": 2247,'+
                '"match_status": "potential_match",'+
                '"created_at": "2015-06-18 16:52:42",' +
                '"risk_level": "unknown",'+
                '"search_term": "Robert Gabriel Mugabe1",'+
                '"submitted_term": "Robert Gabriel Mugabe1",'+
                '"client_ref": "CUST000456",'+
                '"total_matches": 1,'+
                '"share_url": "app-qa.complyadvantage.com/public/search/1554108581-EG5TXMgr/646dc1368af0"'+
            '}'+
       '}'+
     '}';
    
    public static Lead[] leads;
    public static fxAccount__c[] fxAccounts;
    public static Account[] accounts;
    public static Case obCase;
    public static Case complyCase;
    public static Entity_Contact__c[] entityContacts;
    
    static void BlockCASearchTestDataSetup()
    {
        Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
        insert settings;
        
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
        insert caInstanceSetting;
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
                                                                                                          Search_Profile__c = 'CA',
                                                                                                          Fuzziness__c = 60,
                                                                                                          Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
        insert caDivisionSetting;
        
        leads = new Lead[]
        {
            new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', MiddleName='none', Email = 'test1@oanda.com'),
            new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', MiddleName='none', Email = 'test2@oanda.com')     
        };
        insert leads;
        
        fxAccounts = new fxAccount__c[]
        {
            new fxAccount__c(Account_Email__c = 'test1@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[0].Id,Division_Name__c = 'OANDA Canada', Citizenship_Nationality__c = 'Turkey'),
            new fxAccount__c(Account_Email__c = 'test2@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[1].Id,Division_Name__c = 'OANDA Canada', Citizenship_Nationality__c = 'Turkey')  
        };    
        insert fxAccounts; 
        
        accounts = new Account[]
        {
            new Account(FirstName = 'Robert Gabriel',LastName = 'Mugabe', MiddleName='none', PersonMailingCountry = 'Canada',fxAccount__c = fxAccounts[0].Id),
            new Account(FirstName = 'Robert Gabriel',LastName = 'Mugabe', MiddleName='none', PersonMailingCountry = 'Canada',fxAccount__c = fxAccounts[1].Id)  
        };    
        insert accounts; 

        obCase = new Case(Subject = 'Application Review for Robert Gabriel Mugabe',
                               Lead__c = leads[0].Id,
                               RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                               fxAccount__c =fxAccounts[0].Id);
        insert obCase;
        
        complyCase = new Case(Lead__c = leads[0].Id, 
                                   Subject  = 'Comply Advantage Case for Robert Gabriel Mugabe',
                                   fxAccount__c =fxAccounts[0].Id,
                                   accountId = accounts[0].Id,
                                   parentId = obCase.Id,
                                   status = 'Closed',
                                   RecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId());
        insert complyCase;
        
        entityContacts = new Entity_Contact__c[]
        { 
            new Entity_Contact__c(First_Name__c =  'test',Last_Name__c = 'test',Account__c = accounts[0].Id, RecordTypeId = RecordTypeUtil.getRecordTypeId('General', 'Entity_Contact__c')),
            new Entity_Contact__c(First_Name__c =  'test1',Last_Name__c = 'test1',Account__c = accounts[1].Id, RecordTypeId = RecordTypeUtil.getRecordTypeId('General', 'Entity_Contact__c'))
        };
        insert entityContacts;

    }
    
    @isTest
    static void test1_getComplyAdvantageSearchResults_Match() 
    { 
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
        insert caInstanceSetting;
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
                                                                                                          Search_Profile__c = 'CA',
                                                                                                          Fuzziness__c = 60,
                                                                                                          Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
        insert caDivisionSetting;
        
        Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
        insert settings;
                
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        
        Lead lead = new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com');
        insert lead;
        
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'test1@oanda.com';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.Lead__c = lead.Id;
        tradeAccount.Division_Name__c = 'OANDA Canada';
        tradeAccount.Birthdate__c = date.newInstance(1990, 11, 21);
        tradeAccount.Citizenship_Nationality__c = 'Canada';
        insert tradeAccount;
        
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = tradeAccount.Id;
        eidResultObj.Provider__c = 'EquifaxUS';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '1012';
        eidResultObj.Tracking_Number__c = '123456';
        eidResultObj.Result__c = 'pass';
        insert eidResultObj;

        Test.stopTest();
        
        Comply_Advantage_Search__c search = [select Id,
                                                 Match_Status__c,
                                                 Report_Link__c
                                          From Comply_Advantage_Search__c
                                          Where Is_Monitored__c = true and fxAccount__c = :tradeAccount.Id Limit 1];
        
        system.assertEquals('potential_match' , search.Match_Status__c);
        system.assertEquals('app-qa.complyadvantage.com/public/search/1554108581-EG5TXMgr/646dc1368af0' , search.Report_Link__c);
    }

    @isTest
    static void test2_getComplyAdvantageSearchResults_NoMatch() {
        
        string noMatchReponse = sampleApiReponse.replace('potential_match', 'no_match');

        Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
        insert settings;
        
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
        insert caInstanceSetting;
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
                                                                                                          Search_Profile__c = 'CA',
                                                                                                          Fuzziness__c = 60,
                                                                                                          Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
        insert caDivisionSetting;
        
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, noMatchReponse, 'success', null));
        Test.startTest();
        
        Lead lead = new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com');
        insert lead;
        
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'test1@oanda.com';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.Lead__c = lead.Id;
        tradeAccount.Division_Name__c = 'OANDA Canada';
        insert tradeAccount;    
        
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = tradeAccount.Id;
        eidResultObj.Provider__c = 'EquifaxUS';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '1012';
        eidResultObj.Tracking_Number__c = '123456';
        eidResultObj.Result__c = 'pass';
        insert eidResultObj;

        Test.stopTest();
        
        Comply_Advantage_Search__c search = [select Id,
                                                 Match_Status__c,
                                                 Report_Link__c
                                          From Comply_Advantage_Search__c
                                          Where Is_Monitored__c = true and fxAccount__c = :tradeAccount.Id Limit 1];

        system.assertEquals('no_match' , search.Match_Status__c);
    }
    
    
    // @isTest
    // static void test3_getComplyAdvantageSearchReport() {
        
    //     Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
    //     insert caInstanceSetting;
    //     Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
    //                                                                                                       Search_Profile__c = 'CA',
    //                                                                                                       Fuzziness__c = 60,
    //                                                                                                       Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
    //     insert caDivisionSetting;
        
    //     Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
    //     insert settings;
         
    //     Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
    //     Test.startTest();
        
    //     Lead lead = new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com');
    //     insert lead;
        
    //     fxAccount__c tradeAccount = new fxAccount__c();
    //     tradeAccount.Account_Email__c = 'test1@oanda.com';
    //     tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
    //     tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    //     tradeAccount.Lead__c = lead.Id;
    //     tradeAccount.Division_Name__c = 'OANDA Canada';
    //     insert tradeAccount;
       
    //     ComplyAdvantageSearch newComplyAdvantageSearch = new ComplyAdvantageSearch();
    //     newComplyAdvantageSearch.caSearchId ='12344';
    //     newComplyAdvantageSearch.fxAccountId = tradeAccount.Id;
    //     newComplyAdvantageSearch.divisonName = 'OANDA Canada';
        
    //     system.enqueueJob(new ComplyAdvantageReport(newComplyAdvantageSearch)); 
    //     Test.stopTest();
        
    //     Document__c[] report = [select Id from Document__c where fxAccount__c =:tradeAccount.Id];
    //     system.assertEquals(1 , report.size());       
    // }
   
    @isTest
    static void test4_UpdateMatchStatus() {
        
        Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
        insert settings;
        
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
        insert caInstanceSetting;
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
                                                                                                          Search_Profile__c = 'CA',
                                                                                                          Fuzziness__c = 60,
                                                                                                          Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
        insert caDivisionSetting;
        
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        
        Lead lead = new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com');
        insert lead;
        
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'test1@oanda.com';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.Lead__c = lead.Id;
        tradeAccount.Division_Name__c = 'OANDA Canada';
        insert tradeAccount;

        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = tradeAccount.Id;
        eidResultObj.Provider__c = 'EquifaxUS';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '1012';
        eidResultObj.Tracking_Number__c = '123456';
        eidResultObj.Result__c = 'pass';
        insert eidResultObj;
        
        Account account = new Account(FirstName = 'Robert Gabriel',LastName = 'Mugabe', PersonMailingCountry = 'Canada',fxAccount__c = tradeAccount.Id);
        insert account; 
        
        Test.stopTest();
        
        Comply_Advantage_Search__c comAdvSearchInfo = [select Match_Status__c from Comply_Advantage_Search__c Where fxAccount__c = :tradeAccount.Id Limit 1 ];
        comAdvSearchInfo.Match_Status__c = '';
        comAdvSearchInfo.Match_Status_Override_Reason__c = 'Test';
        update comAdvSearchInfo;
    }
    
    @isTest
    static void test5_monitoredSearchUpdated() { 
        BlockCASearchTestDataSetup();
        
        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = leads[0].Name;
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;
        
        RestRequest request = new RestRequest();
        request.requestUri =URL.getSalesforceBaseUrl().toExternalForm() + '/ComplyAdvantage/services/apexrest/monitor';
        request.httpMethod = 'POST';
        request.addParameter('key', 'i9ExW1Swh1qSdfZdhGJ2pRbb8cazYaw8');
        request.addHeader('Content-Type', 'application/json');
        
        string payload = '{';
        payload+= '"event":"monitored_search_updated",';
        payload+= '"data": {';
        payload+=    '"search_id": 5678,';
        payload+=    '"updated": ["8NMXF7QX4QV8LFD", "X4525MEAZKKPX0T", "73HBD537LCX5JT6"],';
        payload+=    '"new": ["Q3OX4KS0KEMCVDH", "UCC60H79WVU94Z0"],';
        payload+=    '"removed": ["9D1ETD0ADTT4HDH", "I38XC0R6Y1EQ083"],';
        payload+=    '"is_suspended": true';
        payload+=   '}';
        payload+= '}';
            
        request.requestBody = Blob.valueOf(payload);
        RestContext.request = request; 
        
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();  
        ComplyAdvantageMonitor.doPost();      
        Test.stopTest();
        
        Case[] verifyCases = [select Id,
                               status
                        from Case 
                        Where fxAccount__c = :fxAccounts[0].Id and 
                              RecordTypeId = :RecordTypeUtil.getComplianceCheckCaseRecordTypeId()];
        
        system.assertEquals('Re-opened' , verifyCases[0].status);  
    }
      
    @isTest
    static void test6_monitoredSearchStatusUpdated() {
        
        BlockCASearchTestDataSetup();
        
        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = leads[0].Name;
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;
        
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri =URL.getSalesforceBaseUrl().toExternalForm() + '/ComplyAdvantage/services/apexrest/monitor';
        request.addParameter('key', 'i9ExW1Swh1qSdfZdhGJ2pRbb8cazYaw8');
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        
        string searchStatusUpdatePayload = '';
        searchStatusUpdatePayload +='{';
        searchStatusUpdatePayload +='"event":"search_status_updated",';
        searchStatusUpdatePayload +='"data":{';
        searchStatusUpdatePayload +=    '"changes":{';
        searchStatusUpdatePayload +=        '"before":{"match_status":"unknown"},';
        searchStatusUpdatePayload +=        '"after":{"match_status":"true_positive"}';
        searchStatusUpdatePayload +=     '},';
        searchStatusUpdatePayload +=     '"ref":"1568314476-wPNHIc4U",';
        searchStatusUpdatePayload +=     '"client_ref":"CUST000456",';
        searchStatusUpdatePayload +=     '"search_id":5678';
        searchStatusUpdatePayload +=     '}';         
        searchStatusUpdatePayload +='}';
            
        request.requestBody = Blob.valueOf(searchStatusUpdatePayload);
        RestContext.request = request;
        
        ComplyAdvantageMonitor.doPost();
        
        Test.stopTest();
        
        Comply_Advantage_Search__c searchResults = [select Match_Status__c from Comply_Advantage_Search__c Where Search_Id__c='5678' Limit 1 ];
        system.assertEquals('true_positive' , searchResults.Match_Status__c);  
    } 
    
    @isTest
    static void test7_AccountNameChange() {
        
         BlockCASearchTestDataSetup();
         
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        accounts[0].MiddleName = 'test';
        checkRecursive.SetOfIDs = new Set<Id>();
        update accounts[0];
        Test.stopTest();
        
        Comply_Advantage_Search__c[] accountNameSearch = [select Id,Search_Reason__c From Comply_Advantage_Search__c where fxAccount__c =: fxAccounts[0].Id];
        system.assertEquals('Name change', accountNameSearch[0].Search_Reason__c);
    }
    
     @isTest
    static void test8_fxAccountChange() {
               
         BlockCASearchTestDataSetup();

         EID_Result__c eidResultObj = new EID_Result__c();
         eidResultObj.fxAccount__c = fxAccounts[0].Id;
         eidResultObj.Provider__c = 'EquifaxUS';
         eidResultObj.Date__c = system.today();
         eidResultObj.Reason__c = '1012';
         eidResultObj.Tracking_Number__c = '123456';
         eidResultObj.Result__c = 'fail';
         insert eidResultObj;
         
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        fxAccounts[0].Citizenship_Nationality__c = 'Canada';
        update fxAccounts[0];
        Test.stopTest();
        
        Comply_Advantage_Search__c[] accountNameSearch = [select Id,Search_Reason__c From Comply_Advantage_Search__c where fxAccount__c =: fxAccounts[0].Id];
        system.assertEquals('Citizenship Nationality change', accountNameSearch[0].Search_Reason__c);
    }
    
    
    @isTest
    static void test9_EntityContactCreate() {
        
         BlockCASearchTestDataSetup();
         
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();

		Affiliate__c aff = new Affiliate__c(
			Username__c = 'aedaefasdf',
			Firstname__c = 'adsc',
			LastName__c = 'wedfwef',
			Division_Name__c = 'OANDA Global Markets',
			Country__c = 'Canada'
		);
		insert aff;

        Entity_Contact__c ec = new Entity_Contact__c(First_Name__c =  'test',
                                                     Last_Name__c = 'test',
                                                     Affiliate__c = aff.Id,
                                                     RecordTypeId = RecordTypeUtil.getRecordTypeId('General', 'Entity_Contact__c'));
        insert ec;
        Test.stopTest();
        
        Comply_Advantage_Search__c[] ecSearches = [select Id,Search_Reason__c From Comply_Advantage_Search__c where Entity_Contact__c =: ec.Id];
        // system.assertEquals(1, ecSearches.size());
    }
    
     @isTest
    static void test10_EntityContactUpdate() {
        
         BlockCASearchTestDataSetup();
         
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        entityContacts[0].First_Name__c =  'stest';
        update entityContacts[0];
        Test.stopTest();
        
        Comply_Advantage_Search__c[] ecSearches = [select Id,Search_Reason__c From Comply_Advantage_Search__c where Entity_Contact__c =: entityContacts[0].Id];
        // system.assertEquals(1, ecSearches.size());
    }
    
    @isTest
    static void test12_MonitorOffSearch() {
        
        BlockCASearchTestDataSetup();
        
        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = leads[0].Name;
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;
        
        ComplyAdvantageSearch newComplyAdvantageSearch = new ComplyAdvantageSearch();
        newComplyAdvantageSearch.fxAccountId = fxAccounts[0].Id;
        newComplyAdvantageSearch.divisonName = 'OANDA Canada';
        newComplyAdvantageSearch.previousSearch = comAdvSearchInfo;
        newComplyAdvantageSearch.caseId =  complyCase.Id;
        newComplyAdvantageSearch.caSearchId = '45567';
        
        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        
         system.enqueueJob(new ComplyAdvantageMonitorOff(newComplyAdvantageSearch.previousSearch.Search_Id__c,
                                                         newComplyAdvantageSearch.settings.apiKey,
                                                         false)); 
        
        Test.stopTest();
        
        Comply_Advantage_Search__c searchResults = [select Is_Monitored__c from Comply_Advantage_Search__c Where Search_Id__c='5678' Limit 1 ];
        // system.assertEquals(false , searchResults.Is_Monitored__c);  
    } 
    
    @isTest
    static void test11_AccountCACaseLink() {

         Settings__c settings = new Settings__c(Name='Default', Comply_Advantage_Enabled__c=true);
        insert settings;
        
        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(Name='NA', API_Key__c='test');
        insert caInstanceSetting;
        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(Name = 'OANDA Canada',
                                                                                                          Search_Profile__c = 'CA',
                                                                                                          Fuzziness__c = 60,
                                                                                                          Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id);
        insert caDivisionSetting;
        
        leads = new Lead[]
        {
            new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com'),
            new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test2@oanda.com')     
        };
        insert leads;
        
        fxAccounts = new fxAccount__c[]
        {
            new fxAccount__c(Account_Email__c = 'test1@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[0].Id,Division_Name__c = 'OANDA Canada'),
            new fxAccount__c(Account_Email__c = 'test2@oanda.com', RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,Lead__c = leads[1].Id,Division_Name__c = 'OANDA Canada')  
        };    
        insert fxAccounts; 

        obCase = new Case(Subject = 'Application Review for Robert Gabriel Mugabe',
                               Lead__c = leads[0].Id,
                               RecordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
                               fxAccount__c =fxAccounts[0].Id);
        insert obCase;
        
        complyCase = new Case(Lead__c = leads[0].Id, 
                                   Subject  = 'Comply Advantage Case for Robert Gabriel Mugabe',
                                   fxAccount__c =fxAccounts[0].Id,
                                   parentId = obCase.Id,
                                   RecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId());
        insert complyCase;
        

        Test.setMock(HttpCalloutMock.class, new CalloutMock(200, sampleApiReponse, 'success', null));
        Test.startTest();
        
        Account account = new Account(FirstName = 'Robert Gabriel',LastName = 'Mugabe', PersonMailingCountry = 'Canada',fxAccount__c = fxAccounts[0].Id);
        insert account; 
        
        Test.stopTest();
        
        Case caCase = [select AccountId From Case where fxAccount__c = :fxAccounts[0].Id and RecordTypeId = :RecordTypeUtil.getComplianceCheckCaseRecordTypeId() Limit 1];
        system.assertEquals(account.Id, caCase.AccountId);
    }
    
     @isTest
    static void test12_CASearchMatchesTest() 
    {
        BlockCASearchTestDataSetup();
        
        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = leads[0].Name;
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;

		Comply_Advantage_Search_Entity__c entity =
			new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = comAdvSearchInfo.Id,
				Unique_ID__c = comAdvSearchInfo.Search_Id__c + ':IWUFH46IEHR',
				Entity_Id__c = 'IWUFH46IEHR'
			);

		insert entity;
        
        Test.setMock(HttpCalloutMock.class,
            new CalloutMock(200, '{"code": 200,"content":[]}', 'success', null));
        Test.startTest();
        ComplyAdvantageSearchEntities.getSearchEntities(comAdvSearchInfo.Id);
        ComplyAdvantageSearchEntities.getAllComments(comAdvSearchInfo.Id);
        Test.stopTest();
    }

	@isTest
    static void addComments() {
		Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = 'De la vega';
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.Is_Monitored__c = true;
        insert comAdvSearchInfo;

		Comply_Advantage_Search_Entity__c entity =
			new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = comAdvSearchInfo.Id,
				Unique_ID__c = comAdvSearchInfo.Search_Id__c + ':IWUFH46IEHR',
				Entity_Id__c = 'IWUFH46IEHR'
			);

		insert entity;

		Test.setMock(
			HttpCalloutMock.class,
			new CalloutMock(200,
				JSON.serialize(new Map<String, String> {
					'comment' => 'this is a comment',
					'entity_id' => 'IWUFH46IEHR'
				}),
				'success',
				null)
		);

		Test.startTest();
        ComplyAdvantageSearchEntities.addComments(
			comAdvSearchInfo.Id,
			'IWUFH46IEHR',
			'test');
		Test.stopTest();
	}

    @isTest
    static void test13_CASearchMatchesTest() 
    {
		ComplyAdvantageKeyInformation template;
        BlockCASearchTestDataSetup();
        
        Comply_Advantage_Search__c comAdvSearchInfo = new Comply_Advantage_Search__c();
        comAdvSearchInfo.Name = leads[0].Name;
        comAdvSearchInfo.Search_Id__c = '5678';
        comAdvSearchInfo.Match_Status__c = 'unknown';
        comAdvSearchInfo.Report_Link__c = 'report link';
        comAdvSearchInfo.fxAccount__c = fxAccounts[0].Id;
        comAdvSearchInfo.Is_Monitored__c = true;
        comAdvSearchInfo.Search_Text__c = leads[0].Name;
        comAdvSearchInfo.Case__c = complyCase.Id;
        insert comAdvSearchInfo;

		Comply_Advantage_Search_Entity__c entity =
			new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = comAdvSearchInfo.Id,
				Unique_ID__c = comAdvSearchInfo.Search_Id__c + ':IWUFH46IEHR',
				Entity_Id__c = 'IWUFH46IEHR'
			);

		insert entity;
        
		Test.setMock(
			HttpCalloutMock.class,
			new CalloutMock(200, '{}', 'success', null)
		);

		template = new ComplyAdvantageKeyInformation();
		template.match_status = 'potential_match';
		template.risk_level = 'low';
		template.is_whitelisted = true;

		Test.startTest();
		ComplyAdvantageSearchEntities.updateSearchEntities(
			comAdvSearchInfo.Id,
			new List<String> { 'IWUFH46IEHR' },
			template,
			'Salt & pepper',
			'Ingredients for the food');

		entity = [
			SELECT Match_Status__c, Risk_Level__c, Is_Whitelisted__c
			FROM Comply_Advantage_Search_Entity__c
			WHERE Id = :entity.Id
		];

		System.assertEquals('potential_match', entity.Match_Status__c);
		System.assertEquals('low', entity.Risk_Level__c);
		System.assertEquals(true, entity.Is_Whitelisted__c);
		Test.stopTest(); 
	}

}