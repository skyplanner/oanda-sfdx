/* Name: InternalCloseRestResourceTest
 * Description : Test Class to for internal close API
 * Author: Michal Piatek (mpiatek@oanda.com)
 * Date : 2024 June 07
 */

 @isTest
 public class InternalCloseRestResourceTest {
     
   public static final string REQUEST_BODY_SINGLE = '{\"user_id\":12345,\"is_live\":true,\"close_flag\":true,\"close_reason\":\"Test Reason\",\"to_be_closed_date\":\"2024.12.12\"}';
   public static final string REQUEST_BODY_ARRAY = '[' + REQUEST_BODY_SINGLE + ']';
   
   public static final string REQUEST_URI = '/api/v1/users/internal_close';

   public static final Integer VALID_USER_ID = 12345;
 
   @TestSetup
   static void initData(){

        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = VALID_USER_ID;
        fxAccount.fxTrade_Global_ID__c = VALID_USER_ID + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        insert fxAccount;
   }  

   @isTest
    public static void testDoPostSingle() {
      
        RestContext.request = createRequest(REQUEST_BODY_SINGLE);
        RestContext.response = new RestResponse();

        Test.startTest();
        InternalCloseRestResource.doPost();
        Test.getEventBus().deliver();
        Test.stopTest();

        fxAccount__c fxa = [
            SELECT 
                Id, 
                Account_Close_Date__c, 
                Internal_Closed_Flag__c, 
                Internal_Closed_Reason__c
            FROM fxAccount__c 
            LIMIT 1
        ];
        
        Assert.areEqual(Date.newInstance(2024, 12, 12), fxa.Account_Close_Date__c, 'Close Date should not be blank');
        Assert.areEqual('Test Reason', fxa.Internal_Closed_Reason__c, 'Wrong Internal Close Reason');
        Assert.areEqual(true, fxa.Internal_Closed_Flag__c, 'Fxa should be closed');
    }


    public static void testDoPostArray() {
      
        RestContext.request = createRequest(REQUEST_BODY_ARRAY);
        RestContext.response = new RestResponse();

        Test.startTest();
        InternalCloseRestResource.doPost();
        Test.getEventBus().deliver();
        Test.stopTest();

        fxAccount__c fxa = [
            SELECT 
                Id,
                Account_Close_Date__c, 
                Internal_Closed_Flag__c, 
                Internal_Closed_Reason__c
            FROM fxAccount__c 
            LIMIT 1
        ];
        
        Assert.areEqual(Date.newInstance(2024, 12, 12), fxa.Account_Close_Date__c, 'Close Date should not be blank');
        Assert.areEqual('Test Reason', fxa.Internal_Closed_Reason__c, 'Wrong Internal Close Reason');
        Assert.areEqual(true, fxa.Internal_Closed_Flag__c, 'Fxa should be closed');
    }

    private static RestRequest createRequest(String requestBody) {
        RestRequest request = new RestRequest();

        request.requestUri = REQUEST_URI;
        request.resourcePath = REQUEST_URI;
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf(requestBody);

        return request;
    }
}