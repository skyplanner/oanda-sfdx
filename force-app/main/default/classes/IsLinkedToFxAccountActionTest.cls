/**
 * @File Name          : IsLinkedToFxAccountActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:10:05 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 10:33:30 AM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class IsLinkedToFxAccountActionTest {
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		List<IsLinkedToFxAccountAction.ActionResult> actionResults;
		// Actual test
		Test.startTest();
		actionResults = IsLinkedToFxAccountAction.isLinkedToFxAccount(null);
		Assert.areEqual(
			true,
			actionResults != null && actionResults.size() > 0,
			'Action Result list should not be null or empty'
		);
		actionResults = null;
		actionResults = IsLinkedToFxAccountAction.isLinkedToFxAccount(
			new List<IsLinkedToFxAccountAction.ActionInfo>()
		);

		Assert.areEqual(
			true,
			actionResults != null && actionResults.size() > 0,
			'Action Result list should not be null or empty'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testIsLinkedToAccountOrLeadThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		List<IsLinkedToFxAccountAction.ActionResult> actionResults;
		// Actual test
		Test.startTest();
		try {
			actionResults = IsLinkedToFxAccountAction.isLinkedToFxAccount(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testTActionResultConstructor() {
		// Test data setup
		MessagingAccountCheckInfo checkInfo = new MessagingAccountCheckInfo(
			true
		);
		checkInfo.fxAccountId = '001000000000000';

		// Actual test
		Test.startTest();
		IsLinkedToFxAccountAction.ActionResult actionResult = new IsLinkedToFxAccountAction.ActionResult(
			checkInfo
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			checkInfo.fxAccountId,
			actionResult.fxAccountId,
			'Fx Account Id should be equal to 001000000000000'
		);

		Assert.areEqual(
			checkInfo.isLinkedToAccountOrLead,
			actionResult.isLinkedToAccountOrLead,
			'Is Linked To Account Or Lead should be equal to true'
		);
	}
	@isTest
	private static void testTestCase() {
		IsLinkedToFxAccountAction.ActionInfo actionInfo = new IsLinkedToFxAccountAction.ActionInfo();
		actionInfo.username = 'fake@server.com';
		actionInfo.messagingSessionId = null;
		// Actual test
		Test.startTest();

		List<IsLinkedToFxAccountAction.ActionResult> actionResults = IsLinkedToFxAccountAction.isLinkedToFxAccount(
			new List<IsLinkedToFxAccountAction.ActionInfo>{ actionInfo }
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			actionResults.get(0).isLinkedToAccountOrLead,
			'Is Linked To Account Or Lead should be equal to false'
		);
		Assert.areEqual(
			true,
			actionResults.get(0).fxAccountId == null,
			'Fx Account Id should be equal to null'
		);
	}
}