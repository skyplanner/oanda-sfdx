/**
 * @File Name          : SPConditionsUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/3/2022, 1:47:39 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2022, 3:33:28 PM   acantero     Initial Version
**/
public inherited sharing class SPConditionsUtil {

    public static void checkRequiredStringField(
        SObjectType objectToken,
        SObjectField fieldToken,
        String fieldValue
    ) {
        if (String.isBlank(fieldValue)) {
            throw SPInvalidFieldValueException.getNewForRequired(
                objectToken.getDescribe().getName(),
                fieldToken.getDescribe().getName()
            );
        }
    }

    public static void checkRequiredStringField(
        SObjectType objectToken,
        SObjectField fieldToken,
        String fieldValue,
        Boolean required
    ) {
        if (
            (required == true) &&
            String.isBlank(fieldValue)
        ) {
            throw SPInvalidFieldValueException.getNewForRequired(
                objectToken.getDescribe().getName(),
                fieldToken.getDescribe().getName()
            );
        }
    }

    public static void checkRequiredParam(
        String paramName,
        String paramValue
    ) {
        if (String.isBlank(paramValue)) {
            throw SPInvalidParamException.getNew(paramName);
        }
    }

    public static void checkRequiredParam(
        String paramName,
        String paramValue,
        Boolean required
    ) {
        if (
            (required == true) &&
            String.isBlank(paramValue)
        ) {
            throw SPInvalidParamException.getNew(paramName);
        }
    }

    public static void checkRequiredObjParam(
        String paramName,
        Object paramValue
    ) {
        if (paramValue == null) {
            throw SPInvalidParamException.getNew(paramName);
        }
    }

    public static void checkRequiredObjParam(
        String paramName,
        Object paramValue,
        Boolean required
    ) {
        if (
            (required == true) &&
            (paramValue == null)
        ) {
            throw SPInvalidParamException.getNew(paramName);
        }
    }

    public static void checkRequiredListParam(
        String paramName,
        List<Object> objectList
    ) {
        if (
            (objectList == null) ||
            objectList.isEmpty()
        ) {
            throw SPInvalidParamException.getNewForList(paramName);
        }
    }

    public static void checkRequiredListParam(
        String paramName,
        List<Object> objectList,
        Boolean required
    ) {
        if (
            (required == true) &&
            (
                (objectList == null) ||
                objectList.isEmpty()
            )
        ) {
            throw SPInvalidParamException.getNewForList(paramName);
        }
    }

    public static void checkRequiredStrSetParam(
        String paramName,
        Set<String> strSet
    ) {
        if (
            (strSet == null) ||
            strSet.isEmpty()
        ) {
            throw SPInvalidParamException.getNewForList(paramName);
        }
    }

    public static void checkRequiredStrSetParam(
        String paramName,
        Set<String> strSet,
        Boolean required
    ) {
        if (
            (required == true) &&
            (
                (strSet == null) ||
                strSet.isEmpty()
            )
        ) {
            throw SPInvalidParamException.getNewForList(paramName);
        }
    }

    public static void checkRequiredResult(
        List<SObject> recordList,
        String entityName,
        String condition
    ){
        if (
            (recordList == null) ||
            recordList.isEmpty()
        ) {
            throw SPNoDataFoundException.getNew(
                entityName, 
                condition
            );
        }
    }

    public static void checkRequiredResult(
        List<SObject> recordList,
        String entityName,
        String condition,
        Boolean required
    ){
        if (
            (required == true) &&
            (
                (recordList == null) ||
                recordList.isEmpty()
            )
        ) {
            throw SPNoDataFoundException.getNew(
                entityName, 
                condition
            );
        }
    }

}