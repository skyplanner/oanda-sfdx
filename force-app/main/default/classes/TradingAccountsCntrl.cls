/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-12-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public with sharing class TradingAccountsCntrl {
    public TradingAccountsCntrl() {
    }

    @AuraEnabled(cacheable=true)
    public static List<TradingAccount> fetchFields() {
        List<TradingAccount> fields = new List<TradingAccount>();

        for (Schema.FieldSetMember fs : SObjectType.fxAccount__c.FieldSets.fxAccount_Related_List.getFields()) {
            TradingAccount ta = new TradingAccount();
            ta.apiName = fs.sobjectfield.getDescribe().getName();
            ta.label = fs.label;
            ta.fieldPath = fs.fieldPath;
            fields.add(ta);
        }

        return fields;
    }

    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchAccounts(Id recordId, List<TradingAccount> fields) {
        String objApiName = SObjectUtils.getObjectNameById(recordId);
        List<String> fieldsNames = getFieldNames(fields);
        String q = getQuery(recordId, objApiName, fieldsNames).replace('"', '\'');
        return Database.query(q);
    }

    private static List<String> getFieldNames(List<TradingAccount> fields) {
        List<String> fieldNames = new List<String>();

        for (TradingAccount ta : fields) {
            fieldNames.add(ta.apiName);
        }

        return fieldNames;
    }

    private static String getQuery(Id recordId, String objApiName, List<String> fieldsNames) {
        String objName = objApiName == SObjectType.Account.getName() 
            ? 'fxAccount__c'
            : (objApiName == SObjectType.fxAccount__c.getName() 
                ? 'Trading_Account__c'
                : null);
        String whereClause = objApiName == SObjectType.Account.getName() 
            ? 'Account__c'
            : (objApiName == SObjectType.fxAccount__c.getName() 
                ? 'fxAccount__c'
                : null);

        return String.format(
            'SELECT Id, {0} FROM {1} WHERE {2} = "{3}"',
            new List<String>{
                String.join(fieldsNames, ', '),
                objName,
                whereClause,
                recordId
            }
        );
    }

    public class TradingAccount {
        // @AuraEnabled
        // public String Id { get; set; }

        @AuraEnabled
        public String apiName { get; set; }

        @AuraEnabled
        public String label { get; set; }

        @AuraEnabled
        public String fieldPath { get; set; }
        
        public TradingAccount() {
        }
    }
}