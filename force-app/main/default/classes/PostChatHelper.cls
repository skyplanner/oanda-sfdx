/**
 * @File Name          : PostChatHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/23/2021, 11:16:55 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/22/2021, 9:45:09 PM   acantero     Initial Version
**/
public without sharing class PostChatHelper {

    public static final String CHAT_DETAILS = 'chatDetails';
    public static final String LANGUAGE_PREFERENCE = 'LanguagePreference';

    public String language {get; private set;}

    ChatDetails details;

    public PostChatHelper(Map<String,String> params) {
        language = ChatConst.DEFAULT_LANGUAGE;
        String chatDetailsJson = params.get(CHAT_DETAILS);
        if (String.isNotBlank(chatDetailsJson)) {
            processChatDetails(chatDetailsJson);
        }
    }

    void processChatDetails(String chatDetailsJson) {
        details = (ChatDetails)JSON.deserialize(
            chatDetailsJson, 
            ChatDetails.class
        );
        udpateLanguage();
    }

    void udpateLanguage() {
		if (details.customDetails != null) {
            for (CustomDetail cd : details.customDetails) {
                if (cd.label == LANGUAGE_PREFERENCE) {
					language = cd.value;
					return;
				}
            }
        }
	}

    //***************************************************************** */

    public class CustomDetail {
		public String value { get; set; }
		public List<String> entityMaps { get; set; }
		public Boolean displayToAgent { get; set; }
		public String label { get; set; }
		public List<String> transcriptFields { get; set; }
	}

	public class ChatDetails {
		public String visitorId { get; set; }
		public List<CustomDetail> customDetails { get; set; }
	}

}