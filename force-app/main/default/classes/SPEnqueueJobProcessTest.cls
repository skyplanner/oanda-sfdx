/**
 * @File Name          : SPEnqueueJobProcessTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2023, 1:53:12 AM
**/
@IsTest
private without sharing class SPEnqueueJobProcessTest {

	@TestSetup
	static void setup() {
		// Log__c is used by SPDoNothingBatch
		insert new Log__c(
			Id__c = 'abcd'
		);
	}

	@IsTest
	static void enqueueIfNoTesting() {
		Test.startTest();
		SPEnqueueJobProcess instance = new SPEnqueueJobProcess(
			new SPDoNothingJob() // job
		);
		Boolean result = instance.enqueueIfNoTesting();
		Test.stopTest();
		Assert.isFalse(
			result, 
			'In a test context the job should not be executed'
		);
	}

	@IsTest
	static void enqueueIfCondition() {
		Test.startTest();
		SPEnqueueJobProcess instance = new SPEnqueueJobProcess(
			new SPDoNothingJob() // job
		);
		Boolean result = instance.enqueueIfCondition(
			true // condition
		);
		Test.stopTest();
		Assert.isTrue(
			result, 
			'If the condition is met, the job must be executed'
		);
	}

	@IsTest
	static void safeEnqueueJob() {
		Test.startTest();
		SPEnqueueJobProcess instance1 = new SPEnqueueJobProcess(
			new SPDoNothingJob() // job
		);
		SPEnqueueJobProcess instance2 = new SPEnqueueJobProcess(
			new SPDoNothingJob() // job
		);
		String jobStringKey = SPDoNothingJob.class.getName();
		Boolean result1 = instance1.safeEnqueueJob(jobStringKey);
		Boolean result2 = instance2.safeEnqueueJob(jobStringKey);
		Test.stopTest();

		Assert.isTrue(
			result1, 
			'The first job should be queued correctly'
		);
		Assert.isFalse(
			result2, 
			'The second Job should not be able to be queued'
		);
	}
	
}