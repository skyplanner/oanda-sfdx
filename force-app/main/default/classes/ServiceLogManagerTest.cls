/**
 * @File Name          : ServiceLogManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 4:35:02 PM
**/
@IsTest
private without sharing class ServiceLogManagerTest {

	/**
	 * test log method that include details param
	 * 
	 * test 1 : log using setting
	 * 
	 * test 2 : enabledForAllCategories = true, details = null
	 * 
	 * test 3 : enabledForAllCategories = false
	 * enabledCategories = ['category1'], category = 'category1'
	 * 
	 * test 4 :enabledForAllCategories = false
	 * enabledCategories = ['category1'], category = 'category2'
	 */
	@IsTest
	static void log1() {
		Contact fakeContact = new Contact(
			LastName = 'Smith'
		);
		Contact nullContact = null;

		Test.startTest();
		ServiceLogManager instance = ServiceLogManager.getInstance();
		// test 1
		Boolean result1 = instance.log(
			'msg1', // msg
			'category1', // category
			fakeContact // details
		);
		// test 2 => return true
		instance.enabledForAllCategories = true;
		Boolean result2 = instance.log(
			'msg1', // msg
			'category1', // category
			nullContact // details
		);
		// test 3 => return true
		instance.enabledForAllCategories = false;
		instance.enabledCategories = new List<String>{ 'category1' };
		Boolean result3 = instance.log(
			'msg1', // msg
			'category1', // category
			fakeContact // details
		);
		// test 4 => return false
		instance.enabledForAllCategories = false;
		instance.enabledCategories = new List<String>{ 'category1' };
		Boolean result4 = instance.log(
			'msg1', // msg
			'category2', // category
			fakeContact // details
		);
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isTrue(result3, 'Invalid result');
		Assert.isFalse(result4, 'Invalid result');
	}

	/**
	 * test log method that do not include details param
	 * 
	 * test 1 : log using setting
	 * 
	 * test 2 : enabledForAllCategories = true
	 * 
	 * test 3 : enabledForAllCategories = false
	 * enabledCategories = ['category1'], category = 'category1'
	 * 
	 * test 4 :enabledForAllCategories = false
	 * enabledCategories = ['category1'], category = 'category2'
	 */
	@IsTest
	static void log2() {
		Test.startTest();
		ServiceLogManager instance = ServiceLogManager.getInstance();
		// test 1
		Boolean result1 = instance.log(
			'msg1', // msg
			'category1' // category
		);
		// test 2 => return true
		instance.enabledForAllCategories = true;
		Boolean result2 = instance.log(
			'msg1', // msg
			'category1' // category
		);
		// test 3 => return true
		instance.enabledForAllCategories = false;
		instance.enabledCategories = new List<String>{ 'category1' };
		Boolean result3 = instance.log(
			'msg1', // msg
			'category1' // category
		);
		// test 4 => return false
		instance.enabledForAllCategories = false;
		instance.enabledCategories = new List<String>{ 'category1' };
		Boolean result4 = instance.log(
			'msg1', // msg
			'category2' // category
		);
		Test.stopTest();
		
		Assert.isNotNull(result1, 'Invalid result');
		Assert.isTrue(result2, 'Invalid result');
		Assert.isTrue(result3, 'Invalid result');
		Assert.isFalse(result4, 'Invalid result');
	}


	/**
	 * Test 1 : currentLogProfile = NONE
	 * Test 2 : currentLogProfile = ALL
	 * Test 3 : currentLogProfile = TEST
	 */
	@IsTest
	static void init() {
		Test.startTest();

		// Test 1 -> return false
		ServiceLogManager instance1 = new ServiceLogManager();
		String currentLogProfile1 = ServiceLogManager.NONE;
		Boolean result1 = instance1.init(currentLogProfile1);

		// Test 2 -> return true
		ServiceLogManager instance2 = new ServiceLogManager();
		String currentLogProfile2 = ServiceLogManager.ALL;
		Boolean result2 = instance2.init(currentLogProfile2);

		// Test 3 -> return true
		ServiceLogManager instance3 = new ServiceLogManager();
		String currentLogProfile3 = 'TEST';
		Boolean result3 = instance3.init(currentLogProfile3);

		Test.stopTest();
		
		// Test 1
		Assert.isFalse(result1, 'Invalid result');
		Assert.isFalse(instance1.enabledForAllCategories, 'Invalid value');
		Assert.areEqual(0, instance1.enabledCategories.size());
		// Test 2
		Assert.isTrue(result2, 'Invalid result');
		Assert.isTrue(instance2.enabledForAllCategories, 'Invalid value');
		Assert.areEqual(0, instance2.enabledCategories.size());
		// Test 3
		Assert.isTrue(result3, 'Invalid result');
		Assert.isFalse(instance3.enabledForAllCategories, 'Invalid value');
		Assert.areNotEqual(0, instance3.enabledCategories.size());
	}
	
}