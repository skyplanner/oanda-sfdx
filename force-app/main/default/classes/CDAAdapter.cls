/*
 *  Provides a single point of contact for Apex classes interacting with Client Data API
*/

public with sharing class CDAAdapter
{
	
	public static final String STATUS_SUCCESS = 'SUCCESS';
	public static final String STATUS_FAILURE = 'FAILURE';
	
	private static String statusString(Boolean isSuccess)
	{
		return isSuccess ? STATUS_SUCCESS : STATUS_FAILURE;
	}
	
    public static String serialize(Boolean isSuccess, String message)
    {
		Map<String, String> result = new Map<String, String>();
	  
		result.put('result', statusString(isSuccess));
		result.put('message', message);
	  
		return JSON.serialize(result); 	
    }
}