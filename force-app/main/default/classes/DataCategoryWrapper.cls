/**
 * @author Ariel Cantero
 * @version 1.0
 * @since 7/1/2019
 */
global class DataCategoryWrapper {

		@AuraEnabled
		global String name {get; set;}
		@AuraEnabled
		global String label {get; set;}
		@AuraEnabled
		global List<TopArticleWrapper> topArticles {get; set;}

		global DataCategoryWrapper(String name, String label) {
			this.name = name;
			this.label = label;
			this.topArticles = new List<TopArticleWrapper>();
		}

		global void addArticle(TopArticleWrapper article) {
			if (topArticles.isEmpty()) {
				topArticles.add(article);
				return;
			}
			//else...
			Integer size = topArticles.size();
			for(Integer i = 0; i < size; i++) {
				if (article.order < topArticles[i].order) {
					topArticles.add(i, article);
					return;
				}
			}
			//else...
			topArticles.add(article);
		}

	}