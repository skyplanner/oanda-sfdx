/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-22-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class ComplianceAlertEmailsMDT {
    private static ComplianceAlertEmailsMDT instance = null;
    private List<Compliance_Alert_Email__mdt> mdtList;
    
    private ComplianceAlertEmailsMDT() {
        mdtList = [
            SELECT Id, Email__c, Type__c, Active__c
            FROM Compliance_Alert_Email__mdt
        ];
    }
    
    public static ComplianceAlertEmailsMDT getInstance() {
        if (instance == null) {
            instance = new ComplianceAlertEmailsMDT();
        }
        return instance;
    }

    public List<String> getAddresses(String type, Boolean active) {
        List<String> addresses = new List<String>();

        if (Test.isRunningTest()) {
            addFakeData();
        }
        
        if (mdtList.size() > 0) {
            for (Compliance_Alert_Email__mdt mdt : mdtList) {
                if (mdt.Active__c == active && mdt.Type__c == type) {
                    addresses.add(mdt.Email__c);
                }
            }
        }

        return addresses;
    }

    private void addFakeData() {
        mdtList.add(new Compliance_Alert_Email__mdt(
            DeveloperName = 'test_email',
            Email__c = 'compliancealertemail@test.com',
            Active__c = true,
            Type__c = 'to'
        ));
        mdtList.add(new Compliance_Alert_Email__mdt(
            DeveloperName = 'test_emailcc',
            Email__c = 'compliancealertemailcc@test.com',
            Active__c = true,
            Type__c = 'cc'
        ));
    }
}