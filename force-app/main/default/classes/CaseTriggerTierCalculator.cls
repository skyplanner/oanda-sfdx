/**
 * Created by akajda on 09/05/2024.
 *
 * Conditions for Tier calculation:
 * Tier 1: SEG_PL= 'high+' OR SEG_PL= 'high' OR OGM HVC clients
 * Tier 2: SEG_PL='mid' OR OGM non HVC clients
 * Tier 3: SEG_PL='low'
 * Tier 4: Practice Clients OR Unassigned (neither Live nor Practice) - Leads with no fxAccount OR
 * non -OGM+SEG_PL=blank OR SEG_PL='inactive'
 */

public inherited sharing class CaseTriggerTierCalculator {

    public static void calculateTierForTelephony(List<Case> newCases){

        Set<Id> accIds = new Set<Id>();
        for(Case c : newCases){
            if(c.Origin=='Phone'){
                if(String.isNotBlank(c.AccountId)) accIds.add(c.AccountId);
            }
        }
        Map<Id, Account> accs = new Map<Id, Account>([SELECT Id, fxAccount__c, fxAccount__r.Seg_PL_F__c,
                fxAccount__r.Is_HVC__c, fxAccount__r.Division_Name__c FROM Account WHERE Id IN :accIds]);

        for(Case c : newCases){
            if(c.Origin=='Phone'){
                //cases may have only AccountId or Lead__c populated
                Account account = (c.AccountId!=null && accs.containsKey(c.AccountId))? accs.get(c.AccountId):null;
                //go to the next iteration if both fields are empty or there is no fxAccount on Account
                if((account==null && c.Lead__c==null) || (account!=null && account.fxAccount__c==null)) continue;

                if((account==null && c.Lead__c!=null) || (account!=null && (
                        account.fxAccount__r.Seg_PL_F__c==OmnichannelCustomerConst.SEGMENTATION_INACTIVE ||
                        (account.fxAccount__r.Division_Name__c != Constants.DIVISIONS_OANDA_GLOBAL_MARKETS  &&
                                (account.fxAccount__r.Seg_PL_F__c==OmnichannelCustomerConst.SEGMENTATION_BLANK ||
                                String.isBlank(account.fxAccount__r.Seg_PL_F__c)))))){
                    c.Tier__c = OmnichannelCustomerConst.TIER_4;
                } else if (account.fxAccount__r.Seg_PL_F__c==OmnichannelCustomerConst.SEGMENTATION_HIGH ||
                        account.fxAccount__r.Seg_PL_F__c== OmnichannelCustomerConst.SEGMENTATION_HIGH_PLUS ||
                        (account.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS && account.fxAccount__r.Is_HVC__c)){
                    c.Tier__c = OmnichannelCustomerConst.TIER_1;
                } else if (account.fxAccount__r.Seg_PL_F__c==OmnichannelCustomerConst.SEGMENTATION_MID ||
                        account.fxAccount__r.Division_Name__c==Constants.DIVISIONS_OANDA_GLOBAL_MARKETS) {
                    c.Tier__c = OmnichannelCustomerConst.TIER_2;
                } else if (account.fxAccount__r.Seg_PL_F__c==OmnichannelCustomerConst.SEGMENTATION_LOW) {
                    c.Tier__c = OmnichannelCustomerConst.TIER_3;
                }
            }
        }
    }
}