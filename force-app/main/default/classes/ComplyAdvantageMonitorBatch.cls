/**
 * SP-SP-7630.
 * Syncs the Is_Monitored flag from all specified Comply Advantage Searches
 * with the Comply Advantage portal. Aafter successfull execution, the
 * Is_Monitored flag in Salesforce and the Monitored? flag in Comply Advantage
 * will have the same value.
 * @author Fernando Gomez
 */
public without sharing class ComplyAdvantageMonitorBatch
		implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection,
			Database.AllowsCallouts,
			Database.Stateful {
				
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private List<String> searchIds;

	/**
	 * @param searchIds
	 */
	public ComplyAdvantageMonitorBatch(List<String> searchIds) {
		this.searchIds = searchIds;
		query=			'SELECT Id, ' +
		'Search_Id__c, ' +
		'Is_Monitored__c, ' +
		'fxAccount__c, ' +
		'fxAccount__r.Division_Name__c, ' +
		'fxAccount__r.Country__c, ' +
		'Affiliate__c, ' +
		'Affiliate__r.Division_Name__c, ' +
		'Affiliate__r.Country__c ' +
		'FROM Comply_Advantage_Search__c ' +
		'WHERE Search_Id__c IN :searchIds ';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query=			'SELECT Id, ' +
		'Search_Id__c, ' +
		'Is_Monitored__c, ' +
		'fxAccount__c, ' +
		'fxAccount__r.Division_Name__c, ' +
		'fxAccount__r.Country__c, ' +
		'Affiliate__c, ' +
		'Affiliate__r.Division_Name__c, ' +
		'Affiliate__r.Country__c ' +
		'FROM Comply_Advantage_Search__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	

	/**
	 * @param BC
	 */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
	 * @param BC
	 * @param csSearches
	 */
	public void execute(Database.BatchableContext BC, List<sObject> scope) {
		processBatch((List<Comply_Advantage_Search__c>)scope);
	}

	/**
	 * Performs the same action as in execute but 
	 * outside of a batch scenario.
	 * @param csSearches
	 */
	public void executeSync(List<Comply_Advantage_Search__c> caSearches) {
		processBatch(caSearches);
	}

	/**
	 * @param BC
	 */
	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

	/**
	 * Allow in-thread execution of the same functionality
	 * @param caSearche
	 */
	private void processBatch(List<Comply_Advantage_Search__c> caSearches) {
		ComplyAdvantageSearch.ComplyAdvantageSettings complyAdvantageSetting;
		ComplyAdvantageMonitorOff caMonitor;

		for (Comply_Advantage_Search__c caSearch : caSearches) {
			// ca settings
			complyAdvantageSetting = getSettings(caSearch);
					
			if (complyAdvantageSetting != null &&
					complyAdvantageSetting.apiKey != null) {
				caMonitor =
					new ComplyAdvantageMonitorOff(
						caSearch.Search_Id__c,
						complyAdvantageSetting.apiKey,
						caSearch.Is_Monitored__c);
				caMonitor.execute(null);
			}
		}
	}

	/**
	 * @return the settings based on division and country
	 * either from the fxAccount of the affilliate
	 */
	private ComplyAdvantageSearch.ComplyAdvantageSettings getSettings(
			Comply_Advantage_Search__c caSearch) {
		// fxAccount
		if (String.isNotBlank(caSearch.fxAccount__c) &&
				String.isNotBlank(caSearch.fxAccount__r.Division_Name__c))
			return ComplyAdvantageSearch.getcomplyAdvantageSettingByDivision(
				caSearch.fxAccount__r.Division_Name__c,
				caSearch.fxAccount__r.Country__c);
		// Affilliate
		else if (String.isNotBlank(caSearch.Affiliate__c) &&
				String.isNotBlank(caSearch.Affiliate__r.Division_Name__c))
			return ComplyAdvantageSearch.getcomplyAdvantageSettingByDivision(
				caSearch.Affiliate__r.Division_Name__c,
				caSearch.Affiliate__r.Country__c);
		// if none of the above, we need an exception
		else
			throw LogException.newInstance(
				'Division name cannot be resolved therefore Comply ' +
				'Advantage settings cannot be obtained.',
				Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY);
	}

}