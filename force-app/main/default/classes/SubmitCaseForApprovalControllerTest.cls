@IsTest public with sharing class SubmitCaseForApprovalControllerTest {
    @IsTest
    static void getCaseDetailsTest() {

        List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

        fxAccount__c fxAcc = new fxAccount__c(
                Account_Email__c = 'testingBatch1@oanda.com',
                Funnel_Stage__c = FunnelStatus.TRADED,
                RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                Division_Name__c = 'OANDA Canada',
                Birthdate__c = Date.newInstance(1980, 11, 21),
                Citizenship_Nationality__c = 'Canada',
                Account__c = accs[0].Id,
                Is_Closed__c = false
        );
        insert fxAcc;

        Case c = new Case(
                Subject = 'Test',
                fxAccount__c = fxAcc.Id,
                RecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId()
        );
        insert c;


        String result = SubmitCaseForApprovalController.getCaseDetails(c.Id);

        fxAcc.Has_ComplyAdvantage_Case__c=false;
        update fxAcc;

        String result2 = SubmitCaseForApprovalController.getCaseDetails(c.Id);
        System.assertEquals('A comply advantage search has not been run for this application.', result2);
        System.assertEquals('ok', result);
    }

    @IsTest
    static void submitApprovalTest() {
        User tUser = new User(
                Alias = 'testuser',
                Email = 'testuser@testemail.com',
                LastName = 'Testing',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Los_Angeles',
                ProfileId = UserUtil.getOBAllSecProfileId(),
                Username = 'testusername01@testemail.com');
        insert tUser;

        String result ='';

        System.runAs(tUser) {
            List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

            fxAccount__c fxAcc = new fxAccount__c(
                    Account_Email__c = 'testingBatch1@oanda.com',
                    Funnel_Stage__c = FunnelStatus.TRADED,
                    RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
                    Division_Name__c = 'OANDA Canada',
                    Birthdate__c = Date.newInstance(1980, 11, 21),
                    Citizenship_Nationality__c = 'Canada',
                    Account__c = accs[0].Id,
                    Is_Closed__c = false,
                    Is_CA_Name_Search_Monitored__c = true
            );
            insert fxAcc;
            Case c = new Case(
                    Subject = 'Test',
                    fxAccount__c = fxAcc.Id,
                    RecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId()
            );
            insert c;
            result = SubmitCaseForApprovalController.submitApproval(c.Id, 'comments');
        }
    }

}