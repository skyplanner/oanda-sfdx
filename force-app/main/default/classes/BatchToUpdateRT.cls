/* Name: BatchToUpdateRT
 * Description : Batch Class to update Risk Tolerance
 * Author: Eugene
* */
public class BatchToUpdateRT implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Schedulable {

    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    String email;
    public static final Integer BATCH_SIZE = 50;
    Id toUserId;
    Id fromUserId;
    List<String> successfulfxAccountIds = new List<String>();
    List<String> failedfxAccountIds = new List<String>();
    integer totalSizeRecords=0;

    public BatchToUpdateRT(String q) {
        query = q;
    }
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Age__c, Division_Name__c, Net_Worth_Range_End__c, Liquid_Net_Worth_End__c, Birthdate__c, Annual_Income__c, Risk_Tolerance_Amount__c, Forex_Experience_Years__c, Batch_Processed__c, Forex_Experience_Years__c, fxTrade_Created_Date__c FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id, Age__c, Division_Name__c, Net_Worth_Range_End__c, Liquid_Net_Worth_End__c, Birthdate__c, Annual_Income__c, Risk_Tolerance_Amount__c, Forex_Experience_Years__c, Batch_Processed__c, Forex_Experience_Years__c, fxTrade_Created_Date__c FROM fxAccount__c WHERE Division_Name__c = \'OANDA Canada\' AND RecordType.Name = \'Retail Live\' AND Calculated_Risk_Tolerance__c != null AND Batch_Processed__c = False ORDER BY fxTrade_Created_Date__c LIMIT 500';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<fxAccount__c> scope) {
        totalSizeRecords+=scope.size();
        
        List<String> fxAccountId = new List<String>();
        for(fxAccount__c fxa: scope) {
            fxAccountUtil.calculateRiskTolerance(fxa);
        }
        try{
            Database.SaveResult[] srList = Database.update(scope,false);
            for(Database.SaveResult sr: srList) {
                if(sr.isSuccess()){
                    successfulfxAccountIds.add(sr.getId());
                }
            }
            for(fxAccount__c i : scope) {
                if(!successfulfxAccountIds.contains(i.Id)){
                    failedfxAccountIds.add(i+',');
                }
            }
        }catch(Exception e) {
        }
    }

    public void execute(SchedulableContext SC) {
		Database.executebatch(new BatchToUpdateRT(query), BATCH_SIZE);
	}
    
    public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }
}