/**
 * Check the Documents looking for those that are already obsolete 
 * (they are not used by any article) and should be eliminated
 * @author Ariel Cantero, Skyplanner LLC
 * @since 07/10/2019
 */
global without sharing class BatchDocumentsCleaner implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {

	global static final Integer BATCH_EXECUTION_SCOPE = 200;

	private String folderId;
	private Boolean onlyTwoWeeksRange;
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	global Boolean isValid {get; private set;}

	global BatchDocumentsCleaner(Boolean onlyTwoWeeksRange) {
		folderId = DocumentManager.getFolderId();
		isValid = String.isNotBlank(folderId);
		this.onlyTwoWeeksRange = onlyTwoWeeksRange;
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM Document WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('BatchDocumentsCleaner -> start');
		return Database.getQueryLocator(DocumentsCleanerHelper.getUselessDocsCheckingQuery(folderId, onlyTwoWeeksRange));
	}

	global void execute(Database.BatchableContext BC, List<SObject> data) {
		System.debug('BatchDocumentsCleaner -> execute');
		List<Document> docList = (List<Document>) data;
		DocumentsCleanerHelper.findAndRemoveUselessDocs(docList);
	}

	global void finish(Database.BatchableContext BC) {
		System.debug('BatchDocumentsCleaner -> finish');
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

	

}