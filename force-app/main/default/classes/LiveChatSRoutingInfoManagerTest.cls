/**
 * @File Name          : LiveChatSRoutingInfoManagerTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/29/2024, 2:41:47 PM
**/
@IsTest
private without sharing class LiveChatSRoutingInfoManagerTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createLiveChatVisitor1(initManager);
		ServiceTestDataFactory.createLiveChatTranscript1(initManager);
		initManager.storeData();
	}

	// PendingServiceRouting registered
	@IsTest
	static void getRoutingInfoList1() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID liveChatTranscript1Id = initManager.getObjectId(
			ServiceTestDataKeys.LIVE_CHAT_TRANSCRIPT_1, 
			true
		);
	
		Test.startTest();
		LiveChatSRoutingInfoManager instance = 
			new LiveChatSRoutingInfoManager();
		instance.registerPendingServiceRouting(
			new PendingServiceRouting(
				WorkItemId = liveChatTranscript1Id
			)
		);
		List<BaseServiceRoutingInfo> result = instance.getRoutingInfoList();
		Test.stopTest();
		
		Assert.areEqual(
			liveChatTranscript1Id, 
			result[0].workItem.Id, 
			'Invalid result'
		);
	}

	/**
	 * test 1 
	 * initial tier = TIER_4, tierHasChanged = true
	 * 
	 * test 2
	 * initial tier = TIER_4, tierHasChanged = false
	 */
	@IsTest
	static void getRecordsToUpdate() {
		LiveChatSRoutingInfoManager instance = 
			new LiveChatSRoutingInfoManager();
		ServiceRoutingInfo routingInfo = 
			(ServiceRoutingInfo) instance.getRoutingInfo(
				new PendingServiceRouting(), // routingObj
				new LiveChatTranscript() // workItem
			);

		Test.startTest();
		// test 1 => result is not empty
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);
		routingInfo.updateTier(OmnichannelCustomerConst.TIER_3);
		List<SObject> result1 = instance.getRecordsToUpdate(routingInfo);
		// test 2 => result is empty
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);
		List<SObject> result2 = instance.getRecordsToUpdate(routingInfo);
		Test.stopTest();

		Assert.isFalse(result1.isEmpty(), 'Invalid result');
		Assert.isTrue(result2.isEmpty(), 'Invalid result');
	}

	@IsTest
	static void getRoutingLog() {
		ServiceRoutingInfo routingInfo = new ServiceRoutingInfo(
			new PendingServiceRouting(
				RoutingPriority = 1
			), // psrObj
			ServiceRoutingInfo.Source.CHAT, // infoSource
			new LiveChatTranscript() // workItem
		);
		routingInfo.initTier(OmnichannelCustomerConst.TIER_4);

		Test.startTest();
		LiveChatSRoutingInfoManager instance = 
			new LiveChatSRoutingInfoManager();
		SObject result = instance.getRoutingLog(routingInfo);
		Test.stopTest();
		
		Assert.isNotNull(result, 'Invalid result');
	}
	
}