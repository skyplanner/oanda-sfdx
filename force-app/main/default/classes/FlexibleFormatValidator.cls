/**
 * @File Name          : FlexibleFormatValidator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/9/2020, 3:25:17 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   acantero     Initial Version
**/
public abstract class FlexibleFormatValidator implements ExpressionValidator{

    public final Integer lettersCount;
    public final Integer digitsCount;
    
    public FlexibleFormatValidator(
        Integer lettersCount,
        Integer digitsCount
    ) {
        this.lettersCount = lettersCount;
        this.digitsCount = digitsCount;
    }

    public String validate(String val) {
        Boolean result = SPValidationUtils.checkFlexibleFormat(
            val, 
            lettersCount, 
            digitsCount
        );
        if (result == false) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        return null;
    }

}