/**
 * @File Name          : IncrementValueActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/19/2023, 9:40:46 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 9:40:46 AM   aniubo     Initial Version
 **/
@isTest
private class IncrementValueActionTest {
	@isTest
	private static void testValueListIsNullOrEmpty() {
		// Test data setup
		List<Decimal> sameValues;
		// Actual test
		Test.startTest();

		sameValues = IncrementValueAction.incrementValue(null);
		Assert.areEqual(0, sameValues.get(0), 'The first value should be 0');

		sameValues = null;

		sameValues = IncrementValueAction.incrementValue(new List<Decimal>());
		Assert.areEqual(0, sameValues.get(0), 'The first value should be 0');
		Test.stopTest();
		// Asserts
	}

	@isTest
	private static void testIncrementValueEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			IncrementValueAction.incrementValue(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testTestCase() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<Decimal> sameValues = IncrementValueAction.incrementValue(
			new List<Decimal>{ 0 }
		);
		Assert.areEqual(1, sameValues.get(0), 'The first value should be 1');
		sameValues = null;
		sameValues = IncrementValueAction.incrementValue(
			new List<Decimal>{ 34 }
		);
		Assert.areEqual(35, sameValues.get(0), 'The first value should be 35');
		Test.stopTest();

		// Asserts
	}
}