/**
 * @File Name          : MessagingSessionUpdateManager.cls
 * @Description        : Process updates for multiple fields of a 
 *                       MessagingSession record
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/29/2023, 12:06:24 AM
**/
public inherited sharing class MessagingSessionUpdateManager 
	extends SObjectFieldsUpdateManager {

	public static final String INQUIRY_NATURE_FIELD = 'Inquiry_Nature__c';
	public static final String CASE_TYPE_FIELD = 'Case_Type__c';
	public static final String CASE_SUBTYPE_FIELD = 'Case_Subtype__c';

	public MessagingSessionUpdateManager() {
		super();
	}

	public override SObject getRecordToUpdate() {
		return new MessagingSession();
	}

	/**
	 * The fields that are dependent picklists are checked and, 
	 * if necessary, the values of the "child" fields are cleaned
	 */
	protected override Map<String, Object> getFieldValueByFieldNames(
		FieldValueSource source
	) {
		Map<String, Object> result = new Map<String, Object>();
		Map<String, Object> originalFieldsValueMap = super.getFieldValueByFieldNames(source);

		if (originalFieldsValueMap.containsKey(INQUIRY_NATURE_FIELD)) {
			result.put(CASE_TYPE_FIELD, null);
			result.put(CASE_SUBTYPE_FIELD, null);
			
		} else if (originalFieldsValueMap.containsKey(CASE_TYPE_FIELD)) {
			result.put(CASE_SUBTYPE_FIELD, null);
		}

		for (String fieldName : originalFieldsValueMap.keySet()) {
			result.put(fieldName, originalFieldsValueMap.get(fieldName));
		}
		
		return result;
	}

}