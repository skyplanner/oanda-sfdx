/**
 * 
 */
public class CurrentDeskUpdate
		extends Callout
		implements Queueable, Database.AllowsCallouts {
	private CurrentDeskSync.LiveUser liveUser;
	private List<String> errorsToReport;

	public CurrentDeskUpdate(CurrentDeskSync.LiveUser userInfo) {
		liveUser = userInfo;
		errorsToReport = new List<String>();
	} 

	public void execute(QueueableContext context) {
		Logger.info(
			'Start Current Desk Update - liveUser',
			Constants.LOGGER_CURRENT_DESK_CATEGORY,
			liveUser);
			
		try {
			// SP-9238, SP-9239
			// we update individual details if account
			// is not red only or loced
			if (liveUser.updateIndividual) {
				Logger.info(
					'Updating Individual',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				updateIndividualDetails();
			} 

			// update Customer Details callout
			if (liveUser.updateClient) {
				Logger.info(
					'Updating Customer',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				updateCustomerDetails();
			} 
				
			// SP-9304
			// user might need to be archived
			if (liveUser.updateArchived) {
				Logger.info(
					'Archiving User',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				archiveUser();
			} 
			
			// SP-9304
			// user access might need to be modified
			if (liveUser.updateAccess) {
				Logger.info(
					'Updating User Access',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				updateUserAccess();
			} 
				
			if (liveUser.updateFlag) {
				// update Flags Details callout
				
				Logger.info(
					'Updating Flag',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				updateFlag();

				// retrieveMT4Logins callout
				if (liveUser.reducePositionOnly) {
					Logger.info(
						'Retrieving MT4 Login',
						Constants.LOGGER_CURRENT_DESK_CATEGORY);
					retrieveMT4Logins();
				}
			}

			// SP-12265
			// update to make a client close only for US Share CFD
			if (liveUser.updateTradingAccountInfo) {
				Logger.info(
					'Updating Trading Account Info',
					Constants.LOGGER_CURRENT_DESK_CATEGORY);
				updateTradingAccountInfo();
			}
		} 
		catch(Exception e) {
			Logger.exception(
				Constants.LOGGER_CURRENT_DESK_CATEGORY,
				e);
		}

		// errors are sent at the end
		sendErrorEmail();
    }
    
    private void updateCustomerDetails()
    {
        Http http = new Http();
        HttpRequest httpRequest = getClientDetailUpdateRequest();
        HttpResponse response = http.send(httpRequest);
        
        if (response.getStatusCode() != 200)
        {
            http = new Http();
            response = http.send(httpRequest);
        }      
        if(response.getStatusCode() != 200)
        {
			addError('update Customer Details',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
        }
    }

	/**
	 * SP-9238
	 * account-approved-status=true IF the “Funding limited” flag 
	 * is unchecked for an account that’s already created in CurrentDesk
	 */
	private void updateIndividualDetails() {
		Http http = new Http();
		HttpRequest httpRequest = getIndividualDetailUpdateRequest();
		HttpResponse response = http.send(httpRequest);
		
		// we launch a second attempt if it fails
		if (response.getStatusCode() != 200) {
			http = new Http();
			response = http.send(httpRequest);
		}
		
		if (response.getStatusCode() != 200)
			addError('update Individual Details',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
    }
    
    private void updateFlag()
    {
        Http http = new Http();
        HttpRequest httpRequest = getUpdateFlagsRequest();
        HttpResponse response = http.send(httpRequest);
        
        if(response.getStatusCode() != 200)
        {
            http = new Http();
            response = http.send(httpRequest);
        }
        
        if(response.getStatusCode() != 200)
        {
            addError('update Flag',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
        }
    }
	
	private void updateTradingAccountInfo()
	{
		Http http = new Http();
        HttpRequest httpRequest = getUpdateTradingAccountInfo();
        HttpResponse response = http.send(httpRequest);
        
        if(response.getStatusCode() != 200)
        {
            http = new Http();
            response = http.send(httpRequest);
        }
        
        if(response.getStatusCode() != 200)
        {
            addError('update Trading Account Info',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
        }
	}
    
    private void retrieveMT4Logins()
    {
        Http http = new Http();
        HttpRequest httpRequest = getMT4LoginsRequest();
        HttpResponse response = http.send(httpRequest);
 
        if(response.getStatusCode() != 200)
        {
            http = new Http();
            response = http.send(httpRequest);
        }
        
        if(response.getStatusCode() == 200)
        {
            if(string.isNotBlank(response.getBody()))
            {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                if(results.get('Logins') != null)
                {
                    fxAccount__c fxa = new fxAccount__c(Id = liveUser.fxAccountId);
                    fxa.MT4_Logins__c = (string) results.get('Logins');
                    update fxa;
                }
            }  
        }
        else
        {
            addError('Retrieve MT4 Logins',
				httpRequest.getEndpoint(),
				'',
				response.getBody());
        }
    }

	/**
	 * SP-9304
	 */
	private void archiveUser() {
		Http http = new Http();
		HttpRequest httpRequest = getArchiveUserRequest();
		HttpResponse response = http.send(httpRequest);
		
		// we launch a second attempt if it fails
		if (response.getStatusCode() != 200) {
			http = new Http();
			response = http.send(httpRequest);
		}
		
		if (response.getStatusCode() != 200)
			addError('archive User',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
	}
	
	/**
	 * SP-9304, PUT /misc/partner/useraccess
	 */
	private void updateUserAccess() {
		Http http = new Http();
		HttpRequest httpRequest = getUserAccessRequest();
		HttpResponse response = http.send(httpRequest);
		
		// we launch a second attempt if it fails
		if (response.getStatusCode() != 200) {
			http = new Http();
			response = http.send(httpRequest);
		}
		
		if (response.getStatusCode() != 200)
			addError('update User Access',
				httpRequest.getEndpoint(),
				httpRequest.getBody(),
				response.getBody());
	}
    
    private HttpRequest getClientDetailUpdateRequest() 
    { 
        string clientId =string.valueOf(liveUser.CurrentDeskClientId);
        string requestBody = getClientDetailUpdateRequestBody();

        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/updateclient?id=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setHeader('id',clientId);
        httpRequest.setHeader('clientUpdateModel', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setTimeout(120000);

        httpRequest.setBody(requestBody);
        return httpRequest;
    }
	
	/**
	 * SP-9238
	 * account-approved-status=true IF the “Funding limited” flag 
	 * is unchecked for an account that’s already created in CurrentDesk
	 */
	private HttpRequest getIndividualDetailUpdateRequest() {
		HttpRequest httpRequest;
		String bodyJson,
			clientId = string.valueOf(liveUser.CurrentDeskClientId);
		Map<String, Object> requestBody = new Map<String, Object>();

		if (liveUser.isChanged('individualAddress'))
			requestBody.put('individual-address',
				StringUtil.replaceIfBlank(liveUser.individualAddress, 'N/A'));
		
		if (liveUser.isChanged('individualAddressCity'))
			requestBody.put('individual-address-city',
				StringUtil.replaceIfBlank(liveUser.individualAddressCity, 'N/A'));

		if (liveUser.isChanged('individualAddressCountryId'))
			requestBody.put('individual-address-country-id',
				String.valueOf(liveUser.individualAddressCountryId));

		if (liveUser.isChanged('individualAddressPostalCode'))
			requestBody.put('individual-address-postal-code',
			StringUtil.replaceIfBlank(liveUser.individualAddressPostalCode, '00000'));

		if (liveUser.isChanged('individualCitizenshipCountryId'))
			requestBody.put('individual-citizenship-country-id',
			String.valueOf(liveUser.individualCitizenshipCountryId));

		if (liveUser.isChanged('individualEmail'))
			requestBody.put('individual-email',
				StringUtil.replaceIfBlank(liveUser.individualEmail, 'N/A'));

		if (liveUser.isChanged('individualEmploymentIndustry'))
			requestBody.put('individual-employment-industry',
			StringUtil.replaceIfBlank(liveUser.individualEmploymentIndustry, 'N/A'));

		if (liveUser.isChanged('individualEmploymentStatusId'))
			requestBody.put('individual-employment-status-id',
				String.valueOf(liveUser.individualEmploymentStatusId));
		
		if (liveUser.isChanged('individualFirstName'))
			requestBody.put('individual-first-name',
				StringUtil.replaceIfBlank(liveUser.individualFirstName, 'N/A'));

		if (liveUser.isChanged('individualMiddleName'))
			requestBody.put('individual-middle-name',
				StringUtil.replaceIfBlank(liveUser.individualMiddleName, 'N/A'));

		if (liveUser.isChanged('individualLastName'))
			requestBody.put('individual-last-name',
				StringUtil.replaceIfBlank(liveUser.individualLastName, 'N/A'));

		if (liveUser.isChanged('individualIdentificationNumber'))
			requestBody.put('individual-identification-number',
				StringUtil.replaceIfBlank(liveUser.individualIdentificationNumber, 'N/A'));

		if (liveUser.isChanged('individualMobile'))
			requestBody.put('individual-mobile',
				StringUtil.replaceIfBlank(liveUser.individualMobile, 'N/A'));

		if (liveUser.isChanged('individualOccupation'))
			requestBody.put('individual-occupation',
				StringUtil.replaceIfBlank(liveUser.individualOccupation, 'N/A'));

		if (liveUser.isChanged('individualResidenceCountryId'))
			requestBody.put('individual-residence-country-id',
				String.valueOf(liveUser.individualResidenceCountryId));

		if (liveUser.isChanged('individualTelephone'))
			requestBody.put('individual-telephone',
				StringUtil.replaceIfBlank(liveUser.individualTelephone, 'N/A'));

		if (liveUser.isChanged('individualTitleId'))
			requestBody.put('individual-title-id',
				String.valueOf(liveUser.individualTitleId));

		if (liveUser.isChanged('individualBirthDate'))
			requestBody.put('individual-birth-date',
				StringUtil.replaceIfBlank(liveUser.individualBirthDate, 'N/A'));
		
		if (liveUser.isChanged('isFundingLimited'))
			requestBody.put('account-approved-status',
				liveUser.isFundingLimited ? false : true);

		if (liveUser.isChanged('accountPreferenceId'))
			requestBody.put('account-preference-id',
				String.valueOf(liveUser.accountPreferenceId));
		
				String lastName,firstName;
				if(!String.isBlank(liveUser.aliasName))
				{
					lastName =   liveUser.aliasName.substringAfter(' ');
					firstName =  liveUser.aliasName.subStringBefore(' ');
				}else{
					lastName = liveUser.individualFirstName;
					firstName = liveUser.individualLastName;
				}
				   
				requestBody.put('custom-parameter-value-03',firstName);
				requestBody.put('custom-parameter-value-04',lastName);

		httpRequest = new HttpRequest();
		httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/updateindividual?accountId=' + clientId);
		httpRequest.setHeader('clientUpdateModel',
			CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setMethod('PUT');
		httpRequest.setHeader('Content-Type', 'application/json');
		httpRequest.setHeader('authorization',
			CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setHeader('id', clientId);
		httpRequest.setTimeout(120000);

		bodyJson = JSON.serializePretty(requestBody);
		System.debug(bodyJson);
		httpRequest.setBody(bodyJson); 
		return httpRequest;
	}

	private string getClientDetailUpdateRequestBody() {
		Map<String, Object> result = new Map<String, String>();
		result.put('client-readonly', liveUser.accountReadOnly);
		result.put('client-enabled', !liveUser.lockAccount && !liveUser.disableLogin);
		return JSON.serializePretty(result);
	}
    
    private HttpRequest getUpdateFlagsRequest() { 
        string clientId = string.valueOf(liveUser.CurrentDeskClientId);

        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/updateflag'  + '?id=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setHeader('id', string.valueOf(liveUser.CurrentDeskClientId));
        httpRequest.setTimeout(120000);

        string requestBody = getUpdateFlagsRequestBody();
        httpRequest.setHeader('clientUpdateModel', CurrentDeskSync.ServiceSettings.clientSecret);
        httpRequest.setBody(requestBody); 

        return httpRequest;
    }

	private HttpRequest getUpdateTradingAccountInfo() {
		HttpRequest httpRequest;
		String clientId = string.valueOf(liveUser.CurrentDeskClientId);
		String requestBody = getUpdateTradingAccountInfoRequestBody();

        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/updatetradingaccountinfo'  + '?accountId=' + clientId);
        httpRequest.setMethod('PUT');
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setHeader('id', clientId);
		httpRequest.setTimeout(120000);

		httpRequest.setBody(requestBody);

        return httpRequest;
	}
    
    private HttpRequest getMT4LoginsRequest() 
    { 
        HttpRequest httpRequest = new HttpRequest();
        string clientId = string.valueOf(liveUser.CurrentDeskClientId);

        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl +
			'/misc/client/platformlogins' + '?id=' + clientId);
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);   
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setMethod('GET');

        return httpRequest;
    }

	/**
	 * SP-9304
	 */
	private HttpRequest getArchiveUserRequest() {
		HttpRequest httpRequest = new HttpRequest();
		String clientId = string.valueOf(liveUser.CurrentDeskClientId);

		httpRequest.setEndpoint(
			CurrentDeskSync.ServiceSettings.baseUrl +
				'/misc/client/archiveuser?id=' + clientId);
		httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setHeader('Content-Type', 'application/json');
		httpRequest.setMethod('PUT');
		httpRequest.setBody(getArchiveUserRequestBody());

		return httpRequest;
	}

	/**
	 * SP-9304
	 */
	private HttpRequest getUserAccessRequest() {
		HttpRequest httpRequest = new HttpRequest();
		String clientId = string.valueOf(liveUser.CurrentDeskClientId);

		httpRequest.setEndpoint(
			CurrentDeskSync.ServiceSettings.baseUrl +
				'/misc/client/useraccess?id=' + clientId);
		httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);
		httpRequest.setHeader('Content-Type', 'application/json');
		httpRequest.setMethod('PUT');
		httpRequest.setBody(getUserAccessRequestBody());

		return httpRequest;
	}

	private string getUpdateFlagsRequestBody() {
		return JSON.serializePretty(new Map<String, Object> {
			'flag-enabled' => liveUser.reducePositionOnly,
			'flag-id' => '1'
		});
	}

	/**
	 * SP-9304
	 */
	private string getArchiveUserRequestBody() {
		return JSON.serializePretty(new Map<String, Object> {
			'account-archived' => liveUser.archiveUser
		});
	}
	
	/**
	 * SP-9304
	 */
	private string getUserAccessRequestBody() {
		return JSON.serializePretty(new Map<String, Object> {
			'account-enabled' => !liveUser.disableLogin
		});
	}

	/**
	 * SP-12265
	 */
	private string getUpdateTradingAccountInfoRequestBody() {
		return JSON.serializePretty(new Map<String, Object> {
			'zip' => liveUser.zipCode
		});
	}

	/**
	 * Saves an email to report at the end fo the calls
	 */
	private void addError(
		String subject,
		String endppoint,
		String reqBody,
		String resBody) {
		// Build error
		String error =
			String.format(
				'CurrentDesk {0} Failed for user: {1}\n' +
				'----------------------------------\n' +
				'Repsonse: \n{2}\n' +
				'Salesforce link: {3}/{4}\n' +
				'CD Endpoint: {5}\n' +
				'Request Details: \n{6}',
				new List<String> {
					subject,
					liveUser.fxAccountDetails.Name,
					resBody,
					URL.getSalesforceBaseUrl().toExternalForm(),
					liveUser.fxAccountId,
					endppoint,
					reqBody
				}
			);

		// Save error log
		Logger.error(
			'CurrentDesk Error',
			Constants.LOGGER_CURRENT_DESK_CATEGORY,
			error);

		// Add error to the error list
		errorsToReport.add(error);
	}

	/**
	 * Send errors by email
	 */
	private void sendErrorEmail() {
		if (!errorsToReport.isEmpty())
			EmailUtil.sendEmail(
				'salesforce@oanda.com',
				'CurrentDesk Error Report',
				String.join(errorsToReport, '\n\n'));
	}
}