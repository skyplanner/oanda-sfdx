/**
 * @description       : class to conditionally return named credential and handle(update) persistent token with external credential
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 20 May 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class NamedCredentialsUtil {

    public static final String NAMED_CREDENTIAL_PERMISSION_SET = 'External_Credentials_Access';

    private final static String LOGGER_CATEGORY = 'No credential in org';
    private final static String LOGGER_MSG = 'Credential do not exist in org!!!!';
    @TestVisible
    private static Boolean isTestUseSeeAllData = false;
    /**
	 * @param credentialName Name of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
	 * @param externalPrincipleName principal name to get correct Authentication Parameters if the value is empty or cannot be found the first principal from the list will be used.
	 * @param methodName the name of the method to be executed.
	 * '' or empty value - will only return named credentials based on input parameters
	 * manageToken - additional will check the token expiration date and attempting to refresh if necessary
	 * @return the NamedCredentialData based on input param
	 * @throws MissingCredentialException happens when the named credentials will not find or get error based on permissions and ect.
	 * @example
	 * NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, 'manageToken');
	 * NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, '');
	 * NamedCredentialsUtil.NamedCredentialData namedCU = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, '', '');
	 */
    public static NamedCredentialData getOrgDefinedNamedCredential(String credentialName, String externalPrincipleName, String methodName) {
        methodName = String.isBlank(methodName) ? '' : methodName;

        return new NamedCredentialData(
                credentialName + Util.getValidCredentialSuffix(),
                Constants.CALLOUTS_CONSTANT + credentialName + Util.getValidCredentialSuffix());

        //The current solution was basically added to provide the ability to check the token expiration date and update
        // it directly or from Update_Token__e, but using Connect_Api may run into the
        // "ConnectAPI limit of 2000 packets per user per hour" limitation.
        //NOTE: Based on this, it is necessary to research a new solution to this problem.

//        ConnectApi.Credential credential;
//        ConnectApi.NamedCredential namedCredential;
//        Map<String, Object> credentialResultMap = getCredential(credentialName, externalPrincipleName);
//
//        if ((String)credentialResultMap.get('status') == Constants.STATUS_ERROR) {
//            Logger.error(
//                        LOGGER_MSG,
//                        LOGGER_CATEGORY,
//                        (String)credentialResultMap.get('message'));
//            throw new MissingCredentialException((String)credentialResultMap.get('message'));
//        }
//
//        credential = (ConnectApi.Credential)credentialResultMap.get('credential');
//        namedCredential = (ConnectApi.NamedCredential)credentialResultMap.get('namedCredential');

        //The current solution was basically added to provide the ability to check the token expiration date and update
        // it directly or from Update_Token__e, but using Connect_Api may run into the
        // "ConnectAPI limit of 2000 packets per user per hour" limitation.
        //NOTE: Based on this, it is necessary to explore a new solution to this problem.
//        switch on methodName {
//            when 'manageToken' {
//                TokenHelper.TokenHelperResult tokenHelperResult = TokenHelper.manageNamedCredentialToken(credentialName, credential,false);
//
//                if (tokenHelperResult.isSuccess == false) {
//                    if (tokenHelperResult.status != Constants.STATUS_WARNING) {
//                        Logger.error(
//                                LOGGER_MSG,
//                                LOGGER_CATEGORY,
//                                tokenHelperResult.message);
//                    }
//                    throw new MissingCredentialException(tokenHelperResult.message);
//                }
//
//
//            } when else {
//
//            }
//        }
//
//            } when else {
//
//            }
//        }
//
//        NamedCredentialData ncd = new NamedCredentialData(
//                namedCredential.developerName,
//                Constants.CALLOUTS_CONSTANT + namedCredential.developerName,
//                credential);
//        return ncd;
    }

    /**
    * @param namedCredentialName Name of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
    * @param externalPrincipleName principal name to get correct Authentication Parameters if the value is empty or cannot be found the first principal from the list will be used.
    * @description method checks if the input credential exist and has principals.
    * NOTE: To correct work the external credential should have at least 1 principal (more relevant for token renewal)
    * @return the Map<String, Object> record
    */
    public static Map<String, Object> getCredential(String namedCredentialName, String externalPrincipleName) {
        externalPrincipleName = String.isBlank(externalPrincipleName) ? '' : externalPrincipleName;
        Map<String, Object> result = new Map<String, Object>{
                'status' => Constants.STATUS_ERROR,
                'message' => ''
        };
        String namedCredentialDevName = namedCredentialName + Util.getValidCredentialSuffix();

        try {
            ConnectApi.NamedCredential namedCredential = ConnectApi.NamedCredentials.getNamedCredential(namedCredentialDevName);
            result.put('namedCredential', namedCredential);

            ConnectApi.ExternalCredential externalCredential = ConnectApi.NamedCredentials.getExternalCredential(namedCredential.externalCredentials[0].developerName);
            ConnectApi.ExternalCredentialPrincipal externalCredentialPrincipal;
            for (ConnectApi.ExternalCredentialPrincipal principal : externalCredential.principals) {
                if (principal.principalName == externalPrincipleName) {
                    externalCredentialPrincipal = principal;
                }
            }

            if (externalCredential != null && !externalCredential.principals.isEmpty() && (externalCredentialPrincipal != null || String.isNotBlank(externalPrincipleName))) {
                ConnectApi.Credential credential = ConnectApi.NamedCredentials.getCredential(
                        namedCredentialDevName,
                        (externalCredentialPrincipal != null) ? externalCredentialPrincipal.principalName : externalCredential.principals[0].principalName,
                        (externalCredentialPrincipal != null) ? externalCredentialPrincipal.principalType : externalCredential.principals[0].principalType);
                result.put('credential', credential);
            }

            result.put('status', Constants.STATUS_SUCCESS);
        } catch (Exception ex) {
            result.put('message', ex.getMessage());
        }

        return result;
    }

    public class NamedCredentialData {
        public String credentialName {get; private set;}
        public String credentialUrl {get; private set;}

        public NamedCredentialData(String credentialName, String credentialUrl) {
            this.credentialName = credentialName;
            this.credentialUrl  = credentialUrl;
        }
    }

    public class MissingCredentialException extends Exception {}
}