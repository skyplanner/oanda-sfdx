/**
 * @File Name          : ExpressionValidationInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/23/2020, 11:06:54 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/18/2020   acantero     Initial Version
**/
public abstract class ExpressionValidationInfo {

    @testVisible
    public MiFIDExpValConst.LetterCase letterCase {get; protected set;}
    @testVisible
    public Boolean checkConsecutiveNumbers {get; protected set;}
    @testVisible
    public Boolean checkRepetitiveNumber {get; protected set;}
    @testVisible
    public Boolean checkConsecutiveLetters {get; protected set;}
    @testVisible
    public Boolean checkRepetitiveLetter {get; protected set;}
    @testVisible
    public Boolean checkOnlyValidFragments {get; protected set;}
    @testVisible
    public String separators {get; protected set;}
    @testVisible
    public Integer prefixSize {get; protected set;}
    @testVisible
    public String canBeDefaultValue {get; protected set;}
    @testVisible
    public String canBeBlank {get; protected set;}

    public Boolean canBeADefaultValue() {
        return (
            (canBeDefaultValue == MiFIDExpValConst.CAN_BE_VALUE) ||
            (canBeDefaultValue == MiFIDExpValConst.MUST_BE_VALUE) 
        );
    }

    public Boolean mustBeADefaultValue() {
        return (canBeDefaultValue == MiFIDExpValConst.MUST_BE_VALUE);
    }

    public Boolean canBeBlankValue() {
        return (
            (canBeBlank == MiFIDExpValConst.CAN_BE_VALUE) ||
            (canBeBlank == MiFIDExpValConst.MUST_BE_VALUE)
        );
    }

    public Boolean canBeBlankWithConditions() {
        return (canBeBlank == MiFIDExpValConst.CAN_BE_CONDITIONAL_VALUE);
    }

    public Boolean mustBeBlankValue() {
        return (canBeBlank == MiFIDExpValConst.MUST_BE_VALUE);
    }

}