/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 12-15-2022
 * @last modified by  : Eugene Jung
**/
public with sharing class CurrentDeskSync
{
    public static set<string> customFxAccountFields = new set<string>{};
    public static set<string> customAccountFields = new set<string>{};
    public static set<string> customLeadFields = new set<string>{};

    @TestVisible private static boolean run = true;

    public static set<string> accountFields = new set<string>
    {
        'Salutation',
        'FirstName',
        'MiddleName',
        'LastName',
        'Phone',
        'PersonMailingStreet',
        'PersonMailingCity',
        'PersonMailingCountry',
        'PersonMailingPostalCode',
        'PersonEmail',
        'Nickname__pc'
    };

    public static set<string> leadFields = new set<string>
    {
        'Id', 
        'Title',
        'FirstName',
        'MiddleName',
        'LastName',
        'Street',
        'City',
        'PostalCode',
        'Country',
        'Phone',
        'Email',
        'fxAccount__c',
        'fxAccount__r.Name',
        'aliases__c'
    };

    public static set<string> fxAccountCDSyncFields = new Set<String> {
        'CD_Account_Read_Only__c',
        'CD_Lock_Account__c'
    };

	public static set<string> fxAccountCDFlagFields = new Set<String> {
		'CD_Reduce_Position_Only__c'
	};

	public static set<string> fxAccountCDArchiveFields = new Set<String> {
        'CD_Archive_User__c'
    };

	public static set<String> fxAccountCDAccessFields = new Set<String> {
		'CD_Disable_Login__c'
	};

    public static set<String> fxAccountCDTradingAccountInfoFields = new Set<String> {
		'Conditionally_Approved__c'
	};

	public static set<string> fxAccountFields = new set<string> {
		'Funding_limited__c',
		'Citizenship_Nationality__c',
		'Email__c',
		'Industry_of_Employment__c',
		'Employment_Status__c',
		'Government_ID__c',
		'Employment_Status__c',
		'Birthdate__c',
		'CD_Account_Preference_ID__c'
    };

    public static Map<string, integer> countryIdMap;
    public static Map<string, integer> titleMap = new Map<string, integer>
    {
        null => 1,
        'Mr.' => 1,
        'Mrs.'  => 2,
        'Miss' => 3,
        'Ms.' => 4,
        'Dr.' => 5,
        'Prof.' => 6
    };
    public static Map<string, integer> employmentStatusMap = new Map<string, integer>
    {
        'Employed' => 1,
        'Unemployed' => 2,
        'Self Employed' => 3,
        'Retired' => 4,
        'Student' => 5
    };
    public static Map<string, integer> languageMap = new Map<string, integer>
    {
        'Arabic' => 9,
        'Chinese Simplified' => 16,
        'Chinese Traditional' => 2,
        'English' => 1,
        'French' => 3,
        'German' => 6,
        'Italian' => 30,
        'Japanese' => 4,
        'Korean' => 32,
        'Portuguese' => 43,
        'Russian' => 45,
        'Spanish' => 50,
        'Vietnamese' => 61,
        'Thai' => 55,
        'Bahasa Indonesia' => 28,
        'French' => 3,
        'Bahasa Malay' => 70
    };

	private static Map<String, String> getIndividualFieldMapping() {
		return new Map<String, String> {
			'PersonMailingStreet' => 'individualAddress',
			'PersonMailingCity' => 'individualAddressCity',
			'PersonMailingPostalCode' => 'individualAddressPostalCode',
			'PersonMailingCountry' =>
				'individualAddressCountryId,' +
				'individualResidenceCountryId',
			'Citizenship_Nationality__c' => 'individualCitizenshipCountryId',
			'PersonEmail' => 'individualEmail',
			'Industry_of_Employment__c' => 'individualEmploymentIndustry',
			'Employment_Status__c' =>
				'individualEmploymentStatusId,' +
				'individualEmploymentIndustry,' +
				'individualOccupation',
			'FirstName' => 'individualFirstName',
			'MiddleName' => 'individualMiddleName',
			'LastName' => 'individualLastName',
			'Government_ID__c' => 'individualIdentificationNumber',
			'Phone' => 'individualMobile,individualTelephone',
			'Employment_Job_Title__c' => 'individualOccupation',
			'Salutation' => 'individualTitleId',
			'Birthdate__c' => 'individualBirthDate',
			'Funding_limited__c' => 'isFundingLimited',
			'CD_Account_Preference_ID__c' => 'accountPreferenceId'
		};
	}
    
    private static string individualIdentificationNumberDummy = 'dummy_id';

    public static Map<ID, Schema.RecordTypeInfo> fxAccountRecordTypeMap = Schema.SObjectType.fxAccount__c.getRecordTypeInfosById(); 
    public static Map<ID, Schema.RecordTypeInfo> leadRecordTypeMap = Schema.SObjectType.Lead.getRecordTypeInfosById(); 

    private static APISettings apiSettings;
    public static APISettings ServiceSettings
    {
       get 
       {
           if(apiSettings == null)
           {
                apiSettings = APISettingsUtil.getApiSettingsByName(Constants.CURRENT_DESK_SETTINGS);
           }
           return apiSettings; 
       }
    }

    private static Current_Desk_Parameter__mdt[] currentDeskParameters= getCurrentDeskParameters();
    private static Map<string,string> customParameters = getCustomParameters();
    private static Map<String, object> constantParameters = getConstantParameters();

    private static DateTime SEVEN_DAYS_AGO = DateTime.now().addDays(-7);

	/**
	 * @return true if CurrentDesk integration is enabled
	 */
	public static Boolean isCurrentDeskEnabled() {
		Settings__c settings = Settings__c.getValues('Default');

		return settings != null &&
			settings.Enable_Current_Desk_Integration__c == true &&
			runOnce();
	}

	/**
	 * @param fxAccount
	 * @return true if the account is ready fpr current desk
	 */
	public static Boolean isCurrentDeskReady(fxAccount__c fxAccount) {
		return 
            fxAccount.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS &&
			fxAccount.RecordTypeId == RecordTypeUtil.getFxAccountLiveId() &&
			fxAccount.Funnel_Stage__c == Constants.FUNNEL_RFF &&
			fxAccount.CD_Client_ID__c == null &&
            (fxAccount.Ready_for_Funding_DateTime__c == null ||
            fxAccount.Ready_for_Funding_DateTime__c > SEVEN_DAYS_AGO);
	}

	/**
	 * @param fxAccount
	 * @return true if the account is already in current desk
	 */
	public static Boolean isCurrentDeskIntegrated(fxAccount__c fxAccount) {
		return 
            fxAccount.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS &&
            fxAccount.CD_Client_ID__c != null;
	}

	/**
	 * SP-9239.
	 * Laucnhes an update in the current desk based on changes in the
	 * person account that should be reflected in the current desk portal
	 */
	public static void syncCustomerRecords(
			List<Account> newAccounts, List<Account> oldAccounts) {
		Integer currenIndex = 0;
		Map<Id, Set<String>> changedAccountIds = new Map<Id, Set<String>>();
		Set<String> changedFields;
		List<fxAccount__c> fxAccountsToUpdate;
		List<String> marketsInvolved =
			new List<String> { Constants.DIVISIONS_OANDA_GLOBAL_MARKETS };
		Id liveRtId;
		
		// we first need to extract those account 
		// whose changes are influential
		for (Account newAccount : newAccounts) {
			changedFields = compareRecords(
				accountFields, oldAccounts[currenIndex++], newAccount);

			if (!changedFields.isEmpty())
				changedAccountIds.put(newAccount.Id, changedFields);
		}

		// if there were account that changed in meaningful fields,
		// we fetch the related fxAccounts
		if (!changedAccountIds.isEmpty()) {
			liveRtId = RecordTypeUtil.getFxAccountLiveId();

			// fxaccounts updated should be
			// 1. Opened and not locked
			// 2. Must have a CD Client Id already
			// 3. Must be a live account
			// 4. and must be from the division allowed in CD
			fxAccountsToUpdate = [
				SELECT Id,
					Name,
					Account__c,
					CD_Client_ID__c,
					CD_Account_Read_Only__c,
					CD_Lock_Account__c,
					CD_Disable_Login__c,
					CD_Reduce_Position_Only__c,
					CD_Archive_User__c,
					CD_Account_Preference_ID__c,
					Citizenship_Nationality__c,
					Birthdate__c,
					Employment_Status__c,
					Industry_of_Employment__c,
					Employment_Job_Title__c,
					Government_ID__c,
					Funding_limited__c,
					Language_Preference__c,
					fxTrade_User_Id__c,
                    Conditionally_Approved__c
				FROM fxAccount__c
				WHERE Account__c IN :changedAccountIds.keySet()
				AND Is_Closed__c = false
				AND CD_Client_ID__c != NULL
				AND RecordTypeId = :liveRtId
				AND Division_Name__c IN :marketsInvolved
			];

			if (fxAccountsToUpdate.isEmpty())
				return;

			for (fxAccount__c fxAccount : fxAccountsToUpdate)
				updateLiveUser(fxAccount, changedAccountIds.get(fxAccount.Account__c));
		}
	}

	public static void syncCustomerRecords(
			List<fxAccount__c> newFxAccounts,
			List<fxAccount__c> oldFxAccounts) {
		Set<string> updatedFieldsToSync;
		fxAccount__c newFxAccount, oldFxAccount;

		Logger.debug(newFxAccounts + ' ');

		for (Integer i = 0; i < newFxAccounts.size(); i++) {
			newFxAccount = newFxAccounts.get(i);
			oldFxAccount = oldFxAccounts.get(i);

			// we register the live user if its ready
			if (isCurrentDeskReady(newFxAccount))
				registerLiveUser(newFxAccount);
			// or we update the user if
			// it is already integrated
			else if (isCurrentDeskIntegrated(newFxAccount)) {
				updatedFieldsToSync = new Set<String>();
				updatedFieldsToSync.addAll(
					compareRecords(fxAccountCDSyncFields, oldFxAccount, newFxAccount));
				updatedFieldsToSync.addAll(
					compareRecords(fxAccountCDFlagFields, oldFxAccount, newFxAccount));
				updatedFieldsToSync.addAll(
					compareRecords(fxAccountCDArchiveFields, oldFxAccount, newFxAccount));
				updatedFieldsToSync.addAll(
					compareRecords(fxAccountCDAccessFields, oldFxAccount, newFxAccount));
				updatedFieldsToSync.addAll(
					compareRecords(fxAccountFields, oldFxAccount, newFxAccount));
                updatedFieldsToSync.addAll(
					compareRecords(fxAccountCDTradingAccountInfoFields, oldFxAccount, newFxAccount));                 
				
				if (!updatedFieldsToSync.isEmpty()) 
					updateLiveUser(newFxAccount, updatedFieldsToSync);
			}
		} 
	}

    public static void registerDemoUser(Lead demoLead)
    {
        if(demoLead.fxAccount__c !=null && 
           demoLead.FirstName != null && 
           demoLead.LastName  != null)
        {
            Id demoLeadId = demoLead.Id;

            Set<String> fxAccountFields = new Set<String>();
            fxAccountFields.addAll(customFxAccountFields);
            fxAccountFields.add('Id');
            fxAccountFields.add('Name');
            fxAccountFields.add('CD_Demo_Lead_Id__c');
            fxAccountFields.add('Division_Name__c');

            String fxaccountQuery = 'select ' + SoqlUtil.getSoqlFieldList(fxAccountFields) +' from fxAccount__c where Lead__c = :demoLeadId limit 1';
            fxAccount__c[] fxAccountList = Database.query(fxaccountQuery);
 
            if(fxAccountList.size() > 0 && 
               fxAccountList[0].Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS &&
               fxAccountList[0].CD_Demo_Lead_Id__c == null)
            {
                DemoUser currentDeskLeadInfo = new DemoUser(fxAccountList[0], demoLead);     
                CurrentDeskDemoLeadCallout demoCallout = new CurrentDeskDemoLeadCallout(currentDeskLeadInfo);       
                System.enqueueJob(demoCallout);
            }
        }
    }

    public static void registerLiveUser(fxAccount__c fxAccount)
    {
        Account account = string.isNotBlank(fxAccount.Account__c) ? getAccountDetails(fxAccount.Account__c) : null;
        Lead lead = string.isNotBlank(fxAccount.Lead__c) ? getLeadDetails(fxAccount.Lead__c) : null;         
        LiveUser liveUser = new LiveUser(fxAccount, account, lead);

        CurrentDeskLiveUserCallOut liveRegCallout = new CurrentDeskLiveUserCallOut(liveUser);       
        System.enqueueJob(liveRegCallout);
    }

    public static void updateLiveUser(
        fxAccount__c fxAccount,
		set<string> updatedFieldsToSync)
    {
        Logger.info(
            'Update Live User',
            Constants.LOGGER_CURRENT_DESK_CATEGORY,
            new Map<String, Object>{
                'fxAccount' => fxAccount,
                'updatedFieldsToSync' => updatedFieldsToSync
            });

		Map<String, String> individualFieldMapping;
        LiveUser liveUser = new LiveUser(
			fxAccount,
			String.isNotBlank(fxAccount.Account__c) ?
				getAccountDetails(fxAccount.Account__c) :
				null,
			null);
		
		// should we update the client
		liveUser.updateClient =
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountCDSyncFields);
		
		// should we update the individual
		liveUser.updateIndividual = 
			Utilities.doSetsIntersect(updatedFieldsToSync, accountFields) ||
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountFields);

		// should we update the flag
		liveUser.updateFlag =
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountCDFlagFields);
		
		// should we update the archived consideration
		liveUser.updateArchived =
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountCDArchiveFields);
		
		// should we update the user access
		liveUser.updateAccess =
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountCDAccessFields);
        
        // should we update the trading account info
		liveUser.updateTradingAccountInfo =
			Utilities.doSetsIntersect(updatedFieldsToSync, fxAccountCDTradingAccountInfoFields);          
		
		// SP-9239.. we need to record any changes
		individualFieldMapping = getIndividualFieldMapping();
		for (String field : updatedFieldsToSync)
			if (individualFieldMapping.containsKey(field))
				liveUser.individualChangedFields.addAll(
					individualFieldMapping.get(field).split(','));
		
		CurrentDeskUpdate updateCallout = new CurrentDeskUpdate(liveUser);
        System.enqueueJob(updateCallout);
    }

	/**
	 * SP-10406
	 * @return all account preference mappings
	 * for PIU annual processes in the FROM direction
	 */
	public static Map<Integer, Integer> getPreferenceMappings() {
		return getPreferenceMappings('From');
	}

	/**
	 * SP-10406
	 * @return all account preference mappings
	 * for PIU annual processes in the FROM direction
	 */
	public static Map<Integer, Integer> getPreferenceMappingsInversed() {
		return getPreferenceMappings('To');
	}

    public static Map<Integer, Integer> getUSSharesPreferenceMappings() {
        return getPreferenceMappings('US_Shares_Trading_From');
    }    

    public static Map<Integer, Integer> getUSSharesPreferenceMappingsInversed() {
        return getPreferenceMappings('US_Shares_Trading_To');
    }

	/**
	 * SP-10406
	 * @return all account preference mappings
	 * for PIU annual processes
	 */
	private static Map<Integer, Integer> getPreferenceMappings(String source) {
		Map<Integer, Integer> mappings;
		String numberStr,
			calculatedSource = 'Preference_Mapping_' + source;
		
		// we'll convert to integer
		mappings = new Map<Integer, Integer>{};

		for (String paramName : constantParameters.keySet())
			if (paramName.startsWith(calculatedSource))
				// we put both integers in the map
				mappings.put(
					Integer.valueOf(paramName.substringAfterLast('_')),
					Integer.valueOf(constantParameters.get(paramName))
				);
			
		return mappings; 
	}

    private static set<string> compareRecords(set<string> compareFields, sobject oldRecord, sobject newRecord)
    {
        set<string> changedFields = new set<string>{};           
        for(string field : compareFields)
        {
            if(oldRecord.get(field) != newRecord.get(field))
            {
                changedFields.add(field);
            }
        }
        return changedFields;
    }

    private static Account getAccountDetails(Id accountId)
    {
        if(!customAccountFields.isEmpty())
        {
           accountFields.addAll(customAccountFields);
        }
        String accountQuery = 'select ' + SoqlUtil.getSoqlFieldList(accountFields) +' from Account where Id = :accountId limit 1';
        Account[] accountList = Database.query(accountQuery);
        return accountList.size() > 0 ? accountList[0] : null;
    }
    private static Lead getLeadDetails(Id leadId)
    {
        if(!customLeadFields.isEmpty())
        {
            leadFields.addAll(customLeadFields);
        }
        String leadQuery = 'select ' + SoqlUtil.getSoqlFieldList(leadFields) +' from Lead where Id = :leadId limit 1';
        Lead[] leadList = Database.query(leadQuery);
        return leadList.size() > 0 ? leadList[0] : null;
    }

    private static string formatDate(Date input)
    { 
       string formattedDate = '';
       if(input != null)
       {
            formattedDate = DateTime.newInstance(
                                input.year(), input.month(), input.day()
                            ).format('yyyy-MM-dd');
       }
       return formattedDate;
    }
    
	/**
	 * SP-9745, country matching made case insenstive
	 */
    public static Integer getCountryId(String countryName) {
		Integer countryId = 0;
		String loweredName, cName;

		if (String.isNotBlank(countryName)) {
			if (countryIdMap == null) {
				countryIdMap = new Map<string, integer>();

				for (Country_Setting__c countrySetting :
						Country_Setting__c.getAll().values()) {
					cName = String.isNotBlank(countrySetting.Long_Name__c) ?
						countrySetting.Long_Name__c.toLowerCase() :
						countrySetting.Name.toLowerCase();

					countryIdMap.put(cName,
						Integer.valueOf(countrySetting.Current_Desk_Country_Id__c));
				}
			}

			// only lowered case matching
			loweredName = countryName.toLowerCase();

			if (countryIdMap.containsKey(loweredName)) 
				countryId = countryIdMap.get(loweredName);
		}

		return countryId;
	}

    private static Current_Desk_Parameter__mdt[] getCurrentDeskParameters()
    {
        Current_Desk_Parameter__mdt[] currentDeskParameters = [select Name__c,
                                                                      Mapped_Field_Name__c,
                                                                      Value__c
                                                               From Current_Desk_Parameter__mdt];
        return currentDeskParameters; 
    }

    private static Map<String, String> getCustomParameters()
    {
        Map<String, String> customParams = new Map<String, String>{};

        for(Current_Desk_Parameter__mdt cdp : currentDeskParameters)
        {
            if(string.isNotBlank(cdp.Mapped_Field_Name__c))
            {
                string fieldName = cdp.Mapped_Field_Name__c.substringAfter('.');
                customParams.put(fieldName, cdp.Name__c);

                if(cdp.Mapped_Field_Name__c.indexOf('fxaccount.') != -1)
                {
                    customFxAccountFields.add(fieldName);
                }
                else if(cdp.Mapped_Field_Name__c.indexOf('account.') !=  -1)
                {
                    customAccountFields.add(fieldName);
                }
                else if(cdp.Mapped_Field_Name__c.indexOf('lead.') !=  -1)
                {
                    customLeadFields.add(fieldName);
                }
            }  
        }
        return customParams; 
    }

    public static Map<String, Object> getConstantParameters()
    {
        Map<String, Object> constParams = new Map<String, Object>{};

        for(Current_Desk_Parameter__mdt cdp : currentDeskParameters)
        {
            if(string.isNotBlank(cdp.Value__c))
            {
                constParams.put(cdp.Name__c, cdp.Value__c);
            }  
        }
        return constParams; 
    }

    private static Map<String, String> getCustomMessage(SObject obj, set<String> customFields) {
        //now only string custom params supported
        Map<String, String> additioanlParameters = new Map<String, String>();

        if (obj != null && customFields != null && !customFields.isEmpty()) 
            for(string fieldName : customFields)
                if (obj.get(fieldName) != null) {
                    additioanlParameters.put(
						customParameters.get(fieldName),
						String.valueof(obj.get(fieldName)));
                }
				
        return additioanlParameters;
    }

    private static Map<String, Object> getLiveCustomerCustomMessage(
			fxAccount__c fxa, Account account, Lead lead) {
		Map<String, Object> additioanlParameters = new Map<String, Object>();

        if (!customFxAccountFields.isEmpty())
            additioanlParameters.putAll(getCustomMessage(fxa, customFxAccountFields));

        if (!customAccountFields.isEmpty())
            additioanlParameters.putAll(getCustomMessage(account, customAccountFields));

		if (!customLeadFields.isEmpty())
			additioanlParameters.putAll(getCustomMessage(lead, customLeadFields));

        return additioanlParameters;
    }

    private static Map<String, Object> getDemoUserCustomMessage(
			fxAccount__c fxa, Lead lead) {
		Map<String, Object> additioanlParameters = new Map<String, Object>();

		if (!customFxAccountFields.isEmpty())
			additioanlParameters.putAll(getCustomMessage(fxa, customFxAccountFields));

		if (!customLeadFields.isEmpty())
			additioanlParameters.putAll(getCustomMessage(lead, customLeadFields));

		return additioanlParameters;
    }

    public static boolean runOnce() 
    {
         if(run)
         {
             run=false;
             return true;
         }
         else
         {
             return run;
         }
    }
    
    public class DemoUser
    {
        public Id fxAccountId;
        public Id leadId;
        public fxAccount__c fxAccountDetails;
        public Lead leadDetails;


        public integer titleId;
        public string firstName;
        public string lastName;
        public string email;
        public string telephone;
        public integer residenceCountryId;
        public integer languageId;
        public Boolean accountEnglishStatus;
        public Boolean accountDemoStatus;
        public integer accountCategoryId;
        public integer accountTradingPlatformId;
        public integer tradingAverageDealSizeId;
        public integer accountCurrencyId;
        public integer accountPreferenceId;
        public integer initialDepositId;
        public integer tradingExpereinceId;
        public string proxyDomain;
        public Map<String, Object> reqCustomParameters;

        public DemoUser()
        {

        }
        public DemoUser(fxAccount__c fxAccount, Lead lead)
        {
            leadId = lead.Id;
            leadDetails = lead;
            fxAccountId = fxAccount.Id;
            fxAccountDetails = fxAccount;
            
            titleId = titleMap.get(lead.Title);
            firstName = lead.FirstName;
            lastName = lead.LastName;
            email = lead.Email;
            telephone = lead.Phone;
            residenceCountryId = getCountryId(lead.Country);
            languageId = Integer.valueOf(constantParameters.get('Account_Language_Id'));
            accountEnglishStatus = Boolean.valueOf(constantParameters.get('Account_English_Status'));
            if(lead.Language_Preference__c != null) 
            {
                if(languageMap.containsKey(lead.Language_Preference__c))
                {
                    languageId = languageMap.get(lead.Language_Preference__c);
                }

                // SP-14575 false if lead's language preference is non-English
                accountEnglishStatus = lead.Language_Preference__c == 'English';
            }
            accountDemoStatus = true;
            accountCategoryId = Integer.valueOf(constantParameters.get('Account_Category_Id'));
            accountTradingPlatformId =  Integer.valueOf(constantParameters.get('Account_Trading_Platform_Id'));
            tradingAverageDealSizeId =  Integer.valueOf(constantParameters.get('Trading_Average_Deal_Size_Id'));
            accountCurrencyId =  Integer.valueOf(constantParameters.get('Account_Currency_Id'));
            accountPreferenceId =  Integer.valueOf(constantParameters.get('Account_Preference_Id_Demo'));
            tradingExpereinceId =  Integer.valueOf(constantParameters.get('Trading_Expereince_Id'));
            initialDepositId =  Integer.valueOf(constantParameters.get('Initial_Deposit_Id'));
            proxyDomain = (string)constantParameters.get('Proxy_Domain');
            reqCustomParameters =  getDemoUserCustomMessage(fxAccount, lead);
        }
    }

    public class LiveUser 
    {
        public integer currentDeskClientId;
        public Id fxAccountId;
        public Id accountId;
        public Id leadId;
        public fxAccount__c fxAccountDetails;
        public Account accountDetails;
        public Lead leadDetails;

        public integer accountCodeId;
        public integer accountLanguageId;
        public integer accountCurrencyId;
        public string accountPassword;
        public string accountPhoneId;
        public integer accountTradingPlatformId;
        public integer accountPreferenceId;

        public string individualAddress;
        public string individualAddressCity;
        public integer individualAddressCountryId;
        public integer individualAddressMonths;
        public string individualAddressPostalCode;
        public string individualBirthDate;
        public integer individualCitizenshipCountryId;
        public string individualEmail;
        public string individualEmploymentIndustry;
        public integer individualEmploymentStatusId;
        public string individualFirstName;
        public string individualMiddleName;
        public integer individualGenderId;
        public string individualIdentificationNumber;
        public integer individualIdentificationTypeId;
        public string individualLastName;
        public string individualMobile;
        public string individualOccupation;
        public integer individualResidenceCountryId;
        public string individualTelephone;
        public integer individualTitleId;
        public integer tradingFuturesExperienceId;
        public integer tradingOptionsExperienceId;
        public integer tradingSharesExperienceId;
        public integer tradingSpotExperienceId;
        public Map<String, Object> reqCustomParameters;

        public boolean reducePositionOnly;

		

        // https://oandacorp.atlassian.net/browse/SP-9161
        public Boolean isFundingLimited;

		public boolean accountReadOnly;
		public boolean lockAccount;
		public boolean archiveUser;
		public boolean disableLogin;

        public Boolean updateIndividual;
        public Boolean updateClient;
		public Boolean updateFlag;
		public Boolean updateArchived;
		public Boolean updateAccess;
        public Boolean updateTradingAccountInfo;
        
        public string proxyDomain;

        private Set<String> individualChangedFields;
        //SP-10485
        public String aliasName;        
        //SP-12265
        public String zipCode;

        public LiveUser() {
			individualChangedFields = new Set<String>();
        }

        public LiveUser(fxAccount__c fxAccount, Account account, Lead lead) {
			this();

            if(fxAccount.CD_Client_ID__c != null)
            {
                currentDeskClientId = Integer.ValueOf(fxAccount.CD_Client_ID__c);
            }
            
            fxAccountId = fxAccount.Id;
            fxAccountDetails = fxAccount;

            individualCitizenshipCountryId = getCountryId(fxAccount.Citizenship_Nationality__c);
            individualBirthDate = formatDate(fxAccount.Birthdate__c);
            individualEmploymentStatusId = employmentStatusMap.get(fxAccount.Employment_Status__c);
            individualEmploymentIndustry = string.isNotBlank(fxAccount.Industry_of_Employment__c)? fxAccount.Industry_of_Employment__c : fxAccount.Employment_Status__c;
            individualOccupation = string.isNotBlank(fxAccount.Employment_Job_Title__c)? fxAccount.Employment_Job_Title__c : fxAccount.Employment_Status__c; 
            individualIdentificationNumber = string.isNotBlank(fxAccount.Government_ID__c) ? fxAccount.Government_ID__c : individualIdentificationNumberDummy;
            individualIdentificationTypeId =  Integer.valueOf(constantParameters.get('Individual_Identification_Type_Id'));

			// SP-9161: The field will be used to indicate/control 
			// if client is subject to a funding limit once our new "KYC lite"
			// onboarding process is launched. and default value for 
			// this field is YES/TRUE.
			isFundingLimited = fxAccount.Funding_limited__c == true;

			// SP-9304, SP-9304, SP-9304
			accountReadOnly = fxAccount.CD_Account_Read_Only__c == true;
			lockAccount = fxAccount.CD_Lock_Account__c == true;
			archiveUser = fxAccount.CD_Archive_User__c == true;
			disableLogin = fxAccount.CD_Disable_Login__c == true;
			reducePositionOnly = fxAccount.CD_Reduce_Position_Only__c == true;

            // SP-12265
            zipCode = fxAccount.Conditionally_Approved__c ? (string)constantParameters.get('Zip_Code') : '00000';
           
            if(account != null)
            {
                accountDetails = account;
                accountId = account.Id; 
                individualTitleId = titleMap.get(account.Salutation);
                individualFirstName = account.FirstName;
                individualMiddleName = account.MiddleName;
                individualLastName = account.LastName;
                individualAddress = string.isNotBlank(account.PersonMailingStreet) ? account.PersonMailingStreet.replaceAll('\r\n|\n|\r',' ') : null;
                individualAddressCity = account.PersonMailingCity;
                individualAddressPostalCode = account.PersonMailingPostalCode;
                individualAddressCountryId = getCountryId(account.PersonMailingCountry);
                individualResidenceCountryId = getCountryId(account.PersonMailingCountry);
                individualMobile = account.Phone;
                individualTelephone = account.Phone;
                individualEmail = account.PersonEmail;
                aliasName = account.Nickname__pc;
            }
            if(lead != null)
            {
                leadId = lead.Id; 
                leadDetails = lead;
                individualTitleId = titleMap.get(lead.Title);
                individualFirstName = lead.FirstName;
                individualMiddleName = lead.MiddleName;
                individualLastName = lead.LastName;
                individualAddress =  string.isNotBlank(lead.Street) ? lead.Street.replaceAll('\r\n|\n|\r',' ') : null;
                individualAddressCity = lead.City;
                individualAddressPostalCode = lead.PostalCode;
                individualAddressCountryId = getCountryId(lead.Country);
                individualResidenceCountryId = getCountryId(lead.Country);
                individualMobile = lead.Phone;
                individualTelephone = lead.Phone;
                individualEmail = lead.Email;
                aliasName = lead.aliases__c;
            }
            
            tradingFuturesExperienceId =  Integer.valueOf(constantParameters.get('Trading_Futures_Experience_Id'));
            tradingOptionsExperienceId =  Integer.valueOf(constantParameters.get('Trading_Options_Experience_Id'));
            tradingSharesExperienceId =  Integer.valueOf(constantParameters.get('Trading_Shares_Experience_Id'));
            tradingSpotExperienceId =  Integer.valueOf(constantParameters.get('Trading_Spot_Experience_Id'));

            //we are not capturing below fields
            accountCodeId =  Integer.valueOf(constantParameters.get('Account_Code_Id')); 
            accountTradingPlatformId =  Integer.valueOf(constantParameters.get('Account_Trading_Platform_Id'));

			// SP-10406
			accountPreferenceId = Integer.valueOf(fxAccount.CD_Account_Preference_ID__c);
				// Integer.valueOf(constantParameters.get('Account_Preference_Id'));

            accountLanguageId =  Integer.valueOf(constantParameters.get('Account_Language_Id'));
            if(fxAccount.Language_Preference__c != null && languageMap.containsKey(fxAccount.Language_Preference__c))
            {
                accountLanguageId = languageMap.get(fxAccount.Language_Preference__c);
            }

            accountCurrencyId =  Integer.valueOf(constantParameters.get('Account_Currency_Id'));

            string ct = string.valueOf(Datetime.now());
            accountPassword = fxAccount.Name + ct.replace(':', '').replace('-', '').replace(' ', '');
            
            proxyDomain = (string)constantParameters.get('Proxy_Domain');

            //dummy values
            accountPhoneId = 'dummyPhoneId';
            individualGenderId = 3; //not specified
            individualAddressMonths = 1;

            reqCustomParameters = getLiveCustomerCustomMessage(fxAccount, account, lead);
        }

		/**
		 * @param field
		 * @return if property comes from a field that has changed
		 */
		public Boolean isChanged(String field) {
			return individualChangedFields.contains(field);
		}
    }
}