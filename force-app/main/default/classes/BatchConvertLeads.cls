public with sharing class BatchConvertLeads implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {

	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchConvertLeads() {
		query = 'SELECT Id, Email, Funnel_Stage__c, OwnerId, RecordTypeId, IsConverted FROM Lead WHERE RecordTypeId = \'' + LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE + '\' AND (Funnel_Stage__c = \'Traded\' OR OwnerId!=\'' + UserUtil.getSystemUserId() + '\') AND IsConverted = false';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Email, Funnel_Stage__c, OwnerId, RecordTypeId, IsConverted FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	public Database.Querylocator start(Database.BatchableContext bc) {
		CustomSettings.setEnableLeadConversion(true);
		return Database.getQueryLocator(query);
	}
	
	public void execute(Database.BatchableContext bc, List<SObject> batch) {
		
   		if (!CustomSettings.isEnableLeadConversion()) {
   			throw new ApplicationException('Lead conversion must be enabled before converting migrated leads');
   		}
		
		// Trigger lead conversion on update
		LeadUtil.convertReadyLeads(batch);
	}
	
	public void finish(Database.BatchableContext bc) {
		try {
			BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
		}
		catch (Exception e) {}
	}
	
	public static Id executeBatch() {
		return Database.executeBatch(new BatchConvertLeads(), 10);
	}
}