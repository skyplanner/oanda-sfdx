/**
 * Test: FeedbackHomeCtrl
 * @author Fernando Gomez
 * @version 1.0
 * @since Apr 10th
 */
@isTest
private class FeedbackHomeCtrlTest {
	/**
	 * Bad parameter
	 */
	@isTest
	static void isBadParameter() {
		FeedbackHomeCtrl f;
		PageReference pr = Page.FeedbackHome;

		pr.getParameters().put('survey', 'invalid_json');
		Test.setCurrentPage(pr);
		f = new FeedbackHomeCtrl();
		System.assert(f.isBadParameter);

		pr.getParameters().put('survey',
			'{"c":"0010t000005LGN1","s":"a1D0t000000vzrDEAQ","l":"English"}');
		f = new FeedbackHomeCtrl();
		System.assert(f.isBadParameter);
	}

	/**
	 * not available
	 */
	@isTest
	static void isNotAvailable() {
		FeedbackHomeCtrl f;
		PageReference pr = Page.FeedbackHome;
		Case c;

		insert new SurveySettings__c(CurrentSurveyName__c = 'Sample');

		c = new Case(
			Subject = 'sample',
			Origin = 'Phone',
			Live_or_Practice__c = 'Live',
			Priority = 'Normal',
			Inquiry_Nature__c = 'Registration',
			Status = 'Open',
			RecordTypeId = 
				Schema.SObjectType.Case.getRecordTypeInfosByName().get(
					'Support').getRecordTypeId()
		);
		insert c;

		pr.getParameters().put('survey',
			'{"c":"' + c.Id + '","s":"a1D0t000000vzrDEAQ","l":"English"}');
		Test.setCurrentPage(pr);
		f = new FeedbackHomeCtrl();
		System.assert(f.isNotAvailable);
	}

	/**
	 * is expired
	 */
	@isTest
	static void isExpired() {
		FeedbackHomeCtrl f;
		PageReference pr = Page.FeedbackHome;
		Case c;
		Survey__c s;

		insert new SurveySettings__c(CurrentSurveyName__c = 'Sample');

		c = new Case(
			Subject = 'sample',
			Origin = 'Phone',
			Live_or_Practice__c = 'Live',
			Priority = 'Normal',
			Inquiry_Nature__c = 'Registration',
			Status = 'Open',
			RecordTypeId = 
				Schema.SObjectType.Case.getRecordTypeInfosByName().get(
					'Support').getRecordTypeId(),
			FeedbackReminderSentOn__c = System.now() - 10
		);
		insert c;

		s = new Survey__c(
			Name = 'Sample'
		);
		insert s;

		pr.getParameters().put('survey',
			'{"c":"' + c.Id + '","s":"' + s.Id + '","l":"English"}');
		Test.setCurrentPage(pr);
		f = new FeedbackHomeCtrl();
		System.assert(f.isExpired);
	}

	/**
	 * all good
	 */
	@isTest
	static void isAllGood() {
		FeedbackHomeCtrl f;
		PageReference pr = Page.FeedbackHome;
		Case c;
		Survey__c s;

		insert new SurveySettings__c(CurrentSurveyName__c = 'Sample');

		c = new Case(
			Subject = 'sample',
			Origin = 'Phone',
			Live_or_Practice__c = 'Live',
			Priority = 'Normal',
			Inquiry_Nature__c = 'Registration',
			Status = 'Open',
			RecordTypeId = 
				Schema.SObjectType.Case.getRecordTypeInfosByName().get(
					'Support').getRecordTypeId(),
			FeedbackReminderSentOn__c = System.now()
		);
		insert c;

		s = new Survey__c(
			Name = 'Sample'
		);
		insert s;

		pr.getParameters().put('survey',
			'{"c":"' + c.Id + '","s":"' + s.Id + '","l":"English"}');
		Test.setCurrentPage(pr);
		f = new FeedbackHomeCtrl();
		System.assert(f.isAllGood);
	}
}