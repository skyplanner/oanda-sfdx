/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 01-20-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiAuditLogsGetResponse {
    public List<UserApiAuditLog> logs {get; set;}
}