@RestResource(urlMapping='/User/*')
global without sharing class FxAccountRestResource {

    static final String NAME_SYSTEM_USER = 'System User';
    static Id getSystemUserId() {
        return [SELECT Id, Username, Name FROM User WHERE Name = :NAME_SYSTEM_USER][0].Id;
    }
    
    static final String NL = '<br />\n';

    @HttpGet
    global static Account doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account result = [SELECT Id, Name, Phone, Website FROM Account WHERE Id = :accountId];
        return result;
    }
  /*
        Immediate Requirement   data type
Name    Expression/Lookup   YES string
LastName    IIF(ISNULL(customerName), IIF(ISNULL(businessName), 'None', businessName), customerName)    YES string
Street  address YES string
Country country YES string
Phone   telephone   YES string
Email   email   YES string
Territory__c    region  YES string
Business_Name__c    businessName    YES string
Annual_Income__c    householdIncome YES string
Net_Worth_Range_Start__c    netWorthRangeStart  YES int
Net_Worth_Range_End__c  netWorthRangeEnd    YES int
Employment_Status__c    employmentStatus    YES string
Language_Preference__c  language    YES string
Business_Type__c    businessType    YES string
Bankruptcy__c   IIF( UPPER(isBankrupt)='TRUE', TRUE, FALSE) YES boolean
Employer__c employerName    YES string
Annual_Income_Currency__c   householdIncomeCurrency YES string
Net_Worth_Currency__c   netWorthCurrency    YES string
Citizenship__c  citizenship YES string
Type__c type    YES string
Detail_of_Business__c   businessTypeDetails YES string
Detail_of_Products_Services__c  productsServicesDetails YES string
Username__c username    YES string
Birthdate__c    IIF(IS_DATE(birthdate), birthdate, null)    YES date
Client_Status__c    entityAccess    YES 
Account Type        YES string
Division        YES int
Customer Since      YES date
Registration Source     YES string
PEFP        YES boolean
Bankruptcy Status       YES boolean
Bankruptcy          YES string
Security Employee       YES boolean
Security Employee Explanation       YES string
SSN     YES string
Employer        YES string
Employment Position     YES string
Intermediary        YES boolean
Intermediary Specification      YES string
Other Guarantor     YES string
Status      YES string
Industry        YES string
Description of Products and Services        YES string
Aux User Data       YES depends on the field
List of legal agreements        YES array of consents. Each consent consists of agreement name (string) agreement version (string)
EID Verification Responses      YES this is a structure, contents vary by eid provider

  */
  
	final static Map<String, String> paramNameByLeadFieldName = new Map<String, String> {
		'RecordTypeId'				=> 'recordType', 
		'LastName'					=> 'lastname', 
		'Street'					=> 'address', 
		'Country'					=> 'country', 
		'Email'						=> 'email', 
		'Phone'						=> 'phone', 
		'Employer__c'				=> 'employerName', 
		'Detail_of_Business__c'		=> 'employerBusinessDetails', 
		'Detail_of_Products_Services__c'	=> 'productsServicesDetails', 
		'Bankruptcy__c'				=> 'declaredBankruptcy', 
		'Employment_Status__c'		=> 'employmentStatus', 
		'Language_Preference__c'	=> 'languageCode', 
		'Citizenship__c'			=> 'citizenship',
		'Birthdate__c'				=> 'birthdate', // Date
		'Division_Name__c'			=> 'divisionname',
		'Net_Worth_Currency__c'		=> 'netWorthCurrencyType',
		'Net_Worth_Range_Start__c'	=> 'networthstart',
		'Net_Worth_Range_End__c'	=> 'networthend',
		'Annual_Income__c'			=> 'income',
		'Trading_Objective__c'		=> 'tradingObjective',
		'Singapore_Resident__c'		=> 'singapore_resident',
		'Commodities_Experience_Years__c'	=> 'commoditiesExperienceYears',
		'Equities_Experience_Years__c'	=> 'equityExperienceYears',
		'Forex_Experience_Years__c'	=> 'forexExperienceYears',
		'Futures_Experience_Years__c'	=> 'futuresExperienceYears',
		'Securities_Experience_Years__c'	=> 'securitiesExperienceYears',
		'Singapore_Dependent__c'	=> 'oapIsSingaporeDependent',
		'Singapore_Experience_Details__c'	=> 'oapSingaporeExperienceDetails',
		'Singapore_Has_Traded_CFDs__c'	=> 'oapHasTradedCfds',
		'Singapore_Has_Traded_Forex__c'	=> 'oapHasTradedForex',
		'EID_Result__c'				=> 'eid'
	};
	
	final static Map<String, String> paramNameByAccountFieldName = new Map<String, String> {
		'LastName'					=> 'lastname', 
		'PersonMailingStreet'		=> 'address', 
		'PersonMailingCountry'		=> 'country', 
		'PersonEmail'				=> 'email', 
		'Phone'						=> 'phone', 
		'Employer__pc'				=> 'employerName', 
		'Detail_of_Business__c'		=> 'employerBusinessDetails', 
		'Detail_of_Products_Services__c'	=> 'productsServicesDetails', 
		'Bankruptcy__c'				=> 'declaredBankruptcy', 
		'Employment_Status__pc'		=> 'employmentStatus', 
		'Language_Preference__pc'	=> 'languageCode', 
		'Citizenship__pc'			=> 'citizenship',
		'PersonBirthdate'			=> 'birthdate', // Date
		'Division_Name__c'			=> 'divisionname',
		'Net_Worth_Currency__pc'	=> 'netWorthCurrencyType',
		'Net_Worth_Range_Start__pc'	=> 'networthstart',
		'Net_Worth_Range_End__pc'	=> 'networthend',
		'Annual_Income__c'			=> 'income',
		'Email_Validated__c'		=> 'emailvalidated', // Boolean
		'Trading_Objective__pc'		=> 'tradingObjective',
		'Singapore_Resident__pc'	=> 'singapore_resident',
		'Commodities_Experience_Years__pc'	=> 'commoditiesExperienceYears',
		'Equities_Experience_Years__pc'	=> 'equityExperienceYears',
		'Forex_Experience_Years__pc'	=> 'forexExperienceYears',
		'Futures_Experience_Years__pc'	=> 'futuresExperienceYears',
		'Securities_Experience_Years__pc'	=> 'securitiesExperienceYears',
		'Singapore_Dependent__c'	=> 'oapIsSingaporeDependent',
		'Singapore_Experience_Details__c'	=> 'oapSingaporeExperienceDetails',
		'Singapore_Has_Traded_CFDs__c'	=> 'oapHasTradedCfds',
		'Singapore_Has_Traded_Forex__c'	=> 'oapHasTradedForex',
		'EID_Result__c'				=> 'eid'
	};
	
	final static Map<String, String> paramNameByFxAccountFieldName = new Map<String, String> {
		'RecordTypeId'				=> 'recordType', 
		'Name'						=> 'username', 
		'Funnel_Stage__c'			=> 'funnelstage',
		'Email_Validated__c'		=> 'emailvalidated' // Boolean
	};
	
	final static Set<String> fieldNamesDates = new Set<String> { 'Birthdate__c', 'PersonBirthdate' };
	final static Set<String> fieldNamesBoolean = new Set<String> { 'Email_Validated__c', 'Singapore_Dependent__pc', 'Singapore_Has_Traded_CFDs__pc', 'Singapore_Has_Traded_Forex__pc', 'Singapore_Dependent__c', 'Singapore_Has_Traded_CFDs__c', 'Singapore_Has_Traded_Forex__c' };
	final static Set<String> fieldNamesJson = new Set<String> { 'aux_user__c' };
	final static Set<String> fieldNamesJsonAppend = new Set<String> { 'EID_Result__c' };
	
    @HttpPost
    global static String doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        //decode the field value from request body
        String reqBody = '';
        if(req.requestBody != null){
        	reqBody = req.requestBody.toString().trim();
        }
        
        
        Map<String, String> paramValueByname = parseRequestBody(reqBody);

		String username = paramValueByname.get('username');
        if(username==null || username=='') {
        	return 'Username is required';
        }
		String recordType = paramValueByname.get('recordType');
		
		Id fxAccountRecTypeId = getRecordTypeId(recordType, 'fxAccount__c');
		if(fxAccountRecTypeId == null){
			return 'No Record Type or Invalid Record Type';
		}
        
        fxAccount__c fxa = getFxAccount(username, fxAccountRecTypeId);
        
        Lead l;
        Account a;
        Boolean updateLead = false;
        Boolean updateAccount = false;
        Boolean updateFxAccount = false;
        
        Database.DMLOptions dmo;
		if(fxa==null) {
			// create a new lead
			l = new Lead(OwnerId=getSystemUserId());
			updateLead = true;
			dmo = new Database.DMLOptions();
		    dmo.assignmentRuleHeader.useDefaultRule = true;
			
			fxa = new fxAccount__c();
			updateFxAccount = true;
		} else {
			// update
			if(fxa.Lead__c!=null) {
				String query = 'SELECT Id, '+ SoqlUtil.getSoqlFieldList(paramNameByLeadFieldName.keySet()) +' FROM Lead WHERE IsConverted=false AND Id=\'' + fxa.Lead__c + '\'';
				l = (Lead) Database.query(query)[0];
			} else if(fxa.Account__c!=null) {
				String query = 'SELECT Id, '+ SoqlUtil.getSoqlFieldList(paramNameByAccountFieldName.keySet()) +' FROM Account WHERE Id=\'' + fxa.Account__c + '\'';
				a = (Account) Database.query(query)[0];
			}
		}
		
		if(l!=null) {
			for(String fieldName : paramNameByLeadFieldName.keySet()) {
				String paramValue = paramValueByname.get(paramNameByLeadFieldName.get(fieldName)); 
				System.debug('fieldName: ' + fieldName);
				System.debug('paramNameByLeadFieldName.get(fieldName): ' + paramNameByLeadFieldName.get(fieldName));
				System.debug('paramValue: ' + paramValue);
				updateLead |= updateSObject(l, fieldName, paramValue);
			}
		}
		
		if(a!=null) {
			for(String fieldName : paramNameByAccountFieldName.keySet()) {
				String paramValue = paramValueByname.get(paramNameByAccountFieldName.get(fieldName)); 
				updateAccount |= updateSObject(a, fieldName, paramValue);
			}
		}
		
		for(String fieldName : paramNameByFxAccountFieldName.keySet()) {
			String paramValue = paramValueByname.get(paramNameByFxAccountFieldName.get(fieldName)); 
			updateFxAccount |= updateSObject(fxa, fieldName, paramValue);
		}
		
		if(updateLead) {
			System.debug('l: ' + l);
			System.debug('dmo: ' + dmo);
			if(dmo!=null) {
				Database.insert(l, dmo);
			} else {
				upsert l;
			}
			
			System.debug('l: ' + l);
			
			// check to see if the lead was converted
			l = [SELECT Id, IsConverted, ConvertedAccountId, Email FROM Lead WHERE Email=:l.Email][0];
			
			System.debug('fxa: ' + fxa);
			if(l.IsConverted==true) {
				fxa.Account__c = l.ConvertedAccountId;
				updateFxAccount = true;
			} else if(fxa.Lead__c != l.Id) {
				fxa.Lead__c = l.Id;
				updateFxAccount = true;
			}
		}
		
		if(updateAccount) {
			upsert a;
			
			if(fxa.Account__c != a.Id) {
				fxa.Account__c = a.Id;
				updateFxAccount = true;
			}
		}
		
		if(updateFxAccount) {
			upsert fxa;
		}
		
		return 'Done.';
    }

	static Boolean updateSObject(Sobject so, String fieldname, String updateValue) {
		if(updateValue!=null && updateValue!='' && so.get(fieldname)!=updateValue) {
			if(isDateField(fieldname)) {
				so.put(fieldname, Date.valueOf(updateValue)); //TODO, do proper parsing
			} else if(isBooleanField(fieldname)) {
				so.put(fieldname, (updateValue.toLowerCase()=='true')?true:false);
			} else if(isLanguageField(fieldname)) {
				so.put(fieldname, CustomSettings.getLanguageByCode(updateValue));
			} else if(isCountryField(fieldname)) {
				so.put(fieldname, updateValue);
				so.put('Territory__c', CustomSettings.getRegionByCountry(updateValue));
			} else if(isDivisionField(fieldname)) {
				so.put(fieldname, getDivisionName(updateValue));
			} else if(isRecordTypeField(fieldname)) {
				so.put(fieldname, getRecordTypeId(updateValue, so.getSObjectType().getDescribe().getName()));
			} else if(isJsonField(fieldname)) {
				so.put(fieldname, jsonToString(updateValue));
			} else if(isJsonAppendField(fieldname)) {
				String oldValue = (String) so.get(fieldname);
				if(oldValue==null || oldValue=='') {
					oldValue='';
				} else {
					oldValue = NL + oldValue;
				}
				so.put(fieldname, jsonToString(updateValue) + oldValue);
			} else {
				so.put(fieldname, updateValue);
			}
			return true;
		}
		return false;
	}
	
	static Boolean isDateField(String fieldname) {
		return fieldNamesDates.contains(fieldname);
	}
	
	static Boolean isBooleanField(String fieldname) {
		return fieldNamesBoolean.contains(fieldname);
	}
	
	static Boolean isJsonField(String fieldname) {
		return fieldNamesJson.contains(fieldname);
	}
	
	static Boolean isJsonAppendField(String fieldname) {
		return fieldNamesJsonAppend.contains(fieldname);
	}
	
	static Boolean isCountryField(String fieldname) {
		return (fieldname=='Country' || fieldname=='PersonMailingCountry');
	}
	
	static Boolean isLanguageField(String fieldname) {
		return (fieldname.startsWith('Language_Preference'));
	}
	
	static Boolean isDivisionField(String fieldname) {
		return (fieldname=='Division_Name__c');
	}
	
	static Boolean isRecordTypeField(String fieldname) {
		return (fieldname=='RecordTypeId');
	}
	
	static String jsonToString(String resp) {
		JSONParser parser = JSON.createParser(resp);
		String result = '';
		while (parser.nextToken()!= null) {
			if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
				String fieldName = parser.getText();
				parser.nextToken();
				if(fieldName=='Result') {
					result += fieldName + ': <b>' + parser.getText() + '</b>' + NL;
				} else {
					result += fieldName + ': ' + parser.getText() + NL;
				}
			}
		}
		return result;
	}

	static Id getRecordTypeId(String tradeOrDemo, String obj) {
		if(obj=='fxAccount__c') {
			if(tradeOrDemo=='trade') {
				return RecordTypeUtil.getFxAccountLiveId();
			} else if(tradeOrDemo=='demo') {
				return RecordTypeUtil.getFxAccountPracticeId();
			}
		}
		if(obj=='Lead') {
			if(tradeOrDemo=='trade') {
				return RecordTypeUtil.getLeadRetailId();
			} else if(tradeOrDemo=='demo') {
				return RecordTypeUtil.getLeadRetailPracticeId();
			}
		}
		return null;
	}
	
	static Map<String, String> divisionNameByShortName = new Map<String, String> {
		'OC'	=> 'OANDA Corporate',
		'OCAN'	=> 'OANDA Canada',
		'OAP'	=> 'OANDA Asia-Pacific',
		'OEL'	=> 'OANDA Europe',
		'OAU'	=> 'OANDA Australia',
		'OJ'	=> 'OANDA Japan'
	};
	
	static String getDivisionName(String shortname) {
		String result = divisionNameByShortName.get(shortname);
		if(result==null) {
			return shortname;
		} else {
			return result;
		}
	}
	
	/*
    global static String doPost2(
        String email,
        String firstname, 
        String lastname, 
        String username, 
        String divisionname, 
        String phone, 
        String birthdate, 
        String citizenship, 
        String networthstart, 
        String networthend, 
        String income, 
        String emailvalidated, 
        String funded, 
        String funnelstage, 
        String address, 
        String country, 
        String region,
        String businessname,
        String employmentstatus,
        String language,
        String businesstype,
        String isbankrupt,
        String employername,
        String incomecurrency,
        String networthcurrency,
        String type,
        String businesstypedetails,
        String productsservicesdetails,
        String entityaccess,
        String securityanswer,
        String securityquestion,
        String accounttype,
        String divisionnumber,
        String customersince,
        String registrationsource,
        String pefp,
        String bankruptcystatus,
        String bankruptcy,
        String issecurityemployee,
        String securityemployeeexplanation,
        String ssn,
        String employer,
        String employerposition,
        String isintermediary,
        String intermediaryspecification,
        String otherguarantor,
        String industry
    ) {
        Account a;
        Lead l;
        
        if(username==null || username='') {
        	return 'Username is required';
        }
        
        fxAccount__c fxa = getLiveFxAccount(username);
        
        if(fxa!=null && fxa.Account__c!=null) {
	        List<Account> accounts = [SELECT Name, PersonEmail FROM Account WHERE Id=:fxa.Account__c];
	        System.assert(accounts.size()<=1, 'More than 1 account with email: ' + email);

            a = accounts[0];
            
            boolean doUpdate = false;
            if(firstname!='') {
                a.LastName = firstname + ' ' + lastname;
                doUpdate=true;
            }
            if(phone!='') {
                a.Phone = phone;
                doUpdate=true;
            }
            if(citizenship!='') {
                a.Citizenship__pc = citizenship;
                doUpdate=true;
            }
            // TODO: check date format
            if(birthdate!='') {
                a.PersonBirthdate = Date.parse(birthdate);
                doUpdate=true;
            }
            if(divisionname!='') {
                a.Division_Name__c = divisionname;
            }
            if(networthstart!='') {
                a.Net_Worth_Range_Start__pc = networthstart;
                doUpdate=true;
            }
            if(networthend!='') {
                a.Net_Worth_Range_End__pc = networthend;
                doUpdate=true;
            }
            if(income!='') {
                a.Annual_Income__c = income;
                doUpdate=true;
            }
            // TODO: move to fxAccount
            /*
            if(funnelstage!='') {
                a.Funnel_Stage__c = funnelstage;
            }
            */
/*            if(emailvalidated!='') {
                a.Email_Validated__c = (emailvalidated=='true'?true:false);
                doUpdate=true;
            }
            if(doUpdate) {
                update a;
            }
            
        } else {
            // no account, check for lead
            if(fxa!=null && fxa.Lead__c!=null) {
	            List<Lead> leads = [SELECT LastName, Email, FirstName, Phone, Citizenship__c, Division_Name__c, Net_Worth_Range_Start__c, Birthdate__c, Net_Worth_Range_End__c, Annual_Income__c, Funnel_Stage__c, Email_Validated__c FROM Lead WHERE Id=:fxa.Lead__c];
	            System.assert(leads.size()<=1, 'More than 1 lead with email: ' + email);
                boolean doUpdate = false;
                l = leads[0];
                if(firstname!='') {
                    l.LastName = firstname + ' ' + lastname;
                    doUpdate=true;
                }
                if(phone!='') {
                    l.Phone = phone;
                    doUpdate=true;
                }
                if(citizenship!='') {
                    l.Citizenship__c = citizenship;
                    doUpdate=true;
                }
                if(birthdate!='') {
                    l.Birthdate__c = Date.valueOf(birthdate);
                    doUpdate=true;
                }
                if(divisionname!='') {
                    l.Division_Name__c = divisionname;
                }
                if(networthstart!='') {
                    l.Net_Worth_Range_Start__c = networthstart;
                    doUpdate=true;
                }
                if(networthend!='') {
                    l.Net_Worth_Range_End__c = networthend;
                    doUpdate=true;
                }
                if(income!='') {
                    l.Annual_Income__c = income;
                    doUpdate=true;
                }
                if(funnelstage!='') {
                    l.Funnel_Stage__c = funnelstage;
                }
                if(emailvalidated!='') {
                    l.Email_Validated__c = (emailvalidated=='true'?true:false);
                    doUpdate=true;
                }
                if(doUpdate) {
                    update l;
                }
            } else {
                l = new Lead();
                l.LastName = firstname + ' ' + lastname;
                l.Email = email;
                l.Phone = phone;
                l.Citizenship__c = citizenship;
                l.Birthdate__c = Date.valueOf(birthdate);
                l.Net_Worth_Range_Start__c = networthstart;
                l.Net_Worth_Range_End__c = networthend;
                l.Annual_Income__c = income;
                l.Division_Name__c = divisionname;
                l.Email_Validated__c = (emailvalidated=='true'?true:false);
                l.RecordTypeId = RecordTypeUtil.getLeadRetailId();
                l.OwnerId = getSystemUserId();
                l.Funnel_Stage__c = funnelstage;
                insert l;
            }
        }
        
        if(fxa!=null) {
            List<fxAccount__c> fxas = [SELECT Name, Lead__c, Account__c FROM fxAccount__c WHERE Name=:username AND RecordTypeId = :RecordTypeUtil.getLeadRetailId()];
            if(fxas.size()==1) {
                fxAccount__c f = fxas[0];
                boolean doUpdate = false;
                if(funnelstage!='') {
                    f.Funnel_Stage__c = funnelstage;
                }
                 if(doUpdate) {
                    update f;
                }
            }
        } else {
            fxAccount__c f = new fxAccount__c();
            f.Name = username;
            if(a!=null) {
                f.Account__c = a.Id;
            } else {
                f.Lead__c = l.Id;
            }
            insert f;
        }
        
        /*
        if(documentUrl!=null && documentUrl!='') {
            Case c = new Case();
            if(l!=null) {
                c.Lead__c = l.Id;
            }
            if(a!=null) {
                c.AccountId = a.Id;
            }
            c.Document_URL__c = documentUrl;
            
            insert c;
        }
        */
/*        return 'Done';
    }
  */  
    static fxAccount__c getFxAccount(String username, Id recordTypeId) {
		String query = 'SELECT Id, Lead__c, Account__c, '+ SoqlUtil.getSoqlFieldList(paramNameByFxAccountFieldName.keySet()) +' FROM fxAccount__c WHERE Name=\'' + username + '\' AND RecordTypeId = \'' + recordTypeId + '\'';
		List<fxAccount__c> fxas = Database.query(query);

		for(fxAccount__c fxa : fxas) {
			// case sensitive match
			if(fxa.Name.equals(username)) {
				return fxa;
			}
		}
		return null;
    }
    
    private static Map<String, String> parseRequestBody(String body){
    	
    	
    	Map<String, String> fieldValueByName = new Map<String, String>();
    	
    	JSONParser parser = JSON.createParser(body);

		while (parser.nextToken() != null) {
		
		    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
		
		        String fieldName = parser.getText();
		        parser.nextToken();
		        
		        String fieldValue = parser.getText();
                
                fieldValueByName.put(fieldName, fieldValue);
			}
		 
		}
    	
    	
    	return fieldValueByName;
    }
}