/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-15-2022
 * @last modified by  : Ariel Niubo
 **/
public with sharing class ComboPicklistController {
	@AuraEnabled(cacheable=true)
	public static List<PicklistInfoWrapper> getPicklistInfo(
		String objName,
		String picklistField
	) {
		List<SelectOption> picklistInfo = PicklistUtils.getPicklistValues(
			objName,
			picklistField
		);
		List<PicklistInfoWrapper> wrapper = new List<PicklistInfoWrapper>();
		for (SelectOption option : picklistInfo) {
			wrapper.add(new PicklistInfoWrapper(option));
		}
		return wrapper;
	}
	public with sharing class PicklistInfoWrapper {
		@AuraEnabled
		public String label { get; private set; }
		@AuraEnabled
		public String value { get; private set; }
		public PicklistInfoWrapper(SelectOption option) {
			this.label = option.getLabel();
			this.value = option.getValue();
		}
	}
}