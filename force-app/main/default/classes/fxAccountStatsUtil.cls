public without sharing class fxAccountStatsUtil {
	
	private static boolean allow_Practice_FirstCreated = false;
	private static boolean allow_Practice_FirstTradeDate = false;
	private static boolean allow_Practice_LastLoggedDate = false;
	private static boolean allow_Practice_last7DaysTradeCount = false;
	private static boolean allow_Practice_last7DaysRealizedGL = false;
	private static boolean allow_Practice_lastUnrealizedGL = false;
    
    //The input is list of live fxAccounts
    public static void processLiveCreationTime(List<fxAccount__c> liveAccounts){
    	if(liveAccounts.size() == 0){
    		System.debug('No Live fxAccounts to process');
    		return;
    	}
    	
    	System.debug('Number of Live fxAccounts: ' + liveAccounts.size());
    	
    	//get related Account and lead IDs
    	Set<ID> accountIds = new Set<ID>();
    	Set<ID> leadIds = new Set<ID>();
    	
    	for(fxAccount__c fxa : liveAccounts){
    		
    		if(fxa.Account__c != null){
    			accountIds.add(fxa.Account__c);
    		}else if(fxa.Lead__c != null){
    			leadIds.add(fxa.Lead__c);
    		}
    	}
    	
    	//get all related fxAccounts
    	List<fxAccount__c> relatedFxAccounts = [select id, Account__c, Lead__c, fxTrade_Created_Date_Adjusted__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and (Account__c in :accountIds or Lead__c in : leadIds)];
    	
    	System.debug('Total Number of Related fxAccounts: ' + relatedFxAccounts.size());
    	
    	Map<Id, List<fxAccount__c>> fxAccountsByAccountId = new Map<Id, List<fxAccount__c>>();
    	Map<Id, List<fxAccount__c>> fxAccountsByLeadId = new Map<Id, List<fxAccount__c>>();
    	
    	for(fxAccount__c fxa : relatedFxAccounts){
    		List<fxAccount__c> fxAccList = null;
    		
    		if(fxa.Account__c != null){
    			fxAccList = fxAccountsByAccountId.get(fxa.Account__c);
    			
    			if(fxAccList == null){
    				fxAccList = new List<fxAccount__c>();
    			}
    			
    			fxAccList.add(fxa);
    			fxAccountsByAccountId.put(fxa.Account__c, fxAccList);
    			
    		}else if(fxa.Lead__c != null){
    			fxAccList = fxAccountsByLeadId.get(fxa.Lead__c);
    			
    			if(fxAccList == null){
    				fxAccList = new List<fxAccount__c>();
    			}
    			
    			fxAccList.add(fxa);
    			fxAccountsByLeadId.put(fxa.Lead__c, fxAccList);
    		}
    		
    	}
    	
    	//process accounts
    	if(fxAccountsByAccountId.size() > 0){
    		updateAccounts(accountIds, fxAccountsByAccountId);
    	}
    	
    	//process leads
    	if(fxAccountsByLeadId.size() > 0){
    		updateLeads(leadIds, fxAccountsByLeadId);
    	}
    	
    	System.debug('processLiveCreationTime Finished');
    }
    
    private static void updateAccounts(Set<Id> accountIds, Map<Id, List<fxAccount__c>> fxAccountsByAccountId){
    	List<Account> accountsToUpdate = [select id, First_Live_fxAccount_DateTime__c from Account where Id in :accountIds];
    	if(accountsToUpdate.size() == 0){
    		return;
    	}
    	
    	for(Account acct : accountsToUpdate){
    		List<fxAccount__c> fxAccList = fxAccountsByAccountId.get(acct.Id);
    		
    		DateTime firstCreated = getFirstCreatedDateTime(fxAccList);
    		acct.First_Live_fxAccount_DateTime__c = firstCreated;
    	}
    	
    	System.debug('Number of accounts to update: ' + accountsToUpdate.size());
    	update accountsToUpdate;
    }
    
    private static void updateLeads(Set<ID> leadIds, Map<Id, List<fxAccount__c>> fxAccountsByLeadId){
    	List<Lead> leadsToUpdate = [select id, First_Live_fxAccount_DateTime__c from Lead where Id in : leadIds];
    	if(leadsToUpdate.size() == 0){
    		return;
    	}
    	
    	for(Lead ld : leadsToUpdate){
    		List<fxAccount__c> fxAccList = fxAccountsByLeadId.get(ld.Id);
    		
    		DateTime firstCreated = getFirstCreatedDateTime(fxAccList);
    		ld.First_Live_fxAccount_DateTime__c = firstCreated;
    	}
    	
    	System.debug('Number of leads to update: ' + leadsToUpdate.size());
    	update leadsToUpdate;
    }
    
    public static DateTime getFirstCreatedDateTime(List<fxAccount__c> fxAccList){
    	DateTime firstCreated = null;
    	
    	if(fxAccList == null || fxAccList.size() == 0){
    		return firstCreated;
    	}
    	
    	for(fxAccount__c fxa : fxAccList){
    		if(firstCreated == null || firstCreated > fxa.fxTrade_Created_Date_Adjusted__c){
    			firstCreated = fxa.fxTrade_Created_Date_Adjusted__c;
    		}
    	}
    	
    	return firstCreated;
    }
    
    //The input is list of practice fxAccounts
    public static void processPracticeStats(List<fxAccount__c> practiceAccounts){
    	if(practiceAccounts.size() == 0){
    		System.debug('No Practice fxAccounts to Process');
    		return;
    	}
    	
    	System.debug('Number of Practice fxAccounts: ' + practiceAccounts.size());
    	
    	//get related Account and lead IDs
    	Set<ID> accountIds = new Set<ID>();
    	Set<ID> leadIds = new Set<ID>();
    	
    	for(fxAccount__c fxa : practiceAccounts){
    		
    		if(fxa.Account__c != null){
    			accountIds.add(fxa.Account__c);
    		}else if(fxa.Lead__c != null){
    			leadIds.add(fxa.Lead__c);
    		}
    	}
    	
    	//get all related fxAccounts
    	List<fxAccount__c> relatedFxAccounts = [select id, Account__c, Lead__c, fxTrade_Created_Date_Adjusted__c, First_Trade_Date_Practice__c, last7DaysRealizedGL__c, last7DaysTradeCount__c, LastLoggedDate__c, 	lastUnrealizedGL__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountPracticeId() and (Account__c in :accountIds or Lead__c in : leadIds)];
    	
    	System.debug('Total Number of Related fxAccounts: ' + relatedFxAccounts.size());
    	
    	Map<Id, List<fxAccount__c>> fxAccountsByAccountId = new Map<Id, List<fxAccount__c>>();
    	Map<Id, List<fxAccount__c>> fxAccountsByLeadId = new Map<Id, List<fxAccount__c>>();
    	
    	for(fxAccount__c fxa : relatedFxAccounts){
    		List<fxAccount__c> fxAccList = null;
    		
    		if(fxa.Account__c != null){
    			fxAccList = fxAccountsByAccountId.get(fxa.Account__c);
    			
    			if(fxAccList == null){
    				fxAccList = new List<fxAccount__c>();
    			}
    			
    			fxAccList.add(fxa);
    			fxAccountsByAccountId.put(fxa.Account__c, fxAccList);
    			
    		}else if(fxa.Lead__c != null){
    			fxAccList = fxAccountsByLeadId.get(fxa.Lead__c);
    			
    			if(fxAccList == null){
    				fxAccList = new List<fxAccount__c>();
    			}
    			
    			fxAccList.add(fxa);
    			fxAccountsByLeadId.put(fxa.Lead__c, fxAccList);
    		}
    		
    	}
    	
    	//process accounts
    	if(fxAccountsByAccountId.size() > 0){
    		updateAccountsPracticeStats(accountIds, fxAccountsByAccountId);
    	}
    	
    	//process leads
    	if(fxAccountsByLeadId.size() > 0){
    		updateLeadsPracticeStats(leadIds, fxAccountsByLeadId);
    	}
    	
    	
    }
    
    private static void updateAccountsPracticeStats(Set<Id> accountIds, Map<Id, List<fxAccount__c>> fxAccountsByAccountId){
    	List<Account> accountsToUpdate = [select id, First_Practice_fxAccount_DateTime__c from Account where Id in :accountIds];
    	if(accountsToUpdate.size() == 0){
    		return;
    	}
    	
    	for(Account acct : accountsToUpdate){
    		List<fxAccount__c> fxAccList = fxAccountsByAccountId.get(acct.Id);
    		
    		//The first Created Datetime
    		DateTime firstCreated = getFirstCreatedDateTime(fxAccList);
    		acct.First_Practice_fxAccount_DateTime__c = firstCreated;
    		
    	}
    	
    	System.debug('Number of accounts to update: ' + accountsToUpdate.size());
    	update accountsToUpdate;
    }
    
    private static void updateLeadsPracticeStats(Set<ID> leadIds, Map<Id, List<fxAccount__c>> fxAccountsByLeadId){
    	List<Lead> leadsToUpdate = [select id, First_Practice_fxAccount_DateTime__c, recordTypeId, First_Trade_Date__c, LastLoggedDate__c, lastUnrealizedGL__c, last7DaysRealizedGL__c, last7DaysTradeCount__c from Lead where Id in : leadIds];
    	if(leadsToUpdate.size() == 0){
    		return;
    	}
    	
    	for(Lead ld : leadsToUpdate){
    		List<fxAccount__c> fxAccList = fxAccountsByLeadId.get(ld.Id);
    		
    		//The first Created Datetime
    		if(allow_Practice_FirstCreated){
    			DateTime firstCreated = getFirstCreatedDateTime(fxAccList);
    			ld.First_Practice_fxAccount_DateTime__c = firstCreated;
    		}
    		
    		//only update these stats on practice leads
    		if(ld.recordTypeId == RecordTypeUtil.getLeadRetailPracticeId()){
    			//The first First Trade Datetime
    			if(allow_Practice_FirstTradeDate){
    				DateTime firstTradeDateTime = getFirstTradeDateTime(fxAccList);
    				ld.First_Trade_Date__c = firstTradeDateTime;
    			}
    			
    			
    			//The latest Last Login Datetime
    			if(allow_Practice_LastLoggedDate){
    				DateTime lastLoginTime = getLatestLastLoginDateTime(fxAccList);
    				ld.LastLoggedDate__c = lastLoginTime;
    			}
    			
    			
    			//The sum of last7DaysTradeCount__c
    			if(allow_Practice_last7DaysTradeCount){
    				Integer tradeCount = (Integer)sumFxAccountField(fxAccList, 'last7DaysTradeCount__c');
    				ld.last7DaysTradeCount__c = tradeCount;
    			}
    			
    			
    			//The sum of last7DaysRealizedGL__c
    			if(allow_Practice_last7DaysRealizedGL){
    				Decimal last7DaysRealizedGL = sumFxAccountField(fxAccList, 'last7DaysRealizedGL__c');
    				ld.last7DaysRealizedGL__c = last7DaysRealizedGL;
    			}
    			
    			
    			//The sum of lastUnrealizedGL__c
    			if(allow_Practice_lastUnrealizedGL){
    				Decimal lastUnrealizedGL = sumFxAccountField(fxAccList, 'lastUnrealizedGL__c');
    				ld.lastUnrealizedGL__c = lastUnrealizedGL;
    			}
    			
    		}
    		
    		
    	}
    	
    	System.debug('Number of leads to update: ' + leadsToUpdate.size());
    	update leadsToUpdate;
    }
    
    private static DateTime getFirstTradeDateTime(List<fxAccount__c> fxAccList){
    	DateTime firstTradeDateTime = null;
    	
    	if(fxAccList == null || fxAccList.size() == 0){
    		return firstTradeDateTime;
    	}
    	
    	for(fxAccount__c fxa : fxAccList){
    		if(firstTradeDateTime == null || (fxa.First_Trade_Date_Practice__c != null && fxa.First_Trade_Date_Practice__c < firstTradeDateTime)){
    			firstTradeDateTime = fxa.First_Trade_Date_Practice__c;
    		}
    	}
    	
    	return firstTradeDateTime;
    }
    
    private static DateTime getLatestLastLoginDateTime(List<fxAccount__c> fxAccList){
    	DateTime lastLoginDateTime = null;
    	
    	if(fxAccList == null || fxAccList.size() == 0){
    		return lastLoginDateTime;
    	}
    	
    	for(fxAccount__c fxa : fxAccList){
    		if(lastLoginDateTime == null || (fxa.LastLoggedDate__c != null && fxa.LastLoggedDate__c > lastLoginDateTime)){
    			lastLoginDateTime = fxa.LastLoggedDate__c;
    		}
    	}
    	
    	return lastLoginDateTime;
    }
    
    private static Decimal sumFxAccountField(List<fxAccount__c> fxAccList, String fieldName){
    	Decimal result = null;
    	
    	if(fxAccList == null || fxAccList.size() == 0){
    		return result;
    	}
    	
    	for(fxAccount__c fxa : fxAccList){
    		if(fxa.get(fieldName) != null){
    			
    			result = (result == null ? 0 : result) + (Decimal)fxa.get(fieldName);
    		}
    		
    	}
    	
    	return result;
    }
    
    public static void loadConfig(String settingName){
    	Practice_fxAccount_Setting__c setting = Practice_fxAccount_Setting__c.getValues(settingName);
    	
    	if(setting != null){
    		System.debug('Load Practice fxAccount Setting');
    		System.debug(setting);
    		
    		allow_Practice_FirstCreated = setting.Allow_fxTradeCreateDate__c;
	        allow_Practice_FirstTradeDate = setting.Allow_First_Trade_Date__c;
	        allow_Practice_LastLoggedDate = setting.Allow_LastLoggedDate__c;
	        allow_Practice_last7DaysTradeCount = setting.Allow_last7DaysTradeCount__c;
	        allow_Practice_last7DaysRealizedGL = setting.Allow_last7DaysRealizedGL__c;
	        allow_Practice_lastUnrealizedGL = setting.Allow_lastUnrealizedGL__c;
    	}
    }
}