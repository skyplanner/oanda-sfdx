/**
 * @File Name          : NonHVCCaseSRoutingInfoManager.cls
 * @Description        : 
 * Creates ServiceRoutingInfo records from PendingServiceRouting records
 * linked to Non_HVC_Case__c object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/31/2024, 1:28:55 AM
**/
public inherited sharing class NonHVCCaseSRoutingInfoManager 
	extends BaseSRoutingInfoManager {

	public NonHVCCaseSRoutingInfoManager() {
		super();
	}

	public override List<BaseServiceRoutingInfo> getRoutingInfoList() {
		if (psrByWorkItemId.isEmpty()) {
			return null;
		}
		// else...
		CaseSRoutingInfoManager routingInfoManager = 
			new CaseSRoutingInfoManager();
			
		List<Non_HVC_Case__c> nonHvcCaseList = [
			SELECT Case__c
			FROM Non_HVC_Case__c
			WHERE Id IN :psrByWorkItemId.keySet()
		];

		for (Non_HVC_Case__c nonHvcCaseObj : nonHvcCaseList) {
			PendingServiceRouting routingObj = 
				psrByWorkItemId.get(nonHvcCaseObj.Id);
			routingInfoManager.registerPendingServiceRouting(
				routingObj, // pendingRoutingObj
				nonHvcCaseObj.Case__c // workItemId
			);
		}
		List<BaseServiceRoutingInfo> result = 
			routingInfoManager.getRoutingInfoList();
		return result;
	}

}