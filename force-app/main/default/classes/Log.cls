public class Log {
    
    // Log types
    public enum Type {INFO, DEBUG, WARNING, ERROR, EXCEPT}

    public String id;
    public String logType;
    public String category;
    public String msg;
    public String details;

    /**
     * Constructor
     */
    public Log(
        Type logType,
        String category,
        String msg,
        String details)
    {
        id = getId();
        this.logType = logType.name();
        this.category = category;
        
        if(String.isNotBlank(msg)) {
            this.msg = msg.abbreviate(255);
        }

        if(String.isNotBlank(details)) {
            this.details = details.abbreviate(131072);
        }
    }

    /**
     * Constructor
     */
    public Log(LogEvt__e lEvent)
    {
        id = lEvent.Id__c;
        logType = lEvent.Type__c;
        category = lEvent.Category__c;
        msg = lEvent.Message__c;
        details = lEvent.Details__c;
    }

    /**
     * Get Log platform event
     */
    public LogEvt__e getEvent() {
        return
            new LogEvt__e(
                Type__c = logType,
                Category__c = category,
                Message__c = msg,
                Details__c = details,
                Id__c = id);
    }

    /**
     * Get log record
     */
    public Log__c getRecord()
    {
        return
            new Log__c(
                Id__c = id,
                Type__c = logType,
                Category__c = category,
                Message__c = msg,
                Details__c = details);
    }

    /**
     * Know if log is a debug
     */
    public Boolean isDebug() {
        return logType == Type.DEBUG.name();
    }

    /**
	 * @return An external Id for the new log.
	 */
	private String getId() {
		return 
            UserUtil.currentUser.Id + '@' + 
            Datetime.now().getTime();
	}

}