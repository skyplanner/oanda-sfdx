/**
 * @Description  : Test class for EmailTemplateSelector.
 * @Author       : Jakub Fik
 * @Date         : 2024-06-13
 **/
@isTest
private class EmailTemplateSelectorTest {
    /**
     * @description Success test for onInitDefaults method.
     * @author Jakub Fik | 2024-06-13
     **/
    @IsTest
    static void onInitDefaultsTest() {
        Boolean isNotError = true;
        Case testCase = new Case(RecordTypeId = RecordTypeUtil.getOTMSSupportCaseRecordTypeId());
        insert testCase;
        List<Map<String, Object>> defaultSettingAsObject = new List<Map<String, Object>>{
            new Map<String, Object>{
                'targetSObject' => new EmailMessage(
                    ValidatedFromAddress = 'salesforce@test.com'
                ),
                'contextId' => testCase.Id,
                'actionType' => 'SendEmail',
                'actionName' => 'Case.EmailOtms',
                'fromAddressList' => new List<String>{ 'salesforce@test.com' }
            }
        };

        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = (List<QuickAction.SendEmailQuickActionDefaults>) JSON.deserialize(
            JSON.serialize(defaultSettingAsObject),
            List<QuickAction.SendEmailQuickActionDefaults>.class
        );

        Test.startTest();
        try {
            EmailTemplateSelector emailSelector = new EmailTemplateSelector();
            emailSelector.onInitDefaults(defaultsSettings);
        } catch (Exception ex) {
            isNotError = false;
        }
        Test.stopTest();
        System.assert(isNotError, 'Method should not throw error.');
    }
}