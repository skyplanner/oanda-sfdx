/**
 * @File Name          : CheckBusinessHoursActionTest.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 5:10:22 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/29/2024, 5:09:06 PM   aniubo     Initial Version
**/
@isTest
private class CheckBusinessHoursActionTest {

	@isTest
	private static void testCheckBusinessHours() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<Boolean> results = CheckBusinessHoursAction.checkBusinessHours();
		Test.stopTest();
		System.assertEquals(true, results != null,'Results should not be null');
		// Asserts
	}
	@isTest
	private static void testCheckBusinessHoursEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean hasError = false;
		// Actual test
		Test.startTest();
		try {
			List<Boolean> results = CheckBusinessHoursAction.checkBusinessHours();
		} catch (Exception ex) {
			hasError = true;
		}
		Test.stopTest();
		System.assertEquals(true, hasError,'Exception should be thrown');
		// Asserts
	}

}