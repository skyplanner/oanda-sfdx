/**
 * @File Name          : FxAccountMIFIDValidationManager.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 09-16-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/16/2020   acantero     Initial Version
**/
public without sharing class FxAccountMIFIDValidationManager implements Queueable {

    List<Id> fxAccountIdList;
    Boolean updateExistingOpenCases;

    public FxAccountMIFIDValidationManager(
        List<Id> fxAccountIdList,
        Boolean updateExistingOpenCases
    ) {
        this.fxAccountIdList = fxAccountIdList;
        this.updateExistingOpenCases = updateExistingOpenCases;
    }

    public static void validatefxAccounts(
        List<Id> fxAccountIdList,
        Boolean updateExistingOpenCases
    ) {
        if ((fxAccountIdList == null) || (fxAccountIdList.isEmpty())) {
    		return;
    	}
        //else...
        System.enqueueJob(
            new FxAccountMIFIDValidationManager(
                fxAccountIdList,
                updateExistingOpenCases
            )
        );
    }

    public void execute(QueueableContext context) {
        executeValidation();     
    }

    public void executeValidation(){
        if ((fxAccountIdList == null) || (fxAccountIdList.isEmpty())) {
    		return;
        }
        //else        
       for(List<fxAccount__c> fxAccountList : [
            SELECT
                Id,
                Name,
                National_Personal_ID__c,
                Passport_Number__c,
                Citizenship_Nationality__c,
                Last_Trade_Date__c,
                Account__c,
                Division_Name__c,
                Contact__c,
                (
                    SELECT 
                        Id, 
                        Valid__c, 
                        fxAccount__c, 
                        LastModifiedDate  
                    FROM 
                        MiFID_Validation_Results__r 
                    ORDER BY LastModifiedDate DESC 
                    LIMIT 1
                )
            FROM
                fxAccount__c
            WHERE
                Id IN :fxAccountIdList
            AND
                Last_Trade_Date__c <> NULL
            AND
                (
                    Last_Trade_Date__c = THIS_MONTH
                    OR
                    Last_Trade_Date__c = LAST_N_MONTHS:5
                )
            ORDER BY 
                Citizenship_Nationality__c ASC
        ]){      
            FxAccountMIFIDValidator validator = 
                new FxAccountMIFIDValidator(
                    fxAccountList, 
                    updateExistingOpenCases
                );
            validator.validate();
        }
    }
    /* 
    This method was called from fxAccount before update trigger, 
    now is unused becouse there is a Process Builder on fxAccount for the same 
    */
    public static void checkFxAccountMifidStatus(
        List<fxAccount__c> fxAccountList,
        Map<ID,fxAccount__c> oldRecordsMap
    ) {
        if (
            (fxAccountList == null) ||
            fxAccountList.isEmpty() ||
            (oldRecordsMap == null) ||
            oldRecordsMap.isEmpty()
        ) {
            return;
        }
        //else...  
        Map<Id, MiFID_Validation_Result__c> fxAccountValidResult = getfxAccountValidationResult(fxAccountList);
        List<MiFID_Validation_Result__c> toUpdate = new List<MiFID_Validation_Result__c>();
        for(fxAccount__c fxAccountObj : fxAccountList) {
            fxAccount__c oldRecord = oldRecordsMap.get(fxAccountObj.Id);   
            MiFID_Validation_Result__c resultObj = fxAccountValidResult.get(fxAccountObj.Id);
            if (mifidValStatusMustChange(fxAccountObj, oldRecord, resultObj)) {
                resultObj.Valid__c = false;
                toUpdate.add(resultObj);            
            }
        }
        
        if(toUpdate.size()>0)
          update toUpdate; 
    }

    @testVisible
    static Boolean mifidValStatusMustChange(
        fxAccount__c fxAccountObj,
        fxAccount__c oldRecord,
        MiFID_Validation_Result__c resultObj
    ) {
        if (
            (fxAccountObj == null) || 
            (oldRecord == null)
        ) {
            return false;
        }
        //else...
        Boolean isValidated = (resultObj != null ) ?
                               resultObj.Valid__c :
                               false;
        System.debug('isValidated ' + isValidated);
        System.debug('fxAccObj ' + fxAccountObj);                               
        System.debug('oldRecord ' + oldRecord);                               
        Boolean result = (
            (isValidated == true) &&
            (
                (fxAccountObj.Citizenship_Nationality__c != oldRecord.Citizenship_Nationality__c) ||
                (fxAccountObj.National_Personal_ID__c != oldRecord.National_Personal_ID__c) ||
                (fxAccountObj.Passport_Number__c != oldRecord.Passport_Number__c)
            )
        );
        System.debug('Result ' + result);
        return result;
    }
    /** this method is call from an InvocableAction called from fxAccount Process Builder when Person Information is changed */
    public static void resetValidationResult(List<Id> fxAccountIdList){
        if ((fxAccountIdList == null) || (fxAccountIdList.isEmpty())) {
    		return;
        }
        //else
        List<MiFID_Validation_Result__c> fxAccountResultValidList = [SELECT Id, 
                                                                            Valid__c, 
                                                                            fxAccount__c, 
                                                                            LastModifiedDate  
                                                                    FROM MiFID_Validation_Result__c 
                                                                    WHERE fxAccount__c IN :fxAccountIdList
                                                                    AND Valid__c = TRUE];

        if(fxAccountResultValidList == null || fxAccountResultValidList.size() == 0 )                                                                      
           return;
        //else
        for(MiFID_Validation_Result__c validResultObj : fxAccountResultValidList){
            validResultObj.Valid__c = false;
        }
        
        update fxAccountResultValidList;
    }
    
    /* this method is call from fxAccount before delete trigger */
    public static void removeResultValidationsRelated(Set<Id> fxAccountIdList){
        if ((fxAccountIdList == null) || (fxAccountIdList.isEmpty())) {
    		return;
        }
        //else
        List<MiFID_Validation_Result__c> fxAccountResultValidList = [SELECT Id, 
                                                                            Valid__c, 
                                                                            fxAccount__c, 
                                                                            LastModifiedDate  
                                                                    FROM MiFID_Validation_Result__c 
                                                                    WHERE fxAccount__c IN :fxAccountIdList];
                                                                    
        if(fxAccountResultValidList == null || fxAccountResultValidList.size() == 0 )                                                                      
           return;      
        //else                                                        
        delete fxAccountResultValidList;
    }
    
    /* This method is call from an InvocableAction for PB Case Mifid Validation */
    public static void updateValidationResultFromCase(List<Id> caseList){
        if ((caseList == null) || (caseList.isEmpty())) {
    		return;
        }
        //else
        
      List<Id> fxAccountIdList = new List<Id>();
      List<Case> caseObjectList = [SELECT Id, fxAccount__c, MiFID_validated__c  
                                   FROM Case 
                                   WHERE Id IN: caseList AND fxAccount__c != ''];
    
      for(Case caseObj : caseObjectList){
         fxAccountIdList.add(caseObj.fxAccount__c);
      }
       
      Map<Id, MiFID_Validation_Result__c> fxAccountValidResult = new Map<Id, MiFID_Validation_Result__c>();
    
      for(MiFID_Validation_Result__c resultObj : [SELECT Id, 
                                                         Valid__c, 
                                                         fxAccount__c, 
                                                         LastModifiedDate 
                                                  FROM MiFID_Validation_Result__c 
                                                  WHERE fxAccount__c IN :fxAccountIdList]){
           fxAccountValidResult.put(resultObj.fxAccount__c, resultObj); 
      }

      List<MiFID_Validation_Result__c> toUpdate = new List<MiFID_Validation_Result__c>();
      
      for(Case caseObj : caseObjectList){
        MiFID_Validation_Result__c  resultObj = fxAccountValidResult.get(caseObj.fxAccount__c) !=  null? 
                                                fxAccountValidResult.get(caseObj.fxAccount__c) :
                                                new MiFID_Validation_Result__c();
        resultObj.Valid__c = caseObj.MiFID_validated__c;
        resultObj.fxAccount__c = caseObj.fxAccount__c;// for those who are new;
        toUpdate.add(resultObj);
      }

      if(toUpdate.size()>0)
        upsert toUpdate;
    }

    static Map<Id, MiFID_Validation_Result__c> getfxAccountValidationResult(List<fxAccount__c> fxAccountList){
     
        Map<Id, MiFID_Validation_Result__c> fxAccountValidResult = new  Map<Id, MiFID_Validation_Result__c>();
        if (
            (fxAccountList == null) ||
            fxAccountList.isEmpty() 
        ) {
            return fxAccountValidResult;
        }
        //else
        Set<Id> fxAccountsIds = new Set<Id>();
        for(fxAccount__c fxAccountObj : fxAccountList) {
            fxAccountsIds.add(fxAccountObj.Id);
        }

        for (MiFID_Validation_Result__c valResultObj : [SELECT Id, fxAccount__c, Valid__c, LastModifiedDate 
                                                        FROM MiFID_Validation_Result__c
                                                        WHERE fxAccount__c IN :fxAccountsIds]) {
            fxAccountValidResult.put(valResultObj.fxAccount__c, valResultObj);        
        }

        return fxAccountValidResult;

    }

}