/**
 * Description : Email Service Handler to service Partner emails.
 * Author: ejung
 * Date : 2024-04-22
 */
public with sharing class AffiliateEmailHandler implements Messaging.InboundEmailHandler {
    
    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        try{
            // Look up partner record based on the email toAddresses
            List<Affiliate__c> partners = [SELECT Id 
                                        FROM Affiliate__c 
                                        WHERE Email__c IN :email.toAddresses AND
                                            (NOT Email__c LIKE '@oanda.com')
                                        LIMIT 1];

            // Look up internal user based on the email fromAddress
            List<User> internalUsers = [SELECT Id, Email 
                                        FROM User 
                                        WHERE Email =: email.fromAddress
                                        LIMIT 1];
            
            // if partner record or internal user exist, do not create a task                                        
            if(partners.isEmpty() || internalUsers.isEmpty()) {
                result.success = false;
                return result;
            }

            Affiliate__c partner = partners.get(0);
            User internalUser = internalUsers.get(0);
            
            // Add a new email task related to the first found partner record
            Task newTask = new Task(
                Subject = 'Email: ' + email.subject,
                WhatId = partner.Id,
                Status = 'Completed',
                ActivityDate = System.today(),
                Type = 'Email',
                TaskSubtype = 'Email',
                Description =  'Additional To: ' + email.toAddresses + '\n' +
                    'CC: ' + email.ccAddresses + '\n\n' + 
                    'Subject: ' + email.subject + '\n' +
                    'Body: ' + email.plainTextBody,
                OwnerId = internalUser.Id
            );
            insert newTask;
        }
        catch(Exception e){
            result.success = false;
            result.message = 'An error occurred while processing the email.'+e.getMessage();
        }

        // Set success to true only if no exception occurred
        if (result.success != false) {
            result.success = true;
        }
        return result;
    }
}