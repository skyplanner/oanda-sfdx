/**
 * @File Name          : DataCategoryTopArticlesHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 11/4/2019, 3:48:13 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   acantero     Initial Version
**/
@isTest
private class DataCategoryTopArticlesHelper_Test {

	static final String TEST_DIVISION_1 = 'TestDivision1';
	static final String TEST_CATEGORY_1 = 'TestCategory1';
	static final String TEST_CATEGORY_2 = 'TestCategory2';
	static final String TEST_CATEGORY_3 = 'TestCategory3';

	@testSetup
	static void setup () {
	    ArticlesTestDataFactory.createTopArticles(TEST_DIVISION_1, TEST_CATEGORY_1, TEST_CATEGORY_2, TEST_CATEGORY_3);
	}

	@isTest
	static void getFaqCategories_test() {
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(TEST_DIVISION_1, null);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategories_test1() {
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(TEST_DIVISION_1, 'en_US');
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategories_test2() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		settingsMgr.faqCategories = null;
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(null, null);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategories_test3() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = new List<String>{'About_OANDA'};
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(faqCategories, TEST_DIVISION_1, 'es');
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategories_test4() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = new List<String>{'abcdefgh'};
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(faqCategories, TEST_DIVISION_1, 'es');
		Test.stopTest();
		System.assertNotEquals(null, result);
	}


	@isTest
	static void getFaqCategories_test5() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = new List<String>{TEST_CATEGORY_1, TEST_CATEGORY_2, TEST_CATEGORY_3};
		Map<String,String> catMap = new Map<String,String>{
			TEST_CATEGORY_1 => 'Test Category 1',
			TEST_CATEGORY_2 => 'Test Category 2',
			TEST_CATEGORY_3 => 'Test Category 3'
		};
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(faqCategories, catMap, TEST_DIVISION_1, null);
		Test.stopTest();
		Integer expectedCount = (settingsMgr.restrictTopArticles) ? 3 : 4;
		System.assertNotEquals(null, result);
		System.assertEquals(3, result.size());
		DataCategoryWrapper cat1 = result[0];
		System.assertEquals(expectedCount, cat1.topArticles.size());
	}

	@isTest
	static void getFaqCategories_test6() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = new List<String>{TEST_CATEGORY_1, TEST_CATEGORY_2, TEST_CATEGORY_3};
		Map<String,String> catMap = new Map<String,String>{
			TEST_CATEGORY_1 => 'Test Category 1',
			TEST_CATEGORY_2 => 'Test Category 2',
			TEST_CATEGORY_3 => 'Test Category 3'
		};
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(faqCategories, catMap, TEST_DIVISION_1, 'en_US');
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getFaqCategories_test7() {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = new List<String>();
		Map<String,String> catMap = null;
		String division = null;
		Test.startTest();
		List<DataCategoryWrapper> result = DataCategoryTopArticlesHelper.getFaqCategories(faqCategories, catMap, division, null);
		Test.stopTest();
		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
	}

	@isTest
	static void dataCategoryWrapper_test() {
		Test.startTest();
		DataCategoryWrapper catWrapper = new DataCategoryWrapper('name1', 'label1');
		TopArticleWrapper artW1 = new TopArticleWrapper(
			'1', 
			'1',
			'1', 
			'some title', 
			'some url', 
			1
		);
		TopArticleWrapper artW2 = new TopArticleWrapper(
			'2', 
			'2',
			'2', 
			'some title', 
			'some url', 
			2
		);
		TopArticleWrapper artW3 = new TopArticleWrapper(
			'3', 
			'3',
			'3', 
			'some title', 
			'some url', 
			3
		);
		catWrapper.addArticle(artW2);
		catWrapper.addArticle(artW1);
		catWrapper.addArticle(artW3);
		Test.stopTest();
		System.assertNotEquals(null, catWrapper.topArticles);
		System.assertEquals(3, catWrapper.topArticles.size());
	}

}