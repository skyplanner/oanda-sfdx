/**
 * Created by Agnieszka Kajda on 24/11/2023.
 */

@IsTest
public class ChatTranscriptEventControllerTest {
    @IsTest
    private static void getTranscriptEventsTest() {
        Case c = new Case();
        insert c;

        LiveChatVisitor v = new LiveChatVisitor();
        insert v;

        LiveChatTranscript lct = new LiveChatTranscript();
        lct.LiveChatVisitorId = v.Id;
        lct.CaseId = c.Id;
        insert lct;

        Id u = UserInfo.getUserId();
        LiveChatTranscriptEvent ret = new LiveChatTranscriptEvent(Detail='test', Time=System.now(), Type='Enqueue', LiveChatTranscriptId=lct.Id, AgentId=u);
        insert ret;

        System.assertEquals(1, ChatTranscriptEventsController.getTranscriptEvents(lct.Id).size());
    }
    @IsTest
    private static void getTranscriptEventsEmptyTest() {
        System.assertEquals(new List<LiveChatTranscriptEvent>(), ChatTranscriptEventsController.getTranscriptEvents(null));
    }


}