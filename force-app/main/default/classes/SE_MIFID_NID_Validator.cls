/**
 * @File Name          : SE_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/5/2020, 2:08:48 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   acantero     Initial Version
**/
public class SE_MIFID_NID_Validator implements ExpressionValidator {

    public static final Integer YEAR_OF_BIRTH_LENGTH = 4;
    public static final Integer MAX_AGE = 100;
    public static final Integer MIN_AGE = 18;


    public String validate(String val) {
        if (String.isEmpty(val)) {
            return null;
        }
        //else...
        if (val.length() < YEAR_OF_BIRTH_LENGTH) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        String yearOfBirthStr = val.substring(0, YEAR_OF_BIRTH_LENGTH);
        if (!yearOfBirthStr.isNumeric()) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        Integer yearOfBirth = Integer.valueOf(yearOfBirthStr);
        Integer currentYear = Date.today().year();
        Integer age = currentYear - yearOfBirth;
        if (
            (age < MIN_AGE) ||
            (age > MAX_AGE)
        ) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        Boolean luhnCheck = SPValidationUtils.verifyLuhnCheckDigit(val);
        if (luhnCheck != true) {
            return System.Label.MifidValPasspChecksumError;
        }
        //else...
        return null;
    }

}