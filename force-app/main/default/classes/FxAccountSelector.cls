public inherited sharing class FxAccountSelector {

    public static FxAccount__c getLiveFxAccountByUserId(Integer userId) {
        String globalId = userId + '+' + fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE.to15();
        List <FxAccount__c> fxAccounts = [
            SELECT Id, Authentication_Code__c, Authentication_Code_Expires_On__c, Lead__c, Account__c
            FROM FxAccount__c
            WHERE fxTrade_Global_ID__c = :globalId
        ];

        if (!fxAccounts.isEmpty()) {
            return fxAccounts[0];
        }

        return null;
    }

    public static FxAccount__c getFxAccountByOneId(String oneId) {
        List <FxAccount__c> fxAccounts = [
            SELECT Id, Authentication_Code__c, Authentication_Code_Expires_On__c
            FROM FxAccount__c
            WHERE fxTrade_One_Id__c = :oneId
        ];

        if (!fxAccounts.isEmpty()) {
            return fxAccounts[0];
        }

        return null;
    }
}