/**
 * @File Name          : CreateMessagingCaseCommentsAction.cls
 * @Description        : Saves "Case Comment" records from the 
 *                       "Messaging Comment" records associated 
 *                       with the "Messaging Session"
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/14/2023, 5:56:11 PM
**/
public without sharing class CreateMessagingCaseCommentsAction { 

	@InvocableMethod(label='Create Messaging Case Comments' callout=false)
	public static void createMessagingCaseComments(List<ActionInfo> infoList) {
		try {
			ExceptionTestUtil.execute();  
			 
			if (
				(infoList == null) ||
				infoList.isEmpty()
			) {
				return;
			}
			// else...     
			ActionInfo info = infoList[0];
			MessagingCommentProcess process = new MessagingCommentProcess(
				info.messagingSessionId // messagingSessionId
			);
			process.createCaseComments(info.caseId);

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				CreateMessagingCaseCommentsAction.class.getName(),
				ex
			);
		}
	} 

	// *******************************************************************

	public class ActionInfo {

		@InvocableVariable(label = 'messagingSessionId' required = true)
		public String messagingSessionId;

		@InvocableVariable(label = 'caseId' required = true)
		public String caseId;

	}

}