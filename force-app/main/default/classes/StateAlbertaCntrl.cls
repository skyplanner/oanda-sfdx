/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-07-2022
 * @last modified by  : Yaneivys Gutierrez
**/
global without sharing class StateAlbertaCntrl {
    @AuraEnabled
    global static List<Account> getAccountRecord(String accountId) {
        return [SELECT Id, State_Alberta__c FROM Account WHERE Id = :accountId];
    }

    @AuraEnabled
    global static void userResponse(String accountId, Boolean resp) {
        List<Account> accList = [SELECT Id, State_Alberta__c FROM Account WHERE Id = :accountId];
        if (accList.size() > 0) {
            Account acc = accList[0];
            if (resp) {
                // Keep
                acc.State_Alberta__c = false;
                update acc;
            }
            else {
                // Clean
                acc.State_Alberta__c = false;
                acc.PersonMailingState = null;
                update acc;
            }
        }
    }
}