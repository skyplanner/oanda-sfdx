/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 01-19-2023
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class RelatedListWrapper {
    @AuraEnabled
    public String mdtName { get; set; }

    @AuraEnabled
    public String objApiName { get; set; }

    @AuraEnabled
    public String childRelationshipName { get; set; }

    @AuraEnabled
    public List<String> fieldLabels { get; set; }

    @AuraEnabled
    public List<String> apiNames { get; set; }

    @AuraEnabled
    public List<Schema.DisplayType> types { get; set; }

    @AuraEnabled
    public List<Boolean> linkToRecord { get; set; }
    
    @AuraEnabled
    public List<RelatedListWrapper> childrenWrapper { get; set; }
    
    @AuraEnabled
    public List<RelatedListRecord> records { get; set; }

    public RelatedListWrapper() {
        fieldLabels = new List<String>();
        apiNames = new List<String>();
        types = new List<Schema.DisplayType>();
        linkToRecord = new List<Boolean>();
        childrenWrapper = new List<RelatedListWrapper>();
        records = new List<RelatedListWrapper.RelatedListRecord>();
    }

    public class RelatedListRecord {
        @AuraEnabled
        public Id id { get; set; }

        @AuraEnabled
        public List<RelatedListField> fields { get; set; }
    
        @AuraEnabled
        public List<RelatedListWrapper> children { get; set; }
        
        public RelatedListRecord() {
            fields = new List<RelatedListWrapper.RelatedListField>();
            children = new List<RelatedListWrapper>();
        }
    }

    public class RelatedListField {
        @AuraEnabled
        public String label { get; set; }

        @AuraEnabled
        public String apiName { get; set; }

        @AuraEnabled
        public Object value { get; set; }        

        @AuraEnabled
        public Map<String, Object> type { get; set; }
        
        public RelatedListField() {
        }
    }
}