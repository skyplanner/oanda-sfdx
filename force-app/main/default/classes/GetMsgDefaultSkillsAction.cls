/**
 * @File Name          : GetMsgDefaultSkillsAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/18/2024, 12:31:16 PM
**/
public without sharing class GetMsgDefaultSkillsAction { 

	@InvocableMethod(label='Get Messaging Default Skills' callout=false)
	public static List<List<ID>> getMsgDefaultSkills() {
		List<ID> skillsIds = new List<ID>();
		String msgSkillDevName = SPSettingsManager.getSetting(
			MessagingConst.MSG_SKILL_DEV_NAME_SETTING
		);
		Set<String> skillNameSet = new Set<String> { msgSkillDevName };
		List<Skill> skills = SkillRepo.getByDevNames(skillNameSet);

		for (Skill skillObj : skills) {
			skillsIds.add(skillObj.Id);
		}

		ServiceLogManager.getInstance().log(
			'getMsgDefaultSkills -> skillsIds', // msg
			GetMsgDefaultSkillsAction.class.getName(), // category
			skillsIds // details
		);
		List<List<ID>> result = new List<List<ID>> { skillsIds };
		return result;
	} 

}