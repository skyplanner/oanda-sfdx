/**
 * Tests: FxAccountConditionalUpdate
 * @author Fernando Gomez
 */
@isTest
private class FxAccountConditionalUpdateTest {
	/**
	 * public void execute_Deposit_USD_MTD_Update_DateTime()
	 */
	@isTest
	static void execute_Deposit_USD_MTD_Update_DateTime() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		
		fxAccount1 = new fxAccount__c(
			Name = 'lead.Email.1',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Deposit_USD_MTD__c = 530.00
		);

		fxAccount2 = new fxAccount__c(
			Name = 'lead.Email.2',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Deposit_USD_MTD__c = 650.00
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_Deposit_USD_MTD_Update_DateTime');
		System.assert(fxAccount2.Deposit_USD_MTD_Update_DateTime__c != null);
	}

	/**
	 * public void execute_Withdrawal_USD_MTD_Update_DateTime()
	 */
	@isTest
	static void execute_Withdrawal_USD_MTD_Update_DateTime() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		
		fxAccount1 = new fxAccount__c(
			Name = 'lead.Email.1',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Withdrawal_USD_MTD__c = 530.00
		);

		fxAccount2 = new fxAccount__c(
			Name = 'lead.Email.2',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Withdrawal_USD_MTD__c = 650.00
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_Withdrawal_USD_MTD_Update_DateTime');
		System.assert(fxAccount2.Withdrawal_USD_MTD_Update_DateTime__c != null);
	}

	/**
	 * public void execute_Withdrawal_USD_MTD_Update_DateTime()
	 */
	@isTest
	static void execute_fxAccount_Retail_Live() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		
		fxAccount1 = new fxAccount__c(
			Name = 'lead.Email.1',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Account__c = null,
			Lead__c = '00Q3C0000042qG9UAI',
			OwnerId = UserUtil.getSystemUserId()
		);

		fxAccount2 = new fxAccount__c(
			Name = 'lead.Email.2',
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Account__c = null,
			Lead__c = '00Q3C0000042qG9UAI',
			OwnerId = UserUtil.getSystemUserId()
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_fxAccount_Retail_Live');
		System.assert(fxAccount2.Trigger_Lead_Assignment_Rules__c);
	}

	/**
	 * public void execute_fxAccount_Set_CD_Defaults()
	 */
	@isTest
	static void execute_fxAccount_Set_CD_Defaults() {
		fxAccount__c fxAccount1;
		FxAccountConditionalUpdate updater;
		Integer defaultValue = Integer.valueOf(
			CurrentDeskSync.getConstantParameters().get('Account_Preference_Id'));
		
		fxAccount1 = new fxAccount__c(
			CD_Account_Preference_ID__c = null,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = null,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId()
		);

		updater = new FxAccountConditionalUpdate(fxAccount1, fxAccount1);
		updater.execute('WF_fxAccount_Set_CD_Defaults');
		System.assertEquals(defaultValue,
			Integer.valueOf(fxAccount1.CD_Account_Preference_ID__c));
	}	

	/**
	 * public void execute_fxAccount_Set_CD_Defaults()
	 */
	@isTest
	static void execute_fxAccount_Set_CD_Preference_ID() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		Map<Integer, Integer> toMap;
		toMap = CurrentDeskSync.getPreferenceMappingsInversed();
		
		fxAccount1 = new fxAccount__c(
			CD_Account_Preference_ID__c = 72,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 1234,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Info_Last_Updated__c= null
		);

		fxAccount2 = new fxAccount__c(
			CD_Account_Preference_ID__c = 72,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 5678,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			Info_Last_Updated__c = System.now().addDays(-15)
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_fxAccount_Set_CD_Preference_ID');

		// should not change since the value is not in the TO mapping
		System.assertEquals(72,
			Integer.valueOf(fxAccount2.CD_Account_Preference_ID__c));

		fxAccount1.CD_Account_Preference_ID__c = 83;
		fxAccount2.CD_Account_Preference_ID__c = 83;

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_fxAccount_Set_CD_Preference_ID');

		// should have changed
		System.assertEquals(toMap.get(83),
			Integer.valueOf(fxAccount2.CD_Account_Preference_ID__c));
	}

	/**
	 * public void execute_fxAccount_US_Shares_Trading_Set_CD_Preference_ID()
	 */
	@isTest
	static void usSharesTradingEnabledTest() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		Map<Integer, Integer> toMap;
		toMap = CurrentDeskSync.getPreferenceMappingsInversed();
		
		fxAccount1 = new fxAccount__c(
			CD_Account_Preference_ID__c = 72,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 1234,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			US_Shares_Trading_Enabled__c = false
		);

		fxAccount2 = new fxAccount__c(
			CD_Account_Preference_ID__c = 72,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 5678,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			US_Shares_Trading_Enabled__c = true
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_fxAccount_US_Shares_CD_Preference');
		
		System.assertEquals(114,
			Integer.valueOf(fxAccount2.CD_Account_Preference_ID__c),
			'invalid mapping');
	}

	/**
	 * public void execute_fxAccount_US_Shares_Trading_Set_CD_Preference_ID()
	 */
	@isTest
	static void usSharesTradingDisabledTest() {
		fxAccount__c fxAccount1, fxAccount2;
		FxAccountConditionalUpdate updater;
		Map<Integer, Integer> toMap;
		toMap = CurrentDeskSync.getPreferenceMappingsInversed();
		
		fxAccount1 = new fxAccount__c(
			CD_Account_Preference_ID__c = 114,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 1234,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			US_Shares_Trading_Enabled__c = true
		);

		fxAccount2 = new fxAccount__c(
			CD_Account_Preference_ID__c = 114,
			Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS,
			CD_Client_ID__c = 5678,
			RecordTypeId = RecordTypeUtil.getFxAccountLiveId(),
			US_Shares_Trading_Enabled__c = false
		);

		updater = new FxAccountConditionalUpdate(fxAccount2, fxAccount1);
		updater.execute('WF_fxAccount_US_Shares_CD_Preference');
		
		System.assertEquals(72,
			Integer.valueOf(fxAccount2.CD_Account_Preference_ID__c),
			'invalid mapping');
	}
}