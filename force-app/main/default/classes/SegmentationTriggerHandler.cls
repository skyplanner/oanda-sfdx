/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-16-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class SegmentationTriggerHandler extends TriggerHandler {
    List<Segmentation__c> newList;
    Map<Id, Segmentation__c> oldMap;

    public SegmentationTriggerHandler(
        List<Segmentation__c> newList, Map<Id, Segmentation__c> oldMap
    ) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    public override void afterInsert() {
        SegmentationManager segmentationManager = new SegmentationManager();
        segmentationManager.manageSegmentationActivated(newList, oldMap);
    }

    public override void afterUpdate() {
        SegmentationManager segmentationManager = new SegmentationManager();
        segmentationManager.manageSegmentationActivated(newList, oldMap);
    }

    public override void afterDelete() {
        SegmentationManager segmentationManager = new SegmentationManager();
        segmentationManager.manageSegmentationRemoved(oldMap.values());
    }
}