@isTest
private class NF_RestartChatApexControllerTest {
    testMethod static void testCommunityUrl(){
        test.startTest();
        String communityUrl = NF_RestartChatApexController.getCommunityURL();
        test.stopTest();
        system.assertNotEquals(null, communityUrl);
    }
}