/**
 * @File Name          : SLAViolationNotificationManagerFactory.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 3:53:29 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/19/2024, 2:30:13 PM   aniubo     Initial Version
 **/
public with sharing class SLAViolationNotificationManagerFactory {
	public static SLAViolationNotificationManager createNotificationManager(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAConst.AgentSupervisorModel supervisorModel,
		Map<String, Milestone_Time_Settings__mdt> milestonesByType,
		SupervisorProviderCreator supervisorProviderCreator
	) {
		SLAViolationNotificationManager manager;
		if (
			notificationObjectType ==
			SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT
		) {
			manager = new ChatNotificationManager(
				supervisorModel,
				violationType,
				milestonesByType,
				supervisorProviderCreator
			);
		} else if (
			notificationObjectType ==
			SLAConst.SLANotificationObjectType.CHAT_CASE_NOT ||
			notificationObjectType ==
			SLAConst.SLANotificationObjectType.EMAIL_CASE_NOT ||
			notificationObjectType ==
			SLAConst.SLANotificationObjectType.PHONE_CASE_NOT
		) {
			manager = new CaseChatNotificationManager(
				supervisorModel,
				violationType,
				milestonesByType,
				supervisorProviderCreator
			);
		} else if (
			notificationObjectType ==
			SLAConst.SLANotificationObjectType.MESSAGING_NOT
		) {
			manager = new MessagingNotificationManager(
				supervisorModel,
				violationType,
				milestonesByType,
				supervisorProviderCreator
			);
		}
		return manager;
	}
}