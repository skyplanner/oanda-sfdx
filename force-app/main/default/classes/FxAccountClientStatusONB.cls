public with sharing class FxAccountClientStatusONB extends OutgoingNotificationBus {
    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        FxAccount__c newfxAccount = (FxAccount__c) newRecord;
        FxAccount__c oldfxAccount = (FxAccount__c) oldRecord;
        if(newFxAccount.fxTrade_User_Id__c != null
            && fxAccountUtil.isLiveAccount(newFxAccount)
            && newFxAccount.Approved_to_Fund__c != null
            && newfxAccount.Approved_to_Fund__c != oldfxAccount.Approved_to_Fund__c
        ) {
            return JSON.serialize(newRecord);
        }

        return null;
    }

    public override String getEventKey(SObject newRecord) {
        
        return JSON.serialize(new Map<String, String> {
            'recordId' => String.valueOf(((fxAccount__c) newRecord).Id),
            'env' => 'live',
            'tradeUserId' => ((Integer)((fxAccount__c) newRecord).fxTrade_User_Id__c).toString()
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>)JSON.deserializeUntyped(key);
        recordId = (String) keys.get('recordId');

        UserApiStatus.StatusWrapper wrapper = 
            UserApiStatus.setApprovedToFund();
        new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME).updateStatus(
                new EnvironmentIdentifier((String) keys.get('env'), (String) keys.get('tradeUserId')),
                wrapper.status
        );
        // new UserCallout().updateStatus(
        //         new EnvironmentIdentifier((String) keys.get('env'), (String) keys.get('tradeUserId')),
        //         wrapper.status
        // );
    }

    /********************************* Helpers *********************************************/

    public override String getRecordId() {
        return recordId;
    }

}