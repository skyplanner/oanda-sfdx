public class BatchFormatLeadInfo implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

	public BatchFormatLeadInfo() {
		//Query for all non-converted Leads
		query = 'SELECT Id, Email, Phone FROM Lead';
	}

	public BatchFormatLeadInfo(String q) {
   		query = q;
  	}

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Email, Phone FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		LeadUtil.formatLeadInfo((Lead[]) batch);
	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		BatchDeleteLeadFieldHistory b = new BatchDeleteLeadFieldHistory();
		database.executebatch(b);
	}


}