/**
 * @File Name          : CommonSRoutingInfoManager.cls
 * @Description        : 
 * Base class for classes that implement the SRoutingInfoManager interface
 * by querying the records of the corresponding object
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/9/2024, 1:16:28 AM
**/
public inherited sharing abstract class CommonSRoutingInfoManager 
	extends BaseSRoutingInfoManager {

	public CommonSRoutingInfoManager() {
		super();
	}

	public override List<BaseServiceRoutingInfo> getRoutingInfoList() {
		if (psrByWorkItemId.isEmpty()) {
			return null;
		}
		//else...
		List<BaseServiceRoutingInfo> result = 
			new List<BaseServiceRoutingInfo>();
		Set<ID> workItemIdSet = psrByWorkItemId.keySet();
		List<SObject> workItems = getWorkItems(workItemIdSet);
		processWorkItems(workItems);

		for (SObject workItem : workItems) {
			PendingServiceRouting routingObj = 
				psrByWorkItemId.get(workItem.Id);
			result.add(
				getRoutingInfo(
					routingObj, // routingObj
					workItem // workItem
				)
			);
		}
		return result;
	}

	protected abstract List<SObject> getWorkItems(Set<ID> workItemIdSet);

	protected abstract BaseServiceRoutingInfo getRoutingInfo(
		PendingServiceRouting routingObj, 
		SObject workItem
	);

	protected virtual void processWorkItems(List<SObject> workItems) {
		// override in descendants
	}

}