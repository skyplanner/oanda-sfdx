/**
 * UserApiFieldLwc controller
 */
public class UserApiFieldLwcCtrl {

    /**
     * Get picklist options
     */
    @AuraEnabled
    public static List<Map<String, String>> getOptions(
        String fieldName)
    {
        try {
            List<Map<String, String>> options =
                new List<Map<String, String>>();

            for (Schema.PickListEntry entry :
                UserApiAppSettings.getInstance().fieldsMap.get(
                    fieldName).getDescribe().getPickListValues())
            {
                options.add(
                    new Map<String, String>{
                        'label' => entry.getLabel(),
                        'value' => entry.getValue()});
            }

            return options;
        } catch (Exception ex) {
            throw LogException.newInstance(
                Constants.LOGGER_USER_API_CATEGORY,
                ex);
        }
    }

}