/**
 * @File Name          : GB_MIFID_NID_Validator_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/5/2020, 1:28:37 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/2/2020   acantero     Initial Version
**/
@isTest
private class GB_MIFID_NID_Validator_Test {

    @isTest
    static void validate_test() {
        Test.startTest();
        String result1 = new GB_MIFID_NID_Validator().validate(null);
        String result2 = new GB_MIFID_NID_Validator().validate('OC123456A'); //valid
        String result3 = new GB_MIFID_NID_Validator().validate('A'); //invalid length
        String result4 = new GB_MIFID_NID_Validator().validate('NC123456A'); //invalid prefix
        Test.stopTest();
        System.assertEquals(null, result1);
        System.assertEquals(null, result2);
        System.assertEquals(System.Label.MifidValPasspFormatError, result3);
        System.assertEquals(System.Label.MifidValPasspFormatError, result4);
    }
}