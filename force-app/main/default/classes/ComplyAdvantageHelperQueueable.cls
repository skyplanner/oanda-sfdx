public with sharing class ComplyAdvantageHelperQueueable
    implements Queueable, Database.AllowsCallouts {
    
    // Processes to do 
    public enum Process {TURN_OFF_MONT_STATUS}

    // Specific process
    Process procs;
    // fxAccount ids
    Set<Id> fxAccountIds;

    /**
     * Constructor 1
     */
    public ComplyAdvantageHelperQueueable(
        Process procs,
        Set<Id> fxAccountIds)
    {
        this.procs = procs;
        this.fxAccountIds = fxAccountIds;
    }

    /**
	 * Truns off the Is_monitored flag on all searches related
	 * to the specified fxAccounts
	 * @param fxAccountIds
	 */
    public static void turnOffMonitorStatusForFxAccounts(
        Set<Id> fxAccountIds)
    {
        if(LimitsUtil.canEnqueueJob()) {
            System.enqueueJob(
                new ComplyAdvantageHelperQueueable(
                    Process.TURN_OFF_MONT_STATUS,
                    fxAccountIds));
        } else {
            ComplyAdvantageHelper.turnOffMonitorStatusForFxAccounts(
                fxAccountIds);
        }
    }

    /**
     * Queueable execute
     */
    public void execute(QueueableContext context) {
        doProcess();
    }

    /**
     * Do specific process
     * Processes
     * - turnOffMonitorStatusForFxAccounts
     * - more here...
     */
    private void doProcess() {
        switch on procs {
            when TURN_OFF_MONT_STATUS {
                ComplyAdvantageHelper.turnOffMonitorStatusForFxAccounts(
                    fxAccountIds);
            }
            // Add more process here...
        }
    }
    
}