/**
 * Wrapper for Comply advantage JSON: Search Comment
 * @author Fernando Gomez
 */
public class ComplyAdvantageSearchComment {
	@AuraEnabled
	public String id { get; set; }

	@AuraEnabled
	public String created_at { get; set; }

	@AuraEnabled
	public String message { get; set; }

	@AuraEnabled
	public String entity_id { get; set; }

	@AuraEnabled
	public Integer user_id { get; set; }

	@AuraEnabled
	public ComplyAdvantageUser user { get; set; }
}