/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-19-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public inherited sharing class ScheduleMailSettingRepo {
	public static Schedule_Mail_Setting__mdt getFirst() {
		List<Schedule_Mail_Setting__mdt> settingMtdList = [
			SELECT
				Id,
				AM_or_PM__c,
				Day_of_the_Week__c,
				Delete_a_Draft_Email__c,
				Scope__c,
				Time__c,
				Notification_Template_Failed_Message__c,
				Org_Address_Sender_Name__c
			FROM Schedule_Mail_Setting__mdt
			LIMIT 1
		];
		return settingMtdList.isEmpty() ? null : settingMtdList.get(0);
	}
}