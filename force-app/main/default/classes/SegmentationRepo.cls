/**
 * @File Name          : SegmentationRepo.cls
 * @Description        : 
 * @Author             : aniubo
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/15/2024, 4:56:56 PM
**/
public inherited sharing class SegmentationRepo {

	public static List<Segmentation__c> getActiveByFxAccount(
		Set<ID> fxAccountIds
	) {
		return [
			SELECT 
				fxAccount__c, 
				Seg_PL__c
			FROM Segmentation__c
			WHERE fxAccount__c IN :fxAccountIds 
			AND Active__c = true
		];
	}
}