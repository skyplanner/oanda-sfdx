/**
 * @File Name          : PrepareServiceRoutingInfoCmd.cls
 * @Description        : 
 * Complete missing information in ServiceRoutingInfo records
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:20:03 AM
**/
public inherited sharing class PrepareServiceRoutingInfoCmd {

	@TestVisible
	Map<String, ServiceRoutingInfo> infoByEmailMap;
	
	public PrepareServiceRoutingInfoCmd(
		List<BaseServiceRoutingInfo> infoList
	) {
		infoByEmailMap = 
			new Map<String, ServiceRoutingInfo>();
		
		if (infoList != null) {
			for (BaseServiceRoutingInfo info : infoList) {
				ServiceRoutingInfo routingInfo = (ServiceRoutingInfo) info;
				if (routingInfo.infoIsIncomplete == true) {
					infoByEmailMap.put(routingInfo.email, routingInfo);
				}
			}
		}
	}

	public Boolean execute() {
		if (infoByEmailMap.isEmpty()) {
			return false;
		}
		//else...
		completeAccountInformation();
		completeLeadInformation();
		return true;
	}

	@TestVisible
	void completeAccountInformation() {
		Set<String> emailSet = infoByEmailMap.keySet();

		if (emailSet.isEmpty()) {
			return;
		}
		//else...
		ServiceLogManager.getInstance().log(
			'completeAccountInformation -> emailSet', // msg
			PrepareServiceRoutingInfoCmd.class.getName(), // category
			emailSet // details
		);
		List<Account> accountList = [
			SELECT
				PersonEmail,
				Primary_Division_Name__c,
				Language_Preference__pc,
				Is_High_Value_Customer__c,
				Funnel_Stage__pc,
				Last_Trade_Date__c,
				fxAccount__c,
				CreatedDate
			FROM Account
			WHERE PersonEmail IN :emailSet
		];

		AccountSegmentationManager segmentationManager = 
			new AccountSegmentationManager();

		// All fxAccount Ids are registered first 
		// to be able to bulkify the search
		for (Account accountObj : accountList) {
			segmentationManager.registerFxAccount(accountObj.fxAccount__c);
		}

		for (Account accountObj : accountList) {
			ServiceRoutingInfo info = 
				infoByEmailMap.get(accountObj.PersonEmail);
			
			if (info != null) {
				// Segmentation must be assigned before 
				// setting up the account or lead
				info.segmentation =
					segmentationManager.getSegmentation(
						accountObj.fxAccount__c
					);
				info.setRelatedAccount(accountObj);
			}
			infoByEmailMap.remove(accountObj.PersonEmail);
		}
	}

	@TestVisible
	void completeLeadInformation() {
		Set<String> emailSet = infoByEmailMap.keySet();
		
		if (emailSet.isEmpty()) {
			return;
		}
		// else...
		ServiceLogManager.getInstance().log(
			'completeLeadInformation -> emailSet', // msg
			PrepareServiceRoutingInfoCmd.class.getName(), // category
			emailSet // details
		);
		List<Lead> leadList = [
			SELECT
				Email,
				fxAccount__c,
				Language_Preference__c,
				Division_from_Region__c,
				CreatedDate
			FROM Lead
			WHERE Email IN :emailSet
		];

		AccountSegmentationManager segmentationManager = 
			new AccountSegmentationManager();

		// All fxAccount Ids are registered first 
		// to be able to bulkify the search
		for (Lead leadObj : leadList) {
			segmentationManager.registerFxAccount(leadObj.fxAccount__c);
		}

		for (Lead leadObj : leadList) {
			ServiceRoutingInfo info = infoByEmailMap.get(leadObj.Email);

			if (info != null) {
				// Segmentation must be assigned before 
				// setting up the account or lead
				info.segmentation =
					segmentationManager.getSegmentation(
						leadObj.fxAccount__c
					);
				info.setRelatedLead(leadObj);
			}
			infoByEmailMap.remove(leadObj.Email);
		}
		checkMissingLeads();
	}

	/**
	 * The leads that are created in the ChatTranscript session are not yet
	 * available in the before insert of PendingServiceRouting, but it is 
	 * known that if the chat has an email associated with it and it is not
	 * linked to an account then it is linked to a lead. Therefore, even if
	 * there is no lead record, it is assumed that a lead exists in order to
	 * calculate the Tier.
	 */
	@TestVisible
	Boolean checkMissingLeads() {
		if (infoByEmailMap.isEmpty()) {
			return false;
		}
		// else...
		ServiceLogManager.getInstance().log(
			'checkMissingLeads -> emailSet', // msg
			PrepareServiceRoutingInfoCmd.class.getName(), // category
			infoByEmailMap.keySet() // details
		);
		
		for (ServiceRoutingInfo sRoutingInfo : infoByEmailMap.values()) {
			sRoutingInfo.calculateTierForNewLead();
		}
		return true;
	}
	
}