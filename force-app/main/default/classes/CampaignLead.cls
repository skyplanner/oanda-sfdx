/**
 * Take data from the denormalized Campaign_Lead__c object, and create campaign members
 * but also create leads if they don't already exist (based on email address)
 */
public with sharing class CampaignLead {
    
    static final String LEAD_SOURCE_WEBINAR = 'Webinar';
    static final String LEAD_SOURCE_EVENT = 'Event';
    static final String LEAD_SOURCE_CX = 'CX';
    static final String LEAD_SOURCE_REACTIVATION = 'Reactivation';
    static final String CAMPAIGN_NAME_PREFIX_CX = 'CX';
    static final String TASK_SUBJECT_ATTENDED_WEBINAR = 'Attended Webinar';
    static final String TASK_SUBJECT_ATTENDED_EVENT = 'Attended Event';
    static final String TASK_SUBJECT_FOLLOW_UP_CX = 'Follow Up CX';
    static final String TASK_STATUS_NOT_STARTED = 'Not Started';
    static final String TASK_STATUS_COMPLETED = 'Completed';
    
    static boolean isCxCampaignLead(Campaign_Lead__c cl) {
        if(cl.Lead_Source__c==LEAD_SOURCE_CX) {
            return true;
        } else {
            return false;
        }
    }

    static boolean isReactivationLead(Campaign_Lead__c cl) {
        if(cl.Lead_Source__c==LEAD_SOURCE_REACTIVATION) {
            return true;
        } else {
            return false;
        }
    }
    
    static boolean isEventCampaignLead(Campaign_Lead__c cl) {
        if(cl.Lead_Source__c==LEAD_SOURCE_EVENT) {
            return true;
        } else {
            return false;
        }
    }

    static boolean isReferrerCampaignLead(Campaign_Lead__c cl) {
        if(cl.RAF_Referrer_First_Name__c != null
        || cl.RAF_Referrer_Last_Name__c != null
        || cl.RAF_Referrer_Email_Address__c != null
        || cl.RAF_Referrer_Username__c != null) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * For new leads that need to be created and assigned
     */
     public static Lead getLead(Campaign_Lead__c cl)
    {
        Lead l = new Lead(
                        Title=cl.Title__c,
                        LastName=cl.Last_Name__c, 
                        FirstName=cl.First_Name__c, 
                        Email=cl.Email__c,
                        Company=cl.Company__c, 
                        City=cl.City__c, 
                        Country=cl.Country__c, 
                        PostalCode=cl.Zip_Postal_Code__c,
                        State=cl.StateProvince__c,
                        Phone=cl.Phone__c, 
                        Other_Phone__c=cl.Other_Phone__c,
                        LeadSource=cl.Lead_Source__c, 
                        OwnerId=UserUtil.getSystemUserId(),
                        Description=cl.Notes__c,
                        RAF_Referrer_First_Name__c = cl.RAF_Referrer_First_Name__c,
                        RAF_Referrer_Last_Name__c = cl.RAF_Referrer_Last_Name__c,
                        RAF_Referrer_Email_Address__c = cl.RAF_Referrer_Email_Address__c,
                        RAF_Referrer_Username__c = cl.RAF_Referrer_Username__c,
                        fxTrade_One_Id__c = cl.fxTrade_One_Id__c,
                        Division_Name__c = cl.Division_Name__c


                    );
        if(cl.Lead_Record_Type_Name__c!='') {
            Id leadRecordTypeId = RecordTypeUtil.getRecordTypeId(cl.Lead_Record_Type_Name__c, 'Lead');
            if(leadRecordTypeId!=null) {
                l.RecordTypeId = leadRecordTypeId;
            }
        }
        if(isCxCampaignLead(cl))
        {
            l.LeadSource=LEAD_SOURCE_CX;
        } else if(cl.Lead_Source__c==''){
            l.LeadSource=LEAD_SOURCE_WEBINAR; 
        }
        LeadUtil.useDefaultAssignment(l);
        return l;
    }
    
    /**
     * Add campaign membership for each lead
     */
    public static CampaignMember getCampaignMember(Campaign_Lead__c campaignLead, Lead lead)
    {
        // maintain links to the new records
        campaignLead.Contact__c = lead.ConvertedContactId;
        campaignLead.Lead__c = (lead.isConverted?null:lead.Id);
        campaignLead.Campaign__c = CampaignUtil.getIdByName(campaignLead.Campaign_Name__c);
        
        return new CampaignMember(
                                    CampaignId=CampaignUtil.getIdByName(campaignLead.Campaign_Name__c),
                                    LeadId=(lead.IsConverted?null:lead.Id),
                                    ContactId=lead.ConvertedContactId,
                                    Status=(campaignLead.Status__c!=null?campaignLead.Status__c:(campaignLead.Attended__c=='Yes'?'Responded':'Sent')),
                                    OTA_Tuition_Amount__c=campaignLead.OTA_Tuition_Amount__c
                                );
    }
    
    /**
     * Take a list of campaign leads, and add them to a campaign.  Create and/or assign the lead if required
     * Create followup task and send email as well.
     */
    public static void addToCampaign(Campaign_Lead__c[] campaignLeads)
    {
        Id systemUserId = UserUtil.getSystemUserId();
        boolean hasReactivationLead = false;
        boolean hasEventLead = false;
        boolean hasCreateTask = false;
        boolean hasDontCreateTask = false;

        // create the campaign for the webinar
        Set<String> campaignNames = new Set<String>();
        for(Campaign_Lead__c cl : campaignLeads) {
            ///If Campaign lead has an associated webinar/campaign, add it to the list
            if(cl.Campaign_Name__c!=null && cl.Campaign_Name__c!='')
            {
                campaignNames.add(cl.Campaign_Name__c);
            }
            
            CampaignUtil.createWebinars(campaignNames);        ///Should this be after the for loop?
            
            ///Does the Campaign Lead list have Reactivation or Event leads?
            if(isReactivationLead(cl)) {
                hasReactivationLead = true;
            } else if(isEventCampaignLead(cl)) {    
                hasEventLead = true;
            }
            
            ///Does the Campaign Lead list have instructions to create or not create tasks?
            if(cl.Create_Task__c==null) {
                // don't do anything/
            } else if(cl.Create_Task__c.toUpperCase()=='YES' || cl.Create_Task__c.toUpperCase()=='TRUE') {
                hasCreateTask = true;
            } else if(cl.Create_Task__c.toUpperCase()=='NO' || cl.Create_Task__c.toUpperCase()=='FALSE') {
                hasDontCreateTask = true;
            }
        }
        
        Map<String, Campaign_Lead__c[]> campaignLeadsByEmail = new Map<String, Campaign_Lead__c[]>();
        Map<String, Webinar_Survey__c[]> webinarSurveysByEmail = new Map<String, Webinar_Survey__c[]>();    ///Is this used?
        CampaignResponseManager manager = new CampaignResponseManager();
        
        ///Organize the Campaign Leads into a map, referenced by email.
        for(Campaign_Lead__c cl : campaignLeads) 
        {
            Campaign_Lead__c[] cls = campaignLeadsByEmail.get(cl.Email__c);
            
            if(cls==null)                ///If this email wasn't added yet, add it
            {
                cls = new List<Campaign_Lead__c>();
                campaignLeadsByEmail.put(cl.Email__c, cls);
            }
            cls.add(cl);                    ///Add the current campaign to this email
            
            manager.addCampaignLead(cl);    ///I think this is to make a list of people with all their associated campaign leads and opportunities etc.... maybe not
//            manager.addWebinarSurvey(cl.Email__c, getWebinarSurvey(cl));
        }

        // reassign any leads/opps that are unassigned or assigned to inactive users
        Map<String, Lead> existingLeadByEmail = new Map<String, Lead>();
        List<Lead> leadsToUpdate = new List<Lead>();
        List<Contact> contactToReferreUpdate = new List<Contact>();
        
        ///Reference the email addresses from the Campaign Lead list with the existing leads
        Boolean leadNeedToBeUpdated;
        for(Lead l : [SELECT Id, Email, OwnerId, Status, Owner.IsActive, IsConverted, 
                        ConvertedContactId, ConvertedOpportunityId, ConvertedOpportunity.Owner.IsActive, ConvertedOpportunity.StageName 
                    FROM Lead 
                    WHERE Email IN :campaignLeadsByEmail.keySet()]) {
            existingLeadByEmail.put(l.Email, l);
            leadNeedToBeUpdated = false;
            
            ///If the lead is not converted (i.e. not an opportunity and the list has no Reactivation leads in it)   
            ///Wait... so, if the list has a reactivation lead, we're not going to check the entire list?
            if(l.IsConverted==false && hasReactivationLead==false) { 
                ///Lead assigned to inactive sales guy or a sales guy who is part of a queue (a particular Sales team)
                if(l.Owner.IsActive==false || UserUtil.isQueueUser(l.OwnerId))
                {
                    l.OwnerId = systemUserId;    
                }
                
                ///This is true for inactive-assigned or assigned to a team or leads owned by the systemUser, but I don't know who that is
                if(l.OwnerId==systemUserId) {
                    if(l.Status.contains('Archived') && l.Status!='Archived Do Not Contact') {
                        l.Status='Not Contacted (default)';
                        LeadUtil.useDefaultAssignment(l);
                        //here map must not conatin lead
                        leadNeedToBeUpdated = true;
                    }
                }
            //handle update of Reffrer Fields
                if(campaignLeadsByEmail.containsKey(l.Email)) { 
                    //&& isReferrerCampaignLead(campaignLeadsByEmail.get(l.Email))) {
                    for (Campaign_Lead__c cl : campaignLeadsByEmail.get(l.Email)) {
                        if(isReferrerCampaignLead(cl)) {
                            l.RAF_Referrer_First_Name__c = cl.RAF_Referrer_First_Name__c;
                            l.RAF_Referrer_Last_Name__c = cl.RAF_Referrer_Last_Name__c;
                            l.RAF_Referrer_Email_Address__c = cl.RAF_Referrer_Email_Address__c;
                            l.RAF_Referrer_Username__c = cl.RAF_Referrer_Username__c;
                            leadNeedToBeUpdated = true;
                        }
                    }
                }
                if(leadNeedToBeUpdated) {
                    leadsToUpdate.add(l);
                }
            } else if(campaignLeadsByEmail.containsKey(l.Email) && l.IsConverted && l.ConvertedContactId != null) {
                for (Campaign_Lead__c cl : campaignLeadsByEmail.get(l.Email)) {
                    if(isReferrerCampaignLead(cl)) {
                        Contact c = new Contact();
                        c.Id = l.ConvertedContactId;    
                        c.RAF_Referrer_First_Name__c = cl.RAF_Referrer_First_Name__c;
                        c.RAF_Referrer_Last_Name__c = cl.RAF_Referrer_Last_Name__c;
                        c.RAF_Referrer_Email_Address__c = cl.RAF_Referrer_Email_Address__c;
                        c.RAF_Referrer_Username__c = cl.RAF_Referrer_Username__c;
                        contactToReferreUpdate.add(c);
                    }
                }
            }
            
        }
        
        if(leadsToUpdate.size()>0) {
            update leadsToUpdate;    ///Put those randomly assigned leads into SALESFORCE
        }

        if(contactToReferreUpdate.size()>0) {
            update contactToReferreUpdate;    ///Put those randomly assigned leads into SALESFORCE
        }
        
        // CREATE NEW LEADS FOR NEW CLIENTS
        
        /// Put any emails that were not in the "existing emails list" into a new list
        String[] newEmails = new List<String>();
        for(String email: campaignLeadsByEmail.keySet())
        {
            Lead existingLead = existingLeadByEmail.get(email);
            if(existingLead==null)
            {
                newEmails.add(email);
            }
        }
        
        ///Take the list of new leads' emails and map it to a list of leads
        Map<String, Lead> newLeadByEmail = new Map<String, Lead>();
        for(String newEmail: newEmails)
        {
            // assume all the Campaign Lead entries with same email address are the same lead, just insert it once
            newLeadByEmail.put(newEmail, getLead(campaignLeadsByEmail.get(newEmail)[0]));
        }
        
        ///Insert the leads into Salesforce!
        if(newLeadByEmail.size()>0)
        {
            insert newLeadByEmail.values();
            
            ///Now we want just a list of all the emails from the Campaign Lead list    
            existingLeadByEmail.putAll(newLeadByEmail);
        }
        
        
        // prevent contact/lead from being added more than once to a campaign
        
        ///Looks up campaigns that are being mentioned in our input list and pulls any campaign members that have already been added to these campaigns
        Set<String> leadIdContactIdCampaignIdSet = new Set<String>();
        for(CampaignMember cm : [SELECT Id, CampaignId, ContactId, LeadId FROM CampaignMember WHERE CampaignId IN (SELECT Id FROM Campaign WHERE Name IN :campaignNames)])
        {
            String leadIdContactIdCampaignId = '' + (cm.ContactId!=null?null:cm.LeadId) + cm.ContactId + cm.CampaignId;
            leadIdContactIdCampaignIdSet.add(leadIdContactIdCampaignId);
        }
        
        ///Now that we have a list of new and existing customers, add them to the campaign
        CampaignMember[] cmToInsert = new List<CampaignMember>();
        for(String existingEmail : existingLeadByEmail.keySet())
        {
            manager.setLead(existingEmail, existingLeadByEmail.get(existingEmail));        ///This will associate the lead with the campaign leads/opportunity
            
            ///Go through the list of campaign leads
            for(Campaign_Lead__c cl : campaignLeadsByEmail.get(existingEmail))
            {
                CampaignMember cm = getCampaignMember(cl, existingLeadByEmail.get(existingEmail));
                String leadIdContactIdCampaignId = '' + cm.LeadId + cm.ContactId + cm.CampaignId;
                if(leadIdContactIdCampaignIdSet.contains(leadIdContactIdCampaignId)==false) {
                    leadIdContactIdCampaignIdSet.add(leadIdContactIdCampaignId);        ///Make a list of campaign members that haven't been added yet 
                    cmToInsert.add(cm);
                    manager.addCampaignMember(existingEmail, cm);                        ///Make sure we put the campaign member with the lead, opportunity, etc...
                } else {
                    //cl.addError((cm.LeadId!=null?'LeadId: ' + cm.LeadId:'ContactId: ' + cm.ContactId) + ' already in Campaign ' + cl.Campaign_Name__c + ', Id: ' + cm.CampaignId);
                }
            }
        }
    
        System.debug('inserting cmToInsert: '+cmToInsert);        
        insert cmToInsert;

        ///If anyone did a webinar survey, insert it.
        List<Webinar_Survey__c> webinarSurveys = manager.getWebinarSurveys();
        if(webinarSurveys.size()>0) {
            insert webinarSurveys;
        }

        ///If any campaign lead told us to not create a task, we're done
        if(hasDontCreateTask) {
            return;
        }
        
        // create follow up tasks for the Sales Rep
        Task[] tasksToInsert = new List<Task>();
        for(Lead l : [SELECT Id, Email, OwnerId, Owner.IsActive, IsConverted, ConvertedContactId, ConvertedOpportunityId, ConvertedOpportunity.Owner.IsActive, ConvertedOpportunity.StageName, LeadSource FROM Lead WHERE Email IN :campaignLeadsByEmail.keySet()])
        {
            ///Do we need to make a task for this lead?
//            if(shouldGetTask(l) || hasCreateTask || hasReactivationLead || hasEventLead) {  /// event leads no longer create task, if they have traded
            if(shouldGetTask(l) || (hasCreateTask && (l.ConvertedOpportunity==null || l.ConvertedOpportunity.StageName!='Traded')) || hasReactivationLead) {
                Task t = getTask(l, campaignLeadsByEmail.get(l.Email)[0], systemUserId);            ///systemUserId is the person inserting????
                tasksToInsert.add(t);
            }
        }
        
        //update round robin custom setting
        RoundRobinAssignment.commitAssignments();
        
        System.debug('inserting tasksToInsert: '+tasksToInsert);
        if(tasksToInsert.size()>0)
        {        
            insert tasksToInsert;
        }
        
        ///If there are no reactivation leads, we're going to email all the sales guys
        if(hasReactivationLead==false) {
            emailAttendeeOwners(tasksToInsert);
        }
    }
    
    static void handleSaveResult(CampaignMember[] cmToInsert, Map<String, Campaign_Lead__c[]> campaignLeadsByEmail) {
    }
    
    /**
     * Notify Sales Rep, one email per rep
     */
    static void emailAttendeeOwners(Task[] tasks)
    {
        ///Organize tasks by owner
        Map<Id, Task[]> tasksByOwnerId = new Map<Id, Task[]>();
        for(Task t : tasks)
        {
            Task[] ownerTasks = tasksByOwnerId.get(t.OwnerId);
            if(ownerTasks==null)
            {
                ownerTasks = new List<Task>();
                tasksByOwnerId.put(t.OwnerId, ownerTasks);
            }
            ownerTasks.add(t);
        }
        
        ///Email each owner their new leads
        for(Id ownerId : tasksByOwnerId.keySet())
        {
            String emailText = 'The following customers assigned to you have attended a webinar:\n\n';
            for(Task t : tasksByOwnerId.get(ownerId))
            {
                if(t.WhatId!=null)
                {
                    emailText += 'Opportunity: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.WhatId;
                }
                else
                {
                    emailText += 'Lead: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.WhoId;
                }
                emailText += ', Task: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.Id + '\n\n';
            }
            EmailUtil.sendEmail(ownerId, 'URGENT: Your leads have attended a webinar', emailText);
            // Id toId = '005U0000001XNWd';
            // EmailUtil.sendEmail(toId, 'URGENT: Your leads have attended a webinar', emailText);
        }
    }
    
    static boolean shouldGetTask(Lead l) {
        // not if they've already Traded, is not a Webinar/CX, didn't attended
/*        if((l.IsConverted==true && l.ConvertedOpportunity.StageName=='Traded')
        || (l.LeadSource!='Webinar'&&l.LeadSource!='CX')
        || cl.Attended__c!='Yes')*/
        // not if they've already Traded, is not a Webinar/CX
        if((l.IsConverted==true && l.ConvertedOpportunity.StageName=='Traded')
        || (l.LeadSource!='Webinar'&&l.LeadSource!='CX'&&l.LeadSource!='Event')) {
            return false;
        } else {
            return true;
        }
    }

    static String getSubject(Campaign_Lead__c cl) {
        if(cl.Task_Subject__c!=null&&cl.Task_Subject__c!='') {
            return cl.Task_Subject__c;
        } else if(isCxCampaignLead(cl)) {
            return TASK_SUBJECT_FOLLOW_UP_CX;
        } else if(isEventCampaignLead(cl)) {
            return TASK_SUBJECT_ATTENDED_EVENT;
        } else {
            return TASK_SUBJECT_ATTENDED_WEBINAR;
        }
    }
    
    /**
     * Create a follow up task for the Sales Rep
     */
    static Task getTask(Lead l, Campaign_Lead__c cl, Id defaultOwnerId) {
        Task t = new Task(
                        WhoId=(l.ConvertedOpportunityId!=null?l.ConvertedContactId:l.Id),
                        WhatId=l.ConvertedOpportunityId,
                        Subject=getSubject(cl),
                        ActivityDate=(cl.Task_Due_Date__c==null?Date.today():cl.Task_Due_Date__c),
                        Type='Reminder',
                        ReminderDateTime=DateTime.now(),
                        Status = TASK_STATUS_NOT_STARTED,
                        Priority = (isCxCampaignLead(cl)||isEventCampaignLead(cl)?'Normal':'High'),
                        IsReminderSet = true
                    );
        if(l.IsConverted==true)
        {
            t.OwnerId = (l.ConvertedOpportunity.Owner.IsActive ? l.ConvertedOpportunity.OwnerId : defaultOwnerId);
        }
        else
        {
            t.OwnerId = l.OwnerId;
        }
        if(isReactivationLead(cl)) { // is retention lead
            Id queueId;
            if(cl.Queue_for_Task_Round_Robin__c!=null && cl.Queue_for_Task_Round_Robin__c!='') {
                queueId = UserUtil.getQueueId(cl.Queue_for_Task_Round_Robin__c);
            }
            Id ownerId = RoundRobinAssignment.getNextOwnerId(queueId);
            t.OwnerId = (ownerId!=null ? ownerId : defaultOwnerId);
            t.ActivityDate = cl.Task_Due_Date__c;
        }
        return t;
    }
    
    public static boolean isEmptyWebinarSurvey(Webinar_Survey__c ws) {
        if(ws.Useful__c==null && ws.Speaker_Rating__c==null && ws.FX_Trading_Experience__c==null && ws.Topics__c==null && ws.Selection_Factors__c==null && ws.Help_Actively_Trade__c==null && ws.Other_FX_Broker__c==null && ws.Contact_Details_and_Questions__c==null && ws.Other_Comments__c==null) {
            return true;
        } else {
            return false;
        }
    }
    /*
    public static Webinar_Survey__c getWebinarSurvey(Campaign_Lead__c cl) {
        Webinar_Survey__c ws = new Webinar_Survey__c();
        ws.Useful__c = cl.Useful__c;
        ws.Speaker_Rating__c = cl.Speaker_Rating__c;
        ws.FX_Trading_Experience__c = cl.FX_Trading_Experience__c;
        ws.Topics__c = cl.Topics__c;
        ws.Selection_Factors__c = cl.Selection_Factors__c;
        ws.Help_Actively_Trade__c = cl.Help_Actively_Trade__c;
        ws.Other_FX_Broker__c = cl.Other_FX_Broker__c;
        ws.Contact_Details_and_Questions__c = cl.Contact_Details_and_Questions__c;
        ws.Other_Comments__c = cl.Other_Comments__c;
        
        ws.Campaign__c = CampaignUtil.getIdByName(cl.Campaign_Name__c);
        
        return ws;
    }
*/
    class CampaignResponseManager {
        Map<String, CampaignResponse> campaignResponses = new Map<String, CampaignResponse>();
        
        public CampaignResponse getCampaignResponse(String email) {
            CampaignResponse cr = campaignResponses.get(email);
            if(cr==null) {
                cr = new CampaignResponse();
                campaignResponses.put(email, cr);
            }
            return cr;
        }
        
        public void addCampaignLead(Campaign_Lead__c cl) {
            CampaignResponse cr = getCampaignResponse(cl.Email__c);
            cr.addCampaignLead(cl);
        }
        
        public void addWebinarSurvey(String email, Webinar_Survey__c ws) {
            if(isEmptyWebinarSurvey(ws)) {
                return;
            }
            CampaignResponse cr = getCampaignResponse(email);
            cr.addWebinarSurvey(ws);
        }
        
        public void addCampaignMember(String email, CampaignMember cm) {
            CampaignResponse cr = getCampaignResponse(email);
            cr.addCampaignMember(cm);
        }
        
        public void setLead(String email, Lead l) {
            CampaignResponse cr = getCampaignResponse(email);
            cr.setLead(l);
        }
        
        public List<Webinar_Survey__c> getWebinarSurveys() {
            List<Webinar_Survey__c> result = new List<Webinar_Survey__c>();
            for(CampaignResponse cr : campaignResponses.values()) {
                result.addAll(cr.getWebinarSurveys());
            }
            return result;
        }        
    }
    
    class CampaignResponse {
        List<Campaign_Lead__c> campaignLeads;
        Lead lead;
        Account account;
        Opportunity opp;
        List<CampaignMember> campaignMembers;
        List<Task> tasks;
        List<Webinar_Survey__c> webinarSurveys;
        
        public CampaignResponse() {
            campaignLeads = new List<Campaign_Lead__c>();
            campaignMembers = new List<CampaignMember>();
            tasks = new List<Task>();
            webinarSurveys = new List<Webinar_Survey__c>();
        }
        
        public void addCampaignLead(Campaign_Lead__c cl) {
            campaignLeads.add(cl);
        }
        
        public void addWebinarSurvey(Webinar_Survey__c ws) {
            webinarSurveys.add(ws);
        }
        
        public void addCampaignMember(CampaignMember cm) {
            campaignMembers.add(cm);
        }
        
        public void setLead(Lead l) {
            lead = l;
        }
        
        public Id getContactId() {
            return lead.ConvertedContactId;
        }
        
        public Id getLeadId() {
            return (lead.isConverted?null:lead.Id);
        }
        
        public Id getWhoId() {
            return (lead.ConvertedOpportunityId!=null?lead.ConvertedContactId:lead.Id);
        }
        
        public List<Webinar_Survey__c> getWebinarSurveys() {
            List<Webinar_Survey__c> result = new List<Webinar_Survey__c>();
            for(Webinar_Survey__c ws : webinarSurveys) {
                ws.Contact__c = getContactId();
                ws.Lead__c = getLeadId();
                result.add(ws);
            }
            return result;
        }
    }
}