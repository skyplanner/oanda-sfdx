/**
 * @Description  : Test class for BatchRemoveOldLogs.
 * @Author       : Jakub Fik
 * @Date         : 2024-04-04
 **/
@isTest
public with sharing class BatchRemoveOldLogsTest {
    private static final String CRON_EXP = '0 0 0 3 4 ? 2124';
    private static final String TEST_NAME1 = 'Test1';
    private static final String TEST_NAME2 = 'Test2';
    private static final String ACCOUNT_API_NAME = 'Account';
    private static final String JOB_TYPE_BATCH = 'BatchApex';
    private static final String SCHEDULER_NAME = 'Test schedlue batch';
    private static final String JOB_STATUS_COMPLETED = 'Completed';

    private static final String ASSERTION_MSG_STATUS = 'Status of batch should be completed.';
    private static final String ASSERTION_MSG_ERRORS = 'Batch shouldn\'t have any errors.';
    private static final String ASSERTION_MSG_PROCESSED = 'Batch should process only one record.';
    private static final String ASSERTION_MSG_EXISTING_PROCESS = 'Before test should not exist any jobs.';
    private static final String ASSERTION_MSG_CRON = 'CronTrigger should be created.';
    private static final String ASSERTION_MSG_BATCH = 'One batch job should be created.';

    @TestSetup
    static void setup() {
        List<Account> testList = new List<Account>{
            new Account(Name = TEST_NAME1),
            new Account(Name = TEST_NAME2)
        };
        insert testList;
        DateTime yearAgo = Datetime.now().addYears(-1);
        Test.setCreatedDate(testList[0].Id, yearAgo);
    }

    /**
     * @description Success test for batch.
     * @author Jakub Fik | 2024-04-04
     **/
    @IsTest
    static void batchTest() {
        BatchRemoveOldLogs testBatch = new BatchRemoveOldLogs(
            ACCOUNT_API_NAME,
            1
        );

        Test.startTest();
        Id batchId = Database.executeBatch(testBatch);
        Test.stopTest();

        AsyncApexJob jobResults = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed
            FROM AsyncApexJob
            WHERE Id = :batchId
        ];

        System.assertEquals(
            JOB_STATUS_COMPLETED,
            jobResults.Status,
            ASSERTION_MSG_STATUS
        );
        System.assertEquals(0, jobResults.NumberOfErrors, ASSERTION_MSG_ERRORS);
        System.assertEquals(
            1,
            jobResults.JobItemsProcessed,
            ASSERTION_MSG_PROCESSED
        );
    }

    /**
     * @description Success test for scheduler.
     * @author Jakub Fik | 2024-04-04
     **/
    @IsTest
    static void scheduleTest() {
        BatchRemoveOldLogs testBatch = new BatchRemoveOldLogs(
            ACCOUNT_API_NAME,
            1
        );
        Integer scheduleJobsBefore = [SELECT COUNT() FROM AsyncApexJob];
        System.assertEquals(
            0,
            scheduleJobsBefore,
            ASSERTION_MSG_EXISTING_PROCESS
        );

        Test.startTest();
        String jobId = System.schedule(SCHEDULER_NAME, CRON_EXP, testBatch);
        Test.stopTest();

        CronTrigger ct = [
            SELECT Id, CronExpression, TimesTriggered, NextFireTime, State
            FROM CronTrigger
            WHERE id = :jobId
        ];

        Integer batchCounter = [
            SELECT COUNT()
            FROM AsyncApexJob
            WHERE JobType = :JOB_TYPE_BATCH
        ];

        System.assert(ct != null, ASSERTION_MSG_CRON);
        System.assertEquals(1, batchCounter, ASSERTION_MSG_BATCH);
    }
}