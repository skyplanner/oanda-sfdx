/**
 * @lastmodifiedon 11-16-2021
 * @lastmodifiedby Fernando Gomez
 */
public class ComplyAdvantageSearchEntities 
{
    static string caApikey;
    static string caSearchId;
    
    @AuraEnabled 
    public static void updateSearchEntities(Id searchId,
        	List<String> entityIds,
			ComplyAdvantageKeyInformation template,
			string summaryTitle,
			string summaryBody) 
	{
		ComplyAdvantageEntitiesDownloader downloader;
		try 
		{
			Comply_Advantage_Search__c search = ComplyAdvantageUtil.getSearchBySalesforceId(searchId);
			if(search != null)
			{
				// we upload to CA
				downloader = new ComplyAdvantageEntitiesDownloader(search);
				downloader.upload(entityIds, template);

				// we then add the note
				Note updateSummaryNote = new Note();
				updateSummaryNote.ParentId = searchId;
				updateSummaryNote.Body =  UserInfo.getName() + ' ' + summaryBody;
				updateSummaryNote.Title = UserInfo.getName() + ' ' + summaryTitle;
				insert updateSummaryNote;
			}
		}
		catch (Exception ex) 
		{
			throw new AuraHandledException(ex.getMessage());
		}
    }

	/**
	 * @param searchId
	 * @param entityId
	 */
	@AuraEnabled(cacheable=true)
	public static ComplyAdvantageSearchHit getSingleEntity(Id searchId, String entityId) 
	{
		try 
		{
			Comply_Advantage_Search__c search = ComplyAdvantageUtil.getSearchBySalesforceId(searchId);
			ComplyAdvantageEntitiesDownloader downloader = new ComplyAdvantageEntitiesDownloader(search);
			return downloader.getEntity(entityId);
		} 
		catch (Exception ex) 
		{
			throw new AuraHandledException(ex.getMessage());
		}
    }
    
	/**
	 * @param caSearchId
	 */
	@AuraEnabled(cacheable=true)
	public static List<Comply_Advantage_Search_Entity__c> getSearchEntities(
			Id caSearchId) {
		return [
			SELECT Id,
				Name,
				Comply_Advantage_Search__c,
				Countries__c,
				Date_Of_Birth__c,
				Entity_Id__c,
				Entity_Type__c,
				Is_Whitelisted__c,
				Keywords__c,
				Match_Status__c,
				Primary_Country__c,
				Relevance__c,
				Risk_Level__c,
				Sources__c,
				Entity_Match_Types__c,
				Unique_ID__c,
				AKA__c,
				Monitor_Change_Status__c,
				Is_Removed__c,
				Match_Types__c,
				Is_Internal_Hit__c,
				Internal_Hit_Id__c,
				Internal_Hit_Notes__c,
				Internal_Hit_Phone__c,
				Internal_Hit_Email__c,
				Internal_Hit_Address__c
			FROM Comply_Advantage_Search_Entity__c
			WHERE Comply_Advantage_Search__c = :caSearchId and Is_Removed__c = false Limit 100
		];
	}
   
   @AuraEnabled 
   public static void addCommentsSummaryNote(string searchId,string matchName, string comment)
   {
        if(string.isNotBlank(searchId))
        {
            if(string.isNotBlank(matchName))
            {
                Note updateSummaryNote = new Note();
                updateSummaryNote.ParentId = searchId;
                string body ='comment: \n ------------------------- \n' + comment;
                body +=  '\n\n Search matches:\n ------------------------- \n' + matchName;
                updateSummaryNote.Body =body; 
                updateSummaryNote.Title = UserInfo.getName() + ' added new comment to search matches';
                Insert updateSummaryNote;
            }
            else
            {
             	Note updateSummaryNote = new Note();
                updateSummaryNote.ParentId = searchId;
                updateSummaryNote.Body =  comment; 
                updateSummaryNote.Title = UserInfo.getName() + ' added new comment to search';
                Insert updateSummaryNote;   
            }
        }
   }
    
	/**
	 * @param searchId
	 * @param matchId
	 * @param comment
	 */
	@AuraEnabled 
	public static void addComments(String searchId, String matchId, String comment) {
		ComplyAdvantageCommentsDownloader downloader;
		try {
			downloader = new ComplyAdvantageCommentsDownloader(getSearchId(searchId));
			downloader.addComment(comment, matchId);
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}
	
	/**
	 * @param searchId
	 */
	@AuraEnabled
	public static List<ComplyAdvantageSearchComment> getAllComments(string searchId) {
		ComplyAdvantageCommentsDownloader downloader;
		//try {
			downloader = new ComplyAdvantageCommentsDownloader(getSearchId(searchId));
			return downloader.getComments();
		//} catch (Exception ex) {
		//	throw new AuraHandledException(ex.getMessage());
		//}
	}


	@AuraEnabled
	public static void acknowledgeChanges(string searchId) 
	{
		try 
		{
			ComplyAdvantageAcknowledgeMonitorChanges ackMonitorChangesHandler = 
					new ComplyAdvantageAcknowledgeMonitorChanges(searchId, getSearchId(searchId));
			ackMonitorChangesHandler.ackMonitorChanges();
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	private static String getSearchId(String searchId) {
		return [
			SELECT Search_Id__c
			FROM Comply_Advantage_Search__c
			WHERE Id = :searchId
		].Search_Id__c;
	}
}