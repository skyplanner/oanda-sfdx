@IsTest
private class UpdateTokenBatchTest {
    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenPositiveTest1() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);

        UpdateTokenBatch updateTokenBatch = new UpdateTokenBatch();
        String jobId = '';
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            jobId = Database.executeBatch(updateTokenBatch);
        Test.stopTest();
        Assert.areEqual('Completed', [SELECT Id, Status FROM AsyncApexJob WHERE Id =:jobId].get(0).Status);
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenPositiveTest2() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);

        UpdateTokenBatch updateTokenBatch = new UpdateTokenBatch(Constants.USER_API_NAMED_CREDENTIALS_NAME);
        String jobId = '';
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            jobId = Database.executeBatch(updateTokenBatch);
        Test.stopTest();
        Assert.areEqual('Completed', [SELECT Id, Status FROM AsyncApexJob WHERE Id =:jobId].get(0).Status);
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenPositiveTest3() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);

        UpdateTokenBatch updateTokenBatch = new UpdateTokenBatch(new List<String>{Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.TAS_API_NAMED_CREDENTIALS_NAME});
        String jobId = '';
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            jobId = Database.executeBatch(updateTokenBatch);
        Test.stopTest();
        Assert.areEqual('Completed', [SELECT Id, Status FROM AsyncApexJob WHERE Id =:jobId].get(0).Status);
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenNegativeTest1() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);

        UpdateTokenBatch updateTokenBatch = new UpdateTokenBatch();
        updateTokenBatch.tokensToUpdate.put('WRONG_VALUE_TEST', 'WRONG_VALUE_TEST');
        String jobId = '';
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            jobId = Database.executeBatch(updateTokenBatch);
        Test.stopTest();
        Assert.areEqual('Completed', [SELECT Id, Status FROM AsyncApexJob WHERE Id =:jobId].get(0).Status);  Database.executeBatch(updateTokenBatch);
    }

    @IsTest(SeeAllData=true)
    static void manageNamedCredentialTokenNegativeTest2() {
        TokenHelperTest.MockService mockData = new TokenHelperTest.MockService();
        mockData.addEndPoint('http://collaut.test.com', 200, '{"access_token": "test123", "token_type" : "test", "expires_in" : "600"}', 'OK', null);
        mockData.addEndPoint('http://currentorg.url.com', 201, '', 'OK', null);
        Test.setMock(HttpCalloutMock.class, mockData);

        String credentialSuffix = Util.getValidCredentialSuffix();
        UpdateTokenBatch updateTokenBatch = new UpdateTokenBatch(Constants.TAS_API_NAMED_CREDENTIALS_NAME);
        updateTokenBatch.tokensToUpdate.put((Constants.USER_API_NAMED_CREDENTIALS_NAME + credentialSuffix).toLowerCase(), 'WRONG_VALUE_TEST');
        String jobId = '';
        Test.startTest();
            NamedCredentialsUtil.isTestUseSeeAllData = true;
            jobId = Database.executeBatch(updateTokenBatch);
        Test.stopTest();
        Assert.areEqual('Completed', [SELECT Id, Status FROM AsyncApexJob WHERE Id =:jobId].get(0).Status);
    }
}