/**
 * @File Name          : OnCaseOwnerOrStatusChangesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/28/2024, 1:38:20 PM
**/
public inherited sharing class OnCaseOwnerOrStatusChangesCmd {

    public void checkRecord(
        Case rec, 
        Case oldRec
    ) {
        // whenever Routed__c, Routed_And_Assigned__c or Agent_Capacity_Status__c 
        // changes it is assumed that is an internal change and the ownerId or 
        // status changes are ignored
        if (!isAnInternalChange(rec, oldRec)) {
            Boolean ownerChange = (rec.OwnerId != oldRec.OwnerId);
            Boolean statusChange = (rec.Status != oldRec.Status);
            processChanges(
                rec, 
                oldRec,
                ownerChange,
                statusChange
            );
        }
    }

    @TestVisible
    Boolean isAnInternalChange(
        Case rec, 
        Case oldRec
    ) {
        return (
            (rec.Ready_For_Routing__c != oldRec.Ready_For_Routing__c) ||
            (rec.Routed__c != oldRec.Routed__c) ||
            (rec.Routed_And_Assigned__c != oldRec.Routed_And_Assigned__c) ||
            (rec.Agent_Capacity_Status__c != oldRec.Agent_Capacity_Status__c)
        );
    }

    @TestVisible
    void processChanges(
        Case rec, 
        Case oldRec,
        Boolean ownerChange,
        Boolean statusChange
    ) {
        if (ownerChange) {
            if (isAValidRoutedCase(rec)) {
                markRoutedCaseAsReleased(
                    rec, 
                    true // ownerChange
                );
                //...
            } else {
                checkNewOwnerImpliesRouting(rec);
            }
            //...
        } else if (statusChange) {
            if (
                isAValidRoutedCase(rec) &&
                (oldRec.Status == OmnichanelConst.CASE_STATUS_OPEN ||
                oldRec.Status == OmnichanelConst.CASE_STATUS_RE_OPENED)
            ) {
                markRoutedCaseAsReleased(
                    rec, 
                    false // ownerChange
                );
                //...
            } else {
                checkNewStatusImpliesRouting(rec, oldRec);
            }
        }
    }

    @TestVisible
    Boolean checkNewStatusImpliesRouting(
        Case rec,
        Case oldRec
    ) {
        Boolean result = false;
        if (CaseReroutingManager.getInstance().newStatusImpliesRouting(rec, oldRec)) {
            prepareCaseForNewRouting(rec);
            result = true;
        }
        return result;
    }

    @TestVisible
    Boolean isAValidRoutedCase(Case rec) {
        return (
            (rec.Routed__c == true) &&
            (rec.Agent_Capacity_Status__c != OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED)
        );
    }

    @TestVisible
    void markRoutedCaseAsReleased(
        Case rec,
        Boolean ownerChange
    ) {
        rec.Agent_Capacity_Status__c = 
            (
                (ownerChange == true) &&
                CaseReroutingManager.getInstance().newOwnerImpliesRouting(rec)
            )
                ? OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE
                : OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED;
    }

    @TestVisible
    Boolean checkNewOwnerImpliesRouting(Case rec) {
        Boolean result = false;
        if (CaseReroutingManager.getInstance().newOwnerImpliesRouting(rec)) {
            prepareCaseForNewRouting(rec);
            result = true;
        }
        return result;
    }

    @TestVisible
    void prepareCaseForNewRouting(Case rec) {
        rec.Agent_Capacity_Status__c = OmnichanelConst.CASE_CAPACITY_STATUS_ROUTING;
        rec.Internally_Routed__c = true;
        rec.Routed__c = true; // to avoid having to update the case again after it is routed
        rec.Routed_And_Assigned__c = false;
    }
    
}