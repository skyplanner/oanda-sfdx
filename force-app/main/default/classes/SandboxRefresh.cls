public without sharing class SandboxRefresh implements SandboxPostCopy {
	public void runApexClass(SandboxContext context) {

		//Set up streaming topics
		PushTopicUtil.upsertTopics();

		//Schedule bi-hourly Lead Merge process
		BatchMergeLeadsScheduleable.schedule();

		//Schedule bi-hourly Lead assignment process
		BatchTriggerLeadAssignment.schedule();

		//Schedule bi-hourly Lead convert process
		BatchConvertLeadsScheduleable.schedule();

		//Sanitize client data
		BatchFormatAccountInfo b = new BatchFormatAccountInfo();
		database.executebatch(b);
				
	}
}