/* Name: BatchAssignOpportunityBasedOnCVI
 * Description : Batch Class on Opportunity for Before Insert and Before Update.
 * Author: OANDA
 * 
 */
public class BatchAssignOpportunityBasedOnCVI implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query,queueName;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    private Decimal clientValueScore;

	public static final Integer BATCH_SIZE = 25;
	public static final String CRON_NAME,CRON_SCHEDULE;
    
 	public static void schedule() {
    	System.schedule(CRON_NAME, CRON_SCHEDULE, new BatchAssignOpportunityBasedOnCVI());
  	}

	public BatchAssignOpportunityBasedOnCVI() {
		//Scheduled batches are found here: https://oandacorp.atlassian.net/wiki/spaces/SF/pages/1454801844/Salesforce+Batch+Classes
    }

	public BatchAssignOpportunityBasedOnCVI(String q, String queueName) {
		String queryInUpper = q.toUpperCase();
		if(queryInUpper.indexOf('FXACCOUNT__C') == -1){
			query = queryInUpper.substringBefore(' FROM ') + ', FXACCOUNT__C FROM ' + queryInUpper.substringAfter(' FROM ');
		} else {
			query = queryInUpper;
		}
        this.queueName = queueName;
        this.clientValueScore = clientValueScore;
  	}
	  
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT id, Client_Value_Indicator_Score__c,Owner.name,AccountId,Account_Mailing_Country__c,Region_by_Country__c,CreatedDate from Opportunity where Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('Final Query ::: ' + query);
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<sObject> batch) {
		OpportunityUtil.roundRobinOpportunitiesBasedOnCVI((Opportunity[]) batch,queueName);
	}
	
  	public void execute(SchedulableContext context) {
    	Database.executeBatch(new BatchAssignOpportunityBasedOnCVI(query,queueName), BATCH_SIZE);
  	}

	public void finish(Database.BatchableContext BC) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}