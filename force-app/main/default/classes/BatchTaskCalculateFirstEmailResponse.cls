public with sharing class BatchTaskCalculateFirstEmailResponse implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection
{
    string[] originFilter = new string[]{'Phone','Chat','Chatbot'};
    string[] ownerRoleFilter = new string[]{'CX Associate - Singapore','CX Supervisor - Singapore'};
	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public BatchTaskCalculateFirstEmailResponse() 
    {
        string query = 'SELECT ID '+
                       'FROM Case '+
                       'WHERE Origin NOT IN :originFilter '+
                            'AND Case_Owner_Role__c IN :ownerRoleFilter '+
                            'AND Status = \'Closed\' '+
                            'AND CreatedDate >= 2019-01-01T00:00:00Z and CreatedDate <= 2020-06-30T00:00:00Z';
             
        
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id FROM Case WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public static void executeBatch() 
    {
        Database.executeBatch(new BatchTaskCalculateFirstEmailResponse(), 200);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
        

    	return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Case> scope) 
    {
        set<Id> caseIds = new set<Id>{};
        for(Case c: scope)
        {
            caseIds.add(c.Id);
        }
        TaskUtil.updateFirstEmailResponse(caseIds);
	}   
    public void finish(Database.BatchableContext bc){
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
}