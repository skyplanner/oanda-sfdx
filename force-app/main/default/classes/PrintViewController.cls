/* Name: PrintViewController
 * Description : Apex Class to show print view 
 * Author: vanglirajan kalimuthu
 * Date : 2020-06-03
 */
public with sharing class PrintViewController {

    public String getPrintableView() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        String html = new PageReference('/' + id + '/p').getContent().toString();
        return html.substringAfter('</head>').substringBefore('</html>');
    }
}