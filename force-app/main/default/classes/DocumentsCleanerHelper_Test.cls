@isTest
private class DocumentsCleanerHelper_Test {

	//@testSetup
	//static void setup () {
	//}

	@isTest
	static void findAndRemoveUselessDocs_test() {
		ArticlesTestDataFactory.createArticlesWithDummyDocs();
		Integer beforeCount = [select count() from Document];
		ArticlesTestDataFactory.createOrphanDocs(2);
		List<Document> docs = [select Id from Document];
		Map<Id,Document> docMap = new Map<Id,Document>(docs);
		Set<ID> docIdList = docMap.keySet();
		Test.startTest();
		DocumentsCleanerHelper.findAndRemoveUselessDocs(docIdList);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

	//delete docs with images
	// @isTest
	// static void findAndRemoveUselessDocs_test2() {
	// 	ArticlesTestDataFactory.createArticlesWithDummyDocs();
	// 	Integer beforeCount = [select count() from Document];
	// 	ArticlesTestDataFactory.createOrphanDocs(4);
	// 	List<Document> docList = [select Id from Document];
	// 	Document fakeImg = ArticlesTestDataFactory.createOrphanDocWithBody('5', null);
	//     String docBody = 
	//     	'[{"languageCode":"en_US",' + 
	//     	'"languageLabel":"English",' +
	//     	'"question":"ACL 7-11-2019 5",' +
	//     	'"error":false,"items":[' +
	//     	'{"name":"Image","tag":"img",' +
	//     	'"fileUrl":"/servlet/servlet.ImageServer?id=' + fakeImg.Id + '&oid=00D0t0000000wRVEAY",' +
	//     	'"fileName":"1.jpg","fileSize":12917,"fileType":"image/jpeg",' + 
	//     	'"caption":"","error":null},' +
	//     	'{"name":"Paragraph","tag":"p","value":"some thing","deleted":false,"error":false}]}]';
	//     Document orphanDocWithBody = ArticlesTestDataFactory.createOrphanDocWithBody('6', docBody);
	//     docList.add(orphanDocWithBody);
	// 	Test.startTest();
	// 	DocumentsCleanerHelper.findAndRemoveUselessDocs(docList);
	// 	Test.stopTest();
	// 	Integer count = [select count() from Document];
	// 	System.assertEquals(beforeCount, count);
	// }

	@isTest
	static void findAndRemoveUselessDocs_test3() {
		Integer beforeCount = [select count() from Document];
		Set<ID> docIdList = null;
		Test.startTest();
		DocumentsCleanerHelper.findAndRemoveUselessDocs(docIdList);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

	@isTest
	static void findAndRemoveUselessDocs_test4() {
		Integer beforeCount = [select count() from Document];
		List<Document> docList = null;
		Test.startTest();
		DocumentsCleanerHelper.findAndRemoveUselessDocs(docList);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

	@isTest
	static void findAndRemoveUselessDocs_test5() {
		ArticlesTestDataFactory.createArticlesWithDummyDocs();
		Integer beforeCount = [select count() from Document];
		List<Document> docList = null;
		Test.startTest();
		DocumentsCleanerHelper.findAndRemoveUselessDocs(docList);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}


	@isTest
	static void getUselessDocsCheckingQuery_test() {
		String docsFolderId = 'abc';
		Boolean onlyTwoWeeksRange = true;
		Test.startTest();
		String result = DocumentsCleanerHelper.getUselessDocsCheckingQuery(docsFolderId, onlyTwoWeeksRange);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getUselessDocsCheckingQuery_test2() {
		String docsFolderId = 'abc';
		Boolean onlyTwoWeeksRange = false;
		Test.startTest();
		String result = DocumentsCleanerHelper.getUselessDocsCheckingQuery(docsFolderId, onlyTwoWeeksRange);
		Test.stopTest();
		System.assertNotEquals(null, result);
	}

	@isTest
	static void getUselessDocsCheckingQuery_test3() {
		String docsFolderId = null;
		Boolean onlyTwoWeeksRange = false;
		Test.startTest();
		String result = DocumentsCleanerHelper.getUselessDocsCheckingQuery(docsFolderId, onlyTwoWeeksRange);
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest
	static void excludeArticlesDocs_test() {
		Map<ID,Document> docMap = new Map<ID,Document>();
		Test.startTest();
		DocumentsCleanerHelper.excludeArticlesDocs(docMap, null);
		Test.stopTest();
		System.assertEquals(0, docMap.size());
	}

}