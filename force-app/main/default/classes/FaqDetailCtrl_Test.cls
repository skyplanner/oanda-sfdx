/**
 * @File Name          : FaqDetailCtrl_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/26/2020, 12:51:57 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/25/2020   acantero     Initial Version
**/
@istest
private class FaqDetailCtrl_Test {
    
    //valid Id and language params
    @istest
    static void FaqDetailCtrl_test1() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.SPANISH_LANGUAGE
        );
        PageReference faqDetailPr = Page.FaqDetail;
        Map<String, String> params = faqDetailPr.getParameters();
        params.put(FaqDetailCtrl.ID_PARAM, artId);
        params.put(FaqDetailCtrl.LANGUAGE_PARAM, FaqPreviewTestDataFactory.SPANISH_LANGUAGE);
        Test.setCurrentPage(faqDetailPr);
        Test.startTest();
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        Test.stopTest();
        System.assertEquals(FaqPreviewTestDataFactory.ART_TITLE, ctrl.title);
    }

    //valid urlName param, no language param 
    @istest
    static void FaqDetailCtrl_test2() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.DEFAULT_LANGUAGE
        );
        PageReference faqDetailPr = Page.FaqDetail;
        Map<String, String> params = faqDetailPr.getParameters();
        params.put(FaqDetailCtrl.URL_NAME_PARAM, FaqPreviewTestDataFactory.ART_URL_NAME);
        Test.setCurrentPage(faqDetailPr);
        Test.startTest();
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        Test.stopTest();
        System.assertEquals(FaqPreviewTestDataFactory.ART_TITLE, ctrl.title);
    }

    //valid urlName param, wrong language param, valid console app param
    @istest
    static void FaqDetailCtrl_test3() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.DEFAULT_LANGUAGE,
            '<p><a href="/AnswersSupport?urlName=What-are-OANDA-s-divisions">My Link</a></p>'
        );
        PageReference faqDetailPr = Page.FaqDetail;
        Map<String, String> params = faqDetailPr.getParameters();
        params.put(FaqDetailCtrl.URL_NAME_PARAM, FaqPreviewTestDataFactory.ART_URL_NAME);
        params.put(FaqDetailCtrl.LANGUAGE_PARAM, FaqPreviewTestDataFactory.SPANISH_LANGUAGE);
        params.put(FaqDetailCtrl.CONSOLE_PARAM, FaqDetailCtrl.CONSOLE_PARAM_VALUE);
        Test.setCurrentPage(faqDetailPr);
        Test.startTest();
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        ctrl.errorMessage = '';
        Test.stopTest();
        System.assertEquals(FaqPreviewTestDataFactory.ART_TITLE, ctrl.title);
    }

    //valid urlName param, no language param, blank body 
    @istest
    static void FaqDetailCtrl_test4() {
        ID artId = FaqPreviewTestDataFactory.createTestArticle(
            FaqPreviewTestDataFactory.DEFAULT_LANGUAGE,
            '   ' //blank body
        );
        PageReference faqDetailPr = Page.FaqDetail;
        Map<String, String> params = faqDetailPr.getParameters();
        params.put(FaqDetailCtrl.URL_NAME_PARAM, FaqPreviewTestDataFactory.ART_URL_NAME);
        Test.setCurrentPage(faqDetailPr);
        Test.startTest();
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        Test.stopTest();
        System.assertEquals(FaqPreviewTestDataFactory.ART_TITLE, ctrl.title);
    }

    //invalid param
    @istest
    static void fixArticleLinks_test1() {
        Test.startTest();
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        String result = ctrl.fixArticleLinks(null);
        Test.stopTest();
        System.assertEquals(null, result);
    }

    //malformed html
    @istest
    static void fixArticleLinks_test2() {
        Test.startTest();
        String invalidHtml = '<a malformed html';
        FaqDetailCtrl ctrl = new FaqDetailCtrl();
        String result = ctrl.fixArticleLinks(invalidHtml);
        Test.stopTest();
        System.assertEquals(invalidHtml, result);
    }

}