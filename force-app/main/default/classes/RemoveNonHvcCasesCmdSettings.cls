/**
 * @File Name          : RemoveNonHvcCasesCmdSettings.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/17/2024, 12:01:44 PM
**/
public without sharing class RemoveNonHvcCasesCmdSettings {

	public static final String NON_HVC_CASES_DAYS_STORAGE = 
		'Non_Hvc_Cases_Days_Storage';
	public static final String NON_HVC_CASES_DELETE_COUNT = 
		'Non_Hvc_Cases_Delete_Count';

	public static final Integer DEFAULT_DAYS_STORAGE = 2;
	public static final Integer DEFAULT_RECORDS_LIMIT = 1000;

	public static Integer recordsLimit {
		get {
			if (recordsLimit == null) {
				new RemoveNonHvcCasesCmdSettings().init();
			}
			return recordsLimit;
		} 
		set;
	}

	public static Datetime lastModifiedDate {
		get {
			if (lastModifiedDate == null) {
				new RemoveNonHvcCasesCmdSettings().init();
			}
			return lastModifiedDate;
		} 
		set;
	}

	@TestVisible
	void init() {
		Integer daysStorage = 
			SPSettingsManager.getSettingAsInt(NON_HVC_CASES_DAYS_STORAGE);
		recordsLimit = 
			SPSettingsManager.getSettingAsInt(NON_HVC_CASES_DELETE_COUNT);
			
		daysStorage = getValidValue(
			daysStorage, // currentValue
			1, // minValue 
			DEFAULT_DAYS_STORAGE // defaultValue
		);
		recordsLimit = getValidValue(
			recordsLimit, // currentValue
			100, // minValue 
			DEFAULT_RECORDS_LIMIT // defaultValue
		);
		lastModifiedDate = getLastModifiedDateFilter(daysStorage);
	}

	@TestVisible
	Datetime getLastModifiedDateFilter(Integer pDaysStorage) {
		Datetime now = Datetime.now();
		Datetime tempDatetime = Datetime.newInstance(
			now.year(), // year
			now.month(), // month
			now.day() // day
		);
		Datetime result = tempDatetime.addDays((pDaysStorage * -1));
		return result;
	}

	@TestVisible
	Integer getValidValue(
		Integer currentValue, 
		Integer minValue,
		Integer defaultValue
	) {
		if (
			(currentValue == null) ||
			(currentValue < minValue)
		) {
			return defaultValue;
		}
		// else...
		return currentValue;
	}
	
}