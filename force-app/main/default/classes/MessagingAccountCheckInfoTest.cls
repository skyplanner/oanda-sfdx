/**
 * @File Name          : MessagingAccountCheckInfoTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 4:30:30 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 4:30:30 PM   aniubo     Initial Version
 **/
@isTest
private class MessagingAccountCheckInfoTest {
	@isTest
	private static void testMessagingAccountCheckInfoConstructor() {
		// Test data setup

		// Actual test
		Test.startTest();
		MessagingAccountCheckInfo accountCheckInfo = new MessagingAccountCheckInfo(
			false
		);
		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			accountCheckInfo.isLinkedToAccountOrLead,
			'isLinkedToAccountOrLead should be false'
		);
		Assert.areEqual(
			null,
			accountCheckInfo.fxAccountId,
			'fxAccountId should be null'
		);
	}
}