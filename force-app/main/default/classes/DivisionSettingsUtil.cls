/**
 * @File Name          : DivisionSettingsUtil.cls
 * @Description        : new class to manage division settings
 * @Author             : Mikolaj Juras
 * @Group              : 
 * @Last Modified By   : Mikolaj Juras
**/
public with sharing class DivisionSettingsUtil {

    private static DivisionSettingsUtil instance = null;

    private DivisionSettingsUtil(){}

    public static DivisionSettingsUtil getInstance(){
        if(instance == null){
           instance = new DivisionSettingsUtil();
         }
     return instance;
 }

    @testVisible
    private List<Division_Settings__mdt> allDivisionSettings;

    public List<Division_Settings__mdt> getAllDivisionSettings() {
        if(allDivisionSettings == null) {
            allDivisionSettings = Division_Settings__mdt.getAll().values();
        }
        return allDivisionSettings;
    }

    public Division_Settings__mdt getDivisionSettingByDivisionName(String divSettingName) {
        for(Division_Settings__mdt ds : getAllDivisionSettings()) {
            if (ds.Division_Name__c == divSettingName) {
                return ds;
            }
        }
        return null;
    }

}