/**
 * @File Name          : SupervisorByDivisions.cls
 * @Description        : Retrieve the managers Id for divisions, for the given divisions
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 4:53:33 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/2/2021, 2:01:33 PM   acantero     Initial Version
**/
public without sharing class SupervisorByDivisions implements SupervisorProvider {

    Set<String> divisionCodeSet;

    Map<String, Set<String>> divisionSupervisorIdsMap;

    public SupervisorByDivisions() {
        this.divisionCodeSet = new Set<String>();
        this.divisionSupervisorIdsMap = new Map<String, Set<String>>();
    }

    public void addCriteria(String criteria) {
        if (String.isNotBlank(criteria)) {
            divisionCodeSet.add(criteria);
        }
    }

    public Boolean isEmpty() {
        return divisionSupervisorIdsMap.isEmpty();
    }

    public Set<String> getSupervisors(String criteria) {
        return getDivisionSupervisors(criteria);
    }

    public Set<String> getDivisionSupervisors(String divisionCode) {
        if (String.isBlank(divisionCode)) {
            return null;
        }
        //else...
        return divisionSupervisorIdsMap.get(divisionCode);
    }
    
    public void getSupervisors() {
        if ((divisionCodeSet == null) || divisionCodeSet.isEmpty()) {
            return;
        }
        //else...
        OmnichanelSettings omniSettings = OmnichanelSettings.getRequiredDefault();
        if (omniSettings.supervisorRoleDevNameList != null) {
            System.debug('getSupervisors');
            System.debug('divisionCodeSet: ' + divisionCodeSet);
            System.debug('roles: ' + omniSettings.supervisorRoleDevNameList);
            List<ServiceResourceSkill> srsList = [
                SELECT  
                    Skill.DeveloperName, 
                    ServiceResource.RelatedRecordId 
                FROM 
                    ServiceResourceSkill 
                WHERE 
                    Skill.DeveloperName IN :divisionCodeSet 
                AND
                    ServiceResource.ResourceType = :OmnichanelConst.SERVICE_RES_AGENT 
                AND
                    ServiceResource.IsActive = true
                AND
                    ServiceResource.RelatedRecord.IsActive = true 
                AND
                    ServiceResource.RelatedRecord.UserRole.DeveloperName IN :omniSettings.supervisorRoleDevNameList 
            ];
            for(ServiceResourceSkill srs : srsList) {
                Set<String> managerIdSet = divisionSupervisorIdsMap.get(srs.Skill.DeveloperName);
                if (managerIdSet == null) {
                    managerIdSet = new Set<String>();
                    divisionSupervisorIdsMap.put(srs.Skill.DeveloperName, managerIdSet);
                }
                managerIdSet.add(srs.ServiceResource.RelatedRecordId);
            }
        }
    }

}