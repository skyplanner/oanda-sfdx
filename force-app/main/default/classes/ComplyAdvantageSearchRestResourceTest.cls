/**
 * @description       :
 * @author            : OANDA
 * @group             :
 * @last modified on  : 29 Jul 2024
 * @last modified by  : Ryta Yahavets
**/

@IsTest
private class ComplyAdvantageSearchRestResourceTest {
    @isTest
    static void testCAS404() {
        RestRequest request = new RestRequest();
        request.requestUri = '/api/v1/user/854125/comply_advantage';
        request.resourcePath = '/services/apexrest/api/v1/user/*/comply_advantage';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
            ComplyAdvantageSearchRestResource.doPost();
        Test.stopTest();

        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse body =
                (ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse)JSON.deserialize(
                        RestContext.response.responseBody.toString(),
                        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse.class
                );

        Assert.areEqual(200, RestContext.response.statusCode);
        Assert.areEqual(404, body.response_code);
    }

    @isTest
    static void testCAS409FieldsWrong() {
        Lead l = TestDataFactory.getTestLead(true);
        insert l;
        fxAccount__c fx = new fxAccount__c();
        fx.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fx.fxTrade_User_Id__c = 854125;
        fx.Knowledge_Result__c = 'Pass';
        fx.Lead__c = l.Id;
        insert fx;

        RestRequest request = new RestRequest();
        request.requestUri = '/api/v1/user/854125/comply_advantage';
        request.resourcePath = '/services/apexrest/api/v1/user/*/comply_advantage';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
            ComplyAdvantageSearchRestResource.doPost();
        Test.stopTest();

        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse body =
                (ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse)JSON.deserialize(
                        RestContext.response.responseBody.toString(),
                        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse.class
                );

        Assert.areEqual(200, RestContext.response.statusCode);
        Assert.areEqual(409, body.response_code);
    }

    @isTest
    static void testCAS200Lead() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
                Name = 'Default',
                Comply_Advantage_Enabled__c = false
        );
        insert settings;

        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
                Name = 'NA',
                API_Key__c = 'test'
        );
        insert caInstanceSetting;

        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
                Name = 'OANDA Canada',
                Search_Profile__c = 'CA Temp',
                Secondary_Search_Profile__c = 'CA Temp Sec',
                Fuzziness__c = 0,
                Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
                Region__c = 'OCAN',
                New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
        );
        insert caDivisionSetting;

        Lead l = TestDataFactory.getTestLead(true);
        l.Country = 'Canada';
        insert l;
        fxAccount__c fx = new fxAccount__c();
        fx.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fx.fxTrade_User_Id__c = 854125;
        fx.Knowledge_Result__c = 'Pass';
        fx.Citizenship_Nationality__c = 'Canada';
        fx.Birthdate__c = Date.newInstance(1990, 11, 21);
        fx.Division_Name__c = 'OANDA Canada';

        fx.Lead__c = l.Id;
        insert fx;

        RestRequest request = new RestRequest();
        request.requestUri = '/api/v1/user/854125/comply_advantage';
        request.resourcePath = '/services/apexrest/api/v1/user/*/comply_advantage';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
            ComplyAdvantageSearchRestResource.doPost();
        Test.stopTest();

        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse body =
                (ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse)JSON.deserialize(
                        RestContext.response.responseBody.toString(),
                        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse.class
                );

        Assert.areEqual(200, RestContext.response.statusCode);
        Assert.areEqual(200, body.response_code);
    }

    @isTest
    static void testCAS200Account() {
        Country_Setting__c cs = new Country_Setting__c();
        cs.Name = 'Canada';
        cs.ISO_Code__c = 'ca';
        cs.Group__c = 'Canada';
        cs.Region__c = 'North America';
        cs.Zone__c = 'Americas';
        insert cs;

        Settings__c settings = new Settings__c(
                Name = 'Default',
                Comply_Advantage_Enabled__c = false
        );
        insert settings;

        Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
                Name = 'NA',
                API_Key__c = 'test'
        );
        insert caInstanceSetting;

        Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
                Name = 'OANDA Canada',
                Search_Profile__c = 'CA Temp',
                Secondary_Search_Profile__c = 'CA Temp Sec',
                Fuzziness__c = 0,
                Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
                Region__c = 'OCAN',
                New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
        );
        insert caDivisionSetting;

        fxAccount__c fx = new fxAccount__c();
        fx.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fx.fxTrade_User_Id__c = 854125;
        fx.Knowledge_Result__c = 'Pass';
        fx.Citizenship_Nationality__c = 'Canada';
        fx.Birthdate__c = Date.newInstance(1990, 11, 21);
        fx.Division_Name__c = 'OANDA Canada';
        fx.Account__c = (new TestDataFactory()).createTestAccountsEid().Id;
        insert fx;

        RestRequest request = new RestRequest();
        request.requestUri = '/api/v1/user/854125/comply_advantage';
        request.resourcePath = '/services/apexrest/api/v1/user/*/comply_advantage';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
            ComplyAdvantageSearchRestResource.doPost();
        Test.stopTest();

        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse body =
                (ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse)JSON.deserialize(
                        RestContext.response.responseBody.toString(),
                        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse.class
                );

        Assert.areEqual(200, RestContext.response.statusCode);
        Assert.areEqual(200, body.response_code);
    }

    @isTest
    static void testCAS500() {
        RestRequest request = new RestRequest();
        request.requestUri = '/api/v1/user/t3t3-hfhfa-1234/comply_advantage';
        request.resourcePath = '/services/apexrest/api/v1/user/*/comply_advantage';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
        ComplyAdvantageSearchRestResource.doPost();
        Test.stopTest();

        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse body =
                (ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse)JSON.deserialize(
                        RestContext.response.responseBody.toString(),
                        ComplyAdvantageSearchRestResource.ComplyAdvantageRestResponse.class
                );

        Assert.areEqual(200, RestContext.response.statusCode);
        Assert.areEqual(500, body.response_code);
    }
}