/**
 * @File Name          : DivisionAliasInfo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 1/3/2024, 3:41:38 AM
**/
public without sharing class DivisionAliasInfo {

	public String division {get; set;}

	public DivisionAliasInfo() {
		// for testing purposes only
	}

	public DivisionAliasInfo(Division_Alias__mdt rec) {
		this.division = rec.Division__c;
	}

	public static DivisionAliasInfo getByAlias(String alias) {
		if (String.isBlank(alias)) {
			return null;
		}
		// else...
		DivisionAliasInfo result = null;
		Division_Alias__mdt rec = Division_Alias__mdt.getInstance(alias);

		if (rec != null) {
			result = new DivisionAliasInfo(rec);
		}
		return result;
	}

}