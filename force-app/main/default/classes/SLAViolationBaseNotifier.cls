/**
 * @File Name          : SLAViolationBaseNotifier.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 5/7/2024, 1:09:35 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/16/2024, 4:18:02 PM   aniubo     Initial Version
 **/
public inherited sharing abstract class SLAViolationBaseNotifier implements SLAViolationNotifiable {
	protected SLAViolationNotificationManager notificationManager { get; set; }
	protected SLAConst.SLAViolationType violationType { get; set; }
	protected SLAConst.SLANotificationObjectType notificationObjectType {
		get;
		set;
	}
	protected Map<String, Milestone_Time_Settings__mdt> settingsByViolationType {
		get;
		set;
	}
	protected ServiceLogManager logManager { get; set; }
	protected SLAViolationDataReader violationDataReader { get; set; }

	protected SLAViolationCanNotifiable violationNotificationChecker {
		get;
		set;
	}
	public SLAViolationBaseNotifier(
		SLAConst.SLANotificationObjectType notificationObjectType,
		SLAConst.SLAViolationType violationType,
		SLAViolationDataReader dataReader,
		SLAViolationCanNotifiable violationNotificationChecker
	) {
		this.notificationObjectType = notificationObjectType;
		this.violationDataReader = dataReader;
		this.violationType = violationType;
		this.settingsByViolationType = getMilestoneTimeSettingsByViolationType();
		logManager = ServiceLogManager.getInstance();
		this.violationNotificationChecker = violationNotificationChecker;
	}

	public abstract void notifyViolation(
		SLAViolationNotificationData notificationData
	);

	protected abstract void setSLAViolationNotificationManager();

	protected virtual List<SLAViolationInfo> getViolationInfos(
		List<SObject> records
	) {
		List<SLAViolationInfo> violationInfos = new List<SLAViolationInfo>();
		for (SObject record : records) {
			violationInfos.add(createViolationInfo(record));
		}
		return violationInfos;
	}

	protected abstract SLAViolationInfo createViolationInfo(SObject record);

	protected virtual List<SLAViolationInfo> filterViolationNotifiable(
		List<SLAViolationInfo> infos
	) {
		return infos;
	}

	protected void saveViolationInfo(List<SLAViolationInfo> violationInfoList) {
		if (violationInfoList != null && !violationInfoList.isEmpty()) {
			List<SLA_Violation__c> slaViolationList = new List<SLA_Violation__c>();
			for (SLAViolationInfo info : violationInfoList) {
				if (info.isPersistent) {
					slaViolationList.add(createViolationRecord(info));
				}
			}
			if (!slaViolationList.isEmpty()) {
				insert slaViolationList;
			}
		}
	}

	protected SLA_Violation__c createViolationRecord(SLAViolationInfo info) {
		return new SLA_Violation__c(
			Agent__c = info.ownerId,
			Case__c = info.caseId,
			Channel__c = info.channel,
			Division__c = info.division,
			Inquiry_Nature__c = info.inquiryNature,
			Tier__c = info.Tier,
			Language__c = info.language,
			Live_or_Practice__c = info.liveOrPractice,
			Violation_Type__c = info.violationType
		);
	}

	protected String getViolationType() {
		return this.violationType == SLAConst.SLAViolationType.ANSWER
			? SLAConst.ALERT_NO_ANSWER_VT
			: this.violationType == SLAConst.SLAViolationType.AHT
					? SLAConst.AHT_EXCEEDED_VT
					: SLAConst.ASA_EXCEEDED_VT;
	}

	protected virtual Map<String, Milestone_Time_Settings__mdt> getMilestoneTimeSettingsByViolationType() {
		return MilestoneUtils.getSettingsFilterByViolationType(
			new List<String>{ getViolationType() }
		);
	}
}