public with sharing class IntroducingBrokerChangedONB extends OutgoingNotificationBus {

    private String recordId = '';

    /********************************* Logic to prepeare new event *********************************************/

    public override String getEventBody(SObject newRecord, SObject oldRecord) {
        String result;

        if (
            ((fxAccount__c) newRecord).Introducing_Broker__c != ((fxAccount__c) oldRecord)?.Introducing_Broker__c
            || ((fxAccount__c) newRecord).Introducing_Broker__r?.Introducing_Broker_Name__c != ((fxAccount__c) oldRecord)?.Introducing_Broker__r?.Introducing_Broker_Name__c
        ) {
            result = JSON.serialize(newRecord);
        }

        if (result != null && String.isBlank(((fxAccount__c) newRecord).fxTrade_One_Id__c)) {
            addEventsError(newRecord.Id, 'IntroducingBrokerChangedONB - missing fxTrade_One_Id__c');
            return null;
        }

        return result;
    }

    public override String getEventKey(SObject newRecord) {
        return JSON.serialize( new Map<String, String> {
                'recordId' =>  ((fxAccount__c) newRecord).Id,
                'tradeOneId' => ((fxAccount__c) newRecord).fxTrade_One_Id__c
        });
    }

    /********************************* Logic to sent callout *********************************************/

    public override void sendRequest(String key, String body) {
        Map<String, Object> keys = (Map<String, Object>) JSON.deserializeUntyped(key);
        recordId = (String)keys.get('recordId');
        UserApiUserPatchRequest.UserPatchRequestWrapper wrapper = getWrapper(body);
        makeRequest((String)keys.get('tradeOneId'), wrapper.user);
    }

    public override String getRecordId() {
        return recordId;
    }

    /********************************* Helpers *********************************************/

    private UserApiUserPatchRequest.UserPatchRequestWrapper getWrapper(String body) {
        fxAccount__c updatedRecord = (fxAccount__c) JSON.deserialize(body, fxAccount__c.class);
        if (updatedRecord.Introducing_Broker__c != null && updatedRecord.Introducing_Broker__r.Introducing_Broker_Name__c == null) {
            updatedRecord = [
                    SELECT
                            Id, fxTrade_User_Id__c, Introducing_Broker__r.Introducing_Broker_Name__c
                    FROM fxAccount__c
                    WHERE Id = :updatedRecord.Id
                    LIMIT 1
            ];
        }

        UserApiUserPatchRequest.UserPatchRequestWrapper wrapper =
                UserApiUserPatchRequest.getUpdateIntroducingBrokerWrapper(updatedRecord);
        return wrapper;
    }

    private void makeRequest(String oneId, UserApiUserPatchRequest wrapper){
        NamedCredentialsUtil.NamedCredentialData namedCredentialData;
        namedCredentialData = NamedCredentialsUtil.getOrgDefinedNamedCredential(Constants.USER_API_NAMED_CREDENTIALS_NAME, Constants.PRINCIPAL_NAME_CREDENTIALS, '');
        
        String baseUrl = namedCredentialData.credentialUrl;

        String resource = String.format(
            SPGeneralSettings.getInstance().getValue(
                Constants.USER_API_INTRODUCING_BROKER_ENDPOINT),
            new List<String> {oneId}
        );

        String currentUrl = baseUrl + resource;

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(currentUrl);
        req.setBody(JSON.serialize(wrapper, true));
        req.setMethod('PATCH');
        req.setHeader('Content-Type', 'application/json');

        HttpResponse response = http.send(req);

        if (response.getStatusCode() != 200){
            throw new ApplicationException(response.getBody());
        }
    }

}