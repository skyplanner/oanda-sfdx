/*
 *	Author : Deepak Malkani.
 *	Created Date : July 26 2016
*/
public with sharing class GlobalStaticVar {
	public GlobalStaticVar() {}

	//Initialise variables
	public static Boolean canRun = true;

	public static boolean canIRun(){
		return canRun;
	}

	public static void stopTrigger(){
		canRun = false;
	}
}