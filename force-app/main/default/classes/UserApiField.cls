/**
 * User API field
 */
public abstract class UserApiField {
    @AuraEnabled
    public String name { get; set; }

    @AuraEnabled
    public String label { get; set; }

    @AuraEnabled
    public Boolean showOnList { get; set; }

    @AuraEnabled
    public Boolean showOnDetails { get; set; }

    @AuraEnabled
    public Boolean readOnly { get; set; }

    @AuraEnabled
    public Integer order { get; set; }

    @AuraEnabled
    public Boolean isText { get; set; }

    @AuraEnabled
    public Boolean isTextArea { get; set; }

    @AuraEnabled
    public Boolean isCheck { get; set; }

    @AuraEnabled
    public Boolean isEmail { get; set; }

    @AuraEnabled
    public Boolean isNumber { get; set; }

    @AuraEnabled
    public Boolean isDate { get; set; }

    @AuraEnabled
    public Boolean isDateTime { get; set; }

    @AuraEnabled
    public Boolean isPicklist { get; set; }

    @AuraEnabled
    public Boolean isPhone { get; set; }
}