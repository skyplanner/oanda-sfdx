/**
 * @description       : helper class to get/update external credential token
 * @author            : OANDA
 * @group             :
 * @last modified on  : 20 May 2024
 * @last modified by  : Ryta Yahavets
**/
public with sharing class TokenHelper {

    private static final String NAMED_CRED_RELATIVE_URI = '/services/data/v60.0/named-credentials/credential';

    private static final String TOKEN = '_token';

    private static final String ACCESS_TOKEN = 'access_token';
    private static final String ACCESS_TOKEN_EXPIRE_DATE = 'access_token_expire_date';

    private static final Integer MINUTES_BEFORE_TOKEN_EXPIRATION = 10;
    private static final Set<Integer> SUCCESS_STATUS_CODES = new Set<Integer>{200, 201};

    //To support other tokens a new one should be added here(used in getNewToken())
    private enum CONTEXT {
        User_API,
        Tas_API
    }

    //To support other tokens a new one should be added here
    private static final Map<String, String> manageTokenEndpointMap = new Map<String, String>{
        Constants.USER_API_NAMED_CREDENTIALS_NAME => Constants.USER_API_NAMED_CREDENTIALS_NAME,
        Constants.TAS_API_NAMED_CREDENTIALS_NAME => Constants.USER_API_NAMED_CREDENTIALS_NAME
    };

    //To support other tokens a new one should be added here
    private static final Map<String, String> audienceType = new Map<String, String>{
        Constants.USER_API_NAMED_CREDENTIALS_NAME => 'user-api',
        Constants.TAS_API_NAMED_CREDENTIALS_NAME => 'tas-api'
    };

    /**
	 * @param namedCredentialName Name of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
	 * @param principlesName principal name to get correct Authentication Parameters if the value is empty or cannot be found the first principal from the list will be used.
	 * @return the TokenHelperResult based on input params (also contains errors getting during execution of HTTP or Named Credentials)
	 * @description the updateNamedCredentialToken can be use to immediate token updating
	 */
    public static TokenHelperResult updateNamedCredentialToken(String namedCredentialName, String principlesName) {
        TokenHelperResult result = new TokenHelperResult();
        Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(namedCredentialName, principlesName);
        if ((String)credentialResultMap.get('status') == Constants.STATUS_ERROR) {
            result.status = (String)credentialResultMap.get('status');
            result.message = (String)credentialResultMap.get('message');
            return result;
        }

        return manageNamedCredentialToken(namedCredentialName, (ConnectApi.Credential)credentialResultMap.get('credential'),true);
    }

    /**
	 * @param namedCredentialName Name of named credentials (used to get endpoint for a new token)
	 * @param credential for that ConnectApi.Credential will be check or update token
	 * @param updateImmediately boolean variable to control when should be get new token. NOTE: better to do this in Anonymous Developer Console or Batch Apex
	 * @return the TokenHelperResult based on input params (also contains errors getting during execution of HTTP or Named Credentials)
	 * @description the TokenHelperResult used in 2 modes based on the updateImmediately value,
	 * if true (actual for Anonymous Developer Console or Batch Apex for immediate token refresh) and false for any other places
	 */
    public static TokenHelperResult manageNamedCredentialToken(String namedCredentialName, ConnectApi.Credential credential, Boolean updateImmediately) {
        TokenHelperResult result = new TokenHelperResult();
        if (credential == null) {
            result.status = Constants.STATUS_ERROR;
            result.message = 'The correct configuration was not found for ' + namedCredentialName + Util.getValidCredentialSuffix() + ' named credentials. Please reach to admins to review the named credentials and configuration';
            return result;
        }

        if (!updateImmediately && !isNeedUpdateToken(credential)) {
            return result;
        }

        if (!updateImmediately && isTokenSentOnUpdating(namedCredentialName + Util.getValidCredentialSuffix())) {
            result.status = Constants.STATUS_WARNING;
            result.message = 'The token has been sent for renewal. Please refresh the page in a few minutes.';
            return result;
        }

        ApiAuthentication authentication = getNewToken(
                namedCredentialName,
                result);

        if (authentication == null && !result.isSuccess) {
            return result;
        }

        // The method use UserInfo.getSessionId() and do not work correctly with Lightning components by security policy (sessions created by Lightning components aren’t enabled for API access).
        // to receive token for this case was implemented logic based on platform event
        result = updateToken(credential, authentication);

        result.minToTokenExpire = authentication.expires_in / 60;

        return result;
    }

    /**
     * @param credential ConnectApi.Credential to check token expiration date
     * @description method to get time in minutes between token expiration and System.now()
     * @return the TokenHelperResult
     */
    public static Integer getMinutesBetweenNowAndTokenExpirationDate(ConnectApi.Credential credential) {
        Integer result = 0;
        if (credential.credentials.containsKey(ACCESS_TOKEN)
                && credential.credentials.containsKey(ACCESS_TOKEN_EXPIRE_DATE)) {

            ConnectApi.CredentialValue expires = credential.credentials.get(ACCESS_TOKEN_EXPIRE_DATE);
            if (expires.value != null) {
                result = (Integer) (Datetime.valueOf(expires.value).getTime() - System.now().getTime()) / 60000;
            }
        }

        return result;
    }

    /**
    * @param input credential data to update in Map<String, Object> format
    * @param mappingExistsAlready boolean variable to track if external credentials already has token info (changes type of request PUT/POST)
     * @return the HttpResponse
    */
    public static HttpRequest getUpdateNamedCredentialRequest(String body, Boolean isUpdateCredentials){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Test.isRunningTest()
                ? 'http://currentorg.url.com'
                : Url.getOrgDomainUrl().toExternalForm() + NAMED_CRED_RELATIVE_URI);
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        req.setMethod(isUpdateCredentials ? 'PATCH' : 'POST');
        req.setTimeout(120000);
        req.setBody(body);

        return req;
    }

    //******************** Helpers ******************************
    /**
	 * @param credential
	 * @description method checks if the input credential has a valid token
	 * @return the Boolean variable
	 */
    private static Boolean isNeedUpdateToken(ConnectApi.Credential credential) {
        if (credential.credentials.containsKey(ACCESS_TOKEN)
                && credential.credentials.containsKey(ACCESS_TOKEN_EXPIRE_DATE)) {

            ConnectApi.CredentialValue expires = credential.credentials.get(ACCESS_TOKEN_EXPIRE_DATE);
            return Test.isRunningTest()
                    ? true
                    : (expires.value != null && Datetime.valueOf(expires.value) < System.now().addMinutes(MINUTES_BEFORE_TOKEN_EXPIRATION));
        }
        return true;
    }

    /**
    * @param namedCredentialName Name of named credentials (without _DEV, _STG, _PROD, this postfix will be added during method execution)
    * @param tokenHelperResult used to track errors
    * @description method make a callout to get a new token
    * @return the ApiAuthentication with the new token info
    */
    private static ApiAuthentication getNewToken(String namedCredentialName, TokenHelperResult tokenHelperResult) {
        ApiAuthentication authData;
        String namedCredentialsForToken = manageTokenEndpointMap.get(namedCredentialName) + TOKEN;

        Map<String, Object> credentialResultMap = NamedCredentialsUtil.getCredential(namedCredentialsForToken, Constants.PRINCIPAL_NAME_CREDENTIALS);
        if ((String)credentialResultMap.get('status') == Constants.STATUS_ERROR || credentialResultMap.get('credential') == null) {
            tokenHelperResult.status  = Constants.STATUS_ERROR;
            tokenHelperResult.message = (credentialResultMap.get('credential') == null)
                ? ('The correct configuration was not found for ' + namedCredentialsForToken + Util.getValidCredentialSuffix() + ' named credentials. Please reach to admins to review the named credentials and configuration')
                : (String)credentialResultMap.get('message');

            return authData;
        }
        ConnectApi.Credential credential = (ConnectApi.Credential) credentialResultMap.get('credential');
        ConnectApi.NamedCredential namedCredential = (ConnectApi.NamedCredential) credentialResultMap.get('namedCredential');

        HttpRequest req = new HttpRequest();
        req.setEndpoint(Test.isRunningTest()
                ? 'http://collaut.test.com'
                : Constants.CALLOUTS_CONSTANT + namedCredential.developerName);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');

        CONTEXT context =  CONTEXT.valueof(namedCredentialName);
        
        switch on context {
            when Tas_API, User_API {
                req.setBody('{"client_id": "{!$Credential.' + credential.externalCredential + '.client_id}",'
                        + '"client_secret": "{!$Credential.' + credential.externalCredential + '.client_secret}",'
                        + '"audience": "' + audienceType.get(namedCredentialName) + '",'
                        + '"grant_type": "client_credentials"}');
            } when else {

            }
        }

        Http http = new Http();
        HttpResponse response = http.send(req);
        if (SUCCESS_STATUS_CODES.contains(response.getStatusCode()) && response?.getBody() != null) {
            authData = (ApiAuthentication)JSON.deserialize(response.getBody(), ApiAuthentication.class);
        } else {
            tokenHelperResult.status = Constants.STATUS_ERROR;
            tokenHelperResult.message = 'Service error. Status code: ' + response.getStatusCode() + '. Please reach to admins to review the integration.';
        }

        return authData;
    }

    /**
    * @param credential actual NamedCredential
    * @param authentication new token values
    * @description method is prepare data to update external credentials based on new authentication values.
    * @return the HttpResponse
    */
    private static TokenHelperResult updateToken(ConnectApi.Credential credential, ApiAuthentication authentication) {
        TokenHelperResult result = new TokenHelperResult();
        Map<String, Object> credentialToRequestMap = new Map<String, Object>{
                'authenticationProtocol' => String.valueOf(credential.authenticationProtocol),
                'externalCredential'     => String.valueOf(credential.externalCredential),
                'principalName'          => String.valueOf(credential.principalName),
                'principalType'          => String.valueOf(credential.principalType)
        };
        credentialToRequestMap.put(Constants.PRINCIPAL_NAME_CREDENTIALS, new Map<String, Map<String, Object>>());

        ((Map<String, Map<String, Object>>)credentialToRequestMap.get(Constants.PRINCIPAL_NAME_CREDENTIALS)).put(ACCESS_TOKEN, new Map<String, Object>{
                'encrypted' => true,
                'value' => authentication.token_type + ' ' + authentication.access_token
        });

        ((Map<String, Map<String, Object>>)credentialToRequestMap.get(Constants.PRINCIPAL_NAME_CREDENTIALS)).put(ACCESS_TOKEN_EXPIRE_DATE, new Map<String, Object>{
                'encrypted' => false,
                'value' => String.valueOf(System.now().addSeconds(authentication.expires_in))
        });

        HttpRequest request = getUpdateNamedCredentialRequest(
                JSON.serialize(credentialToRequestMap),
                !credential.credentials.isEmpty()
        );

        HttpResponse response = new Http().send(request);
        if (!SUCCESS_STATUS_CODES.contains(response.getStatusCode())) {
            if (String.isNotBlank(response.getBody()) && response.getBody().contains('INVALID_SESSION_ID')) {
                result.status = Constants.STATUS_WARNING;
                result.message = 'The token has been sent for renewal. Please refresh the page in a few minutes.';
            } else {
                result.status = Constants.STATUS_ERROR;
                result.message = 'Status code: ' + response.getStatusCode() + '. Body:' + response.getBody();
            }

            EventBus.publish(
                new Update_Token__e(
                    External_Credential_DeveloperName__c = credential.externalCredential,
                    Request_Body__c = JSON.serialize(credentialToRequestMap),
                    Is_Update_Credential__c = !credential.credentials.isEmpty()
                )
            );
        }

        return result;
    }

    /**
   * @param namedCredentialDeveloperName Developer Name of named credentials
   * @description method used for send Update_Token__e
   */
    private static Boolean isTokenSentOnUpdating(String namedCredentialDeveloperName) {
        List<String> inProgressStatuses = new List<String>{'Preparing', 'Processing', 'Queued'};
        Boolean isJobRunning = [SELECT count()
            FROM AsyncApexJob
            WHERE JobType = 'Queueable'
                AND ApexClass.Name = 'UpdateTokenEnvTriggerHelper'
                AND Status IN :inProgressStatuses
        ] > 1;

        Boolean isLogRecordActual = false;
        if (!isJobRunning) {
            isLogRecordActual = [SELECT count()
                FROM Log__c
                WHERE Category__c = :('Update token for ' + namedCredentialDeveloperName)
                AND CreatedDate > :System.now().addMinutes(-5)
                ] > 1;

            if (!isLogRecordActual) {
                Logger.warning(
                        'Token was sent to refresh and will update form UpdateTokenEnvTrigger',
                        'Update token for ' + namedCredentialDeveloperName);
            }
        }

        return isJobRunning || isLogRecordActual;
    }

    //***************** Inner Classes ****************************

    public class ApiAuthentication {
        String access_token {get; set;}
        String token_type  {get; set;}
        Integer expires_in {get; set;}
    }

    public class TokenHelperResult {
        public String status {
            get {
                if (String.isBlank(status)) {
                    status = Constants.STATUS_SUCCESS;
                }
                return status;
            }
            private set;
        }

        public Boolean isSuccess {
            get {return (status == Constants.STATUS_SUCCESS);}
            private set;
        }

        public String message {get; private set;}
        public Integer minToTokenExpire {get; private set;}
        public ConnectApi.Credential credential {get; private set;}
    }
}