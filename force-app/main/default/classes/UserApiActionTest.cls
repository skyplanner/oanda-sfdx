/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-21-2023
 * @last modified by  : Dianelys Velazquez
**/
@isTest
public class UserApiActionTest {
    @isTest
    public static void userApiActionCloseAccOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionCloseAcc action = new UserApiActionCloseAcc();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }
    
    @isTest
    public static void userApiActionEnableAllInterestOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionEnableAllInterest action
            = new UserApiActionEnableAllInterest();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }    
    
    @isTest
    public static void userApiActionEnableBalInterestTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionEnableBalInterest action
            = new UserApiActionEnableBalInterest();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionGrantAPIAccessTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionGrantAPIAccess action
            = new UserApiActionGrantAPIAccess();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionGrantPremiumAccessTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionGrantPremiumAccess action
            = new UserApiActionGrantPremiumAccess();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }
    
    @isTest
    public static void userApiActionLockAccountOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionLockAccount action = new UserApiActionLockAccount();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }    
    
    @isTest
    public static void userApiActionLockDepositsOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionLockDeposits action = new UserApiActionLockDeposits();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }
    
    @isTest
    public static void userApiActionLockTpaAuthOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionLockTpaAuth action = new UserApiActionLockTpaAuth();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }
    
    @isTest
    public static void userApiActionLockTradingOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionLockTrading action = new UserApiActionLockTrading();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionLockWithdrawalsOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionLockWithdrawals action 
            = new UserApiActionLockWithdrawals();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionSetDocsApprovedOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionSetDocsApproved action 
            = new UserApiActionSetDocsApproved();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionSetEmailValidatedOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionSetEmailValidated action 
            = new UserApiActionSetEmailValidated();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionSetRegisCompletedOkTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionSetRegisCompleted action 
            = new UserApiActionSetRegisCompleted();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    @isTest
    public static void userApiActionCloseAccInvalidUserIdTest() {
        Boolean throwException = false;
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = new Map<String, Object> {
            'actionName' => 'Action Name'
        };
        UserApiActionCloseAcc action = new UserApiActionCloseAcc();

        try {
            action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
        }
        catch(Exception e){
            throwException = true;
        }
            
        Test.stopTest();
        
        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    public static void userApiActionCloseReturnsInternalServerErrorShouldThrowExceptionTest() {
        Boolean throwException = false;
        Test.setMock(HttpCalloutMock.class, CalloutMock.getErrorMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionCloseAcc action = new UserApiActionCloseAcc();

        try {
            action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
        }
        catch(Exception e){
            throwException = true;
        }
            
        Test.stopTest();
        
        System.assert(throwException, 'Should throw exception.');
    }

    @isTest
    public static void userApiActionUpdatePIUTest() {
        Test.setMock(HttpCalloutMock.class, CalloutMock.getOkMock());

        Test.startTest();

        Map<String, Object> record = getValidRecord();
        UserApiActionUpdatePIU action = new UserApiActionUpdatePIU();
        action.call(UserApiActionBase.ACTION_NAME_INVOKE_ACTION, record);
            
        Test.stopTest();
        
        System.assert(true, 'Everything goes ok');
    }

    public static Map<String, Object> getValidRecord() {
        return new Map<String, Object> {
            'fxTrade_User_Id__c' => 'userId',
            'Env' => 'demo',
            'actionName' => 'Action_Name',
            'actionLabel' => 'Action Label'
        };
    }
}