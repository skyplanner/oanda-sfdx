/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-22-2022
 * @last modified by  : Ariel Niubo
 **/
@isTest
private class ScheduledEmailSetting_Test {
	@isTest
	private static void settingIsNull() {
		// Test data setup

		// Actual test
		ScheduledEmailSetting setting = new ScheduledEmailSetting(null);
		System.assertEquals(
			ScheduledEmailSetting.DEFAULT_SCOPE,
			setting.getScope(),
			'Ten is the value by default'
		);
		System.assertEquals(
			ScheduledEmailSetting.DEFAULT_AM_PM,
			setting.getAMOrPM(),
			'PM is the value by default'
		);
		System.assertEquals(
			false,
			setting.getAllowDelete(),
			'false is the value by default'
		);
		System.assertEquals(
			ScheduledEmailSetting.DEFAULT_TIME,
			setting.getTime(),
			'4 is the value by default'
		);
		Integer day = setting.convertWeekDayToInteger(
			ScheduledEmailSetting.DEFAULT_WEEK_DAY
		);
		Date nextSunday = setting.nextDate(day);
		System.assertEquals(
			nextSunday,
			setting.getDate(),
			'next sunday is the value by default'
		);

		// Asserts
	}
	@isTest
	private static void convertWeekDayToInteger() {
		// Test data setup

		// Actual test
		ScheduledEmailSetting setting = new ScheduledEmailSetting(null);
		Integer day = setting.convertWeekDayToInteger('Sunday');
		System.assertEquals(0, day, 'Sunday is zero');
		day = setting.convertWeekDayToInteger('Monday');
		System.assertEquals(1, day, 'Monday is one');
		day = setting.convertWeekDayToInteger('Tuesday');
		System.assertEquals(2, day, 'Tuesday is two');
		day = setting.convertWeekDayToInteger('Wednesday');
		System.assertEquals(3, day, 'Wednesday is three');
		day = setting.convertWeekDayToInteger('Thursday');
		System.assertEquals(4, day, 'Thursday is four');
		day = setting.convertWeekDayToInteger('Friday');
		System.assertEquals(5, day, 'Friday is five');
		day = setting.convertWeekDayToInteger('Saturday');
		System.assertEquals(6, day, 'Saturday is six');
		// Asserts
	}
	@isTest
	private static void settingIsOk() {
		// Test data setup
		Schedule_Mail_Setting__mdt settingMtd = ScheduleMailSettingRepo.getFirst();

		// Actual test
		ScheduledEmailSetting setting = new ScheduledEmailSetting(settingMtd);
		System.assertEquals(
			settingMtd.Scope__c,
			setting.getScope(),
			'Ten is the value by default'
		);
		System.assertEquals(
			settingMtd.AM_or_PM__c,
			setting.getAMOrPM(),
			'PM is the value by default'
		);
		System.assertEquals(
			settingMtd.Delete_a_Draft_Email__c,
			setting.getAllowDelete(),
			'false is the value by default'
		);
		System.assertEquals(
			settingMtd.Time__c,
			setting.getTime(),
			'4 is the value by default'
		);
		Integer day = setting.convertWeekDayToInteger(
			settingMtd.Day_of_the_Week__c
		);
		Date nextDay = setting.nextDate(day);
		System.assertEquals(
			nextDay,
			setting.getDate(),
			'next sunday is the value by default'
		);

		// Asserts
	}
}