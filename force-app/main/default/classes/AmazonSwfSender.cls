public class AmazonSwfSender
{
    private String getSwfDate() {
        return DateTime.now().formatGmt('yyyyMMdd');
    }
 
    private String getSwfTime() {
        return 'T' + DateTime.now().formatGmt('HHmmss') + 'Z';
    }

    public void send(String message, String target) {        
        String access_key ='AKIAJNXUVDCMKHHI4JMA';
        String secret_key = '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';
        String host = 'swf.us-east-1.amazonaws.com';
        String region_name = 'us-east-1';
        String service_name = 'swf';
        
        String datestamp = getSwfDate(); // '20141210';
        String timestamp = datestamp + getSwfTime(); // '20141210T000445Z';
 
        Map<String,String> headers = new Map<String,String>();

        headers.put('Content-Length', String.valueOf(message.length()));
        headers.put('Content-Encoding', 'amz-1.0');
        headers.put('X-Amz-Target', target);
        headers.put('X-Amz-Date', timestamp);
        headers.put('Host', host);
        headers.put('Content-Type', 'application/json; charset=UTF-8');

        String signed_headers = 'host;x-amz-date;x-amz-target';
        
        String canonical_request = 'POST\n/\n\nhost:' + host + '\nx-amz-date:' + timestamp + '\nx-amz-target:' + target + '\n\n' + signed_headers + '\n' + getSHA256(message);

        String scope = datestamp + '/' + region_name + '/' + service_name + '/aws4_request';
        
        String string_to_sign = 'AWS4-HMAC-SHA256\n' + timestamp + '\n' + scope + '\n' + getSHA256(canonical_request);

        Blob k_date = getMac('AWS4' + secret_key, datestamp);
        Blob k_region = getMac(k_date, region_name);
        Blob k_service = getMac(k_region, service_name);
        Blob k_signing = getMac(k_service, 'aws4_request');
        String signature = getMacHex(k_signing, string_to_sign);

        String auth_header = 'AWS4-HMAC-SHA256 Credential=' + access_key + '/' + scope + ',SignedHeaders=' + signed_headers + ',Signature=' + signature;

        headers.put('Authorization', auth_header);

        String url = 'https://'+ host +'/';
 
        HttpRequest req = new HttpRequest();
        req.setEndPoint(url);
        
        for (String key : headers.keySet()) {
            req.setHeader(key, headers.get(key));
        }
        
        req.setBody(message);
        req.setMethod('POST');
        Http http = new Http();
        try {
            HttpResponse res = http.send(req);
            System.debug('Status: ' + res.getStatus());
            System.debug('Code  : ' + res.getStatusCode());
            System.debug('Body  : ' + res.getBody());
        }
        catch (System.CalloutException e) {
            System.debug('ERROR: ' + e);
        } 
    }
 
    private String getSHA256(String message) {
        String algorithmName = 'SHA-256';
        Blob input = Blob.valueOf(message);
        return EncodingUtil.convertToHex(Crypto.generateDigest(algorithmName, input));
    }

    private Blob getMac(Blob key, String message) {
        String algorithmName = 'HmacSHA256';
        Blob input = Blob.valueOf(message);
        return Crypto.generateMac(algorithmName, input, key);
    }
    
    private Blob getMac(String key, String message) {
        return getMac(Blob.valueOf(key), message);
    }
    
    private String getMacHex(Blob key, String message) {
        return EncodingUtil.convertToHex(getMac(key, message));
    }

    public static void startWorkflow(String username) {
        AmazonSwfSender t = new AmazonSwfSender();
        String target = 'com.amazonaws.swf.service.model.SimpleWorkflowService.StartWorkflowExecution';
        String message = '{"domain": "activation.dev", "workflowId": "approve_documents_' + username + '", "workflowType":{"name": "approve_documents", "version": "v0.1"}, "taskList":{"name": "r10n"}, "input": "{\\\"username\\\":\\\"' + username + '\\\"}"}';
        t.send(message, target);
    }
}