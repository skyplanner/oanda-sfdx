/**
 * @File Name          : InitMsgSessionActionTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/14/2024, 3:40:22 AM
**/
@IsTest
private without sharing class InitMsgSessionActionTest {

	// exception
	@IsTest
	static void initMsgSession1() {
		List<InitMsgSessionAction.ActionInfo> infoList = null;
		Boolean error = false;
		
		Test.startTest();
		ExceptionTestUtil.prepareDummyException();

		try {
			List<InitMsgSessionAction.ActionResult> result = 
				InitMsgSessionAction.initMsgSession(infoList);

		} catch(Exception ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	} 

	// test 1 : messagingSessionIds = null
	// test 1 : messagingSessionIds is empty
	@IsTest
	static void initMsgSession2() {
		List<InitMsgSessionAction.ActionInfo> infoList1 = null;
		List<InitMsgSessionAction.ActionInfo> infoList2 = 
			new List<InitMsgSessionAction.ActionInfo>();

		Test.startTest();
		// test 1
		List<InitMsgSessionAction.ActionResult> result1 = 
			InitMsgSessionAction.initMsgSession(infoList1);
		// test 2
		List<InitMsgSessionAction.ActionResult> result2 = 
			InitMsgSessionAction.initMsgSession(infoList2);
		Test.stopTest();
		
		// test 1 asserts
		Assert.isNull(result1[0].region, 'Invalid result');
		Assert.isFalse(result1[0].requestRegion, 'Invalid result');
		// test 2 asserts
		Assert.isNull(result2[0].region, 'Invalid result');
		Assert.isFalse(result2[0].requestRegion, 'Invalid result');
	} 
	
	// ok (infoList is not empty)
	@IsTest
	static void initMsgSession3() {
		List<InitMsgSessionAction.ActionInfo> infoList = 
			new List<InitMsgSessionAction.ActionInfo> { 
				new InitMsgSessionAction.ActionInfo() 
			};

		Test.startTest();
		List<InitMsgSessionAction.ActionResult> result = 
			InitMsgSessionAction.initMsgSession(infoList);
		Test.stopTest();
		
		Assert.isNotNull(result[0], 'Invalid result');
	} 
	
}