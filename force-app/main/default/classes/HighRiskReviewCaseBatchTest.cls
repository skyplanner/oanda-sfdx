/**
 * @Description  : Test class for HighRiskReviewCaseBatch.
 * @Author       : Jakub Fik
 * @Date         : 2024-06-27
**/
@IsTest
public with sharing class HighRiskReviewCaseBatchTest {

    @TestSetup
    static void setup(){
        UserUtil.currentUser.Triggers_Disabled__c = true;
        insert new fxAccount__c(
            Account_Email__c = 'test@email.com',
            Division_Name__c = Constants.DIVISIONS_OANDA_CORPORATION,
            High_Risk_account_review_due_date__c = date.today()
        );
    }

    @IsTest
    static void batchTest() {
        HighRiskReviewCaseBatch testBatch = new HighRiskReviewCaseBatch(
        );

        Test.startTest();
        Id batchId = Database.executeBatch(testBatch);
        Test.stopTest();

        AsyncApexJob jobResults = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed
            FROM AsyncApexJob
            WHERE Id = :batchId
        ];

        Integer newCasesCounter = [SELECT COUNT() FROM Case];

        System.assertEquals(
            'Completed',
            jobResults.Status,
            'Batch should process only one record.'
        );
        System.assertEquals(0, jobResults.NumberOfErrors, 'Batch shouldn\'t have any errors.');
        System.assertEquals(
            1,
            jobResults.JobItemsProcessed,
            'Batch should process only one record.'
        );
        System.assertEquals(1, newCasesCounter, 'Should be one new case created.');
    }
}