/**
 * User API 'Unset Registration Completed' action manager class
 */
public class UserApiActionUnsetRegisCompleted extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.unsetRegistrationCompleted();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.registrationCompleted;
    }
}