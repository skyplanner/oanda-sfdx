@RestResource(urlMapping='/ComplyAdvantage/monitor')
global without sharing class ComplyAdvantageMonitor
{
   public static final string complianceCheckCaseRecordTypeId = RecordTypeUtil.getComplianceCheckCaseRecordTypeId();
    
   public static final string MONITOR_SEARCH_UPDATED_EVENT = 'monitored_search_updated';
   public static final string SEARCH_STATUS_UPDATED_EVENT = 'search_status_updated';
   public static final string ENTITY_MATCH_STATUS_UPDATED_EVENT = 'match_status_updated';
   
   public static final string CASE_STATUS_OPEN = 'Open';
   public static final string CASE_STATUS_CLOSED = 'Closed';
   public static final string CASE_STATUS_VERIFIED_CLOSED = 'Verified and Closed';

   public static final set<string>  COMPLY_ADVANTAGE_SEARCH_FEILDS = new set<string>
   {
    'Id',
    'Search_Id__c',
    'search_reference__c',
    'search_text__c',
    'search_parameter_birth_year__c',
    'search_parameter_citizenship_nationality__c',
    'search_parameter_mailing_country__c',
    'Match_Status__c',
    'total_hits__c', 
    'custom_division_name__c',
    'division_name__c',
    'monitoring_turned_off_datetime__c',
    'monitoring_turned_on_datetime__c',
    'entities_download_status__c',
    'monitor_changes_acknowledged_date__c',
    'monitor_changes_acknowledged_by__c',
    'has_monitor_changes__c',
    'monitor_change_date__c',
    'fxAccount__c',
    'fxAccount__r.Name',
    'fxAccount__r.Division_Name__c',
    'fxAccount__r.Funnel_Stage__c',
    'Affiliate__c',
    'Affiliate__r.Name',
    'Case__c',
    'Case__r.ContactId',
    'Case__r.Lead__c',
    'Case__r.AccountId',
    'Case__r.Affiliate__c',
    'Case__r.Priority',
    'Case__r.status',
    'Case__r.RecordTypeId',
    'Case__r.OwnerId',
    'Case__r.parentId'
   };
   public static ComplyAdvantageMonitorNotification.MonitorData monitorNotificationData;
   public static Comply_Advantage_Search__c complyAdvantageSearch;

   @HttpPost 
   global static void doPost()
   {  
       if(!validateIp(RestContext.request))
       {
            system.debug('Unknown IP');
            return;
       }    
       if(Settings__c.getValues('Default') != null && 
          Settings__c.getValues('Default').Comply_Advantage_Enabled__c) 
       {
            try 
            {
                string requestBody = RestContext.request.requestBody.toString();
                requestBody = requestBody.replaceAll('"new":', '"added":');

                ComplyAdvantageMonitorNotification monitorNotification = (ComplyAdvantageMonitorNotification)JSON.deserialize(requestBody, ComplyAdvantageMonitorNotification.class);
                String eventName = monitorNotification.event;
                monitorNotificationData = monitorNotification.data;
                string searchId = monitorNotificationData.search_id;

                if(eventName == MONITOR_SEARCH_UPDATED_EVENT)
                {
                    Logger.info('ComplyAdvantageMonitor-reqBody', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, requestBody);
                }
                
                string query = 'select ' + SoqlUtil.getSoqlFieldList(COMPLY_ADVANTAGE_SEARCH_FEILDS)+ ' from Comply_Advantage_Search__c where Search_Id__c =:searchId';
                Comply_Advantage_Search__c[] caSearches =  (Comply_Advantage_Search__c[]) Database.Query(query);
                
                if(caSearches.size() > 0 && caSearches[0].Case__r != null)
                {
                    complyAdvantageSearch = caSearches[0];
                    Case complyAdvatageCase =  complyAdvantageSearch.Case__r;

                    Logger.debug('ComplyAdvantageMonitor-complyAdvantageSearch', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY,complyAdvantageSearch);
                    
                    if(eventName == SEARCH_STATUS_UPDATED_EVENT)
                    {   
                        searchStatusChanged();
                    }
                    if(eventName == MONITOR_SEARCH_UPDATED_EVENT && complyAdvatageCase.RecordTypeId == complianceCheckCaseRecordTypeId)
                    {
                        monitorSearchUpdated();
                    }
                    if(eventName == ENTITY_MATCH_STATUS_UPDATED_EVENT)
                    {   
                        entityMatchStatusUpdated();
                    }
                }
            } 
            catch (Exception ex) 
            {
               system.debug('Error parsing monitor data' + ex.getMessage());
            }
        }
    }
    
    private static Boolean validateIp(RestRequest request) 
    {
        if(Test.isRunningTest())
        {
           return true;
        }
		return SPGeneralSettings.getInstance().getValueAsList('CA_Allowed_IP_Addresses', ',').contains(request.remoteAddress);
	}
    
    private static void monitorSearchUpdated()
    {
        complyAdvantageSearch.Monitor_Change_Date__c = System.today();
        complyAdvantageSearch.Has_Monitor_Changes__c = true;
        update complyAdvantageSearch;
        
        ComplyAdvantageMonitorChange monitorChange = new ComplyAdvantageMonitorChange(complyAdvantageSearch, monitorNotificationData);
        system.enqueueJob(monitorChange);
    }
    private static void searchStatusChanged()
    {
        if(monitorNotificationData.changes != null &&  monitorNotificationData.changes.after != null)
        {
            if(monitorNotificationData.changes.after.match_status != null && 
                complyAdvantageSearch.Match_Status__c != monitorNotificationData.changes.after.match_status)
            {
                string newMatchStatus = monitorNotificationData.changes.after.match_status;
                string oldStatus = complyAdvantageSearch.Match_Status__c;
                complyAdvantageSearch.Match_Status__c = newMatchStatus;
                complyAdvantageSearch.Match_Status_Override_Reason__c = 'dummy';
                ComplyAdvantageHelper.updateComplyAdvantageSearch(complyAdvantageSearch, false); 

                complyAdvantageSearch.Match_Status_Override_Reason__c = '';
                ComplyAdvantageHelper.updateComplyAdvantageSearch(complyAdvantageSearch, false); 
                
                Note statusOverrideReasonNote = new Note();
                statusOverrideReasonNote.ParentId = complyAdvantageSearch.Id;
                statusOverrideReasonNote.Body =  'Match status changed from Comply Advantage case management dashboard';
                statusOverrideReasonNote.Title = 'Match Status Changed From ' + oldStatus + ' to ' + newMatchStatus;
                Insert statusOverrideReasonNote;
            }
            if(monitorNotificationData.changes.after.risk_level != null && 
                complyAdvantageSearch.Risk_Level__c != monitorNotificationData.changes.after.risk_level)
            {
                string risk_new = monitorNotificationData.changes.after.risk_level;
                string risk_old = complyAdvantageSearch.Risk_Level__c;
                complyAdvantageSearch.Risk_Level__c = risk_new;
                ComplyAdvantageHelper.updateComplyAdvantageSearch(complyAdvantageSearch, false); 
                
                Note statusOverrideReasonNote = new Note();
                statusOverrideReasonNote.ParentId = complyAdvantageSearch.Id;
                statusOverrideReasonNote.Body =  'Risk Level changed from Comply Advantage case management dashboard';
                statusOverrideReasonNote.Title = 'Risk Level Changed From ' + risk_old + ' to ' + risk_new;
                Insert statusOverrideReasonNote;
            }
        }
    }
    private static void entityMatchStatusUpdated()
    {
        string searchId = monitorNotificationData.search_id;
        string entityId = monitorNotificationData.entity_id;
        string uniqueId = monitorNotificationData.search_id + ':' + monitorNotificationData.entity_id;

        Comply_Advantage_Search_Entity__c[] entities = [select Id, 
                                                               Match_Status__c,
                                                               Is_Whitelisted__c 
                                                        From Comply_Advantage_Search_Entity__c  
                                                        where Unique_ID__c=:uniqueId];

        if(entities.size() > 0)
        {
            Comply_Advantage_Search_Entity__c entity = entities[0];
            if(monitorNotificationData.entity_match_status != null)
            {
                entity.Match_Status__c = monitorNotificationData.entity_match_status;
            }
            if(monitorNotificationData.is_whitelisted != null)
            {
                entity.Is_Whitelisted__c = monitorNotificationData.is_whitelisted;
            }
            update entity;
        }
    }


    private static Boolean isValidRequest(string complyAdvantageKey)
    {       
         Boolean isValidRequest = false;
         /*
         if(string.isNotBlank(complyAdvantageKey) && string.isNotEmpty(complyAdvantageKey))
         {
            for(Comply_Advantage_Instance_Setting__c setting : [SELECT Name, API_Key__c FROM Comply_Advantage_Instance_Setting__c])
            {                 
                 if(setting.API_Key__c == complyAdvantageKey)
                 {
                     isValidRequest = true;
                     break;
                 }
            }
        }
        if(Test.isRunningTest())
        {
           isValidRequest = true;
        }
        */
        return isValidRequest;
    }
}