public with sharing class FxAccountApprovalController {
    private final fxAccount__c fxa;
    
    public FxAccountApprovalController (ApexPages.StandardController stdController) {
        this.fxa = (fxAccount__c)stdController.getRecord();
    }
    
    public PageReference save() {
        // Id leadId = [SELECT Id FROM Lead WHERE Id=:c.Lead__c][0].Id;
        // Id leadId = [SELECT Lead__c FROM Case WHERE Id=:c.Id][0].Lead__c;
        String username = [SELECT Name FROM fxAccount__c WHERE Id=:fxa.Id][0].Name;
        
        AmazonSwfSender.startWorkflow(username);
   
        fxa.Funnel_Stage__c = 'Ready For Funding';
        update fxa;
        return null;
    }
}