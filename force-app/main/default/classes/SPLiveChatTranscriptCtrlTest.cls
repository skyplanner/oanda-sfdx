/**
 * @File Name          : SPLiveChatTranscriptCtrlTest.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 4:42:57 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/15/2019, 2:14:11 PM   dmorales     Initial Version
**/
@isTest
private with sharing class SPLiveChatTranscriptCtrlTest {
 
    @isTest static void test_getAccountFromChatTranscript() {
	     
			TestDataFactory factory = new TestDataFactory();
     		Account acct = factory.createTestAccount();					
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now(), AccountId=acct.Id);
			insert t;
			
			Account a = SPLiveChatTranscriptCtrl.getAccountFromChatTranscript(t.Id);
			
			System.assertEquals(a.Id, acct.Id);		

	}

	@isTest static void test_getAccount(){
       
			TestDataFactory factory = new TestDataFactory();
     		Account acct = factory.createTestAccount();		
			Account a = SPLiveChatTranscriptCtrl.getAccount(acct.Id);		
			System.assertEquals(a.Id, acct.Id);
        

	}

	@isTest static void testgetCaseFromChatTranscript() {
		
			Lead l = new Lead(LastName='test', Email='test@oanda.com', RecordTypeId=RecordTypeUtil.getLeadRetailPracticeId());
			insert l;
			
			Case c = new Case(Subject='test subject', Lead__c=l.Id, Origin='Chat');
			insert c;
			
			LiveChatVisitor v = new LiveChatVisitor();
			insert v;
			
			Post_Chat_Survey__c s = new Post_Chat_Survey__c(Chat_Key__c='testkey');
			insert s;
			
			LiveChatTranscript t = new LiveChatTranscript(LiveChatVisitorId=v.Id, StartTime=DateTime.now(), ChatKey='testkey');
			t.CaseId = c.Id;
			insert t;
			
			Case cr = SPLiveChatTranscriptCtrl.getCaseFromChatTranscript(t.Id);

			System.assert( cr != null);
		
	}
	@isTest
	private static void testGetAccountDynamic() {
		// Test data setup
	
		// Actual test
		Test.startTest();
		Account acc =  SPLiveChatTranscriptCtrl.getAccountDynamic(null,'ChatTranscript');
		System.assert( acc == null);

		acc =  SPLiveChatTranscriptCtrl.getAccountDynamic(null,'MessagingSession');
		System.assert( acc == null);

		acc =  SPLiveChatTranscriptCtrl.getAccountDynamic(null,'Account');
		System.assert( acc == null);
	
		Test.stopTest();
	
		// Asserts
	}
}