/**
 * @File Name          : RouteCaseCmdTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/6/2024, 3:26:55 AM
**/
@IsTest
private without sharing class RouteCaseCmdTest {

	@TestSetup
	static void setup() {
		SPDataInitManager initManager = new SPDataInitManager();
		// setup code
		ServiceTestDataFactory.createAccount1(initManager);
		ServiceTestDataFactory.createCase1(initManager);
		initManager.storeData();
	}

	@IsTest
	static void invokeWithEvent() {
		SPDataInitManager initManager = SPDataInitManager.reload();
		ID case1Id = initManager.getObjectId(ServiceTestDataKeys.CASE_1, true);

		Test.startTest();
		RouteCaseCmd.invokeWithEvent(case1Id);
		Test.stopTest();

		Integer psrCount = [SELECT count() FROM PendingServiceRouting];
		Boolean psrCreated = (psrCount > 0);
		
		Assert.isTrue(
			psrCreated, 
			'A PendingServiceRouting record should have been created'
		);
	}
	
}