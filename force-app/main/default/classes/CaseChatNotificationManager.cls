/**
 * @File Name          : CaseChatNotificationManager.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/5/2024, 1:15:52 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/23/2024, 10:29:33 AM   aniubo     Initial Version
 **/
public without sharing class CaseChatNotificationManager extends SLAViolationBaseNotificationManager {
	public CaseChatNotificationManager(
		SLAConst.AgentSupervisorModel supervisorModel,
		SLAConst.SLAViolationType violationType,
		Map<String, Milestone_Time_Settings__mdt> milestonesByType,
		SupervisorProviderCreator supervisorProviderCreator
	) {
		super(
			supervisorModel,
			violationType,
			milestonesByType,
			supervisorProviderCreator
		);
	}
	protected override Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> getNotificationParamsMap(
		Map<String, Milestone_Time_Settings__mdt> milestonesByType
	) {
		Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>> notifications;
		notifications = new Map<SLAConst.SLAViolationType, Map<String, Map<string, NotificationParams>>>();
		Map<SLAConst.SLAViolationType, Map<String, Milestone_Time_Settings__mdt>> byViolations;
		byViolations = getByViolations(milestonesByType);
		for (SLAConst.SLAViolationType key : byViolations.keySet()) {
			Map<String, Map<string, NotificationParams>> byTiers = getNotificationParamFromSettings(
				byViolations.get(key)
			);
			notifications.put(key, byTiers);
		}
		// Map<String, Map<string, NotificationParams>> ahtByTiers = getNotificationParamFromSettings(
		// 	milestonesByType
		// );
		// notifications.put(SLAConst.SLAViolationType.AHT, ahtByTiers);

		this.notificationTierByViolationType = new Map<SLAConst.SLAViolationType, Boolean>{
			SLAConst.SLAViolationType.AHT => true,
			SLAConst.SLAViolationType.ASA => true,
			SLAConst.SLAViolationType.BOUNCED => true
		};
		return notifications;
	}
	protected override void setNotificationTargetId(
		Messaging.CustomNotification notification,
		SLAViolationInfo info
	) {
		notification.setTargetId(info.caseId);
	}

	protected override NotificationParams getNotificationParams(
		SLAViolationInfo info
	) {
		NotificationParams notParams = null;
		String channel = getChannelNameByObjectNotification(
			info.notificationObjectType
		);
		SLAConst.SLAViolationType currentViolationType = getViolationType(info.violationType);
		Map<string, NotificationParams> notificationParamsByTier = notificationParamsByViolationTypeMap.get(
			currentViolationType
			)
			.get(channel);

		Boolean isByTier = notificationTierByViolationType.get(currentViolationType);
		if (isByTier) {
			String tier = String.isBlank(info.tier)
				? OmnichannelCustomerConst.TIER_4
				: info.tier;
			notParams = notificationParamsByTier.get(tier);
		} else {
			notParams = notificationParamsByTier.get(currentViolationType.name());
		}
		return notParams;
	}

	private Map<SLAConst.SLAViolationType, Map<String, Milestone_Time_Settings__mdt>> getByViolations(
		Map<String, Milestone_Time_Settings__mdt> milestonesByType
	) {
		Map<SLAConst.SLAViolationType, Map<String, Milestone_Time_Settings__mdt>> byViolations;
		byViolations = new Map<SLAConst.SLAViolationType, Map<String, Milestone_Time_Settings__mdt>>();
		for (String key : milestonesByType.keySet()) {
			Milestone_Time_Settings__mdt setting = milestonesByType.get(key);
			SLAConst.SLAViolationType currentViolationType = getViolationType(
				setting.Violation_Type__c
			);
			Map<String, Milestone_Time_Settings__mdt> byName = byViolations.get(
				currentViolationType
			);
			if (byName == null) {
				byName = new Map<String, Milestone_Time_Settings__mdt>();
				byViolations.put(currentViolationType, byName);
			}
			byName.put(key, setting);
		}
		return byViolations;
	}

	private SLAConst.SLAViolationType getViolationType(String violationType) {
		if (violationType == SLAConst.AHT_EXCEEDED_VT) {
			return SLAConst.SLAViolationType.AHT;
		} else if (violationType == SLAConst.ASA_EXCEEDED_VT) {
			return SLAConst.SLAViolationType.ASA;
		}
		return SLAConst.SLAViolationType.BOUNCED;
	}
}