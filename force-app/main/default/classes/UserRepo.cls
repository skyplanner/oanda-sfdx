/**
 * @File Name          : UserRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/9/2024, 5:03:09 PM
**/
public inherited sharing class UserRepo {

	public static User getSummaryByName(String name) {
		List<User> recList = [
			SELECT Id
			FROM User
			WHERE Name = :name
			LIMIT 1
		];
		User result = null;
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

	public static List<User> getSummaryForStandardUsers(Set<ID> userIds) {
		return getSummaryByIdAndType(
			userIds, // userIds
			'Standard' // userType
		);
	}
	
	public static List<User> getSummaryByIdAndType(
		Set<ID> userIds,
		String userType
	) {
		return [
			SELECT 
				Username,
				UserType
			FROM User
			WHERE Id IN :userIds
			AND UserType = :userType
		];
	}
	
}