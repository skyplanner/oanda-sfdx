/**
 * @File Name          : VerificationCodeUtility.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/13/2019, 11:53:51 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/10/2019   dmorales     Initial Version
**/
public without sharing class VerificationCodeUtility {

    public static List<Verification_Code_Setting__c> getVerificationCodesAvailable(Integer count) {
        /**
        * 1- Find in Verification Code Custom Settings as codes as count in parameter
        * 2- Delete Verification Codes from Custom Settings to avoid repeated
        * 3- Check if Verification Codes list count is less than Count (in custom metadata) and re-populate from Big Object
        **/
        List<Verification_Code_Setting__c> verifCodeAll = Verification_Code_Setting__c.getAll().values();
        List<Verification_Code_Setting__c> verifCodeToAssign = new List<Verification_Code_Setting__c>();
        /** If there are not enough Verification Codes available in Custom Setting, then return empty and throw error in Case Trigger */
        if(verifCodeAll.size() < count){
           return verifCodeToAssign;
        }
        
        for(Integer i = 0; i < count; i ++){
           verifCodeToAssign.add(verifCodeAll[i]);
        }

        Database.delete(verifCodeToAssign);

        return verifCodeToAssign;
    }
     
    @future
    public static void deleteVerificationCodeFromBigObject(List<Decimal> uniqueIndex){
        List<Verification_Code__b> oppCodes = new List<Verification_Code__b>();
        oppCodes.addAll([SELECT Order__c, Code__c FROM Verification_Code__b where Order__c IN :uniqueIndex]);
        System.debug('Codes ' + oppCodes);
        Database.DeleteImmediate(oppCodes);
    }  

}