/**
 * @File Name          : OnNonHvcCaseReleasedCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/25/2024, 5:59:46 PM
**/
public inherited sharing class OnNonHvcCaseReleasedCmd {

	@TestVisible
	final Set<ID> releasedCaseIdSet;

	@TestVisible
	final Set<ID> reroutedCaseIdSet;

	@TestVisible
	final List<Non_HVC_Case__c> recordsToUpdate;

	@TestVisible
	final List<Non_HVC_Case__c> recordsToDelete;


	public OnNonHvcCaseReleasedCmd() {
		releasedCaseIdSet = new Set<ID>();
		reroutedCaseIdSet = new Set<ID>();
		recordsToUpdate = new List<Non_HVC_Case__c>();
		recordsToDelete = new List<Non_HVC_Case__c>();
	}

	public void checkRecord(
		Case rec, 
		Case oldRec
	) {
		if (
			(rec.Routed__c == true) &&
			(rec.Agent_Capacity_Status__c != oldRec.Agent_Capacity_Status__c) &&
			(
				(rec.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_RELEASED) ||
				(rec.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE)
			)
		) {
			releasedCaseIdSet.add(rec.Id);
			if (rec.Agent_Capacity_Status__c == OmnichanelConst.CASE_CAPACITY_STATUS_REROUTE) {
				reroutedCaseIdSet.add(rec.Id);
			}
		} 
	}

	public Boolean execute() {
		if (releasedCaseIdSet.isEmpty()) {
			return false;
		}
		// else...
		List<Non_HVC_Case__c> nonHvcCaseList = 
			NonHVCCaseHelper.getByCase(releasedCaseIdSet);
		prepareToReleaseNonHvcCases(nonHvcCaseList);
		update recordsToUpdate;
		delete recordsToDelete;

		if (reroutedCaseIdSet.isEmpty() == false) {
			// re route cases
			CaseReroutingManager.getInstance().fireRerouteEventForCases(
				reroutedCaseIdSet
			);
		}
		return true;
	}

	@TestVisible
	void prepareToReleaseNonHvcCases(List<Non_HVC_Case__c> nonHvcCaseList) {
		for (Non_HVC_Case__c nonHvcCase : nonHvcCaseList) {
			if (
				nonHvcCase.Status__c == 
				OmnichanelConst.NON_HVC_CASE_STATUS_DEFAULT
			) {
				nonHvcCase.Status__c = 
					OmnichanelConst.NON_HVC_CASE_STATUS_RELEASED;
				recordsToUpdate.add(nonHvcCase);

			} else {
				recordsToDelete.add(nonHvcCase);
			}
		}
	}
}