/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 02-09-2023
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class TasDivisionTradingGroupId {
    public Integer divisionID { get; set; }

    public Integer tradingGroupID { get; set; }
}