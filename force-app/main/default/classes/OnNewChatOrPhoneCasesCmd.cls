/**
 * @File Name          : OnNewChatOrPhoneCasesCmd.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 6/5/2024, 11:52:55 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/17/2021, 10:56:04 PM   acantero     Initial Version
**/
public inherited sharing class OnNewChatOrPhoneCasesCmd {

    List<Case> caseList = new List<Case>();

    public void checkRecord(
        Case rec
    ) {
        if (
            (rec.Origin == OmnichanelConst.CASE_ORIGIN_CHAT) ||
            (rec.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE) ||
            (rec.Origin == OmnichanelConst.CASE_ORIGIN_PHONE) 
        ) {
            caseList.add(rec);
            //...
            if (
                (rec.Origin == OmnichanelConst.CASE_ORIGIN_CHAT) &&
                (rec.Chat_Spot_Crypto__c == OmnichanelConst.YES)
            ) {
                // Spot Crypto trick
                // chat api can't fill a checkbox field
                // then fill a picklist and this command fill the checkbox
                rec.Spot_Crypto__c = true;
            }
        } 
    }

    public void execute() {
        if (caseList.isEmpty()) {
            return;
        }
        //else...
        processCases();
    }

    @testVisible
    void processCases() {
        List<Case> hvcCaseList = caseList;
        for(Case caseObj : hvcCaseList) {
            setEntitlement(caseObj);
        }
    }

    void setEntitlement(Case caseObj) {
        if (
            (caseObj.Origin == OmnichanelConst.CASE_ORIGIN_CHAT) ||
            (caseObj.Origin == OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE)
        ) {
            caseObj.EntitlementId = 
                EntitlementSettings.getChatEntitlementId();
            //...
        } else if (caseObj.Origin == OmnichanelConst.CASE_ORIGIN_PHONE) {
            caseObj.EntitlementId = 
                EntitlementSettings.getPhoneEntitlementId();
        }
    }

}