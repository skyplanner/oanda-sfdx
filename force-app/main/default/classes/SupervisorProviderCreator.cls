/**
 * @File Name          : SupervisorProviderCreator.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/29/2024, 3:44:58 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/29/2024, 3:44:28 PM   aniubo     Initial Version
**/
public interface SupervisorProviderCreator {
    SupervisorProvider createSupervisorProvider(SLAConst.AgentSupervisorModel supervisorModel);
}