/**
 * @File Name          : NonHvcCaseRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2024, 12:34:39 PM
**/
public inherited sharing class NonHvcCaseRepo {
		
	public static List<Non_HVC_Case__c> getModifiedBeforeDate(
		String status,
		Datetime lastModifiedDate,
		Integer queryLimit
	) {
		return [
			SELECT Id
			FROM Non_HVC_Case__c
			WHERE Status__c = :status
			AND LastModifiedDate <= :lastModifiedDate
			ORDER BY LastModifiedDate ASC
			LIMIT :queryLimit
		];
	}

}