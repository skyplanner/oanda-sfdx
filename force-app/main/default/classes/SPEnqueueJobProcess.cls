/**
 * @File Name          : SPEnqueueJobProcess.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/5/2023, 1:45:54 AM
**/
public inherited sharing class SPEnqueueJobProcess {

	final Queueable job;

	public System.AsyncOptions options {get; set;}

	public SPEnqueueJobProcess(Queueable job) {
		this.job = job;
	}

	public Boolean enqueueIfNoTesting() {
		return enqueueIfCondition((!Test.isRunningTest()));
	}

	public Boolean enqueueIfCondition(Boolean condition) {
		Boolean result = false;
		if (condition == true) {
			enqueueJob();
			result = true;
		}
		return result;
	}

	public Boolean safeEnqueueJob(String jobStringKey) {
		Boolean result = true;

		if (options == null) {
			options = new System.AsyncOptions();
		}
		
		// .addId([SELECT Id FROM ApexClass WHERE Name='MyQueueable'].Id)
		options.DuplicateSignature = 
			QueueableDuplicateSignature.Builder()
			.addString(jobStringKey) 
			.build();

		try {
			enqueueJob();

		} catch (DuplicateMessageException ex) {
			//Exception is thrown if there is already an enqueued job with the same 
			//signature
			result = false;
		}
		return result;
	}

	@TestVisible
	void enqueueJob() {
		if (options != null) {
			System.enqueueJob(job, options);

		} else {
			System.enqueueJob(job);
		}
	}
	
}