/**
 * @File Name          : UpdateMessagingSessionFieldActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:14:14 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 12:03:45 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class UpdateMessagingSessionFieldActionTest {
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		List<String> actionResults;
		// Actual test
		Test.startTest();
		actionResults = UpdateMessagingSessionFieldAction.setMessagingSessionField(
			null
		);
		Assert.areEqual(
			null,
			actionResults.get(0),
			'Action Result should be null'
		);
		actionResults = null;
		actionResults = UpdateMessagingSessionFieldAction.setMessagingSessionField(
			new List<UpdateMessagingSessionFieldAction.UpdateInfo>()
		);

		Assert.areEqual(
			null,
			actionResults.get(0),
			'Action Result should be null'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testSetMessagingSessionField() {
		// Test data setup
		UpdateMessagingSessionFieldAction.UpdateInfo updateInfo = new UpdateMessagingSessionFieldAction.UpdateInfo();
		updateInfo.messagingSessionId = null;
		updateInfo.fieldName = 'ChannelType';
		updateInfo.fieldValue = 'EmbeddedMessaging';
		Boolean exceptionThrown = false;

		Test.startTest();
		try {
			UpdateMessagingSessionFieldAction.setMessagingSessionField(
				new List<UpdateMessagingSessionFieldAction.UpdateInfo>{
					updateInfo
				}
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Map<String, Object> result = updateInfo.getFieldValueByFieldNames();

		Assert.areEqual(
			updateInfo.fieldValue,
			result.get(updateInfo.fieldName),
			'Field value should be equal' + updateInfo.fieldValue
		);

		Test.stopTest();

		// Asserts
		Assert.areEqual(
			true,
			exceptionThrown,
			'Exception should be thrown because messagingSessionId is not exist'
		);
	}

	@isTest
	private static void testSetMessagingSessionField1() {
		// Test data setup
		UpdateMessagingSessionFieldAction.UpdateInfo updateInfo = new UpdateMessagingSessionFieldAction.UpdateInfo();
		Boolean exceptionThrown = false;
		List<String> actionResults;
		Test.startTest();
		try {
			actionResults = UpdateMessagingSessionFieldAction.setMessagingSessionField(
				new List<UpdateMessagingSessionFieldAction.UpdateInfo>{
					updateInfo
				}
			);
		} catch (Exception ex) {
			exceptionThrown = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(
			true,
			exceptionThrown,
			'Exception should be thrown because messagingSessionId is not exist'
		);
	}
}