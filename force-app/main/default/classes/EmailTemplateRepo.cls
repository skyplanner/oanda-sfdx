/**
 * @File Name          : EmailTemplateRepo.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/5/2023, 4:30:10 AM
**/
public inherited sharing class EmailTemplateRepo {

	public static EmailTemplate getByDevName(String devName) {
		List<EmailTemplate> recList = [
			SELECT Id
			FROM EmailTemplate
			WHERE DeveloperName = :devName
		];
		EmailTemplate result = null;
		
		if (recList.isEmpty() == false) {
			result = recList[0];
		}
		return result;
	}

}