/**
 * @description       : Test for SegmentationTrigger, SegmentationTriggerHandler and SegmentationManager
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 08-16-2022
 * @last modified by  : Dianelys Velazquez
**/
@isTest
private class SegmentationManagerTest {
    @testSetup
	static void init() {
		Account acc = TestDataFactory.getPersonAccount(true);
        TestDataFactory factory = new TestDataFactory();
		fxAccount__c fxAcc = factory.createFXTradeAccount(acc);
	}

    @isTest
    static void testInsert() {
        fxAccount__c fx = [SELECT Id FROM fxAccount__c LIMIT 1];
        Segmentation__c s = new Segmentation__c(
            Seg_Date__c = Date.today(),
            Seg_PL__c = 'low',
            fxAccount__c = fx.Id
        );

        Test.startTest();

        insert s;
        
        Test.stopTest();

        List<Segmentation__c> segs = [SELECT Active__c FROM Segmentation__c];

        System.assertEquals(false, segs.isEmpty(),
            'A Segmentation should be created.');
        System.assertEquals(true, segs[0].Active__c,
            'The Segmentation created should be Activated.');

        fx = [SELECT Segmentation__c FROM fxAccount__c WHERE Id = :fx.Id];

        System.assertEquals(segs[0].Id, fx.Segmentation__c,
            'The Segmentation should be associated with the fxAccount.');
    }

    @isTest
    static void testInsertAgain() {
        fxAccount__c fx = [SELECT Id FROM fxAccount__c LIMIT 1];
        Segmentation__c s = new Segmentation__c(
            Seg_Date__c = Date.today() - 1,
            Seg_PL__c = 'low',
            fxAccount__c = fx.Id
        );
        insert s;
        Test.setCreatedDate(s.Id, System.now().addDays(-1));

        Segmentation__c s2 = new Segmentation__c(
            Seg_Date__c = Date.today(),
            Seg_PL__c = 'mid',
            fxAccount__c = fx.Id
        );

        Test.startTest();

        insert s2;
        
        Test.stopTest();

        List<Segmentation__c> segs = [SELECT Active__c 
            FROM Segmentation__c
            ORDER BY Seg_Date__c DESC];

        System.assertEquals(2, segs.size(),
            'Two Segmentation should be created.');
        System.assertEquals(true, segs[0].Active__c,
            'The last Segmentation created should be Activated.');
        System.assertEquals(false, segs[1].Active__c,
            'The first Segmentation created should be Activated.');

        fx = [SELECT Segmentation__c FROM fxAccount__c WHERE Id = :fx.Id];

        System.assertEquals(segs[0].Id, fx.Segmentation__c,
            'The last Segmentation should be associated with the fxAccount.');
    }

    @isTest
    static void removeActiveSegmentationShouldActivateMostRecentTest() {
        fxAccount__c fx = [SELECT Id FROM fxAccount__c LIMIT 1];

        Segmentation__c s1 = new Segmentation__c(
            Seg_Date__c = Date.today().addDays(-1),
            Seg_PL__c = 'low',
            fxAccount__c = fx.Id
        );
        insert s1;
        Test.setCreatedDate(s1.Id, System.now().addDays(-1));
        Segmentation__c s2 = new Segmentation__c(
            Seg_Date__c = Date.today(),
            Seg_PL__c = 'mid',
            fxAccount__c = fx.Id
        );
        insert s2;
        //just revertin the order will help, I guess that hyperforce change. To make sure date are different setcreateDate is used. 
         //insert new List<Segmentation__c> {s2, s1}; original-> {s1, s2}
         // but lets fix the root cause
        // milisecond part after save dissapear so test might not be valid due to code:
        /*
                private void inactivateOldSegmentations(Set<Id> fxAccountIds) {
                List<Segmentation__c> segmentationList = new List<Segmentation__c>();

                for (fxAccount__c fx : [SELECT (SELECT Id 
                        FROM Segmentations__r 
                        WHERE Active__c = true 
                        ORDER BY CreatedDate DESC)
                    FROM fxAccount__c 
                    WHERE Id IN: fxAccountIds]
                ) {
                    if (fx.Segmentations__r?.size() > 1) {
                        for (Integer i = 1; i < fx.Segmentations__r.size(); i++) {
                            fx.Segmentations__r[i].Active__c = false;
                            segmentationList.add(fx.Segmentations__r[i]);
                        }
                    }
                }....
        */
        //insert new List<Segmentation__c> {s2, s1};

        s1 = [SELECT Active__c FROM Segmentation__c WHERE Id =: s1.Id];
        s2 = [SELECT Active__c FROM Segmentation__c WHERE Id =: s2.Id];

        System.assert(!s1.Active__c, 'S1 should not be active segmentation.');
        System.assert(s2.ACtive__c, 'S2 should be active segmentation.');

        Test.startTest();

        delete s2;
        
        Test.stopTest();

        s1 = [SELECT Active__c, fxAccount__r.Segmentation__c
            FROM Segmentation__c 
            WHERE Id =: s1.Id];

        System.assert(s1.ACtive__c, 'S1 should be active segmentation.');
        System.assertEquals(s1.Id, s1.fxAccount__r.Segmentation__c,
            'The last Segmentation should be associated with the fxAccount.');
    }
}