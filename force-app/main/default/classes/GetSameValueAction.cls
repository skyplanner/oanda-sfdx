/**
 * @File Name          : GetSameValueAction.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2023, 1:28:51 PM
**/
public without sharing class GetSameValueAction { 

	@InvocableMethod(label='Get same value' callout=false)
	public static List<String> getSameValue(List<String> valueList) {    
		return valueList;
	} 

}