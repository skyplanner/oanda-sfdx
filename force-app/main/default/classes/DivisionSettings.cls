/**
 * @File Name          : DivisionSettings.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/3/2020, 3:52:35 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   dmorales     Initial Version
**/
global class DivisionSettings {

    global static final String DEFAULT_SETTING_NAME = 'OC';

    global String divisionDataCategory {get; private set;}
    global String divisionAcronym {get; private set;}
    global String divisionLabel {get; private set;}
    // global List<Division_Settings__mdt> divisionList {get; private set;}
        
    @TestVisible
    global Boolean initializationFails {get; private set;}

    private static DivisionSettings instance;

    global static DivisionSettings getInstance(String settingsName) {
        if (instance == null) {
            instance = new DivisionSettings(settingsName);
        }
        return instance;
    }
    
    global DivisionSettings(String settingsName) {
        init(settingsName);
    }

    private void init(String settingsName) {
        Division_Settings__mdt record = querySettings(settingsName);

    	if (record == null) {
    		InitializationFails = true;
    		return;
    	}

    	//else...
    	initializationFails = false;
        divisionDataCategory = record.Division_Data_Category__c ;     
        divisionAcronym = record.Division_Acronyms__c  ;   
        divisionLabel = record.Division_Label__c  ;
        
        // divisionList = getDivisionHelpPortal();  
    }

    private Division_Settings__mdt querySettings(String settingsName) {
        String settingsNameQuery = String.isEmpty(settingsName) ?
			DEFAULT_SETTING_NAME : settingsName;
        
        for (Division_Settings__mdt ds : [
                    SELECT 
                        DeveloperName,
                        QualifiedApiName,
                        Division_Data_Category__c,
                        Division_Acronyms__c,
                        Division_Label__c,
                        Visible__c
                    FROM Division_Settings__mdt
                ])
			if (ds.QualifiedApiName == settingsNameQuery ||
					ds.Division_Data_Category__c == settingsNameQuery ||
					ds.Division_Label__c == settingsNameQuery)
				return ds;

		return null;
    }
   
}