/* Name: CustomNotificationManager
 * Description : Send custom push notifications to users
 * Author: ejung
 * Date : 2023-03-15
 */
public without sharing class CustomNotificationManager {

    public static boolean firstRun = true;

    public static final String NOTIFICATION_TYPE_SALES = 'Sales_Notification';
    public static final String PRICING_GROUP_CHANGE_REQUEST = 'PRICING GROUP CHANGE REQUEST';
    public static final DateTime THREE_DAYS_AGO = Datetime.now().addDays(-3);

    public static set<string> caseFields = new set<string>
    {
        'Id',
        'CreatedDate',
        'Account.Name',
        'Account.OwnerId',
        'Account.fxAccount__r.Opportunity__r.OwnerId',
        'Account.Primary_Division_Name__c',
        'Account.fxAccount__r.Funnel_Stage__c',
        'Account.fxAccount__r.Ready_for_Funding_DateTime__c',
        'Owner.Profile.Name',
        'RecordTypeId',
        'CaseNumber',
        'Subject',
        'Inquiry_Nature__c',
        'Type__c',
        'Origin'
    };

    public static void sendNotificationsForOGMCases(
        List<SObject> sObjectList, Map<Id, SObject> oldMap) 
    {
        String objName;
		List<Id> caseIdList = new List<Id>();
        Map<Id, Case> caseOldMap = new Map<Id, Case>();
		for(SObject obj : sObjectList) 
        {
            objName = obj.Id.getSobjectType().getDescribe().getName();
            System.debug('Object Name ::: '+ objName);

            switch on objName {
                when 'Document__c' {
                    Document__c doc = (Document__c) obj;
                    Map<Id, Document__c> docOldMap = (Map<Id, Document__c>) oldMap;
                    Document__c oldDoc = docOldMap?.get(doc.Id);
                    if(doc.Account__c != null && 
                        SObjectUtil.isRecordFieldsChanged(doc, oldDoc, 'Case__c') && 
                        doc.Account_Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
                    {
                        caseIdList.add(doc.Case__c);
                    }
                }
                when 'LiveChatTranscript' {
                    LiveChatTranscript lct = (LiveChatTranscript) obj;
                    Map<Id, LiveChatTranscript> lctOldMap = (Map<Id, LiveChatTranscript>) oldMap;
                    LiveChatTranscript oldLct = lctOldMap?.get(lct.Id);
                    if(SObjectUtil.isRecordFieldsChanged(lct, oldLct, 'CaseId') &&
                        (lct.Type__c == 'Deposit' || lct.Inquiry_Nature__c == 'Trade') &&
                        lct.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
                    {
                        caseIdList.add(lct.CaseId);
                    }
                }
                when 'CaseComment' {
                    CaseComment comment = (CaseComment) obj;
                    caseIdList.add(comment.ParentId);
                }
                when 'EmailMessage' {
                    EmailMessage em = (EmailMessage) obj;
                    if(!em.Incoming) {
                        caseIdList.add(em.ParentId);
                    }
                }
                when 'Case' {
                    Case c = (Case) obj;
                    Map<Id, Case> cOldMap = (Map<Id, Case>) oldMap;
                    Case oldCase = cOldMap?.get(c.Id);
                    if(c.AccountId != null && 
                        c.Account_Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
                    {
                        caseIdList.add(c.Id);
                        caseOldMap.put(c.Id, oldCase);
                    }
                }
            }
        }

        System.debug('caseIdList ::: '+ caseIdList);

        if(!caseIdList.isEmpty()) 
        {
            sendNotificationsForOGMCases(getOGMCases(caseIdList), caseOldMap, objName);
        }
    }

    /**
	 * Push notifications from OGM cases
     * @param cases
     * @param oldMap
     * @param sourceObject
	 */
    public static void sendNotificationsForOGMCases(
        List<Case> cases, 
        Map<Id, Case> oldMap, 
        String sourceObject)
    {    
        String notificationTitle;
        String notificationBody;

		for(Case c : cases) 
        {
            Case oldCase = oldMap?.get(c.Id);
            String funnelStage = c.Account.fxAccount__r.Funnel_Stage__c;

            // Get the Id for the custom notification type
		    CustomNotificationType notificationType = CustomNotificationTypeHelper.getByDevName(NOTIFICATION_TYPE_SALES, false);

            // add the recipients - account owner and opportunity owner
            Set<String> recipientsIds = new Set<String>();
            recipientsIds.add(c.Account.OwnerId);
            recipientsIds.add(c.Account.fxAccount__r.Opportunity__r.OwnerId);

            // Onboarding case
            if(c.RecordTypeId == RecordTypeUtil.onboardingCaseRecordTypeId) {
                /**
                 * Funnel stage  = RFF/Funded
                 * document uploaded by OB
                 */
                if((funnelStage == Constants.FUNNEL_RFF || funnelStage == Constants.FUNNEL_FUNDED) &&
                    sourceObject == 'Document__c') {
                    notificationTitle = Label.Document_Uploaded_Subject;
                    notificationBody = Label.Document_Uploaded_Body
                                    .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                    .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
                }
                /**
                 * Funnel stage  = Traded
                 * Case RecordType = Onboarding
                 * Case comment or email or any update made by OB or AML
                 */
                else if(funnelStage == Constants.FUNNEL_TRADED &&
                        isCurrentUserOB() &&
                        (sourceObject == 'Case' || sourceObject == 'CaseComment' || sourceObject == 'EmailMessage')) 
                {        
                    // set notification title and body
                    notificationTitle = Label.Application_Review_Updated_Subject;
                    notificationBody = Label.Application_Review_Updated_Body
                                .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
                }
            }

            // Pricing Group Change Request case
            else if(c.subject != null && c.subject.toUpperCase().indexOf(PRICING_GROUP_CHANGE_REQUEST) != -1 &&
                // RFF/Funded/Traded
                (funnelStage == Constants.FUNNEL_RFF || funnelStage == Constants.FUNNEL_FUNDED || FunnelStage == Constants.FUNNEL_TRADED) &&
                // Case comment or email or any update made by OB or AML
                isCurrentUserOB() &&
                (sourceObject == 'Case' || sourceObject == 'CaseComment' || sourceObject == 'EmailMessage')) 
            {
                    notificationTitle = Label.Pricing_Change_Request_Updated_Subject;
                    notificationBody = Label.Pricing_Change_Request_Updated_Body
                                .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
            }

            // Email or Chat case
            else if((c.Origin.startsWithIgnoreCase('Email') || c.Origin == Constants.CASE_ORIGIN_CHAT) && 
                // RFF/Funded
                (funnelStage == Constants.FUNNEL_RFF || funnelStage == Constants.FUNNEL_FUNDED)) 
            {    
                // Type = Deposit
                if(c.Type__c == Constants.CASE_TYPE_DEPOSIT && 
                    (sourceObject == 'LiveChatTranscript' ||
                    (SObjectUtil.isRecordFieldsChanged(c, oldCase, 'Type__c') && sourceObject == 'Case')))
                {
                    notificationTitle = Label.New_Deposit_Case_Subject;
                    notificationBody = Label.New_Deposit_Case_Body
                                .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
                }
                // Inquiry Nature = Trade
                else if(c.Inquiry_Nature__c == Constants.CASE_INQUIRY_NATURE_TRADE && 
                    (sourceObject == 'LiveChatTranscript' || 
                    (SObjectUtil.isRecordFieldsChanged(c, oldCase, 'Inquiry_Nature__c') && sourceObject == 'Case')))
                {
                    notificationTitle = Label.New_Trade_Inquiry_Subject;
                    notificationBody = Label.New_Trade_Inquiry_Body
                                .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
                }
                // Inquiry Nature = Login
                else if(c.Inquiry_Nature__c == Constants.CASE_INQUIRY_NATURE_LOGIN && 
                    (c.Account.fxAccount__r.Ready_for_Funding_DateTime__c > THREE_DAYS_AGO) &&
                    (sourceObject == 'LiveChatTranscript' || 
                    (SObjectUtil.isRecordFieldsChanged(c, oldCase, 'Inquiry_Nature__c') && sourceObject == 'Case')))
                {
                    notificationTitle = Label.New_Login_Inquiry_Subject;
                    notificationBody = Label.New_Login_Inquiry_Body
                                .replace(Label.ClientNamePlaceholder, c.Account.Name)
                                .replace(Label.CaseNumberPlaceholder, c.CaseNumber);
                }
            }

            if(notificationTitle != null) 
            {
                sendNotification(recipientsIds, 
                                    c.Id, 
                                    notificationType.Id, 
                                    notificationTitle, 
                                    notificationBody);
            }
		}
    }

    /**
	 * Send notifications
     * @param recipientsIds
     * @param targetId
     * @param notificationTypeId
     * @param title
     * @param body
	 */
	public static void sendNotification(
        Set<String> recipientsIds, 
        String targetId, 
        Id notificationTypeId, 
        String title, 
        String body) 
    {
		Messaging.CustomNotification notification = new Messaging.CustomNotification();

		// Set the contents for the notification
		notification.setTitle(title);
		notification.setBody(body);

		// Set the notification type and target
		notification.setNotificationTypeId(notificationTypeId);
		notification.setTargetId(targetId);

		// Send the notification
		try {
            notification.send(recipientsIds);
        }
        catch (Exception e) {
            System.debug('Problem sending notification: ' + e.getMessage());
        }
	}

    static List<Case> getOGMCases(
        List<Id> caseIdList) 
    {
        String query = 'SELECT ' + soqlUtil.getSoqlFieldList(caseFields);
        query +=' FROM Case';
        query +=' WHERE AccountId != null';
        query +=' AND Account_Division_Name__c =\'' + Constants.DIVISIONS_OANDA_GLOBAL_MARKETS + '\'';
        query +=' AND Id IN:caseIdList';
        
        System.debug('Query ::: ' + query);

        return (List<Case>)Database.Query(query);
    }

    static Boolean isCurrentUserOB()
    {
        Boolean result = false;
        return (userinfo.getProfileId() == UserUtil.getOBProfileId() || 
            userinfo.getProfileId() == UserUtil.getOBAllSecProfileId() ||
            userinfo.getProfileId() == UserUtil.getAMLProfileId());
    }
}