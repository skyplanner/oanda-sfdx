/* Name: CustomNotificationManager
 * Description : tests for CustomNotificationManager class
 * Author: ejung
 * Date : 2023-03-20
 */
@isTest
public with sharing class CustomNotificationManager_Test {

    @testSetup
    static void setup() {
        TestDataFactory testHandler = new TestDataFactory();
		Account acc;
		fxAccount__c fxa;
		Case sampleOBCase, sampleSupportCase;
		
		acc = testHandler.createTestAccount();
		acc.RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId();
		update acc;

		fxa = new fxAccount__c();
        fxa.Account__c = acc.Id;
        fxa.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        fxa.Division_Name__c = Constants.DIVISIONS_OANDA_GLOBAL_MARKETS;
        fxa.Funnel_Stage__c = Constants.FUNNEL_RFF;
        insert fxa;

		sampleOBCase = new Case(
			recordTypeId = RecordTypeUtil.getOnBoardingCaseTypeId(),
			fxAccount__c = fxa.Id, 
			AccountId = acc.Id
		);
		insert sampleOBCase;

        Document__c doc = new Document__c(
            Name = 'TestDoc',
            Document_Type__c = 'Passport',
            Case__c = sampleOBCase.Id,
            fxAccount__c = fxa.Id,
            Account__c = fxa.Account__c
        );
        insert doc;

        sampleSupportCase = new Case(
			recordTypeId = RecordTypeUtil.getSupportCaseTypeId(),
            Subject = CustomNotificationManager.PRICING_GROUP_CHANGE_REQUEST,
			AccountId = acc.Id
		);
		insert sampleSupportCase;

        CaseComment cc = new CaseComment(
            CommentBody = 'testComment',
            ParentId = sampleSupportCase.Id
        );
        insert cc;

        EmailMessage em = new EmailMessage(
            Incoming = false,
            Subject = 'Test',
            TextBody = 'TestBody',
            ParentId = sampleSupportCase.Id
        );
        insert em;
    }

    @isTest
    static void sendNotificationsForEmailOnOGMCaseTest() {
        
        List<EmailMessage> ems = [SELECT Id, Incoming, ParentId FROM EmailMessage];

        Boolean error = false;

        Test.startTest();
        try {
            CustomNotificationManager.sendNotificationsForOGMCases(ems, null);
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @isTest
    static void sendNotificationsForPricingGroupChangeRequestCase() {
        
        List<CaseComment> cc = [SELECT Id, CreatedDate, ParentId FROM CaseComment];

        Boolean error = false;

        Test.startTest();
        try {
            CustomNotificationManager.sendNotificationsForOGMCases(cc, null);
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @isTest
    static void sendNotificationsForOnboardingDocOnOGMCaseTest() {
        
        List<Document__c> docs = [SELECT Id, CreatedDate, Case__c, Account__c, Account_Division_Name__c FROM Document__c];

        Boolean error = false;

        Test.startTest();
        try {
            CustomNotificationManager.sendNotificationsForOGMCases(docs, null);
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }

    @isTest
    static void sendNotificationsForOGMCasesTest() {

        List<Case> cases = [SELECT Id, AccountId, Account_Division_Name__c FROM Case WHERE RecordTypeId =: RecordTypeUtil.getOnBoardingCaseTypeId()];

        Boolean error = false;

        Test.startTest();
        try {
            CustomNotificationManager.sendNotificationsForOGMCases(cases, null);
        } catch (Exception ex) {
            System.debug(
                LoggingLevel.ERROR, ex.getLineNumber() + ' -> ' + ex.getMessage()
            );
            error = true;
        }
        Test.stopTest();
        System.assertEquals(false, error);
    }
}