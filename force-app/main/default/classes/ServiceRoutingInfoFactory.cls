/**
 * @File Name          : ServiceRoutingInfoFactory.cls
 * @Description        : 
 * Responsible for creating new ServiceRoutingInfo records 
 * @Author             : acantero
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/1/2024, 3:18:57 PM
**/
public inherited sharing class ServiceRoutingInfoFactory {

	static ServiceRoutingInfoFactory instance;

	SCustomerTypeProvider customerTypeProvider;
	SCustomerTierCalculator customerTierCalculator;

	// private constructor
	ServiceRoutingInfoFactory() {
		this.customerTypeProvider = new ServiceCustomerTypeProvider();
		this.customerTierCalculator = new ServiceCustomerTierCalculator();
	}

	public static ServiceRoutingInfoFactory getInstance() {
		if (instance == null) {
			instance = new ServiceRoutingInfoFactory();
		}
		return instance;
	}

	public ServiceRoutingInfo createNewRecord(
		PendingServiceRouting psrObj, 
		ServiceRoutingInfo.Source infoSource,
		SObject workItem
	) {
		ServiceRoutingInfo result = new ServiceRoutingInfo(
			psrObj, // psrObj
			infoSource, // infoSource
			workItem // workItem
		);
		result.customerTypeProvider = customerTypeProvider;
		result.customerTierCalculator = customerTierCalculator;
		return result;
	}

}