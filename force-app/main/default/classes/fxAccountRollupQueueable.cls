public with sharing class fxAccountRollupQueueable
    implements Queueable, Database.AllowsCallouts {
    
    List<fxAccount__c> fxas;
	Map<Id, fxAccount__c> oldMap;
	boolean isInsert;

	/**
	 * Constructor
	 */
	public fxAccountRollupQueueable(
		List<fxAccount__c> fxas,
		Map<Id, fxAccount__c> oldMap,
		boolean isInsert)
	{
		this.fxas = fxas;
		this.oldMap = oldMap;
		this.isInsert = isInsert;
	}

    /**
     * Queueable execute
     */
    public void execute(QueueableContext context) {
        new fxAccountRollup().doFullRollup(
            fxas, oldMap, isInsert);
    }

    /**
	 * SP-9495
	 * Do full rollups Async
	 * - part1 (Moved here from fxAccountUtil class)
	 * - part2 (Already in this class)
	 */
	public static void doFullRollup(
		List<fxAccount__c> fxas,
		Map<Id, fxAccount__c> oldMap,
		boolean isInsert)
	{
        if(LimitsUtil.canEnqueueJob()) {
            System.enqueueJob(
                new fxAccountRollupQueueable(
                    fxas, oldMap, isInsert));
        } else {
            new fxAccountRollup().doFullRollup(
                fxas, oldMap, isInsert);
        }
    }

}