/* Name: BatchIBDetachableSchedulableTest
 * Description : Apex Test Class for the BatchIBDetachableSchedulable
 * Author: Sri 
 * Date : 2020-09-03
 */
@isTest(seeAllData=false)
public class BatchIBDetachableSchedulableTest
{

    final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);

    @testSetup static void init()
    {
        //Create test data
        List<Account> accntInsList = new List<Account>();
        for(Integer i=0; i<4;i++)
        {
            Account accnt = new Account();
            accnt.FirstName = 'TestFirst'+i;
            accnt.LastName = 'TestLast' +i;
            accnt.PersonEmail = 'test'+i+'@oanda.com';
            accnt.Account_Status__c = 'Active';
            if(i<2)
            {
                accnt.PersonMailingCountry = 'Canada'; 
                accnt.Division_Name__c = 'OANDA Canada';  
            }
            else
            {
                accnt.PersonMailingCountry = 'United Kingdom';   
                accnt.Division_Name__c = 'OANDA Europe';
            }
            accnt.Broker_Number__c = '123456'+i;
            accnt.IB_Status__c='Inactive';
            accntInsList.add(accnt);
        }

        if(!accntInsList.isEmpty())
        {
            try
            {
            insert accntInsList;
            }
            catch(Exception e)
            {
                System.debug(e.getMessage());
            }
        }
        List<Opportunity> opptyInsList = new List<Opportunity>();
        for(Integer k = 0; k<4; k++)
        {
            Opportunity oppty = new Opportunity(Name='test name'+k, AccountId=accntInsList[k].Id, ownerId= BatchIBDetachableSchedulableTest.SYSTEM_USER.Id, CloseDate=Date.today(), StageName=FunnelStatus.TRADED, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, 'Opportunity'));
            opptyInsList.add(oppty);
        }
        
        if(!opptyInsList.isEmpty())
        {
            try
            {
            insert opptyInsList;
            }
            catch(Exception e)
            {
            }
        }
        List<fxAccount__c> fxaList = new List<fxAccount__c>();
        for(Integer k =0; k<4 ; k++)
        {
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'test'+k+'@oanda.com';
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            tradeAccount.First_Trade_Date__c = Datetime.now();
            tradeAccount.Last_Trade_Date__c = DateTime.now();
            tradeAccount.Balance_USD__c = 12000;
            tradeAccount.Account__c = accntInsList[k].Id;
            tradeAccount.Opportunity__c = opptyInsList[k].Id;
            tradeAccount.Division_Name__c='OANDA Europe';
            tradeAccount.Introducing_Broker__c= accntInsList[k].Id;
            tradeAccount.Introducing_Broker_Number__c = '123456'+k;
            fxaList.add(tradeAccount);
        }
        if(! fxaList.isEmpty())
        {
            try
            {
            insert fxaList;
            }
            catch(Exception e)
            {
                System.debug(e.getMessage());
            }
        }

        System.debug([SELECT id, Introducing_Broker__c, Introducing_Broker_Number__c  FROM fxAccount__c ]);
    }
    
    @isTest static void testIBDetatchSchedulable1() 
    {
        // executing the AMR batch job under test context
        Test.startTest();
        CheckRecursive.setOfIDs = new Set<Id>();
        System.debug('update start');
        String Query = 'Select Id from fxAccount__c where Introducing_Broker__c != null AND  Introducing_Broker__r.Is_an_Introducing_Broker__c  = true AND Introducing_Broker__r.IB_Status__c = \'Inactive\' ';
        BatchIBDetachableSchedulable  batch = new BatchIBDetachableSchedulable(query);
        Database.executeBatch(batch);
        System.debug('update stop');
        Test.stopTest();
        // re-querying the data for checking the owner. we need to make sure the owner of the opportunity is not updated even though we change the account owner
        for(fxAccount__c var : [SELECT id, Introducing_Broker__c, Introducing_Broker_Number__c  FROM fxAccount__c  ])
        {
            system.assertEquals(var.Introducing_Broker__c , null);
            system.assertEquals(var.Introducing_Broker_Number__c  , null);
        }
    }
   
}