/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 08-19-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   08-19-2020   dmorales   Initial Version
**/
@isTest
private class CaseMiFIDValidatedChange_Test {
    @isTest
    static void test_invocable() {
        fxAccount__c validatedFxAccount = MIFIDValidationTestDataFactory.createValidatedFxAccount(
            'Negrete', 
            MIFIDValidationTestDataFactory.MEXICO_COUNTRY, 
            null, 
            '1224568'
        );
        Case caseFxAcc = MIFIDValidationTestDataFactory.createCaseWithFxAccount(validatedFxAccount);

        MiFID_Validation_Result__c validResult = [SELECT Id, Valid__c 
                                                  FROM MiFID_Validation_Result__c
                                                  WHERE fxAccount__c = :validatedFxAccount.Id][0];
        Boolean validatedBefore = validResult.Valid__c;
        Test.startTest();
           CaseMiFIDValidatedChange.fxAccountSetValidationResult(new List<Id>{caseFxAcc.Id});
        Test.stopTest();
        MiFID_Validation_Result__c validResultAfter = [SELECT Id, Valid__c 
                                                       FROM MiFID_Validation_Result__c
                                                       WHERE fxAccount__c = :validatedFxAccount.Id][0];
        Boolean validatedAfter = validResultAfter.Valid__c;
        System.assert(validatedBefore);
        System.assert(!validatedAfter);
    }
}