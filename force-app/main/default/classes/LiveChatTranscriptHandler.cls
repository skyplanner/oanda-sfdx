/**
 * Handles the events for the trigger LiveChatTranscript
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   27/04/2021
 */
public with sharing class LiveChatTranscriptHandler {

    public static final String CONV_RS_ALIAS = 'cdevt';
    public static final String CONV_RS_USER_PROFILE = 'Integration';
    public static final String ERROR_TITLE = 'Chat Transcript trigger error';
    public static final String NO_USER_ERROR_MSG = 'No active conv.rs user in the system';

    public static final String CONV_RS_TASK_SUBJECT = 'Conv.rs';
    public static final String CONV_RS_TASK_TYPE = 'SMS';
    public static final String CONV_RS_TASK_CALL_RESULT = 'Text Message';
    public static final String CONV_RS_TASK_STATUS = 'Completed';

    /**
     * Handles the After Insert event on the trigger LiveChatTranscriptTrigger
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   27/04/2021
     * @return: void
     * @param:  newList, the trigger.new context variable
     */
    public void handleAfterInsert(List<LiveChatTranscript> newList){
        updateCaseChatTranscriptCount(newList);
        createActivityIfConvRSUser(newList);
    }

    /**
     * Handles the After Delete event on the trigger LiveChatTranscriptTrigger
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   27/04/2021
     * @return: void
     * @param:  oldList, the trigger.old context variable
     */
    public void handleAfterDelete(List<LiveChatTranscript> oldList){
        updateCaseChatTranscriptCount(oldList);
    }

    public void updateCaseChatTranscriptCount(List<LiveChatTranscript> liveChatTranscriptList){

        Set<Id> caseIdSet = new Set<Id>();
        for(LiveChatTranscript l : liveChatTranscriptList){
            if(l.CaseId != null){
                caseIdSet.add(l.CaseId);
            }
        }

        map<Id, Decimal> caseIdLCTCount = new map<Id, Decimal>();
        List<Case> caseList = new List<Case>();
        for(AggregateResult agg : [
            SELECT Count(Id) cnt,
                CaseId 
            FROM LiveChatTranscript 
            WHERE CaseId IN :caseIdSet
            GROUP BY CaseId])
        {
            caseIdLCTCount.put(
                (Id)agg.get('CaseId'),
                (Decimal)agg.get('cnt')
            );
        }
        
        for(Id cId : caseIdSet){
            if(caseIdLCTCount.containsKey(cId)){
                caseList.add(
                    new Case(
                        Id = cId, 
                        LiveChatTranscript_Count__c = caseIdLCTCount.get(cId)
                    )
                );
            }
            else {
                caseList.add(
                    new Case(
                        Id = cId, 
                        LiveChatTranscript_Count__c = 0
                    )
                );
            }
        }

        if(!caseList.isEmpty()){
            List<Database.SaveResult> results = Database.update(caseList, false);
            List<Id> failedRecords = new List<Id>();
            for(Database.SaveResult result: results){
                if(!result.isSuccess()) failedRecords.add(result.id);
            }
            if(!failedRecords.isEmpty()){
                RetryLockedRecordsQueueable rerunAsynch = new RetryLockedRecordsQueueable(failedRecords, 'updateCaseChatTranscriptCount');
                System.enqueueJob(rerunAsynch);
            }
        }
    }

    public void createActivityIfConvRSUser(List<LiveChatTranscript> liveChatTranscriptList) {
        Map<Id,User> convRSUserMap = new Map<Id,User>([SELECT Id 
                                        FROM User
                                        WHERE Alias = :CONV_RS_ALIAS
                                            AND IsActive = true 
                                                AND Profile.Name = :CONV_RS_USER_PROFILE]);
        
        if(convRSUserMap.isEmpty()) {
            Logger.error(
                ERROR_TITLE,
                Constants.LOGGER_GENERAL_CATEGORY,
                NO_USER_ERROR_MSG);
        } else {
            List<Id> convRSUsersChatTranscriptIds = new List<Id>();

            for (LiveChatTranscript lct : liveChatTranscriptList) {
                if (convRSUserMap.containsKey(lct.OwnerId)) {
                    convRSUsersChatTranscriptIds.add(lct.Id);
                }
            }

            List<LiveChatTranscript> selectedTranscripts = [SELECT Id, Account.OwnerId, Account.fxAccount__r.Opportunity__c
                                                                FROM LiveChatTranscript
                                                                WHERE Id IN :convRSUsersChatTranscriptIds];
            List<Task> tasksToInsert = new List<Task>();
        
            for(LiveChatTranscript lct : selectedTranscripts) {
                if (lct.Account?.fxAccount__r?.Opportunity__c != null) {
                    Task t = new Task();
                    t.Subject = CONV_RS_TASK_SUBJECT;
                    t.Type = CONV_RS_TASK_TYPE;
                    t.CallDisposition = CONV_RS_TASK_CALL_RESULT;
                    t.OwnerId = lct.Account.OwnerId;
                    t.Status = CONV_RS_TASK_STATUS;
                    t.WhatId = lct.Account.fxAccount__r.Opportunity__c;
    
                    tasksToInsert.add(t);
                }
            }

            if (tasksToInsert.size() > 0) {
                insert tasksToInsert;
            }

        }
    }
}