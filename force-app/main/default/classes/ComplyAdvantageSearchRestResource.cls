/**
 * @description       :
 * @author            : OANDA
 * @group             :
 * @last modified on  : 29 Jul 2024
 * @last modified by  : Ryta Yahavets
 * @Description
 * URL: /services/apexrest/api/v1/user/{user_id}/comply_advantage
**/
@RestResource(urlMapping='/api/v1/user/*/comply_advantage')
global with sharing class ComplyAdvantageSearchRestResource {
    public static final List<String> POST_PARAMS = new List<String>{'user_id'};

    @HttpPost
    global static void doPost() {
        ComplyAdvantageRestResponse response;

        RestResourceHelper context = new RestResourceHelper(RestContext.request, RestContext.response, POST_PARAMS);
        try {

            Decimal fxAccountUserId = Decimal.valueOf(context.getPathParam('user_id'));
            System.debug('ComplyAdvantageRestResource fxAccount User Id: ' + fxAccountUserId);

            Map<String, Object> result = ComplyAdvantageHelper.complyAdvantageSearchPOST(fxAccountUserId);
            response = new ComplyAdvantageRestResponse(
                    (String) result.get('status'),
                    (Integer) result.get('statusCode'),
                    (String) result.get('message'),
                    (List<Map<String, Object>>) result.get('searchResponse'));
        }
        catch (Exception ex) {
            Logger.error('ca-search-trigger', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex.getMessage());
            response = new ComplyAdvantageRestResponse('Failed', 500, ex.getMessage(), null);
        } finally {
            context.setResponse(200, response);
        }
    }

    public class ComplyAdvantageRestResponse {
        public String status;
        public Integer response_code;
        public String message;
        public List<Map<String, Object>> comply_advantage_response;

        ComplyAdvantageRestResponse(
                String status,
                Integer responseCode,
                String message,
                List<Map<String, Object>> searchResponse)
        {
            this.status = status;
            this.response_code = responseCode;
            this.message = message;
            this.comply_advantage_response = searchResponse;
        }
    }
}