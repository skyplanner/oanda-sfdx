/**
 * @File Name          : GB_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/5/2020, 1:30:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/28/2020   acantero     Initial Version
**/
public class GB_MIFID_NID_Validator implements ExpressionValidator {

    public static final Integer VALIDABLE_TEXT_LENGTH = 2;
    public static final Integer VALIDABLE_TEXT_START_POS = 0;

    public static final List<String> INVALID_VALUES = new List<String> {
        'OO', 'CR', 'FY', 'MW', 'NC', 'PP', 'PZ', 'TN'
    };
    
    public String validate(String val) {
        if (String.isEmpty(val)) {
            return null;
        }
        //else...
        if (val.length() < (VALIDABLE_TEXT_START_POS + VALIDABLE_TEXT_LENGTH)) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        String text = val.substring(VALIDABLE_TEXT_START_POS, VALIDABLE_TEXT_LENGTH);
        if (INVALID_VALUES.contains(text)) {
            return System.Label.MifidValPasspFormatError;
        } 
        //else...
        return null;
    }

}