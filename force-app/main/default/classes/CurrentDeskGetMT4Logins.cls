public with sharing class CurrentDeskGetMT4Logins implements Queueable,Database.AllowsCallouts
{
    public CurrentDeskSync.LiveUser liveUser;
    public CurrentDeskGetMT4Logins(CurrentDeskSync.LiveUser userInfo) 
    {
        liveUser = userInfo;
    }

    public void execute(QueueableContext context) 
    {
        HttpRequest req = getMT4LoginsRequest();
        callService(req);
    }

    private HttpRequest getMT4LoginsRequest() 
    { 
        HttpRequest httpRequest = new HttpRequest();
        httpRequest = new HttpRequest();
        string clientId = string.valueOf(liveUser.CurrentDeskClientId);

        httpRequest.setEndpoint(CurrentDeskSync.ServiceSettings.baseUrl + '/misc/client/platformlogins' + '?id=' + clientId);
        httpRequest.setHeader('authorization', CurrentDeskSync.ServiceSettings.clientSecret);   
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setMethod('GET');

        return httpRequest;
    }

    private void callService(HttpRequest req) 
    { 
        Http http = new Http();
        HttpResponse response = http.send(req);
        if(response.getStatusCode() == 408)
        {
            http = new Http();
            response = http.send(req);
        } 

        //handle response
        if(response.getStatusCode() == 200)
        {
            if(string.isNotBlank(response.getBody()))
            {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());               
                if(results.get('Logins') != null)
                {
                    fxAccount__c fxa = new fxAccount__c(Id = liveUser.fxAccountId);
                    fxa.MT4_Logins__c = (string) results.get('Logins');
                    update fxa;
                }
            }    
        }
        else 
        {
            //send email
            string subject = 'CurrentDesk : MT4 logins retrieval failed for user : ' + liveUser.fxAccountDetails.Name;
            string emailBody = 'Repsonse: \n ' + response.getBody();
            emailBody +=  '\n\nSalesforce link: \n' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + liveUser.fxAccountId;
            EmailUtil.sendEmail('salesforce@oanda.com', subject, emailBody);
        }
    }
}