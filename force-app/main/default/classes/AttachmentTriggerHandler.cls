/**
 * @Description  : Trigger handler for Attachment sobject.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-02
**/
public class AttachmentTriggerHandler extends TriggerHandler {
    List<Attachment> triggerNew;
    List<Attachment> triggerOld;

    /**
    * @description Constructor.
    * @author Jakub Fik | 2024-05-02 
    **/
    public AttachmentTriggerHandler() {
        this.triggerNew = (List<Attachment>) Trigger.new;
        this.triggerOld = (List<Attachment>) Trigger.old;
    }

    /**
    * @description Override beforeInsert method.
    * @author Jakub Fik | 2024-05-02 
    **/
    public override void beforeInsert() {
        Attachment_Util.reparentFromEmailMessageToDocument(this.triggerNew);
        Attachment_Util.reparentFromAccountToDocument(this.triggerNew);
    }

    /**
    * @description  Override beforeDelete method.
    * @author Jakub Fik | 2024-05-02 
    **/
    public override void beforeDelete() {
        Attachment_Util.preventDeleteAttachment(this.triggerOld);
    }

    /**
    * @description  Override afterInsert method.
    * @author Jakub Fik | 2024-05-02 
    **/
    public override void afterInsert() {
        // Do GBG scan
        new GbgManager(this.triggerNew).scan();
    }
}