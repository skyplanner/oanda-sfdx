/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsField {
    public String locale { get; set; }

    public String name { get; set; }

    public String source { get; set; }

    public String tag { get; set; }
    
    public String value { get; set; }
}