public class BatchDocumentExpiryCheck  implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Database.Stateful, Schedulable {
    
    String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
    public static final Integer BATCH_SIZE = 200;   
    //schedule everyday at 1 AM
    public static final String CRON_SCHEDULE = '0 0 1 ? * * *';   
    public List<String> errs = new List<String>();
    public set<string> divisions_apac = new set<string>
    {
        'OANDA Asia Pacific',
        'OANDA Asia Pacific CFD',
        'OANDA Australia',
        'OANDA Global Markets'
    };
        
    private static final string DOC_NAME_WHOLESALE_CLIENT='Document Wholesale Client';
    private static final string DOC_NAME_LEI='Document LEI';
    private static final string CASE_DESCRIPTION_SUBSTRING='will expire for fxAccount';
    public static final string onboardingQueueId = UserUtil.getQueueId('On Boarding Cases');
    public static final string expiredDocumentsVerificationQueueId = UserUtil.getQueueId('Expired Documents Verification');
    public static final string adhocOAPOAUQueueId = UserUtil.getQueueId('Ad Hoc (OAP/OAU) Cases');
	public static final Id supportOBCaseRecordTypeId = RecordTypeUtil.getSupportOBCaseTypeId();
    public static final Id orgWideEmailId = getOrgWideEmailId('frontdesk@oanda.com');
    public static date checkDate = Date.today();
    public string[] queryFields =  new string[]
    {
      'Id', 
      'Name', 
      'Document_Type__c', 
      'Expiry_Date__c',
      'Account__c',
      'fxAccount__c',
      'fxAccount__r.Name',
      'fxAccount__r.fxTrade_User_Id__c',
      'fxAccount__r.Contact__c',
      'fxAccount__r.Division_Name__c',
      'lead__c'
    };

    public BatchDocumentExpiryCheck() 
    {
        query = 'Select ' + String.join(queryFields,',') + ' From Document__c ';
        query += 'where Expiry_Date__c != null and ';
        query += 'Expiry_Date__c = :checkDate and ';
        query += 'fxAccount__c != null and '; 
        query += 'fxAccount__r.Is_Closed__c = false and ';
        query += 'fxAccount__r.Ready_for_Funding_DateTime__c != null and ';
      	query += 'fxAccount__r.RecordType.Name = \'Retail Live\'';
    }       

	public BatchDocumentExpiryCheck(String q) 
    {
		query = q;
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;

        query = 'Select ' + String.join(queryFields,',') + ' From Document__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {       
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<Document__c> scope)
    {
        case[] DocumentExpiryNotificationCases = new case[]{};
        for(Document__c doc : scope)
        {
            case DocumentExpiryNotificationCase = new case();
            DocumentExpiryNotificationCase.AccountId = doc.Account__c;  
            DocumentExpiryNotificationCase.fxAccount__c = doc.fxAccount__c;
            DocumentExpiryNotificationCase.Lead__c = doc.Lead__c;
            if(doc.fxAccount__c != null)
            {
                DocumentExpiryNotificationCase.ContactId = doc.fxAccount__r.Contact__c;
                if(divisions_apac.contains(doc.fxAccount__r.Division_Name__c)&& doc.fxAccount__r.Division_Name__c != Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
                {
                    DocumentExpiryNotificationCase.OwnerId = adhocOAPOAUQueueId;
                }
                //SP-13422
                else if(doc.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_EUROPE || doc.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS){
                    DocumentExpiryNotificationCase.OwnerId = expiredDocumentsVerificationQueueId;
                } 
                else {
                    DocumentExpiryNotificationCase.OwnerId = onboardingQueueId;
                     }
            }
            DocumentExpiryNotificationCase.Inquiry_Nature__c = 'Account'; 
            DocumentExpiryNotificationCase.Type__c = 'Expired docs';
            DocumentExpiryNotificationCase.Live_or_Practice__c = 'Live';
            DocumentExpiryNotificationCase.Origin = 'Internal';
            DocumentExpiryNotificationCase.RecordTypeId = supportOBCaseRecordTypeId;
            DocumentExpiryNotificationCase.Subject = 'Document Expiry Notification';
            DocumentExpiryNotificationCase.Status = 'Waiting on Date';
            DocumentExpiryNotificationCase.Waiting_for_Date__c = doc.Expiry_Date__c.addDays(30);
            DocumentExpiryNotificationCase.Expiry_Document_Due_Date__c = doc.Expiry_Date__c.addDays(30);
            DocumentExpiryNotificationCase.Expiry_Document_Name__c = doc.Name +' (' + doc.Document_Type__c  + ')';
           
            DocumentExpiryNotificationCase.Description = 'Document ' + doc.Name  + ' ' + CASE_DESCRIPTION_SUBSTRING + ' ';
            DocumentExpiryNotificationCase.Description += doc.fxAccount__r.Name  + ' (fxTrade User Id: ' +doc.fxAccount__r.fxTrade_User_Id__c+ ') on ';
            DocumentExpiryNotificationCase.Description += doc.Expiry_Date__c.year() + '-' + doc.Expiry_Date__c.month() + '-' + doc.Expiry_Date__c.day();
            DocumentExpiryNotificationCase.Description += '. Document link: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + doc.Id;
            
            DocumentExpiryNotificationCases.add(DocumentExpiryNotificationCase);
            
           system.debug('DocumentExpiryNotificationCase :' + DocumentExpiryNotificationCase);
        }  
        
        Database.SaveResult[] caseCreationResults = Database.Insert(DocumentExpiryNotificationCases , false);
        for (Database.SaveResult res : caseCreationResults) 
        {
            if(!res.isSuccess())
            {
                for (Database.Error error : res.getErrors()) 
                {
                   System.debug('Error returned: ' + error.getStatusCode() +' - '+ error.getMessage());
                }
            }
         }
    } 
    
    //schedule
    public void execute(SchedulableContext context) 
    {
		Database.executeBatch(new BatchDocumentExpiryCheck(), BATCH_SIZE);
    }
    

     public static void execute() 
     {
         Database.executeBatch(new BatchDocumentExpiryCheck(), BATCH_SIZE);
     }
    
    public static void schedule(string cronExpression) 
    { 
         if(String.isNotBlank(cronExpression) && String.isNotEmpty(cronExpression))
         {
			System.schedule('BatchDocumentExpiryCheck', cronExpression, new BatchDocumentExpiryCheck());
         }
	}
    
    //schedule the batch job
    public static void schedule() 
    {      
        schedule(CRON_SCHEDULE);
	}
    
    public void finish(Database.BatchableContext bc)
    {  
        if(!Test.isRunningTest())
        {
        	EmailUtil.sendEmailForBatchJob(bc.getJobId());
        }
    }
    
    // public static Id getEmailTemplateId(string templateName)
    // {
    //     EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName LIMIT 1];
    //     return template.Id;
    // }
    
    public static Id getOrgWideEmailId(string emailId)
    {
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :emailId];
        if (owea.size() > 0 ) 
        {
            return owea.get(0).Id;
        }
        return null;
    }
    
    @InvocableMethod(label='Document Expiry- Send Reminder' description='Sends email to customer')
    public static void sendDocumentExpiryEmail(List<Id> caseIds)
    {
    string templateId;
        
        if(caseIds.size() > 0) 
        { 
            Messaging.SingleEmailMessage[] emailMessages = new Messaging.SingleEmailMessage[]{};
            for(Case c : [select Id,
                                status,
                                Subject,
                                Description,
                                CreatedDate, 
                                contactId,
                                fxAccount__r.Division_Name__c,
                                fxAccount__r.Country__c,
                                fxAccount__r.Is_Closed__c
                            From Case 
                            where Id =:caseIds])
            {
                if(c.status == 'Waiting on Date' && c.fxAccount__c != null && c.fxAccount__r.Is_Closed__c == false && c.ContactId != null)
                {
                    if(c.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_GLOBAL_MARKETS)
                    {
                        templateId = CustomMetadataUtil.getTemplateDeveloperName(c.fxAccount__r.Country__c + ' - OGM');
                        if(String.isBlank(templateId))
                        {
                            templateId = CustomMetadataUtil.getTemplateDeveloperName('OGM Default');
                        }
                    }
                    else if(c.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_ASIA_PACIFIC || 
                        c.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD)
                    {
                        templateId = CustomMetadataUtil.getTemplateDeveloperName(c.fxAccount__r.Country__c);
                        if(String.isBlank(templateId))
                        {
                            templateId = CustomMetadataUtil.getTemplateDeveloperName('OAP/OAU Default');
                        }
                    }
                    else if(c.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_AUSTRALIA)
                    {
                        String docName = getDocumentNameFromCaseDescription(c.Description);
                        System.debug('docName ::: ' + docName);

                        if(docName != null && docName.containsIgnoreCase(DOC_NAME_WHOLESALE_CLIENT))
                        {
                            templateId = CustomMetadataUtil.getTemplateDeveloperName('OAU - PRO');
                        }
                        else if(docName != null && docName == DOC_NAME_LEI)
                        {
                            templateId = CustomMetadataUtil.getTemplateDeveloperName('OAU - LEI');
                        }
                        else
                        {
                            templateId = CustomMetadataUtil.getTemplateDeveloperName(c.fxAccount__r.Country__c);
                            if(String.isBlank(templateId))
                            {
                                templateId = CustomMetadataUtil.getTemplateDeveloperName('OAP/OAU Default');
                            }
                        }
                    }
                    else if(c.fxAccount__r.Division_Name__c == Constants.DIVISIONS_OANDA_EUROPE)
                    {
                        templateId = CustomMetadataUtil.getTemplateDeveloperName(c.fxAccount__r.Division_Name__c);
                    }

                    if(c.Subject == 'Document Expiry Notification: FIN')
                    {
                        templateId = CustomMetadataUtil.getTemplateDeveloperName('OAP - FIN');
                    }

                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(c.contactId);
                    mail.setSaveAsActivity(true);
                    mail.setTemplateId(templateId);
                    mail.setWhatId(c.Id);
                    mail.setOrgWideEmailAddressId(orgWideEmailId);
                    emailMessages.add(mail);
                }
            }
            if(emailMessages.size() > 0)
            {
                Messaging.sendEmail(emailMessages, false);
            }
        }    
    }

    private static string getDocumentNameFromCaseDescription(String caseDescription)
    {
        Integer index = caseDescription.indexOf(CASE_DESCRIPTION_SUBSTRING);
        
        if (index != -1) {
            return caseDescription.substring(0, index-1);
        } else {
            return null;
        }
    }
}