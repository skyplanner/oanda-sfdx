/**
 * @File Name          : SRoutingHelper.cls
 * @Description        : 
 * Methods that allow you to customize the routing performed by Omnichannel
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/27/2024, 12:29:29 PM
**/
public interface SRoutingHelper {

	void setPriorityCalculator(SRoutingPriorityCalculator priorityCalculator);

	SRoutingPriorityCalculator getPriorityCalculator();

	void setSkillReqManager(SSkillRequirementsManager skillReqManager);

	SSkillRequirementsManager getSkillReqManager();

	void calculatePriority();

	List<SkillRequirement> getSkillRequirements();

	Boolean updateWorkItems();

	Boolean createRoutingLogs();

}