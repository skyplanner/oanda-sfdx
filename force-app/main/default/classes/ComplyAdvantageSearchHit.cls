/**
 * Wrapper for Comply advantage JSON: Search Details
 * @author Fernando Gomez
 */
public class ComplyAdvantageSearchHit {
	@AuraEnabled
	public ComplyAdvantageKeyInformation key_information { get; set; }

	@AuraEnabled
	public String id { get; set; }
	@AuraEnabled
	public String last_updated_utc { get; set; }
	@AuraEnabled
	public String created_utc { get; set; }

	@AuraEnabled
	public ComplyAdvantageSearchDoc uncategorized { get; set; }

	@AuraEnabled
	public Map<String, Map<String, ComplyAdvantageSearchListingInfo>> full_listing { 
		get; set; }

	public boolean isInternalHit { get; set; }

	/**
	 * @param caSearchId
	 * @param caSearchId
	 * @return a Salesforce version of ther entity
	 */
	public Comply_Advantage_Search_Entity__c getSalesforceRecord(
			Id caSearchId,
			String searchId) {
		Comply_Advantage_Search_Entity__c result =
			new Comply_Advantage_Search_Entity__c(
				Comply_Advantage_Search__c = caSearchId,
				Entity_Id__c = id,
				Unique_ID__c = searchId + ':' + id
			);

		if (key_information != null) 
		{
			result.Name = key_information.name.length() > 80 ? key_information.name.substring(0, 79) : key_information.name;
			result.Entity_Type__c = key_information.entity_type;
			result.Is_Whitelisted__c = key_information.is_whitelisted;
			result.Risk_Level__c = key_information.risk_level;
			result.Match_Status__c = key_information.match_status;
			result.Match_Types__c = key_information.match_types != null ? String.join(key_information.match_types, ', ') : null;
			result.Primary_Country__c = key_information.primary_country;
			result.Countries__c = key_information.country_names;
			result.Date_Of_Birth__c = key_information.date_of_birth;
			result.Age__c = key_information.age;
			result.Entity_Match_Types__c = key_information.types != null ? String.join(key_information.types, ';') : null;
			result.Sources__c = key_information.sources != null ? String.join(key_information.sources, ', ') : null;
			result.AKA__c = StringUtil.joinValues(key_information.aka, ', ');

			result.Internal_Hit_Address__c = key_information.address;
			result.Internal_Hit_Email__c = key_information.email;
			result.Internal_Hit_Id__c = key_information.user_defined_id;
			result.Internal_Hit_Notes__c = key_information.notes;
			result.Internal_Hit_Phone__c = key_information.phone;
			
			result.Is_Internal_Hit__c = this.isInternalHit;
		}

		if (uncategorized != null) {
			result.Keywords__c = uncategorized.keywords != null ?
				String.join(uncategorized.keywords, ', ') : null;
		}

		return result;
	}
}