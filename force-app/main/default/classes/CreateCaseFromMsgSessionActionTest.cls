/**
 * @File Name          : CreateCaseFromMsgSessionActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 12:43:57 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 12:21:55 PM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class CreateCaseFromMsgSessionActionTest {
	@isTest
	private static void testMessagingSessionIdsIsNullOrEmpty() {
		// Test data setup
		List<CreateCaseFromMsgSessionAction.CaseInfo> caseInfos;
		// Actual test
		Test.startTest();
		caseInfos = CreateCaseFromMsgSessionAction.createCaseFromMsgSession(
			null
		);
		Assert.areEqual(
			true,
			caseInfos != null && caseInfos.size() > 0,
			'Case info list should not be null or empty'
		);
		caseInfos = null;
		caseInfos = CreateCaseFromMsgSessionAction.createCaseFromMsgSession(
			new List<ID>()
		);

		Assert.areEqual(
			true,
			caseInfos != null && caseInfos.size() > 0,
			'Case info list should not be null or empty'
		);
		Test.stopTest();

		// Asserts
	}

	@isTest
	private static void testLanguageIsAvailableThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		List<CreateCaseFromMsgSessionAction.CaseInfo> caseInfos;
		// Actual test
		Test.startTest();
		try {
			caseInfos = CreateCaseFromMsgSessionAction.createCaseFromMsgSession(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@isTest
	private static void testCreateCaseFromMsgSession() {
		// Test data setup
		List<CreateCaseFromMsgSessionAction.CaseInfo> caseInfos;
		// Actual test
		Test.startTest();
		caseInfos = CreateCaseFromMsgSessionAction.createCaseFromMsgSession(
			new List<ID>{ null }
		);
		Test.stopTest();
		Assert.areEqual(
			true,
			caseInfos != null && caseInfos.size() > 0,
			'Case info list should not be null or empty'
		);

		Assert.areEqual(
			true,
			String.isBlank(caseInfos.get(0).caseId),
			'Case Id should not be blank'
		);
		Assert.areEqual(
			true,
			String.isBlank(caseInfos.get(0).caseNumber),
			'Case Number should not be blank'
		);
	}
}