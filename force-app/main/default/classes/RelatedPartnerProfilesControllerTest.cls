/**
 * @Description  : Test class for RelatedPartnerProfilesController apex class.
 * @Author       : Jakub Fik
 * @Date         : 2024-04-18
**/
@isTest
public with sharing class RelatedPartnerProfilesControllerTest {

    @TestSetup
    static void setup(){
        Affiliate__c testPartner1 = new Affiliate__c (
            Username__c = 'testUserName1',
            Firstname__c = 'testName',
            LastName__c = 'testLastName',
            Email__c = 'test@test.test',
            Division_Name__c = 'OANDA Corporation'
        );
        Affiliate__c testPartner2 = new Affiliate__c (
            Username__c = 'testUserName2',
            Firstname__c = 'testName',
            LastName__c = 'testLastName',
            Email__c = 'test@test.test',
            Division_Name__c = 'OANDA Canada'
        );
        insert new List<Affiliate__c> {testPartner1, testPartner2};
    }

    /**
    * @description Success test for getRelatedPartnerProfiles method.
    * @author Jakub Fik | 2024-04-18 
    **/
    @IsTest
    static void getRelatedPartnerProfilesTest(){
        Affiliate__c testPartner = [SELECT Id FROM Affiliate__c LIMIT 1];
        Test.startTest();
        List<Affiliate__c> relatedAccounts = RelatedPartnerProfilesController.getRelatedPartnerProfiles(testPartner.Id);
        Test.stopTest();
        System.assertEquals(1, relatedAccounts.size(), 'One Affiliate__c object should be returned.');
        System.assertNotEquals(testPartner.Id, relatedAccounts[0].Id, 'Related object should have different Ids.');
    }

    /**
    * @description Error fail test.
    * @author Jakub Fik | 2024-04-18 
    **/
    @IsTest
    static void incorrectIdErrorTest(){
        Boolean isAuraException = false;
        Test.startTest();
        try {
            RelatedPartnerProfilesController.getRelatedPartnerProfiles(UserUtil.currentUser.Id);
        } catch (AuraHandledException exc) {
            isAuraException = true;
        }
        Test.stopTest();
        System.assert(isAuraException, 'Method accepts incorrect Id. Exception should be thrown.');
    }
}