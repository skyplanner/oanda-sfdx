@isTest
private class DocumentsCleanerScheduler_Test {

	@testSetup
	static void setup () {
	    ArticlesTestDataFactory.createArticlesWithDummyDocs();
	}

	@isTest
	static void execute_test() {
		Integer beforeCount = [select count() from Document];
		DocumentsCleanerScheduler scheduler = new DocumentsCleanerScheduler();
		Test.startTest();
		scheduler.execute(null);
		Test.stopTest();
		Integer count = [select count() from Document];
		System.assertEquals(beforeCount, count);
	}

}