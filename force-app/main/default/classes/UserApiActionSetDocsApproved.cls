/**
 * User API 'Set Documents Approved' action manager class
 */
public class UserApiActionSetDocsApproved extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.setDocumentsApproved();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return !user.user_status.documentsApproved;
    }
}