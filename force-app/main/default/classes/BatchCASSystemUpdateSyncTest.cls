/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 08-03-2022
 * @last modified by  : Yaneivys Gutierrez
**/
@isTest
private class BatchCASSystemUpdateSyncTest {
	@isTest
	static void updateSearchAccount400() {
        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			400,
			'{ "code": 400, "status": "error" }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        BatchCASSystemUpdateSync batchSearchUpdate = new BatchCASSystemUpdateSync();
        Database.executeBatch(batchSearchUpdate);

		Test.stopTest();

        search = [
			SELECT Is_Monitored__c 
			FROM Comply_Advantage_Search__c
			WHERE Id = :search.Id
		];

        System.assertEquals(true, search.Is_Monitored__c);
	}

	@isTest
	static void updateSearchAccount() {
        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			200,
			'{ "content": { "is_monitored": false } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        BatchCASSystemUpdateSync batchSearchUpdate = new BatchCASSystemUpdateSync();
        Database.executeBatch(batchSearchUpdate);

		Test.stopTest();

        search = [
			SELECT Is_Monitored__c 
			FROM Comply_Advantage_Search__c
			WHERE Id = :search.Id
		];

        System.assertEquals(false, search.Is_Monitored__c);
	}

	@isTest
	static void updatefxAccountSched() {
        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			200,
			'{ "content": { "is_monitored": false } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

		BatchCASSystemUpdateSync batchSearchUpdate = new BatchCASSystemUpdateSync();
        batchSearchUpdate.execute(null);

		Test.stopTest();

        search = [
			SELECT Is_Monitored__c 
			FROM Comply_Advantage_Search__c
			WHERE Id = :search.Id
		];

        System.assertEquals(false, search.Is_Monitored__c);
	}

	@isTest
	static void updatefxAccountSched2() {
        Settings__c settings = new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = false
		);
		insert settings;
		
		Comply_Advantage_Instance_Setting__c caInstanceSetting = new Comply_Advantage_Instance_Setting__c(
			Name = 'NA',
			API_Key__c = 'test'
		);
		insert caInstanceSetting;
		
		Comply_Advantage_Division_Setting__c caDivisionSetting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Canada',
			Search_Profile__c = 'CA Temp',
			Secondary_Search_Profile__c = 'CA Temp Sec',
			Fuzziness__c = 60,
			Comply_Advantage_Instance_Setting__c = caInstanceSetting.Id,
			Region__c = 'OCAN',
			New_Seach_Triggered_On_Objects__c = 'Account;Affiliate__c;Contact;fxAccount__c;Lead'
		);
		insert caDivisionSetting;

		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);

		fxAccount__c fxAcc = new fxAccount__c(
			Account_Email__c = 'testingBatch1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = Date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = false,
            Is_CA_Name_Search_Monitored__c = true
		);
		insert fxAcc;

        Comply_Advantage_Search__c search = new Comply_Advantage_Search__c(
			Name = 'Sample',
			fxAccount__c = fxAcc.Id,
			Search_Id__c = '478009745',
			Is_Monitored__c = true,
			Custom_Division_Name__c = 'OANDA Canada',
			Search_Parameter_Mailing_Country__c = 'Canada'
		);
		insert search;

		Settings__c setts = Settings__c.getValues('Default');
		setts.Comply_Advantage_Enabled__c = true;
		update setts;

		CalloutMock mock = new CalloutMock(
			200,
			'{ "content": { "is_monitored": false } }',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.StartTest();

        String jobId = BatchCASSystemUpdateSync.schedule(null);

		Test.stopTest();

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
		System.assertEquals(BatchCASSystemUpdateSync.CRON_SCHEDULE, ct.CronExpression);
		System.assertEquals(0, ct.TimesTriggered);
	}
}