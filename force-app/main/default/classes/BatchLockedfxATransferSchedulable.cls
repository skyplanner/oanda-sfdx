/* Name: BatchLockedfxATransferSchedulable 
 * Description : transfers the locked fxAccount and its opp & accounts to system user
 * Author: Sri 
 * Date : 2020-09-01
 */
public class BatchLockedfxATransferSchedulable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable 
{
    
    private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();

    public static final Integer BATCH_SIZE = 1;
    
    public BatchLockedfxATransferSchedulable() 
    {
     /* //   execute below for  detaching the inactive IB from fxaccount
     Id suid= UserUtil.getSystemUserId();
    BatchLockedfxATransferSchedulable batch=  new BatchLockedfxATransferSchedulable('Select Id,FXAIDonRelatedOpp__c,Opportunity__c from fxAccount__c where LockedAccTrxfrCriteria__c = true') ;
    System.schedule('BatchLockedfxATransferSchedulable', '0 0 4 * * ?', batch);
        */
    }

    public BatchLockedfxATransferSchedulable(String q) 
    {
        query = q;
    }
    public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'Select Id,FXAIDonRelatedOpp__c,Opportunity__c from fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> batch) 
    {
        List<fxAccount__c> fxAccs= (List<fxAccount__c>) batch;
        System.debug('****'+fxAccs);
        List<Opportunity> opps = new List<Opportunity>();
        Set<Id> forDuplicateCheck = new Set<Id>();
        Id suId= Userutil.getSystemUserId();
        for(fxAccount__c var : fxAccs)
        {
            if( var.FXAIDonRelatedOpp__c == var.Id && forDuplicateCheck.contains(var.Opportunity__c) == false)
            {
                forDuplicateCheck.add(var.Opportunity__c);
                opps.add(new Opportunity(id = var.Opportunity__c, ownerid=suId));
            }
        }
        if(opps.size()>0)
        {
            try
            {
                update opps;
            }
            catch(Exception e)
            {
                System.debug('Error :'+e.getMessage());
            }
        }
        checkRecursive.SetOfIDs  = new Set<Id>();
    }
    
    public void execute(SchedulableContext context) 
    {
        Database.executeBatch(new BatchLockedfxATransferSchedulable(query), BATCH_SIZE);
    }

    public void finish(Database.BatchableContext BC) 
    {
        BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
    }

}