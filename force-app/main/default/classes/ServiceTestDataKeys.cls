/**
 * @File Name          : ServiceTestDataKeys.cls
 * @Description        :
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/14/2024, 12:02:44 PM
 **/
@IsTest
public without sharing class ServiceTestDataKeys {

	public static final String LEAD_1 = 'LEAD_1';
	public static final String ACCOUNT_1 = 'ACCOUNT_1';
	public static final String FX_ACCOUNT_1 = 'FX_ACCOUNT_1';
	public static final String CASE_1 = 'CASE_1';
	public static final String CONTACT_1 = 'CONTACT_1';
	public static final String LEAD_CASE_1 = 'LEAD_CASE_1';
	public static final String LIVE_CHAT_VISITOR_1 = 'LIVE_CHAT_VISITOR_1';
	public static final String LIVE_CHAT_TRANSCRIPT_1 = 'LIVE_CHAT_TRANSCRIPT_1';
	public static final String NON_HVC_CASE_1 = 'NON_HVC_CASE_1';
	public static final String NON_HVC_CASE_2 = 'NON_HVC_CASE_2';
	public static final String NON_HVC_CASE_3 = 'NON_HVC_CASE_3';

	/*
	public static final String HVC_ACCOUNT = 'HVC_ACCOUNT';
	public static final String NEW_CUSTOMER_ACCOUNT = 'NEW_CUSTOMER_ACCOUNT';
	public static final String ACTIVE_CUSTOMER_ACCOUNT = 'ACTIVE_CUSTOMER_ACCOUNT';
	public static final String CUSTOMER_ACCOUNT = 'CUSTOMER_ACCOUNT';

	public static final String NEW_CUSTOMER_LEAD = 'NEW_CUSTOMER_LEAD';
	public static final String CUSTOMER_LEAD = 'CUSTOMER_LEAD';
	*/

	public static final String QUEUE_CASE_1 = 'QUEUE_CASE_1';
	public static final String USER_1 = 'USER_1';
	public static final String EMAIL_TEMPLATE_1 = 'EMAIL_TEMPLATE_1';
	public static final String SETTINGS_1 = 'SETTINGS_1';

	public static final String ACCOUNT1_EMAIL = 'account1@a.com';
	public static final String LEAD1_EMAIL = 'lead1@a.com';

	/*
	public static final String HVC_EMAIL = 'hvc@a.com';
	public static final String NEW_CUSTOMER_EMAIL = 'newcustomer@a.com';
	public static final String ACTIVE_CUSTOMER_EMAIL = 'activecustomer@a.com';
	public static final String CUSTOMER_EMAIL = 'customer@a.com';

	public static final String LEAD_NEW_CUSTOMER_EMAIL = 'newcustomer@l.com';
	public static final String LEAD_CUSTOMER_EMAIL = 'customer@l.com';
	*/
	public static final String SEGMENTATION_1 = 'SEGMENTATION_1';
	public static final String AGENT_EXTRA_INFO_1 = 'AGENT_EXTRA_INFO_1';
}