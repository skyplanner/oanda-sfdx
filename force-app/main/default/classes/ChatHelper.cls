/**
 * @File Name          : ChatHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 3/4/2024, 12:31:01 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/5/2020   acantero     Initial Version
**/
public class ChatHelper {

    public static final String NF_VALID_SPOT_CRYPTO = 'true';

    List<LiveChatTranscript> chatTranscriptList;

    public ChatHelper(List<LiveChatTranscript> chatTranscriptList) {
        this.chatTranscriptList = chatTranscriptList;
    }

    // public void fillFromNFChatData() {
    //     if ((chatTranscriptList == null) || chatTranscriptList.isEmpty()) {
    //         return;
    //     }
    //     //else...
    //     Map<String,LiveChatTranscript> chatMap = new Map<String,LiveChatTranscript>();
    //     for(LiveChatTranscript chatObj : chatTranscriptList) {
    //         if (
    //             (chatObj.From_Pre_Chat_Form__c != true) &&
    //             String.isNotBlank(chatObj.ChatKey)
    //         ) {
    //             chatMap.put(chatObj.ChatKey, chatObj);
    //         }
    //     }
    //     if (chatMap.isEmpty()) {
    //         return;
    //     }
    //     //else...
    //     List<nfchat__Chat_Log__c> nfChatList = getNfChatLogs(chatMap.keySet());
    //     copyChatInfo(nfChatList, chatMap);
    // }

    // @testVisible
    // void copyChatInfo(
    //     List<nfchat__Chat_Log__c> nfChatList,
    //     Map<String,LiveChatTranscript> chatMap
    // ) {
    //     for(nfchat__Chat_Log__c nfChatObj : nfChatList) {
    //         LiveChatTranscript chatObj = chatMap.get(nfChatObj.nfchat__Chat_Key__c);
    //         if (chatObj != null) {
    //             chatObj.Chat_Email__c = nfChatObj.nfchat__Email__c;
    //             chatObj.Chat_Language_Preference__c = 
    //                 ServiceLanguagesManager.getLanguageCode(nfChatObj.Language_Preference__c);
    //             chatObj.Chat_Type_of_Account__c = nfChatObj.Chat_Type_of_Account__c;
    //             chatObj.Inquiry_Nature__c = nfChatObj.Inquiry_Nature__c;
    //             chatObj.Type__c = nfChatObj.Type__c;
    //             chatObj.Spot_Crypto__c = (nfChatObj.Spot_Crypto__c == NF_VALID_SPOT_CRYPTO);
    //             //chatObj.Is_HVC__c = nfChatObj.Is_HVC__c; //has a wrong value
    //         }
    //     }
    // }

    // @testVisible
    // List<nfchat__Chat_Log__c> getNfChatLogs(Set<String> chatkeySet) {
    //     return [
    //         SELECT
    //             nfchat__Chat_Key__c,
    //             Language_Preference__c,
    //             nfchat__Email__c,
    //             Chat_Type_of_Account__c,
    //             Inquiry_Nature__c,
    //             Type__c,
    //             Is_HVC__c,
    //             Spot_Crypto__c
    //         FROM
    //             nfchat__Chat_Log__c
    //         WHERE
    //             nfchat__Chat_Key__c IN :chatkeySet
    //     ];
    // }
    
    public void checkMissedChats(Map<Id,LiveChatTranscript> oldMap) {
        if ((chatTranscriptList == null) || chatTranscriptList.isEmpty()) {
            return;
        }
        //else...
        List<Id> chatMissedIdList = new List<Id>();
        for(LiveChatTranscript chatObj : chatTranscriptList) {
            String oldStatus = (oldMap != null)
                ? oldMap.get(chatObj.Id).Status
                : null;
            if (
                (chatObj.Status == OmnichanelConst.CHAT_STATUS_MISSED) && 
                (chatObj.Status != oldStatus)
            ) {
                chatMissedIdList.add(chatObj.Id);
            }
        }
        //insert new Acl_log__c(Text1__c = 'checkMissedChats', Text2__c = 'size: ' + hvcChatMissedIdList.size());
        // try {
        if (!chatMissedIdList.isEmpty()) {
            SLAViolationNotifiableFactory.SLAViolationNotifiableFactory(
            SLAConst.SLANotificationObjectType.LIVE_CHAT_NOT,
            SLAConst.SLAViolationType.ANSWER,
            new ChatSLAViolationNotifierDataReader()
            )?.notifyViolation(
                new ChatSLAViolationNotificationData(chatMissedIdList)
            );
        }
        // } catch (Exception ex) {
        //     insert new Acl_log__c(
        //         Text1__c = 'checkMissedChats Exception', 
        //         Text2__c = ex.getMessage(), 
        //         Text5__c = ex.getStackTraceString()
        //     );
        // } 
    }

    public void completeInfo() {
        if ((chatTranscriptList == null) || chatTranscriptList.isEmpty()) {
            return;
        }
        //else...
        Set<String> emailSet = new Set<String>();
        List<LiveChatTranscript> chatIncompleteList = new List<LiveChatTranscript>();
        for(LiveChatTranscript lct : chatTranscriptList) {
            if (
                String.isNotBlank(lct.Chat_Email__c) &&
                String.isBlank(lct.LeadId) &&
                (
                    String.isBlank(lct.AccountId) ||
                    String.isBlank(lct.ContactId) ||
                    (lct.Is_HVC__c == null) ||
                    String.isBlank(lct.Chat_Division__c)
                ) 
            ) {
                emailSet.add(lct.Chat_Email__c);
                chatIncompleteList.add(lct);
            } 
        }
        if (!emailSet.isEmpty()) {
            chatIncompleteList = completeInfoFromAccount(chatIncompleteList, emailSet);
            if (!emailSet.isEmpty()) {
                completeInfoFromLead(chatIncompleteList, emailSet);
            }
        }
    }

    /**
     * Complete the information of the chats related to Accounts
     * Remove emails from Accounts related chats from "emailSet"
     * Returns a list with chats that are not related to Accounts
     */
    @testVisible
    List<LiveChatTranscript> completeInfoFromAccount(
        List<LiveChatTranscript> chatIncompleteList,
        Set<String> emailSet
    ) {
        List<LiveChatTranscript> result = new List<LiveChatTranscript>();
        Map<String, Account> emailAccountMap = new Map<String, Account>();
        System.debug('completeInfoFromAccount');
        for(Account acc : [SELECT   Id,
                                    Is_High_Value_Customer__c,
                                    PersonContactId,
                                    PersonEmail,
                                    fxAccount__c,
                                    Primary_Division_Name__c 
                            FROM    Account 
                            WHERE   PersonEmail IN: emailSet]) {
            emailAccountMap.put(acc.PersonEmail, acc);
        }
        for(LiveChatTranscript lct : chatIncompleteList) {
            Account accountObj = emailAccountMap.get(lct.Chat_Email__c);
            if(accountObj != null) { 
                emailSet.remove(lct.Chat_Email__c);
                completeChatInfoFromAccount(lct, accountObj);
                //...
            } else {
                lct.Is_HVC__c = OmnichanelConst.CHAT_IS_HVC_NO;
                result.add(lct);
            }
        }
        return result;
    }

    @testVisible
    void completeChatInfoFromAccount(LiveChatTranscript lct, Account accountObj) {
        if (String.isBlank(lct.AccountId)) {
            lct.AccountId = accountObj.Id;
        }
        if (String.isBlank(lct.ContactId)) {
            lct.ContactId = accountObj.PersonContactId;
        }
        if(lct.Is_HVC__c == null) {
            lct.Is_HVC__c = (accountObj.Is_High_Value_Customer__c)
                ? OmnichanelConst.CHAT_IS_HVC_YES
                : OmnichanelConst.CHAT_IS_HVC_NO;
        }
        if (String.isBlank(lct.Chat_Division__c)) {
            lct.Chat_Division__c = 
                ServiceDivisionsManager.getInstance().getDivisionCode(
                    accountObj.Primary_Division_Name__c
                );
        }
    }

    @testVisible
    void completeInfoFromLead(
        List<LiveChatTranscript> chatIncompleteList,
        Set<String> emailSet
    ) {
        Map<String, ID> emailLeadIdMap = new Map<String, ID>();
        for(Lead leadObj : [
            SELECT 
                Id,
                Email
            FROM
                Lead 
            WHERE   
                Email IN: emailSet
        ]) {
            emailLeadIdMap.put(leadObj.Email, leadObj.Id);
        }
        if (!emailLeadIdMap.isEmpty()){
            for(LiveChatTranscript lct : chatIncompleteList) {
                ID leadID = emailLeadIdMap.get(lct.Chat_Email__c);
                if(leadID != null) { 
                    lct.LeadId = leadID;
                } 
            }
        }
    }

}