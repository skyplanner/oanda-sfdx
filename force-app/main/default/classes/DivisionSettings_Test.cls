/**
 * @File Name          : DivisionSettings_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10/23/2019, 12:29:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/23/2019   dmorales     Initial Version
**/
@isTest
private  class DivisionSettings_Test {
    @isTest
    static void getDefaultInstance() {
        Test.startTest();
		DivisionSettings result = DivisionSettings.getInstance('');
		Test.stopTest();
		System.assert(result != null);
    }

     @isTest
    static void getSpecificInstance() {
        Test.startTest();
		DivisionSettings result = DivisionSettings.getInstance('OCAN');
		Test.stopTest();
		System.assert(result != null);
    }

      @isTest
    static void getFailInstance() {
        Test.startTest();
		DivisionSettings result = DivisionSettings.getInstance('No Division');
		Test.stopTest();
		System.assert(result.initializationFails == true);
    }
}