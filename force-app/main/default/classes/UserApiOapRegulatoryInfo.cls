/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 06-23-2022
 * @last modified by  : Dianelys Velazquez
**/
public with sharing class UserApiOapRegulatoryInfo {
    public String experience_details {get; set;}

    public Integer experience_education {get; set;}

    public Integer experience_employed {get; set;}

    public Integer experience_qualified {get; set;}

    public String has_traded {get; set;}

    public String singapore_dependent {get; set;}

    public String singapore_resident {get; set;}
}