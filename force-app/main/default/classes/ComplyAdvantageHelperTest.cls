/**
 * @author Fernando @SkyPlanner
 * @since 12/18/2020
 */
@isTest
private class ComplyAdvantageHelperTest {
	/**
	 * public static void validate(
	 *		Affiliate__c oldAffiliate, Affiliate__c newAffiliate)
	 */
	@isTest
	static void validate_4() {
		Affiliate__c affilliate;
		Comply_Advantage_Instance_Setting__c instance;
		Comply_Advantage_Division_Setting__c setting;
		
		insert new Settings__c(
			Name = 'Default',
			Comply_Advantage_Enabled__c = true
		);

		// we need a new instance
		instance = new Comply_Advantage_Instance_Setting__c(
			Name = 'APAC',
			API_Key__c = 'Yeoq64g73X977NYV3F1TDxSrt99K1lqv'
		);
		insert instance;

		// we need a division setting
		setting = new Comply_Advantage_Division_Setting__c(
			Name = 'OANDA Global Markets',
			New_Seach_Triggered_On_Objects__c = 'Affiliate__c',
			Region__c = 'OGM',
			Comply_Advantage_Instance_Setting__c = instance.Id
		);
		insert setting;

		// we need an Affiliate
		affilliate = new Affiliate__c(
			Username__c = 'username',
			Firstname__c = 'John',
			LastName__c = 'Doe',
			Company__c = 'Oanda Corp',
			Division_Name__c = 'OANDA Global Markets',
			Country__c = 'Macao',
			Alias__c = 'doer',
			Email__c = 'email@email.com'
		);
		insert affilliate;

		System.assertEquals(1, [SELECT COUNT() FROM Case]);
	}

	@isTest
	static void updatefxAccountMatchStatusOnInsertTest() {
		
		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);
	
		// comply adv is disabled, so no problem
		fxAccount__c fxAcc1 = new fxAccount__c(
			Account_Email__c = 'test1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = true
		);
		insert fxAcc1;
	
		// ca searches
		List<Comply_Advantage_Search__c> searches = new List<Comply_Advantage_Search__c> {
			new Comply_Advantage_Search__c(
				Name = 'Sample',
				fxAccount__c = fxAcc1.Id,
				Search_Id__c = '478009745',
				Is_Monitored__c = true,
				Match_Status__c= 'potential_match'
			)
		};
		searches.add(new Comply_Advantage_Search__c(
				Name = 'Sample',
				Search_Id__c = '478009746',
				Is_Monitored__c = true,
				Match_Status__c= 'potential_match'
		));
		insert searches;

		fxAccount__c fxAccAfter = [SELECT Id, CA_Match_Status__c FROM fxAccount__c WHERE Id = :fxAcc1.Id];
		System.assertEquals('potential_match', fxAccAfter.CA_Match_Status__c);


		searches[0].Match_Status__c='true_positive';
		searches[0].Match_Status_Override_Reason__c='No reason.';
		update searches;

		fxAccAfter = [SELECT Id, CA_Match_Status__c FROM fxAccount__c WHERE Id = :fxAcc1.Id];
		System.assertEquals('potential_match', fxAccAfter.CA_Match_Status__c);
	}


	@isTest
	static void updatefxAccountMatchStatusOnUpdateTest() {
		
		List<Account> accs = (new TestDataFactory()).createTestAccounts(1);
	
		// comply adv is disabled, so no problem
		fxAccount__c fxAcc1 = new fxAccount__c(
			Account_Email__c = 'test1@oanda.com',
			Funnel_Stage__c = FunnelStatus.TRADED,
			RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE,
			Division_Name__c = 'OANDA Canada',
			Birthdate__c = date.newInstance(1980, 11, 21),
			Citizenship_Nationality__c = 'Canada',
			Account__c = accs[0].Id,
			Is_Closed__c = true
		);
		insert fxAcc1;
	
		// ca searches
		List<Comply_Advantage_Search__c> searches = new List<Comply_Advantage_Search__c> {
			new Comply_Advantage_Search__c(
				Name = 'Sample',
				fxAccount__c = fxAcc1.Id,
				Search_Id__c = '478009745',
				Is_Monitored__c = true
			)
		};
		insert searches;

		fxAccount__c fxAccAfter = [SELECT Id, CA_Match_Status__c FROM fxAccount__c WHERE Id = :fxAcc1.Id];
		System.assertEquals(null, fxAccAfter.CA_Match_Status__c);

		searches[0].Match_Status__c='potential_match';
		searches[0].Match_Status_Override_Reason__c='No reason.';
		update searches;

		fxAccAfter = [SELECT Id, CA_Match_Status__c FROM fxAccount__c WHERE Id = :fxAcc1.Id];
		System.assertEquals('potential_match', fxAccAfter.CA_Match_Status__c);
	}


}