/**
 * @File Name          : GetHvcCasesCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/20/2021, 3:21:07 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2021, 12:47:55 PM   acantero     Initial Version
**/
@IsTest
private without sharing class GetHvcCasesCmd_Test {

	@testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createTestAccounts();
        OmnichanelRoutingTestDataFactory.createTestLeads();
    }

    @isTest
    static void getHvcCases() {
        Map<String, ID> accountMap = OmnichanelRoutingTestDataFactory.getAccountMap();
        Map<String, ID> leadMap = OmnichanelRoutingTestDataFactory.getLeadMap();
        CaseRoutingManager instance = CaseRoutingManager.getInstance();
        Set<ID> originalCasesQueueIdSet = CaseRoutingManager.getOriginalCaseQueueIdSet();
        String originalCasesQueueId = new List<ID>(originalCasesQueueIdSet)[0];
        List<Case> caseList = new List<Case>();
        //simulating chat case
        Case hvcChatCase1 = new Case(
            Chat_Is_HVC__c = OmnichanelConst.CHAT_IS_HVC_YES,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY,
            Origin = OmnichanelConst.CASE_ORIGIN_CHAT
        );
        caseList.add(hvcChatCase1);
        //...
        //simulating neuraflash chat case
        Case hvcChatCase2 = new Case(
            Chat_Email__c = OmnichanelRoutingTestDataFactory.HVC_EMAIL,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY,
            Origin = OmnichanelConst.CASE_ORIGIN_CHATBOT_CASE
        );
        caseList.add(hvcChatCase2);
        //...
        //Non HVC Case
        Case nonHvcCase = new Case(
            Chat_Email__c = OmnichanelRoutingTestDataFactory.ACTIVE_CUSTOMER_EMAIL,
            Subject = 'Active Customer Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_THIRD_PARTY
        );
        System.debug('nonHvcCase.Is_HVC__c: ' + nonHvcCase.Is_HVC__c);
        caseList.add(nonHvcCase);
        //...
        //HVC case with account
        ID hvcId = accountMap.get(OmnichanelRoutingTestDataFactory.HVC_EMAIL);
        Case hvcCaseAccount = new Case(
            AccountId = hvcId,
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_TRADE,
            OwnerId = originalCasesQueueId,
            Origin = OmnichanelConst.CASE_ORIGIN_EMAIL_FRONTDESK
        );
        caseList.add(hvcCaseAccount);
        //...
        //Lead Case
        ID newCustomerLeadId = leadMap.get(OmnichanelRoutingTestDataFactory.LEAD_NEW_CUSTOMER_EMAIL);
        Case newCustomerLeadCase = new Case(
            Lead__c = newCustomerLeadId,
            Subject = 'New Customer Lead Case',
            Inquiry_Nature__c = OmnichanelConst.INQUIRY_NAT_OTHER
        );
        caseList.add(newCustomerLeadCase);
        //...
        Test.startTest();
        List<Case> result = GetHvcCasesCmd.getHvcCases(caseList);
        Test.stopTest(); 
        System.debug('hvcCases');
        for(Case caseObj: result) {
            System.debug(caseObj);
        }
        System.assertEquals(3, result.size());
    }
    
}