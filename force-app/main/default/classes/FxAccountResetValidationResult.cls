/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 08-17-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   08-17-2020   dmorales   Initial Version
**/
public class FxAccountResetValidationResult {
    @InvocableMethod(label='fxAccount resetValidationResult' description='')
    public static void resetValidationResult(List<Id> fxAccountIdList) {
        MiFIDExpValSettings valSettings = MiFIDExpValSettings.getRequiredDefault();
        if (valSettings.isActive) {
            FxAccountMIFIDValidationManager.resetValidationResult(fxAccountIdList);
        }
    }
}