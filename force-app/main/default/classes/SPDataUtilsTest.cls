/**
 * @File Name          : SPDataUtilsTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 12/20/2023, 3:20:28 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/20/2023, 3:12:03 PM   aniubo     Initial Version
 **/
@isTest
private class SPDataUtilsTest {
	@IsTest
	static void insertIfNoTesting() {
		Test.startTest();
		Boolean result = SPDataUtils.insertIfNoTesting(getDummyObject());
		Test.stopTest();
		System.assertEquals(false, result, 'Invalid result');
	}

	@IsTest
	static void insertIfCondition() {
		Test.startTest();
		Boolean result = SPDataUtils.insertIfCondition(getDummyObject(), true);
		Test.stopTest();
		System.assertEquals(true, result, 'Invalid result');
	}

	@IsTest
	static void insertIfNoTesting2() {
		Test.startTest();
		Boolean result = SPDataUtils.insertIfNoTesting(
			new List<SObject>{ getDummyObject() }
		);
		Test.stopTest();
		System.assertEquals(false, result, 'Invalid result');
	}

	@IsTest
	static void insertIfCondition2() {
		Test.startTest();
		Boolean result = SPDataUtils.insertIfCondition(
			new List<SObject>{ getDummyObject() },
			true
		);
		Test.stopTest();
		System.assertEquals(true, result, 'Invalid result');
	}

	@IsTest
	static void updateIfNoTesting() {
		Test.startTest();
		Boolean result = SPDataUtils.updateIfNoTesting(getDummyObject());
		Test.stopTest();
		System.assertEquals(false, result, 'Invalid result');
	}

	@IsTest
	static void updateIfCondition() {
		Test.startTest();
		SObject dummy = getDummyObject();
		insert dummy;
		Boolean result = SPDataUtils.updateIfCondition(dummy, true);
		Test.stopTest();
		System.assertEquals(true, result, 'Invalid result');
	}

	@IsTest
	static void deleteIfNoTesting() {
		SObject dummyObj = getDummyObject();
		insert dummyObj;
		Test.startTest();
		Boolean result = SPDataUtils.deleteIfNoTesting(
			new List<SObject>{ dummyObj }
		);
		Test.stopTest();
		System.assertEquals(false, result, 'Invalid result');
	}

	@IsTest
	static void deleteIfCondition() {
		SObject dummyObj = getDummyObject();
		insert dummyObj;
		Test.startTest();
		Boolean result = SPDataUtils.deleteIfCondition(
			new List<SObject>{ dummyObj }, // records
			true // condition
		);
		Test.stopTest();
		System.assertEquals(true, result, 'Invalid result');
	}

	public static SObject getDummyObject() {
		return new Account(Name = 'abcd');
	}
}