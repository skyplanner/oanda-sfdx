/**
 * @File Name          : ServiceDivisionsManager.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2024, 5:40:06 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/14/2021, 11:28:54 AM   acantero     Initial Version
 * 2.0 plese move with new logic to DivisionSettingsUtil with higher API. Due to multiple references new class were created
**/
public with sharing class ServiceDivisionsManager {

    static ServiceDivisionsManager instance;

    @testVisible
    Map<String,ServiceDivisionInfo> mapByCodes;
    @testVisible
    Map<String,ServiceDivisionInfo> mapByNames;

    ServiceDivisionsManager() {
        this.mapByCodes = new Map<String,ServiceDivisionInfo>();
        this.mapByNames = new Map<String,ServiceDivisionInfo>();
    }

    public static ServiceDivisionsManager getInstance() {
        if (instance == null) {
            instance = new ServiceDivisionsManager();
        }
        return instance;
    }

    public String getDivisionName(String divisionCode) {
        ServiceDivisionInfo divisionInfo = getDivisionByCode(divisionCode);
        String result = divisionInfo?.divisionName;
        return (String.isNotBlank(result))
            ? result
            : divisionCode;
    }

    public String getDivisionCode(String divisionName) {
        ServiceDivisionInfo divisionInfo = getDivisionByName(divisionName);
        String result = divisionInfo?.divisionCode;
        return (String.isNotBlank(result))
            ? result
            : divisionName;
    }

    /**
     * From a parameter that can be the code or can be the name, it returns
     * the corresponding code
     */
    public String getValidDivisionCode(String divisionStr) {
        ServiceDivisionInfo divisionInfo = getDivisionByCode(divisionStr);

        if (divisionInfo == null) {
            divisionInfo = getDivisionByName(divisionStr);
        }

        String result = divisionInfo?.divisionCode;
        return (String.isNotBlank(result))
            ? result
            : divisionStr;
    }

    public String getDivisionSkillDevName(String divisionStr) {
        ServiceDivisionInfo divisionInfo = getDivisionByCode(divisionStr);
        if (divisionInfo == null) {
            divisionInfo = getDivisionByName(divisionStr);
        }
        return divisionInfo?.skillDevName;
    }

    public ServiceDivisionInfo getDivisionByCode(String divisionCode) {
        if (String.isBlank(divisionCode)) {
            return null;
        }
        //else...
        ServiceDivisionInfo result = mapByCodes.get(divisionCode);
        if (result == null) {
            result = getByDivisionCode(divisionCode);
        }
        return result;
    }

    public ServiceDivisionInfo getDivisionByName(String divisionName) {
        if (String.isBlank(divisionName)) {
            return null;
        }
        //else...
        ServiceDivisionInfo result = mapByNames.get(divisionName);
        if (result == null) {
            result = getByDivisionName(divisionName);
        }
        return result;
    }

    @testVisible
    ServiceDivisionInfo getByDivisionCode(String divisionCode) {
        List<Division_Settings__mdt> divisionList = [
            SELECT 
                DeveloperName,
                Division_Name__c,
                Skill_Dev_Name__c
            FROM 
                Division_Settings__mdt 
            WHERE 
                DeveloperName = :divisionCode
        ];
        return getValidResult(divisionList, null);
    }

    @testVisible
    ServiceDivisionInfo getByDivisionName(String divisionName) {
        List<Division_Settings__mdt> divisionList = 
            getByDivisionName1(divisionName);
        if (divisionList.isEmpty()) {
            divisionList = getByDivisionName2(divisionName);
            if (divisionList.isEmpty()) {
                divisionList = getByDivisionName3(divisionName);
            }
        }
        return getValidResult(divisionList, divisionName);
    }

    @testVisible
    List<Division_Settings__mdt> getByDivisionName1(String divisionName) {
        return [
            SELECT 
                DeveloperName,
                Division_Name__c,
                Skill_Dev_Name__c
            FROM 
                Division_Settings__mdt 
            WHERE 
                Division_Name__c = :divisionName
        ];
    }

    @testVisible
    List<Division_Settings__mdt> getByDivisionName2(String divisionName) {
        return [
            SELECT 
                DeveloperName,
                Division_Name__c,
                Skill_Dev_Name__c
            FROM 
                Division_Settings__mdt 
            WHERE 
                Division_Name_2__c = :divisionName
        ];
    }

    @testVisible
    List<Division_Settings__mdt> getByDivisionName3(String divisionName) {
        return [
            SELECT 
                DeveloperName,
                Division_Name__c,
                Skill_Dev_Name__c
            FROM 
                Division_Settings__mdt 
            WHERE 
                Division_Name_3__c = :divisionName
        ];
    }

    @testVisible
    ServiceDivisionInfo getValidResult(
        List<Division_Settings__mdt> divisionList,
        String divisionName
    ) {
        ServiceDivisionInfo result = null;
        if (!divisionList.isEmpty()) {
            Division_Settings__mdt divisionObj = divisionList[0];
            String validDivisionName = getValidDivisionName(
                divisionName, 
                divisionObj.Division_Name__c
            );
            result = new ServiceDivisionInfo(
                divisionObj.DeveloperName, // divisionCode
                validDivisionName, // divisionName
                divisionObj.Skill_Dev_Name__c // skillDevName
            );
            mapByCodes.put(result.divisionCode, result);
            mapByCodes.put(result.divisionName, result);
        }
        return result;
    }

    @testVisible
    String getValidDivisionName(
        String divisionName,
        String recDivisionName
    ) {
        return (String.isNotBlank(divisionName))
            ? divisionName
            : recDivisionName;
    }

    public class ServiceDivisionInfo  {

        public String divisionCode {get; private set;}
        public String divisionName {get; private set;}
        public String skillDevName {get; private set;}

        public ServiceDivisionInfo(
            String divisionCode,
            String divisionName,
            String skillDevName
        ) {
            this.divisionCode = divisionCode;
            this.divisionName = divisionName;
            this.skillDevName = skillDevName;
        }

    }

}