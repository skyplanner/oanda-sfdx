@RestResource(urlMapping='/api/v1/users/Documents/verify')
global without sharing class CRMDocumentVerificationRestResource 
{
    @HttpPut
    global static void verifyCRMMigration() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        try
        {  
            String reqBody = req.requestBody.toString().trim();
           
            CRMDocumentVerificationRequest documentVerificationRequest = (CRMDocumentVerificationRequest)JSON.deserialize(reqBody, CRMDocumentVerificationRequest.class);

            Map<Id, list<string>> user_crm_documents_map =  documentVerificationRequest.user_crm_documents_map;
            
            set<Id> fxaccountIds = user_crm_documents_map.keySet();

            if(!fxaccountIds.isEmpty())
            {
                fxAccount__c[] fxaccounts = new fxAccount__c[]{};

                Map<Id, set<string>> fxacIdDocsMap = getDocumentsByFxaccount(fxaccountIds);

                Map<Id, set<string>> fxacIdErrorDocsMap = getErrorDocumentsByFxaccount(fxaccountIds);

                system.debug('fxacIdDocsMap' + fxacIdDocsMap);
                system.debug('fxacIdErrorDocsMap' + fxacIdErrorDocsMap);

                for(Id fxacId : fxaccountIds)
                {
                    list<string> crmDocs = user_crm_documents_map.get(fxacId);
                    set<string> sfDocs = fxacIdDocsMap.get(fxacId);
                    set<string> sfErrorDocs = fxacIdErrorDocsMap.get(fxacId);
                    
                    if(sfDocs == null) sfDocs = new set<string>{};
                    if(sfErrorDocs == null) sfErrorDocs = new set<string>{};

                    if(crmDocs.isEmpty())
                    {
                        fxAccount__c fxac = new fxAccount__c(Id=fxacId, Document_Migration_Status__c = 'Verified_Success_No_Files');
                        fxaccounts.add(fxac);
                    }
                    else if(!crmDocs.isEmpty())
                    {
                        if(sfDocs.containsAll(crmDocs))
                        {
                            fxAccount__c fxac = new fxAccount__c(Id=fxacId, Document_Migration_Status__c = 'Verified_Success');
                            fxaccounts.add(fxac);
                        }
                        else
                        {  
                            string documentMigrationStatus = 'Verified_Success_Error_Notes';

                            for(string crmDocName : crmDocs)    
                            {
                                if(sfDocs.contains(crmDocName) || sfErrorDocs.contains(crmDocName))
                                {
                                    continue;
                                }
                                else
                                {
                                    documentMigrationStatus = 'Verified_Migration_Has_Errors';
                                }
                            }
                            fxAccount__c fxac = new fxAccount__c(Id=fxacId, Document_Migration_Status__c = documentMigrationStatus);
                            fxaccounts.add(fxac);
                        } 
                    }
                }

                Database.update(fxaccounts, false);
            }
        }
        catch(Exception ex)
        {
            res.addHeader('Content-Type', 'application/json');
            string errorInfo = ex.getMessage() + '\n' + ex.getStackTraceString();
            res.responseBody = Blob.valueOf('{ "stack trace" : '+errorInfo+'}');
            res.statusCode = 500;
        }
    }
    public static Map<Id, set<string>> getErrorDocumentsByFxaccount(set<Id> fxaccountIds)
    {
        Map<Id, set<string>> fxacIdAttachmentsNameMap = new Map<Id, set<string>>{};

        Map<Id, Id> docIdFxAccIdMap = new Map<Id, Id>{};

        set<Id> crmDocumentIds = new  set<Id>();

        for(Document__c doc : [Select Id, fxAccount__c From Document__c Where Name='CRM Migration Documents' AND fxAccount__c IN :fxaccountIds])
        {
            docIdFxAccIdMap.put(doc.Id, doc.fxAccount__c);
            crmDocumentIds.add(doc.Id);
        }

        for(Note note :  [SELECT Id, ParentId, Title FROM Note WHERE ParentId IN :crmDocumentIds AND Title Like '%Failed :%'])
        {
            String attachmentName = note.Title.substring(8);
            Id fxAcId = docIdFxAccIdMap.get(note.ParentId);
            
            set<string> attachmentNames = fxacIdAttachmentsNameMap.get(fxAcId);
            if(attachmentNames == null)
            {
                attachmentNames = new set<string>{};
            }
            attachmentNames.add(attachmentName);
            fxacIdAttachmentsNameMap.put(fxAcId, attachmentNames);
        }

        return fxacIdAttachmentsNameMap;
    }
    private static Map<Id, set<string>> getDocumentsByFxaccount(set<Id> fxaccountIds)
    {
        Map<Id, set<string>> fxacIdAttachmentsNameMap = new Map<Id, set<string>>{};
        Map<Id, string> docIdFxacIdMap = new Map<Id, string>{};
        set<Id> documentParentIds = new set<Id>{};
        Map<Id, Id> fxAccountRelatedRecordsMap = new Map<Id, Id>{};

        for(fxAccount__c fxa : [select Id,
                                       Name,
                                       Account__c,
                                       (select Id From Cases__r)
                                from fxAccount__c
                                where Id IN :fxaccountIds])
        {
            fxacIdAttachmentsNameMap.put(fxa.Id, new set<string>{});
            
            documentParentIds.add(fxa.Id);

            if(fxa.Account__c != null)
            {
                documentParentIds.add(fxa.Account__c);
                fxAccountRelatedRecordsMap.put(fxa.Account__c, fxa.Id);
            }
            if(fxa.Cases__r != null && fxa.Cases__r.size() > 0)
            {
                for(Case ca : fxa.Cases__r)
                {
                    documentParentIds.add(ca.Id);
                    fxAccountRelatedRecordsMap.put(ca.Id, fxa.Id);
                }
            }
        }

        set<Id> documentIds = new set<Id>{};
        for(Document__c doc : [select Id, 
                                      Name,
                                      Account__c,
                                      fxAccount__c,
                                      Case__c
                               From Document__c 
                               Where Account__C IN :documentParentIds OR 
                                     fxAccount__c IN :documentParentIds OR 
                                     Case__c IN :documentParentIds])
        {
            Id fxacId;
            if(string.isNotBlank(doc.fxAccount__c))
            {
                fxacId = doc.fxAccount__c;               
            }
            else if(string.isNotBlank(doc.Account__c) && string.isBlank(fxacId))
            {
                fxacId = fxAccountRelatedRecordsMap.get(doc.Account__c);
            }
            else if(string.isNotBlank(doc.Case__c) && string.isBlank(fxacId))
            {
                fxacId = fxAccountRelatedRecordsMap.get(doc.Case__c);
            }

            if(string.isNotBlank(fxacId))
            {
                docIdFxacIdMap.put(doc.Id, fxacId);
                documentIds.add(doc.Id);
            }
        }
       
        if(!documentIds.isEmpty())
        {
            for(Attachment attachment : [SELECT Id, Name, ParentId 
                                        FROM Attachment 
                                        WHERE ParentId IN :documentIds])
            {
                Id fxacId = docIdFxacIdMap.get(attachment.ParentId);

                if(string.isNotBlank(fxacId))
                {
                    set<string> attachmentNames = fxacIdAttachmentsNameMap.get(fxacId);
                    
                    if(attachmentNames == null)
                    {
                        attachmentNames = new set<string>{};
                    }

                    attachmentNames.add(attachment.Name);

                    fxacIdAttachmentsNameMap.put(fxacId, attachmentNames);
                }
            }

            for(ContentDocumentLink attachment : [SELECT ContentDocument.title,ContentDocument.FileExtension, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :documentIds])
            {
                if(attachment.ContentDocument != null)
                {
                    string fxacId = docIdFxacIdMap.get(attachment.LinkedEntityId);
                    if(string.isNotBlank(fxacId))
                    {
                        set<string> attachmentNames = fxacIdAttachmentsNameMap.get(fxacId);
                        if(attachmentNames == null)
                        {
                            attachmentNames = new set<string>{};
                        }

                        attachmentNames.add(attachment.ContentDocument.title);
                        attachmentNames.add(attachment.ContentDocument.title + '.' + attachment.ContentDocument.FileExtension);
                        
                        fxacIdAttachmentsNameMap.put(fxacId, attachmentNames);
                    }
                }
            }
        }
        return fxacIdAttachmentsNameMap;
    }

    Class CRMDocumentVerificationRequest
    {
        Map<Id, String[]> user_crm_documents_map;
    }
    
}