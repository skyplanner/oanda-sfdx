/**
 * @File Name          : CheckMessagingLanguageActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 10:49:45 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/18/2023, 11:02:03 AM   aniubo     Initial Version
 **/
@isTest
private class CheckMessagingLanguageActionTest {
	@isTest
	private static void testLanguageCodesIsNullOrEmpty() {
		// Test data setup

		// Actual test
		Test.startTest();
		List<Boolean> result = CheckMessagingLanguageAction.languageIsAvailable(
			null
		);
		Assert.areEqual(
			false,
			result.get(0),
			'Language is not available because language code is null'
		);
		result = CheckMessagingLanguageAction.languageIsAvailable(
			new List<String>()
		);
		Assert.areEqual(
			false,
			result.get(0),
			'Language is not available because language code is empty'
		);
		Test.stopTest();

		// Asserts
	}
	@isTest
	private static void testLanguageIsAvailableThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			List<Boolean> result = CheckMessagingLanguageAction.languageIsAvailable(
				null
			);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been threw');
	}

	@IsTest
	static void testLanguageIsAvailable() {
		List<String> languageCodes = new List<String>{ 'de' };
		Test.startTest();
		List<Boolean> result = CheckMessagingLanguageAction.languageIsAvailable(
			languageCodes
		);
		Test.stopTest();
		Service_Language__mdt serviceLanguage = ServiceLanguagesManager.getLanguageRecord(
			languageCodes.get(0)
		);
		Boolean isAvailable = serviceLanguage?.Available_for_Chatbot__c;

		Assert.areEqual(
			isAvailable,
			result.get(0),
			isAvailable ? 'Language is available' : 'Language is not available '
		);
	}
}