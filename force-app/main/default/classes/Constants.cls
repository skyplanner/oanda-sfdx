/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-01-2024
 * @last modified by  : SkyPlanner - Dianelys Velazquez
**/
public with sharing class Constants 
{
    //Divisions
    public static final String DIVISIONS_OANDA_CORPORATION = 'OANDA Corporation';
    public static final String DIVISIONS_OANDA_CORPORATION_INTERNAL = 'OANDA Corporation Internal';
    public static final String DIVISIONS_OANDA_CANADA = 'OANDA Canada';
    public static final String DIVISIONS_OANDA_ASIA_PACIFIC = 'OANDA Asia Pacific';
    public static final String DIVISIONS_OANDA_ASIA_PACIFIC_CFD = 'OANDA Asia Pacific CFD';
    public static final String DIVISIONS_OANDA_EUROPE = 'OANDA Europe';
    public static final String DIVISIONS_OANDA_EUROPE_AB = 'OEL';
    public static final String DIVISIONS_OANDA_CORPORATE_ECP = 'OANDA Corporate ECP';
    public static final String DIVISIONS_OANDA_JAPAN = 'OANDA Japan';
    public static final String DIVISIONS_OANDA_JAPAN_CORPORATE = 'OANDA Japan Corporate';
    public static final String DIVISIONS_OANDA_AUSTRALIA = 'OANDA Australia';
    public static final String DIVISIONS_OANDA_EUROPE_MARKETS = 'OANDA Europe Markets';
    public static final String DIVISIONS_OANDA_MARKETS_EUROPA_AB = 'OEM';
    public static final String DIVISIONS_OANDA_GLOBAL_MARKETS = 'OANDA Global Markets';

    public static final String CA_ALL_DIVISIONS_BANKING_PARTNERS = 'All Division - Banking Partners';
    public static final String CA_ALL_DIVISIONS_VENDORS = 'All Division - Vendors';

    // Assi divisions list
    public static final List<String> DIVISIONS_ASIA_LIST =
        new List<String>{
            DIVISIONS_OANDA_ASIA_PACIFIC,
            DIVISIONS_OANDA_ASIA_PACIFIC_CFD};

    //Customer risk assessment
    public static final String HIGH_RISK = 'High';
    public static final String MEDIUM_RISK = 'Medium';

    public static final String CASE_ONBOARDING_RECTYP = 'Registration';
    public static final String FX_ACCOUNT_TYPE_INDIVIDUAL = 'Individual';

    //Funnel stages
    public static final String FUNNEL_BEFORE_YOU_BEGIN = 'Before You Begin';
    public static final String FUNNEL_AWAIT_CLIENT_INFO = 'Awaiting Client Info';
    public static final String FUNNEL_DOCS_UPLOADED = 'Documents Uploaded';
    public static final String FUNNEL_MORE_INFO_REQUIRED = 'More Info Required';
    public static final String FUNNEL_RFF = 'Ready For Funding';
    public static final String FUNNEL_FUNDED = 'Funded';
    public static final String FUNNEL_TRADED = 'Traded';
    public static final String CRYPTO_FUNNEL_PENDING = 'Pending';
    public static final String CRYPTO_FUNNEL_ERROR = 'Error';
    public static final String CRYPTO_FUNNEL_REJECTED_BY_PAXOS = 'Rejected by Paxos';
    public static final String CRYPTO_FUNNEL_DISABLED_BY_PAXOS = 'Disabled by Paxos';
    public static final String CRYPTO_FUNNEL_CLOSED = 'Closed';

    //Paxos status
    public static final String PAXOS_STATUS_PENDING = 'Pending';
    public static final String PAXOS_STATUS_ERROR = 'Error';
    public static final String PAXOS_STATUS_DENIED = 'Denied';
    public static final String PAXOS_STATUS_APPROVED = 'Approved';
    public static final String PAXOS_STATUS_DISABLED = 'Disabled';

    //IHS document status
    public static final String IHS_DOCUMENT_STATUS_READY = 'Ready';
    public static final String IHS_DOCUMENT_STATUS_SENDING_TO_IHS_INITIATED = 'Sending to IHS Initiated';
    public static final String IHS_DOCUMENT_STATUS_SENT_TO_IHS_SUCCESSFULLY = 'Sent to IHS successfully';
    public static final String IHS_DOCUMENT_STATUS_ERROR = 'Error';

    //Application Form Funnel stage
    public static final string APP_FUNNEL_APPLICATION_COMPLETED = 'Application Completed';
    public static final string APP_FUNNEL_THANK_YOU = 'Thank You';
    public static final string APP_FUNNEL_CORPORATE_COMPLETED = 'Corporate Completed';

    public static final String OANDA_DOMAIN = 'oanda.com';

    public static final String DOC_TYPE_DRIVER_LICENSE = 'Driver\'s License'; 
    
    // Logger categories
    public static final String LOGGER_GENERAL_CATEGORY = 'General';
    public static final String LOGGER_CURRENT_DESK_CATEGORY = 'Current-Desk';
    public static final String LOGGER_SHARED_SERVICES_CATEGORY = 'Shared-Services';
    public static final String LOGGER_COMPLY_ADVANTAGE_CATEGORY = 'Comply-Advantage';
    public static final String LOGGER_LEGAL_AGREEMENT_CATEGORY = 'Legal-Agreement';
    public static final String LOGGER_COMPLIANCE_RECORD_CATEGORY = 'Compliance-Record';
    public static final String LOGGER_USER_API_CATEGORY = 'User-API-App';
    public static final String LOGGER_ONBOARDING_AMP_FOLLOWUP_CATEGORY = 'Onboarding-AMP-Followup';
    public static final String LOGGER_CRYPTO_CATEGORY = 'Crypto';
    public static final String LOGGER_OPTIMOVE_API_CATEGORY = 'Optimove-API-Integration';
    public static final String LOGGER_GCP_API_CATEGORY = 'GCP-API-Integration';

    public static final String CASE_LIVE = 'Live';
    public static final String CASE_PRACTICE = 'Practice';    
    public static final String CASE_TYPE_AMP_FOLLOWUP = 'AMP follow up';
    public static final String CASE_TYPE_TAX_DECLARATION = 'TAX declaration';
    public static final String CASE_TYPE_DEPOSIT = 'Deposit';
    public static final String CASE_TYPE_GENERAL_ACCOUNT_QUESTIONS = 'General Account Questions';
    public static final String CASE_INQUIRY_NATURE_REGISTRATION = 'Registration';
    public static final String CASE_INQUIRY_NATURE_ACCOUNT = 'Account';
    public static final String CASE_INQUIRY_NATURE_TRADE = 'Trade';
    public static final String CASE_INQUIRY_NATURE_LOGIN = 'Login';
    public static final String CASE_ORIGIN_INTERNAL = 'Internal';
    public static final String CASE_ORIGIN_CHAT = 'Chat';
    public static final String CASE_APP_STATUS_SENT_BACK_TO_MAKER = 'Sent back to maker';
    public final static String CASE_STATUS_OPEN = 'Open';
    public final static String CASE_STATUS_RE_OPENED = 'Re-opened';
    public final static String CASE_STATUS_PENDING = 'Pending';
    public final static String CASE_STATUS_CLOSED = 'Closed';
    public final static String CASE_STATUS_WAITING_ON_INTERNAL = 'Waiting on Internal';
    public final static String CASE_STATUS_WAITING_ON_DATE = 'Waiting on Date';
    public final static String CASE_SUBTYPE_OTHER_UNKNOWN = 'Other/Unknown';
    public final static String CASE_SUBJECT_OC_HIGH_RISK_REVIEW_DUE = 'OC High Risk Review Due';


    public static final String COUNTRY_UNITED_STATES = 'United States';
    
    public static final String CRYPTO_ACCOUNT_CLOSE_ACTION = 'crypto-account-close-action';
    public static final String CRYPTO_ACCOUNT_ENABLE_ACTION = 'crypto-account-enable-action';
    public static final String CRYPTO_ACCOUNT_LOCK_ACTION = 'crypto-account-lock-action';

    public static final String USER_API_SEARCH_ENDPOINT = 'User_API_Search_Endpoint';
    public static final String TAS_API_WRAPPER_SETTING_NAME = 'TAS_API';
    public static final String TAS_API_GROUPS_ENDPOINT = 'TAS_API_Groups_Endpoint';
    public static final String TAS_API_MT4_SERVERS_ENDPOINT = 'TAS_API_MT4_Servers_Endpoint';
    public static final String TAS_API_RESEND_MT4_NOTIFICATION_ENDPOINT = 'TAS_API_Resend_MT4_Notification_Endpoint';
    public static final String TAS_API_USER_ACCOUNT_ENDPOINT = 'TAS_API_User_Account_Endpoint';
    public static final String TAS_API_USER_ACCOUNT_PATCH_ENDPOINT = 'TAS_API_User_Account_Patch_Endpoint';
    public static final String TAS_API_USER_V20_ACCOUNT_PATCH_ENDPOINT = 'TAS_API_User_V20Account_Patch_Endpoint';    
    public static final String TAS_API_V20_ACCOUNT_ENDPOINT = 'TAS_API_V20Account_Endpoint';
    public static final String TAS_API_GROUP_RECALCULATE_ENDPOINT = 'TAS_Api_Group_Recalculate_Endpoint';

    public static final String USER_API_WRAPPER_SETTING_NAME = 'Crypto_Mule_App';
    public static final String USER_API_GET_USER_ENDPOINT = 'User_API_Get_Endpoint';
    public static final String USER_API_INTRODUCING_BROKER_ENDPOINT = 'User_API_Introducing_Broker_Endpoint';
    public static final String USER_API_PRIVILEGE_ENDPOINT = 'User_API_Privilege_Endpoint';
    public static final String USER_API_STATUS_ENDPOINT = 'User_API_Status_Endpoint';
    public static final String USER_API_GROUP_RECALCULATE_ENDPOINT = 'User_Api_Group_Recalculate_Endpoint';

    public static final String GROUP_RECALCULATION_ENDPOINT = 'Group_Recalculation_Trigger_Enpoint';

    public static final String TAS_API_MT5_ENDPOINT = 'TAS_Api_MT5_Account_Endpoint';
    public static final String TAS_API_MT5_LOCK_UNLOCK_ENDPOINT = 'TAS_Api_MT5_Account_Lock_Unlock_Endpoint';
    
    public static final String API_ENV = 'Crypto_Account_Env';
    public static final String USER_API_UPD_ADDRESS_ENDPOINT = 'User_API_Upd_Address_Endpoint';
    public static final String USER_API_CONTACT_PHONE_ENDPOINT = 'User_API_Contact_Phone_Endpoint';
    public static final String USER_API_GET_AUDIT_LOG_ENDPOINT = 'User_API_Get_Audit_Log_Endpoint';
    public static final String USER_API_GET_ONE_ID_BY_EMAIL_ENDPOINT = 'UserEmail_Endpoint';
    public static final String TAS_API_MT5_ACCOUNT_CLOSE_ONLY_ENDPOINT = 'TAS_Api_MT5_Account_Close_Only_Endpoint';

    public static final String OPTIMOVE_GET_ATTRIBUTES_ENDPOINT = 'Optimove_Api_Get_Attributes_Endpoint';
    public static final String OPTIMOVE_UPDATE_ATTRIBUTES_ENDPOINT = 'Optimove_Api_Update_Attributes_Endpoint';
    public static final String OPTIMOVE_PREFERENCES_ENDPOINT = 'Optimove_Api_Preferences_Endpoint';
    public static final String OPTIMOVE_GET_CHANNEL_INTERACTIONS_ENDPOINT = 'Optimove_Api_Channel_Interact_Endpoint';
    public static final String OPTIMOVE_GET_MARKETING_TEMPLATE_ENDPOINT = 'Optimove_Api_Marketing_Template_Endpoint';
    public static final String OPTIMOVE_GET_TRANSACTIONAL_TEMPLATE_ENDPOINT = 'Optimove_Api_Transact_Template_Endpoint';
    public static final String OPTIMOVE_CHECK_PREFERENCE_CHANNELS = 'Optimove_Check_Preference_Channels';
    public static final String OPTIMOVE_CALLBACK_SITE_NAME = 'Optimove_Callback_Site_Name';

    public static final String GCP_NOTIFY_EVENT11_ENDPOINT = 'GCP_Api_Event11_Endpoint';
    public static final String GCP_NOTIFY_EVENT13_ENDPOINT = 'GCP_Api_Event13_Endpoint';
    public static final String GCP_GET_COMMUNICATION_LOGS_ENDPOINT = 'GCP_Api_Get_Communication_Logs_Endpoint';
    public static final String GCP_NOTIFICATION_ATTEMPTS_LIMIT = 'GCP_Notification_Attempts_Limit';

    public static final String CURRENT_DESK_SETTINGS = 'Current_Desk_Settings';

    public static final String API_FIELD_ID = 'Id';
    public static final String API_FIELD_USER_ID = 'User_Id';
    public static final String API_FIELD_USERNAME = 'UserName';
    public static final String API_FIELD_EMAIL = 'Email';
    public static final String API_FIELD_DIVISION = 'Division';
    public static final String API_FIELD_BALANCE = 'Balance';
    public static final String API_FIELD_NAME = 'Name';
    public static final String API_FIELD_CITIZENSHIP = 'Citizenship';
    public static final String API_FIELD_TELEPHONE = 'Telephone';
    public static final String API_FIELD_API_ACCESS = 'ApiAccess';
    public static final String API_FIELD_PREMIUM_ACCESS = 'PremiumAccess';
    public static final String API_FIELD_NEW_POSITIONS_LOCKED = 'NewPositionsLocked';
    public static final String API_FIELD_CREATED_DATE = 'Date_Created';
    public static final String API_FIELD_ENV = 'Env';
    public static final String API_FIELD_PRICING_GROUP = 'PricingGroup';
    public static final String API_FIELD_COMMISSION_GROUP = 'CommissionGroup';
    public static final String API_FIELD_CURRENCY_CONVERSION_GROUP = 'CurrencyConversionGroup';
    public static final String API_FIELD_FINANCING_GROUP = 'FinancingGroup';
    public static final String API_FIELD_COUNTRY = 'Country';
    public static final String API_FIELD_REGION = 'Region';
    public static final String API_FIELD_CITY = 'City';
    public static final String API_FIELD_STREET1 = 'Street1';
    public static final String API_FIELD_STREET2 = 'Street2';
    public static final String API_FIELD_POSTAL_CODE = 'PostalCode';

    public static final String MT5_IS_DISABLED = 'isDisabled';

    public static final List<String> PRICING_GROUP_NUMBERS_TO_IGNORE = new List<String> {'1001','1002','1003'};

    public static final String ACCOUNT_OB_CHECK_AUTOMATED_REVIEW = 'Automated Review - Accepted';
    public static final String ACCOUNT_OB_CHECK_MANUAL_REVIEW = 'Automated Review - Manual Review Required';

    public static final String INSTANCE_NAME_STAGING = 'staging';
    public static final String INSTANCE_NAME_DEVELOPMENT = 'dev';

    public static final String CREDENTIAL_CONFIGURATION_NAME_STG = 'CredentialSwitchForStg';
    public static final String CREDENTIAL_CONFIGURATION_NAME_DEV = 'CredentialSwitchForDev';
    public static final String CREDENTIAL_CONFIGURATION_NAME_PROD = 'CredentialSwitchForProd';

    public static final Date DEFAULT_BIRTHDATE = Date.newInstance(1900, 1, 1);

    public static final String FXACCOUNT_LIVE_RT = 'Retail Live';
    public static final String FXACCOUNT_DEMO_RT = 'Retail Practice';
    public static final String LEAD_LIVE_RT = 'Retail Lead House Account';
    public static final String LEAD_DEMO_RT = 'Retail Practice Lead';
    
    public final static Map<String, String> DIVISION_NAME_MAPPING = new Map <String,String> {
        '1' => 'OANDA Corporation',
        '2' => 'OANDA Canada',
        '3' => 'OANDA Asia Pacific',
        '4' => 'OANDA Europe',
        '6' => 'OANDA Corporate ECP',
        '7' => 'OANDA Asia Pacific CFD',
        '8' => 'OANDA Corporation Internal',
        '9' => 'OANDA Japan',
        '10' => 'OANDA Japan Corporate',
        '11' => 'OANDA Australia',
        '12' => 'OANDA Europe Markets',
        '13' => 'OANDA Global Markets'
    };

    //in initial solution we get codes, now I think we have strings but keep this for reference
    public final static Map<String, String> EMPLOYMENT_STATUS_MAPPING = new Map <String,String> {
        '0' => 'Employed',
        '2' => 'Retired',
        '1' => 'Self Employed',
        '5' => 'Student',
        '3' => 'Unemployed'
    };

    public final static Map<String, String> REVERSED_DIVISION_NAME_MAPPING {get {
        if(REVERSED_DIVISION_NAME_MAPPING != null) {
            return REVERSED_DIVISION_NAME_MAPPING;
        }
        Map<String, String> reversedMapping = new Map<String, String>();
        for(String key : DIVISION_NAME_MAPPING.keySet()){
            reversedMapping.put(DIVISION_NAME_MAPPING.get(key), key);
        }
        REVERSED_DIVISION_NAME_MAPPING = reversedMapping;
        return REVERSED_DIVISION_NAME_MAPPING;
    } private set;}

    public final static Map<String, String> REVERSED_EMPLOYMENT_STATUS_MAPPING {get {
        if(REVERSED_EMPLOYMENT_STATUS_MAPPING != null) {
            return REVERSED_EMPLOYMENT_STATUS_MAPPING;
        }
        Map<String, String> reversedMapping = new Map<String, String>();
        for(String key : EMPLOYMENT_STATUS_MAPPING.keySet()){
            reversedMapping.put(EMPLOYMENT_STATUS_MAPPING.get(key), key);
        }
        REVERSED_EMPLOYMENT_STATUS_MAPPING = reversedMapping;
        return REVERSED_EMPLOYMENT_STATUS_MAPPING;
    } private set;}


    public static final String PRINCIPAL_NAME_CREDENTIALS      = 'credentials';
    public static final String CALLOUTS_CONSTANT               = 'callout:';
    public static final String USER_API_NAMED_CREDENTIALS_NAME = 'User_API';
    public static final String TAS_API_NAMED_CREDENTIALS_NAME  = 'Tas_API';
    public static final String OPTIMOVE_API_NAMED_CREDENTIALS_NAME = 'Optimove_API';
    public static final String OPTIMOVE_TRANSACTIONAL_API_NAMED_CREDENTIALS_NAME = 'Optimove_Transactional_API';
    public static final String OPTIMOVE_PUBLIC_API_NAMED_CREDENTIALS_NAME = 'Optimove_Public_API';
    public static final String GCP_API_NAMED_CREDENTIALS_NAME = 'GCP_API';
    public static final String GCP_API_TOKEN_NAMED_CREDENTIALS_NAME = 'GCP_API_Token';
    public static final String GCP_API_EXTERNAL_CREDENTIAL_NAME = 'GCP_API';

    public static final String STATUS_SUCCESS = 'Success';
    public static final String STATUS_ERROR = 'Error';
    public static final String STATUS_WARNING = 'Warning';

    public static final String TASK_PRIORITY_NORMAL = 'Normal';
    public static final String TASK_STATUS_NOT_STARTED = 'Not Started';
    public static final String TASK_SUBJECT_MORE_INFO_REQUIRED = 'Customer: More Info Required';
    public static final String TASK_SUBJECT_READY_FOR_FUNDING = 'Customer Ready for Funding';
    public static final String TASK_SUBJECT_FUNDING_FIELD = 'Client attempted to fund but failed';
    public static final String TASK_SUBJECT_FUNDED = 'Customer Funded';

    public static final String USER_ROLE_FOREX_SPECIALIST_US_NY = 'Forex Specialist - US NY';
    public static final String USER_ROLE_FOREX_SPECIALIST_US_TO = 'Forex Specialist - US TO';
    public static final String USER_ROLE_FOREX_SPECIALIST_LEAD_US_NY = 'Forex Specialist Lead - US NY';
    public static final String USER_ROLE_FOREX_SPECIALIST_LEAD_US_TO = 'Forex Specialist Lead - US TO';
    
    public static final String OPPORTUNITY_STAGE_MORE_INFO_REQUIRED = 'More Info Required';
    public static final String OPPORTUNITY_STAGE_RFF = 'Ready for Funding';
    public static final String OPPORTUNITY_STAGE_FUNDED = 'Funded';
    public static final String OPPORTUNITY_STAGE_CLOSED_WON = 'Closed Won';

    public static final String CASE_ACTION_EMAIL_OTMS = 'Case.EmailOtms';
    public static final String EMAIL_TEMPLATE_OTMS_CX_ENGLISH = 'OTMS_CX_Generic_Email_Template_English';
    public static final String EMAIL_TEMPLATE_OTMS_CX_SPANISH = 'OTMS_CX_Generic_Email_Template_Spanish';
    public static final String EMAIL_TEMPLATE_OTMS_CX_POLISH = 'OTMS_CX_Generic_Email_Template_Polish';
    public static final String EMAIL_TEMPLATE_OTMS_CX_ENGLISH_OANDA_LOGO = 'OTMS_CX_Generic_Email_Template_English_Oanda_logo';
    public static final String EMAIL_TEMPLATE_OTMS_CX_SPANISH_OANDA_LOGO = 'OTMS_CX_Generic_Email_Template_Spanish_Oanda_logo';
    public static final String EMAIL_TEMPLATE_OTMS_CX_POLISH_OANDA_LOGO = 'OTMS_CX_Generic_Email_Template_Polish_Oanda_logo';

    public static final String LANGUAGE_ENGLISH = 'English';
    public static final String LANGUAGE_SPANISH = 'Spanish';
    public static final String LANGUAGE_POLISH = 'Polish';
    public static final String UP_TRACKING_TRACK_ENDPOINT = '/v2/tracking/track';

    public static final String UP_TRACKING_EVENT_FXACC_DEMO_CREATION = 'DemoFxccCreateUPEvent';
    public static final String UP_TRACKING_EVENT_FXACC_FUNNEL_CHANGED = 'FunnelStageChangedUPEvent';
    public static final String UP_TRACKING_EVENT_FXACC_US_SHARES_ENABLED = 'USSharesTradingEnabledUPEvent';
    public static final String UP_TRACKING_EVENT_PAXOS_STATUS_CHANGED = 'PaxosStatusChangedUPEvent';
    public static final String UP_TRACKING_EVENT_CAMPAIGN_ASSIGMENT = 'CampaignAssignmentUPEvent';

    public static final String QUEUE_NAME_SPAM = 'Spam';
    public static final String QUEUE_NAME_OC_HIGHT_RISK_REVIEW_DUE = 'OC High Risk Review Due';

    public static final String FX_ACCOUNT_STREAM_UPDATES_CM = 'fxAccnt_stream_updates';
    public static final String FX_ACCOUNT_CLIENT_STATUS_CM = 'fxAccnt_client_status';
    public static final String FX_ACCOUNT_INTRODUCING_BROKER_CM = 'fxAccnt_introducing_broker';


    public static final String NEW_DOCUMENT_UPLOADED = 'New document uploaded -';

    public static final String FX_ACCOUNT_GROUP_RECALCULATE_CM = 'fxAccnt_group_recalculate';
}