/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-12-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsMatchTypeDet {
    public Map<String, List<String>> match_types { get; set; }

    public String type { get; set; }
}