/**
 * @File Name          : CamapinLeadsApiRestResource.cls
 * @Description        : 
 * @Author             : Mikolaj Juras
 * @CreateDate         : 8th April 2024
**/
@RestResource(urlMapping='/api/v1/campaignLeads/')
global with sharing class CamapinLeadsApiRestResource {

    private static String CAMPAIGN_LEAD_URL = '/api/v1/campaignLeads/';
    private static String CL_CAMPAIGN_NAMES = 'sf_campaign_names';
    private static String CL_CAMPAIGN_NAME = 'campaignName';
    private static String CL_ONE_ID = 'oneId';
    private static String CL_EMAIL = 'email';
    
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try {

            String reqBody = '';
            if(req.requestBody != null){
                reqBody = req.requestBody.toString().trim();
            }            
          
            // LogEvt__e log = new LogEvt__e(
            //                         Type__c = 'Info',
            //                         Category__c = CAMPAIGN_LEAD_URL ,
            //                         Message__c = 'Input payload for ' +  CAMPAIGN_LEAD_URL,
            //                         Details__c = reqBody,
            //                         Id__c = (String)UserInfo.getUserId());
            // EventBus.publish(log);

            Map<String, Object> input = (Map<String, Object>)JSON.deserializeUntyped(reqBody);
            //get email to get oneId....
           input.put(CL_ONE_ID,getOneIdByEmailFromUserApi((String)input.get(CL_EMAIL)));

            //get List<String> campaign names
            List<String> campaignNames = new List<String>();
            List<Object> unCastedNames =  (List<Object>)input.get(CL_CAMPAIGN_NAMES);
            for (Object obj : unCastedNames) {
                campaignNames.add((String)obj);
            }
            if(campaignNames.isEmpty()){
                String respBody = 'No campaigns found in the body ' + reqBody;
                res.responseBody = Blob.valueOf(respBody);
                res.statusCode = 400;
                Logger.error(
                    CAMPAIGN_LEAD_URL,
                    'No campaigns found: ',
                    res.statusCode + ' ' + respBody);
                    return;
                }
            //remove list to be replaced with single campaign name
            input.remove(CL_CAMPAIGN_NAMES);

            List<Campaign_Leads__e> cle = new List<Campaign_Leads__e>();

            for(String campName : campaignNames) {
                input.put(CL_CAMPAIGN_NAME,campName);
                //create 'campaignLEad' payload event
                Campaign_Leads__e cl = new Campaign_Leads__e();
                cl.CL_Payload__c = JSON.serialize(input);
                cl.Replay_Counter__c = 0;
                cle.add(cl);
            }
                       
            Database.SaveResult[] sr = EventBus.publish(cle);
            // if (!sr.isSuccess()) {
            //     LogEvt__e errLog = new LogEvt__e(
            //     Type__c = 'Error',
            //     Category__c = CAMPAIGN_LEAD_URL,
            //     Message__c = 'Error publishing event for endpoint: '+ CAMPAIGN_LEAD_URL,
            //     Details__c = reqBody,
            //     Id__c = (String)UserInfo.getUserId());
            //     EventBus.publish(errLog);
            // }
            
            //provide response to user-api
            res.responseBody = Blob.valueOf('Records will be processed.');
            res.statusCode = 200;
            return;

        } catch (Exception ex) { 
            String respBody = 'Something went wrong record will not be processed. Err msg: ' + ex.getMessage() + ex.getLineNumber() +ex.getStackTraceString();
            res.responseBody = Blob.valueOf(respBody);
            res.statusCode = 500;
            Logger.error(
                CAMPAIGN_LEAD_URL,
                'Error processing: '+ CAMPAIGN_LEAD_URL,
                res.statusCode + ' ' + respBody);
                return;
        }
    }

    public static String getOneIdByEmailFromUserApi(String email) {
        SimpleCallouts sc = new SimpleCallouts(Constants.USER_API_NAMED_CREDENTIALS_NAME);
        return sc.postOneIdByEmail(email);
    }

    public class OneIdResponseBody{
        public String language { get; set; }
        public String email { get; set; }
        public String uuid { get; set; }
    }}