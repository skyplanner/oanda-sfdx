public without sharing class PIUCKAHelperClass {
    static Set<String> divisions =
        new Set<String>{
            Constants.DIVISIONS_OANDA_CANADA,
            Constants.DIVISIONS_OANDA_EUROPE,
            Constants.DIVISIONS_OANDA_EUROPE_MARKETS,
            Constants.DIVISIONS_OANDA_AUSTRALIA,
            Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Constants.DIVISIONS_OANDA_ASIA_PACIFIC_CFD};

    @AuraEnabled(cacheable=true)
    public  static List<Personal_Information_Update__c> getQandA(Id fxAccountId) 
    {
       List<Personal_Information_Update__c> items = [select id , Name ,createddate ,Overall_Result__c,Trading_Experience_Result__c,Knowledge_Assessment_Result__c , (select Question__c ,Answer__c, No__c from Questionnaire__r order by No__c ASC) from Personal_Information_Update__c where fxAccount__c= : fxAccountId order by createddate DESC];
       return items;
    }

    // SP-10871 update results from PIU
    public static void updateResultsOnfxAccount(List<Personal_Information_Update__c> PIUs)
    {
        Map<Id, Personal_Information_Update__c> fxaIdPIUMap = new Map<Id, Personal_Information_Update__c>();
        for(Personal_Information_Update__c piu : PIUs)
        {
            fxaIdPIUMap.put(piu.fxAccount__c, piu);
        }

        List<fxAccount__c> fxAccountsToUpdate = new List<fxAccount__c>();
        for(fxAccount__c fxa : [SELECT Id, Division_Name__c, PIU_Knowledge_Result__c FROM fxAccount__c WHERE Id IN :fxaIdPIUMap.keyset()]) 
        {
            if(divisions.contains(fxa.Division_Name__c)) {
                fxa.PIU_Knowledge_Result__c = fxaIdPIUMap.get(fxa.Id).Knowledge_Assessment_Result__c;
                fxa.PIU_Overall_Result__c = fxaIdPIUMap.get(fxa.Id).Overall_Result__c;
                fxAccountsToUpdate.add(fxa);
            }
        }

        if(fxAccountsToUpdate.size() > 0)
        {
            update fxAccountsToUpdate;
        }
    }
}