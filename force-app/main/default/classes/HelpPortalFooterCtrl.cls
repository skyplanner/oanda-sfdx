/**
 * @File Name          : HelpPortalFooterCtrl.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : Dianelys Velazquez
 * @Last Modified On   : 07-25-2022
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   dmorales     Initial Version
 **/
public without sharing class HelpPortalFooterCtrl {
	public String regionCode { set; get; }

	public String regionLabel { set; get; }

	public String countryName { set; get; }

	public String countryCode { set; get; }

	public String languageCode { set; get; }

	public Map<String, String> regionList {
		set; 
		get { return getDivisionList(); }
	}


	public String urlImage {
		set;
		get { 
			return getUrlImagenRegion(); 
		}
	}

	public String productLink {
		set;
		get {
			return getCustomLink('Product');
		}
	}

	public String platformLink {
		set;
		get {
			return getCustomLink('Platform');
		}
	}
					
	public String toolsLink {
		set;
		get {
			return getCustomLink('Tools');
		}
	}

	public String getCustomLink(String linkName) {
		switch on regionCode {
			when 'OEL' {
				switch on linkName {
					when 'Product' {
						return 'https://www.oanda.com/uk-en/trading/';
					}
					when 'Platform' {
						return 'https://www.oanda.com/uk-en/trading/platforms/';
					}
					//Tools
					when else {
						return 'https://www.oanda.com/uk-en/trading/tools/';
					}
				}
			}
			when 'OC' {
				switch on linkName {
					when 'Product' {
						return 'https://www.oanda.com/us-en/trading/';
					}
					when 'Platform' {
						return 'https://www.oanda.com/us-en/trading/platforms/';
					}
					//Tools
					when else {
						return 'https://www.oanda.com/us-en/trading/tools/';
					}
				}
			}
			when else {
				switch on linkName {
					when 'Product' {
						return 'https://www1.oanda.com/forex-trading/markets/';
					}
					when 'Platform' {
						return 'https://www1.oanda.com/forex-trading/platform/';
					}
					//Tools
					when else {
						return 'https://www1.oanda.com/forex-trading/tools/';
					}
				}
			}
		}
	}

	public String getUrlImagenRegion() {
		String tempUrl = '/resource/HelpPortalImgRegion/'+regionCode + '-' + languageCode +'.png';
		String urlImg = regionCode + '-' + languageCode +'.png';
		
		try {
			/* Try to find the image inside Static Resource...if not exist then url is blank */
			new PageReference(tempUrl).getContent();
		} catch(VisualforceException e) {
			//if urlImg with this name regionCode + '-' + languageCode +'.png' does not exist, 
			//try to find default image with languageCode = 'en_US'
			try {
				tempUrl = '/resource/HelpPortalImgRegion/'+regionCode + '-en_US.png';
				urlImg = regionCode + '-en_US.png';
				new PageReference(tempUrl).getContent();
			}
			catch(VisualforceException e1) {
				urlImg = '';
			}		
		}
		return urlImg;
	}

	public Map<String, String> getDivisionList() {
		List<Division_Settings__mdt> divisionVisible = DivisionMetadataUtil.getDivisionVisible();
		Map<String, String> mapDivision = new Map<String, String>();

		if (Test.isRunningTest())
			mapDivision.put('OANDA','OANDA TEST');
			
		for (Division_Settings__mdt div : divisionVisible){
			mapDivision.put(div.DeveloperName, div.Division_Label__c);
		}

		return mapDivision;	
	}

	public List<LinkSection> getLinksByDivision() {
		List<LinkSection> result = new List<LinkSection>();

		if (String.isBlank(regionCode))
			return result;

		LinkSection s;

		for (Help_Portal_Footer_Link__mdt l : [SELECT Link_Order__c, Url__c, 
				Link_Custom_Label__c, Section_Order__c,
				Section__c, Section_Custom_Label__c
			FROM Help_Portal_Footer_Link__mdt
			WHERE Division__c =: regionCode
			ORDER BY Section_Order__c , Link_Order__c ]
		) {
			if (s == null || s.name != l.Section__c) {
				s = new LinkSection(l);
				result.add(s);
			}

			s.links.add(new Link(l));
		}

		return result;
	}

	public class LinkSection {
		public String name {get; set;}

		public String customLabel {get; set;}

		public List<Link> links {get; set;}

		public LinkSection(Help_Portal_Footer_Link__mdt l) {
			name = l.Section__c;
			customLabel = l.Section_Custom_Label__c;
			links = new List<Link>();
		}
	}

	public class Link {
		public String url {get; set;}
		
		public String customLabel {get; set;}

		public Link(Help_Portal_Footer_Link__mdt l) {
			url = l.Url__c;
			customLabel = l.Link_Custom_Label__c;
		}
	}
}