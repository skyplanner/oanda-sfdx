public without sharing class ComplianceFxTrade {
	public final static String DEFAULT_QUEUE_NAME = 'On Boarding Cases'; 
	
	final static Map<String, Integer> hosueholdIncomeMap = new Map<String, Integer> {
		
		'0_5K'           => 5000,
		'0_25K'          => 25000,
		'25K'            => 25000,
		'15_25K'         => 25000,
		'25_50K'         => 50000,
		'50_100K'        => 100000,
		'100_250K'       => 250000,
		'250_750K'       => 750000,
		'750_1000K'      => 1000000,
		'1000_2000K'     => 2000000, 
	    '2000K'          => 2000000
	};
	
	public class CaseDesc{
		List<String> descList = new List<String>();
		
		public void addDesc(String d){
			descList.add(d);
		}
		
		
		public String getDescs(){
			String result = '';
			
			for(String d : descList){
				result += d + '\n';
			}
			
			return result;
		}
	}
	
	private static String caseQueueId = null;
	private static Id currentUserId = UserInfo.getUserId();
    
    public static void createCases(List<Compliance_FxTrade__c> fxTrades){
    	Set<String> fxTradeUserNames = new Set<String>();
    	
    	for(Compliance_FxTrade__c fxt : fxTrades){
    		fxTradeUserNames.add(fxt.fxTrade_UserName__c);
    	}
    	
    	List<fxAccount__c> fxas = [select id, name, Account__c, Account__r.fxAccount__c, Lead__c, Lead__r.fxAccount__c, Contact__c from fxAccount__c where name in :fxTradeUserNames and recordTypeId = : RecordTypeUtil.getFxAccountLiveId()];
    	if(fxas.size() == 0){
    		//no valid fxAccounts
    		System.debug('No Matched fxAccounts');
    		return;
    	}
    	
    	Map<String, fxAccount__c> fxAccountByName = new Map<String, fxAccount__c>();
    	for(fxAccount__c fxa : fxas){
    		fxAccountByName.put(fxa.name, fxa);
    	}
    	
    	List<Case> cases = new List<Case>();
    	
    	for(Compliance_FxTrade__c fxt : fxTrades){
    		boolean createCase = false;
    		CaseDesc des = new CaseDesc();
    		des.addDesc('FxAccount User Name: ' + fxt.fxTrade_UserName__c);
    		
    		if(fxt.Employment_Status__c == 'Student' && createCaseForStudent(fxt, des)){
    			createCase = true;
    		}else if(fxt.Employment_Status__c == 'Retired' && createCaseForRetired(fxt, des)){
    			createCase = true;
    		}else if(fxt.Employment_Status__c == 'Unemployed' && createCaseForUnemployed(fxt, des)){
    			createCase = true;
    		}else if(fxt.Employment_Status__c == 'Self_Employed' && createCaseForSelfEmployed(fxt,des)){
    			createCase = true;
    		}else if(fxt.Employment_Status__c == 'Employed' && createCaseForEmployed(fxt, des)){
    			createCase = true;
    		}
    		
    		if(createCase){
    			System.debug('Create case for : ' + fxt.fxTrade_UserName__c);
    			Case theCase = createCase(fxt, des);
    			
    			//assocate the case with account or lead
    			fxAccount__c fxa = fxAccountByName.get(fxt.fxTrade_UserName__c);
    			if(fxa != null){
    				theCase.accountId = fxa.Account__c;
    				theCase.contactId = fxa.contact__c;
    				theCase.Lead__c = fxa.Lead__c;
    			}
    			
    			if(theCase.accountId != null || theCase.Lead__c != null){
    				cases.add(theCase);
    			}else{
    				fxt.addError('Invalid fxTrade User Name');
    			}
    			
    		}
    	}
    	
    	System.debug('Number of Cases to create: ' + cases.size());
    	if(cases.size() > 0){
    		
    		insert cases;
    	}
    	
    }
    
    private static String getDivision(Compliance_FxTrade__c fxTrade){
    	String result = null;
    	
    	if(fxTrade.Division_ID__c == '1' || fxTrade.Division_ID__c == '6'){
    		result = 'OC';
    	}else if(fxTrade.Division_ID__c == '3'){
    		result = 'OEL';
    	}
    	
    	return result;
    }
    
    private static boolean createCaseForStudent(Compliance_FxTrade__c fxt, CaseDesc des){
    	Boolean result = false;
    	
    	/*
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Student__c))
    	   || fxt.Net_Worth__c > fxt.Limit_Net_Worth_Student__c
    	   || (fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != '')
    	   || fxt.Age__c > fxt.Limit_Age_Student__c
    	   || (getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c)
    	   || (getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c)){
    	   	
    		result = true;
    	}
    	*/
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Student__c))){
    		des.addDesc(createDescription('household_income_student'));
    		result = true;
    	}
    	
    	if(fxt.Net_Worth__c > fxt.Limit_Net_Worth_Student__c){
    		des.addDesc(createDescription('networth_student'));
    		result = true;
    	}
    	
    	if(fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != ''){
    		des.addDesc(createDescription('income_source_student'));
    		result = true;
    	}
    	
    	if(fxt.Age__c > fxt.Limit_Age_Student__c){
    		des.addDesc(createDescription('age_student'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c){
    		des.addDesc(createDescription('bankruptcy_student'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c){
    		des.addDesc(createDescription('intermediary_student'));
    		result = true;
    	}
    	
    	return result;
    }
    
    private static boolean createCaseForRetired(Compliance_FxTrade__c fxt, CaseDesc des){
    	boolean result = false;
    	
    	/*
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Retired__c))
    	    || fxt.Net_Worth__c > fxt.Limit_Net_Worth_Retired__c
    	    || (fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != '')
    	    || fxt.Age__c < fxt.Limit_Age_Retired__c
    	    || (getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c)
    	    || (getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c)){
    	    	
    	    	result = true;
    		
    	}
    	*/
    	if(hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Retired__c)){
    		des.addDesc(createDescription('household_income_retired'));
    		result = true;
    	}
    	
    	if(fxt.Net_Worth__c > fxt.Limit_Net_Worth_Retired__c){
    		des.addDesc(createDescription('networth_retired'));
    		result = true;
    	}
    	
    	if(fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != ''){
    		des.addDesc(createDescription('income_source_retired'));
    		result = true;
    	}
    	
    	if(fxt.Age__c < fxt.Limit_Age_Retired__c){
    		des.addDesc(createDescription('age_retired'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c){
    		des.addDesc(createDescription('bankruptcy_retired'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c){
    		des.addDesc(createDescription('intermediary_retired'));
    		result = true;
    	}
    	
    	return result;
    }
    
    private static boolean createCaseForUnemployed(Compliance_FxTrade__c fxt, CaseDesc des){
    	boolean result = false;
    	
    	/*
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Unemployed__c))
    	   || fxt.Net_Worth__c > fxt.Limit_Net_Worth_Unemployed__c
    	   || (fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != '')
    	   || (getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c)
    	   || (getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c)){
    	   	
    	   	result = true;
    	}
    	*/
    	
    	if(hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Unemployed__c)){
    		des.addDesc(createDescription('household_income_unemployed'));
    		result = true;
    	}
    	
    	if(fxt.Net_Worth__c > fxt.Limit_Net_Worth_Unemployed__c){
    		des.addDesc(createDescription('networth_unemployed'));
    		result = true;
    	}
    	
    	if(fxt.Income_Source__c != null && fxt.Income_Source__c.trim() != ''){
    		des.addDesc(createDescription('income_source_unemployed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c){
    		des.addDesc(createDescription('bankruptcy_unemployed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c){
    		des.addDesc(createDescription('intermediary_unemployed'));
    		result = true;
    	}
    	
    	return result;
    }
    
    private static boolean createCaseForSelfEmployed(Compliance_FxTrade__c fxt, CaseDesc des){
    	boolean result = false;
    	
    	/*
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Self_Employed__c))
    	   || fxt.Net_Worth__c > fxt.Limit_Net_Worth_Self_Employed__c
    	   || (fxt.Industry__c == null || fxt.Industry__c.trim().length() < 4)
    	   || (fxt.Description_Of_Products_And_Services__c == null || fxt.Description_Of_Products_And_Services__c.trim().length() < 4)
    	   || (getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c)
    	   || (getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c)){
    		
    		result = true;
    	}*/
    	
    	if(hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Self_Employed__c)){
    		des.addDesc(createDescription('household_income_selfemployed'));
    		result = true;
    	}
    	
    	if(fxt.Net_Worth__c > fxt.Limit_Net_Worth_Self_Employed__c){
    		des.addDesc(createDescription('networth_selfemployed'));
    		result = true;
    	}
    	
    	if(fxt.Industry__c == null || fxt.Industry__c.trim().length() < 4){
    		des.addDesc(createDescription('industry_selfemployed'));
    		result = true;
    	}
    	
    	if(fxt.Description_Of_Products_And_Services__c == null || fxt.Description_Of_Products_And_Services__c.trim().length() < 4){
    		des.addDesc(createDescription('product_selfemployed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c){
    		des.addDesc(createDescription('bankruptcy_selfemployed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c){
    		des.addDesc(createDescription('intermediary_selfemployed'));
    		result = true;
    	}
    	
    	return result;
    }
    
    private static boolean createCaseForEmployed(Compliance_FxTrade__c fxt, CaseDesc des){
    	boolean result = false;
    	
    	/*
    	if((hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Employed__c))
    	   || fxt.Net_Worth__c > fxt.Limit_Net_Worth_Employed__c
    	   || (fxt.Employer__c == null || fxt.Employer__c.trim().length() < 4)
    	   || (fxt.Employment_Position__c == null || fxt.Employment_Position__c.trim().length() < 4)
    	   || (getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c)
    	   || (getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c)){
    	   	
    	   	result = true;
    		
    	}
    	*/
    	
    	if(hosueholdIncomeMap.get(fxt.Household_Income__c) != null && hosueholdIncomeMap.get(fxt.Household_Income__c) > hosueholdIncomeMap.get(fxt.Limit_Household_Income_Employed__c)){
    		des.addDesc(createDescription('household_income_employed'));
    		result = true;
    	}
    	
    	if(fxt.Net_Worth__c > fxt.Limit_Net_Worth_Employed__c){
    		des.addDesc(createDescription('networth_employed'));
    		result = true;
    	}
    	
    	if(fxt.Employer__c == null || fxt.Employer__c.trim().length() < 4){
    		des.addDesc(createDescription('employer_employed'));
    		result = true;
    	}
    	
    	if(fxt.Employment_Position__c == null || fxt.Employment_Position__c.trim().length() < 4){
    		des.addDesc(createDescription('position_employed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.Bankruptcy_Status__c){
    		des.addDesc(createDescription('bankruptcy_employed'));
    		result = true;
    	}
    	
    	if(getDivision(fxt) == 'OC' && fxt.INTERMEDIARY__c){
    		des.addDesc(createDescription('intermediary_employed'));
    		result = true;
    	}
    	
    	return result;
    }
     
    
    private static Case createCase(Compliance_FxTrade__c fxt, CaseDesc des){
    	Case theCase = new Case(Subject = fxt.Case_Subject__c, Type = fxt.Case_Type__c, Subtype__c = fxt.Case_Subtype__c);
    	theCase.description = des.getDescs();
    	theCase.recordtypeId = RecordTypeUtil.getSupportCaseTypeId();
    	
        theCase.ownerId = getQueueId(fxt.Case_Queue__c);
        
        
        return theCase;
    }
    
    private static Id getQueueId(String queueName){
    	
    	if(caseQueueId == null){
    		List<Group> theGroups = [select Id from Group where Type = 'Queue' and name = :queueName];
    		
    		if(theGroups.size() > 0){
    			caseQueueId = theGroups[0].Id;
    		}
    	}
    	
    	if(caseQueueId == null){
    		List<Group> theGroups = [select Id from Group where Type = 'Queue' and name = :DEFAULT_QUEUE_NAME];
    		
    		if(theGroups.size() > 0){
    			caseQueueId = theGroups[0].Id;
    		}
    	}
    	
    	return caseQueueId;
    	
    }
    
    private static String createDescription(String msgKey){
    	String result = '';
    	
    	Compliance_Message__c msg = Compliance_Message__c.getValues(msgKey);
    	if(msg != null){
    		result = msg.Message__c;
    	}
    	
    	return result;
    }
    
    
    
    
}