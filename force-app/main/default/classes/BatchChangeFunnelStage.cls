/*
 * 
 */
public with sharing class BatchChangeFunnelStage implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{

	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchChangeFunnelStage() {
		query = 'SELECT Id, Funnel_Stage__c, RecordTypeId FROM fxAccount__c WHERE Funnel_Stage__c = NULL AND RecordTypeId IN (SELECT Id FROM RecordType WHERE SObjectType=\'fxAccount__c\' AND Name = \'Retail Practice\')';
	}

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, Funnel_Stage__c, RecordTypeId FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
   	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		List<fxAccount__c> fxAccounts = (List<fxAccount__c>)batch;
   		for (fxAccount__c fxAccount : fxAccounts) {
   			fxAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
   		}
   		update fxAccounts;
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}
	
	public static Id executeBatch() {
		return Database.executeBatch(new BatchChangeFunnelStage());
	}
}