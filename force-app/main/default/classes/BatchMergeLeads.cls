/*
 * 
 */
public with sharing class BatchMergeLeads implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection{

	String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	
	public BatchMergeLeads() {
		query = 'SELECT ' + LeadBatchUtil.getAllLeadFieldsSoql() + ' FROM Lead WHERE IsConverted=false AND Email!=\'\'';
//		query = 'SELECT Id, RecordTypeId, Email, Funnel_Stage__c, CreatedDate FROM Lead WHERE IsConverted=false AND Email=\'testleadmerge@oanda.com\'';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT ' + LeadBatchUtil.getAllLeadFieldsSoql() + ' FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
   	public Database.QueryLocator start(Database.BatchableContext bc) {
   		CustomSettings.setEnableLeadConversion(false);
   		CustomSettings.setEnableLeadMerging(true);
    	return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bc, List<sObject> batch) {
   		
   		if (CustomSettings.isEnableLeadConversion()) {
   			throw new ApplicationException('Lead conversion must be disabled before merging leads');
   		}
   		
    	LeadBatchUtil.mergeLeads((Lead[]) batch);
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
		CustomSettings.setEnableLeadConversion(true);
   		CustomSettings.setEnableLeadMerging(false);
   		try {
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
   		}
   		catch (Exception e) {}
	}
	
	// BatchMergeLeads.executeBatch()
	public static Id executeBatch() {
		return Database.executeBatch(new BatchMergeLeads());
	}
}