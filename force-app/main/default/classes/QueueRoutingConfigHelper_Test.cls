/**
 * @File Name          : QueueRoutingConfigHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 12:23:21 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/22/2021, 12:00:05 AM   acantero     Initial Version
**/
@IsTest
private without sharing class QueueRoutingConfigHelper_Test {

	// @testSetup
    // static void setup() {
    // }

    //blank devName => throw exception
    @IsTest
    static void getByDevName1() {
        Boolean error = false;
        Test.startTest();
        try {
            QueueRoutingConfig result = 
                QueueRoutingConfigHelper.getByDevName(null, true);
            //...
        } catch (QueueRoutingConfigHelper.QueueRoutingConfigHException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }

    //invalid devName => throw exception
    @IsTest
    static void getByDevName2() {
        Boolean error = false;
        Test.startTest();
        try {
            QueueRoutingConfig result = 
                QueueRoutingConfigHelper.getByDevName('fakeDevName', true);
            //...
        } catch (QueueRoutingConfigHelper.QueueRoutingConfigHException ex) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
    
}