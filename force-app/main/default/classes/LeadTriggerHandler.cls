/**
 * Class to handle the Lead trigger events
 * @author: Michel Carrillo (SkyPlanner)
 * @date:   17/03/2021
 */
public with sharing class LeadTriggerHandler {

    final List<Lead> newList;
    final Map<Id, Lead> oldMap;

    static final Map<String, String> FXACC_LEAD_FIELDS_MAP =
            new Map<String, String>{
                    'Business_Name__c' => 'Business_Name__c',
                    'City__c' => 'City',
                    'Country_Name__c' => 'Country',
                    'Email__c' => 'Email',
                    'First_Name__c' => 'FirstName',
                    'Has_Opted_Out_Of_Email__c' => 'HasOptedOutOfEmail',
                    'Language_Preference__c' => 'Language_Preference__c',
                    'Last_Name__c' => 'LastName',
                    'Middle_Name__c' => 'MiddleName',
                    'Phone__c' => 'Phone',
                    'Postal_Code__c' => 'PostalCode',
                    'State__c' => 'State',
                    'Street__c' => 'Street',
                    'Suffix__c' => 'Suffix',
                    'Title__c' => 'Salutation'
            };

    final Id leadRetailId = RecordTypeUtil.getLeadRetailId();

    public LeadTriggerHandler()
    {

    }
    /**
     * Constructor
     */
    public LeadTriggerHandler(List<Lead> newList, Map<Id, Lead> oldMap)
    {
        this.newList = newList;
        this.oldMap = oldMap;
    }


    /**
     * Handles the before insert event
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   17/03/2021
     * @return: void
     * @param:  pNew, the Trigger.new context variable
     */
    public void beforeInsert(List<Lead> newList){
        for(Lead l : newList){
            logicFromWF(l);
        }
    }

    /**
     * Handles the before update event
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   17/03/2021
     * @return: void
     * @param:  newList, the Trigger.new context variable
     * @param:  oldMap, the Trigger.oldMap context variable
     */
    public void beforeUpdate(List<Lead> newList, Map<Id, Lead> oldMap){
        for(Lead l : newList){
            logicFromWF(l);
        }
    }

    public void onAfterUpdate()
    {
        Map<Id,Lead> fxAccsToLeadsMap = new Map<Id, Lead>();
        List<Id> leadsToTrgCRACalc = new List<Id>();
        for(Lead lead :newList)
        {
            Lead oldLead = oldMap.get(lead.Id);

            TrustedContactPersonUtil.checkFlagChange(oldLead, lead);
            verify(lead, oldLead, fxAccsToLeadsMap);
            checkCountryChange(lead, oldLead, leadsToTrgCRACalc);
        }

        TrustedContactPersonUtil.deleteTCPRecords();
        rollDownFxActs(fxAccsToLeadsMap);
        LeadUtil.updateCRAFromLead(leadsToTrgCRACalc);
    }

    /**
     * Implemets the logic from a workflow rule
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   17/03/2021
     * @return: void
     * @param:  lead, the Trigger.new context variable
     */
    private static void logicFromWF(Lead lead){
        if(String.isNotBlank(lead.Entry__c)){
            lead.Notes__c = getNote(lead.Entry__c) + (String.isNotBlank(lead.Notes__c) ? lead.Notes__c : '');
            lead.Entry__c = null;
        }
    }

    /**
     * Gets the note in the following format:
     * 'Mar 17 2021, 03:03:55 PM (Eastern Daylight Time) <Michel Carillo>'
     * @author: Michel Carrillo (SkyPlanner)
     * @date:   17/03/2021
     * @return: String
     * @param:  pEntry, the content of the note
     */
    private static String getNote(String pEntry){
        //Mar 17 2021, 03:03:55 PM (Eastern Daylight Time) <Michel Carillo>
        return
            System.now().format('MMM d yyyy, h:mm:ss a (zzzz)') +
            ' <' + UserInfo.getName() + '> \n' +
            pEntry + '\n ------- \n\n';
    }

    public void verify(Lead lead, Lead oldLead, Map<Id,Lead>fxAccsToLeadsMap){
        if(lead != null && lead.fxAccount__c != null && lead.RecordTypeId == leadRetailId &&
                !UserUtil.getIntegrationProfileIds().contains(UserInfo.getProfileId()) &&
                SObjectUtil.isRecordFieldsChanged(lead,oldLead,FXACC_LEAD_FIELDS_MAP.values())){
            fxAccsToLeadsMap.put(lead.fxAccount__c, lead);
        }
    }
    private static void rollDownFxActs(Map<Id,Lead>fxAccsToLeadsMap) {

        if(fxAccsToLeadsMap.isEmpty()) return;

        List<fxAccount__c> fxAccsToUpd =
        [SELECT Id, Lead__c FROM fxAccount__c WHERE Id IN :fxAccsToLeadsMap.keySet() FOR UPDATE];
        if(fxAccsToUpd.isEmpty()) return;

        for(fxAccount__c fxAcc :fxAccsToUpd){
            fillFxAcc(fxAcc, fxAccsToLeadsMap);
        }
        update fxAccsToUpd;
    }

    private static void fillFxAcc(fxAccount__c fxAcc, Map<Id,Lead>fxAccsToLeadsMap){

        SObject obj = fxAcc;
        Lead lead = fxAccsToLeadsMap.get(fxAcc.Id);
        for(String field :FXACC_LEAD_FIELDS_MAP.keySet()) {
            obj.put(field, lead.get(FXACC_LEAD_FIELDS_MAP.get(field)));
        }
    }

    private static void checkCountryChange(Lead lead, Lead oldLead, List<Id> leadsToTrgCRACalc)
    {
        if(lead.RecordTypeId == RecordTypeUtil.getLeadRetailId() && // lead is live lead
                lead.Country != oldLead.Country)  // country has changed
        {
            leadsToTrgCRACalc.add(lead.Id);
        }
    }
}