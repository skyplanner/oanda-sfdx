@IsTest
public with sharing class BatchMergeLeadsScheduleableTest {
	
	final static User SYSTEM_USER = UserUtil.getUserByName(UserUtil.NAME_SYSTEM_USER);
	
	static {
		CustomSettings.setEnableLeadConversion(true);
		CustomSettings.setEnableLeadMerging(true);
	}
	
	static testMethod void testNoMerge() {
		System.runAs(SYSTEM_USER) {
			Lead l1 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Email='joe1@example.org', LastName='Schmoe');
			Lead l2 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe2@example.org', LastName='Schmoe', Territory__c = 'North America');
			
			insert new Lead[] { l1, l2 };
			
			fxAccount__c fxAccount1 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Lead__c = l1.Id);
			fxAccount__c fxAccount2 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = l2.Id);
			
			insert new fxAccount__c[] { fxAccount1, fxAccount2 };
			
			Test.startTest();
			BatchMergeLeadsScheduleable.executeBatch();
			Test.stopTest();
			
			//Verify that both leads still exist
			l1 = [SELECT Id FROM Lead WHERE Id =: l1.Id];
			l2 = [SELECT Id FROM Lead WHERE Id =: l2.Id];
			
			System.assert(true);
		}
	}
	
	static testMethod void testSimpleMerge() {
		
		System.runAs(SYSTEM_USER) {
			Lead l1 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Email='joe@example.org', LastName='Schmoe');
			Lead l2 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe@example.org', LastName='Schmoe', Territory__c = 'North America');
			
			insert new Lead[] { l1, l2 };
			  checkRecursive.SetOfIDs = new Set<Id>();
			fxAccount__c fxAccount1 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE, Lead__c = l1.Id);
			fxAccount__c fxAccount2 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = l2.Id);
			
			insert new fxAccount__c[] { fxAccount1, fxAccount2 };
			
			  checkRecursive.SetOfIDs = new Set<Id>();
			// At this point no merging should have taken place
			l1 = [SELECT Id FROM Lead WHERE Id =: l1.Id];
			l2 = [SELECT Id FROM Lead WHERE Id =: l2.Id];
			
			Test.startTest();
			BatchMergeLeadsScheduleable.executeBatch();
			Test.stopTest();
			
			//l1 should now be merged
			try {
				l1 = [SELECT Id FROM Lead WHERE Id =: l1.Id];
				System.assert(false);
			}
			catch (Exception e) {}
			
			l2 = [SELECT Id, RecordTypeId FROM Lead WHERE Id =: l2.Id];
			fxAccount1 = [SELECT Id, Lead__c FROM fxAccount__c WHERE Id =: fxAccount1.Id];
			fxAccount2 = [SELECT Id, Lead__c FROM fxAccount__c WHERE Id =: fxAccount2.Id];
			
			System.assertEquals(LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, l2.RecordTypeId);
			System.assertEquals(l2.Id, fxAccount1.Lead__c);
			System.assertEquals(l2.Id, fxAccount2.Lead__c);
		}
	}
	
	
	static testMethod void alreadyConvertedMerge() {
		TestsUtil.forceJobsSync = true;
		
		System.runAs(SYSTEM_USER) {
			Lead l1 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe@example.org', LastName='Schmoe', Territory__c = 'North America');
			insert l1;
		
			fxAccount__c fxAccount1 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = l1.Id, Funnel_Stage__c = FunnelStatus.TRADED);
			insert fxAccount1;
			
			fxAccount1.Trigger_Lead_Assignment_Rules__c = false;
			update fxAccount1;
			
			BatchConvertLeadsScheduleable.executeInline();

			Test.startTest();
						
			Lead l2 = new Lead(RecordTypeId=LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Email='joe@example.org', LastName='Schmoe', Territory__c = 'North America');
			insert l2;
            checkRecursive.SetOfIDs = new Set<Id>();
			fxAccount__c fxAccount2 = new fxAccount__c(RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE, Lead__c = l2.Id, Funnel_Stage__c = FunnelStatus.BEFORE_YOU_BEGIN);
			insert fxAccount2;
			
			l2 = [SELECT Id, IsConverted, Duplicate_Email__c FROM Lead WHERE Id =: l2.Id];
			System.assert(!l2.IsConverted);
			System.assert(l2.Duplicate_Email__c);
			
			BatchMergeLeadsScheduleable.executeInline();
			
			Test.stopTest();
			
			try {
				l2 = [SELECT Id FROM Lead WHERE Id =: l2.Id];
				System.assert(false);
			}
			catch (Exception e) {}
			
			l1 = [SELECT Id, IsConverted, ConvertedOpportunityId, ConvertedAccountId, ConvertedContactId FROM Lead WHERE Id =: l1.Id];
			fxAccount2 = [SELECT Id, Lead__c, Account__c, Opportunity__c, Contact__c FROM fxAccount__c WHERE Id =: fxAccount2.Id];
			
			System.assert(l1.IsConverted);
			System.assert(fxAccount2.Lead__c == null);
			System.assertEquals(l1.ConvertedAccountId, fxAccount2.Account__c);
			System.assertEquals(l1.ConvertedContactId, fxAccount2.Contact__c);
		}
	}
	
    private static void insertFxtradeUser() {
    	System.runAs(SYSTEM_USER) {
    		
    		String uname = 'joe' + Math.round(Math.random() * 1000000);
    		
    		Lead lead = new Lead();
    		lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		lead.LastName = 'Schmoe';
    		lead.Email = uname + '@example.org';
    		lead.Territory__c = 'North America';
    		
    		insert lead;
    		
    		fxAccount__c practiceAccount = new fxAccount__c();
    		practiceAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
    		practiceAccount.Name = uname;
    		practiceAccount.Lead__c = lead.Id;
    		practiceAccount.Funnel_Stage__c = FunnelStatus.DEMO_REGISTERED;
    		
    		fxAccount__c fxAccount = new fxAccount__c();
    		fxAccount.RecordTypeId = fxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
    		fxAccount.Name = uname;
    		fxAccount.Lead__c = lead.Id;
    		fxAccount.Funnel_Stage__c = FunnelStatus.TRADED;
    		
    		insert fxAccount;
    	}
    }
}