@isTest
public without sharing class FxAccountUpsertTriggerHandlerTest {
    private static string emptyRestUrl = '/services/apexrest/api/v1/fxAccount/';
    private static string restUrl = '/services/apexrest/api/v1/fxAccount/123123123';
    private static string postMethod = 'POST';
    
    static String liveAcc = '{"language_preference": "English", "note": {"author": "Registration system", "timestamp": 1721127340171, "subject": "", "comment": "User onboarded from PROHIBITED country." }, "has_professional_experience": "true","registration_date": 1711117673000, "declaration_update_date":1718878635012.2854, "marketing_email_opt_out": true,"risk_ack_question1": "true", "address": {"street1": "3-900 Lasalle Blvd", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "2005-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "Unemployed", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';
    static String liveAcc2 = '{"language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "3-900 Lasalle Blvd", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';
    static String liveAcc3 = '{"language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "3-900 Lasalle Blvd", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';
    static String liveAcc4 = '{"language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "3-900 Lasalle Blvd", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';

    static String liveAccNewEmail = '{"language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123124@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';
    static String liveAccExtraConfig = '{"dummydummy": "dummydummy", "language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "3-900 Lasalle Blvd", "street2": "", "city": "Sudbury", "region": "", "postcode": "P3A 5W8", "country": "Canada"}, "security_answer": "", "birth_date": "2005-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "net_worth_value": "5555", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": false, "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';

    static String liveAccNoDemo = '{"language_preference": "English", "registration_date": 1711117673000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "", "region": "", "postcode": "", "country": "Canada"}, "security_answer": "", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "annual_income": "", "full_name": "", "net_worth_currency_type": "CAD", "oanda_relationship": false, "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "self_employment_details": "", "registration_source": "9sf", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "timestamp": 1711117696692, "author": "fxdb", "agent": "user-api"}';
   
    static String demoAcc ='{"language_preference": "English", "registration_date": 1711117698000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "", "region": "", "postcode": "", "country": "Canada"}, "security_answer": "", "bankruptcy_details": "NA", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employer_business": "NA", "employer_name": "NA", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "full_name": "", "net_worth_value": "0", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "registration_source": "user-api", "government_id": "NA", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": true, "timestamp": 1711117699469, "author": "fxdb", "agent": "user-api"}';
    static String demoAcc2 ='{"language_preference": "English", "registration_date": 1711117698000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "", "region": "", "postcode": "", "country": "Canada"}, "security_answer": "", "bankruptcy_details": "NA", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employer_business": "NA", "employer_name": "NA", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "full_name": "", "net_worth_value": "0", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "registration_source": "user-api", "government_id": "NA", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": true, "timestamp": 1711117699469, "author": "fxdb", "agent": "user-api"}';
    static String demoAcc3 ='{"language_preference": "English", "registration_date": 1711117698000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "", "region": "", "postcode": "", "country": "Canada"}, "security_answer": "", "bankruptcy_details": "NA", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employer_business": "NA", "employer_name": "NA", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "full_name": "", "net_worth_value": "0", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "registration_source": "user-api", "government_id": "NA", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": true, "timestamp": 1711117699469, "author": "fxdb", "agent": "user-api"}';
    static String demoAcc4 ='{"language_preference": "English", "registration_date": 1711117698000, "marketing_email_opt_out": true, "address": {"street1": "", "street2": "", "city": "", "region": "", "postcode": "", "country": "Canada"}, "security_answer": "", "bankruptcy_details": "NA", "birth_date": "1900-01-01", "nationality": "", "division_id": "2", "employer_business": "NA", "employer_name": "NA", "employment_length": "0", "employment_position": "", "employment_status": "", "business_name": "na", "entity_type": "individual", "full_name": "", "net_worth_value": "0", "one_id": "5eccc628-e858-11ee-951a-42010a7801d3", "security_question": "", "registration_source": "user-api", "government_id": "NA", "trading_objective": "0", "username": "cf2cb24692149c056eb8ea50bf571bfb", "email": "testEmail123123@oanda.com", "is_demo": true, "timestamp": 1711117699469, "author": "fxdb", "agent": "user-api"}';


    

    @isTest
    static void testDoPostNoDemo() {

        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(1000, new List<String>{liveAccNoDemo});
    
        fxAccUserIdToReplayCounter.put(1000, 1);

        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        try {
            fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);
        } catch (FxAccountUpsertTriggerHandler.FxAccountInvalidRequestException ex) { 
            Assert.isTrue(ex.getMessage().contains('is_demo and email are required'), 'is demo is required');
        }
            
    }

    @isTest
    static void testInsertAcc() {
        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(1000, new List<String>{demoAcc});
        fxAccUserIdToPayloads.put(1001, new List<String>{liveAcc});
    
    
        fxAccUserIdToReplayCounter.put(1000, 0);
        fxAccUserIdToReplayCounter.put(1001, 0);

        Assert.areEqual(0, [SELECT Name FROM fxAccount__c limit 1].size() , 'No fxAccount is present');  
        
        Test.startTest();
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);
        Test.stopTest();

        Assert.areEqual(2, [SELECT count() FROM fxAccount__c ] , 'fxAccs are Inserted');  
        Assert.areEqual(1, [SELECT count() FROM Lead] , 'Lead is Inserted');     
    }

    @isTest
    static void testSendEventsToreplay() {
        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(1000, new List<String>{demoAcc});
        fxAccUserIdToPayloads.put(1001, new List<String>{liveAcc});
        fxAccUserIdToPayloads.put(1002, new List<String>{liveAccNewEmail});
    
    
        fxAccUserIdToReplayCounter.put(1000, 2);
        fxAccUserIdToReplayCounter.put(1001, 2);
        fxAccUserIdToReplayCounter.put(1002, 2);

        Assert.areEqual(0, [SELECT Name FROM fxAccount__c limit 1].size() , 'No fxAccount is present');  
        
        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;


        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 1001;
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        fxAccount.fxTrade_Global_ID__c = '1001+' + String.valueOf(RecordTypeUtil.getFxAccountLiveId()).left(15);
        insert fxAccount;

        // Test.startTest();
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.sendEventsToreplay(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter, new Set<Decimal>{1000, 1002}, new Set<Id>{account.Id});
        Test.getEventBus().deliver();
        // Test.stopTest();

        Assert.areEqual(3, [SELECT count() FROM fxAccount__c ] , 'fxAccs are present');  
        Assert.areEqual(2, [SELECT count() FROM Lead] , 'Lead is Inserted');     
    }

    @isTest
    static void testInsertAccExtraConfig() {
        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(1000, new List<String>{liveAccExtraConfig});
    
    
        fxAccUserIdToReplayCounter.put(1000, 0);

        
        // Test.startTest();
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);
        
        // Test.stopTest();
        
        Test.getEventBus().deliver();
        Assert.areEqual(1, [SELECT count() FROM fxAccount__c ] , 'fxAccs are Inserted');  
        //seems issue in tests, logger might be an event or other issue, in reality mapping error is created
        //Assert.isTrue([SELECT Details__c FROM Log__c WHERE Message__c = 'Missing mapping' AND Type__c = 'Info' LIMIT 1].Details__c.contains('dummydummy') , 'Log have infor of extra field');     
    }

    @isTest
    static void testInsertAccMass() {
        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(2000, new List<String>{demoAcc, demoAcc2, demoAcc3});
        fxAccUserIdToPayloads.put(1000, new List<String>{liveAcc, liveAcc2, liveAcc4});
        fxAccUserIdToPayloads.put(1001, new List<String>{liveAccNewEmail});

    
    
        fxAccUserIdToReplayCounter.put(1000, 0);
        fxAccUserIdToReplayCounter.put(1001, 0);
        fxAccUserIdToReplayCounter.put(2000, 0);

        Assert.areEqual(0, [SELECT Name FROM fxAccount__c limit 1].size() , 'No fxAccount is present');  
        
        Test.startTest();
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);
        Test.stopTest();

        Assert.areEqual(3, [SELECT count() FROM fxAccount__c ] , 'fxAccs are Inserted');  
        Assert.areEqual(2, [SELECT count() FROM Lead] , 'Lead is Inserted');    
        Lead leadAfterUpdate = [SELECT Id, City, Street FROM Lead LIMIT 1];
        Assert.areEqual('Sudbury', leadAfterUpdate.city, 'City equal to payload city');
        Assert.areEqual('3-900 Lasalle Blvd', leadAfterUpdate.Street, 'Street equal to payload street1'); 
    }

    @isTest
    static void testUpdateLiveAcc() {
        Test.startTest();

        User testuser1 = UserUtil.getRandomTestUser();    
        TestDataFactory testHandler = new TestDataFactory();
        Account account = testHandler.createTestAccount();
        Contact contact = new Contact(MailingCountry = 'Canada', LastName ='test', Account = account, OwnerId = testuser1.Id);
        insert contact;


        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Account__c = account.Id;
        fxAccount.Contact__c = contact.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.OwnerId = testuser1.Id; 
        fxAccount.fxTrade_User_ID__c = 123123123;
        fxAccount.Net_Worth_Value__c = 10000;
        fxAccount.Liquid_Net_Worth_Value__c = 5000;
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = account.PersonEmail;
        fxAccount.Name = 'testusername';
        fxAccount.fxTrade_Global_ID__c = '123123123+' + String.valueOf(RecordTypeUtil.getFxAccountLiveId()).left(15);
        insert fxAccount;


        Assert.areEqual(10000, [SELECT Net_Worth_Value__c FROM fxAccount__c WHERE Id = :fxAccount.Id].Net_Worth_Value__c , 'Initial value of Net_Worth_Value__c');
        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(123123123, new List<String>{liveAcc});
    
        fxAccUserIdToReplayCounter.put(123123123, 0);
        // Test.startTest();
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);

        Test.stopTest();
        Assert.areEqual(5555, [SELECT Net_Worth_Value__c FROM fxAccount__c WHERE Id = :fxAccount.Id].Net_Worth_Value__c , 'Net_Worth_Value__c is updated');       
        Account acountAfterUpdate = [SELECT Id, BillingPostalCode, BillingCity, PersonMailingCity FROM Account WHERE Id = :account.Id];
        Assert.areEqual('Sudbury', acountAfterUpdate.BillingCity, 'City should equal to payload city');
        Assert.areEqual('P3A 5W8', acountAfterUpdate.BillingPostalCode, 'Postal Code should equal to payload street1');

    }

    @isTest
    static void testInsertDemoAccLiveExist() {
        Test.startTest();

        Lead lead = new Lead(
            LastName='test lead',
            Email='testEmail123123@oanda.com');
        insert lead;

        fxAccount__c fxAccount = new fxAccount__c();
        fxAccount.Lead__c = lead.Id;
        fxAccount.Funnel_Stage__c = FunnelStatus.READY_FOR_FUNDING;
        fxAccount.RecordTypeId = RecordTypeUtil.getFxAccountLiveId();
        fxAccount.Division_Name__c = 'OANDA Europe';
        fxAccount.Citizenship_Nationality__c = 'Canada';
        fxAccount.US_Shares_Trading_Enabled__c = false;
        fxAccount.Email__c = 'testEmail123123@oanda.com';
        fxAccount.Name = 'testusername';
        fxAccount.fxTrade_Global_ID__c = '123123123+' + String.valueOf(RecordTypeUtil.getFxAccountLiveId()).left(15);
        insert fxAccount;


        
        Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
        Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    
        fxAccUserIdToPayloads.put(123123124, new List<String>{demoAcc});
    
        fxAccUserIdToReplayCounter.put(123123123, 0);

        Assert.areEqual(1, [SELECT count() FROM fxAccount__c ] , '1 fxa ');  
        Assert.areEqual(1, [SELECT count() FROM Lead ] , '1 lead ');  

        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();
        fxAccUpsertHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);

        Test.stopTest();
        Assert.areEqual(2, [SELECT count() FROM fxAccount__c WHERE Email__c = 'testEmail123123@oanda.com'] , 'par of fxAccs');   
        Assert.areEqual(1, [SELECT count() FROM Lead ] , '1 lead ');      


    }

    @isTest
    static void TestIsErrorSubjectedToBeReplayed() {
        FxAccountUpsertTriggerHandler fxAccUpsertHandler = new FxAccountUpsertTriggerHandler();       

        Assert.isTrue( fxAccUpsertHandler.isErrorSubjectedToBeReplayed('ENTITY_IS_DELETED'), 'ENTITY_IS_DELETED should be true');
        Assert.isFalse( fxAccUpsertHandler.isErrorSubjectedToBeReplayed('AAA'), 'AAA should be false');
    }

    // private static FxAccDTO getFxAccPayload() {
    //     Address a = new Address();
    //     a.city = 'Sudbury';
    //     // a.country = 'Canada';
    //     a.postcode = 'P3A 5W8';
    //     a.region ='Ontario';
    //     a.street1 = '3-900 Lasalle Blvd';
    //     a.street2 = '';

    //     TradingExperience te = new TradingExperience();
    //     te.experience_duration = '0';
    //     te.experience_type = '1';

    //     TradingExperience te2 = new TradingExperience();
    //     te2.experience_duration = '50';
    //     te2.experience_type = '51';

    //     FxAccDTO fxA = new FxAccDTO();
    //     fxA.address = a;
    //     fxA.trading_experiences = new List<TradingExperience>{te,te2};
    //     fxA.username = 'testusername';
    //     fxA.net_worth_value = '6000';
    //     fxA.email ='testPOSTMETHOD@oanda.com';
    //     fxA.is_demo = false; 
    //     fxA.division_id = '1';
    //     fxA.employment_status = '5';
    //     fxA.birth_date = '1992-04-15';
    //     fxA.registration_date = 1698844934000l;
    //     fxA.one_id = 'ac081b5e-78b9-11ee-af79-42010a780137';
    //     fxa.declared_bankruptcy = false;
    //     fxa.agent = 'user-api';


    //     return fxA;

    // }




    // public class Address{
    //     public String city{ get; set; }
    //     public String country{ get; set; }
    //     public String postcode{ get; set; }
    //     public String region{ get; set; }
    //     public String street1{ get; set; }
    //     public String street2{ get; set; }
    // }
    
    // public class FxAccDTO{


    //     public List<TradingExperience> trading_experiences{ get; set; }
    //     public String username{ get; set; }
    //     public String net_worth_value{ get; set; }
    //     public String email{ get; set; }
    //     public boolean is_demo{ get; set; }
    //     public String division_id{ get; set; }
    //     public String employment_status{ get; set; }
    //     public String birth_date{ get; set; }
    //     public long registration_date{ get; set; }
    //     public String one_id{ get; set; }
    //     public boolean declared_bankruptcy{ get; set; }
    //     public Address address{ get; set; }
    //     public String agent{ get; set; }
        
    // }
    
    // public class TradingExperience{
    //     public String experience_duration{ get; set; }
    //     public String experience_type{ get; set; }
    // }
}