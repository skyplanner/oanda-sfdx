/**
 * @File Name          : DataCategoryTopArticlesHelper.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/21/2020, 1:53:28 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
global without sharing class DataCategoryTopArticlesHelper {

	global static List<DataCategoryWrapper> getFaqCategories(String division, String language) {
		FAQSettingsManager settingsMgr = FAQSettingsManager.getInstance();
		List<String> faqCategories = settingsMgr.faqCategories;
		if ((faqCategories == null) || (faqCategories.isEmpty())) {
			return new List<DataCategoryWrapper>();
		}
		//else...
		return getFaqCategories(faqCategories, division, language);
	}

	global static List<DataCategoryWrapper> getFaqCategories(
		List<String> faqCategories, 
		String division,
		String language
	) {
		Map<String,String> catMap = DataCategoriesHelper.getDataCategoriesMapByGroup(
			DataCategoriesConstants.ARTICLE_SOBJECT_NAME,
			DataCategoriesConstants.DAT_CAT_CATEGORIES
		);
		return getFaqCategories(faqCategories, catMap, division, language);
	}

	@TestVisible
	private static List<DataCategoryWrapper> getFaqCategories(
		List<String> faqCategories, 
		Map<String,String> catMap,
		String division,
		String language
	) {
		List<DataCategoryWrapper> result = new List<DataCategoryWrapper>();
		if ((faqCategories == null) || (catMap == null) || faqCategories.isEmpty() || catMap.isEmpty()) {
			return result;
		}
		//else...
		Map<String,Integer> dataCatIndexMap = new Map<String,Integer>();
		List<String> validFaqCategories = new List<String>();
		for(String ctName : faqCategories) {
			String label = catMap.get(ctName);
			if (label != null) {
				result.add(new DataCategoryWrapper(ctName, label));
				dataCatIndexMap.put(ctName, result.size() - 1);
				validFaqCategories.add(ctName);
			}
		}
		if (validFaqCategories.isEmpty()) {
			return result;
		}
		//else...
		List<Category_Top_Article__c> topArticles = [
			SELECT 
				DataCategoryName__c, KnoledgeArticleId__c, Order__c
			FROM 
				Category_Top_Article__c
			WHERE 
				DataCategoryName__c IN :validFaqCategories
			AND
				DivisionName__c = :division
		];
		if (topArticles.isEmpty()) {
			return result;
		}
		//else...
		Set<String> knoledgeArtIdSet = new Set<String>();
		Map<String,Object> knoledgeArtIdMap = new Map<String,Object>();
		Map<String,Integer> knoledgeArtIdCounter = new Map<String,Integer>();
		for(Category_Top_Article__c topArt : topArticles) {
			knoledgeArtIdSet.add(topArt.KnoledgeArticleId__c);
			Integer counter = knoledgeArtIdCounter.get(topArt.KnoledgeArticleId__c);
			if (counter == null) {
				counter = 0;
			} 
			if (counter == 0) {
				knoledgeArtIdMap.put(topArt.KnoledgeArticleId__c, topArt);
				//
			} else if (counter == 1) {
				Category_Top_Article__c current = (Category_Top_Article__c)knoledgeArtIdMap.get(topArt.KnoledgeArticleId__c);
				List<Category_Top_Article__c> tempList = new List<Category_Top_Article__c>();
				tempList.add(current);
				tempList.add(topArt);
				knoledgeArtIdMap.put(topArt.KnoledgeArticleId__c, tempList);
				//
			} else if (counter > 1) {
				List<Category_Top_Article__c> tempList = (List<Category_Top_Article__c>)knoledgeArtIdMap.get(topArt.KnoledgeArticleId__c);
				tempList.add(topArt);
			}
			knoledgeArtIdCounter.put(topArt.KnoledgeArticleId__c, ++counter);
		}
		List<FAQ__kav> faqArtList = getFaqArtList(knoledgeArtIdSet, language);
		if (faqArtList == null || faqArtList.isEmpty()) {
			return result;
		}
		//else...
		for(FAQ__kav faqArt : faqArtList) {
			Integer counter = knoledgeArtIdCounter.get(faqArt.KnowledgeArticleId);
			//Object temp = knoledgeArtIdMap.get(faqArt.KnowledgeArticleId);
			if (counter == 1) {
				Category_Top_Article__c catTopArt = (Category_Top_Article__c)knoledgeArtIdMap.get(faqArt.KnowledgeArticleId);
				addArticleToCatList(catTopArt, result, faqArt, dataCatIndexMap);
				//
			} else if (counter > 1) {
				List<Category_Top_Article__c> topArtList = (List<Category_Top_Article__c>)knoledgeArtIdMap.get(faqArt.KnowledgeArticleId);
				for(Category_Top_Article__c catTopArt : topArtList) {
					addArticleToCatList(catTopArt, result, faqArt, dataCatIndexMap);
				}
			}
		}
		return result;
	}

	private static List<FAQ__kav> getFaqArtList(Set<String> knoledgeArtIdSet, String language) {
		System.debug('getFaqArtList -> language: ' + language);
		List<FAQ__kav> faqArtList = null;
		String artQuery = 
			'SELECT Id, KnowledgeArticleId, Title, UrlName ' +
			' FROM FAQ__kav ' +
			' WHERE PublishStatus = \'Online\' ' +
			' AND KnowledgeArticleId IN :knoledgeArtIdSet ' +
			(
				String.isBlank(language)
				? ' AND IsMasterLanguage = true '
				: ' AND Language = \'' + String.escapeSingleQuotes(language) + '\' '
			);
		try{
           faqArtList = Database.query(artQuery);
		}
		catch(Exception e){
            return null;
		}		
		return faqArtList;
	}

	private static void addArticleToCatList(Category_Top_Article__c catTopArt, 
											List<DataCategoryWrapper> catList, 
											FAQ__kav faqArt,
											Map<String,Integer> dataCatIndexMap) {
		//
		Integer dataCatIndex = dataCatIndexMap.get(catTopArt.DataCategoryName__c);
		DataCategoryWrapper datCatWrapper = catList.get(dataCatIndex);
		datCatWrapper.addArticle(
			new TopArticleWrapper(
				catTopArt.Id,
				faqArt.KnowledgeArticleId,
				faqArt.Id,
				faqArt.Title,
				faqArt.UrlName,
				catTopArt.Order__c.intValue()
			)
		);
	}

}