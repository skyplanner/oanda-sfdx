/**
 * User API 'Disable Balance Interest' action manager class
 */
public class UserApiActionDisableBalInterest extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        Id fxAccountId = getfxAccountId(record);
        EnvironmentIdentifier identifier = getfxTradeUserId(record);
        UserApiUserPatchRequest.UserPatchRequestWrapper wrapper 
            = UserApiUserPatchRequest.getDisableBalanceInterestWrapper();
        UserCallout userCallout = new UserCallout();
        userCallout.updateUser(identifier, wrapper.user);
        String action = getActionLabel(record);
        auditTrailManager.saveAuditTrail(
            identifier.id, fxAccountId, action, wrapper.change);

        return action + ' action success!!!';
    }
    
    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.allInterestEnabled &&
            user.user.balanceInterestEnabled;
    }
}