/**
 * @File Name          : HelpPortalRichText_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/4/2021, 2:10:41 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/3/2021, 4:15:00 PM   acantero     Initial Version
**/
@isTest
private without sharing class HelpPortalRichText_Test {

    public static final String FOLDER_NAME = 'HelpPortal';
    public static final String TEST_LANGUAGE_CODE = 'en_US';
    public static final String TEST_DOCUMENT_PREFIX = 'testDocPrefix2020';
    public static final String TEST_DIVISION_CODE = 'OC';

    @testSetup
    static void setup() {
        String dataChatOff = '<p>This is an example with a link</p>'+
                             '<a href="http://staticlink_deposits" target="_blank">Link Here</a>';
        ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_NAME LIMIT 1][0].Id;
        String docName = HelpPortalRichText.getDocumentName(
            TEST_LANGUAGE_CODE, 
            TEST_DOCUMENT_PREFIX, 
            TEST_DIVISION_CODE
        );
        Document docTest = new Document(
            Name = 'testDocument2020.html',
            ContentType = 'html',
            DeveloperName = docName,
            FolderId = folderId,
            Body = Blob.valueOf(dataChatOff)
        );
        insert docTest;
    }

    @isTest
	static void getHtmlLinkFixed() {
        // String dataChatOff = '<p>This is an example with a link</p>'+
        //                      '<a href="http://staticlink_deposits" target="_blank">Link Here</a>';
        // ID folderId = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_NAME LIMIT 1][0].Id;
        // Document docTest = new Document(
        // Name = 'chatOfflineText.html',
        // ContentType = 'html',
        // DeveloperName = 'OC_en_US_chatOfflineText',
        // FolderId = folderId,
        // Body = Blob.valueOf(dataChatOff)
        // );
        // insert docTest;
   		Test.startTest();
        HelpPortalRichText chatOfflineText = new HelpPortalRichText(
            TEST_LANGUAGE_CODE, 
            TEST_DOCUMENT_PREFIX, 
            TEST_DIVISION_CODE
        );		
        String chatOfflineTextHtml = chatOfflineText.getHtmlLinkFixed(TEST_DIVISION_CODE);
       	Test.stopTest();
		System.assert(chatOfflineTextHtml != '');
	}

    //documentPrefix is null
    @isTest
    static void helpPortalRichText1() {
        Boolean error = false;
        Test.startTest();
        try {
            HelpPortalRichText instance = new HelpPortalRichText(
                TEST_LANGUAGE_CODE, 
                null, 
                TEST_DIVISION_CODE
            );
            //...
        } catch (HelpPortalRichText.HelpPortalRichTextException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }

    @isTest
    static void getHtml() {
        Test.startTest();
        HelpPortalRichText instance = new HelpPortalRichText(
            TEST_LANGUAGE_CODE, 
            TEST_DOCUMENT_PREFIX, 
            TEST_DIVISION_CODE
        );
        String result = instance.getHtml();
        Test.stopTest();
        System.assertNotEquals(null, result);
    }

    //update existint document
    @isTest
    static void saveHtml1() {
        String newBody = '<p>Hello</p>';
        Test.startTest();
        HelpPortalRichText instance = new HelpPortalRichText(
            TEST_LANGUAGE_CODE, 
            TEST_DOCUMENT_PREFIX, 
            TEST_DIVISION_CODE
        );
        instance.saveHtml(newBody);
        String result = instance.getHtml();
        Test.stopTest();
        System.assertEquals(newBody, result);
    }

    //insert new document
    @isTest
    static void saveHtml2() {
        String newBody = '<p>Hello</p>';
        Test.startTest();
        HelpPortalRichText instance = new HelpPortalRichText(
            TEST_LANGUAGE_CODE, 
            TEST_DOCUMENT_PREFIX + 'A', 
            TEST_DIVISION_CODE
        );
        instance.saveHtml(newBody);
        String result = instance.getHtml();
        Test.stopTest();
        System.assertEquals(newBody, result);
    }

    //folder does not exists
    @isTest
    static void checkRequiredFolder() {
        Boolean error = false;
        Test.startTest();
        try {
            HelpPortalRichText instance = new HelpPortalRichText('en_US', 'chatOfflineText', 'OC');
            instance.checkRequiredFolder(null);
            //...
        } catch (HelpPortalRichText.HelpPortalRichTextException e) {
            error = true;
        }
        Test.stopTest();
        System.assertEquals(true, error);
    }
    
}