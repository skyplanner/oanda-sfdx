/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 09-14-2022
 * @last modified by  : Yaneivys Gutierrez
**/
public inherited sharing class CASearchDetailsMedia {
    public String datee { get; set; }

    public String pdf_url { get; set; }

    public String snippet { get; set; }

    public String title { get; set; }

    public String url { get; set; }
}