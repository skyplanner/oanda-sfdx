/**
 * @File Name          : EntitlementSettings_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/20/2021, 12:58:41 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/17/2021, 9:50:48 PM   acantero     Initial Version
**/
@isTest
private without sharing class EntitlementSettings_Test {

    @isTest
    static void test() {
        Boolean requiredSettingException = false;
        Test.startTest();
        String chatEntitlementId = EntitlementSettings.getChatEntitlementId();
        String phoneEntitlementId = EntitlementSettings.getPhoneEntitlementId();
        String emailFrontdeskEntitlementId = EntitlementSettings.getEmailFrontdeskEntitlementId();
        String emailApiEntitlementId = EntitlementSettings.getEmailApiEntitlementId();
        String emailChatbotEntitlementId = EntitlementSettings.getEmailChatbotEntitlementId();
        try {
            String requiredResult = EntitlementSettings.getEntitlementId('fake settings name', true);
        } catch (EntitlementSettings.EntitlementSettingsException ex) {
            requiredSettingException = true;
        }
        String notRequiredResult = EntitlementSettings.getEntitlementId('fake settings name', false);
        Test.stopTest();
        System.assertNotEquals(null, chatEntitlementId);
        System.assertNotEquals(null, phoneEntitlementId);
        System.assertNotEquals(null, emailFrontdeskEntitlementId);
        System.assertNotEquals(null, emailApiEntitlementId);
        System.assertNotEquals(null, emailChatbotEntitlementId);
        System.assertEquals(true, requiredSettingException);
        System.assertEquals(null, notRequiredResult);
    }

}