public abstract class ComplyAdvantageSearchTriggerHandler 
{
    protected boolean isInsert;
    protected boolean isUpdate;
    protected boolean isBefore;
    protected boolean isAfter;
    protected list<Comply_Advantage_Search__c> trgNew;
    protected Map<Id, Comply_Advantage_Search__c> trgNewMap;
    protected Map<Id, Comply_Advantage_Search__c> trgOldMap;

    protected set<Id> complyAdvantageSearchIds;
 
    protected set<Id> realtedFxAccountIds;
    protected Map<Id,fxAccount__c> relatedFxAccountsMap;

    protected ComplyAdvantageSearchTriggerDataHandler triggerDataHandler;

    public static set<string> relatedFxAccountFields = new set<string>
    {
        'Id',
        'Is_CA_Name_Search_Monitored__c',
        'Is_CA_Alias_Search_Monitored__c',
        'CA_Match_Status__c',
        '(select Id, Is_Alias_Search__c, Entity_Contact__c, Is_Monitored__c From Comply_Advantage_Searches__r)'
    };

    public ComplyAdvantageSearchTriggerHandler(boolean trgInsert,
                                                boolean trgUpdate,
                                                boolean trgIsBefore,
                                                boolean trgIsAfter,
                                                list<Comply_Advantage_Search__c> triggerNew,
                                                Map<Id, Comply_Advantage_Search__c> triggerOldMap) 
    {
        triggerDataHandler = new ComplyAdvantageSearchTriggerDataHandler();
        complyAdvantageSearchIds = new set<Id>{};

        this.isInsert = trgInsert;
        this.isUpdate = trgUpdate;
        this.isBefore = trgIsBefore;
        this.isAfter = trgIsAfter;
        this.trgNew = triggerNew;
        this.trgOldMap = triggerOldMap;

        for(Comply_Advantage_Search__c search: trgNew)
        {
            if(string.isNotBlank(search.Id))
            {
                complyAdvantageSearchIds.add(search.Id);
            }
        }
    }

    public void populateRelatedFxaccounts()
    {
        realtedFxAccountIds = new set<Id>{};
        relatedFxAccountsMap = new Map<Id,fxAccount__c>{};

        for(Comply_Advantage_Search__c caSearch : trgNew)
        {
            realtedFxAccountIds.add(caSearch.fxAccount__c);
        }

        if(!complyAdvantageSearchIds.isEmpty())
        {
            string fxaccountQuery = 'Select ' +  soqlUtil.getSoqlFieldList(relatedFxAccountFields) + ' From fxAccount__c Where Id IN :realtedFxAccountIds';
            for(fxAccount__c fxAc : Database.query(fxaccountQuery))
            {
                relatedFxAccountsMap.put(fxAc.Id, fxAc);
            }
        }
    }
}