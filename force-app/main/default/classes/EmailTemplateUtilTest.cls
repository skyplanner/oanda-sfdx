/**
 * Tests: EmailTemplateUtil
 * @author Fernando Gomez
 */
@isTest
private class EmailTemplateUtilTest {
	
	/**
	 * public static EmailTemplate getTemplateByLanguage(
	 *		String namePrefix, String language)
	 */
	@isTest
	static void getTemplateByLanguage() {
		System.assertEquals('Case_Authentication_Code_Spanish',
			EmailTemplateUtil.getTemplateByLanguage(
				'Case_Authentication_Code', 'Spanish').DeveloperName);
		
		System.assertEquals(null,
			EmailTemplateUtil.getTemplateByLanguage(
				'Case_Authentication_Code', 'No Language'));
	}
}