/**
 * User API 'Unlock Tpa Authorization' action manager class
 */
public class UserApiActionUnlockTpaAuth extends UserApiActionBase {
    protected override String call(Map<String, Object> record) {
        UserActionCall uac = new UserActionCall();
        uac.action = getActionLabel(record);
        uac.statusWrapper = UserApiStatus.unlockTpaAuthorization();
        uac.record = record;
        return callUserAction(uac);
    }

    protected override Boolean isVisible(Map<String, Object> record) {
        UserApiUserGetResponse user = getUser(record);

        return user.user_status.tpaAuthorizationLocked;
    }
}