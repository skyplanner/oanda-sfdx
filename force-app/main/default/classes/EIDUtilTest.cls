/**
 *	@author : Gilbert Gao
 *	Created Date : Jan 15, 2017
 *
*/

@IsTest
private class EIDUtilTest{

   final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);

    @TestSetup static void init(){
    
    List<fxAccount__c> fxas = new List<fxAccount__c>();

		//Preload Test with some test data
		TestDataFactory testHandler = new TestDataFactory();
		fxas.add(testHandler.createFXTradeAccount(testHandler.createTestAccount()));


		Lead ld = testHandler.createLiveLeads('test', 'test@abc.com');
		insert ld;
		
		fxas.add(testHandler.createFXTradeAccount(ld));

    for(fxAccount__c fxa : fxas){
      fxa.Division_Name__c = 'OANDA Canada';
    }

    update fxas;
	}

    //An failed EID result will create a new closed onboarding case on live lead
  @IsTest
  static void testInsertEIDResult_1(){
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Lead__c != NULL LIMIT 1];

        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          //initial failed EID result
          EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
          insert eid;
          
          eid = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid.Id];

          //check if an onboarding case is created
          Case c = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];
          
          System.assertEquals('EID Verification', c.Origin);
          System.assertNotEquals(REGISTRATION_USER.Id, c.OwnerId);
          System.assertEquals(fxa.Lead__c, c.Lead__c);
          System.assertEquals(fxa.Id, c.fxAccount__c);
          System.assertEquals(c.Id, eid.Case__c);
          System.assertEquals('Closed', c.Status);
          
          //the second Passed EID result
          EID_Result__c eid1 = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', fxAccount__c = fxa.Id);
          insert eid1;

          eid1 = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid1.Id];
          Case c1 = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];

          System.assertEquals(c.Id, c1.Id);
          System.assertEquals('Closed', c1.Status);

          Test.stopTest();

      }
	}

    //An failed EID result will create a new closed onboarding case on account
  @IsTest
  static void testInsertEIDResult_2(){
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Account__c != NULL LIMIT 1];

        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          //initial failed EID result
          EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
          insert eid;
          
          eid = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid.Id];

          //check if an onboarding case is created
          Case c = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];
          
          System.assertEquals('EID Verification', c.Origin);
          System.assertNotEquals(REGISTRATION_USER.Id, c.OwnerId);
          System.assertEquals(fxa.Account__c, c.AccountId);
          System.assertEquals(fxa.Id, c.fxAccount__c);
          System.assertEquals(c.Id, eid.Case__c);
          System.assertEquals('Closed', c.Status);
          
          //the second Passed EID result
          EID_Result__c eid1 = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', fxAccount__c = fxa.Id);
          insert eid1;

          eid1 = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid1.Id];
          Case c1 = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];

          System.assertEquals(c.Id, c1.Id);
          System.assertEquals('Closed', c1.Status);

          Test.stopTest();

      }
	}

  //A failed EID result is attached to an existing onboarding case, which is created due to document uploading
  @IsTest
  static void testInsertEIDResult_3(){
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Account__c != NULL LIMIT 1];
      
        System.runAs(REGISTRATION_USER) {
          Test.startTest();
          
          //create the onboarding case due to document uploading
          Document__c d = new Document__c(Name = 'Test', RecordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
          insert d;

          Case c = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];

          System.assertEquals('Document Upload', c.Origin);
          System.assertEquals('Open', c.Status);

          //attach EID result
          EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
          insert eid;

          Test.stopTest();

          Case c1 = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];
          eid = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid.Id];

          System.assertEquals(c.Id, c1.Id);
          System.assertEquals('Document Upload', c1.Origin);
          System.assertEquals('Open', c1.Status);
          System.assertEquals(fxa.Account__c, c1.AccountId);
          System.assertEquals(fxa.Id, c1.fxAccount__c);
          System.assertEquals(c1.Id, eid.Case__c);

      }
	}

  //fxAccount is FROM a non-configured division
  //fxAccount is under a live lead
  @IsTest
  static void testInsertEIDResult_4(){
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c, Division_Name__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Lead__c != NULL LIMIT 1];
        fxa.Division_Name__c = 'abc';

        update fxa;

        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          //initial failed EID result
          EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
          insert eid;

          //check if an onboarding case is created
          List<Case> cases = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId()];
          
          System.assertEquals(0, cases.size());
        
          
          //the second Passed EID result
          EID_Result__c eid1 = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', fxAccount__c = fxa.Id);
          insert eid1;

          List<Case> cases1 = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];

          System.assertEquals(0, cases1.size());

          Test.stopTest();

      }
  }


  //fxAccount is FROM a non-configured division
  //fxAccount is under an account
  @IsTest
  static void testInsertEIDResult_5(){
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c, Division_Name__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Account__c != NULL LIMIT 1];
        fxa.Division_Name__c = 'abc';
        update fxa;

        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          //initial failed EID result
          EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
          insert eid;

          //check if an onboarding case is created
          List<Case> cases = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];
          
          System.assertEquals(0, cases.size());
          
          //the second Passed EID result
          EID_Result__c eid1 = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', fxAccount__c = fxa.Id);
          insert eid1;

          List<Case> cases1 = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE RecordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];

          System.assertEquals(0, cases1.size());

          Test.stopTest();

      }
  }

/*
    //fxAccount is in OAU 
    @IsTest
    static void testInsertEIDResultOAU() {
        fxAccount__c fxa = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE recordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Lead__c != NULL LIMIT 1];
        fxa.Division_Name__c = 'OANDA Australia';
        update fxa;
        
        System.runAs(REGISTRATION_USER) {
            Test.startTest();
            
            //Initial EID result
            EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', Reason__c = '0102', fxAccount__c = fxa.Id);
            insert eid;
            
            eid = [SELECT Id, Case__c, fxAccount__c FROM EID_Result__c WHERE Id = :eid.Id];
            
            //Check if an onboarding case is created
            Case c = [SELECT Id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c, Status FROM Case WHERE recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() LIMIT 1];
            
            System.assertEquals('EID Verification', c.Origin);
            system.assertNotEquals(REGISTRATION_USER.Id, c.OwnerId);
            System.assertEquals(fxa.Lead__c, c.Lead__c);
            System.assertEquals(fxa.Id, c.fxAccount__c);
            System.assertEquals(c.Id, eid.Case__c);
            System.assertEquals('Open', c.Status);            //Status should be open when a new EID enters
        }
    }
*/
	//EID_Pass_Count checks for the number of EID passes for a given fxAccount
	@IsTest
	static void testInsertEIDResultPassCount() {
		fxAccount__c fxa = [SELECT Id, Account__c, Lead__c FROM fxAccount__c WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Lead__c != NULL LIMIT 1];
		update fxa;
		
		System.runAs(REGISTRATION_USER) {
			Test.startTest();
			
			//Add one passed EID result AND one failed EID result
			List<EID_Result__c> eids = new List<EID_Result__c>();
			eids.add(new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', Reason__c = '0102', fxAccount__c = fxa.Id));
			eids.add(new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id));
			insert eids;
			
			fxa = [SELECT Id, EID_Pass_Count__c FROM fxAccount__c WHERE Id = :fxa.Id LIMIT 1];
			System.assertEquals(fxa.EID_Pass_Count__c, 1);
		}
	}

  @IsTest
  static void testBeforeInsertDataUpdatefxAccount() {
    fxAccount__c fxa = [
      SELECT Id,
        Account__c,
        Lead__c,
        Division_Name__c
      FROM fxAccount__c
      WHERE RecordTypeId = :RecordTypeUtil.getFxAccountLiveId() AND Account__c != NULL
      LIMIT 1];
    fxa.First_Name__c = 'Qwertyuiop';
    fxa.Last_Name__c = 'Poiuytrewq';
    fxa.Birthdate__c = Date.today();
    fxa.Street__c = 'Kendale 62';
    fxa.City__c = 'Miami';
    fxa.State__c = 'Fl';
    fxa.Postal_Code__c = '33152';
    fxa.Country_Name__c = 'US';
    update fxa;

    Test.startTest();

    //initial failed EID result
    EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', fxAccount__c = fxa.Id);
    insert eid;

    Test.stopTest();

    eid = [
      SELECT Id,
        First_Name_PD__c,
        Last_Name_PD__c,
        Birthdate_PD__c,
        Street_PD__c,
        City_PD__c,
        State_PD__c,
        Postal_Code_PD__c,
        Country_PD__c,
        Account__c,
        Lead__c
      FROM EID_Result__c
      WHERE Id = :eid.Id
      LIMIT 1];

      System.assertEquals(fxa.First_Name__c, eid.First_Name_PD__c, 'Wrong First Name associated');
      System.assertEquals(fxa.Last_Name__c, eid.Last_Name_PD__c, 'Wrong Last Name associated');
      System.assertEquals(fxa.Birthdate__c, eid.Birthdate_PD__c, 'Wrong Birthdate associated');
      System.assertEquals(fxa.Street__c, eid.Street_PD__c, 'Wrong Street associated');
      System.assertEquals(fxa.City__c, eid.City_PD__c, 'Wrong City associated');
      System.assertEquals(fxa.State__c, eid.State_PD__c, 'Wrong State associated');
      System.assertEquals(fxa.Postal_Code__c, eid.Postal_Code_PD__c, 'Wrong Postal Code associated');
      System.assertEquals(fxa.Country_Name__c, eid.Country_PD__c, 'Wrong Country associated');
      System.assertEquals(fxa.Account__c, eid.Account__c, 'Wrong Account associated');
      System.assertEquals(fxa.Lead__c, eid.Lead__c, 'Wrong Account associated');
  }

  @IsTest
  static void testBeforeInsertDataUpdateAccount() {
    TestDataFactory testHandler = new TestDataFactory();
    Account a = testHandler.createTestAccountsEid();

    Test.startTest();

    //initial failed EID result
    EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', Account__c = a.Id);
    insert eid;

    Test.stopTest();

    eid = [
      SELECT Id,
        First_Name_PD__c,
        Last_Name_PD__c,
        Birthdate_PD__c,
        Street_PD__c,
        City_PD__c,
        State_PD__c,
        Postal_Code_PD__c,
        Country_PD__c
      FROM EID_Result__c
      WHERE Id = :eid.Id
      LIMIT 1];

      System.assertEquals(a.FirstName, eid.First_Name_PD__c, 'Wrong First Name associated');
      System.assertEquals(a.LastName, eid.Last_Name_PD__c, 'Wrong Last Name associated');
      System.assertEquals(a.PersonMailingStreet, eid.Street_PD__c, 'Wrong Street associated');
      System.assertEquals(a.PersonMailingCity, eid.City_PD__c, 'Wrong City associated');
      System.assertEquals(a.PersonMailingState, eid.State_PD__c, 'Wrong State associated');
      System.assertEquals(a.PersonMailingPostalCode, eid.Postal_Code_PD__c, 'Wrong Postal Code associated');
      System.assertEquals(a.PersonMailingCountry, eid.Country_PD__c, 'Wrong Country associated');
  }

  @IsTest
  static void testBeforeInsertDataUpdateLead() {
    TestDataFactory testHandler = new TestDataFactory();
    Lead l = testHandler.createTestLeadEid();

    Test.startTest();

    //initial failed EID result
    EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Failed', Reason__c = '0102', Lead__c = l.Id);
    insert eid;

    Test.stopTest();

    eid = [
      SELECT Id,
        First_Name_PD__c,
        Last_Name_PD__c,
        Birthdate_PD__c,
        Street_PD__c,
        City_PD__c,
        State_PD__c,
        Postal_Code_PD__c,
        Country_PD__c
      FROM EID_Result__c
      WHERE Id = :eid.Id
      LIMIT 1];

      System.assertEquals(l.FirstName, eid.First_Name_PD__c, 'Wrong First Name associated');
      System.assertEquals(l.LastName, eid.Last_Name_PD__c, 'Wrong Last Name associated');
      System.assertEquals(l.Street, eid.Street_PD__c, 'Wrong Street associated');
      System.assertEquals(l.City, eid.City_PD__c, 'Wrong City associated');
      System.assertEquals(l.State, eid.State_PD__c, 'Wrong State associated');
      System.assertEquals(l.PostalCode, eid.Postal_Code_PD__c, 'Wrong Postal Code associated');
      System.assertEquals(l.Country, eid.Country_PD__c, 'Wrong Country associated');
  }
  @IsTest
  static void testReopenCase() {

    List<fxAccount__c> fxaccs = [SELECT Id, Account__c, Lead__c FROM fxAccount__c LIMIT 2];
    fxaccs[0].Division_Name__c=Constants.DIVISIONS_OANDA_AUSTRALIA;
    fxaccs[1].Division_Name__c=Constants.DIVISIONS_OANDA_ASIA_PACIFIC;
    update fxaccs;
    List<Case> cases = new List<Case>();
    cases.add(new Case(Subject='Test', Status='Closed'));
    cases.add(new Case(Subject='Test', Status='Closed'));
    insert cases;
    Test.startTest();

    EID_Result__c eid = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', Reason__c = '0102',
            fxAccount__c=fxaccs[0].Id, Case__c=cases[0].Id);
    insert eid;

    EID_Result__c eid2 = new EID_Result__c(Provider__c = 'EquifaxUS', Date__c = Datetime.now(), Result__c = 'Passed', Reason__c = '0102',
            fxAccount__c=fxaccs[1].Id, Case__c=cases[1].Id);
    insert eid2;

    Test.stopTest();
    Case c = [SELECT Id, Status FROM Case WHERE Id =:cases[0].Id];
    Case c2 = [SELECT Id, Status FROM Case WHERE Id =:cases[1].Id];

    System.assertEquals(c.Status, 'Open');
    System.assertEquals(c2.Status, 'Closed');
  }

}