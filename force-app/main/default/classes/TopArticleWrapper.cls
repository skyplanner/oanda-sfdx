/**
 * @author Ariel Cantero
 * @version 1.0
 * @since 7/1/2019
 */
global class TopArticleWrapper {
	@AuraEnabled
	global String topArtId {get; set;}
	@AuraEnabled
	global String knowledgeArtId {get; set;}
	@AuraEnabled
	global String artId {get; set;}
	@AuraEnabled
	global String title {get; set;}
	@AuraEnabled
	global String url {get; set;}
	@AuraEnabled
	global Integer order {get; set;}

	global TopArticleWrapper(String topArtId, 
							String knowledgeArtId,
							String artId, 
							String title, 
							String url, 
							Integer order) {
		//
		this.topArtId = topArtId;
		this.knowledgeArtId = knowledgeArtId;
		this.artId = artId;
		this.title = title;
		this.url = url;
		this.order = order;
	}
}