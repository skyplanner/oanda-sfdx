@isTest(SeeAllData=true)
private with sharing class viewShareSurveyComponentController_Test {
    private static testmethod void testShareSurvey()
    {
        Survey__c mySurvey = new Survey__c();
        mySurvey.Submit_Response__c = 'empty';
        insert mySurvey;

        viewShareSurveyComponentController vss = new viewShareSurveyComponentController();
        vss.surveyId = mySurvey.Id;

        vss.selectedURLType = 'Chatter';
        String surverySiteUrl = vss.surveySite;
        String VFUrl = vss.getVFUrl();
        System.assertEquals(vss.getVFUrl(), vss.surveyURLBase);
        System.assertEquals('id=' + mySurvey.Id + '&cId=none&caId=none', vss.surveyURL);
        System.assertEquals(VFUrl, surverySiteUrl);

        vss.selectedURLType = 'Email Link w/ Contact Merge';
        System.assertEquals('id=' + mySurvey.Id + '&cId={!Contact.Id}&caId=none', vss.surveyURL);

        vss.selectedURLType = 'Email Link w/ Contact & Case Merge';
        System.assertEquals('id=' + mySurvey.Id +  '&cId={!Contact.Id}&caId={!Case.id}', vss.surveyURL);
       
        List < Selectoption > listSelectedOptions = vss.sitesPicklist;
        System.assert(listSelectedOptions != null);

    }

    private static testmethod void testResultController()
    {
        Survey__c mySurvey = new Survey__c();
        mySurvey.Submit_Response__c = 'empty';  
        insert mySurvey;
        
        viewSurveyResultsComponentController vsr = new viewSurveyResultsComponentController();
        vsr.surveyId = mySurvey.Id;
        
        String mySurveyId = mySurvey.Id;
        PageReference pageRef = new PageReference ('/' + vsr.reportId + '?pv0=' + mySurveyId.substring(0,15));
        System.assertEquals(pageRef.getURL(),vsr.getResults().getURL());
        
    }
}