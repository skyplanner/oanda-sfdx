/**
 * @File Name          : SPQueueUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2022, 1:07:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2021, 6:19:14 PM   acantero     Initial Version
**/
@isTest
private without sharing class SPQueueUtil_Test {

    static final String FAKE_QUEUE_NAME = 'Fake Queue Name 2021';

	@testSetup
    static void setup() {
        Group testGroup = new Group(
            Name = FAKE_QUEUE_NAME, 
            Type = 'Queue'
        );
        insert testGroup;
    }
    
    @isTest
    static void test() {
        Group testQueue = getQueue(FAKE_QUEUE_NAME);
        String devName = testQueue.DeveloperName;
        Boolean throwException = false;
        Test.startTest();
        String result1 = SPQueueUtil.getQueueIdByDevName(devName, true);
        Boolean result2 = SPQueueUtil.ownerIsAQueue(result1);
        Set<ID> result3 = SPQueueUtil.getQueueIdSetByDevNames(
            new List<String>{ devName }
        );
        Group result4 = SPQueueUtil.getQueueByDevName(devName, true);
        Test.stopTest();
        System.assert(String.isNotBlank(result1));
        System.assertEquals(true, result2);
        System.assertEquals(1, result3.size());
        System.assert(result3.contains(testQueue.Id));
        System.assertEquals(testQueue.Id, result4.Id);
    }

    // queueDevName is invalid  (no queue found) => throw exception
    @isTest
    static void getQueueIdByDevName() {
        Boolean throwException = false;
        Test.startTest();
        try {
            String result = SPQueueUtil.getQueueIdByDevName('Fake Queue DevName', true);
            //...
        } catch (Exception ex) {
            throwException = true;
        }
        Test.stopTest();
        System.assertEquals(true, throwException);
    }

    // queueDevName is invalid  (no queue found) => throw exception
    @isTest
    static void getQueueByDevName() {
        Boolean throwException = false;
        Test.startTest();
        try {
            Group result = SPQueueUtil.getQueueByDevName('Fake Queue DevName', true);
            //...
        } catch (Exception ex) {
            throwException = true;
        }
        Test.stopTest();
        System.assertEquals(true, throwException);
    }

    static Group getQueue(String queueName) {
        return [
            select Id, DeveloperName 
            from Group 
            where Name = :queueName
            limit 1
        ];
    }
    
}