/**
*  Reassign all PHVC clients to Shanice Yin (SP-13303)
    PHVC clients:

   	1. Mailing Country equals singapore
	2. Account Owner does not contain Gwen Chen,Ryan Tan,Hong Liu,Shanice
	3. fxAccount: Closed? equals False
	4. fxAccount: Balance USD greater or equal "10,000"
	5. fxAccount: Trade Volume USD Last 7 Days greater or equal "2,000,000"
	6. Current Trade Volume USD MTD greater or equal "5,000,000"
	7. Person Account: FXS/RM Requested does not contain Gwen Chen,Hong Liu, Ryan Tan
	8. fxAccount: Funnel Stage equals Traded
	((1 AND 2 AND 3 AND (4 OR 5 OR 6)) AND 7) AND 8
*/

public with sharing class BatchReassignPVCClients implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable{
	
	String query;
	String phvcClientsQueue = 'PHVC Clients - OAP';
	String oapRelationshipManagersQueue = 'OAP Relationship Managers';
	String funnelTraded = Constants.FUNNEL_TRADED;
	public Set<String> divisions_OAP = new set<String>
    {
        'OANDA Asia Pacific',
        'OANDA Asia Pacific CFD'
    };
	public static final Integer BATCH_SIZE = 100;
	public static final String CRON_NAME,CRON_SCHEDULE;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	

    public BatchReassignPVCClients() {
		query = 'SELECT id, OwnerId, Account__r.Id, Account__r.PersonMailingCountry, Account__r.FXS_RM_Requested__pc, Balance_USD__c, Trade_Volume_USD_Last7d__c, Funnel_Stage__c,'+
		+' Is_Closed__c, Account__r.Client_Value_Indicator_Score__c,Trade_Volume_USD_MTD__c, Notes__c FROM fxAccount__c WHERE last_trade_date__c = LAST_N_DAYS:30 AND '+
		'Account__r.PersonMailingCountry=\'Singapore\' AND Is_Closed__c=false AND Funnel_Stage__c=:funnelTraded AND Division_Name__c = :divisions_OAP'+
		' AND (Balance_USD__c>=10000 OR Trade_Volume_USD_Last7d__c >= 2000000 OR Trade_Volume_USD_MTD__c >=5000000)';
    }

	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT id, OwnerId, Account__r.Id, Account__r.PersonMailingCountry, Account__r.FXS_RM_Requested__pc, Balance_USD__c, Trade_Volume_USD_Last7d__c, Funnel_Stage__c,'+
		+' Is_Closed__c, Account__r.Client_Value_Indicator_Score__c,Trade_Volume_USD_MTD__c, Notes__c FROM fxAccount__c WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}

	public static void schedule() {
    	System.schedule(CRON_NAME, CRON_SCHEDULE, new BatchReassignPVCClients());
  	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
 
    	return Database.getQueryLocator(query);
	}

    public void execute(SchedulableContext context) {
        Database.executeBatch(new BatchReassignPVCClients(), BATCH_SIZE);
    }

   	public void execute(Database.BatchableContext bc, List<SObject> batch) {
   		
   		List<fxAccount__c> fxAccounts = (List<fxAccount__c>) batch;
   		List<Account> accounts = new List<Account>();
		String phvcClientsQueueId = UserUtil.getQueueId(phvcClientsQueue);
		if(phvcClientsQueueId == null) {
			throw new ApplicationException(phvcClientsQueue+' queue does not exist.');
		}
		String managersQueueId = UserUtil.getQueueId(oapRelationshipManagersQueue);
		List<Id> userIds = RoundRobinAssignment.getSortedUserIds(phvcClientsQueueId);
		List<Id> managersIds = new List<Id>();
		if(managersQueueId!=null) managersIds = RoundRobinAssignment.getSortedUserIds(managersQueueId);
		Id newOwnerId = RoundRobinAssignment.getNextOwnerId(phvcClientsQueueId);
   		for(fxAccount__c acct : fxAccounts){
   			if(!userIds.contains(acct.OwnerId) && !managersIds.contains(acct.OwnerId) &&
			(managersIds.isEmpty() || !managersIds.contains(acct.Account__r.FXS_RM_Requested__pc))){
				Account acc = new Account(Id= acct.Account__r.Id, OwnerId= newOwnerId);
				accounts.add(acc);
   			}
   			
   		}
		update accounts;
   		
	}

	public void finish(Database.BatchableContext bc) {
		BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
	}

}