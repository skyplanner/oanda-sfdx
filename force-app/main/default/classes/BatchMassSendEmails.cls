/**
 * Created by akajda on 04/01/2024.
 */

public with sharing class BatchMassSendEmails implements Database.Batchable<SObject>, Database.RaisesPlatformEvents, BatchReflection {
        String query;
        Boolean isRerun = false;
        List<Id> ids = new List<Id>();
        String emailTemplate ='';
        String queueName ='';
        String caseSubject='';
        String caseType='';
        public static final String FRONTDESK_MAIL = 'frontdesk@oanda.com';

        public BatchMassSendEmails(List<Id> accIds, String emailTemplate, String queueName, String caseSubject, String caseType) {
            ids=accIds;
            this.emailTemplate = emailTemplate;
            this.queueName = queueName;
            this.caseSubject = caseSubject;
            this.caseType = caseType;
            query='SELECT Id, PersonContactId FROM Account WHERE Id IN:ids';
        }
        public Id rerunSetup(List<Id> recIds, Integer batchSize){
            ids = recIds;
            query = 'SELECT Id, PersonContactId FROM Account WHERE Id IN:ids';
            isRerun=true;
            return Database.executeBatch(this, batchSize);
        }

        public Database.QueryLocator start(Database.BatchableContext bc) {
            return Database.getQueryLocator(query);
        }

        public void execute(Database.BatchableContext bc, List<SObject> batch) {
            List<Account> accs = (List<Account>) batch;
            List<Case> cases = new List<Case>();
            Map<Id,String> personContactsByAccountId = new Map<Id,String>();
            for(Account acc: accs){
                String queueId = UserUtil.getQueueId(queueName);
                Case newCase = new Case(
                        AccountId = acc.Id,
                        ContactId = acc.PersonContactId,
                        RecordTypeId = RecordTypeUtil.getSupportComplianceCaseTypeId(),
                        Subject = caseSubject,
                        Type__c = caseType,
                        OwnerId = queueId,
                        Status = 'Closed'
                );
                cases.add(newCase);
                personContactsByAccountId.put(acc.Id, acc.PersonContactId);
                acc.Send_Failed_CKA_Email__c=false;
            }
            insert cases;
            for(Case c: cases){
                EmailUtil.sendEmailByTemplate(personContactsByAccountId.get(c.AccountId), emailTemplate, c.Id, true, FRONTDESK_MAIL);
            }
        }

        public void finish(Database.BatchableContext bc) {
            BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
        }

    }