/**
 * @File Name          : LoggerTestDataFactory.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/18/2024, 5:43:56 PM
**/
@IsTest
public without sharing class LoggerTestDataFactory {

	public static void enableLogSettings() {
		insert new LogSettings__c(
			Active__c = true,
			Debug_Active__c = true,
			Days_Store__c = 15
		);
	}

	public static Boolean areErrorsOrExceptions() {
		String errorLogType = Log.Type.ERROR.name();
		String exceptionLogType = Log.Type.EXCEPT.name();
		Set<String> logTypes = new Set<String> { errorLogType, exceptionLogType };

		List<Log__c> logs = [
			SELECT Id
			FROM Log__c
			WHERE Type__c IN :logTypes
			LIMIT 1
		];
		
		Boolean result = (logs.isEmpty() == false);
		return result;
	}
	
}