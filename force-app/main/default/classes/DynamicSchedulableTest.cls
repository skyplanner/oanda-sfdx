/**
 * @File Name          : DynamicSchedulableTest.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/8/2024, 4:35:48 AM
**/
@IsTest
public without sharing class DynamicSchedulableTest {

	/**
	 * valid class name => ok
	 */
	@IsTest
	static void execute1() {
	
		Test.startTest();
		DynamicSchedulable instance = 
			new DynamicSchedulable('DynamicSchedulableTest.DummySchedulable');
		String jobId = System.schedule(
			'DummySchedulable', // jobName
			'0 0 0 3 9 ? 2042', // cronExp
			instance // schedulable
		);
		Test.stopTest();
		
		Assert.isTrue(true, 'Invalid result');
	}

	/**
	 * handle LogException 
	 */
	@IsTest
	static void execute2() {
		Boolean error = false;
	
		Test.startTest();
		try {
			DynamicSchedulable instance = new DynamicSchedulable('DynamicSchedulableTest');
			ExceptionTestUtil.prepareLogException();
			instance.execute(null);
			
		} catch(LogException ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}

	/**
	 * handle Exception 
	 */
	@IsTest
	static void execute3() {
		Boolean error = false;
	
		Test.startTest();
		try {
			DynamicSchedulable instance = new DynamicSchedulable('DynamicSchedulableTest');
			ExceptionTestUtil.prepareSpException();
			instance.execute(null);
			
		} catch(LogException ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}
	
	/**
	 * className is blank => exception
	 */
	@IsTest
	static void exception1() {
		Boolean error = false;
	
		Test.startTest();
		try {
			DynamicSchedulable instance = new DynamicSchedulable(null);
			
		} catch(Exception ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}

	/**
	 * className is not a valid class name => exception
	 */
	@IsTest
	static void exception2() {
		Boolean error = false;
	
		Test.startTest();
		try {
			DynamicSchedulable instance = new DynamicSchedulable('123456789');
			
		} catch(Exception ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}

	/**
	 * className stores the name of a class that does not implements 
	 * the "Schedulable" interface => exception
	 */
	@IsTest
	static void exception3() {
		Boolean error = false;
	
		Test.startTest();
		try {
			DynamicSchedulable instance = new DynamicSchedulable('DynamicSchedulableTest');
			Schedulable schedulableObj = instance.getDynamicIntance();
			
		} catch(Exception ex) {
			error = true;
		}
		Test.stopTest();
		
		Assert.isTrue(error, 'Invalid result');
	}

	//*************************************************************************

	public class DummySchedulable implements Schedulable {
		
		public void execute(SchedulableContext ctx) {
			// do nothing
		}

	}

}