global without sharing class NF_GetBroadcast  implements nfchat.PreInvokeClassAIMethodService{
    
    
    public List<String> ReturnStringResponse(nfchat.AIParameters data) {
        DialogFlow_Parameter__mdt parametersEntity = new DialogFlow_Parameter__mdt();
        String source = label.Chatbot;

        Map<String, Object> paramsMap = null;
        if (String.isNotEmpty(data.params)) {
            paramsMap = (Map<String, Object>) JSON.deserializeUntyped(
                data.params
            );
            System.debug('>>NF_GetBroadcast: paramsMap=' + paramsMap);
        }

        Map<String, String> eventParams = new Map<String, String>();

        List<String> tempResultFormatter = new List<String>();
        
        String broadcastName = '';
        String broadcastPromptToReplace = '';
        if (paramsMap != null) {
            broadcastName = (String) paramsMap.get('broadcastName');
        }

        List<Chat_Broadcast__c> broadcastMessages = [
            SELECT Name, Department__c, Message__c
            FROM Chat_Broadcast__c
            WHERE Active__c = true AND Name =: broadcastName
            ORDER BY Priority__c ASC
            LIMIT 1
        ];

        if(broadcastMessages.isEmpty()){
            System.debug('>>NF_GetBroadcasts: No broadcasts returned');
            eventParams.put('Result','false');
        }
        else{      
            System.debug('>>NF_GetBroadcasts: Got broadcasts!');
            System.debug(broadcastMessages[0]);
            if (paramsMap != null) {
                System.debug('Params Map: ' + paramsMap);
                String promptMessage = (String) paramsMap.get(label.prompt);
                    promptMessage = promptMessage.replace(
                        'BROADCAST_MESSAGE',
                        broadcastMessages[0].Message__c
                    );
                    tempResultFormatter.add(promptMessage);               
            }
        }
        return tempResultFormatter;
    }
}