/**
 * @File Name          : ExpressionValidationUtil.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/12/2020, 3:52:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/16/2020   acantero     Initial Version
**/
public class ExpressionValidationUtil {

    public enum ExpType {ALPHANUMERIC, LETTERS, NUMBERS}

    static final String ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    static final String ALPHABET_REVERSED = ALPHABET.reverse();
    static final String DIGITS = '01234567890';
    static final String DIGITS_REVERSED = DIGITS.reverse();
    

    final ExpressionValidationInfo valInfo;
    final String val;
    final MiFIDExpValSettings valSettings;

    //validation fields
    Integer validableLength;
    Integer validableLengthForNumber;
    String current;
    String lastChar;
    Boolean currentIsLetters = false;
    Boolean currentIsNumber = false;
    Boolean currentIsSeparator = false;
    Boolean checkRepetitive;
    Boolean checkConsecutive;
    //validation result fields
    Boolean valid;
    Boolean repetitive;
    Boolean consecutive;
    Boolean invalidChar;
    Boolean invalidLength;

    public Boolean isRepetitive {
        get{
            return (repetitive == true);
        }
    }

    public Boolean isConsecutive {
        get{
            return (consecutive == true);
        }
    }

    public Boolean hasInvalidChar {
        get{
            return (invalidChar == true);
        }
    }

    public Boolean hasInvalidLength {
        get{
            return (invalidLength == true);
        }
    }

    public String invalidPart {get; private set;}

    public Boolean isValid {
        get {
            if (valid == null) {
                validate();
            }
            return valid;
        }
    }

    public ExpressionValidationUtil(
        String val,
        ExpressionValidationInfo valInfo,
        Boolean checkPrefix
    ) {
        this.valInfo = valInfo;
        if (checkPrefix) {
            val = removePrefix(val, valInfo.prefixSize);
        }
        this.val = val;
        valSettings = MiFIDExpValSettings.getRequiredDefault();
    }

    Boolean checkLength() {
        Integer valLenght = (val != null)
            ? val.length()
            : 0;
        if (
            (valSettings.minLength > valLenght) ||
            (valSettings.maxLength < valLenght)
        ) {
            valid = false;
            invalidLength = true;
            return false;
        }
        //else...
        return true;
    }

    @testVisible
    void calculateValidableLengthForNumber() {
        validableLengthForNumber = valSettings.minInvalidNumber;
        Integer valLenght = (val != null)
            ? val.length()
            : 0;
        if (
            (valLenght < 2) || 
            (valSettings.minPercentNumber == null) ||
            (valSettings.minPercentNumber == 0)
        ) {
            return;
        }
        //...
        Integer mod = Math.mod(valLenght, 2);
        if (mod != 0) {
            valLenght++;
        }
        Integer percentResult = (valLenght * valSettings.minPercentNumber) / 100;
        if (percentResult < validableLengthForNumber) {
            validableLengthForNumber = percentResult;
        }
    }

    public static String removePrefix(String text, Integer prefixSize) {
        if (
            String.isEmpty(text) || 
            (prefixSize == null) ||
            (prefixSize <= 0) ||
            (prefixSize >= text.length())
        ) {
            return text;
        }
        //else...
        return text.substring(prefixSize);
    }

    public static String extractNumber(String val, Integer prefixSize) {
        val = removePrefix(val, prefixSize);
        return extractSomething(val, true);
    }

    public static String extractLetters(String val, Integer prefixSize) {
        val = removePrefix(val, prefixSize);
        return extractSomething(val, false);
    }

    @testVisible
    static String extractSomething(String val, Boolean isNumber) {
        if (String.isBlank(val)) {
            return null;
        }
        String result = '';
        for(Integer i = 0; i < val.length(); i++) {
            String temp = val.substring(i, i + 1);
            Boolean isValid = (isNumber)
                ? temp.isNumeric()
                : temp.isAlpha();
            if (isValid) {
                result += temp;
            } else {
                if (!String.isEmpty(result)) {
                    if(result.length() > 2) {
                        return result;
                    } else {
                        result = ''; //reinit look for another number in expression
                    } 
                }
            }
        }
        return (String.isEmpty(result)) 
            ? null
            : result;
    }


    @testVisible
    void initExpInfo(
        String initialChar, 
        Boolean isLetter,
        Boolean isNumber
    ) {
        valid = true;
        current = initialChar;
        lastChar = initialChar;
        repetitive = null;
        consecutive = null;
        invalidChar = false;
        currentIsLetters = isLetter;
        currentIsNumber = isNumber;
        currentIsSeparator = false;
        if (currentIsLetters) {
            validableLength = valSettings.minInvalidText;
            checkConsecutive = (valInfo.checkConsecutiveLetters == true);
            checkRepetitive = (valInfo.checkRepetitiveLetter == true);
        } else if (currentIsNumber){
            validableLength = validableLengthForNumber;
            checkConsecutive = (valInfo.checkConsecutiveNumbers == true);
            checkRepetitive = (valInfo.checkRepetitiveNumber == true);
        } else {
            String separators = valInfo.separators;
            currentIsSeparator = (
                (separators != null) &&
                (separators.indexOf(initialChar) != -1)
            );
            if (!currentIsSeparator) {
                valid = false;
                invalidChar = true;
                invalidPart = initialChar;
            }
        }
    }

    @testVisible
    Boolean checkLetterCase(String currentChar) {
        Boolean result = true;
        if (
            String.isNotEmpty(currentChar) &&
            (valInfo.letterCase != null)
        ) {
            result = (valInfo.letterCase == MiFIDExpValConst.LetterCase.UPPER_CASE)
                ? currentChar.isAllUpperCase()
                : currentChar.isAllLowerCase();
            if (!result) {
                valid = false;
                invalidChar = true;
                invalidPart = currentChar;
            }
        }
        return result;
    }

    @testVisible
    void validate() {
        valid = true;
        if (!checkLength()) {
            return;
        }
        //else...
        Integer length = String.isBlank(val)
            ? 0
            : val.length();
        if (length < 2) {
            return;
        }
        //else...
        calculateValidableLengthForNumber();
        String firstChar = val.substring(0, 1);
        initExpInfo(
            firstChar, 
            (firstChar.isAlpha() && checkLetterCase(firstChar)), 
            firstChar.isNumeric()
        );
        if (!valid) {
            return;
        }
        //else...
        for(Integer i = 1; i < val.length(); i++) {
            String currentChar = val.substring(i, i + 1);
            if (currentChar.isAlpha()) {
                if (!checkLetterCase(currentChar)) {
                    return;
                }
                //else...
                if (!currentIsLetters) {
                    initExpInfo(currentChar, true, false);
                    continue;
                } 
                //else...
            } else if (currentChar.isNumeric()) {
                if (!currentIsNumber) {
                    initExpInfo(currentChar, false, true);
                    continue;
                }
            } else {
                initExpInfo(currentChar, false, false);
                if (valid) {
                    continue;
                } else {
                    return;
                }
            }
            if (!addToCurrentExp(currentChar)) {
                return;
            }
        }
        //is valid
        repetitive = false;
        consecutive = false;
    }

    Boolean addToCurrentExp(String currentChar) {
        current += currentChar;
        Boolean oldRepetitive = repetitive;
        Boolean oldConsecutive = consecutive;
        if (checkRepetitive && (repetitive != false)) {
            repetitive = (currentChar == lastChar);
        }
        if (
            (repetitive == true) && 
            (current.length() >= validableLength)
        ) {
            valid = false;
            invalidPart = current;
            return false;
        }
        if (
            (repetitive != true) && 
            checkConsecutive && 
            (consecutive != false)
        ) {
            consecutive = checkTextConsecutive(
                current,
                currentIsLetters,
                currentIsNumber
            );
        }
        if (
            (consecutive == true) && 
            (current.length() >= validableLength)
        ) {
            valid = false;
            invalidPart = current;
            return false;
        }
        //else...
        if (oldRepetitive == true) {
            if (repetitive == false) {
                checkLastTwoCharsConsecutive(currentChar);
            }
        } else if ((oldConsecutive == true) && (consecutive == false)) {
            if (!checkLastTwoCharsRepetitive(currentChar)) {
                checkLastTwoCharsConsecutive(currentChar);
            }
        }
        if ((repetitive != true) && (consecutive != true)) {
            initExpInfo(currentChar, currentIsLetters, currentIsNumber);
        } else {
            lastChar = currentChar;
        }
        return true;
    }

    @testVisible
    Boolean checkLastTwoCharsRepetitive(String currentChar) {
        if (currentChar == lastChar) {
            repetitive = true;
            current = lastChar + currentChar;
            return true;
        }
        //else...
        return false;
    }

    @testVisible
    Boolean checkLastTwoCharsConsecutive(String currentChar) {
        String text = lastChar + currentChar;
        Boolean result = checkTextConsecutive(
            text,
            currentIsLetters,
            currentIsNumber
        );
        if (result) {
            consecutive = true;
            current = text;
        }
        return result;
    }

    @testVisible
    Boolean checkTextConsecutive(
        String text,
        Boolean isLetters,
        Boolean isNumber
    ) {
        Boolean result = false;
        if (isLetters) {
            result = (
                (ALPHABET.indexOf(text) != -1) ||
                (ALPHABET_REVERSED.indexOf(text) != -1)
            );
        } else if (isNumber){
            result = (
                (DIGITS.indexOf(text) != -1) ||
                (DIGITS_REVERSED.indexOf(text) != -1)
            );
        }
        return result;
    }

}