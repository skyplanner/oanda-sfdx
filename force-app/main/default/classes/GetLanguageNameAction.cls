/**
 * @File Name          : GetLanguageNameAction.cls
 * @Description        : Returns the name of the language (in its native 
 *                       version) corresponding to a code
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/15/2023, 11:42:01 AM
**/
public without sharing class GetLanguageNameAction { 

	@InvocableMethod(label='Get Language Name' callout=false)
	public static List<String> getLanguageName(List<String> languageCodes) { 
		try {
			ExceptionTestUtil.execute();
			List<String> result = new List<String>{ '' };
			
			if (
				(languageCodes == null) ||
				languageCodes.isEmpty()
			) {
				return result;
			}
			// else...
			result[0] = MessagingChatbotProcess.getLanguageName(languageCodes[0]);
			return result;

		} catch (LogException lex) {
			throw lex;
			//...
		} catch (Exception ex) {
			throw LogException.newInstance(
				GetLanguageNameAction.class.getName(),
				ex
			);
		}
	} 

}