/**
 * IBUtil: operations involved with Introducting Brokers
 */
public class IBUtil {

    @InvocableMethod(label='Link IB number to broker' description='Based on the given IB numbers, match fxAccounts with their brokers')
    public static void linkToBroker(List<fxAccount__c> fxas){
    	
    	//get the IB numbers
    	Set<String> ibNumbers = new Set<String>();
    	for(fxAccount__c fxa : fxas){
    		if(fxa.Introducing_Broker_Number__c != null){
    			ibNumbers.add(fxa.Introducing_Broker_Number__c);
    		}
    		
    	}
    	
    	if(ibNumbers.size() == 0){
    		System.debug('No IB number to link');
    		return;
    	}else{
    		System.debug('IB Numbers to link: ' + ibNumbers);
    	}
    	
    	//get all the brokers from the IB numbers
    	List<Account> brokers = [select Id, name, recordTypeId, Broker_Number__c from Account where Broker_Number__c in :ibNumbers];
    	if(brokers.size() == 0){
    		System.debug('No match for IB numbers');
    		return;
    	}
    	
    	Map<String, Account> accountByBrokerNumber = new Map<String, Account>();
    	
    	for(Account acct : brokers){
    		accountByBrokerNumber.put(acct.Broker_Number__c, acct);
    	}
    	
    	
    	//link to account
    	List<fxAccount__c> fxaToUpdate = new List<fxAccount__c>();
    	
    	for(fxAccount__c fxa : fxas){
    		Account acct = accountByBrokerNumber.get(fxa.Introducing_Broker_Number__c);
    		
    		if(acct != null){
    			fxAccount__c tmp = new fxAccount__c(Id = fxa.Id, Introducing_Broker__c = acct.Id);
    			
    			fxaToUpdate.add(tmp);
    		}
    		
    	}
    	
    	if(fxaToUpdate.size() > 0){
    		System.debug('Number of fxAccount to update: ' + fxaToUpdate.size());
    		update fxaToUpdate;
    	}
    }
}