/*
 *  Author : Deepak Malkani.
 *  Created Date : June 03 2016
 *  Purpose : Common reuable class for creating test data across APEX Test classes. 
*/
@isTest
public with sharing class TestDataFactory {
    private static Integer COUNTER;
    
    public TestDataFactory() {}

    private static String getUniqueEmail() {
        if (COUNTER == null)
            COUNTER = 1;
        return 'test' + (COUNTER++) + '@oanda.com';
    }

    //Used to create a sample test person account 
    public Account createTestAccount(){
        Account accnt = new Account(FirstName = 'First1', LastName='Last1', PersonEmail = getUniqueEmail(), Account_Status__c = 'Active', PersonMailingCountry = 'Canada');
        insert accnt;
        return accnt;
    }

    //Used to create a sample test person account 
    public Account createTestIntroducingBroker(String introducingBrokerName, String email){
        Account accnt = new Account(FirstName = 'First1', LastName='Last1', PersonEmail = email, Account_Status__c = 'Active', PersonMailingCountry = 'Canada', Introducing_Broker_Name__c = introducingBrokerName);
        insert accnt;
        return accnt;
    }

    //Used to create a sample test person account with owner as System User
    public Account createTestAccountforSysOwner(){
        Account accnt = new Account(FirstName = 'First1', LastName='Last1', PersonEmail = getUniqueEmail(), Account_Status__c = 'Active', PersonMailingCountry = 'Canada', OwnerId = UserUtil.getSystemUserId());
        insert accnt;
        return accnt;
    }

    public static Account createPersonAccount(String pName, String email, Boolean performDml){
        Account accnt = getPersonAccount(false);
        accnt.FirstName = null;
        accnt.LastName = pName;
        accnt.PersonEmail = email;
        if(performDml){
            insert accnt;
        }
        return accnt;
    }

    public static Account getPersonAccount(Boolean performDml) {
        Account acc1 = new Account(
            Business_Name__c = 'Business1',
            PersonMailingCity = 'Miami',
            PersonMailingCountry = 'USA',
            PersonEmail = 'a@b.com',
            FirstName = 'First1',
            LastName = 'Last1',
            PersonHasOptedOutOfEmail = true,
            Language_Preference__pc = 'English',
            MiddleName = 'Middle1',
            Phone = '7863659887',
            PersonMailingPostalCode = '11172',
            PersonMailingState = 'FL',
            PersonMailingStreet = '155',
            Suffix = 'Suff1',
            Salutation = 'Salut1',
            One_Id__c = 'test',
            RecordTypeId = RecordTypeUtil.getPersonAccountRecordTypeId()
        );

        if (performDml)
            insert acc1;

        return acc1;
    }

    //used to create bulk test accounts.
    public List<Account> createTestAccounts(Integer j){
        List<Account> accntInsList = new List<Account>();
        for(Integer i=0; i<j;i++){
            Account accnt = new Account();
            accnt.FirstName = 'TestFirst'+i;
            accnt.LastName = 'TestLast' +i;
            accnt.PersonEmail = 'test'+i+'@oanda.com';
            accnt.Account_Status__c = 'Active';
            accnt.PersonMailingCountry = 'Canada';
            accntInsList.add(accnt);
        }

        if(!accntInsList.isEmpty())
            insert accntInsList;
        return accntInsList;
    }

    //used to create eid result.
    public Account createTestAccountsEid(){
        Account accnt = new Account(
            FirstName = 'First1',
            LastName='Last1',
            PersonEmail = 'first1.last1.eid@test.com',
            Account_Status__c = 'Active',
            PersonMailingStreet = '452 ELM',
            PersonMailingCity = 'City',
            PersonMailingState = 'Ontario',
            PersonMailingPostalCode = '4555455',
            PersonMailingCountry = 'Canada',
            OwnerId = UserUtil.getSystemUserId());
        insert accnt;
        return accnt;
    }

    //Used to create a Live FX Trade Account
    public fxAccount__c createFXTradeAccount(Account accnt){
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'j1@example.org';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
        tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
        tradeAccount.Total_Deposits_USD__c = 2000;
        tradeAccount.Account__c = accnt.Id;
        insert tradeAccount;
        return tradeAccount;
    }

    //create a live fxAccount under a lead
    public fxAccount__c createFXTradeAccount(Lead ld){
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'j1@example.org';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
        tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
        tradeAccount.Total_Deposits_USD__c = 2000;
        tradeAccount.Lead__c = ld.Id;
        insert tradeAccount;
        return tradeAccount;
    }

    /**
    * @description Used to create fxAccount for Crypto trading under account.
    * @author Dianelys Velazquez | 05-26-2022 
    * @param acc 
    * @return fxAccount__c 
    **/
    public static fxAccount__c createFXTradeCryptoAccount(
        Account acc, Boolean performDml
    ) {
        fxAccount__c fxAcc = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID,
            Division_Name__c = Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Government_ID__c = '0123456789',
            Account__c = acc.Id,
            Email__c = 'test@testmethods.com'
        );

        if (performDml) {
            insert fxAcc;

            acc.Crypto_Fxaccount__c = fxAcc.Id;
            update acc;
        }

        return fxAcc;
    }

    /**
    * @description Used to create fxAccount for Crypto trading under account.
    * @author Yaneivys Gutierrez | 07-25-2022 
    * @param acc 
    * @return fxAccount__c 
    **/
    public static fxAccount__c createFXTradeCryptoAccountWithOneId(
        Account acc, Boolean performDml, Boolean createPaxos
    ) {
        fxAccount__c fxAcc = new fxAccount__c(
            Funnel_Stage__c = FunnelStatus.TRADED,
            RecordTypeId = RecordTypeUtil.FXACCOUNT_CRYPTO_RECORD_TYPE_ID,
            Division_Name__c = Constants.DIVISIONS_OANDA_ASIA_PACIFIC,
            Government_ID__c = '0123456789',
            Account__c = acc.Id,
            fxTrade_One_Id__c = '0dc000dc-dcdc-11dd-111c-00000ccc000d'
        );

        if (createPaxos) {
            Paxos__c p = new Paxos__c();
            p.Paxos_Status__c = 'Approved';
            insert p;
            fxAcc.Paxos_Identity__c = p.Id;
        }

        if (performDml) {
            insert fxAcc;

            if (createPaxos) {
                Paxos__c p = new Paxos__c();
                p.Id = fxAcc.Paxos_Identity__c;
                p.fxAccount__c = fxAcc.Id;
                update p;
            }

            acc.Crypto_Fxaccount__c = fxAcc.Id;
            update acc;
        }

        return fxAcc;
    }

    public List<Lead> createLeads(Integer Count){
        List<Lead> leadInsList = new List<Lead>();
        for(Integer i=0; i<Count;i++){
            Lead ldObj = new Lead();
            ldObj.FirstName = 'TestDemoLead'+i;
            ldObj.LastName = 'testLastName'+i;
            ldObj.Phone = '222-222-5500';
            ldObj.LeadSource = 'Practice Account';
            ldObj.Funnel_Stage__c = 'Demo Traded';
            ldObj.Email = 'test'+i+'@oanda.com';
            leadInsList.add(ldObj);
        }
        if(!leadInsList.isEmpty())
            insert leadInsList;
        return leadInsList;
    }

    public List<Lead> createDemoLeads(Integer Count){
        List<Lead> leadInsList = new List<Lead>();
        for(Integer i=0; i<Count;i++){
            Lead ldObj = new Lead();
            ldObj.FirstName = 'TestDemoLead'+i;
            ldObj.LastName = 'testLastName'+i;
            ldObj.Phone = '222-222-5500';
            ldObj.LeadSource = 'Practice Account';
            ldObj.Funnel_Stage__c = 'Demo Traded';
            ldObj.Email = 'test'+i+'@oanda.com';
            ldObj.RecordTypeId = RecordTypeUtil.getLeadRetailPracticeId();
            leadInsList.add(ldObj);
        }
        if(!leadInsList.isEmpty())
            insert leadInsList;
        return leadInsList;
    }

    //used to create eid result.
    public Lead createTestLeadEid(){
        Lead ldObj = new Lead(
            FirstName = 'First1',
            LastName='Last1',
            Street = '452 ELM',
            City = 'City',
            State = 'Ontario',
            PostalCode = '4555455',
            Country = 'Canada',
            Phone = '222-222-5500',
            LeadSource = 'Practice Account',
            Funnel_Stage__c = 'Demo Traded',
            Email = 'testLead@oanda.com'
        );
        insert ldObj;
        return ldObj;
    }

    //Used to create a list of FX Live Accunts for a List of Parent Person Accounts
    public List<fxAccount__c> createFXTradeAccounts(List<Lead> leadsList){
        List<fxAccount__c> fxTradList = new List<fxAccount__c>();
        for(Integer i=0; i< leadsList.size(); i++){
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'j1@example.org';
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
            tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
            tradeAccount.Total_Deposits_USD__c = 2000;
            tradeAccount.Lead__c = leadsList[i].Id;
            fxTradList.add(tradeAccount);
        }
        if(!fxTradList.isEmpty())
            insert fxTradList;
        return fxTradList;
    }
    //Used to create a list of FX Live Accunts for a List of Parent Person Accounts
    public List<fxAccount__c> createFXTradeAccounts(List<Account> accntList){
        List<fxAccount__c> fxTradList = new List<fxAccount__c>();
        for(Integer i=0; i< accntList.size(); i++){
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'j1@example.org';
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
            tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
            tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
            tradeAccount.Total_Deposits_USD__c = 2000;
            tradeAccount.Account__c = accntList[i].Id;
            fxTradList.add(tradeAccount);
        }
        if(!fxTradList.isEmpty())
            insert fxTradList;
        return fxTradList;
    }

    //Used to create a list of FX Demo Accounts for a List of Parent Leads
    public List<fxAccount__c> createFXDemoTradeAccounts(List<Lead> leadList){
        List<fxAccount__c> fxTradList = new List<fxAccount__c>();
        for(Integer i=0; i< leadList.size(); i++){
            fxAccount__c tradeAccount = new fxAccount__c();
            tradeAccount.Account_Email__c = 'j1@example.org';
            tradeAccount.Email__c = 'fxacc' + i + '@example.org';
            tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
            tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
            tradeAccount.First_Trade_Date__c = Datetime.now().addMonths(-6);
            tradeAccount.Last_Trade_Date__c = DateTime.now().addMonths(-2);
            tradeAccount.Total_Deposits_USD__c = 2000;
            tradeAccount.Lead__c = leadList[i].Id;
            fxTradList.add(tradeAccount);
        }
        if(!fxTradList.isEmpty())
            insert fxTradList;
        return fxTradList;
    }

    //Used to create an Opportunity Record for a Person Account.
    public Opportunity createOpportunity(Account accnt){
        Opportunity oppty = new Opportunity(Name='test name', AccountId=accnt.Id, CloseDate=Date.today(), StageName=FunnelStatus.TRADED, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, 'Opportunity'));
        insert oppty;
        return oppty;
    }

    //Used to create a Retention Opportunity Record for a Person Account.
    public Opportunity createRetOpportunity(Account accnt){
        Opportunity oppty = new Opportunity(Name='test retention', AccountId=accnt.Id, CloseDate=Date.today(), StageName=FunnelStatus.RETENTION_CLOSED_LOST_DO_NOT_CONTACT, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETENTION, 'Opportunity'));
        insert oppty;
        return oppty;
    }

    //Used to create a list of Opprtunities against a list of Person Accounts.
    public List<Opportunity> createOpportunities(List<Account> accntList){
        List<Opportunity> opptyInsList = new List<Opportunity>();
        for(Integer k = 0; k<accntList.size(); k++){
            Opportunity oppty = new Opportunity(Name='test name'+k, AccountId=accntList[k].Id, CloseDate=Date.today(), StageName=FunnelStatus.TRADED, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, 'Opportunity'));
            opptyInsList.add(oppty);
        }
        if(!opptyInsList.isEmpty())
            insert opptyInsList;
        return opptyInsList;
    }

    public User createUser(Profile p){

        User usrObj = new User(FirstName = 'testUser', LastName='testLast', ProfileId = p.Id, Alias = 'test123', LanguageLocaleKey = 'en_US', Email='testmyuser@oanda.com', EmailEncodingKey='UTF-8', isActive = true, LocaleSIDKey='en_US', TimeZoneSidKey = 'America/New_York', Username='testmyuser@oanda.com');
        insert usrObj;
        return usrObj;
    }
    
    public static Lead getTestLead(Boolean trade) {
        Id recordType;
        if (trade) {
            recordType = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        }
        else {
            recordType = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE;
        }
        Integer rand = Math.round(Math.random() * 100000);
        return getTestLead(recordType, rand + '@example.org', FunnelStatus.BEFORE_YOU_BEGIN);
    }
    
    public static Lead getTestLead(Id recordType, String email, String funnelStage) {
        Lead lead = new Lead();
        lead.FirstName = 'John';
        lead.LastName = 'Doe';
        lead.Email = email;
        lead.RecordTypeId = recordType;
        lead.Funnel_Stage__c = funnelStage;
        
        return lead;
    }

    public static Lead createStdLead(){
        RecordType rtypeObj = [SELECT id FROM RecordType WHERE SobjectType = 'Lead' AND isActive = true AND Name = 'Standard Lead' LIMIT 1];
        Lead ldObj = new Lead();
        ldObj.FirstName = 'TestFirst';
        ldObj.LastName = 'TestLast';
        ldObj.Email = 'test@oanda.com';
        ldObj.RecordTypeId = rtypeObj.Id;
        insert ldObj;
        return ldObj;
    }

    public static Task createTasksforLead(Lead ldObj){
        Task tkObj = new Task();
        tkObj.Subject = 'Other';
        tkObj.OwnerId = UserInfo.getUserId();
        tkObj.WhoId = ldObj.Id;
        tkObj.Status = 'Not Started';
        tkObj.Priority = 'High';
        tkObj.Update_Lead_Status__c = 'Warm';
        insert tkObj;
        return tkObj;
    }

    public static List<Lead> createStdLeadsBulk(Integer Count){
        RecordType rtypeObj = [SELECT id FROM RecordType WHERE SobjectType = 'Lead' AND isActive = true AND Name = 'Standard Lead' LIMIT 1];
        List<Lead> ldList = new List<Lead>();

        for(Integer i=0; i< Count; i++){
            Lead ldObj = new Lead();
            ldObj.FirstName = 'TestFirst'+i;
            ldObj.LastName = 'TestLast'+i;
            ldObj.Email = 'test'+i+'@oanda.com';
            ldObj.RecordTypeId = rtypeObj.Id;
            ldList.add(ldObj);
        }
        if(!ldList.isEmpty())
            insert ldList;
        return ldList;
    }

    public static List<Opportunity> createOpptyBulk(Integer Count){
        List<Opportunity> opptyInsList = new List<Opportunity>();
        for(Integer k = 0; k<Count; k++){
            Opportunity oppty = new Opportunity(Name='test name'+k, CloseDate=Date.today(), StageName=FunnelStatus.TRADED, RecordTypeId=RecordTypeUtil.getRecordTypeId(RecordTypeUtil.NAME_OPPORTUNITY_RETAIL, 'Opportunity'));
            opptyInsList.add(oppty);
        }
        if(!opptyInsList.isEmpty())
            insert opptyInsList;
        return opptyInsList;
    }


    public static List<Task> createTasksforLeadBulk(List<Lead> ldList){

        List<Task> tkList = new List<Task>();
        for(Integer i=0; i< ldList.size(); i++){
            Task tkObj = new Task();
            tkObj.Subject = 'Other';
            tkObj.OwnerId = UserInfo.getUserId();
            tkObj.WhoId = ldList[i].Id;
            tkObj.Status = 'Not Started';
            tkObj.Priority = 'High';
            tkObj.Update_Lead_Status__c = 'Warm';
            tkList.add(tkObj);
        }
        if(!tkList.isEmpty())
            insert tkList;
        return tkList;
    }

    public static List<Task> createTasksforOpptyBulk(List<Opportunity> opptyList){

        List<Task> tkList = new List<Task>();
        for(Integer i=0; i< opptyList.size(); i++){
            Task tkObj = new Task();
            tkObj.Subject = 'Other';
            tkObj.OwnerId = UserInfo.getUserId();
            tkObj.WhatId = opptyList[i].Id;
            tkObj.Status = 'Not Started';
            tkObj.Priority = 'High';
            tkObj.Update_Lead_Status__c = 'Warm';
            tkList.add(tkObj);
        }
        if(!tkList.isEmpty())
            insert tkList;
        return tkList;
    }

    //Method used to create EID Results for EquifaxUS and a given fxAccount
    public static EID_Result__c createEIDResult_EquifaxUS(fxAccount__c fxAccnt){
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = fxAccnt.Id;
        eidResultObj.Provider__c = 'EquifaxUS';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '1012';
        eidResultObj.Tracking_Number__c = '123456';
        insert eidResultObj;
        return eidResultObj;
    }

    //Method used to create EID Results for EquifaxUK and a given fxAccount
    public static EID_Result__c createEIDResult_EquifaxUK(fxAccount__c fxAccnt){
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = fxAccnt.Id;
        eidResultObj.Provider__c = 'EquifaxUK';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '2021252629';
        eidResultObj.Tracking_Number__c = '123456';
        insert eidResultObj;
        return eidResultObj;
    }

    //Method used to create EID Results for EquifaxUK and a given Lead
    public static EID_Result__c createEIDResult_Lead(Lead ldObj){
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.Lead__c = ldObj.Id;
        eidResultObj.Provider__c = 'EquifaxUK';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '2021252629';
        eidResultObj.Tracking_Number__c = '123456';
        insert eidResultObj;
        return eidResultObj;
    }

    //Method used to create Document for a given Lead
    public static Document__c createDocument_Lead(Lead ldObj){
        Document__c docObj = new Document__c();
        docObj.name = 'TestDoc';
        docObj.Lead__c = ldObj.Id;
        docObj.Document_Type__c = 'Passport';
        insert docObj;
        return docObj;
    }


    //Method used to populate Custom Settings for EID Response Codes
    public static List<EID_Reason_Codes__c> createResponseCodesCustSett(){
        List<EID_Reason_Codes__c> reasonCdList = new List<EID_Reason_Codes__c>();
        for(Integer i=10; i<20; i++){
            EID_Reason_Codes__c respCodeCustSett = new EID_Reason_Codes__c();
            respCodeCustSett.Provider__c = 'EquifaxUS';
            respCodeCustSett.ReasonText__c = 'Test'+i;
            respCodeCustSett.ReasonCode__c = string.valueOf(i);
            respCodeCustSett.ClassId__c = 'class'+i;
            respCodeCustSett.ActionText__c = 'action'+i;
            respCodeCustSett.Name = string.valueOf(i);
            reasonCdList.add(respCodeCustSett);
        }
        for(Integer i=20; i<30; i++){
            EID_Reason_Codes__c respCodeCustSett = new EID_Reason_Codes__c();
            respCodeCustSett.Provider__c = 'EquifaxUK';
            respCodeCustSett.ReasonText__c = 'Test'+i;
            respCodeCustSett.ReasonCode__c = string.valueOf(i);
            respCodeCustSett.ClassId__c = 'class'+i;
            respCodeCustSett.ActionText__c = 'action'+i;
            respCodeCustSett.Name = string.valueOf(i);
            reasonCdList.add(respCodeCustSett);
        }
        if(!reasonCdList.isEmpty())
            insert reasonCdList;
        return reasonCdList;
    }

    //Method used to create a Live Lead
    public Lead createLiveLeads(String lastName, String emailAddr){
        Lead lead = new Lead();
        lead.FirstName = 'John';
        lead.LastName = lastName;
        lead.Email = emailAddr;
        lead.RecordTypeId = LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        lead.Funnel_Stage__c = FunnelStatus.TRADED;
        return lead;

    }

    //Method used to create Trading Experience Duration
    public static List<Trading_Experience_Duration_Code__c> createTradingExpDurnCodesSett(){
        List<Trading_Experience_Duration_Code__c> tradingDurnLst = new List<Trading_Experience_Duration_Code__c>();
        for(Integer i=0; i<250 ; i++){
            Trading_Experience_Duration_Code__c tradeDurn = new Trading_Experience_Duration_Code__c();
            tradeDurn.Name = String.valueOf(i);
            tradeDurn.Duration_Code__c = String.valueOf(i);
            tradeDurn.Duration__c = 'Test' + i;
            tradingDurnLst.add(tradeDurn);
        }
        if(!tradingDurnLst.isEmpty())
            insert tradingDurnLst;
        return tradingDurnLst;
    }

    //Method used to create Trading Experience Type Codes
    public static List<Trading_Experience_Type_Code__c> createTradingExpTypeCodesSett(){
        List<Trading_Experience_Type_Code__c> tradeExpTypeLst = new List<Trading_Experience_Type_Code__c>();
        for(Integer i=0; i<250 ; i++){
            Trading_Experience_Type_Code__c tradExpType = new Trading_Experience_Type_Code__c();
            tradExpType.Name = String.valueOf(i);
            tradExpType.Type_Code__c = String.valueOf(i);
            tradExpType.Type__c = 'Test Type '+ i;
            tradeExpTypeLst.add(tradExpType);
        }
        if(!tradeExpTypeLst.isEmpty())
            insert tradeExpTypeLst;
        return tradeExpTypeLst;
    }

    public static Compliance_Record__c createComplianceRecord(
        Id fxAccountId,
        String documentName,
        String version,
        Datetime dateSent    
    ){
        Compliance_Record__c cr = new Compliance_Record__c();
        cr.fxAccount__c = fxAccountId;
        cr.Document_Name__c = documentName;
        cr.Document_Version__c = version;
        cr.Date_Sent__c = dateSent;
        cr.MT5_Account_Number__c = '1234567';
        cr.MT5_Order_Number__c = '3456789';

        insert cr;
        return cr;
    }

    public static Compliance_Record__c createComplianceRecordKid(
        Id fxAccountId,
        String documentName,
        String version,
        Datetime dateSent    
    ){
        Compliance_Record__c cr = new Compliance_Record__c();
        cr.fxAccount__c = fxAccountId;
        cr.Document_Name__c = documentName;
        cr.Document_Version__c = version;
        cr.Date_Sent__c = dateSent;
        cr.MT5_Account_Number__c = '1234567';
        cr.MT5_Order_Number__c = '3456789';
        cr.recordTypeId = Schema.SObjectType.Compliance_Record__c.getRecordTypeInfosByName().get('KID').getRecordTypeId();
        insert cr;
        return cr;
    }
}