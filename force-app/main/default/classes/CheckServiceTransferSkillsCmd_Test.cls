/**
 * @File Name          : CheckServiceTransferSkillsCmd_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/1/2024, 9:40:31 PM
**/
@IsTest
private without sharing class CheckServiceTransferSkillsCmd_Test {

	@testSetup
    static void setup() {
        OmnichanelRoutingTestDataFactory.createEntitlementData();
        OmnichanelRoutingTestDataFactory.createTestLeads();
        SPCaseTriggerHandler.enabled = false; //i do not want routing logic here
        OmnichanelRoutingTestDataFactory.createTestCasesForLeads();
    }

    //psr without skills -> return false
    @IsTest
    static void execute1() {
        ID caseId = [select Id from Case limit 1].Id;
        Non_HVC_Case__c nonHvcCase = new Non_HVC_Case__c(
            Case__c = caseId
        );
        insert nonHvcCase;
        //...
        CaseRoutingManager routingManager = CaseRoutingManager.getInstance();
        PendingServiceRouting psr = 
            routingManager.createPSRforNonHvcCase(nonHvcCase.Id);
        //TRICK because trigger will be disable
        psr.RoutingPriority = OmnichanelPriorityConst.DEFAULT_PRIORITY;
        //IMPORTANT disable trigger before insert psr
        PendingServiceRouting_Trigger_Utility.enabled = false;
        insert psr;
        //...
        List<PendingServiceRouting> fakeTransferList = 
            new List<PendingServiceRouting> {psr};
        Test.startTest();
        CheckServiceTransferSkillsCmd instance = 
            new CheckServiceTransferSkillsCmd(fakeTransferList);
        Boolean result = instance.execute();
        Test.stopTest();
        System.assertEquals(false, result);
    }

    //empty list -> return true
    @IsTest
    static void execute2() {
        Test.startTest();
        CheckServiceTransferSkillsCmd instance = 
            new CheckServiceTransferSkillsCmd(null);
        Boolean result = instance.execute();
        Test.stopTest();
        System.assertEquals(true, result);
    }
    
}