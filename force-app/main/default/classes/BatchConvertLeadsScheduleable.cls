public with sharing class BatchConvertLeadsScheduleable implements Database.Batchable<sObject>, Database.RaisesPlatformEvents, BatchReflection, Schedulable {
	
	private String query;
	Boolean isRerun = false;
	List<Id> ids = new List<Id>();
	private Map<Id, List<String>> conversionErrors;
	
	public static final Integer BATCH_SIZE = 5;
	
	public static final String CRON_NAME = 'BatchConvertLeadsScheduleable';
	public static final String CRON_SCHEDULE_1X35 = '0 35 * * * ?';
	
	private static DateTime ONE_DAYS_AGO = DateTime.now().addDays(-1);
	
	
	public static void schedule() {
		System.schedule(CRON_NAME + '_1X35', CRON_SCHEDULE_1X35, new BatchConvertLeadsScheduleable());
	}
	
	public BatchConvertLeadsScheduleable() {
		// Select all live not converted leads where either the lead is Traded, or it is owned by a non system user
    	query = 'SELECT Id, LastName, Email, RecordTypeId, Funnel_Stage__c, OwnerId, IsConverted FROM Lead WHERE RecordTypeId = \'' + LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE + '\' AND IsConverted = false and LastModifiedDate > ' + SoqlUtil.getSoql(ONE_DAYS_AGO) + ' ORDER BY Funnel_Stage__c ASC';
    	system.debug('query: ' + query);
//     	query = 'SELECT Id, Email, RecordTypeId, Funnel_Stage__c, OwnerId, IsConverted FROM Lead WHERE RecordTypeId = \'' + LeadUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE + '\' AND IsConverted = false AND ( Funnel_Stage__c = \'' + FunnelStatus.TRADED + '\' OR OwnerId != \'' + UserUtil.getSystemUserId() + '\' )';
	}
	public Id rerunSetup(List<Id> recIds, Integer batchSize){
		ids = recIds;
		query = 'SELECT Id, LastName, Email, RecordTypeId, Funnel_Stage__c, OwnerId, IsConverted FROM Lead WHERE Id IN :ids';
		isRerun=true;
		return Database.executeBatch(this, batchSize);
	}
	
	public BatchConvertLeadsScheduleable(String q){
		query = q;
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(query);
	}
	
	private void execute(List<Lead> leads) {
		
		// First reassign any leads with inactive owners
		Set<Id> reassignedLeadIds = LeadUtil.reassignLeadsWithInactiveOwners(leads);
		
		if (reassignedLeadIds.size() > 0) {
			
			List<Lead> reassignedLeads = [SELECT Id, Email, RecordTypeId, Funnel_Stage__c, OwnerId, IsConverted FROM Lead WHERE Id IN :reassignedLeadIds];
			for (Lead lead : leads) {
				if (!reassignedLeadIds.contains(lead.Id)) {
					reassignedLeads.add(lead);
				}
			}
			
			// Replace the original queried leads with the new list which contains those newly reassigned leads
			leads = reassignedLeads;
		}
		
		//check if the leads are already assigned. We only converted the leads are assigned
		Set<Id> leadIds = new Set<Id>();
		for(Lead ld : leads){
			leadIds.add(ld.Id);
		}
		
		List<fxAccount__c> liveFxAccounts = [select id, Trigger_Lead_Assignment_Rules__c, lead__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and lead__c in : leadIds];
		Set<Id> unassignedLeadIds = new Set<Id>();
		System.debug(liveFxAccounts);
		
		for(fxAccount__c fxa : liveFxAccounts){
			if(fxa.Trigger_Lead_Assignment_Rules__c && reassignedLeadIds.contains(fxa.lead__c) == false){
				unassignedLeadIds.add(fxa.lead__c);
			}
		}
		
		//remove the unassigned leads
		List<Lead> assignedLeads = new List<Lead>();
		for(Lead Ld : leads){
			if(unassignedLeadIds.contains(Ld.Id) == false){
				assignedLeads.add(Ld);
			}
		}

		Map<String, Object> result = LeadUtil.convertReadyLeads(assignedLeads);
		this.conversionErrors = (Map<Id, List<String>>)result.get('errors');
		List<Lead> convertedLeads = (List<Lead>)result.get('converted');
		LeadUtil.postprocessLeadConversion(convertedLeads);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		this.execute((List<Lead>)batch);
	}
	
	public void execute(SchedulableContext context) {
		if(query != null){
			Database.executeBatch(new BatchConvertLeadsScheduleable(query), BATCH_SIZE);
		}else{
			executeBatch();
		}
		
	}

	public void finish(Database.BatchableContext bc) {
		try {
			BatchApexErrorHandler.checkBatchErrors(isRerun, bc);
			EmailUtil.sendEmailForBatchJob(bc.getJobId());
			this.sendErrorEmail(bc);		
		}
		catch (Exception e) {}
	}
	
	private void sendErrorEmail(Database.BatchableContext bc) {
		if (this.conversionErrors != null && this.conversionErrors.size() > 0) {
			
			String subject = CRON_NAME + ': ' + this.conversionErrors.size() + ' Conversion Error(s)';
			
			EmailUtil.sendEmailForBatchJob(bc.getJobId(), subject, getErrorMsg());
		}
	}
	
	private String getErrorMsg() {
		String msg = '';
			
		for (Id leadId : this.conversionErrors.keySet()) {
			List<String> errors = this.conversionErrors.get(leadId);
			
			if (errors.size() == 0) {
				continue;
			}
			
			msg += leadId + ': \n';
			
			for (String error : errors) {
				msg += error + '\n';
			}
			
			msg += '\n\n';
		}
		
		return msg;
	}
	
	public static Id executeBatch() {
		return executeBatch(BATCH_SIZE);
	}
	
	public static Id executeBatch(Integer batchSize) {
		return Database.executeBatch(new BatchConvertLeadsScheduleable(), batchSize);
	}
	
	// Executes lead conversion in same EC
	public static void executeInline() {
		
		BatchConvertLeadsScheduleable job = new BatchConvertLeadsScheduleable();
		
		List<Lead> leads = Database.query(job.query);
		
		job.execute(leads);
		
		if (job.conversionErrors != null && job.conversionErrors.size() > 0) {
			throw new ApplicationException(job.getErrorMsg());
		}
	}
}