/**
 * @description       :
 * @author            : Ariel Niubo
 * @group             :
 * @last modified on  : 07-29-2022
 * @last modified by  : Ariel Niubo
 **/
@SuppressWarnings('PMD.ApexCRUDViolation')
public inherited sharing class CronTriggerRepo {
	public final static String CRON_STATE_COMPLETE = 'COMPLETE';
	public final static String CRON_STATE_ERROR = 'ERROR';
	public final static String CRON_STATE_DELETED = 'DELETED';
	public final static String CRON_STATE_WAITING = 'WAITING';
	public final static String SCHEDULE_JOB_TYPE = '7';
	public final static List<String> stoppedStates = new List<String>{
		CRON_STATE_COMPLETE,
		CRON_STATE_ERROR,
		CRON_STATE_DELETED
	};
	public static CronTrigger byNameAndState(
		String jobName,
		List<String> notInstateList,
		String jobType
	) {
		List<CronTrigger> cronTriggerList = [
			SELECT Id, CronExpression, CronJobDetail.Name
			FROM CronTrigger
			WHERE
				CronJobDetail.JobType = :jobType
				AND CronJobDetail.Name = :jobName
				AND State NOT IN :notInstateList
			LIMIT 1
		];
		return cronTriggerList.isEmpty() ? null : cronTriggerList.get(0);
	}
	public static CronTrigger byNameAndNotStopped(String jobName) {
		return byNameAndState(jobName, stoppedStates, SCHEDULE_JOB_TYPE);
	}
}