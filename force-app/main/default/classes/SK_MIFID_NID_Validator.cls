/**
 * @File Name          : SK_MIFID_NID_Validator.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/11/2020, 1:03:07 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/9/2020   acantero     Initial Version
**/
public class SK_MIFID_NID_Validator implements ExpressionValidator {
    
    public String validate(String val) {
        if (String.isEmpty(val)) {
            return null;
        }
        //else...
        if (!val.isNumeric()) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        Long longVal = Long.valueOf(val);
        Long mod11 = Math.mod(longVal, 11);
        if (mod11 != 0) {
            return System.Label.MifidValPasspFormatError;
        }
        //else...
        return null;
    }

}