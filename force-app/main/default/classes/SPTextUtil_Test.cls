/**
 * @File Name          : SPTextUtil_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/25/2021, 1:42:03 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/27/2020   acantero     Initial Version
**/
@isTest
private class SPTextUtil_Test {

    @isTest
    static void getValueOrEmpty() {
        String someText = 'some text';
        Test.startTest();
        String result1 = SPTextUtil.getValueOrEmpty(someText);
        String result2 = SPTextUtil.getValueOrEmpty(null);
        Test.stopTest();
        System.assertEquals(someText, result1);
        System.assertEquals('', result2);
    }

    @isTest
    static void getStringValueOrEmpty() {
        Test.startTest();
        String result1 = SPTextUtil.getStringValueOrEmpty(5);
        String result2 = SPTextUtil.getStringValueOrEmpty(null);
        Test.stopTest();
        System.assertEquals('5', result1);
        System.assertEquals('', result2);
    }

    //invalid params
    @isTest
    static void parseMultilineText_test() {
        Test.startTest();
        List<String> result1 = SPTextUtil.parseMultilineText(null);
        List<String> result2 = SPTextUtil.parseMultilineText(
            'some' + SPTextUtil.END_OF_LINE_SHORT + 'value'
        );
        Test.stopTest();
        System.assert(result1.isEmpty());
        System.assertEquals(2, result2.size());
    }

    //invalid params
    @isTest
    static void addValuesToText_test() {
        List<String> values = new List<String> {'John', 'Fitzgerald', 'Doe'};
        String firstItemPrefix = 'Mr. '; 
        String firstItemSuffix = ''; 
        String othersItemPrefix = ' ';
        String othersItemSuffix = '';
        Test.startTest();
        String result1 = SPTextUtil.addValuesToText(
            'test',
            null,
            null,
            null,
            null,
            null
        );
        String result2 = SPTextUtil.addValuesToText(
            null,
            values, 
            firstItemPrefix, 
            firstItemSuffix, 
            othersItemPrefix,
            othersItemSuffix
        );
        Test.stopTest();
        System.assertEquals('test', result1);
        System.assertEquals('Mr. John Fitzgerald Doe', result2);
    }
}