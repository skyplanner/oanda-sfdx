/**
 * @Description  : Class assigns template into case's quick action.
 * @Author       : Jakub Fik
 * @Date         : 2024-06-13
 **/
public inherited sharing class EmailTemplateSelector implements QuickAction.QuickActionDefaultsHandler {
    private static Map<String, String> languageToTemplateMapOTMSLogo = new Map<String, String>{
        Constants.LANGUAGE_ENGLISH => Constants.EMAIL_TEMPLATE_OTMS_CX_ENGLISH,
        Constants.LANGUAGE_SPANISH => Constants.EMAIL_TEMPLATE_OTMS_CX_SPANISH,
        Constants.LANGUAGE_POLISH => Constants.EMAIL_TEMPLATE_OTMS_CX_POLISH
    };
    private static Map<String, String> languageToTemplateMapOandaLogo = new Map<String, String>{
        Constants.LANGUAGE_ENGLISH => Constants.EMAIL_TEMPLATE_OTMS_CX_ENGLISH_OANDA_LOGO,
        Constants.LANGUAGE_SPANISH => Constants.EMAIL_TEMPLATE_OTMS_CX_SPANISH_OANDA_LOGO,
        Constants.LANGUAGE_POLISH => Constants.EMAIL_TEMPLATE_OTMS_CX_POLISH_OANDA_LOGO
    };

    /**
     * @description Empty constructor required for interface.
     * @author Jakub Fik | 2024-06-13
     **/
    public EmailTemplateSelector() {
    }

    /**
     * @description Implemented method from interface. Applied template for EmailOtms quick action.
     * @author Jakub Fik | 2024-06-13
     * @param defaults
     **/
    public void onInitDefaults(QuickAction.QuickActionDefaults[] defaults) {
        QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = null;

        // Check if the quick action is the standard case feed Email action
        for (Integer j = 0; j < defaults.size(); j++) {
            if (defaults.get(j).getActionName().equals(Constants.CASE_ACTION_EMAIL_OTMS)) {
                sendEmailDefaults = (QuickAction.SendEmailQuickActionDefaults) defaults.get(j);
                break;
            }
        }
        if (sendEmailDefaults != null) {
            Case c = [
                SELECT RecordTypeId, Language_Preference__c
                FROM Case
                WHERE Id = :sendEmailDefaults.getContextId()
            ];
            Id otmsRecordTypeId = RecordTypeUtil.getOTMSSupportCaseRecordTypeId();

            if (c.RecordTypeId == otmsRecordTypeId) {
                EmailMessage emailMessage = (EmailMessage) sendEmailDefaults.getTargetSObject();
                String fromEmailAddress = String.valueOf(emailMessage.ValidatedFromAddress);
                Map<String, String> templateMap = (fromEmailAddress.contains('@oanda.com'))
                    ? languageToTemplateMapOandaLogo
                    : languageToTemplateMapOTMSLogo;

                String defaultTemplate = (templateMap == languageToTemplateMapOandaLogo)
                    ? Constants.EMAIL_TEMPLATE_OTMS_CX_ENGLISH_OANDA_LOGO
                    : Constants.EMAIL_TEMPLATE_OTMS_CX_ENGLISH;

                String templateName = templateMap.containsKey(c.Language_Preference__c)
                    ? templateMap.get(c.Language_Preference__c)
                    : defaultTemplate;

                EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName];
                sendEmailDefaults.setTemplateId(et.Id);
            }
        }
    }
}