/*
 *  @author : Deepak Malkani.
 *  Created Date : Jan 31 2017
 *  Purpose : Test class for onBoarding Email Handler.
*/

@isTest
private class OnBoardingEmailHandlerTest
{
    @testSetup static void init(){

        //Preload Test with some test data
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAcct = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        Lead ld = testHandler.createLiveLeads('OandaLiveTest', 'livelead_test@oanda.com');
        insert ld;

        //update fxAccount division name
        fxAcct.Division_Name__c = 'OANDA Canada';
        update fxAcct;

    }

    @isTest
    static void testInboundEmail_onBoarding_wt_Account()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name FROM fxAccount__c];
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'This is a test email';
        email.fromAddress = 'test1@oanda.com';
        email.ccAddresses = null;
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        email.toAddresses = toAddress;
        //email.ccAddresses = 'testing123@oanda.com';
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(1, [SELECT Count() FROM Case WHERE fxAccount__c = : fxAccntRec.Id]);
        system.assertEquals(1, [SELECT Count() FROM EmailMessage]);
    }

    @isTest
    static void testInboundEmail_onBoarding_no_Account()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name FROM fxAccount__c];
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'This is a test email';
        email.fromAddress = 'orphanEmail@oanda.com';
        email.ccAddresses = null;
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        email.toAddresses = toAddress;
        //email.ccAddresses = 'testing123@oanda.com';
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(0, [SELECT Count() FROM Case WHERE fxAccount__c = : fxAccntRec.Id]);
        system.assertEquals(1, [SELECT Count() FROM Case]);
        system.assertEquals(1, [SELECT Count() FROM EmailMessage]);
    }

    @isTest
    static void testInboundEmail_onBoarding_wt_Lead()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name FROM fxAccount__c];
        Lead ldRec = [SELECT id, FirstName, LastName, fxAccount__c FROM Lead];
        ldRec.fxAccount__c = fxAccntRec.Id;
        update ldRec;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'This is a test email';
        email.fromAddress = 'livelead_test@oanda.com';
        email.ccAddresses = null;
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        email.toAddresses = toAddress;
        //email.ccAddresses = 'testing123@oanda.com';
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(1, [SELECT Count() FROM Case]);
        system.assertEquals(fxAccntRec.Id, [SELECT fxAccount__c FROM Case].fxAccount__c);
        system.assertEquals(1, [SELECT Count() FROM EmailMessage]);
    }

    @isTest
    static void testInboundEmail_onBoarding_wt_Lead_wt_BinAttachment()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name FROM fxAccount__c];
        Lead ldRec = [SELECT id, FirstName, LastName, fxAccount__c FROM Lead];
        ldRec.fxAccount__c = fxAccntRec.Id;
        update ldRec;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
        email.subject = 'This is a test email';
        email.fromAddress = 'livelead_test@oanda.com';
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        String[] ccAddress = new String[] {'myccAddress1@oanda.com', 'myccAddress2@oanda.com'};
        email.toAddresses = toAddress;
        email.ccAddresses = ccAddress;
        // set the body of the attachment
        inAtt.body = blob.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'application/pdf';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt }; 
        
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(1, [SELECT Count() FROM Case]);
        system.assertEquals(fxAccntRec.Id, [SELECT fxAccount__c FROM Case].fxAccount__c);
        system.assertEquals(1, [SELECT Count() FROM EmailMessage]);
        system.assertEquals(1, [SELECT Count() FROM Document__c]);
    }

    @isTest
    static void testInboundEmail_onBoarding_wt_Lead_wt_TxtAttachment()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name FROM fxAccount__c];
        Lead ldRec = [SELECT id, FirstName, LastName, fxAccount__c FROM Lead];
        ldRec.fxAccount__c = fxAccntRec.Id;
        update ldRec;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.TextAttachment inAtt = new Messaging.InboundEmail.TextAttachment();
        email.subject = 'This is a test email';
        email.fromAddress = 'livelead_test@oanda.com';
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        String[] ccAddress = new String[] {'myccAddress1@oanda.com', 'myccAddress2@oanda.com'};
        email.toAddresses = toAddress;
        email.ccAddresses = ccAddress;
        // set the body of the attachment
        inAtt.body = String.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/txt';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] {inAtt }; 
        
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(1, [SELECT Count() FROM Case]);
        system.assertEquals(fxAccntRec.Id, [SELECT fxAccount__c FROM Case].fxAccount__c);
        system.assertEquals(1, [SELECT Count() FROM EmailMessage]);
        system.assertEquals(1, [SELECT Count() FROM Document__c]);
    }
    
    //fxAccount is from a non configured division
    @isTest
    static void testInboundEmail_onBoarding_wt_Account2()
    {
        
        //PLAN : Prepare the Test data for the test class
        fxAccount__c fxAccntRec = [SELECT id, Name, Division_Name__c FROM fxAccount__c];
        fxAccntRec.Division_Name__c = 'abc';
        update fxAccntRec;

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'This is a test email';
        email.fromAddress = 'test1@oanda.com';
        email.ccAddresses = null;
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        email.toAddresses = toAddress;
        //email.ccAddresses = 'testing123@oanda.com';
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(0, [SELECT Count() FROM Case WHERE fxAccount__c = : fxAccntRec.Id]);
        system.assertEquals(0, [SELECT Count() FROM EmailMessage]);
    }

    
    //fxAccount is from a non configured division
    @isTest
    static void testInboundEmail_onBoarding_wt_Lead2()
    {
        
        //PLAN : Prepare the Test data for the test class
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxAccntRec = [SELECT id, Name, Division_Name__c FROM fxAccount__c];
        fxAccntRec.Division_Name__c = 'abc';
        update fxAccntRec;

        Lead ldRec = [SELECT id, FirstName, LastName, fxAccount__c FROM Lead];
        ldRec.fxAccount__c = fxAccntRec.Id;
        update ldRec;

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'This is a test email';
        email.fromAddress = 'livelead_test@oanda.com';
        email.ccAddresses = null;
        email.htmlBody = 'This is my body';
        email.plainTextBody = 'This is my plain text body';
        email.fromName = 'Test 1';
        String[] toAddress = new String[] {'onboarding_test@oanda.com'};
        email.toAddresses = toAddress;
        //email.ccAddresses = 'testing123@oanda.com';
        //ACT : Start the testing 
        Test.startTest();
        OnBoardingEmailHandler onBoardinginboundEmail = new OnBoardingEmailHandler();
        onBoardinginboundEmail.handleInboundEmail(email, env);
        Test.stopTest();
        //ASSERT : Perform assertions/validations
        system.assertEquals(0, [SELECT Count() FROM Case]);
        system.assertEquals(0, [SELECT Count() FROM EmailMessage]);
    }

}