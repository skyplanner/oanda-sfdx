/**
 * @File Name          : ServiceResourceHelper_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/26/2021, 11:27:27 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/23/2021, 3:53:41 PM   acantero     Initial Version
**/
@isTest(isParallel = false)
private without sharing class ServiceResourceHelper_Test {

    @isTest
    static void getAgentsIdSet() {
        Test.startTest();
        Set<String> result;
        User testUser = SPTestUtil.createUser('u0001');
        System.runAs(testUser) {
            String userId = UserInfo.getUserId();
            String serviceResourceId = 
                ServiceResourceTestDataFactory.createAgentServiceResource(userId);
            Set<String> serviceResourceIdSet = new Set<String> {serviceResourceId};
            result = 
                ServiceResourceHelper.getAgentsIdSet(serviceResourceIdSet);
        }
        Test.stopTest();
        System.assertEquals(1, result.size());
    }
    
}