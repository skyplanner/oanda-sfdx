public with sharing class SubAccountViewCon {
    
    public static final String SORT_ORDER_ASC = 'asc';
    public static final String SORT_ORDER_DESC = 'desc';
    public static final String SORT_FIELD_NAME_INIT = 'Trade_Volume_USD_MTD__c';
    public static final String SORT_ORDER_INIT = SORT_ORDER_DESC;
    
    public List<BIWebService.SubAccount> subAccounts {set; get;}
    public Id selectedFxAccountId {set; get;}
    public List<fxAccount__c> fxAccounts {get; set;}
    public String sortFieldName {set; get;}
    public String sortOrder {get; set;}
    
    //private transient BIWebService wService;
    private fxAccount__c currentFxAccount;
    
    
    public SubAccountViewCon(){
    	sortFieldName = SORT_FIELD_NAME_INIT;
    	sortOrder = SORT_ORDER_INIT;
    }
   
    
    public void showSubAccount(){
    	System.debug('Selected: ' + selectedFxAccountId);
    	
    	List<fxAccount__c> fxAccounts = [select id, name, fxTrade_User_Id__c, recordTypeId from fxAccount__c where id = :selectedFxAccountId];
    	if(fxAccounts.size() == 0 || fxAccounts[0].fxTrade_User_Id__c == null){
    		String message = 'No fxAccount Matched, or no fxTrade User Id';
    		System.debug(message);
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
    		return;
    	}
    	
    	currentFxAccount = fxAccounts[0];
    	System.debug(currentFxAccount);
    	
    	try{
	    	BIWebService wService = new BIWebService();
	    	
	    	if(currentFxAccount.recordTypeId == RecordTypeUtil.getFxAccountLiveId()){
	    		subAccounts = wService.queryLiveFxTrade(String.valueOf(currentFxAccount.fxTrade_User_Id__c));
	    	}else{
	    		subAccounts = wService.queryDemoFxTrade(String.valueOf(currentFxAccount.fxTrade_User_Id__c));
	    	}
    	}catch(Exception e){
    		ApexPages.addMessages(e);
    	}
    	
    }
    
    public boolean getShouldShowSubAccounts(){
    	boolean result = true;
    	
    	if(subAccounts == null || subAccounts.size() == 0){
    		result = false;
    	}else{
    		List<ApexPages.Message> msgs = ApexPages.getMessages();
    		if(msgs != null && msgs.size() > 0){
    			result = false;
    		}
    		
    	}
    	
    	
    	return result;
    }
    
    public String getSubAccountBlockTitle(){
    	String result = '';
    	
    	if(currentFxAccount != null){
    		result = currentFxAccount.name + ' (' + currentFxAccount.fxTrade_User_Id__c + ')';
    	}
    	
    	return result;
    	
    }

    public String getLinkTarget(){

        String url = ApexPages.currentPage().getURL();
        String target = '_blank';

        if(url.contains('lightning') || url.contains('one.app')){
            target = '_self';
        }
        
        return target;
    }


    public void sortFxAccounts(){
    	System.debug('sort by column: ' + sortFieldName);
    	
    	if(fxAccounts == null || fxAccounts.size() == 0){
    		System.debug('Nothing to sort');
    		return;
    	}
    	
    	
    	//toggle the sort direction
    	if(sortOrder == SORT_ORDER_DESC){
    		sortOrder = SORT_ORDER_ASC;
    	}else{
    		sortOrder = SORT_ORDER_DESC;
    	}
    	
    	
        Integer size = fxAccounts.size();
        
        //sort fxAccounts
        for(Integer i =0; i<size-1; i++){
        	Integer foundIdx = i;
        	
        	for(Integer j=i+1; j<size; j++){
        		
        		if((sortOrder == SORT_ORDER_DESC && compare(fxAccounts[foundIdx], fxAccounts[j],  sortFieldName) == -1)
        		   || (sortOrder == SORT_ORDER_ASC && compare(fxAccounts[foundIdx], fxAccounts[j],  sortFieldName) == 1)){
        			
        			foundIdx = j;
        		}
        	}
        	
        	fxAccount__c temp = fxAccounts[foundIdx];
        	fxAccounts[foundIdx] = fxAccounts[i];
        	fxAccounts[i] = temp;
        }
        
    }
    
    //if fxa1 is larger than fxa2, then return 1;
    //If fxa1 is smaller than fxa2, then return -1;
    //null value is always smaller
    private Integer compare(fxAccount__c fxa1, fxAccount__c fxa2, String fieldName ){
    	Schema.SObjectField field  = Schema.SObjectType.fxAccount__c.fields.getMap().get(fieldName);
    	Schema.DisplayType FldType = field.getDescribe().getType();
    	
    	Object v1 = fxa1.get(fieldName);
    	Object v2 = fxa2.get(fieldName);
    	
    	boolean isV1Bigger, isEqual;
    	if(v1 == null && v2 == null){
    		isV1Bigger = false;
    		isEqual = true;
    	}else if (v1 == null){
    		isV1Bigger = false;
    		isEqual = false;
    	}else if (v2 == null){
    		isV1Bigger = true;
    		isEqual = false;
    	}else if(FldType == Schema.DisplayType.DateTime){
    		isV1Bigger = (DateTime)v1 > (DateTime) v2;
    		isEqual = (DateTime)v1 == (DateTime) v2;
    	}else if(FldType == Schema.DisplayType.Currency
                 || FldType == Schema.DisplayType.Double
                 ||  FldType == Schema.DisplayType.Integer){
                 	
            isV1Bigger = (Double)v1 > (Double) v2;
    		isEqual = (Double)v1 == (Double) v2;   	
        }else if (FldType == Schema.DisplayType.Boolean){
        	Integer val1 = ((boolean) v1) ? 1 : 0;
        	Integer val2 = ((boolean) v2) ? 1 : 0;
        	
        	isV1Bigger = val1 > val2;
    		isEqual = val1 == val2;
        }else{
        	//default is string type
        	isV1Bigger = ((String)v1).compareTo((String) v2) < 0;
    		isEqual = ((String)v1).compareTo((String) v2) == 0;
        }
    	
    	//decide the result
    	Integer result = -1;
    	if(isV1Bigger){
    		result = 1;
    	}else if(isEqual){
    		result = 0;
    	}
    	
    	return result;
    }






    
}