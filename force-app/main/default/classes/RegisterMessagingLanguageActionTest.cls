/**
 * @File Name          : RegisterMessagingLanguageActionTest.cls
 * @Description        :
 * @Author             : aniubo
 * @Group              :
 * @Last Modified By   : aniubo
 * @Last Modified On   : 1/29/2024, 1:10:54 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    12/19/2023, 10:52:40 AM   aniubo     Initial Version
 **/
@SuppressWarnings('PMD.AvoidHardcodingId')
@isTest
private class RegisterMessagingLanguageActionTest {
	@isTest
	private static void testInfoListIsNullOrEmpty() {
		// Test data setup
		Boolean threwException = false;

		// Actual test
		Test.startTest();
		try {
			RegisterMessagingLanguageAction.registerLanguage(null);
			RegisterMessagingLanguageAction.registerLanguage(
				new List<RegisterMessagingLanguageAction.ActionInfo>()
			);
		} catch (Exception ex) {
			threwException = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(
			false,
			threwException,
			'The exception must not have been thrown'
		);
	}
	@isTest
	private static void testRegisterLanguageThrowEx() {
		// Test data setup
		ExceptionTestUtil.prepareDummyException();
		Boolean isError = false;
		// Actual test
		Test.startTest();
		try {
			RegisterMessagingLanguageAction.registerLanguage(null);
		} catch (Exception ex) {
			isError = true;
		}

		Test.stopTest();

		// Asserts
		Assert.areEqual(true, isError, 'Exception should have been thrown');
	}

	@isTest
	private static void testRegisterLanguage() {
		// Test data setup
		RegisterMessagingLanguageAction.ActionInfo info = new RegisterMessagingLanguageAction.ActionInfo();
		info.endUserId = '0PADU00000004e04AA';
		info.messagingSessionId = null;
		info.languageCode = 'en-US';
		Boolean threwException = false;
		// Actual test
		Test.startTest();
		try {
			RegisterMessagingLanguageAction.registerLanguage(
				new List<RegisterMessagingLanguageAction.ActionInfo>{ info }
			);
		} catch (Exception ex) {
			threwException = true;
		}

		Test.stopTest();

		Assert.areEqual(
			false,
			threwException,
			'The exception must not have been thrown'
		);
		// Asserts
	}
}