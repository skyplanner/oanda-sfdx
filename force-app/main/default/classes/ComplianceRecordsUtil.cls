public class ComplianceRecordsUtil {

    /**
     * Retrieve all KIDs associated with given fxAccount
     */
    public static KIDResponse getFxAccountsKIDs(Id fxAccId) {

        KIDResponse r = new KIDResponse();

        if (isAccountFromCorrectDivision(fxAccId)) {

            r.isAllowed = true;

            List<Compliance_Record__c> complianceRecords = [SELECT Document_Name__c, MT5_Account_Number__c, MT5_Order_Number__c,
                                                                    Document_Version__c, Date_Sent__c
                                                                FROM Compliance_Record__c
                                                                WHERE fxAccount__c = :fxAccId
                                                                ORDER BY Document_Name__c ASC, Date_Sent__c DESC];
    
            Map<String,KIDWrapper> nameToGroupedRecordMap = new Map<String,KIDWrapper>();
    
            for (Compliance_Record__c cr : complianceRecords) {
    
                KIDWrapper kid = new KIDWrapper();
    
                kid.name = cr.Document_Name__c;
                kid.mt5AccountNumber = cr.MT5_Account_Number__c;
                kid.mt5OrderNumber = cr.MT5_Order_Number__c;
                kid.version = cr.Document_Version__c;
                kid.dateSent = cr.Date_Sent__c;
    
    
                KIDWrapper currentMaster = nameToGroupedRecordMap.get(kid.name);
                if (currentMaster != null) {
                    if(currentMaster.children != null) {
                        currentMaster.children.add(kid);
                    } else {
                        currentMaster.children = new List<KIDWrapper> {kid};
                    }
                } else {
                    nameToGroupedRecordMap.put(kid.name, kid);
                }
            }
    
            r.results = nameToGroupedRecordMap.isEmpty() ? null : nameToGroupedRecordMap.values();
        } else {
            r.isAllowed = false;
        }

        return r;
    }

    /**
     * Check the list of divisions in custom metadata
     * to see if current account's division is allowed to display KIDs 
     */
    public Static Boolean isAccountFromCorrectDivision(Id fxAccId) {
        String allowedDivisionsString = SPGeneralSettings.getInstance().getValue('KID_Allowed_Divisions');
        List<String> allowedDivisions = String.isBlank(allowedDivisionsString) 
            ? new List<String>() : allowedDivisionsString.split(';');
        if (!allowedDivisions.isEmpty()) {
            fxAccount__c account = [SELECT Id, Division_Name__c FROM fxAccount__c WHERE Id = :fxAccId];
            return allowedDivisions.contains(account.Division_Name__c);
        } else {
            return false;
        }
    }

    public class KIDWrapper {
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String mt5AccountNumber {get; set;}
        @AuraEnabled
        public String mt5OrderNumber {get; set;}
        @AuraEnabled
        public String version {get; set;}
        @AuraEnabled
        public Datetime dateSent {get; set;}
        @AuraEnabled
        public List<KIDWrapper> children {get; set;}
    }

    public class KIDResponse {
        @AuraEnabled
        public Boolean isAllowed {get; set;}
        @AuraEnabled
        public List<KIDWrapper> results {get; set;}
    
    }
}