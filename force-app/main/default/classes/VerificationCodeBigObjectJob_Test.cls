/**
 * @File Name          : VerificationCodeBigObjectJob_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 12/12/2019, 3:09:20 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/12/2019   dmorales     Initial Version
**/
@isTest
private class VerificationCodeBigObjectJob_Test {
    @isTest
    static void textExecute() {
        List<Verification_Code_Setting__c> verifCode = new List<Verification_Code_Setting__c>();    
        verifCode.add( new Verification_Code_Setting__c(Name = 'ABCDEF', Order__c = 1 ));
        Test.startTest();
          System.enqueueJob(new VerificationCodeBigObjectJob(verifCode));   
        Test.stopTest();
         
        List<Verification_Code__b > codes = [SELECT Order__c, Code__c 
                                             FROM Verification_Code__b  
                                             WHERE Order__c = 1 AND Code__c = 'ABCDEF'];
        System.assert(codes.size() == 0);
    }
}