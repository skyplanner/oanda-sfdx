/**
 * @File Name          : Utilities_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 9/19/2019, 6:50:19 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/17/2019   dmorales     Initial Version
**/
@isTest
private class Utilities_Test {
    @isTest
    private static void getInstance() {
        String instance = '';
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;

        //Set this header to test it
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');

        if(orgType == 'Developer Edition' || insName.startsWithIgnoreCase('cs')){
            List<String> parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
            instance = parts[parts.size() - 4] + '.';
        }

        System.assertEquals(instance, Utilities.getInstance());
    }
    @isTest
    private static void getSubdomainPrefix(){
        //This will always be empty unless it's sandbox
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;

        String subdomain = '';
        if(insName.startsWithIgnoreCase('cs')){
            subdomain = UserInfo.getUserName().substringAfterLast('.')+ '-';
        }       
        System.assertEquals(subdomain, Utilities.getSubdomainPrefix());
    }

    @isTest
    private static void testPicklistDependent() {
        //Testing getDependentPickList method 
        Test.startTest();
        Map<Object, List<String>> result =
            Utilities.getDependentPicklistValues
                            (Case.Type);
               
        System.assert( result == null );       

        Map<Object, List<String>> resultWithValues =
            Utilities.getDependentPicklistValues
                            (Case.Type__c);
               
        System.assert( resultWithValues != null ); 
        Test.stopTest(); 
    }

    @isTest
    private static void testSetDiffs() {
        Set<Id> setOne = new Set<Id>{
            '500Rt00000A8nkAIAR', 
            '500Rt00000A8ZMPIA3',
            '500Rt00000A8dGdIAJ',
            '500Rt00000A8aANIAZ',
            '500Rt00000A8XM1IAN'
        };
            
        Set<Id> setTwo = new Set<Id>{
            '500Rt00000A8dGdIAJ',
            '500Rt00000A8aANIAZ'
        };

        Test.startTest();
        Set<Id> setDiff = Utilities.findDiff(setOne, setTwo);
        System.assertEquals(setDiff.size(), setOne.size() - setTwo.size());

        setTwo.addAll(setDiff);
        Set<Id> setDiff2 = Utilities.findDiff(setOne, setTwo);
        System.assertEquals(setDiff2.size(), 0);
        Test.stopTest(); 
    }
}