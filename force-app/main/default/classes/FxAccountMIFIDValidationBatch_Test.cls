/**
 * @File Name          : FxAccountMIFIDValidationBatch_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/24/2020   acantero     Initial Version
**/
@isTest
private class FxAccountMIFIDValidationBatch_Test {

	@isTest
	static void execute_test() {
		Boolean error = false;
		MIFIDValidationTestDataFactory.createValidableFxAccounts();
		Datetime now = Datetime.now();
		Test.startTest();
		FxAccountMIFIDValidationBatch validationBatch = new FxAccountMIFIDValidationBatch();
		Database.executeBatch(validationBatch, FxAccountMIFIDValidationBatch.BATCH_EXECUTION_SCOPE);
		Test.stopTest();
	    List<MiFID_Validation_Result__c> validationResul = [SELECT Id, Valid__c FROM MiFID_Validation_Result__c];
		System.assert(validationResul.size() > 0);
        
	}

}