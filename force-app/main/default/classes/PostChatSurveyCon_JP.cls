public with sharing class PostChatSurveyCon_JP {
    public Post_Chat_Survey__c postChatSurvey { get; private set; }

	public String isFirstContact { get; set; }
	public String wasIssueResolved { get; set; }
		
    public PostChatSurveyCon_JP() {
        postChatSurvey = new Post_Chat_Survey__c();
//        postChatSurvey.Live_Chat_Transcript__c = ApexPages.currentPage().getParameters().get('transcript');
        postChatSurvey.Chat_Key__c = ApexPages.currentPage().getParameters().get('chatKey');
    }

    public PageReference save() {
    	postChatSurvey.Is_First_Contact__c = (isFirstContact=='true');
    	postChatSurvey.Was_Issue_Resolved__c = (wasIssueResolved=='true');
    	
        insert postChatSurvey;
		
		PageReference nextPage = new PageReference('/apex/PostChatThankYou_JP');
        nextPage.setRedirect(true);
        return nextPage;
    }
    
    public List<SelectOption> getRecommendationRatingOptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('5','5 非常に高い')); 
        options.add(new SelectOption('4','4 高い')); 
        options.add(new SelectOption('3','3 普通'));
        options.add(new SelectOption('2','2 低い'));
        options.add(new SelectOption('1','1 非常に低い'));
        return options; 
    }
    
    public List<SelectOption> getIsFirstContactOptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('true','はい')); 
        options.add(new SelectOption('false','いいえ')); 
        return options; 
    }
    
    public List<SelectOption> getWasIssueResolvedOptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('true','はい')); 
        options.add(new SelectOption('false','いいえ')); 
        return options; 
    }

    public List<SelectOption> getOverallExperienceRatingOptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('5','5 非常に良い')); 
        options.add(new SelectOption('4','4 良い')); 
        options.add(new SelectOption('3','3 普通'));
        options.add(new SelectOption('2','2 悪い'));
        options.add(new SelectOption('1','1 非常に悪い'));
        return options; 
    }
}