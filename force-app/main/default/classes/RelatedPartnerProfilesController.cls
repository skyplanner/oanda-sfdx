/**
 * @Description  : Apex controller for relatedPartnerProfiles LWC component.
 * @Author       : Jakub Fik
 * @Date         : 2024-04-18
**/
public with sharing class RelatedPartnerProfilesController {

    /**
    * @description Method returns list of Affiliate__c records with same email addresses.
    * @author Jakub Fik | 2024-04-18 
    * @param profileId 
    * @return List<Affiliate__c> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<Affiliate__c> getRelatedPartnerProfiles(Id profileId) {
        List<Affiliate__c> relatedProfiles;
        try {
            if (String.isNotBlank(profileId)) {
                Affiliate__c profile = [
                    SELECT Id, Email__c
                    FROM Affiliate__c
                    WHERE Id = :profileId
                    WITH SECURITY_ENFORCED
                ];
                if (String.isNotBlank(profile.Email__c)) {
                        relatedProfiles = [
                        SELECT Id, Email__c, Division_Name__c, Username__c
                        FROM Affiliate__c
                        WHERE Email__c = :profile.Email__c AND ID != :profileId
                        WITH SECURITY_ENFORCED
                    ];
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return relatedProfiles;
    }
}