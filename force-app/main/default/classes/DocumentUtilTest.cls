@isTest
private class DocumentUtilTest {

	final static User REGISTRATION_USER = UserUtil.getUserByName(UserUtil.NAME_REGISTRATION_USER);

    @testSetup static void init(){

		//Preload Test with some test data
		TestDataFactory testHandler = new TestDataFactory();
		fxAccount__c fxAcct = testHandler.createFXTradeAccount(testHandler.createTestAccount());

		fxAcct.Division_Name__c = 'OANDA Canada';
		update fxAcct;
		
	}

  //test creating a new onBoarding case by inserting a document
  static testMethod void testInsertOnBoardDoc_1() {
        fxAccount__c fxa = [select id, Account__c, Lead__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and Account__c != null limit 1];
      
        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
          insert d;

          Test.stopTest();

          d = [select id, name, RecordTypeId, Case__c, fxAccount__c, Account__c from Document__c where Id = :d.Id];

          //check if an onboarding case is created
          Case c = [select id, Origin, OwnerId, AccountId, fxAccount__c from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() limit 1];
          
          system.assertEquals('Document Upload', c.Origin);
          system.assertNotEquals(REGISTRATION_USER.Id, c.OwnerId);
          system.assertEquals(fxa.Account__c, c.AccountId);
          system.assertEquals(fxa.Id, c.fxAccount__c);

          //check if the document is linked properly
          system.assertEquals(fxa.Account__c, d.Account__c);
          //system.assertEquals(null, d.Account__c);
          system.assertEquals(fxa.Id, d.fxAccount__c);
          //system.assertEquals(null, d.fxAccount__c);
          system.assertEquals(c.Id, d.Case__c);

      }
      
  }
  
  // test attaching document to an existing onboarding case
  static testMethod void testInsertOnBoardDoc_2() {
      fxAccount__c fxa = [select id, Account__c, Lead__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and Account__c != null limit 1];
      
      System.runAs(REGISTRATION_USER) {
	      Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
	      insert d;

	      Case c = [select id, Origin, OwnerId, AccountId, fxAccount__c, Status from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() limit 1];
	      c.Status = 'Re-opened';
	      c.Origin = 'Email - Onboarding';

	      update c;

	      Test.startTest();
	      
	      Document__c d1 = new Document__c(name = 'Test1', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
	      insert d1;

	      Test.stopTest();

	      d1 = [select id, name, RecordTypeId, Case__c, Case__r.Status, Case__r.Origin, fxAccount__c, Account__c from Document__c where Id = :d1.Id];

	      system.assertEquals('Email - Onboarding', d1.Case__r.Origin);
	      system.assertEquals(c.Id, d1.Case__c);
	      system.assertEquals('Re-opened', d1.Case__r.Status);

      }

  }

  // test attaching doc to a closed onboarding case
  static testMethod void testInsertOnBoardDoc_3() {
      fxAccount__c fxa = [select id, Account__c, Lead__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and Account__c != null limit 1];
      
      System.runAs(REGISTRATION_USER) {
	      Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
	      insert d;

	      Case c = [select id, Origin, OwnerId, AccountId, fxAccount__c, Status from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() limit 1];
	      c.Status = 'Closed';

	      update c;

	      Test.startTest();
	      
	      Document__c d1 = new Document__c(name = 'Test1', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
	      insert d1;

	      Test.stopTest();

	      d1 = [select id, name, RecordTypeId, Case__c, Case__r.Status, Case__r.Origin, fxAccount__c, Account__c from Document__c where Id = :d1.Id];

	      system.assertEquals(c.Id, d1.Case__c);
	      system.assertEquals('Re-opened', d1.Case__r.Status);

      }

  }

  //test creating onBoarding case for live lead
  static testMethod void testInsertOnBoardDoc_4(){
  	
    System.runAs(REGISTRATION_USER) {
          TestDataFactory testHandler = new TestDataFactory();

		  Lead ld = testHandler.createLiveLeads('test', 'test@abc.com');
		  insert ld;

		  fxAccount__c fxa = new fxAccount__c(name = 'test', recordTypeId = RecordTypeUtil.getFxAccountLiveId(), Lead__c = ld.id, Division_Name__c = 'OANDA Canada');
		  insert fxa;

          Test.startTest();

          Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
          insert d;

          Test.stopTest();

          d = [select id, name, RecordTypeId, Case__c, fxAccount__c, Account__c,Lead__c from Document__c where Id = :d.Id];

          //check if an onboarding case is created
          Case c = [select id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId() limit 1];
          system.assertNotEquals(REGISTRATION_USER.Id, c.OwnerId);

          system.assertEquals(fxa.Lead__c, c.Lead__c);
          system.assertEquals(fxa.Id, c.fxAccount__c);

          //check if the document is linked properly
          //system.assertEquals(null, d.Account__c);
          system.assertEquals(fxa.Lead__c, d.Lead__c);
          //system.assertEquals(null, d.Lead__c);
          system.assertEquals(fxa.Id, d.fxAccount__c);
          //system.assertEquals(null, d.fxAccount__c);
          system.assertEquals(c.Id, d.Case__c);

    }
  }

  //fxAccount is from non configured division
  //fxAccount is associated with a live lead
  static testMethod void testInsertOnBoardDoc_5(){
  	
    System.runAs(REGISTRATION_USER) {
          TestDataFactory testHandler = new TestDataFactory();

		  Lead ld = testHandler.createLiveLeads('test', 'test@abc.com');
		  insert ld;

		  fxAccount__c fxa = new fxAccount__c(name = 'test', recordTypeId = RecordTypeUtil.getFxAccountLiveId(), Lead__c = ld.id, Division_Name__c = 'abc');
		  insert fxa;

          Test.startTest();

          Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
          insert d;

          Test.stopTest();

          d = [select id, name, RecordTypeId, Case__c, fxAccount__c, Account__c,Lead__c from Document__c where Id = :d.Id];

          //check if an onboarding case is created
          List<Case> cases = [select id, Origin, OwnerId, AccountId, fxAccount__c, Lead__c from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId()];
          
          system.assertEquals(0, cases.size());

          //check if the document is linked properly
          system.assertEquals(null, d.Account__c);
          system.assertEquals(fxa.Lead__c, d.Lead__c);
          system.assertEquals(fxa.Id, d.fxAccount__c);
          system.assertEquals(null, d.Case__c);

    }
  }

  //fxAccount is from non configured division
  //fxAccount is associated with a account
  static testMethod void testInsertOnBoardDoc_6() {
        fxAccount__c fxa = [select id, Account__c, Lead__c, Division_Name__c from fxAccount__c where recordTypeId = :RecordTypeUtil.getFxAccountLiveId() and Account__c != null limit 1];
        fxa.Division_Name__c = 'abc';
        update fxa;

        System.runAs(REGISTRATION_USER) {
          Test.startTest();

          Document__c d = new Document__c(name = 'Test', recordTypeId = RecordTypeUtil.getScanDocumentTypeId(), fxAccount__c = fxa.Id);
          insert d;

          Test.stopTest();

          d = [select id, name, RecordTypeId, Case__c, fxAccount__c, Account__c from Document__c where Id = :d.Id];

          //check if an onboarding case is created
          List<Case> cases = [select id, Origin, OwnerId, AccountId, fxAccount__c from Case where recordTypeId = :RecordTypeUtil.getOnBoardingCaseTypeId()];
          
          system.assertEquals(0, cases.size());
          
          //check if the document is linked properly
          system.assertEquals(fxa.Account__c, d.Account__c);
          system.assertEquals(fxa.Id, d.fxAccount__c);
          system.assertEquals(null, d.Case__c);

      }
      
  }

}