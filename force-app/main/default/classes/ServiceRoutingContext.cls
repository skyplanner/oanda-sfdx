/**
 * @File Name          : ServiceRoutingContext.cls
 * @Description        : 
 * Manages the instances of the classes that allow you to customize
 * omnichannel routing
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/13/2024, 5:28:09 PM
**/
public inherited sharing class ServiceRoutingContext 
	extends BaseSRoutingContext {

	static ServiceRoutingContext instance;

	public static ServiceRoutingContext getInstance(
		List<PendingServiceRouting> routingObjList
	) {
		if (
			(instance != null) &&
			(instance.isSameTransaction(routingObjList))
		) {
			ServiceLogManager.getInstance().log(
				'getInstance -> use existing instance', // msg
				ServiceRoutingContext.class.getName() // category
			);
			return instance;
		}
		// else...
		ServiceLogManager.getInstance().log(
			'getInstance -> create new instance', // msg
			ServiceRoutingContext.class.getName() // category
		);
		instance = new ServiceRoutingContext(routingObjList);
		return instance;
	}

	public ServiceRoutingContext(
		List<PendingServiceRouting> routingObjList
	) {
		super(routingObjList);
	}

	protected override SPendingServiceRoutingFilter createPendingSRoutingFilter() {
		return new SkillsPSRoutingFilter();
	}

	protected override SRoutingHelper createRoutingHelper() {
		return new ServiceRoutingHelper(routingInfoList);
	}

	protected override SRoutingPriorityCalculator createPriorityCalculator() {
		return new SRoutingPriorityProcess();
	}

	protected override SSkillRequirementsManager createSkillReqManager() {
		return new ServiceSkillReqManager();
	}

	protected override void initRoutingInfoManagersMap() {
		// LiveChatTranscript
		registerRoutingInfoManager(
			Schema.LiveChatTranscript.SObjectType, // routingSObjectType
			new LiveChatSRoutingInfoManager() // routingInfoManager
		);
		// Case
		registerRoutingInfoManager(
			Schema.Case.SObjectType, // routingSObjectType
			new CaseSRoutingInfoManager() // routingInfoManager
		); 
		// Non_HVC_Case__c
		registerRoutingInfoManager(
			Schema.Non_HVC_Case__c.SObjectType, // routingSObjectType
			new NonHVCCaseSRoutingInfoManager() // routingInfoManager
		); 
		// MessagingSession
		registerRoutingInfoManager(
			Schema.MessagingSession.SObjectType, // routingSObjectType
			new MsgSessionSRoutingInfoManager() // routingInfoManager
		); 
	}
	
}