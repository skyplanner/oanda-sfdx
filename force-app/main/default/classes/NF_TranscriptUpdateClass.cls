/**
* Created by Neuraflash LLC on 28/06/18.
* Implementation : Chatbot
* Summary : Attaches a Lead record to Live Chat Trabscript
* Details : 1. Attach Lead with Live Agent Trancript Record.
*           2. Update the Live Chat Transcript Record with Page URL and SOID.
*
*/
public class NF_TranscriptUpdateClass {
    
    public static void getCaseUpdateonTranscript(List<LiveChatTranscript> listChat){
        try{
            List<Case> cases = new List<Case>();
            Set<String> chatKeys = new Set<String>();
            
            for(LiveChatTranscript transcript : listChat) {
                if(String.isNotBlank(transcript.caseId)){
                    Case cas = new Case();
                    cas.Id = transcript.caseId;
                    cas.ownerId = transcript.ownerId;
                    cases.add(cas);
                }
                else {
                    chatKeys.add(transcript.chatKey);
                }
            }
            if(chatKeys.size() > 0) {
                list<nfchat__Chat_Log__c> logs = [SELECT id, nfchat__Chat_Key__c,
                                                  (SELECT id FROM nfchat__cases__r ORDER BY createdDate DESC LIMIT 1)
                                                  FROM nfchat__Chat_Log__c WHERE nfchat__Chat_Key__c IN :chatKeys];
                
                Map<String, Case> chatKeyCasesMap = new Map<String, Case>();
                Map<String, nfchat__Chat_Log__c> chatKeyLogMap = new Map<String, nfchat__Chat_Log__c>();
                for(nfchat__Chat_Log__c log : logs) {
                    chatKeyLogMap.put(log.nfchat__Chat_Key__c, log);
                    for(case ld : log.nfchat__cases__r){
                        chatKeyCasesMap.put(log.nfchat__Chat_Key__c, ld);
                    }
                }
                List<nfchat__Chat_Log__c> logsToUpdate = new List<nfchat__Chat_Log__c>();
                List<LiveChatTranscript> transcriptsToUpdate = new List<LiveChatTranscript>();
                for(LiveChatTranscript transcript : listChat) {
                    if(String.isBlank(transcript.caseId) && String.isNotBlank(transcript.chatKey)) {
                        Case cas = chatKeyCasesMap.get(transcript.chatKey);
                        if(Cas != null) {
                            transcriptsToUpdate.add( new LiveChatTranscript( //update transcript record with lead
                                id = transcript.id,
                                CaseId = cas.id
                            ));
                            cases.add( new Case(
                                id = cas.id,
                                ownerId = transcript.ownerId
                            ));
                        }

                        nfchat__Chat_Log__c log = chatKeyLogMap.get(transcript.chatKey);
                        if(log != null) {
                            logsToUpdate.add( new nfchat__Chat_Log__c(
                                    id = log.id,
                                    nfchat__Live_Chat_Transcript__c = transcript.id
                            ));
                        }
                    }
                }
                if (transcriptsToUpdate.size() > 0) {
                    update transcriptsToUpdate; //dml update
                }
                if (logsToUpdate.size() > 0) {
                    update logsToUpdate;
                }
            }
            if (cases.size() > 0) {
                system.debug('Cases to update--->'+cases);
                update cases;
            }
        }catch (DmlException e) {
            NF_GenerateOandaCase.createDebugLog(e);
        }
    }
}