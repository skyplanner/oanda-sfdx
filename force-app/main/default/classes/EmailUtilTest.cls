@isTest
private class EmailUtilTest {

    static User CX_USER = UserUtil.getRandomTestUser();

    static testMethod void testEmailDelete() {
   	    
        System.runAs(CX_USER) {

        	// Test 1: user is NOT an Email Deletion User
        	Settings__c settings = new Settings__c(name = 'Default', Email_Deletion_User__c = '');
        	insert settings;

        	Case c = new Case(Subject='Test Case');
        	insert c;

        	EmailMessage em = new EmailMessage();
	    	em.FromAddress = 'test@example.com';
	    	em.ToAddress = 'hello@abc.com';
	    	em.Subject = 'Test email';
	    	em.ParentId = c.Id;
            em.Status = '3';
	    	insert em;

            try {
                delete em;
            } catch(Exception e) {
                
            }
            System.assertEquals(1, [SELECT Id FROM EmailMessage WHERE Id = :em.Id].size());

            // Test 2: user is an Email Deletion User
        	settings.Email_Deletion_User__c = userinfo.getName();
        	update settings;

            try {
                delete em;
            } catch(Exception e) {
                
            }
            System.assertEquals(0, [SELECT Id FROM EmailMessage WHERE Id = :em.Id].size());

            // Test 3: email is a draft
            settings.Email_Deletion_User__c = '';
            update settings;

            EmailMessage em2 = new EmailMessage();
	    	em2.FromAddress = 'test@example.com';
	    	em2.ToAddress = 'hello@abc.com';
	    	em2.Subject = 'Test email 2';
	    	em2.ParentId = c.Id;
            em2.Status = '5';
	    	insert em2;

            try {
                delete em2;
            } catch(Exception e) {
                
            }
            System.assertEquals(0, [SELECT Id FROM EmailMessage WHERE Id = :em2.Id].size());

        }
    }

    static testMethod void emailOverSizeTest() {
        //construct a big message
        String msg = '';
        
        for(integer i=0; i<32000; i++){
        	msg = msg + '0';
        }
        
        System.debug('message size: ' + msg.length());
        
        
        Case c = new Case(Subject = 'Test Case');
        insert c;

	    EmailMessage em = new EmailMessage();
	    em.FromAddress = 'test@example.com';
	    em.Incoming = false;
	    em.ToAddress = 'hello@abc.com';
	    em.Subject = 'Test email';
	    em.HtmlBody = msg;
	    em.ParentId = c.Id; 
	    
	    boolean overSized = false;
	    Test.startTest();
	    try{
	    	insert em;
	    }catch(System.DmlException e){
	    	overSized = true;
	    }
	    Test.stopTest();
	    
	    System.assertEquals(true, overSized);
        
        
    }

    static testMethod void sendEmailTest() {
        List<String> toAddresses = new List<String> { 'test1@example.com', 'test2@example.com' };
        List<String> subjects = new List<String> { 'Subject1', 'Subject2' };
        List<String> bodies = new List<String> { 'Body1', 'Body2' };
        
        EmailTemplate emailTemplate = new EmailTemplate(DeveloperName='sendEmailTest', Name='sendEmailTest', FolderId=UserInfo.getUserId(), TemplateType='text', isActive=true); // the FolderId is any junk id
        Lead lead = new Lead(Email='test3@example.com', LastName='Test');
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            insert emailTemplate;
            insert lead;
        }
        
        Test.startTest();
        EmailUtil.sendEmail(toAddresses[0], subjects[0], bodies[0]);    // Send to one email
        EmailUtil.sendEmail(toAddresses, subjects[1], bodies[1]);       // Send to multiple emails
        EmailUtil.sendEmailByTemplate(lead.Id, 'sendEmailTest', null);  // Use a template by referring to its name
        EmailUtil.sendEmailByTemplate(lead.Id, emailTemplate.Id, null); // Use a template by referring to its ID
        Integer emailInvocations = Limits.getEmailInvocations();        
        Test.stopTest();
        
        System.assertEquals(4, emailInvocations);
    }

    @isTest
    static void testUpdateRelatedCases() {
        Case c = new Case(Subject='Test Case');
        insert c;

        EmailMessage msg = new EmailMessage(
            Subject =  'Test email',
            ToAddress = 'test@oanda.com',
            ParentId = c.Id,
            Incoming = true
        );
        insert msg;

        Case updatedCase = [SELECT Id, Email_To_Address__c FROM Case LIMIT 1];
        
        Assert.areEqual('test@oanda.com', updatedCase.Email_To_Address__c);
    }
}