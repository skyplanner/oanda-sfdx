/**
 * @File Name          : ServiceCustomerTypeProvider.cls
 * @Description        : 
 * Returns the CustomerType based on the information of a specific customer
 * @Author             : acantero
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/20/2024, 1:08:39 AM
**/
public inherited sharing class ServiceCustomerTypeProvider 
	implements SCustomerTypeProvider {

	SCustomerTypeInfo customerInfo;

	public OmnichannelCustomerConst.CustomerType getCustomerType(
		SCustomerTypeInfo customerInfo
	) {
		this.customerInfo = customerInfo;
		OmnichannelCustomerConst.CustomerType result = null;

		if (customerInfo == null) {
			return result;
		}

		if (customerInfo.accountId != null) {
			result = getAccountCustomerType();

		} else if (customerInfo.isALead == true) {
			result = getLeadCustomerType();
		}
		
		return result;
	}

	@TestVisible
	OmnichannelCustomerConst.CustomerType getAccountCustomerType() {
		OmnichannelCustomerConst.CustomerType result = null;
		Date dateSixMonthsBefore = Date.today().addMonths(-6);

		if (customerInfo.isHVC == true) {
			result = OmnichannelCustomerConst.CustomerType.HVC;
			
		} else if (
			(customerInfo.funnelStage != OmnichanelConst.FUNNEL_STAGE_READY_FOR_FUNDING) &&
			(customerInfo.funnelStage != OmnichanelConst.FUNNEL_STAGE_FUNDED) &&
			(customerInfo.funnelStage != OmnichanelConst.FUNNEL_STAGE_TRADED) &&
			(customerInfo.createdDate >= dateSixMonthsBefore)
		) {
			result = OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER;
			
		} else if (
			(
				(customerInfo.funnelStage == OmnichanelConst.FUNNEL_STAGE_READY_FOR_FUNDING) ||
				(customerInfo.funnelStage == OmnichanelConst.FUNNEL_STAGE_FUNDED) ||
				(customerInfo.funnelStage == OmnichanelConst.FUNNEL_STAGE_TRADED)
			) && (customerInfo.lastTradeDate >= dateSixMonthsBefore)
		) {
			result = OmnichannelCustomerConst.CustomerType.ACTIVE;
			
		} else {
			result = OmnichannelCustomerConst.CustomerType.CUSTOMER;
		}

		return result;
	}

	@TestVisible
	OmnichannelCustomerConst.CustomerType getLeadCustomerType() {
		OmnichannelCustomerConst.CustomerType result = null;
		Date date90daysBefore = Date.today().addDays(-90);

		if (customerInfo.CreatedDate >= date90daysBefore) {
			result = OmnichannelCustomerConst.CustomerType.NEW_CUSTOMER;
			
		} else {
			result = OmnichannelCustomerConst.CustomerType.CUSTOMER;
		}

		return result;
	}
	
}