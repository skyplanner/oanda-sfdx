@isTest
public with sharing class EIDApiRestResourceTest {

    private static string restUrl = '/services/apexrest/api/v1/user/1234/eid_results';
    private static string restUrlNoUser = '/services/apexrest/api/v1/user/9999/eid_results';
    private static string postMethod = 'POST';

    
    @testSetup 
    static void init(){
        API_Access_Control__c control1 = new API_Access_Control__c(name = CustomSettings.API_NAME_EID_RESULT, Enabled__c = true);
        insert control1;
    }


    @isTest
    static void createEIDResultsNoFxAcc() {
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = restUrlNoUser;  
        req.httpMethod = postMethod;
        
        String responseTxt;
        // Make the REST Call out
        RestContext.request = req;
        RestContext.response = res;
        
        responseTxt = EIDApiRestResource.createEID(
            'EqifaxUS', 
            true, 
            'aa', 
            '0102', 
            '01020304', 
            true, 
            'aa', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        Test.stopTest();
        
        Assert.areEqual(true, responseTxt.contains('FAILURE'));
        
    }

    @isTest
    static void createEIDEqifaxUS()
    {
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 1234;
        update fxaccntObj;
        String responseTxt;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = restUrl;  
        req.httpMethod = postMethod;
        // Make the REST Call out
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        responseTxt = EIDApiRestResource.createEID(
            'EqifaxUS', 
            false, 
            '', 
            '102', 
            '[{\"name\":\"Credit Agency 2\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Enhanced Credit 1\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Utility\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}}]',			
            false, 
            '95991919111451654', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        Test.stopTest();
        
        system.assertEquals(true, responseTxt.contains('SUCCESS'));
        
        EID_Result__c eidRes = [
            SELECT Id, 
            fxTrade_User_Id__c,
            Provider__c,
            Reason__c, 
            EID_Response__c,
            Tracking_Number__c, 
            Date__c,
            Watchlist_Result__c,
            Provider_Link_API__c
            FROM EID_Result__c where Trulliio_Agency__c = null];
        
        System.assertEquals('watchlistSample', eidRes.Watchlist_Result__c, res);
        System.assertEquals('providerlinksample', eidRes.Provider_Link_API__c, res);
    }

    @isTest
    static void createEIDEqifaxUS2() {
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 1234;
        update fxaccntObj;
        String responseTxt;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = restUrl;  
        req.httpMethod = postMethod;

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        responseTxt = EIDApiRestResource.createEID(
            'EqifaxUS', 
            false, 
            '', 
            '102', 
            '[{\"name\":\"Credit Agency 2\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Enhanced Credit 1\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}},{\"name\":\"Utility\",\"response\":{\"fraud_flag\":{\"status\":true,\"flags\":\"[<array of fraud flags]\"},\"first_name\":\"match\",\"last_name\":\"nomatch\",\"dob\":\"match\",\"UnitNumber\":\"missing\",\"BuildingNumber\":\"match\",\"StreetName\":\"nomatch\",\"StreetType\":\"match\",\"City\":\"match\",\"PostalCode\":\"match\",\"StateProvinceCode\":\"match\",\"telephone\":\"nomatch\",\"ssn\":\"nomatch\"}}]',			
            false, 
            '95991919111451654', 
            1493901511000L,
            'watchlistSample',
            'providerlinksample');
        
        EID_Result__c result = [SELECT fxTrade_User_Id__c, Result__c, Tracking_Number__c FROM EID_Result__c where Trulliio_Agency__c = null];
        Test.stopTest();
        
        Assert.areEqual(true, responseTxt.contains('SUCCESS'));
        Assert.areEqual(1234, result.fxTrade_User_Id__c);
        Assert.areEqual('Failed', result.Result__c);
        Assert.areEqual('95991919111451654', result.Tracking_Number__c);
    }

    @isTest
    static void createEIDEquifaxDIT() {
        TestDataFactory testHandler = new TestDataFactory();
        fxAccount__c fxaccntObj = testHandler.createFXTradeAccount(testHandler.createTestAccount());
        fxaccntObj.fxTrade_User_Id__c = 1234;
        update fxaccntObj;
        String responseTxt;
        
        Test.startTest();

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = restUrl;  
        req.httpMethod = postMethod;

        RestContext.request = req;
        RestContext.response = res;
        
        responseTxt = EIDApiRestResource.createEID(
            'EquifaxDIT', 
            false, 
            '', 
            '105', 
            '{ \"transactionId\": \"11261f29-12de-43cb-84dc-6dc7d8ced8cb\", \"correlationId\": \"25d002df-f42c-42ce-984c-48fcac661b57\", \"referenceTransactionId\": \"reference-1\", \"decision\": \"Deny\", \"timestamp\": \"2021-10-25T07:40:22.0131732+00:00\", \"details\": [ { \"key\": \"phoneTrust\", \"value\": \"Y\" }, { \"key\": \"phoneVerification\", \"value\": \"N\" }, { \"key\": \"phoneVerificationReason\", \"value\": \"phoneNotValid,phoneTypeNotMobile\" }, { \"key\": \"phoneAffiliation\", \"value\": \"N\" }, { \"key\": \"phoneAffiliationReason\", \"value\": \"\" }, { \"key\": \"phoneInsights\", \"value\": \"N\" }, { \"key\": \"phoneInsightsReason\", \"value\": \"\" }, { \"key\": \"emailTrust\", \"value\": \"Y\" }, { \"key\": \"emailVerification\", \"value\": \"N\" }, { \"key\": \"emailVerificationReason\", \"value\": \"emailExists\" }, { \"key\": \"emailAffiliation\", \"value\": \"N\" }, { \"key\": \"emailAffiliationReason\", \"value\": \"\" }, { \"key\": \"emailInsights\", \"value\": \"N\" }, { \"key\": \"emailInsightsReason\", \"value\": \"accountDomainRisk\" }, { \"key\": \"addressTrust\", \"value\": \"N\" }, { \"key\": \"addressVerification\", \"value\": \"N\" }, { \"key\": \"addressVerificationReason\", \"value\": \"\" }, { \"key\": \"addressAffiliation\", \"value\": \"N\" }, { \"key\": \"addressAffiliationReason\", \"value\": \"\" }, { \"key\": \"addressInsights\", \"value\": \"N\" }, { \"key\": \"addressInsightsReason\", \"value\": \"\" }, { \"key\": \"identityVerification\", \"value\": \"N\" }, { \"key\": \"identityVerificationReason\", \"value\": \"\" }, { \"key\": \"identityResolution\", \"value\": \"Y\" }, { \"key\": \"identityResolutionReason\", \"value\": \"identityStrongCorroboration\" }, { \"key\": \"identityRisk\", \"value\": \"N\" }, { \"key\": \"identityRiskReason\", \"value\": \"identityRiskLow\" }, { \"key\": \"identityTrust\", \"value\": \"N\" }, { \"key\": \"SSNTrust\", \"value\": \"N\" }, { \"key\": \"SSNVerification\", \"value\": \"N\" }, { \"key\": \"SSNVerificationReason\", \"value\": \"\" }, { \"key\": \"SSNAffiliation\", \"value\": \"N\" }, { \"key\": \"SSNAffiliationReason\", \"value\": \"No Data Found\" }, { \"key\": \"SSNInsights\", \"value\": \"N\" }, { \"key\": \"SSNInsightsReason\", \"value\": \"\" }, { \"key\": \"dobVerification\", \"value\": \"N\" }, { \"key\": \"dobVerificationReason\", \"value\": \"DOB Not Verified\" }, { \"key\": \"dobAffiliation\", \"value\": \"N\" }, { \"key\": \"dobAffiliationReason\", \"value\": \"No Data Found\" }, { \"key\": \"dobInsights\", \"value\": \"N\" }, { \"key\": \"dobInsightsReason\", \"value\": \"DOB is in Future\" }, { \"key\": \"dobTrust\", \"value\": \"N\" }, { \"key\": \"deviceTrust\", \"value\": \"N\" }, { \"key\": \"deviceAssociation\", \"value\": \"N\" }, { \"key\": \"deviceAssociationReason\", \"value\": \"\" }, { \"key\": \"deviceReputation\", \"value\": \"N\" }, { \"key\": \"deviceReputationReason\", \"value\": \"\" } ], \"originalTransactionId\": \"11261f29-12de-43cb-84dc-6dc7d8ced8cb\" }',
            true,
            '95991919111451654',
            1493901511000L,
            'watchlistSample',
            'providerlinksample');     

        Test.stopTest();
        EID_Result__c result = [SELECT fxTrade_User_Id__c, Result__c, Tracking_Number__c, Transaction_Id__c, Correlation_Id__c, 
                                    Phone_Trust__c, Phone_Verification_Reason__c, Time_Stamp__c
                            FROM EID_Result__c 
                            WHERE Trulliio_Agency__c = null];
    
    
        Assert.areEqual(true, responseTxt.contains('SUCCESS'));
        Assert.areEqual(1234, result.fxTrade_User_Id__c);
        Assert.areEqual('Failed', result.Result__c);
        Assert.areEqual('95991919111451654', result.Tracking_Number__c);
        Assert.isTrue(result.Phone_Trust__c, 'Phone trust should be true');
        Assert.areEqual('phoneNotValid,phoneTypeNotMobile', result.Phone_Verification_Reason__c, 'Phone Not valid msg should be present');
        Assert.isTrue(result.Time_Stamp__c != null, 'Date should be valid');
    }


    @isTest
    static void getEIDErr()
    {


        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/api/v1/user/abc/eid_results';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        EIDApiRestResource.getEID();
        Test.stopTest();
        
        system.assertEquals(res.statusCode, 500);
    }

    @isTest
    static void getEID()
    {
        Lead lead = new Lead(FirstName = 'Robert Gabriel', LastName='Mugabe', Email = 'test1@oanda.com');
        insert lead;
        
        fxAccount__c tradeAccount = new fxAccount__c();
        tradeAccount.Account_Email__c = 'test1@oanda.com';
        tradeAccount.Funnel_Stage__c = FunnelStatus.TRADED;
        tradeAccount.RecordTypeId = FxAccountUtil.ID_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE;
        tradeAccount.Lead__c = lead.Id;
        tradeAccount.Division_Name__c = 'OANDA Canada';
        tradeAccount.Birthdate__c = date.newInstance(1990, 11, 21);
        tradeAccount.Citizenship_Nationality__c = 'Canada';
        tradeAccount.fxTrade_User_Id__c = 56789;
        tradeAccount.fxTrade_Global_ID__c  = '56789+'+String.valueOf(RecordTypeUtil.getFxAccountLiveId()).left(15);
        insert tradeAccount;
        
        EID_Result__c eidResultObj = new EID_Result__c();
        eidResultObj.fxAccount__c = tradeAccount.Id;
        eidResultObj.Provider__c = 'EquifaxUS';
        eidResultObj.Date__c = system.today();
        eidResultObj.Reason__c = '1012';
        eidResultObj.Tracking_Number__c = '123456';
        eidResultObj.Result__c = 'pass';
        insert eidResultObj;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/api/v1/user/56789/eid_results';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        EIDApiRestResource.getEID();
        Test.stopTest();
        
        system.assertEquals(res.statusCode, 200);
    }
}