/**
 * Tests: ComplyAdvantageCommentsDownloader
 * @author Fernando Gomez
 * @since 11/15/2021
 */
@isTest
global class ComplyAdvantageCommentsDownloaderTest {
	/**
	 * Tests:
	 * public List<ComplyAdvantageSearchComment> getComments()
	 */
	@isTest
	private static void getComments_good() {
		ComplyAdvantageCommentsDownloader d;
		List<ComplyAdvantageSearchComment> comments;

		insert new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);		
		Test.setMock(HttpCalloutMock.class, new Mock1());

		Test.startTest();
		d = new ComplyAdvantageCommentsDownloader('123456');
		comments = d.getComments();
		System.assertEquals(2, comments.size());
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public List<ComplyAdvantageSearchComment> getComments()
	 */
	@isTest
	private static void getComments_fail() {
		CalloutMock mock;
		ComplyAdvantageCommentsDownloader d;
		List<ComplyAdvantageSearchComment> comments;

		insert new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);

		mock = new CalloutMock(
			400,
			'{' +
				'"code": 400,' +
				'"status": "error"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageCommentsDownloader('123456');

		try {
			comments = d.getComments();
			System.assert(false, 'should fail');
		} catch (Exception ex) {
			// all good
		}
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public void addComment(String comment, String entityId)
	 */
	@isTest
	private static void addComment_good() {
		CalloutMock mock;
		ComplyAdvantageCommentsDownloader d;
		List<ComplyAdvantageSearchComment> comments;

		insert new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);
		
		mock = new CalloutMock(
			200,
			'{' +
				'"code": 200,' +
				'"status": "success"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageCommentsDownloader('123456');
		d.addComment('comment', '123456');
		Test.stopTest();
	}

	/**
	 * Tests:
	 * public void addComment(String comment, String entityId)
	 */
	@isTest
	private static void addComment_fail() {
		CalloutMock mock;
		ComplyAdvantageCommentsDownloader d;
		List<ComplyAdvantageSearchComment> comments;

		insert new Comply_Advantage_Search__c(
			Search_Id__c = '123456'
		);
		
		mock = new CalloutMock(
			400,
			'{' +
				'"code": 400,' +
				'"status": "error"' +
			'}',
			'OK',
			null
		);
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		d = new ComplyAdvantageCommentsDownloader('123456');

		try {
			d.addComment('comment', '123456');
			System.assert(false, 'should fail');
		} catch (Exception ex) {
			// all good
		}
		Test.stopTest();
	}

	global class Mock1 implements HttpCalloutMock {
		global HTTPResponse respond(HttpRequest req) {
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setStatusCode(200);
			res.setStatus('OK');

			if (req.getEndpoint().contains('/users'))
				res.setBody('{' +
					'"code": 200,' +
					'"status": "success",' +
					'"content": {' +
						'"data": [' +
							'{' +
								'"id": 12102,' +
								'"email": "fnazari@oanda.com",' +
								'"name": "Fareshta Nazari",' +
								'"updated_at": "2020-03-27 20:29:07",' +
								'"created_at": "2020-03-26 06:34:29"' +
							'}' +
						']' +
					'}' +
				'}');
		
			if (req.getEndpoint().contains('/comments'))
				res.setBody('{' +
					'"code": 200,' +
					'"status": "success",' +
					'"content": [' +
						'{' +
							'"id": "61932b07199f3c127d229a82",' +
							'"created_at": "2021-11-16T03:52:39.244000",' +
							'"message": "this is a comment",' +
							'"user_id": 12102' +
						'},' +
						'{' +
							'"id": "6193422022fe54aec1368616",' +
							'"created_at": "2021-11-16T05:31:12.127000",' +
							'"message": "this is a another comment",' +
							'"user_id": 12102' +
						'}' +
					']' +
				'}');

			return res;
		}
	}
}