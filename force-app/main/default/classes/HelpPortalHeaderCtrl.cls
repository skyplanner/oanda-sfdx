/**
 * @File Name          : HelpPortalHeaderCtrl.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 4/24/2020, 12:54:17 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/20/2019   dmorales     Initial Version
 * 01/07/2020 		   : Sahil changes as according to SP-8877/SP-8869 
 **/
public without sharing class HelpPortalHeaderCtrl extends HelpPortalBase {

public String regionCode {set; get;}
public String countryName {set; get;}
public String countryCode {set; get;}
public static String regionFromFooter {set; get;}
public List<SelectOption> languages {get; private set; }

public Boolean isBottomCookie {set;
	                       get {
				       return checkCookiePosition();
			       }}
public String riskBaseText {set;
	                    get {
				    return getRiskBaseText();
			    }}


public HelpPortalHeaderCtrl() {
	super();
	languages = getLanguageOptions();
}

private List<SelectOption> getLanguageOptions() {
	List<SelectOption> result = new List<SelectOption>();
	//List<Map<String, String>> languageList = getLanguages();
        System.debug('p.regionCode --->'+regionCode);
        System.debug('p.countryName --->'+countryName);
        System.debug('p.regionFromFooter --->'+regionFromFooter);
        System.debug('p.countryCode --->'+countryCode);
        System.debug('p.region --->'+region);

   /* for (Map<String, String> p : languageList){
        System.debug('p.key --->'+p.get('label'));
        System.debug('p.value --->'+p.get('value'));
        System.debug('p.p.keySet --->'+p.keySet());
 		String label = p.get('label');
        if(region =='OC' &&(label=='English' ||label=='Spanish' )){
            result.add(new SelectOption(p.get('value'), label));
        }else if(region =='OCAN' && (label=='English' ||label=='French')){
            result.add(new SelectOption(p.get('value'), label));
        }else if(region =='OAU'&&(label=='English'|| label=='German' ||label=='Spanish'||label=='French'||label=='Italian'||label=='Portuguese (Brazil)'||label=='Chinese (Simplified)'||label=='Chinese (Traditional)')){
            result.add(new SelectOption(p.get('value'),label));
        }else if(region =='OGM'&&(label=='English'|| label=='German' ||label=='Spanish'||label=='French'||label=='Italian'||label=='Portuguese (Brazil)'||label=='Chinese (Simplified)'||label=='Chinese (Traditional)')){
            result.add(new SelectOption(p.get('value'), label));
        }else if(region =='OAP'&&(label=='English'|| label=='Chinese (Simplified)'||label=='Chinese (Traditional)' ||label=='French' ||label=='German' ||label=='Italian'||label=='Portuguese (Brazil)' ||label=='Spanish')){
            result.add(new SelectOption(p.get('value'), label));
        }else if(region =='OEM'&&(label=='English'|| label=='German' ||label=='Spanish'||label=='French'||label=='Italian'||label=='Portuguese (Brazil)')){
            result.add(new SelectOption(p.get('value'), label));
        }else if(region =='OEL'&&(label=='English'|| label=='German' ||label=='Spanish'||label=='French'||label=='Italian'||label=='Portuguese (Brazil)'||label=='Chinese (Simplified)'||label=='Chinese (Traditional)')){
            result.add(new SelectOption(p.get('value'), label));
        }
        //result.add(new SelectOption(p.get('value'), p.get('label')));

    }*/
    if(region =='OC'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('es', 'Spanish'));
    }else if(region =='OCAN'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('fr', 'French'));
    }else if(region =='OAU'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('zh_CN', 'Chinese (Simplified)'));
        result.add(new SelectOption('zh_TW', 'Chinese (Traditional)'));
      /*  result.add(new SelectOption('es', 'Spanish'));
        result.add(new SelectOption('pt_BR', 'Portuguese'));
        result.add(new SelectOption('fr', 'French'));
        result.add(new SelectOption('de', 'German'));
        result.add(new SelectOption('it', 'Italian'));*/
    }else if(region =='OGM'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('zh_CN', 'Chinese (Simplified)'));
        result.add(new SelectOption('zh_TW', 'Chinese (Traditional)'));
        result.add(new SelectOption('fr', 'French'));
      //  result.add(new SelectOption('de', 'German'));
      //  result.add(new SelectOption('it', 'Italian'));
        result.add(new SelectOption('pt_BR', 'Portuguese'));
        result.add(new SelectOption('es', 'Spanish'));
        result.add(new SelectOption('vi', 'Vietnamese'));
        result.add(new SelectOption('th', 'Thai'));
        result.add(new SelectOption('in', 'Indonesian'));
        result.add(new SelectOption('ms', 'Malay'));
    }else if(region =='OAP'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('zh_CN', 'Chinese (Simplified)'));
        result.add(new SelectOption('zh_TW', 'Chinese (Traditional)'));
    /*    result.add(new SelectOption('fr', 'French'));
        result.add(new SelectOption('de', 'German'));
        result.add(new SelectOption('it', 'Italian'));
        result.add(new SelectOption('pt_BR', 'Portuguese'));
        result.add(new SelectOption('es', 'Spanish')); */
    }else if(region =='OEM'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('es', 'Spanish'));
        result.add(new SelectOption('de', 'German'));
        result.add(new SelectOption('fr', 'French'));
        result.add(new SelectOption('pt_BR', 'Portuguese'));
        result.add(new SelectOption('it', 'Italian'));
    }else if(region =='OEL'){
        result.add(new SelectOption('en_US', 'English'));
        result.add(new SelectOption('es', 'Spanish'));
        result.add(new SelectOption('de', 'German'));
      //  result.add(new SelectOption('zh_CN', 'Chinese (Simplified)'));
        //result.add(new SelectOption('zh_TW', 'Chinese (Traditional)'));
        result.add(new SelectOption('fr', 'French'));
        result.add(new SelectOption('pt_BR', 'Portuguese'));
        result.add(new SelectOption('it', 'Italian'));
    }
    return result;
}

public String getRiskBaseText(){
	String riskText = '';
	String labelName = 'HelpPFooterRisk'+regionCode;
    System.debug('regionCode'+regionCode);
	Component.Apex.OutputText output = new Component.Apex.OutputText();
	try{
		output.expressions.value = '{!$Label.' + labelName + '}';
		riskText = String.valueOf(output.value);
      riskText = riskText.replace('\n', '<br>');
	}
	catch(Exception e) {
		return riskText;
	}

	return riskText;
}

public Boolean  checkCookiePosition(){
	Boolean isBottom = false;
	if (regionCode == 'OEL') {
		isBottom = true;
	}
	return isBottom;
}

}