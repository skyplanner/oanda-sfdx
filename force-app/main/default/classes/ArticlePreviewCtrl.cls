/**
 * Controller for page: ArticlePreview
 * @author Fernando Gomez
 * @since 5/13/2019
 * @LastModified: 04/16/2020 by dmorales
 */
global without sharing class ArticlePreviewCtrl extends HelpPortalBase {
	global Boolean isPreviewAvailable { get; private set; }
	global String errorMessage { get; private set; }
	global transient String translationsJson { get; private set; }
	global Map<String, ArticleWrapper> mappedHtml { get; private set; }
	global ArticleWrapper currentArticle { get; private set; }
	global List<SelectOption> languageOptions { get; private set; }

	/**
	 * Main controller.
	 */
	global ArticlePreviewCtrl() {
		super('en_US');
		//	System.debug('ArticlePreviewCtrl() Contructor');
		languageOptions = new List<SelectOption>();
	}

	/**
	 * Main controller.
	 */
	global ArticlePreviewCtrl(Id documentPreviewId) {
		super('en_US');
		isPreviewAvailable = false;
		buildPreviewHtml(documentPreviewId);
	}

	/**
	 * Main controller.
	 */
	global ArticlePreviewCtrl(String translationsJson) {
		super('en_US');	
		//System.debug('ArticlePreviewCtrl(String translationsJson) Contructor');
		this.translationsJson = translationsJson;
	}

	/**
	 * Setsup the first preview
	 */
	global PageReference extractPreview() {
		Id previewId;
		DocumentManager dManager;
		Document previewDocument;
		Map<String, String> params;
		List<ItemWrapper> translations;
		PageReference current;

		current = ApexPages.currentPage();
		if (current != null) {
			params = current.getParameters();
			previewId = params.get('previewId');
			buildPreviewHtml(previewId);
		}

		return null;
	}

	/**
	 * Setsup the first preview
	 */
	private void buildPreviewHtml(Id previewId) {
		DocumentManager dManager;
		Document previewDocument;
		List<ItemWrapper> translations;
		isPreviewAvailable = false;

		if (previewId != null) {
			dManager = new DocumentManager();

			try {
				previewDocument = dManager.getPreviewDocument(previewId);
			    translations = (List<ItemWrapper>)
					JSON.deserialize(
						previewDocument.Body.toString(), 
						List<ItemWrapper>.class);
					
				mappedHtml = getHtmlMapping(translations);
				changeLanguage();
				
				for (ItemWrapper w : translations)
					languageOptions.add(new SelectOption(
						w.languageCode, w.languageLabel));

				isPreviewAvailable = true;
			} catch (Exception ex) {
				errorMessage = ex.getMessage() + '<br />' +
					ex.getStackTraceString();
				System.debug('ERROR: ' + errorMessage);
			}
		}
	}

	/**
	 * Changes the current language and preview
	 */
	global PageReference changeLanguage() {
		isPreviewAvailable = mappedHtml.containsKey(language);
		currentArticle = mappedHtml.get(language);
		return null;
	}

	/**
	 * @param previewDocumentId
	 * Id of the current preview document
	 */
	global Id getPreview(Id previewDocumentId) {
		DocumentManager dManager;
		Document cDocument;
		dManager = new DocumentManager();
		cDocument = dManager.createPreviewDocumentId(previewDocumentId);
		cDocument.Body = Blob.valueOf(translationsJson);
		upsert cDocument;
		return cDocument.Id;
	}

	/**
	 * Creates and returns the new FAQ
	 * @param previewDocumentId
	 */
	global FAQ__kav createArticle(Id previewDocumentId) {
		FAQ__kav faq;
		List<ItemWrapper> translations;
		ArticleWrapper main;

		// first, we need actual translations
		translations = (List<ItemWrapper>)
			JSON.deserialize(translationsJson, List<ItemWrapper>.class);
		mappedHtml = getHtmlMapping(translations);

		// english is the basic lnaguage
		main = mappedHtml.get('en_US');

		faq = new FAQ__kav(
			Title = main.question,
			Answer__c = main.html,
			PreviewDocument__c = previewDocumentId,
			UrlName = getUrl(main.question),
			AllowFeedback__c = true
		);
		insert faq;
		return faq;
	}

	/**
	 * Updates and returns the updated FAQ
	 * @param knwoledgeArticleId
	 * @param previewDocumentId
	 */
	global FAQ__kav updateArticle(Id knwoledgeArticleId, Id previewDocumentId) {
		FAQ__kav faq;
		Id versionId;
		List<ItemWrapper> translations;
		ArticleWrapper main;

		// first, we need actual translations
		translations = (List<ItemWrapper>)
			JSON.deserialize(translationsJson, List<ItemWrapper>.class);
		mappedHtml = getHtmlMapping(translations);

		// english is the basic lnaguage
		main = mappedHtml.get('en_US');

		// and we need to obtain the FAQ
		faq = getArticle(knwoledgeArticleId);

		// if the FAQ is in published, we need to create a version
		// in draft
		if (faq.PublishStatus == 'Online') {
			versionId = KbManagement.PublishingService.editOnlineArticle(
				faq.KnowledgeArticleId, false);
			faq = getArticle(versionId);
		}

		// we modify the url only if the title has changed
		if (main.question != faq.Title)
			faq.UrlName = getUrl(main.question);

		faq.Title = main.question;
		faq.Answer__c = main.html;
		faq.PreviewDocument__c = previewDocumentId;
		update faq;
		return faq;
	}

	/**
	 * Given a knowledge article build with this tool, this method
	 * the original strucure of the article before converting to HTML.
	 * @param knowledgeArticleId
	 */
	global TranslationsWrapper getTranslations(Id knowledgeArticleId) {
		FAQ__kav faq, draft = null, published = null;
		DocumentManager dManager;
		Document cDocument;
		TranslationsWrapper result;

		// we need to find the article
		try {
			faq = getArticle(knowledgeArticleId);
		} catch(QueryException ex) {
			// we throw an exception if faq was not found
			throw new ArticleNotFoundException();
		}

		// if this faq is Onlin we try to find a draft version..
		// if its draft, we try to find a published version
		switch on faq.PublishStatus {
			when 'Online' {
				draft = getDraftVersion(faq.KnowledgeArticleId);
			}
			when 'Draft' {
				published = getPublishedVersion(faq.KnowledgeArticleId);
			}
		}

		// the article must have a preview document..
		// otherwise, it was not built with this tool
		if (String.isBlank(faq.PreviewDocument__c))
			throw new ArticleNotSupportedException();

		// now, we have to fetch the document with the translation json
		dManager = new DocumentManager();
		cDocument = dManager.getPreviewDocumentOrNull(faq.PreviewDocument__c);

		// if the preview document was not found, 
		// the article also has a problem for the tool
		if (cDocument == null)
			throw new ArticleNotSupportedException();

		// now that we found the document, the body must contain the JSON 
		// for the translations..
		try {
			translationsJson = cDocument.Body.toString();
			result = new TranslationsWrapper();
			result.faq = faq;
			result.draft = draft;
			result.published = published;
			result.translations = (List<ItemWrapper>)
				JSON.deserialize(translationsJson, List<ItemWrapper>.class);
			return result;
		} catch(Exception ex) {
			throw new ArticleNotSupportedException();
		}
	}

	/**
	 * Return the article with the specified id
	 * @param knowledgeArticleId
	 */
	private FAQ__kav getArticle(Id knowledgeArticleId) {
		return [
			SELECT Id,
				KnowledgeArticleId,
				Title,
				Answer__c,
				PreviewDocument__c,
				UrlName,
				AllowFeedback__c,
				PublishStatus
			FROM FAQ__kav
			WHERE Id = :knowledgeArticleId
		];
	}

	/**
	 * Return the article with the specified id
	 * @param knowledgeArticleId
	 */
	private FAQ__kav getDraftVersion(Id knowledgeArticleId) {
		try {
			return [
				SELECT Id
				FROM FAQ__kav
				WHERE KnowledgeArticleId = :knowledgeArticleId
				AND PublishStatus = 'Draft'
				LIMIT 1
			];
		} catch(QueryException ex) {
			return null;
		}
	}

	/**
	 * Return the article with the specified id
	 * @param knowledgeArticleId
	 */
	private FAQ__kav getPublishedVersion(Id knowledgeArticleId) {
		try {
			return [
				SELECT Id
				FROM FAQ__kav
				WHERE KnowledgeArticleId = :knowledgeArticleId
				AND PublishStatus = 'Online'
				LIMIT 1
			];
		} catch(QueryException ex) {
			return null;
		}
	}

	/**
	 * Wraps the current text in the specified tag
	 * @param tag
	 * @param text
	 * @param escape
	 * @param attributes
	 */
	private String wrapInTag(String text, String tag,
			Boolean escape, Map<String, String> attributes) {
		String result = '<{0} {1}>{2}</{3}>',
			attrs = '';

		if (attributes != null)
			for (String attrName : attributes.keySet())
				attrs += attrName + '="' + attributes.get(attrName) + '" ';

		return String.format(result,
			new List<String> {
				tag,
				attrs,
				(String.isBlank(text) ? '' : escape ? text.escapeHtml4() : text),
				tag
			});
	}

	private String wrapSection(String text, Integer index, String collapsed ){
		String showCollapsed = collapsed == 'true' ? '1':'0';
		String classA = collapsed == 'true' ? 'collapsed': '';
		String ariaExpanded = collapsed == 'true' ? 'false': 'true';
		String classP = collapsed == 'true' ? '': 'show';
		
		String html = '<div id="section'+index+'_'+showCollapsed+'" class="accordion accordionArticle">' +
		'<h5 class="px-3 mb-0 pb-2">'+ // class="px-3 mb-0 pb-3"
			'<a href="javascript:void(0)"'+  
			 ' data-toggle="collapse"' +
			 ' data-target="#panel'+index+'"' +
			 ' aria-expanded="'+ariaExpanded+'"'+
			 ' aria-controls="panel'+index+'" '+
			 ' class="h4 d-flex text-decoration-none '+ classA+'"' + //section_a'+index+' text-truncate align-items-center
			 ' target="_section_a'+index+'"> '+
				'<span class="section-span-symbol section_span'+index+'" data-toggle-handle="true">+</span>'+
				'<span class="section-accordion-header">'+text+'</span>'+
			'</a>'+
		'</h5>'+
		'<div id="panel'+index+'" data-parent="#section'+index+'_'+showCollapsed+'" class="pl-4 collapse '+classP+'">'+
			'<div class="list-group section-container"> '+
				'<sust>'+ index + '</sust>'+
			'</div>'+
		'</div>'+
		'</div>';
		return html;
	}

	/**
	 * Wraps the current text in the specified tag
	 * @param tag
	 * @param text
	 * @param marginIndex
	 */
	private String getUrl(String question) {
		return question.replaceAll('[^a-zA-z0-9]', '-')
			.left(100)
			.replaceAll('^-+|-+$', '');
	}

	/**
	 * Returns the html in the article structure
	  @param translations
	 */
	private Map<String, ArticleWrapper> getHtmlMapping(List<ItemWrapper> translations) {
		Map<String, ArticleWrapper> result;
		ArticleWrapper current;
		Decimal width;
		String temp;
		Map<String, String> attributes, innerAttr;
		result = new Map<String, ArticleWrapper>();
		attributes = new Map<String, String>();
		innerAttr = new Map<String, String>();

		for (ItemWrapper w : translations) {
			current = new ArticleWrapper();
			current.question = w.question;
			current.html = '';
			//element inside sections will be add in a different html array
			List<String> sectionHtmlContainer = new List<String>();
			String hmtlSection = '';
			Boolean isSectionOpen = false;
			Integer index = 0;
			
			// we need to turn each item into html
			if (w.items != null)
				for (ItemWrapper item : w.items){
					 String htmlItem = '';
					switch on item.tag {
						when 'section_start' {					
							 isSectionOpen = true;													
							 current.html += wrapSection(item.value, index, item.collapsed);	
						}
						when 'section_end' {
							sectionHtmlContainer.add(hmtlSection);
							isSectionOpen = false;
							hmtlSection = '';
							index ++;				
						}
						when 'h2', 'h4' {
							attributes.clear();
							attributes.put('style', getBasicStyle());
							htmlItem = 
								wrapInTag(item.value, item.tag, true, attributes);						
						}
						when 'p' {
							attributes.clear();
							attributes.put('style', getBasicStyle());
							htmlItem = wrapInTag(item.value, 'p', false, attributes);							
						}
						when 'ul' {
							attributes.clear();
							attributes.put('style', getLiBasicStyle());
							htmlItem = '<ul>';
							for (ItemWrapper innerItem : item.items)
							    htmlItem += 
								wrapInTag(innerItem.value, 'li', false, attributes);
								htmlItem += '</ul>';
							
						}
						when 'ol' {
							attributes.clear();
							attributes.put('style', getLiBasicStyle());
							htmlItem += '<ol>';
							for (ItemWrapper innerItem : item.items)
								 htmlItem += 
								wrapInTag(innerItem.value, 'li', false, attributes);
								 htmlItem += '</ol>';					
						}
						when 'table' {
							// we ensure tables are 100% width

							htmlItem = 
								'<table style="' + getTableBasicStyle() + '"><thead>';
							width = 100 /
								(item.columns.size() != 0 ? item.columns.size() : 1);

							// we also need to manually set the with for each cell
							attributes.clear();
							attributes.put('style',
								String.format(getTdBasicStyle(),
									new List<String> { width.setScale(2) + '' }));

							// we add all columns
							for (ItemWrapper innerItem : item.columns)
								 htmlItem +=
									wrapInTag(innerItem.value, 'th', false, attributes);
							// we close the header
							 htmlItem += '</thead><tbody>';
							// we add the rows
							for (ItemWrapper innerItem : item.rows) {
								// we open the table row
								 htmlItem += '<tr>';
								// we then add the cells
								for (ItemWrapper deeperItem : innerItem.cells)
									 htmlItem += 
										wrapInTag(deeperItem.value,
											'td', false, attributes);
								// we close the table row
								 htmlItem += '</tr>'; 
							}
							// we then close the body and table
							 htmlItem += '</tbody></table>';
						}
						when 'img' {
							attributes.clear();
							attributes.put('style', getBasicStyle());

							innerAttr.clear();
							innerAttr.put('src', item.fileUrl);
							innerAttr.put('style', getImgBasicStyle());

							htmlItem = 
								wrapInTag(
									wrapInTag('', 'img', false, innerAttr) +
									'<br/>' +
									wrapInTag(item.caption, 'i', true, null),
								'p', false, attributes);
						}
						when 'a' {
							temp = item.id + 
								(String.isBlank(item.extra) ? '' : item.extra);
							attributes.clear();
							attributes.put('id', temp);
							attributes.put('name', temp);
							htmlItem = wrapInTag('', item.tag, false, attributes);
						}
						when 'video' {
							temp = 'https://www.youtube.com/embed/{0}?autoplay={1}';
							attributes.clear();
							switch on item.type {
								when 'youtube.com' {
									attributes.put('src',
										String.format(temp,
											new List<String> {
												item.videoId,
												item.autoplay ? '1' : '0'
											}));
								}
							}
							attributes.put('style', getIframeBasicStyle());
							attributes.put('scrolling', 'auto');
							attributes.put('frameborder', '0');
							attributes.put('allowfullscreen', 'true');
							htmlItem = wrapInTag('', 'iframe', false, attributes);

							attributes.clear();
							attributes.put('style', getBasicStyle());
							htmlItem += 
								wrapInTag(
									wrapInTag(item.caption, 'i', true, null),
									'p', false, attributes);
						}
					}
					if(isSectionOpen)
					  hmtlSection += htmlItem;						
			     	else
				      current.html += htmlItem;
				}
				   
			
			//if there is a section in sectionHtmlContainer, then we replace the
			//html container inside the current html.
		
			if(sectionHtmlContainer.size()>0){
				Integer i = 0;
				for(String htmlSectionItem : sectionHtmlContainer){
					current.html = current.html.replace('<sust>'+i+'</sust>', htmlSectionItem);						
					i++;				
				}
			}		
			// we place the entry in the map
			result.put(w.languageCode, current);
		}

		return result;
	}

	/**
	 * Styles tag for H2, H3, P
	 */
	private String getBasicStyle() {
		return 'margin-top: 0;' +
			'margin-bottom: 24px;';
	}
	/**
	 * Styles tag for li
	 */
	private String getLiBasicStyle() {
		return 'margin-top: 0;' +
			'margin-bottom: 14px;';
	}

	/**
	 * Styles tag for TABLE
	 */
	private String getTableBasicStyle() {
		return 'min-width: 100%;' +
			'table-layout: fixed;' +
			'border-spacing: 0;' ;
	}

	/**
	 * Styles tag for TD
	 */
	private String getTdBasicStyle() {
		return 
			'width: {0}px;' +
			'vertical-align: top;' +
			'text-align: left;' +
			'vertical-align: top;' +
			// 'border-bottom: 1px solid;' +
			'padding: 12px;';
	}

	/**
	 * Styles tag for IMG
	 */
	private String getImgBasicStyle() {
		return 'max-width: 100%;';
	}

	/**
	 * Styles tag for IFRAME
	 */
	private String getIframeBasicStyle() {
		return 'width: 550px;' +
			'height: 300px;';
	}

	/**
	 * @param translations
	 * @param previewDocumentId
	 * @return the id of the poreview document
	 */
	@AuraEnabled
	global static Id getPreview(String translationsJson, Id previewDocumentId) {
		ArticlePreviewCtrl ctrl;
		try {
			ctrl = new ArticlePreviewCtrl(translationsJson);
			return ctrl.getPreview(previewDocumentId);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @return a list of currently active language
	 */
	@AuraEnabled
	global static List<Map<String, String>> getSupportedLanguages() {
		ArticlePreviewCtrl ctrl;
		try {
			ctrl = new ArticlePreviewCtrl();
			return ctrl.getLanguages();
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param fileName
	 * @param contentType
	 * @param base64Data
	 * @return creates and return the url of a new doc
	 */
	@AuraEnabled
	global static String convertFileToDocument(String fileName,
			String contentType, String base64Data) {
		DocumentManager manager;
		try {
			manager = new DocumentManager();
			return manager.convertFileToDocument(fileName, contentType, base64Data);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param translations
	 * @return 
	 */
	@AuraEnabled
	global static FAQ__kav createNewArticle( 
			String translationsJson, Id previewDocumentId) {
		ArticlePreviewCtrl ctrl;
		try {
			ctrl = new ArticlePreviewCtrl(translationsJson);
			return ctrl.createArticle(previewDocumentId);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param translations
	 * @return 
	 */
	@AuraEnabled
	global static FAQ__kav updateExistentArticle(Id knwoledgeArticleId, 
			String translationsJson, Id previewDocumentId) {
		ArticlePreviewCtrl ctrl;
		try {
			ctrl = new ArticlePreviewCtrl(translationsJson);
			return ctrl.updateArticle(knwoledgeArticleId, previewDocumentId);
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param knowledgeArticleId
	 * @return TranslationsWrapper
	 */
	@AuraEnabled
	global static TranslationsWrapper getTranslationsFromArticle(
			Id knowledgeArticleId) {
		ArticlePreviewCtrl ctrl;
		try {
			ctrl = new ArticlePreviewCtrl();
			return ctrl.getTranslations(knowledgeArticleId);
		} catch(ArticleNotFoundException ex) {
			throw new AuraHandledException(
				'That article does not exist. It may have been deleted ' +
				'or the ID might be missing or misstyped. ' +
				'Contact an administrator if you think this is a mistake.');
		} catch(ArticleNotSupportedException ex) {
			throw new AuraHandledException(
				'There was a problem parsing the structure of the article.' +
				'Usually, this means that the article was not built ' +
				'using this tool and does not have the correct format. ' +
				'Please, contact an administrator if you think this is a mistake.');
		} catch(Exception ex) {
			System.debug('EXCEPTION: ' + 
				ex.getMessage() + '\n' + 
				ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * Wrapper question and html in a single question
	 */
	global class ArticleWrapper { 
		global String question { get; set; }
		global String html { get; set; }
	}

	/**
	 * Translations and article info 
	 */
	global class TranslationsWrapper { 
		@AuraEnabled
		global FAQ__kav faq { get; set; }
		@AuraEnabled
		global FAQ__kav draft { get; set; }
		@AuraEnabled
		global FAQ__kav published { get; set; }
		@AuraEnabled
		global List<ItemWrapper> translations { get; set; }
	}

	/**
	 * Wrapper for items as added in the tool.
	 */
	global class ItemWrapper {
		@AuraEnabled
		global String languageCode { get; set; }
		@AuraEnabled
		global String languageLabel { get; set; }
		@AuraEnabled
		global String question { get; set; }
		@AuraEnabled
		global String name { get; set; }
		@AuraEnabled
		global String id { get; set; }
		@AuraEnabled
		global String extra { get; set; }
		@AuraEnabled
		global String tag { get; set; }
		@AuraEnabled
		global String value { get; set; }
		@AuraEnabled
		global Boolean deleted { get; set; }
		@AuraEnabled
		global String caption { get; set; }
		@AuraEnabled
		global List<ItemWrapper> items { get; set; }
		@AuraEnabled
		global List<ItemWrapper> columns { get; set; }
		@AuraEnabled
		global List<ItemWrapper> rows { get; set; }
		@AuraEnabled
		global List<ItemWrapper> cells { get; set; }
		@AuraEnabled
		global String fileUrl { get; set; }
		@AuraEnabled
		global Long fileSize { get; set; }
		@AuraEnabled
		global String fileType { get; set; }
		@AuraEnabled
		global String type { get; set; }
		@AuraEnabled
		global String videoId { get; set; }
		@AuraEnabled
		global Boolean autoplay { get; set; }
		@AuraEnabled
		global Boolean isValid { get; set; }
		@AuraEnabled
		global String collapsed { get; set; }
	}

	/**
	 * Throwable exceptions
	 */
	global class ArticleNotFoundException extends Exception { }
	global class ArticleNotSupportedException extends Exception { }
}