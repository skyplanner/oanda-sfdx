/**
 * @description       : Create Action platform events for Mule to consume, and update the IHS Document Status to 'Sent'
 * @author            : Eugene Jung
 * @last modified on  : 11-22-2023
 * @last modified by  : Eugene Jung
*/
public with sharing class IHSEventManager 
{    
    // Doc who need to be sent to IHS
    Map<Id, Document__c> docsMap;

    // docIds successfully published
    Set<Id> publishedDocs;

    // Manager class for action platform events
    ActionUtil actUtil;

    /**
     * Constructor
     */
    public IHSEventManager(List<Document__c> docs) 
    {
        docsMap = new Map<Id, Document__c>(docs);
        actUtil = new ActionUtil();
    }

    /**
     * Publish an action IHS platform event which mulesoft is subscribed
     */
    public void scan() 
    {
        if(docsMap == null || docsMap.isEmpty())
            return;

        // Publish action platform events
        publish();

        // Update docs
        updateDocs();
    }

    /**
     * Publish IHS platform event
     */
    private void publish() 
    {
        if(docsMap.isEmpty())
            return;

        publishedDocs = actUtil.publishIhsActions(docsMap.values());
    }

    /**
     * Set IHS Document Status to 'Sent'
     */
    private void updateDocs() 
    {
        if(publishedDocs == null || publishedDocs.isEmpty())
            return;

        Document__c doc;
        List<Document__c> docsToUpdate = new List<Document__c>();

        for(Id docId :publishedDocs) 
        {
            doc = new Document__c(Id=docId);
            doc.IHS_Document_Status__c = Constants.IHS_DOCUMENT_STATUS_SENDING_TO_IHS_INITIATED;
            docsToUpdate.add(doc);
        }

        if(docsToUpdate.isEmpty())
            return;

        // Nothing to do on document trigger with this IHS status update
        TriggersUtil.disableObjTriggers(TriggersUtil.Obj.DOCUMENT);
        
        update docsToUpdate;
        
        TriggersUtil.enableObjTriggers(TriggersUtil.Obj.DOCUMENT);
    }
   
}