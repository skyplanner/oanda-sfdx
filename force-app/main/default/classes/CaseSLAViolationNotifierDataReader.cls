/**
 * @File Name          : CaseSLAViolationNotifierDataReader.cls
 * @Description        : 
 * @Author             : aniubo
 * @Group              : 
 * @Last Modified By   : aniubo
 * @Last Modified On   : 2/28/2024, 4:36:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/28/2024, 4:36:29 PM   aniubo     Initial Version
**/
public with sharing class CaseSLAViolationNotifierDataReader implements SLAViolationDataReader {
	public List<SObject> getData(SLAViolationNotificationData notificationData) {
		List<Case> cases = new List<Case>();
		CaseSLAViolationNotificationData caseData = (CaseSLAViolationNotificationData) notificationData;
		if (caseData.newList == null || caseData.newList.isEmpty()) {
			return new List<SObject>();
		}
		for (Case newCase : caseData.newList) {
			Case oldCase = caseData.oldMap.get(newCase.Id);
			if (
				String.isNotBlank(newCase.Milestone_Violated__c) &&
				((newCase.Milestone_Violated__c !=
				oldCase.Milestone_Violated__c) ||
				((newCase.Milestone_Violation_Time__c != null) &&
				(newCase.Milestone_Violation_Time__c !=
				oldCase.Milestone_Violation_Time__c)))
			) {
				cases.add(newCase);
			}
		}
		return cases;
	}
}