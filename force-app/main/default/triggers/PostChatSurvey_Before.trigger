trigger PostChatSurvey_Before on Post_Chat_Survey__c (before insert, before update) {
	PostChatSurveyUtil.setLiveChatTranscript(Trigger.new, Trigger.oldMap, Trigger.isInsert);
}