trigger Entity_Contact_After on Entity_Contact__c (after insert, after update)
{
   
    public static Map<ID, Schema.RecordTypeInfo> contactsRecordTypeMap = Schema.SObjectType.Entity_Contact__c.getRecordTypeInfosById(); 
    
   if(Settings__c.getValues('Default') != null &&
      Settings__c.getValues('Default').Comply_Advantage_Enabled__c &&
      Trigger.new.size() == 1 &&
      contactsRecordTypeMap.get(Trigger.new[0].RecordTypeId).getName() == RecordTypeUtil.NAME_ENTITY_CONTACT_GENERAL)
   {
       ComplyAdvantageHelper.validate(((Trigger.old != null && Trigger.old.size() > 0) ? Trigger.old[0] : null), Trigger.New[0]);
   }
   
}