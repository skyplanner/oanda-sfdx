/**
 * @File Name          : ServiceResource.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/16/2021, 3:14:25 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/4/2021, 5:35:18 PM   acantero     Initial Version
**/
trigger ServiceResource on ServiceResource (after insert, after update) {
    if (!ServiceResourceTriggerUtility.isEnabled()) {
        return;
    }
    //else..
    ServiceResourceTriggerUtility handler;
    if (Trigger.isInsert) {
        handler = new ServiceResourceTriggerUtility(
            Trigger.new,
            null
        );
        if (Trigger.isAfter) {
            handler.handleAfterInsert();
        }
        //...
    } else if (Trigger.isUpdate) {
        handler = new ServiceResourceTriggerUtility(
            Trigger.new,
            Trigger.oldMap
        );
        if (Trigger.isAfter) {
            handler.handleAfterUpdate();
        }
    }
   
}