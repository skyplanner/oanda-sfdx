/**
 * GeneralAction__e trigger
 * It is a general platform event how is not going to have logic,
 * it will call some callable class to do the process
 */
trigger GeneralActionTrigger on GeneralAction__e (after insert) {
    GeneralAction.process(trigger.new);
}