/* Name: Account_Before 
 * Description : Apex Trigger on Account for Before Insert and Before Update.
 * Author: Re-Authored by Sri 
 * 
 * Update : @Sri - Added Recursive Check  and code restructure 2020-05-28.
 */
trigger Account_After on Account (after insert, after update)  
{
    if(UserUtil.currentUser != null && 
        !UserUtil.currentUser.Triggers_Disabled__c &&
        TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_AFTER))
    {
        // creating the Trigger Context fields after filtering the records in the recursive check
        CheckRecursive recursiveHandler = new CheckRecursive(Trigger.NewMap,Trigger.OldMap);
        List<Account> triggerNew ;
        List<Account> triggerOld ;
        Map<Id,Account> triggerNewMap ;
        Map<Id,Account> triggerOldMap ;
		Settings__c settings = Settings__c.getValues('Default');

        If( recursiveHandler.triggerNew != null &&  recursiveHandler.triggerNew.size()>0)
        {
            triggerNew = recursiveHandler.triggerNew;
            triggerNewMap = new Map<Id, Account>(triggerNew);
        }
        If( recursiveHandler.triggerOld != null &&  recursiveHandler.triggerOld.size()>0)
        {
            triggerOld = recursiveHandler.triggerOld;
            triggerOldMap = new Map<Id, Account>(triggerOld);
        }

        if(Trigger.isUpdate) 
        {
            //If there are any (new) records after filtering for recursiveness then continue with actual trigger business 
            if(triggerNew != null && triggerNew.size() > 0 &&
					triggerOld != null && triggerOld.size() > 0)
            {
                System.debug('TRG Update Account_After: AccountUtil.updateFxAccountOwner');
                fxAccountOwnerUtil.updateFxAccountOwner(TriggerNew, TriggerOldMap);

				// SP-9239
				if (settings != null &&
						settings.Enable_Current_Desk_Integration__c &&
						CurrentDeskSync.runOnce())
					CurrentDeskSync.syncCustomerRecords(triggerNew, triggerOld);
                
                if (settings != null && settings.Comply_Advantage_Enabled__c && TriggerNew.size() == 1)
                {
                    ComplyAdvantageHelper.validate(TriggerOld[0], TriggerNew[0]);
                }

                // Update hashed fields
                fxAccountHashUtil.setHashedFields(
                    triggerNew, triggerOld);
                
                // Calculate customer risk assessment
                AccountUtil.calculateCustomerRiskAssessment(
                    triggerNew, TriggerOldMap);
                
                new AccountTriggerHandler(
                    triggerNew, TriggerOldMap).onAfterUpdate();
            }
        }

        if(Trigger.isInsert)
        {
            //If there are any (new) records after filtering for recursiveness then continue with actual trigger business        
            if (triggerNew != null && triggerNew.size() > 0 )
            {
                if(settings != null && settings.Comply_Advantage_Enabled__c && TriggerNew.size() == 1)
                {
                    ComplyAdvantageHelper.linkAccountComplyAdvantageCase(TriggerNew);
                }

                // Calculate customer risk assessment
                AccountUtil.calculateCustomerRiskAssessment(
                    triggerNew);
                
                new AccountTriggerHandler(
                    trigger.new, null).onAfterInsert();
            }
        }

        //adding the processed IDs to the recursive check static variable so that the these records will be excluded next time
        if(TriggerNewMap != null && TriggerNewMap.size()>0)
        {
            checkRecursive.SetOfIDs.addAll(TriggerNewMap.keySet());
        }
    }
}