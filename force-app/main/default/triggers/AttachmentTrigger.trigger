/**
 * @Description  : Trigger for Attachment sobject.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-02
**/
trigger AttachmentTrigger on Attachment(
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete
) {
    new AttachmentTriggerHandler().run();
}