trigger Task_Before on Task (before insert, before update) {
	if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c && TriggersUtil.isTriggerEnabled(
			TriggersUtil.Triggers.TRIGGER_TASK_BEFORE)){
		if(Trigger.isInsert) {
			System.debug('TRG Insert Task_Before: TaskUtil.checkForWeekendActivityDate');
			TaskUtil.checkForWeekendActivityDate(Trigger.new);
			TaskUtil.checkForMeaningfulInteractions(Trigger.new, null);
			TaskUtil.syncCallResultService(Trigger.new);
		}
		if(Trigger.isUpdate) {
			TaskUtil.checkForMeaningfulInteractions(Trigger.new, Trigger.oldMap);
			TaskUtil.completeUnansweredTask(Trigger.new, Trigger.oldMap);
		}
	}
	/* TODO 2014-03-18 enabled after deploy
	TaskUtil.updateCaseModificationTime(Trigger.new);
	*/
}