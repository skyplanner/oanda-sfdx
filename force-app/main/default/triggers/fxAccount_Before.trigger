/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 10-05-2022
 * @last modified by  : Dianelys Velazquez
**/
trigger fxAccount_Before on fxAccount__c (before insert, before update, before delete) {}