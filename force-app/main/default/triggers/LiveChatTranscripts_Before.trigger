trigger LiveChatTranscripts_Before on LiveChatTranscript (before insert, before update) {
	User runningUser = [SELECT Triggers_Disabled__c FROM User WHERE Id = :UserInfo.getUserId()];
	if (!runningUser.Triggers_Disabled__c) {
		LiveChatTranscriptUtil.linkLiveChatTranscriptToCaseAndLead(Trigger.new);
		LiveChatTranscriptUtil.linkToAccount(Trigger.new);
		LiveChatTranscriptUtil.populateWordCount(Trigger.new, Trigger.oldMap);
	}
}