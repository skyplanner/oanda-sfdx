/**
 * @description       : 
 * @author            : OANDA
 * @group             : 
 * @last modified on  : 11-22-2022
 * @last modified by  : Yaneivys Gutierrez
**/
trigger fxAccount_After on fxAccount__c (after insert, after update) {}