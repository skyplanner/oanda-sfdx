trigger FxAccountRestEventTrigger on FxAccount_Upsert__e (after insert) {
    Map<Decimal, List<String>> fxAccUserIdToPayloads = new Map<Decimal, List<String>>();
    Map<Decimal, Decimal> fxAccUserIdToReplayCounter = new Map<Decimal, Decimal>();
    Integer recordsCounterLimit = 200;
    Integer replayCounter = 10;
    Integer recordsCounter = 0;
    Set<FxAccount_Upsert__e> eventsToRepublish = new Set<FxAccount_Upsert__e>();
    List<DEcimal> eventsToRepublishUserIds = new List<Decimal>();

    for (FxAccount_Upsert__e e : Trigger.new) {
        recordsCounter ++;
        if(recordsCounter <  recordsCounterLimit ) {
            if (e.Replay_Counter__c == null || e.Replay_Counter__c  <= replayCounter) {
                if(fxAccUserIdToPayloads.containsKey(e.fxTrade_User_Id__c)) {
                    fxAccUserIdToPayloads.get(e.fxTrade_User_Id__c).add(e.User_Payload__c);
                    fxAccUserIdToReplayCounter.put(e.fxTrade_User_Id__c, e.Replay_Counter__c);

                } else{
                    fxAccUserIdToPayloads.put(e.fxTrade_User_Id__c, new List<String>{e.User_Payload__c});
                    fxAccUserIdToReplayCounter.put(e.fxTrade_User_Id__c, e.Replay_Counter__c);

                }
            } else {
                Logger.error(
                    'replay event replay infitie loop1',
                    'replay event replay replay infitie loop',
                    'replay event replay:  ' + e + ' replay count: ' + e.Replay_Counter__c);
            }
        } else  { 
            FxAccount_Upsert__e fxAccEvent = new FxAccount_Upsert__e();
            fxAccEvent.fxTrade_User_Id__c = e.fxTrade_User_Id__c;
            fxAccEvent.User_Payload__c = e.User_Payload__c;
            fxAccEvent.Replay_Counter__c = e.Replay_Counter__c != null &&  e.Replay_Counter__c > 0
                                            ? e.Replay_Counter__c + 1 
                                            : 1;

            eventsToRepublish.add(fxAccEvent);
            eventsToRepublishUserIds.add(fxAccEvent.fxTrade_User_Id__c);
            
            
        } 
    }

    FxAccountUpsertTriggerHandler fxAccUpHandler  = new FxAccountUpsertTriggerHandler();
    fxAccUpHandler.handleFxAccUpsert(fxAccUserIdToPayloads, fxAccUserIdToReplayCounter);
    if(eventsToRepublish.size() > 0) {
        List<FxAccount_Upsert__e> events = new List<FxAccount_Upsert__e>(eventsToRepublish);
       List<Database.SaveResult> sr = EventBus.publish(events);
        recordsCounter = 0;
    }
   }