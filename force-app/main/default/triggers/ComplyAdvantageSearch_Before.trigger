trigger ComplyAdvantageSearch_Before on Comply_Advantage_Search__c (
		before insert,
		before update) {
	// we do nothing if triggers are disabled
	if (UserUtil.areTriggersEnabledForUser() &&
			ComplyAdvantageHelper.complyAdvantageSearchAfterTriggerEnabled) {
		// on insert or update, we perform conditional updates
		if (trigger.isInsert || trigger.isUpdate)
			// SP-8600, we perform conditional update
			ComplyAdvantageHelper.performConditionalUpdates(trigger.new, trigger.old);
	}
}