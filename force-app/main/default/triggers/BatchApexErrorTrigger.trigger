/**
 * Created by akajda on 17/04/2023.
 */

trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {

    if(Trigger.isInsert){
        BatchApexErrorHandler.upsertErrorLogs(Trigger.new);
    }

}