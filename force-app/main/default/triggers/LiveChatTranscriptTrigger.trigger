trigger LiveChatTranscriptTrigger on LiveChatTranscript (after insert, after delete) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            LiveChatTranscriptHandler h = new LiveChatTranscriptHandler();
            h.handleAfterInsert(Trigger.new);
        }
        else if(Trigger.isDelete){
            LiveChatTranscriptHandler h = new LiveChatTranscriptHandler();
            h.handleAfterDelete(Trigger.old);
        }
    }
}