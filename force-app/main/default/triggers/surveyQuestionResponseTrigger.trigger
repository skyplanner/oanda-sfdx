/* Name: surveyQuestionResponseTrigger
 * Description : Apex Trigger  on SurveyQuestionResponse__c for Before Insert and Before Update.
 * Author: Sahil Handa
 */
trigger surveyQuestionResponseTrigger on SurveyQuestionResponse__c (before insert,before update) {
    if(Trigger.isInsert || Trigger.isUpdate){
        surveyQuestionResponseHelper.updateResponseInNumber(Trigger.new);
    }
}