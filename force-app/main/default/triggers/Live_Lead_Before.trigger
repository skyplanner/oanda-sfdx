trigger Live_Lead_Before on Live_Lead__c (before insert) {
    
    LiveLeadUtil.checkLiveLeadConverted(Trigger.New);
}