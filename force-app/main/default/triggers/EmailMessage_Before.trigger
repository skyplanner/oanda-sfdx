/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 08-02-2022
 * @last modified by  : Ariel Niubo
**/
trigger EmailMessage_Before on EmailMessage (before delete, before insert) {}