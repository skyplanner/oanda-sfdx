/**
 * @description       : Trigger for all before events
 * @author            : Gilbert Gao
 * @date			  : Jan 26, 2017
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : Yaneivys Gutierrez
**/
trigger EID_Result_Before on EID_Result__c (Before Insert) {



}