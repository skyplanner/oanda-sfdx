/**
 * User Trigger
 * @author:	Michel Carrillo (SkyPlanner)
 * @since:	02/04/2021
 */
trigger UserTrigger on User (before insert) {
    if(Trigger.isBefore && Trigger.isInsert){
        UserTriggerHandler h = new UserTriggerHandler();
        h.handleBeforeInsert(Trigger.new);
    }
}