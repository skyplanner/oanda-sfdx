trigger Practice_Lead_Before on Practice_Lead__c (before delete, before insert, before update) {
	
	if(Trigger.isInsert){
		PracticeLeadUtil.checkDemoLeadExists(Trigger.New);
	}
    
}