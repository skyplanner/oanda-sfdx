trigger DuplicateRecordItemTrigger on DuplicateRecordItem (after insert) {
    DuplicateRecordItemTriggerHandler th = new DuplicateRecordItemTriggerHandler();
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            th.onAfterInsert(Trigger.new);
        }
    }
}