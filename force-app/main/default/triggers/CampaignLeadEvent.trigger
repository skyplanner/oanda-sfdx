trigger CampaignLeadEvent on Campaign_Leads__e (after insert) {
    Integer recordsCounterLimit = 200;
    Integer replayCounterLimit = 10;
    Integer recordsCounter = 0;

    List<String> campaignLeadsToProcess = new List <String>();
    Set<Campaign_Leads__e> eventsToRepublish = new Set<Campaign_Leads__e>();

    for (Campaign_Leads__e cle : Trigger.new) {
        system.debug('MJ in loop: ' + cle);
        recordsCounter ++;
        if(recordsCounter <  recordsCounterLimit ) {
            if (cle.Replay_Counter__c == null || cle.Replay_Counter__c  <= replayCounterLimit) {
                campaignLeadsToProcess.add(cle.CL_Payload__c);
            } else {
                Logger.error(
                    'CampaignLeadEvent trigger',
                    'infitie loop on CampaignLeadEvent',
                    'replay event replay count:  ' + cle.Replay_Counter__c + ' replay event: ' + cle);
            }
        } else  { 
            Campaign_Leads__e cl_replay = new Campaign_Leads__e();
            cl_replay.CL_Payload__c = cle.CL_Payload__c;
            cl_replay.Replay_Counter__c = cle.Replay_Counter__c != null &&  cle.Replay_Counter__c > 0
                                            ? cle.Replay_Counter__c + 1 
                                            : 1;
                                            
            eventsToRepublish.add(cl_replay);

        } 
        
    }
    Logger.info(
        'test trigger',
        'test',
        'input:  ' + campaignLeadsToProcess );
        system.debug('MJ trigger: ' + campaignLeadsToProcess);
    CampaignLeadEventTriggerHandler clhandler  = new CampaignLeadEventTriggerHandler();
    clhandler.handleCamapignLeadInsert(campaignLeadsToProcess);
    system.debug('MJ trigger: after ' );
    if(eventsToRepublish.size() > 0) {
        List<Campaign_Leads__e> events = new List<Campaign_Leads__e>(eventsToRepublish);
        List<Database.SaveResult> sr = EventBus.publish(events);
        recordsCounter = 0;
    }
}