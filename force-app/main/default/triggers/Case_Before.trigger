trigger Case_Before on Case (before insert, before update) {
    CaseUtil.linkToAccountContactOrLead(Trigger.new);
    CaseUtil.copyReasonFailed(Trigger.new);
    //CaseUtil.attachCaseToLead(Trigger.new);
    if(Trigger.isInsert) {
    	CaseUtil.createLeadIfNeeded(Trigger.new);
    }

    //Validate Onboarding Case Approval
    if(Trigger.isUpdate && !UserUtil.getIntegrationProfileIDs().Contains(userinfo.getProfileId())) {
    	CaseUtil.validateCaseApproval(Trigger.new, Trigger.oldMap);
    }

}