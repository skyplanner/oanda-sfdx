/**
 * Created by Agnieszka Kajda on 26/10/2023.
 */

trigger FeedCommentTrigger on FeedComment (before insert, before update, before delete) {

    if(!UserUtil.canUserModifyFeedComments()){
        for(FeedComment fc : Trigger.new){
            fc.addError('You cannot create, edit nor delete comments.');
        }
    }

}