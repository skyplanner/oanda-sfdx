trigger ComplyAdvantageSearch_After on Comply_Advantage_Search__c (after insert, after update) 
{
	if (UserUtil.areTriggersEnabledForUser() &&
			ComplyAdvantageHelper.complyAdvantageSearchAfterTriggerEnabled) 
    {
        if (Trigger.isUpdate && Trigger.new.size() == 1)    
        {
            if(Trigger.new[0].Match_Status__c != Trigger.old[0].Match_Status__c)
            {
                ComplyAdvantageHelper.updateMatchStatus(Trigger.new[0],Trigger.old[0]);
            }
            
			if(Trigger.new[0].Match_Status__c == Trigger.old[0].Match_Status__c && 
               string.isNotBlank(Trigger.new[0].Match_Status_override_reason__c) &&
               Trigger.new[0].Match_Status_override_reason__c != Trigger.old[0].Match_Status_override_reason__c)
            {
                ComplyAdvantageHelper.addNote(Trigger.new[0].Id,
                                              UserInfo.getName() + ' updated Match status override reason.',
                                              Trigger.old[0].Match_Status_override_reason__c);
            }
        }

        if(Trigger.isInsert)
        {
             ComplyAdvantageHelper.downloadEntities(trigger.new, trigger.old);
        }
		if (Trigger.isUpdate) 
        {
            ComplyAdvantageHelper.downloadEntities(trigger.new, trigger.old);

			// SP-7630, turning monitoring on anf off bulkified,
			// so many searches can sync at once
			ComplyAdvantageHelper.updateSearchMonitorStatus(trigger.new, trigger.old);
		}
    }
}