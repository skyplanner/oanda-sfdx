trigger Note_Before on Note (before insert, before delete) 
{
	 if(trigger.isDelete)
     {
          for(Note a : Trigger.old) 
          {
              if(!UserUtil.isSystemAdministratorProfile(UserInfo.getUserId()) && !UserUtil.isIntegrationUserId(UserInfo.getUserId())) 
              {
               	 a.addError('Error: Notes cannot be deleted');
              }
         }
     }
}