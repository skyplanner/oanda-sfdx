trigger TAX_Declaration on TAX_Declarations__c (before insert, after insert,
        before update, after update,
        before delete, after delete,
        after undelete) {
    TaxDeclarationTriggerHandler handler;

    // Order: before
    if (trigger.isBefore) 
    {	
        /* // Operation: Insert
        if (trigger.isInsert) {
        } */

        /* // Operation: Update
        if (trigger.isUpdate) {
        } */

        /* // Operation: Delete
        if (trigger.isDelete) {
        } */
    }

    // Order: after
    else if (trigger.isAfter) 
    {
        // Operation: Insert
        if (trigger.isInsert) {
            handler = new TaxDeclarationTriggerHandler(trigger.new);
            handler.handleAfterInsert();
        }
        // Operation: Update
        if (trigger.isUpdate) {
            handler = new TaxDeclarationTriggerHandler(trigger.new, trigger.oldMap);
            handler.handleAfterUpdate();
        }
        /* // Operation: Delete
        if (trigger.isDelete) {
        } */

        /* // Operation: Undelete
        /* if (trigger.isUndelete) {

        } */
    }
}