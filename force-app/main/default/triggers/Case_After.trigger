trigger Case_After on Case (after insert, after update) 
{
    // link the onboarding case with Comply Advantage case
    if(UserUtil.currentUser != null &&
        !UserUtil.currentUser.Triggers_Disabled__c &&
        TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_CASE_AFTER))
    {
        if(Trigger.isInsert) {
            new CaseTriggerHandler(
                Trigger.new).onAfterInsert();
        } else if(Trigger.isUpdate) {
            new CaseTriggerHandler(
                Trigger.new,
                Trigger.oldMap).onAfterUpdate();
        }
    }
   
}