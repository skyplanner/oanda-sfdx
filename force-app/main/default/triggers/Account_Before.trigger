/**
 * Name: Account_Before 
 * Description : Apex Trigger on Account for Before Insert and Before Update.
 * Author: Re-Authored by Sri 
 * 
 * Update : @Sri - Added Recursive Check  and code restructure 2020-05-28.
 */
trigger Account_Before on Account (before insert, before update, before delete) 
{
    if(UserUtil.currentUser != null && 
        !UserUtil.currentUser.Triggers_Disabled__c &&
        TriggersUtil.isTriggerEnabled(
            TriggersUtil.Triggers.TRIGGER_ACCOUNT_BEFORE))
    {
        // creating the Trigger Context fields after filtering the records in the recursive check
        CheckRecursive recursiveHandler = new CheckRecursive(Trigger.NewMap,Trigger.OldMap);
        List<Account> triggerNew ;
        List<Account> triggerOld ;
        Map<Id,Account> triggerNewMap ;
        Map<Id,Account> triggerOldMap ;
        If( recursiveHandler.triggerNew != null &&  recursiveHandler.triggerNew.size()>0)
        {
            triggerNew = recursiveHandler.triggerNew;
            triggerNewMap = new Map<Id, Account>(triggerNew);
        }
        If( recursiveHandler.triggerOld != null &&  recursiveHandler.triggerOld.size()>0)
        {
            triggerOld = recursiveHandler.triggerOld;
            triggerOldMap = new Map<Id, Account>(triggerOld);
        }

        if(Trigger.isInsert){
            new AccountTriggerHandler(trigger.new, null).onBeforeInsert();
        }

        if(Trigger.isUpdate) 
        {
            //If there are any (new) records after filtering for recursiveness then continue with actual trigger business 
            if(triggerNew != null && triggerNew.size()>0 && triggerOld != null && triggerOld.size()>0)
            {
                CustomSettings.validateCountryName(triggerNew , triggerOldMap );
                // CustomMetadataUtil.validateFxdbFields(triggerNew , triggerOldMap );
                AccountUtil.checkIBStatus(triggerNew,triggerOldMap);
            }

            new AccountTriggerHandler(trigger.new, trigger.oldMap).onBeforeUpdate();
        }

		// SP-9723
		// if trigger is enabled, deletion
		// is only allowed to some users
		if (trigger.isDelete &&
				!AccountUtil.isDeletionAllowed())
			AccountUtil.preventDeletion(trigger.old);
    }
}