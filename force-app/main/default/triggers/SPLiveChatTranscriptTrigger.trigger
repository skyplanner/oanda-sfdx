/**
 * @File Name          : SPLiveChatTranscriptTrigger.trigger
 * @Description        : 
 * @Author             : Michel Carrillo (SkyPlanner)
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 11:46:22 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    03/26/2020   Michel Carrillo (SkyPlanner)     Initial Version
**/
trigger SPLiveChatTranscriptTrigger on LiveChatTranscript (
    before insert, 
    before update,
    after insert, 
    after update
) {
    try {
        ExceptionTestUtil.execute();
        if (!SPLiveChatTranscriptTriggerHandler.isEnabled()) {
            return;
        }
        //else..
        SPLiveChatTranscriptTriggerHandler handler;
        if (Trigger.isInsert) {
            handler = new SPLiveChatTranscriptTriggerHandler(
                Trigger.new,
                null
            );
            if (Trigger.isBefore) {
                handler.handleBeforeInsert();
                //...
            } else if (Trigger.isAfter) {
                handler.handleAfterInsert();
            }
            //...
        } else if (Trigger.isUpdate) {
            handler = new SPLiveChatTranscriptTriggerHandler(
                Trigger.new,
                Trigger.oldMap
            );
            if (Trigger.isBefore) {
                handler.handleBeforeUpdate();
                //...
            } else if (Trigger.isAfter) {
                handler.handleAfterUpdate();
            }
        }
        //...
    } catch (LogException lex) {
        throw lex;
        //...
    } catch (Exception ex) {
        throw LogException.newInstance(
            SPLiveChatTranscriptTriggerHandler.class.getName(),
            ex
        );
    }
    
}