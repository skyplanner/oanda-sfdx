/**
 * @description       : 
 * @author            : Dianelys Velazquez
 * @group             : 
 * @last modified on  : 08-12-2022
 * @last modified by  : Dianelys Velazquez
**/
trigger SegmentationTrigger on Segmentation__c (before insert, after insert,
    before update, after update, before delete, after delete
) {
    new SegmentationTriggerHandler(Trigger.new, Trigger.oldMap).run();
}