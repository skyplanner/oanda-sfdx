/**
*  Trigger for all before events
*
*  Gilbert Gao
*  Jan 25, 2017
*/
trigger Document_Before on Document__c (
    before insert, before update)
{    
    if(TriggersUtil.isTriggerEnabled(
        TriggersUtil.Triggers.TRIGGER_DOCUMENT_BEFORE))
    {
        if(Trigger.isInsert)
        {
            new DocumentTriggerHandler(
                Trigger.new).onBeforeInsert();
        } else if(Trigger.isUpdate)
        {
            new DocumentTriggerHandler(
                Trigger.new).onBeforeUpdate();
        }
    }  
}