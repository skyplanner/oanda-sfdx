/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
**/
trigger Job_Status on Job_Status__c(after update) {
	if (!JobStatusTriggerHandler.isEnabled()) {
		return;
	}
	//else..
	JobStatusTriggerHandler handler;
	if (Trigger.isUpdate) {
		//*************************** UPDATE ***************************
		handler = new JobStatusTriggerHandler(Trigger.new, Trigger.oldMap);
		if (Trigger.isAfter) {
			//******* AFTER UPDATE *******
			handler.handleAfterUpdate();
		}
	}
}