/* Name: Opportunity_After 
 * Description : Apex Trigger on Account for Before Insert and Before Update.
 * Author: Re-Authored by Sri 
 * 
 * Update : @Sri - Added Recursive Check  and code restructure 2020-07-29.
 */
trigger Opportunity_After on Opportunity (after insert, after update)  
{
    if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c)
    {
        // creating the Trigger Context fields after filtering the records in the recursive check
        CheckRecursive recursiveHandler = new CheckRecursive(Trigger.NewMap,Trigger.OldMap);
        List<Opportunity> triggerNew ;
        List<Opportunity> triggerOld ;
        Map<Id,Opportunity> triggerNewMap ;
        Map<Id,Opportunity> triggerOldMap ;
        If( recursiveHandler.triggerNew != null &&  recursiveHandler.triggerNew.size()>0)
        {
            triggerNew = recursiveHandler.triggerNew;
            triggerNewMap = new Map<Id, Opportunity>(triggerNew);
        }
        If( recursiveHandler.triggerOld != null &&  recursiveHandler.triggerOld.size()>0)
        {
            triggerOld = recursiveHandler.triggerOld;
            triggerOldMap = new Map<Id, Opportunity>(triggerOld);
        }
		        System.debug('----'+triggerNew);
        if(Trigger.isUpdate) 
        {
            //If there are any (new) records after filtering for recursiveness then continue with actual trigger business 
            if(triggerNew != null && triggerNew.size()>0 && triggerOld != null && triggerOld.size()>0)
            {
                System.debug('TRG Update Opportunity_After: OpportunityUtil.changeParentAccountOwner');
                OpportunityUtil.changeParentAccountOwner(triggerNew ,triggerOldMap );

                OpportunityUtil.checkMeaningfulInteractionsAfterOppClose(triggerNewMap ,triggerOldMap );
            }
        }
        //adding the processed IDs to the recursive check static variable so that the these records will be excluded next time
        if(TriggerNewMap != null && TriggerNewMap.size()>0)
        {
        //  checkRecursive.SetOfIDs.addAll(TriggerNewMap.keySet());
        }
    } 
}