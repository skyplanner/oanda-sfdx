trigger Event_Message_After on Event_Message__c (after insert, after update) {

    if(Trigger.isInsert || Trigger.isUpdate) {
        EventMessageUtil.deactivateExistingMessages(Trigger.new);
    }

}