trigger Document_After on Document__c
    (after insert, after update)
{
    if(TriggersUtil.isTriggerEnabled(
        TriggersUtil.Triggers.TRIGGER_DOCUMENT_AFTER))
    {
        if(Trigger.isInsert) {
            new DocumentTriggerHandler(
                Trigger.new).onAfterInsert();
        } else if(Trigger.isUpdate) {
            new DocumentTriggerHandler(
                Trigger.new,
                Trigger.oldMap).onAfterUpdate();
        }
    } 
        
}