/**
 * @auther Fernando @Skyplanner
 * @since 12/10/2020
 */
trigger Affiliate on Affiliate__c (before insert, after insert,
		before update, after update,
		before delete, after delete,
		after undelete) {
	AffiliateTriggerHandler handler;

	// Order: before
	if (trigger.isBefore) 
	{	
		if (trigger.isInsert) 
		{
			handler = new AffiliateTriggerHandler(trigger.new, trigger.old);
			handler.handleBeforeInsert();
		} 
		/* // Operation: Update
		if (trigger.isUpdate) {

		} */
		/* // Operation: Delete
		if (trigger.isDelete) {

		} */
		/* // Operation: Undelete
		if (trigger.isUndelete) {

		} */
	}
	// Order: after
	else if (trigger.isAfter) {
		// Operation: Insert
		if (trigger.isInsert) {
			handler = new AffiliateTriggerHandler(trigger.new, trigger.old);
			handler.handleAfterInsert();
		}
		// Operation: Update
		if (trigger.isUpdate) {
			handler = new AffiliateTriggerHandler(trigger.new, trigger.old);
			handler.handleAfterUpdate();
		}
		/* // Operation: Delete
		if (trigger.isDelete) {

		} */
		// Operation: Undelete
		/* if (trigger.isUndelete) {

		} */
	}
}