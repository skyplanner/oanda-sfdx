trigger OutgoingNotificationEventTrigger on Outgoing_Notification__e (after insert) {
    //the number of trigger records will be limited by the OutgoingNotificationEventTriggerConfig setting (batch size 50)
    new OutgoingNotificationEventTriggerHandler().run();
}