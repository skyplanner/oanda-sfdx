/* Name: Opportunity_Before 
 * Description : Apex Trigger on Account for Before Insert and Before Update.
 * Author: Re-Authored by Sri 
 * 
 * Update : @Sri - Added Recursive Check  and code restructure 2020-07-23.
 */
trigger Opportunity_Before on Opportunity (before insert, before update)
{
    if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c)
    {
        // creating the Trigger Context fields after filtering the records in the recursive check
        CheckRecursive recursiveHandler = new CheckRecursive(Trigger.NewMap,Trigger.OldMap);
        List<Opportunity> triggerNew ;
        List<Opportunity> triggerOld ;
        Map<Id,Opportunity> triggerNewMap ;
        Map<Id,Opportunity> triggerOldMap ;
        If( recursiveHandler.triggerNew != null &&  recursiveHandler.triggerNew.size()>0)
        {
            triggerNew = recursiveHandler.triggerNew;
            triggerNewMap = new Map<Id, Opportunity>(triggerNew);
        }
        If( recursiveHandler.triggerOld != null &&  recursiveHandler.triggerOld.size()>0)
        {
            triggerOld = recursiveHandler.triggerOld;
            triggerOldMap = new Map<Id, Opportunity>(triggerOld);
        }   
        
        if(Trigger.isInsert) 
        {
            for(Opportunity o : Trigger.New) 
            {
                    o.Name = o.Name.removeStart('null ');
                    System.debug(o.Account_Phone__c );
                    if(o.accountId != null && o.Account_Phone__c != null)
                    {
                        o.Account_Phone_Callable__c = o.Account_Phone__c;
                    }
            }
            System.debug('TRG Insert Opportunity_Before: OpportunityUtil.countNumberOfTasks');
            OpportunityUtil.countNumberOfTasks(Trigger.New,Trigger.OldMap, Trigger.isInsert);
            OpportunityUtil.calculateTradedAttribution(Trigger.New, Trigger.oldMap, Trigger.isInsert);
        }
    
        if(Trigger.isUpdate) 
        {
            //If there are any (new) records after filtering for recursiveness then continue with actual trigger business 
            if(triggerNew != null && triggerNew.size()>0 && triggerOld != null && triggerOld.size()>0)
            {
                System.debug('TRG Update Opportunity_Before: OpportunityUtil.countNumberOfTasks');
                // last parameter should be Trigger.IsInsert not isUpdate
                OpportunityUtil.countNumberOfTasks(triggerNew , triggerOldMap , Trigger.isInsert);
                OpportunityUtil.calculateTradedAttribution(triggerNew , triggerOldMap , Trigger.isInsert);
            }
        }
    }
}