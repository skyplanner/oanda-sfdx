/**
 * @File Name          : ServiceResourceSkill.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/25/2021, 9:47:16 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/25/2021, 9:46:44 AM   acantero     Initial Version
**/
trigger ServiceResourceSkill on ServiceResourceSkill (before insert, before update) {
    if (!ServiceResourceSkillTriggerUtility.isEnabled()) {
        return;
    }
    //else..
    ServiceResourceSkillTriggerUtility handler;
    if (Trigger.isInsert) {
        handler = new ServiceResourceSkillTriggerUtility(
            Trigger.new,
            null
        );
        if (Trigger.isBefore) {
            handler.handleBeforeInsert();
        }
        //...
    } else if (Trigger.isUpdate) {
        handler = new ServiceResourceSkillTriggerUtility(
            Trigger.new,
            Trigger.oldMap
        );
        if (Trigger.isBefore) {
            handler.handleBeforeUpdate();
        }
    }
}