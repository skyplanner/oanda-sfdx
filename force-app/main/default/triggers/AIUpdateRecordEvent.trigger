/**
 * @File Name          : AIUpdateRecordEvent.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 9/30/2021, 3:08:03 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    9/29/2021, 5:51:39 PM   acantero     Initial Version
**/
trigger AIUpdateRecordEvent on AIUpdateRecordEvent (after insert) {
    if (!AIUpdateRecordEventHandler.isEnabled()) {
        return;
    }
    //else...
    AIUpdateRecordEventHandler handler = 
        new AIUpdateRecordEventHandler(
            Trigger.new
        );
    handler.handleAfterInsert();
}