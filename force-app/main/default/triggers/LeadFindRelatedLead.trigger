/*
 * This trigger will search for related lead (Retail Practice 
 * or Retail) to match by Email and save it in Related_Lead__c field
 *
 * This is helpful to see if a lead created new fxTrade account
 * but has already been contacted via practice lead
 *
 */
trigger LeadFindRelatedLead on Lead (before insert,after insert, before update) {
    map<String,Lead> leadEmailToLeadMap = new map<String,Lead>();
    
    // get leads that are either new or had their email changed
    for (Lead lead : Trigger.new) {
        if (lead.Email != null && lead.Email != '' &&
                (Trigger.isInsert || lead.Email != Trigger.oldMap.get(lead.id).Email)) {
            leadEmailToLeadMap.put(lead.Email,lead);
        }
    }
    
    // update already existing leads with new Related_Lead__c value
    if (leadEmailToLeadMap.keySet().size() > 0) {
        list<Lead> leadsToUpdate = new list<Lead>();
        set<Id> currentLeadIds = new set<Id>();
        // if trigger is not before insert (don't have ids yet)
        if (Trigger.newMap != null)
            currentLeadIds = Trigger.newMap.keySet();
        for (Lead lead : [select Id,Email,Related_Lead__c from Lead where Email =: leadEmailToLeadMap.keySet()
                            and RecordTypeId in
                                (select Id from RecordType 
                                    where (Name='Retail Practice Lead' or Name='Retail Lead')
                                    and SObjectType='Lead')
                            and Id !=: currentLeadIds]) {
            // save to already existing lead
            if (Trigger.newMap != null) {
                lead.Related_Lead__c = leadEmailToLeadMap.get(lead.email).Id;
                leadsToUpdate.add(lead);
            }
            // save to current lead
            if (Trigger.isBefore) {
                leadEmailToLeadMap.get( lead.email ).Related_Lead__c = lead.id;
            }
        }
        
        if (leadsToUpdate.size() > 0)
            update leadsToUpdate;
    }
}