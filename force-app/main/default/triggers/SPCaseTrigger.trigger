/**
 * Case trigger by SkyPlanner
 * @author:	Michel Carrillo (SkyPlanner)
 * @date:	03/13/2020
 */
trigger SPCaseTrigger on Case (
    before insert, 
    after insert, 
    before update, 
    after update
) {
    try {
        ExceptionTestUtil.execute();
        if (!SPCaseTriggerHandler.isEnabled()) {
            return;
        }
        //else..
        SPCaseTriggerHandler handler;
        if (Trigger.isInsert) {
            handler = new SPCaseTriggerHandler(
                Trigger.new,
                null
            );
            if (Trigger.isBefore) {
                handler.handleBeforeInsert();
                //...
            } else if (Trigger.isAfter) {
                handler.handleAfterInsert();
            }
            //...
        } else if (Trigger.isUpdate) {
            handler = new SPCaseTriggerHandler(
                Trigger.new,
                Trigger.oldMap
            );
            if (Trigger.isBefore) {
                handler.handleBeforeUpdate();
                //...
            } else if (Trigger.isAfter) {
                handler.handleAfterUpdate();
            }
        }
        //...
    } catch (LogException lex) {
        throw lex;
        //...
    } catch (Exception ex) {
        throw LogException.newInstance(
            SPCaseTriggerHandler.class.getName(),
            ex
        );
    }
}