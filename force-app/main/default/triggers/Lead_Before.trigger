/* Name: Lead_Before
 * Description : Apex Trigger on Lead for Before Insert and Before Update.
 * Author: OANDA
 * 
 * Update : @Sahil Handa - Add Recursive Check 2020-05-28.
 * Update : @Sahil Handa - Reverting Recursive Check because of issues in production running batch classes 2020-07-18.
 */
trigger Lead_Before on Lead (before insert, before update) {

	//SP-9086 Pending
	// if(!LeadTriggerHandler.isFirstTime())
	// 	return;

	if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c)
    {
		System.debug('TRG Insert/Update Lead_Before: LeadUtil.preventNullEmail');
		//LeadUtil.preventNullEmail(Trigger.new);

		if (Trigger.isInsert) {
			// Prevents insert of standard lead where an already existing practice/live lead exists
			System.debug('TRG Insert Lead_Before: LeadUtil.preventDuplicateInsert');
			LeadUtil.preventDuplicateInsert(Trigger.new);
			
			//If Last Name is null, replace it with the business name
			LeadUtil.validateReplaceLastName(Trigger.new);
            
			//To check if Division Name is filled for OTMS leads
			LeadUtil.handleOTMSLeads(Trigger.new);

			LeadTriggerHandler handler = new LeadTriggerHandler();
			handler.beforeInsert(Trigger.new);
		}
		
		System.debug('TRG Insert/Update Lead_Before: LeadUtil.markDuplicateLeads');
		// Mark any of the leads in this batch if any other lead in the system (both in the batch and already
		// saved) has the same email address.  This will allows to efficiently merge these leads later
		LeadUtil.markDuplicateLeads(Trigger.new);
		
		System.debug('TRG Insert/Update Lead_Before: LeadUtil.updateCreateTaskOnPracticeTraded');
		LeadUtil.updateCreateTaskOnPracticeTraded(Trigger.new, Trigger.oldMap, Trigger.isInsert);
		
		System.debug('TRG Insert/Update Lead_Before: LeadUtil.automaticLeadAssignment');
		// Automatically assigns practice and live leads out
		
		// <--- Lead assiggnment  --->
		if (Trigger.isInsert &&
			Settings__c.getValues('Default') != null && 
			Settings__c.getValues('Default').Enable_Control_Group__c)
		{
			ControlGroupHelper.assignToControlGroup(Trigger.new, Trigger.oldMap);
		}
		else
		{
			LeadUtil.automaticLeadAssignment(Trigger.new, Trigger.oldMap, Trigger.isInsert);
		}
		
		if (Trigger.isUpdate) {
			System.debug('TRG Update Lead_Before: LeadUtil.preventReassignment');
			LeadUtil.preventReassignment(Trigger.new, Trigger.oldMap);
			CustomSettings.validateCountryName(Trigger.new, Trigger.oldMap);

			LeadTriggerHandler handler = new LeadTriggerHandler();
			handler.beforeUpdate(Trigger.new, Trigger.oldMap);
		}
		
		//set territory for standard lead
		LeadUtil.adjustFieldValue(Trigger.new, Trigger.oldMap);
		
	}
}