/**
 * @File Name          : NonHVCCase.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/22/2021, 11:46:06 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/25/2021, 2:26:44 PM   acantero     Initial Version
**/
trigger NonHVCCase on Non_HVC_Case__c (
    after update, 
    after delete
) {
    try {
        ExceptionTestUtil.execute();
        if (!NonHVCCaseTriggerHandler.isEnabled()) {
            return;
        }
        //else..
        NonHVCCaseTriggerHandler handler;
        if (Trigger.isUpdate) {
            //*************************** UPDATE ***************************
            handler = new NonHVCCaseTriggerHandler(
                Trigger.new,
                Trigger.oldMap
            );
            if (Trigger.isAfter) {
                //******* AFTER UPDATE *******
                handler.handleAfterUpdate();
            }
            //...
        } else if (Trigger.isDelete) {
            //*************************** DELETE ***************************
            handler = new NonHVCCaseTriggerHandler(
                null,
                Trigger.oldMap
            );
            if (Trigger.isAfter) {
                //******* AFTER DELETE *******
                handler.handleAfterDelete();
            }
        }
        //...
    } catch (LogException lex) {
        throw lex;
        //...
    } catch (Exception ex) {
        throw LogException.newInstance(
            NonHVCCaseTriggerHandler.class.getName(),
            ex
        );
    }
    

}