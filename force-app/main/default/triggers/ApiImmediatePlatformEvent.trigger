trigger ApiImmediatePlatformEvent on Api_Event_Immediate__e (after insert) {
    new EventTriggerHandler().run();
}