/**
 * @File Name          : CaseReroutedTrigger.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/16/2022, 4:30:04 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/10/2022, 12:44:55 PM   acantero     Initial Version
**/
trigger CaseReroutedTrigger on Case_Rerouted__e (after insert) {
    if (!CaseReroutedTriggerHandler.isEnabled()) {
        return;
    }
    //else..
    CaseReroutedTriggerHandler handler = 
        new CaseReroutedTriggerHandler(
            Trigger.new
        );
    handler.handleAfterInsert();
}