trigger PersonalInformationUpdateEvent on Piu_Cka_Update__e (after insert) {
    Integer recordsCounterLimit = 200;
    Integer replayCounterLimit = 10;
    Integer recordsCounter = 0;

    List<Piu_Cka_Update__e> piusToProcess = new List<Piu_Cka_Update__e>();
    Set<Piu_Cka_Update__e> eventsToRepublish = new Set<Piu_Cka_Update__e>();

    for (Piu_Cka_Update__e piuEvent : Trigger.new) {
        recordsCounter ++;
        if(recordsCounter <  recordsCounterLimit ) {
            if (piuEvent.Replay_Counter__c == null || piuEvent.Replay_Counter__c  <= replayCounterLimit) {
                piusToProcess.add(piuEvent);
            } else {
                Logger.error(
                    'CampaignLeadEvent trigger',
                    'infitie loop on CampaignLeadEvent',
                    'replay event replay count:  ' + piuEvent.Replay_Counter__c + ' replay event: ' + piuEvent);
            } 
        } else  { 
            Piu_Cka_Update__e piu_replay = new Piu_Cka_Update__e();
            piu_replay.User_Payload__c = piuEvent.User_Payload__c;
            piu_replay.Replay_Counter__c = piuEvent.Replay_Counter__c != null &&  piuEvent.Replay_Counter__c > 0
                                            ? piuEvent.Replay_Counter__c + 1 
                                            : 1;
                                            
            eventsToRepublish.add(piu_replay);
        } 
    }

    PersonalInfoUpdateEventTriggerHandler piuHandler  = new PersonalInfoUpdateEventTriggerHandler();
    piuHandler.handlePiuInsert(piusToProcess);
    if(eventsToRepublish.size() > 0) {
        List<Piu_Cka_Update__e> events = new List<Piu_Cka_Update__e>(eventsToRepublish);
        List<Database.SaveResult> sr = EventBus.publish(events);
        recordsCounter = 0;
    }
}