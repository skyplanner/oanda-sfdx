/**
 * @File Name          : PendingServiceRouting_Trigger.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/20/2021, 11:35:30 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/18/2020   acantero     Initial Version
**/
trigger PendingServiceRouting_Trigger on PendingServiceRouting (before insert, before update, after insert) {
    if (!PendingServiceRouting_Trigger_Utility.isEnabled()) {
        return;
    }
    //else..
    PendingServiceRouting_Trigger_Utility handler;
    
    if (Trigger.isInsert) {
        //*************************** INSERT ***************************
        handler = new PendingServiceRouting_Trigger_Utility(
            Trigger.new,
            null
        );
        if (Trigger.isBefore) {
            //******* BEFORE INSERT *******
            handler.assignRoutingPriorityBeforeInsert();
            //...
        } else if (Trigger.isAfter) {
            //******* AFTER INSERT *******
            handler.assignSkillsAfterInsert();
        }
        //...
    } else if (Trigger.isUpdate) {
        //*************************** UPDATE ***************************
        handler = new PendingServiceRouting_Trigger_Utility(
            Trigger.new,
            Trigger.oldMap
        );
        if (Trigger.isBefore) {
            //******* BEFORE UPDATE *******
            handler.checkTransfersSkills();
            //...
        } 
    }
    
}