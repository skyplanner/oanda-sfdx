/*
 *	Modifier : Vanglirajan Kalimuthu.
 *	Modification Date : July 17 2021
 * 	Purpose : Refactored the code and added the Task Util updateLastSalesActivity method.
 * 
*/
trigger Task_After on Task (after insert, after update) {
	if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c) {
		if (trigger.isInsert) {
			TaskUtil.updateLastSalesActivity(Trigger.new);
			TaskUtil.updateLeadStatus(Trigger.newMap, Trigger.oldMap);
			TaskUtil.insertPersonaTag(Trigger.new, Trigger.oldMap, Trigger.isInsert);
			TaskUtil.updateFirstEmailResponse(Trigger.new);
			TaskUtil.updateRelatedOpportunity();
		}
		if (trigger.isUpdate && GlobalStaticVar.canIRun()) {
			TaskUtil.updateLastSalesActivity(Trigger.new);
			TaskUtil.updateLeadStatus(Trigger.newMap, Trigger.oldMap);
			TaskUtil.updateRelatedOpportunity();
			GlobalStaticVar.stopTrigger();
		}

		Tasks.commitAfterTriggerUpdates();
	}
}