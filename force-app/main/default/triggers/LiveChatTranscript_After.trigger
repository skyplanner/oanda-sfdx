trigger LiveChatTranscript_After on LiveChatTranscript
	(after insert, after update)
{	
	if(TriggersUtil.isTriggerEnabled(
		 TriggersUtil.Triggers.TRIGGER_LIVECHATTRANSCRIPT_AFTER))
 	{
		LiveChatTranscriptUtil.linkPostChatSurvey(
			Trigger.new,
			Trigger.oldMap,
			Trigger.isInsert);	
	}

	if(trigger.isUpdate) 
	{
		System.debug('livechattranscript after trigger :::');
		if(CustomSettings.isCustomNotificationForSalesEnabled()) 
        {
            CustomNotificationManager.sendNotificationsForOGMCases(Trigger.new, Trigger.oldMap);
        }
	}

}