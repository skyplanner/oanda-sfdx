trigger ComplyAdvantageSearch on Comply_Advantage_Search__c  (before insert, after insert,
                                                              before update, after update,
                                                              before delete, after delete,
                                                              after undelete) 
{
    try 
    {
        if(Trigger.isBefore)
        {
            ComplyAdvantageSearchBeforeTrgHandler beforeTriggerHander  = new ComplyAdvantageSearchBeforeTrgHandler(trigger.isInsert, trigger.isUpdate, trigger.new, trigger.oldMap); 
            if(Trigger.isInsert)
            {
                beforeTriggerHander.onBeforeInsert();
            } 
            if(Trigger.isUpdate) 
            {
                beforeTriggerHander.onBeforeAfterUpdate();
            } 
        }  
        if(Trigger.isAfter)
        { 
            ComplyAdvantageSearchAfterTrgHandler afterTriggerHandler  = new ComplyAdvantageSearchAfterTrgHandler(trigger.isInsert, trigger.isUpdate, trigger.new, trigger.oldMap);       
            if(Trigger.isInsert)
            {
                afterTriggerHandler.onAfterInsert();
            } 
            if(Trigger.isUpdate)
            {
                afterTriggerHandler.onAfterUpdate();
            } 
            afterTriggerHandler.commitTriggerChanges();
        }
    } 
    catch (Exception ex) 
    {
        Logger.error('ComplyAdvantage-Trigger', Constants.LOGGER_COMPLY_ADVANTAGE_CATEGORY, ex.getMessage() + ex.getStackTraceString());   
    }
}