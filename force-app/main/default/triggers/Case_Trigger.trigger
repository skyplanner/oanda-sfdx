/**
 * @File Name          : Case_Trigger.trigger
 * @Description        : This trigger is created for manage SKYPLANNER implementations
 * @Author             : dmorales
 * @Last Modified By   : Jakub Fik
 * @Last Modified On   : 2024-05-06
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    12/4/2019         dmorales                 Initial Version
 * 2.0    12/4/2019         Jakub Fik                TriggerHandler framework implementation.
 **/
trigger Case_Trigger on Case(
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete
) {
    new CaseTriggerHandler().run();
}