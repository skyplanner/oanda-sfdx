/**
 * @File Name          : AgentWork_Trigger.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/12/2022, 12:44:56 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/31/2020   acantero     Initial Version
**/
trigger AgentWork_Trigger on AgentWork (before insert, after insert, after update) {
    //the "before insert" is set only because the imposibility of create "right" test classes for AgentWork object
    Boolean isAfterInsert = (trigger.isAfter && trigger.isInsert);
    Boolean isAfterUpdate = (trigger.isAfter && trigger.isUpdate);
    Map<Id,AgentWork> oldMap = (Trigger.isUpdate)
        ? Trigger.oldMap
        : null;
    AgentWork_Trigger_Utility.process(
        Trigger.new,
        oldMap,
        isAfterInsert,
        isAfterUpdate
    );
}