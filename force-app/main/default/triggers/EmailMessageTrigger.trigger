/**
 * @Description  : Trigger for EmailMessage sObject.
 * @Author       : Jakub Fik
 * @Date         : 2024-05-21
**/
trigger EmailMessageTrigger on EmailMessage(
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete
) {
    new NewEmailMessageTriggerHandler().run();
}