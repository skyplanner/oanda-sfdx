/**
 * Name: CQG_Account
 * Description : CQG_Account__c Trigger
 * Author: ejung
 * Date : 2024-03-06
 */
trigger CQG_Account on CQG_Account__c (before insert) {

    new CQGAccountTriggerHandler().run();
}