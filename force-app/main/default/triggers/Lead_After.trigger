/* Name: Lead_After
 * Description : Apex Trigger on Lead for After Insert and After Update.
 * Author: OANDA
 * 
 * Update : @Sahil Handa - Add Recursive Check 2020-05-28.
 * Update : @Sahil Handa - Reverting Recursive Check because of issues in production running batch classes 2020-07-18.
 */

trigger Lead_After on Lead (after insert, after update) {

    //SP-9086 Pending
	// if(!LeadTriggerHandler.isFirstTime())
	// 	return;

    if(UserUtil.currentUser != null && !UserUtil.currentUser.Triggers_Disabled__c)
    {
        Map<ID, Schema.RecordTypeInfo> leadRecordTypeMap = Schema.SObjectType.Lead.getRecordTypeInfosById(); 
        if(Trigger.isUpdate &&
        Trigger.new.size() == 1  &&  
        Settings__c.getValues('Default') != null &&
        Settings__c.getValues('Default').Comply_Advantage_Enabled__c &&
        leadRecordTypeMap.get(trigger.new[0].recordTypeId).getName() == RecordTypeUtil.NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_LIVE)
        {		 
            ComplyAdvantageHelper.validate(Trigger.old[0], trigger.new[0]); 
        }

        if(Trigger.new.size() == 1  &&  
            Settings__c.getValues('Default') != null &&
            Settings__c.getValues('Default').Enable_Current_Desk_Integration__c &&
            leadRecordTypeMap.get(trigger.new[0].recordTypeId).getName() == RecordTypeUtil.NAME_LEAD_RECORDTYPE_FX_ACCOUNT_TYPE_PRACTICE)
        {		 
            CurrentDeskSync.registerDemoUser(trigger.new[0]); 
        }
        
        if(Trigger.isUpdate) 
        {
            System.debug('TRG Update Lead_After: LeadUtil.updateFxAccountOwner');
            fxAccountOwnerUtil.updateFxAccountOwner(Trigger.new, Trigger.oldMap);

            // Update hashed fields
            fxAccountHashUtil.setHashedFields(Trigger.new, Trigger.old);

            new LeadTriggerHandler(Trigger.new, Trigger.oldMap).onAfterUpdate();
        }
    
        System.debug('LeadUtil.isReadyForConversion(Trigger.new.get(0)): ' + LeadUtil.isReadyForConversion(Trigger.new.get(0)));
        
        if (    CustomSettings.isEnableLeadConversion() && 
                CustomSettings.isEnableLeadMerging() &&
                Trigger.size == 1 && (UserUtil.getIntegrationProfileIDs().contains(UserInfo.getProfileId()) == false) &&
                !LeadBatchUtil.MERGING &&
                (
                    !LeadUtil.isStandardLead(Trigger.new.get(0)) ||
                    Trigger.new.get(0).LeadSource == 'Chat'
                )
            ) {
                    
            LeadBatchUtil.MERGING = true;
            LeadBatchUtil.mergeLeads(Trigger.new);
            if (LeadUtil.isReadyForConversion(Trigger.new.get(0)) && !UserUtil.isIntegrationUserId(Trigger.new.get(0).OwnerId)) {
                LeadUtil.convertReadyLeads(Trigger.new);
                LeadUtil.postprocessLeadConversion(Trigger.new);
            }         
        }
    } 
}