/* Name: CaseCommentTrigger
 * Description : CaseComment Trigger
 * Author: ejung
 * Date : 2023-03-31
 */
trigger CaseCommentTrigger on CaseComment (after insert) 
{   
    System.debug('Trigger CaseCommentTrigger');
    new CaseCommentTriggerHandler().run();
}