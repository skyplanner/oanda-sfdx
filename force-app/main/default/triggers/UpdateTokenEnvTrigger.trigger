trigger UpdateTokenEnvTrigger on Update_Token__e (after insert) {

    new UpdateTokenEnvTriggerHelper().run();

}