trigger LogEvtTrigger on LogEvt__e (after insert) {

    new LogEvtTriggerHandler(
        trigger.new).onAfterInsert();
        
}