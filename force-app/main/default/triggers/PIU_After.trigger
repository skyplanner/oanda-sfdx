/* Name: PIU_After 
 * Description : Apex Trigger on Personal_Information_Update__c for After Insert and After Update.
 * Author: ejung
 */
trigger PIU_After on Personal_Information_Update__c (after insert, after update) {

    if(trigger.isInsert || trigger.isUpdate)
    {
        //SP-12351 Update PIU knowledge result on fxAccount
        PIUCKAHelperClass.updateResultsOnfxAccount(Trigger.new);
	} 

}