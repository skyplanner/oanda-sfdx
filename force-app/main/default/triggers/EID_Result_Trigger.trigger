/**
 * Created by Agnieszka Kajda on 30/11/2023.
 */

trigger EID_Result_Trigger on EID_Result__c (before insert, before update, after insert, after update) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
            EIDUtil.processEIDForCase(Trigger.new);
            EIDUtil.updateEidData(Trigger.new);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert)
        {
            ComplyAdvantageHelper.initialComplyAdvantageSearch(Trigger.new);
            EIDUtil.reopenCase(Trigger.new);
        }
        // Update the number of EID passes for the associated fxAccount
        if(Trigger.isInsert || Trigger.isUpdate)
        {
            EIDUtil.setEIDPassCount(Trigger.new);
        }
    }

}