trigger fxAccount on fxAccount__c (before insert, after insert,
                                   before update, after update,
                                   before delete, after delete,
                                   after undelete) 
{
    if(Trigger.isBefore) {
        if (UserUtil.currentUser != null 
            && !UserUtil.currentUser.Triggers_Disabled__c 
            && TriggersUtil.isTriggerEnabled(TriggersUtil.Triggers.TRIGGER_FXACCOUNT_BEFORE)) {
            fxAccountBeforeTriggerHandler beforeTriggerHander  = new fxAccountBeforeTriggerHandler(trigger.isInsert, trigger.isUpdate, trigger.new, trigger.oldMap); 
            if(Trigger.isInsert) {
                beforeTriggerHander.onBeforeInsert();
            } 
            if(Trigger.isUpdate) {
                beforeTriggerHander.onBeforeUpdate();
            }
            if(Trigger.isDelete) {
                beforeTriggerHander.onBeforeDelete();
            } 
        }
    }  
    if(Trigger.isAfter) { 
        if (UserUtil.currentUser != null 
            && !UserUtil.currentUser.Triggers_Disabled__c 
            && TriggersUtil.isTriggerEnabled(TriggersUtil.Triggers.TRIGGER_FXACCOUNT_AFTER)) {
            fxAccountAfterTriggerHandler afterTriggerHandler  = new fxAccountAfterTriggerHandler(trigger.isInsert, trigger.isUpdate, trigger.new, trigger.oldMap);       
            if(Trigger.isInsert) {
                afterTriggerHandler.onAfterInsert();
            } 
            if(Trigger.isUpdate) {
                afterTriggerHandler.onAfterUpdate();
            } 
            afterTriggerHandler.commitTriggerChanges();
        }
    }
}