/* Name: Paxos
 * Description : Apex Trigger on Paxos Identity
 * Author: Eugene
 * Date: 2022-01-25
 */

trigger Paxos on Paxos__c (after insert, after update) 
{
	// Order: after
	if (trigger.isAfter) {

		// Operation: Insert
		if (trigger.isInsert) {
            new PaxosTriggerHandler(
                Trigger.new).onAfterInsert();
		}

		// Operation: Update
		if (trigger.isUpdate) {
			new PaxosTriggerHandler(
                Trigger.new,
                Trigger.oldMap).onAfterUpdate();
		}
	}
}