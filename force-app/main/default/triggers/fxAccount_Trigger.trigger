/**
 * @File Name          : fxAccount_Trigger.trigger
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/21/2020   acantero     Initial Version
**/
trigger fxAccount_Trigger on fxAccount__c (before delete) {
    /*****This logic was copy to fxAccount_Before trigger for Oanda requirement.****/
   /* if (Trigger.isBefore) {
        if (Trigger.isDelete) {
            fxAccount_Trigger_Utility.beforeDelete(
                Trigger.oldMap);
        }
    }*/
}