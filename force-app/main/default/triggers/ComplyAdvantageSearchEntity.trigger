trigger ComplyAdvantageSearchEntity on Comply_Advantage_Search_Entity__c (before insert, before update, before delete,
                                                                            after insert, after update, after delete, after undelete) {
    ComplyAdvantageSearchEntityTHandler handler = new ComplyAdvantageSearchEntityTHandler();
    handler.handleTrigger(Trigger.new, Trigger.oldMap, Trigger.operationType);
}