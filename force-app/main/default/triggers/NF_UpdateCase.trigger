/**
* Created by Neuraflash LLC on 28/06/18.
* Implementation : Chatbot
* Summary : Runs on After Insert when Live Chat Transcript is Saved
*
*/
trigger NF_UpdateCase on LiveChatTranscript (After Insert) {
    User runningUser = [SELECT Triggers_Disabled__c FROM User WHERE Id = :UserInfo.getUserId()];
	if (!runningUser.Triggers_Disabled__c) {
        if(trigger.isAfter){
            if(trigger.isInsert){
                NF_TranscriptUpdateClass.getCaseUpdateonTranscript(Trigger.new); //updating Case onto Live Chat Transcipt Record
            }
        }
    }
}