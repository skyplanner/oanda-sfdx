/**
 * @description       : 
 * @author            : Ariel Niubo
 * @group             : 
 * @last modified on  : 08-01-2022
 * @last modified by  : Ariel Niubo
**/
trigger SpEmailMessage on EmailMessage(
	before insert,
	before delete,
	after delete,
	after insert,
	after update
) {
	new EmailMessageTriggerHandler().run();
}