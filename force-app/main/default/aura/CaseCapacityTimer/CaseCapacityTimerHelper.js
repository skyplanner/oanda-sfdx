/**
 * @File Name          : CaseCapacityTimerHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/9/2021, 1:03:49 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/16/2021, 1:31:29 AM   acantero     Initial Version
**/
({

    CAPACITY_STATUS_IN_USE : 'In use',
    
    getCaseWorkClosingInfo : function(cmp, helper) {
        const caseId = cmp.get("v.recordId");
        const params = {
            caseId
        };
        return helper.runServerAction(cmp, helper, 'getCaseWorkClosingInfo', params)
            .then(function(result) {
                if (result) {
                    console.log('getCaseWorkClosingInfo -> result: ' + JSON.stringify(result));
                    let timeFrameInSeconds = result.wrapUpTime;
                    if (result.closingId) {
                        cmp.set('v.closingObjId', result.closingId);
                        timeFrameInSeconds = timeFrameInSeconds - result.closingTime;
                        if (timeFrameInSeconds < 3) {
                            return helper.freeCapacity(cmp, helper);
                        } 
                        //else...
                        cmp.set('v.timeFrameInSeconds', timeFrameInSeconds);
                        cmp.set('v.timerVisible', true);
                        return 'case timer activated';
                    }
                    //else...
                    cmp.set('v.timeFrameInSeconds', timeFrameInSeconds);
                    return 'timeFrameInSeconds updated';
                } 
                //else...
                console.error('getCaseWorkClosingInfo -> result is null');
                return 'result is null';
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('getCaseWorkClosingInfo -> error: ' + JSON.stringify(error));
                }
                return 'getCaseWorkClosingInfo -> failed';
            });
    },

    processClosedCase : function(cmp, event, helper) {
        const releaseCapacity = event.getParam('releaseCapacity');
        console.log(
            'CaseCapacityTimer -> processClosedCase, releaseCapacity: ' + 
            releaseCapacity
        );
        if (releaseCapacity == true) {
            return Promise.resolve(
                'ReleaseCapacity is true -> then do nothing'
            );
        }
        //else...
        const timeFrameInSeconds = cmp.get('v.timeFrameInSeconds');
        if (timeFrameInSeconds == 0) {
            console.log(
                'timeFrameInSeconds is 0 ' + 
                '-> then freeCapacity right now...'
            );
            return helper.freeCapacity(cmp, helper);
        }
        //else...
        helper.fireTimerStartedEvent(cmp, helper);
        return helper.beginCaseClosing(cmp, helper)
            .then($A.getCallback(function(beginCaseClosingResult) {
                console.log('beginCaseClosing -> ' + beginCaseClosingResult);
                return helper.getCurrentWorkId(cmp, helper);
                //...
            })).then($A.getCallback(function(workId) {
                if (workId) {
                    cmp.set('v.timerVisible', true);
                    return 'case timer activated, workId: ' + workId;
                } 
                //else...
                return 'workId not found';
            }));
    },

    beginCaseClosing : function(cmp, helper) {
        var caseId = cmp.get("v.recordId");
        var params = {
            caseId
        };
        return helper.runServerAction(cmp, helper, 'beginCaseClosing', params)
            .then(function(result) {
                if (result) {
                    cmp.set('v.closingObjId', result);
                    return 'beginCaseClosing -> result: ' + result;
                } 
                //else...
                var errorMsg = 'beginCaseClosing -> result is null';
                console.error(errorMsg);
                return errorMsg;
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('beginCaseClosing -> error(stringify): ' + JSON.stringify(error));
                }
                return 'beginCaseClosing -> failed';
            });
    },

    freeCapacity : function(cmp, helper) {
        console.log('CaseCapacityTimer -> freeCapacity');
        cmp.set('v.timerVisible', false);
        //...
        var caseId = cmp.get("v.recordId");
        var closingObjId = cmp.get("v.closingObjId");
        var params = {
            caseId,
            closingObjId
        };
        return helper.runServerAction(cmp, helper, 'freeCaseCapacity', params)
            .then(function(result) {
                return 'freeCapacity -> ok';
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('freeCapacity -> ' + JSON.stringify(error));
                }
                return 'freeCapacity -> failed';
            });
    },

    getCurrentWorkId : function(cmp, helper) {
        console.log('CaseCapacityTimer -> getCurrentWorkId');
        var recordId = cmp.get("v.recordId");
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI.getAgentWorks()
            .then(function(getAgentWorksResult) {
                var msg;
                var works = JSON.parse(getAgentWorksResult.works);
                for(var work of works) {
                    //console.log('workId: ' + work.workId);
                    if (helper.compareIds(recordId, work.workItemId)) {
                        return work.workId;
                    }
                }
                //else...
                return null;
            }).catch(function(error) {
                console.error('getCurrentWorkId -> failed');
                if (error) {
                    console.error('getCurrentWorkId -> ' + error);
                }
                return null;
            });
    },

    fireTimerStartedEvent : function(cmp, helper) {
        console.log('fireTimerStartedEvent -> begin');
        try {
            const timerStartedEvent = $A.get('e.c:CapacityTimerStartedEvent');
            const caseId = cmp.get("v.recordId");
            const timeFrameInSeconds = cmp.get('v.timeFrameInSeconds') + 2;//safe delay
            timerStartedEvent.setParams({
                caseId : caseId,
                timeFrameInSeconds : timeFrameInSeconds
            });
            timerStartedEvent.fire();
            console.log('fireTimerStartedEvent -> ok');
            //...
        } catch(error) {
            console.error('fireTimerStartedEvent -> fail');
            if (error) {
                console.error('fireTimerStartedEvent -> error: ' + error);
            }
        }
    },

    compareIds : function(id1, id2) {
		if ((!id1) || (!id2)) {
			return false;
		}
		//else...
		if (id1.length == id2.length) {
			return (id1 == id2);
		}
		//else...
		if (id1.length < id2.length) {
			return (id2.indexOf(id1) == 0);
		}
		//else...
		return (id1.indexOf(id2) == 0);
	},

    runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }

})