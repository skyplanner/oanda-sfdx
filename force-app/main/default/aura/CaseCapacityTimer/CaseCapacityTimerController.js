/**
 * @File Name          : CaseCapacityTimerController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/9/2021, 1:04:55 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/16/2021, 1:03:36 AM   acantero     Initial Version
**/
({
    
    doInit : function(cmp, event, helper) {
        console.log('CaseCapacityTimer 1.36');
    },

    onRecordUpdated : function(cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            console.error('CaseCapacityTimer -> onRecordUpdated -> ERROR');
            return;
        } else if (changeType === "REMOVED"){
            console.log('CaseCapacityTimer -> onRecordUpdated -> REMOVED');
            return;
        } else if (changeType === "LOADED"){
            console.log('CaseCapacityTimer -> onRecordUpdated -> LOADED');
            const loaded = cmp.get('v.loaded');
            const caseRecord = cmp.get('v.caseRecord');
            let recordLoadedOk = false;
            let caseRouted = false;
            let capacityInUse = false;
            let isHvc = false;
            if (caseRecord) {
                recordLoadedOk = true;
                caseRouted = caseRecord.Routed_And_Assigned__c;
                isHvc = caseRecord.Is_HVC__c;
                capacityInUse = 
                    (caseRecord.Agent_Capacity_Status__c == helper.CAPACITY_STATUS_IN_USE); 
            }
            if (
                (loaded == true) || 
                (caseRouted == false) ||
                (capacityInUse == false) ||
                (isHvc == false)
            ) {
                console.log(
                    'CaseCapacityTimer -> onRecordUpdated -> not valid case, then exit (' +
                    'loaded: ' + loaded + 
                    ', recordLoadedOk: ' + recordLoadedOk +
                    ', caseRouted: ' + caseRouted + 
                    ', isHvc: ' + isHvc + 
                    ', capacityInUse: ' + capacityInUse + ')'
                );
                cmp.set('v.active', false);
                return;
            }
            //else...
            cmp.set('v.active', true);
            cmp.set('v.loaded', true);
            helper.getCurrentWorkId(cmp, helper)
                .then(function(workId) {
                    if (workId) {
                        return helper.getCaseWorkClosingInfo(cmp, helper);
                    } 
                    //else...
                    return 'workId not found';
                    //...
                }).then(function(result) {
                    console.log('getCaseWorkClosingInfo -> result: ' + result);
                }).catch(function(error) {
                    console.error(error);
                });
        }
    },

    onLogout : function(cmp, event, helper) {
        const timerVisible = cmp.get('v.timerVisible');
        console.log('CaseCapacityTimer -> onLogout, timerVisible: ' + timerVisible);
        if (timerVisible == true) {
            console.log('then stop timer...');
            const workCapacityTimer = cmp.find("workCapacityTimer");
            workCapacityTimer.stop();
            cmp.set('v.timerVisible', false);
        }
    },

    onFreeCapacity : function(cmp, event, helper) {
        console.log('CaseCapacityTimer -> onFreeCapacity');
        helper
            .freeCapacity(cmp, helper)
            .then(function(result) {
                console.log('onFreeCapacity -> result: ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },

    onCaseClosed: function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        console.log('CaseCapacityTimer -> onCaseClosed, caseId: ' + recordId);
        var active = cmp.get("v.active");
        if (!active) {
            console.log('not a valid case, then exit');
            return;
        }
        //else...
        var caseId = event.getParam('caseId');
        var releaseCapacity = event.getParam('releaseCapacity');
        console.log('onCaseClosed -> caseId: ' + caseId + ', releaseCapacityParam: ' + releaseCapacity);
        if (!helper.compareIds(caseId, recordId)) {
			console.log('is not my problem, so... exit');
			return;
		}
        //else...
        console.log('then process case closed...');
        helper
            .processClosedCase(cmp, event, helper)
            .then(function(result) {
                console.log('CaseCapacityTimer -> onCaseClosed result: ' + result);
            }).catch(function(error) {
                console.error(error);
            });
	}

})