/**
 * @File Name          : HelpPortalRichTextEditorController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/3/2021, 5:23:06 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         4/30/2019                Fernando Gomez            Initial Version
**/
({
	getHtml: function(component, languageCode, documentPrefix, divisionCode,
			onSuccess, onError) {
		this._invoke(component, "c.getHtml", {
			"languageCode": languageCode,
			"documentPrefix": documentPrefix,
           "divisionCode":divisionCode
		},  onSuccess, onError);
	},
	saveHtml: function(component, languageCode, documentPrefix, html,divisionCode,
			onSuccess, onError) {
		this._invoke(component, "c.saveHtml", {
			"languageCode": languageCode,
			"documentPrefix": documentPrefix,
            "divisionCode" : divisionCode,
			"html": html
		},  onSuccess, onError);
	},
	parseFiles: function(component, html, onMatch, onComplete) {
		var rg, match, result = html + "",
			matches = [], helper = this;
		// we need to extract all file so we can exchange them
		// with document that will be visible
		rg = new RegExp([
			'<img src="https:\\/\\/[a-zA-Z0-9\\-\\.]*?\\.content\\.force\\.com\\/',
			'sfc\\/servlet\\.shepherd\\/version\\/download\\/',
			'([a-zA-Z0-9]{15})\\?asPdf=false&amp;operationContext=CHATTER" *?\\/?>'
		].join(""), "g");

		// we gather all matchs
		while (match = rg.exec(html || ""))
			matches.push({
				tag: match[0],
				id: match[1]
			});

		// if we found a match
		if (matches.length) {
			onMatch();
			matches.forEach(function(m, i) {
				console.log(m.tag);
				helper._invoke(
					component,
					"c.convertFileToDocument", 
					{ "contentVersionId": m.id },  
					function(newUrl) {
						// we replace the file
						result = result.replace(
							m.tag, '<img src="' + newUrl + '" />');
						// if we reached the end, we halt
						if (i + 1 == matches.length)
							onComplete(result);
					},
					function(e) {
						console.error(e);
						// we remove the bad image
						result = result.replace(m.tag, '');
						// if we reached the end, we halt
						if (i + 1 == matches.length)
							onComplete(result);
					});
			});
		}
	},
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					onError((errors && errors[0] && errors[0].message)
						|| "Unknow Error");
					break;
			}
		});
		$A.enqueueAction(action);
	}
})