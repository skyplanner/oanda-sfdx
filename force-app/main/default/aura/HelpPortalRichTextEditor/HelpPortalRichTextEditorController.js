/**
 * @File Name          : HelpPortalRichTextEditorController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/3/2021, 5:21:51 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         4/30/2019                Fernando Gomez            Initial Version
**/

({
	doInit: function(component, event, helper) {
		component.set("v.isError", false);
		component.set("v.isDisabled", true);
		component.set("v.message", null);
		component.set("v.isReady", false);
		component.set("v.isSaving", false);
		component.set("v.isSaved", false);
		component.set("v.isChanged", false);
		//alert(component.get("v.divisionCode"));
		// we then fetch the languages
		helper.getHtml(
			component,
			component.get("v.languageCode"),
			component.get("v.documentPrefix"),
			component.get("v.divisionCode"),
			function(result) {
				component.set("v.html", result);
				component.set("v.originalHtml", result);
				component.set("v.isDisabled", false);
				setTimeout(function() {
					component.set("v.isReady", true);
				}, 300);
			},
			function(error) {
				component.set("v.isError", true);
				component.set("v.message", error);
			});
	},
	handleChange: function(component, event, helper) {
		if (component.get("v.isReady")) {
			component.set("v.isChanged", true);
			helper.parseFiles(
				component,
				component.get("v.html"),
				function() {
					component.set("v.isDisabled", true);
				},
				function(newHtml) {
					component.set("v.isDisabled", false);
					component.set("v.html", newHtml);
				});
		}
	},
	save: function(component, event, helper) {
		component.set("v.isError", false);
		component.set("v.message", null);
		component.set("v.isSaving", true);
		component.set("v.isSaved", false);

		// we save the html
		helper.saveHtml(
			component,
			component.get("v.languageCode"),
			component.get("v.documentPrefix"),
			component.get("v.html"),
			component.get("v.divisionCode"),
            
			function(result) {
				component.set("v.isSaving", false);
				component.set("v.isSaved", true);
				component.set("v.isChanged", false);
			},
			function(error) {
				component.set("v.isSaving", false);
				component.set("v.isError", true);
				component.set("v.message", error);
			});
	}
})