/**
 * @File Name          : CloseCaseHelper.js
 * @Description        : "Close case" block to be included in subtab or modal window
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/9/2021, 1:17:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/10/2019, 4:18:12 PM    dmorales      Initial version
 * 1.1    8/7/2019, 4:18:12 PM     acantero      Update cmp to make it reusable
 * 1.2    12-22-2020, 4:18:12 PM   dmorales      Improve validate method
**/
({

	PRACTICE : 'Practice',
	LIVE : 'Live',
	INQUIRY : 'Inquiry',
	TYPE : 'Type',

	loadAll: function(cmp, helper){
		console.log('CloseCase -> loadAll');
		Promise.all([
			helper.getSubtypeByAgentList(cmp, helper),
			helper.loadDependencies(cmp, helper, 'Case.Type__c'), 
			helper.loadDependencies(cmp, helper, 'Case.Subtype__c')
		]).then(function(results) {
			console.log('CloseCase -> loadAll -> results');
			let subtypeByAgentList = results[0]; //Results from Promise 1
			let dependType = results[1]; //Results from Promise 2
			let dependSubType = results[2]; //Results from Promise 3
			cmp.set('v.subtypeByAgentList', subtypeByAgentList);
			console.log('subtypeByAgentList: ' +  JSON.stringify(subtypeByAgentList));
			//fill dependent picklist values
			cmp.set("v.dependType", dependType);
			cmp.set("v.dependSubtype", dependSubType);  
			//...
			cmp.set("v.loaded", true);
			//console.log("2. dependType-> " + JSON.stringify(dependType));
			//console.log("3. dependSubtype-> " + JSON.stringify(dependSubType));
		}).catch(function (error) {
			console.log("err: " + error);
			console.log("Error-->" + JSON.stringify(error));
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
				"title": "Error!",
				"message": "Error connecting with Data!.",
				"type": "error"}
			);			
			toastEvent.fire();
		});
	},
	
	loadDependencies: function(cmp, helper, dependToken){
		const params = {
			field: dependToken
		};
		return helper.runServerAction(cmp, helper, 'getDependencies', params);
	},

	getSubtypeByAgentList : function(cmp, helper) {
		return helper.runServerAction(cmp, helper, 'getSubtypeByAgentList', null);
	},

	validForm : function(cmp, fields) {
	  var existType = cmp.get("v.existValueType");
	  var existSubType = cmp.get("v.existValueSubType");
	  var isValid = true;
	 if( 
		   this.isEmpty(fields.Status) ||
		   this.isEmpty(fields.Origin) ||
		   this.isEmpty(fields.Live_or_Practice__c) ||
		   this.isEmpty(fields.Inquiry_Nature__c) ||
		   (this.isEmpty(fields.Type__c) && existType) ||
		   (this.isEmpty(fields.Subtype__c) && existSubType)
	   ){
		   isValid = false;
	    }
	  
	   return isValid;
	},

	isEmpty : function(str){
	   return ( !str || str.length === 0 )
	},

	changeValue : function(cmp, event, helper, fieldFired) {
		const closed = cmp.get('v.closed');
		if (closed) {
			return;
		}
		//else...
		console.log('CloseCase -> changeValue, fieldFired: ' + fieldFired);
		var newValue = event.getParam('value');
		console.log('newValue: ' + newValue);
		if(fieldFired == helper.INQUIRY) {
			const existValueType = helper.getExistType(cmp, helper, newValue);
			console.log('existValueType: ' + existValueType);
			cmp.set("v.existValueType", existValueType);
			//...
		} else if (fieldFired == helper.TYPE) {
			const existSubType = helper.getExistSubType(cmp, helper);
			console.log('existSubType: ' + existSubType);
			cmp.set("v.existValueSubType", existSubType);
		}
	},

	getExistType : function(cmp, helper, inquiryNature) {
		console.log("CloseCase -> getExistType");
		const dpType = cmp.get("v.dependType");
		//console.log("dpType: " + JSON.stringify(dpType));
		console.log("inquiryNature: " + inquiryNature);
		if(
			(inquiryNature !== '') && 
			(inquiryNature !== null) && 
			(dpType !== null)
		){
			const listDep = dpType[inquiryNature];
			console.log("listDep: " + listDep);
			if(listDep !== undefined) {
				return true;
			}
		}
		//else...
		return false;
	},

	getExistSubType : function(cmp, helper) {
		console.log('CloseCase -> getExistSubType');
		const subtypeByAgentList = cmp.get('v.subtypeByAgentList');
		const liveOrPractice = cmp.find("liveOrPractice").get("v.value");
		const live = (liveOrPractice == helper.LIVE);
		const practice = (liveOrPractice == helper.PRACTICE);
		const inquiryNature = cmp.find("inquiry").get("v.value");
		const type = cmp.find("type").get("v.value");
		console.log('live: ' + live);
		console.log('practice: ' + practice);
		console.log('inquiryNature: ' + inquiryNature);
		console.log('type: ' + type);
		for(let info of subtypeByAgentList) {
			/*
			// DO NOT REMOVE
			console.log('------------------------');
			console.log('info.live: ' + info.live);
			console.log('info.practice: ' + info.practice);
			console.log('info.inquiryNatureMapped: ' + info.inquiryNatureMapped);
			console.log('info.serviceType: ' + info.serviceType);
			const liveCheck = (info.live == true) && (live == true);
			const practiceCheck = (info.practice == true) && (practice == true);
			const inquiryNatureCheck = (info.inquiryNatureMapped == inquiryNature);
			const typeCheck = (info.serviceType == type);
			console.log('liveCheck: ' + liveCheck);
			console.log('practiceCheck: ' + practiceCheck);
			console.log('inquiryNatureCheck: ' + inquiryNatureCheck);
			console.log('typeCheck: ' + typeCheck);
			*/
			if (
				(
					((info.live == true) && (live == true)) ||
					((info.practice == true) && (practice == true))
				) &&
				(info.inquiryNatureMapped == inquiryNature) &&
				(info.serviceType == type)
			) {
				console.log('getExistSubType -> true');
				return true;
			}
		}
		//else...
		console.log('getExistSubType -> false');
		return false;
	},

	getRecordFormFieldsDataObj : function(cmp) {
		var result = {
			Status : cmp.find("status").get("v.value"),
			Origin : cmp.find("origin").get("v.value"),
			Live_or_Practice__c : cmp.find("liveOrPractice").get("v.value"),
			Inquiry_Nature__c : cmp.find("inquiry").get("v.value"),
			Type__c : cmp.find("type").get("v.value"),
			Subtype__c : cmp.find("subtype").get("v.value")
		};
		return result;
	},

	fireCaseClosedEvt : function(cmp, source) {
		console.log('CloseCaseHelper.fireCaseClosedEvt');
		var recordId = cmp.get("v.recordId");
		var releaseCapacity = cmp.get('v.releaseCapacity');
		console.log('recordId: ' + recordId + ', source: ' + source);
		if (recordId) {
			var caseClosedEvent = $A.get("e.c:CaseClosedEvent"); 
			caseClosedEvent.setParam("caseId", recordId);
			caseClosedEvent.setParam("source", source);
			caseClosedEvent.setParam("releaseCapacity", releaseCapacity);
			caseClosedEvent.fire();
		} else {
			console.error('event can not be fired, recordId is null or undefined');
		}
	},

	runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }

})