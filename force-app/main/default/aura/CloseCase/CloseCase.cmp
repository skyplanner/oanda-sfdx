<!--
  @Component Name     : CloseCase.cmp
  @Description        : "Close case" block to be included in subtab or modal window
  @Author             : dmorales
  @Group              : 
  @Last Modified By   : acantero
  @Last Modified On   : 12/9/2021, 1:16:07 PM
  @Modification Log   : 
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    7/10/2019,   4:03:35 PM   dmorales      Initial Version
  1.1    8/7/2019,   4:03:35 PM    acantero      Update cmp to make it reusable
  1.2    12-22-2020, 10:03:35 AM   dmorales      Improve validation method
-->
<aura:component 
    implements="flexipage:availableForAllPageTypes" 
    access="global"
    controller="CloseCaseCmpController">
    <!-- api properties -->
    <aura:attribute name="recordId" type="String" />
    <aura:attribute name="owner" type="String" />
    <!--  -->
    <aura:attribute name="loaded" type="Boolean" default="false"/>
    <aura:attribute name="busy" type="Boolean" default="true" />
    <aura:attribute name="closed" type="Boolean" default="false" />
    <aura:attribute name="disableCloseCase" type="Boolean" default="false" />
    <aura:attribute name="releaseCapacity" type="Boolean" default="false" />
    <!-- attb associated with force:recordData -->
    <aura:attribute name="case_recordLoader" type="Object"/>
    <aura:attribute name="simpleCase" type="Object"/>
    <aura:attribute name="caseError" type="String"/>
    <!-- For manage dependent picklist -->
    <aura:attribute name="dependType" type="Map"/>
    <aura:attribute name="dependSubtype" type="Map"/>
    <aura:attribute name="subtypeByAgentList" type="List"/>
    <!-- for manage onchange of the lighning:inputField associated with the 
        dependent picklist Type-->
    <aura:attribute name="inquiryValue" type="String" />
    <aura:attribute name="existValueType" type="Boolean" default="false"/>
    <aura:handler name="change" value="{!v.inquiryValue}" 
        action="{!c.onInquiryChange}"/> 
    <!-- for manage onchange of the lighning:inputField associated with the 
        dependent picklist Subtype-->
    <aura:attribute name="typeValue" type="String" />
    <aura:attribute name="existValueSubType" type="Boolean" default="false"/>

    <aura:registerEvent name="caseClosed" type="c:CaseClosedEvent"/>

    <aura:handler name="change" value="{!v.typeValue}"
        action="{!c.onTypeChange}"/>

    <aura:handler name="change" value="{!v.recordId}"
        action="{!c.onRecordIdChange}"/>
    
    <!-- for manage init event-->
    <aura:handler name="init" value="{! this }" action="{! c.doInit }" />
        
    <aura:method name="submit" action="{!c.submit}">
        <aura:attribute name="releaseCapacity" type="Boolean" default="false" />
    </aura:method>
    <aura:method name="refresh" action="{!c.refresh}"/>
    <aura:method name="fireAnonymousCaseClosedEvt" action="{!c.fireAnonymousCaseClosedEvt}"/>
    
    <lightning:omniToolkitAPI aura:id="omniToolkit" />
    
    <aura:if isTrue="{! v.loaded }">
        <!-- Was used force:recordData insted of aura:if 
         in order to load faster the component as Case is loaded from beginner-->
        <force:recordData aura:id="caseRecordLoader"
            recordId="{!v.recordId}"
            fields="Origin,Live_or_Practice__c,Status,Is_Marked_as_Important__c,
            Complaint__c,Inquiry_Nature__c,Type__c,Subtype__c,RecordTypeId"
            targetRecord="{!v.case_recordLoader}"
            targetFields="{!v.simpleCase}"
            targetError="{!v.caseError}" />

        <!-- onsubmit="{!c.handleSubmit}" -->
        <lightning:recordEditForm aura:id="closeCaseForm" 
            onload="{!c.handleLoad}"
            onerror="{! c.handleError}"
            onsuccess="{!c.handleSuccess}"
            recordId="{!v.recordId}" objectApiName="Case">
                
            <lightning:messages />
            <lightning:inputField aura:id="recTypeId" fieldName="RecordTypeId" class="slds-hide" />
            <lightning:inputField 
                aura:id="agentCapacityStatus" 
                fieldName="Agent_Capacity_Status__c" 
                value="Closed"
                class="slds-hide" />
            <lightning:layout>
                <lightning:layoutItem flexibility="auto" padding="around-small">
                    <lightning:inputField aura:id="status" fieldName="Status" 
                        class="customRequired" disabled="true"/>
                    <lightning:inputField aura:id="comments" fieldName="Comments" />
                    <lightning:inputField aura:id="liveOrPractice" 
                        fieldName="Live_or_Practice__c" 
                        class="customRequired"/>
                    <lightning:inputField aura:id="origin" fieldName="Origin" 
                        class="customRequired"/> 				           
                </lightning:layoutItem>
                <lightning:layoutItem flexibility="auto" padding="around-small">
                    <lightning:inputField aura:id="inquiry"
                    fieldName="Inquiry_Nature__c" class="customRequired"
                    value="{!v.inquiryValue}"/>
                    <lightning:inputField aura:id="type" fieldName="Type__c"
                    class="{! v.existValueType ? 'customRequired' : ''}"
                    value="{! v.typeValue }"/> 				
                    <lightning:inputField aura:id="subtype" 
                    fieldName="Subtype__c" 
                    class="{! v.existValueSubType ? 'customRequired' : ''}"/> 
                    <lightning:inputField aura:id="isComplaint"
                    fieldName="Complaint__c" /> 
                    <lightning:inputField aura:id="markedImportant" 
                        fieldName="Is_Marked_as_Important__c" />   
                </lightning:layoutItem>           
            </lightning:layout>          
        </lightning:recordEditForm> 	 
        <aura:if isTrue="{! v.busy }">
            <div>
                <lightning:spinner variant="brand" size="small" />
            </div>
        </aura:if>
    </aura:if>
</aura:component>