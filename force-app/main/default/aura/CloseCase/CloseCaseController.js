/**
 * @File Name          : CloseCaseController.js
 * @Description        : "Close case" block to be included in subtab or modal window
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 12/9/2021, 1:20:28 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/10/2019,   4:03:35 PM   dmorales     Initial Version
 * 1.1    8/7/2019,   4:03:35 PM    acantero      Update cmp to make it reusable
 * 1.2    12-22-2020, 10:03:35 AM   dmorales      Improve validation method
**/
({
	doInit : function (component, event, helper) {
        console.log('CloseCase 1.23');
        var recordId = component.get("v.recordId");
        var owner = component.get("v.owner");
        console.log('recordId: ' + recordId + ', owner: ' + owner);
        if (recordId) {
			helper.loadAll(component, helper);
		}		
    },

    onRecordIdChange : function(component, event, helper) {
        console.log('onRecordIdChange');
        var newVal = event.getParam('value');
        var recordId = component.get("v.recordId");
        console.log('newVal: ' + newVal + ', recordId: ' + recordId);
        if (recordId) {
			helper.loadAll(component, helper);
        }
	},
	
	fireAnonymousCaseClosedEvt : function(component, event, helper) {
		console.log('fireAnonymousCaseClosedEvt');
		helper.fireCaseClosedEvt(component, "");
	},
    
    refresh : function(component, event, helper) {
		console.log('CloseCaseController.refresh');
		var loaded = component.get("v.loaded");
		console.log('loaded: ' + loaded);
		if (loaded) {
			component.find("caseRecordLoader").reloadRecord();
			var handleLoad = component.get('c.handleLoad');
			$A.enqueueAction(handleLoad);
		}
	},

    submit : function(component, event, helper) {
		console.log('CloseCaseController.submit');
		component.set("v.busy", true);
		var params = event.getParam('arguments');
		if (params) {
			var releaseCapacity = params.releaseCapacity;
			component.set('v.releaseCapacity', releaseCapacity);
		}
		var data = helper.getRecordFormFieldsDataObj(component);
		//check de required fields
		var isValid = helper.validForm(component, data);
		console.log('isValid: ' + isValid);
		if(isValid){
			if (releaseCapacity) {
				component
					.find("agentCapacityStatus")
					.set("v.value", "Released" );
			}
		   component.find('closeCaseForm').submit();
		   return true;
		}
		//else... 
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error!",
			"message": $A.get("$Label.c.CloseCaseRequiredMsg"),
			"type": "error"
		});
		toastEvent.fire();
		component.set("v.busy", false);
		return false;
    },
	
	handleLoad : function (component, event, helper) {
		 //set * for required fields
	
		 //set default values according with type of Case
		 var caseObj = component.get("v.simpleCase");
		 if (!caseObj) {
			 console.error('CloseCaseController.handleLoad -> caseObj is null or undefined');
			 return;
		 }
		 //else...
		 component.find("status").set("v.value", "Closed" );
	     component.find("origin").set("v.value", caseObj.Origin );
	     component.find("liveOrPractice").set("v.value",
	     	caseObj.Live_or_Practice__c );
	     component.find("markedImportant").set("v.value", 
	     	caseObj.Is_Marked_as_Important__c );
		 component.find("isComplaint").set("v.value", caseObj.Complaint__c );
		 component.find("inquiry").set("v.value", caseObj.Inquiry_Nature__c );
		 component.find("type").set("v.value", caseObj.Type__c );
		 component.find("subtype").set("v.value", caseObj.Subtype__c );
		 component.find("recTypeId").set("v.value", caseObj.RecordTypeId );
		 //stop show spinner
		 component.set("v.busy", false);
    }, 
    
	handleSuccess : function (component, event, helper) {
        console.log('CloseCase -> handleSuccess');
		component.set("v.closed", true);
		component.set("v.busy", false);
		var owner = component.get("v.owner");
		helper.fireCaseClosedEvt(component, owner);
	},

	handleError : function (component, event, helper) {
		var errors = event.getParams();

		component.set("v.busy", false);
		component.set("v.showModal",false);        

		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({"title": "Error!",
			"message": errors.message,
			"type": "error"});
		toastEvent.fire();
	   
	},

	onInquiryChange : function (component, event, helper) {
		 helper.changeValue(component, event, helper, helper.INQUIRY);             
	},

	onTypeChange : function (component, event, helper) {
		 helper.changeValue(component, event, helper, helper.TYPE); 	             
	}
	 
})