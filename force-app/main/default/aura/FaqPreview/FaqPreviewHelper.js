/**
 * @File Name          : FaqPreviewHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/25/2020, 4:23:51 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/26/2020   acantero     Initial Version
**/
({
    FAQ_DETAIL_URL : '/apex/FaqDetail',
    ID_PARAM : 'id',
    CONSOLE_PARAM : 'capp',
    SOURCE_URL_PARAM : 'surl',
    CONSOLE_PARAM_VALUE : '1',

    getArticleInfo : function(cmp, helper, recordId) {
        var params = {
            articleId : recordId
        };
        return helper.runServerAction(cmp, helper, 'getArticleLanguage', params).then(
			function(result) {
                if (result) {
                    console.log('getArticleLanguage result: ' + result);
                    cmp.set("v.language", result);
                    helper.setArticleUrl(cmp, helper, recordId);
                }
			}
		);
    },

    setArticleUrl : function(cmp, helper, recordId) {
        var baseUrl = window.location.href;
        console.log('baseUrl: ' + baseUrl);
        var prefix = '://';
        var pos = baseUrl.indexOf(prefix);
        if (pos != -1) {
            var pos2 = baseUrl.indexOf('/', pos + prefix.length);
            if (pos2 != -1) {
                baseUrl = baseUrl.substring(0, pos2);
            }
        }
        console.log('baseUrl: ' + baseUrl);
        var artUrl = helper.FAQ_DETAIL_URL + '?' + 
            helper.ID_PARAM + '=' + recordId;
        var previewUrl = artUrl + '&' +
            helper.CONSOLE_PARAM + '=' + helper.CONSOLE_PARAM_VALUE + '&' +
            helper.SOURCE_URL_PARAM + '=' + baseUrl;
        console.log('setArticleUrl -> previewUrl: ' + previewUrl);
        cmp.set("v.previewUrlInNewTab", artUrl);
        cmp.set("v.previewUrl", previewUrl);
        cmp.set('v.loaded', true);
    },

    navigateToArticle : function(cmp, helper, urlName) {
        console.log('navigateToArticle');
        var language = cmp.get('v.language');
        console.log('language: ' + language);
        var params = {
            urlName : urlName, 
            language: language
        };
        return helper.runServerAction(cmp, helper, 'getArticleId', params).then(
			function(result) {
                if (result) {
                    console.log('new article id: ' + result);
                    var pageReference = {    
                        "type": "standard__recordPage",
                        "attributes": {
                            "recordId": result,
                            "objectApiName": "FAQ__kav",
                            "actionName": "view"
                        }
                    };
                    var navService = cmp.find("navService");
                    navService.navigate(pageReference);
                }
			}
		);
    },

    runServerAction : function(cmp, helper, method, params) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(self, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
	}
})