/**
 * @File Name          : FaqPreviewController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 7/8/2020, 6:55:56 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/26/2020   acantero     Initial Version
**/
({
    doInit : function (cmp, event, helper) {
		console.log('FaqPreviewController.js v32');
		var recordId = cmp.get("v.recordId");
		console.log('recordId: ' + recordId);
		if (recordId) {
			helper.getArticleInfo(cmp, helper, recordId);
		}
		window.addEventListener("message", $A.getCallback(function(event) {
			//console.log('event.data: ' + event.data);
			var urlNameSufix = 'URL_NAME:';
			if (
				event.data &&
				(typeof event.data == "string") &&
				(event.data.startsWith(recordId))
			) {
				if (event.stopPropagation) {
					console.log('stopPropagation');
					event.stopPropagation();
				}
				var first = recordId.length;
				if (event.data.length == recordId.length) {
					console.log('onMessage -> invalid event.data lenght');
					return;
				}
				//else...
				var urlName = event.data.substring(first);
				console.log('urlName: ' + urlName);
				//alert('received article name: ' + articleName);
				helper.navigateToArticle(cmp, helper, urlName);
			}
        }), false);
	}
	
})