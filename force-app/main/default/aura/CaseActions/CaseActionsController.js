/**
 * @File Name          : CaseActionsController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/22/2021, 11:32:33 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/22/2021, 5:51:06 PM   acantero     Initial Version
**/
({

    doInit : function(cmp, event, helper) {
        console.log('CaseActions v 1.9');
    },

    onRecordUpdated : function(cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            console.error('CaseActions -> onRecordUpdated -> ERROR');
            return;
        } else if (changeType === "REMOVED"){
            console.log('CaseActions -> onRecordUpdated -> REMOVED');
            return;
        } 
        //else...
        console.log('CaseActions -> onRecordUpdated');
        cmp.set('v.ready', true);
        const caseRecord = cmp.get('v.caseRecord');
        helper.udpateVisibility(
            cmp, 
            helper, 
            caseRecord.IsClosed
        );
    },

    onRefreshView : function(cmp, event, helper) {
        console.log('CaseActions -> onRefreshView');
        cmp.find("recordLoader").reloadRecord();
    },

    onCaseClosed : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        console.log('CaseActions -> onCaseClosed, caseId: ' + recordId);
        var caseId = event.getParam('caseId');
        if (!helper.compareIds(caseId, recordId)) {
			console.log('is not my problem, so... exit');
			return;
		}
        //else...
        console.log('then update visibility...');
        helper.udpateVisibility(cmp, helper, true);
	}
    
})