/**
 * @File Name          : CaseActionsHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/22/2021, 11:12:00 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/22/2021, 10:30:14 PM   acantero     Initial Version
**/
({

    udpateVisibility : function(cmp, helper, isClosed) {
        console.log('CaseActions -> udpateVisibility, isClosed: ' + isClosed);
        const hideIfClosed = cmp.get('v.hideIfClosed');
        const topContainer = cmp.find('top-container');
        let newClass;
        let oldClass;
        if ((isClosed == true) && (hideIfClosed == true)) {
            newClass = 'hidden';
            oldClass = 'visible';
        } else {
            newClass = 'visible';
            oldClass = 'hidden';
        }
        $A.util.removeClass(topContainer, oldClass);
        $A.util.addClass(topContainer, newClass);
        console.log('CaseActions -> udpateVisibility, oldClass: ' + oldClass + ', newClass: ' + newClass);
    },

    compareIds : function(id1, id2) {
		if ((!id1) || (!id2)) {
			return false;
		}
		//else...
		if (id1.length == id2.length) {
			return (id1 == id2);
		}
		//else...
		if (id1.length < id2.length) {
			return (id2.indexOf(id1) == 0);
		}
		//else...
		return (id1.indexOf(id2) == 0);
	}

})