/** 
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 4/30/2019
 */
({
	getFxAccounts: function(component, accountId, isLive, onSuccess, onError) {
		this._invoke(component, "c.getFxAccounts", 
			{"accountId": accountId, "isLive": isLive}, onSuccess, onError);
	},
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					onError((errors && errors[0] && errors[0].message) ||
						"Unknow Error");
					break;
			}
		});
		$A.enqueueAction(action);
	}
})