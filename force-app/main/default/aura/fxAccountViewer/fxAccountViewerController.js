/**
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 5/2/2019
 */
({
	doInit: function(component, event, helper) {
		var a = component.get("c.getAccounts");
		$A.enqueueAction(a);
	},
	getAccounts: function(component, event, helper) {
		component.set("v.isBusy", true);
		component.set("v.isError", false);
		component.set("v.accountsCount", 0);
		component.set("v.accounts", []);
		component.set("v.lastUpdated", new Date());
		helper.getFxAccounts(
			component,
			component.get("v.recordId"),
			component.get("v.isLive"),
			function(result) {
				component.set("v.isBusy", false);
				component.set("v.accountsCount", result.length);
				component.set("v.accounts", result);
				setTimeout(function() {
					$A.enqueueAction(component.get("c.fixWidth"));
				}, 1000);
			},
			function(error) {
				component.set("v.isBusy", false);
				component.set("v.isError", true);
				component.set("v.errorMessage", error);
			})
	},
	fixWidth: function(component, event, helper) {
		var headerTable, bodyDiv, bodyTable;
		headerTable = $(component.find("headerTable").getElement()),
		bodyDiv = $(component.find("bodyDiv").getElement()),
		bodyTable = $(component.find("bodyTable").getElement());
		headerTable.css({"paddingRight": bodyDiv.width() - bodyTable.width()});
	},
	handleSelect: function(component, event, helper) {
		switch (event.getParam("value")) {
			case "GoToRecord":
				var navEvt = $A.get("e.force:navigateToSObject");
				if (navEvt) {
					navEvt.setParams({
						"recordId": event.getSource().get("v.value"),
						"slideDevName": "detail"
					});
					navEvt.fire();
				} 
				break;
			case "Change2FAPhone":
				component.set("v.showModal", true);
				component.set("v.currentRecordId", event.getSource().get("v.value"));
				component.set("v.activeComponent", "PhoneAuthDialog");
				break;
			case "ChangePassword":
				component.set("v.showModal", true);
				component.set("v.currentRecordId", event.getSource().get("v.value"));
				component.set("v.activeComponent", "PasswordReset");
				break;
		}
	},
	closeModal: function(component, event, helper) {
		component.set("v.showModal", false);
		component.set("v.currentRecordId", null);
		component.set("v.activeComponent", null);
	},
	handleOpen: function(component, event, helper) {
		/*
		var el = $(event.getSource().getElement()),
			offset = el.offset();
		el.css({
			"positions": fixed,
			"top": offset.top,
			"left": offset.left,
			"right": "auto"
		});
		*/
	}
})