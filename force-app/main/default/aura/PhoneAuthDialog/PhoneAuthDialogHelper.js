({
	connectCometd : function(component) {
        var helper = this;
        
        // Configure CometD
        var cometdUrl = window.location.protocol+'//'+window.location.hostname+'/cometd/40.0/';
        var cometd = component.get('v.cometd');
        cometd.configure({
          url: cometdUrl,
          requestHeaders: { Authorization: 'OAuth '+ component.get('v.sessionId')},
          appendMessageTypeToURL : false
        });
        cometd.websocketEnabled = false;
        
        // Establish CometD connection
        console.log('Connecting to CometD: '+ cometdUrl);
        cometd.handshake(function(handshakeReply) {
          if (handshakeReply.successful) {
            console.log('Connected to CometD.');
            // Subscribe to platform event
            var newSubscription = cometd.subscribe('/event/Notification__e',
              function(platformEvent) {
                console.log('Platform event received: '+ JSON.stringify(platformEvent));
                helper.onReceive2FANotification(component, platformEvent);
              }
            );
            
            // Save subscription for later
            var subscriptions = component.get('v.cometdSubscriptions');
            subscriptions.push(newSubscription);
            component.set('v.cometdSubscriptions', subscriptions);
          }
          else
            console.error('Failed to connected to CometD.');
        });
      },
  disconnectCometd : function(component) {
    var cometd = component.get('v.cometd');
    
    // Unsuscribe all CometD subscriptions
    cometd.batch(function() {
      var subscriptions = component.get('v.cometdSubscriptions');
      subscriptions.forEach(function (subscription) {
        cometd.unsubscribe(subscription);
      });
    });
    component.set('v.cometdSubscriptions', []);
      
    // Disconnect CometD
    cometd.disconnect();
    console.log('CometD disconnected.');
  },
 
  onReceive2FANotification : function(component, platformEvent) {
        
        const TYPE_2FA_RESET = '2fa';
        
        var payload = platformEvent.data.payload;
        
      console.log('fxTrade user id: ' + component.get('v.fxAccount.fxTrade_User_Id__c'));
      console.log('user id: ' + component.get('v.userId'));
      console.log('session id: ' + component.get('v.sessionId'));
      
        if (component.get('v.fxAccount.fxTrade_User_Id__c') == payload.fxTrade_User_ID__c
            && payload.Type__c == TYPE_2FA_RESET
           	&& component.get('v.userId') == payload.Created_By_Id__c
            && component.get('v.sessionId') == payload.Session_Id__c
           ) {
            
            console.log('set the token');
            component.set('v.message', 'Please send this 2FA reset token to the user: ');  
            component.set('v.token', payload.Content__c);
           
        }
    }
})