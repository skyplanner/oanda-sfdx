({
	doInit : function(component, event, helper) {
      
      // Disconnect CometD when leaving page
      window.addEventListener('unload', function(event) {
        helper.disconnectCometd(component);
      });
      
      // Retrieve session id
      var action = component.get('c.getSessionId');
      action.setCallback(this, function(response) {
        if (component.isValid() && response.getState() === 'SUCCESS') {
          component.set('v.sessionId', response.getReturnValue());
          console.log('session id: ' + response.getReturnValue());
            
          if (component.get('v.cometd') != null){
                helper.connectCometd(component);
          }
            
        }
        else
          console.error(response);
      });
      $A.enqueueAction(action);
       
      // Retrieve user id
      var actionGetUserId = component.get('c.getUserId');
      actionGetUserId.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === 'SUCCESS') {
                component.set('v.userId', response.getReturnValue());
            }
            else
                console.error(response);
       });
       $A.enqueueAction(actionGetUserId);
        
        // Retrieve fxAccount
        if (component.get("v.recordId")) {
            var actionGetFxAccount = component.get('c.getFxAccount');
            actionGetFxAccount.setParams({
                "fxaId" : component.get("v.recordId"),
            });
            actionGetFxAccount.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === 'SUCCESS') {
                    component.set('v.fxAccount', response.getReturnValue());
                }
                else
                    console.error(response);
            });
            
            $A.enqueueAction(actionGetFxAccount);
        }
    },
   onCometdLoaded : function(component, event, helper) {
      
      var cometd = new org.cometd.CometD();
      component.set('v.cometd', cometd);
      if (component.get('v.sessionId') != null)
        helper.connectCometd(component);
   },
   
   on2FAReset : function(component, event, helper) {
        
        
        var button = event.getSource();
        button.set("v.disabled", true);
        
        var actionPublishAction = component.get("c.publishAction");
        
        console.log("fxAccount Id: " + component.get("v.recordId"));
        console.log("session id: " + component.get("v.sessionId"))
		        
        if (component.get("v.recordId")) {
            actionPublishAction.setParams({
                "fxTradeUserId" : component.get("v.fxAccount.fxTrade_User_Id__c"),
                "actionType" : "2fa",
                "parameter_1" : component.get('v.sessionId')
            });
            
            actionPublishAction.setCallback(this, function(resp) {
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    console.log(resp.getReturnValue());
                } else {
                    console.log(resp.getError());
                }
            });
            
            $A.enqueueAction(actionPublishAction);
            
            component.set('v.message', 'Retrieving 2FA reset token...');
        }
        
        
    }
   
})