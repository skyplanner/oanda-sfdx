({
	loadAccount: function (cmp, source) {
		var action = cmp.get("c.getAccountDynamic");
		var recordId = cmp.get("v.recordId");
		action.setParams({
			Id: recordId,
			sObjectName: source
		});

		action.setCallback(this, function (response) {
			var state = response.getState();
			var accountId;

			if (state === "SUCCESS") {
				var account = response.getReturnValue();
				if (account != null) {
					cmp.set("v.existAccount", true);
					cmp.set("v.accountId", account.Id);
					accountId = account.Id;
					this.createFxAccontViewer(cmp, account.Id);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(
					"Errors : " + JSON.stringify(response.getError()[0])
				);
			}
			var cmpEvent = cmp.getEvent("loadRecordEvent");
			if (cmpEvent) {
				cmpEvent.setParams({ recordId: accountId });
				cmpEvent.fire();
			}
		});
		$A.enqueueAction(action);
	},

	createFxAccontViewer: function (cmp, idAccount) {
		if (idAccount) {
			var isLive = cmp.get("v.isLive");
			$A.createComponent(
				"c:fxAccountViewer",
				{
					recordId: idAccount,
					isLive: isLive
				},
				function (result, status, errorMessage) {
					//Add the new button to the body array
					if (status === "SUCCESS") {
						var body = cmp.get("v.body");
						body.push(result);
						console.log(
							"CXNotesContainerHelper create cmp result " +
								JSON.stringify(result)
						);
						cmp.set("v.body", body);
					} else if (status === "INCOMPLETE") {
						console.log(
							"No response from server or client is offline."
						);
						// Show offline error
					} else if (status === "ERROR") {
						console.log(
							"CXNotesContainerHelper Error creating cmp: " +
								errorMessage
						);
						// Show error message
					}
				}
			);
		}
	}
});