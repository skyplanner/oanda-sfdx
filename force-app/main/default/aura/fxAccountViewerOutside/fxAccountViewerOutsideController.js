({
	doInit: function (component, event, helper) {
		var source = component.get("v.source");
		helper.loadAccount(component, source);
	}
});