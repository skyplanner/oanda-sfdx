/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-07-2022
 * @last modified by  : Yaneivys Gutierrez
**/
({
    doInit : function(component, event, helper) {
        var a = component.get("c.getAccount");
		$A.enqueueAction(a);
    },
	getAccount: function(component, event, helper) {
        console.log(component.get("v.recordId"));
		helper.getAccountInvoke(
			component,
			component.get("v.recordId"),
			function(result) {
                console.log(result);
                component.set("v.account", result[0]);
                var a = component.get("c.showMsg");
		        $A.enqueueAction(a);
			},
			function(error) {
                console.log(error);
			})
	},
	showMsg: function(component, event, helper) {
		var acc = component.get("v.account");
        if (acc.State_Alberta__c) {
            console.log(acc.State_Alberta__c);
            component.set('v.showConfirmDialog', true);
        }
	},
    handleConfirmDialogKeep: function(component, event, helper) {
        console.log('Keep');
        helper.invokeResponse(
			component,
			component.get("v.recordId"),
            true,
			function(result) {
                console.log('success');
                $A.get('e.force:refreshView').fire();
			},
			function(error) {
                console.log(error);
			}
        );
        component.set('v.showConfirmDialog', false);
    },
    handleConfirmDialogClean: function(component, event, helper) {
        console.log('Clean');
        helper.invokeResponse(
			component,
			component.get("v.recordId"),
            false,
			function(result) {
                console.log('success');
                $A.get('e.force:refreshView').fire();
			},
			function(error) {
                console.log(error);
			}
        );
        component.set('v.showConfirmDialog', false);
    },
})