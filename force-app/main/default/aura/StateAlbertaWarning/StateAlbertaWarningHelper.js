/**
 * @description       : 
 * @author            : Yaneivys Gutierrez
 * @group             : 
 * @last modified on  : 12-07-2022
 * @last modified by  : Yaneivys Gutierrez
**/
({
    getAccountInvoke: function(component, accountId, onSuccess, onError) {
		this._invoke(component,
            "c.getAccountRecord", 
			{"accountId": accountId },
            onSuccess,
            onError
        );
	},
    invokeResponse: function(component, accountId, resp, onSuccess, onError) {
		this._invoke(
            component,
            "c.userResponse", 
			{"accountId": accountId, "resp": resp, },
            onSuccess,
            onError
        );
	},
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
        console.log('Invoke');
        console.log(parameters);
		var action, errors;
		action = component.get(methodName);

		if (parameters) {
            action.setParams(parameters);
        }

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					onError((errors && errors[0] && errors[0].message) ||
						"Unknow Error");
					break;
			}
		});
		$A.enqueueAction(action);
	}
})