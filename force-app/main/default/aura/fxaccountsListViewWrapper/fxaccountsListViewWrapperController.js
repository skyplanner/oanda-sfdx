({
    doInit : function (cmp, event, helper) {
        var myPageRef = cmp.get("v.pageReference");
        cmp.set("v.id", myPageRef.state.c__id);
    },
    setTabInfo : function(component, event, helper){
        console.log('Setting Tab Name');
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "fxAccounts"
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "custom:custom90"
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})