/**
 * @File Name          : OmniExtraController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2024, 2:16:37 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/22/2020   acantero     Initial Version
**/
({

    init: function(cmp, event, helper) { 
        console.log("OmniExtra 1.94");
        var isActive = cmp.get("v.isActive");
        console.log('isActive: ' + isActive);
        if (isActive) {
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            console.log('userId: ' + userId);
            cmp.set("v.userId", userId);
            helper.initWorkIdBackup(cmp, helper);
            helper.getOmniExtraInfo(cmp, helper);
        }
    },

    onLoginSuccess : function(cmp, event, helper) {
        var isActive = cmp.get("v.isActive");
        if (!isActive) {
            return;
        }
        //else...
        console.log('onLoginSuccess');
        //clean non-hvc list
        cmp.set('v.nonHvcCaseIdList', []);
        //...
        helper.createCheckClosingChatsInterval(cmp, helper);
        helper.registerAgentLogin(cmp, helper)
            .then($A.getCallback(updateResult => {
                if (updateResult == true) {
                    console.log('registerAgentLogin -> ok');
                    return helper.subscribeToNonHvcReleased(cmp, helper)
                        .then($A.getCallback(subscription => {
                            console.log('Subscription ok, channel: ', subscription.channel);
                            // Save subscription to unsubscribe later
                            cmp.set('v.nonHvcReleasedSubscription', subscription);
                            return helper.finishPendingCaseClosing(cmp, helper);
                        }));
                } 
                //else...
                return 'registerAgentLogin -> failed';
                //...
            })).then(result => {
                console.log('onLoginSuccess -> result: ' + result);
            }).catch(error => {
                console.error(error);
            });
    },

    onLogout : function(cmp, event, helper) {
        const isActive = cmp.get("v.isActive");
        if (!isActive) {
            return;
        }
        //else...
        console.log('onLogout');
        cmp.set('v.chatWorkClosingChecked', false);
        cmp.set('v.chatWorkClosingList', []);
        cmp.set('v.nonHvcCaseIdList', []);
        //...
        const interval = cmp.get('v.checkClosingChatsInterval');
        if (interval) {
            clearInterval(interval);
        }
        helper.unsubscribeToNonHvcReleased(cmp, helper);
    },

    onWorkloadChanged : function(cmp, event, helper) {
        var isActive = cmp.get("v.isActive");
        if (!isActive) {
            return;
        }
        //else...
        console.log('onWorkloadChanged');
        var configuredCapacity = helper.getIntValue(event.getParam('configuredCapacity'));
        var previousWorkload = helper.getIntValue(event.getParam('previousWorkload'));
        var newWorkload = helper.getIntValue(event.getParam('newWorkload'));
        console.log('configured: ' + configuredCapacity);
        console.log('previous: ' + previousWorkload);
        console.log('new: ' +newWorkload);
        var currentCapacity = configuredCapacity - newWorkload;
        console.log('currentCapacity: ' + currentCapacity);
        helper.updateAgentCapacity(cmp, helper, currentCapacity)
            .then(function(result) {
                console.log('onWorkloadChanged -> result: ' + result);
            }).catch(function(error) {
                console.error('onWorkloadChanged -> ' + error);
            });
    },

    // onStatusChanged : function(cmp, event, helper) {
    //     var isActive = cmp.get("v.isActive");
    //     if (!isActive) {
    //         return false;
    //     }
    //     //else...
    //     console.log('');
    //     var statusId = event.getParam('statusId');
    //     var channels = event.getParam('channels');
    //     var statusName = event.getParam('statusName');
    //     var statusApiName = event.getParam('statusApiName');
    //     console.log(statusId);
    //     console.log(channels);
    //     console.log(statusName);
    //     console.log(statusApiName);
    //     //var channelsList = JSON.parse(channels);
    //     if (channels) {
    //         var valid = true;
    //         var i = 0;
    //         do {
    //             if (channels[i]) {
    //                 console.log('channel ' + i + ': ' + channels[i]);
    //                 i++;
    //             } else {
    //                 valid = false;
    //             }
    //         } while (valid);
    //     }
    // },

    onWorkAssigned : function(cmp, event, helper) {
        if (!helper.isFunctional(cmp, helper)) {
            return;
        }
        //else...
        var pushTimeout = cmp.get("v.pushTimeout");
        var workId = event.getParam('workId');
        var workItemId = event.getParam('workItemId');
        console.log('onWorkAssigned -> workId: ' + workId + ', workItemId: ' + workItemId);
        var isAMsgSession = helper.isAMessagingSession(cmp, helper, workItemId); 
        console.log('isAMsgSession: ' + isAMsgSession);

        if (isAMsgSession) {
            return;
        }
        
        var isChat = helper.isAChat(cmp, helper, workItemId);
        console.log('isChat: ' + isChat);

        if (isChat) {
            helper.registerWorkId(
                cmp,
                helper,
                workId,
                workItemId
            );
            helper.startPushTimer(
                cmp,
                helper,
                workId,
                pushTimeout
            );
            return;
        }
        // else...
        var isACase = helper.isACase(cmp, helper, workItemId);
        console.log('isACase: ' + isACase);
        
        if (!isACase) {
            helper.onNonHvcCaseAssigned(cmp, helper, workId, workItemId);
        }
    },

    onWorkAccepted : function(cmp, event, helper) {
        if (!helper.isFunctional(cmp, helper)) {
            return;
        }
        //else...
        var workId = event.getParam('workId');
        var workItemId = event.getParam('workItemId');
        console.log('onWorkAccepted -> workId: ' + workId + ', workItemId: ' + workItemId);
        var isChat = helper.isAChat(cmp, helper, workItemId);
        console.log('isChat: ' + isChat);
        if (!isChat) {
            return;
        }
        //else...
        helper.removeWorkId(
            cmp,
            helper,
            workId
        );
    },

    onCaseCapacityTimerStarted : function(cmp, event, helper) {
        console.log('OmniExtra -> onCaseCapacityTimerStarted');
        const caseId = event.getParam('caseId');
        const timeFrameInSeconds = event.getParam('timeFrameInSeconds');
        if (caseId && timeFrameInSeconds) {
            const stepInMilliseconds = timeFrameInSeconds * 1000; 
            setTimeout(
                $A.getCallback(function() {
                    helper.freeCaseCapacity(cmp, helper, caseId)
                        .then(function(result) {
                            console.log('case capacity timeout -> result: ' + result);
                            //...
                        }).catch(function(error) {
                            console.error(error);
                        });
                }), 
                stepInMilliseconds
            );
        }
    },

    onStatusChanged: function(cmp, event, helper) {
        console.log('OmniExtra -> onStatusChanged');
        var isActive = cmp.get("v.isActive");
        console.log('isActive: ' + isActive);
        if (!isActive) {
          return;
        }
        //else...
        var statusApiName = event.getParam("statusApiName");
        console.log('new status: ' + statusApiName);
        cmp.set("v.currentStatus", statusApiName);
    },

    onNonHvcCaseParked: function(cmp, event, helper) {
        console.log('OmniExtra -> onNonHvcCaseParked');
        cmp.set('v.parkingCase', false);
    },

    onNonHvcCaseParkingFail: function(cmp, event, helper) {
        console.log('OmniExtra -> onNonHvcCaseParkingFail');
        cmp.set('v.parkingCase', false);
    },

    onParkingCaseChange: function(cmp, event, helper) {
        console.log('OmniExtra -> onParkingCaseChange');
        const parkingCase = cmp.get('v.parkingCase');
        console.log('onParkingCaseChange -> parkingCase (new value): ' + parkingCase);
        if (!parkingCase) {
            helper.invokeProcessNonHvcCase(cmp, helper);
        }
    }
    
})