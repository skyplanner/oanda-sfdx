/**
 * @File Name          : OmniExtraHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 2/26/2024, 2:16:25 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/22/2020   acantero     Initial Version
**/
({
    LIVECHATTRANSCRIPT_PREFIX : '570',
    CASE_PREFIX : '500',
    MESSAGING_SESSION_PREFIX : '0Mw',
    NON_HVC_CASE_RELEASED_CHANNEL : '/event/Non_HVC_Case_Released__e',

    capacity : null,
    
    getOmniExtraInfo : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'getOmniExtraInfo', null).then(
			function(result) {
                if (result) {
                    helper.io(result, 'getOmniExtraInfo result: ');
                    if (result.pushTimeout > 0) {
                        var pushTimeoutMiliS = result.pushTimeout * 1000;
                        cmp.set("v.maxPushAttempts", result.maxPushAttempts);
                        cmp.set("v.pushTimeout", pushTimeoutMiliS);
                        cmp.set("v.servPresenceStatusIdList", result.servPresenceStatusIdList);
                        cmp.set("v.noHvcParkPresentStatuses", result.noHvcParkPresentStatuses);
                        cmp.set("v.isReady", true);
                        console.log('maxPushAttempts: ' + result.maxPushAttempts + ', pushTimeout: ' + pushTimeoutMiliS + ', isReady: true');
                    }
                }
			}
		).catch(function(error) {
            console.error('getOmniExtraInfo -> ' + error);
        });
    },

    isFunctional : function(cmp, helper) {
        var isActive = cmp.get("v.isActive");
        if (!isActive) {
            return false;
        }
        var isReady = cmp.get("v.isReady");
        return isReady;
    },

    isAChat : function(cmp, helper, workItemId) {
        var result = false;
        if (workItemId) {
            result = workItemId.startsWith(helper.LIVECHATTRANSCRIPT_PREFIX);
        }
        return result;
    },

    isACase : function(cmp, helper, workItemId) {
        var result = false;
        if (workItemId) {
            result = workItemId.startsWith(helper.CASE_PREFIX);
        }
        return result;
    },
    
    isAMessagingSession : function(cmp, helper, workItemId) {
        var result = false;
        if (workItemId) {
            result = workItemId.startsWith(helper.MESSAGING_SESSION_PREFIX);
        }
        return result;
    },

    createCheckClosingChatsInterval : function(cmp, helper) {
        console.log('createCheckClosingChatsInterval -> begin');
        const stepInMilliseconds = 1000;
        const interval = setInterval(
            $A.getCallback(function() {
                console.log('CheckClosingChatsInterval iteration begin');
                const chatWorkClosingChecked = cmp.get('v.chatWorkClosingChecked');
                if (!chatWorkClosingChecked) {
                    console.log('chatWorkClosingChecked is false');
                    helper.getPendingChatWorks(cmp, helper)
                        .then(function(chatWorksResult) {
                            console.log('getPendingChatWorks -> ' + chatWorksResult);
                        }).catch(function(error) {
                            console.error(error);
                        });
                    //...
                } else {
                    console.log('chatWorkClosingChecked is true');
                    const chatWorkClosingList = cmp.get('v.chatWorkClosingList');
                    if (chatWorkClosingList.length) {
                        console.log('chatWorkClosingList length: ' + chatWorkClosingList.length);
                        const closingInfo = chatWorkClosingList.shift();
                        cmp.set('v.chatWorkClosingList', chatWorkClosingList);
                        console.log('chatWorkClosingList updated (after shift)');
                        console.log('closingInfo.workId: ' + closingInfo.workId);
                        console.log('closingInfo.closingId: ' + closingInfo.closingId);
                        helper.freeCapacity(cmp, helper, closingInfo)
                            .then(function(freeCapacityResult) {
                                console.log('freeCapacity -> result: ' + freeCapacityResult);
                                //...
                            }).catch(function(error) {
                                console.error(error);
                            });
                    } else {
                        console.log('chatWorkClosingList is empty, then clear interval...');
                        const interval = cmp.get('v.checkClosingChatsInterval');
                        if (interval) {
                            clearInterval(interval);
                        }
                        console.log('CheckClosingChatsInterval clear interval ok');
                    }
                }
                console.log('CheckClosingChatsInterval iteration end');
            }), 
            stepInMilliseconds
        );
        cmp.set('v.checkClosingChatsInterval', interval);
        console.log('createCheckClosingChatsInterval -> ok');
    },

    getPendingChatWorks : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'getPendingChatWorks', null)
            .then(function(workList) {
                let validWorkList = [];
                let result = '';
                if (workList) {
                    result = 'getPendingChatWorks -> ok';
                    validWorkList = workList;
                    //...
                } else {
                    result = 'getPendingChatWorks -> return null';
                }
                cmp.set('v.chatWorkClosingList', validWorkList);
                cmp.set('v.chatWorkClosingChecked', true);
                return result;
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('getPendingChatWorks -> error: ' + JSON.stringify(error));
                }
                return 'getPendingChatWorks -> failed';
            });
    },

    freeCapacity : function(cmp, helper, closingInfo) {
        console.log('OmniExtra -> freeCapacity');
        const validWorkId = closingInfo.workId.substr(0,15);
        console.log('freeCapacity -> validWorkId: ' + validWorkId);
        return helper.closeAgentWork(cmp, helper, validWorkId);
    },

    closeAgentWork : function(cmp, helper, workId) {
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI
            .closeAgentWork({ workId })
            .then(function(closeResult) {
                if (closeResult) {
                    console.log('Work Item is closed successfully -> ' + closeResult);
                    return true;
                } 
                //else...
                console.log('Close work failed');
                return false;
            }).catch(function(error) {
                console.log('Close work failed');
                console.error(error);
                return false;
            });
    },

    endWorkClosing : function(cmp, helper, closingObjId) {
        var params = {
            closingObjId
        };
        return helper.runServerAction(cmp, helper, 'endWorkClosing', params)
            .then(function(result) {
                console.log('endWorkClosing -> ok');
                return true;
            }).catch(function(error) {
                console.log('endWorkClosing -> failed');
                if (error) {
                    console.error('endWorkClosing -> error: ' + JSON.stringify(error));
                }
                return false;
            });
    },

    registerAgentLogin : function(cmp, helper) {
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI.getAgentWorkload()
            .then(function(getWorkloadResult) {
                console.log('getAgentWorkload successfully');
                var configuredCapacity = helper.getIntValue(getWorkloadResult.configuredCapacity);
                var currentWorkload = helper.getIntValue(getWorkloadResult.currentWorkload);
                console.log('configured: ' + configuredCapacity);
                console.log('workload: ' +  currentWorkload);
                var currentCapacity = configuredCapacity - currentWorkload;
                console.log('capacity: ' + currentCapacity);
                var params = {
                    capacity : currentCapacity
                };
                return helper.runServerAction(cmp, helper, 'onAgentLogin', params)
                    .then(function(updateResult) {
                        return true;
                    }).catch(function(error) {
                        if (error) {
                            console.error('registerAgentLogin -> error: ' + JSON.stringify(error));
                        }
                        return false;
                    });
            });
    },

    updateAgentCapacity : function(cmp, helper, capacity) {
        var params = {
            capacity : capacity
        };
        return helper.runServerAction(cmp, helper, 'updateAgentCapacity', params)
            .then(function(result) {
                return 'updateAgentCapacity -> ok';
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('updateAgentCapacity -> error: ' + JSON.stringify(error));
                }
                return 'updateAgentCapacity -> fail';
            });
    },

    deleteCurrentAgentInfo : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'deleteCurrentAgentInfo', null);
    },

    finishPendingCaseClosing : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'finishPendingCaseClosing', null)
            .then(function(finishPendingWorkResult) {
                return 'finishPendingCaseClosing -> ok';
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('finishPendingWorkClosing -> error: ' + error);
                    console.error('finishPendingWorkClosing -> error: ' + JSON.stringify(error));
                }
                return 'finishPendingCaseClosing -> failed';
            });
    },

    _finishPendingWorkClosing : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'finishPendingWorkClosing', null)
            .then(function(closingChatIdList) {
                if (closingChatIdList) {
                    console.log('closingChatIdList: ' + closingChatIdList);
                    if (closingChatIdList.length > 0) {
                        var omniAPI = cmp.find("omniToolkit");
                        return omniAPI.getAgentWorks()
                            .then(function(getAgentWorksResult) {
                                var works = JSON.parse(getAgentWorksResult.works);
                                return helper.freeClosingChat(
                                    helper,
                                    closingChatIdList,
                                    works,
                                    omniAPI
                                );
                            });
                    } 
                    //else...
                    return 'closingChatIdList is empty';
                } 
                //else...
                var nullResultMsg = 'closingChatIdList is null';
                console.error(nullResultMsg); 
                return nullResultMsg;
                //...
            });
    },

    processClosingChatIdList : function(cmp, helper) {
        var closingChatIdList = cmp.get('v.closingChatIdList');
        console.log(
            'processClosingChatIdList -> closingChatIdList.length: ' + 
            closingChatIdList.length
        );
        if (closingChatIdList.length == 0) {
            return;
        }
        var omniAPI = cmp.find("omniToolkit");
        console.log('will call getAgentWorks');
        omniAPI.getAgentWorks()
            .then(function(getAgentWorksResult) {
                if (!getAgentWorksResult) {
                    return 'getAgentWorksResult is null';
                }
                //else...
                console.log('getAgentWorks ok');
                var closingWorkIdList = [];
                console.log('getAgentWorks works: ' + getAgentWorksResult.works);
                var works = JSON.parse(getAgentWorksResult.works);
                console.log('works is ok');
                for(var chatId of closingChatIdList) {
                    for(var work of works) {
                        if (helper.compareIds(chatId, work.workItemId)) {
                            closingWorkIdList.push(work.workId);
                        }
                    }
                }
                cmp.set('v.closingWorkIdList', closingWorkIdList);
                return 'set closingWorkIdList ok';
                //...
            }).then(function(result) {
                console.log('processClosingWorkIdList -> ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },

    processClosingWorkIdList : function(cmp, helper) {
        var closingWorkIdList = cmp.get('v.closingWorkIdList');
        console.log(
            'processClosingWorkIdList -> closingWorkIdList.length: ' + 
            closingWorkIdList.length
        );
        if (closingWorkIdList.length == 0) {
            return;
        }
        //else...
        var workId = closingWorkIdList[0];
        console.log('processClosingWorkIdList -> workId: ' + workId);
        var omniAPI = cmp.find("omniToolkit");
        omniAPI.closeAgentWork({ workId })
            .then(function(closeResult) {
                if (closeResult) {
                    return 'Work Item is closed successfully -> ' + workId;
                } 
                //else...
                return 'Close work failed';
                //...
            }).then(function(result) {
                console.log('processClosingWorkIdList -> ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },

    freeClosingChat : function(helper, chatIdList, workList, omniAPI) {
        if (chatIdList.length == 0) {
            return Promise.resolve('freeClosingChat end ok');
        }
        //else...
        var chatId = chatIdList.pop();
        console.log('freeClosingChat -> chatId: ' + chatId);
        var workId;
        for(var work of workList) {
            if (helper.compareIds(chatId, work.workItemId)) {
                workId = work.workId;
                break;
            }
        }
        if (workId) {
            console.log('freeClosingChat -> workId: ' + workId);
            //var omniAPI = cmp.find("omniToolkit");
            return omniAPI.closeAgentWork({ workId })
                .then(function(closeResult){
                    if (closeResult) {
                        console.log('work closed ok, workId: ' + workId);
                    } else {
                        console.log('close work failed, workId: ' + workId);
                    }
                    return helper.freeClosingChat(helper, chatIdList, workList, omniAPI);
                });
        }
        //else... 
        console.log('workId not found for chatId: ' + chatId);
        return helper.freeClosingChat(helper, chatIdList, workList, omniAPI);
    },

    startPushTimer : function(cmp, helper, workId, timeout) {
        window.setTimeout(
            $A.getCallback(function() {
                console.log('timeout -> workId: ' + workId);
                var workInfo = helper.getValidWorkInfo(cmp, helper, workId);
                var isValid = (workInfo || false);
                console.log('workId isValid: ' + isValid);
                if (isValid) {
                    helper.tryToDeclineWork(cmp, helper, workInfo);
                }
            }), 
            timeout
        );
    },

    tryToDeclineWork : function(cmp, helper, workInfo) {
        var servicePresenceStatusIdList = cmp.get("v.servPresenceStatusIdList");
        var params = {
            workId : workInfo.workId,
            servicePresenceStatusIdList : servicePresenceStatusIdList
        };
        return helper.runServerAction(cmp, helper, 'workCanBeDeclined', params)
            .then(function(result) {
                console.log('workCanBeDeclined result: ' + result);
                if (result) {
                    return helper.processDeclineResult(
                        cmp,
                        helper,
                        workInfo,
                        result
                    );
                } 
                //else...
                return 'workCanBeDeclined return null';
            })
            .then(function (result3) {
                console.log("tryToDeclineWork result: " + result3);
            })
            .catch(function(error) {
                console.error('tryToDeclineWork -> ' + error);
            });
    },

    declineWork : function(cmp, helper, workId) {
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI.declineAgentWork( { workId: workId })
            .then($A.getCallback(declineResult => {
                if (declineResult) {
                    console.log("Declined work successfully");
                    return true;
                } 
                //else...
                console.log("Decline work failed");
                return false;
            }));
    },

    processDeclineResult : function(cmp, helper, workInfo, declineResult) {
        var workId = workInfo.workId;
        if (declineResult.workIsValid) {
            if (declineResult.canBeDeclined) {
                helper.removeWorkId(cmp, helper, workId);
                return helper.declineWork(cmp, helper, workId)
                    .then($A.getCallback(declineResult => {
                        if (declineResult) {
                            return helper.registerDeclinedChat(cmp, helper, workInfo);
                        } 
                        //else...
                        return "processDeclineResult -> failed";
                    }));
            } 
            //else...
            helper.registerPushAttemptResult(cmp, helper, workId);
            return 'work can not be declined';
        } 
        //else...
        helper.removeWorkId(cmp, helper, workId);
        return 'work is not valid';
    },

    registerDeclinedChat : function(cmp, helper, workInfo) {
        var params = {
            chatId : workInfo.workItemId
        };
        return helper.runServerAction(cmp, helper, 'registerDeclinedChat', params)
            .then(function(result) {
                console.log('registerDeclinedChat successfully');
                return 'registerDeclinedChat successfully';
            });
    },

    initWorkIdBackup : function(cmp, helper) {
        var workIdBackup = [];
        cmp.set("v.workIdBackup", workIdBackup);
        return workIdBackup;
    },

    registerWorkId : function(cmp, helper, workId, workItemId) {
        if (!workId) {
            console.log('workId is not valid');
            return;
        }
        //else...
        var workIdBackup = cmp.get("v.workIdBackup");
        if (!workIdBackup) {
            console.log('workIdBackup is empty, reinit');
            workIdBackup = helper.initWorkIdBackup(cmp, helper);
        }
        workIdBackup.push( 
            {
                workId : workId,
                workItemId : workItemId,
                attempts : 0
            }
        );
        //console.log('workIdBackup: ' + workIdBackup);
        cmp.set("v.workIdBackup", workIdBackup);
    },

    removeWorkId : function(cmp, helper, workId) {
        if (!workId) {
            console.log('workId is not valid');
            return false;
        }
        //else...
        var workIdBackup = cmp.get("v.workIdBackup");
        if (!workIdBackup) {
            console.log('workIdBackup is empty');
            return false;
        }
        //else...
        console.log('removeWorkId -> workIdBackup.length: ' + workIdBackup.length);
        for(var i = 0; i < workIdBackup.length; i++) {
            var info = workIdBackup[i];
            if (info.workId == workId) {
                workIdBackup.splice(i, 1);
                cmp.set("v.workIdBackup", workIdBackup);
                return true;
            }
        }
        //else...
        return false;
    },

    isValidWorkId : function(cmp, helper, workId) {
        var workIdBackup = cmp.get("v.workIdBackup");
        var workInfo = helper.getWorkInfo(workIdBackup, workId);
        if (workInfo) {
            return true;
        }
        //else...
        return false;
    },

    getValidWorkInfo : function(cmp, helper, workId) {
        var workIdBackup = cmp.get("v.workIdBackup");
        var workInfo = helper.getWorkInfo(workIdBackup, workId);
        return workInfo;
    },

    getWorkInfo : function(workIdBackup, workId) {
        if (!workId) {
            console.log('getWorkInfo -> workId is not valid');
            return null;
        }
        //else...
        if (!workIdBackup) {
            console.log('workIdBackup is empty');
            return null;
        }
        //else...
        for(var i = 0; i < workIdBackup.length; i++) {
            var info = workIdBackup[i];
            if (info.workId == workId) {
                return info;
            }
        }
        //else...
        return null;
    },

    registerPushAttemptResult : function(cmp, helper, workId) {
        var workIdBackup = cmp.get("v.workIdBackup");
        var workInfo = helper.getWorkInfo(workIdBackup, workId);
        if (!workInfo) {
            console.log('registerPushAttemptResult -> invalid workId: ' + workId);
            return;
        }
        //else...
        workInfo.attempts++;
        var maxPushAttempts = cmp.get("v.maxPushAttempts");
        if (workInfo.attempts == maxPushAttempts) {
            console.log('Max attempts reached, workId: ' + workId);
            helper.removeWorkId(cmp, helper, workId);
        } else {
            cmp.set("v.workIdBackup", workIdBackup);
            var pushTimeout = cmp.get("v.pushTimeout");
            console.log('start new attempt -> ' + workId + ', ' + workInfo.attempts);
            helper.startPushTimer(cmp, helper, workId, pushTimeout);
        }
    },

    freeCaseCapacity : function(cmp, helper, caseId) {
        console.log('OmniExtra -> freeCaseCapacity');
        var params = {
            caseId : caseId
        };
        return helper.runServerAction(cmp, helper, 'freeCaseCapacity', params)
            .then(function(result) {
                return 'freeCaseCapacity -> ok';
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('freeCaseCapacity -> ' + JSON.stringify(error));
                }
                return 'freeCaseCapacity -> failed';
            });
    },

    declineAssignedNonHvcCases : function(cmp, helper, rejectedNonHvcCaseInfo) {
        console.log(
            'OmniExtra -> declineAssignedNonHvcCases, rejectedNonHvcCaseId: ' + 
            rejectedNonHvcCaseInfo
        );
        const nonHvcCaseIdList = cmp.get('v.nonHvcCaseIdList');
        cmp.set('v.nonHvcCaseIdList', []);
        if (rejectedNonHvcCaseInfo) {
            nonHvcCaseIdList.push(rejectedNonHvcCaseInfo);
        }
        if (nonHvcCaseIdList.length == 0) {
            console.log('declineAssignedNonHvcCases -> nonHvcCaseIdList is empty');
            return Promise.resolve(false); 
        }
        //else...
        const realNonHvcCaseIdList = [];
        for(let nonHvcCaseInfo of nonHvcCaseIdList) {
            realNonHvcCaseIdList.push(nonHvcCaseInfo.nonHvcCaseId);
        }
        return helper.runServerAction(
            cmp, 
            helper, 
            'declineNonHvcCases', 
            {nonHvcCaseIdList : realNonHvcCaseIdList}
        ).then($A.getCallback(result => {
            console.log('declineAssignedNonHvcCases -> ok (deleted old non-hvc cases)');
            const declineActionList = [];
            const omniAPI = cmp.find("omniToolkit");
            console.log('declineAssignedNonHvcCases -> try to decline items in nonHvcCaseIdList');
            for(let nonHvcCaseInfo of nonHvcCaseIdList) {
                declineActionList.push(
                    omniAPI.declineAgentWork( 
                        { workId: nonHvcCaseInfo.workId }
                    )
                );
            }
            return Promise.all(
                declineActionList
            ).then($A.getCallback(result2 => {
                console.log('decline (omniApi) -> result: ' + result2);
                return true;
            }));    
            // console.log('try to decline, rejectedNonHvcCaseInfo.workId: ' + rejectedNonHvcCaseInfo.workId);

            // return helper.declineWork(
            //     cmp, 
            //     helper, 
            //     rejectedNonHvcCaseInfo.workId
            // ).then($A.getCallback(declineResult => {
            //     console.log('decline rejectedNonHvcCase -> result: ' + declineResult);
            //     return true;
            // }));
        }));
    },

    onNewNonHvcCase : function(cmp, helper, nonHvcCaseInfo) {
        console.log('OmniExtra -> onNewNonHvcCase');
        var currentStatus = cmp.get("v.currentStatus");
        console.log('currentStatus: ' + currentStatus);
        return helper.runServerAction(
            cmp, 
            helper, 
            'onNewNonHvcCase', 
            {currentStatus : currentStatus}
        ).then($A.getCallback(onNewNonHvcCaseResult => {
            console.log('onNewNonHvcCase -> canBeAssigned: ' + onNewNonHvcCaseResult.canBeAssigned);
            console.log('onNewNonHvcCase -> newStatusId: ' + onNewNonHvcCaseResult.newStatusId);
            const rejectedNonHvcCaseInfo = (onNewNonHvcCaseResult.canBeAssigned)
                ? null
                : nonHvcCaseInfo;
            if (onNewNonHvcCaseResult.newStatusId) {
                var validStatusId = onNewNonHvcCaseResult.newStatusId.substring(0,15);
                console.log('onNewNonHvcCase -> validStatusId: ' + validStatusId);
                var omniAPI = cmp.find("omniToolkit");
                return omniAPI
                    .setServicePresenceStatus({ statusId: validStatusId })
                    .then($A.getCallback(changeStatusResult => {
                        console.log("setServicePresenceStatus -> ok, new status: " + changeStatusResult.statusName);
                        return helper
                            .declineAssignedNonHvcCases(cmp, helper, rejectedNonHvcCaseInfo)
                            .then($A.getCallback(declineResult => {
                                console.log("declineAssignedNonHvcCases -> result: " + declineResult);
                                return onNewNonHvcCaseResult.canBeAssigned;
                            }));
                    }));
            } 
            //else...
            return onNewNonHvcCaseResult.canBeAssigned;
        }));
    },

    parkNonHvcCase : function(cmp, helper, nonHvcCaseInfo) {
        return helper
            .onNewNonHvcCase(cmp, helper, nonHvcCaseInfo)
            .then($A.getCallback(onNewNonHvcCaseResult => {
                console.log('onNewNonHvcCase -> result: ' + onNewNonHvcCaseResult);
                if (onNewNonHvcCaseResult) {
                    var omniNonHvcCaseCmp = cmp.find('omniNonHvcCase');
                    omniNonHvcCaseCmp.parkNonHvcCase(
                        nonHvcCaseInfo.nonHvcCaseId
                    );
                    console.log(
                        'parkNonHvcCase -> nonHvcCaseId: ' + 
                        nonHvcCaseInfo.nonHvcCaseId + 
                        ', parked ok'
                    );
                    return 'nonHvcCase parked ok';
                } 
                //else...
                cmp.set('v.parkingCase', false);
                return 'nonHvcCase can not be parked';
            }));
    },

    processNonHvcCase : function(cmp, helper) {
        console.log('OmniExtra -> processNonHvcCase');
        const nonHvcCaseIdList = cmp.get('v.nonHvcCaseIdList');
        console.log('nonHvcCaseIdList.length: ' + nonHvcCaseIdList.length);
        if (nonHvcCaseIdList.length > 0) {
            cmp.set('v.parkingCase', true);
            const nonHvcCaseInfo = nonHvcCaseIdList.shift();
            cmp.set('v.nonHvcCaseIdList', nonHvcCaseIdList);
            return helper.parkNonHvcCase(cmp, helper, nonHvcCaseInfo);
        }
        //else...
        cmp.set('v.parkingCase', false);
        return Promise.resolve('processNonHvcCase -> nonHvcCaseIdList is empty');
    },

    registerNonHvcCase : function(cmp, helper, workId, nonHvcCaseId) {
        console.log('registerNonHvcCase -> nonHvcCaseId: ' + nonHvcCaseId);
        const nonHvcCaseIdList = cmp.get('v.nonHvcCaseIdList');
        nonHvcCaseIdList.push(
            {
                workId : workId,
                nonHvcCaseId : nonHvcCaseId
            }
        );
        console.log('registerNonHvcCase -> nonHvcCaseIdList.length: ' + nonHvcCaseIdList.length);
        cmp.set('v.nonHvcCaseIdList', nonHvcCaseIdList);
        const parkingCase = cmp.get('v.parkingCase');
        console.log('registerNonHvcCase -> parkingCase (current value): ' + parkingCase);
        if (parkingCase) {
            return Promise.resolve('parkingCase is true -> wait for the nonHvcCaseParked event');
        } 
        //else...
        console.log('registerNonHvcCase -> parkingCase is false, then process...');
        cmp.set('v.parkingCase', true);
        return helper.processNonHvcCase(cmp, helper);
    },

    invokeProcessNonHvcCase: function(cmp, helper) {
        console.log('OmniExtra -> invokeProcessNonHvcCase');
        helper.processNonHvcCase(cmp, helper)
            .then($A.getCallback(result => {
                console.log('invokeProcessNonHvcCase -> result: ' + result);
                //return true;
                //...
            })).catch($A.getCallback(error => {
                console.error('invokeProcessNonHvcCase -> fail');
                if (error) {
                    console.error('invokeProcessNonHvcCase -> ' + error);
                }
                console.log('invokeProcessNonHvcCase -> execute again');
                helper.invokeProcessNonHvcCase(cmp, helper);
                //return false;
            }));
    },

    onNonHvcCaseReleased : function(cmp, helper) {
        console.log('OmniExtra -> onNonHvcCaseReleased');
        var currentStatus = cmp.get("v.currentStatus");
        return helper.runServerAction(
            cmp, 
            helper, 
            'onNonHvcCaseReleased', 
            {currentStatus : currentStatus}
        ).then($A.getCallback(newStatusId => {
            console.log('onNonHvcCaseReleased -> newStatusId: ' + newStatusId);
            if (newStatusId) {
                var validStatusId = newStatusId.substring(0,15);
                console.log('onNonHvcCaseReleased -> validStatusId: ' + validStatusId);
                var omniAPI = cmp.find("omniToolkit");
                return omniAPI
                    .setServicePresenceStatus({ statusId: validStatusId })
                    .then($A.getCallback(changeStatusResult => {
                        console.log("setServicePresenceStatus -> ok, new status: " + changeStatusResult.statusName);
                        return 'onNonHvcCaseReleased -> status changed ok';
                        //...
                    }));
            } 
            //else...
            return 'onNonHvcCaseReleased -> no status change required';
        }));
    },

    checkNonHvcCaseReleased : function(cmp, helper, agentId) {
        let userId = cmp.get('v.userId');
        console.log('userId: ', userId);
        console.log(
            'checkNonHvcCaseReleased ->: agentId: ' + 
            agentId + ', userId: ' + userId
        );
        if (userId != agentId) {
            console.log('a "parked" case has been released, but it is not mine');
            return;
        } 
        //else...
        console.log('a "parked" case of mine has been released');
        helper
            .onNonHvcCaseReleased(cmp, helper)
            .then($A.getCallback(onNewNonHvcCaseResult => {
                console.log('checkNonHvcCaseReleased -> result: ' + onNewNonHvcCaseResult);
                //...
            })).catch($A.getCallback(error => {
                console.log('checkNonHvcCaseReleased -> fail: ' + onNewNonHvcCaseResult);
                console.error('checkNonHvcCaseReleased -> error: ' + error);
            }));
    },

    onNonHvcCaseAssigned : function(cmp, helper, workId, nonHvcCaseId) {
        console.log('OmniExtra -> onNonHvcCaseAssigned');
        var noHvcParkPresentStatuses = cmp.get("v.noHvcParkPresentStatuses");
        console.log('noHvcParkPresentStatuses: ' + noHvcParkPresentStatuses);
        var currentStatus = cmp.get("v.currentStatus");
        console.log('currentStatus: ' + currentStatus);
        var parking = (
            (!currentStatus) ||  
            (!noHvcParkPresentStatuses) || 
            (!noHvcParkPresentStatuses.includes(currentStatus))
        );
        console.log('Parking Ability: ' + parking);
        if (parking) {
            helper.registerNonHvcCase(cmp, helper, workId, nonHvcCaseId)
                .then($A.getCallback(parkingResult => {
                    console.log('onWorkAssigned -> parkingResult: ' + parkingResult);
                    //...
                })).catch($A.getCallback(error => {
                    console.error('parkNonHvcCase -> fail');
                    if (error) {
                        console.error('parkNonHvcCase -> ' + error);
                    }
                    cmp.set('v.parkingCase', false);
                }));

        } else {
            // This "NoN-Hvc" case is not parked, which implies that the 
            // agent can open it, therefore it is necessary to mark it 
            // as assigned and change the owner of the case
            helper.registerAssignedNonHvcCase(cmp, helper, nonHvcCaseId)
                .then($A.getCallback(actionResult => {
                    console.log('registerAssignedNonHvcCase -> actionResult: ' + actionResult);
                    //...
                })).catch($A.getCallback(error => {
                    console.error('registerAssignedNonHvcCase -> fail');
                    if (error) {
                        //console.error('registerAssignedNonHvcCase -> ' + error);
                        console.error(JSON.stringify(error));
                    }
                }));
        }
    },

    registerAssignedNonHvcCase : function(cmp, helper, nonHvcCaseId) {
        console.log('OmniExtra -> registerAssignedNonHvcCase');
        return helper.runServerAction(
            cmp, // cmp
            helper, // helper 
            'registerAssignedNonHvcCase', // method
            {nonHvcCaseId : nonHvcCaseId} // params
        );
    },

    subscribeToNonHvcReleased : function(cmp, helper) {
        const empApi = cmp.find('empApi');
        return empApi.subscribe(
            helper.NON_HVC_CASE_RELEASED_CHANNEL, 
            -1, //Replay option to get new events
            $A.getCallback(platformEvent => {
                // Process event (this is called each time we receive an event)
                console.log('Received event: ', JSON.stringify(platformEvent));
                let agentId = platformEvent.data.payload.Agent_ID__c;
                console.log('Agent_ID__c: ', agentId);
                helper.checkNonHvcCaseReleased(cmp, helper, agentId);
            })
        );
    },

    unsubscribeToNonHvcReleased : function(cmp, helper) {
        const empApi = cmp.find('empApi');
        const subscription = cmp.get('v.nonHvcReleasedSubscription');
        if (!subscription) {
            return;
        }
        //else...
        empApi.unsubscribe(
            subscription,
            $A.getCallback(unsubscribeResult => {
                console.log('Unsubscribed ok, channel: '+ unsubscribeResult.subscription);
                cmp.set('v.nonHvcReleasedSubscription', null);
            })
        );
    },

    getIntValue : function(value) {
        console.log('getIntValue -> value: ' + value);
        var str = '0' + value;
        str = str.replace(/\.|,/g, '');
        return parseInt(str);
    },

    runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    },
    
    io : function(obj, objId) {
        var str = (objId) ? (objId + ' ->\n') : '';
        for (const prop in obj) {
            str += (prop + ': ' + obj[prop] + '\n');
        }
        console.log(str);
    }

})