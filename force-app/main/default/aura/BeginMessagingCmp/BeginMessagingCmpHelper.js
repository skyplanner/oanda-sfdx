/* eslint-disable no-console */
({
	ACTIVE_STATUS: "Active",
	openTab: function (cmp,helper,recordIdList) {
		var recordId = cmp.get("v.recordId");
		var workspaceAPI = cmp.find("workspace");
		workspaceAPI
			.isConsoleNavigation()
			.then(
				$A.getCallback(function (isConsoleNavigationResult) {
					if (isConsoleNavigationResult === true) {
						return workspaceAPI.getAllTabInfo().then(
							$A.getCallback(function (tabInfoList) {
								if (tabInfoList && tabInfoList.length > 0) {
									var tabInfoFound = false;
									var filterTabs = tabInfoList.filter(function(tab) {
										return (
											tab.recordId &&
											helper.compareIds(
												recordId,
												tab.recordId
											)
										);
									});
									if (filterTabs.length > 0) {
										var tabInfo = filterTabs[0];
										console.log(
											"tabInfo.recordId: " +
												tabInfo.recordId
										);
										console.log(
											"tabInfo.tabId: " + tabInfo.tabId
										);
										tabInfoFound = true;
									}
									if (tabInfoFound) {
										console.log(
											"tabInfoFound -> will openSubTab"
										);

										var promiseList = recordIdList.map(
											function (recordTIdToOpen) {
												return workspaceAPI.openSubtab({
													parentTabId: tabInfo.tabId,
													pageReference: null,
													recordId: recordTIdToOpen,
													url: null,
													focus: false
												});
											}
										);
										Promise.all(promiseList).then(
											$A.getCallback(function (
												subTabIdList
											) {
												console.log(
													"Promise.all ends ok"
												);
												return (
													"subTab Ids: " +
													subTabIdList
												);
											})
										);
									}
									return (
										"tab with recordId = " +
										recordId +
										" not found"
									);
								}
								return "tabInfoList is null or empty";
							})
						);
					}
					return "Is not a console navigation";
				})
			)
			.then(function (result) {
				console.log("openTab -> " + result);
			})
			.catch(function (error) {
				console.error("openTab -> fail, error: " + error);
			});
	},
	compareIds: function (id1, id2) {
		if (!id1 || !id2) {
			return false;
		}
		//else...
		if (id1.length === id2.length) {
			return id1 === id2;
		}
		//else...
		if (id1.length < id2.length) {
			return id2.indexOf(id1) === 0;
		}
		//else...
		return id1.indexOf(id2) === 0;
	}
});