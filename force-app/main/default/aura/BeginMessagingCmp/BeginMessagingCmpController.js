/* eslint-disable no-console */
({
	init: function () {
		console.log("BeginMessagingCmp v 1.00");
	},
	onRecordUpdated: function (cmp, event, helper) {
		console.log("BeginChatCmp -> onRecordUpdated");

		var changeType = event.getParams().changeType;

		if (changeType === "ERROR") {
			console.error("BeginChatCmp -> onRecordUpdated -> ERROR");
		} else if (changeType === "REMOVED") {
			console.log("BeginChatCmp -> onRecordUpdated -> REMOVED");
		} else if (changeType === "LOADED") {
			console.log("BeginChatCmp -> onRecordUpdated -> LOADED");
			var messagingSessionRecord = cmp.get("v.messagingSessionRecord");
			if (messagingSessionRecord) {
				console.log("Status: " + messagingSessionRecord.Status);
				//if (messagingSessionRecord.Status === helper.ACTIVE_STATUS) {
					console.log("CaseId: " + messagingSessionRecord.CaseId);
					console.log(
						"EndUserAccountId: " + messagingSessionRecord.EndUserAccountId
					);
					console.log("LeadId: " + messagingSessionRecord.LeadId);
					var recordIdList = [];
					var accountOrLeadId = null;
					var fxAccountId = null;
					if (messagingSessionRecord.EndUserAccountId) {
						accountOrLeadId =
							messagingSessionRecord.EndUserAccountId;
						if (messagingSessionRecord.EndUserAccount) {
							console.log(
								"EndUserAccount.fxAccount__c: " +
									messagingSessionRecord.EndUserAccount
										.fxAccount__c
							);
							fxAccountId =
								messagingSessionRecord.EndUserAccount
									.fxAccount__c;
						}
					} else if (messagingSessionRecord.LeadId) {
						accountOrLeadId = messagingSessionRecord.LeadId;
						if (messagingSessionRecord.Lead) {
							console.log(
								"Lead.fxAccount__c: " +
									messagingSessionRecord.Lead.fxAccount__c
							);
							fxAccountId =
								messagingSessionRecord.Lead.fxAccount__c;
						}
					}
					if (accountOrLeadId) {
						recordIdList.push(accountOrLeadId);
					}
					if (messagingSessionRecord.CaseId) {
						recordIdList.push(messagingSessionRecord.CaseId);
					}
					if (fxAccountId) {
						recordIdList.push(fxAccountId);
					}
					if (recordIdList.length > 0) {
						helper.openTab(cmp, helper, recordIdList);
					}
				//}
			}
		}
	}
});