/**
 * @File Name          : ArticleListController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/13/2019, 4:09:31 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/13/2019, 11:45:19 AM   acantero     Initial Version
**/
({
	doInit : function(component, event, helper) {
		//console.log('ArticleList2Controller v1');
		helper.initStatusList(component);
		helper.getCategories(component);
		helper.getAudiences(component);
		helper.getDivisions(component);
		helper.initSort(component);
		helper.fetch(component);
	},

	newArticle : function (component, event, helper) {
		var newArticleEvent = $A.get("e.force:navigateToURL");
	    newArticleEvent.setParams({
            "url": "/lightning/cmp/c__ArticleBuilder"
        });
        newArticleEvent.fire();
	},

	onSearchTxtKeyUp : function (component, event, helper) {
    	if (event.key == "Enter") {
    		//console.log('onSearchTxtKeyUp -> enter');
    		helper.fetch(component);
    	}
    },

    onSearchTxtChanged : function (component, event, helper) {
    	var searchTxt = component.get("v.searchText");
    	if (searchTxt == '') {
    		//console.log('onSearchTxtChanged -> searchText cleaned');
    		helper.fetch(component);
    	}
    },

    changeStatus: function(component, event, helper) {
    	helper.initSort(component);
		helper.fetch(component);
    },

	fetch: function(component, event, helper) {
		helper.fetch(component);
	},

	updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        //console.log('ArticleList2Controller.updateColumnSorting -> fieldName: ' + fieldName + ', sortDirection: ' + sortDirection);
        var sortBy = component.get("v.sortBy");
        //console.log('ArticleList2Controller.updateColumnSorting -> fieldName: ' + fieldName + ', sortDirection: ' + sortDirection);
        component.set("v.sortBy", fieldName);
        component.set("v.sortDirection", sortDirection);
        helper.fetch(component);
    },

	loadMore: function(component, event, helper) {
		//console.log('ArticleList2Controller -> loadMore');
		var dataTableCmp = event.getSource();
		dataTableCmp.set("v.isLoading", true);
		helper.fetchRecords(component,
			function(result) {
				helper.updateDataList(component, result, true);
				//console.log('after loadMore -> infLoading: ' + component.get('v.infLoading'));
				dataTableCmp.set("v.isLoading", false);
			},
			function(errors) {
				helper.showErrorToast(errors && errors[0] ? 
					errors[0].message : helper.msgs.UNKNOWN_ISSUE);
				dataTableCmp.set("v.isLoading", false);
			}
		);
	}

})