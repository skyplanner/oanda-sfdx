/**
 * @File Name          : ArticleListHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/13/2019, 4:11:05 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/9/2019, 1:53:18 PM   acantero     Initial Version
**/
({

	STATUS_ONLINE_LABEL: 'Published',
	STATUS_ONLINE: 'Online',
	STATUS_DRAFT: 'Draft',

	DESC_ORDER: 'desc',
	ASC_ORDER: 'asc',


	initSort: function(component) {
		var sortBy = (this.draftIsStatusSelected(component)) ? 
			'LastModifiedDate' : 'LastPublishedDate';
		component.set("v.sortBy", sortBy);
		component.set("v.sortDirection", this.DESC_ORDER);
	},

	initDataTable: function(component) {
		var columns = [{
			label: 'Title',
			fieldName: 'TitleLink',
			sortable: true,
			type: 'url',
			typeAttributes: {label: {fieldName: 'Title'}, target: '_self'} // _blank
		}];
		var dateColumn = { 
			sortable: true, 
			type: 'date', 
			// initialWidth: 200,
			typeAttributes: {
				year: "numeric",
				month: "numeric",
				day: "numeric",
				hour: "numeric",
				minute: "2-digit"
			}
		};
		if (this.draftIsStatusSelected(component)) {
			dateColumn.label = 'Last Modified Date';
			dateColumn.fieldName = 'LastModifiedDate';
		} else {
			dateColumn.label = 'Last Published Date';
			dateColumn.fieldName = 'LastPublishedDate';
		}
		columns.push(dateColumn);
		component.set("v.columns", columns);
	},

	draftIsStatusSelected: function(component) {
		var status = component.get("v.status");
		return (status == this.STATUS_DRAFT);
	},

	initStatusList: function(component) {
		var statusList = [
			{
				label: this.STATUS_ONLINE_LABEL,
				value: this.STATUS_ONLINE,
				selected : true
			},
			{
				label: this.STATUS_DRAFT,
				value: this.STATUS_DRAFT,
				selected : false
			}
		];
		component.set("v.statusList", statusList);
		component.set("v.status", this.STATUS_DRAFT);
	},

	getValidFilterList: function(list) {
		var result = [{
			label: 'No Filter',
			value: ''
		}];
		if (list && list.length) {
			result = result.concat(list);
		}
		return result;
	},

	getCategories: function(component) {
		var self = this;
		this._invoke(
			component,
			'c.getCategories',
			null,
			function(result) {
				component.set("v.categories", self.getValidFilterList(result));
			}
		);
	},

	getAudiences: function(component) {
		var self = this;
		this._invoke(
			component,
			'c.getAudiences',
			null,
			function(result) {
				component.set("v.audiences", self.getValidFilterList(result));
			}
		);
	},

	getDivisions: function(component) {
		var self = this;
		this._invoke(
			component,
			'c.getDivisions',
			null,
			function(result) {
				component.set("v.divisions", self.getValidFilterList(result));
			}
		);
	},

	fetch: function(component) {
		var helper = this;
		component.set('v.busy', true);
		component.set('v.articles', []);
		component.set('v.offset', 0);
		component.set('v.infLoading', true);
		this.initDataTable(component);
		component.set('v.recordCount', 0);
		component.set('v.offset', 0);
		this.countRecords(component,
			function(r) {
				var validCount = r || 0;
				component.set('v.recordCount', validCount);
				console.log('countRecords -> recordCount: '+ validCount);
			}
		);
		// we fetch the initial chunk of data
		this.fetchRecords(component,
			function(result) {
				helper.updateDataList(component, result, false);
				component.set('v.busy', false);
			}
		);
	},

	updateDataList: function(component, items, concat) {
		var newList = items || [];
		if (newList.length == 0) {
			//console.log('updateArticleList -> no results, set infLoading = false');
			component.set('v.infLoading', false);
			return;
		}
		//else...
		if (concat) {
			var current = component.get('v.articles') || [];
			var newList = current.concat(newList);
		}
		component.set('v.articles', newList);
		var newOffset = newList.length;
		component.set('v.offset', newOffset);
		//console.log('updateArticleList -> concat: ' + concat + ', newOffset: '+ newOffset);
		var recordCount = component.get('v.recordCount');
		var maxRows = component.get('v.maxRows');
		var maxLenght = ((recordCount > 0) && (recordCount < maxRows))
			? recordCount
			: maxRows;
		if (newOffset >= maxLenght) {
			component.set('v.infLoading', false);
			//console.log('updateOffset -> infLoading = false');
		}
	},

	getCriterias: function(component) {
		var criterias = {
			searchText: component.get("v.searchText") || null,
			status: component.get("v.status"),
			category: component.get("v.category"),
			audience: component.get("v.audience"),
			division: component.get("v.division"),
			hasPreviewDoc : true,
			excludeArtIds : null,
			restrictToVisibleInPkb : false
		};
		//this.io(criterias, 'criterias');
		return criterias;
	},

	countRecords: function(component, onSuccess, onError) {
		var params = {
			criterias: this.getCriterias(component)
		};
		this._invoke(
			component,
			'c.countArticles',
			params,
			onSuccess,
			onError
		);
	},

	fetchRecords: function(component, onSuccess, onError) {
		var self = this;
		var sortBy = component.get("v.sortBy");
		if (sortBy) {
			sortBy = sortBy.replace('Link', '');
		}
		var offset = component.get("v.offset");
		console.log('fetchRecords -> offset: '+ offset);
		var params = {
			criterias: this.getCriterias(component),
			pageSize: component.get("v.pageSize"),
			offset: offset,
			sortBy: sortBy,
			isDescending: (component.get("v.sortDirection") == this.DESC_ORDER) 
		};
		this._invoke(
			component,
			'c.fetchArticles',
			params,
			function(result) {
				self.prepareRecords(component, result);
				onSuccess(result);
			},
			onError
		);
	},

	prepareRecords: function(component, records) {
		if (records && records.length) {
			for(var i = 0; i < records.length; i++)
				records[i].TitleLink = 
					'/lightning/cmp/c__ArticleBuilder?c__recordId=' + records[i].Id;
		}
	},

	showErrorToast: function(msg) {
		this.showToast('error', 'Error', msg);
	},

	showSuccessfulToast: function(msg) {
		this.showToast('success', 'Successful', msg);
	},

	showToast: function(type, header, msg) {
		$A.get("e.force:showToast").setParams({
			 title: header,
			 type: type,
			 message: msg
		}).fire();
	},

	io : function(obj, objId) {
		var str = (objId) ? (objId + ' ->\n') : '';
		for (const prop in obj) {
			str += (prop + ': ' + obj[prop] + '\n');
		}
		console.log(str);
	},

	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.busy", false);
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}
})