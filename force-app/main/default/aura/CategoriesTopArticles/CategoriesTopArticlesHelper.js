/**
 * @File Name          : CategoriesTopArticlesHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2019, 3:48:02 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
({
	DELETE_ACTION : 1,
	MOVE_UP_ACTION : 2,
	MOVE_DOWN_ACTION : 3,
	ADD_ACTION : 4,

	getCategories: function(component) {
		var self = this;
		this._invoke(
			component,
			'c.getFaqCategoriesInfo',
			null,
			function(result) {
				component.set("v.maxArticles", result.maxArticles);
				component.set("v.restrictTopArticles", result.restrictTopArticles);
				component.set("v.commonDivision", result.commonDivision);
				component.set("v.divisions", result.divisions);
				if (result.divisions && result.divisions.length) {
					var divisionObj = result.divisions[result.selDivisionIndex];
					component.set("v.division", divisionObj.value);
					component.set("v.divisionName", divisionObj.label);
				}  
				self.updateCategoriesInfo(component, result.categories);
				component.set("v.loading", false);
				component.set("v.busy", false);
			}
		);
	},

	refreshCategories: function(component) {
		console.log('refreshCategories');
		var self = this;
		var params = {
			division : component.get("v.division")
		};
		this._invoke(
			component,
			'c.getFaqCategories',
			params,
			function(categories) {
				self.updateCategoriesInfo(component, categories);
				component.set("v.loading", false);
				component.set("v.busy", false);
				component.set("v.visible", true);
				if (categories && categories.length) {
					component.find("accordion").set('v.activeSectionName', 'c0');
					self.checkSelCategoryIndex(component);
				}
			}
		);
	},

	updateCategoriesInfo: function(component, categories) {
		component.set("v.categories", categories);
		if (categories && categories.length) {
			component.set("v.selCategoryIndex", 0);
			component.set("v.selCategoryName", categories[0].name);
			var selCategoryMaxArticles = component.get("v.maxArticles") - categories[0].topArticles.length;
			component.set("v.selCategoryMaxArticles", selCategoryMaxArticles);
		}
	},

	checkSelCategoryIndex: function(component) {
		var activeSectionName = component.find("accordion").get('v.activeSectionName');
		var selCategoryIndex = component.get("v.selCategoryIndex");
		console.log('checkSelCategoryIndex -> activeSectionName: ' + activeSectionName + ', selCategoryIndex: ' + selCategoryIndex);
		var activeCategoryIndex = parseInt(activeSectionName.substring(1));
		if (activeCategoryIndex != selCategoryIndex) {
			component.set("v.selCategoryIndex", activeCategoryIndex);
			var selCat = component.get("v.categories")[activeCategoryIndex];
			component.set("v.selCategoryName", selCat.name);
			var selCategoryMaxArticles = component.get("v.maxArticles") - selCat.topArticles.length;
			component.set("v.selCategoryMaxArticles", selCategoryMaxArticles);
			//console.log('selCategoryIndex: ' + activeCategoryIndex + ', selCategoryName: ' + selCat.name + ', selCategoryMaxArticles: ' + selCategoryMaxArticles);
		}
	},

	prepareToAddArticle: function(component, index) {
		//prepare selector division
		var selectorDivision = component.get("v.division");
		var commonDivision = component.get("v.commonDivision");
		if (commonDivision && (commonDivision != '')) {
			selectorDivision += (',' + commonDivision);
		}
		component.set("v.selectorDivision", selectorDivision);
		//...
		var selCategoryIndex = component.get("v.selCategoryIndex");
		var selCat = component.get("v.categories")[selCategoryIndex];
		var selCategoryArtIds = '';
		if (selCat.topArticles.length) {
			for(var i = 0; i < selCat.topArticles.length; i++) {
				if (i > 0) {
					selCategoryArtIds += ',';	
				}
				selCategoryArtIds += selCat.topArticles[i].artId;
			}
		}
		component.set("v.selCategoryArtIds", selCategoryArtIds);
		//console.log('prepareToAddArticle -> selCategoryArtIds: ' + selCategoryArtIds);
	},

	changeArticleOrder: function(component, index, moveUp) {
		//console.log('CategoriesTopArticlesHelper -> changeArticleOrder');
		var self = this;
		var selCategoryIndex = component.get("v.selCategoryIndex");
		var selCat = component.get("v.categories")[selCategoryIndex];
		//console.log('selCategoryIndex: ' + selCategoryIndex + ', selCat: ' + selCat);
		//console.log('selCat.name: ' + selCat.name + ', selCat.topArticles.length: ' + selCat.topArticles.length);
		var topArt = selCat.topArticles[index];
		var secundaryIndex = moveUp ? (index - 1) : (index + 1);
		//console.log('moveUp: ' + moveUp + ', index: ' + index + ', secundaryIndex: ' + secundaryIndex);
		if ((secundaryIndex < 0) || (secundaryIndex >= selCat.topArticles.length)) {
			console.error('Invalid secundaryIndex: ' + secundaryIndex + ' (selCat.topArticles.length: ' +  selCat.topArticles.length + ')');
			return;
		}
		var topArt1Id = topArt.topArtId;
		var topArt2Id = selCat.topArticles[secundaryIndex].topArtId;
		//console.log('topArtId: ' + topArt.topArtId);
		var params = {
			topArt1Id : topArt1Id,
			topArt2Id : topArt2Id
		};
		component.set("v.busy", true);
		this._invoke(
			component,
			'c.changeArticlesOrder',
			params,
			function(result) {
				//console.log('changeArticleOrder -> result: ' + result);
				if (!result) {
					component.set("v.busy", false);
					return;
				}
				//else...
				//update orders
				var tempOrder = selCat.topArticles[index].order;
				selCat.topArticles[index].order = selCat.topArticles[secundaryIndex].order;
				selCat.topArticles[secundaryIndex].order = tempOrder;
				//swap items
				var temp = selCat.topArticles[index];
				selCat.topArticles[index] = selCat.topArticles[secundaryIndex];
				selCat.topArticles[secundaryIndex] = temp;
				var detailCmp = self.getDetailCmp(component);
				if (detailCmp) {
					//console.log('update cmp articles');
					detailCmp.set("v.articles", selCat.topArticles);
				}
				component.set("v.busy", false);
			}
		);
	},

	
	deleteArticle: function(component, index) {
		//console.log('CategoriesTopArticlesHelper -> deleteArticle');
		var self = this;
		component.set("v.busy", true);
		var selCategoryIndex = component.get("v.selCategoryIndex");
		var selCat = component.get("v.categories")[selCategoryIndex];
		//console.log('selCat: ' + selCat);
		//console.log('selCat.topArticles: ' + selCat.topArticles);
		var topArt = selCat.topArticles[index];
		console.log('topArtId: ' + topArt.topArtId);
		var params = {
			category : selCat.name,
			division : component.get("v.division"),
			topArtId : topArt.topArtId
		};
		this._invoke(
			component,
			'c.deleteArticle',
			params,
			function(result) {
				//remove item
				selCat.topArticles.splice(index, 1);
				//update items orders below
				for(var i = index; i < selCat.topArticles.length; i++) {
					selCat.topArticles[i].order--;
				}
				var detailCmp = self.getDetailCmp(component);
				component.set("v.selCategoryMaxArticles", component.get("v.selCategoryMaxArticles") + 1);
				// if (detailCmp) {
				// 	console.log('update cmp articles');
				// 	detailCmp.set("v.articles", selCat.topArticles);
				// }
				self.refreshChildCmpArticles(component, selCat.topArticles);
				component.set("v.busy", false);
			}
		);
	},

	addArticles: function(component, selection) {
		//console.log('CategoriesTopArticlesHelper -> addArticles');
		var self = this;
		component.set("v.busy", true);
		var selCategoryIndex = component.get("v.selCategoryIndex");
		var selCat = component.get("v.categories")[selCategoryIndex];
		var currentArtList = selCat.topArticles || [];
		var lastOrder = (currentArtList.length) ? currentArtList[currentArtList.length - 1].order : 0;
		//...
		var newArticles = [];
		for(var i = 0; i < selection.length; i++) {
			newArticles.push({
				knoledgeArticleId : selection[i].knoledgeArticleId,
				order : ++lastOrder
			});
		}
		var params = {
			category : selCat.name,
			division : component.get("v.division"),
			newArticles : newArticles
		};
		this._invoke(
			component,
			'c.addArticles',
			params,
			function(result) {
				console.log('result: ' + result);
				if ((!result) || (!result.length)) {
					console.log('invalid result');
					component.set("v.busy", false);
					return;
				}
				//else...
				if (result.length != selection.length) {
					console.log('result.length != selection.length');
					component.set("v.busy", false);
					return;
				}
				//else...
				for(var j = 0; j < result.length; j++) {
					currentArtList.push({
						topArtId : result[j],
						artId : selection[j].artId,
						title : selection[j].title,
						order : newArticles[j].order
					});
				}
				selCat.topArticles = currentArtList;
				var selCategoryMaxArticles = component.get("v.maxArticles") - selCat.topArticles.length;
				component.set("v.selCategoryMaxArticles", selCategoryMaxArticles);
				self.refreshChildCmpArticles(component, currentArtList);
				component.set("v.busy", false);
			}
		);
	},

	refreshChildCmpArticles: function(component, articles) {
		var detailCmp = this.getDetailCmp(component);
		if (detailCmp) {
			console.log('update cmp articles');
			//detailCmp.set("v.articles", []);
			//detailCmp.set("v.visible", false);
			detailCmp.set("v.articles", articles);
			//detailCmp.set("v.visible", true);
		}
	},

	getDetailCmp: function(component) {
		var selCategoryIndex = component.get("v.selCategoryIndex");
		var cmpList = component.find("catArtDetail");
		var validCmpList = [].concat(cmpList);
		for(var i = 0; i < validCmpList.length; i++) {
			var cmpCatIndex = validCmpList[i].get("v.categoryIndex");
			if (selCategoryIndex == cmpCatIndex) {
				return validCmpList[i];
			}
		}
		//else...
		return null;
	},

	io : function(obj, objId) {
		var str = (objId) ? (objId + ' ->\n') : '';
		for (const prop in obj) {
			str += (prop + ': ' + obj[prop] + '\n');
		}
		console.log(str);
	},

	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.busy", false);
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}
})