/**
 * @File Name          : CategoriesTopArticlesController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/17/2019, 10:20:46 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
({
	doInit : function(component, event, helper) {
		console.log('CategoriesTopArticlesController v5');
		helper.getCategories(component);
	},

	selectorClosed : function(component, event, helper) {
		//console.log('selectorClosed');
		event.stopPropagation();
		component.set("v.showSelector", false); 
		var selection = event.getParam('selection');
		//console.log('selection: ' + selection);
		if (selection && selection.length) {
			helper.addArticles(component, selection);
		}
	},

	onArticleAction : function(component, event, helper) {
		//console.log('onArticleAction');
		event.stopPropagation();
		var action = event.getParam('action');
		var index = event.getParam('index');
		//console.log('action: ' + action + ', index: ' + index);
		helper.checkSelCategoryIndex(component);
		if (action == helper.DELETE_ACTION) {
			helper.deleteArticle(component, index);
		} else if (action == helper.MOVE_UP_ACTION) {
			helper.changeArticleOrder(component, index, true);
		} else if (action == helper.MOVE_DOWN_ACTION) {
			helper.changeArticleOrder(component, index, false);
		} else if (action == helper.ADD_ACTION) {
			helper.prepareToAddArticle(component);
			component.set("v.showSelector", true);
		}
	},

	onDivisionChange : function(component, event, helper) {
		console.log('onDivisionChange');
		var newDivision = event.getParam("value");
		var divisionLabel = '';
		var divisions = component.get("v.divisions");
		for(var i = 0; i < divisions.length; i++) {
			if (divisions[i].value == newDivision) {
				divisionLabel = divisions[i].label;
				break;
			}
		}
		component.set("v.divisionName", divisionLabel);
		//component.set("v.categories", []);
		component.set("v.visible", false);
		component.set("v.busy", true);
		component.set("v.loading", true);
		helper.refreshCategories(component);
	}

})