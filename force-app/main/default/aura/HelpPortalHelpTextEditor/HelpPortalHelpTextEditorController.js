/** 
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 4/30/2019
 */
({
	doInit: function(component, event, helper) {
		component.set("v.selectedLanguage", "en_US");
        helper.getDivisions(
			component,
			function(result) {
				component.set("v.division", result);
			});
	},
	handleActive: function(component, event, helper) {
		component.set("v.selectedLanguage", event.getSource().get("v.id"));
	}
})