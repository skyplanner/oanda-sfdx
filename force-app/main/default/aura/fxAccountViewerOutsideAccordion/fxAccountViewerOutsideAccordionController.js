({
	handleLoadRecord: function (cmp, event) {
		var recordId = event.getParam("recordId");
		// set the handler attributes based on event data
		cmp.set("v.showFxAccount", recordId !== undefined);
	}
});