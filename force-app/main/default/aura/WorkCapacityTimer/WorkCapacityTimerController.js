/**
 * @File Name          : WorkCapacityTimerController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/23/2021, 10:58:43 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/10/2021, 12:45:17 AM   acantero     Initial Version
**/
({

    doInit : function(cmp, event, helper) {
        console.log('WorkCapacityTimer v 1.31');
        const timeFrameInSeconds = cmp.get('v.timeFrameInSeconds');
        let warningFirstValue = timeFrameInSeconds / 2;
        let alertFirstValue = warningFirstValue / 2;
        cmp.set('v.warningFirstValue', warningFirstValue);
        cmp.set('v.alertFirstValue', alertFirstValue);
        helper.initTimer(cmp, helper);
    },

    freeCapacity : function(cmp, event, helper) {
        helper.fireFreeCapacityEvent(cmp, helper);
    },

    stop : function(cmp, event, helper) {
        const interval = cmp.get('v.interval');
        if (interval) {
            clearInterval(interval);
            cmp.set('v.interval', null);
        }
    }

})