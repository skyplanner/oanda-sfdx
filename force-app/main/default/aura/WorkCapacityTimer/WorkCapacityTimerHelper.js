/**
 * @File Name          : WorkCapacityTimerHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/23/2021, 10:58:10 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/10/2021, 12:59:22 AM   acantero     Initial Version
**/
({

    INITIAL_COLOR : '#4bca81',
    WARNING_COLOR : '#ffb75d',
    ALERT_COLOR : '#d4504c',

    initTimer : function(cmp, helper) {
        const timeFrameInSeconds = cmp.get('v.timeFrameInSeconds');
        cmp.set('v.counter', timeFrameInSeconds);
        cmp.set('v.counterColor', helper.INITIAL_COLOR);
        cmp.set('v.ready', true);
        const stepInSeconds = cmp.get('v.stepInSeconds'); 
        const stepInMilliseconds = stepInSeconds * 1000; 
        const interval = setInterval(
            $A.getCallback(function() {
                let counter = cmp.get('v.counter');
                counter--;
                const warningFirstValue = cmp.get('v.warningFirstValue');
                const alertFirstValue = cmp.get('v.alertFirstValue');
                if (counter < alertFirstValue) {
                    cmp.set("v.counterColor", helper.ALERT_COLOR);
                } else if (counter < warningFirstValue) {
                    cmp.set("v.counterColor", helper.WARNING_COLOR);
                }
                cmp.set("v.counter", counter);
                if (counter == 0) {
                    const interval = cmp.get('v.interval');
                    if (interval) {
                        clearInterval(interval);
                        helper.fireFreeCapacityEvent(cmp, helper);
                        cmp.set('v.interval', null);
                    }
                }
            }), 
            stepInMilliseconds
        );
        cmp.set('v.interval', interval);
    },

    fireFreeCapacityEvent : function(cmp, helper) {
        const freeCapacityEvt = cmp.getEvent("freeCapacity");
        freeCapacityEvt.fire();
    }

})