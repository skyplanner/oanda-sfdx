({
    connectCometd : function(component) {
        var helper = this;
        
        // Configure CometD
        var cometdUrl = window.location.protocol+'//'+window.location.hostname+'/cometd/39.0/';
        var cometd = component.get('v.cometd');
        cometd.configure({
            url: cometdUrl,
            requestHeaders: { Authorization: 'OAuth '+ component.get('v.sessionId')},
            appendMessageTypeToURL : false
        });
        cometd.websocketEnabled = false;
        
        // Establish CometD connection
        console.log('Connecting to CometD: '+ cometdUrl);
        cometd.handshake(function(handshakeReply) {
            if (handshakeReply.successful) {
                console.log('Connected to CometD.');
                // Subscribe to platform event
                var newSubscription = cometd.subscribe(
                    '/event/Notification__e',
                    function(platformEvent) {
                        console.log('Platform event received: '+ JSON.stringify(platformEvent));
                        helper.onReceiveNotification(component, platformEvent);
                    }
                );
                
                // Save subscription for later
                var subscriptions = component.get('v.cometdSubscriptions');
                subscriptions.push(newSubscription);
                component.set('v.cometdSubscriptions', subscriptions);
            }
            else
                console.error('Failed to connected to CometD.');
        });
    },
    
    disconnectCometd : function(component) {
        var cometd = component.get('v.cometd');
        
        // Unsuscribe all CometD subscriptions
        cometd.batch(function() {
            var subscriptions = component.get('v.cometdSubscriptions');
            subscriptions.forEach(function (subscription) {
                cometd.unsubscribe(subscription);
            });
        });
        component.set('v.cometdSubscriptions', []);
        
        // Disconnect CometD
        cometd.disconnect();
        console.log('CometD disconnected.');
    },
    
    onReceiveNotification : function(component, platformEvent) {
        // TODO: Genericize type keywords so it is not specific to OPA. This requires doing work in CDA.
        const TYPE_PASSWORD_RESET = 'password-reset';
        
        var payload = platformEvent.data.payload;
        console.log(component.get('v.sessionId'));
        console.log(payload.Session_Id__c);
        
        if (component.get('v.fxAccount.fxTrade_User_Id__c') == payload.fxTrade_User_ID__c
            && payload.Type__c == TYPE_PASSWORD_RESET
           	&& component.get('v.userId') == payload.Created_By_Id__c
            && component.get('v.sessionId') == payload.Session_Id__c
           ) {
            component.set('v.message', 'Please send this password reset token to the user: ');
            component.set('v.token', payload.Content__c);
            
            // Create a new closed case
            var actionCreateCase = component.get('c.createCase');
            actionCreateCase.setParams({
                'fxaId' : component.get('v.recordId'),
                'token' : payload.Content__c
            });
            
            actionCreateCase.setCallback(this, function(resp) {
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    console.log(resp.getReturnValue());
                } else {
                    console.log(resp.getError());
                }
            });
            
            $A.enqueueAction(actionCreateCase);
        }
    },
})