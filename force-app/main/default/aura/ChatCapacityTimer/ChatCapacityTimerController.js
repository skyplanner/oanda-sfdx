/**
 * @File Name          : ChatCapacityTimerController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/18/2021, 1:03:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/10/2021, 1:48:50 PM   acantero     Initial Version
**/
({
    
    doInit : function(cmp, event, helper) {
        console.log('ChatCapacityTimer v 1.29');
        helper
            .getWrapUpTime(cmp, helper)
            .then(function(result) {
                console.log('doInit -> result: ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },

    onLogout : function(cmp, event, helper) {
        console.log('ChatCapacityTimer -> onLogout');
        const timerVisible = cmp.get('v.timerVisible');
        if (timerVisible == true) {
            console.log('then stop timer...');
            const workCapacityTimer = cmp.find("workCapacityTimer");
            workCapacityTimer.stop();
            cmp.set('v.timerVisible', false);
        }
    },

    onFreeCapacity : function(cmp, event, helper) {
        console.log('ChatCapacityTimer -> onFreeCapacity');
        helper
            .freeCapacity(cmp, helper)
            .then(function(result) {
                console.log('onFreeCapacity -> result: ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },

    onChatEnded: function(cmp, event, helper) {
		console.log('ChatCapacityTimer.onChatEnded');
		var recordId = cmp.get("v.recordId");
		var evtRecordId = event.getParam("recordId");
		console.log('onChatEnded, recordId: ' + recordId + ', evtRecordId: ' + evtRecordId);
		if (!helper.compareIds(evtRecordId, recordId)) {
			console.log('is not my problem, so... exit');
			return;
		}
		//else...
        console.log('is mi problem then getCurrentWorkId...');
        helper
            .processChatEnded(cmp, helper)
            .then(function(result) {
                console.log('onChatEnded -> result: ' + result);
            }).catch(function(error) {
                console.error(error);
            });
    },
    
    onCaseClosed: function(cmp, event, helper) {
        console.log('ChatCapacityTimer -> onCaseClosed');
        var timerVisible = cmp.get("v.timerVisible");
        var chatRecord = cmp.get("v.chatRecord");
        var caseId = event.getParam('caseId');
        var releaseCapacity = event.getParam('releaseCapacity');
        console.log('onCaseClosed -> caseId: ' + caseId + ', releaseCapacityParam: ' + releaseCapacity);
        if ((timerVisible == true) && 
            chatRecord && 
            (chatRecord.CaseId == caseId) && 
            (releaseCapacity == true)
        ) {
            console.log('then freeCapacity...');
            helper
                .freeCapacity(cmp, helper)
                .then(function(result) {
                    console.log('onCaseClosed -> result: ' + result);
                }).catch(function(error) {
                    console.error(error);
                });
        }
	}

})