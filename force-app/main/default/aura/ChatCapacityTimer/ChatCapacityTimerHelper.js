/**
 * @File Name          : ChatCapacityTimerHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/18/2021, 12:58:13 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/10/2021, 2:37:25 PM   acantero     Initial Version
**/
({

    getWrapUpTime : function(cmp, helper) {
        return helper.runServerAction(cmp, helper, 'getWrapUpTime', null)
            .then(function(result) {
                if (result || (result == 0)) {
                    cmp.set('v.timeFrameInSeconds', result);
                    return 'getWrapUpTime -> result: ' + result;
                } 
                //else...
                var noResultMsg = 'getWrapUpTime -> result is null';
                console.error(noResultMsg);
                return noResultMsg;
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('getWrapUpTime -> ' + JSON.stringify(error));
                }
                return 'getWrapUpTime -> failed';
            });
    },

    processChatEnded : function(cmp, helper) {
        return helper.getCurrentWorkId(cmp, helper)
            .then(function(workId) {
                if (workId) {
                    console.log('getCurrentWorkId -> result: ' + workId);
                    cmp.set('v.workId', workId);
                    var timeFrameInSeconds = cmp.get('v.timeFrameInSeconds');
                    if (timeFrameInSeconds == 0) {
                        console.log('timeFrameInSeconds is 0, then freeCapacity right now...');
                        return helper.freeCapacity(cmp, helper);
                    }
                    //else...
                    return helper.beginChatClosing(cmp, helper, workId)
                        .then(function(beginChatClosingResult) {
                            console.log('chat timer activated, workId: ' + workId);
                            cmp.set('v.timerVisible', true);
                            return beginChatClosingResult;
                        });
                } 
                //else...
                return 'workId not found';
            });
    },

    beginChatClosing : function(cmp, helper, workId) {
        var chatId = cmp.get("v.recordId");
        var params = {
            chatId,
            workId
        };
        return helper.runServerAction(cmp, helper, 'beginChatClosing', params)
            .then(function(result) {
                if (result) {
                    cmp.set('v.closingObjId', result);
                    return 'beginChatClosing -> result: ' + result;
                } 
                //else...
                var errorMsg = 'beginChatClosing -> result is null';
                console.error(errorMsg);
                return errorMsg;
                //...
            }).catch(function(error) {
                if (error) {
                    console.error('beginChatClosing -> error: ' + JSON.stringify(error));
                }
                return 'beginChatClosing -> failed';
            });
    },

    endWorkClosing : function(cmp, helper) {
        var closingObjId = cmp.get("v.closingObjId");
        if (!closingObjId) {
            return Promise.resolve('endWorkClosing -> closingObjId is null');
        }
        //else...
        var params = {
            closingObjId
        };
        return helper.runServerAction(cmp, helper, 'endWorkClosing', params)
            .then(function(result) {
                return 'endWorkClosing -> ok';
            }).catch(function(error) {
                if (error) {
                    console.error('endWorkClosing -> ' + JSON.stringify(error));
                }
                return 'endWorkClosing -> failed';
            });
    },

    getCurrentWorkId : function(cmp, helper) {
        console.log('getCurrentWorkId -> begin');
        var recordId = cmp.get("v.recordId");
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI.getAgentWorks()
            .then(function(getAgentWorksResult) {
                var msg;
                var works = JSON.parse(getAgentWorksResult.works);
                for(var work of works) {
                    console.log('workId: ' + work.workId);
                    if (helper.compareIds(recordId, work.workItemId)) {
                        return work.workId;
                    }
                }
                //else...
                return null;
            });
    },

    freeCapacity : function(cmp, helper) {
        console.log('ChatCapacityTimer -> freeCapacity');
        cmp.set('v.timerVisible', false);
        //...
        return helper
            .closeAgentWork(cmp, helper)
            .then(function(closeResult) {
                if (closeResult == true) {
                    return helper.endWorkClosing(cmp, helper);
                }
                //else...
                return 'closeAgentWork failed, then workClosing persist';
            });
    },

    closeAgentWork : function(cmp, helper) {
        var workId = cmp.get("v.workId");
        var omniAPI = cmp.find("omniToolkit");
        return omniAPI
            .closeAgentWork({ workId })
            .then(function(closeResult) {
                if (closeResult) {
                    console.log('Work Item is closed successfully -> ' + closeResult);
                    return true;
                } 
                //else...
                console.log('Close work failed');
                return false;
            });
    },

    compareIds : function(id1, id2) {
		if ((!id1) || (!id2)) {
			return false;
		}
		//else...
		if (id1.length == id2.length) {
			return (id1 == id2);
		}
		//else...
		if (id1.length < id2.length) {
			return (id2.indexOf(id1) == 0);
		}
		//else...
		return (id1.indexOf(id2) == 0);
	},

    runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }

})