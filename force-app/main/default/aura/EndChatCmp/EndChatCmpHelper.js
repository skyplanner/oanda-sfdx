/**
 * @File Name          : EndChatCmpHelper.js
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/9/2019, 12:10:23 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    ....................   dmorales     Initial Version
 * 1.0    8/8/2019, 9:49:59 PM   acantero     Open a sub tab to close clase
**/
({
	getCaseId : function (cmp, chatTranscriptId) {
        console.log('EndChatCmpHelper.getCaseId');
		this._invoke(
			cmp,
			'c.getCaseIdFromChatTranscript',
			{chatTranscriptId : chatTranscriptId},
			function(caseId) {
                console.log('caseId: ' + caseId);
                if (caseId) {
                    cmp.set("v.caseId", caseId);
                } else {
                    console.error('caseId is null or undefined');
                }
			}
		);
    },

	compareIds : function(id1, id2) {
		if ((!id1) || (!id2)) {
			return false;
		}
		//else...
		if (id1.length == id2.length) {
			return (id1 == id2);
		}
		//else...
		if (id1.length < id2.length) {
			return (id2.indexOf(id1) == 0);
		}
		//else...
		return (id1.indexOf(id2) == 0);
	},

	io : function(obj, objId) {
        var str = (objId) ? (objId + ' ->\n') : '';
        for (const prop in obj) {
            str += (prop + ': ' + obj[prop] + '\n');
        }
        console.log(str);
	},
	
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.busy", false);
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}

})