/**
 * @File Name          : EndChatCmpController.js
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/19/2021, 9:02:09 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    ....................   dmorales     Initial Version
 * 2.0    8/7/2019, 5:05:42 PM   acantero     Open a sub tab to close clase
**/
({
	init: function(cmp, evt, helper) {
		console.log('EndChatCmpController v 1.24');
	},

	onChatEnded: function(cmp, evt, helper) {
		console.log('EndChatCmpController.onChatEnded');
		var recordId = cmp.get("v.recordId");
		var evtRecordId = evt.getParam("recordId");
		console.log('onChatEnded, recordId: ' + recordId + ', evtRecordId: ' + evtRecordId);
		if (!helper.compareIds(evtRecordId, recordId)) {
			console.log('is not my problem, so... exit');
			return;
		}
		//else...
		console.log('is mi problem then find the caseId...');
		helper.getCaseId(cmp, recordId);
	},

	invokeCloseCase: function(cmp, evt, helper) {
		console.log('EndChatCmpController.invokeCloseCase');
		var caseId = cmp.get("v.caseId");
		console.log('new caseId: ' + caseId);
		var recordId = cmp.get("v.recordId");
		//var evtRecordId = evt.getParam("recordId");
		var workspaceAPI = cmp.find("workspace");
		workspaceAPI.getAllTabInfo()
		.then(function(tabInfoList) {
			if ((!tabInfoList) || (!tabInfoList.length)) {
				console.log('tabInfoList is null or empty');
				return 'tabInfoList is null or empty';
			}
			//else...
			var tabInfoFound = false;
			var tabInfo = null;
			for(var i = 0; i < tabInfoList.length; i++) {
				var tabInfo = tabInfoList[i];
				if (tabInfo.recordId && helper.compareIds(recordId, tabInfo.recordId)) {
					console.log('tab found, index: ' + i);
					console.log('tabInfo.recordId: ' + tabInfo.recordId);
					console.log('tabInfo.tabId: ' + tabInfo.tabId);
					tabInfoFound = true;
					break;
				}
			}
			if (!tabInfoFound) {
				return 'tab with recordId = ' + recordId + ' not found';
			}
			//else...
			console.log('tabInfoFound -> will openSubtab');
			var url = "/lightning/cmp/c__CloseChatCase?c__chatTranscriptId=" + recordId + "&c__caseId=" + caseId;
			console.log('new subtab url: ' + url);
			return workspaceAPI.openSubtab({
				parentTabId : tabInfo.tabId,
				pageReference : null,
				recordId : null,
				url : url,
				focus : false
			}).then(function(subTabId){
				console.log('acl sub tab opened');
				console.log('subtab Id: ' + subTabId);
				return workspaceAPI.setTabLabel({
					tabId: subTabId,
					label: $A.get("$Label.c.Close_Case")
				});
			}).then(function(tabInfo){
				return workspaceAPI.setTabHighlighted({
					tabId : tabInfo.tabId,
					highlighted : true,
					options : {
						pulse: true,
						state: "success"
					}
				});
			}).then(function(tabInfo){
				return 'tab actions ok';
			});
		}).then(function(result) {
			console.log('final result: ' + result);
		}).catch(function(error) {
			console.error(error);
		});      
	},  
})