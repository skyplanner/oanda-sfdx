({
    doInit : function(component, event, helper) {
        
        var actionFxAcct = component.get("c.getFxAccount");
        var actionGetAssessments = component.get("c.getAssessmentQuestions");
        var actionGetAcceptRejectReason = component.get("c.getAcceptRejectReason");
        var actionGetKnowledgeResult = component.get("c.getKnowledgeResult");
        
        console.log("fxAccount Id : " + component.get("v.recordId"));
        
        if(component.get("v.recordId")){
            
            actionFxAcct.setParams({ "fxaId" : component.get("v.recordId") });
            actionFxAcct.setCallback(this,function(resp){
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    
                    component.set("v.fxAccount",resp.getReturnValue());
                    
                }else{
                    console.log(resp.getError());
                }
            });
            
            actionGetAssessments.setParams({ "fxaId" : component.get("v.recordId") });
            actionGetAssessments.setCallback(this,function(resp){
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    
                    component.set("v.assessments",resp.getReturnValue());
                    component.set("v.noOfQuestions", resp.getReturnValue().length);
                    
                }else{
                    console.log(resp.getError());
                }
            });
            
            actionGetAcceptRejectReason.setParams({ "fxaId" : component.get("v.recordId") });
            actionGetAcceptRejectReason.setCallback(this,function(resp){
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    
                    component.set("v.acceptRejectReason",resp.getReturnValue());
                    
                }else{
                    console.log(resp.getError());
                }
            });
            
            actionGetKnowledgeResult.setParams({ "fxaId" : component.get("v.recordId") });
            actionGetKnowledgeResult.setCallback(this,function(resp){
                var state = resp.getState();
                
                if(component.isValid() && state === 'SUCCESS'){
                    
                    component.set("v.knowledgeResult",resp.getReturnValue());
                    
                }else{
                    console.log(resp.getError());
                }
            });
            
            $A.enqueueAction(actionFxAcct);
            $A.enqueueAction(actionGetAssessments);
            $A.enqueueAction(actionGetAcceptRejectReason);
            $A.enqueueAction(actionGetKnowledgeResult);
        }
    },
    
    clickAccept : function(component, event, helper) {
        var reason = component.find("reason");
        var characterCount = reason.get("v.value").length;
        
        if(characterCount > 255) {
            alert("Accept/Reject Reason must not exceed 255 characters!");
        } else if (reason.get("v.value")) {
            component.set("v.fxAccount.Knowledge_Result__c", "Pass");
            component.set("v.fxAccount.CKA_Reason_Override__c", reason.get("v.value"));
            
            helper.updateFxAccount(component, component.get("v.fxAccount"));
            component.set("v.knowledgeResult", component.get("v.fxAccount.Knowledge_Result__c"));
        } else {
            alert("Missing a reason for accepting this customer!");
        }
    },
    
    clickReject : function(component, event, helper) {
        var reason = component.find("reason");
        var characterCount = reason.get("v.value").length;

        if(characterCount > 255) {
            alert("Accept/Reject Reason must not exceed 255 characters!");
        } else if(reason.get("v.value")) {
            component.set("v.fxAccount.Knowledge_Result__c", "Fail");
            component.set("v.fxAccount.CKA_Reason_Override__c", reason.get("v.value"));
            
            helper.updateFxAccount(component, component.get("v.fxAccount"));
            component.set("v.knowledgeResult", component.get("v.fxAccount.Knowledge_Result__c"));
        } else {
            alert("Missing a reason for rejecting this customer!");
        }

    }
})