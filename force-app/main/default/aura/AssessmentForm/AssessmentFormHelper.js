({
    updateFxAccount : function(component, fxa) {
        var actionupdateFxAccount = component.get("c.updateFxAccount");
        
        actionupdateFxAccount.setParams({ "fxa" : fxa });
        actionupdateFxAccount.setCallback(this,function(resp) {
            
            var state = resp.getState();
            
            if(component.isValid() && state === 'SUCCESS') {
                component.set("v.fxAccount",resp.getReturnValue());
            } else {
                console.log(resp.getError());
            }
        });
        
        $A.enqueueAction(actionupdateFxAccount);
    }
})