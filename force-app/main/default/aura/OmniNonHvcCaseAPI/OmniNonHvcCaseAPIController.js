/**
 * @File Name          : OmniNonHvcCaseAPIController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/30/2022, 11:06:21 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/19/2021, 3:14:48 PM   acantero     Initial Version
**/
({
    init: function(cmp, event, helper) { 
        console.log("OmniNonHvcCasesAPI 1.10");
        var isActive = cmp.get("v.isActive");
        console.log('OmniNonHvcCasesAPI isActive: ' + isActive);
    },
    
    parkNonHvcCase : function(cmp, event, helper) {
        console.log("parkNonHvcCase -> begin");
        var params = event.getParam('arguments');
        if (!params) {
            console.log("parkNonHvcCase -> arguments is null");
            return;
        }
        //else...
        var nonHvcCaseId = params.nonHvcCaseId;
        console.log("parkNonHvcCase -> nonHvcCaseId: " + nonHvcCaseId);
        cmp.set("v.processing", true);
        var service = cmp.find('nonHvcCaseService');
        service.set('v.recordId', nonHvcCaseId);
        service.set('v.mode', 'EDIT');
        service.reloadRecord();
    },

    onNonHVCRecordUpdated : function(cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            console.error('OmniNonHvcCaseAPI -> onNonHVCRecordUpdated -> ERROR');
            console.log('Error: ' + cmp.get("v.recordError"));
            helper.endProcessing(cmp, helper, false);
            return;
        }  
        //else...
        if (changeType === "REMOVED"){
            console.log('OmniNonHvcCaseAPI -> onNonHVCRecordUpdated -> REMOVED');
            helper.endProcessing(cmp, helper, false);
            return;
        }  
        //else...
        if (changeType === "CHANGED") {
            console.log('OmniNonHvcCaseAPI -> onNonHVCRecordUpdated -> CHANGED');
            return;
        } 
        //else...
        if (changeType === "LOADED") {
            console.log('OmniNonHvcCaseAPI -> onNonHVCRecordUpdated -> LOADED');
            cmp.set('v.nonHvcCaseFields.Status__c', 'Parked');
            cmp.find("nonHvcCaseService").saveRecord(
                function(saveResult) {
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                        console.log('nonHvcCaseService -> record is saved successfully');
                        helper.endProcessing(cmp, helper, true);
                        return;
                        //...
                    } else if (saveResult.state === "INCOMPLETE") {
                        // handle the incomplete state
                        console.log("nonHvcCaseService -> saveResult = INCOMPLETE");
                        //...
                    } else if (saveResult.state === "ERROR") {
                        // handle the error state
                        console.error(
                            'nonHvcCaseService -> saveResult = ERROR, error: ' +
                            JSON.stringify(saveResult.error)
                        );
                    } else {
                        console.log(
                            'nonHvcCaseService -> Unknown problem, state: ' + saveResult.state + 
                            ', error: ' + JSON.stringify(saveResult.error)
                        );
                    }
                    helper.endProcessing(cmp, helper, false);
                }
            );
        } 
    }
    
})