/**
 * @File Name          : OmniNonHvcCaseAPIHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/30/2022, 11:00:59 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/19/2021, 4:28:53 PM   acantero     Initial Version
**/
({
	endProcessing : function(cmp, helper, success) {
		console.log("OmniNonHvcCaseAPI -> endProcessing, success: " + success);
		const processing = cmp.get("v.processing");
		if (processing == false) {
			return;
		}
		//else
		cmp.set("v.processing", false);
		const newEventName = (success)
			? 'nonHvcCaseParked'
			: 'nonHvcCaseParkingFail';
		const newEvent = cmp.getEvent(newEventName);
        newEvent.fire();
	},

    runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }
    
})