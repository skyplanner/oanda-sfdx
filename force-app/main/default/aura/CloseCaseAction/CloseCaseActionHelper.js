/**
 * @File Name          : CloseCaseActionHelper.js
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/4/2024, 12:52:59 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/5/2019, 9:32:01 AM   dmorales     Initial Version
 * 2.0    8/8/2019, 11:02:00 AM  acantero     Extract CloseCase comp
**/
({
	CASE_ORIGIN_CHAT : 'Chat',
	CASE_ORIGIN_CHATBOT_CASE : 'Chatbot - Case',
    MESSAGING_SESSION_PREFIX : '0Mw',

	checkCase: function(cmp, helper, isARefresh){
		console.log('CloseCaseAction -> checkCase, isARefresh: ' + isARefresh);
		var caseId =  cmp.get("v.recordId");
		let params = {
			caseId : caseId
		}
		return helper
			.runServerAction(cmp, helper, 'getCaseById', params)
			.then($A.getCallback(caseObj => {
				if (!caseObj) {
					console.error("CloseCaseAction -> getCaseById return null");
					return;
				}
				//else...
				console.log(
					"CloseCaseAction -> getCaseById, Is_HVC__c: " + 
					caseObj.Is_HVC__c
				);
                const caseSrcIsAMsgSession = 
                    helper.isAMessagingSession(cmp, helper, caseObj.SourceId);
                console.log('caseSrcIsAMsgSession: ' + caseSrcIsAMsgSession);
				const useCapacity = (
					(caseSrcIsAMsgSession == false) &&
                    (
                        (caseObj.Origin == helper.CASE_ORIGIN_CHAT) ||
                        (caseObj.Origin == helper.CASE_ORIGIN_CHATBOT_CASE)
                    ) 
				);
				console.log('useCapacity: ' + useCapacity);
				cmp.set("v.useCapacity", useCapacity);
				var oldValue = cmp.get("v.caseIsClosed");
				var closed = (caseObj.Status == 'Closed');
				cmp.set("v.caseIsClosed", closed);
				if (isARefresh) {
					if (closed) {
						if (oldValue != closed) {
							console.log('CloseCaseAction -> checkCase, detect case is closed, fire event');
							var closeClase = cmp.find("closeClase");
							closeClase.fireAnonymousCaseClosedEvt();
						}
					} else {
						console.log('will refresh closeClase because case is still open');
						var closeClase = cmp.find("closeClase");
						closeClase.refresh();
					}
				}
			})).catch($A.getCallback(error => {
				console.log('CloseCaseAction -> checkCase fail');
				console.error("checkCase -> error: " + error);
			}));
	},

    isAMessagingSession : function(cmp, helper, workItemId) {
        var result = false;
        if (workItemId) {
            result = workItemId.startsWith(helper.MESSAGING_SESSION_PREFIX);
        }
        return result;
    },

	closeCase : function (cmp, helper, freeCapacity) {
        cmp.set("v.busy", true);
        var closeClase = cmp.find("closeClase");
        if (!closeClase.submit(freeCapacity)) {
            cmp.set("v.busy", false);
        }
    },
	
	onCaseClosed : function (cmp) {
		console.log('CloseCaseActionHelper.onCaseClosed');
		cmp.set("v.busy", false);
		cmp.set("v.showModal", false);
		cmp.set("v.caseIsClosed", true);
	},

	io : function(obj, objId) {
        var str = (objId) ? (objId + ' ->\n') : '';
        for (const prop in obj) {
            str += (prop + ': ' + obj[prop] + '\n');
        }
        console.log(str);
    },

	runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }

	
})