/**
 * @File Name          : CloseCaseActionController.js
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/4/2024, 12:44:08 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/5/2019, 9:32:01 AM   dmorales     Initial Version
 * 2.0    8/8/2019, 11:02:00 AM  acantero     Extract CloseCase comp
**/
({
	doInit : function (component, event, helper) {
		console.log('CloseCaseAction 1.26');
		var recordId =  component.get("v.recordId");
		console.log('recordId: ' + recordId);
		if (recordId) {
			helper.checkCase(component, helper, false);
		}
	},

	handleCloseCaseClick : function (component, event, helper) {
		component.set("v.showModal", true); 
	},

	handleCloseModal : function (component, event, helper) {
		component.set("v.showModal", false); 	
	},

	handleCloseCase : function (component, event, helper) {
		helper.closeCase(component, helper, false);
	},

	handleCloseAndFree : function (component, event, helper) {
		helper.closeCase(component, helper, true);
	},

	forceRefreshViewHandler : function (component, event, helper) {
		console.log('CloseCaseActionController.forceRefreshViewHandler');
		var recordId =  component.get("v.recordId");
		console.log('recordId: ' + recordId);
		if (recordId) {
			helper.checkCase(component, helper, true);
		}
	},

	caseClosed : function (component, event, helper) {
		console.log('CloseCaseAction -> caseClosed');
        var caseIdParam = event.getParam('caseId');
        var caseId = component.get("v.recordId");
        console.log('caseIdParam: ' + caseIdParam + ', caseId: ' + caseId);
        if (caseIdParam == caseId) {
			var sourceParam = event.getParam('source');
			var customName = component.get("v.customName");
			console.log('sourceParam: ' + sourceParam + ', customName: ' + customName);
			console.log('caseIdParam and caseId match then execute onCaseClosed');
			helper.onCaseClosed(component);
            if (sourceParam == customName) {
				console.log('sourceParam and customName match then show msg');
                var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({"title": "Success!",
					"message": $A.get("$Label.c.CloseCaseSuccessMsg"),
					"type": "success"});
				toastEvent.fire();
            }
        }
    }
	 
})