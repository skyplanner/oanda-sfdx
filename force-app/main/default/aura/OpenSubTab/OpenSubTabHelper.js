/* eslint-disable no-console */
({
	CHAT_BOT_ORIGIN_EMAIL: "Chatbot - Email",
	CHAT_BOT_ORIGIN_CHATBOT: "Chatbot - Case",
	MESSAGING_SESSION_PREFIX: "0Mw",

	isCase: function (cmp) {
		var sObjectName = cmp.get("v.sObjectName");
		return sObjectName === "Case";
	},
	isNonHaveCase: function (cmp) {
		var sObjectName = cmp.get("v.sObjectName");
		return sObjectName === "Non_HVC_Case__c";
	},
	isSession: function (sourceId) {
		return sourceId.startsWith(this.MESSAGING_SESSION_PREFIX)
	},
	setFields: function (cmp) {

		var fields = cmp.get("v.fields");
		if (this.isCase(cmp)) {
			fields = ["Origin","SourceId"];
		} else if (this.isNonHaveCase(cmp)) {
			fields = ["Case__c"];
		}
		console.log("OpenSubTab -> setFields ->" + fields);
		cmp.set("v.fields", fields);
		cmp.set("v.loadRecord", true);
		cmp.set("v.isHvcPage", this.isCase(cmp));
	},
	getObjectRecordId: function (cmp) {
		var record = cmp.get("v.record");
		console.log("OpenSubTab -> getObjectRecordId ->" + JSON.stringify(record));
		var objectId;
		if (record) {
			if (this.isCase(cmp)) {
				var sourceId = record.SourceId;
				var origin = record.Origin;
				if (
					origin &&
					(origin === this.CHAT_BOT_ORIGIN_CHATBOT || origin === this.CHAT_BOT_ORIGIN_EMAIL) &&
					sourceId &&
					this.isSession(sourceId)
				) {
					objectId = sourceId;
				}
			} else if (this.isNonHaveCase(cmp)) {
				objectId = record.Case__c;
			}
		}
		return objectId;
	},
	getFields: function (cmp, changeType) {
		console.log("OpenSubTab -> getFields ->" + changeType);
		return changeType === "LOADED"
			? this.getObjectRecordId(cmp)
			: undefined;
	},
	compareIds: function (id1, id2) {
		if (!id1 || !id2) {
			return false;
		}
		//else...
		if (id1.length === id2.length) {
			return id1 === id2;
		}
		//else...
		if (id1.length < id2.length) {
			return id2.indexOf(id1) === 0;
		}
		//else...
		return id1.indexOf(id2) === 0;
	},
	openTab: function (cmp, openTabRecordId, helper) {
		var recordId = cmp.get("v.recordId");
		var workspaceAPI = cmp.find("workspace");
	return 	workspaceAPI
			.isConsoleNavigation()
			.then(
				$A.getCallback(function (isConsoleNavigationResult) {
					if (isConsoleNavigationResult !== true) {
						return "Is not a console navigation";
					}
					//else...
					return workspaceAPI.getAllTabInfo().then(
						$A.getCallback(function (tabInfoList) {
							if (!tabInfoList || !tabInfoList.length) {
								console.log("tabInfoList is null or empty");
								return "tabInfoList is null or empty";
							}
							//else...
							var tabInfoFound = false;
							var tabInfo = null;
							for (var i = 0; i < tabInfoList.length; i++) {
								tabInfo = tabInfoList[i];
								if (
									tabInfo.recordId &&
									helper.compareIds(
										recordId,
										tabInfo.recordId
									)
								) {
									console.log("tab found, index: " + i);
									console.log(
										"tabInfo.recordId: " + tabInfo.recordId
									);
									console.log(
										"tabInfo.tabId: " + tabInfo.tabId
									);
									tabInfoFound = true;
									break;
								}
							}
							if (!tabInfoFound) {
								return (
									"tab with recordId = " +
									recordId +
									" not found"
								);
							}
							var focused = cmp.get("v.focused");
							//else...
							console.log("tabInfoFound -> will openSubtab");
							return workspaceAPI
								.openSubtab({
									parentTabId: tabInfo.tabId,
									pageReference: null,
									recordId: openTabRecordId,
									url: null,
									focus: focused
								})
								.then(
									$A.getCallback(function (result) {
										console.log("openTab -> " + result);
										return result;
									})
								)
								.catch(function (error) {
									return error;
								});
						})
					);
				})
			)
			.then(
				$A.getCallback(function (result) {
					console.log("openTab -> " + result);
					var focused = cmp.get("v.focused");
					var tabHighlighted = cmp.get("v.tabHighlighted");
					if (focused === true) {
						workspaceAPI
							.getFocusedTabInfo()
							.then(function (response) {
								var focusedTabId = response.tabId;
								if (result === focusedTabId) {
									if (tabHighlighted) {
										workspaceAPI
											.setTabHighlighted({
												tabId: focusedTabId,
												highlighted: true,
												options: {
													pulse: true,
													state: "success"
												}
											})
											.then(function (tabResponse) {
												console.log(tabResponse);
											});
									}
								}
							})
							.catch(function (error) {
								console.log(error);
							});
					}
				})
			)
			.catch(function (error) {
				console.error("openTab -> fail, error: " + error);
			});
	},
	openHVCMessagingSession: function (cmp, recordId) {
		if (recordId) {
			if (
				this.isNonHaveCase(cmp) &&
				!this.isSession(recordId)
			) {
				cmp.set("v.loadRecord", false);
				cmp.set("v.focused", false);
				cmp.set("v.tabHighlighted", false);
				cmp.set("v.sObjectName", "Case");
				cmp.set("v.fields", ["Origin", "SourceId"]);
				cmp.set("v.recordIdToLoad", recordId);
				cmp.set("v.loadRecord", true);
			} else {
				cmp.set("v.loadRecord", false);
			}
		}
	}
});