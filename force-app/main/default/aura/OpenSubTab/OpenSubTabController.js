({
	init: function (cmp, event, helper) {
		// eslint-disable-next-line no-console
		cmp.set("v.recordIdToLoad", cmp.get("v.recordId"));
		helper.setFields(cmp);
	},
	recordLoaded: function (cmp, event, helper) {
		var changeType = event.getParams().changeType;
		var tabOpenRecordId = helper.getFields(cmp, changeType);
		if (tabOpenRecordId) {
			helper
				.openTab(cmp, tabOpenRecordId, helper)
				.then(function (result) {
					// eslint-disable-next-line no-console
					console.log("OpenSubTab -> recordLoaded ->" + result);
					if (tabOpenRecordId) {
						helper.openHVCMessagingSession(cmp, tabOpenRecordId);
					}
				});
		}
	}
});