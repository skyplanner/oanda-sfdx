/**
 * @File Name          : BeginChatCmpController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/25/2021, 4:42:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/11/2021, 10:49:55 PM   acantero     Initial Version
**/
({
    init : function(cmp, event, helper) {
        console.log('BeginChatCmp v 1.16');
    },

    onRecordUpdated : function(cmp, event, helper) {
        console.log('BeginChatCmp -> onRecordUpdated');
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
            console.error('BeginChatCmp -> onRecordUpdated -> ERROR');
            return;
        }
        //else... 
        if (changeType === "REMOVED"){
            console.log('BeginChatCmp -> onRecordUpdated -> REMOVED');
            return;
        } 
        //else...
        if (changeType === "LOADED"){
            console.log('BeginChatCmp -> onRecordUpdated -> LOADED');
            const chatRecord = cmp.get('v.chatRecord');
            if (chatRecord) {
                console.log('Status: ' + chatRecord.Status);
                if (chatRecord.Status != helper.IN_PROGRESS_STATUS) {
                    return;
                }
                //else...
                console.log('From_Pre_Chat_Form__c: ' + chatRecord.From_Pre_Chat_Form__c);
                console.log('CaseId: ' + chatRecord.CaseId);
                console.log('AccountId: ' + chatRecord.AccountId);
                console.log('LeadId: ' + chatRecord.LeadId);
                const fromPreChatForm = (chatRecord.From_Pre_Chat_Form__c == true);
                const recordIdList = [];
                if (!fromPreChatForm) {
                    recordIdList.push(chatRecord.CaseId);
                }
                let accountOrLeadId = null;
                let fxAccountId = null;
                if (chatRecord.AccountId) {
                    accountOrLeadId = chatRecord.AccountId;
                    if (chatRecord.Account) {
                        console.log('Account.fxAccount__c: ' + chatRecord.Account.fxAccount__c);
                        fxAccountId = chatRecord.Account.fxAccount__c;
                    }
                    //...
                } else if (chatRecord.LeadId) {
                    accountOrLeadId = chatRecord.LeadId;
                    if (chatRecord.Lead) {
                        console.log('Lead.fxAccount__c: ' + chatRecord.Lead.fxAccount__c);
                        fxAccountId = chatRecord.Lead.fxAccount__c;
                    }
                }
                if (!fromPreChatForm) {
                    if (accountOrLeadId) {
                        recordIdList.push(accountOrLeadId);
                    }
                    if (chatRecord.CaseId) {
                        recordIdList.push(chatRecord.CaseId);
                    }
                }
                if (fxAccountId) {
                    recordIdList.push(fxAccountId);
                }
                console.log('recordIdList.length: ' + recordIdList.length);
                if (recordIdList.length > 0) {
                    helper.openTab(cmp, helper, recordIdList);
                }
            }
        }
    }

})