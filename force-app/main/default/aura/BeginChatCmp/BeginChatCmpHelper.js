/**
 * @File Name          : BeginChatCmpHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/25/2021, 2:02:28 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/11/2021, 11:16:56 PM   acantero     Initial Version
**/
({
    IN_PROGRESS_STATUS : 'InProgress',

    openTab : function(cmp, helper, recordIdList) {
        console.log('BeginChatCmp -> openTab -> begin');
        const recordId = cmp.get("v.recordId");
        const workspaceAPI = cmp.find("workspace");
        workspaceAPI.isConsoleNavigation()
            .then($A.getCallback(function(isConsoleNavigationResult) {
                if (isConsoleNavigationResult != true) {
                    return 'Is not a console navigation';
                }
                //else...
                return workspaceAPI.getAllTabInfo()
                    .then($A.getCallback(function(tabInfoList) {
                        if ((!tabInfoList) || (!tabInfoList.length)) {
                            console.log('tabInfoList is null or empty');
                            return 'tabInfoList is null or empty';
                        }
                        //else...
                        var tabInfoFound = false;
                        var tabInfo = null;
                        for(var i = 0; i < tabInfoList.length; i++) {
                            var tabInfo = tabInfoList[i];
                            if (tabInfo.recordId && helper.compareIds(recordId, tabInfo.recordId)) {
                                console.log('tab found, index: ' + i);
                                console.log('tabInfo.recordId: ' + tabInfo.recordId);
                                console.log('tabInfo.tabId: ' + tabInfo.tabId);
                                tabInfoFound = true;
                                break;
                            }
                        }
                        if (!tabInfoFound) {
                            return 'tab with recordId = ' + recordId + ' not found';
                        }
                        //else...
                        console.log('tabInfoFound -> will openSubtab');
                        const promiseList = [];
                        for(const newTabRecordId of recordIdList) {
                            promiseList.push(
                                workspaceAPI.openSubtab({
                                    parentTabId : tabInfo.tabId,
                                    pageReference : null,
                                    recordId : newTabRecordId,
                                    url : null,
                                    focus : false
                                })
                            );
                        }
                        return Promise.all(promiseList).then($A.getCallback(function(subTabIdList){
                            console.log('Promise.all ends ok');
                            return 'subtab Ids: ' + subTabIdList;
                        }));
                    }));
            })).then($A.getCallback(function(result) {
                console.log('openTab -> ' + result);
            })).catch(function(error) {
                console.error('openTab -> fail, error: ' + error);
            });     
    },

    compareIds : function(id1, id2) {
		if ((!id1) || (!id2)) {
			return false;
		}
		//else...
		if (id1.length == id2.length) {
			return (id1 == id2);
		}
		//else...
		if (id1.length < id2.length) {
			return (id2.indexOf(id1) == 0);
		}
		//else...
		return (id1.indexOf(id2) == 0);
	},

    runServerAction : function(cmp, helper, method, params) {
        return new Promise(function(resolve, reject) {
            var action = cmp.get('c.' + method);
            if (params) {
                action.setParams(params);
            }
			action.setCallback(helper, function(response) {
				var state = response.getState();
				if (state == 'SUCCESS')
					resolve.call(helper, response.getReturnValue());
				else if (state == 'ERROR') {
					var errors = response.getError();
					reject.call(helper, errors);
				}
			});
			$A.enqueueAction(action);
		});
    }

})