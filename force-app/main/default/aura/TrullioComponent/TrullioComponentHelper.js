({
    getEid : function(component, helper) {
        var action = component.get("c.getEIDResults");
        
        var recordId = component.get("v.recordId");
        ///alert(recordId);
        action.setParams({
            'searchKey' : recordId
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                component.set("v.EidList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },	getFlags : function(component, helper) {
        var action = component.get("c.getFlagList");
        
        var recordId = component.get("v.recordId");
        
        ///alert(recordId);
        action.setParams({
            'searchKey' : recordId
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                var arrayToStoreKeys =[];
                component.set("v.FlagList", response.getReturnValue());
                
            }
        });
        $A.enqueueAction(action);
    }
    
    
})