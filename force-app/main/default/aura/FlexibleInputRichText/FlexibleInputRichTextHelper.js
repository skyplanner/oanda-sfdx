({
  loadStaticLinks: function (cmp, optionsToShow, isLinkDisable) {
    //push actual options
    cmp.set("v.linkTypeOptions", optionsToShow);
    if (!isLinkDisable) {
	  this._invoke(cmp,"c.loadStaticLinks", null,
	   function (result) {
          console.log(JSON.stringify(result));
          var linkListTypeOptions = [];
          if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
              var linkLabel = result[i];
              var linkValue = linkLabel.replace(/[^\w]/gi, "").toLowerCase();
              console.log("Link Label " + linkLabel);
              console.log("Link Value " + linkValue);
              linkListTypeOptions.push({
                label: linkLabel,
                value: linkValue
              });
            }
            optionsToShow.push({
              label: "Master Link",
              value: "staticLink"
            });
            cmp.set("v.linkListTypeOptions", linkListTypeOptions);
            //push again if there are any link in the list
            cmp.set("v.linkTypeOptions", optionsToShow);
		  }
		  else{
			 if(optionsToShow.length == 1 ) 
			  cmp.set("v.linkTypeOptions", []);
		  }
        },
        function (error) {
          console.log(error);
        }
      );
    }
  },
  _invoke: function (component, methodName, parameters, onSuccess, onError) {
    var action, errors;
    action = component.get(methodName);

    if (parameters) action.setParams(parameters);

    action.setCallback(this, function (response) {
      switch (response.getState()) {
        case "SUCCESS":
          onSuccess(response.getReturnValue());
          break;
        case "ERROR":
          errors = response.getError();
          onError((errors && errors[0] && errors[0].message) || "Unknow Error");
          break;
      }
    });
    $A.enqueueAction(action);
  },

  getStaticLinkList: function (cmp) {
    var action = cmp.get("c.loadStaticLinks");

    return new Promise(function (resolve, reject) {
      action.setCallback(this, function (response) {
        var state = response.getState();

        if (state === "SUCCESS") {
          resolve(response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          reject(response.getError()[0]);
        }
      });

      $A.enqueueAction(action);
    });
  }
});