/**
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 4/30/2019
 */
({
  doInit: function (component, event, helper) {
    var isAnchorLinkDisabled = component.get("v.isAnchorLinkDisabled"),
      isOpenChatLinkDisabled = component.get("v.isOpenChatLinkDisabled"),
      isListLinkDisabled = component.get("v.isListLinkDisabled"),
      linkTypeOptions = [];

    // if we have other properties, we add the default one..
    // if no other properties we leave the options empty as we don't
    // any other options
    if (
      !isAnchorLinkDisabled ||
      !isOpenChatLinkDisabled ||
      !isListLinkDisabled
    ) {
      linkTypeOptions.push({
        label: "External URL",
        value: "url"
      });

      // if anchor is not disabled...
      if (!isAnchorLinkDisabled)
        linkTypeOptions.push({
          label: "Anchor",
          value: "anchor"
        });

      // if chat is not disabled...
      if (!isOpenChatLinkDisabled)
        linkTypeOptions.push({
          label: "Open Chat",
          value: "chat"
        });
    }

    // we set the link options...
    helper.loadStaticLinks(component, linkTypeOptions, isListLinkDisabled);
  },
  handleChange: function (component, event, helper) {
    component.set(
      "v.readyToSubmit",
      component.find("field").reduce(function (validSoFar, inputCmp) {
        return validSoFar && inputCmp.get("v.validity").valid;
      }, true)
    );
  },
  handleAddLink: function (component, event, helper) {
    component.set("v.linkType", "url");
    component.set("v.linkSource", "append");
    component.set("v.linkText", null);
    component.set("v.linkHref", null);
    component.set("v.readyToSubmit", false);
    component.set("v.showAddLinkModal", true);
  },
  handleCancelAddLink: function (component, event, helper) {
    component.set("v.showAddLinkModal", false);
  },
  handleOnFocus: function (component, event, helper) {
    console.log("On Focuss");
    var event = component.getEvent("onfocus");
    event.fire();
  },
  handleSaveLink: function (component, event, helper) {
    var mustClearHtml = component.get("v.mustClearHtml");

    var htmlValue = component.get("v.htmlValue"),
      linkType = component.get("v.linkType"),
      linkStaticType = component.get("v.linkStaticType"),
      linkHref =
        linkType == "chat"
        //  ? "#open_live_chat"
          ? "http://www.open-live-chat.com" //fake address, in HelpPortal will be change. We remove the 
          : linkType == "staticLink"
          ? "http://staticlink_" + linkStaticType
          : component.get("v.linkHref"),
      linkText = component.get("v.linkText"),
      tag = `<a href="${linkHref}" rel="${linkType}">${linkText}</a>`;
    /*	newHtmlValue = (htmlValue ?
				htmlValue.replace(/<\/?(p|br)>/gi, '') + " " : "") + tag;*/

    //by default is the same
    var newHtmlValue = htmlValue;
    if (mustClearHtml == true) {
		console.log('must clear html');
      newHtmlValue =
		(htmlValue ? htmlValue.replace(/<\/?(p|br)>/gi, "") + " " : "") + tag;
		
    } else {
	  console.log('dont clear html');
      var lineBreak = "<br>";
      var closedP = "</p>";
      var closedLi = "</li>";

      var lastBreak = htmlValue.lastIndexOf(lineBreak);
      var lastP = htmlValue.lastIndexOf(closedP); //last p closed
      var lastLi = htmlValue.lastIndexOf(closedLi); //last p closed

      var htmlSize = htmlValue.length;

      //testing
      var matches = htmlValue.match(/<.+?>/gi); //match all html tags
      var htmlTagsLength = matches.length;

      if (htmlTagsLength > 1) {
        var lastTag = matches[htmlTagsLength - 1];
        var beforeLast = matches[htmlTagsLength - 2];
        if (lastTag == "</p>") {
          if (beforeLast == "<br>")
            newHtmlValue =
              htmlValue.slice(0, lastBreak) +
              htmlValue.slice(lastBreak).replace(lineBreak, tag);
          else
            newHtmlValue =
              htmlValue.slice(0, lastP) + " " + tag + htmlValue.slice(lastP);
        } else if (lastTag == "</ul>" || lastTag == "</ol>") {
          if (
            matches[htmlTagsLength - 3] != null &&
            matches[htmlTagsLength - 3] == "<br>"
          ) {
            newHtmlValue =
              htmlValue.slice(0, lastBreak) +
              htmlValue.slice(lastBreak).replace(lineBreak, tag);
          }
          // newHtmlValue = htmlValue.slice(0, lastLi) + " " + tag + htmlValue.slice(lastLi);
          else newHtmlValue = htmlValue + tag;
        } else {
          newHtmlValue = htmlValue + tag;
        }
      }
      console.log(matches);
      console.log(matches.length);
      console.log("last break " + lastBreak);
      console.log("htmlSize " + htmlSize);
      console.log("size of linebreak " + lineBreak.length);
      console.log("pos " + lastP);
      console.log("htmlValue " + htmlValue);
      console.log("newHtmlValue " + newHtmlValue);
    }

    component.set("v.htmlValue", newHtmlValue);
    component.set("v.showAddLinkModal", false);
  },

  replaceLast: function (str, word, newWork) {
    // find the index of last time word was used
    // please note lastIndexOf() is case sensitive
    var n = str.lastIndexOf(word);

    // slice the string in 2, one from the start to the lastIndexOf
    // and then replace the word in the rest
    str = str.slice(0, n) + str.slice(n).replace(word, newWork);
    return str;
  }
});