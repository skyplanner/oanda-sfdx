/** 
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 4/30/2019
 */
({
	getLanguages: function(component, onSuccess, onError) {
		this._invoke(component, "c.getSupportedLanguages", null,  onSuccess, onError);
	},
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					if (onError)
						onError((errors && errors[0] && errors[0].message) ||
							"Unknow Error");
					else
						console.error(errors);
					break;
			}
		});
		$A.enqueueAction(action);
	}
})