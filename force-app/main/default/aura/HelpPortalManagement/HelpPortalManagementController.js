/** 
 * @author Fernando Gomez, SkyPlanner LLC
 * @since 4/30/2019
 */
({
	doInit: function (component, event, helper) {
		// initially, we pre-select the first tab
		component.set("v.selectedItem", "faq_list");

		// we then fetch the languages
		helper.getLanguages(
			component,
			function(result) {
				component.set("v.languages", result);
			});
	},
	handleActive: function(component, event, helper) {
		component.set("v.selectedLanguage", event.getSource().get("v.id"));
	}
})