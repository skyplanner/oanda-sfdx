/**
 * Created by Ashish Jethi on 2019-04-24.
 */
({  
    restartChat: function(component) {
        if(confirm("All of this conversation's history will be lost if you restart this chat. Continue?")){
            var standardEvent = $A.get("e.nfchat:ChatEvent");
            standardEvent.setParams({
                type : 'action',
                payload : 'clearChat'
            });
            standardEvent.fire();
            var standardEvent2 = $A.get("e.nfchat:ChatEvent");
            standardEvent2.setParams({
                type : 'action',
                payload : 'openChat'
            });
            standardEvent2.fire();
        }
    }
})