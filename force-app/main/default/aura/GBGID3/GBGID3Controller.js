/**
 * @description       : 
 * @author            : Oanda
 * @group             : 
 * @last modified on  : 06-17-2022
 * @last modified by  : Yaneivys Gutierrez
**/
({
	doInit : function(component, event, helper) {        
        helper.getEid(component, helper);
    }
})