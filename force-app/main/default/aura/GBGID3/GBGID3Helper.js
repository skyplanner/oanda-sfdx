/**
 * @description       :
 * @author            : Oanda
 * @group             :
 * @last modified on  : 06-21-2022
 * @last modified by  : Yaneivys Gutierrez
 **/
({
	getEid: function (component, helper) {
		var action = component.get("c.getGBGID3Results");

		var recordId = component.get("v.recordId");
		action.setParams({
			searchKey: recordId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var resultData = response.getReturnValue();
				component.set("v.eid", resultData[0]);
				component.set("v.JsonResponse", parsedID3);
			}
		});
		$A.enqueueAction(action);
	}
});