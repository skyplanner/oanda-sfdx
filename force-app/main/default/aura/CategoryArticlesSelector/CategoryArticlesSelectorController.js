({
	doInit : function(component, event, helper) {
		console.log('CategoryArticlesSelectorHelper v1');
		helper.initDataTable(component);
		helper.fetch(component);
	},

	close : function(component, event, helper) {
		helper.fireCloseSelectorEvt(component, null);
	},

	onSearchTxtKeyUp : function (component, event, helper) {
    	if (event.key == "Enter") {
    		//console.log('onSearchTxtKeyUp -> enter');
    		helper.fetch(component);
    	}
    },

    onSearchTxtChanged : function (component, event, helper) {
    	var searchTxt = component.get("v.searchText");
    	if (searchTxt == '') {
    		//console.log('onSearchTxtChanged -> searchText cleaned');
    		helper.fetch(component);
    	}
    },

	select : function(component, event, helper) {
		//console.log('CategoryArticlesSelectorController -> close');
		var selArticleList = [];
		var articlesDt = component.find('articlesDt');
		var selectedRows = articlesDt.getSelectedRows();
		var length = (selectedRows) ? selectedRows.length : 0;
		//console.log('selectedRows.length: ' + length);
		if (length == 0) {
			helper.showErrorToast($A.get("$Label.c.HelpPortalSelectOneArticleMsg"));
			return;
		}
		//else...
		var maxSelection = component.get('v.maxSelection');
		if (length > maxSelection) {
			var msg = helper.strFormat($A.get("$Label.c.HelpPortalSelectUpToXArticles"), [maxSelection]);
			helper.showErrorToast(msg);
			return;
		}
		//else...
		for(var i = 0; i < length; i++) {
			selArticleList.push({
				artId :selectedRows[i].Id,
				knoledgeArticleId : selectedRows[i].KnowledgeArticleId,
				title : selectedRows[i].Title
			});
		}
		helper.fireCloseSelectorEvt(component, selArticleList);
	},

	fetch: function(component, event, helper) {
		helper.fetch(component);
	},

	updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortBy", fieldName);
        component.set("v.sortDirection", sortDirection);
        helper.fetch(component);
    },

	loadMore: function(component, event, helper) {
		//console.log('CategoryArticlesSelectorController -> loadMore');
		var dataTableCmp = event.getSource();
		dataTableCmp.set("v.isLoading", true);
		helper.fetchRecords(component,
			function(result) {
				helper.updateDataList(component, result, true);
				//console.log('after loadMore -> infLoading: ' + component.get('v.infLoading'));
				dataTableCmp.set("v.isLoading", false);
			},
			function(errors) {
				helper.showErrorToast(errors && errors[0] ? 
					errors[0].message : helper.msgs.UNKNOWN_ISSUE);
				dataTableCmp.set("v.isLoading", false);
			}
		);
	}
	
})