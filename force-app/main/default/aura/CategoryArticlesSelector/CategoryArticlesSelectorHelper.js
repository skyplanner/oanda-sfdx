/**
 * @File Name          : CategoryArticlesSelectorHelper.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2019, 4:14:16 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/9/2019, 1:53:42 PM   acantero     Initial Version
**/
({
	DESC_ORDER: 'desc',
	ASC_ORDER: 'asc',

	initDataTable: function(component) {
		console.log('CategoryArticlesSelectorHelper initDataTable v5');
		var columns = [{
			label: 'Title',
			fieldName: 'Title',
			sortable: true,
			type: 'text',
		}];
		component.set("v.columns", columns);
	},

	fireCloseSelectorEvt: function(component, selection) {
		//console.log('CategoryArticlesSelectorHelper -> fireCloseSelectorEvt');
		var validSelection = (selection || []);
		var selectorClosedEvent = component.getEvent("selectorClosed"); 
		selectorClosedEvent.setParams({"selection" : validSelection }); 
		selectorClosedEvent.fire();
		//console.log('selectorClosedEvent fired');
	},

	fetch: function(component) {
		var helper = this;
		component.set('v.busy', true);
		component.set('v.articles', []);
		component.set('v.offset', 0);
		component.set('v.infLoading', true);
		component.set('v.recordCount', 0);
		this.countRecords(component,
			function(r) {
				var validCount = r || 0;
				component.set('v.recordCount', validCount);
				console.log('countRecords -> recordCount: '+ validCount);
			}
		);
		// we fetch the initial chunk of data
		this.fetchRecords(component,
			function(result) {
				helper.updateDataList(component, result, false);
				component.set('v.busy', false);
			}
		);
	},

	updateDataList: function(component, items, concat) {
		var newList = items || [];
		if (newList.length == 0) {
			//console.log('updateArticleList -> no results, set infLoading = false');
			component.set('v.infLoading', false);
			return;
		}
		//else...
		if (concat) {
			var current = component.get('v.articles') || [];
			var newList = current.concat(newList);
		}
		component.set('v.articles', newList);
		var newOffset = newList.length;
		component.set('v.offset', newOffset);
		//console.log('updateArticleList -> concat: ' + concat + ', newOffset: '+ newOffset);
		var recordCount = component.get('v.recordCount');
		var maxRows = component.get('v.maxRows');
		var maxLenght = ((recordCount > 0) && (recordCount < maxRows))
			? recordCount
			: maxRows;
		if (newOffset >= maxLenght) {
			component.set('v.infLoading', false);
			//console.log('updateOffset -> infLoading = false');
		}
	},

	getCriterias: function(component) {
		//console.log('getCriterias -> excludeArtIds: ' + component.get("v.excludeArtIds"));
		var criterias = {
			searchText: component.get("v.searchText") || null,
			status: component.get("v.status"),
			category: component.get("v.category"),
			audience: component.get("v.audience"),
			division: component.get("v.division"),
			hasPreviewDoc : component.get("v.restrictTopArticles"),
			excludeArtIds : component.get("v.excludeArtIds"),
			restrictToVisibleInPkb : component.get("v.restrictToVisibleInPkb")
		};
		//this.io(criterias, 'criterias');
		return criterias;
	},

	countRecords: function(component, onSuccess, onError) {
		var params = {
			criterias: this.getCriterias(component)
		};
		this._invoke(
			component,
			'c.countArticles',
			params,
			onSuccess,
			onError
		);
	},

	fetchRecords: function(component, onSuccess, onError) {
		var self = this;
		var sortBy = component.get("v.sortBy");
		var offset = component.get("v.offset");
		console.log('fetchRecords -> offset: '+ offset);
		var params = {
			criterias: this.getCriterias(component),
			pageSize: component.get("v.pageSize"),
			offset: offset,
			sortBy: sortBy,
			isDescending: (component.get("v.sortDirection") == this.DESC_ORDER) 
		};
		this._invoke(
			component,
			'c.fetchArticles',
			params,
			function(result) {
				onSuccess(result);
			},
			onError
		);
	},

	showErrorToast: function(msg) {
		this.showToast('error', '', msg);
	},

	showWarningToast: function(msg) {
		this.showToast('warning', '', msg);
	},
	
	showToast: function(type, header, msg) {
		$A.get("e.force:showToast").setParams({
			 title: header,
			 type: type,
			 message: msg
		}).fire();
	},

	strFormat: function(str, params) {
		for(var i = 0; i < params.length; i++) {
			str = str.replace("{" + i + "}", params[i]);
		}
		return str;
	},

	io : function(obj, objId) {
		var str = (objId) ? (objId + ' ->\n') : '';
		for (const prop in obj) {
			str += (prop + ': ' + obj[prop] + '\n');
		}
		console.log(str);
	},

	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.busy", false);
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}

})