/* eslint-disable no-console */
/**
 * @File Name          : HighValueCustomerBannerController.js
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/10/2020, 1:00:20 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/10/2020   acantero     Initial Version
 **/
({
	doInit: function (component, event, helper) {
		console.log("HighValueCustomerBannerController v.3");
		var source = component.get("v.source");
		console.log(source);
		var showForce = true;
		var recordFields = [];
		//call apex function for retrieve IsHVC and DateBecomeCore
		if (source === "Case") {
			recordFields = ["DateBecomeCore__c", "IsHVC__c"];
		} else if (source === "Account") {
			recordFields = [
				"Date_Customer_Became_Core__c",
				"Is_High_Value_Customer__c"
			];
		} else if (source === "MessagingSession") {
			recordFields = [
				"Case.Date_Customer_Became_Core__c",
				"Case.Is_HVC__c"
			];
		} else {
			showForce = false;
			helper.loadCase(component);
		}
		if (recordFields.length > 0) {
			component.set("v.recordFields", recordFields);
		}
		component.set("v.loadRecord", showForce);
	},
	recordUpdated: function (component, event) {
		console.log("recordUpdated");
		var changeType = event.getParams().changeType;
		var fields = ["dateBecomeCore", "isChatHVC"];
		if (changeType === "LOADED") {
			var obj = component.get("v.obj");
			console.log(JSON.stringify(obj));
			if (obj) {
				var recordFields = component.get("v.recordFields");
				for (var index = 0; index < recordFields.length; index++) {
					var props = recordFields[index].split(".");
					var objCopy = JSON.parse(JSON.stringify(obj));
					if (objCopy && objCopy !== null) {
						for (
							var i = 0, iLen = props.length - 1;
							i < iLen;
							i++
						) {
							var prop = props[i];
							var candidate =
								prop in objCopy ? objCopy[prop] : undefined;
							if (candidate !== undefined) {
								objCopy = candidate;
							} else {
								break;
							}
						}
					}
					var value =
						objCopy && objCopy !== null && props[i] in objCopy
							? objCopy[props[i]]
							: undefined;
					console.log(recordFields[index] + " : " + value);
					if (value) {
						var field = fields[index];
						component.set("v." + field, value);
						console.log(component.get("v." + field));
					}
				}
			}
		}
	}
});