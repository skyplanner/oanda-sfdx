/**
 * @File Name          : HighValueCustomerBannerHelper.js
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 4/13/2020, 12:06:40 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/13/2020   dmorales     Initial Version
**/
({
	loadCase: function(cmp){
		var action = cmp.get("c.getCaseFromChatTranscript");
        var chatTrancriptId =  cmp.get("v.recordId");       

     		action.setParams({
                Id: chatTrancriptId              
            });

            action.setCallback(this, function (response) {
               var state = response.getState();
                if ( state === "SUCCESS") {
					var values = response.getReturnValue();
					console.log(values);
					if(values != null){
					  cmp.set("v.isChatHVC", values.Is_HVC__c);
					  cmp.set("v.DateBecomeCore", values.Date_Customer_Became_Core__c);
					}					
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    response.getError()[0];
                }
            });

            $A.enqueueAction(action);
         
	},
	
})