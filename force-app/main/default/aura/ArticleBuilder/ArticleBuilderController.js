/**
 * @author Fernando Gomez
 * @since 5/13/2019
 * @LastModified: 16/04/2020 by dmorales
 */
({
	doInit: function(component, event, helper) {
		var pageReference, recordId, workspaceApi;

		component.set("v.isSaving", false);
		component.set("v.savingError", null);
		component.set("v.previewUrl", null);
		component.set("v.showPreview", false);
		component.set("v.showSave", false);
		component.set("v.isSaved", false);
		component.set("v.previewDocumentId", null);
		component.set("v.noEdit", false);

		// we make sure we show the tab properly
		workspaceApi = component.find("workspace");
		workspaceApi
			.getFocusedTabInfo()
			.then(function(response) {
				workspaceAPI.setTabLabel({
					tabId: response.tabId,
					label: "Article Builder"
				});
			});

		// we need to make sure the record id is 
		// is valid if presente
		pageReference = component.get("v.pageReference");
		recordId = component.get("v.recordId") || pageReference.state.c__recordId;
		component.set("v.recordId", recordId);

		if (recordId) {
			component.set("v.isLoading", true);
			helper.getArticle(
				component,
				recordId,
				function(result) {
					component.set("v.isLoading", false);
					component.set("v.translations", result.translations);
					component.set("v.pageTitle", result.faq.Title || "");
					component.set("v.publishStatus", result.faq.PublishStatus);

					switch (result.faq.PublishStatus) {
						case "Online":
							if (result.draft) {
								component.set("v.noEdit", true);
								component.set("v.isPublishedWithDraft", true);
								component.set("v.draftArticleId", result.draft.Id);
							}
							break;
						case "Draft":
							if (result.published) {
								component.set("v.isDraftWithPublished", true);
								component.set("v.publishedArticleId",
									result.published.Id);
							}
							break;
					}
				},
				function(error) {
					component.set("v.isLoading", false);
					component.set("v.loadingError", error);
				});
		} else {
			component.set("v.isLoading", false);
			component.set("v.translations", [{
				languageCode: "en_US",
				languageLabel: "English",
				question: "",
				error: false,
				items: []
			}]);
		}

		// we then fetch the languages
		/*
		helper.getLanguages(
			component,
			function(result) {
				component.set("v.languages", result);
			});
		*/
	},
	reInit: function(component, event, helper) {
		$A.get('e.force:refreshView').fire();
	},
	handleQuestionChange: function(component, event, helper) {
		var title = event.getSource().get("v.value").trim();
		component.set("v.pageTitle", title ? title : "New FAQ");
	},
	handleLanguageChange: function(component, event, helper) {
		var tabs = component.find("languagesTabs"),
			selectedTabId = tabs.get("v.selectedTabId"),
			translations = component.get("v.translations"), 
			mp, languages;

		if (selectedTabId != "newTranslation")
			component.set("v.languageCode", selectedTabId);
		else {
			mp = {};
			languages = [];
			// we only show langguages that have not been taken
			translations.forEach(function(t) {
				mp[t.languageCode] = true;
			});
			// we then go through the languages
			component.get("v.languages").forEach(function(l) {
				if (!mp[l.value])
					languages.push(l);
			});
			component.set("v.availableLanguagesReady", false);
			component.set("v.newTranslationLanguage", null);
			setTimeout(function() {
				component.set("v.availableLanguagesReady", true);
				component.set("v.availableLanguages", languages);
			}, 300);
		}
	},
	handleCreateEmptyTranslation: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.newTranslationLanguage"),
			languages = component.get("v.languages"),
			language = helper.getLanguageByCode(languages, languageCode);

		translations.push({
			languageCode: languageCode,
			languageLabel: language ? language.label : "",
			question: "",
			items: []
		});

		component.set("v.languageCode", languageCode);
		component.set("v.translations", translations);
		component.find("languagesTabs").set("v.selectedTabId", languageCode);
	},
	handleCloneTranslation: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.newTranslationLanguage"),
			languages = component.get("v.languages"),
			cloneFromLanguage = event.getSource().get("v.value"),
			cloneFrom = helper.getTranslation(translations, cloneFromLanguage),
			newTranslation = JSON.parse(JSON.stringify(cloneFrom)),
			language = helper.getLanguageByCode(languages, languageCode);

		newTranslation.languageCode = languageCode;
		newTranslation.languageLabel = language ? language.label : null;
		newTranslation.question = "";
		// we also need to clean the values in the items
		newTranslation.items.forEach(function(item) {
			switch(item.tag) {
				case "h2":
				case "h4":
				case "p":
					item.value = "";
					break;
				case "ul":
				case "ol":
					item.items.forEach(function(i) {
						i.value = "";
					});
					break;
				case "table":
					item.columns.forEach(function(c) {
						c.value = ""
					});
					item.rows.forEach(function(r) {
						r.cells.forEach(function(c) {
							c.value = "";
						});
					});
					break;
				case "img":
					item.caption = "";
					break;
			}
		});
		translations.push(newTranslation);

		component.set("v.languageCode", languageCode);
		component.set("v.translations", translations);
		component.find("languagesTabs").set("v.selectedTabId", languageCode);
	},
	handleCreateNewItem: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			anchors = component.get("v.anchors"),
			translation = helper.getTranslation(translations, languageCode);
		var index = component.get("v.indexSelected");		
		var indexToInsert = index != null ? index : translation.items.length;
		var hasSectionError = false;
		component.set("v.isValidSection", true);
		if (translation && translation.items){
			var newItem = null;
			switch (event.currentTarget.getAttribute("data-value")) {
				case "newItemHeader":
					newItem = helper.addHeader();
					break;
				case "newItemSubHeader":
					newItem = helper.addSubHeader();
					break;
				case "newItemParagraph":
					newItem = helper.addParagraph();
					break;
				case "newItemList":
					newItem = helper.addList();
					break;
				case "newItemTable":
					newItem = helper.addTable();
					break;
				case "newItemImage":
					newItem = helper.addImage();
					break;
				case "newItemAnchor":
					newItem = helper.addAnchor();
					break;
				case "newItemVideo":
					newItem = helper.addVideo();
					break;
				case "newItemSectionStart":
					helper.validateSection(translation.items,component,"newItemSectionStart" );
					var isValid = component.get("v.isValidSection");
					if(!isValid){
						var error = component.get("v.sectionError");
						helper.showErrorToast(error);
						component.set("v.isValidSection", true);
						hasSectionError = true;
						break;
					}
					newItem = helper.addSectionStart();
					break;
				case "newItemSectionEnd":
					helper.validateSection(translation.items,component,"newItemSectionEnd" );
                    var isValid = component.get("v.isValidSection");
					if(!isValid){
						var error = component.get("v.sectionError");
						helper.showErrorToast(error);
						component.set("v.isValidSection", true);
						hasSectionError = true;
						break;
					}
					newItem = helper.addSectionEnd();
					break;
			}
            if(!hasSectionError){
				helper.insertAt(translation.items, indexToInsert, newItem);
		    	component.set("v.translations", translations);
			    component.set("v.indexSelected", index != null ? index + 1 : null);	
			}
	
		}
			
	},
	handleMoveUp: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");
		helper.moveUp(translation.items, index, component);
		component.set("v.translations", translations);
	},
	handleMoveDown: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");
		helper.moveDown(translation.items, index, component);
		component.set("v.translations", translations);
	},
	handleDelete: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");

		helper.markAsDeleted(translation.items, index);
		component.set("v.translations", translations);
		setTimeout(function() {
			helper.delete(translation.items, index, component);
			component.set("v.translations", translations);
		}, 300);
	},
	handleAddListItem: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");

		helper.addListItem(translation.items, index);
		component.set("v.translations", translations);
	},
	handleDeleteListItem: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			value = event.getSource().get("v.value"),
			splitted = value.split(":"),
			index = parseInt(splitted[0]),
			liIndex = parseInt(splitted[1]);

		helper.markListItemAsDeleted(translation.items, index, liIndex);
		component.set("v.translations", translations);
		setTimeout(function() {
			helper.deleteListItem(translation.items, index, liIndex);
			component.set("v.translations", translations);
		}, 300);
	},
	handleAddTableColumn: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");

		helper.addTableColumn(translation.items, index);
		component.set("v.translations", translations);
	},
	handleAddTableRow: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");

		helper.addTableRow(translation.items, index);
		component.set("v.translations", translations);
	},
	handleDeleteTableRow: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			value = event.getSource().get("v.value"),
			splitted = value.split(":"),
			index = parseInt(splitted[0]),
			tdIndex = parseInt(splitted[1]);

		helper.markTableRowAsDeleted(translation.items, index, tdIndex);
		component.set("v.translations", translations);
		setTimeout(function() {
			helper.deleteTableRow(translation.items, index, tdIndex);
			component.set("v.translations", translations);
		}, 300);
	},
	handleDeleteTableColumn: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			value = event.getSource().get("v.value"),
			splitted = value.split(":"),
			index = parseInt(splitted[0]),
			thIndex = parseInt(splitted[1]);

		helper.markTableColumnAsDeleted(translation.items, index, thIndex);
		component.set("v.translations", translations);
		setTimeout(function() {
			helper.deleteTableColumn(translation.items, index, thIndex);
			component.set("v.translations", translations);
		}, 300);
	},
	handleEditImage: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]),
			file = event.getSource().get("v.files")[0];

		if (helper.validateFile(translation.items, index, file)) {
			helper.addImageFile(translation.items, index, file,
				function(image) {
					component.set("v.translations", translations);
					helper.convertFileToDocument(
						component,
						image.fileName,
						image.fileType,
						image.fileUrl,
						function(url) {
							image.fileUrl = url;
							image.error = null;
							component.set("v.translations", translations);
						},
						function(e) {
							image.fileUrl = null;
							image.error = e;
							component.set("v.translations", translations);
						});
				});
		} else 
			component.set("v.translations", translations);
	},
	handleRemoveQuestionError: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode);
		// all of this to simply remove the error marker
		translation.error = false;
		component.set("v.translations", translations);
	},
	handleRemoveError: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]);

		helper.removeError(translation.items, index);
		component.set("v.translations", translations);
	},
	handleRemoveListError: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]),
			liIndex = parseInt(splitted[2]);

		helper.removeListError(translation.items, index, liIndex);
		component.set("v.translations", translations);
	},
	handleRemoveTableColumnError: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]),
			thIndex = parseInt(splitted[2]);

		helper.removeTableColumnError(translation.items, index, thIndex);
		component.set("v.translations", translations);
	},
	handleRemoveTableCellError: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]),
			trIndex = parseInt(splitted[2]),
			tdIndex = parseInt(splitted[3]);

		helper.removeTableCellError(translation.items, index, trIndex, tdIndex);
		component.set("v.translations", translations);
	},
	handleChangeVideoId: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]);
		helper.removeInvalidCharacters(translation.items, index, "videoId");
		helper.validateVideo(translation.items, index);
		component.set("v.translations", translations);
	},
	preventInvalidCharacters: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			name = event.getSource().get("v.name"),
			splitted = name.split(":"),
			index = parseInt(splitted[1]);
		helper.removeInvalidCharacters(translation.items, index, "extra");
		component.set("v.translations", translations);
	},
	handlePreview: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode");
		
		component.set("v.isValidSection", true);	
		helper.validateSections(translations,component);
		var isValid = component.get("v.isValidSection");
		if(!isValid){
			var error = component.get("v.sectionError");
			helper.showErrorToast(error);
			//component.set("v.isValidSection", true);			
			component.set("v.translations", translations);
			return;
		}

		var	previewDocumentId = component.get("v.previewDocumentId"),
			isAllGood = helper.fixAndValidateHtml(translations, component),
			previewJson = JSON.stringify(translations);
		
		
		component.set("v.showPreview", true);
		helper.getPreview(
			component,
			previewJson,
			previewDocumentId || null,
			function(result) {
				component.set("v.previewDocumentId", result);
				component.set("v.previewUrl",
					"/apex/ArticlePreview?language=" + languageCode +
					"&previewId=" + result);
				component.set("v.allowSave", isAllGood);
				component.set("v.translations", translations);
			},
			function(error) {
				
			});	
	},
	handleCancelPreview: function(component, event, helper) {
		component.set("v.previewUrl", null);
		component.set("v.showPreview", false);
		component.set("v.showSave", false);
	},
	handleDoNotSave: function(component, event, helper) {
		component.set("v.showSave", false);
	},
	handleTrySave: function(component, event, helper) {
		component.set("v.showSave", true);
	},
	handleDoSave: function(component, event, helper) {
		var translations = component.get("v.translations"),
			previewDocumentId = component.get("v.previewDocumentId"),
			previewJson = JSON.stringify(translations),
			recordId = component.get("v.recordId") || null;

		component.set("v.isSaving", true);
		component.set("v.savingError", null);
		helper.upsertArticle(
			component,
			recordId,
			JSON.stringify(translations),
			previewDocumentId,
			function(result) {
				var urlEvent;
				component.set("v.recordId", result.Id);
				component.set("v.pageTitle", result.Title);
				component.set("v.isSaving", false);
				component.set("v.savingError", null);
				component.set("v.showSave", false);
				component.set("v.isSaved", true);
			},
			function(error) {
				component.set("v.isSaving", false);
				component.set("v.savingError", error);
			});		
	},
	handleCreateNewFaq: function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": "/lightning/n/Article_Builder"
		});
		urlEvent.fire();
	},
	handleOpenDraft: function(component, event, helper) {
		var draftArticleId = component.get("v.draftArticleId"),
			urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": "/lightning/cmp/c__ArticleBuilder?c__recordId=" + draftArticleId
		});
		urlEvent.fire();
	},
	handleOpenPublished: function(component, event, helper) {
		var publishedArticleId = component.get("v.publishedArticleId"),
			urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": "/lightning/cmp/c__ArticleBuilder?c__recordId=" + publishedArticleId
		});
		urlEvent.fire();
	},
	handleGoToFaq: function(component, event, helper) {
		var recordId = component.get("v.recordId"),
			urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": "/lightning/r/FAQ__kav/" + recordId + "/view"
		});
		urlEvent.fire();
	},
	/*** NEW FOR SELECT ANY ITEM***/
	handleSelect: function(component, event, helper) {
		var translations = component.get("v.translations"),
			languageCode = component.get("v.languageCode"),
			translation = helper.getTranslation(translations, languageCode),
			index = event.getSource().get("v.value");
		helper.selectItem(translation.items, index, component);
		component.set("v.translations", translations);
	},
})