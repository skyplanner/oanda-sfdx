/**
 * @author Fernando Gomez
 * @since 5/13/2019
 * @LastModified: 16/04/2020 by dmorales
 */
({
	getTranslation: function(translations, languageCode) {
		return translations.find(function(t) {
			return (languageCode == t.languageCode);
		});
	},
	getLanguageByCode: function(languages, languageCode) {
		return languages.find(function(l) {
			return (languageCode == l.value);
		});
	},
	addHeader: function() {
		var newItem = {
			name: "Header",
			tag: "h2",
			value: "",
			deleted: false,
			error: false,
			selected: false
		};	
	   return newItem;
	},
	addSubHeader: function() {
	    var newItem = {
			name: "Sub-Header",
			tag: "h4",
			value: "",
			deleted: false,
			error: false,
			selected: false
		};	
	    return newItem;
	},
	addParagraph: function() {
		var newItem = {
			name: "Paragraph",
			tag: "p",
			value: "",
			deleted: false,
			error: false,
			selected: false
		};	
		return newItem;		
	},
	addList: function() {
		var newItem = {
			name: "List",
			tag: "ul",
			items: [{
				value: "",
				deleted: false,
				error: false
			}],
			deleted: false,
			selected: false
		};		
		return newItem;
	},
	addTable: function() {
		var newItem = {
			name: "Table",
			tag: "table",
			columns: [{
				value: "",
				deleted: false,
				error: false
			}],
			rows: [{
				cells: [{ 
					value: "",
					deleted: false,
					error: false
				}],
				deleted: false
			}],
			selected: false
		};		
		return newItem;
	},
	addImage: function() {
		var newItem = {
			name: "Image",
			tag: "img",
			fileUrl: null,
			fileName: null,
			fileSize: null,
			fileType: null,
			caption: "",
			error: null,
			selected: false
		};	
		return newItem;
	},
	addAnchor: function() {
		var newItem = {
			name: "Anchor",
			tag: "a",
			id: "anchor_" + (new Date()).getTime() + "_",
			extra: null,
			deleted: false,
			selected: false
		};		
		return newItem;
	},
	addVideo: function() {
		var newItem = {
			name: "Video",
			tag: "video",
			type: "youtube.com",
			videoId: null,
			caption: null,
			autoplay: false,
			isValid: false,
			deleted: false,
			selected: false
		};	
		return newItem;
	},
	addSectionStart: function() {
		var newItem = {
			name: "Section-Start",
			tag: "section_start",
			collapsed: true,
			value: "",
			deleted: false,
			error: false,
			errorSection: false,
			selected: false		
		};	
		return newItem;
	},
	addSectionEnd: function() {
		var newItem = {
			name: "Section-End",
			tag: "section_end",
			value: "",
			deleted: false,
			errorSection: false,
			selected: false		
		};		
		return newItem;
	},
	moveUp: function(items, index, cmp) {
		var i1 = items[index],
			i2 = items[index - 1];
		if (index > 0) {
			items[index] = i2;
			items[index - 1] = i1;
			//if the element or it's adjacent are selected then update the selected index
			if(i1.selected || i2.selected )
			  cmp.set("v.indexSelected", 
			  (i1.selected) ?
			   index - 1 : 
			   index);		   
		}
		return items;
	},
	moveDown: function(items, index, cmp) {
		var i1 = items[index],
			i2 = items[index + 1];			
		if (index < items.length - 1) {
			items[index] = i2;
			items[index + 1] = i1;
			//if the element or it's adjacent are selected then update the selected index
			if(i1.selected || i2.selected)
			  cmp.set("v.indexSelected", 
						(i1.selected) ?
						index + 1 : 
						index);						
			
		} 
		return items;
	},
	markAsDeleted: function(items, index) {
		console.log('MARK AS DELETE');
		var item = items[index];
		if (item)
			item.deleted = true;
		return items;	
	},
	delete: function(items, index, cmp) {
		console.log('DELETE');
		var item = items[index];
		if (item)
			items.splice(index, 1);

		var indexSelected = cmp.get("v.indexSelected");	

		console.log('indexSelected ' + indexSelected);	
		console.log('index ' + index);
		console.log('(indexSelected > index) ' + (indexSelected > index));
		console.log('(indexSelected == index) ' + (indexSelected == index));
	
		cmp.set("v.indexSelected", 
				(indexSelected > index)? 
				  indexSelected - 1: 
				(indexSelected == index)?
				 null:
				 indexSelected);
		
		console.log('cmp.get("v.indexSelected") ' + cmp.get("v.indexSelected"));		 
		return items;	
	},
	addListItem: function(items, index) {
		var list = items[index];
		if (list && list.items)
			list.items.push({
				value: "",
				deleted: false
			});
		return items;
	},
	markListItemAsDeleted: function(items, index, liIndex) {
		var list = items[index];
		if (list && list.items && list.items[liIndex])
			list.items[liIndex].deleted = true;
		return items;
	},
	deleteListItem: function(items, index, liIndex) {
		var list = items[index];
		if (list && list.items && list.items[liIndex])
			list.items.splice(liIndex, 1);
		return items;
	},
	addTableColumn: function(items, index) {
		var table = items[index];
		if (table && table.columns) {
			table.columns.push({
				value: "",
				deleted: false,
				error: false
			});
			// now, we need a new cell at the end of each row
			if (table.rows)
				table.rows.forEach(function(row) {
					row.cells.push({ 
						value: "",
						deleted: false,
						error: false
					});
				});
		}
		return items;
	},
	addTableRow: function(items, index) {
		var table = items[index], cells;
		if (table && table.rows) {
			cells = [];
			// we add a cell per colum
			if (table.columns)
				table.columns.forEach(function(colunm) {
					cells.push({
						value: "",
						deleted: false,
						error: false
					});
				});
			// then we add the row with the cells
			table.rows.push({
				cells: cells,
				deleted: false,
				error: false
			});
		}
		return items;
	},
	markTableColumnAsDeleted: function(items, index, thIndex) {
		var table = items[index];
		if (table && table.columns && table.columns[thIndex]) {
			table.columns[thIndex].deleted = true;
			// we need to mark each row in the same index as deleted
			if (table.rows)
				table.rows.forEach(function(row) {
					if (row.cells && row.cells[thIndex])
						row.cells[thIndex].deleted = true;
				});
		}
		return items;
	},
	deleteTableColumn: function(items, index, thIndex) {
		var table = items[index];
		if (table && table.columns && table.columns[thIndex]) {
			table.columns.splice(thIndex, 1);
			// we need to mark each row in the same index as deleted
			if (table.rows)
				table.rows.forEach(function(row) {
					if (row.cells && row.cells[thIndex])
						row.cells.splice(thIndex, 1);
				});
		}
		return items;
	},
	markTableRowAsDeleted: function(items, index, trIndex) {
		var table = items[index];
		if (table && table.rows && table.rows[trIndex])
			table.rows[trIndex].deleted = true;
		return items;
	},
	deleteTableRow: function(items, index, trIndex) {
		var table = items[index];
		if (table && table.rows && table.rows[trIndex])
			table.rows.splice(trIndex, 1);
		return items;
	},
	removeError: function(items, index) {
		var item = items[index];
		if (item)
			item.error = false;
		return items;	
	},
	removeListError: function(items, index, liIndex) {
		var list = items[index];
		if (list && list.items && list.items[liIndex])
			list.items[liIndex].error = false;
		return items;
	},
	removeTableColumnError: function(items, index, thIndex) {
		var table = items[index];
		if (table && table.columns && table.columns[thIndex])
			table.columns[thIndex].error = false;
		return items;
	},
	removeTableCellError: function(items, index, trIndex, tdIndex) {
		var table = items[index];
		if (table && table.rows && table.rows[trIndex] && 
				table.rows[trIndex].cells && table.rows[trIndex].cells[tdIndex])
			table.rows[trIndex].cells[tdIndex].error = false;
		return items;
	},
	validateFile: function(items, index, file) {
		var image = items[index];

		if (file.size > 200000) {
			image.error = "Images should not be larger than 200 Kb.";
			return false;
		}

		if (file.type != "image/png" &&
				file.type != "image/jpeg" &&
				file.type != "image/giff") {
			image.error = "File format not supported. Only .png, .jpg, .giff.";
			return false;
		}

		image.error = null;
		return true;
	},
	removeInvalidCharacters: function(items, index, propName) {
		var item = items[index];
		if (item[propName])
			item[propName] = item[propName].replace(/[^a-zA-Z0-9\-_]/, "");
	},
	validateVideo: function(items, index) {
		var video = items[index];
		return this._validateVideo(video);
	},
	_validateVideo: function(video) {
		switch (video.type) {
			case "youtube.com":
				video.isValid = video.videoId != null &&
					video.videoId.match(/^[a-zA-Z0-9\-_]{11}$/) != null;
				break;
			default:
				video.isValid = false;
				break;
		}

		return video.isValid;
	},
	addImageFile: function(items, index, file, onCompleted) {
		var converter = this.convertFileToDocument,
			image = items[index],
			reader  = new FileReader();
		// we need to extract the bytes
		reader.addEventListener("load", function() {
			image.fileUrl = reader.result;
			image.fileName = file.name;
			image.fileSize = file.size;
			image.fileType = file.type;
			onCompleted(image);
		});
		reader.readAsDataURL(file);
	},
	changeImageUrl: function(items, index, url, onInProgress, onCompleted) {
		var converter = this.convertFileToDocument,
			image = items[index];
		image.fileUrl = url;
		return items;
	},
	fixAndValidateHtml: function(translations) {
		var rg = /<\/?(p|br)>/gi,
			ag = /<a href=".*?(#anchor_[a-zA-Z0-9_\-]*?)" *target="_blank">/gi,
			isAllGood = true,
			fixHtml = function(html) {
				return !html ? "" : html
					.replace(rg, "")
					.replace(ag, '<a href="$1">')
					.trim();
			},
			validateVideo = this._validateVideo;

		// if we have no translations
		if (!translations || !translations.length)
			isAllGood = false;
		else{
		
		    // we also need to clean the values in the items
			translations.forEach(function(translation) {
				translation.error = false;

				if (!translation.question || !translation.question.trim()) {
					isAllGood = false;
					translation.error = true;
				}

				if (!translation.items || !translation.items.length)
					isAllGood = false;
				else{
					
					translation.items.forEach(function(item, index) {
						switch(item.tag) {
							case "h2":
							case "h4":
								item.value = item.value ? item.value.trim() : "";
								item.error = false;
								if (!item.value) {
									isAllGood = false;
									item.error = true;
								}
								break;
							case "p":
								item.value = fixHtml(item.value);
								item.error = false;
								if (!item.value){
									isAllGood = false;
									item.error = true;
								}
								break;
							case "ul":
							case "ol":
								if (!item.items || !item.items.length)
									isAllGood = false;
								else
									item.items.forEach(function(i) {
										i.value = fixHtml(i.value);
										i.error = false;
										if (!i.value) {
											isAllGood = false;
											i.error = true;
										}
									});
								break;
							case "table":
								if (!item.columns || !item.columns.length)
									isAllGood = false;
								else
									item.columns.forEach(function(c) {
										c.value = c.value ? c.value.trim() : "";
										c.error = false;
										if (!c.value) {
											isAllGood = false;
											c.error = true;
										}
									});

								if (!item.rows || !item.rows.length)
									isAllGood = false;
								else
									item.rows.forEach(function(r) {
										if (!r.cells || !r.cells.length)
											isAllGood = false;
										else
											r.cells.forEach(function(c) {
												c.value = fixHtml(c.value);
												c.error = false;
												if (!c.value) {
													isAllGood = false;
													c.error = true;
												}
											});
									});
								break;
							case "img":
								item.caption = item.caption ? item.caption.trim() : "";
								item.error = null;
								if (!item.fileUrl) {
									isAllGood = false;
									item.error = "No file specified";
								}
								break;
							case "video":
								item.caption = item.caption ? item.caption.trim() : "";
								item.error = null;
								if (!validateVideo(item)) {
									isAllGood = false;
									item.error = "Invalid video ID";
								}
								break;
							case "section_start":
								item.value = item.value ? item.value.trim() : "";
								item.error = false;
								if (!item.value) {
									isAllGood = false;
									item.error = true;
								}						
								break;							
						}
					});		
				
				}
					
			});
	
		}	

	
		return isAllGood;
	},
	// SERVER SUPPPORT
	getLanguages: function(component, onSuccess, onError) {
		this._invoke(component, "c.getSupportedLanguages", null,  onSuccess, onError);
	},
	convertFileToDocument: function(component, 
			fileName, fileType, fileUrl, onSuccess, onError) {
		this._invoke(component, "c.convertFileToDocument", {
				"fileName": fileName, 
				"contentType": fileType, 
				"base64Data": fileUrl.split("base64,")[1]
			},
			onSuccess, onError);
	},
	getPreview: function(component, translationsJson, 
			previewDocumentId, onSuccess, onError) {
		this._invoke(component, "c.getPreview", {
			"translationsJson": translationsJson,
			"previewDocumentId": previewDocumentId
		}, onSuccess, onError);
	},
	upsertArticle: function(component, recordId, translationsJson,
			previewDocumentId, onSuccess, onError) {
		if (recordId)
			this._invoke(component, "c.updateExistentArticle", {
				"knwoledgeArticleId": recordId,
				"translationsJson": translationsJson,
				"previewDocumentId": previewDocumentId
			}, onSuccess, onError);
		else
			this._invoke(component, "c.createNewArticle", {
				"translationsJson": translationsJson,
				"previewDocumentId": previewDocumentId
			}, onSuccess, onError);
	},
	getArticle: function(component, knowledgeArticleId, onSuccess, onError) {
		this._invoke(component, "c.getTranslationsFromArticle", {
			"knowledgeArticleId": knowledgeArticleId
		}, onSuccess, onError);
	},
	_invoke: function(component, methodName, parameters, onSuccess, onError) {
		var errors, e,
			action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
						(onError ? onError : console.error)(e);
					break;
			}
		});
		$A.enqueueAction(action);
	},
	/** NEW FUNCTION FOR ACCORDION */
	showErrorToast: function(msg) {
		this.showToast('error', 'Error', msg);
	},

	showSuccessfulToast: function(msg) {
		this.showToast('success', 'Successful', msg);
	},

	showToast: function(type, header, msg) {
		$A.get("e.force:showToast").setParams({
			 title: header,
			 type: type,
			 message: msg
		}).fire();
	},
	selectItem: function(items, index, cmp ) {
		var item = items[index];
		var indexActualSelect = cmp.get("v.indexSelected");
		console.log('item ' + JSON.stringify(item));
		console.log('Index ' + index);
		console.log('indexActualSelect ' + indexActualSelect);
		if (item){
			var isActualSelected = item.selected;
			console.log(isActualSelected);			
			if(index != indexActualSelect){ //user select a new element
				var itemSelected = indexActualSelect != null ? items[indexActualSelect]: [];
				console.log('itemSelected ' + JSON.stringify(itemSelected));
				item.selected = true;
				if(itemSelected)
				  itemSelected.selected = false;				
			}		   
			else{
				item.selected = !isActualSelected; 	 //user undo the selection  
			}  
			cmp.set("v.indexSelected",  isActualSelected ? null : index); 
		}
			
		return items;
	},
	/**var item = items[index];
	 * item.selected = !isActualSelected;
			cmp.set("v.allowSelection", isActualSelected);
			cmp.set("v.indexSelected",  isActualSelected ? null : index);	*/
	insertAt: function (items, index, element) {
		items.splice(index, 0, element);
		return items;
	},
	validateSection: function(items, cmp, type){
		var sectionTags = [];
	
		//if a section is selected then just verify until that index.
		var indexSelected = cmp.get("v.indexSelected");	
		for(var i = 0; i < items.length; i++){
			var item = items[i];
			if(indexSelected == i) 
			  break; 
			if(item.tag.indexOf('section')> -1)
			  sectionTags.push(item);					
		}
		
		console.log(JSON.stringify(sectionTags));
			

		if(type == 'newItemSectionStart'){
			if(sectionTags.length > 0){			
				if(sectionTags[sectionTags.length - 1].tag != 'section_end'){
					console.log(cmp.get("v.isValidSection"));
					cmp.set("v.isValidSection", false);
					cmp.set("v.sectionError", "You must close the section that was previously open");
				}
			}		
		}
	
		if(type == 'newItemSectionEnd'){
			if(sectionTags.length == 0){
				cmp.set("v.isValidSection", false);
				cmp.set("v.sectionError", "You cannot include a Section (End) without a Section (Start)");
				return;
			}
			//else
			if(sectionTags[sectionTags.length - 1].tag != 'section_start'){
				cmp.set("v.isValidSection", false);
				cmp.set("v.sectionError", "You cannot include a Section (End) without a Section (Start)");
			}		
		}

	},	
	validateSections : function(translations, cmp){
		var self = this;
		translations.forEach(function(translation) {
			self._checkAllSectionTags(translation.items, cmp);			
		});
	},
	_checkAllSectionTags: function(items, cmp){
		var sectionTags = [];
		items.forEach(function(item, index) {
		//	console.log(JSON.stringify(item));
		//	console.log(item.tag.indexOf('section'));
			if(item.tag.indexOf('section')> -1){
				item.index = index;
				sectionTags.push(item);
			}
             			
		});

		var error = '';
		var validSection = true;

		if(sectionTags.length == 0){
			cmp.set("v.isValidSection", validSection);
			return validSection;
		}
		//else
			//clear all section errors before and check if there are content after each section start. 
			for (var i = 0; i < sectionTags.length; i++){
				sectionTags[i].errorSection = false;
			//	console.log( "BEFORE sectionTags[i].errorSection " + sectionTags[i].errorSection );
				if(sectionTags[i].tag == 'section_start')
				{
					
					var itemAfter = items[sectionTags[i].index + 1];
					console.log("itemAfter " + itemAfter);
					if(itemAfter == null || itemAfter.tag.indexOf('section')> -1){
						if(validSection){
							validSection = false;
					    	error += " All sections must have content inside" + " \n";
						}					
						sectionTags[i].errorSection = true;
					}
				}

				//console.log( "AFTER sectionTags[i].errorSection " + sectionTags[i].errorSection );
			}

			//check if the first tag is a section start
			if(sectionTags[0].tag != 'section_start'){
				sectionTags[0].errorSection = true;
				error += " The first 'Section' element must be a Section (Start)" + " \n";				
				validSection = false;				
			}
			if(sectionTags.length == 1){
				error = " It seems that one Section element is missing." +
				        " Check if you have a Section(Start) without a Section (End) or viceversa" + " \n";				
				validSection = false;	
			}
			else{
				if(sectionTags[sectionTags.length-1].tag != 'section_end'){
					sectionTags[sectionTags.length-1].errorSection = true;
				    error += " The last 'Section' element must be a Section (End)" + "  \n";				
			    	validSection = false;	
				}
				//check if exist two tags of the same type adjacent
				var existTwoEqual = false;
				for (var i = 0; i < sectionTags.length - 1; i++){
					var item1 = sectionTags[i] ;
					var item2 = sectionTags[i + 1];
					if(item1.tag == item2.tag){
						existTwoEqual = true;
						item1.errorSection = true;
						item2.errorSection = true;
					}					
				}
				if(existTwoEqual){
					error += " Please review the 'Section' elements on the article. " +
					         " It seems there are two Section elements of the same type together." + " \n";				
			    	validSection = false;					
				}
			} 						
		

		if(!validSection){
			cmp.set("v.isValidSection", validSection);
			cmp.set("v.sectionError", error);	
		 }	

		return validSection;
	},
	
})