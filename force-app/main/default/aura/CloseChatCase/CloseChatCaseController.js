/**
 * @File Name          : CloseChatCaseController.js
 * @Description        : Close chat case in a new subtab
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/15/2021, 5:58:23 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/7/2019, 4:30:25 PM   acantero     Initial Version
**/

({
	init : function (cmp, event, helper) {
		console.log('CloseChatCaseController v 3.4');
		var myPageRef = cmp.get("v.pageReference");
        var chatTranscriptId = myPageRef.state.c__chatTranscriptId;
        var caseId = myPageRef.state.c__caseId;
		console.log('chatTranscriptId: ' + chatTranscriptId + ', caseId: ' + caseId);
		if (chatTranscriptId) {
            cmp.set("v.chatTranscriptId", chatTranscriptId);
            if (caseId) {
                cmp.set("v.caseId", caseId);
                cmp.set("v.loaded", true);
                console.log('CloseChatCaseController loaded');
            } else {
                helper.getCaseId(cmp, chatTranscriptId);
            }
		}
    },
    
    cancel : function (cmp, event, helper) {
        cmp.set("v.busy", true);
        helper.closeTab(cmp);
    },

    save : function (cmp, event, helper) {
        helper.closeCase(cmp, helper, false);
    },

    saveAndFree : function (cmp, event, helper) {
        helper.closeCase(cmp, helper, true);
    },

    caseClosed : function (cmp, event, helper) {
        console.log('CloseChatCaseController.caseClosed');
        var caseIdParam = event.getParam('caseId');
        var sourceParam = event.getParam('source');
        var caseId = cmp.get("v.caseId");
        var customName = cmp.get("v.customName");
        console.log('caseIdParam: ' + caseIdParam + ', caseId: ' + caseId);
        console.log('sourceParam: ' + sourceParam + ', customName: ' + customName);
        if (caseIdParam == caseId) {
            if (sourceParam == customName) {
                helper.showSuccessfulToast($A.get("$Label.c.CloseCaseSuccessMsg"));
            }
            helper.closeTab(cmp);
        }
    }
	 
})