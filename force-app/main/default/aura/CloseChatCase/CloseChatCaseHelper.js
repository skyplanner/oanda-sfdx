/**
 * @File Name          : CloseChatCaseHelper.js
 * @Description        : Close chat case in a new subtab
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/11/2021, 10:47:11 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/7/2019, 1:52:44 PM   acantero     Initial Version
**/
({

    getCaseId : function (cmp, chatTranscriptId) {
        console.log('CloseChatCaseHelper.getCaseId v3');
		this._invoke(
			cmp,
			'c.getCaseIdFromChatTranscript',
			{chatTranscriptId : chatTranscriptId},
			function(caseId) {
                console.log('caseId: ' + caseId);
                if (caseId) {
                    cmp.set("v.caseId", caseId);
					cmp.set("v.loaded", true);
					console.log('CloseChatCaseController loaded');
                } else {
                    console.error('caseId is null or undefined');
                }
			}
		);
	},
	
	closeCase : function (cmp, helper, freeCapacity) {
        cmp.set("v.busy", true);
        var closeClase = cmp.find("closeClase");
        if (!closeClase.submit(freeCapacity)) {
            cmp.set("v.busy", false);
        }
    },

    closeTab : function(cmp) {
		var workspaceAPI = cmp.find("workspace");
		workspaceAPI.getEnclosingTabId()
		.then(function(tabId){
			console.log('tabId: ' + tabId);
			if (tabId) {
				workspaceAPI.closeTab({tabId : tabId})
				.then(function(result){
					console.log('closeTab result: ' + result);
				});
			}
		});
    },

    showErrorToast: function(msg) {
		this.showToast('error', '', msg);
	},

	showWarningToast: function(msg) {
		this.showToast('warning', '', msg);
	},
	
	showSuccessfulToast: function(msg) {
		this.showToast('success', '', msg);
	},

	showInfoToast: function(msg) {
		this.showToast('info', '', msg);
	},
	
	showToast: function(type, header, msg) {
		$A.get("e.force:showToast").setParams({
			 title: header,
			 type: type,
			 message: msg
		}).fire();
	},
    
    _invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.busy", false);
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}

})