/**
 * @File Name          : CXNoteListController.js
 * @Description        : 
 * @Author             : Gilbert Gao
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/3/2021, 1:31:42 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         ???                      Gilbert Gao            Initial Version
 * 2.0         02-18-2021               Silvia Skyplanner
**/
({
	doInit : function(component, event, helper) {

		//load the CX Notes for the current fxAccount
        helper.loadCXNotes(component);

        var actionSelectFxa = component.get("c.getLiveFxAccounts");

        console.log("AccountId : " + component.get("v.recordId"));

        //request the fxAccounts for this Account
       if(component.get("v.recordId")){

       	   //get the list fxAccounts
       	   actionSelectFxa.setParams({ "accountId" : component.get("v.recordId") });
       	   actionSelectFxa.setCallback(this,function(resp){
                    var state = resp.getState();
                    
                    if(component.isValid() && state === 'SUCCESS'){
                         console.log('Response: '+JSON.stringify(resp.getReturnValue()));
                         component.set("v.fxAccounts",resp.getReturnValue());
                         component.set("v.selectedValue", component.get("v.currentFxAccountId"));
                        //  component.find("selectFxAccount").set("v.value", component.get("v.currentFxAccountId"));

                         var dataArray = component.get("v.fxAccounts");
                         console.log('Data Array: '+ JSON.stringify(dataArray));

                        if(dataArray.length > 0 && 
                            (dataArray[0].Firstname__c != null || 
                              dataArray[0].Corporate_Name__c != null))
                        {
                           helper.getRecordFullName(component, dataArray[0]);
                        }
                        else{
                          component.set("v.fxAccountActive",true);
                          component.set("v.recordLabel","Select fxAccount");
                        }
                        
                    }else{
                        console.log(resp.getError());
                    }
           });

           $A.enqueueAction(actionSelectFxa);
       }
	},

	updateSelectedFxAccount : function(component, event, helper){
		var selectedValue = component.find("selectFxAccount").get("v.value");

		component.set("v.currentFxAccountId",selectedValue);

        //reload the CX Notes based on the current fxAccount
		helper.loadCXNotes(component);
	},

  addNote : function(component, event, helper){

    var actionAddNote = component.get("c.addNoteEntry");
    var noteEntry = component.find("cxNoteInput").get("v.value");
    
    //reset input text area
    component.find("cxNoteInput").set("v.value", "");

    console.log("note entry: " + noteEntry);

    //add note to the current fxAccount
    if(component.get("v.currentFxAccountId") && noteEntry){

       actionAddNote.setParams({"fxAccountId" : component.get("v.currentFxAccountId"),
                                "noteEntry" : noteEntry});

       actionAddNote.setCallback(this,function(resp){
             var state = resp.getState();
              
              if(component.isValid() && state === 'SUCCESS'){
                   //pass the records to be displayed
                   component.set("v.cxNotes",resp.getReturnValue());

                   helper.formatNoteOutput(component);
                   helper.fireSubmitEvent(component);
                  
              }else{
                  console.log(resp.getError());
              }
       });

       $A.enqueueAction(actionAddNote);

    }
  }
})