/**
 * @File Name          : CXNoteListHelper.js
 * @Description        : 
 * @Author             : Gilbert Gao
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 6/3/2021, 1:32:15 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         ???                      Gilbert Gao            Initial Version
 * 2.0         02-18-2021               Silvia Skyplanner
**/
({
	loadCXNotes : function(component) {
        //load the notes for the current fxAccount
        var action = component.get("c.getCXNotes");

       //request the CX Note for the current fxAccount
       if(component.get("v.currentFxAccountId")){

           action.setParams( {"fxAccountId": component.get("v.currentFxAccountId")} );

           action.setCallback(this,function(resp){
                    var state = resp.getState();
                    
                    if(component.isValid() && state === 'SUCCESS'){
                        
                        //pass the records to be displayed
                        component.set("v.cxNotes",resp.getReturnValue());

                        this.formatNoteOutput(component);

                    }else{
                        console.log(resp.getError());
                    }
           });

           $A.enqueueAction(action);

       }
    },
    
    getRecordFullName : function(component, data){
        let affType = data.Affiliate_Type__c;
        let fullName;
        if(affType == 'Individual'){
            fullName = (data.Middle_Name__c != null) 
                    ? (data.Firstname__c + ' ' +
                        data.Middle_Name__c + ' ' +
                        data.LastName__c)
                    : (data.Firstname__c + ' ' +
                        data.LastName__c);
        }
        else 
            if(affType == 'Corporate'){
                fullName = data.Corporate_Name__c;
            }
            else
                fullName = 'Undefined';

        console.log("Affiliate name: "+ fullName);
        component.set("v.fxAccountActive",false);
        component.set("v.recordLabel","Affiliate");
        component.set("v.recordName",fullName);
    },

	formatNoteOutput : function(component) {
       var output = '';
       var cxNotes = component.get("v.cxNotes");

       cxNotes.sort(this.compareCXNote);

       var timezone = new Date().toString().match(/\(([A-Za-z\s].*)\)/)[1];

       for (var i = 0; i < cxNotes.length; i++) {
          output += $A.localizationService.formatDateTime(cxNotes[i].createdDateTime, "MMM DD YYYY, hh:mm:ss a") + " (" + timezone + ") ";

          if(cxNotes[i].subject != null){
          	output += cxNotes[i].subject + " ";

          }

          output += "<" + cxNotes[i].author + ">\n";

          output = output + cxNotes[i].comment;
          output = output + "\n-------\n\n";
       }

       component.set("v.cxNoteOutput",output);
	},

    compareCXNote : function(note1, note2){

    	if(note1.createdDateTime < note2.createdDateTime){
    		return 1;
    	}else if(note1.createdDateTime > note2.createdDateTime){
    		return -1;
    	}else{
    		return 0;
    	}

    },

    fireSubmitEvent : function(component, source) {
        console.log('FireSubmitEvent in CXNoteList Ctrl');
        //var recordId = component.get("v.recordId");
        var sourceContainer = component.get("v.sourceContainer")
        console.log('CXNoteList sourceContainer: ' + sourceContainer );
        if (sourceContainer) {
            var cxNoteSubmitEvt = $A.get("e.c:CXNoteSubmitEvent"); 
            cxNoteSubmitEvt.setParam("sourceContainer", sourceContainer);      
            cxNoteSubmitEvt.fire();
        } else {
            console.error('CXNoteSubmitEvent can not be fired, sourceContainer is null or undefined');
        }
    }
    
})