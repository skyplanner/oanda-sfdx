({
	doInit : function(component, event, helper) {
		
        var actionFxAcctName = component.get("c.getFxAccountName");
        var actionAgreements = component.get("c.getAgreements");
        
        console.log("fxAccount Id : " + component.get("v.recordId"));
        
        if(component.get("v.recordId")){
            
            actionFxAcctName.setParams({ "fxaId" : component.get("v.recordId") });
       	    actionFxAcctName.setCallback(this,function(resp){
                    var state = resp.getState();
                    
                    if(component.isValid() && state === 'SUCCESS'){
                        
                        component.set("v.fxAccountName",resp.getReturnValue());
                        
                    }else{
                        console.log(resp.getError());
                    }
           });
            
           actionAgreements.setParams({ "fxaId" : component.get("v.recordId") });
           actionAgreements.setCallback(this,function(resp){
                    var state = resp.getState();
                    
                    if(component.isValid() && state === 'SUCCESS'){
                        
                        component.set("v.agreements",resp.getReturnValue());
                        
                    }else{
                        console.log(resp.getError());
                    }
           });
            
           $A.enqueueAction(actionFxAcctName);
           $A.enqueueAction( actionAgreements);
        
        }
        
	}
})