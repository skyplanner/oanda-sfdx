({
	loadAccount: function (cmp, source) {
		console.log("CXNotesHelper v.7");
		var action = cmp.get("c.getAccountDynamic");
		// if(source == 'Account'){
		//     action = cmp.get("c.getAccount");
		// }
		// else
		//   if(source == 'ChatTranscript'){
		//     action = cmp.get("c.getAccountFromChatTranscript");
		//   }
		if (action) {
			var recordId = cmp.get("v.recordId");

			action.setParams({
				Id: recordId,
				sObjectName: source
			});

			action.setCallback(this, function (response) {
				var state = response.getState();

				if (state === "SUCCESS") {
					var account = response.getReturnValue();
					console.log(
						"loadAccounInChat: " +
							JSON.stringify(response.getReturnValue())
					);
					console.log("account: " + account);
					if (account != null) {
						cmp.set("v.existAccount", true);
						cmp.set("v.accountId", account.Id);
						cmp.set("v.fxAccountId", account.fxAccount__c);
						this.createCXNotesCmp(
							cmp,
							account.Id,
							account.fxAccount__c
						);
					}
				} else if (state === "ERROR") {
					var errors = response.getError();
					console.log(
						"CXNotesContainerHelper Errors : " +
							JSON.stringify(response.getError()[0])
					);
				}
			});
			$A.enqueueAction(action);
		}
	},

	createCXNotesCmp: function (cmp, idAccount, fxAccount) {
		var source = cmp.get("v.source");
		console.log(
			"CXNotesContainerHelper idAccount " +
				idAccount +
				" fxAccount " +
				fxAccount +
				" source " +
				source
		);
		if (idAccount) {
			$A.createComponent(
				"c:CXNoteList",
				{
					currentFxAccountId: fxAccount,
					recordId: idAccount,
					fullViewPage: "CXNotesPage",
					sourceContainer: source
				},
				function (result, status, errorMessage) {
					//Add the new button to the body array
					if (status === "SUCCESS") {
						var body = cmp.get("v.body");
						body.push(result);
						console.log(
							"CXNotesContainerHelper create cmp result " +
								JSON.stringify(result)
						);
						cmp.set("v.body", body);
					} else if (status === "INCOMPLETE") {
						console.log(
							"No response from server or client is offline."
						);
						// Show offline error
					} else if (status === "ERROR") {
						console.log(
							"CXNotesContainerHelper Error creating cmp: " +
								errorMessage
						);
						// Show error message
					}
				}
			);
		}
	}
});