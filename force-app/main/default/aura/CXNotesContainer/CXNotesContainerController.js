({
  doInit: function(component, event, helper) {
    console.log("CXNotesContainer v.13");
    var source = component.get("v.source");
    var recordId = component.get("v.recordId");
    console.log(
      "CXNotesContainer Source in Init " + source + "recordId " + recordId
    );
    helper.loadAccount(component, source);
  },

  refreshCXNoteList: function(component, event, helper) {
    console.log("CXNotesContainer Cmp catch the event throw by CXNoteList");
    //this always return Account Id since CXNoteList is who throw the event
    var sourceContainer = event.getParam("sourceContainer");
    //this recordId could be Account Id or Chat Transcript Id, depend on which layout is this component located.
    var source = component.get("v.source");

    console.log(
      "CXNotesContainer sourceContainerEvent: " +
        sourceContainer +
        ", recordId: " +
        source
    );

    if (sourceContainer == source) {
       console.log("I catch the event but my child was who throw it so..i dont have to reload")
    }
    else
    {
      component.set("v.body", []);
      var idAccount = component.get("v.accountId");
      var fxAccount = component.get("v.fxAccountId");
      helper.createCXNotesCmp(component, idAccount, fxAccount);
    }
  }
});