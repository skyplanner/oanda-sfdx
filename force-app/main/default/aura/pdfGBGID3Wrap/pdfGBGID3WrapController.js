/**
 * @description       :
 * @author            : Yaneivys Gutierrez
 * @group             :
 * @last modified on  : 06-20-2022
 * @last modified by  : Yaneivys Gutierrez
 **/
({
	doInit: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		window.open('/apex/pdfGBGID3?id=' + recordId, '_blank');
        $A.get("e.force:closeQuickAction").fire();
	}
});