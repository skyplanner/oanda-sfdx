({
	DELETE_ACTION : 1,
	MOVE_UP_ACTION : 2,
	MOVE_DOWN_ACTION : 3,
	ADD_ACTION : 4,

	fireArticleAction : function(component, artIndex, action) {
		var topArticleActionEvent = component.getEvent("topArticleAction"); 
		topArticleActionEvent.setParams({
			action : action,
			index : artIndex
		}); 
		topArticleActionEvent.fire();
		//console.log('topArticleActionEvent fired');
	}
})