/**
 * @File Name          : CategoryTopArticlesDetailController.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 10/18/2019, 6:01:24 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/17/2019   acantero     Initial Version
**/
({
	doInit : function(component, event, helper) {
		console.log('CategoryTopArticlesDetailController v3');
		var columns = [{
			label: 'Title',
			fieldName: 'Title',
			sortable: false,
			type: 'text',
		}];
		component.set("v.columns", columns);
	},

	delete : function(component, event, helper) {
		var artIndex = event.getSource().get("v.value");
		helper.fireArticleAction(component, artIndex, helper.DELETE_ACTION);
	},

	moveUp : function(component, event, helper) {
		var artIndex = event.getSource().get("v.value");
		helper.fireArticleAction(component, artIndex, helper.MOVE_UP_ACTION);
	},

	moveDown : function(component, event, helper) {
		var artIndex = event.getSource().get("v.value");
		helper.fireArticleAction(component, artIndex, helper.MOVE_DOWN_ACTION);
	},

	add : function(component, event, helper) {
		helper.fireArticleAction(component, -1, helper.ADD_ACTION);
	},

	onArticlesChange : function(component, event, helper) {
		console.log('CategoryTopArticlesDetailController -> onArticlesChange');
		var articles = component.get("v.articles");
		console.log('articles.length: ' + articles.length);
		for(var i = 0; i < articles.length; i++) {
			console.log('article ' + i + ': ' + articles[i].title);
		}
	}

})