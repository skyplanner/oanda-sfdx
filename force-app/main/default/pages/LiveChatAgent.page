<!--
  @File Name          : LiveChatAgent.page
  @Description        : 
  @Author             : dmorales
  @Group              : 
  @Last Modified By   : Dianelys Velazquez
  @Last Modified On   : 10-28-2022
  @Modification Log   : 
  Ver       Date            Author      		    Modification
  1.0    7/03/2019   dmorales     Initial Version
-->
<apex:page controller="LiveChatBySkillsSelectionCon" 
	docType="HTML-5.0"
	applyhtmltag="false"
	applybodytag="false"
	showheader="false"
	sidebar="false"
	standardstylesheets="false"
	language="{!$CurrentPage.parameters.LanguagePreference}">
<html lang="{!$CurrentPage.parameters.LanguagePreference}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="Live Chat" />
		<meta name="keywords" content="Live Chat" />
		<meta name="viewport" 
			content="width=device-width,initial-scale=1.0,
				maximum-scale=1.0,user-scalable=0" />

		<link rel="stylesheet" 
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
			integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
			crossorigin="anonymous" />
		<link rel="stylesheet"
			href="https://use.fontawesome.com/releases/v5.8.0/css/all.css"
			integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y"
			crossorigin="anonymous" />
		<link REL="SHORTCUT ICON" HREF="{!URLFOR($Resource.Oanda_Logo_ICO)}"/>
		<apex:stylesheet value="{! $Resource.OandaBaseCss }" />
		<apex:stylesheet value="{! $Resource.FeedbackCss }" />

		<style>

			.custom-link {
				text-decoration: underline !important;
			}

			.panel-visible {
				display: block;
			}

			.panel-hidden {
				display: none;
			}

			.warning-header {
				font-weight: bold;
			}

			.offline-warning {
				text-align: left;
			}
		
		</style>

		<script type='text/javascript' src='https://{! JSRoot }/deployment.js'></script>
	</head>
	<body  class="oanda-body bg-light"
		style="padding-bottom:85px;">
		<!-- messages -->
		<input type="hidden" name="unableToConnectMsg" id="unableToConnectMsg"
			value="{! unableToConnectMsg }" />
		<input type="hidden" name="unableToConnectMsgDefaultLang" id="unableToConnectMsgDefaultLang"
			value="{! unableToConnectMsgDefaultLang }" />
		<!-- chat lang selection params -->
		<form id="chatLangSelectionForm" action="{! chatLangSelectionUrl }" method="post">
			<input type="hidden" name="LanguagePreference" id="langSelLangPreference"
				value="{! languagePreference }" />
		</form>
		<!-- error way -->
		<apex:outputPanel rendered="{! isAPostchatRedirect }" layout="none">
			<input type="hidden" name="error" value="{! $CurrentPage.parameters.error }" />
			<input type="hidden" name="oldChatDetails" id="oldChatDetails" 
				value="{! $CurrentPage.parameters.chatDetails }" />
		</apex:outputPanel>
		<!-- normal way -->
		<apex:outputPanel rendered="{! !isAPostchatRedirect }" layout="none">
			<!-- page properties -->
			<input type="hidden" name="LanguageLabel" id="LanguageLabel"
				value="{! languageLabel }" />
			<input type="hidden" name="LanguageCorrected" id="LanguageCorrected"
				value="{! languageCorrected }" />
			<input type="hidden" name="DivisionName" id="DivisionName"
				value="{! divisionName }" />
			<input type="hidden" name="InquiryNatureMapped" id="InquiryNatureMapped"
				value="{! inquiryNatureMapped }" />
			<input type="hidden" name="ServiceType" id="ServiceType"
				value="{! serviceType }" />
			<input type="hidden" name="ServiceSubtype" id="ServiceSubtype"
				value="{! serviceSubtype }" />
			<!-- page params -->
			<form id="refreshForm" action="{! refreshUrl }" method="post">
				<input type="hidden" name="automaticConnection" id="automaticConnection"
					value="{! automaticConnection }" />
				<input type="hidden" name="LanguagePreference" id="LanguagePreference"
					value="{! languagePreference }" />
				<input type="hidden" name="CustomerLastName" id="CustomerLastName"
					value="{! $CurrentPage.parameters.CustomerLastName }" />
				<input type="hidden" name="CustomerEmail" id="CustomerEmail"
					value="{! $CurrentPage.parameters.CustomerEmail }" />
				<input type="hidden" name="AccountType" id="AccountType"
					value="{! $CurrentPage.parameters.AccountType }" />
				<input type="hidden" name="Division" id="Division"
					value="{! $CurrentPage.parameters.Division }" />
				<input type="hidden" name="InquiryNature" id="InquiryNature"
					value="{! $CurrentPage.parameters.InquiryNature }" />
				<input type="hidden" name="SpecificQuestion" id="SpecificQuestion"
					value="{! $CurrentPage.parameters.SpecificQuestion }" />
				<input type="hidden" name="IsAccount" id="IsAccount"
					value="{! $CurrentPage.parameters.IsAccount }" />
				<input type="hidden" name="IsHVC" id="IsHVC"
					value="{! $CurrentPage.parameters.IsAccountHVC }" />
				<input type="hidden" name="fxAccount" id="fxAccount"
					value="{! $CurrentPage.parameters.fxAccount }" />
				<input type="hidden" name="ldRecordTypeId" id="ldRecordTypeId"
					value="{! $CurrentPage.parameters.ldRecordTypeId }" />
				<input type="hidden" name="Crypto" id="Crypto"
					value="{! $CurrentPage.parameters.Crypto }" />
			</form>
		</apex:outputPanel>
		
		<header id="header" class="mb-3">
			<nav class="navbar navbar-expand navbar-dark bg-dark" id="oanda_menu"> 
				<div class="container container-sm">
					<span class="navbar-brand border-secondary py-0 pr-3">
						<img width="133" height="40" 
							src="{! URLFOR($Resource.HelpPortalImg, 
								'oanda-logo-inverse.svg') }" />
					</span>
				</div>
			</nav>
		</header>
		<div class="container container-sm bg-white rounded py-3">
			<div class="d-flex" 
				style="height: 300px;align-items: center;justify-content: center;text-align: center;flex-direction: column">
				
				<!-- normal way -->
				<apex:outputPanel rendered="{! (!automaticConnection) && agentsAvailable }" layout="none">
					<div id="chatButtonTopPanel">
						<h4>{! $Label.Finish_PreChat_Wizard } </h4>
						<div class="text-center p-4 bg-white" style="min-width:71px;">	
							<div id="chatButtonPanel" style="display: none;">
								<button
									id="liveagent_button_online_{! DefaultChatButtonId }"
									type="button"
									class="btn btn-primary"
									onclick="agent.safeStartDefaultChat()">
									{! $Label.Start_Chat }
								</button>
							</div>
						</div>
						<apex:outputPanel rendered="{! offlineHours }" layout="none">
							<div class="offline-warning pt-4">
								<div class="warning-header mb-2">
									{! $Label.Chat_Note_Header }
								</div>
								<p>
									{! $Label.Crypto_Offline_Warning }
								</p>
							</div>
						</apex:outputPanel>
					</div>
				</apex:outputPanel>

				<div id="noAgentsPanel" style="display:{! IF(!agentsAvailable, 'block', 'none') }">
					<p id="noAgentsPanelMsg"></p>
				</div>

				<div id="loader" style="display: none;" class="container-sm rounded py-3">
					<div class="col-md-auto">
						<div class="spinner-grow text-success" role="status" style="width: 2rem; height: 2rem;">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="fakeOnlineElement" style="display: none"></div>
		<div id="fakeOfflineElement" style="display: none"></div>
		
		<script type="text/javascript"
			src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
		</script>
		<script type="text/javascript" src="{! $Resource.LiveChatAgentScript }"></script>  
		
		<script>
			$(document).ready(function() {
				//init constants (defined in LiveChatAgentScript)
				constants.HELP_PORTAL_URL = '{! $Site.BaseUrl }';
				constants.API_ROOT = '{! APIRoot }';
				constants.DEPLOY_PARAM_1 = '{! DeployParam1 }';
				constants.DEPLOY_PARAM_2 = '{! DeployParam2 }';
				//...
				constants.HELP_PORTAL_LABEL = '{! $Label.Link_Help_Portal }';
				constants.CLICK_HERE_LABEL = '{! $Label.Link_Click_Here }';
				//...
				//init agent properties (defined in LiveChatAgentScript)
				agent.defaultChatButtonId = '{! DefaultChatButtonId }';
				agent.oneChatButtonForAll = {! oneChatButtonForAll };
				agent.isAPostchatRedirect = {! isAPostchatRedirect };
				agent.automaticConnection = {! automaticConnection };
				agent.agentsAvailable = {! agentsAvailable };
				agent.isEnglish = {! isEnglish };
				agent.defaultLanguage = '{! defaultLanguage }';
				agent.defaultLanguageLabel = '{! defaultLanguageLabel }';
				//...
				agent.setup();	
			});
		</script>
		<site:googleAnalyticsTracking />	
	</body>
</html>
</apex:page>