<!--
  @Page Name          : LiveChatEventMessage.page
  @Description        : 
  @Author             : ????
  @Group              : 
  @Last Modified By   : Dianelys Velazquez
  @Last Modified On   : 10-28-2022
  @Modification Log   : 
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0        ????                      ????                   Initial Version
-->
<apex:page docType="HTML-5.0" applyhtmltag="false" applybodytag="false" 
	showheader="false" sidebar="false" standardstylesheets="false"
 	controller="LiveChatEventMessageCtrl" language="{!language}" cache="false">
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta name="description" content="Live Chat" />
			<meta name="keywords" content="Live Chat" />
			<meta name="viewport" content="width=device-width,initial-scale=1.0,
			maximum-scale=1.0,user-scalable=0" />
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
				integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
				crossorigin="anonymous" />
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css"
				integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y"
				crossorigin="anonymous" />
			<link REL="SHORTCUT ICON" HREF="{!URLFOR($Resource.Oanda_Logo_ICO)}"/>
			<apex:stylesheet value="{! $Resource.OandaBaseCss }" />
			<apex:stylesheet value="{! $Resource.FeedbackCss }" />
			<apex:stylesheet value="{! $Resource.ChatOfflineCss }" />

			<style>
				.btn.disabled, .btn:disabled {
					opacity: .10;
				}

				.custom-link {
					text-decoration: underline !important;
				}
			</style>

		</head>
		<body class="oanda-body bg-light" style="padding-bottom:85px;">

			<form id="pageForm" action="{! nextUrl }" method="post">
				<input type="hidden" name="language" id="language"
					value="{! language }" />
				<input type="hidden" name="isRedirect" id="isRedirect"
					value="{! $CurrentPage.parameters.isRedirect }" />
				<input type="hidden" name="CustomerLastName" id="CustomerLastName"
					value="{! $CurrentPage.parameters.CustomerLastName }" />
				<input type="hidden" name="CustomerEmail" id="CustomerEmail"
					value="{! $CurrentPage.parameters.CustomerEmail }" />
				<input type="hidden" name="AccountType" id="AccountType"
					value="{! $CurrentPage.parameters.AccountType }" />
				<input type="hidden"  name="refreshed" id="refreshed"
					value="{! $CurrentPage.parameters.refreshed }" />
				<input type="hidden" name="when" id="when"
					value="{! $CurrentPage.parameters.when }" />
				<!-- language parameter expected by LiveChatWizard -->
				<input type="hidden" name="fromLanguage" id="fromLanguage"
					value="{! $CurrentPage.parameters.fromLanguage }" />
			</form>

			<header id="header" class="mb-3">
				<nav class="navbar navbar-expand navbar-dark bg-dark" id="oanda_menu">
					<div class="container container-sm">
						<span class="navbar-brand border-secondary py-0 pr-3">
							<img width="133" height="40" src="{! URLFOR($Resource.HelpPortalImg, 
								'oanda-logo-inverse.svg') }" />
						</span>
					</div>
				</nav>
			</header>
			
			<div id="message-container"
				style="display: none"
				class="container container-sm bg-white rounded py-3">

				<apex:outputPanel rendered="{! getActiveEventMessageChat != null }" id="eventMessage" layout="none">
					<h4 class="text-light-green text-center">
						<apex:outputText escape="false" value="{! getActiveEventMessageChat }" />
					</h4>
				</apex:outputPanel>

				<div class="fixed-bottom text-center p-3 bg-white border-top"
					style="min-width:71px;">
					<div id="footer-buttons" style="display: flex;justify-content: space-between;">
						<div id="footer-buttons" style="display: flex;justify-content: flex-start;">
							<button id="nextUrl" type="button" class="btn btn-primary" onclick="page.submit()">
								{! $Label.Proceed_Anyway }
							</button>
						</div>
						<div id="footer-buttons" style="display: flex;justify-content: flex-end;">
							<button onclick="page.done();" type="button" class="btn bg-gray text-muted float-right">
								{! $Label.HelpPortalCancelBtn}
							</button>			
						</div>	
					</div>
				</div>
			</div>

			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script type="text/javascript" src="{! $Resource.ChatCommonScript }"></script>  
			<script>
				var page = {

					isRedirect : false,

					init : function() {
						this.isRedirect = (($("#isRedirect").val() || '') == "true");
						$('#message-container').show();
						pageUIHelper.linkMargins(
							'message-container', 
							'footer-buttons',
							true
						);
					},
					
					submit : function() {
						$('#pageForm').submit();
					},

					done : function() {
						if (this.isRedirect) {
							window.location.href = '{! $Site.BaseUrl }';
							//...
						} else {
							window.close(); 
						}
					}
				};

				$(document).ready(function () {
					page.init();
				});
				
			</script>
		</body>
	</html>
</apex:page>