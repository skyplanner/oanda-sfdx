var app = angular.module("ChatWizardApp", []);

app.directive('validateEmail', function() {
    var EMAIL_REGEXP = /^([a-zA-Z0-9_\-\.]+)@((\[a-z]{1,3}\.[a-z]{1,3}\.[a-z]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,}|[0-9]{1,3})$/;
  
    return {
        require: '?ngModel',
        link: function(scope, elm, attrs, ctrl) {
            // only apply the validator if ngModel is present and AngularJS has added the email validator
            if (ctrl && ctrl.$validators.email) {
                // this will overwrite the default AngularJS email validator
                ctrl.$validators.email = function(modelValue) {
                    return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                };
            }
        }
    };
});

app.controller("chatWizardCtrl", ["$scope",'$window', '$q', '$rootScope',
    function($scope, $window, $q, $rootScope) {
        $scope.step1IsValid = ($("#isCryptoEnabled").val() == 'true');
        $scope.canJumpToStep = parseInt($("#canJumpToStep").val());
        $scope.total = 4;

        //dependent picklist
        $scope.JSTypeAccount = JSON.parse($("#jsTypeAccount").val());
        $scope.JSInquiryNature = JSON.parse($("#jsInquiryNature").val());

        //each picklist field with its pair value,label
        $scope.JSPicklistAccount = JSON.parse($("#jsPicklistAccount").val());
        $scope.JSPicklistInquiry = JSON.parse($("#jsPicklistInquiry").val());
        $scope.JSPicklistQuestion = JSON.parse($("#jsPicklistQuestion").val());
        $scope.JSPicklistDivision = JSON.parse($("#jsPicklistDivision").val());
        /*for controlling values not labels of each option selected 
        in picklist dependents */
        
        $scope.selectedInquiryValue = "";
        $scope.selectedQuestionValue = "";
        $scope.selectedDivision = "";
        $scope.selectedCountry = "";
        $scope.showSpecificQuestion = false;
        $scope.accountDivision = "";
        $scope.accountDivisionName = "";
        $scope.fxAccount = "";
        $scope.ldRecordTypeId = "";
        $scope.crypto = false;
        $scope.cryptoDivisions = "";

        $scope.initValues = function() {
            let customerLastNameParam = $("#customerLastNameParam").val();
            let customerEmailParam = $("#customerEmailParam").val();
            let accountTypeParam = $("#accountTypeParam").val();
            let isAnAccount = $("#isAnAccount").val();
            let accountIsHVC = $("#accountIsHVC").val();
            let accountDivision = $("#accountDivision").val();
            let accountDivisionName = $("#accountDivisionName").val();
            let fxAccount = $("#fxAccount").val();
            let ldRecordTypeId = $("#ldRecordTypeId").val();
            let offlineHours = $("#offlineHours").val();
            let isCryptoValid = $("#isCryptoValid").val();
            let cryptoDivisions = $("#cryptoDivisions").val();
            
            if (customerLastNameParam) {
                $scope.customerLastName = customerLastNameParam;
            }

            if (customerEmailParam) {
                $scope.customerEmail = customerEmailParam;
            }
            
            $scope.matchAccount = (isAnAccount == 'true');
            $scope.isHVC = (accountIsHVC == 'true');
            $scope.offlineHours = (offlineHours == 'true');
            $scope.isCryptoValid = (isCryptoValid == 'true');
            $scope.setStep(parseInt($("#initialStep").val()));
            $scope.totalLabel = $scope.total;

            if (!$scope.step1IsValid) {
                $scope.totalLabel--;
            }

            if (accountDivision) {
                $scope.accountDivision = accountDivision;
            }

            if (accountDivisionName) {
                $scope.accountDivisionName = accountDivisionName;
            } 

            if (fxAccount) {
                $scope.fxAccount = fxAccount;
            }

            if (ldRecordTypeId) {
                $scope.ldRecordTypeId = ldRecordTypeId;
            }

            if (cryptoDivisions) {
                $scope.cryptoDivisions = cryptoDivisions;
            }

            if (accountTypeParam) {
                //We have to do this because in the lists the label is being used as key
                let accTypeLabel = $scope.getAccountTypeLabel(accountTypeParam);
                if (accTypeLabel) {
                    $scope.selectedAccount = accTypeLabel;
                    $scope.typeAccount();
                } else {
                    $scope.selectedAccountValue = '';
                }
            } else {
                $scope.selectedAccountValue = '';
            }
        };

        $scope.getAccountTypeLabel = function(accType) {
            let accTypeInfo = $scope.JSPicklistAccount;
            for (const accTypeLabel in accTypeInfo) {
                if (accType == accTypeLabel) {
                    return accTypeLabel;
                }
                //else...
                let accTypeValue = accTypeInfo[accTypeLabel];
                if (accType == accTypeValue) {
                    return accTypeLabel;
                }
            }
            //else...
            return null;

        };

        $scope.updateStepLabel = function() {
            $scope.stepLabel = $scope.step;

            if (!$scope.step1IsValid) {
                $scope.stepLabel--;
            }
        };

        $scope.setStep = function(newStep) {
            $scope.step = newStep;
            $scope.updateStepLabel();
        };

        $scope.moveNextStep = function() {
            console.log('moveNextStep, begin step: ' + $scope.step);
            if($scope.step == 2) {
                $scope.getClientInfo().then(function(){
                    if($scope.isInvalidCryptoAccount()) {
                        $('#offlineForm').submit();
                        //...
                    } else {
                        $scope.setStep(3); //this fix the multiple clicks problem
                    }
                });                                               
            } else if ($scope.step == 3) {
                if($scope.isInvalidCryptoLead()) {
                    $('#offlineForm').submit();
                } else {
                    $scope.setStep(4); //this fix the multiple clicks problem
                }
            } else {
                if($scope.step < $scope.total) {
                    $scope.setStep($scope.step + 1);
                } else {
                    if ($scope.step == $scope.total) {
                        $scope.submitToNextForm();
                    }
                } 
            } 
            console.log('moveNextStep, end step: ' + $scope.step);
        };
		  
        $scope.movePrevStep = function() {
            const readyToSubmitBack = $scope.step == 1 || 
                ($scope.step == 2 && $scope.step1IsValid == false);

            if (readyToSubmitBack) {
                console.log('submit backForm');
                $('#backForm').submit();
            } else {
                if ($scope.step > 1) {
                    $scope.setStep($scope.step - 1);
                }
            }
        };

        $scope.submitToNextForm = function() {
            let division = '';
            let divisionName = '';
            if ($scope.matchAccount == true) {
                division = $scope.accountDivision;
                divisionName = $scope.accountDivisionName;
                //...
            } else {
                division = $scope.selectedDivision;
            }
            //...
            $('#AccountType').val($scope.selectedAccountValue);
            $('#InquiryNature').val($scope.selectedInquiryValue);
            $('#SpecificQuestion').val($scope.selectedQuestionValue);
            $('#CustomerLastName').val($scope.customerLastName);
            $('#CustomerEmail').val($scope.customerEmail);
            $('#Division').val(division);
            $('#DivisionName').val(divisionName);
            $('#IsAccount').val($scope.matchAccount);
            $('#IsAccountHVC').val($scope.isHVC);
            $('#fxAccount').val($scope.fxAccount);
            $('#ldRecordTypeId').val($scope.ldRecordTypeId);
            $('#Crypto').val($scope.crypto);
            //...
            $('#nextForm').submit();
        };

        $scope.executeJump = function() {
            // The jump is only used the first time
            // from there the jump becomes a single step movement
            const canJumpToStep = $scope.canJumpToStep;
            $scope.canJumpToStep = 2;
            $scope.setStep(canJumpToStep);
        };

        $scope.isInvalidCryptoAccount = function() {
            console.log('isInvalidCryptoAccount -> begin');
            console.log('matchAccount: ' + $scope.matchAccount);
            console.log('offlineHours: ' + $scope.offlineHours);
            console.log('isCryptoValid: ' + $scope.isCryptoValid);
            const result = $scope.crypto && $scope.matchAccount &&
                $scope.offlineHours && !$scope.isCryptoValid;
            console.log('isInvalidCryptoAccount -> result: ' + result);
            return result;
        };

        $scope.isInvalidCryptoLead = function() {
            console.log('isInvalidCryptoLead -> begin');
            console.log('crypto: ' + $scope.crypto);
            console.log('offlineHours: ' + $scope.offlineHours);
            console.log('matchAccount: ' + $scope.matchAccount);
            if (
                (!$scope.crypto) ||
                (!$scope.offlineHours) ||
                $scope.matchAccount
            ) {
                console.log('isInvalidCryptoLead -> result: false');
                return false;
            }
            //else...
            console.log('selectedDivision: ' + $scope.selectedDivision);
            console.log('cryptoDivisions: ' + $scope.cryptoDivisions);
            let result = true;
            if ($scope.cryptoDivisions) {
                result = (
                    $scope.cryptoDivisions.indexOf(
                        ',' + $scope.selectedDivision + ','
                    ) == -1
                );
            }
            console.log('isInvalidCryptoLead -> result: ' + result);
            return result;
        };

        $scope.setCrypto = function() {
            console.log('setCrypto');
            if($scope.isInvalidCryptoAccount()) {
                $('#offlineForm').submit();
                //...
            } else {
                $scope.crypto = true;
                $scope.executeJump();
            }
        };

        $scope.setForexCfds = function() {
            console.log('setForexCfds');
            if($scope.offlineHours) {
                $('#offlineForm').submit();
                //...
            } else {
                $scope.crypto = false;
                $scope.executeJump();
            }
        };
		  
        $scope.closeWindow = function(){
            var isRedirectParam = $("#isRedirectParam").val();
            if(isRedirectParam == 'true') {
                console.log('submit backForm');
                $('#backForm').submit();
            } else {
                $window.close();
            }
        };

        $scope.typeAccount = function () { 
            var inquiryNatureOfAccount = 
                    $scope.JSTypeAccount[$scope.selectedAccount];
            var inquiryNatureList = [];
            for(var i = 0; i < inquiryNatureOfAccount.length; i ++ ){
                inquiryNatureList.push(inquiryNatureOfAccount[i]);
            }
            $scope.inquiryNatureList = inquiryNatureList;     

            //save value of account type according to label selected
            $scope.selectedAccountValue = 
                $scope.JSPicklistAccount[$scope.selectedAccount];
            $scope.selectedInquiryNature = "";   
        };

        $scope.countryChange = function () { 
            console.log('selectedCountry: ' + $scope.selectedCountry);
            if (!$scope.selectedCountry) {
                return;
            }
            //else...
            let countryInfo = $scope.selectedCountry;
            let separatorPos = countryInfo.indexOf('-');
            if (separatorPos != -1) {
                let firstDivisionChar = separatorPos + 1;
                if (firstDivisionChar < countryInfo.length) {
                    let division = countryInfo.substr(firstDivisionChar);
                    $scope.selectedDivision = division;
                    console.log('division: ' + division);
                }
            }
        };

        $scope.inquiryNatureChange = function(){
            var specificQuestion = 
                    $scope.JSInquiryNature[$scope.selectedInquiryNature];
            
            if(specificQuestion != null){
                var specificQuestionList = [];
                for(var i = 0; i < specificQuestion.length; i ++ ){
                    specificQuestionList.push(specificQuestion[i]);
                }
                $scope.specificQuestionList = specificQuestionList;
                $scope.showSpecificQuestion = true;
            } 
            else{
                $scope.showSpecificQuestion = false;  
            } 

            //save value of inquiry nature according to label selected
                $scope.selectedInquiryValue = 
                    $scope.JSPicklistInquiry[$scope.selectedInquiryNature];            
        };

        $scope.specificQuestionChange = function(){
           $scope.selectedQuestionValue = 
                 $scope.JSPicklistQuestion[$scope.selectedSpecificQuestion];
        };
        
        $scope.getClientInfo = function() {
            var email = $scope.customerEmail;
            var deferred = $q.defer();
            Visualforce.remoting.Manager.invokeAction(
                'LiveChatWizardCtrl.getClientInfoByEmail', 
                email,
                function(result, event){
                    if (event.status){
                        $scope.error = null;
                        if(result != null){
                            console.log("getClientInfo result: " + JSON.stringify(result));
                            $scope.matchAccount = result.isAnAccount;
                            $scope.isHVC = result.isHVC;
                            $scope.isCryptoValid = result.isCryptoValid;
                            $scope.accountDivision = result.division;                          
                            $scope.accountDivisionName = result.divisionName;                   
                            $scope.fxAccount = result.fxAccount;                          
                            $scope.ldRecordTypeId = result.ldRecordTypeId;
                        } else {
                            console.log("getClientInfo result is null");
                            $scope.matchAccount = false;
                            $scope.isHVC = false;
                            $scope.isCryptoValid = false;
                            $scope.accountDivision = '';
                            $scope.accountDivisionName = '';
                            $scope.fxAccount = '';
                            $scope.ldRecordTypeId = '';
                        }
                        deferred.resolve(result); 
                    } else {
                        $scope.error = event.message;
                    }                   
                    $scope.$apply();
                }, 
                {escape: false, buffer: false, timeout: 120000}
            );
            return deferred.promise;
        };

        //initialization from params
        $scope.initValues();
    }]); 