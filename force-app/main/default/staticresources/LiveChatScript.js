
$(document).ready(function () {
  console.log('LiveChatScript -> begin');
  $('form').attr('autocomplete', 'off');
  /*
  $('.liveAgentQueuePosition').on('DOMSubtreeModified', function () {
      showSpinner();
      var pos = $(this).html();
      updateavgWaitTime(pos);
      
      hideSpinner();
  });
  */
});  

function updateavgWaitTime(pos) {
  var avgWaitTime = $('#avgWaitTime').val();
  if (pos != '') {
    //   var total = parseInt(pos) * parseInt(avgWaitTime);     
      var total =  parseInt(avgWaitTime);
      if(total > 0){          
          var hours = extractHours(total);
          var mins = extractMins(total);
          var secs = extractSeconds(total);
          showTimeMsg();
  /******** Check if there is hours to show *******/  
          if(hours > 0){
            $('#avgTimeHours').html(hours);
            showHours(hours);                           
          }
          else
            hideHours();

  /******** Check if there is minutes to show*****/  
          if(mins > 0){
            $('#avgTimeMins').html(mins);
            showMinutes(mins);
          }
          else
            hideMinutes();                      
        
  /******** Check if there is second, and show only if there are not minutes and hours *****/ 
        if(secs > 0 && mins == 0 && hours == 0){
          $('#avgTimeSecs').html(secs);
          showSeconds(secs);              
        }
        else{          
          hideSeconds();
        }                
      }
      else{
          hideTimeMsg();
          hideMinutes();
          hideHours();
      }     
  }
}

function extractHours(s){
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;
  return hrs;
}

function extractMins(s){
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60; 
  return mins;
}

function extractSeconds(s){
  var secs = s % 60;
  return secs;
}


function showSpinner(){
  $('#chatSpinner').css('display', 'block');
  $('#queueSpinner').css('display', 'block');
}

function hideSpinner(){
  $('#chatSpinner').css('display', 'none');
  $('#queueSpinner').css('display', 'none');
}

function showMinutes(mins){
  $('#avgTimeMins').css('display', 'inline');
  if(mins == 1){
    $('#textAvgTimeMin').css('display', 'inline');
    $('#textAvgTimeMins').css('display', 'none');
  }    
  else{
    $('#textAvgTimeMins').css('display', 'inline'); 
    $('#textAvgTimeMin').css('display', 'none'); 
  } 
}

function hideMinutes(){
  $('#avgTimeMins').css('display', 'none');
  $('#textAvgTimeMins').css('display', 'none');
  $('#textAvgTimeMin').css('display', 'none');
}

function showHours(hours){
  $('#avgTimeHours').css('display', 'inline');
  if(hours == 1){
    $('#textAvgTimeHour').css('display', 'inline');
    $('#textAvgTimeHours').css('display', 'none');
  }    
  else{
    $('#textAvgTimeHours').css('display', 'inline'); 
    $('#textAvgTimeHour').css('display', 'none'); 
  }
       
}
function hideHours(){
  $('#avgTimeHours').css('display', 'none');
  $('#textAvgTimeHours').css('display', 'none');
  $('#textAvgTimeHour').css('display', 'none');
}

function showSeconds(secs){
  $('#avgTimeSecs').css('display', 'inline');
  if(secs == 1){
    $('#textAvgTimeSec').css('display', 'inline');
    $('#textAvgTimeSecs').css('display', 'none');
  }    
  else{
    $('#textAvgTimeSecs').css('display', 'inline');
    $('#textAvgTimeSec').css('display', 'none'); 
  }
 
}

function hideSeconds(){
  $('#avgTimeSecs').css('display', 'none');
  $('#textAvgTimeSecs').css('display', 'none');
  $('#textAvgTimeSec').css('display', 'none');
}

function showTimeMsg(){
  $('#timeText').css('display', 'inline');
}

function hideTimeMsg(){
  $('#timeText').css('display', 'none');
}