 function initHeaderCmp() {
            if (!jQuery) {
                console.log('jQuery not defined yet header');
            } else {
                console.log('jQuery defined header');               
                alertBar(".js-cookie", 365);
                resizeHeader();
                
                $(window).on('resize', function () {                    
                    resizeHeader();
                });
            }
        }

        function resizeHeader() {
            var heightRiskDisclaimer = $("#risk-disclaimer").innerHeight();
            console.log("Height "+ heightRiskDisclaimer);
            if (heightRiskDisclaimer != null)
                $("#oanda_menu").css('margin-top', heightRiskDisclaimer);
        }

        function alertBar(targetElem, expires) {
            let thisBar = $(targetElem);
            let cookieName = "cookie_id-" + thisBar.attr("data-alert-id");
            let thisBar_accept = thisBar.find('.js-close');
            let checkCookie = Cookies.get(cookieName);
            thisBar_accept.click(function (event) {
                event.preventDefault();
                if (expires > 0) {
                    Cookies.set(cookieName, true, { expires: expires });
                } else {
                    // Session cookie
                    Cookies.set(cookieName, true);
                }
                thisBar.addClass("d-none");
                resizeHeader();
            });
            //* If there is an alert but no corresponding cookie, the alert is shown.
            if (!checkCookie) {
                thisBar.removeClass("d-none");
            }
        }      