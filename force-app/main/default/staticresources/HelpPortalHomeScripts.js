/**
 * @author Fernando Gomez
 * @version 1.0
 * @since March 18th, 2019
 */
 angular.module("HelpPortal", [
	"ngRoute",
	"ngSanitize",
	"ngCookies"
])
.factory("$helper", [
	"$q",
function($q) {
	var $helper = {
		templates: {
			search: $("#searchTemplate").val(),
			rate: $("#rateTemplate").val(),
			spinner: $("#spinnerTemplate").val()
		},
		saveUrlPattern: function(originalPath, keys) {
			
		},
		scrollTop: function() {
			$("html,body").animate({
				"scrollTop": 0
			}, 200);
		},
		checkWhenParam: function(path) {
			console.log('window.location.href: ' + path);
			var newLocation = null;
			var whenParam = 'when=';
			if (path && (path.indexOf(whenParam) == -1)) {
				console.log('no when param, redirect !!!!');
				var sep = '&';
				if (path.indexOf('?') == -1) {
					sep = '?';
				}
				var angularPrefix = '#!';
				var pos = path.indexOf(angularPrefix);
				if (pos != -1) {
					newLocation = path.substring(0, pos);
					newLocation += (sep + "when=" + $.now());
					newLocation += path.substring(pos);
				} else {
					newLocation = path + (sep + "when=" + $.now());
				}
			}
			return newLocation;
		},
		//...
		checkCountryCode: function(countryCode, path) {
			var newLocation = null;
			console.log('countryCode: ' + countryCode);
			console.log('location.path: ' + path);

			if ((!countryCode) || (countryCode == '')) {
				console.log('no countryCode, redirect !!!!');
				var articlePrefix = '/article/';
				var p1 = null;
				var p2 = null;
				if (path && (path.indexOf(articlePrefix) == 0)) {
					var params = path.substr(articlePrefix.length);
					var sepPos = params.indexOf('/');
					if (sepPos != 1) {
						p1 = params.substring(0, sepPos);
						if ((sepPos + 1) < params.length) {
							p2 = params.substring(sepPos + 1);
						}
					} else {
						p1 = params;
					}
				}
				newLocation = '/helpportalcountryselection';
				if (p1) {
					newLocation += ('?artP1=' + p1);
					if (p2) {
						newLocation += ('&artP2=' + p2);
					}
				}
			}
			return newLocation;
		},
		ctrl: {
			search: function(language, searchText, division) {
				return $helper.ctrl._call("search", [language, searchText, division]);
			},
			submitRating: function(articleId, rating, ranking, comments, language) {
				return $helper.ctrl._call("submitRating",
					[articleId, rating, ranking, comments, language]);
			},
			_call: function(method, params) {
				var deferred = $q.defer();
				HelpPortalHomeCtrl[method].apply(
					HelpPortalHomeCtrl,
					params.concat([
						function(result, event) {
							if (event.status)
								deferred.resolve(result);
							else
								deferred.reject(event.message);
						}, {
							escape: false,
							buffer: false,
							timeout: 120000
						}
					]));
				return deferred.promise;
			}
		}
	};
	return $helper;
}])
.config([
	"$routeProvider",
	function($routeProvider) {
		// we configure the route as this is a wizzard.
		// not all pages are loading at the same time
		$routeProvider
			.when("/", {
				cache: false,
				templateUrl: function(params) {
					return "/apex/HelpPortalWelcome" +
						"?language=" + $("#languageCode").val() +
						"&division=" + $("#divisionDataCategory").val() +
						"&when=" + $.now();
				},
				controller: "welcomeCtrl"
			})
			.when("/article/:knowledgeArticleId/:faqId/", {
				cache: false,
				templateUrl: function(params) {
					return "/apex/HelpPortalArticle" +
						"?id=" + params.knowledgeArticleId + 
						"&articleId=" + params.faqId + 
						"&language=" + $("#languageCode").val() +
						"&when=" + $.now();
				},
				controller: "articleCtrl"
			})
			.when("/search/:pageNo/:pageSize/:criteria", {
				cache: false,
				templateUrl: function(params) {
					var trustedSearch =  decodeURIComponent(
						decodeURIComponent(params.criteria))
						.replace(/[\u00A0-\u9999<>\&]/g, function(i) {
						return '&#'+i.charCodeAt(0)+';';
					 });
					return "/apex/HelpPortalSearch" +
						"?search=" +  trustedSearch + 
						"&pageNo=" + params.pageNo +
						"&pageSize=" + params.pageSize +
						"&language=" + $("#languageCode").val() +
						"&division=" + $("#divisionDataCategory").val() +
						"&when=" + $.now();
				},
				controller: "searchCtrl"
			})
			.when("/category/:pageNo/:pageSize/:category", {
				cache: false,
				templateUrl: function(params) {
					return "/apex/HelpPortalSearch" +
						"?category=" + params.category + 
						"&pageNo=" + params.pageNo +
						"&pageSize=" + params.pageSize +
						"&language=" + $("#languageCode").val() +
						"&division=" + $("#divisionDataCategory").val() +
						"&when=" + $.now();
				},
				controller: "categoryCtrl"
			})
			.when("/:language/error", {
				cache: false,
				templateUrl: function(params) {
					return "/apex/HelpPortalError" +
						"?language=" + $("#languageCode").val();
				}
			})
			.otherwise({
				cache: false,
				templateUrl: '/apex/HelpPortal404?division='+
					$("#divisionDataCategory").val()
			});
	}
])
.run([
	"$helper",
	"$rootScope",
	"$location",
	"$window",
	function($helper, $rootScope, $location, $window) {
		var newLocation = $helper.checkWhenParam($window.location.href);
		if (!newLocation) {
			var countryCode = $("#countryCode").val();
			newLocation = $helper.checkCountryCode(countryCode, $location.path());
		}
		if (newLocation) {
			$window.location.href = newLocation;
			return;
		}
		//else...
		$rootScope.$on("$routeChangeStart", $helper.scrollTop);
		$rootScope.$on("$routeChangeError", 
			function(event, current, previous) {
				$location.path("/" + current.params.language + "/error");
			});
	}
])
.controller("mainCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$timeout",
	"$window",
	"$cookies",
	function($scope, $helper, $location, $timeout, $window, $cookies) {
		$scope.selection = null;
		$scope.urlPattern = 

		// selection is only changed for generals pages...
		// inner workings should be local variables
		$scope.$on("$itemSelected", function(event, args) {
			$scope.selection = args;
		});

		// we now can setup the chat thingy...
		/*$timeout(function() {
			$Lightning.use("nfchat:Chat",
				function() {
					$Lightning.createComponent(
						"nfchat:ChatBot", {
							"bot": "Support_PROD"
						},
						"chatBotContainer");
				},
				$window.location.origin
			);
		}, 200);*/

		// redirect to the same page, with a different language
		$scope.changeLanaguage = function(language) {
			console.log(language);
			var path = "#!" + $location.path();
			if($location.path().includes("article")){
				path = "#!";
			}
			var url = $window.location.protocol + "//" +
				$window.location.host + "?language=" + language +
				"&change=" + language +
				"&when=" + $.now() + path;

			$window.location.href = url;
		};

		//redirect to the same page, with a different language
		$scope.changeRegion = function(region) {
			console.log(region);
		    /* Use an array to control number of change for region	*/
		   /*var regionCookie = $cookies.get('apex__region_number');
			var regionJSON = JSON.parse(regionCookie);
			console.log(regionJSON[region]);		
			var count = regionJSON[region] + 1;
			if(count == 3){
			   console.log('ENTRO');
			   var actualRegion = $cookies.get('apex__prefered_region');
			   console.log('Actual Region '+ actualRegion)
			   $cookies.put('apex__prefered_region', region);
			   for (var i=0; i < regionJSON.length; i++) {
			    	regionJSON[i]=0;				
			   }			   
			}
			else{
				console.log('ELSE');
				regionJSON[region]=count;				
			}
			$cookies.put('apex__region_number', JSON.stringify(regionJSON));
			console.log(' Despues ' + JSON.stringify(regionJSON)); */
		
		/*	var actualRegion = $cookies.get('apex__prefered_region'); 
			console.log('Actual Region '+ actualRegion)
			$cookies.put('apex__prefered_region', region);
			console.log('Change Region to ' + region);*/
			
			/** Redirect to the same page **/
			var path = "#!" + $location.path();
			if($location.path().includes("article")){
				path = "#!";
			}
			var url = $window.location.protocol + "//" +
				$window.location.host + "?changeRegion=" + region +
				"&when=" + $.now() + path;	
			$window.location.href = url;
		};
	}
])
.controller("welcomeCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$rootScope",
	function($scope, $helper, $location, $rootScope) {
		$rootScope.$broadcast("$itemSelected", {
			type: "welcome",
			shrinkHeader: false,
			showSearch: false,
			showPopularFaqs: false,
			showFaqCategories: false,
			showArticlesPerCategory: true
		});
	}
])
.controller("articleCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$rootScope",
	"$routeParams",
	function($scope, $helper, $location, $rootScope, $routeParams) {
		/**
		 * Handles interations with 
		 * he current article.
		 */
		var languageCode = $("#languageCode").val() != ""?
		$("#languageCode").val():"en_US";
		$scope.article = {
			isRating: false,
			language: languageCode, 
			rating: {
				isBusy: false,
				step: 0,
				likes: null,
				rate: null,
				completed: false,
				comments: null,
				hasError: false,
				message: null
			},
			openRate: function() {
				$scope.article.isRating = true;
				$scope.article.rating.isBusy = false;
				$scope.article.rating.step = 0;
				$scope.article.rating.likes = null;
				$scope.article.rating.ranking = null;
				$scope.article.rating.completed = false;
				$scope.article.rating.comments = null;
				$scope.article.rating.hasError = false;
				$scope.article.rating.message = null;
			},
			cancelRate: function() {
				$scope.article.isRating = false;
			},
			submitOverallOpinion: function(likes) {
				$scope.article.rating.likes = likes;
			},
			submitRating: function(likes) {
				$scope.article.rating.isBusy = true;
				$scope.article.rating.hasError = false;
				$helper.ctrl.submitRating(
					$routeParams.faqId,
					$scope.article.rating.likes ? 'Like' : 'Dislike',
					$scope.article.rating.ranking,
					$scope.article.rating.ranking > 3 ? null :
					$scope.article.rating.comments,
					$scope.article.language).then(
					function() {
						$scope.article.rating.isBusy = false;
						$scope.article.rating.completed = true;
					},
					function(error) {
						$scope.article.rating.isBusy = false;
						$scope.article.rating.hasError = true;
						$scope.article.rating.message = error;
					});
			}
		};

		$rootScope.$broadcast("$itemSelected", {
			type: "article",
			id: $routeParams.knowledgeArticleId, 
			//id: $routeParams.faqId,
			shrinkHeader: true,
			showSearch: true,
			showPopularFaqs: true,
			showFaqCategories: true,
			showArticlesPerCategory: false
		});
	}
])
.controller("searchCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$rootScope",
	"$routeParams",
	function($scope, $helper, $location, $rootScope, $routeParams) {
		$rootScope.$broadcast("$itemSelected", {
			type: "search",
			criteria: $routeParams.criteria,
			shrinkHeader: true,
			showSearch: true,
			showPopularFaqs: true,
			showFaqCategories: true,
			showArticlesPerCategory: false
		});
	}
])
.controller("categoryCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$rootScope",
	"$routeParams",
	function($scope, $helper, $location, $rootScope, $routeParams) {
		$rootScope.$broadcast("$itemSelected", {
			type: "category",
			name: $routeParams.category,
			shrinkHeader: true,
			showSearch: true,
			showPopularFaqs: true,
			showFaqCategories: true,
			showArticlesPerCategory: false
		});
	}
])
.directive("spinner", [
	"$helper",
	"$timeout",
	function($helper, $timeout) {
		return {
			restrict: 'E',
			replace: true,
			scope: true,
			templateUrl: $helper.templates.spinner,
			link: function($scope, $elem, $attrs) {
				$scope.show = function() {
					$elem.show("fast");
				};
				$scope.hide = function() {
					$timeout(function() {
						$elem.fadeOut("slow");
					}, 300);
				};
				$elem.hide();

				$scope.$on("$locationChangeStart", $scope.show);
				$scope.$on("$locationChangeSuccess", $scope.hide);
				$scope.$on("$locationChangeError", $scope.hide);
			}
		};
	}
])
.directive("search", [
	"$helper",
	"$location",
	"$timeout",
	"$sce",
	function($helper, $location, $timeout, $sce) {
		var text = $("#searchText").val() != ""?
		$("#searchText").val():"Search here...";
		return {
			restrict: "E",
			replace: true,
			scope: {
				model: "=ngModel",
				language: "@language",
				division: "@division"
			},
			templateUrl: $helper.templates.search,
			link: function($scope, $elem, $attrs) {
				/**
				 * Submits the search form
				 */
				$scope.search = {
					criteria: null,
					isMenuActive: false,
					faqs: [],
					placeholder: $("#searchText").val() ?
						$("#searchText").val() : "Search here...",
					submit: function() {
						$scope.search.criteria
							= $sce.getTrustedHtml($scope.search.criteria);
							
						if ($scope.search._isValid()) {
							$scope.search._shutDown();
							$location.path("/search/0/20/" +
								encodeURIComponent($scope.search.criteria));
						}
					},
					search: function() {
						if ($scope.search._isValid()) {
							$scope.search.isMenuActive = true;
							$helper.ctrl.search(
								$scope.language,
								$scope.search.criteria,
								$scope.division).then(
								function(result) {
									$scope.search.faqs = result;
								},
								function(error) {

								});
						} else {
							$scope.search._shutDown();
						}
					},
					abort: function() {
						$timeout($scope.search._shutDown, 300);
					},
					_isValid: function() {
						return $scope.search.criteria && 
							$scope.search.criteria.length >= 3;
					},
					_shutDown: function() {
						$scope.search.isMenuActive = false;
						$scope.search.faqs = [];
					}
				};

				$scope.$on("$itemSelected", function(event, args) {
					if (args.type != "search")
						$scope.search.criteria = null;
				});
			}
		};
	}
])
.directive("rate", [
	"$helper",
	"$location",
	"$timeout",
	function($helper, $location, $timeout) {
		return {
			restrict: "E",
			replace: true,
			scope: {
				model: "=ngModel",
				disabled: "=ngDisabled"
			},
			templateUrl: $helper.templates.rate,
			link: function($scope, $elem, $attrs) {
				// we clone the current rating
				$scope.rate = $scope.model || null;

				/**
				 * Records a specific rate
				 * 1 to 5
				 */	
				$scope.doRate = function(rate) {
					$scope.rate = rate;
				};

				/**
				 * Removes any pending rating
				 */	
				$scope.unRate = function(rate) {
					$scope.rate = $scope.model;
				};

				/**
				 * Submits a rating.
				 */
				$scope.submit = function(rate) {
					$scope.rate = rate;
					$scope.model = rate;
				}
			}
		};
	}
])
.directive("anchor", [
	"$anchorScroll",
	"$location",
	function($anchorScroll, $location) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				var href = $attrs.href.replace("#", "");
				// we override the href foor good
				// $elem.attr("href", "javascript:void(0)");
				// we also need to make sure the target attr does not take us
				// to another page
				$elem.removeAttr("target");

				// anchor links should use this function
				$elem.on("click", function(e) {
					e.preventDefault();
					$location.hash(href);
					$anchorScroll();
				});
			}
		};
	}
])
.directive("openChat", [
	"$window",
	function($window) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				$elem.removeAttr("target");

				// anchor links should use this function
				$elem.on("click", function(e) {
					e.preventDefault();
					$window.open(
						'/LiveChatLanguage',
						'Oanda Live Chat',
						'scrollbars=1,width=500,height=640');
				});
			}
		};
	}
])
.directive("wrapTable", [
	"$anchorScroll",
	"$location",
	function($anchorScroll, $location) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				$elem.wrap(
					'<div class="table-container border overflow-auto mb-5"></div>');
			}
		};
	}
])
.filter("trust", [
	"$sce",
	function($sce) {
		return function(input) {
			return $sce.trustAsHtml(input);
		};
	}
]);
