var footerCmpMobileLandscape = "(max-width:768px)";
function localNavCollapse() {
	var menuNames = $('.local-nav__menu_name');
	menuNames.on('click', function (event) {
		if (window.matchMedia(footerCmpMobileLandscape).matches) {
			console.log('menuNames.onClick window.matchMedia = true');
			var container = $(this).parents('.local-nav__menu');
			var links = $(this).siblings('.local-nav__menu_links');
			if (container.hasClass('local-nav__menu--open')) {
				//links.css('max-height', 0);
				links.css('max-height', '');
				container.removeClass('local-nav__menu--open');
			} else {
				links.css('max-height', links.prop('scrollHeight'));
				container.addClass('local-nav__menu--open');
			}
		} else {
			console.log('menuNames.onClick window.matchMedia = false');
		}
	});
}

function initFooterCmp() {
	if (!jQuery) {
		console.log('jQuery not defined yet');
	} else {
		console.log('jQuery defined ok');
		jQuery(function() {
			localNavCollapse();
		});
	}
}