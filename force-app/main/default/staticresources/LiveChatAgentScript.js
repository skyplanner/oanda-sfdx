/**
 * @File Name          : LiveChatAgentScript.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 8/12/2022, 12:27:00 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/3/2021, 10:08:38 AM   acantero     Initial Version
**/

var constants = {
    HELP_PORTAL_URL : '',
    API_ROOT : '',
    DEPLOY_PARAM_1 : '',
    DEPLOY_PARAM_2 : '',
    //...
    HELP_PORTAL_LABEL : '',
    CLICK_HERE_LABEL : ''
};

var agent = {

    defaultChatButtonId : '',
    oneChatButtonForAll : false,
    isAPostchatRedirect : false,
    automaticConnection : false,
    agentsAvailable : false,
    isEnglish : false,
    validChatDetails : false,
    defaultLanguage : '',
    defaultLanguageLabel : '',
    languagePreference : '',
    //...
    liveagent : null,


    getEmptyChatDetails : function() {
        return {
            buttonId : this.defaultChatButtonId,
            systemSubject : 'Chat',
            systemStatus : 'Open',
            languagePreferenceLabel : '',
            languageCorrected : '',
            divisionName : '',
            fromPreChatForm : true,
            customerLastName : '',
            customerEmail : '',
            languagePreference : '',
            inquiryNatureCase : '',
            chatDivision : '',
            typeCase : '',
            subtypeCase : '',
            accountType : '',
            isAccount : false,
            accountHVC : 'No',
            fxAccount : '',
            ldRecordTypeId : '',
            crypto : false,
            cryptoCase : 'No'
        };
    },

    prepareChatDetails : function(chatDetails) {
        var initFromOldValues = false;
        var oldChatDetails = null;
        if (this.isAPostchatRedirect) {
            var oldChatDetailsStr = $("#oldChatDetails").val() || '';
            if (oldChatDetailsStr != '') {
                oldChatDetails = JSON.parse(oldChatDetailsStr);
                if (
                    oldChatDetails && 
                    oldChatDetails.customDetails && 
                    (oldChatDetails.customDetails.length > 0)
                ) {
                    initFromOldValues = true;
                }
            }
        }
        console.log("initFromOldValues: " + initFromOldValues);
        if (initFromOldValues) {
            this.initChatDetailsFromOldValues(chatDetails, oldChatDetails);
            //set language to default lang (english)
            chatDetails.languagePreference = this.defaultLanguage;
            chatDetails.languagePreferenceLabel = this.defaultLanguageLabel;
            chatDetails.languageCorrected = this.defaultLanguageLabel;
            //...
        } else {
            this.initChatDetailsFromParams(chatDetails);
        }
        this.validChatDetails = (chatDetails.customerEmail != '');
        console.log("validChatDetails: " + this.validChatDetails);
        console.log("chatDetails: " + JSON.stringify(chatDetails));
    },

    initChatDetailsFromParams : function(chatDetails) {
        var divisionName = $("#DivisionName").val() || '';
        var accountHVC = (($("#IsHVC").val() || '') == "true")
            ? 'Yes'
            : 'No';
        //...
        chatDetails.buttonId = this.defaultChatButtonId;
        chatDetails.systemSubject = 'Chat';
        chatDetails.systemStatus = 'Open';
        chatDetails.languagePreferenceLabel = $("#LanguageLabel").val() || '';
        chatDetails.languageCorrected = $("#LanguageCorrected").val() || '';
        chatDetails.divisionName = divisionName;
        chatDetails.fromPreChatForm = true;
        chatDetails.customerLastName = $("#CustomerLastName").val() || '';
        chatDetails.customerEmail = $("#CustomerEmail").val() || '';
        chatDetails.languagePreference = $("#LanguagePreference").val() || '';
        chatDetails.inquiryNatureCase = $("#InquiryNatureMapped").val() || '';
        chatDetails.chatDivision = $("#Division").val() || '';
        chatDetails.typeCase = $("#ServiceType").val() || '';
        chatDetails.subtypeCase = $("#ServiceSubtype").val() || '';
        chatDetails.accountType = $("#AccountType").val() || '';
        chatDetails.isAccount = (($("#IsAccount").val() || '') == "true");
        chatDetails.accountHVC = accountHVC;
        chatDetails.fxAccount = $("#fxAccount").val() || '';
        chatDetails.ldRecordTypeId = $("#ldRecordTypeId").val() || '';
        chatDetails.crypto = (($("#Crypto").val() || '') == "true");
        chatDetails.cryptoCase = (chatDetails.crypto)
            ? 'Yes'
            : 'No';
    },

    initChatDetailsFromOldValues : function(chatDetails, oldChatDetails) {
        for(var i = 0; i < oldChatDetails.customDetails.length; i++) {
            var detail = oldChatDetails.customDetails[i];
            if (!detail.label) {
                continue;
            }
            //else...
            var dotPos = detail.label.indexOf('.');
            var spacePos = detail.label.indexOf(' ');
            if (
                (dotPos == -1) &&
                (spacePos == -1) 
            ) {
                var validFieldName = detail.label.charAt(0).toLowerCase();
                if (detail.label.length > 1) {
                    validFieldName += detail.label.substring(1);
                }
                chatDetails[validFieldName] = detail.value;
            }
        }
    },

    setup: function() {
        console.log('setup begin');
        console.log('defaultChatButtonId: ' + this.defaultChatButtonId);
        console.log('oneChatButtonForAll: ' + this.oneChatButtonForAll);
        console.log('isAPostchatRedirect: ' + this.isAPostchatRedirect);
        console.log('automaticConnection: ' + this.automaticConnection);
        console.log('agentsAvailable: ' + this.agentsAvailable);
        console.log('isEnglish: ' + this.isEnglish);
        if (this.defaultChatButtonId == '') {
            var noChatButtonErrorMsg = 'FATAL ERROR -> defaultChatButtonId is empty';
            alert(noChatButtonErrorMsg);
            console.error(noChatButtonErrorMsg);
            return;
        }
        //else...
        this.prepareChatPanels();
        this.registerChatPanels();
        this.setupChat();
        if (this.automaticConnection && (!this.isAPostchatRedirect)) {
            this.safeStartDefaultChat();
        }
    },

    getTryLinkCode : function(linkText) {
        return '<a href="#" onclick="agent.tryChat()" class="custom-link">' +
            linkText +
            '</a>';
    },

    getHelpPortalLinkCode : function() {
        return '<a href="' + constants.HELP_PORTAL_URL + '" class="custom-link">' +
            constants.HELP_PORTAL_LABEL +
            '</a>';
    },

    prepareChatPanels : function() {
        var unableToConnectMsg = $('#unableToConnectMsg').val();
        this.prepareNoAgentsPanel(unableToConnectMsg);
    },

    refreshNoAgentsPanelMsg : function() {
        var unableToConnectMsg = $('#unableToConnectMsgDefaultLang').val();
        this.prepareNoAgentsPanel(unableToConnectMsg);
    },

    prepareNoAgentsPanel : function(unableToConnectMsg) {
        var msgCode = unableToConnectMsg;
        msgCode = msgCode.replace(
            /\[TRY_LINK\]/g, 
            this.getTryLinkCode(constants.CLICK_HERE_LABEL)
        );
        msgCode = msgCode.replace(
            /\[HELP_PORTAL_LINK\]/g, 
            this.getHelpPortalLinkCode()
        );
        $('#noAgentsPanelMsg').html(msgCode);
    },

    registerChatPanels : function() {
        var showWhenOnlineEl = 'fakeOnlineElement'; //chatButtonPanel
        var showWhenOfflineEl = 'fakeOfflineElement'; //noAgentsPanel
        if (
            (!this.automaticConnection) && 
            this.agentsAvailable
        ) {
        	if (this.oneChatButtonForAll) {
                //liveAgent is not expected to show the button
                $('#chatButtonPanel').show();
                //...
            } else {
                //liveAgent is responsible for displaying the button
                showWhenOnlineEl = 'chatButtonPanel';
                showWhenOfflineEl = 'noAgentsPanel';
            }
        }
        console.log('showWhenOnlineEl: ' + showWhenOnlineEl);
        console.log('showWhenOfflineEl: ' + showWhenOfflineEl);
        //...
        if (!window._laq) { 
            window._laq = []; 
        }
        var self = this;
        window._laq.push(
            function() {
                liveagent.showWhenOnline(
                    self.defaultChatButtonId, 
                    document.getElementById(
                        showWhenOnlineEl
                    )
                );
                liveagent.showWhenOffline(
                    self.defaultChatButtonId,
                    document.getElementById(
                        showWhenOfflineEl
                    )
                );
            }
        );
    },

    setupChat: function() {
        console.log('setupChat begin'); 
        var chatDetails = this.getEmptyChatDetails();
        this.prepareChatDetails(
            chatDetails
        );
        console.log('validChatDetails: ' + this.validChatDetails); 
        if (!this.validChatDetails) {
            return;
        }
        //else...
        this.languagePreference = chatDetails.languagePreference;
        console.log('languagePreference: ' + this.languagePreference); 
        //...
        liveagent.addCustomDetail('liveagent.prechat.buttons', chatDetails.buttonId);
        liveagent.addCustomDetail("System Subject", chatDetails.systemSubject);
        liveagent.addCustomDetail("System Status", chatDetails.systemStatus);
        liveagent.addCustomDetail("LanguagePreferenceLabel", chatDetails.languagePreferenceLabel);
        liveagent.addCustomDetail("LanguageCorrected", chatDetails.languageCorrected);
        liveagent.addCustomDetail("IsAccount", chatDetails.isAccount);
        liveagent.addCustomDetail("CryptoCase", chatDetails.cryptoCase);
        
        liveagent
            .addCustomDetail("FromPreChatForm", chatDetails.fromPreChatForm)
            .saveToTranscript('From_Pre_Chat_Form__c');
        liveagent
            .addCustomDetail("CustomerLastName", chatDetails.customerLastName)
            .saveToTranscript('Chat_Last_Name__c');
        liveagent
            .addCustomDetail("CustomerEmail", chatDetails.customerEmail)
            .saveToTranscript('Chat_Email__c');
        liveagent
            .addCustomDetail("LanguagePreference", chatDetails.languagePreference)
            .saveToTranscript('Chat_Language_Preference__c');
        liveagent
            .addCustomDetail("InquiryNatureCase", chatDetails.inquiryNatureCase) 
            .saveToTranscript('Inquiry_Nature__c');	
        liveagent
            .addCustomDetail("ChatDivision", chatDetails.chatDivision)
            .saveToTranscript('Chat_Division__c');
        liveagent
            .addCustomDetail("DivisionName", chatDetails.divisionName)
            .saveToTranscript('Division_Name__c');		
        liveagent
            .addCustomDetail("TypeCase", chatDetails.typeCase) 
            .saveToTranscript('Type__c');
        liveagent
            .addCustomDetail("SubtypeCase", chatDetails.subtypeCase) 
            .saveToTranscript('Subtype__c');
        liveagent
            .addCustomDetail("AccountType", chatDetails.accountType)
            .saveToTranscript('Chat_Type_of_Account__c');
        liveagent
            .addCustomDetail("AccountHVC", chatDetails.accountHVC)
            .saveToTranscript('Is_HVC__c');	
        liveagent
            .addCustomDetail("Crypto", chatDetails.crypto)
            .saveToTranscript('Spot_Crypto__c');
        liveagent
            .addCustomDetail("fxAccount", chatDetails.fxAccount);
        liveagent
            .addCustomDetail("Lead Record Type", chatDetails.ldRecordTypeId);
        liveagent
            .addCustomDetail("Case Record Type", "012U00000004rqHIAQ");

        //Case associated with Chat Tranascript
        liveagent
            .findOrCreate("Case")
            .map("Subject", "System Subject", false, false, true)
            .map("Status", "System Status", false, false, true)
            .map("Origin", "System Subject",false, false ,true)
            .map("Department__c", "InquiryNature", false, false, true)
            .map("Chat_Email__c", "CustomerEmail", false, false, true)
            .map("Chat_Is_HVC__c", "AccountHVC", false, false, true)
            .map("Chat_Language_Preference__c", "LanguagePreferenceLabel", false, false, true)
            .map("Language_Corrected__c", "LanguageCorrected", false, false, true)
            .map("Chat_Type_of_Account__c", "AccountType", false, false, true)
            .map("Live_or_Practice__c", "AccountType", false, false, true)
            .map("Chat_Last_Name__c", "CustomerLastName", false, false, true)
            .map("Chat_Division__c", "ChatDivision" , false, false, true)
            .map("Division__c", "DivisionName" , false, false, true)
            .map("Inquiry_Nature__c", "InquiryNatureCase", false, false, true)
            .map("Type__c", "TypeCase", false, false, true)
            .map("Subtype__c", "SubtypeCase", false, false, true)
            .map("Chat_Spot_Crypto__c", "CryptoCase", false, false, true)
            .map("RecordTypeId","Case Record Type",false,false,true)
            .saveToTranscript("CaseId")
            .showOnCreate();	
            

        if (chatDetails.isAccount) {
            liveagent
                .findOrCreate("Contact")
                .map("Email", "CustomerEmail", true, true, false)
                .saveToTranscript("ContactId")
                .showOnCreate()
                .linkToEntity("Case", "ContactId");
        } else if (chatDetails.ldRecordTypeId == '') {
            liveagent
                .findOrCreate("Lead")
                .map("Email", "CustomerEmail", false, false, true)
                .map("LastName", "CustomerLastName", false, false, true)
                .map("LeadSource", "System Subject", false, false, true)
                .map("Language_Preference__c", "LanguagePreferenceLabel", false, false, true)
                .map("Division_Name__c", "DivisionName", false, false, true)
                .saveToTranscript("LeadId")
                .showOnCreate()
                .linkToEntity("Case","Lead__c");
        } else {
            liveagent
                .findOrCreate("Lead")
                .map("Email", "CustomerEmail", true, true, true)
                .map("RecordTypeId", "Lead Record Type", true, true, true)
                .map("LastName", "CustomerLastName", false, false, true)
                .map("LeadSource", "System Subject", false, false, true)
                .map("Language_Preference__c", "LanguagePreferenceLabel", false, false, true)
                .map("Division_Name__c", "DivisionName", false, false, true)
                .saveToTranscript("LeadId")
                .showOnCreate()
                .linkToEntity("Case","Lead__c");
        }
        var liveAgentChatUrl = 'https://' + constants.API_ROOT + '/chat';
        console.log('liveAgentChatUrl: ' + liveAgentChatUrl);
        liveagent.init(
            liveAgentChatUrl,
            constants.DEPLOY_PARAM_1,
            constants.DEPLOY_PARAM_2
        );
    },

    tryChat : function() {
        if (this.automaticConnection) {
            this.safeStartDefaultChat();
        } else {
            this.safeRefresh();
        }
    },

    safeStartDefaultChat : function() {
        this.safeStartChat(this.defaultChatButtonId);
    },

    refresh : function() {
        $('#loader').show();
        $('#LanguagePreference').val(this.defaultLanguage);
        $('#refreshForm').submit();
    },

    startChat: function(chatId) {
        console.log('liveagent.startChat, chatId: ' + chatId);
        liveagent.startChat(chatId);
        window.close();
        if (!window.closed) {
            window.location.href = constants.HELP_PORTAL_URL;
        }
    },

    safeStartChat : function(chatId) {
        if (!this.validChatDetails) {
            document.getElementById("chatLangSelectionForm").submit();
            return;
        }
        //else...
        $('#liveagent_button_online_' + chatId).hide();
        console.log('safeStartChat -> invoke checkLanguageIsAvailable');
        console.log('automaticConnection: ' + this.automaticConnection);
        var language = (this.automaticConnection)
            ? this.defaultLanguage
            : this.languagePreference;
        console.log('safeStartChat -> language: ' + language);
        var self = this;
        this.checkLanguageIsAvailable(language).then(
            function(result) {
                console.log('safeStartChat result: ' + result);
                if (result) {
                    self.startChat(chatId);
                } else {
                    $('#chatButtonTopPanel').hide();
                    if (self.automaticConnection) {
                        self.refreshNoAgentsPanelMsg();
                    }
                    $('#noAgentsPanel').show();
                }
            }
        ).catch(function(error) {
            var errorMsg = error || '';
            console.error('safeStartChat -> fail: ' + errorMsg);
        });
    },

    safeRefresh : function() {
        var self = this;
        this.checkLanguageIsAvailable(this.defaultLanguage).then(
            function(result) {
                console.log('safeRefresh result: ' + result);
                if (result) {
                    self.refresh();
                } else {
                    self.refreshNoAgentsPanelMsg();
                }
            }
        ).catch(function(error) {
            var errorMsg = error || '';
            console.error('safeRefresh -> fail: ' + errorMsg);
        });
    },

    checkLanguageIsAvailable : function(language) {
        console.log('checkLanguageIsAvailable -> param: ' + language);
        var action = 'LiveChatBySkillsSelectionCon.checkLanguageIsAvailable';
        return this.runServerAction(this, action, language, 'loader');
    },

    runServerAction : function(owner, method, param, spinnerId) {
        return new Promise(function(resolve, reject) {
            if (spinnerId) {
                $('#' + spinnerId).show();
            }
            Visualforce.remoting.Manager.invokeAction(
                method, 
                param,
                function(result, event){
                    if (spinnerId) {
                        $('#' + spinnerId).hide();
                    }
                    if (event.status){
                        console.log('runServerAction event.status: ' + event.status);
                        console.log('runServerAction result: ' + result);
                        resolve.call(owner, result);
                    } else {
                        console.log('runServerAction event.message: ' + event.message);
                        reject.call(owner, event.message);
                    }                   
                }, 
                {escape: false, buffer: false, timeout: 120000}
            );
        });
    }
    
};