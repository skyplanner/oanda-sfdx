/**
 * @File Name          : ChatCommonScript.js
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 3/3/2021, 11:59:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    3/3/2021, 11:59:36 PM   acantero     Initial Version
**/

var pageUIHelper = {

    linkMargins : function(sourceId, targetId, persistent) {
        this.cloneMargins(sourceId, targetId);
        var self = this;
        if (persistent) {
            $(window).on('resize', function () { 
                self.cloneMargins(sourceId, targetId);      
            }); 
        }
    },

    cloneMargins : function(sourceId, targetId) {
        var leftMargin = $("#" + sourceId).css('margin-left');  
        var rightMargin = $("#" + sourceId).css('margin-right');  
        //console.log('linkElementsMargins -> left: ' + leftMargin + ', right: ' + rightMargin);                 
        $("#" + targetId).css('margin-left', leftMargin);                   
        $("#" + targetId).css('margin-right', rightMargin);
    }

};
