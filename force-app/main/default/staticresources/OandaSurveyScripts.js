/**
 * 
 * @param {*} idForm 
 */
$(function() {
	$.survey = {
		isMobile: $("#isMobile").val() == "true",
		checkSelection: function(index) {
			$("#spinner" + index).removeClass("d-none");
			checkSelection(index);	
		},
		showSpinner: function() {
			$("html,body").animate({"scrollTop": 0}, "normal");
			$("#spinner").removeClass("d-none");
		}
	};

	// we need to make sure that, if we are in a browser,
	// there is room for the whole CX surve..
	if (!$.survey.isMobile)
		window.resizeTo(600, 690);
});

