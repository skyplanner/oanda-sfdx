
var FAQ_DETAIL_URL = '/apex/FaqDetail';
var ID_PARAM = 'id';
var CONSOLE_PARAM = 'capp';
var CONSOLE_PARAM_VALUE = '0';
var URL_NAME_PARAM = 'urlName';
var LANGUAGE_PARAM = 'language';

angular.module("FaqDetail", [
	"ngRoute",
	"ngSanitize",
	"ngCookies"
])
.factory("$helper", [
	"$q",
function($q) {
	var $helper = {
		scrollTop: function() {
			$("html,body").animate({
				"scrollTop": 0
			}, 200);
		}
	};
	return $helper;
}])
.run([
	"$helper",
	"$rootScope",
	"$location",
	"$window",
	function($helper, $rootScope, $location, $window) {
	}
])
.controller("mainCtrl", [
	"$scope",
	"$helper",
	"$location",
	"$timeout",
	"$window",
	"$cookies",
	function($scope, $helper, $location, $timeout, $window, $cookies) {
	}
])
.directive("anchor", [
	"$anchorScroll",
	"$location",
	function($anchorScroll, $location) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				var href = $attrs.href.replace("#", "");
				// we override the href foor good
				// $elem.attr("href", "javascript:void(0)");
				// we also need to make sure the target attr does not take us
				// to another page
				$elem.removeAttr("target");

				// anchor links should use this function
				$elem.on("click", function(e) {
					e.preventDefault();
					$location.hash(href);
					$anchorScroll();
					/*
					var element = document.getElementById(href);
					if (element) {
						console.log('element to scroll exists: ' + href);
						element.scrollIntoView();
					} else {
						console.log('element to scroll does not exists: ' + href);
					}
					*/
				});
			}
		};
	}
])
.directive("openChat", [
	"$window",
	function($window) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				$elem.on("click", function(e) {
					e.preventDefault();
				});
			}
		};
	}
])
.directive("art", [
	"$location",
	function($location) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				var articleUrlName = $attrs.href.replace("#", "");
				// we override the href foor good
				$elem.attr("href", "javascript:void(0)");
				// we also need to make sure the target attr does not take us
				// to another page
				$elem.removeAttr("target");
				
				// article links should use this function
				$elem.on("click", function(e) {
					e.preventDefault();
					var consoleApp = $("#consoleApp").val();
					if (consoleApp == '1') {
						var baseURL = $("#baseURL").val();
						var articleId = $("#articleId").val();
						var messageData = articleId + articleUrlName;
						parent.postMessage(messageData, baseURL);
					} else {
						var language = $("#language").val();
						var articleInNewTabUrl = FAQ_DETAIL_URL + '?' + 
							URL_NAME_PARAM + '=' + articleUrlName + '&' + 
							LANGUAGE_PARAM + '=' + language;
						window.open(articleInNewTabUrl, '_blank');
					}
					
				});
			}
		};
	}
])
.directive("wrapTable", [
	"$anchorScroll",
	"$location",
	function($anchorScroll, $location) {
		return {
			restrict: 'A',
			link: function($scope, $elem, $attrs) {
				$elem.wrap(
					'<div class="table-container border overflow-auto mb-5"></div>');
			}
		};
	}
]);
